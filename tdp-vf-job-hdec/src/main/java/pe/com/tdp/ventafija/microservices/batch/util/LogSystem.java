package pe.com.tdp.ventafija.microservices.batch.util;

import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Funciones para grabar logs
 *
 * @author Jhair Lozano
 * @date 2017-12-07
 */
public class LogSystem {

    /**
     * Función para grabar log de error
     *
     * @author Jhair Lozano
     * @date 2017-12-07
     * @Logger logger       Hereda Configuración de la Clase
     * @String service      Nombre del método
     * @String variable     Nombre de la variable
     * @Object obj          Request recibido por el método
     */
    public static void error(Logger logger, String clase, String metodo, String exception, Exception e) {
        logger.error(clase + "::" + metodo + " => " + exception, e);
    }

    /**
     * Función para grabar log de los objetos en forma de json
     *
     * @author Jhair Lozano
     * @date 2017-12-07
     * @Logger logger       Hereda Configuración de la Clase
     * @String service      Nombre del método
     * @String variable     Nombre de la variable
     * @Object obj          Request recibido por el método
     */
    public static void info(Logger logger, String clase, String metodo, String info) {
        logger.info(clase + "::" + metodo + " => " + info);
    }

    /**
     * Función para grabar log de los objetos en forma de json
     *
     * @author Jhair Lozano
     * @date 2017-12-07
     * @Logger logger       Hereda Configuración de la Clase
     * @String service      Nombre del método
     * @String variable     Nombre de la variable
     * @Object obj          Request recibido por el método
     */
    public static void infoString(Logger logger, String clase, String metodo, String variable, String info) {
        logger.info(clase + "::" + metodo + " => " + variable + ": " + info);
    }

    /**
     * Función para grabar log de los objetos en forma de json
     *
     * @author Jhair Lozano
     * @date 2017-12-07
     * @Logger logger       Hereda Configuración de la Clase
     * @String service      Nombre del método
     * @String variable     Nombre de la variable
     * @Object obj          Request recibido por el método
     */
    public static void infoObject(Logger logger, String clase, String metodo, String variable, Object obj) {
        JSONObject jsonResponse = new JSONObject();
        if (obj != null)
            jsonResponse = new JSONObject(obj);
        logger.info(clase + "::" + metodo + " => " + variable + ": " + jsonResponse.toString());
    }

    /**
     * Función para grabar log de las listas en forma de json
     *
     * @author Jhair Lozano
     * @date 2017-12-07
     * @Logger logger       Hereda Configuración de la Clase
     * @String service      Nombre del método
     * @String variable     Nombre de la variable
     * @Object obj          Request recibido por el método
     */
    public static void infoArray(Logger logger, String clase, String metodo, String variable, List obj) {
        JSONArray jsonResponse = new JSONArray();
        if (obj != null)
            jsonResponse = new JSONArray(obj);
        logger.info(clase + "::" + metodo + " => " + variable + "(" + jsonResponse.length() + "): " + jsonResponse.toString());
    }

    /**
     * Función para grabar el query del dao
     *
     * @author Jhair Lozano
     * @date 2017-12-07
     * @Logger logger       Hereda Configuración de la Clase
     * @String stmt         Sentendia del PreparedStatement
     */
    public static void infoQuery(Logger logger, String clase, String metodo, String variable, String stmt) {
        logger.info(clase + "::" + metodo + " => " + variable + ": " + getQuery(stmt));
    }

    public static String getQuery(String query) {
        int position = 0;

        if (query.indexOf("SELECT") >= 0) {
            position = query.indexOf("SELECT");
        } else if (query.indexOf("INSERT") >= 0) {
            position = query.indexOf("INSERT");
        } else if (query.indexOf("UPDATE") >= 0) {
            position = query.indexOf("UPDATE");
        }

        return query.substring(position);
    }
}
