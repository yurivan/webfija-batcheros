package pe.com.tdp.ventafija.microservices.batch.hdec;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.engine.jdbc.batch.spi.Batch;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.batch.config.BatchConfig;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaBean;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.HDECEnvioProcessor;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Scheduler de la contingencia HDEC. El cron es configurable en base datos y no soporta cambios en caliente.
 *
 * @author glazaror
 * @since 1.5
 */
@Component
public class ContingenciaProcessorScheduler {

    private static final Logger logger = LogManager.getLogger(ContingenciaProcessorScheduler.class);

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job hdecContingenciaProcessor;

    @Autowired
    private BatchConfig batchConfig;

    @Autowired
    private HDECEnvioProcessor hdecEnvioProcessor;

    @Value("${contingenciahdec.batch.sync.flag}")
    private String flagReintentoHdec;

    //@Scheduled(cron = "0 */1 * * * ?")
    @Scheduled(cron = "${contingenciahdec.batch.sync.cron}")
    public void startJob() {

        try {
            LogSystem.info(logger, "ContingenciaProcessorScheduler", "startJob", "Starting hdec contingencia sync..!");


            if (flagReintentoHdec.equals("1")) {

                //Listar las ventas
                ArrayList<VentaBean> ventaBean = batchConfig.getVentasDB();
                VentaBean ventaBeanUnit;
                //Procesar las ventas
                for (VentaBean vb : ventaBean) {
                    ventaBeanUnit = hdecEnvioProcessor.process(vb);
                    batchConfig.updateStatusVenta(ventaBeanUnit);
                }


                LogSystem.info(logger, "ContingenciaProcessorScheduler", "finJob", "Finish hdec contingencia sync..!");

            } else {
                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                c.add(Calendar.DAY_OF_MONTH, 1);
                c.add(Calendar.MONTH, 11);
                c.add(Calendar.HOUR_OF_DAY, 1);
                Date job = c.getTime();

                logger.info("c.get(Calendar.DAY_OF_MONTH) " + c.get(Calendar.DAY_OF_MONTH));
                logger.info("c.get(Calendar.MONTH) " + c.get(Calendar.MONTH));
                logger.info("c.get(Calendar.HOUR_OF_DAY) " + c.get(Calendar.HOUR_OF_DAY));

                JobParameters params = new JobParametersBuilder().addLong("time", job.getTime()).toJobParameters();
                JobExecution execution = jobLauncher.run(hdecContingenciaProcessor, params);
                LogSystem.info(logger, "ContingenciaProcessorScheduler", "startJob",
                        String.format("Contingencia batch excecution started at %s and finished at %s wit status %s", execution.getStartTime(), execution.getEndTime(), execution.getExitStatus()));

            }


        } catch (Exception e) {
            logger.error("Error startJob. ", e);
        }
    }
}
