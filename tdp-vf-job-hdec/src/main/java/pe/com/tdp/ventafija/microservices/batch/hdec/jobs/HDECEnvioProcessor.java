package pe.com.tdp.ventafija.microservices.batch.hdec.jobs;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaBean;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.HDECService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextImpl;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

import java.util.Arrays;
import java.util.List;

/**
 * Implementacion Processor de Spring Batch para la contingencia HDEC.
 * Se evaluan todas las ventas que se encuentran en estado 'ENVIANDO' y que se encuentren en dicho estado
 * un tiempo considerado excesivo (configurado en base datos).
 * Cada venta es enviada nuevamente a HDEC, se manejan los siguientes escenarios:
 * <ul>
 * <li> Si es que la respuesta es SATISFACTORIA o DUPLICADO entonces la venta es actualizada a estado PENDIENTE.
 * <li> Si es que la respuesta es un ERROR entonces se volvera a intentar el envio controlado por un limite de reintentos.
 * <li> Si es que la respuesta es un ERROR y ya se llego al limite de reintentos entonces se actualiza la venta con estado 'CAIDA'.
 * </ul>
 *
 * @author glazaror
 * @since 1.5
 */
public class HDECEnvioProcessor implements ItemProcessor<VentaBean, VentaBean> {

    private static final Logger logger = LogManager.getLogger(HDECEnvioProcessor.class);

    private HDECService hdecService;
    private Integer cantidadReintentosLimite;
    private List<String> mensajesErrorProcesandoAsList;

    public HDECEnvioProcessor(HDECService hdecService, Integer cantidadReintentosLimite, String mensajesErrorProcesando) {
        this.hdecService = hdecService;
        this.cantidadReintentosLimite = cantidadReintentosLimite;
        this.mensajesErrorProcesandoAsList = Arrays.asList(mensajesErrorProcesando.split(","));
    }

    @Override
    public VentaBean process(final VentaBean ventaBean) throws Exception {
        LogSystem.info(logger, "HDECEnvioProcessor", "process", "Start");
        LogSystem.infoObject(logger, "HDECEnvioProcessor", "process", "VentaBean", ventaBean);
        ventaBean.setCantidadIntentosEnvioHDEC(ventaBean.getCantidadIntentosEnvioHDEC() + 1);

        try {
            startCallContext(ventaBean);
            if ((this.cantidadReintentosLimite + 1) == ventaBean.getCantidadIntentosEnvioHDEC()) {
                // De tratarse de otro error y llegar al limite maximo de intentos
                ventaBean.setEstado("CAIDA");
            } else {
                hdecService.sendDirectToHDEC(ventaBean.getId());
            }
            endCallContext();
            ventaBean.setEstado("PENDIENTE");

        } catch (ClientException e) {
            LogSystem.error(logger, "HDECEnvioProcessor", "process", "ClientException", e);
            // Si es que la respuesta de error contiene el texto "CODIGO UNICO ya existe" entonces actualizamos
            // el estado de la venta a 'PENDIENTE'
            if (e.getMessage() != null && containsMensajeProcesando(e.getMessage())) {
                ventaBean.setEstado("PENDIENTE");

            } else if (cantidadReintentosLimite.equals(ventaBean.getCantidadIntentosEnvioHDEC())) {
                // De tratarse de otro error y llegar al limite maximo de intentos
                ventaBean.setEstado("CAIDA");
            }

        } catch (Exception ex) {
            LogSystem.error(logger, "HDECEnvioProcessor", "process", "Exception", ex);
            if (cantidadReintentosLimite.equals(ventaBean.getCantidadIntentosEnvioHDEC())) {
                // De tratarse de otro error y llegar al limite maximo de intentos
                ventaBean.setEstado("CAIDA");
            }
        }
        LogSystem.infoObject(logger, "HDECEnvioProcessor", "process", "VentaBean", ventaBean);
        LogSystem.info(logger, "HDECEnvioProcessor", "process", "End");
        return ventaBean;
    }

    private boolean containsMensajeProcesando(String mensajeError) {
        for (String mensajeErrorProcesando : mensajesErrorProcesandoAsList) {
            if (mensajeError.contains(mensajeErrorProcesando)) {
                return true;
            }
        }
        return false;
    }

    public void startCallContext(VentaBean ventaBean) {

        ServiceCallEvent event = new ServiceCallEvent();
        event.setUsername(ventaBean.getCodigoVendedor());
        event.setOrderId(ventaBean.getId());
        event.setServiceCode("HDEC");
        event.setDocNumber(ventaBean.getDniCliente());
        event.setSourceApp("BATCH");
        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);
    }

    private void endCallContext() {
        VentaFijaContextHolder.clearContext();
    }

}