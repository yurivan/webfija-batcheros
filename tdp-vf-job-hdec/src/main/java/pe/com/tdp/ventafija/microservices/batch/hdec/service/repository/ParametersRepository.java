package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.Parameters;

@Repository
public interface ParametersRepository extends JpaRepository<Parameters, Integer> {

	Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element);

}
