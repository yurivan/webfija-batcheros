package pe.com.tdp.ventafija.microservices.batch.hdec.service.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "order", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema
@NamedNativeQueries({
        //@NamedNativeQuery(name = "spRetriveHdecData", query = "call sp_retrive_hdec_data(:inparam)"),

        @NamedNativeQuery(name = "spRetriveHdecData", query = "select "
                + "ORD.id,"
                + "CUS.firstName,"
                + "CUS.lastName1,"
                + "CUS.lastName2,"
                + "CUS.docType,"
                + "CUS.docNumber,"
                + "CUS.email,"
                + "ODET.sendContracts,"
                + "ODET.dataProtection,"
                + "VEN.canal,"
                + "VEN.entidad,"
                + "VEN.nombre,"
                + "VEN.codATIS,"
                + "VEN.codCMS,"
                + "VEN.departamento,"
                + "VEN.provincia,"
                + "ORD.productName cat,"
                + "ORD.productName,"
                + "ODET.department,"
                + "ODET.province,"
                + "ODET.district,"
                + "ORD.commercialOperation,"
                + "ORD.migrationPhone as telefono_migrar,"
                + "ODET.disableWhitePage,"
                + "ODET.enableDigitalInvoice,"
                + "ODET.internetTech,"
                + "ODET.expertoCode,"
                + "CAST('SI' AS varchar(2)) as tiene_grabacion,"
                + "ORD.registeredTime registeredTime1,"
                + "ORD.registeredTime registeredTime2,"
                + "ORD.paymentMode,"
                + "ORD.ID as ID2,"
                + "ODET.tvTech,"
                + "CAST('NO' AS varchar(2)) as no_result,"
                + "COALESCE(ORD.paquetizacion, 'NO') as paquetizacion,"
                + "ORD.equipmentDecoType,"
                + "VEN.dni,"
                + "USR.phone,"
                + "ORD.serviceType as service_type,"
                + "VEN.distrito,"
                + "ODET.decosSD,"
                + "ODET.decosHD,"
                + "ODET.decosDVR,"
                + "ODET.blockTV as bloque_producto,"
                + "ODET.svaInternet,"
                + "ODET.svaLine,"
                + "CAST(' ' AS varchar(2)) as descuento_winback,"
                + "ODET.tvBlock,"
                + "ODET.blockTV,"
                + "CUS.customerPhone,"
                + "CUS.customerPhone2,"
                + "CAST( CONCAT(ODET.address , ' - ' , coalesce(ODET.addressComplement,''))  AS varchar(200) ),"
                + "ORD.campaign,"
                + "ORD.productType,"
                + "ORD.cmsCustomer,"
                + "ORD.cmsServiceCode,"
                + "ODET.altasTV,"
                + "ORD.coordinatex,"
                + "ORD.coordinatey,"
                + "ODET.parentalprotection,"
                + "ORD.cashprice,"
                + "CAST('' AS VARCHAR(1)) as codigopostal,"
                + "ODET.publicationwhitepages, "
                + "ORD.productcategory, "
                + "COALESCE(ORD.appcode, '') as appcode, "
                + "COALESCE(COUNTRY.country_name, 'Perú') as nationality,"
                + "ODET.internetrsw, "
                + "PPE.codpago, "
                + "ORD.whatsapp, "
                + "ODET.automaticdebit, "
                + "PPE.fechapago, "
                /*SPRINT 24 RUC INICIO*/
                + "CUS.doctyperrll, "
                + "CUS.docnumberrrll, "
                + "CUS.fullnamerrll, "
                /*SPRINT 24 RUC FIN*/
                /*SPRINT 24 FLAG_RECUPERO_CAIDA INICIO*/
                + "VI.id_transaccion, "
                + "(CASE WHEN ORD.recupero_venta = '1' THEN true ELSE false END) as flg_recupero_venta "
                /*SPRINT 24 FLAG_RECUPERO_CAIDA FIN*/
                + " from "
                + " ibmx_a07e6d02edaf552.order ORD"
                + " JOIN ibmx_a07e6d02edaf552.customer CUS ON (ORD.customerID = CUS.ID)"
                + " JOIN ibmx_a07e6d02edaf552.order_detail ODET ON (ORD.ID = ODET.ORDERID )"
                + " JOIN ibmx_a07e6d02edaf552.tdp_sales_agent VEN ON (ORD.userID = VEN.codATIS)"
                + " JOIN ibmx_a07e6d02edaf552.user USR ON (ORD.userID = USR.ID)"
                + " LEFT JOIN ibmx_a07e6d02edaf552.peticion_pago_efectivo PPE ON (ORD.ID = PPE.ORDER_ID)"
                + " LEFT JOIN ibmx_a07e6d02edaf552.tdp_country COUNTRY ON COUNTRY.country_id = CUS.nationality"
                + " LEFT JOIN ibmx_a07e6d02edaf552.tdp_visor VI ON (VI.id_visor = ORD.ID)"
                + " WHERE ORD.ID = ?1 "),
        //@Query("select od.id from OrderDetail od where od.orderId = ?1") //glazaror prueba

        @NamedNativeQuery(name = "spMigrateVisor", query = "call sp_migrate_visor(:inparam)"),
        //@NamedNativeQuery(name = "spGetUserOrders", query = "call getUserOrders(:inParamUserId, :inParamNumDays)") //sprint3

        // Sprint 7 - Se exluyen los registros con estado 'GENERANDO_CIP'
        @NamedNativeQuery(name = "spGetUserOrders", query = ""
                + "SELECT v.ESTADO_SOLICITUD, CAST(COUNT(*) AS INTEGER)"
                + " FROM ibmx_a07e6d02edaf552.order o"
                + " JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerID"
                + " LEFT JOIN ibmx_a07e6d02edaf552.tdp_visor v ON v.id_visor = o.id"
                + " LEFT JOIN ibmx_a07e6d02edaf552.order_detail od ON od.orderId = o.id"
                + " WHERE o.userId = ?1 AND o.registrationDate > date_trunc('day', (CURRENT_TIMESTAMP - interval '30' day))"
                + " AND (v.ESTADO_SOLICITUD <> 'GENERANDO_CIP' OR v.ESTADO_SOLICITUD IS NULL)"
                + " GROUP BY v.ESTADO_SOLICITUD"
                + "")
})


public class Order {
    @Id
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerid")
    private Customer customer;

    @Column(name = "commercialoperation")
    private String commercialOperation;
    @Column(name = "producttype")
    private String productType;
    @Column(name = "productcategory")
    private String productCategory;
    @Column(name = "productcode")
    private String productCode;
    @Column(name = "paymentmode")
    private String paymentMode;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "`status`")
    private String status;
    @Column(name = "statuslegacy")
    private String statusLegacy;
    @Column(name = "registrationdate")
    private Date registrationDate;
    @Column(name = "canceldescription")
    private String cancelDescription;
    @Column(name = "coordinatex")
    private BigDecimal coordinateX;
    @Column(name = "coordinatey")
    private BigDecimal coordinateY;
    @Column(name = "migrationphone")
    private String migrationPhone;
    @Column(name = "equipmentdecotype")
    private String equipmentDecoType;
    @Column(name = "sourcephone")
    private String sourcePhone;
    @Column(name = "servicetype")
    private String serviceType;
    @Column(name = "cmscustomer")
    private String cmsCustomer;
    @Column(name = "cmsservicecode")
    private String cmsServiceCode;
    @Column(name = "registeredtime")
    private Date registeredTime;
    @Column(name = "lastupdatetime")
    private Date lastUpdateTime;
    @Column(name = "campaign")
    private String campaign;
    @Column(name = "productname")
    private String productName;
    @Column(name = "promprice")
    private BigDecimal promPrice;
    @Column(name = "cashprice")
    private BigDecimal cashPrice;
    @Column(name = "linetype")
    private String lineType;
    @Column(name = "monthperiod")
    private Integer monthPeriod;
    @Column(name = "financingmonth")
    private Integer financingMonth;
    @Column(name = "financingcost")
    private BigDecimal financingCost;

    // Sprint 6 - Se adiciona campo para diferenciar el origen de la venta
    @Column(name = "appcode")
    private String appcode;

    // Sprint 9 - Se adiciona campo para diferenciar por producto al cual pertenece el SVA (solo flujo SVA)
    @Column(name = "sourceproductname")
    private String sourceProductName;

    public String getSourceProductoName() {
        return sourceProductName;
    }

    public void setSourceProductName(String sourceProductName) {
        this.sourceProductName = sourceProductName;
    }

    public String getAppcode() {
        return appcode;
    }

    public void setAppcode(String appcode) {
        this.appcode = appcode;
    }

    // Sprint 5 - propiedades adicionales nombrePuntoVenta y entidad
    @Column(name = "nompuntoventa")
    private String nombrePuntoVenta;

    @Column(name = "entidad")
    private String entidad;

    //sprint 3 - se adiciona propiedad statusAudio
    @Column(name = "statusaudio")
    private String statusAudio;

    public String getWatsonRequest() {
        return watsonRequest;
    }

    public void setWatsonRequest(String watsonRequest) {
        this.watsonRequest = watsonRequest;
    }

    @Column(name = "watsonrequest")
    private String watsonRequest;

    //Campo para la cantidad de cada estado del usuario
//	@Column(name = "quantity")
//	private String quantity;


    /* Sprint 10 */
    @Column(name = "promspeed")
    private Integer promoSpeed;
    @Column(name = "periodpromspeed")
    private Integer periodoPromoSpeed;
    @Column(name = "equiplinea")
    private String equipamientoLinea;
    @Column(name = "equipinternet")
    private String equipamientoInternet;
    @Column(name = "equiptv")
    private String equipamientoTv;
    /* Sprint 10 */

    /* Sprint 16 */
    @Column(name = "paquetizacion")
    private String paquetizacion;
    /* Sprint 16 */

    @Column(name = "whatsapp")
    private Integer whatsapp;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCommercialOperation() {
        return commercialOperation;
    }

    public void setCommercialOperation(String commercialOperation) {
        this.commercialOperation = commercialOperation;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusLegacy() {
        return statusLegacy;
    }

    public void setStatusLegacy(String statusLegacy) {
        this.statusLegacy = statusLegacy;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getCancelDescription() {
        return cancelDescription;
    }

    public void setCancelDescription(String cancelDescription) {
        this.cancelDescription = cancelDescription;
    }

    public BigDecimal getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(BigDecimal coordinateX) {
        this.coordinateX = coordinateX;
    }

    public BigDecimal getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(BigDecimal coordinateY) {
        this.coordinateY = coordinateY;
    }

    public String getMigrationPhone() {
        return migrationPhone;
    }

    public void setMigrationPhone(String migrationPhone) {
        this.migrationPhone = migrationPhone;
    }

    public String getEquipmentDecoType() {
        return equipmentDecoType;
    }

    public void setEquipmentDecoType(String equipmentDecoType) {
        this.equipmentDecoType = equipmentDecoType;
    }

    public String getSourcePhone() {
        return sourcePhone;
    }

    public void setSourcePhone(String sourcePhone) {
        this.sourcePhone = sourcePhone;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getCmsCustomer() {
        return cmsCustomer;
    }

    public void setCmsCustomer(String cmsCustomer) {
        this.cmsCustomer = cmsCustomer;
    }

    public String getCmsServiceCode() {
        return cmsServiceCode;
    }

    public void setCmsServiceCode(String cmsServiceCode) {
        this.cmsServiceCode = cmsServiceCode;
    }

    public Date getRegisteredTime() {
        return registeredTime;
    }

    public void setRegisteredTime(Date registeredTime) {
        this.registeredTime = registeredTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPromPrice() {
        return promPrice;
    }

    public void setPromPrice(BigDecimal promPrice) {
        this.promPrice = promPrice;
    }

    public BigDecimal getCashPrice() {
        return cashPrice;
    }

    public void setCashPrice(BigDecimal cashPrice) {
        this.cashPrice = cashPrice;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public Integer getMonthPeriod() {
        return monthPeriod;
    }

    public void setMonthPeriod(Integer monthPeriod) {
        this.monthPeriod = monthPeriod;
    }

    public Integer getFinancingMonth() {
        return financingMonth;
    }

    public void setFinancingMonth(Integer financingMonth) {
        this.financingMonth = financingMonth;
    }

    public BigDecimal getFinancingCost() {
        return financingCost;
    }

    public void setFinancingCost(BigDecimal financingCost) {
        this.financingCost = financingCost;
    }

    @PrePersist
    public void onPrePersist() {
        this.registeredTime = new Date();
    }

    @PreUpdate
    public void onPreUpdate() {
        this.lastUpdateTime = new Date();
    }

    // Sprint 3 - estado del audio
    public String getStatusAudio() {
        return statusAudio;
    }

    public void setStatusAudio(String statusAudio) {
        this.statusAudio = statusAudio;
    }

    public String getNombrePuntoVenta() {
        return nombrePuntoVenta;
    }

    public void setNombrePuntoVenta(String nombrePuntoVenta) {
        this.nombrePuntoVenta = nombrePuntoVenta;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Integer getPromoSpeed() {
        return promoSpeed;
    }

    public void setPromoSpeed(Integer promoSpeed) {
        this.promoSpeed = promoSpeed;
    }

    public Integer getPeriodoPromoSpeed() {
        return periodoPromoSpeed;
    }

    public void setPeriodoPromoSpeed(Integer periodoPromoSpeed) {
        this.periodoPromoSpeed = periodoPromoSpeed;
    }

    public String getEquipamientoLinea() {
        return equipamientoLinea;
    }

    public void setEquipamientoLinea(String equipamientoLinea) {
        this.equipamientoLinea = equipamientoLinea;
    }

    public String getEquipamientoInternet() {
        return equipamientoInternet;
    }

    public void setEquipamientoInternet(String equipamientoInternet) {
        this.equipamientoInternet = equipamientoInternet;
    }

    public String getEquipamientoTv() {
        return equipamientoTv;
    }

    public void setEquipamientoTv(String equipamientoTv) {
        this.equipamientoTv = equipamientoTv;
    }

    public String getPaquetizacion() {
        return paquetizacion;
    }

    public void setPaquetizacion(String paquetizacion) {
        this.paquetizacion = paquetizacion;
    }

    public Integer getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(Integer whatsapp) {
        this.whatsapp = whatsapp;
    }
}
