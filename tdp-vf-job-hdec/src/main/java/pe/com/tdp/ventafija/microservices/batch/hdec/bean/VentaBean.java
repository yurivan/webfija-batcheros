package pe.com.tdp.ventafija.microservices.batch.hdec.bean;

import java.util.Date;

/**
 * Bean que representa una Venta y su estado de envio al BACK (tambien conocido como HDEC)
 * @author      glazaror
 * @since       1.5
 */
public class VentaBean {
    private String id;
    private String estado;
    private Date fechaGrabacion;
    private Integer cantidadIntentosEnvioHDEC;

    private String dniCliente;
    private String codigoVendedor;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaGrabacion() {
        return fechaGrabacion;
    }

    public void setFechaGrabacion(Date fechaGrabacion) {
        this.fechaGrabacion = fechaGrabacion;
    }

    public Integer getCantidadIntentosEnvioHDEC() {
        return cantidadIntentosEnvioHDEC;
    }

    public void setCantidadIntentosEnvioHDEC(Integer cantidadIntentosEnvioHDEC) {
        this.cantidadIntentosEnvioHDEC = cantidadIntentosEnvioHDEC;
    }

    public String getDniCliente() {
        return dniCliente;
    }

    public void setDniCliente(String dniCliente) {
        this.dniCliente = dniCliente;
    }

    public String getCodigoVendedor() {
        return codigoVendedor;
    }

    public void setCodigoVendedor(String codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }
}
