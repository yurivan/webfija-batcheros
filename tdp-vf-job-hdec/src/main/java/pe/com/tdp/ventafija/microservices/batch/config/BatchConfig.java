package pe.com.tdp.ventafija.microservices.batch.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import pe.com.tdp.ventafija.microservices.batch.hdec.ContingenciaProcessorScheduler;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaBean;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper.VentaRowMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.AzureRequestProcessorSkipPolicy;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.HDECEnvioProcessor;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.HDECEnvioProcessorListener;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.HDECService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    private static final Logger logger = LogManager.getLogger(BatchConfig.class);

    @Autowired
    private DataSource dataSource;
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private HDECService hdecService;

    @Value("${contingenciahdec.limitereintentos}")
    private Integer cantidadReintentos;

    @Value("${contingenciahdec.minutosestadoenviando}")
    private Integer minutosEnEstadoEnviando;

    @Value("${hdec.mensajeerror.procesando}")
    private String hdecMensajesProcesando;

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new ResourcelessTransactionManager();
    }

    @Bean
    public JobRepository jobRepository() throws Exception {
        return new MapJobRepositoryFactoryBean(transactionManager()).getObject();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        SyncTaskExecutor executor = new SyncTaskExecutor();
        return executor;
    }

    @Bean
    public SimpleJobLauncher jobLauncher() throws Exception {
        SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        simpleJobLauncher.setJobRepository(jobRepository());
        simpleJobLauncher.setTaskExecutor(taskExecutor());
        return simpleJobLauncher;
    }

    @Bean
    public ContingenciaProcessorScheduler contingenciaProcessorScheduler() {
        return new ContingenciaProcessorScheduler();
    }

    @Bean
    public JdbcCursorItemReader<VentaBean> ventasReader() {
        String sql = "SELECT VIS.ID_VISOR, VIS.FECHA_GRABACION, VIS.ESTADO_SOLICITUD, VIS.CNT_INTENTOS_HDEC, VIS.DNI, ORD.USERID AS COD_ATIS " +
                "FROM ibmx_a07e6d02edaf552.TDP_VISOR AS VIS " +
                "INNER JOIN ibmx_a07e6d02edaf552.ORDER AS ORD ON ORD.ID = VIS.ID_VISOR " +
                "WHERE VIS.ESTADO_SOLICITUD = 'ENVIANDO' AND VIS.ID_VISOR LIKE '-%' " +
                "AND (VIS.FECHA_GRABACION + interval '" + minutosEnEstadoEnviando + "' minute) < (now() AT TIME ZONE 'America/Lima') " +
                "ORDER BY VIS.FECHA_GRABACION ASC";


        LogSystem.infoQuery(logger, "BatchConfig", "ventasReader", "sql", sql);

        JdbcCursorItemReader<VentaBean> reader = new JdbcCursorItemReader<VentaBean>();
        reader.setDataSource(dataSource);
        reader.setSql(sql);
        reader.setRowMapper(new VentaRowMapper());
        reader.setFetchSize(10);
        return reader;
    }

    @Bean
    public JdbcBatchItemWriter<VentaBean> ventasWriter() {
        JdbcBatchItemWriter<VentaBean> writer = new JdbcBatchItemWriter<VentaBean>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<VentaBean>());
        // Actualizamos el estado de la venta si es que la venta sigue en estado 'ENVIANDO'
        writer.setSql("UPDATE ibmx_a07e6d02edaf552.TDP_VISOR set estado_solicitud = :estado, cnt_intentos_hdec = :cantidadIntentosEnvioHDEC where id_visor = :id AND estado_solicitud = 'ENVIANDO'");
        writer.setDataSource(dataSource);
        return writer;
    }

    @Bean
    public HDECEnvioProcessor ventasProcessor() {
        return new HDECEnvioProcessor(hdecService, cantidadReintentos, hdecMensajesProcesando);
    }

    @Bean
    public Job hdecContingenciaProcessor(Step contingenciaHDECStep) throws Exception {
        return jobBuilderFactory.get("hdecContingenciaProcessor").incrementer(new RunIdIncrementer())
                .repository(jobRepository()).flow(contingenciaHDECStep).end().build();
    }

    @Bean
    public SkipPolicy hdecEnvioProcessorSkipPolicy() {
        return new AzureRequestProcessorSkipPolicy();
    }

    @Bean
    public ItemProcessListener<VentaBean, VentaBean> hdecEnvioProcessorListener() {
        return (ItemProcessListener<VentaBean, VentaBean>) new HDECEnvioProcessorListener();
    }

    @Bean
    public Step contingenciaHDECStep(HDECEnvioProcessor processor) {
        return stepBuilderFactory.get("contingenciaHDECStep")
                .<VentaBean, VentaBean>chunk(0)
                .reader(ventasReader())
                .processor(ventasProcessor())
                .faultTolerant()
                .skipPolicy(hdecEnvioProcessorSkipPolicy())
                .listener(hdecEnvioProcessorListener())
                .writer(ventasWriter())
                .build();
    }

    public ArrayList<VentaBean> getVentasDB() {
        ArrayList<VentaBean> ventaBean = new ArrayList<>();
        try (Connection con = dataSource().getConnection()) {
            String sql = "SELECT VIS.ID_VISOR, VIS.FECHA_GRABACION, VIS.ESTADO_SOLICITUD, VIS.CNT_INTENTOS_HDEC, VIS.DNI, ORD.USERID AS COD_ATIS " +
                    "FROM ibmx_a07e6d02edaf552.TDP_VISOR AS VIS " +
                    "INNER JOIN ibmx_a07e6d02edaf552.ORDER AS ORD ON ORD.ID = VIS.ID_VISOR " +
                    "WHERE VIS.ESTADO_SOLICITUD = 'ENVIANDO'  AND VIS.ID_VISOR LIKE '-%' " +
                    "ORDER BY VIS.FECHA_GRABACION ASC LIMIT 300";
            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    VentaBean v = new VentaBean();
                    v.setId(rs.getString(1));
                    v.setFechaGrabacion(rs.getDate(2));
                    v.setEstado(rs.getString(3));
                    v.setCantidadIntentosEnvioHDEC(rs.getInt(4));
                    v.setDniCliente(rs.getString(5));
                    v.setCodigoVendedor(rs.getString(6));
                    ventaBean.add(v);
                }
                rs.close();
            }
            dataSource().getConnection().close();
        } catch (Exception e) {
            logger.error("Error read Ventas.", e);
        }
        return ventaBean;
    }

    public void updateStatusVenta(VentaBean ventaBeanUnit) {
        String estado = "";
        String sql = "";
        try (Connection con = dataSource().getConnection()) {
            sql = "SELECT VIS.ID_VISOR, VIS.FECHA_GRABACION, VIS.ESTADO_SOLICITUD, VIS.CNT_INTENTOS_HDEC, VIS.DNI, ORD.USERID AS COD_ATIS " +
                    "FROM ibmx_a07e6d02edaf552.TDP_VISOR AS VIS " +
                    "INNER JOIN ibmx_a07e6d02edaf552.ORDER AS ORD ON ORD.ID = VIS.ID_VISOR " +
                    "WHERE VIS.ID_VISOR = ? ";
            try (PreparedStatement stmt = con.prepareStatement(sql)) {
                stmt.setString(1,ventaBeanUnit.getId());
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    estado = rs.getString(3);
                }
                rs.close();
            }

            boolean flag1 = estado.equals("CAIDA");
            boolean flag2 = estado.equals("PENDIENTE");

            if (flag1 || flag2) {
                logger.info("========== Actualizacion cancelada " + ventaBeanUnit.getId() +" ==========");
            }else {
                sql = "UPDATE ibmx_a07e6d02edaf552.TDP_VISOR set " +
                        "estado_solicitud = ?, " +
                        "cnt_intentos_hdec = ?" +
                        "where id_visor = ?";
                try (PreparedStatement stmt = con.prepareStatement(sql)) {
                    stmt.setString(1,ventaBeanUnit.getEstado());
                    stmt.setInt(2,ventaBeanUnit.getCantidadIntentosEnvioHDEC());
                    stmt.setString(3,ventaBeanUnit.getId());

                    int n = stmt.executeUpdate();

                    if(n>0){
                        logger.info("========== Se actualizo venta " + ventaBeanUnit.getId() +" ==========");
                    }else{
                        logger.info("========== No se actualizo venta " + ventaBeanUnit.getId()+" ==========");
                    }

                }
            }

            dataSource().getConnection().close();
        } catch (Exception e) {
            logger.error("Error conexion.", e);
        }
    }

    public DataSource dataSource() {

        HikariConfig config = new HikariConfig();

        config.setDriverClassName(System.getenv("TDP_FIJA_DB_DRIVER"));
        config.setJdbcUrl(System.getenv("TDP_FIJA_DB_URL"));
        config.setUsername(System.getenv("TDP_FIJA_DB_USR"));
        config.setPassword(System.getenv("TDP_FIJA_DB_PW"));

        config.addDataSourceProperty("ApplicationName", "Hdec-Batch");
        config.setMinimumIdle(Integer.parseInt(System.getenv("TDP_FIJA_DB_MINIMUM_IDLE")));
        config.setMaximumPoolSize(Integer.parseInt(System.getenv("TDP_FIJA_DB_POOLING")));
        config.setIdleTimeout(Integer.parseInt(System.getenv("TDP_FIJA_DB_TIMEOUT_IDLE")));

        /*if (true) {
            config.setJdbcUrl("jdbc:postgresql://sl-us-south-1-portal.13.dblayer.com:28973/compose?useSSL=false");
            config.setPassword("IXECEUBFNJLAZZEV");
        }*/

        return new HikariDataSource(config);
    }
}