package pe.com.tdp.ventafija.microservices.batch.hdec.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.Order;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.Parameters;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.OrderRepository;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.ParametersRepository;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.hdec.HdecClientErrorSimulator;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.dto.HdecApiHdecRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.HdecApiHdecResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.hdec.HdecClient;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.exception.ApplicationException;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.MessageConstants;
import pe.com.tdp.ventafija.microservices.common.util.NumberUtils;
import pe.com.tdp.ventafija.microservices.common.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class HDECService {

    private static final Logger logger = LogManager.getLogger(HDECService.class);

    @Autowired
    private ParametersRepository parametersRepository;
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private DataSource dataSource;

    @Value("${tdp.api.hdec.uri}")
    private String apiUri;
    @Value("${tdp.api.hdec.registrarpreventa.apiid}")
    private String hdecRegistrarPreVentaApiId;
    @Value("${tdp.api.hdec.registrarpreventa.apisecret}")
    private String hdecRegistrarPreVentaApiSecret;

    @Value("${tipodocumento.equivalencia.hdec.dni}")
    private String codigoDNIEquivalenteHDEC;
    @Value("${tipodocumento.equivalencia.hdec.cex}")
    private String codigoCEXEquivalenteHDEC;
    @Value("${tipodocumento.equivalencia.hdec.pas}")
    private String codigoPASEquivalenteHDEC;
    @Value("${tipodocumento.equivalencia.hdec.ruc}")
    private String codigoRUCEquivalenteHDEC;
    @Value("${tipodocumento.equivalencia.hdec.otros}")
    private String codigoOTROSEquivalenteHDEC;

    @Value("${hdec.controlerror.codigos}")
    private String codigosErrorHDEC;
    private List<String> codigosErrorControladosAsList;

    private String HDEC_DEFAULT_EMAIL = "nulo@nulo.com";
    private String HDEC_DEFAULT_TELF_CONTACTO_2 = "11111111";
    private String HDEC_DEFAULT_TELF_MIGRAR = "11111111";
    private String HDEC_DEFAULT_CLIENTE_CMS = "1111111";
    private String HDEC_DEFAULT_COD_SRV_CMS = "1111111";

    private String HDEC_DEFAULT_0 = "0";
    private String HDEC_DEFAULT_NO_APLICA = "NO APLICA";
    private String HDEC_DEFAULT_EMPTY_STRING = "";
    private String HDEC_DEFAULT_EXPERTO_CODE_NA = "N/A";

    @PostConstruct
    public void postConstruct() {
        this.codigosErrorControladosAsList = new ArrayList<String>();
        String[] codigosError = codigosErrorHDEC.split(",");
        for (String codigoError : codigosError) {
            codigosErrorControladosAsList.add("\"" + codigoError + "\"");
        }
    }

    @Transactional
    public void sendDirectToHDEC(String firebaseId) throws ApplicationException, ClientException {
        //LogSystem.info(logger,"HDECService", "sendDirectToHDEC","start");
        logger.info("Inicio SendDirectToHDEC Service.");

        Order order = orderRepository.findOne(firebaseId);
        if (order == null) {
            logger.info("sendDirectToHDEC => order: null");
            throw new ApplicationException(MessageConstants.ORDER_NOT_SAVED);
        }

        List<Object[]> data = orderRepository.spRetriveHdecData(firebaseId);
        LogSystem.infoArray(logger, "HDECService", "sendDirectToHDEC", "data", data);

        List<HdecApiHdecRequestBody> apiData = new ArrayList<>();
        for (Object[] row : data) {
            String orderId = (String) row[0];
            String firstName = row[1] != null ? (String) row[1] : "";
            String lastName1 = row[2] != null ? (String) row[2] : "";
            String lastName2 = row[3] != null ? (String) row[3] : "";

            // Sprint 7... si es que el apellido materno (lastName2) esta vacio entonces enviamos a HDEC un espacio en blanco
            if (lastName2 == null || lastName2.isEmpty()) {
                lastName2 = " ";
            }

            String docType = (String) row[4];
            String docNumber = (String) row[5];
            String email = (String) row[6];
            String sendContracts = (String) row[7];
            String dataProtection = (String) row[8];
            String canal = (String) row[9];
            String entidad = (String) row[10];
            String nombreVendedor = (String) row[11];
            String codATIS = (String) row[12];
            String codCMS = (String) row[13];
            String departamento = (String) row[14];
            String provincia = (String) row[15];
            String productCategory = (String) row[16];
            String productName = (String) row[17];
            String department = (String) row[18];
            String province = (String) row[19];
            String district = (String) row[20];
            String commercialOperation = (String) row[21];
            String telefonoMigrar = (String) row[22];
            String disableWhitepage = (String) row[23];
            String enableDigitalInvoice = (String) row[24];
            String internetTech = (String) row[25];
            String expertoCode = (String) row[26];
            String tieneMigracion = (String) row[27];
            Date registeredTime1 = (Date) row[28];
            // Date registeredTime2 = (Date) row[29]; no use
            String paymentMode = (String) row[30];
            String recordingId = (String) row[31];
            String tvTech = (String) row[32];
            String noResult = (String) row[33]; // no result
            String paquetizacion = (String) row[34];
            String equipmentDecoType = (String) row[35];
            String dniVendedor = (String) row[36];
            String telefonoOrigen = (String) row[37];
            String serviceType = (String) row[38];
            String distritoVendedor = (String) row[39];

            String decosSD = NumberUtils.isNullOrZero(row[40]) ? null : ((Integer) row[40]).toString();
            String decosHD = NumberUtils.isNullOrZero(row[41]) ? null : ((Integer) row[41]).toString();
            String decosDVR = NumberUtils.isNullOrZero(row[42]) ? null : ((Integer) row[42]).toString();

            String bloqueProducto = (String) row[43];
            String svaInternet = (String) row[44];
            String svaLine = (String) row[45];
            String descuentoWinback = (String) row[46];
            String tvBlock = (String) row[47]; // no use so far
            String blockTv = (String) row[48]; // no use so far
            String customerPhone = (String) row[49];
            String customerPhone2 = (String) row[50];
            String address = (String) row[51];
            String campaing = (String) row[52];
            String productType = (!commercialOperation.equals("SVAS") ? (String) row[53] : (productName.indexOf("PUNTO ADICIONAL") == 0 || productName.indexOf("EQUIPO") == 0 || productName.indexOf("MODEM") == 0 || productName.indexOf("SOLUCIONES") == 0 || productName.indexOf("ULTRA") == 0 ? "EQUIPOS" : "SVAS"));
            String cmsCustomer = (String) row[54];
            String cmsServiceCode = (String) row[55];
            String altasTv = (String) row[56];
            String coordenadasx = row[57] != null ? String.valueOf((BigDecimal) row[57]) : "";
            String coordenadasy = row[58] != null ? String.valueOf((BigDecimal) row[58]) : "";
            String webParental = (String) row[59];
            String montoContado = row[60] != null ? String.valueOf((BigDecimal) row[60]) : "";
            String coidgoPostal = (String) row[61];
            String publicarGuia = (String) row[62];
            String productcategory = (String) row[63];
            String orderOrigen = row[64] != null ? (String) row[64] : "";
            String clientNationality = row[65] != null ? (String) row[65] : "";
            Integer internetRsw = NumberUtils.isNullOrZero(row[66]) ? 0 : ((Integer) row[66]);
            String cod_CIP = row[67] != null ? (String) row[67] : "";
            Integer whatsapp = NumberUtils.isNullOrZero(row[68]) ? 0 : ((Integer) row[68]);

            // Afiliación al débito automático
            String automaticDebit = row[69] != null ? (String) row[69] : "";

            String fechapagoCIP = row[70] != null ? (String) row[70] : "";

            //Sprint 24 RUC
            String tipDocRrll = row[71] != null ? (String) row[71] : "";
            String docNumRrll = row[72] != null ? (String) row[72] : "";
            String nomRrll = row[73] != null ? (String) row[73] : "";

            //Sprint 24 RECUPERO CAIDA
            String id_transaccion = row[74] != null ? (String) row[74] : "";
            boolean flag_recupero = (boolean) row[75];

            address = address == null ? null : address.trim().substring(0, Math.min(199, address.trim().length()));

            HdecApiHdecRequestBody request = new HdecApiHdecRequestBody();
            request.setCodigoUnico(orderId);
            request.setCampana(campaing);
            request.setNombreDeCliente(firstName.trim().equalsIgnoreCase("") ? "-" : firstName);
            request.setApellidoPaterno(lastName1.trim().equalsIgnoreCase("") ? "-" : lastName1);
            request.setApellidoMaterno(lastName2.trim().equalsIgnoreCase("") ? "-" : lastName2);

            // Sprint 7 - verificamos las equivalencias de tipos documento en HDEC
            if ("DNI".equals(docType)) {
                docType = codigoDNIEquivalenteHDEC;
            } else if ("CEX".equals(docType)) {
                docType = codigoCEXEquivalenteHDEC;
            } else if ("PAS".equals(docType)) {
                docType = codigoPASEquivalenteHDEC;
            } else if ("RUC".equals(docType)) {
                docType = codigoRUCEquivalenteHDEC;
            } else if ("OTROS".equals(docType)) {
                docType = codigoOTROSEquivalenteHDEC;
            }
            request.setTipoDeDocumento(docType);
            request.setNumeroDeDocumento(docNumber);
            request.setTelefonoDeContacto1(customerPhone);
            request.setTelefonoDeContacto2(StringUtils.isEmptyString(customerPhone2) ? HDEC_DEFAULT_TELF_CONTACTO_2 : customerPhone2);
            request.setEmail(StringUtils.isEmptyString(email) ? HDEC_DEFAULT_EMAIL : email);
            request.setEnvioDeContratos(sendContracts);
            request.setProteccionDeDatos(dataProtection);
            request.setCanalDeVenta(StringUtils.isEmptyString(canal) ? HDEC_DEFAULT_NO_APLICA : canal);
            request.setDetalleDeCanal(StringUtils.isEmptyString(entidad) ? HDEC_DEFAULT_NO_APLICA : entidad);
            request.setNombreVendedor(StringUtils.isEmptyString(nombreVendedor) ? HDEC_DEFAULT_NO_APLICA : nombreVendedor);
            request.setCodVendedorAtis(codATIS);
            request.setCodVendedorCms(codCMS);
            request.setZonalDepartamentoVendedor(StringUtils.isEmptyString(departamento) ? HDEC_DEFAULT_NO_APLICA : departamento);
            request.setRegionProvinciaDistritoVendedor(StringUtils.isEmptyString(provincia) ? HDEC_DEFAULT_NO_APLICA : provincia);
            request.setTipoDeProducto(productType);
            request.setSubProducto(productName);

            /* Parche para Cerrar Ventas SVAS por HDEC */
            if (commercialOperation.equals("SVAS")) {
                if (productName.indexOf("PUNTO ADICIONAL") != 0) {
                    request.setSubProducto(productcategory);
                }
                if (internetRsw > 0) {
                    request.setSubProducto("REPETIDOR SMART WIFI");
                }
            }
            request.setDepartamento(StringUtils.isEmptyString(department) ? HDEC_DEFAULT_NO_APLICA : department);
            request.setProvincia(StringUtils.isEmptyString(province) ? HDEC_DEFAULT_NO_APLICA : province);
            request.setDistrito(StringUtils.isEmptyString(district) ? HDEC_DEFAULT_NO_APLICA : district);
            request.setDireccion(StringUtils.isEmptyString(address) ? HDEC_DEFAULT_NO_APLICA : address);
            request.setOperacionComercial(commercialOperation);

            request.setTelefonoAMigrar(StringUtils.isEmptyString(telefonoMigrar) ? HDEC_DEFAULT_TELF_MIGRAR : telefonoMigrar);

            //request.setDesafiliacionDePaginasBlancas(disableWhitepage);
            request.setDesafiliacionDePaginasBlancas((whatsapp == 0 ? "MOTORIZADO" : (whatsapp == 1 ? "WHATSAPP" : "-")));
            request.setAfiliacionAFacturaDigital(enableDigitalInvoice);
            request.setTecnologiaDeInternet(StringUtils.isEmptyString(internetTech) ? HDEC_DEFAULT_NO_APLICA : internetTech);
            request.setCodigoExperto(expertoCode == null || expertoCode.equalsIgnoreCase("") ? HDEC_DEFAULT_EXPERTO_CODE_NA : expertoCode);

            request.setTieneGrabacion(tieneMigracion);
            request.setFechaRegistro(new SimpleDateFormat("dd-MM-yyyy").format(registeredTime1));
            request.setHoraRegistroWeb(new SimpleDateFormat("HH:mm:ss").format(registeredTime1));
            request.setModalidadDePago(StringUtils.isEmptyString(paymentMode) ? HDEC_DEFAULT_NO_APLICA : paymentMode);
            request.setIdGrabacionNativo(recordingId);
            request.setTecnologiaTelevision(StringUtils.isEmptyString(tvTech) ? HDEC_DEFAULT_NO_APLICA : tvTech);
            request.setPaquetizacion(paquetizacion);
            request.setAltasTv(altasTv);

            request.setTipoEquipamientoDeco(StringUtils.isEmptyString(equipmentDecoType) ? HDEC_DEFAULT_NO_APLICA : equipmentDecoType);
            request.setDniVendedor(dniVendedor);
            request.setTelefonoOrigen(telefonoOrigen == null ? HDEC_DEFAULT_TELF_CONTACTO_2 : telefonoOrigen);

            request.setTipoServicio(serviceType);
            request.setClienteCms(StringUtils.isEmptyString(cmsCustomer) ? HDEC_DEFAULT_CLIENTE_CMS : cmsCustomer);
            request.setDistritoVendedor(StringUtils.isEmptyString(distritoVendedor) ? HDEC_DEFAULT_NO_APLICA : distritoVendedor);
            request.setDecosSd(StringUtils.isEmptyString(decosSD) ? HDEC_DEFAULT_0 : decosSD);
            request.setDecosHd(StringUtils.isEmptyString(decosHD) ? HDEC_DEFAULT_0 : decosHD);
            request.setDecosDvr(StringUtils.isEmptyString(decosDVR) ? HDEC_DEFAULT_0 : decosDVR);
            request.setBloqueTv(StringUtils.isEmptyString(blockTv) ? HDEC_DEFAULT_NO_APLICA : blockTv.substring(0, (blockTv.length() < 100 ? blockTv.length() : 100)));
            request.setSvaInternet(StringUtils.isEmptyString(svaInternet) ? HDEC_DEFAULT_EMPTY_STRING : svaInternet);
            request.setSvaLinea(StringUtils.isEmptyString(svaLine) ? HDEC_DEFAULT_NO_APLICA : svaLine);

            if (request.getModalidadDePago().equalsIgnoreCase("Contado")) {
                request.setDescuentoWinback(cod_CIP);
                request.setFechapago(fechapagoCIP);
            } else {
                request.setDescuentoWinback("");
                request.setFechapago("");
            }

            request.setCodigoDeServicioCms(StringUtils.isEmptyString(cmsServiceCode) ? HDEC_DEFAULT_COD_SRV_CMS : cmsServiceCode);
            request.setBloqueProducto(StringUtils.isEmptyString(tvBlock) ? HDEC_DEFAULT_NO_APLICA : tvBlock);

            request.setCoordenadasX(coordenadasx);
            request.setCoordenadasY(coordenadasy);
            request.setWebParental(webParental);
            request.setMontoContado(montoContado);
            request.setCodigoPostal(coidgoPostal);
            request.setPublicarGuia(automaticDebit);  // request.setPublicarGuia(publicarGuia);
            request.setRepetidorSmartWifi(internetRsw);
            request.setModoVenta(orderOrigen.equalsIgnoreCase("APPVF") ? "APP VENTA" : "WEB VENTA");
            request.setNacionalidad(clientNationality);

            //Sprint 24 RUC
            if(docType.equals("RUC") && docNumber.substring(0,2).equals("20")){
                request.setTipoDocumentoRrll(tipDocRrll);
                request.setNumeroDocumentoRrll(docNumRrll);
                request.setNombreCompletoRrll(nomRrll);
            } else {
                request.setTipoDocumentoRrll("");
                request.setNumeroDocumentoRrll("");
                request.setNombreCompletoRrll("");
            }

            //Sprint 24 RECUPERO CAIDA
            if(flag_recupero){
                request.setFlagRecupero("SI");
                request.setIdTransaccion(id_transaccion);
            }

            apiData.add(request);

            try {
                sendHdec(request);
            } catch (ClientException e) {
                logger.error("Error SendDirectToHDEC Service.", e);
                logger.info("Fin SendDirectToHDEC Service.");

                if ((e + "").contains("CODIGO_UNICO")) {
                    editTdpVisor("PENDIENTE", orderId);
                } else if((e + "").contains("CAMPO VACIO") ||
                        (e + "").contains("TAMAÑO EXCESIVO") ||
                        (e + "").contains("TIPO DE VARIABLE INCORRECTA") ||
                        (e + "").contains("FORMATO DE FECHA INCORRECTA") ||
                        (e + "").contains("FORMATO DE HORA INCORRECTA") ||
                        (e + "").contains("FORMATO DE EMAIL INCORRECTA")){
                    editTdpVisor("CAIDA", orderId);
                }else {
                    //ERRORES CONTROLADOS
                    if (e.getEvent().getServiceResponse().contains("4002") ||
                            e.getEvent().getServiceResponse().contains("4003") ||
                            e.getEvent().getServiceResponse().contains("4001") ||
                            e.getEvent().getServiceResponse().contains("4000") ||
                            e.getEvent().getServiceResponse().contains("500")) {
                        editTdpVisor("ENVIANDO", orderId);
                    } else {
                        //MANDARLO A CAIDA
                        editTdpVisor("CAIDA", orderId);
                    }
                }
                throw e;
            } catch (Exception e) {
                logger.error("Error SendDirectToHDEC Service.", e);
                logger.info("Fin SendDirectToHDEC Service.");
                throw e;
            }
        }
        logger.info("Fin SendDirectToHDEC Service.");
    }

    private ApiResponse<HdecApiHdecResponseBody> sendHdec(HdecApiHdecRequestBody apiRequestBody) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder().setUrl(apiUri)
                .setApiId(hdecRegistrarPreVentaApiId)
                .setApiSecret(hdecRegistrarPreVentaApiSecret)
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_HDEC)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_HDEC).build();

        ClientResult<ApiResponse<HdecApiHdecResponseBody>> result = postHdec(apiRequestBody, config);
        result.getEvent().setOrderId(apiRequestBody.getIdGrabacionNativo());

        registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        } else {
            ApiResponse<HdecApiHdecResponseBody> apiResponse = result.getResult();
            if (!Constants.RESPONSE.equals(apiResponse.getHeaderOut().getMsgType())) {
                String message = "Error HDC";
                if (apiResponse.getBodyOut() != null) {
                    HdecApiHdecResponseBody body = apiResponse.getBodyOut();
                    if (body.getClientException() != null) {
                        if (body.getClientException().getAppDetail() != null) {
                            message = body.getClientException().getAppDetail().getExceptionAppMessage();
                        }
                    }
                }
                throw new ClientException(message);
            }
        }

        return result.getResult();

    }

    // Sprint 11 - para evaluar hdec en modo testing o produccion
    public ClientResult<ApiResponse<HdecApiHdecResponseBody>> postHdec(HdecApiHdecRequestBody apiRequestBody, ClientConfig config) {
        String jsonResponse = null;
        Parameters parameter = parametersRepository.findOneByDomainAndCategoryAndElement("TEST", "PARAMETER", "TESTHDEC");
        ClientResult<ApiResponse<HdecApiHdecResponseBody>> result = null;
        if ("NO".equals(parameter.getStrValue())) {
            HdecClient client = new HdecClient(config);
            result = client.post(apiRequestBody);

        } else {
            // Entonces ejecutamos en modo test... procedemos a verificar que error simular (4002 o 4003)
            Parameters parameterErrorCode = parametersRepository.findOneByDomainAndCategoryAndElement("TEST", "PARAMETER", "HDECERRORCODE");

            Parameters parameterMessageErrorCode = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "PARAMETER", "HDECERROR.MENSAJE." + parameterErrorCode.getStrValue());
            jsonResponse = parameterMessageErrorCode.getStrValue();
            HdecClient client = new HdecClientErrorSimulator(config, jsonResponse);
            result = client.post(apiRequestBody);
            result.getEvent().setServiceResponse(jsonResponse);
        }
        return result;
    }

    private void registerEvent(ServiceCallEvent event) {
        try (Connection con = dataSource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events (event_datetime, tag, username,msg,"
                    + "orderId,docNumber,serviceCode,serviceUrl,serviceRequest,serviceResponse,sourceApp,"
                    + "sourceAppVersion,result) "
                    + "VALUES (CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement q = con.prepareStatement(sql);

            q.setString(1, "SERVICE_CALL");
            q.setString(2, event.getUsername());
            q.setString(3, event.getMsg());
            q.setString(4, event.getOrderId());
            q.setString(5, event.getDocNumber());
            q.setString(6, event.getServiceCode());
            q.setString(7, event.getServiceUrl());
            q.setString(8, event.getServiceRequest());
            q.setString(9, event.getServiceResponse());
            q.setString(10, event.getSourceApp());
            q.setString(11, event.getSourceAppVersion());
            q.setString(12, event.getResult());

            q.executeUpdate();
            dataSource.getConnection().close();
        } catch (Exception e) {
            logger.error("Error insert", e);
        }
    }


    private void editTdpVisor(String estado, String orderid) {
        try (Connection con = dataSource.getConnection()) {
            String sql = "UPDATE ibmx_a07e6d02edaf552.tdp_visor SET estado_solicitud = ? "
                    + "WHERE id_visor = ?";

            PreparedStatement q = con.prepareStatement(sql);

            q.setString(1, estado);
            q.setString(2, orderid);

            q.executeUpdate();
            dataSource.getConnection().close();
        } catch (Exception e) {
            logger.error("Error insert", e);
        }
    }
}
