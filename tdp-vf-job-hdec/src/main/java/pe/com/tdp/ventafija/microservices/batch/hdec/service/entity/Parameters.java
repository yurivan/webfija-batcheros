package pe.com.tdp.ventafija.microservices.batch.hdec.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parameters", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema
public class Parameters {
    @Id
    @Column
    private Integer id;
    @Column(name = "auxiliar")
    private Integer auxiliar;
    @Column(name = "domain")
    private String domain;
    @Column(name = "category")
    private String category;
    @Column(name = "element")
    private String element;
    @Column(name = "strvalue")
    private String strValue;
    @Column(name = "strfilter")
    private String strfilter;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(Integer auxiliar) {
        this.auxiliar = auxiliar;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(String strValue) {
        this.strValue = strValue;
    }

    public String getStrfilter() {
        return strfilter;
    }

    public void setStrfilter(String strfilter) {
        this.strfilter = strfilter;
    }
}
