package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class HdecApiHdecRequestBody {

  private String CodigoUnico;
  private String Campana;
  private String NombreDeCliente;
  private String ApellidoPaterno;
  private String ApellidoMaterno;
  private String TipoDeDocumento;
  private String NumeroDeDocumento;
  private String TelefonoDeContacto1;
  private String TelefonoDeContacto2;
  private String Email;
  private String EnvioDeContratos;
  private String ProteccionDeDatos;
  private String CanalDeVenta;
  private String DetalleDeCanal;
  private String NombreVendedor;
  private String CodVendedorAtis;
  private String CodVendedorCms;
  private String ZonalDepartamentoVendedor;
  private String RegionProvinciaDistritoVendedor;
  private String TipoDeProducto;
  private String SubProducto;
  private String Departamento;
  private String Provincia;
  private String Distrito;
  private String Direccion;
  private String OperacionComercial;
  private String TelefonoAMigrar;
  private String DesafiliacionDePaginasBlancas;
  private String AfiliacionAFacturaDigital;
  private String TecnologiaDeInternet;
  private String CodigoExperto;
  private String TieneGrabacion;
  private String FechaRegistro;
  private String HoraRegistroWeb;
  private String ModalidadDePago;
  private String IdGrabacionNativo;
  private String TecnologiaTelevision;
  private String Paquetizacion;
  private String AltasTv;
  private String TipoEquipamentoDeco;
  private String DniVendedor;
  private String TelefonoOrigen;
  private String TipoServicio;
  private String ClienteCms;
  private String DistritoVendedor;
  private String DecosSd;
  private String DecosHd;
  private String DecosDvr;
  private String BloqueTv;
  private String SvaInternet;
  private String SvaLinea;
  private String DescuentoWinback;
  private String CodigoDeServicioCms;
  private String BloqueProducto;

  public String getCodigoUnico() {
    return CodigoUnico;
  }

  public void setCodigoUnico(String codigoUnico) {
    CodigoUnico = codigoUnico;
  }

  public String getCampana() {
    return Campana;
  }

  public void setCampana(String campana) {
    Campana = campana;
  }

  public String getNombreDeCliente() {
    return NombreDeCliente;
  }

  public void setNombreDeCliente(String nombreDeCliente) {
    NombreDeCliente = nombreDeCliente;
  }

  public String getApellidoPaterno() {
    return ApellidoPaterno;
  }

  public void setApellidoPaterno(String apellidoPaterno) {
    ApellidoPaterno = apellidoPaterno;
  }

  public String getApellidoMaterno() {
    return ApellidoMaterno;
  }

  public void setApellidoMaterno(String apellidoMaterno) {
    ApellidoMaterno = apellidoMaterno;
  }

  public String getTipoDeDocumento() {
    return TipoDeDocumento;
  }

  public void setTipoDeDocumento(String tipoDeDocumento) {
    TipoDeDocumento = tipoDeDocumento;
  }

  public String getNumeroDeDocumento() {
    return NumeroDeDocumento;
  }

  public void setNumeroDeDocumento(String numeroDeDocumento) {
    NumeroDeDocumento = numeroDeDocumento;
  }

  public String getTelefonoDeContacto1() {
    return TelefonoDeContacto1;
  }

  public void setTelefonoDeContacto1(String telefonoDeContacto1) {
    TelefonoDeContacto1 = telefonoDeContacto1;
  }

  public String getTelefonoDeContacto2() {
    return TelefonoDeContacto2;
  }

  public void setTelefonoDeContacto2(String telefonoDeContacto2) {
    TelefonoDeContacto2 = telefonoDeContacto2;
  }

  public String getEmail() {
    return Email;
  }

  public void setEmail(String email) {
    Email = email;
  }

  public String getEnvioDeContratos() {
    return EnvioDeContratos;
  }

  public void setEnvioDeContratos(String envioDeContratos) {
    EnvioDeContratos = envioDeContratos;
  }

  public String getProteccionDeDatos() {
    return ProteccionDeDatos;
  }

  public void setProteccionDeDatos(String proteccionDeDatos) {
    ProteccionDeDatos = proteccionDeDatos;
  }

  public String getCanalDeVenta() {
    return CanalDeVenta;
  }

  public void setCanalDeVenta(String canalDeVenta) {
    CanalDeVenta = canalDeVenta;
  }

  public String getDetalleDeCanal() {
    return DetalleDeCanal;
  }

  public void setDetalleDeCanal(String detalleDeCanal) {
    DetalleDeCanal = detalleDeCanal;
  }

  public String getNombreVendedor() {
    return NombreVendedor;
  }

  public void setNombreVendedor(String nombreVendedor) {
    NombreVendedor = nombreVendedor;
  }

  public String getCodVendedorAtis() {
    return CodVendedorAtis;
  }

  public void setCodVendedorAtis(String codVendedorAtis) {
    CodVendedorAtis = codVendedorAtis;
  }

  public String getCodVendedorCms() {
    return CodVendedorCms;
  }

  public void setCodVendedorCms(String codVendedorCms) {
    CodVendedorCms = codVendedorCms;
  }

  public String getZonalDepartamentoVendedor() {
    return ZonalDepartamentoVendedor;
  }

  public void setZonalDepartamentoVendedor(String zonalDepartamentoVendedor) {
    ZonalDepartamentoVendedor = zonalDepartamentoVendedor;
  }

  public String getRegionProvinciaDistritoVendedor() {
    return RegionProvinciaDistritoVendedor;
  }

  public void setRegionProvinciaDistritoVendedor(String regionProvinciaDistritoVendedor) {
    RegionProvinciaDistritoVendedor = regionProvinciaDistritoVendedor;
  }

  public String getTipoDeProducto() {
    return TipoDeProducto;
  }

  public void setTipoDeProducto(String tipoDeProducto) {
    TipoDeProducto = tipoDeProducto;
  }

  public String getSubProducto() {
    return SubProducto;
  }

  public void setSubProducto(String subProducto) {
    SubProducto = subProducto;
  }

  public String getDepartamento() {
    return Departamento;
  }

  public void setDepartamento(String departamento) {
    Departamento = departamento;
  }

  public String getProvincia() {
    return Provincia;
  }

  public void setProvincia(String provincia) {
    Provincia = provincia;
  }

  public String getDistrito() {
    return Distrito;
  }

  public void setDistrito(String distrito) {
    Distrito = distrito;
  }

  public String getDireccion() {
    return Direccion;
  }

  public void setDireccion(String direccion) {
    Direccion = direccion;
  }

  public String getOperacionComercial() {
    return OperacionComercial;
  }

  public void setOperacionComercial(String operacionComercial) {
    OperacionComercial = operacionComercial;
  }

  public String getTelefonoAMigrar() {
    return TelefonoAMigrar;
  }

  public void setTelefonoAMigrar(String telefonoAMigrar) {
    TelefonoAMigrar = telefonoAMigrar;
  }

  public String getDesafiliacionDePaginasBlancas() {
    return DesafiliacionDePaginasBlancas;
  }

  public void setDesafiliacionDePaginasBlancas(String desafiliacionDePaginasBlancas) {
    DesafiliacionDePaginasBlancas = desafiliacionDePaginasBlancas;
  }

  public String getAfiliacionAFacturaDigital() {
    return AfiliacionAFacturaDigital;
  }

  public void setAfiliacionAFacturaDigital(String afiliacionAFacturaDigital) {
    AfiliacionAFacturaDigital = afiliacionAFacturaDigital;
  }

  public String getTecnologiaDeInternet() {
    return TecnologiaDeInternet;
  }

  public void setTecnologiaDeInternet(String tecnologiaDeInternet) {
    TecnologiaDeInternet = tecnologiaDeInternet;
  }

  public String getCodigoExperto() {
    return CodigoExperto;
  }

  public void setCodigoExperto(String codigoExperto) {
    CodigoExperto = codigoExperto;
  }

  public String getTieneGrabacion() {
    return TieneGrabacion;
  }

  public void setTieneGrabacion(String tieneGrabacion) {
    TieneGrabacion = tieneGrabacion;
  }

  public String getFechaRegistro() {
    return FechaRegistro;
  }

  public void setFechaRegistro(String fechaRegistro) {
    FechaRegistro = fechaRegistro;
  }

  public String getHoraRegistroWeb() {
    return HoraRegistroWeb;
  }

  public void setHoraRegistroWeb(String horaRegistroWeb) {
    HoraRegistroWeb = horaRegistroWeb;
  }

  public String getModalidadDePago() {
    return ModalidadDePago;
  }

  public void setModalidadDePago(String modalidadDePago) {
    ModalidadDePago = modalidadDePago;
  }

  public String getIdGrabacionNativo() {
    return IdGrabacionNativo;
  }

  public void setIdGrabacionNativo(String idGrabacionNativo) {
    IdGrabacionNativo = idGrabacionNativo;
  }

  public String getTecnologiaTelevision() {
    return TecnologiaTelevision;
  }

  public void setTecnologiaTelevision(String tecnologiaTelevision) {
    TecnologiaTelevision = tecnologiaTelevision;
  }

  public String getPaquetizacion() {
    return Paquetizacion;
  }

  public void setPaquetizacion(String paquetizacion) {
    Paquetizacion = paquetizacion;
  }

  public String getAltasTv() {
    return AltasTv;
  }

  public void setAltasTv(String altasTv) {
    AltasTv = altasTv;
  }

  public String getTipoEquipamentoDeco() {
    return TipoEquipamentoDeco;
  }

  public void setTipoEquipamentoDeco(String tipoEquipamentoDeco) {
    TipoEquipamentoDeco = tipoEquipamentoDeco;
  }

  public String getDniVendedor() {
    return DniVendedor;
  }

  public void setDniVendedor(String dniVendedor) {
    DniVendedor = dniVendedor;
  }

  public String getTelefonoOrigen() {
    return TelefonoOrigen;
  }

  public void setTelefonoOrigen(String telefonoOrigen) {
    TelefonoOrigen = telefonoOrigen;
  }

  public String getTipoServicio() {
    return TipoServicio;
  }

  public void setTipoServicio(String tipoServicio) {
    TipoServicio = tipoServicio;
  }

  public String getClienteCms() {
    return ClienteCms;
  }

  public void setClienteCms(String clienteCms) {
    ClienteCms = clienteCms;
  }

  public String getDistritoVendedor() {
    return DistritoVendedor;
  }

  public void setDistritoVendedor(String distritoVendedor) {
    DistritoVendedor = distritoVendedor;
  }

  public String getDecosSd() {
    return DecosSd;
  }

  public void setDecosSd(String decosSd) {
    DecosSd = decosSd;
  }

  public String getDecosHd() {
    return DecosHd;
  }

  public void setDecosHd(String decosHd) {
    DecosHd = decosHd;
  }

  public String getDecosDvr() {
    return DecosDvr;
  }

  public void setDecosDvr(String decosDvr) {
    DecosDvr = decosDvr;
  }

  public String getBloqueTv() {
    return BloqueTv;
  }

  public void setBloqueTv(String bloqueTv) {
    BloqueTv = bloqueTv;
  }

  public String getSvaInternet() {
    return SvaInternet;
  }

  public void setSvaInternet(String svaInternet) {
    SvaInternet = svaInternet;
  }

  public String getSvaLinea() {
    return SvaLinea;
  }

  public void setSvaLinea(String svaLinea) {
    SvaLinea = svaLinea;
  }

  public String getDescuentoWinback() {
    return DescuentoWinback;
  }

  public void setDescuentoWinback(String descuentoWinback) {
    DescuentoWinback = descuentoWinback;
  }

  public String getCodigoDeServicioCms() {
    return CodigoDeServicioCms;
  }

  public void setCodigoDeServicioCms(String codigoDeServicioCms) {
    CodigoDeServicioCms = codigoDeServicioCms;
  }

  public String getBloqueProducto() {
    return BloqueProducto;
  }

  public void setBloqueProducto(String bloqueProducto) {
    BloqueProducto = bloqueProducto;
  }
}
