package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class PagoEfectivoConsultaRequest {

    private String numeroCIP;
    private String orderId;
    private String docNumber;

    public PagoEfectivoConsultaRequest(){
        super();
    }

    public String getNumeroCIP() {
        return numeroCIP;
    }

    public void setNumeroCIP(String numeroCIP) {
        this.numeroCIP = numeroCIP;
    }

    public String getOrderId() { return orderId; }

    public void setOrderId(String orderId) { this.orderId = orderId;  }

    public String getDocNumber() {  return docNumber;   }

    public void setDocNumber(String docNumber) {  this.docNumber = docNumber;  }
}
