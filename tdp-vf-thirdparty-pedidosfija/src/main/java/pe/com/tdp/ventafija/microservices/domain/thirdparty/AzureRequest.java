package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class AzureRequest {

	private String key;
	private String status;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
