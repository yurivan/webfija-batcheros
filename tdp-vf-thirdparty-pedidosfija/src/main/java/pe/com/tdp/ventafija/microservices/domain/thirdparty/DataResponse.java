package pe.com.tdp.ventafija.microservices.domain.thirdparty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DataResponse {

    @JsonProperty("cip")
    private Integer cip;
    @JsonProperty("transactionCode")
    private String transactionCode;
    @JsonProperty("amount")
    private Integer amount;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("status")
    private Integer status;
    @JsonProperty("statusName")
    private String statusName;
    @JsonProperty("dateCreation")
    private String dateCreation;
    @JsonProperty("dateExpiry")
    private String dateExpiry;
    @JsonProperty("datePayment")
    private String datePayment;
    @JsonProperty("dateRemoval")
    private String dateRemoval;
    @JsonProperty("token")
    private String token;


    @JsonProperty("cip")
    public Integer getCip() {
        return cip;
    }

    @JsonProperty("cip")
    public void setCip(Integer cip) {
        this.cip = cip;
    }

    @JsonProperty("transactionCode")
    public String getTransactionCode() {
        return transactionCode;
    }

    @JsonProperty("transactionCode")
    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    @JsonProperty("amount")
    public Integer getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonProperty("statusName")
    public String getStatusName() {
        return statusName;
    }

    @JsonProperty("statusName")
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @JsonProperty("dateCreation")
    public String getDateCreation() {
        return dateCreation;
    }

    @JsonProperty("dateCreation")
    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    @JsonProperty("dateExpiry")
    public String getDateExpiry() {
        return dateExpiry;
    }

    @JsonProperty("dateExpiry")
    public void setDateExpiry(String dateExpiry) {
        this.dateExpiry = dateExpiry;
    }

    @JsonProperty("datePayment")
    public String getDatePayment() {
        return datePayment;
    }

    @JsonProperty("datePayment")
    public void setDatePayment(String datePayment) {
        this.datePayment = datePayment;
    }

    @JsonProperty("dateRemoval")
    public String getDateRemoval() {
        return dateRemoval;
    }

    @JsonProperty("dateRemoval")
    public void setDateRemoval(String dateRemoval) {
        this.dateRemoval = dateRemoval;
    }

    @JsonProperty("token")
    public String getToken() {
        return token;
    }

    @JsonProperty("token")
    public void setToken(String token) {
        this.token = token;
    }
}