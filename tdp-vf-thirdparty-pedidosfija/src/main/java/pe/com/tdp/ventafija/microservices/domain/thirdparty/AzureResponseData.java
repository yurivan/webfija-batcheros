package pe.com.tdp.ventafija.microservices.domain.thirdparty;

import java.sql.Timestamp;

public class AzureResponseData {

	private String dni;
	private String id;
	private String channel;
	private String phoneNumber;
	private String telephoneNumber;
	private Timestamp documentDate;
	private String providerName;
	private String campaignName;
	private String documentNumber;
	private String customerName;
	private String business;
	private String disableWhitePage;
	private String enableDigitalInvoice;
	private String dataProtection;
	private String providerDetail;

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	public Timestamp getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(Timestamp documentDate) {
		this.documentDate = documentDate;
	}

	public String getProviderName() {
		return providerName;
	}

	public void setProviderName(String providerName) {
		this.providerName = providerName;
	}

	public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getBusiness() {
		return business;
	}

	public void setBusiness(String business) {
		this.business = business;
	}

	public String getDisableWhitePage() {
		return disableWhitePage;
	}

	public void setDisableWhitePage(String disableWhitePage) {
		this.disableWhitePage = disableWhitePage;
	}

	public String getEnableDigitalInvoice() {
		return enableDigitalInvoice;
	}

	public void setEnableDigitalInvoice(String enableDigitalInvoice) {
		this.enableDigitalInvoice = enableDigitalInvoice;
	}

	public String getDataProtection() {
		return dataProtection;
	}

	public void setDataProtection(String dataProtection) {
		this.dataProtection = dataProtection;
	}

	public String getProviderDetail() {
		return providerDetail;
	}

	public void setProviderDetail(String providerDetail) {
		this.providerDetail = providerDetail;
	}

}
