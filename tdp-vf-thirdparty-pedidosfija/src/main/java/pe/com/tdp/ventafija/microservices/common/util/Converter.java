package pe.com.tdp.ventafija.microservices.common.util;

public class Converter {
	public static String fixPrice(String price){
		return price.replaceFirst("[.]", "").replaceAll("[^0-9.]", "");
	}
}
