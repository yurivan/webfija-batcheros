package pe.com.tdp.ventafija.microservices.batch.config;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import pe.com.tdp.ventafija.microservices.batch.hdec.ContingenciaProcessorScheduler;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaAutomatizador;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper.VentaRowMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.AzureRequestProcessorSkipPolicy;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.HDECEnvioProcessor;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.HDECEnvioProcessorListener;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.LogAutomatizadorService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    private static final Logger logger = LogManager.getLogger(BatchConfig.class);

    @Autowired
    private DataSource dataSource;
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;


    @Value("${tdp.maximo.cantidad.automatizador}")
    private Integer maxLimit;

    @Value("${tdp.reintentos.max.automatizador}")
    private Integer maxLimitReintentos;


    @Bean
    public PlatformTransactionManager transactionManager() {
        return new ResourcelessTransactionManager();
    }

    @Bean
    public JobRepository jobRepository() throws Exception {
        return new MapJobRepositoryFactoryBean(transactionManager()).getObject();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        SyncTaskExecutor executor = new SyncTaskExecutor();
        return executor;
    }

    @Bean
    public SimpleJobLauncher jobLauncher() throws Exception {
        SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        simpleJobLauncher.setJobRepository(jobRepository());
        simpleJobLauncher.setTaskExecutor(taskExecutor());
        return simpleJobLauncher;
    }

    @Bean
    public ContingenciaProcessorScheduler contingenciaProcessorScheduler() {
        return new ContingenciaProcessorScheduler();
    }

    @Bean
    public JdbcCursorItemReader<VentaAutomatizador> ventasReader() {
        String sql = "SELECT estado_solicitud,id_visor,codigo_pedido,auto_retry " +
                     "FROM ibmx_a07e6d02edaf552.tdp_visor " +
                     "LEFT JOIN ibmx_a07e6d02edaf552.tdp_order " +
                     "ON tdp_order.order_id = tdp_visor.id_visor " +
                     "WHERE estado_solicitud='PENDIENTE' " +
                     "AND automatizador_ok = '1' and (auto_retry < '"+maxLimitReintentos+"' or auto_retry is null) " +
                     "LIMIT '"+maxLimit+"'";


        LogSystem.infoQuery(logger, "BatchConfig", "ventasReader", "sql", sql);

        JdbcCursorItemReader<VentaAutomatizador> reader = new JdbcCursorItemReader<VentaAutomatizador>();
        reader.setDataSource(dataSource);
        reader.setSql(sql);
        reader.setRowMapper(new VentaRowMapper());
        reader.setFetchSize(1);
        return reader;
    }

    @Bean
    public JdbcBatchItemWriter<VentaAutomatizador> ventasWriter() {
        JdbcBatchItemWriter<VentaAutomatizador> writer = new JdbcBatchItemWriter<VentaAutomatizador>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<VentaAutomatizador>());
        // Actualizamos el estado de la venta si es que la venta sigue en estado 'ENVIANDO'
        writer.setSql("UPDATE ibmx_a07e6d02edaf552.TDP_VISOR set estado_solicitud = :estado, codigo_pedido =:codigoPedido, auto_retry =:auto_retry where id_visor = :id AND estado_solicitud = 'PENDIENTE'");
        writer.setDataSource(dataSource);
        return writer;
    }

    @Bean
    public HDECEnvioProcessor ventasProcessor() {
        return new HDECEnvioProcessor();
    }

    @Bean
    public Job hdecContingenciaProcessor(Step contingenciaHDECStep) throws Exception {
        return jobBuilderFactory.get("hdecContingenciaProcessor").incrementer(new RunIdIncrementer())
                .repository(jobRepository()).flow(contingenciaHDECStep).end().build();
    }

    @Bean
    public SkipPolicy hdecEnvioProcessorSkipPolicy() {
        return new AzureRequestProcessorSkipPolicy();
    }

    @Bean
    public ItemProcessListener<VentaAutomatizador, VentaAutomatizador> hdecEnvioProcessorListener() {
        return (ItemProcessListener<VentaAutomatizador, VentaAutomatizador>) new HDECEnvioProcessorListener();
    }

    @Bean
    public Step contingenciaHDECStep(HDECEnvioProcessor processor) {
        return stepBuilderFactory.get("contingenciaHDECStep")
                .<VentaAutomatizador, VentaAutomatizador>chunk(0)
                .reader(ventasReader())
                .processor(ventasProcessor())
                .faultTolerant()
                .skipPolicy(hdecEnvioProcessorSkipPolicy())
                .listener(hdecEnvioProcessorListener())
                .writer(ventasWriter())
                .build();
    }
}