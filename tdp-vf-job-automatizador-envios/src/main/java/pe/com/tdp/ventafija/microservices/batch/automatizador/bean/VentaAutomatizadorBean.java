package pe.com.tdp.ventafija.microservices.batch.automatizador.bean;

import java.util.Date;

/**
 * Bean que representa una Venta y su estado de envio al BACK (tambien conocido como HDEC)
 * @author      glazaror
 * @since       1.5
 */
public class VentaAutomatizadorBean {
    private String id;
    private String estado;
    private Date fechaGrabacion;
    private String dniCliente;
    private String codigoVendedor;

    public Date getFechaGrabacion() {
        return fechaGrabacion;
    }

    public void setFechaGrabacion(Date fechaGrabacion) {
        this.fechaGrabacion = fechaGrabacion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public String getDniCliente() {
        return dniCliente;
    }

    public void setDniCliente(String dniCliente) {
        this.dniCliente = dniCliente;
    }

    public String getCodigoVendedor() {
        return codigoVendedor;
    }

    public void setCodigoVendedor(String codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
