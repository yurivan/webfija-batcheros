package pe.com.tdp.ventafija.microservices.batch.automatizador;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.batch.automatizador.bean.VentaAutomatizadorBean;
import pe.com.tdp.ventafija.microservices.batch.automatizador.bean.mapper.VentaAutoRowMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.OfflineProcessor;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextImpl;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.service.automatizador.AutomatizadorService2;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;

@Service
public class SendAutomatizadorService {
    private static final Logger logger = LogManager.getLogger(SendAutomatizadorService.class);

    @Autowired
    private DataSource dataSource;


    @Autowired
    public AutomatizadorService2 automatizadorService;

    public void process3() throws Exception{

        JdbcCursorItemReader<VentaAutomatizadorBean> ventasReader = ventasReader();

        ventasReader.open(new ExecutionContext());
        VentaAutomatizadorBean ventaBean;

        while ((ventaBean = ventasReader.read())!=null) {
            startCallContext(ventaBean);

            LogSystem.info(logger, "automatizadorReenviosProcessor", "process", "Start");
            LogSystem.infoObject(logger, "automatizadorReenviosProcessor", "process", "VentaAutomatizadorBean", ventaBean);

            Boolean success;


        }
    }

    public JdbcCursorItemReader<VentaAutomatizadorBean> ventasReader() {

        String sql = "SELECT * FROM (SELECT VIS.ID_VISOR, VIS.FECHA_GRABACION, VIS.ESTADO_SOLICITUD, " +
                "VIS.DNI, ORD.USERID AS COD_ATIS FROM ibmx_a07e6d02edaf552.TDP_VISOR AS VIS INNER JOIN " +
                "ibmx_a07e6d02edaf552.ORDER AS ORD ON ORD.ID = VIS.ID_VISOR inner join ibmx_a07e6d02edaf552.tdp_order" +
                " as o ON o.order_id = VIS.ID_VISOR WHERE VIS.ESTADO_SOLICITUD = 'ENVIANDO' ORDER BY VIS.FECHA_GRABACION " +
                "ASC LIMIT 10) " +
                "ALS LIMIT 10";


        LogSystem.infoQuery(logger, "BatchConfig", "ventasReader", "sql", sql);

        JdbcCursorItemReader<VentaAutomatizadorBean> reader = new JdbcCursorItemReader<VentaAutomatizadorBean>();
        reader.setDataSource(dataSource);
        reader.setSql(sql);
        reader.setRowMapper(new VentaAutoRowMapper());
        reader.setFetchSize(1);
        return reader;
    }


    public void startCallContext(VentaAutomatizadorBean ventaBean) {

        ServiceCallEvent event = new ServiceCallEvent();
        event.setUsername(ventaBean.getCodigoVendedor());
        event.setOrderId(ventaBean.getId());
        event.setServiceCode("AUTOMATIZADOR-REENVIOS");
        event.setDocNumber(ventaBean.getDniCliente());
        event.setSourceApp("BATCH");
        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);
    }

    public void updateVisor(VentaAutomatizadorBean ventaBean) {

        try {
            Connection con = dataSource.getConnection();

            String sql = "UPDATE ibmx_a07e6d02edaf552.tdp_visor set estado_solicitud = '"+ventaBean.getEstado()+"' " +
               "where id_visor = '"+ventaBean.getId()+"' AND estado_solicitud = 'ENVIANDO_AUTOMATIZADOR'";
            System.out.println(sql);
            PreparedStatement ps = con.prepareStatement(sql);
            ps.executeUpdate();
            con.commit();
        } catch (Exception e) {

        }

    }
}
