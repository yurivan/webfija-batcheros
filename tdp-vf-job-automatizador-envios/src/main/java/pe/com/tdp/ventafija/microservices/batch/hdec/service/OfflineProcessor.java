package pe.com.tdp.ventafija.microservices.batch.hdec.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.batch.config.EmailConfig;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.Order;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.PeticionPagoEfectivo;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.OrderRepository;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.ParametersRepository;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.PeticionPagoEfectivoRepository;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.clients.Cip.GenerarCip;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.customer.ConsultaRucDataRequestBody;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.mailing.*;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.DateUtils;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipRequest;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipResponse;
import pe.com.tdp.ventafija.microservices.domain.customer.CustomerRucResponseData;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyResponseData;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import javax.swing.text.NumberFormatter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static pe.com.tdp.ventafija.microservices.controller.order.OrderWebController.parseStringBornDmY;

@Service
public class OfflineProcessor {

    private static final Logger logger = LogManager.getLogger(OfflineService.class);

    @Autowired
    private ParametersRepository parametersRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private DataSource dataSource;

    private EmailConfig constants;

    @Autowired
    private PeticionPagoEfectivoRepository pagoEfectivoRepository;

    String emailToken;

    @PostConstruct
    public void postConstruct() {

    }

    public boolean validaRuc(String numRuc,String razSoc){
        ConsultaRucDataRequestBody objRucRequest = new ConsultaRucDataRequestBody();
        CustomerRucResponseData objRucResponse = null;
        objRucRequest.setNumDoc(numRuc);
        objRucRequest.setSisOri("1");
        List<Object[]> objListaRuc = new ArrayList<>();
        try{
            RucService rucService = new RucService();
            rucService.configParameters(parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.api.consultaruc.uri").getStrValue(),
                    parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.api.consultaruc.apiid").getStrValue(),
                    parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.api.consultaruc.apisecret").getStrValue(),
                    parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.api.consultaruc.activo").getStrValue());

            if(rucService.getFlagRuc() == "false"){
                objListaRuc = parametersRepository.getDataRuc(numRuc);
            }
            Response<CustomerRucResponseData> objRuc = rucService.consultaRuc(objRucRequest,objListaRuc);

            if(objRuc != null && objRuc.getResponseData() != null){ //Avance Mijail
                objRucResponse = objRuc.getResponseData();

                if(objRucResponse.getRazonSocial().equalsIgnoreCase(razSoc) && objRucResponse.getEstContrib().equalsIgnoreCase("ACTIVO") && objRucResponse.getCondDomicilio().equalsIgnoreCase("HABIDO")){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
        }catch (Exception e){
            System.out.println(e.getStackTrace().toString());
            throw e;
        }

    }


    public boolean validaReniec(String dni,HashMap<String,String> body) throws  Exception{

        IdentifyRequest requestReniec = new IdentifyRequest();
        requestReniec.setDocumentType("DNI");
        requestReniec.setDocumentNumber(dni);
        try{
            ReniecService service = new ReniecService();

            service.setCredentials(parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.api.reniec.consultacliente.uri").getStrValue(),
                                   parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.api.reniec.consultacliente.apiid").getStrValue(),
                                   parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.api.reniec.consultacliente.apisecret").getStrValue()
                                    );

            Response<IdentifyResponseData> objReniec = service.identify(requestReniec);
            if(objReniec != null){
                if (!objReniec.getResponseCode().equals("-1")){
                    if (objReniec.getResponseData()!=null){
                        String apellidos = objReniec.getResponseData().getLastName()+" "+objReniec.getResponseData().getMotherLastName();

                        if (objReniec.getResponseData().getNames().equals(body.get("NOMBRECONTINGENCIA")) &&
                            apellidos.equals(body.get("APELLIDOSCONTINGENCIA")) &&
                            objReniec.getResponseData().getBirthdate().equals(body.get("FECHANACIMIENTO").replace("-","")) &&
                            objReniec.getResponseData().getDistrictBirthLocation().equals(body.get("DISTNACIMIENTO")) &&
                            objReniec.getResponseData().getProvinceBirthLocation().equals(body.get("PROVNACIMIENTO")) &&
                            objReniec.getResponseData().getDepartmentBirthLocation().equals(body.get("DEPNACIMIENTO")) &&
                            (objReniec.getResponseData().getMotherName().equals(body.get("MADRECONTINGENCIA")) ||
                            objReniec.getResponseData().getMotherName().contains(body.get("MADRECONTINGENCIA")))){
                            return true;
                        }else {
                            return false;
                        }
                    }else{
                        throw new Exception("Error que no deberia pasar");
                    }
                }else{
                    throw new Exception("Respuesta en un formato invalido");
                }
            }else{
                throw new Exception("Servicio no responde");
            }
        }catch (Exception e){
            throw e;
        }
    }

    public String generarCip(Order saveOrderRequest){

        String cip = null;
        GenerarCipResponse cipResponse = new GenerarCipResponse();
        GenerarCipRequest cipRequest = new GenerarCipRequest();

        PeticionPagoEfectivo ppe = registrarPeticionCIP(saveOrderRequest.getId());

        if (ppe != null) {
            cipRequest.setCurrency("PEN");
            cipRequest.setAmount(ppe.getTotal());
            cipRequest.setTransactionCode(saveOrderRequest.getId());
            cipRequest.setAdminEmail("lincoln.morales@telefonica.com");
            cipRequest.setDateExpiry(parseStringBornDmY(ppe.getFechaAExpirar()));
            cipRequest.setPaymentConcept("Movistar");
            cipRequest.setAdditionalData("Generando online");
            cipRequest.setUserEmail(saveOrderRequest.getCustomer().getEmail() != null || saveOrderRequest.getCustomer().getEmail() != "" ? saveOrderRequest.getCustomer().getEmail() : "Nulo@nulo.com");
            cipRequest.setUserName(saveOrderRequest.getCustomer().getFirstName());
            cipRequest.setUserLastName(saveOrderRequest.getCustomer().getLastName1() + " " + saveOrderRequest.getCustomer().getLastName2());
            cipRequest.setUserUbigeo("150115");
            cipRequest.setUserCountry("PERU");
            String documenttype;
            switch (saveOrderRequest.getCustomer().getDocNumber()) {
                case "DNI":
                    documenttype = "DNI";
                    break;
                case "CEX":
                    documenttype = "NAN";
                    break;
                case "PAS":
                    documenttype = "PAS";
                    break;
                case "RUC":
                    documenttype = "NAN";
                    break;
                case "Otros":
                    documenttype = "NAN";
                    break;
                default:
                    documenttype = "NAN";
                    break;
            }
            cipRequest.setUserDocumentType(documenttype);
            cipRequest.setUserDocumentNumber(saveOrderRequest.getCustomer().getDocNumber());
            cipRequest.setUserCodeCountry("+51");
            cipRequest.setUserPhone(saveOrderRequest.getCustomer().getCustomerPhone() != null || saveOrderRequest.getCustomer().getCustomerPhone() != "" ? saveOrderRequest.getCustomer().getCustomerPhone() : "999999999");
            String token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJNT1YiLCJqdGkiOiIxYjUxYmJjYi0wYmQ5LTQwMmUtYTRlOC1lYWIwNTQ2N2I3N2MiLCJuYW1laWQiOiIxMDQiLCJleHAiOjE1ODAwMTIxMDF9.2TVKKT-X7jFiWx-_7DJUZBMYkknrsNY1WX1tqipubAw";

            cipResponse = generarCIP(cipRequest, token);

            cip = cipResponse != null ? cipResponse.getData().getCip() + "" : null;
        }
        return cip;
    }

    public GenerarCipResponse generarCIP(GenerarCipRequest cipRequest, String token) {
        GenerarCipResponse response = new GenerarCipResponse();
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl("http://services.pagoefectivo.pe/v1/cips")
                .build();
        GenerarCip generarCip = new GenerarCip(config);
        pe.com.tdp.ventafija.microservices.common.clients.ClientResult<GenerarCipResponse> result = generarCip.postCIP(cipRequest, token);
        result.getEvent().setOrderId(cipRequest.getTransactionCode());
        try {
            if (!result.isSuccess()) {
                registerEvent(result.getEvent());
                response = result.getResult();
            } else {
                registerEvent(result.getEvent());
                actualizarPagoEfectivo(result.getResult().getData().getTransactionCode() + "", "EN" ,result.getResult().getData().getCip() + "");
                response = result.getResult();
            }
            //con.close();
        }catch (Exception e){

        }
        return response;
    }

    public PeticionPagoEfectivo registrarPeticionCIP(String orderId){

        logger.info("llamando a pago efectivo con el orderId " + orderId);

        Order order = orderRepository.findOne(orderId);

        String pagoEfectivoPaymentModeCondition = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","pe.api.paymentmode.condition").getStrValue();
        String pagoEfectivoInvalidProductTypeCode = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","pe.api.invalid.producttypecode").getStrValue();
        String pagoEfectivoCodigoServicio = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","pe.api.code.service").getStrValue();
        String conceptoPago = "";
        Integer pagoEfectivoExpirationDays = Integer.parseInt(parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","pe.api.expiration.days").getStrValue());

        if (pagoEfectivoPaymentModeCondition != null
                && !pagoEfectivoPaymentModeCondition.equalsIgnoreCase(order.getPaymentMode())) {
            return null;
        }

        if (pagoEfectivoInvalidProductTypeCode.equalsIgnoreCase(order.getProductType())) {
            return null;
        } else if (Constants.COMMERCIAL_OPE_SVA.equalsIgnoreCase(order.getCommercialOperation())) {
            if (pagoEfectivoInvalidProductTypeCode.equalsIgnoreCase(order.getProductCategory())) {
                return null;
            }
        }

        PeticionPagoEfectivo ppe = new PeticionPagoEfectivo();
        ppe.setCodServicio(pagoEfectivoCodigoServicio);
        ppe.setCodTransaccion(orderId);
        ppe.setConceptoPago(conceptoPago);
        ppe.setEmailComercio("");
        ppe.setFechaAExpirar(DateUtils.format("dd/MM/yyyy", DateUtils.addDays(new Date(), pagoEfectivoExpirationDays))
                + " 22:00:00");
        ppe.setIdMoneda("1");
        ppe.setMetodosPago("1");
        ppe.setOrderId(orderId);
        ppe.setStatus("PE"); // status pendiente

        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("0.00", dfs);
        NumberFormatter nf = new NumberFormatter(decimalFormat);
        try {
            ppe.setTotal(nf.valueToString(order.getCashPrice()));
        } catch (ParseException e) {
            ppe.setTotal(order.getCashPrice().toString());
        }

        ppe.setUsuarioApellidos(String.format("%s %s",
                order.getCustomer().getLastName1() == null ? "" : order.getCustomer().getLastName1(),
                order.getCustomer().getLastName2() == null ? "" : order.getCustomer().getLastName2()));
        ppe.setUsuarioNombre(order.getCustomer().getFirstName());
        ppe.setUsuarioTipoDoc(order.getCustomer().getDocType());
        ppe.setUsuarioNumeroDoc(order.getCustomer().getDocNumber());
        ppe.setUsuarioEmail(order.getCustomer().getEmail());
        ppe.setUsuarioId(order.getUser().getId() + "");
        return nuevaPeticion(ppe);
    }

    public PeticionPagoEfectivo nuevaPeticion (PeticionPagoEfectivo peticion) {
        logger.info("registrar peticion pago efectivo");
        peticion.setStatus("PE");
        PeticionPagoEfectivo ppe = pagoEfectivoRepository.save(peticion);
        logger.info("fin registro peticion pago efectivo");
        return ppe;
    }

    public int validateOffline(String firebaseId,HashMap<String,String> dataTomaPedido){
        //sendMailExpress("luis.ortega.mel@gmail.com","sub","text");

        boolean validacionRuc = true;
        boolean validacionReniec = true;

        Order order = orderRepository.findOne(firebaseId);

        if (order == null) {
            logger.info("validateOffline => order: null");
        }

        List<Object[]> data = orderRepository.spRetriveHdecData(firebaseId);

        LogSystem.infoArray(logger, "OfflineService", "validateOffline", "data", data);

        for (Object[] row : data) {
            String firstName = row[1] != null ? (String) row[1] : "";
            String docNumber = (String) row[5];


            if(dataTomaPedido.get("FLAG").indexOf('1')>-1){
                try{
                    validacionRuc = validaRuc(docNumber,firstName);
                }catch (Exception e){
                    LogSystem.info(logger, "OfflineService", "validateOffline", "Validacion RUC no responde");
                    return 0;
                }
            }

            if (!validacionRuc){
                return -1;
            }

            if(dataTomaPedido.get("FLAG").indexOf('6')>-1){
                try{
                    validacionReniec = validaReniec(docNumber,dataTomaPedido);
                }catch (Exception e){
                    LogSystem.info(logger, "OfflineService", "validateOffline", "Validacion RENIEC no responde ");
                    return 0;
                }
            }

            if (!validacionReniec){
                return -6;
            }

            if(dataTomaPedido.get("FLAG").indexOf('7')>-1){
                try{
                    String codigoGenerado = generarCip(order);
                    System.out.println("Se genero el codigo "+codigoGenerado);
                    if (codigoGenerado != null){
                        sendMailExpress(dataTomaPedido.get("EMAILVENDEDOR"),
                                "CODIGO PAGO EFECTIVO GENERADO",
                                "Se ha generado el codigo pago efectivo "+codigoGenerado);
                    }else{
                        LogSystem.info(logger, "OfflineService", "validateOffline", "Generacion CIP no responde");
                        return 0;
                    }
                }catch (Exception e){
                    LogSystem.info(logger, "OfflineService", "validateOffline", "Generacion CIP no responde");
                    return 0;
                }
            }

        }
        return 1;
    }


    public EmailResponseBody sendMailExpress(String email, String message, String subject){
        emailToken = "";
        constants = new EmailConfig(parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.mail.emblue.authenticate.uri").getStrValue(),
                parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.mail.emblue.checkconexion.uri").getStrValue(),
                parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.mail.emblue.sendexpress.uri").getStrValue(),
                parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.mail.emblue.user").getStrValue(),
                parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.mail.emblue.password").getStrValue(),
                parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.mail.emblue.token").getStrValue(),
                parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","PARAMETER","tdp.mail.emblue.sendmailexpresss.actionId").getStrValue());

        emailToken = getToken();

        EmailRequestBody request = new EmailRequestBody();
        request.setEmail(email);
        request.setActionId(constants.getEmailSendExpressActionId());
        request.setMessage(message);
        request.setToken(emailToken);
        request.setSubject(subject);

        EmailClient<EmailRequestBody, EmailResponseBody> emailSendExpress = new EmailClient<>(constants.getEmailSendExpressUri());
        ClientResult<EmailResponseBody> sendExpressResponse = emailSendExpress.sendRequest(request);
        return sendExpressResponse.getResult();
    }

    private String getToken(){
        if(emailToken.equals("")) emailToken = authenticate();

        Boolean result = checkConection(emailToken);
        if(!result) emailToken = authenticate();
        return emailToken;
    }

    private String authenticate(){
        AuthenticateRequestBody authenticateRequest = new AuthenticateRequestBody();
        authenticateRequest.setUser(constants.getEmailUser());
        authenticateRequest.setPass(constants.getEmailPassword());
        authenticateRequest.setToken(constants.getEmailToken());
        System.out.println("Autenticando");
        System.out.println(constants.getEmailUser());
        System.out.println(constants.getEmailPassword());
        System.out.println(constants.getEmailToken());
        EmailClient<AuthenticateRequestBody, EmailResponseBody> emailAuthenticate = new EmailClient<>(constants.getEmailAutenticateUri());
        ClientResult<EmailResponseBody> authenticateResponse = emailAuthenticate.sendRequest(authenticateRequest);

        String tokenToSend = authenticateResponse.getResult().getToken();
        System.out.println(tokenToSend);
        return tokenToSend;
    }

    private Boolean checkConection(String tokenToSend){
        CheckConexionRequestBody checkConexionRequest = new CheckConexionRequestBody();
        checkConexionRequest.setToken(tokenToSend);

        EmailClient<CheckConexionRequestBody, EmailResponseBody> emailCheckConexion = new EmailClient<>(constants.getEmailCheckConexionUri());
        ClientResult<EmailResponseBody> checkConexionResponse = emailCheckConexion.sendRequest(checkConexionRequest);

        EmailResponseBody checkConexionResult = checkConexionResponse.getResult();
        return (checkConexionResult != null) ? checkConexionResult.getResult() : false;
    }


    public void registerEvent(ServiceCallEvent event) {
        try {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events (event_datetime, tag, username, msg,"
                    + "orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp,"
                    + "sourceAppVersion, result) "
                    + "VALUES (CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?,?)";

            Connection con = dataSource.getConnection();
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "SERVICE_CALL");
            ps.setString(2, event.getUsername());
            ps.setString(3, event.getMsg());
            ps.setString(4, event.getOrderId());
            ps.setString(5, event.getDocNumber());
            ps.setString(6, event.getServiceCode());
            ps.setString(7, event.getServiceUrl());
            ps.setString(8, event.getServiceRequest());
            ps.setString(9, event.getServiceResponse());
            ps.setString(10, event.getSourceApp());
            ps.setString(11, event.getSourceAppVersion());
            ps.setString(12, event.getResult());
            ps.executeUpdate();

        } catch (Exception e) {

        }
    }

    public void actualizarPagoEfectivo(String id, String status, String CIP) {
        try {
            Connection con = dataSource.getConnection();
            PreparedStatement stm = con.prepareStatement("update ibmx_a07e6d02edaf552.peticion_pago_efectivo set status = ?, codpago = ? where order_id = ?");
            stm.setString(1, status);
            stm.setString(2, CIP);
            stm.setString(3, id);
            stm.executeUpdate();

        } catch (SQLException e) {
            logger.error("Error getMetadata DAO.", e);
        }
    }
}