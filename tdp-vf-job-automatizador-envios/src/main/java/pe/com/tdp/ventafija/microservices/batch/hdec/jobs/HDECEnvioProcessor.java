package pe.com.tdp.ventafija.microservices.batch.hdec.jobs;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaAutomatizador;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.LogAutomatizador;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.LogAutomatizadorService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextImpl;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

/**
 * Implementacion Processor de Spring Batch para la contingencia HDEC.
 * Se evaluan todas las ventas que se encuentran en estado 'ENVIANDO' y que se encuentren en dicho estado
 * un tiempo considerado excesivo (configurado en base datos).
 * Cada venta es enviada nuevamente a HDEC, se manejan los siguientes escenarios:
 * <ul>
 * <li> Si es que la respuesta es SATISFACTORIA o DUPLICADO entonces la venta es actualizada a estado PENDIENTE.
 * <li> Si es que la respuesta es un ERROR entonces se volvera a intentar el envio controlado por un limite de reintentos.
 * <li> Si es que la respuesta es un ERROR y ya se llego al limite de reintentos entonces se actualiza la venta con estado 'CAIDA'.
 * </ul>
 *
 * @author glazaror
 * @since 1.5
 */
public class HDECEnvioProcessor implements ItemProcessor<VentaAutomatizador, VentaAutomatizador> {

    private static final Logger logger = LogManager.getLogger(HDECEnvioProcessor.class);

    @Autowired
    private LogAutomatizadorService logAutomatizadorService;
    ;



    @Override
    public VentaAutomatizador process(final VentaAutomatizador ventaAutomatizador) throws Exception {
        LogSystem.info(logger, "AutomatizadorProcessor", "process", "Start");
        LogSystem.infoObject(logger, "AutomatizadorProcessor", "process", "VentaAutomatizador", ventaAutomatizador);

        try {
            startCallContext(ventaAutomatizador);
            LogAutomatizador finded  = logAutomatizadorService.findByCodvtappcd(ventaAutomatizador.getId());
            if(finded!=null){


                if(finded.getCodestvtaapp().equals("R")){
                    ventaAutomatizador.setEstado("INGRESADO");
                    if(finded.getDeserrds().equalsIgnoreCase("OK")){
                        if(finded.getCodpedaticd()!=null){
                            if(!finded.getCodpedaticd().isEmpty()){
                                ventaAutomatizador.setCodigoPedido(finded.getCodpedaticd());
                            }
                        }
                    }
                    if(finded.getDeserrds().equalsIgnoreCase("OK_CMS")){
                        if(finded.getCodreqeedcd()!=null){
                            if(!finded.getCodreqeedcd().isEmpty()){
                                ventaAutomatizador.setCodigoPedido(finded.getCodreqeedcd());
                            }
                        }
                    }
                }
                if(finded.getCodestvtaapp().equals("C")){
                    ventaAutomatizador.setEstado("CAIDA");
                }
                if(finded.getCodestvtaapp().equals("S")){
                   // ventaAutomatizador.setEstado("SEGUIMIENTO");
                    int numIntentos =  ventaAutomatizador.getAuto_retry() !=null ? ventaAutomatizador.getAuto_retry(): 0;
                    ventaAutomatizador.setAuto_retry(numIntentos+1);
                }
                if(finded.getCodestvtaapp().equals("N")){
                    ventaAutomatizador.setEstado("ENVIANDO");
                }


            }else{
                int numIntentos =  ventaAutomatizador.getAuto_retry() !=null ? ventaAutomatizador.getAuto_retry(): 0;
                ventaAutomatizador.setAuto_retry(numIntentos+1);

            }

        } catch (Exception ex) {
            LogSystem.error(logger, "AutomatizadorProcessor", "process", ex.toString(),ex);
        }
        endCallContext();
        LogSystem.infoObject(logger, "AutomatizadorProcessor", "process", "VentaAutomatizador", ventaAutomatizador);
        LogSystem.info(logger, "AutomatizadorProcessor", "process", "End");
        return ventaAutomatizador;
    }

    public void startCallContext(VentaAutomatizador ventaAutomatizador) {
        ServiceCallEvent event = new ServiceCallEvent();
        event.setOrderId(ventaAutomatizador.getId());
        event.setServiceCode("HDEC");
        event.setSourceApp("BATCH");
        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);
    }

    private void endCallContext() {
        VentaFijaContextHolder.clearContext();
    }

}