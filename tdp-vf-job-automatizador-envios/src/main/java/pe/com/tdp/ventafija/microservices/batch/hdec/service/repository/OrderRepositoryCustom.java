package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import java.util.List;


public interface OrderRepositoryCustom {

	/**
	 * Retrives data that needs to be sent to HDEC
	 * @param orderId that will be queried
	 * @return the data from query
	 */
	List<Object[]> spRetriveHdecData(String orderId);


}
