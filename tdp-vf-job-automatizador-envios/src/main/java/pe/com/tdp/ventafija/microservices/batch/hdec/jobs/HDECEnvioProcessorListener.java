package pe.com.tdp.ventafija.microservices.batch.hdec.jobs;

import org.springframework.batch.core.listener.ItemListenerSupport;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaAutomatizador;

/**
 * Implementacion ProcessorListener de Spring Batch para la contingencia HDEC.
 * Implementado para controlar errores.
 * @author      glazaror
 * @since       1.5
 */
public class HDECEnvioProcessorListener extends ItemListenerSupport<VentaAutomatizador, VentaAutomatizador> {

    public HDECEnvioProcessorListener() {
        super();
    }

    @Override
    public void onProcessError(VentaAutomatizador item, Exception e) {
        super.onProcessError(item, e);
    }

}
