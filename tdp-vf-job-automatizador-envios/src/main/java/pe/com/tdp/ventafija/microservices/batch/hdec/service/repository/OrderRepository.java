package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, String>, OrderRepositoryCustom {

}
