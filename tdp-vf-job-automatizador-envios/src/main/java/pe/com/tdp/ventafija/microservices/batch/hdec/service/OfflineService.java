package pe.com.tdp.ventafija.microservices.batch.hdec.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaBean;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper.VentaRowOfflineMapper;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextImpl;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;

@Service
public class OfflineService {

    private static final Logger logger = LogManager.getLogger(OfflineService.class);

    @Autowired
    private DataSource dataSource;

    @Autowired
    public OfflineProcessor offlineProcessor;

    public void process2() throws Exception{

        JdbcCursorItemReader<VentaBean> ventasReader = ventasReader();

        ventasReader.open(new ExecutionContext());
        VentaBean ventaBean;

        while ((ventaBean = ventasReader.read())!=null) {
            startCallContext(ventaBean);

            LogSystem.info(logger, "FlujoOfflineProcessor", "process", "Start");
            LogSystem.infoObject(logger, "FlujoOfflineProcessor", "process", "VentaBean", ventaBean);

            int success;

            HashMap<String, String> dataTomaPedido = new HashMap<>();

            String[] body = ventaBean.getOffline().split(";");

            for (int i = 0; i < body.length; i++) {
                String[] par = body[i].split(":");
                dataTomaPedido.put(par[0].trim().toUpperCase(), par[1].trim().toUpperCase());
            }

            success = offlineProcessor.validateOffline(ventaBean.getId(), dataTomaPedido);

            System.out.println(success);

            if (success == 1) {
                if (ventaBean.getEstadoAnterior() == null) {
                    if (dataTomaPedido.get("FLAG").indexOf('7') > -1) {
                        ventaBean.setEstado("PENDIENTE_CIP");
                    } else {
                        ventaBean.setEstado("ENVIANDO");
                    }
                } else if (ventaBean.getEstadoAnterior().equals("PENDIENTE")) {
                    if (dataTomaPedido.get("FLAG").indexOf('7') > -1) {
                        ventaBean.setEstado("PENDIENTE_CIP");
                    } else {
                        ventaBean.setEstado("PENDIENTE");
                    }
                }
            } else if (success < 0) {

                if (success == -1)
                    ventaBean.setMotivoCaida("DATOS INCORRECTOS EN RUC");
                if (success == -6)
                    ventaBean.setMotivoCaida("DATOS INCORRECTOS EN RENIEC");

                ventaBean.setEstado("CAIDA");
            }

            if (success!=0){
                updateVisor(ventaBean);
            }

        }
    }

    public JdbcCursorItemReader<VentaBean> ventasReader() {

        String sql = "SELECT * FROM (SELECT VIS.ID_VISOR, VIS.FECHA_GRABACION, VIS.ESTADO_SOLICITUD," +
                "VIS.DNI,VIS.ESTADO_ANTERIOR,ORD.OFFLINE , ORD.USERID AS COD_ATIS, VIS.MOTIVO_ESTADO " +
                "FROM ibmx_a07e6d02edaf552.TDP_VISOR AS VIS " +
                "INNER JOIN ibmx_a07e6d02edaf552.ORDER AS ORD ON ORD.ID = VIS.ID_VISOR " +
                "WHERE VIS.ESTADO_SOLICITUD = 'OFFLINE' " +
                "AND VIS.FECHA_GRABACION >  NOW() - INTERVAL '30 DAYS' " +
                //"AND VIS.ID_VISOR ='-nacok6ra63a3m2zqt8e' " +
                "ORDER BY VIS.FECHA_GRABACION ASC " +
                "LIMIT 1000) " +
                "ALS " +
                "LIMIT 10";


        LogSystem.infoQuery(logger, "BatchConfig", "ventasReader", "sql", sql);

        JdbcCursorItemReader<VentaBean> reader = new JdbcCursorItemReader<VentaBean>();
        reader.setDataSource(dataSource);
        reader.setSql(sql);
        reader.setRowMapper(new VentaRowOfflineMapper());
        reader.setFetchSize(1);
        return reader;
    }


    public void startCallContext(VentaBean ventaBean) {

        ServiceCallEvent event = new ServiceCallEvent();
        event.setUsername(ventaBean.getCodigoVendedor());
        event.setOrderId(ventaBean.getId());
        event.setServiceCode("OFFLINE");
        event.setDocNumber(ventaBean.getDniCliente());
        event.setSourceApp("BATCH");
        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);
    }

    public void updateVisor(VentaBean ventaBean) {

        try {
            Connection con = dataSource.getConnection();

            String sql = "UPDATE ibmx_a07e6d02edaf552.tdp_visor set estado_solicitud = '"+ventaBean.getEstado()+"',motivo_estado = '"+ventaBean.getMotivoCaida()+"' " +
                    "where id_visor = '"+ventaBean.getId()+"' AND estado_solicitud = 'OFFLINE'";
            System.out.println(sql);
            PreparedStatement ps = con.prepareStatement(sql);
            ps.executeUpdate();
            con.commit();
        } catch (Exception e) {

        }

    }
}
