package pe.com.tdp.ventafija.microservices.batch.hdec.bean;

import java.util.Date;

/**
 * Bean que representa una Venta y su estado de envio al BACK (tambien conocido como HDEC)
 * @author      glazaror
 * @since       1.5
 */
public class VentaBean {
    private String id;
    private String estado;
    private Date fechaGrabacion;
    private String estadoAnterior;
    private String offline;
    private String dniCliente;
    private String codigoVendedor;
    private String motivoCaida;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaGrabacion() {
        return fechaGrabacion;
    }

    public void setFechaGrabacion(Date fechaGrabacion) {
        this.fechaGrabacion = fechaGrabacion;
    }

    public String getDniCliente() {
        return dniCliente;
    }

    public void setDniCliente(String dniCliente) {
        this.dniCliente = dniCliente;
    }

    public String getCodigoVendedor() {
        return codigoVendedor;
    }

    public void setCodigoVendedor(String codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }

    public String getEstadoAnterior() {
        return estadoAnterior;
    }

    public void setEstadoAnterior(String estadoAnterior) {
        this.estadoAnterior = estadoAnterior;
    }

    public String getOffline() {
        return offline;
    }

    public void setOffline(String offline) {
        this.offline = offline;
    }

    public String getMotivoCaida() {
        return motivoCaida;
    }

    public void setMotivoCaida(String motivoCaida) {
        this.motivoCaida = motivoCaida;
    }
}
