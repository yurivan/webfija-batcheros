package pe.com.tdp.ventafija.microservices.batch.hdec;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.batch.automatizador.SendAutomatizadorService;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.OfflineService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.service.automatizador.AutomatizadorService;

import java.util.Calendar;
import java.util.Date;

/**
 * Scheduler de la contingencia HDEC. El cron es configurable en base datos y no soporta cambios en caliente.
 *
 * @author      glazaror
 * @since       1.5
 */
@Component
public class ContingenciaProcessorScheduler {

    private static final Logger logger = LogManager.getLogger(ContingenciaProcessorScheduler.class);


    @Autowired
    private SendAutomatizadorService sendAutomatizadorService;



   // @Scheduled(cron = "${tdp.offline.batch.cron}")
    @Scheduled(cron = "*0 * * * * *")
    public void startJob3() {

        try {
            LogSystem.info(logger, "AutomatizadorReintentosProcessorScheduler", "startJob", "Starting Automatizador reintentos sync..!");

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DAY_OF_MONTH, 1);
            c.add(Calendar.MONTH, 11);
            c.add(Calendar.HOUR_OF_DAY, 1);
            sendAutomatizadorService.process3();
            LogSystem.info(logger, "AutomatizadorReintentosProcessorScheduler", "endJob","Success");

        } catch (Exception e) {
            logger.error("Error startJob. ", e);
        }
    }
}
