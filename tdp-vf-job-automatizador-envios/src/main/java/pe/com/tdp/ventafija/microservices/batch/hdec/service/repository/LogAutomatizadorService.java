package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.LogAutomatizador;

@Repository
public interface LogAutomatizadorService extends JpaRepository<LogAutomatizador, Integer> {

    LogAutomatizador findByCodvtappcd(String codvtappcd);

}