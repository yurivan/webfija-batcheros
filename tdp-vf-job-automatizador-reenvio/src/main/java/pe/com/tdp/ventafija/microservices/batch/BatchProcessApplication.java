package pe.com.tdp.ventafija.microservices.batch;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import pe.com.tdp.ventafija.microservices.batch.config.DatabaseSourcePlaceholderConfigurer;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.IOException;

@EnableScheduling
@SpringBootApplication

@EnableJpaRepositories(basePackages = {"pe.com.tdp.ventafija.microservices.batch.hdec.service.repository"})
public class BatchProcessApplication {

    static boolean production = false;


    @Bean(name = "dataSource", destroyMethod = "close")
    public DataSource dataSource() throws FileNotFoundException, IOException {

        HikariConfig config = new HikariConfig();

        config.setDriverClassName(System.getenv("TDP_FIJA_DB_DRIVER"));
        config.setJdbcUrl(System.getenv("TDP_FIJA_DB_URL"));
        config.setUsername(System.getenv("TDP_FIJA_DB_USR"));
        config.setPassword(System.getenv("TDP_FIJA_DB_PW"));


        if (production) {
            config.setJdbcUrl("jdbc:postgresql://sl-us-south-1-portal.13.dblayer.com:28973/compose?useSSL=false");
            config.setPassword("IXECEUBFNJLAZZEV");
        }


        config.addDataSourceProperty("ApplicationName", "Hdec-Batch");
        config.setMinimumIdle(Integer.parseInt(System.getenv("TDP_FIJA_DB_MINIMUM_IDLE")));
        config.setMaximumPoolSize(Integer.parseInt(System.getenv("TDP_FIJA_DB_POOLING")));
        config.setIdleTimeout(Integer.parseInt(System.getenv("TDP_FIJA_DB_TIMEOUT_IDLE")));

        return new HikariDataSource(config);
    }

    @Bean
    public PropertyPlaceholderConfigurer databaseSourcePlaceholderConfigurer() {
        PropertyPlaceholderConfigurer props = new DatabaseSourcePlaceholderConfigurer();
        props.setIgnoreResourceNotFound(true);
        props.setIgnoreUnresolvablePlaceholders(true);
        props.setOrder(2);
        return props;
    }

    public static void main(String[] args) {
        SpringApplication.run(BatchProcessApplication.class, args);
    }
}
