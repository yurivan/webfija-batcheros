package pe.com.tdp.ventafija.microservices.batch.hdec.jobs;

import org.springframework.batch.core.listener.ItemListenerSupport;
import pe.com.tdp.ventafija.microservices.batch.automatizador.bean.VentaAutomatizadorBean;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaAutomatizador;

/**
 * Implementacion ProcessorListener de Spring Batch para la contingencia HDEC.
 * Implementado para controlar errores.
 * @author      glazaror
 * @since       1.5
 */
public class HDECEnvioProcessorListener extends ItemListenerSupport<VentaAutomatizadorBean, VentaAutomatizadorBean> {

    public HDECEnvioProcessorListener() {
        super();
    }

    @Override
    public void onProcessError(VentaAutomatizadorBean item, Exception e) {
        super.onProcessError(item, e);
    }

}
