package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.clients.arpu.OffersApiCalculadoraArpuResponseBody;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ServiceCallEventsDao {
    private Logger LOGGER = LogManager.getLogger(ServiceCallEventsDao.class);
    private DataSource datasource;

    @Autowired
    public ServiceCallEventsDao(DataSource datasource) {
        this.datasource = datasource;
    }

    public void registerEvent(ServiceCallEvent event) {
        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events (event_datetime, tag, username, msg,"
                    + "orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp,"
                    + "sourceAppVersion, result) "
                    + "VALUES (CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "SERVICE_CALL");
            ps.setString(2, event.getUsername());
            ps.setString(3, event.getMsg());
            ps.setString(4, event.getOrderId());
            ps.setString(5, event.getDocNumber());
            ps.setString(6, event.getServiceCode());
            ps.setString(7, event.getServiceUrl());
            ps.setString(8, event.getServiceRequest());
            ps.setString(9, event.getServiceResponse());
            ps.setString(10, event.getSourceApp());
            ps.setString(11, event.getSourceAppVersion());
            ps.setString(12, event.getResult());

            ps.executeUpdate();
        } catch (Exception e) {
            LOGGER.error("Error al registrar evento de llamada a API", e);
        }
    }

    public List<OffersApiCalculadoraArpuResponseBody> consultaCalculadora(String telefono) {
        List<OffersApiCalculadoraArpuResponseBody> response = new ArrayList<>();
        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_catalog " +
                    " where commercialoperation = 'Migracion' and canal ='Todos' ORDER BY priority asc ";

            try (PreparedStatement ps = con.prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    OffersApiCalculadoraArpuResponseBody item = new OffersApiCalculadoraArpuResponseBody();

                    item.setTelefono(telefono);
                    item.setServicio(telefono);
                    //item.setPaquetePsOrigen("22844");//--> productcode
                    //item.setPaqueteOrigen("Trio Plano Local 16 Mbps Estandar Digital");//--> Product
                    item.setCantidadDecos(null);
                    item.setRentaTotal(rs.getString("price"));//--> price
                    item.setInternet(rs.getString("internetspeed"));//--> internetspeed
                    item.setPrioridad(rs.getString("priority"));//--> priority
                    item.setProductoDestinoPs(rs.getString("productcode"));//--> productcode
                    if(rs.getString("bloquetv").length() >4){
                        item.setSVAs(rs.getString("bloquetv"));
                    }else if(rs.getString("svalinea").length() >4){
                        item.setSVAs(rs.getString("svalinea"));
                    }else if(rs.getString("svainternet").length() >4){
                        item.setSVAs(rs.getString("svainternet"));
                    }else{item.setSVAs(null);}

                    item.setProductoDestinoNombre(rs.getString("productname"));//--> productname
                    item.setMontoFinanciando(null);
                    item.setCuotasFinanciado(null);
                    item.setCodigoOferta(rs.getString("prodcategorycode")); //--> prodcategorycode

                    response.add(item);
                }

            }
            return response;
        } catch (Exception e) {
            LOGGER.error("Error al registrar evento de llamada a API", e);
        }
        return response= null;
    }

    public List<OffersApiCalculadoraArpuResponseBody> consultaCalculadora(String telefono, String psprincipal,String productDescription) {
        List<OffersApiCalculadoraArpuResponseBody> response = new ArrayList<>();
        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_catalog " +
                    " where commercialoperation = 'Migracion' and canal ='Todos' ORDER BY priority asc ";
            String price=null;
            String internetspeed=null;

            String sql1 = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_catalog " +
                    " where productcode = '"+psprincipal+"' ORDER BY id DESC LIMIT 1";

            try (PreparedStatement ps1 = con.prepareStatement(sql1)) {
                ResultSet rs1 = ps1.executeQuery();
                while (rs1.next()) {

                    price = rs1.getString("price");//--> price
                    internetspeed = rs1.getString("internetspeed");//--> internetspeed

                }
            }


            try (PreparedStatement ps = con.prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    OffersApiCalculadoraArpuResponseBody item = new OffersApiCalculadoraArpuResponseBody();

                    item.setTelefono(telefono);
                    item.setServicio(null);
                    item.setPaquetePsOrigen(psprincipal);//--> productcode
                    item.setPaqueteOrigen(productDescription);//--> Product
                    item.setCantidadDecos("2");
                    item.setRentaTotal(price);//--> price
                    item.setInternet(internetspeed+"000");//--> internetspeed
                    item.setPrioridad(rs.getString("priority"));//--> priority
                    item.setProductoDestinoPs(rs.getString("productcode"));//--> productcode
                    if(rs.getString("bloquetv").length() >3){
                        item.setSVAs(rs.getString("bloquetv"));
                    }else if(rs.getString("svalinea").length() >3){
                        item.setSVAs(rs.getString("svalinea"));
                    }else if(rs.getString("svainternet").length() >3){
                        item.setSVAs(rs.getString("svainternet"));
                    }else{item.setSVAs(null);}

                    item.setProductoDestinoNombre(rs.getString("productname"));//--> productname
                    item.setMontoFinanciando(null);
                    item.setCuotasFinanciado(null);
                    item.setCodigoOferta(rs.getString("prodcategorycode")); //--> prodcategorycode

                    response.add(item);
                }

            }
            return response;
        } catch (Exception e) {
            LOGGER.error("Error al registrar evento de llamada a API", e);
        }
        return response= null;
    }

}
