package pe.com.tdp.ventafija.microservices.batch.automatizador.service;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "automatizador_sale_request_body", schema = "ibmx_a07e6d02edaf552")
public class AutomatizadorSaleService {


    @Id
    @Column(name = "COD_VTA_APP_CD") //=> ID
    private String codvtappcd;

    @Column(name ="NOM_DS")
    private String nom_ds;
    @Column(name ="PRI_APE_DS")
    private String pri_ape_ds;
    @Column(name ="SEG_APE_DS")
    private String seg_ape_ds;
    @Column(name ="NUM_DOC_CLI_CD")
    private String num_doc_cli_cd;
    @Column(name ="DIG_CTL_DOC_CD")
    private String dig_ctl_doc_cd;
    @Column(name ="TIP_DOC_CD")
    private String tip_doc_cd;
    @Column(name ="TEL_PRI_COT_CD")
    private String tel_pri_cot_cd;
    @Column(name ="TEL_SEG_COT_CD")
    private String tel_seg_cot_cd;
    @Column(name ="COD_ACT_ECO_CD")
    private String cod_act_eco_cd;
    @Column(name ="COD_SAC_ECO_CD")
    private String cod_sac_eco_cd;
    @Column(name ="TIP_CAL_ATI_CD")
    private String tip_cal_ati_cd;
    @Column(name ="NOM_CAL_DS")
    private String nom_cal_ds;
    @Column(name ="NUM_CAL_NU")
    private String num_cal_nu;
    @Column(name ="COD_CNJ_HBL_CD")
    private String cod_cnj_hbl_cd;
    @Column(name ="TIP_CNJ_HBL_CD")
    private String tip_cnj_hbl_cd;
    @Column(name ="NOM_CNJ_HBL_NO")
    private String nom_cnj_hbl_no;
    @Column(name ="PRI_TIP_CMP_CD")
    private String pri_tip_cmp_cd;
    @Column(name ="PRI_CMP_DIR_DS")
    private String pri_cmp_dir_ds;
    @Column(name ="SEG_TIP_CMP_CD")
    private String seg_tip_cmp_cd;
    @Column(name ="SEG_CMP_DIR_DS")
    private String seg_cmp_dir_ds;
    @Column(name ="TRC_TIP_CMP_CD")
    private String trc_tip_cmp_cd;
    @Column(name ="TRC_CMP_DIR_DS")
    private String trc_cmp_dir_ds;
    @Column(name ="CAO_TIP_CMP_CD")
    private String cao_tip_cmp_cd;
    @Column(name ="CAO_CMP_DIR_DS")
    private String cao_cmp_dir_ds;
    @Column(name ="QUI_TIP_CMP_CD")
    private String qui_tip_cmp_cd;
    @Column(name ="QUI_CMP_DIR_DS")
    private String qui_cmp_dir_ds;
    @Column(name ="SET_TIP_CMP_CD")
    private String set_tip_cmp_cd;
    @Column(name ="SET_CMP_DIR_DS")
    private String set_cmp_dir_ds;
    @Column(name ="SPT_TIP_CMP_CD")
    private String spt_tip_cmp_cd;
    @Column(name ="SPT_CMP_DIR_DS")
    private String spt_cmp_dir_ds;
    @Column(name ="OCT_TIP_CMP_CD")
    private String oct_tip_cmp_cd;
    @Column(name ="OCT_CMP_DIR_DS")
    private String oct_cmp_dir_ds;
    @Column(name ="NOV_TIP_CMP_CD")
    private String nov_tip_cmp_cd;
    @Column(name ="NOV_CMP_DIR_DS")
    private String nov_cmp_dir_ds;
    @Column(name ="DCO_TIP_CMP_CD")
    private String dco_tip_cmp_cd;
    @Column(name ="DCO_CMP_DIR_DS")
    private String dco_cmp_dir_ds;
    @Column(name ="MSX_CBR_VOI_GES_IN")
    private String msx_cbr_voi_ges_in;
    @Column(name ="MSX_CBR_VOI_GIS_IN")
    private String msx_cbr_voi_gis_in;
    @Column(name ="MSX_IND_SNL_GIS_CD")
    private String msx_ind_snl_gis_cd;
    @Column(name ="MSX_IND_GPO_GIS_CD")
    private String msx_ind_gpo_gis_cd;
    @Column(name ="COD_FZA_VEN_CD")
    private String cod_fza_ven_cd;
    @Column(name ="COD_FZA_GEN_CD")
    private String cod_fza_gen_cd;
    @Column(name ="COD_CNL_VEN_CD")
    private String cod_cnl_ven_cd;
    @Column(name ="COD_IND_SEN_CMS")
    private String cod_ind_sen_cms;
    @Column(name ="COD_CAB_CMS")
    private String cod_cab_cms;
    @Column(name ="ID_COD_PRO_CD")
    private String id_cod_pro_cd;
    @Column(name ="PS_ADM_DEP_1")
    private String ps_adm_dep_1;
    @Column(name ="PS_ADM_DEP_2")
    private String ps_adm_dep_2;
    @Column(name ="PS_ADM_DEP_3")
    private String ps_adm_dep_3;
    @Column(name ="PS_ADM_DEP_4")
    private String ps_adm_dep_4;
    @Column(name ="ID_COD_SVA_CD_1")
    private String id_cod_sva_cd_1;
    @Column(name ="ID_COD_SVA_CD_2")
    private String id_cod_sva_cd_2;
    @Column(name ="ID_COD_SVA_CD_3")
    private String id_cod_sva_cd_3;
    @Column(name ="ID_COD_SVA_CD_4")
    private String id_cod_sva_cd_4;
    @Column(name ="ID_COD_SVA_CD_5")
    private String id_cod_sva_cd_5;
    @Column(name ="ID_COD_SVA_CD_6")
    private String id_cod_sva_cd_6;
    @Column(name ="ID_COD_SVA_CD_7")
    private String id_cod_sva_cd_7;
    @Column(name ="ID_COD_SVA_CD_8")
    private String id_cod_sva_cd_8;
    @Column(name ="ID_COD_SVA_CD_9")
    private String id_cod_sva_cd_9;
    @Column(name ="ID_COD_SVA_CD_10")
    private String id_cod_sva_cd_10;
    @Column(name ="FEC_VTA_FF")
    private String fec_vta_ff;
    @Column(name ="NUM_IDE_TLF")
    private String num_ide_tlf;
    @Column(name ="COD_CLI_EXT_CD")
    private String cod_cli_ext_cd;
    @Column(name ="TIP_TRX_CMR_CD")
    private String tip_trx_cmr_cd;
    @Column(name ="COD_SRV_CMS_CD")
    private String cod_srv_cms_cd;
    @Column(name ="FEC_NAC_FF")
    private String fec_nac_ff;
    @Column(name ="COD_SEX_CD")
    private String cod_sex_cd;
    @Column(name ="COD_EST_CIV_CD")
    private String cod_est_civ_cd;
    @Column(name ="DES_MAI_CLI_DS")
    private String des_mai_cli_ds;
    @Column(name ="COD_ARE_TE1_CD")
    private String cod_are_te1_cd;
    @Column(name ="COD_ARE_TE2_CD")
    private String cod_are_te2_cd;
    @Column(name ="COD_CIC_FAC_CD")
    private String cod_cic_fac_cd;
    @Column(name ="COD_DEP_CD")
    private String cod_dep_cd;
    @Column(name ="COD_PRV_CD")
    private String cod_prv_cd;
    @Column(name ="COD_DIS_CD")
    private String cod_dis_cd;
    @Column(name ="COD_FAC_TEC_CD")
    private String cod_fac_tec_cd;
    @Column(name ="NUM_COD_X_CD")
    private String num_cod_x_cd;
    @Column(name ="NUM_COD_Y_CD")
    private String num_cod_y_cd;
    @Column(name ="COD_VEN_CMS_CD")
    private String cod_ven_cms_cd;
    @Column(name ="COD_SVE_CMS_CD")
    private String cod_sve_cms_cd;
    @Column(name ="NOM_VEN_DS")
    private String nom_ven_ds;
    @Column(name ="REF_DIR_ENT_DS")
    private String ref_dir_ent_ds;
    @Column(name ="COD_TIP_CAL_CD")
    private String cod_tip_cal_cd;
    @Column(name ="DES_TIP_CAL_CD")
    private String des_tip_cal_cd;
    @Column(name ="TIP_MOD_EQU_CD")
    private String tip_mod_equ_cd;
    @Column(name ="NUM_DOC_VEN_CD")
    private String num_doc_ven_cd;
    @Column(name ="NUM_TEL_VEN_DS")
    private String num_tel_ven_ds;
    @Column(name ="IND_ENV_CON_CD")
    private String ind_env_con_cd;
    @Column(name ="IND_ID_GRA_CD")
    private String ind_id_gra_cd;
    @Column(name ="MOD_ACE_VEN_CD")
    private String mod_ace_ven_cd;
    @Column(name ="DET_CAN_VEN_DS")
    private String det_can_ven_ds;
    @Column(name ="COD_REG_CD")
    private String cod_reg_cd;
    @Column(name ="COD_CIP_CD")
    private String cod_cip_cd;
    @Column(name ="COD_EXP_CD")
    private String cod_exp_cd;
    @Column(name ="COD_ZON_VEN_CD")
    private String cod_zon_ven_cd;
    @Column(name ="IND_WEB_PAR_CD")
    private String ind_web_par_cd;
    @Column(name ="COD_STC")
    private String cod_stc;
    @Column(name ="DES_NAC_DS")
    private String des_nac_ds;
    @Column(name ="COD_MOD_VEN_CD")
    private String cod_mod_ven_cd;
    @Column(name ="CAN_EQU_VEN")
    private String can_equ_ven;

    public String getCodvtappcd() {
        return codvtappcd;
    }

    public void setCodvtappcd(String codvtappcd) {
        this.codvtappcd = codvtappcd;
    }

    public String getNom_ds() {
        return nom_ds;
    }

    public void setNom_ds(String nom_ds) {
        this.nom_ds = nom_ds;
    }

    public String getPri_ape_ds() {
        return pri_ape_ds;
    }

    public void setPri_ape_ds(String pri_ape_ds) {
        this.pri_ape_ds = pri_ape_ds;
    }

    public String getSeg_ape_ds() {
        return seg_ape_ds;
    }

    public void setSeg_ape_ds(String seg_ape_ds) {
        this.seg_ape_ds = seg_ape_ds;
    }

    public String getNum_doc_cli_cd() {
        return num_doc_cli_cd;
    }

    public void setNum_doc_cli_cd(String num_doc_cli_cd) {
        this.num_doc_cli_cd = num_doc_cli_cd;
    }

    public String getDig_ctl_doc_cd() {
        return dig_ctl_doc_cd;
    }

    public void setDig_ctl_doc_cd(String dig_ctl_doc_cd) {
        this.dig_ctl_doc_cd = dig_ctl_doc_cd;
    }

    public String getTip_doc_cd() {
        return tip_doc_cd;
    }

    public void setTip_doc_cd(String tip_doc_cd) {
        this.tip_doc_cd = tip_doc_cd;
    }

    public String getTel_pri_cot_cd() {
        return tel_pri_cot_cd;
    }

    public void setTel_pri_cot_cd(String tel_pri_cot_cd) {
        this.tel_pri_cot_cd = tel_pri_cot_cd;
    }

    public String getTel_seg_cot_cd() {
        return tel_seg_cot_cd;
    }

    public void setTel_seg_cot_cd(String tel_seg_cot_cd) {
        this.tel_seg_cot_cd = tel_seg_cot_cd;
    }

    public String getCod_act_eco_cd() {
        return cod_act_eco_cd;
    }

    public void setCod_act_eco_cd(String cod_act_eco_cd) {
        this.cod_act_eco_cd = cod_act_eco_cd;
    }

    public String getCod_sac_eco_cd() {
        return cod_sac_eco_cd;
    }

    public void setCod_sac_eco_cd(String cod_sac_eco_cd) {
        this.cod_sac_eco_cd = cod_sac_eco_cd;
    }

    public String getTip_cal_ati_cd() {
        return tip_cal_ati_cd;
    }

    public void setTip_cal_ati_cd(String tip_cal_ati_cd) {
        this.tip_cal_ati_cd = tip_cal_ati_cd;
    }

    public String getNom_cal_ds() {
        return nom_cal_ds;
    }

    public void setNom_cal_ds(String nom_cal_ds) {
        this.nom_cal_ds = nom_cal_ds;
    }

    public String getNum_cal_nu() {
        return num_cal_nu;
    }

    public void setNum_cal_nu(String num_cal_nu) {
        this.num_cal_nu = num_cal_nu;
    }

    public String getCod_cnj_hbl_cd() {
        return cod_cnj_hbl_cd;
    }

    public void setCod_cnj_hbl_cd(String cod_cnj_hbl_cd) {
        this.cod_cnj_hbl_cd = cod_cnj_hbl_cd;
    }

    public String getTip_cnj_hbl_cd() {
        return tip_cnj_hbl_cd;
    }

    public void setTip_cnj_hbl_cd(String tip_cnj_hbl_cd) {
        this.tip_cnj_hbl_cd = tip_cnj_hbl_cd;
    }

    public String getNom_cnj_hbl_no() {
        return nom_cnj_hbl_no;
    }

    public void setNom_cnj_hbl_no(String nom_cnj_hbl_no) {
        this.nom_cnj_hbl_no = nom_cnj_hbl_no;
    }

    public String getPri_tip_cmp_cd() {
        return pri_tip_cmp_cd;
    }

    public void setPri_tip_cmp_cd(String pri_tip_cmp_cd) {
        this.pri_tip_cmp_cd = pri_tip_cmp_cd;
    }

    public String getPri_cmp_dir_ds() {
        return pri_cmp_dir_ds;
    }

    public void setPri_cmp_dir_ds(String pri_cmp_dir_ds) {
        this.pri_cmp_dir_ds = pri_cmp_dir_ds;
    }

    public String getSeg_tip_cmp_cd() {
        return seg_tip_cmp_cd;
    }

    public void setSeg_tip_cmp_cd(String seg_tip_cmp_cd) {
        this.seg_tip_cmp_cd = seg_tip_cmp_cd;
    }

    public String getSeg_cmp_dir_ds() {
        return seg_cmp_dir_ds;
    }

    public void setSeg_cmp_dir_ds(String seg_cmp_dir_ds) {
        this.seg_cmp_dir_ds = seg_cmp_dir_ds;
    }

    public String getTrc_tip_cmp_cd() {
        return trc_tip_cmp_cd;
    }

    public void setTrc_tip_cmp_cd(String trc_tip_cmp_cd) {
        this.trc_tip_cmp_cd = trc_tip_cmp_cd;
    }

    public String getTrc_cmp_dir_ds() {
        return trc_cmp_dir_ds;
    }

    public void setTrc_cmp_dir_ds(String trc_cmp_dir_ds) {
        this.trc_cmp_dir_ds = trc_cmp_dir_ds;
    }

    public String getCao_tip_cmp_cd() {
        return cao_tip_cmp_cd;
    }

    public void setCao_tip_cmp_cd(String cao_tip_cmp_cd) {
        this.cao_tip_cmp_cd = cao_tip_cmp_cd;
    }

    public String getCao_cmp_dir_ds() {
        return cao_cmp_dir_ds;
    }

    public void setCao_cmp_dir_ds(String cao_cmp_dir_ds) {
        this.cao_cmp_dir_ds = cao_cmp_dir_ds;
    }

    public String getQui_tip_cmp_cd() {
        return qui_tip_cmp_cd;
    }

    public void setQui_tip_cmp_cd(String qui_tip_cmp_cd) {
        this.qui_tip_cmp_cd = qui_tip_cmp_cd;
    }

    public String getQui_cmp_dir_ds() {
        return qui_cmp_dir_ds;
    }

    public void setQui_cmp_dir_ds(String qui_cmp_dir_ds) {
        this.qui_cmp_dir_ds = qui_cmp_dir_ds;
    }

    public String getSet_tip_cmp_cd() {
        return set_tip_cmp_cd;
    }

    public void setSet_tip_cmp_cd(String set_tip_cmp_cd) {
        this.set_tip_cmp_cd = set_tip_cmp_cd;
    }

    public String getSet_cmp_dir_ds() {
        return set_cmp_dir_ds;
    }

    public void setSet_cmp_dir_ds(String set_cmp_dir_ds) {
        this.set_cmp_dir_ds = set_cmp_dir_ds;
    }

    public String getSpt_tip_cmp_cd() {
        return spt_tip_cmp_cd;
    }

    public void setSpt_tip_cmp_cd(String spt_tip_cmp_cd) {
        this.spt_tip_cmp_cd = spt_tip_cmp_cd;
    }

    public String getSpt_cmp_dir_ds() {
        return spt_cmp_dir_ds;
    }

    public void setSpt_cmp_dir_ds(String spt_cmp_dir_ds) {
        this.spt_cmp_dir_ds = spt_cmp_dir_ds;
    }

    public String getOct_tip_cmp_cd() {
        return oct_tip_cmp_cd;
    }

    public void setOct_tip_cmp_cd(String oct_tip_cmp_cd) {
        this.oct_tip_cmp_cd = oct_tip_cmp_cd;
    }

    public String getOct_cmp_dir_ds() {
        return oct_cmp_dir_ds;
    }

    public void setOct_cmp_dir_ds(String oct_cmp_dir_ds) {
        this.oct_cmp_dir_ds = oct_cmp_dir_ds;
    }

    public String getNov_tip_cmp_cd() {
        return nov_tip_cmp_cd;
    }

    public void setNov_tip_cmp_cd(String nov_tip_cmp_cd) {
        this.nov_tip_cmp_cd = nov_tip_cmp_cd;
    }

    public String getNov_cmp_dir_ds() {
        return nov_cmp_dir_ds;
    }

    public void setNov_cmp_dir_ds(String nov_cmp_dir_ds) {
        this.nov_cmp_dir_ds = nov_cmp_dir_ds;
    }

    public String getDco_tip_cmp_cd() {
        return dco_tip_cmp_cd;
    }

    public void setDco_tip_cmp_cd(String dco_tip_cmp_cd) {
        this.dco_tip_cmp_cd = dco_tip_cmp_cd;
    }

    public String getDco_cmp_dir_ds() {
        return dco_cmp_dir_ds;
    }

    public void setDco_cmp_dir_ds(String dco_cmp_dir_ds) {
        this.dco_cmp_dir_ds = dco_cmp_dir_ds;
    }

    public String getMsx_cbr_voi_ges_in() {
        return msx_cbr_voi_ges_in;
    }

    public void setMsx_cbr_voi_ges_in(String msx_cbr_voi_ges_in) {
        this.msx_cbr_voi_ges_in = msx_cbr_voi_ges_in;
    }

    public String getMsx_cbr_voi_gis_in() {
        return msx_cbr_voi_gis_in;
    }

    public void setMsx_cbr_voi_gis_in(String msx_cbr_voi_gis_in) {
        this.msx_cbr_voi_gis_in = msx_cbr_voi_gis_in;
    }

    public String getMsx_ind_snl_gis_cd() {
        return msx_ind_snl_gis_cd;
    }

    public void setMsx_ind_snl_gis_cd(String msx_ind_snl_gis_cd) {
        this.msx_ind_snl_gis_cd = msx_ind_snl_gis_cd;
    }

    public String getMsx_ind_gpo_gis_cd() {
        return msx_ind_gpo_gis_cd;
    }

    public void setMsx_ind_gpo_gis_cd(String msx_ind_gpo_gis_cd) {
        this.msx_ind_gpo_gis_cd = msx_ind_gpo_gis_cd;
    }

    public String getCod_fza_ven_cd() {
        return cod_fza_ven_cd;
    }

    public void setCod_fza_ven_cd(String cod_fza_ven_cd) {
        this.cod_fza_ven_cd = cod_fza_ven_cd;
    }

    public String getCod_fza_gen_cd() {
        return cod_fza_gen_cd;
    }

    public void setCod_fza_gen_cd(String cod_fza_gen_cd) {
        this.cod_fza_gen_cd = cod_fza_gen_cd;
    }

    public String getCod_cnl_ven_cd() {
        return cod_cnl_ven_cd;
    }

    public void setCod_cnl_ven_cd(String cod_cnl_ven_cd) {
        this.cod_cnl_ven_cd = cod_cnl_ven_cd;
    }

    public String getCod_ind_sen_cms() {
        return cod_ind_sen_cms;
    }

    public void setCod_ind_sen_cms(String cod_ind_sen_cms) {
        this.cod_ind_sen_cms = cod_ind_sen_cms;
    }

    public String getCod_cab_cms() {
        return cod_cab_cms;
    }

    public void setCod_cab_cms(String cod_cab_cms) {
        this.cod_cab_cms = cod_cab_cms;
    }

    public String getId_cod_pro_cd() {
        return id_cod_pro_cd;
    }

    public void setId_cod_pro_cd(String id_cod_pro_cd) {
        this.id_cod_pro_cd = id_cod_pro_cd;
    }

    public String getPs_adm_dep_1() {
        return ps_adm_dep_1;
    }

    public void setPs_adm_dep_1(String ps_adm_dep_1) {
        this.ps_adm_dep_1 = ps_adm_dep_1;
    }

    public String getPs_adm_dep_2() {
        return ps_adm_dep_2;
    }

    public void setPs_adm_dep_2(String ps_adm_dep_2) {
        this.ps_adm_dep_2 = ps_adm_dep_2;
    }

    public String getPs_adm_dep_3() {
        return ps_adm_dep_3;
    }

    public void setPs_adm_dep_3(String ps_adm_dep_3) {
        this.ps_adm_dep_3 = ps_adm_dep_3;
    }

    public String getPs_adm_dep_4() {
        return ps_adm_dep_4;
    }

    public void setPs_adm_dep_4(String ps_adm_dep_4) {
        this.ps_adm_dep_4 = ps_adm_dep_4;
    }

    public String getId_cod_sva_cd_1() {
        return id_cod_sva_cd_1;
    }

    public void setId_cod_sva_cd_1(String id_cod_sva_cd_1) {
        this.id_cod_sva_cd_1 = id_cod_sva_cd_1;
    }

    public String getId_cod_sva_cd_2() {
        return id_cod_sva_cd_2;
    }

    public void setId_cod_sva_cd_2(String id_cod_sva_cd_2) {
        this.id_cod_sva_cd_2 = id_cod_sva_cd_2;
    }

    public String getId_cod_sva_cd_3() {
        return id_cod_sva_cd_3;
    }

    public void setId_cod_sva_cd_3(String id_cod_sva_cd_3) {
        this.id_cod_sva_cd_3 = id_cod_sva_cd_3;
    }

    public String getId_cod_sva_cd_4() {
        return id_cod_sva_cd_4;
    }

    public void setId_cod_sva_cd_4(String id_cod_sva_cd_4) {
        this.id_cod_sva_cd_4 = id_cod_sva_cd_4;
    }

    public String getId_cod_sva_cd_5() {
        return id_cod_sva_cd_5;
    }

    public void setId_cod_sva_cd_5(String id_cod_sva_cd_5) {
        this.id_cod_sva_cd_5 = id_cod_sva_cd_5;
    }

    public String getId_cod_sva_cd_6() {
        return id_cod_sva_cd_6;
    }

    public void setId_cod_sva_cd_6(String id_cod_sva_cd_6) {
        this.id_cod_sva_cd_6 = id_cod_sva_cd_6;
    }

    public String getId_cod_sva_cd_7() {
        return id_cod_sva_cd_7;
    }

    public void setId_cod_sva_cd_7(String id_cod_sva_cd_7) {
        this.id_cod_sva_cd_7 = id_cod_sva_cd_7;
    }

    public String getId_cod_sva_cd_8() {
        return id_cod_sva_cd_8;
    }

    public void setId_cod_sva_cd_8(String id_cod_sva_cd_8) {
        this.id_cod_sva_cd_8 = id_cod_sva_cd_8;
    }

    public String getId_cod_sva_cd_9() {
        return id_cod_sva_cd_9;
    }

    public void setId_cod_sva_cd_9(String id_cod_sva_cd_9) {
        this.id_cod_sva_cd_9 = id_cod_sva_cd_9;
    }

    public String getId_cod_sva_cd_10() {
        return id_cod_sva_cd_10;
    }

    public void setId_cod_sva_cd_10(String id_cod_sva_cd_10) {
        this.id_cod_sva_cd_10 = id_cod_sva_cd_10;
    }

    public String getFec_vta_ff() {
        return fec_vta_ff;
    }

    public void setFec_vta_ff(String fec_vta_ff) {
        this.fec_vta_ff = fec_vta_ff;
    }

    public String getNum_ide_tlf() {
        return num_ide_tlf;
    }

    public void setNum_ide_tlf(String num_ide_tlf) {
        this.num_ide_tlf = num_ide_tlf;
    }

    public String getCod_cli_ext_cd() {
        return cod_cli_ext_cd;
    }

    public void setCod_cli_ext_cd(String cod_cli_ext_cd) {
        this.cod_cli_ext_cd = cod_cli_ext_cd;
    }

    public String getTip_trx_cmr_cd() {
        return tip_trx_cmr_cd;
    }

    public void setTip_trx_cmr_cd(String tip_trx_cmr_cd) {
        this.tip_trx_cmr_cd = tip_trx_cmr_cd;
    }

    public String getCod_srv_cms_cd() {
        return cod_srv_cms_cd;
    }

    public void setCod_srv_cms_cd(String cod_srv_cms_cd) {
        this.cod_srv_cms_cd = cod_srv_cms_cd;
    }

    public String getFec_nac_ff() {
        return fec_nac_ff;
    }

    public void setFec_nac_ff(String fec_nac_ff) {
        this.fec_nac_ff = fec_nac_ff;
    }

    public String getCod_sex_cd() {
        return cod_sex_cd;
    }

    public void setCod_sex_cd(String cod_sex_cd) {
        this.cod_sex_cd = cod_sex_cd;
    }

    public String getCod_est_civ_cd() {
        return cod_est_civ_cd;
    }

    public void setCod_est_civ_cd(String cod_est_civ_cd) {
        this.cod_est_civ_cd = cod_est_civ_cd;
    }

    public String getDes_mai_cli_ds() {
        return des_mai_cli_ds;
    }

    public void setDes_mai_cli_ds(String des_mai_cli_ds) {
        this.des_mai_cli_ds = des_mai_cli_ds;
    }

    public String getCod_are_te1_cd() {
        return cod_are_te1_cd;
    }

    public void setCod_are_te1_cd(String cod_are_te1_cd) {
        this.cod_are_te1_cd = cod_are_te1_cd;
    }

    public String getCod_are_te2_cd() {
        return cod_are_te2_cd;
    }

    public void setCod_are_te2_cd(String cod_are_te2_cd) {
        this.cod_are_te2_cd = cod_are_te2_cd;
    }

    public String getCod_cic_fac_cd() {
        return cod_cic_fac_cd;
    }

    public void setCod_cic_fac_cd(String cod_cic_fac_cd) {
        this.cod_cic_fac_cd = cod_cic_fac_cd;
    }

    public String getCod_dep_cd() {
        return cod_dep_cd;
    }

    public void setCod_dep_cd(String cod_dep_cd) {
        this.cod_dep_cd = cod_dep_cd;
    }

    public String getCod_prv_cd() {
        return cod_prv_cd;
    }

    public void setCod_prv_cd(String cod_prv_cd) {
        this.cod_prv_cd = cod_prv_cd;
    }

    public String getCod_dis_cd() {
        return cod_dis_cd;
    }

    public void setCod_dis_cd(String cod_dis_cd) {
        this.cod_dis_cd = cod_dis_cd;
    }

    public String getCod_fac_tec_cd() {
        return cod_fac_tec_cd;
    }

    public void setCod_fac_tec_cd(String cod_fac_tec_cd) {
        this.cod_fac_tec_cd = cod_fac_tec_cd;
    }

    public String getNum_cod_x_cd() {
        return num_cod_x_cd;
    }

    public void setNum_cod_x_cd(String num_cod_x_cd) {
        this.num_cod_x_cd = num_cod_x_cd;
    }

    public String getNum_cod_y_cd() {
        return num_cod_y_cd;
    }

    public void setNum_cod_y_cd(String num_cod_y_cd) {
        this.num_cod_y_cd = num_cod_y_cd;
    }

    public String getCod_ven_cms_cd() {
        return cod_ven_cms_cd;
    }

    public void setCod_ven_cms_cd(String cod_ven_cms_cd) {
        this.cod_ven_cms_cd = cod_ven_cms_cd;
    }

    public String getCod_sve_cms_cd() {
        return cod_sve_cms_cd;
    }

    public void setCod_sve_cms_cd(String cod_sve_cms_cd) {
        this.cod_sve_cms_cd = cod_sve_cms_cd;
    }

    public String getNom_ven_ds() {
        return nom_ven_ds;
    }

    public void setNom_ven_ds(String nom_ven_ds) {
        this.nom_ven_ds = nom_ven_ds;
    }

    public String getRef_dir_ent_ds() {
        return ref_dir_ent_ds;
    }

    public void setRef_dir_ent_ds(String ref_dir_ent_ds) {
        this.ref_dir_ent_ds = ref_dir_ent_ds;
    }

    public String getCod_tip_cal_cd() {
        return cod_tip_cal_cd;
    }

    public void setCod_tip_cal_cd(String cod_tip_cal_cd) {
        this.cod_tip_cal_cd = cod_tip_cal_cd;
    }

    public String getDes_tip_cal_cd() {
        return des_tip_cal_cd;
    }

    public void setDes_tip_cal_cd(String des_tip_cal_cd) {
        this.des_tip_cal_cd = des_tip_cal_cd;
    }

    public String getTip_mod_equ_cd() {
        return tip_mod_equ_cd;
    }

    public void setTip_mod_equ_cd(String tip_mod_equ_cd) {
        this.tip_mod_equ_cd = tip_mod_equ_cd;
    }

    public String getNum_doc_ven_cd() {
        return num_doc_ven_cd;
    }

    public void setNum_doc_ven_cd(String num_doc_ven_cd) {
        this.num_doc_ven_cd = num_doc_ven_cd;
    }

    public String getNum_tel_ven_ds() {
        return num_tel_ven_ds;
    }

    public void setNum_tel_ven_ds(String num_tel_ven_ds) {
        this.num_tel_ven_ds = num_tel_ven_ds;
    }

    public String getInd_env_con_cd() {
        return ind_env_con_cd;
    }

    public void setInd_env_con_cd(String ind_env_con_cd) {
        this.ind_env_con_cd = ind_env_con_cd;
    }

    public String getInd_id_gra_cd() {
        return ind_id_gra_cd;
    }

    public void setInd_id_gra_cd(String ind_id_gra_cd) {
        this.ind_id_gra_cd = ind_id_gra_cd;
    }

    public String getMod_ace_ven_cd() {
        return mod_ace_ven_cd;
    }

    public void setMod_ace_ven_cd(String mod_ace_ven_cd) {
        this.mod_ace_ven_cd = mod_ace_ven_cd;
    }

    public String getDet_can_ven_ds() {
        return det_can_ven_ds;
    }

    public void setDet_can_ven_ds(String det_can_ven_ds) {
        this.det_can_ven_ds = det_can_ven_ds;
    }

    public String getCod_reg_cd() {
        return cod_reg_cd;
    }

    public void setCod_reg_cd(String cod_reg_cd) {
        this.cod_reg_cd = cod_reg_cd;
    }

    public String getCod_cip_cd() {
        return cod_cip_cd;
    }

    public void setCod_cip_cd(String cod_cip_cd) {
        this.cod_cip_cd = cod_cip_cd;
    }

    public String getCod_exp_cd() {
        return cod_exp_cd;
    }

    public void setCod_exp_cd(String cod_exp_cd) {
        this.cod_exp_cd = cod_exp_cd;
    }

    public String getCod_zon_ven_cd() {
        return cod_zon_ven_cd;
    }

    public void setCod_zon_ven_cd(String cod_zon_ven_cd) {
        this.cod_zon_ven_cd = cod_zon_ven_cd;
    }

    public String getInd_web_par_cd() {
        return ind_web_par_cd;
    }

    public void setInd_web_par_cd(String ind_web_par_cd) {
        this.ind_web_par_cd = ind_web_par_cd;
    }

    public String getCod_stc() {
        return cod_stc;
    }

    public void setCod_stc(String cod_stc) {
        this.cod_stc = cod_stc;
    }

    public String getDes_nac_ds() {
        return des_nac_ds;
    }

    public void setDes_nac_ds(String des_nac_ds) {
        this.des_nac_ds = des_nac_ds;
    }

    public String getCod_mod_ven_cd() {
        return cod_mod_ven_cd;
    }

    public void setCod_mod_ven_cd(String cod_mod_ven_cd) {
        this.cod_mod_ven_cd = cod_mod_ven_cd;
    }

    public String getCan_equ_ven() {
        return can_equ_ven;
    }

    public void setCan_equ_ven(String can_equ_ven) {
        this.can_equ_ven = can_equ_ven;
    }
}
