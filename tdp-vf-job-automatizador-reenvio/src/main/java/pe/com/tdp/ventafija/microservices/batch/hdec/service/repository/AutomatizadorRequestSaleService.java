package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.automatizador.service.AutomatizadorSaleService;

@Repository
public interface AutomatizadorRequestSaleService extends JpaRepository<AutomatizadorSaleService, String> {

    AutomatizadorSaleService findByCodvtappcd(String codvtappcd);

}