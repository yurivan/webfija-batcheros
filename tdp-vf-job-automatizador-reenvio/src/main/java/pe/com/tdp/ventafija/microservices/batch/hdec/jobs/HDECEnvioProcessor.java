package pe.com.tdp.ventafija.microservices.batch.hdec.jobs;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import pe.com.tdp.ventafija.microservices.batch.automatizador.bean.VentaAutomatizadorBean;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.AutomatizadorRequestSaleService;
import pe.com.tdp.ventafija.microservices.batch.automatizador.service.AutomatizadorSaleService;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.LogAutomatizadorService;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.automatizadorsale.AutomatizadorSaleClient;
import pe.com.tdp.ventafija.microservices.common.clients.automatizadorsale.AutomatizadorSaleRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.automatizadorsale.AutomatizadorSaleResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextImpl;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.util.Constants;

/**
 * Implementacion Processor de Spring Batch para la contingencia HDEC.
 * Se evaluan todas las ventas que se encuentran en estado 'ENVIANDO' y que se encuentren en dicho estado
 * un tiempo considerado excesivo (configurado en base datos).
 * Cada venta es enviada nuevamente a HDEC, se manejan los siguientes escenarios:
 * <ul>
 * <li> Si es que la respuesta es SATISFACTORIA o DUPLICADO entonces la venta es actualizada a estado PENDIENTE.
 * <li> Si es que la respuesta es un ERROR entonces se volvera a intentar el envio controlado por un limite de reintentos.
 * <li> Si es que la respuesta es un ERROR y ya se llego al limite de reintentos entonces se actualiza la venta con estado 'CAIDA'.
 * </ul>
 *
 * @author glazaror
 * @since 1.5
 */
public class HDECEnvioProcessor implements ItemProcessor<VentaAutomatizadorBean, VentaAutomatizadorBean> {

    private static final Logger logger = LogManager.getLogger(HDECEnvioProcessor.class);

    @Autowired
    private LogAutomatizadorService logAutomatizadorService;

    @Autowired
    private AutomatizadorRequestSaleService automatizadorRequestSaleService;

    @Autowired
    private ApiHeaderConfig apiHeaderConfig;

    @Autowired
    private ServiceCallEventsService serviceCallEventsService;

    @Value("${tdp.reintentos.max.automatizador_batch}")
    private Integer maxLimitReintentos;

    @Value("${tdp.automatizador.errBusParametrizable}")
    private String errBusParametrizable;

    @Override
    public VentaAutomatizadorBean process(final VentaAutomatizadorBean ventaAutomatizador) throws Exception {
        LogSystem.info(logger, "AutomatizadorProcessor", "process", "Start");
        LogSystem.infoObject(logger, "AutomatizadorProcessor", "process", "VentaAutomatizador", ventaAutomatizador);

        try {
            startCallContext(ventaAutomatizador);

            AutomatizadorSaleRequestBody request = new AutomatizadorSaleRequestBody();


            AutomatizadorSaleService finded  = automatizadorRequestSaleService.findByCodvtappcd(ventaAutomatizador.getId());



            if(finded != null){
                int numIntentos =  ventaAutomatizador.getAuto_retry_batch() !=null ? ventaAutomatizador.getAuto_retry_batch(): 0;
                ventaAutomatizador.setAuto_retry_batch(numIntentos+1);


                AutomatizadorSaleRequestBody automatizadorSaleService = poblarRequest(finded);
                ApiResponse<AutomatizadorSaleResponseBody> response = registerSale(automatizadorSaleService);
                System.out.println(request);


                if (response != null && response.getHeaderOut() != null && response.getHeaderOut().getMsgType() != null &&
                        response.getHeaderOut().getMsgType().equalsIgnoreCase("RESPONSE")) {
                    ventaAutomatizador.setEstado("PENDIENTE");
                }




                if (response != null && response.getHeaderOut() != null && response.getHeaderOut().getMsgType() != null &&
                        response.getHeaderOut().getMsgType().equalsIgnoreCase("ERROR")) {




                        if(response.getBodyOut() !=null){
                            if(response.getBodyOut().getClientException() != null){

                                String[] erroresBuss = errBusParametrizable.split(",");

                                try{
                                    if(response.getBodyOut().getClientException().getAppDetail().getExceptionAppCode().equalsIgnoreCase("ERR-0004")){
                                        //addressDao._registerDuplicado(orderId);
                                        //ventaAutomatizador.setEstado("PENDIENTE");
                                        ventaAutomatizador.setAuto_retry_batch(100);
                                    }else if(response.getBodyOut().getServerException() != null) {
                                        for (String i : erroresBuss) {
                                            int res = Integer.parseInt(i);
                                            if (response.getBodyOut().getServerException().getExceptionCode() == res) {
                                                if(maxLimitReintentos <= numIntentos){
                                                    ventaAutomatizador.setEstado("ENVIANDO");
                                                }
                                            }

                                        }
                                    }else{
                                        ventaAutomatizador.setEstado("ENVIANDO");
                                    }

                                }catch (Exception e){
                                    ventaAutomatizador.setEstado("ENVIANDO");

                                }

                            }
                        }

                    if(maxLimitReintentos <= numIntentos){
                        ventaAutomatizador.setEstado("ENVIANDO");
                    }


                }

            }else{
                ventaAutomatizador.setEstado("ENVIANDO");
            }







        } catch (Exception ex) {
            LogSystem.error(logger, "AutomatizadorProcessor", "process", ex.toString(),ex);
        }
        endCallContext();
        LogSystem.infoObject(logger, "AutomatizadorProcessor", "process", "VentaAutomatizador", ventaAutomatizador);
        LogSystem.info(logger, "AutomatizadorProcessor", "process", "End");
        return ventaAutomatizador;
    }

    private AutomatizadorSaleRequestBody poblarRequest(AutomatizadorSaleService request) {
        AutomatizadorSaleRequestBody automatizadorSaleRequestBody = new AutomatizadorSaleRequestBody();

        automatizadorSaleRequestBody.setClienteNombre(request.getNom_ds());
        automatizadorSaleRequestBody.setClienteApellidoPaterno(request.getPri_ape_ds());
        automatizadorSaleRequestBody.setClienteApellidoMaterno(request.getSeg_ape_ds());
        automatizadorSaleRequestBody.setClienteNumeroDoc(request.getNum_doc_cli_cd());
        automatizadorSaleRequestBody.setClienteRucDigitoControl(request.getDig_ctl_doc_cd());
        automatizadorSaleRequestBody.setClienteTipoDoc(request.getTip_doc_cd());
        automatizadorSaleRequestBody.setClienteTelefono1(request.getTel_pri_cot_cd());
        automatizadorSaleRequestBody.setClienteTelefono2(request.getTel_seg_cot_cd());
        automatizadorSaleRequestBody.setClienteRucActividadCodigo(request.getCod_act_eco_cd());
        automatizadorSaleRequestBody.setClienteRucSubActividadCodigo(request.getCod_sac_eco_cd());
        automatizadorSaleRequestBody.setAddressCalleAtis(request.getTip_cal_ati_cd());
        automatizadorSaleRequestBody.setAddressCalleNombre(request.getNom_cal_ds());
        automatizadorSaleRequestBody.setAddressCalleNumero(request.getNum_cal_nu());
        automatizadorSaleRequestBody.setAddressCCHHCodigo(request.getCod_cnj_hbl_cd());
        automatizadorSaleRequestBody.setAddressCCHHTipo(request.getTip_cnj_hbl_cd());
        automatizadorSaleRequestBody.setAddressCCHHNombre(request.getNom_cnj_hbl_no());
        automatizadorSaleRequestBody.setAddressCCHHCompTipo1(request.getPri_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressCCHHCompNombre1(request.getPri_cmp_dir_ds());
        automatizadorSaleRequestBody.setAddressCCHHCompTipo2(request.getSeg_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressCCHHCompNombre2(request.getSeg_cmp_dir_ds());
        automatizadorSaleRequestBody.setAddressCCHHCompTipo3(request.getTrc_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressCCHHCompNombre3(request.getTrc_cmp_dir_ds());
        automatizadorSaleRequestBody.setAddressCCHHCompTipo4(request.getCao_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressCCHHCompNombre4(request.getCao_cmp_dir_ds());
        automatizadorSaleRequestBody.setAddressCCHHCompTipo5(request.getQui_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressCCHHCompNombre5(request.getQui_cmp_dir_ds());
        automatizadorSaleRequestBody.setAddressViaCompTipo1(request.getSet_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressViaCompNombre1(request.getSet_cmp_dir_ds());
        automatizadorSaleRequestBody.setAddressViaCompTipo2(request.getSpt_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressViaCompNombre2(request.getSpt_cmp_dir_ds());
        automatizadorSaleRequestBody.setAddressViaCompTipo3(request.getOct_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressViaCompNombre3(request.getOct_cmp_dir_ds());
        automatizadorSaleRequestBody.setAddressViaCompTipo4(request.getNov_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressViaCompNombre4(request.getNov_cmp_dir_ds());
        automatizadorSaleRequestBody.setAddressViaCompTipo5(request.getDco_tip_cmp_cd());
        automatizadorSaleRequestBody.setAddressViaCompNombre5(request.getDco_cmp_dir_ds());
        automatizadorSaleRequestBody.setOrderGisXDSL(request.getMsx_cbr_voi_ges_in());
        automatizadorSaleRequestBody.setOrderGisHFC(request.getMsx_cbr_voi_gis_in());
        automatizadorSaleRequestBody.setOrderGisTipoSenial(request.getMsx_ind_snl_gis_cd());
        automatizadorSaleRequestBody.setOrderGisGPON(request.getMsx_ind_gpo_gis_cd());
        automatizadorSaleRequestBody.setUserAtis(request.getCod_fza_ven_cd());
        automatizadorSaleRequestBody.setUserFuerzaVenta(request.getCod_fza_gen_cd());
        automatizadorSaleRequestBody.setUserCanalCodigo(request.getCod_cnl_ven_cd());
        automatizadorSaleRequestBody.setProductoSenial(request.getCod_ind_sen_cms());
        automatizadorSaleRequestBody.setProductoCabecera(request.getCod_cab_cms());
        automatizadorSaleRequestBody.setProductoCodigo(request.getId_cod_pro_cd());
        automatizadorSaleRequestBody.setProductoPSAdmin1(request.getPs_adm_dep_1());
        automatizadorSaleRequestBody.setProductoPSAdmin2(request.getPs_adm_dep_2());
        automatizadorSaleRequestBody.setProductoPSAdmin3(request.getPs_adm_dep_3());
        automatizadorSaleRequestBody.setProductoPSAdmin4(request.getPs_adm_dep_4());
        automatizadorSaleRequestBody.setProductoSVACodigo1(request.getId_cod_sva_cd_1());
        automatizadorSaleRequestBody.setProductoSVACodigo2(request.getId_cod_sva_cd_2());
        automatizadorSaleRequestBody.setProductoSVACodigo3(request.getId_cod_sva_cd_3());
        automatizadorSaleRequestBody.setProductoSVACodigo4(request.getId_cod_sva_cd_4());
        automatizadorSaleRequestBody.setProductoSVACodigo5(request.getId_cod_sva_cd_5());
        automatizadorSaleRequestBody.setProductoSVACodigo6(request.getId_cod_sva_cd_6());
        automatizadorSaleRequestBody.setProductoSVACodigo7(request.getId_cod_sva_cd_7());
        automatizadorSaleRequestBody.setProductoSVACodigo8(request.getId_cod_sva_cd_8());
        automatizadorSaleRequestBody.setProductoSVACodigo9(request.getId_cod_sva_cd_9());
        automatizadorSaleRequestBody.setProductoSVACodigo10(request.getId_cod_sva_cd_10());
        automatizadorSaleRequestBody.setOrderId(request.getCodvtappcd());
        automatizadorSaleRequestBody.setOrderFecha(request.getFec_vta_ff());
        automatizadorSaleRequestBody.setOrderParqueTelefono(request.getNum_ide_tlf());
        automatizadorSaleRequestBody.setClienteCMSCodigo(request.getCod_cli_ext_cd());
        automatizadorSaleRequestBody.setOrderOperacionComercial(request.getTip_trx_cmr_cd());
        automatizadorSaleRequestBody.setClienteCMSServicio(request.getCod_srv_cms_cd());
        automatizadorSaleRequestBody.setClienteNacimiento(request.getFec_nac_ff());
        automatizadorSaleRequestBody.setClienteSexo(request.getCod_sex_cd());
        automatizadorSaleRequestBody.setClienteCivil(request.getCod_est_civ_cd());
        automatizadorSaleRequestBody.setClienteEmail(request.getDes_mai_cli_ds());
        automatizadorSaleRequestBody.setClienteTelefono1Area(request.getCod_are_te1_cd());
        automatizadorSaleRequestBody.setClienteTelefono2Area(request.getCod_are_te2_cd());
        automatizadorSaleRequestBody.setClienteCMSFactura(request.getCod_cic_fac_cd());
        automatizadorSaleRequestBody.setAddressDepartamentoCodigo(request.getCod_dep_cd());
        automatizadorSaleRequestBody.setAddressProvinciaCodigo(request.getCod_prv_cd());
        automatizadorSaleRequestBody.setAddressDistritoCodigo(request.getCod_dis_cd());
        automatizadorSaleRequestBody.setOrderGisNTLT(request.getCod_fac_tec_cd());
        automatizadorSaleRequestBody.setOrderGPSX(request.getNum_cod_x_cd());
        automatizadorSaleRequestBody.setOrderGPSY(request.getNum_cod_y_cd());
        automatizadorSaleRequestBody.setUserCSMPuntoVenta(request.getCod_ven_cms_cd());
        automatizadorSaleRequestBody.setUserCMS(request.getCod_sve_cms_cd());
        automatizadorSaleRequestBody.setUserNombre(request.getNom_ven_ds());
        automatizadorSaleRequestBody.setAddressReferencia(request.getRef_dir_ent_ds());
        automatizadorSaleRequestBody.setOrderCallTipo(request.getCod_tip_cal_cd());
        automatizadorSaleRequestBody.setOrderCallDesc(request.getDes_tip_cal_cd());
        automatizadorSaleRequestBody.setOrderModelo(request.getTip_mod_equ_cd());
        automatizadorSaleRequestBody.setUserDNI(request.getNum_doc_ven_cd());
        automatizadorSaleRequestBody.setUserTelefono(request.getNum_tel_ven_ds());
        automatizadorSaleRequestBody.setOrderContrato(request.getInd_env_con_cd());
        automatizadorSaleRequestBody.setOrderIDGrabacion(request.getInd_id_gra_cd());
        automatizadorSaleRequestBody.setOrderModalidadAcep(request.getMod_ace_ven_cd());
        automatizadorSaleRequestBody.setOrderEntidad(request.getDet_can_ven_ds());
        automatizadorSaleRequestBody.setOrderRegion(request.getCod_reg_cd());
        automatizadorSaleRequestBody.setOrderCIP(request.getCod_cip_cd());
        automatizadorSaleRequestBody.setOrderExperto(request.getCod_exp_cd());
        automatizadorSaleRequestBody.setOrderZonal(request.getCod_zon_ven_cd());
        automatizadorSaleRequestBody.setOrderWebParental(request.getInd_web_par_cd());
        automatizadorSaleRequestBody.setOrderCodigoSTC6I(request.getCod_stc());
        automatizadorSaleRequestBody.setClienteNacionalidad(request.getDes_nac_ds());
        automatizadorSaleRequestBody.setOrderModoVenta(request.getCod_mod_ven_cd());
        automatizadorSaleRequestBody.setOrderCantidadEquipos(request.getCan_equ_ven());


        return automatizadorSaleRequestBody;
    }

    private ApiResponse<AutomatizadorSaleResponseBody> registerSale(AutomatizadorSaleRequestBody apiRequestBody) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getAutomatizadorRegisterSaleUri())
                .setApiId(apiHeaderConfig.getAutomatizadorRegisterSaleApiId())
                .setApiSecret(apiHeaderConfig.getAutomatizadorRegisterSaleApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_AUTOMATIZADOR_REGISTER)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_AUTOMATIZADOR)
                .build();

        AutomatizadorSaleClient automatizadorRegisterSaleClient = new AutomatizadorSaleClient(config);
        ClientResult<ApiResponse<AutomatizadorSaleResponseBody>> result = automatizadorRegisterSaleClient.post(apiRequestBody);
        result.getEvent().setOrderId(apiRequestBody.getOrderId());

        result.getEvent().setDocNumber(apiRequestBody.getClienteNumeroDoc());
        result.getEvent().setUsername(apiRequestBody.getUserAtis());

        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

    public void startCallContext(VentaAutomatizadorBean ventaAutomatizador) {
        ServiceCallEvent event = new ServiceCallEvent();
        event.setOrderId(ventaAutomatizador.getId());
        event.setServiceCode("AUTOMATIZADOR-REINTENTO");
        event.setSourceApp("BATCH");
        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);
    }

    private void endCallContext() {
        VentaFijaContextHolder.clearContext();
    }


}