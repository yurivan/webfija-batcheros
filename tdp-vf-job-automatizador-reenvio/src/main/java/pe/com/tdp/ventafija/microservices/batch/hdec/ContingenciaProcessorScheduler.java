package pe.com.tdp.ventafija.microservices.batch.hdec;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import java.util.Calendar;
import java.util.Date;

/**
 * Scheduler de la contingencia HDEC. El cron es configurable en base datos y no soporta cambios en caliente.
 *
 * @author      glazaror
 * @since       1.5
 */
@Component
public class ContingenciaProcessorScheduler {

    private static final Logger logger = LogManager.getLogger(ContingenciaProcessorScheduler.class);

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job hdecContingenciaProcessor;

   // @Scheduled(cron = "*0 * * * * *")
    @Scheduled(cron = "${tdp.automatizador.batch.reintentos.sync.cron}")
    public void startJob() {

        try {
            LogSystem.info(logger, "ContingenciaProcessorScheduler", "startJob", "Starting hdec contingencia sync..!");

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DAY_OF_MONTH, 1);
            c.add(Calendar.MONTH, 11);
            c.add(Calendar.HOUR_OF_DAY, 1);
            Date job = c.getTime();

            JobParameters params = new JobParametersBuilder().addLong("time", job.getTime()).toJobParameters();
            JobExecution execution = jobLauncher.run(hdecContingenciaProcessor, params);
            LogSystem.info(logger, "ContingenciaProcessorScheduler", "startJob",
                    String.format("Contingencia batch excecution started at %s and finished at %s wit status %s", execution.getStartTime(), execution.getEndTime(), execution.getExitStatus()));

        } catch (Exception e) {
            logger.error("Error startJob. ", e);
        }
    }
}
