package pe.com.tdp.ventafija.microservices.batch.config;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import pe.com.tdp.ventafija.microservices.batch.automatizador.bean.VentaAutomatizadorBean;
import pe.com.tdp.ventafija.microservices.batch.automatizador.bean.mapper.VentaAutoRowMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.ContingenciaProcessorScheduler;
//import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaAutomatizador;
//import pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper.VentaRowMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.AzureRequestProcessorSkipPolicy;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.HDECEnvioProcessor;
import pe.com.tdp.ventafija.microservices.batch.hdec.jobs.HDECEnvioProcessorListener;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.LogAutomatizadorService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import javax.sql.DataSource;
import java.util.Date;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    private static final Logger logger = LogManager.getLogger(BatchConfig.class);

    @Autowired
    private DataSource dataSource;
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;


    @Value("${tdp.maximo.cantidad.automatizador_batch}")
    private Integer maxLimit;

    @Value("${tdp.reintentos.max.automatizador_batch}")
    private Integer maxLimitReintentos;


    @Bean
    public PlatformTransactionManager transactionManager() {
        return new ResourcelessTransactionManager();
    }

    @Bean
    public JobRepository jobRepository() throws Exception {
        return new MapJobRepositoryFactoryBean(transactionManager()).getObject();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        SyncTaskExecutor executor = new SyncTaskExecutor();
        return executor;
    }

    @Bean
    public SimpleJobLauncher jobLauncher() throws Exception {
        SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        simpleJobLauncher.setJobRepository(jobRepository());
        simpleJobLauncher.setTaskExecutor(taskExecutor());
        return simpleJobLauncher;
    }

    @Bean
    public ContingenciaProcessorScheduler contingenciaProcessorScheduler() {
        return new ContingenciaProcessorScheduler();
    }

    @Bean
    public JdbcCursorItemReader<VentaAutomatizadorBean> ventasReader() {

        String sql = " SELECT * FROM (SELECT VIS.ID_VISOR, VIS.auto_retry_batch, VIS.FECHA_GRABACION, VIS.ESTADO_SOLICITUD," +
                " VIS.DNI, ORD.USERID AS COD_ATIS FROM ibmx_a07e6d02edaf552.TDP_VISOR AS VIS" +
                " INNER JOIN ibmx_a07e6d02edaf552.ORDER AS ORD ON ORD.ID = VIS.ID_VISOR " +
                "inner join ibmx_a07e6d02edaf552.tdp_order as o ON o.order_id = VIS.ID_VISOR " +
                "WHERE VIS.ESTADO_SOLICITUD = 'ENVIANDO_AUTOMATIZADOR' AND" +
                " (VIS.auto_retry_batch < '"+maxLimitReintentos+"' or VIS.auto_retry_batch is null) " +
                "ORDER BY VIS.FECHA_GRABACION ASC LIMIT '"+maxLimit+"') ALS LIMIT '"+maxLimit+"'";


        LogSystem.infoQuery(logger, "BatchConfig", "ventasReader", "sql", sql);

        JdbcCursorItemReader<VentaAutomatizadorBean> reader = new JdbcCursorItemReader<VentaAutomatizadorBean>();
        reader.setDataSource(dataSource);
        reader.setSql(sql);
        reader.setRowMapper(new VentaAutoRowMapper());
        reader.setFetchSize(1);
        return reader;
    }



    @Bean
    public JdbcBatchItemWriter<VentaAutomatizadorBean> ventasWriter() {
        JdbcBatchItemWriter<VentaAutomatizadorBean> writer = new JdbcBatchItemWriter<VentaAutomatizadorBean>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<VentaAutomatizadorBean>());
        // Actualizamos el estado de la venta si es que la venta sigue en estado 'ENVIANDO'
        writer.setSql("UPDATE ibmx_a07e6d02edaf552.TDP_VISOR" +
                " set estado_solicitud = :estado," +
                " auto_retry_batch =:auto_retry_batch " +
                "where id_visor = :id " +
                "AND estado_solicitud = 'ENVIANDO_AUTOMATIZADOR'");
        writer.setDataSource(dataSource);
        return writer;
    }

    @Bean
    public HDECEnvioProcessor ventasProcessor() {
        return new HDECEnvioProcessor();
    }

    @Bean
    public Job hdecContingenciaProcessor(Step contingenciaHDECStep) throws Exception {
        return jobBuilderFactory.get("hdecContingenciaProcessor").incrementer(new RunIdIncrementer())
                .repository(jobRepository()).flow(contingenciaHDECStep).end().build();
    }

    @Bean
    public SkipPolicy hdecEnvioProcessorSkipPolicy() {
        return new AzureRequestProcessorSkipPolicy();
    }

    @Bean
    public ItemProcessListener<VentaAutomatizadorBean, VentaAutomatizadorBean> hdecEnvioProcessorListener() {
        return (ItemProcessListener<VentaAutomatizadorBean, VentaAutomatizadorBean>) new HDECEnvioProcessorListener();
    }

    @Bean
    public Step contingenciaHDECStep(HDECEnvioProcessor processor) {
        return stepBuilderFactory.get("contingenciaHDECStep")
                .<VentaAutomatizadorBean, VentaAutomatizadorBean>chunk(0)
                .reader(ventasReader())
                .processor(ventasProcessor())
                .faultTolerant()
                .skipPolicy(hdecEnvioProcessorSkipPolicy())
                .listener(hdecEnvioProcessorListener())
                .writer(ventasWriter())
                .build();
    }
}