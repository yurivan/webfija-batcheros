package pe.com.tdp.ventafija.microservices.batch.automatizador.bean.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.com.tdp.ventafija.microservices.batch.automatizador.bean.VentaAutomatizadorBean;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper del Bean Venta.
 *
 * @author VALDEMAR
 * @since 1.5
 */
public class VentaAutoRowMapper implements RowMapper<VentaAutomatizadorBean> {

    @Override
    public VentaAutomatizadorBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        VentaAutomatizadorBean ventaAutomatizador = new VentaAutomatizadorBean();

        ventaAutomatizador.setId(rs.getString("ID_VISOR"));
        ventaAutomatizador.setEstado(rs.getString("ESTADO_SOLICITUD"));
        ventaAutomatizador.setFechaGrabacion(rs.getDate("FECHA_GRABACION"));
        ventaAutomatizador.setDniCliente(rs.getString("DNI"));
        ventaAutomatizador.setCodigoVendedor(rs.getString("COD_ATIS"));
        ventaAutomatizador.setAuto_retry_batch(rs.getInt("auto_retry_batch"));

        return ventaAutomatizador;
    }

}
