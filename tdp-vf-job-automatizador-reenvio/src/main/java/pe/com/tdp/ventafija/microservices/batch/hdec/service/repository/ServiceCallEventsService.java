package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

@Service
public class ServiceCallEventsService {
	ServiceCallEventsDao dao;
	
	@Autowired
	public ServiceCallEventsService(ServiceCallEventsDao dao) {
		this.dao = dao;
	}

	public void registerEvent (ServiceCallEvent event) {
		this.dao.registerEvent(event);
	}
}
