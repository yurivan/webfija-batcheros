package pe.com.telefonica.everis.common.azure;

import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;

public class AzureApi {
	private Logger logger = LoggerFactory.getLogger(AzureApi.class);

	/**
	 * Uploads an audio to azure
     *
	 * @param f file to be uploaded
	 * @param audioFilename file name
	 * @param destination connection string to azure
	 * @param dateString date for metadata
	 * @throws AzureApiException when an error occurs
	 */
    public void upload(String orderId, File f, String audioFilename, String destination, String dateString) throws AzureApiException {
        logger.info("::" + orderId + ":: Upload => Inicio");
        logger.info("::" + orderId + ":: Upload => audioFilename: " + audioFilename);

        try (FileInputStream is = new FileInputStream(f)) {
			long imagenFileSize = f.length();
            CloudBlobContainer container = createBlobContainer(orderId, destination, dateString);
			CloudBlob azureAudioLegacy = container.getBlockBlobReference(audioFilename);
			azureAudioLegacy.upload(is, imagenFileSize);
		} catch (Exception e) {
            logger.error("::" + orderId + ":: Upload => Error de carga de archivo. " + e.getMessage(), e);
            logger.info("::" + orderId + ":: Upload => Fin");
			throw new AzureApiException("Error de carga de archivo", e);
		}
        logger.info("::" + orderId + ":: Upload => Fin");
	}
	
    public CloudBlobContainer createBlobContainer(String orderId, String azureConnection, String directoryName) {
        logger.info("::" + orderId + ":: CreateBlobContainer => Inicio");
		CloudStorageAccount account;
		try {
			account = CloudStorageAccount.parse(azureConnection);
            logger.info("::" + orderId + ":: CreateBlobContainer => account: Obtención de Cuenta Azure");
		} catch (Exception e) {
            logger.error("::" + orderId + ":: CreateBlobContainer => Error en Cuenta. " + e.getMessage(), e);
            logger.info("::" + orderId + ":: CreateBlobContainer => Fin");
			return null;
		}
		CloudBlobClient client = account.createCloudBlobClient();

		CloudBlobContainer container;
		try {
			container = client.getContainerReference(directoryName);
            logger.info("::" + orderId + ":: CreateBlobContainer => container: Obtención de Container");
		} catch (Exception e) {
            logger.info("::" + orderId + ":: CreateBlobContainer => Fin");
            logger.error("::" + orderId + ":: CreateBlobContainer => Error Compartido. " + e.getMessage(), e);
			return null;
		}

		try {
			if (container.createIfNotExists()) {
                logger.info("::" + orderId + ":: CreateBlobContainer => container: New container created");
			}
		} catch (Exception e) {
            logger.error("::" + orderId + ":: CreateBlobContainer => Error crear Compartido. " + e.getMessage(), e);
		}

        logger.info("::" + orderId + ":: CreateBlobContainer => Fin");
		return container;
	}
}
