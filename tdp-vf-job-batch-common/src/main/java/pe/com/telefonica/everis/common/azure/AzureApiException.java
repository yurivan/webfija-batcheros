package pe.com.telefonica.everis.common.azure;

public class AzureApiException extends Exception {
	private static final long serialVersionUID = 1L;

	public AzureApiException(String message, Throwable cause) {
		super(message, cause);
	}

}
