package pe.com.tdp.ventafija.microservices.service.thirdparty;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.jcraft.jsch.*;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSelector;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemOptions;
import org.apache.commons.vfs2.Selectors;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.OutputStream;

@Service
public class FtpService {
	private static final Logger logger = LogManager.getLogger(FtpService.class);

	@Value("${ftp.api.connection.hostname}")
	private String hostName;
	@Value("${ftp.api.connection.username}")
	private String username;
	@Value("${ftp.api.connection.password}")
	private String password;
	@Value("${ftp.api.connection.proxy}")
	private String proxy;

	private static String proxyStatic;

	// mcg
	private Session session;
	private Channel channel;
	private ChannelSftp c;


	private static String createConnectionString(String hostName, String username, String password,
												 String remoteFilePath) {
		return "sftp://" + username + ":" + password + "@" + hostName + "/" + remoteFilePath;
	}

	public static FileSystemOptions createDefaultOptions() throws FileSystemException {
		FileSystemOptions opts = new FileSystemOptions();
		SftpFileSystemConfigBuilder.getInstance().setStrictHostKeyChecking(opts, "no");
		SftpFileSystemConfigBuilder.getInstance().setUserDirIsRoot(opts, true);
		SftpFileSystemConfigBuilder.getInstance().setTimeout(opts, 10000);
		return opts;
	}

	/**
	 * Lists the files with in a folder
	 * @param dir to list from
	 * @return a list of strings with the names of the files found
	 */
	public List<String> listFiles(String dir, boolean isDevueltas) {
//		logger.info("listando archivos");
//		StandardFileSystemManager manager = new StandardFileSystemManager();
//		List<String> files = new ArrayList<>();
//		try {
//			manager.init();
//			FileObject localFileObject = manager.resolveFile(createConnectionString(hostName, username, password, dir), createDefaultOptions());
////			FileSelector fs = new FileSe
////			localFileObject.findFiles()
//			FileObject[] children = localFileObject.getChildren();
//			for (int i = 0; i < children.length; i++) {
//				FileObject f = children[i];
////				if (f.isFile()) {
//					files.add(f.getName().getBaseName());
////				}
//			}
// 		} catch (Exception e) {
//			logger.error("error", e);
//		} finally {
//			manager.close();
//		}
//		logger.info("finalizado listando archivos");
//		return files;

		logger.info("listFiles...");
		List<String> files = new ArrayList<>();
		java.util.Date ahora = new java.util.Date();
		try {
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch=new JSch();
//			String username = "usrexteveris";
//			String host = "200.60.223.152";
//			String password = "everis2017.";

			logger.info("obteniendo sesion sftp...");
			int port = 22;
			//int port = 8801;
			//int port = 8563;
			session=jsch.getSession(username, hostName, port);
			session.setPassword(password);
			session.setConfig(config);

			///////// javier
			logger.info("username:" + username);
			logger.info("hostName:" + hostName);
			logger.info("password:" + password );
			logger.info("port:" + port);

			proxyStatic = proxy;

			Proxy proxy = getSockProxy();
			if (proxy ==null){
				logger.error("REPORT: Statica proxyError");
				return null;
			}
			session.setProxy(proxy);
			////// javier

			session.connect();

			channel=session.openChannel("sftp");
			channel.connect();
			logger.info("Se conecto al sftp");

			c=(ChannelSftp)channel;

			String initialPath = c.pwd();

			c.cd("."+dir);
			//c.cd(dir);

			logger.info("listando archivos de ."+dir);
			Vector<ChannelSftp.LsEntry> list = c.ls(".");

			for(ChannelSftp.LsEntry entry : list) {
				logger.info(entry.getFilename());
				files.add(entry.getFilename());
			}

			logger.info("se termino el listar archivos de ."+dir);
			//c.cd("..");
			//logger.info("se hizo cd ..");

			//if(isDevueltas){
			//c.cd("..");
			//logger.info("se hizo cd ..");
			//}

			c.cd(initialPath);

			logger.info("directorio actual: "+c.pwd());

		} catch (Exception e) {
			logger.error("Error de listado de archivos :/ --> "+ e.getMessage());
		}
		logger.info("finalizado listando archivos desde "+ahora+" hasta "+new java.util.Date());

		return files;
	}

	public void renameFile (String dir, String fileName, String newFileName) {
		logger.info("cambiando nombre de archivo");
		StandardFileSystemManager manager = new StandardFileSystemManager();
		try {
			manager.init();
			String path = dir + fileName;
			String newPath = dir + newFileName;
			FileObject fileObject = manager.resolveFile(createConnectionString(hostName, username, password, path), createDefaultOptions());
			FileObject newFileObject = manager.resolveFile(createConnectionString(hostName, username, password, newPath), createDefaultOptions());
			if (fileObject.exists() && fileObject.canRenameTo(newFileObject)) {
				fileObject.moveTo(newFileObject);
			}
		} catch (Exception e) {
			logger.error("error", e);
		} finally {
			manager.close();
		}
		logger.info("finalizado cambiando nombre de archivo");
	}

	public void moveFile (String srcDir, String fileName, String destDir, String newFileName) {
		logger.info("cambiando nombre de archivo");

		/////////////////// anterior everis

//		StandardFileSystemManager manager = new StandardFileSystemManager();
//		try {
//			manager.init();
//			String path = srcDir + fileName;
//			String newPath = destDir + newFileName;
//			FileObject fileObject = manager.resolveFile(createConnectionString(hostName, username, password, path), createDefaultOptions());
//			FileObject newFileObject = manager.resolveFile(createConnectionString(hostName, username, password, newPath), createDefaultOptions());
//			if (fileObject.exists() && fileObject.canRenameTo(newFileObject)) {
//				fileObject.moveTo(newFileObject);
//			}
// 		} catch (Exception e) {
//			logger.error("error", e);
//		} finally {
//			manager.close();
//		}

		// mcg
		logger.info("ruta archivo procesado: "+srcDir+fileName);
		logger.info("nuevo nombre y ruta del archivo procesado: "+destDir+newFileName);
		try {
			c.rename("."+srcDir+fileName, "."+destDir+newFileName);
			logger.info("archivo movido y renombrado con exito ..........");
		} catch (Exception e) {
			logger.error("error: "+ e.getMessage());
		}

		logger.info("finalizado cambiando nombre de archivo");
	}

	public File retrieveFile (String remoteFilePath) throws IOException {
		logger.info("obtener archivo");
		File f = File.createTempFile("ftpFile"+new Date().getTime(), ".tmp");

//		StandardFileSystemManager manager = new StandardFileSystemManager();
		String fileURI=f.getAbsolutePath();
		logger.info("archivo temporal.......  "+fileURI);
		// mcg
		OutputStream output = new FileOutputStream(
				fileURI);
		try {
			///// anterior everis
//            manager.init();
//            FileObject localFile = manager.resolveFile(fileURI);
//            FileObject remoteFile = manager.resolveFile(createConnectionString(hostName, username, password, remoteFilePath), createDefaultOptions());
//            localFile.copyFrom(remoteFile, Selectors.SELECT_SELF);

			// mcg
			c.get(remoteFilePath,output);

			logger.info("File download success!");
		} catch (Exception e) {
			logger.info("Error en SFTP.", e);

		} finally {
			output.close();
//            manager.close();
		}
		logger.info("finalizado obtener archivo");
		logger.info("tamaño del archivo: "+ f.length() + " bytes");
		return f;
	}

	public void uploadFile (File f, String remoteFilePath) {
		if (f == null) {
			return;
		}
		StandardFileSystemManager manager = new StandardFileSystemManager();
		try {
			manager.init();
			FileObject localFile = manager.resolveFile(f.getAbsolutePath());
			FileObject remoteFile = manager.resolveFile(createConnectionString(hostName, username, password, remoteFilePath), createDefaultOptions());
			remoteFile.copyFrom(localFile, Selectors.SELECT_SELF);
		} catch (Exception e) {
			logger.info("Error en SFTP.", e);
		} finally {
			manager.close();
		}
	}
	//	// javier
	private static Proxy getSockProxy() {

		logger.info("procesando SOCKS5 ..........");
		ProxySOCKS5 proxyTunnel = null;
		//String staticaUrl = "socks5://statica4359:eb95a2835f1e5907@sl-ams-01-guido.statica.io:1080";

		String staticaUrl = proxyStatic;

		// PropertiesUtil.getPropertyFromFile("sftp.properties", "sftp.socks5");
		//String staticaUrl ="socks5://statica4348:bbba0267e0a9fcbd@sl-ams-01-guido.statica.io:1080";
//		String staticaUrl = "socks5://statica4362:908a26f50e62c490@sl-ams-01-guido.statica.io:1080";
		if (staticaUrl!=null) {
			try {
				// OBTENER LOS DATOS DE HOST, PORT, USER Y PASSWORD
				//URL proxyUrl = new URL(staticaUrl);
				String authString = staticaUrl.substring(9, staticaUrl.indexOf("@"));
				String user = authString.split(":")[0];
				String password = authString.split(":")[1];
				String hostAndPort = staticaUrl.substring(staticaUrl.indexOf("@")+1);
				String host = hostAndPort.split(":")[0];
				int port = Integer.valueOf(hostAndPort.split(":")[1]);
				logger.info("authString: " + authString);
				logger.info("user: " + user);
				logger.info("password: " + password);
				logger.info("hostAndPort: " + hostAndPort );
				logger.info("host: " +host);


				//proxy = new ProxySOCKS5(host, port);
				//proxy.setUserPasswd(user, password);

				proxyTunnel = new ProxySOCKS5(host,port);
				proxyTunnel.setUserPasswd(user, password);
				//session.setProxy(proxyTunnel);

			} catch (Exception e) {
				logger.fatal("REPORT: " + e.getMessage());
				e.printStackTrace();
			}

		} else {
			logger.error("REPORT: Url Statica no Configurada");
		}
		return proxyTunnel;
	}
//	// javier
}
