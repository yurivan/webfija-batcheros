package pe.com.tdp.ventafija.microservices.domain.thirdparty;

import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.tdp.ventafija.microservices.domain.ApiResponseBodyFault;

public class HdecApiHdecResponseBody {
	@JsonProperty("Mensaje")
	private String Mensaje;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;

	public String getMensaje() {
		return Mensaje;
	}

	public void setMensaje(String mensaje) {
		Mensaje = mensaje;
	}

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

}
