package pe.com.tdp.ventafija.microservices.common.util.exception;

public class SyncException extends Exception {
	private static final long serialVersionUID = 1L;

	public SyncException (Exception e) {
		super(e);
	}
}
