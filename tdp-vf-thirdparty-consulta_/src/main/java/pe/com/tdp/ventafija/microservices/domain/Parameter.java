package pe.com.tdp.ventafija.microservices.domain;

public class Parameter {
private String element;
  private String strValue;

  public String getElement() {
    return element;
  }

  public void setElement(String element) {
    this.element = element;
  }

  public String getStrValue() {
    return strValue;
  }

  public void setStrValue(String strValue) {
    this.strValue = strValue;
  }
}
