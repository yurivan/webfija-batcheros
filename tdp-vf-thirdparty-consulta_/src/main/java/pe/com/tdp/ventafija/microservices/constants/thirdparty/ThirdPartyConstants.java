package pe.com.tdp.ventafija.microservices.constants.thirdparty;

public class ThirdPartyConstants {
  public static final String HDEC_RESPONSE_CODE_OK = "0";
  public static final String HDEC_RESPONSE_DATA = "OK";
}
