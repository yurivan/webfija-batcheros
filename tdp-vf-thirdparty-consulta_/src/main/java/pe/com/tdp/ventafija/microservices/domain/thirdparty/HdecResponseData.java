package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class HdecResponseData {
	
	private String id_visor;
	private String back;
	private String fecha_grabacion;
	private String id_grabacion;
	private String cliente;
	private String telefono;
	private String direccion;
	private String departamento;
	private String dni;
	private String nombre_producto;
	private String estado_solicitud;
	private String motivo_estado;
	private String speech;
	private String accion;
	private String operacion_comercial;
	private String distrito;
	private String codigo_llamada;
	private String sub_producto;
	private String tipo_producto;
	private String codigo_getion;
	private String codigo_sub_gestion;
	private String flag_dependencia;
	private String version_fila;
	private String tipo_documento;
	private String accion_aplicativo_back;
	private String provincia;
	private String fecha_de_llamada;
	private String codigo_pedido;
	private String version_fila_t;
	private String sub_producto_equiv;
	private String producto; //31

	//nuevos

	private String producto_ps;
	private String precio_normal_producto;
	private String precio_promo_producto;
	private String meses_promo_producto;
	private String velocidad_promo_inter;
	private String tiempo_promo_inter;
	private String nombre_sva1;
	private String precio_uni_sva1;
	private String cantidad_sva1;
	private String code_sva1;
	private String nombre_sva2;
	private String precio_uni_sva2;
	private String cantidad_sva2;
	private String code_sva2;
	private String nombre_sva3;
	private String precio_uni_sva3;
	private String cantidad_sva3;
	private String code_sva3;
	private String nombre_sva4;
	private String precio_uni_sva4;
	private String cantidad_sva4;
	private String code_sva4;
	private String nombre_sva5;
	private String precio_uni_sva5;
	private String cantidad_sva5;
	private String code_sva5;
	private String nombre_sva6;
	private String precio_uni_sva6;
	private String cantidad_sva6;
	private String code_sva6;
	private String nombre_sva7;
	private String precio_uni_sva7;
	private String cantidad_sva7;
	private String code_sva7;
	private String nombre_sva8;
	private String precio_uni_sva8;
	private String cantidad_sva8;
	private String code_sva8;
	private String nombre_sva9;
	private String precio_uni_sva9;
	private String cantidad_sva9;
	private String code_sva9;
	private String nombre_sva10;
	private String precio_uni_sva10;
	private String cantidad_sva10;
	private String code_sva10;
	private String aceptacion_recibo_digital;
	private String aceptacion_proteccion_datos;
	private String filtro_web_parental;
	private String modalidad_pago;
	private String monto_contado;
	private String meses_devolucion;
	private String email_cliente;
	private String id_transaccion;
	private String costo_instalacion;
	private String equipamiento_tv;
	private String equipamiento_internet;
	private String equipamiento_linea;
	private String tecnologia_internet;
	private String tecnologia_tv; //60 + 31 = 91

	private Integer numero_operacion;

	private String periodo_retorno;
	private String velocidad_internet;
	private String envio_contrato;
	private String desafiliacion_pb;
	private String tipo_registro;
	private String meses_financiamiento; //97

	public String getId_visor() {
		return id_visor;
	}
	public void setId_visor(String id_visor) {
		this.id_visor = id_visor;
	}
	public String getBack() {
		return back;
	}
	public void setBack(String back) {
		this.back = back;
	}
	public String getFecha_grabacion() {
		return fecha_grabacion;
	}
	public void setFecha_grabacion(String fecha_grabacion) {
		this.fecha_grabacion = fecha_grabacion;
	}
	public String getId_grabacion() {
		return id_grabacion;
	}
	public void setId_grabacion(String id_grabacion) {
		this.id_grabacion = id_grabacion;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getNombre_producto() {
		return nombre_producto;
	}
	public void setNombre_producto(String nombre_producto) {
		this.nombre_producto = nombre_producto;
	}
	public String getEstado_solicitud() {
		return estado_solicitud;
	}
	public void setEstado_solicitud(String estado_solicitud) {
		this.estado_solicitud = estado_solicitud;
	}
	public String getMotivo_estado() {
		return motivo_estado;
	}
	public void setMotivo_estado(String motivo_estado) {
		this.motivo_estado = motivo_estado;
	}
	public String getSpeech() {
		return speech;
	}
	public void setSpeech(String speech) {
		this.speech = speech;
	}
	public String getAccion() {
		return accion;
	}
	public void setAccion(String accion) {
		this.accion = accion;
	}
	public String getOperacion_comercial() {
		return operacion_comercial;
	}
	public void setOperacion_comercial(String operacion_comercial) {
		this.operacion_comercial = operacion_comercial;
	}
	public String getDistrito() {
		return distrito;
	}
	public void setDistrito(String distrito) {
		this.distrito = distrito;
	}
	public String getCodigo_llamada() {
		return codigo_llamada;
	}
	public void setCodigo_llamada(String codigo_llamada) {
		this.codigo_llamada = codigo_llamada;
	}
	public String getSub_producto() {
		return sub_producto;
	}
	public void setSub_producto(String sub_producto) {
		this.sub_producto = sub_producto;
	}
	public String getTipo_producto() {
		return tipo_producto;
	}
	public void setTipo_producto(String tipo_producto) {
		this.tipo_producto = tipo_producto;
	}
	public String getCodigo_getion() {
		return codigo_getion;
	}
	public void setCodigo_getion(String codigo_getion) {
		this.codigo_getion = codigo_getion;
	}
	public String getFlag_dependencia() {
		return flag_dependencia;
	}
	public void setFlag_dependencia(String flag_dependencia) {
		this.flag_dependencia = flag_dependencia;
	}
	public String getVersion_fila() {
		return version_fila;
	}
	public void setVersion_fila(String version_fila) {
		this.version_fila = version_fila;
	}
	public String getTipo_documento() {
		return tipo_documento;
	}
	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}
	public String getAccion_aplicativo_back() {
		return accion_aplicativo_back;
	}
	public void setAccion_aplicativo_back(String accion_aplicativo_back) {
		this.accion_aplicativo_back = accion_aplicativo_back;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getFecha_de_llamada() {
		return fecha_de_llamada;
	}
	public void setFecha_de_llamada(String fecha_de_llamada) {
		this.fecha_de_llamada = fecha_de_llamada;
	}
	public String getCodigo_pedido() {
		return codigo_pedido;
	}
	public void setCodigo_pedido(String codigo_pedido) {
		this.codigo_pedido = codigo_pedido;
	}
	public String getVersion_fila_t() {
		return version_fila_t;
	}
	public void setVersion_fila_t(String version_fila_t) {
		this.version_fila_t = version_fila_t;
	}
	public String getSub_producto_equiv() {
		return sub_producto_equiv;
	}
	public void setSub_producto_equiv(String sub_producto_equiv) {
		this.sub_producto_equiv = sub_producto_equiv;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getCodigo_sub_gestion() {
		return codigo_sub_gestion;
	}
	public void setCodigo_sub_gestion(String codigo_sub_gestion) {
		this.codigo_sub_gestion = codigo_sub_gestion;
	}
	
	//nuevos


	public String getProducto_ps() {
		return producto_ps;
	}

	public void setProducto_ps(String producto_ps) {
		this.producto_ps = producto_ps;
	}

	public String getPrecio_normal_producto() {
		return precio_normal_producto;
	}

	public void setPrecio_normal_producto(String precio_normal_producto) {
		this.precio_normal_producto = precio_normal_producto;
	}

	public String getPrecio_promo_producto() {
		return precio_promo_producto;
	}

	public void setPrecio_promo_producto(String precio_promo_producto) {
		this.precio_promo_producto = precio_promo_producto;
	}

	public String getMeses_promo_producto() {
		return meses_promo_producto;
	}

	public void setMeses_promo_producto(String meses_promo_producto) {
		this.meses_promo_producto = meses_promo_producto;
	}

	public String getVelocidad_promo_inter() {
		return velocidad_promo_inter;
	}

	public void setVelocidad_promo_inter(String velocidad_promo_inter) {
		this.velocidad_promo_inter = velocidad_promo_inter;
	}

	public String getTiempo_promo_inter() {
		return tiempo_promo_inter;
	}

	public void setTiempo_promo_inter(String tiempo_promo_inter) {
		this.tiempo_promo_inter = tiempo_promo_inter;
	}

	public String getNombre_sva1() {
		return nombre_sva1;
	}

	public void setNombre_sva1(String nombre_sva1) {
		this.nombre_sva1 = nombre_sva1;
	}

	public String getPrecio_uni_sva1() {
		return precio_uni_sva1;
	}

	public void setPrecio_uni_sva1(String precio_uni_sva1) {
		this.precio_uni_sva1 = precio_uni_sva1;
	}

	public String getCantidad_sva1() {
		return cantidad_sva1;
	}

	public void setCantidad_sva1(String cantidad_sva1) {
		this.cantidad_sva1 = cantidad_sva1;
	}

	public String getCode_sva1() {
		return code_sva1;
	}

	public void setCode_sva1(String code_sva1) {
		this.code_sva1 = code_sva1;
	}

	public String getNombre_sva2() {
		return nombre_sva2;
	}

	public void setNombre_sva2(String nombre_sva2) {
		this.nombre_sva2 = nombre_sva2;
	}

	public String getPrecio_uni_sva2() {
		return precio_uni_sva2;
	}

	public void setPrecio_uni_sva2(String precio_uni_sva2) {
		this.precio_uni_sva2 = precio_uni_sva2;
	}

	public String getCantidad_sva2() {
		return cantidad_sva2;
	}

	public void setCantidad_sva2(String cantidad_sva2) {
		this.cantidad_sva2 = cantidad_sva2;
	}

	public String getCode_sva2() {
		return code_sva2;
	}

	public void setCode_sva2(String code_sva2) {
		this.code_sva2 = code_sva2;
	}

	public String getNombre_sva3() {
		return nombre_sva3;
	}

	public void setNombre_sva3(String nombre_sva3) {
		this.nombre_sva3 = nombre_sva3;
	}

	public String getPrecio_uni_sva3() {
		return precio_uni_sva3;
	}

	public void setPrecio_uni_sva3(String precio_uni_sva3) {
		this.precio_uni_sva3 = precio_uni_sva3;
	}

	public String getCantidad_sva3() {
		return cantidad_sva3;
	}

	public void setCantidad_sva3(String cantidad_sva3) {
		this.cantidad_sva3 = cantidad_sva3;
	}

	public String getCode_sva3() {
		return code_sva3;
	}

	public void setCode_sva3(String code_sva3) {
		this.code_sva3 = code_sva3;
	}

	public String getNombre_sva4() {
		return nombre_sva4;
	}

	public void setNombre_sva4(String nombre_sva4) {
		this.nombre_sva4 = nombre_sva4;
	}

	public String getPrecio_uni_sva4() {
		return precio_uni_sva4;
	}

	public void setPrecio_uni_sva4(String precio_uni_sva4) {
		this.precio_uni_sva4 = precio_uni_sva4;
	}

	public String getCantidad_sva4() {
		return cantidad_sva4;
	}

	public void setCantidad_sva4(String cantidad_sva4) {
		this.cantidad_sva4 = cantidad_sva4;
	}

	public String getCode_sva4() {
		return code_sva4;
	}

	public void setCode_sva4(String code_sva4) {
		this.code_sva4 = code_sva4;
	}

	public String getNombre_sva5() {
		return nombre_sva5;
	}

	public void setNombre_sva5(String nombre_sva5) {
		this.nombre_sva5 = nombre_sva5;
	}

	public String getPrecio_uni_sva5() {
		return precio_uni_sva5;
	}

	public void setPrecio_uni_sva5(String precio_uni_sva5) {
		this.precio_uni_sva5 = precio_uni_sva5;
	}

	public String getCantidad_sva5() {
		return cantidad_sva5;
	}

	public void setCantidad_sva5(String cantidad_sva5) {
		this.cantidad_sva5 = cantidad_sva5;
	}

	public String getCode_sva5() {
		return code_sva5;
	}

	public void setCode_sva5(String code_sva5) {
		this.code_sva5 = code_sva5;
	}

	public String getNombre_sva6() {
		return nombre_sva6;
	}

	public void setNombre_sva6(String nombre_sva6) {
		this.nombre_sva6 = nombre_sva6;
	}

	public String getPrecio_uni_sva6() {
		return precio_uni_sva6;
	}

	public void setPrecio_uni_sva6(String precio_uni_sva6) {
		this.precio_uni_sva6 = precio_uni_sva6;
	}

	public String getCantidad_sva6() {
		return cantidad_sva6;
	}

	public void setCantidad_sva6(String cantidad_sva6) {
		this.cantidad_sva6 = cantidad_sva6;
	}

	public String getCode_sva6() {
		return code_sva6;
	}

	public void setCode_sva6(String code_sva6) {
		this.code_sva6 = code_sva6;
	}

	public String getNombre_sva7() {
		return nombre_sva7;
	}

	public void setNombre_sva7(String nombre_sva7) {
		this.nombre_sva7 = nombre_sva7;
	}

	public String getPrecio_uni_sva7() {
		return precio_uni_sva7;
	}

	public void setPrecio_uni_sva7(String precio_uni_sva7) {
		this.precio_uni_sva7 = precio_uni_sva7;
	}

	public String getCantidad_sva7() {
		return cantidad_sva7;
	}

	public void setCantidad_sva7(String cantidad_sva7) {
		this.cantidad_sva7 = cantidad_sva7;
	}

	public String getCode_sva7() {
		return code_sva7;
	}

	public void setCode_sva7(String code_sva7) {
		this.code_sva7 = code_sva7;
	}

	public String getNombre_sva8() {
		return nombre_sva8;
	}

	public void setNombre_sva8(String nombre_sva8) {
		this.nombre_sva8 = nombre_sva8;
	}

	public String getPrecio_uni_sva8() {
		return precio_uni_sva8;
	}

	public void setPrecio_uni_sva8(String precio_uni_sva8) {
		this.precio_uni_sva8 = precio_uni_sva8;
	}

	public String getCantidad_sva8() {
		return cantidad_sva8;
	}

	public void setCantidad_sva8(String cantidad_sva8) {
		this.cantidad_sva8 = cantidad_sva8;
	}

	public String getCode_sva8() {
		return code_sva8;
	}

	public void setCode_sva8(String code_sva8) {
		this.code_sva8 = code_sva8;
	}

	public String getNombre_sva9() {
		return nombre_sva9;
	}

	public void setNombre_sva9(String nombre_sva9) {
		this.nombre_sva9 = nombre_sva9;
	}

	public String getPrecio_uni_sva9() {
		return precio_uni_sva9;
	}

	public void setPrecio_uni_sva9(String precio_uni_sva9) {
		this.precio_uni_sva9 = precio_uni_sva9;
	}

	public String getCantidad_sva9() {
		return cantidad_sva9;
	}

	public void setCantidad_sva9(String cantidad_sva9) {
		this.cantidad_sva9 = cantidad_sva9;
	}

	public String getCode_sva9() {
		return code_sva9;
	}

	public void setCode_sva9(String code_sva9) {
		this.code_sva9 = code_sva9;
	}

	public String getNombre_sva10() {
		return nombre_sva10;
	}

	public void setNombre_sva10(String nombre_sva10) {
		this.nombre_sva10 = nombre_sva10;
	}

	public String getPrecio_uni_sva10() {
		return precio_uni_sva10;
	}

	public void setPrecio_uni_sva10(String precio_uni_sva10) {
		this.precio_uni_sva10 = precio_uni_sva10;
	}

	public String getCantidad_sva10() {
		return cantidad_sva10;
	}

	public void setCantidad_sva10(String cantidad_sva10) {
		this.cantidad_sva10 = cantidad_sva10;
	}

	public String getCode_sva10() {
		return code_sva10;
	}

	public void setCode_sva10(String code_sva10) {
		this.code_sva10 = code_sva10;
	}

	public String getAceptacion_recibo_digital() {
		return aceptacion_recibo_digital;
	}

	public void setAceptacion_recibo_digital(String aceptacion_recibo_digital) {
		this.aceptacion_recibo_digital = aceptacion_recibo_digital;
	}

	public String getAceptacion_proteccion_datos() {
		return aceptacion_proteccion_datos;
	}

	public void setAceptacion_proteccion_datos(String aceptacion_proteccion_datos) {
		this.aceptacion_proteccion_datos = aceptacion_proteccion_datos;
	}

	public String getFiltro_web_parental() {
		return filtro_web_parental;
	}

	public void setFiltro_web_parental(String filtro_web_parental) {
		this.filtro_web_parental = filtro_web_parental;
	}

	public String getModalidad_pago() {
		return modalidad_pago;
	}

	public void setModalidad_pago(String modalidad_pago) {
		this.modalidad_pago = modalidad_pago;
	}

	public String getMonto_contado() {
		return monto_contado;
	}

	public void setMonto_contado(String monto_contado) {
		this.monto_contado = monto_contado;
	}

	public String getMeses_devolucion() {
		return meses_devolucion;
	}

	public void setMeses_devolucion(String meses_devolucion) {
		this.meses_devolucion = meses_devolucion;
	}

	public String getEmail_cliente() {
		return email_cliente;
	}

	public void setEmail_cliente(String email_cliente) {
		this.email_cliente = email_cliente;
	}

	public String getId_transaccion() {
		return id_transaccion;
	}

	public void setId_transaccion(String id_transaccion) {
		this.id_transaccion = id_transaccion;
	}

	public String getCosto_instalacion() {
		return costo_instalacion;
	}

	public void setCosto_instalacion(String costo_instalacion) {
		this.costo_instalacion = costo_instalacion;
	}

	public String getEquipamiento_tv() {
		return equipamiento_tv;
	}

	public void setEquipamiento_tv(String equipamiento_tv) {
		this.equipamiento_tv = equipamiento_tv;
	}

	public String getEquipamiento_internet() {
		return equipamiento_internet;
	}

	public void setEquipamiento_internet(String equipamiento_internet) {
		this.equipamiento_internet = equipamiento_internet;
	}

	public String getEquipamiento_linea() {
		return equipamiento_linea;
	}

	public void setEquipamiento_linea(String equipamiento_linea) {
		this.equipamiento_linea = equipamiento_linea;
	}

	public String getTecnologia_internet() {
		return tecnologia_internet;
	}

	public void setTecnologia_internet(String tecnologia_internet) {
		this.tecnologia_internet = tecnologia_internet;
	}

	public String getTecnologia_tv() {
		return tecnologia_tv;
	}

	public void setTecnologia_tv(String tecnologia_tv) {
		this.tecnologia_tv = tecnologia_tv;
	}

	public Integer getNumero_operacion() {
		return numero_operacion;
	}

	public void setNumero_operacion(Integer numero_operacion) {
		this.numero_operacion = numero_operacion;
	}

	public String getPeriodo_retorno() {
		return periodo_retorno;
	}

	public void setPeriodo_retorno(String periodo_retorno) {
		this.periodo_retorno = periodo_retorno;
	}

	public String getVelocidad_internet() {
		return velocidad_internet;
	}

	public void setVelocidad_internet(String velocidad_internet) {
		this.velocidad_internet = velocidad_internet;
	}

	public String getEnvio_contrato() {
		return envio_contrato;
	}

	public void setEnvio_contrato(String envio_contrato) {
		this.envio_contrato = envio_contrato;
	}

	public String getDesafiliacion_pb() {
		return desafiliacion_pb;
	}

	public void setDesafiliacion_pb(String desafiliacion_pb) {
		this.desafiliacion_pb = desafiliacion_pb;
	}

	public String getTipo_registro() {
		return tipo_registro;
	}

	public void setTipo_registro(String tipo_registro) {
		this.tipo_registro = tipo_registro;
	}

	public String getMeses_financiamiento() {
		return meses_financiamiento;
	}

	public void setMeses_financiamiento(String meses_financiamiento) {
		this.meses_financiamiento = meses_financiamiento;
	}
}
