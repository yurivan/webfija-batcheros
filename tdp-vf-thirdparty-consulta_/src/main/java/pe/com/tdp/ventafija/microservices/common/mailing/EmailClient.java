package pe.com.tdp.ventafija.microservices.common.mailing;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.glassfish.jersey.logging.LoggingFeature;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Felix on 19/06/2017.
 */
public class EmailClient<R,T> {

    private String url = "";

    public EmailClient(String url){
        this.url = url;
    }

    public ClientResult<T> sendRequest(R request) {
        ClientResult<T> result = new ClientResult<>();
        try {
            String json = doRequest2(request);

            T apiResponse = getResponse2(json);

            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
        } finally {

        }
        return result;
    }

    public String jerseyPOST2(URI uri, Object apiRequest) {
        ObjectMapper mapper = new ObjectMapper();
        String requestJson;
        try {
            requestJson = mapper.writeValueAsString(apiRequest);
            //logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s consulta a %s: %s", timeStamp.getTime(), uri, requestJson));
        } catch (JsonProcessingException e) {}
        Client client = ClientBuilder.newClient();
        client.register(new LoggingFeature());
        WebTarget target = client.target(UriBuilder.fromUri(uri).build());

        String json = "";

        Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("content-type", "application/json");
        Response response = invocationBuilder.post(Entity.entity(apiRequest, MediaType.APPLICATION_JSON), Response.class);
        json = response.readEntity(String.class);

        //logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, json));
        return json;
    }

    protected String doRequest2(R body) throws URISyntaxException {
        URI uri = new URI(url);
        String jsonSMS = jerseyPOST2(uri, /*getApiRequest(*/body/*)*/);
        //getEvent().setServiceResponse(jsonSMS);
        //getEvent().setResult("OK");
        //getEvent().setMsg("OK");
        return jsonSMS;
    }

    protected T getResponse2(String json) throws JsonParseException, JsonMappingException, IOException {
        System.out.println(json);
        ObjectMapper mapper = new ObjectMapper();
        T objAtis = mapper.readValue(json, new TypeReference<EmailResponseBody>() {});
        return objAtis;
    }

}
