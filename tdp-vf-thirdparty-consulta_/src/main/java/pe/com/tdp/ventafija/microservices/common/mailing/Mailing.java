package pe.com.tdp.ventafija.microservices.common.mailing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Mailing {

    private static final Logger logger = LogManager.getLogger(Mailing.class);

    @Value("${tdp.mail.emblue.authenticate.uri}")
    private String emailAutenticateUri;

    @Value("${tdp.mail.emblue.checkconexion.uri}")
    private String emailCheckConexionUri;

    @Value("${tdp.mail.emblue.sendexpress.uri}")
    private String emailSendExpressUri;

    @Value("${tdp.mail.emblue.user}")
    private String emailUser;

    @Value("${tdp.mail.emblue.password}")
    private String emailPassword;

    @Value("${tdp.mail.emblue.token}")
    private String emailEmblueToken;

    @Value("${tdp.mail.emblue.sendmailexpresss.actionId}")
    private String emailSendExpressActionId;

    @Value("${tdp.mail.visor.receiver}")
    private String receivers;

    @Value("${tdp.mail.visor.enabled}")
    private Boolean sendMailingEnable;

    @Value("${tdp.mail.visor.subject.notfound}")
    public String subjectTxtNotFound;

    @Value("${tdp.mail.visor.subject.rowserror}")
    public String subjectTxtWithError;

    @Value("${tdp.mail.visor.body.notfound}")
    public String bodyTxtNotFound;

    @Value("${tdp.mail.visor.body.rowserror}")
    public String bodyTxtWithError;

    String emailToken = "";

    public void sendMailToReceivers(String message, String subject){

        if(!sendMailingEnable){
            logger.info("El envío de emails se encuentra DESACTIVADO.");
            return;
        }

        String[] receiverList =  receivers.split("\\,");
        for(String email : receiverList){
            EmailResponseBody sendExpressResult = sendMailExpress(email, message, subject);
            logger.info("Result ->");
            logger.info("Email: " + email);
            logger.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
            logger.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
        }
    }

    private EmailResponseBody sendMailExpress(String email, String message, String subject){
        emailToken = getToken();

        EmailRequestBody request = new EmailRequestBody();
        request.setEmail(email);
        request.setActionId(emailSendExpressActionId);
        request.setMessage(message);
        request.setToken(emailToken);
        request.setSubject(subject);

        EmailClient<EmailRequestBody, EmailResponseBody> emailSendExpress = new EmailClient<>(emailSendExpressUri);
        ClientResult<EmailResponseBody> sendExpressResponse = emailSendExpress.sendRequest(request);
        return sendExpressResponse.getResult();
    }

    private String getToken(){
        if(emailToken.equals("")) emailToken = authenticate();

        Boolean result = checkConection(emailToken);
        if(!result) emailToken = authenticate();
        return emailToken;
    }

    private String authenticate(){
        AuthenticateRequestBody authenticateRequest = new AuthenticateRequestBody();
        authenticateRequest.setUser(emailUser);
        authenticateRequest.setPass(emailPassword);
        authenticateRequest.setToken(emailEmblueToken);

        EmailClient<AuthenticateRequestBody, EmailResponseBody> emailAuthenticate = new EmailClient<>(emailAutenticateUri);
        ClientResult<EmailResponseBody> authenticateResponse = emailAuthenticate.sendRequest(authenticateRequest);

        String tokenToSend = authenticateResponse.getResult().getToken();
        return tokenToSend;
    }

    private Boolean checkConection(String tokenToSend){
        CheckConexionRequestBody checkConexionRequest = new CheckConexionRequestBody();
        checkConexionRequest.setToken(tokenToSend);

        EmailClient<CheckConexionRequestBody, EmailResponseBody> emailCheckConexion = new EmailClient<>(emailCheckConexionUri);
        ClientResult<EmailResponseBody> checkConexionResponse = emailCheckConexion.sendRequest(checkConexionRequest);

        EmailResponseBody checkConexionResult = checkConexionResponse.getResult();
        return (checkConexionResult != null) ? checkConexionResult.getResult() : false;
    }
}