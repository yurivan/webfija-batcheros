package pe.com.tdp.ventafija.microservices.service.thirdparty;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jcraft.jsch.Proxy;
import com.jcraft.jsch.ProxySOCKS5;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobClient;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.ListBlobItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.Cip.ConsultarCIp;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.UpFrontClient;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.UpFrontRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.UpFrontResponseBody;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextImpl;
import pe.com.tdp.ventafija.microservices.common.dao.MTAzureBlobStorageRepository;
import pe.com.tdp.ventafija.microservices.common.dao.MTAzureFileStorageRepository;
import pe.com.tdp.ventafija.microservices.common.dao.OrdenMTRepository;
import pe.com.tdp.ventafija.microservices.common.domain.dto.HDECResponse;
import pe.com.tdp.ventafija.microservices.common.domain.entity.MTAzureBlobStorage;
import pe.com.tdp.ventafija.microservices.common.domain.entity.MTAzureFileStorage;
import pe.com.tdp.ventafija.microservices.common.domain.entity.OrdenMT;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.exception.ApplicationException;
import pe.com.tdp.ventafija.microservices.common.mailing.Mailing;
import pe.com.tdp.ventafija.microservices.common.services.GoogleStorageService;
import pe.com.tdp.ventafija.microservices.common.services.HDECRetryService;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.services.TwilioService;
import pe.com.tdp.ventafija.microservices.common.util.StringUtils;
import pe.com.tdp.ventafija.microservices.common.util.exception.SyncException;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.*;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncDevComResult;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncDevTecResult;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncHdecResult;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncHdecSyntaxError;
import pe.com.tdp.ventafija.microservices.repository.thirdparty.ThirdpartyDAO;
import pe.com.tdp.ventafija.microservices.util.LogSystem;
import pe.pagoefectivo.PagoEfectivo;
import pe.pagoefectivo.service.ConsultarCIPService;
import pe.pagoefectivo.service.GenerarCIPService;
import pe.pagoefectivo.service.ws.BEGenRequest;
import pe.pagoefectivo.service.ws.BEWSConsultarRequest;
import pe.pagoefectivo.service.ws.BEWSConsultarResponse;
import pe.pagoefectivo.service.ws.BEpaymentResponse;
import pe.com.tdp.ventafija.microservices.domain.Cip2.ConsultarCipResponse;
import pe.com.tdp.ventafija.microservices.domain.Cip2.ConsultarCipRequest;
import pe.com.tdp.ventafija.microservices.domain.Cip2.Data;
import pe.com.tdp.ventafija.microservices.domain.Cip2.DataResponse;

import java.io.*;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.regex.Pattern;

@Service
public class ThirdpartyService {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private ThirdpartyDAO dao;
    @Autowired
    private GoogleStorageService gss;
    @Autowired
    private FtpService ftpService;
    @Autowired
    private TwilioService twilioService;
    @Autowired
    private ApiHeaderConfig apiHeaderConfig;
    @Autowired
    private ServiceCallEventsService serviceCallEventsService;
    @Autowired
    private Mailing mailing;
    @Autowired
    private TaskExecutor taskExecutor;
    @Autowired
    private HDECRetryService hdecRetryService;
    @Autowired
    private OrdenMTRepository ordenMTRepository;
    @Autowired
    private MTAzureFileStorageRepository mtAzureFileStorageRepository;
    @Autowired
    private MTAzureBlobStorageRepository mtAzureBlobStorageRepository;

    @Value("${azure.api.connection.legacy}")
    private String azureConnectionLegacy;
    @Value("${azure.api.connection.nolegacy}")
    private String azureConnectionNoLegacy;

    @Value("${ftp.api.connection.dir}")
    private String remoteDirectory;
    @Value("${ftp.api.connection.takendir}")
    private String takenDirectory;
    @Value("${ftp.api.connection.errordir}")
    private String errorDirectory;

    @Value("${ftp.calculator.api.connection.dir}")
    private String remoteDirectoryCalc;

    @Value("${ftp.api.connection.dirdevueltas}")
    private String remoteDirectoryDevueltas;
    @Value("${ftp.api.connection.takendirdevueltas}")
    private String takenDirectoryDevueltas;
    @Value("${ftp.api.connection.errordirdevueltas}")
    private String errorDirectoryDevueltas;

    @Value("${ftp.api.connection.filename}")
    private String fileName;
    @Value("${ftp.calculator.api.connection.filename}")
    private String fileNameCalc;
    @Value("${ftp.calculator.api.connection.newfilename}")
    private String newFileNameCalc;

    // valores para Pago Efectivo
    @Value("${pe.api.key.private}")
    private String privateKeyLocation;
    @Value("${pe.api.key.public}")
    private String publicKeyLocation;
    @Value("${pe.api.url.cripto}")
    private String cryptoUrl;
    @Value("${pe.api.url.service}")
    private String servicesUrl;
    @Value("${pe.api.code.service}")
    private String serviceCode;
    @Value("${pe.api.proxy.hostname}")
    private String proxyHostname;
    @Value("${pe.api.proxy.port}")
    private String proxyPort;
    @Value("${pe.api.proxy.user}")
    private String proxyUser;
    @Value("${pe.api.proxy.password}")
    private String proxyPassword;
    @Value("${pe.api.sync.startdays}")
    private String startdays;
    @Value("${pe.api.sync.startdate}")
    private String startdate;
    @Value("${pe.api.sync.flaglog}")
    private String flagCipLog;
    @Value("${tdp.vigencia.upfront.inicio}")
    private String flagUpfront;
    @Value("${visor.api.sync.enabled}")
    private Boolean syncEnabled;
    @Value("${devueltasc.api.sync.enabled}")
    private Boolean devComEnabled;
    @Value("${devueltast.api.sync.enabled}")
    private Boolean devTecEnabled;
    @Value("${calculator.api.sync.enabled}")
    private Boolean calcSyncEnabled;
    @Value("${visor.api.sync.back}")
    private String back_app;

    private static final String CSV_EXT = ".csv";
    private static final String TXT_EXT = ".txt";

	/*public Response<String> completeSync() throws SyncException {
		String filePath = remoteDirectory + fileName + CSV_EXT;
		return sync(filePath);
	}*/

    //@Scheduled(cron = "*/30 * * * * *")
    //@Scheduled(cron = "${visor.api.sync.cron}")
    public void diffSync() {
        if (!syncEnabled) {
            logger.info("actualizacion visor no habilitada");
            return;
        }
        Date ahora = new Date();
        long ahoramilisecs = ahora.getTime();
        logger.info("Iniciando actualizacion visor @" + ahora + " .....");
        List<String> filesToProcess = ftpService.listFiles(remoteDirectory, false);
        logger.info(String.format("listado de archivos del ftp en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
        ahoramilisecs = new Date().getTime();
        boolean txtFound = false;
        for (String file : filesToProcess) {
            if (Pattern.matches("BD_VISOR_BI_\\d+\\.txt", file)) {
                txtFound = true;
                logger.info("archivo: " + file);
                String newFileName = String.format("VF_%s_%s", new Date().getTime(), file);
                logger.info(String.format("Archivo cambiado de nombre en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
                ahoramilisecs = new Date().getTime();

                String filePath = "." + remoteDirectory + file;
//				String filePath = remoteDirectory + file;
//				String filePath = takenDirectory + newFileName;
                try {
                    boolean success = sync(filePath, file);
                    if (success) ftpService.moveFile(remoteDirectory, file, takenDirectory, newFileName);
                    else moveFileToError(file);
                } catch (SyncException e) {
                    logger.error(":( --> " + e.getMessage());
                    moveFileToError(file);
					/*int position = file.lastIndexOf(".txt");
					String errorFileName = file;
					if (position!=-1) {
						errorFileName = file.substring(0, position)+"0.txt";
					}

					ftpService.moveFile(remoteDirectory, file, errorDirectory, errorFileName);*/
                }
                logger.info(String.format("Archivo procesado en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
            }
        }
        if (!txtFound) {
            dao.saveVisorLogTxtNotFound();
            sendEmailOnTxtNotFound();
        }
        logger.info("fin actualizacion visor @" + ahora + " .................");
    }

    public void moveFileToError(String file) {
        int position = file.lastIndexOf(".txt");
        String errorFileName = file;
        if (position != -1) {
            errorFileName = file.substring(0, position) + "0.txt";
        }

        ftpService.moveFile(remoteDirectory, file, errorDirectory, errorFileName);
    }
    //..

    //@Scheduled(cron = "${devueltas.api.sync.cron1}")
    //@Scheduled(cron = "*/30 * * * * *")
   // @Scheduled(cron = "${devueltas.api.sync.cron2}")
    public void diffDevComercialSync() {
        if (!devComEnabled) {
            logger.info("actualizacion Devueltas Comerciales no habilitada");
            return;
        }
        Date ahora = new Date();
        long ahoramilisecs = ahora.getTime();
        logger.info("Iniciando actualizacion Devueltas Comerciales @" + ahora + " .....");
        List<String> filesToProcess = ftpService.listFiles(remoteDirectoryDevueltas, true);
        logger.info(String.format("listado de archivos del ftp en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
        ahoramilisecs = new Date().getTime();
        for (String file : filesToProcess) {
            if (Pattern.matches("Online_Comercial_\\d+\\.txt", file)) {
                logger.info("archivo: " + file);
                String newFileName = String.format("VFDC_%s_%s", new Date().getTime(), file);
                logger.info(String.format("Archivo cambiado de nombre en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
                ahoramilisecs = new Date().getTime();

                String filePath = "." + remoteDirectoryDevueltas + file;
//				String filePath = remoteDirectory + file;
//				String filePath = takenDirectory + newFileName;
                try {
                    syncDevComercial(filePath);
                    ftpService.moveFile(remoteDirectoryDevueltas, file, takenDirectoryDevueltas, newFileName);
                } catch (SyncException e) {
                    logger.error(":(", e);
                    int position = file.lastIndexOf(".txt");
                    String errorFileName = file;
                    if (position != -1) {
                        errorFileName = file.substring(0, position) + "0.txt";
                    }

                    ftpService.moveFile(remoteDirectoryDevueltas, file, errorDirectoryDevueltas, errorFileName);
                }
                logger.info(String.format("Archivo procesado en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
            }
        }
        logger.info("fin actualizacion visor @" + ahora + " .................");
    }

    //@Scheduled(cron = "${devueltas.api.sync.cron1}")
    //@Scheduled(cron = "*/30 * * * * *")
   // @Scheduled(cron = "${devueltas.api.sync.cron2}")
    public void diffDevTecnicaSync() {
        if (!devTecEnabled) {
            logger.info("actualizacion Devueltas Tecnicas no habilitada");
            return;
        }
        Date ahora = new Date();
        long ahoramilisecs = ahora.getTime();
        logger.info("Iniciando actualizacion Devueltas Tecnicas @" + ahora + " .....");
        List<String> filesToProcess = ftpService.listFiles(remoteDirectoryDevueltas, true);
        logger.info(String.format("listado de archivos del ftp en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
        ahoramilisecs = new Date().getTime();
        for (String file : filesToProcess) {
            if (Pattern.matches("Online_Tecnico_\\d+\\.txt", file)) {
                logger.info("archivo: " + file);
                String newFileName = String.format("VFDT_%s_%s", new Date().getTime(), file);
                logger.info(String.format("Archivo cambiado de nombre en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
                ahoramilisecs = new Date().getTime();

                String filePath = "." + remoteDirectoryDevueltas + file;
//				String filePath = remoteDirectory + file;
//				String filePath = takenDirectory + newFileName;
                try {
                    syncDevTecnica(filePath);
                    ftpService.moveFile(remoteDirectoryDevueltas, file, takenDirectoryDevueltas, newFileName);
                } catch (SyncException e) {
                    logger.error(":(", e);
                    int position = file.lastIndexOf(".txt");
                    String errorFileName = file;
                    if (position != -1) {
                        errorFileName = file.substring(0, position) + "0.txt";
                    }

                    ftpService.moveFile(remoteDirectoryDevueltas, file, errorDirectoryDevueltas, errorFileName);
                }
                logger.info(String.format("Archivo procesado en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
            }
        }
        logger.info("fin actualizacion visor @" + ahora + " .................");
    }

    public boolean sync(String remoteFilePath, String fileNameTxt) throws SyncException {
        logger.info("starting sync of file: " + remoteFilePath);
        //Response<String> response = new Response<>();
        boolean success = false;
        File file = null;
        try {
            file = ftpService.retrieveFile(remoteFilePath);
            if (file != null) {
                success = processHdcFile2(file, fileNameTxt);
                //processHdcFile(file);
            } else {
                throw new ApplicationException("file.not.found");
            }

            //response.setResponseCode("0");
            //response.setResponseMessage("HDEC se sincronizo correctamente.");
        } catch (IOException | ApplicationException e) {
            logger.info("Error al buscar el archivo", e);
            //response.setResponseCode("1");
            //response.setResponseMessage("Error al sincronizar HDEC.");
        } finally {
            if (file != null) {
                file.delete();
            }
        }
        logger.info("sync finished");
        return success;
    }

    public Response<String> syncDevComercial(String remoteFilePath) throws SyncException {
        logger.info("starting sync of file: " + remoteFilePath);
        Response<String> response = new Response<>();
        File file = null;
        try {
            file = ftpService.retrieveFile(remoteFilePath);
            if (file != null) {
                processDevComercialFile(file);
            } else {
                throw new ApplicationException("file.not.found");
            }

            response.setResponseCode("0");
            response.setResponseMessage("Devuelta Comercial se sincronizo correctamente.");
        } catch (IOException | ApplicationException e) {
            logger.info("Error al buscar el archivo", e);
            response.setResponseCode("1");
            response.setResponseMessage("Error al sincronizar Devuelta Comercial.");
        } finally {
            if (file != null) {
                file.delete();
            }
        }
        logger.info("sync finished");
        return response;
    }

    public Response<String> syncDevTecnica(String remoteFilePath) throws SyncException {
        logger.info("starting sync of file: " + remoteFilePath);
        Response<String> response = new Response<>();
        File file = null;
        try {
            file = ftpService.retrieveFile(remoteFilePath);
            if (file != null) {
                processDevTecnicaFile(file);
            } else {
                throw new ApplicationException("file.not.found");
            }

            response.setResponseCode("0");
            response.setResponseMessage("Devuelta Tecnica se sincronizo correctamente.");
        } catch (IOException | ApplicationException e) {
            logger.info("Error al buscar el archivo", e);
            response.setResponseCode("1");
            response.setResponseMessage("Error al sincronizar Devuelta Tecnica.");
        } finally {
            if (file != null) {
                file.delete();
            }
        }
        logger.info("sync finished");
        return response;
    }

    public void processHdcFile(File file) throws IOException, SyncException {
        logger.info("ThirdpartyService.processHdcFile .........");
        List<HdecResponseData> hdcs = new ArrayList<>();
        List<SyncHdecSyntaxError> syntaxErrors = new ArrayList<>();

        FileInputStream fstream = new FileInputStream(file);

        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        String line;
        String cvsSplitBy = "\\|"; //"\t";

        while ((line = br.readLine()) != null) {
            String[] hdc = line.split(cvsSplitBy);
            if (hdc.length < 30) {
                logger.info("error al procesar linea " + line);
                syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_INCOMPLETE_LINE, line));
                continue;
            }
            HdecResponseData model = new HdecResponseData();

            model.setId_visor(StringUtils.trim(hdc[0]));
            model.setBack(StringUtils.trim(hdc[1]));
            model.setFecha_grabacion(StringUtils.trim(hdc[2]));
            model.setId_grabacion(StringUtils.trim(hdc[3]));
            model.setCliente(StringUtils.trim(hdc[4]));
            model.setTelefono(StringUtils.trim(hdc[5]));
            model.setDireccion(StringUtils.trim(hdc[6]));
            model.setDepartamento(StringUtils.trim(hdc[7]));
            model.setDni(StringUtils.trim(hdc[8]));
            model.setNombre_producto(StringUtils.trim(hdc[9]));
            model.setEstado_solicitud(StringUtils.trim(hdc[10]));
            model.setMotivo_estado(StringUtils.trim(hdc[11]));
            model.setSpeech(StringUtils.trim(hdc[12]));
            model.setAccion(StringUtils.trim(hdc[13]));
            model.setOperacion_comercial(StringUtils.trim(hdc[14]));
            model.setDistrito(StringUtils.trim(hdc[15]));
            model.setCodigo_llamada(StringUtils.trim(hdc[16]));
            model.setSub_producto(StringUtils.trim(hdc[17]));
            model.setTipo_producto(StringUtils.trim(hdc[18]));
            model.setCodigo_getion(StringUtils.trim(hdc[19]));
            model.setCodigo_sub_gestion(StringUtils.trim(hdc[20]));
            model.setFlag_dependencia(StringUtils.trim(hdc[21]));
            model.setVersion_fila(StringUtils.trim(hdc[22]));
            model.setTipo_documento(StringUtils.trim(hdc[23]));
            model.setAccion_aplicativo_back(StringUtils.trim(hdc[24]));
            model.setProvincia(StringUtils.trim(hdc[25]));
            model.setFecha_de_llamada(StringUtils.trim(hdc[26]));
            model.setCodigo_pedido(StringUtils.trim(hdc[27]));
            model.setVersion_fila_t(StringUtils.trim(hdc[28]));
            model.setSub_producto_equiv(StringUtils.trim(hdc[29]));
            model.setProducto(hdc.length > 30 ? StringUtils.trim(hdc[30]) : null);
            //model.setNombre_producto(model.getProducto()); // setea al nombre del producto el ultimo campo de la linea

            if (model.getBack() != null && back_app.toLowerCase().contains(model.getBack().toLowerCase()) &&
                    model.getId_grabacion() != null &&
                    model.getId_grabacion().startsWith("-")) {
                model.setId_visor(model.getId_grabacion());
//					logger.info("procesando registro APP VF");
            }

            if (model.getDni() != null && model.getDni().length() > 11) {
                syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_DNI_ERROR, line));
                System.out.println(model.getId_visor());
                System.out.println(String.format("dni %s %s", model.getDni(), model.getDni() != null ? model.getDni().length() : 0));
            } else {
                if (model.getTelefono() != null && model.getTelefono().length() > 20) {
                    syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_TELEFONO_ERROR, line));
                    System.out.println(model.getId_visor());
                    System.out.println(String.format("telefono %s %s", model.getTelefono(), model.getTelefono() != null ? model.getTelefono().length() : 0));
                } else {
                    if (model.getDistrito() != null && model.getDistrito().length() > 50) {
                        syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_DISTRITO_ERROR, line));
                        System.out.println(model.getId_visor());
                        System.out.println(line);
                        System.out.println(String.format("distrito %s %s", model.getDistrito(), model.getDistrito() != null ? model.getDistrito().length() : 0));
                    } else {
                        hdcs.add(model);
                    }
                }
            }
        }
        in.close();
        fstream.close();
        SyncHdecResult result = dao.syncHDEC(hdcs);
        result.setSyntaxErrors(syntaxErrors);
        result.setErrorCount(result.getErrorCount() + syntaxErrors.size());
        List<HdecResponseData> updatedInfo = result.getData();
//		logger.info(String.format("Total registros leidos: %s, insertados: %s, actualizados: %s, errores: %s",
//				hdcs.size(), result.getInsertCount(), result.getUpdateCount(), result.getErrorCount()));
        logger.info(String.format("Total registros leidos: %s, insertados: %s, actualizados: %s,actualizados order: %s, errores: %s",
                hdcs.size(), result.getInsertCount(), result.getUpdateCount(), result.getOrderCount(), result.getErrorCount()));
        List<String> linesWithError = new ArrayList<>();
        for (SyncHdecSyntaxError error : syntaxErrors) {
            linesWithError.add(error.getLine());
        }
//		if (!linesWithError.isEmpty()) {
//			String errorFileName = String.format("VF_ERRORS_%s.txt", new Date().getTime());
//			String filePath = remoteDirectory + errorFileName;
//			File errorsFile = FileUtils.writeLines(linesWithError);
//			ftpService.uploadFile(errorsFile, filePath);
//			logger.info(String.format("Archivo de errores %s subido", errorFileName));
//			if (errorsFile != null) {
//				errorsFile.delete();
//			}
//		}
        logger.info("fin ThirdpartyService.processHdcFile .....................");
        //processAzure(updatedInfo);
//		processUpFront(updatedInfo);
    }

    public boolean processHdcFile2(File file, String fileNameTxt) throws IOException {
        logger.info("ThirdpartyService.processHdcFile .........");
        List<HdecResponseData> hdcs = new ArrayList<>();
        List<SyncHdecSyntaxError> syntaxErrors = new ArrayList<>();

        boolean success = false;

        try {
            FileInputStream fstream = new FileInputStream(file);

            DataInputStream in = new DataInputStream(fstream);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));

            String line;
            String cvsSplitBy = "\\|"; //"\t";

            int count = 0;

            while ((line = br.readLine()) != null) {

                count++;

                line = line.replace("||", "| |").replace("||", "| |");
                line = line + " ";

                String[] hdc = line.split(cvsSplitBy);
                if (hdc.length < 97) {
                    logger.info("error al procesar linea " + line);
                    syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_INCOMPLETE_LINE, line));
                    continue;
                }

                //logger.info(count);

                if ("ID_VISOR".equals(StringUtils.trim(hdc[0]))) continue;

                HdecResponseData model = new HdecResponseData();

                model.setId_visor(StringUtils.trim(hdc[0]));
                model.setBack(StringUtils.trim(hdc[1]));
                model.setFecha_grabacion(StringUtils.trim(hdc[2]));
                model.setId_grabacion(StringUtils.trim(hdc[3]));
                model.setCliente(StringUtils.trim(hdc[4]));
                model.setTelefono(StringUtils.trim(hdc[5]));
                model.setDireccion(StringUtils.trim(hdc[6]));
                model.setDepartamento(StringUtils.trim(hdc[7]));
                model.setDni(StringUtils.trim(hdc[8]));
                model.setNombre_producto(StringUtils.trim(hdc[9]));
                model.setEstado_solicitud(StringUtils.trim(hdc[10]));
                model.setMotivo_estado(StringUtils.trim(hdc[11]));
                model.setSpeech(StringUtils.trim(hdc[12]));
                model.setAccion(StringUtils.trim(hdc[13]));
                model.setOperacion_comercial(StringUtils.trim(hdc[14]));
                model.setDistrito(StringUtils.trim(hdc[15]));
                model.setCodigo_llamada(StringUtils.trim(hdc[16]));
                model.setSub_producto(StringUtils.trim(hdc[17]));
                model.setTipo_producto(StringUtils.trim(hdc[18]));
                model.setCodigo_getion(StringUtils.trim(hdc[19]));
                model.setCodigo_sub_gestion(StringUtils.trim(hdc[20]));
                model.setFlag_dependencia(StringUtils.trim(hdc[21]));
                model.setVersion_fila(StringUtils.trim(hdc[22]));
                model.setTipo_documento(StringUtils.trim(hdc[23]));
                model.setAccion_aplicativo_back(StringUtils.trim(hdc[24]));
                model.setProvincia(StringUtils.trim(hdc[25]));
                model.setFecha_de_llamada(StringUtils.trim(hdc[26]));
                model.setCodigo_pedido(StringUtils.trim(hdc[27]));
                model.setVersion_fila_t(StringUtils.trim(hdc[28]));
                model.setSub_producto_equiv(StringUtils.trim(hdc[29]));
                model.setProducto(hdc.length > 30 ? StringUtils.trim(hdc[30]) : null);
                //model.setNombre_producto(model.getProducto()); // setea al nombre del producto el ultimo campo de la linea


                model.setProducto_ps(StringUtils.trim(hdc[31]));
                model.setPrecio_normal_producto(StringUtils.trim(hdc[32]));
                model.setPrecio_promo_producto(StringUtils.trim(hdc[33]));
                model.setMeses_promo_producto(StringUtils.trim(hdc[34]));
                model.setVelocidad_promo_inter(StringUtils.trim(hdc[35]));
                model.setTiempo_promo_inter(StringUtils.trim(hdc[36]));
                model.setNombre_sva1(StringUtils.trim(hdc[37]));
                model.setPrecio_uni_sva1(StringUtils.trim(hdc[38]));
                model.setCantidad_sva1(StringUtils.trim(hdc[39]));
                model.setCode_sva1(StringUtils.trim(hdc[40]));
                model.setNombre_sva2(StringUtils.trim(hdc[41]));
                model.setPrecio_uni_sva2(StringUtils.trim(hdc[42]));
                model.setCantidad_sva2(StringUtils.trim(hdc[43]));
                model.setCode_sva2(StringUtils.trim(hdc[44]));
                model.setNombre_sva3(StringUtils.trim(hdc[45]));
                model.setPrecio_uni_sva3(StringUtils.trim(hdc[46]));
                model.setCantidad_sva3(StringUtils.trim(hdc[47]));
                model.setCode_sva3(StringUtils.trim(hdc[48]));
                model.setNombre_sva4(StringUtils.trim(hdc[49]));
                model.setPrecio_uni_sva4(StringUtils.trim(hdc[50]));
                model.setCantidad_sva4(StringUtils.trim(hdc[51]));
                model.setCode_sva4(StringUtils.trim(hdc[52]));
                model.setNombre_sva5(StringUtils.trim(hdc[53]));
                model.setPrecio_uni_sva5(StringUtils.trim(hdc[54]));
                model.setCantidad_sva5(StringUtils.trim(hdc[55]));
                model.setCode_sva5(StringUtils.trim(hdc[56]));
                model.setNombre_sva6(StringUtils.trim(hdc[57]));
                model.setPrecio_uni_sva6(StringUtils.trim(hdc[58]));
                model.setCantidad_sva6(StringUtils.trim(hdc[59]));
                model.setCode_sva6(StringUtils.trim(hdc[60]));
                model.setNombre_sva7(StringUtils.trim(hdc[61]));
                model.setPrecio_uni_sva7(StringUtils.trim(hdc[62]));
                model.setCantidad_sva7(StringUtils.trim(hdc[63]));
                model.setCode_sva7(StringUtils.trim(hdc[64]));
                model.setNombre_sva8(StringUtils.trim(hdc[65]));
                model.setPrecio_uni_sva8(StringUtils.trim(hdc[66]));
                model.setCantidad_sva8(StringUtils.trim(hdc[67]));
                model.setCode_sva8(StringUtils.trim(hdc[68]));
                model.setNombre_sva9(StringUtils.trim(hdc[69]));
                model.setPrecio_uni_sva9(StringUtils.trim(hdc[70]));
                model.setCantidad_sva9(StringUtils.trim(hdc[71]));
                model.setCode_sva9(StringUtils.trim(hdc[72]));
                model.setNombre_sva10(StringUtils.trim(hdc[73]));
                model.setPrecio_uni_sva10(StringUtils.trim(hdc[74]));
                model.setCantidad_sva10(StringUtils.trim(hdc[75]));
                model.setCode_sva10(StringUtils.trim(hdc[76]));
                model.setAceptacion_recibo_digital(StringUtils.trim(hdc[77]));
                model.setAceptacion_proteccion_datos(StringUtils.trim(hdc[78]));
                model.setFiltro_web_parental(StringUtils.trim(hdc[79]));
                model.setModalidad_pago(StringUtils.trim(hdc[80]));
                model.setMonto_contado(StringUtils.trim(hdc[81]));
                model.setMeses_devolucion(StringUtils.trim(hdc[82]));
                model.setEmail_cliente(StringUtils.trim(hdc[83]));
                model.setId_transaccion(StringUtils.trim(hdc[84]));
                model.setCosto_instalacion(StringUtils.trim(hdc[85]));
                model.setEquipamiento_tv(StringUtils.trim(hdc[86]));
                model.setEquipamiento_internet(StringUtils.trim(hdc[87]));
                model.setEquipamiento_linea(StringUtils.trim(hdc[88]));
                model.setTecnologia_internet(StringUtils.trim(hdc[89]));
                model.setTecnologia_tv(StringUtils.trim(hdc[90]));

                model.setPeriodo_retorno(StringUtils.trim(hdc[91]));
                model.setVelocidad_internet(StringUtils.trim(hdc[92]));
                model.setEnvio_contrato(StringUtils.trim(hdc[93]));
                model.setDesafiliacion_pb(StringUtils.trim(hdc[94]));
                model.setTipo_registro(StringUtils.trim(hdc[95]));
                model.setMeses_financiamiento(StringUtils.trim(hdc[96]));


                if (model.getBack() != null && back_app.toLowerCase().contains(model.getBack().toLowerCase()) &&
                        model.getId_grabacion() != null &&
                        model.getId_grabacion().startsWith("-")) {
                    model.setId_visor(model.getId_grabacion());
//					logger.info("procesando registro APP VF");
                }

                if (model.getBack() != null && back_app.toLowerCase().contains(model.getBack().toLowerCase()) &&
                        model.getId_grabacion() != null &&
                        model.getId_grabacion().startsWith("MT")) {
                    model.setId_visor(model.getId_grabacion());
                    //Update Tabla mt_azure_file_storage
                    OrdenMT ventaMT = ordenMTRepository.searchOrdenMt(model.getId_visor());
                    if (ventaMT != null) {
                        if (ventaMT.isRemote() && ventaMT.getModeRetail() == null) {
                            MTAzureFileStorage getVentaFile = mtAzureFileStorageRepository.getMtOrderFile(ventaMT.getIdFijaOrder());
                            if (getVentaFile != null) {
                                mtAzureFileStorageRepository.updateMTAzureFileStorage(model.getEstado_solicitud(), model.getCodigo_pedido(), getVentaFile.getMtOrder());
                            } else {
                                MTAzureBlobStorage getVentaBlob = mtAzureBlobStorageRepository.getMtOrderBlob(ventaMT.getIdFijaOrder());
                                if (getVentaBlob != null) {
                                    mtAzureBlobStorageRepository.updateMTAzureBlobStorage(model.getEstado_solicitud(), model.getCodigo_pedido(), getVentaBlob.getMtOrder());
                                }
                            }
                        } else if (ventaMT.isRemote() && ventaMT.getModeRetail().equals("1")) {
                            List<MTAzureFileStorage> getVentaList = mtAzureFileStorageRepository.getMtOrderFileRetail(ventaMT.getIdFijaOrder());
                            if (getVentaList.size() > 0) {
                                for (MTAzureFileStorage retail : getVentaList) {
                                    mtAzureFileStorageRepository.updateMTAzureFileStorage(model.getEstado_solicitud(), model.getCodigo_pedido(), retail.getMtOrder());
                                }
                            }
                        }
                    }
                }

                if ("fisico".equalsIgnoreCase(model.getEnvio_contrato())) model.setEnvio_contrato("F");
                else if ("email".equalsIgnoreCase(model.getEnvio_contrato())) model.setEnvio_contrato("M");
                else model.setEnvio_contrato("");

                Integer operationNumber = 0;
                if (model.getOperacion_comercial() != null && model.getTipo_registro() != null/* && order.getCampaign() != null*/) {
				/*if (model.getOperacion_comercial().equalsIgnoreCase("Alta Pura")
						&& model.getTipo_registro().equalsIgnoreCase("ATIS")
						&& model.getCampaign().toUpperCase().equalsIgnoreCase("CIUDAD SITIADA")) {
					operationNumber = 3;
				} else */
                    if (model.getOperacion_comercial().equalsIgnoreCase("Alta Pura")
                            && model.getTipo_registro().equalsIgnoreCase("ATIS")) {
                        operationNumber = 1;
                    } else if (model.getOperacion_comercial().equalsIgnoreCase("Alta Pura")
                            && model.getTipo_registro().equalsIgnoreCase("CMS")) {
                        operationNumber = 6;
                    } else if (model.getOperacion_comercial().equalsIgnoreCase("SVAS")
                            && model.getTipo_registro().equalsIgnoreCase("ATIS")) {
                        operationNumber = 2;
                    } else if (model.getOperacion_comercial().equalsIgnoreCase("SVAS")
                            && model.getTipo_registro().equalsIgnoreCase("CMS")) {
                        operationNumber = 7;
                    } else if (model.getOperacion_comercial().equalsIgnoreCase("Migraciones")
                            || model.getOperacion_comercial().equalsIgnoreCase("Alta Componente TV")
                            || model.getOperacion_comercial().equalsIgnoreCase("Alta Componente BA")) {
                        operationNumber = 4;
                    } else {
                        operationNumber = 0;
                    }
                }

                model.setNumero_operacion(operationNumber);

                if (model.getDni() != null && model.getDni().length() > 12) {
                    syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_DNI_ERROR, line));
                    System.out.println(model.getId_visor());
                    System.out.println(String.format("dni %s %s", model.getDni(), model.getDni() != null ? model.getDni().length() : 0));
                } else {
                    if (model.getTelefono() != null && model.getTelefono().length() > 20) {
                        syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_TELEFONO_ERROR, line));
                        System.out.println(model.getId_visor());
                        System.out.println(String.format("telefono %s %s", model.getTelefono(), model.getTelefono() != null ? model.getTelefono().length() : 0));
                    } else {
                        if (model.getDistrito() != null && model.getDistrito().length() > 100) {
                            syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_DISTRITO_ERROR, line));
                            System.out.println(model.getId_visor());
                            System.out.println(line);
                            System.out.println(String.format("distrito %s %s", model.getDistrito(), model.getDistrito() != null ? model.getDistrito().length() : 0));
                        } else {
                            hdcs.add(model);
                        }
                    }
                }
            }
            in.close();
            fstream.close();
            SyncHdecResult result = dao.syncHDEC2(hdcs, fileNameTxt, syntaxErrors);

            Integer syntaxErrorCount = syntaxErrors.size();
            Integer versionFilaErrorCount = result.getErrorCount();

            result.setSyntaxErrors(syntaxErrors);
            result.setErrorCount(result.getErrorCount() + syntaxErrors.size());
            List<HdecResponseData> updatedInfo = result.getData();
            //		logger.info(String.format("Total registros leidos: %s, insertados: %s, actualizados: %s, errores: %s",
            //				hdcs.size(), result.getInsertCount(), result.getUpdateCount(), result.getErrorCount()));
            logger.info(String.format("Total registros leidos: %s, insertados: %s, actualizados: %s,actualizados order: %s, errores: %s",
                    hdcs.size(), result.getInsertCount(), result.getUpdateCount(), result.getOrderCount(), result.getErrorCount()));
            List<String> linesWithError = new ArrayList<>();
            for (SyncHdecSyntaxError error : syntaxErrors) {
                linesWithError.add(error.getLine());
            }
            //		if (!linesWithError.isEmpty()) {
            //			String errorFileName = String.format("VF_ERRORS_%s.txt", new Date().getTime());
            //			String filePath = remoteDirectory + errorFileName;
            //			File errorsFile = FileUtils.writeLines(linesWithError);
            //			ftpService.uploadFile(errorsFile, filePath);
            //			logger.info(String.format("Archivo de errores %s subido", errorFileName));
            //			if (errorsFile != null) {
            //				errorsFile.delete();
            //			}
            //		}
            logger.info("fin ThirdpartyService.processHdcFile .....................");
            //processAzure(updatedInfo);
            //		processUpFront(updatedInfo);

            if (syntaxErrorCount > 0 || versionFilaErrorCount > 0) {
                sendEmailOnError(fileNameTxt, syntaxErrorCount, versionFilaErrorCount);
            }
            success = true;
        } catch (SyncException ex) {
            sendEmailOnError(fileNameTxt, -1, -1);
        }
        return success;
    }

    public void sendEmailOnError(String txtName, Integer syntaxErrorCount, Integer versionFilaErrorCount) {
        //Runnable task = () -> {
        try {
					/*String body = "";

					if(syntaxErrorCount != 0 || versionFilaErrorCount != 0){
						body = body + "El archivo " + txtName + " contiene los siguientes errores: \n";
						if(syntaxErrorCount > 0) body = body + " - Errores de sintaxis (filas excluidas por sintaxis)";
						if(versionFilaErrorCount > 0) body = body + " - Errores en Versión de Fila (filas excluidas por campo version fila incorrecto)";

						if(syntaxErrorCount == -1 && versionFilaErrorCount == -1) body = body + " - Error en txt (el archivo no fue procesado)";

						mailing.sendMailToReceivers(body, mailing.subjectTxtWithError);
					}*/

            mailing.sendMailToReceivers(mailing.bodyTxtWithError, mailing.subjectTxtWithError);

        } catch (Exception ex) {
            logger.info("Error send mail: " + ex.getMessage());
        }
        //};
        //taskExecutor.execute(task);
    }

    public void sendEmailOnTxtNotFound() {
        //Runnable task = () -> {
        try {
            mailing.sendMailToReceivers(mailing.bodyTxtNotFound, mailing.subjectTxtNotFound);
        } catch (Exception ex) {
            logger.info("Error send mail: " + ex.getMessage());
        }
        //};
        //taskExecutor.execute(task);
    }

    public void processDevComercialFile(File file) throws IOException, SyncException {
        logger.info("ThirdpartyService.processDevComercialFile .........");
        List<DevComercialResponseData> devcomList = new ArrayList<>();
        List<SyncHdecSyntaxError> syntaxErrors = new ArrayList<>();

        FileInputStream fstream = new FileInputStream(file);

        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        String line;
        String cvsSplitBy = "\\|";

        boolean first = true;
        while ((line = br.readLine()) != null) {
            if (first) {
                first = false;
                continue;
            }
            line = line + " ";
            String[] devcom = line.split(cvsSplitBy);
            if (devcom.length < 34) {
                logger.info("error al procesar linea " + line);
                syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_INCOMPLETE_LINE, line));
                continue;
            }
            DevComercialResponseData model = new DevComercialResponseData();

            model.setGestion(devcom[0] != null ? StringUtils.trim(devcom[0]) : "");
            model.setRequerimiento(devcom[3] != null ? StringUtils.trim(devcom[3]) : "");
            model.setDesmotv(devcom[7] != null ? StringUtils.trim(devcom[7]) : "");
            model.setDes_mot(devcom[14] != null ? StringUtils.trim(devcom[14]) : "");
            model.setDet_mot(devcom[15] != null ? StringUtils.trim(devcom[15]) : "");
            model.setNomb_tco(devcom[16] != null ? StringUtils.trim(devcom[16]) : "");
            model.setTematico1(devcom[21] != null ? StringUtils.trim(devcom[21]) : "");
            model.setTematico2(devcom[22] != null ? StringUtils.trim(devcom[22]) : "");
            model.setTematico3(devcom[23] != null ? StringUtils.trim(devcom[23]) : "");
            model.setTematico4(devcom[24] != null ? StringUtils.trim(devcom[24]) : "");
            model.setFecagen(devcom[25] != null ? StringUtils.trim(devcom[25]) : "");
            model.setHoraagen(devcom[26] != null ? StringUtils.trim(devcom[26]) : "");
            model.setObsgest(devcom[27] != null ? StringUtils.trim(devcom[27]) : "");
            model.setFecha_carga(devcom[28] != null ? StringUtils.trim(devcom[28]) : "");
            model.setFecha_gestion(devcom[30] != null ? StringUtils.trim(devcom[30]) : "");
            model.setSitua(devcom[32] != null ? StringUtils.trim(devcom[32]) : "");
            model.setUsu_ges(devcom[33] != null ? StringUtils.trim(devcom[33]) : "");

			/*model.setRequerimiento( devcom[0] != null ? StringUtils.trim(devcom[0]) : "" );
			model.setFecha_gestion(devcom[1] != null ? StringUtils.trim(devcom[1]) : "");
			//model.setRespon(devcom[2] != null ? StringUtils.trim(devcom[2]) : "");
			model.setGestion(devcom[2] != null ? StringUtils.trim(devcom[2]) : "");
			model.setActividad(devcom[3] != null ? StringUtils.trim(devcom[3]) : "");
			model.setTematico_1(devcom[4] != null ? StringUtils.trim(devcom[4]) : "");
			model.setTematico_2(devcom[5] != null ? StringUtils.trim(devcom[5]) : "");
			model.setTematico_3(devcom[6] != null ? StringUtils.trim(devcom[6]) : "");
			model.setTematico_4(devcom[7] != null ? StringUtils.trim(devcom[7]) : "");
			model.setObs_gestion(devcom[8] != null ? StringUtils.trim(devcom[8]) : "");
			//model.setResponsable(devcom[10] != null ? StringUtils.trim(devcom[10]) : "");
			model.setFecha_carga(devcom[9] != null ? StringUtils.trim(devcom[9]) : "");
			model.setFecha_sms(devcom[10] != null ? StringUtils.trim(devcom[10]) : "");
			model.setSituacion(devcom[11] != null ? StringUtils.trim(devcom[11]) : "");
			model.setZonal(devcom[12] != null ? StringUtils.trim(devcom[12]) : "");
			model.setJefatura(devcom[13] != null ? StringUtils.trim(devcom[13]) : "");
			model.setNodo(devcom[14] != null ? StringUtils.trim(devcom[14]) : "");
			model.setMotivo_des(devcom[15] != null ? StringUtils.trim(devcom[15]) : "");
			model.setMotivo_det(devcom[16] != null ? StringUtils.trim(devcom[16]) : "");
			model.setNombre_tecnico(devcom[17] != null ? StringUtils.trim(devcom[17]) : "");
			model.setCelular_tecnico(devcom[18] != null ? StringUtils.trim(devcom[18]) : "");
			model.setArea(devcom[19] != null ? StringUtils.trim(devcom[19]) : "");*/

            devcomList.add(model);

        }
        in.close();
        fstream.close();
        SyncDevComResult result = dao.syncDevComercial(devcomList);
        result.setSyntaxErrors(syntaxErrors);
        result.setErrorCount(result.getErrorCount() + syntaxErrors.size());
        List<DevComercialResponseData> updatedInfo = result.getData();
//		logger.info(String.format("Total registros leidos: %s, insertados: %s, actualizados: %s, errores: %s",
//				hdcs.size(), result.getInsertCount(), result.getUpdateCount(), result.getErrorCount()));
        logger.info(String.format("Total registros leidos: %s, insertados: %s, actualizados: %s,actualizados order: %s, errores: %s",
                devcomList.size(), result.getInsertCount(), result.getUpdateCount(), result.getOrderCount(), result.getErrorCount()));
        List<String> linesWithError = new ArrayList<>();
        for (SyncHdecSyntaxError error : syntaxErrors) {
            linesWithError.add(error.getLine());
        }
//		if (!linesWithError.isEmpty()) {
//			String errorFileName = String.format("VF_ERRORS_%s.txt", new Date().getTime());
//			String filePath = remoteDirectory + errorFileName;
//			File errorsFile = FileUtils.writeLines(linesWithError);
//			ftpService.uploadFile(errorsFile, filePath);
//			logger.info(String.format("Archivo de errores %s subido", errorFileName));
//			if (errorsFile != null) {
//				errorsFile.delete();
//			}
//		}
        logger.info("fin ThirdpartyService.processDevComercialFile .....................");
        //processAzure(updatedInfo);
//		processUpFront(updatedInfo);
    }

    public void processDevTecnicaFile(File file) throws IOException, SyncException {
        logger.info("ThirdpartyService.processDevComercialFile .........");
        List<DevTecnicaResponseData> devcomList = new ArrayList<>();
        List<SyncHdecSyntaxError> syntaxErrors = new ArrayList<>();

        FileInputStream fstream = new FileInputStream(file);

        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));

        String line;
        String cvsSplitBy = "\\|"; //"\t";

        int errorCount = 0;

        //boolean first = true;
        while ((line = br.readLine()) != null) {
            //if(first){first = false; continue;}
            line = line + " ";
            String[] devcom = line.split(cvsSplitBy);
            if (devcom.length < 43) {
                errorCount++;
                logger.info("error al procesar linea " + line);
                syntaxErrors.add(new SyncHdecSyntaxError(SyncHdecSyntaxError.CAUSE_INCOMPLETE_LINE, line));
                continue;
            }

			/*boolean validated = false;
			try{
				String codigo_motivo = devcom[3] != null ? StringUtils.trim(devcom[3]) : "-1";
				Integer codigo_motivo_int = Integer.parseInt(codigo_motivo);
				if(codigo_motivo_int > 0 && codigo_motivo_int < 15) validated = true;
			}catch (Exception ex){}

			if(!validated) continue;*/

            DevTecnicaResponseData model = new DevTecnicaResponseData();

            model.setNumero_peticion(devcom[0] != null ? StringUtils.trim(devcom[0]) : "");
            model.setId(devcom[1] != null ? StringUtils.trim(devcom[1]) : "");
            model.setId_motivo(devcom[2] != null ? StringUtils.trim(devcom[2]) : "");
            model.setCodigo_motivo(devcom[3] != null ? StringUtils.trim(devcom[3]) : "");
            model.setMotivo(devcom[4] != null ? StringUtils.trim(devcom[4]) : "");
            model.setSubmotivo(devcom[5] != null ? StringUtils.trim(devcom[5]) : "");
            model.setDescripcion(devcom[6] != null ? StringUtils.trim(devcom[6]) : "");
            model.setNombre(devcom[7] != null ? StringUtils.trim(devcom[7]) : "");
            model.setNumero_cel(devcom[8] != null ? StringUtils.trim(devcom[8]) : "");
            model.setCarnet(devcom[9] != null ? StringUtils.trim(devcom[9]) : "");
            model.setArea_gestion(devcom[10] != null ? StringUtils.trim(devcom[10]) : "");
            model.setEECC(devcom[11] != null ? StringUtils.trim(devcom[11]) : "");
            model.setTematico_1(devcom[12] != null ? StringUtils.trim(devcom[12]) : "");
            model.setTematico_2(devcom[13] != null ? StringUtils.trim(devcom[13]) : "");
            model.setTematico_3(devcom[14] != null ? StringUtils.trim(devcom[14]) : "");
            model.setComentario(devcom[15] != null ? StringUtils.trim(devcom[15]) : "");
            model.setFecha_ingreso(devcom[16] != null ? StringUtils.trim(devcom[16]) : "");
            model.setFecha_liquidacion(devcom[17] != null ? StringUtils.trim(devcom[17]) : "");
            model.setFecha_proceso(devcom[18] != null ? StringUtils.trim(devcom[18]) : "");
            model.setEstado(devcom[19] != null ? StringUtils.trim(devcom[19]) : "");
            model.setUsuario(devcom[20] != null ? StringUtils.trim(devcom[20]) : "");

            model.setSupervision(devcom[21] != null ? StringUtils.trim(devcom[21]) : "");
            model.setPerfil(devcom[22] != null ? StringUtils.trim(devcom[22]) : "");
            model.setZonal(devcom[23] != null ? StringUtils.trim(devcom[23]) : "");
            model.setJefatura(devcom[24] != null ? StringUtils.trim(devcom[24]) : "");
            model.setMov(devcom[25] != null ? StringUtils.trim(devcom[25]) : "");
            model.setNodo(devcom[26] != null ? StringUtils.trim(devcom[26]) : "");
            model.setTelf_1(devcom[27] != null ? StringUtils.trim(devcom[27]) : "");
            model.setTelf_2(devcom[28] != null ? StringUtils.trim(devcom[28]) : "");
            model.setWin_telf_1(devcom[29] != null ? StringUtils.trim(devcom[29]) : "");
            model.setWin_telf_2(devcom[30] != null ? StringUtils.trim(devcom[30]) : "");
            model.setWin_telf_3(devcom[31] != null ? StringUtils.trim(devcom[31]) : "");
            model.setContra(devcom[32] != null ? StringUtils.trim(devcom[32]) : "");
            model.setNegocio(devcom[33] != null ? StringUtils.trim(devcom[33]) : "");
            model.setDato_filtro(devcom[34] != null ? StringUtils.trim(devcom[34]) : "");
            model.setWin_back(devcom[35] != null ? StringUtils.trim(devcom[35]) : "");

            model.setUp_front(devcom[36] != null ? StringUtils.trim(devcom[36]) : "");
            model.setRegion(devcom[37] != null ? StringUtils.trim(devcom[37]) : "");
            model.setCod_trtrn(devcom[38] != null ? StringUtils.trim(devcom[38]) : "");
            model.setCod_lex(devcom[39] != null ? StringUtils.trim(devcom[39]) : "");
            model.setCod_tap(devcom[40] != null ? StringUtils.trim(devcom[40]) : "");
            model.setCod_bor(devcom[41] != null ? StringUtils.trim(devcom[41]) : "");

            devcomList.add(model);

        }
        logger.info("Total de registros con errores: " + errorCount);
        in.close();
        fstream.close();
        SyncDevTecResult result = dao.syncDevTecnica(devcomList);
        result.setSyntaxErrors(syntaxErrors);
        result.setErrorCount(result.getErrorCount() + syntaxErrors.size());
        List<DevTecnicaResponseData> updatedInfo = result.getData();

        logger.info(String.format("Total registros leidos: %s, insertados: %s, actualizados: %s,actualizados order: %s, errores: %s",
                devcomList.size(), result.getInsertCount(), result.getUpdateCount(), result.getOrderCount(), result.getErrorCount()));
        List<String> linesWithError = new ArrayList<>();
        for (SyncHdecSyntaxError error : syntaxErrors) {
            linesWithError.add(error.getLine());
        }

        logger.info("fin ThirdpartyService.processDevTecnicaFile .....................");

    }

    public void processUpFront(List<HdecResponseData> data) {
        if (data == null || data.isEmpty()) {
            return;
        }
        List<String> ids = new ArrayList<>();
        for (HdecResponseData model : data) {
            if (model.getId_visor() != null && model.getId_visor().startsWith("-") && "INGRESADO".equals(model.getEstado_solicitud())) {
                ids.add(model.getId_visor());
            }
        }

        processUpFrontById(ids);
    }

    public void processUpFrontById(List<String> ids) {
        if (ids.isEmpty()) {
            return;
        }
        List<UpFrontRequestBody> upFrontToSend = dao.retrieveUpFrontCandidates(ids);
        UpFrontClient client = new UpFrontClient(apiHeaderConfig);

        for (UpFrontRequestBody request : upFrontToSend) {
            try {
                UpFrontResponseBody response = client.sendData(request);
                String codigoCIP = response.getPagoEfectivoCodigoCIP();
                String phoneNumber = request.getNumeroContacto();
                twilioService.sendSms(phoneNumber, String.format("Por favor, cancele con el codigo %s.", codigoCIP));
            } catch (ApiClientException e) {
                logger.error("Error al enviar upFront", e);
            }
        }
    }

    public void processAzure(List<HdecResponseData> data) throws SyncException {
        logger.info("ThirdpartyService.processAzure..........");
        if (data == null || data.isEmpty()) {
            return;
        }
        List<AzureRequest> requestList = new ArrayList<>();
        for (HdecResponseData model : data) {
            if (model.getId_visor() != null && model.getId_visor().startsWith("-")
                    && ("INGRESADO".equals(model.getEstado_solicitud())
                    || "CAIDA".equals(model.getEstado_solicitud()) ||
                    "NO INGRESADO".equals(model.getEstado_solicitud()))) {
                AzureRequest arq = new AzureRequest();
                arq.setKey(model.getId_visor());
                arq.setStatus(model.getEstado_solicitud());
                requestList.add(arq);
            }
        }

        // TODO: Change process here so that it saves it on the database
        //dao.flagOrderForAzure(requestList);
        logger.info("fin ThirdpartyService.processAzure..........");
//		for (AzureRequest request : requestList) {
//			try {
//				sendAzure(request);
//			} catch (Exception e) {
//				logger.error("error enviando a azure", e);
//			}
//		}
    }

    public Response<String> sendAzure(AzureRequest request) {
        Response<String> response = new Response<>();
        AzureResponseData metadata;
        String imagenExtension = ".pdf";
        String audioExtension = ".gsm";
        String excelExtension = ".xlsx";
        String documentDate;
        String azureDate;
        try {
            metadata = dao.getMetadata(request);
            if (metadata == null || metadata.getId() == null || "".equals(metadata.getId().trim())) {
                response.setResponseCode("-3");
                response.setResponseMessage("No se pudo obtener metadata.");
                return response;
            }
            documentDate = new SimpleDateFormat("ddMMyyyy").format(metadata.getDocumentDate());
            logger.info("DocumentDate:" + metadata.getDocumentDate());
            logger.info("ZonedDate:" + Date.from(ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis")).toInstant()));
            if (metadata.getDocumentDate().before(Date.from(ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis")).toInstant()))) {
                //azureDate=ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis")).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
                azureDate = ZonedDateTime.now().format(DateTimeFormatter.ofPattern("ddMMyyyy"));
            } else {
                //azureDate=ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis")).plusDays(1).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
                azureDate = ZonedDateTime.now().plusDays(1).format(DateTimeFormatter.ofPattern("ddMMyyyy"));
            }
            logger.info("order:" + documentDate + "|azure:" + azureDate);

        } catch (Exception e) {
            logger.error("No se pudo obtener metadata.", e);
            response.setResponseCode("-3");
            response.setResponseMessage("No se pudo obtener metadata.");
            return response;
        }
        String excelFilename = azureDate + excelExtension;
        String audioFilename = request.getKey() + audioExtension;
        String imagenFilename = metadata.getDni() + documentDate + imagenExtension;

        try {
            String audioFirebase = String.format("%s/audios/%s_gsm.zip", request.getKey(), request.getKey());
            File audioFile = gss.retriveFileFromZip(audioFirebase, audioFilename);
            FileInputStream audioFileContent = new FileInputStream(audioFile);
            long fileSize = audioFile.length();

            File excelLegacy = new File(excelFilename);
            File excelNoLegacy = new File(excelFilename);

            String imagenFirebase = String.format("%s/images/%s.pdf", request.getKey(), request.getKey());
            File imagenFile = gss.retrieveFile(imagenFirebase);
            FileInputStream imagenFileContent = new FileInputStream(imagenFile);
            long imagenFileSize = imagenFile.length();

            CloudBlobContainer containerNoLegacy = createBlobContainer(azureConnectionNoLegacy, azureDate);
            Iterable<ListBlobItem> listNoLegacy = containerNoLegacy.listBlobs();
            CloudBlob azureImagenNoLegacy = containerNoLegacy.getBlockBlobReference(imagenFilename);
            CloudBlob azureExcelNoLegacy = containerNoLegacy.getBlockBlobReference(excelFilename);

            if ("INGRESADO".equalsIgnoreCase(request.getStatus())) {
                CloudBlobContainer containerLegacy = createBlobContainer(azureConnectionLegacy, azureDate);
                Iterable<ListBlobItem> listLegacy = containerLegacy.listBlobs();
                CloudBlob azureAudioLegacy = containerLegacy.getBlockBlobReference(audioFilename);
                CloudBlob azureExcelLegacy = containerLegacy.getBlockBlobReference(excelFilename);

                if (!listNoLegacy.iterator().hasNext()) {
                    logger.info("Creating Excel!!!.");
                    metadataCreateNoLegacy(excelNoLegacy, metadata, imagenFilename);
                    azureExcelNoLegacy.uploadFromFile(excelFilename);
                } else {
                    logger.info("Updating Excel!!!.");
                    azureExcelNoLegacy.downloadToFile(excelFilename);
                    metadataUpdateNoLegacy(excelNoLegacy, metadata, imagenFilename);
                    azureExcelNoLegacy.uploadFromFile(excelFilename);
                }
                azureImagenNoLegacy.upload(imagenFileContent, imagenFileSize);

                if (!listLegacy.iterator().hasNext()) {
                    logger.info("Creating Excel!!!.");
                    metadataCreateLegacy(excelLegacy, metadata, audioFilename);
                    azureExcelLegacy.uploadFromFile(excelFilename);
                } else {
                    logger.info("Updating Excel!!!.");
                    azureExcelLegacy.downloadToFile(excelFilename);
                    metadataUpdateLegacy(excelLegacy, metadata, audioFilename);
                    azureExcelLegacy.uploadFromFile(excelFilename);
                }
                azureAudioLegacy.upload(audioFileContent, fileSize);

            } else {
                CloudBlob azureAudioNoLegacy = containerNoLegacy.getBlockBlobReference(audioFilename);
                if (!listNoLegacy.iterator().hasNext()) {
                    logger.info("Creating Excel!!!.");
                    metadataCreateNoLegacy(excelNoLegacy, metadata, imagenFilename);
                    metadataUpdateNoLegacy(excelNoLegacy, metadata, audioFilename);
                    azureExcelNoLegacy.uploadFromFile(excelFilename);
                } else {
                    logger.info("Updating Excel!!!.");
                    azureExcelNoLegacy.downloadToFile(excelFilename);
                    metadataUpdateNoLegacy(excelNoLegacy, metadata, imagenFilename);
                    metadataUpdateNoLegacy(excelNoLegacy, metadata, audioFilename);
                    azureExcelNoLegacy.uploadFromFile(excelFilename);
                }
                azureImagenNoLegacy.upload(imagenFileContent, imagenFileSize);
                azureAudioNoLegacy.upload(audioFileContent, fileSize);
            }

            if (excelLegacy.delete()) {
                logger.info(excelLegacy.getName() + " is deleted!");
            } else {
                logger.info("Delete operation is failed.");
            }

            audioFileContent.close();
            if (audioFile.delete()) {
                logger.info(audioFile.getName() + " is deleted!");
            } else {
                logger.info("Delete operation is failed.");
            }

            imagenFileContent.close();
            if (imagenFile.delete()) {
                logger.info(imagenFile.getName() + " is deleted!");
            } else {
                logger.info("Delete operation is failed.");
            }

        } catch (FileNotFoundException e) {
            logger.info("FileNotFoundException." + e);
            response.setResponseCode("-2");
            response.setResponseMessage("No se encontro el archivo.");
            return response;
        } catch (Exception e) {
            logger.info("Exception SendAzure Service." + e);
            response.setResponseCode("-1");
            response.setResponseMessage("Error al enviar a Azure.");
            return response;
        }

        response.setResponseCode("0");
        response.setResponseMessage("OK");
        return response;
    }

    private CloudBlobContainer createBlobContainer(String azureConnection, String directoryName) {
        CloudStorageAccount account;
        try {
            account = CloudStorageAccount.parse(azureConnection);
        } catch (Exception e) {
            logger.info("Error en Cuenta." + e);
            return null;
        }
        CloudBlobClient client = account.createCloudBlobClient();

        CloudBlobContainer container;
        try {
            container = client.getContainerReference(directoryName);
        } catch (Exception e) {
            logger.info("Error Compartido." + e);
            return null;
        }

        try {
            if (container.createIfNotExists()) {
                logger.info("New container created");
            }
        } catch (Exception e) {
            logger.info("Error Crear Compartido." + e);
        }

        return container;
    }

    private void metadataCreateLegacy(File excelFile, AzureResponseData metadata, String audioFilename) {
        try (FileOutputStream out = new FileOutputStream(excelFile); XSSFWorkbook workbook = new XSSFWorkbook();) {

            XSSFSheet sheet = workbook.createSheet("Metadata");
            XSSFFont font = workbook.createFont();
            font.setBold(true);
            font.setColor(IndexedColors.WHITE.index);
            XSSFCellStyle style = workbook.createCellStyle();
            style.setFont(font);
            style.setFillForegroundColor(IndexedColors.BLUE_GREY.index);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            Map<String, Object[]> data = new HashMap<>();
            data.put("0",
                    new Object[]{"ID_GRABACION", "IDENTIFICADOR_UNICO", "CANAL_VENTA_PROV", "NUMERO_CELULAR_PROV",
                            "NUMERO_LINEA_PROV", "FECHA_DOCUMENTO_PROV", "NOMBRE_PROVEEDOR_PROV", "NOMBRE_CAMPA�A_PROV",
                            "NUMERO_DOCUMENTO_PROV", "NOMBRE_CLIENTE_PROV", "NEGOCIO_PROV",
                            "DESAFILIACION_DE_PAGINAS_BLANCAS_PROV", "AFILIACION_FACTURA_DIGITAL_PROV",
                            "PROTECCION_DE_DATOS_PROV"});
            data.put("1",
                    new Object[]{audioFilename, metadata.getId(), metadata.getChannel(), metadata.getPhoneNumber(),
                            metadata.getTelephoneNumber(), new SimpleDateFormat("dd/MM/yyyy").format(metadata.getDocumentDate()), metadata.getProviderName(),
                            metadata.getCampaignName(), metadata.getDocumentNumber(), metadata.getCustomerName(),
                            metadata.getBusiness(), metadata.getDisableWhitePage(), metadata.getEnableDigitalInvoice(),
                            metadata.getDataProtection()});

            Set<String> keyset = data.keySet();
            int rownum = 0;
            for (String key : keyset) {
                Row row = sheet.createRow(rownum++);
                Object[] objArr = data.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum);
                    if (obj instanceof Date)
                        cell.setCellValue((Date) obj);
                    else if (obj instanceof Boolean)
                        cell.setCellValue((Boolean) obj);
                    else if (obj instanceof String)
                        cell.setCellValue((String) obj);
                    else if (obj instanceof Double)
                        cell.setCellValue((Double) obj);
                    else if (obj instanceof Integer)
                        cell.setCellValue((Integer) obj);

                    if ("0".equals(key)) {
                        cell.setCellStyle(style);
                    } else {
                        sheet.autoSizeColumn(cellnum);
                    }
                    cellnum++;
                }
            }

            workbook.write(out);
            logger.info("Excel created successfully..");

        } catch (FileNotFoundException e) {
            logger.info("FileNotFoundException createExcel." + e);
        } catch (IOException e) {
            logger.info("IOException createExcel." + e);
        }

    }

    private void metadataUpdateLegacy(File excelFile, AzureResponseData metadata, String audioFilename) {
        try (FileInputStream fis = new FileInputStream(excelFile);
             XSSFWorkbook workbook = new XSSFWorkbook(fis);
             FileOutputStream out = new FileOutputStream(excelFile);) {
            XSSFSheet sheet = workbook.getSheet("Metadata");

            int lastRow = sheet.getLastRowNum() + 1;
            XSSFRow newRow = sheet.createRow(lastRow);

            Object[] objArr = new Object[]{audioFilename, metadata.getId(), metadata.getChannel(),
                    metadata.getPhoneNumber(), metadata.getTelephoneNumber(), new SimpleDateFormat("dd/MM/yyyy").format(metadata.getDocumentDate()),
                    metadata.getProviderName(), metadata.getCampaignName(), metadata.getDocumentNumber(),
                    metadata.getCustomerName(), metadata.getBusiness(), metadata.getDisableWhitePage(),
                    metadata.getEnableDigitalInvoice(), metadata.getDataProtection()};
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = newRow.createCell(cellnum++);
                if (obj instanceof Date)
                    cell.setCellValue((Date) obj);
                else if (obj instanceof Boolean)
                    cell.setCellValue((Boolean) obj);
                else if (obj instanceof String)
                    cell.setCellValue((String) obj);
                else if (obj instanceof Double)
                    cell.setCellValue((Double) obj);
                else if (obj instanceof Integer)
                    cell.setCellValue((Integer) obj);
            }

            fis.close();

            workbook.write(out);
            logger.info("Excel updated successfully..");

        } catch (IOException e) {
            logger.info("IOException updateExcel." + e);
        } catch (Exception e) {
            logger.info("Exception updateExcel." + e);
        }

    }

    private void metadataCreateNoLegacy(File excelFile, AzureResponseData metadata, String filename) {
        try (FileOutputStream out = new FileOutputStream(excelFile); XSSFWorkbook workbook = new XSSFWorkbook();) {

            XSSFSheet sheet = workbook.createSheet("Metadata");
            XSSFFont font = workbook.createFont();
            font.setBold(true);
            font.setColor(IndexedColors.WHITE.index);
            XSSFCellStyle style = workbook.createCellStyle();
            style.setFont(font);
            style.setFillForegroundColor(IndexedColors.BLUE_GREY.index);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

            Map<String, Object[]> data = new HashMap<>();
            data.put("0",
                    new Object[]{"NOMBRE_ARCHIVO", "IDENTIFICADOR_UNICO", "CANAL_VENTA", "NUMERO_CELULAR",
                            "NUMERO_LINEA", "FECHA_DOCUMENTO", "NOMBRE_PROVEEDOR", "NOMBRE_CAMPA�A", "NUMERO_DOCUMENTO",
                            "NOMBRE_CLIENTE", "NEGOCIO", "DESAFILIACION_DE_PAGINAS_BLANCAS",
                            "AFILIACION_FACTURA_DIGITAL", "PROTECCION_DE_DATOS", "DETALLE_PROVEEDOR", "TIPO_ARCHIVO"});
            data.put("1",
                    new Object[]{filename, metadata.getId(), metadata.getChannel(), metadata.getPhoneNumber(),
                            metadata.getTelephoneNumber(), new SimpleDateFormat("dd/MM/yyyy").format(metadata.getDocumentDate()), metadata.getProviderName(),
                            metadata.getCampaignName(), metadata.getDocumentNumber(), metadata.getCustomerName(),
                            metadata.getBusiness(), metadata.getDisableWhitePage(), metadata.getEnableDigitalInvoice(),
                            metadata.getDataProtection(), metadata.getProviderDetail(), filename.endsWith(".pdf") ? "FISICO" : "AUDIO"});

            Set<String> keyset = data.keySet();
            int rownum = 0;
            for (String key : keyset) {
                Row row = sheet.createRow(rownum++);
                Object[] objArr = data.get(key);
                int cellnum = 0;
                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum);
                    if (obj instanceof Date)
                        cell.setCellValue((Date) obj);
                    else if (obj instanceof Boolean)
                        cell.setCellValue((Boolean) obj);
                    else if (obj instanceof String)
                        cell.setCellValue((String) obj);
                    else if (obj instanceof Double)
                        cell.setCellValue((Double) obj);
                    else if (obj instanceof Integer)
                        cell.setCellValue((Integer) obj);

                    if ("0".equals(key)) {
                        cell.setCellStyle(style);
                    } else {
                        sheet.autoSizeColumn(cellnum);
                    }
                    cellnum++;
                }
            }

            workbook.write(out);
            logger.info("Excel created successfully..");

        } catch (FileNotFoundException e) {
            logger.info("FileNotFoundException createExcel." + e);
        } catch (IOException e) {
            logger.info("IOException createExcel." + e);
        }

    }

    private void metadataUpdateNoLegacy(File excelFile, AzureResponseData metadata, String filename) {
        try (FileInputStream fis = new FileInputStream(excelFile);
             XSSFWorkbook workbook = new XSSFWorkbook(fis);
             FileOutputStream out = new FileOutputStream(excelFile);) {
            XSSFSheet sheet = workbook.getSheet("Metadata");

            int lastRow = sheet.getLastRowNum() + 1;
            XSSFRow newRow = sheet.createRow(lastRow);

            Object[] objArr = new Object[]{filename, metadata.getId(), metadata.getChannel(),
                    metadata.getPhoneNumber(), metadata.getTelephoneNumber(), new SimpleDateFormat("dd/MM/yyyy").format(metadata.getDocumentDate()),
                    metadata.getProviderName(), metadata.getCampaignName(), metadata.getDocumentNumber(),
                    metadata.getCustomerName(), metadata.getBusiness(), metadata.getDisableWhitePage(),
                    metadata.getEnableDigitalInvoice(), metadata.getDataProtection(), metadata.getProviderDetail(),
                    filename.endsWith(".pdf") ? "FISICO" : "AUDIO"};
            int cellnum = 0;
            for (Object obj : objArr) {
                Cell cell = newRow.createCell(cellnum++);
                if (obj instanceof Date)
                    cell.setCellValue((Date) obj);
                else if (obj instanceof Boolean)
                    cell.setCellValue((Boolean) obj);
                else if (obj instanceof String)
                    cell.setCellValue((String) obj);
                else if (obj instanceof Double)
                    cell.setCellValue((Double) obj);
                else if (obj instanceof Integer)
                    cell.setCellValue((Integer) obj);
            }

            fis.close();

            workbook.write(out);
            logger.info("Excel updated successfully..");

        } catch (IOException e) {
            logger.info("IOException updateExcel." + e);
        } catch (Exception e) {
            logger.info("Exception updateExcel." + e);
        }

    }

    public String genCIP(Integer id) {
        LogSystem.info(logger, id + ":: GenCIP", "Inicio");

        BEGenRequest rq = dao.loadPagoEfectivoData(id);
        LogSystem.infoObject(logger, id + ":: GenCIP", "rq", rq);
        String CIP = null;
        if (rq != null) {

            LogSystem.infoString(logger, id + ":: GenCIP", "privateKeyLocation", privateKeyLocation);
            LogSystem.infoString(logger, id + ":: GenCIP", "publicKeyLocation", publicKeyLocation);
            LogSystem.infoString(logger, id + ":: GenCIP", "cryptoUrl", cryptoUrl);
            LogSystem.infoString(logger, id + ":: GenCIP", "servicesUrl", servicesUrl);
            LogSystem.infoString(logger, id + ":: GenCIP", "serviceCode", serviceCode);
            LogSystem.infoString(logger, id + ":: GenCIP", "proxyHostname", proxyHostname);
            LogSystem.infoString(logger, id + ":: GenCIP", "proxyPort", proxyPort);
            LogSystem.infoString(logger, id + ":: GenCIP", "proxyUser", proxyUser);
            LogSystem.infoString(logger, id + ":: GenCIP", "proxyPassword", proxyPassword);

            PagoEfectivo PE = new PagoEfectivo.Builder()
                    .setPrivateKey(privateKeyLocation)
                    .setPublicKey(publicKeyLocation)
                    .setCryptoUrl(cryptoUrl)
                    .setServicesUrl(servicesUrl)
                    .setCodigoServicio(serviceCode)
                    .setHostnameProxy(proxyHostname)
                    .setPortProxy(proxyPort)
                    .setUserProxy(proxyUser)
                    .setPasswordProxy(proxyPassword)
                    .build();

            BEpaymentResponse rs = getPagoEfectivo(id, rq, PE);
            LogSystem.infoObject(logger, id + ":: GenCIP", "rs", rs);
            if (rs != null) {
                CIP = rs.NumeroCip;

                dao.actualizarPagoEfectivoData(id, "EN", CIP);
            }
        }
        LogSystem.info(logger, id + ":: GenCIP", "Fin");
        return CIP;
    }

  //  @Scheduled(cron = "${calculator.api.sync.cron}")
    public void calcDiffSync() {
        if (!calcSyncEnabled) {
            logger.info("actualizacion calculadora no esta habilitada");
            return;
        }
        logger.info("Iniciando actualizacion calculadora");
        List<String> filesToProcess = ftpService.listFiles(remoteDirectoryCalc, false);

        for (String file : filesToProcess) {
            if (Pattern.matches(fileNameCalc + "\\d+\\" + TXT_EXT, file)) {
                logger.trace("archivo: " + file);
                String filePath = remoteDirectoryCalc + file;
                Response<String> response = syncCalculator(filePath, file);
                if (response.getResponseCode() == "0") {
                    ftpService.renameFile(remoteDirectoryCalc, file, String.format(newFileNameCalc + "_%s_%s", new Date().getTime(), file));
                }
            }
        }
    }

//	public Response<String> completeCalcSync() {
//		String filePath = remoteDirectoryCalc + fileNameCalc + TXT_EXT;
//		return sync(filePath);
//	}

    public boolean processCalculatorFile(File file, String fileName) throws IOException {

        List<CalculatorResponseData> calculatorRows = new ArrayList<CalculatorResponseData>();
        List<CalculatorResponseData> errorRows = new ArrayList<CalculatorResponseData>();

        FileReader f = new FileReader(file);
        BufferedReader b = new BufferedReader(f);

        String lineString;
        String txtSplitBy = "\t";
        int i = 0;
        int id = 0;
        int limitRows = 100000;
        boolean finish = false;
        boolean flagCalc = false;
        boolean flagOffers = false;
        String lastPhone = "";
        String lastService = "";
        String msgError = "";

        ArrayList<String> splitted = new ArrayList<String>();

        Date startDate = new Date();
        logger.info("Procesando archivo {} con saltos de {} @{}", file.getName(), limitRows, startDate);
        try {
            // OBTENER ID DE BD
            id = dao.getIDCalculator();
            while ((lineString = b.readLine()) != null) {

                if (i > 0) {

                    splitted.clear();
                    for (String item : lineString.split(txtSplitBy)) {
                        splitted.add(item);
                    }

                    if (splitted.size() == 11) {
                        splitted.add("");
                        splitted.add("");
                    } else if (splitted.size() == 12) {
                        splitted.add("");
                    }

                    CalculatorResponseData model = new CalculatorResponseData();

                    model.setTelefono(StringUtils.trim(splitted.get(0)));
                    model.setServicio(StringUtils.trim(splitted.get(1)));
                    model.setPaquetepsorigen(StringUtils.trim(splitted.get(2)));
                    model.setPaqueteorigen(StringUtils.trim(splitted.get(3)));
                    model.setCantidaddecos(StringUtils.trim(splitted.get(4)));
                    model.setRentatotal(StringUtils.trim(splitted.get(5)));
                    model.setInternet(StringUtils.trim(splitted.get(6)));

                    model.setPrioridad(StringUtils.trim(splitted.get(7)));
                    model.setProductodestinops(StringUtils.trim(splitted.get(8)));
                    model.setSvas(StringUtils.trim(splitted.get(9)));
                    model.setProductodestinonombre(StringUtils.trim(splitted.get(10)));
                    model.setMontofinanciado(StringUtils.trim(splitted.get(11)));
                    model.setCuotasfinanciado(StringUtils.trim(splitted.get(12)));
                    model.setCodigooferta(StringUtils.trim(splitted.get(13)));

                    model.setLineString("Fila " + i + ": " + lineString);
                    model.setFileName(StringUtils.trim(fileName));
                    msgError = validateCalculator(model);

                    if (msgError.length() > 0) {
                        model.setMsgError(msgError);
                        if (!errorRows.contains(model)) {
                            errorRows.add(model);
                        }
                        if (i % limitRows == 0) {
                            dao.syncCalc_Offers_Error(errorRows);
                            errorRows.clear();
                        }
                    } else {
                        if (i % limitRows == 0) {
                            // logger.info("limitRows: " + i);
                            flagOffers = true;
                        }
                        if (flagOffers) {
                            if (!model.getTelefono().trim().equals(lastPhone)
                                    || !model.getServicio().trim().equals(lastService)) {
                                flagCalc = true;
                            }
                        }
                        if (!lastPhone.equals(model.getTelefono().trim()) || !lastService.equals(model.getServicio().trim())) {
                            lastPhone = model.getTelefono().trim();
                            lastService = model.getServicio().trim();
                        }

                        if (flagOffers && flagCalc) {

                            flagOffers = false;
                            flagCalc = false;
                            lastPhone = "";
                            lastService = "";
                            Date jumpTime = new Date();
                            logger.info("Salto {} @{}", i / limitRows, jumpTime);

                            id = syncCalcl(calculatorRows, id);
                            calculatorRows.clear();
                            calculatorRows.add(model);

                            Date jumpTimeEnd = new Date();
                            logger.info("Salto {} finalizado @{} demoro {} segundos", i / limitRows, jumpTimeEnd, (jumpTimeEnd.getTime() - jumpTime.getTime()) / 1000);
                        } else {
                            calculatorRows.add(model);
                        }
                    }
                }
                i++;

            }
            if (errorRows.size() > 0) {
                dao.syncCalc_Offers_Error(errorRows);
                errorRows.clear();
            }
            if (calculatorRows.size() > 0) {
                syncCalcl(calculatorRows, id);
                calculatorRows.clear();
            }

            logger.info("processCalculatorFile finish");
            finish = true;

        } catch (IOException | SyncException ex) {
            logger.error("Error processCalculatorFile: " + ex.getMessage());
            errorCalculator(ex.getMessage(), fileName);
        } catch (Exception e) {
            logger.error("Error processCalculatorFile: " + e.getMessage(), e);
            errorCalculator(e.getMessage(), fileName);
        }
        b.close();
        return finish;
    }

    public void errorCalculator(String msg, String fileName) {
        try {
            dao.syncCalc_Offers_Error(msg, fileName);
        } catch (Exception e) {
            logger.error("errorCalculator: " + e.getMessage());
        }
    }

    public String validateCalculator(CalculatorResponseData model) {
        String telefono = model.getTelefono().trim();
        String servicio = model.getServicio().trim();
        String decos = model.getCantidaddecos().trim();
        String renta = model.getRentatotal().trim();
        String internet = model.getInternet().trim();
        String monto = model.getMontofinanciado().trim();
        String cuotas = model.getCuotasfinanciado().trim();

        String msg = "";

        if (telefono.equals("0") || telefono.equals("") && servicio.equals("0") || servicio.equals("")) {
            msg += " | Telefono y Servicio vacios";
        }
        if (!telefono.equals("0") || !telefono.equals("")) {
            try {
                int d = Integer.parseInt(telefono);
            } catch (NumberFormatException nfe) {
                msg += " | Telefono no es numerico";
            }
        }

        if (decos.equals("")) {
            msg += " | CantidadDecos campo vacio";
        } else {
            try {
                int i = Integer.parseInt(decos);
            } catch (NumberFormatException nfe) {
                msg += " | CantidadDecos no es numerico";
            }
        }

        if (renta.equals("")) {
            msg += " | RentaTotal campo vacio";
        } else {
            try {
                Double d = Double.parseDouble(renta);
            } catch (NumberFormatException nfe) {
                msg += " | RentaTotal no es numerico";
            }
        }

        if (internet.equals("")) {
            msg += " | Internet campo vacio";
        } else {
            try {
                int i = Integer.parseInt(internet);
            } catch (NumberFormatException nfe) {
                msg += " | Internet no es numerico";
            }
        }

        if (monto.equals("") && !cuotas.equals("")) {
            msg += " | No existe un monto para las CuotasFinanciado";
        } else if (!monto.equals("") && cuotas.equals("")) {
            msg += " | No existen cuotas para el MontoFinanciado";
        }

        if (!monto.equals("")) {
            try {
                Double d = Double.parseDouble(monto);
            } catch (NumberFormatException nfe) {
                msg += " | MontoFinanciado no es una cantidad";
            }
        }

        if (!cuotas.equals("")) {
            try {
                int i = Integer.parseInt(cuotas);
            } catch (NumberFormatException nfe) {
                msg += " | CuotasFinanciado no es numerico";
            }
        }

        return msg;
    }

    public int syncCalcl(List<CalculatorResponseData> calculatorRows, int id) {

        List<String> calculator = new ArrayList<String>();
        List<CalculatorResponseData> calculatorList = new ArrayList<CalculatorResponseData>();
        List<CalculatorResponseData> cal_offers = new ArrayList<CalculatorResponseData>();
        try {

            CalculatorResponseData model;
            // int id = dao.getIDCalculator();

            if (id >= 0) {

                for (int j = 0; j < calculatorRows.size(); j++) {

                    model = calculatorRows.get(j);

                    String lCalc = model.getTelefono() + "\t" + model.getServicio() + "\t" + model.getPaquetepsorigen()
                            + "\t" + model.getPaqueteorigen() + "\t" + model.getCantidaddecos() + "\t"
                            + model.getRentatotal() + "\t" + model.getInternet() + "\t" + model.getCodigooferta();

                    if (!calculator.contains(lCalc)) {

                        if (id == 0) {
                            id = 2;
                        } else {
                            id = id + 10;
                        }

                        model.setCalculatorID(id + "");
                        calculator.add(lCalc);
                        calculatorList.add(model);

                    } else {
                        model.setCalculatorID(id + "");
                    }

                    if (!cal_offers.contains(model)) {
                        cal_offers.add(model);
                    }
                }
                dao.deleteCalculator(calculatorList);
                dao.syncCalculator(calculatorList);
                dao.syncCalc_Offers(cal_offers);
            }
        } catch (Exception e) {
            logger.error("Error syncCalcl " + e.getMessage() + " " + e.getCause());

        }
        return id;
    }

    public Response<String> syncCalculator(String remoteFilePath, String fileName) {
        logger.info("starting sync of file: " + remoteFilePath);
        Response<String> response = new Response<>();
        File file = null;
        boolean finish = false;
        try {
            file = ftpService.retrieveFile(remoteFilePath);
            if (file != null) {
                finish = processCalculatorFile(file, fileName);
            } else {
                throw new ApplicationException("file.not.found");
            }

            if (finish) {
                response.setResponseCode("0");
                response.setResponseMessage("Se sincronizo correctamente Calculator.");
            }

        } catch (IOException | ApplicationException e) {
            logger.info("Error al buscar el archivo", e);
            response.setResponseCode("1");
            response.setResponseMessage("Error al sincronizar Calculator.");
            errorCalculator("Error al buscar el archivo " + e.getMessage(), fileName);
        } finally {
            if (file != null) {
                file.delete();
            }
        }
        logger.info("sync finished");
        return response;
    }

    public BEpaymentResponse getPagoEfectivo(Integer id, BEGenRequest request, PagoEfectivo PE) {
        LogSystem.info(logger, id + ":: GetPagoEfectivo", "Inicio");
        ServiceCallEvent event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
        event.setServiceUrl(PE.getServicesUrl());
        ObjectMapper mapper = new ObjectMapper();

        BEpaymentResponse rs = null;

        String requestJSON;
        try {
            requestJSON = mapper.writeValueAsString(request);
        } catch (Exception e) {
            requestJSON = "N/A";
        }

        event.setServiceCode("PAGOEFECTIVO");
        event.setServiceRequest(requestJSON);

        String responseJSON = "N/A";
        try {
            GenerarCIPService service = new GenerarCIPService(PE);
            Proxy proxy = getSockProxy(PE);
            rs = service.generar(id, request);

            try {
                responseJSON = mapper.writeValueAsString(rs);
            } catch (Exception e) {
                responseJSON = e.getMessage();
            }

            event.setMsg("OK");
            event.setResult("OK");

        } catch (Exception e) {
            LogSystem.error(logger, id + ":: GetPagoEfectivo", "Exception", e);
            responseJSON = e.getMessage();
            event.setMsg(e.getMessage());
            event.setResult("ERROR");
        }
        event.setServiceResponse(responseJSON);

        LogSystem.infoObject(logger, id + ":: GetPagoEfectivo", "event", event);
        serviceCallEventsService.registerEvent(event);

        LogSystem.info(logger, id + ":: GetPagoEfectivo", "Fin");
        return rs;
    }

    public PagoEfectivoConsultaResponse2 consultarCIP(String numeroCip, String orderId, String docNumber, boolean notifyEvent) {

        PagoEfectivo PE = new PagoEfectivo.Builder()
                .setPrivateKey(privateKeyLocation)
                .setPublicKey(publicKeyLocation)
                .setCryptoUrl(cryptoUrl)
                .setServicesUrl(servicesUrl)
                .setCodigoServicio(serviceCode)
                .setHostnameProxy(proxyHostname)
                .setPortProxy(proxyPort)
                .setUserProxy(proxyUser)
                .setPasswordProxy(proxyPassword)
                .build();
        PagoEfectivoConsultaRequest rq = new PagoEfectivoConsultaRequest();
        rq.setNumeroCIP(numeroCip);
        rq.setOrderId(orderId);
        rq.setDocNumber(docNumber);
        //PagoEfectivoConsultaResponse rs = getConsultaPagoEfectivo(rq, PE) ;
        PagoEfectivoConsultaResponse2 rs = (notifyEvent) ? getConsultaPagoEfectivo(rq, PE) : getConsultaPagoEfectivoBatch(rq, PE);

        // Lineas para pruebas de CIP PAGADO
        /*
        rs = new PagoEfectivoConsultaResponse();
        rs.setNumeroCIP(deleteCerosLeft(numeroCip));
        rs.setIdEstadoCIP("23");
        rs.setEstadoCIP("Pagado");
        */
        return rs;
    }

    public String deleteCerosLeft(String cadena) {

        if (cadena != null && !cadena.equals("")) {
            int count = 0;

            for (char c : cadena.toCharArray()) {
                if (c == '0') {
                    count++;
                } else {
                    break;
                }
            }

            cadena = cadena.substring(count);

        } else {
            return "";
        }

        return cadena;

    }

    public PagoEfectivoConsultaResponse2 getConsultaPagoEfectivo(PagoEfectivoConsultaRequest request, PagoEfectivo PE) {


        /*ServiceCallEvent event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
        event.setServiceUrl(PE.getServicesUrl());
        event.setOrderId(request.getOrderId());
        event.setDocNumber(request.getDocNumber());
        ObjectMapper mapper = new ObjectMapper();*/
        Exception ex;

        PagoEfectivoConsultaResponse2 rs = null;


        ServiceCallEvent event = new ServiceCallEvent();
        event.setServiceUrl(PE.getServicesUrl());
        event.setOrderId(request.getOrderId());
        event.setDocNumber(request.getDocNumber());
        event.setServiceCode("CONSULTAPAGOEFECTIVO");
        event.setSourceApp("BCIP");

        ObjectMapper mapper = new ObjectMapper();
        String requestJSON;
        try {
            requestJSON = mapper.writeValueAsString(request);
        } catch (Exception e) {
            requestJSON = "N/A";
        }

        event.setServiceRequest(requestJSON);

        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);

        /*String requestJSON;
        try {
            requestJSON = mapper.writeValueAsString(request);
        } catch (Exception e) {
            requestJSON = "N/A";
        }

        event.setServiceCode("CONSULTAPAGOEFECTIVO");
        event.setServiceRequest(requestJSON);*/

        boolean success = false;
        String responseJSON = null;
        BEWSConsultarRequest bEWSConsultarRequest = null;
        BEWSConsultarResponse bEWSConsultarResponse = null;
        try {
            ConsultarCIPService service = new ConsultarCIPService(PE);

            bEWSConsultarRequest = new BEWSConsultarRequest();
            bEWSConsultarRequest.setNumeroCIP(request.getNumeroCIP());

            bEWSConsultarResponse = service.consultar(bEWSConsultarRequest);

            if (bEWSConsultarResponse != null) {
                rs = new PagoEfectivoConsultaResponse2();
                if (bEWSConsultarResponse.getNumeroCIP() != null) {
                    rs.setNumeroCIP(bEWSConsultarResponse.getNumeroCIP());
                } else {
                    rs.setNumeroCIP("");
                }
                if (bEWSConsultarResponse.getIdEstadoCIP() != null) {
                    rs.setIdEstadoCIP(bEWSConsultarResponse.getIdEstadoCIP());
                } else {
                    rs.setIdEstadoCIP("");
                }
                if (bEWSConsultarResponse.getEstadoCIP() != null) {
                    rs.setEstadoCIP(bEWSConsultarResponse.getEstadoCIP());
                } else {
                    rs.setEstadoCIP("");
                }
                if (bEWSConsultarResponse.getFechaPagoCIP() != null) {
                    rs.setFechaPagoCIP(bEWSConsultarResponse.getFechaPagoCIP());
                } else {
                    rs.setFechaPagoCIP("");
                }
            }

            responseJSON = "N/A";
            try {
                responseJSON = mapper.writeValueAsString(rs);
            } catch (Exception e) {
                logger.error("Error :/", e);
            }

            event.setMsg("OK");
            event.setResult("OK");

            success = true;
        } catch (Exception e) {
            ex = e;
            logger.error("Error :/", e);
            event.setMsg(e.getMessage());
            event.setResult("ERROR");
            responseJSON = "N/A";
        }
        event.setServiceResponse(responseJSON);
        serviceCallEventsService.registerEvent(event);

        endCallContext();

        return rs;
    }

    private static Proxy getSockProxy(PagoEfectivo pagoEfectivo) {
        ProxySOCKS5 proxyTunnel = null;

        try {
            proxyTunnel = new ProxySOCKS5(pagoEfectivo.getHostnameProxy(), Integer.parseInt(pagoEfectivo.getPortProxy()));
            proxyTunnel.setUserPasswd(pagoEfectivo.getUserProxy(), pagoEfectivo.getPasswordProxy());
            //session.setProxy(proxyTunnel);

        } catch (Exception e) {
            logger.fatal("REPORT: " + e.getMessage());
            e.printStackTrace();
        }
        return proxyTunnel;
    }

    private void endCallContext() {
        VentaFijaContextHolder.clearContext();
    }

    //@Scheduled(cron = "*/30 * * * * *")
    @Scheduled(cron = "${pe.api.sync.cron}")
    public void syncPagoEfectivo() {

        Date ahora = new Date();
        long ahoramilisecs = ahora.getTime();
        logger.info("Iniciando BATCH PAGOEFECTIVO @" + ahora + " .....");

        if ("0".equals(flagUpfront)) {
            logger.error("Error : el flagUpfront no esta activo");
            logger.info("Fin BATCH PAGOEFECTIVO @" + ahora + " .................");
            return;
        }


        boolean flagciplog = false;

        if (flagCipLog != null)
            flagciplog = flagCipLog.equals("1") ? true : false;

        int desdeDiasAtras = (startdays != null) ? Integer.parseInt(startdays) : 0;
        // cambiar fechainicioBatch en el pase
        String fechainicioBatch = (startdate != null) ? startdate : "01-12-2018 00:00:00";
        List<PagoEfectivoConsultaRequest2> listaPendientes = dao.loadPagoEfectivoDataPendiente(desdeDiasAtras, fechainicioBatch);
        logger.info(String.format("listado de registros con codigo cip en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));

        try {

            if (listaPendientes != null) {
                logger.info(String.format("Total de  registros con codigo cip a procesar: %s", listaPendientes.size()));
                for (PagoEfectivoConsultaRequest2 pagoEfectivoConsultaRequest : listaPendientes) {
                    try {

                        //Consultar Cip nuevo servicio
                        ConsultarCipRequest consultarCipRequest = new ConsultarCipRequest();
                        ArrayList<Data> dataList = new ArrayList<>();
                        Data data = new Data();
                        if (pagoEfectivoConsultaRequest.getNumeroCIP().substring(0, 1).equalsIgnoreCase("0")) {
                            data.setCip(Integer.parseInt(pagoEfectivoConsultaRequest.getNumeroCIP().substring(6, pagoEfectivoConsultaRequest.getNumeroCIP().length())));
                        } else {
                            data.setCip(Integer.parseInt(pagoEfectivoConsultaRequest.getNumeroCIP()));
                        }
                        dataList.add(data);
                        consultarCipRequest.setData(dataList);
                        ConsultarCipResponse consultarCipResponse = consultarCIPRest(consultarCipRequest, pagoEfectivoConsultaRequest);

                        PagoEfectivoConsultaResponse2 bewsResponse = new PagoEfectivoConsultaResponse2();
                        bewsResponse.setNumeroCIP(consultarCipResponse.getData().get(0).getCip() + "");
                        bewsResponse.setIdEstadoCIP(consultarCipResponse.getData().get(0).getStatus() + "");
                        bewsResponse.setEstadoCIP(consultarCipResponse.getData().get(0).getStatusName());
                        bewsResponse.setFechaPagoCIP(consultarCipResponse.getData().get(0).getDateExpiry());

                        /*PagoEfectivoConsultaResponse2 bewsResponse = consultarCIP(pagoEfectivoConsultaRequest.getNumeroCIP(),
                                pagoEfectivoConsultaRequest.getOrderId(), pagoEfectivoConsultaRequest.getDocNumber(), flagciplog);*/
                        dao.actualizarPagoEfectivoProceso(pagoEfectivoConsultaRequest.getNumeroCIP(),ahora);
                        updateStatusCIP(pagoEfectivoConsultaRequest, bewsResponse);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }


        } catch (Exception e) {
            logger.error("Error BATCH PAGOEFECTIVO :/", e);
        }
        logger.info("Fin BATCH PAGOEFECTIVO @" + ahora + " .................");
    }

    private ConsultarCipResponse consultarCIPRest(ConsultarCipRequest consultarCipRequest, PagoEfectivoConsultaRequest2 pagoEfectivoConsultaRequest2) {
        ConsultarCipResponse consultarCipResponse = new ConsultarCipResponse();
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl("http://services.pagoefectivo.pe/v1/cips/search")
                .build();
        ConsultarCIp consultarCIp = new ConsultarCIp(config);
        ClientResult<ConsultarCipResponse> result = consultarCIp.postCIP(consultarCipRequest, "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJNT1YiLCJqdGkiOiIxYjUxYmJjYi0wYmQ5LTQwMmUtYTRlOC1lYWIwNTQ2N2I3N2MiLCJuYW1laWQiOiIxMDQiLCJleHAiOjE1ODAwMTIxMDF9.2TVKKT-X7jFiWx-_7DJUZBMYkknrsNY1WX1tqipubAw");
        //result.getEvent().setOrderId(consultarCipResponse.getData().get(0).getTransactionCode());

        //REGISTRAR EN SERVICES CALL EVENTS
        ServiceCallEvent event = new ServiceCallEvent();
        event.setServiceUrl(config.getUrl());
        event.setOrderId(result.getResult().getData().get(0).getTransactionCode());
        event.setDocNumber(pagoEfectivoConsultaRequest2.getDocNumber());
        event.setServiceCode("CONSULTAR_CIP");
        event.setSourceApp("BCIP");

        ObjectMapper mapper = new ObjectMapper();
        String requestJSON;
        try {
            requestJSON = mapper.writeValueAsString(consultarCipRequest);
        } catch (Exception e) {
            requestJSON = "N/A";
        }

        event.setServiceRequest(requestJSON);

        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);

        String responseJSON;
        try {
            responseJSON = mapper.writeValueAsString(result.getResult());
        } catch (Exception e) {
            responseJSON = "N/A";
        }

        event.setServiceResponse(responseJSON);

        if (!result.isSuccess()) {
            event.setMsg(result.getResult().getMessage());
            event.setResult("ERROR");
            serviceCallEventsService.registerEvent(event);
            consultarCipResponse = result.getResult();
            //throw new TimeoutException(result.getE().getMessage());
        } else {
            event.setMsg(result.getResult().getMessage());
            event.setResult("OK");
            serviceCallEventsService.registerEvent(event);
            consultarCipResponse = result.getResult();
        }
        return consultarCipResponse;
    }


    public void updateStatusCIP(PagoEfectivoConsultaRequest2 pecr, PagoEfectivoConsultaResponse2 penr) {
        int n = 0;
        String estado = "";
        String motivo = "";
        String ordenVenta = "";
        String status = "";
        String statusCIP = "EN";

        final String STATUS_CIP_PAGADO = "PA";
        final String STATUS_CIP_EXPIRADO = "EX";
        final String STATUS_CIP_ELIMINADO = "EL";


        if (penr != null) {
            if (penr.getIdEstadoCIP() != null) {
                if (penr.getIdEstadoCIP().equalsIgnoreCase("23")) {
                    estado = "PENDIENTE";
                    motivo = "CIP PAGADO";
                    statusCIP = STATUS_CIP_PAGADO;
                } else if (penr.getIdEstadoCIP().equalsIgnoreCase("21")) {
                    estado = "CAIDA";
                    motivo = "CIP EXPIRADO";
                    statusCIP = STATUS_CIP_EXPIRADO;
                } else if (penr.getIdEstadoCIP().equalsIgnoreCase("25")) {
                    estado = "CAIDA";
                    motivo = "CIP ELIMINADO";
                    statusCIP = STATUS_CIP_ELIMINADO;
                }

                ordenVenta = pecr.getOrderId();
                //status = pecr.getStatusLegacy();

                HDECResponse hdecResponse = null;

                //if (status!=null && status.equals("PENDIENTE")) {
                if (penr.getIdEstadoCIP().equalsIgnoreCase("21") || penr.getIdEstadoCIP().equalsIgnoreCase("23")
                        || penr.getIdEstadoCIP().equalsIgnoreCase("25")) {
                    dao.updateEstadoMotivo(estado, motivo, pecr.getNumeroCIP());
                    boolean actualizaStatusPagado = false;
                    for (int i = 0; i <= 2; i++) {
                        actualizaStatusPagado = dao.actualizarPagoEfectivoStatus(statusCIP, pecr.getNumeroCIP(), penr.getFechaPagoCIP());
                        if (actualizaStatusPagado) {
                            break;
                        }
                    }


                    if (penr.getIdEstadoCIP().equalsIgnoreCase("23")) {
                        //try {
                        //boolean actualizaStatusPagado = false;
                        //update pagaefectivo

                        //if (actualizaStatusPagado) {
                        // Ya no se manda directamente a HDEC, por problemas en registrar en el servicecallevents
                        // cuando usamos el scheduled, en su defecto actualizamos a ENVIANDO para que al BATCH HDEC lo procese
                        //hdecResponse = hdecRetryService.sendToHDECWithRetry(ordenVenta);
                        dao.updateEstadoMotivo("ENVIANDO_AUTOMATIZADOR", "ENVIADO A AUTOMATIZADOR", pecr.getNumeroCIP());

                                    /*
                                    if (hdecResponse!=null && HDECResponse.ERROR_CONTROLADO.equals(hdecResponse)) {
                                        logger.info("Error controlado, se actualiza la venta a ENVIANDO");
                                        dao.updateEstadoMotivo("ENVIANDO","",penr.getNumeroCIP());
                                        dao.actualizarPagoEfectivoStatus("EN", pecr.getNumeroCIP());
                                    }
                                    */
                        //}
                               /*
                            } catch (ApiClientException | ClientException e) {
                                if (e.getMessage() != null && e.getMessage().contains("Error: CODIGO_UNICO ya existe")) {
                                    logger.info("Ya existe el codigo unico, el flujo continuara con el proceso");
                                } else {
                                    logger.info("Error diferente al codigo unico en hdec, se detendra el flujo");
                                    // Seteamos a un error distinto
                                    dao.updateEstadoMotivo("CAIDA","",penr.getNumeroCIP());
                                }
                            } catch (ApplicationException e) {
                                e.printStackTrace();
                            }
                            */
                    }

                }
                //}
            }
        }
    }

    public PagoEfectivoConsultaResponse2 getConsultaPagoEfectivoBatch(PagoEfectivoConsultaRequest request, PagoEfectivo PE) {

        PagoEfectivoConsultaResponse2 rs = null;


        BEWSConsultarRequest bEWSConsultarRequest = null;
        BEWSConsultarResponse bEWSConsultarResponse = null;
        try {
            ConsultarCIPService service = new ConsultarCIPService(PE);

            bEWSConsultarRequest = new BEWSConsultarRequest();
            bEWSConsultarRequest.setNumeroCIP(request.getNumeroCIP());

            bEWSConsultarResponse = service.consultar(bEWSConsultarRequest);

            if (bEWSConsultarResponse != null) {
                rs = new PagoEfectivoConsultaResponse2();
                if (bEWSConsultarResponse.getNumeroCIP() != null) {
                    rs.setNumeroCIP(bEWSConsultarResponse.getNumeroCIP());
                } else {
                    rs.setNumeroCIP("");
                }
                if (bEWSConsultarResponse.getIdEstadoCIP() != null) {
                    rs.setIdEstadoCIP(bEWSConsultarResponse.getIdEstadoCIP());
                } else {
                    rs.setIdEstadoCIP("");
                }
                if (bEWSConsultarResponse.getEstadoCIP() != null) {
                    rs.setEstadoCIP(bEWSConsultarResponse.getEstadoCIP());
                } else {
                    rs.setEstadoCIP("");
                }
                if (bEWSConsultarResponse.getFechaPagoCIP() != null) {
                    rs.setFechaPagoCIP(bEWSConsultarResponse.getFechaPagoCIP());
                } else {
                    rs.setFechaPagoCIP("");
                }
            }

        } catch (Exception e) {
            logger.error("Error getConsultaPagoEfectivoBatch :/", e);
        }

        return rs;
    }
}
