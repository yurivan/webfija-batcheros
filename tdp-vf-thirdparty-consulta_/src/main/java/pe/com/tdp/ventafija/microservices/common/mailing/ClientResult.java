package pe.com.tdp.ventafija.microservices.common.mailing;

/**
 * Created by Felix on 19/06/2017.
 */
public class ClientResult<T> {
    private T result;
    private boolean success;

    public ClientResult() {
        super();
        this.success = false;
    }

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
