package pe.com.tdp.ventafija.microservices.domain.thirdparty.dto;

import pe.com.tdp.ventafija.microservices.domain.thirdparty.DevComercialResponseData;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.DevTecnicaResponseData;

import java.util.ArrayList;
import java.util.List;

public class SyncDevTecResult {
	private Integer insertCount;
	private Integer updateCount;
	private Integer errorCount;
	private Integer orderCount;
	private List<DevTecnicaResponseData> data;
	private List<DevTecnicaResponseData> errors;
	private List<SyncHdecSyntaxError> syntaxErrors;

	public SyncDevTecResult() {
		super();
		this.insertCount = 0;
		this.updateCount = 0;
		this.errorCount = 0;
		this.orderCount = 0;
		this.data = new ArrayList<>();
		this.errors = new ArrayList<>();
		this.syntaxErrors = new ArrayList<>();
	}

	public SyncDevTecResult(Integer insertCount, Integer updateCount, Integer errorCount, Integer orderCount,
                            List<DevTecnicaResponseData> data, List<DevTecnicaResponseData> errors) {
		super();
		this.insertCount = insertCount;
		this.updateCount = updateCount;
		this.errorCount = errorCount;
		this.orderCount = orderCount;
		this.data = data;
		this.errors = errors;
		this.syntaxErrors = new ArrayList<>();
	}

	public Integer getInsertCount() {
		return insertCount;
	}

	public void setInsertCount(Integer insertCount) {
		this.insertCount = insertCount;
	}

	public Integer getUpdateCount() {
		return updateCount;
	}

	public void setUpdateCount(Integer updateCount) {
		this.updateCount = updateCount;
	}

	public Integer getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}

	public List<DevTecnicaResponseData> getData() {
		return data;
	}

	public void setData(List<DevTecnicaResponseData> data) {
		this.data = data;
	}

	public List<DevTecnicaResponseData> getErrors() {
		return errors;
	}

	public void setErrors(List<DevTecnicaResponseData> errors) {
		this.errors = errors;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public List<SyncHdecSyntaxError> getSyntaxErrors() {
		return syntaxErrors;
	}

	public void setSyntaxErrors(List<SyncHdecSyntaxError> syntaxErrors) {
		this.syntaxErrors = syntaxErrors;
	}

}
