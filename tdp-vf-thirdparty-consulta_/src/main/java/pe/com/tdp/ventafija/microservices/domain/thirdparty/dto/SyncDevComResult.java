package pe.com.tdp.ventafija.microservices.domain.thirdparty.dto;

import pe.com.tdp.ventafija.microservices.domain.thirdparty.DevComercialResponseData;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.HdecResponseData;

import java.util.ArrayList;
import java.util.List;

public class SyncDevComResult {
	private Integer insertCount;
	private Integer updateCount;
	private Integer errorCount;
	private Integer orderCount;
	private List<DevComercialResponseData> data;
	private List<DevComercialResponseData> errors;
	private List<SyncHdecSyntaxError> syntaxErrors;

	public SyncDevComResult() {
		super();
		this.insertCount = 0;
		this.updateCount = 0;
		this.errorCount = 0;
		this.orderCount = 0;
		this.data = new ArrayList<>();
		this.errors = new ArrayList<>();
		this.syntaxErrors = new ArrayList<>();
	}

	public SyncDevComResult(Integer insertCount, Integer updateCount, Integer errorCount, Integer orderCount,
                            List<DevComercialResponseData> data, List<DevComercialResponseData> errors) {
		super();
		this.insertCount = insertCount;
		this.updateCount = updateCount;
		this.errorCount = errorCount;
		this.orderCount = orderCount;
		this.data = data;
		this.errors = errors;
		this.syntaxErrors = new ArrayList<>();
	}

	public Integer getInsertCount() {
		return insertCount;
	}

	public void setInsertCount(Integer insertCount) {
		this.insertCount = insertCount;
	}

	public Integer getUpdateCount() {
		return updateCount;
	}

	public void setUpdateCount(Integer updateCount) {
		this.updateCount = updateCount;
	}

	public Integer getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(Integer errorCount) {
		this.errorCount = errorCount;
	}

	public List<DevComercialResponseData> getData() {
		return data;
	}

	public void setData(List<DevComercialResponseData> data) {
		this.data = data;
	}

	public List<DevComercialResponseData> getErrors() {
		return errors;
	}

	public void setErrors(List<DevComercialResponseData> errors) {
		this.errors = errors;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public List<SyncHdecSyntaxError> getSyntaxErrors() {
		return syntaxErrors;
	}

	public void setSyntaxErrors(List<SyncHdecSyntaxError> syntaxErrors) {
		this.syntaxErrors = syntaxErrors;
	}

}
