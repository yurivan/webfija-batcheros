package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class DevComercialResponseData {

	private String gestion;
	private String requerimiento;
	private String desmotv;
	private String des_mot;
	private String det_mot;
	private String nomb_tco;
	private String tematico1;
	private String tematico2;
	private String tematico3;
	private String tematico4;
	private String fecagen;
	private String horaagen;
	private String obsgest;
	private String fecha_carga;
	private String fecha_gestion;
	private String situa;
	private String usu_ges;

	public String getGestion() {
		return gestion;
	}

	public void setGestion(String gestion) {
		this.gestion = gestion;
	}

	public String getRequerimiento() {
		return requerimiento;
	}

	public void setRequerimiento(String requerimiento) {
		this.requerimiento = requerimiento;
	}

	public String getDesmotv() {
		return desmotv;
	}

	public void setDesmotv(String desmotv) {
		this.desmotv = desmotv;
	}

	public String getDes_mot() {
		return des_mot;
	}

	public void setDes_mot(String des_mot) {
		this.des_mot = des_mot;
	}

	public String getDet_mot() {
		return det_mot;
	}

	public void setDet_mot(String det_mot) {
		this.det_mot = det_mot;
	}

	public String getNomb_tco() {
		return nomb_tco;
	}

	public void setNomb_tco(String nomb_tco) {
		this.nomb_tco = nomb_tco;
	}

	public String getTematico1() {
		return tematico1;
	}

	public void setTematico1(String tematico1) {
		this.tematico1 = tematico1;
	}

	public String getTematico2() {
		return tematico2;
	}

	public void setTematico2(String tematico2) {
		this.tematico2 = tematico2;
	}

	public String getTematico3() {
		return tematico3;
	}

	public void setTematico3(String tematico3) {
		this.tematico3 = tematico3;
	}

	public String getTematico4() {
		return tematico4;
	}

	public void setTematico4(String tematico4) {
		this.tematico4 = tematico4;
	}

	public String getFecagen() {
		return fecagen;
	}

	public void setFecagen(String fecagen) {
		this.fecagen = fecagen;
	}

	public String getHoraagen() {
		return horaagen;
	}

	public void setHoraagen(String horaagen) {
		this.horaagen = horaagen;
	}

	public String getObsgest() {
		return obsgest;
	}

	public void setObsgest(String obsgest) {
		this.obsgest = obsgest;
	}

	public String getFecha_carga() {
		return fecha_carga;
	}

	public void setFecha_carga(String fecha_carga) {
		this.fecha_carga = fecha_carga;
	}

	public String getFecha_gestion() {
		return fecha_gestion;
	}

	public void setFecha_gestion(String fecha_gestion) {
		this.fecha_gestion = fecha_gestion;
	}

	public String getSitua() {
		return situa;
	}

	public void setSitua(String situa) {
		this.situa = situa;
	}

	public String getUsu_ges() {
		return usu_ges;
	}

	public void setUsu_ges(String usu_ges) {
		this.usu_ges = usu_ges;
	}

	/*private String requerimiento;
	private String fecha_gestion;
	private String respon;
	private String gestion;
	private String actividad;
	private String tematico_1;
	private String tematico_2;
	private String tematico_3;
	private String tematico_4;
	private String obs_gestion;
	private String responsable;
	private String fecha_carga;
	private String fecha_sms;
	private String situacion;
	private String zonal;
	private String jefatura;
	private String nodo;
	private String motivo_des;
	private String motivo_det;
	private String nombre_tecnico;
	private String celular_tecnico;
	private String area;

	public String getRequerimiento() {
		return requerimiento;
	}

	public void setRequerimiento(String requerimiento) {
		this.requerimiento = requerimiento;
	}

	public String getFecha_gestion() {
		return fecha_gestion;
	}

	public void setFecha_gestion(String fecha_gestion) {
		this.fecha_gestion = fecha_gestion;
	}

	public String getRespon() {
		return respon;
	}

	public void setRespon(String respon) {
		this.respon = respon;
	}

	public String getGestion() {
		return gestion;
	}

	public void setGestion(String gestion) {
		this.gestion = gestion;
	}

	public String getActividad() {
		return actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	public String getTematico_1() {
		return tematico_1;
	}

	public void setTematico_1(String tematico_1) {
		this.tematico_1 = tematico_1;
	}

	public String getTematico_2() {
		return tematico_2;
	}

	public void setTematico_2(String tematico_2) {
		this.tematico_2 = tematico_2;
	}

	public String getTematico_3() {
		return tematico_3;
	}

	public void setTematico_3(String tematico_3) {
		this.tematico_3 = tematico_3;
	}

	public String getTematico_4() {
		return tematico_4;
	}

	public void setTematico_4(String tematico_4) {
		this.tematico_4 = tematico_4;
	}

	public String getObs_gestion() {
		return obs_gestion;
	}

	public void setObs_gestion(String obs_gestion) {
		this.obs_gestion = obs_gestion;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getFecha_carga() {
		return fecha_carga;
	}

	public void setFecha_carga(String fecha_carga) {
		this.fecha_carga = fecha_carga;
	}

	public String getFecha_sms() {
		return fecha_sms;
	}

	public void setFecha_sms(String fecha_sms) {
		this.fecha_sms = fecha_sms;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getZonal() {
		return zonal;
	}

	public void setZonal(String zonal) {
		this.zonal = zonal;
	}

	public String getJefatura() {
		return jefatura;
	}

	public void setJefatura(String jefatura) {
		this.jefatura = jefatura;
	}

	public String getNodo() {
		return nodo;
	}

	public void setNodo(String nodo) {
		this.nodo = nodo;
	}

	public String getMotivo_des() {
		return motivo_des;
	}

	public void setMotivo_des(String motivo_des) {
		this.motivo_des = motivo_des;
	}

	public String getMotivo_det() {
		return motivo_det;
	}

	public void setMotivo_det(String motivo_det) {
		this.motivo_det = motivo_det;
	}

	public String getNombre_tecnico() {
		return nombre_tecnico;
	}

	public void setNombre_tecnico(String nombre_tecnico) {
		this.nombre_tecnico = nombre_tecnico;
	}

	public String getCelular_tecnico() {
		return celular_tecnico;
	}

	public void setCelular_tecnico(String celular_tecnico) {
		this.celular_tecnico = celular_tecnico;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}*/
}
