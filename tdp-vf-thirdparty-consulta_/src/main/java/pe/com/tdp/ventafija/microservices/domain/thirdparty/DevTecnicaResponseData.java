package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class DevTecnicaResponseData {
	
	private String numero_peticion;
	private String id;
	private String id_motivo;
	private String codigo_motivo;
	private String motivo;
	private String submotivo;
	private String descripcion;
	private String nombre;
	private String numero_cel;
	private String carnet;
	private String area_gestion;
	private String EECC;
	private String tematico_1;
	private String tematico_2;
	private String tematico_3;
	private String comentario;
	private String fecha_ingreso;
	private String fecha_liquidacion;
	private String fecha_proceso;
	private String estado;
	private String usuario;
	private String supervision;
	private String perfil;
	private String zonal;
	private String jefatura;
	private String mov;
	private String nodo;
	private String telf_1;
	private String telf_2;
	private String win_telf_1;
	private String win_telf_2;
	private String win_telf_3;
	private String contra;
	private String negocio;
	private String dato_filtro;
	private String win_back;
	private String up_front;
	private String region;
	private String cod_trtrn;
	private String cod_lex;
	private String cod_tap;
	private String cod_bor;

	public String getNumero_peticion() {
		return numero_peticion;
	}

	public void setNumero_peticion(String numero_peticion) {
		this.numero_peticion = numero_peticion;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId_motivo() {
		return id_motivo;
	}

	public void setId_motivo(String id_motivo) {
		this.id_motivo = id_motivo;
	}

	public String getCodigo_motivo() {
		return codigo_motivo;
	}

	public void setCodigo_motivo(String codigo_motivo) {
		this.codigo_motivo = codigo_motivo;
	}

	public String getMotivo() {
		return motivo;
	}

	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}

	public String getSubmotivo() {
		return submotivo;
	}

	public void setSubmotivo(String submotivo) {
		this.submotivo = submotivo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNumero_cel() {
		return numero_cel;
	}

	public void setNumero_cel(String numero_cel) {
		this.numero_cel = numero_cel;
	}

	public String getCarnet() {
		return carnet;
	}

	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}

	public String getArea_gestion() {
		return area_gestion;
	}

	public void setArea_gestion(String area_gestion) {
		this.area_gestion = area_gestion;
	}

	public String getEECC() {
		return EECC;
	}

	public void setEECC(String EECC) {
		this.EECC = EECC;
	}

	public String getTematico_1() {
		return tematico_1;
	}

	public void setTematico_1(String tematico_1) {
		this.tematico_1 = tematico_1;
	}

	public String getTematico_2() {
		return tematico_2;
	}

	public void setTematico_2(String tematico_2) {
		this.tematico_2 = tematico_2;
	}

	public String getTematico_3() {
		return tematico_3;
	}

	public void setTematico_3(String tematico_3) {
		this.tematico_3 = tematico_3;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getFecha_ingreso() {
		return fecha_ingreso;
	}

	public void setFecha_ingreso(String fecha_ingreso) {
		this.fecha_ingreso = fecha_ingreso;
	}

	public String getFecha_liquidacion() {
		return fecha_liquidacion;
	}

	public void setFecha_liquidacion(String fecha_liquidacion) {
		this.fecha_liquidacion = fecha_liquidacion;
	}

	public String getFecha_proceso() {
		return fecha_proceso;
	}

	public void setFecha_proceso(String fecha_proceso) {
		this.fecha_proceso = fecha_proceso;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSupervision() {
		return supervision;
	}

	public void setSupervision(String supervision) {
		this.supervision = supervision;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getZonal() {
		return zonal;
	}

	public void setZonal(String zonal) {
		this.zonal = zonal;
	}

	public String getJefatura() {
		return jefatura;
	}

	public void setJefatura(String jefatura) {
		this.jefatura = jefatura;
	}

	public String getMov() {
		return mov;
	}

	public void setMov(String mov) {
		this.mov = mov;
	}

	public String getNodo() {
		return nodo;
	}

	public void setNodo(String nodo) {
		this.nodo = nodo;
	}

	public String getTelf_1() {
		return telf_1;
	}

	public void setTelf_1(String telf_1) {
		this.telf_1 = telf_1;
	}

	public String getTelf_2() {
		return telf_2;
	}

	public void setTelf_2(String telf_2) {
		this.telf_2 = telf_2;
	}

	public String getWin_telf_1() {
		return win_telf_1;
	}

	public void setWin_telf_1(String win_telf_1) {
		this.win_telf_1 = win_telf_1;
	}

	public String getWin_telf_2() {
		return win_telf_2;
	}

	public void setWin_telf_2(String win_telf_2) {
		this.win_telf_2 = win_telf_2;
	}

	public String getWin_telf_3() {
		return win_telf_3;
	}

	public void setWin_telf_3(String win_telf_3) {
		this.win_telf_3 = win_telf_3;
	}

	public String getContra() {
		return contra;
	}

	public void setContra(String contra) {
		this.contra = contra;
	}

	public String getNegocio() {
		return negocio;
	}

	public void setNegocio(String negocio) {
		this.negocio = negocio;
	}

	public String getDato_filtro() {
		return dato_filtro;
	}

	public void setDato_filtro(String dato_filtro) {
		this.dato_filtro = dato_filtro;
	}

	public String getWin_back() {
		return win_back;
	}

	public void setWin_back(String win_back) {
		this.win_back = win_back;
	}

	public String getUp_front() {
		return up_front;
	}

	public void setUp_front(String up_front) {
		this.up_front = up_front;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCod_trtrn() {
		return cod_trtrn;
	}

	public void setCod_trtrn(String cod_trtrn) {
		this.cod_trtrn = cod_trtrn;
	}

	public String getCod_lex() {
		return cod_lex;
	}

	public void setCod_lex(String cod_lex) {
		this.cod_lex = cod_lex;
	}

	public String getCod_tap() {
		return cod_tap;
	}

	public void setCod_tap(String cod_tap) {
		this.cod_tap = cod_tap;
	}

	public String getCod_bor() {
		return cod_bor;
	}

	public void setCod_bor(String cod_bor) {
		this.cod_bor = cod_bor;
	}
}
