package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class PagoEfectivoConsultaRequest2 {

    private String numeroCIP;
    private String orderId;
    private String docNumber;
    private String statusLegacy;

    public PagoEfectivoConsultaRequest2(){
        super();
    }

    public String getNumeroCIP() {
        return numeroCIP;
    }

    public void setNumeroCIP(String numeroCIP) {
        this.numeroCIP = numeroCIP;
    }

    public String getOrderId() { return orderId; }

    public void setOrderId(String orderId) { this.orderId = orderId;  }

    public String getDocNumber() {  return docNumber;   }

    public void setDocNumber(String docNumber) {  this.docNumber = docNumber;  }

    public String getStatusLegacy() {
        return statusLegacy;
    }

    public void setStatusLegacy(String statusLegacy) {
        this.statusLegacy = statusLegacy;
    }
}
