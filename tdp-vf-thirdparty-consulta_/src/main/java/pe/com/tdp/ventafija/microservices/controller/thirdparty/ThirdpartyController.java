package pe.com.tdp.ventafija.microservices.controller.thirdparty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.*;
import pe.com.tdp.ventafija.microservices.service.thirdparty.FtpService;
import pe.com.tdp.ventafija.microservices.service.thirdparty.ThirdpartyService;
import pe.com.tdp.ventafija.microservices.util.LogSystem;

import java.io.File;

@RestController
public class ThirdpartyController {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private ThirdpartyService service;
    @Autowired
    private FtpService ftpService;

    @RequestMapping(value = "/thirdparty/syncHDEC", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> syncHDEC() {

        Response<String> response = new Response<>();

        try {
//			response = service.completeSync();
//			service.processHdcFile(new File("/tmp/BASE_VISOR_R.txt"));
//			service.processHdcFile(new File("/tmp/BD_REGISTROS_BI_ADRIAN_UN_REGISTRO.txt"));
//			service.processHdcFile(new File("/tmp/Base_Visor_BI_20161213084200.txt"));
//			service.processHdcFile(new File("/tmp/Base_Visor_BI_20161213083000.csv"));
//			service.processHdcFile(new File("/tmp/Base_Visor_BI_20161213135800.csv"));
//			service.processHdcFile(new File("/tmp/tmp1.txt"));
//			service.processHdcFile(new File("/tmp/Base_Visor_BI_20161226164200.txt"));
//			service.processHdcFile(new File("/tmp/cascader2332/test1-1.txt"));
//			service.processHdcFile(new File("/tmp/VISOR/Base_Visor_BI_20170220173700/Base_Visor_BI_20170220173700.TXT"));
//			service.processHdcFile(new File("/tmp/HDCE_2306/BD_VISOR_BI_20170621204500.txt"));
            service.processHdcFile(new File("/tmp/TGESTIONA/BD_VISOR_BI_20170724175940.txt"));

//			service.diffSync();

//			ftpService.moveFile("/APPTEST/", "VF_1484656518145_BD_VISOR_BI_20170116123000.txt", "/APPTEST/BackUp/", "wa.txt");
//			java.util.List<String> filenames = ftpService.listFiles("/APPTEST/");
//			java.util.Map<String, String> repetidos = new java.util.HashMap<>();
//			for (String filename : filenames) {
//				if (java.util.regex.Pattern.matches("VF_\\d+\\_BD_VISOR_BI_201702\\d+\\.txt", filename)) {
//					String newFilename = filename;
//					newFilename = newFilename.substring(17);
//					if (repetidos.get(newFilename) == null) {
//						ftpService.renameFile("/APPTEST/", filename, newFilename);
//						logger.info(String.format("archivo: %s --> %s", filename, newFilename));
//					} else {
//						repetidos.put(newFilename, newFilename);
//					}
//				}
////				ftpService.moveFile("/APPTEST/", "VF_1484656518145_BD_VISOR_BI_20170116123000.txt", "/APPTEST/BackUp/", "wa.txt");
//			}
        } catch (Exception e) {
            logger.error("Error SyncHDEC Controller.", e);
        }

        return response;
    }

    //@RequestMapping(value = "/thirdparty/sendAzure", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> sendAzure(@RequestBody AzureRequest request) {

        Response<String> response = new Response<>();

        try {
            response = service.sendAzure(request);
        } catch (Exception e) {
            logger.info("Error sendAzure Controller." + e);
        }
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/thirdparty/pagoefectivo/process", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> pagoEfectivo(@RequestBody PagoEfectivoRequest pagoEfectivoRequest) {
        LogSystem.info(logger, "PagoEfectivo", "Inicio");
        LogSystem.infoObject(logger, "PagoEfectivo", "pagoEfectivoRequest", pagoEfectivoRequest);

        String cip = service.genCIP(pagoEfectivoRequest.getId());
        Response<String> response = new Response<>();
        response.setResponseCode("0");
        response.setResponseMessage("CIP Generado");
        response.setResponseData(cip);

        LogSystem.infoObject(logger, "PagoEfectivo", "response", response);
        LogSystem.info(logger, "PagoEfectivo", "Fin");
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/thirdparty/pagoefectivo/get", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public ResponseEntity<PagoEfectivoConsultaResponse2> consultarPagoEfectivo(@RequestBody PagoEfectivoConsultaRequest pagoEfectivoConsultaRequest) {
        LogSystem.info(logger, "ConsultarPagoEfectivo", "Inicio");
        LogSystem.infoObject(logger, "ConsultarPagoEfectivo", "pagoEfectivoConsultaRequest", pagoEfectivoConsultaRequest);

        ResponseEntity<PagoEfectivoConsultaResponse2> response = null;
        PagoEfectivoConsultaResponse2 bewsResponse = service.consultarCIP(pagoEfectivoConsultaRequest.getNumeroCIP(), pagoEfectivoConsultaRequest.getOrderId(), pagoEfectivoConsultaRequest.getDocNumber(),true);
        if (bewsResponse == null) {
            response = new ResponseEntity<PagoEfectivoConsultaResponse2>(HttpStatus.NOT_FOUND);
        } else {
            response = new ResponseEntity<PagoEfectivoConsultaResponse2>(bewsResponse, HttpStatus.OK);
        }

        LogSystem.infoObject(logger, "ConsultarPagoEfectivo", "response", response);
        LogSystem.info(logger, "ConsultarPagoEfectivo", "Fin");
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/thirdparty/pagoefectivo/sync", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public void syncPagoEfectivo() {
       service.syncPagoEfectivo();
    }

    //@RequestMapping(value = "/thirdparty/syncCalculator", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> syncCalculator() {

        Response<String> response = new Response<>();

        try {
//			service.processCalculatorFile(new File("/temp/Data_Test_Calculadora.txt"), "Data_Test_Calculadora.txt");
            ////response = service.completeCalcSync();
//			for (int i = 1; i < 66; i++) {
//				java.util.Date startTime = new java.util.Date();
//				String fileName= String.format("calc_split_%d.txt", i);
//				System.out.printf("Iniciando %s @%2$te/%2$tm/%2$tY %2$tH:%2$tM:%2$tS.%2$tL %n", fileName, startTime);
//				service.processCalculatorFile(new File("/tmp/calc_data/"+fileName), fileName);
//				java.util.Date endTime = new Date();
//				Long timeDiff = (endTime.getTime() - startTime.getTime()) /1000;
//				System.out.printf("Archivo procesado %s finalizado @%2$te/%2$tm/%2$tY %2$tH:%2$tM:%2$tS.%2$tL in %3$d segundos%n", fileName, endTime, timeDiff);
//			}
//			service.processCalculatorFile(new File("/tmp/calc_data/calc_split_57.txt"), "calc_split_57.txt");
//			service.processCalculatorFile(new File("/tmp/Calculadora21032017100000.txt"), "Calculadora21032017100000.txt");
            service.processCalculatorFile(new File("/tmp/Data_Calculadora.txt"), "Data_Calculadora.txt");
            //response = service.completeCalcSync();

//			service.calcDiffSync();
            response.setResponseCode("0");
            response.setResponseMessage("Terminado");

        } catch (Exception e) {
            logger.error("Error syncCalculator Controller.", e);
        }

        return response;
    }
}
