package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class CalculatorResponseData {
	private String calculatorID;
	private String telefono;
	private String servicio;
	private String paquetepsorigen;
	private String paqueteorigen;
	private String cantidaddecos;
	private String rentatotal;
	private String internet;

	private String prioridad;
	private String productodestinops;
	private String svas;
	private String productodestinonombre;
	private String montofinanciado;
	private String cuotasfinanciado;
	private String codigooferta;
	
	private String lineString;
	private String fileName;
	private String msgError;
	private boolean founded;
	
	public boolean isFounded() {
		return founded;
	}
	public void setFounded(boolean founded) {
		this.founded = founded;
	}
	public String getCalculatorID() {
		return calculatorID;
	}
	public void setCalculatorID(String calculatorID) {
		this.calculatorID = calculatorID;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getServicio() {
		return servicio;
	}
	public void setServicio(String servicio) {
		this.servicio = servicio;
	}
	public String getPaquetepsorigen() {
		return paquetepsorigen;
	}
	public void setPaquetepsorigen(String paquetepsorigen) {
		this.paquetepsorigen = paquetepsorigen;
	}
	public String getPaqueteorigen() {
		return paqueteorigen;
	}
	public void setPaqueteorigen(String paqueteorigen) {
		this.paqueteorigen = paqueteorigen;
	}
	public String getCantidaddecos() {
		return cantidaddecos;
	}
	public void setCantidaddecos(String cantidaddecos) {
		this.cantidaddecos = cantidaddecos;
	}
	public String getRentatotal() {
		return rentatotal;
	}
	public void setRentatotal(String rentatotal) {
		this.rentatotal = rentatotal;
	}
	public String getInternet() {
		return internet;
	}
	public void setInternet(String internet) {
		this.internet = internet;
	}
	public String getPrioridad() {
		return prioridad;
	}
	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}
	public String getProductodestinops() {
		return productodestinops;
	}
	public void setProductodestinops(String productodestinops) {
		this.productodestinops = productodestinops;
	}
	public String getSvas() {
		return svas;
	}
	public void setSvas(String svas) {
		this.svas = svas;
	}
	public String getProductodestinonombre() {
		return productodestinonombre;
	}
	public void setProductodestinonombre(String productodestinonombre) {
		this.productodestinonombre = productodestinonombre;
	}
	public String getMontofinanciado() {
		return montofinanciado;
	}
	public void setMontofinanciado(String montofinanciado) {
		this.montofinanciado = montofinanciado;
	}
	public String getCuotasfinanciado() {
		return cuotasfinanciado;
	}
	public void setCuotasfinanciado(String cuotasfinanciado) {
		this.cuotasfinanciado = cuotasfinanciado;
	}
	public String getLineString() {
		return lineString;
	}
	public void setLineString(String lineString) {
		this.lineString = lineString;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMsgError() {
		return msgError;
	}
	public void setMsgError(String msgError) {
		this.msgError = msgError;
	}
	public String getCodigooferta() {
		return codigooferta;
	}
	public void setCodigooferta(String codigooferta) {
		this.codigooferta = codigooferta;
	}
	
}
