package pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureRequest;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureRequestStatus;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AzureRequestRowMapper implements RowMapper<AzureRequest> {

    @Override
    public AzureRequest mapRow(ResultSet rs, int rowNum) throws SQLException {

        AzureRequest azureRequest = new AzureRequest();
        azureRequest.setOrderId(rs.getString("order_id"));
        azureRequest.setOrderStatus(rs.getString("order_status"));
        azureRequest.setRegistrationDate(rs.getDate("registration_date"));
        azureRequest.setStatus(rs.getString("status"));
        azureRequest.setUsername(rs.getString("userid"));
        azureRequest.setDocnumber(rs.getString("docNumber"));
        azureRequest.setAppcode(rs.getString("appcode"));
        azureRequest.setWhatsApp(rs.getInt("whatsapp"));

        azureRequest.setStatus(AzureRequestStatus.PENDIENTE.getCode());

        return azureRequest;
    }

}
