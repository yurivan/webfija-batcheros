package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean;

public class ApplicationException extends Exception {
	private static final long serialVersionUID = 1L;

	public ApplicationException(Exception e) {
		super(e);
	}
	
	public ApplicationException(String exceptionCode) {
		super(exceptionCode);
	}
	
	protected ApplicationException() {
		super();
	}
}
