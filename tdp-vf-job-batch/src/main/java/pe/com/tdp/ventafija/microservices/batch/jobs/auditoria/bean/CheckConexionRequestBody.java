package pe.com.tdp.ventafija.microservices.batch.jobs.auditoria.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by MCG on 12/11/2018.
 */
@JsonPropertyOrder({ "Token" })
public class CheckConexionRequestBody {
    private String Token;

    @JsonProperty("Token")
    public String getToken() {
        return this.Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

}
