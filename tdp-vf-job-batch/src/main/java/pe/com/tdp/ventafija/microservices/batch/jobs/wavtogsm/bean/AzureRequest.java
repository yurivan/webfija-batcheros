package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "azure_request", schema="ibmx_a07e6d02edaf552") //glazaror se adiciona schema
@SequenceGenerator(name="myseqazure", sequenceName="ibmx_a07e6d02edaf552.azure_request_seq", catalog="ibmx_a07e6d02edaf552", schema="ibmx_a07e6d02edaf552", allocationSize=1)//glazaror

public class AzureRequest {
	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="myseqazure")//glazaror
	private Long id;
	
	@Column(name = "order_id")
	private String orderId;
	
	@Column(name = "order_status")
	private String orderStatus;
	
	@Column(name = "registration_date")
	private Date registrationDate;
	
	@Column(name = "last_update")
	private Date lastUpdate;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "msg")
	private String msg;
	
	@Column(name = "processing_time")
	private Long processingTime;
	
	@Column(name = "encrypted")
	private String encrypted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Date getRegistrationDate() {
		return registrationDate;
	}

	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Long getProcessingTime() {
		return processingTime;
	}

	public void setProcessingTime(Long processingTime) {
		this.processingTime = processingTime;
	}

	public String getEncrypted() {
		return encrypted;
	}

	public void setEncryted(String encrypted) {
		this.encrypted = encrypted;
	}
	
	
	



}
