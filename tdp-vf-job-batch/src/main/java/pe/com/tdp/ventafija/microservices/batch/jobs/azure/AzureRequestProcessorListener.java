package pe.com.tdp.ventafija.microservices.batch.jobs.azure;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.listener.ItemListenerSupport;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureRequest;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureRequestStatus;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.AzureMetadataDao;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.AzureRequestDao;

import java.util.Date;

public class AzureRequestProcessorListener extends ItemListenerSupport<AzureRequest, AzureRequest> {
    private static final Logger logger = LoggerFactory.getLogger(AzureRequestProcessorListener.class);

    private AzureRequestDao azureRequestDao;
    private AzureMetadataDao azureMetadataDao;

    public AzureRequestProcessorListener(AzureRequestDao azureRequestDao, AzureMetadataDao azureMetadataDao) {
        super();
        this.azureRequestDao = azureRequestDao;
        this.azureMetadataDao = azureMetadataDao;
    }

    @Override
    public void onProcessError(AzureRequest item, Exception e) {
        super.onProcessError(item, e);
        JSONObject objAzureRequest = new JSONObject(item);
        logger.error("onProcessError => " + String.format("Error on item: %s", objAzureRequest.toString()), e);

        if (e != null) {
            JSONObject obj = new JSONObject(e.getMessage());
            item.setMsg(obj.get("responseMessage").toString());
        } else {
            item.setMsg("Exception: null");
        }
        item.setLastUpdate(new Date());
        item.setStatus(AzureRequestStatus.ERROR.getCode());

        azureMetadataDao.setMetaData(item, item.getMsg(), e.getMessage(), "ERROR");
        azureRequestDao.update(item);
    }

}
