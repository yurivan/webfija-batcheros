package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "order", schema = "ibmx_a07e6d02edaf552")
public class Order {
    @Id
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customerid")
    private Customer customer;

    @Column(name = "commercialoperation")
    private String commercialOperation;
    @Column(name = "producttype")
    private String productType;
    @Column(name = "productcategory")
    private String productCategory;
    @Column(name = "productcode")
    private String productCode;
    @Column(name = "paymentmode")
    private String paymentMode;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "`status`")
    private String status;
    @Column(name = "statuslegacy")
    private String statusLegacy;
    @Column(name = "registrationdate")
    private Date registrationDate;
    @Column(name = "canceldescription")
    private String cancelDescription;
    @Column(name = "coordinatex")
    private BigDecimal coordinateX;
    @Column(name = "coordinatey")
    private BigDecimal coordinateY;
    @Column(name = "migrationphone")
    private String migrationPhone;
    @Column(name = "equipmentdecotype")
    private String equipmentDecoType;
    @Column(name = "sourcephone")
    private String sourcePhone;
    @Column(name = "servicetype")
    private String serviceType;
    @Column(name = "cmscustomer")
    private String cmsCustomer;
    @Column(name = "cmsservicecode")
    private String cmsServiceCode;
    @Column(name = "registeredtime")
    private Date registeredTime;
    @Column(name = "lastupdatetime")
    private Date lastUpdateTime;
    @Column(name = "campaign")
    private String campaign;
    @Column(name = "productname")
    private String productName;
    @Column(name = "promprice")
    private BigDecimal promPrice;
    @Column(name = "cashprice")
    private BigDecimal cashPrice;
    @Column(name = "linetype")
    private String lineType;
    @Column(name = "monthperiod")
    private Integer monthPeriod;
    @Column(name = "financingmonth")
    private Integer financingMonth;
    @Column(name = "financingcost")
    private BigDecimal financingCost;

    // Sprint 6 - Se adiciona campo para diferenciar el origen de la venta
    @Column(name = "appcode")
    private String appcode;

    // Sprint 9 - Se adiciona campo para diferenciar por producto al cual pertenece el SVA (solo flujo SVA)
    @Column(name = "sourceproductname")
    private String sourceProductName;

    public String getSourceProductoName() {
        return sourceProductName;
    }

    public void setSourceProductName(String sourceProductName) {
        this.sourceProductName = sourceProductName;
    }

    public String getAppcode() {
        return appcode;
    }

    public void setAppcode(String appcode) {
        this.appcode = appcode;
    }

    // Sprint 5 - propiedades adicionales nombrePuntoVenta y entidad
    @Column(name = "nompuntoventa")
    private String nombrePuntoVenta;

    @Column(name = "entidad")
    private String entidad;

    //sprint 3 - se adiciona propiedad statusAudio
    @Column(name = "statusaudio")
    private String statusAudio;

    public String getWatsonRequest() {
        return watsonRequest;
    }

    public void setWatsonRequest(String watsonRequest) {
        this.watsonRequest = watsonRequest;
    }

    @Column(name = "watsonrequest")
    private String watsonRequest;

    //Campo para la cantidad de cada estado del usuario
//	@Column(name = "quantity")
//	private String quantity;


    /* Sprint 10 */
    @Column(name = "promspeed")
    private Integer promoSpeed;
    @Column(name = "periodpromspeed")
    private Integer periodoPromoSpeed;
    @Column(name = "equiplinea")
    private String equipamientoLinea;
    @Column(name = "equipinternet")
    private String equipamientoInternet;
    @Column(name = "equiptv")
    private String equipamientoTv;
    /* Sprint 10 */

    /* Sprint 16 */
    @Column(name = "paquetizacion")
    private String paquetizacion;
    /* Sprint 16 */

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getCommercialOperation() {
        return commercialOperation;
    }

    public void setCommercialOperation(String commercialOperation) {
        this.commercialOperation = commercialOperation;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusLegacy() {
        return statusLegacy;
    }

    public void setStatusLegacy(String statusLegacy) {
        this.statusLegacy = statusLegacy;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getCancelDescription() {
        return cancelDescription;
    }

    public void setCancelDescription(String cancelDescription) {
        this.cancelDescription = cancelDescription;
    }

    public BigDecimal getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(BigDecimal coordinateX) {
        this.coordinateX = coordinateX;
    }

    public BigDecimal getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(BigDecimal coordinateY) {
        this.coordinateY = coordinateY;
    }

    public String getMigrationPhone() {
        return migrationPhone;
    }

    public void setMigrationPhone(String migrationPhone) {
        this.migrationPhone = migrationPhone;
    }

    public String getEquipmentDecoType() {
        return equipmentDecoType;
    }

    public void setEquipmentDecoType(String equipmentDecoType) {
        this.equipmentDecoType = equipmentDecoType;
    }

    public String getSourcePhone() {
        return sourcePhone;
    }

    public void setSourcePhone(String sourcePhone) {
        this.sourcePhone = sourcePhone;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getCmsCustomer() {
        return cmsCustomer;
    }

    public void setCmsCustomer(String cmsCustomer) {
        this.cmsCustomer = cmsCustomer;
    }

    public String getCmsServiceCode() {
        return cmsServiceCode;
    }

    public void setCmsServiceCode(String cmsServiceCode) {
        this.cmsServiceCode = cmsServiceCode;
    }

    public Date getRegisteredTime() {
        return registeredTime;
    }

    public void setRegisteredTime(Date registeredTime) {
        this.registeredTime = registeredTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public BigDecimal getPromPrice() {
        return promPrice;
    }

    public void setPromPrice(BigDecimal promPrice) {
        this.promPrice = promPrice;
    }

    public BigDecimal getCashPrice() {
        return cashPrice;
    }

    public void setCashPrice(BigDecimal cashPrice) {
        this.cashPrice = cashPrice;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public Integer getMonthPeriod() {
        return monthPeriod;
    }

    public void setMonthPeriod(Integer monthPeriod) {
        this.monthPeriod = monthPeriod;
    }

    public Integer getFinancingMonth() {
        return financingMonth;
    }

    public void setFinancingMonth(Integer financingMonth) {
        this.financingMonth = financingMonth;
    }

    public BigDecimal getFinancingCost() {
        return financingCost;
    }

    public void setFinancingCost(BigDecimal financingCost) {
        this.financingCost = financingCost;
    }

    @PrePersist
    public void onPrePersist() {
        this.registeredTime = new Date();
    }

    @PreUpdate
    public void onPreUpdate() {
        this.lastUpdateTime = new Date();
    }

    // Sprint 3 - estado del audio
    public String getStatusAudio() {
        return statusAudio;
    }

    public void setStatusAudio(String statusAudio) {
        this.statusAudio = statusAudio;
    }

    public String getNombrePuntoVenta() {
        return nombrePuntoVenta;
    }

    public void setNombrePuntoVenta(String nombrePuntoVenta) {
        this.nombrePuntoVenta = nombrePuntoVenta;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public Integer getPromoSpeed() {
        return promoSpeed;
    }

    public void setPromoSpeed(Integer promoSpeed) {
        this.promoSpeed = promoSpeed;
    }

    public Integer getPeriodoPromoSpeed() {
        return periodoPromoSpeed;
    }

    public void setPeriodoPromoSpeed(Integer periodoPromoSpeed) {
        this.periodoPromoSpeed = periodoPromoSpeed;
    }

    public String getEquipamientoLinea() {
        return equipamientoLinea;
    }

    public void setEquipamientoLinea(String equipamientoLinea) {
        this.equipamientoLinea = equipamientoLinea;
    }

    public String getEquipamientoInternet() {
        return equipamientoInternet;
    }

    public void setEquipamientoInternet(String equipamientoInternet) {
        this.equipamientoInternet = equipamientoInternet;
    }

    public String getEquipamientoTv() {
        return equipamientoTv;
    }

    public void setEquipamientoTv(String equipamientoTv) {
        this.equipamientoTv = equipamientoTv;
    }

    public String getPaquetizacion() {
        return paquetizacion;
    }

    public void setPaquetizacion(String paquetizacion) {
        this.paquetizacion = paquetizacion;
    }
}
