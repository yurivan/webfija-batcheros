package pe.com.tdp.ventafija.microservices.batch.jobs.auditoria.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.glassfish.jersey.logging.LoggingFeature;
import pe.com.tdp.ventafija.microservices.batch.jobs.auditoria.bean.ClientResult;
import pe.com.tdp.ventafija.microservices.batch.jobs.auditoria.bean.EmailResponseBody;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by MCG on 12/11/2018.
 */
public class EmailClient<R, T> {

    private static final Logger logger = LogManager.getLogger(EmailClient.class);

    private String url = "";

    public EmailClient(String url) {
        this.url = url;
    }

    public ClientResult<T> sendRequest(R request) {
        ClientResult<T> result = new ClientResult<>();

        try {
            String json = doRequest2(request);
            T apiResponse = getResponse2(json);

            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {
            result.setSuccess(false);
        }
        return result;
    }


    public String jerseyPOST2(URI uri, Object apiRequest) {
        Client client = ClientBuilder.newClient();
        client.register(new LoggingFeature());
        WebTarget target = client.target(UriBuilder.fromUri(uri).build());

        String json = "";

        Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("content-type", "application/json");
        Response response = invocationBuilder.post(Entity.entity(apiRequest, MediaType.APPLICATION_JSON), Response.class);
        json = response.readEntity(String.class);

        //logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, json));
        return json;
    }

    protected String doRequest2(R body) throws URISyntaxException {
        URI uri = new URI(url);
        String jsonSMS = jerseyPOST2(uri, body);
        return jsonSMS;
    }

    protected T getResponse2(String json) throws IOException {
        LogSystem.infoString(logger, "GetResponse2", "json", json);
        ObjectMapper mapper = new ObjectMapper();
        T objAtis = mapper.readValue(json, new TypeReference<EmailResponseBody>() {
        });
        return objAtis;
    }


}
