package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean.Order;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, String> {

    //Order findOneById(String id);

    @Query("select o.id from Order o where o.id = ?1 and o.status = ?2")
    String loadId(String id, String statusPendiente);

    @Query("select o.id from Order o where o.id = ?1")
    String loadId(String id);

    @Modifying
    @Query("update Order o set o.status = ?1, o.cancelDescription = ?2, o.lastUpdateTime = ?3 where o.id = ?4")
    void updateStatus(String status, String cancelDescription, Date lastUpdateTime, String id);

    @Modifying
    @Query("update Order o set o.status = ?1, o.lastUpdateTime = ?2 where o.id = ?3")
    void updateStatus(String status, Date lastUpdateTime, String id);

    @Modifying
    @Query("update Order o set o.lastUpdateTime = ?1 where o.id = ?2")
    void updateOrder(Date lastUpdateTime, String id);

    @Modifying
    @Query("update Order o set o.watsonRequest = ?1, o.lastUpdateTime = ?2 where o.id = ?3")
    void updateOrder(String watsonRequest, Date lastUpdateTime, String id);

    /*
        @Modifying
        @Query("update Order o set o.cancelDescription = ?1, o.lastUpdateTime = ?2 where o.id = ?3")
        void updateOrder(String cancelDescription, Date lastUpdateTime, String id);
    */
    List<Order> findByRegistrationDateAfterAndStatus(Date from, String status);


    @Query("select o from Order o where o.id = ?1")
    Order findByFileOrder(String id);

    @Query("select o.status from Order o where o.id = ?1")
    String loadStatusById(String orderId);

    @Query("select o.statusAudio from Order o where o.id = ?1")
    String loadStatusAudioById(String orderId);

    @Modifying
    @Query("update Order o set o.statusAudio = ?1, o.lastUpdateTime = now() where o.id = ?2")
    void updateStatusAudioOrder(String statusAudio, String id);

    /*@Modifying
    @Query("update ibmx_a07e6d02edaf552.tdp_order o set o.address_principal = ?1, o.address_referencia = ?2 where o.order_id = ?3")
    void updateAddressTdpOrder(String address, String referencia, String id);*/


    @Query(value = "select v.id_visor, v.cliente, v.dni, v.codigo_pedido, v.nombre_producto," +
            " CASE " +
            " WHEN v.estado_solicitud = 'PENDIENTE' THEN 'SOLICITADO' " +
            " WHEN v.estado_solicitud = 'PENDIENTE-PAGO' THEN 'SOLICITADO' " +
            " WHEN v.estado_solicitud = 'ENVIANDO' THEN 'SOLICITADO'  " +
            " WHEN v.estado_solicitud = '' THEN 'SOLICITADO' " +
            " WHEN v.estado_solicitud = 'EN PROCESO' THEN 'SOLICITADO'	" +

            " WHEN v.estado_solicitud = 'INGRESADO' THEN 'INGRESADO'	" +
            " WHEN v.estado_solicitud = 'SEGUIMIENTO' THEN 'INGRESADO' " +

            " WHEN v.estado_solicitud = 'CAIDA' THEN 'CAIDA' " +
            " END estado, " +
            " v.id_transaccion, o.user_atis," +
            " to_char(trunc(extract(year from v.fecha_grabacion)),'FM9999') as anio , " +
            " to_char(trunc(extract(month from v.fecha_grabacion)),'FM99') as mes, " +
            " to_char(trunc(extract(day from v.fecha_grabacion)),'FM99') as dia, " +
            " to_char(v.fecha_grabacion, 'HH12:MI AM') as hora, " +
            " v.motivo_estado, " +
            " v.telefono, " +
            " v.telefono as telefonoAsignado, " +
            " v.direccion, " +
            " o.product_precio_normal, " +
            " o.product_precio_promo, " +
            " o.order_operation_commercial, " +
            " o.product_pay_method, " +
            " o.product_pay_price, " +
            " o.product_precio_promo_mes, " +
            " o.affiliation_electronic_invoice, " +
            " o.disaffiliation_white_pages_guide, " +
            " to_char(v.fecha_grabacion, 'DD/MM/YYYY HH12:MI:SS AM') as f_venta, " +
            " o.product_tec_inter," +
            " o.product_tec_tv " +
            " from ibmx_a07e6d02edaf552.tdp_visor v " +
            " join ibmx_a07e6d02edaf552.tdp_order o on v.id_visor = o.order_id " +
            " where o.user_atis = :codigo AND v.fecha_de_llamada BETWEEN :fechaIni AND :fechaFin " +
            " and not v.estado_solicitud in('GENERANDO_CIP','TÉCNICO ASIGNADO') ", nativeQuery = true)
    List<Object[]> getReport(@Param("codigo") final String codigo,
                             @Param("fechaIni") final Timestamp fechaIni,
                             @Param("fechaFin") final Timestamp fechaFin);
//caida registro tipo caida


    @Query(value = "select element, strvalue, strfilter" +
            " from ibmx_a07e6d02edaf552.parameters " +
            " where  domain = 'REPORTCOLOR' AND category = 'ESTADO' order by auxiliar asc", nativeQuery = true)
    List<Object[]> getColoresEstado();

    @Query(value = "select element, strvalue" +
            " from ibmx_a07e6d02edaf552.parameters " +
            " where  domain = 'REPORTCOLOR' AND category = 'MES'", nativeQuery = true)
    List<Object[]> getColoresMes();

    @Query(value = "select peticion, status_toa" +
            " from ibmx_a07e6d02edaf552.toa_status " +
            " where  status_toa IN ('IN_TOA','WO_COMPLETED') AND peticion IN :codeList", nativeQuery = true)
    List<Object[]> getPedidosEstadosToa(@Param("codeList") List<String> codeList);

    @Query(value = "select codigo_pedido, estado, tipo_caida" +
            " from ibmx_a07e6d02edaf552.pedidos_en_caida " +
            " where   codigo_pedido IN :codeList AND (estado = true OR estado IS null)  order by id ", nativeQuery = true)
    List<Object[]> getPedidosEnCaida(@Param("codeList") List<String> codeList);

}
