package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean;

import java.util.Date;

public class SaleApiFirebaseResponseBody {

    private String id;
    private String orderId;
    private String client_doc_number;
    private String vendor_id;
    private Date registeredtime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getClient_doc_number() {
        return client_doc_number;
    }

    public void setClient_doc_number(String client_doc_number) {
        this.client_doc_number = client_doc_number;
    }

    public String getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(String vendor_id) {
        this.vendor_id = vendor_id;
    }

    public Date getRegisteredtime() {
        return registeredtime;
    }

    public void setRegisteredtime(Date registeredtime) {
        this.registeredtime = registeredtime;
    }
}