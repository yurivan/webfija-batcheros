package pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean;

public enum AzureRequestStatus {
    OK("OK"),
    ERROR("ER"),
    TESTING_XX("XX"),
    PENDIENTE("PE"),
    WHATSAPP("WS");

    private String code;

    private AzureRequestStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
