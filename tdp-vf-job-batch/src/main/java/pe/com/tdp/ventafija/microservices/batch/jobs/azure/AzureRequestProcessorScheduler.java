package pe.com.tdp.ventafija.microservices.batch.jobs.azure;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.service.AzureMetadataService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Component
public class AzureRequestProcessorScheduler {

    private static final Logger logger = LogManager.getLogger(AzureRequestProcessorScheduler.class);

    private DateTimeZone dateTimeZone = DateTimeZone.forID("America/Lima");

    @Autowired
    private JobLauncher jobLauncher;
    @Autowired
    private Job azureRequestProcessor;
    @Autowired
    private AzureMetadataService azureMetadataService;
    @Value("${azure.batch.sync.stop}")
    String finBatch;

    //@Scheduled(cron = "0 */5 * * * ?")
    @Scheduled(cron = "${azure.batch.sync.cron}")
    public void startJob() {
        try {
            System.out.println("---------------");
            LogSystem.info(logger, "startJob", "Starting azure sync..!");

            DateTime dateTime = new DateTime(new Date());
            DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);

            DateFormat dateFormat = new SimpleDateFormat("HH:mm");
            String timeString = dateFormat.format(dateTimeWithZone.toDate());

            SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
            Date timeActual = parser.parse(timeString);

            Date timeFin  = parser.parse(finBatch);

            if ((timeActual.after(timeFin))) {
                LogSystem.info(logger, "startJob", "No se ejecutara el batch por generacion del excel");
                System.out.println("---------------");
                return;
            }

            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            c.add(Calendar.DAY_OF_MONTH, 1);
            c.add(Calendar.MONTH, 11);
            c.add(Calendar.HOUR_OF_DAY, 1);
            Date job = c.getTime();

            JobParameters params = new JobParametersBuilder().addLong("time", job.getTime()).toJobParameters();
            JobExecution execution = jobLauncher.run(azureRequestProcessor, params);
            LogSystem.info(logger, "startJob", String.format("Azure batch excecution started at %s and finished at %s wit status %s", execution.getStartTime(), execution.getEndTime(), execution.getExitStatus()));

        } catch (Exception e) {
            LogSystem.error(logger, "startJob", e.getMessage(), e);
        }
        System.out.println("---------------");
    }


    @Scheduled(cron = "${azure.batch.sync.metadata}")
    public void runSecondJob() {
        LogSystem.info(logger, "startJob", "No se ejecutara el batch por generacion del excel");
        DateTime dateTime = new DateTime(new Date());
        DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String azureDate = dateFormat.format(dateTimeWithZone.toDate());
        azureMetadataService.createMetadata(azureDate);
        LogSystem.info(logger, "startJob", String.format("Create excel batch excecution finished"));
    }

    //@Scheduled(cron = "0 33 15 * * ?")
    @Scheduled(cron = "${azure.batch.sync.reprocess}")
    public void runThirdJob() {
        LogSystem.info(logger, "startJob", "Se ejecuta el proceso para volver a pendiente");
        DateTime dateTime = new DateTime(new Date());
        DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone).minusDays(1);
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String azureDate = dateFormat.format(dateTimeWithZone.toDate());
        azureMetadataService.reprocesar(azureDate);
        LogSystem.info(logger, "startJob", String.format("updateNotProccessed batch excecution finished"));
    }

}
