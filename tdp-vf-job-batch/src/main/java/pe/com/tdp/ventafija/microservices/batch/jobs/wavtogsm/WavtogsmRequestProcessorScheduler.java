package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import java.util.Date;

@Component
public class WavtogsmRequestProcessorScheduler {

    private static final Logger logger = LogManager.getLogger(WavtogsmRequestProcessorScheduler.class);

    @Autowired
    private WavtogsmRequestProcessor wavtogsmRequestProcessor;

    //@Scheduled(cron = "0 */1 * * * ?")
    @Scheduled(cron = "${wavtogsm.api.sync.cron}")
    public void startJob() {
        try {
            System.out.println("---------------");
            LogSystem.info(logger, "startJob", "Starting wavtogsm sync..!");

            DateTimeZone dateTimeZone = DateTimeZone.forID("America/Lima");
            DateTime dateTime = new DateTime(new Date());
            DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);

            String[] process = wavtogsmRequestProcessor.process();
            LogSystem.info(logger, "startJob", String.format("Wavtogsm batch excecution started at %s and finished at %s wit status %s", dateTimeWithZone.toDate(), process[0], process[1]));

        } catch (Exception e) {
            LogSystem.error(logger, "startJob", e.getMessage(), e);
        }
        System.out.println("---------------");
    }
}
