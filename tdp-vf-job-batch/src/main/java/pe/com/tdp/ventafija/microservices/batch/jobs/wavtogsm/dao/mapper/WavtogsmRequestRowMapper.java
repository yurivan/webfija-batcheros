package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean.SaleApiFirebaseResponseBody;

import java.sql.ResultSet;
import java.sql.SQLException;

public class WavtogsmRequestRowMapper implements RowMapper<SaleApiFirebaseResponseBody> {

    @Override
    public SaleApiFirebaseResponseBody mapRow(ResultSet rs, int rowNum) throws SQLException {

        SaleApiFirebaseResponseBody saleApiFirebaseResponseBody = new SaleApiFirebaseResponseBody();
        saleApiFirebaseResponseBody.setId(rs.getString("order_id"));
        saleApiFirebaseResponseBody.setOrderId(rs.getString("id"));
        saleApiFirebaseResponseBody.setClient_doc_number(rs.getString("docnumber"));
        saleApiFirebaseResponseBody.setVendor_id(rs.getString("userid"));
        saleApiFirebaseResponseBody.setRegisteredtime(rs.getDate("registeredtime"));

        return saleApiFirebaseResponseBody;
    }

}
