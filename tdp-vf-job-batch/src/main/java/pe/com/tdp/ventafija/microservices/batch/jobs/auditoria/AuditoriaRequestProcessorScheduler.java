package pe.com.tdp.ventafija.microservices.batch.jobs.auditoria;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import java.util.Date;

@Component
public class AuditoriaRequestProcessorScheduler {

    private static final Logger logger = LogManager.getLogger(AuditoriaRequestProcessorScheduler.class);

    @Autowired
    private AuditoriaRequestProcessor auditoriaRequestProcessor;

    //@Scheduled(cron = "0 0 6 ? * SUN-SAT")
    //@Scheduled(cron = "0 */1 * * * ?")
    @Scheduled(cron = "${auditoria.api.sync.cron}")
    public void startJob() {
        try {
            System.out.println("---------------");
            LogSystem.info(logger, "startJob", "Starting CustodiaValidate sync..!");

            DateTimeZone dateTimeZone = DateTimeZone.forID("America/Lima");
            DateTime dateTime = new DateTime(new Date());
            DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);

            String[] process = auditoriaRequestProcessor.process();
            LogSystem.info(logger, "startJob", String.format("CustodiaValidate batch excecution started at %s and finished at %s wit status %s", dateTimeWithZone.toDate(), process[0], process[1]));

        } catch (Exception e) {
            LogSystem.error(logger, "startJob", e.getMessage(), e);
        }
        System.out.println("---------------");
    }
}
