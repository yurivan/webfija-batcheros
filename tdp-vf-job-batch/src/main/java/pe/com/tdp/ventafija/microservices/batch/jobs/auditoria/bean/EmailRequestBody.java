package pe.com.tdp.ventafija.microservices.batch.jobs.auditoria.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Felix on 19/06/2017.
 */
@JsonPropertyOrder({ "Email", "ActionId", "Message", "Token", "Subject" })
public class EmailRequestBody {
    private String Email;
    private String ActionId;
    private String Message;
    private String Token;
    private String Subject;

    @JsonProperty("Email")
    public String getEmail() {
        return this.Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    @JsonProperty("ActionId")
    public String getActionId() {
        return this.ActionId;
    }

    public void setActionId(String ActionId) {
        this.ActionId = ActionId;
    }

    @JsonProperty("Message")
    public String getMessage() {
        return this.Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    @JsonProperty("Token")
    public String getToken() {
        return this.Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

    @JsonProperty("Subject")
    public String getSubject() {
        return this.Subject;
    }

    public void setSubject(String Subject) {
        this.Subject = Subject;
    }
}

