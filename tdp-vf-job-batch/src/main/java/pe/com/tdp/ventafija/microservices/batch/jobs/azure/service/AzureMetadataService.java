package pe.com.tdp.ventafija.microservices.batch.jobs.azure.service;

import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.ListBlobItem;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.AzureRequestProcessorScheduler;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureMetadata;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureRequest;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.AzureMetadataDao;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.telefonica.everis.common.azure.AzureApi;
import pe.com.telefonica.everis.common.google.GoogleApi;

import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class AzureMetadataService {

    private static final Logger logger = LogManager.getLogger(AzureRequestProcessorScheduler.class);

    private DateTimeZone dateTimeZone = DateTimeZone.forID("America/Lima");

    private AzureApi azureApi;
    private AzureMetadataDao metadataDao;
    private String legacyConnection;

    private String fileExtension2;
    private String fileExtension3;
    private Boolean uploadImage;

    private JSONObject metadataLegacy;

    public AzureMetadataService(AzureApi azureApi, AzureMetadataDao metadataDao,
                                 String legacyConnection, String fileExtension2, String fileExtension3, Boolean uploadImage) {
        super();
        this.metadataDao = metadataDao;
        this.azureApi = azureApi;

        this.legacyConnection = legacyConnection;
        this.fileExtension2 = fileExtension2;
        this.fileExtension3 = fileExtension3;
        this.uploadImage = uploadImage;
    }

    /*
    private void processMetadata(AzureRequest item, AzureMetadata metadata, String azureDate, String filename) throws URISyntaxException, StorageException, IOException {
        LogSystem.info(logger, item.getOrderId() + ":: ProcessMetadata", "Inicio");
        LogSystem.infoString(logger, item.getOrderId() + ":: ProcessMetadata", "filename", filename);

        if (filename.equalsIgnoreCase("")) {
            LogSystem.info(logger, item.getOrderId() + ":: ProcessMetadata", "Fin");
            return;
        }

        String excelExtension = ".xlsx";
        String excelFilename = azureDate + excelExtension;
        File excelLegacy = File.createTempFile("legacy", "tmp");

        CloudBlobContainer containerLegacy = azureApi.createBlobContainer(item.getOrderId(), legacyConnection, azureDate);
        CloudBlob azureExcelLegacy = containerLegacy.getBlockBlobReference(excelFilename);
        azureExcelLegacy.getProperties().setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        if (!azureExcelLegacy.exists()) {
            metadataCreateLegacy(item.getOrderId(), excelLegacy, metadata, filename);
        } else {
            azureExcelLegacy.downloadToFile(excelLegacy.getAbsolutePath());
            metadataUpdateLegacy(item.getOrderId(), excelLegacy, metadata, filename);
        }
        azureExcelLegacy.uploadFromFile(excelLegacy.getAbsolutePath());

        if (excelLegacy != null) {
            if (excelLegacy.delete()) {
                LogSystem.infoString(logger, item.getOrderId() + ":: ProcessMetadata", "excelLegacy", "Eliminado: " + excelLegacy.getAbsolutePath());
            }
        }

        LogSystem.info(logger, item.getOrderId() + ":: ProcessMetadata", "Fin");
    }
*/
    private void metadataCreateLegacy(File excelFile, List<AzureMetadata> metadata) {

        try (FileOutputStream out = new FileOutputStream(excelFile); XSSFWorkbook workbook = new XSSFWorkbook()) {

            XSSFSheet sheet = workbook.createSheet("Metadata");
            XSSFFont font = workbook.createFont();
            font.setBold(true);
            font.setColor(IndexedColors.WHITE.index);

            XSSFCellStyle style = workbook.createCellStyle();
            style.setFont(font);
            style.setFillForegroundColor(IndexedColors.BLUE_GREY.index);
            style.setFillPattern(FillPatternType.SOLID_FOREGROUND);

             Object[] cabecera=new Object[]{"ID_GRABACION", "IDENTIFICADOR_UNICO", "CANAL_VENTA_PROV",
                            "NUMERO_CELULAR_PROV", "NUMERO_LINEA_PROV", "FECHA_DOCUMENTO_PROV",
                            "NOMBRE_PROVEEDOR_PROV", "NOMBRE_CAMPA\u00D1A_PROV", "NUMERO_DOCUMENTO_PROV",
                            "NOMBRE_CLIENTE_PROV", "NEGOCIO_PROV", "DESAFILIACION_DE_PAGINAS_BLANCAS_PROV",
                            "AFILIACION_FACTURA_DIGITAL_PROV", "PROTECCION_DE_DATOS_PROV"};

            Map<String, Object[]> data = new HashMap<>();

            Integer i = 1;
            for (AzureMetadata item : metadata){
                LogSystem.info(logger, item.getFilename() + ":: MetadataCreateLegacy", i.toString()+" data puted successfully...");
                data.put(i.toString(), new Object[]{ item.getFilename(), item.getId(), item.getChannel(),
                item.getPhoneNumber(),item.getTelephoneNumber(),new SimpleDateFormat("dd/MM/yyyy").format(item.getDocumentDate()),
                item.getProviderName(),item.getCampaignName(),item.getDocumentNumber(),
                item.getCustomerName(),item.getBusiness(),item.getDisableWhitePage(),
                item.getEnableDigitalInvoice(),item.getDataProtection()});
                i++;
            }

            int rownum = 0;

            Row row_cabecera = sheet.createRow(rownum++);

            int cellnum = 0;
            for (Object obj : cabecera) {
                Cell cell = row_cabecera.createCell(cellnum++);
                if (obj instanceof String)
                    cell.setCellValue((String)obj);
                else if (obj instanceof Integer)
                    cell.setCellValue((Integer)obj);
                else if (obj instanceof SimpleDateFormat)
                    cell.setCellValue(obj.toString());

                cell.setCellStyle(style);
            }

            Set<String> keyset = data.keySet();

            for (String key : keyset) {
                Row row = sheet.createRow(rownum++);
                Object[] objArr = data.get(key);
                LogSystem.info(logger,  ":: MetadataWriteLegacy", rownum +" row puted successfully...");
                cellnum = 0;
                for (Object obj : objArr) {
                    Cell cell = row.createCell(cellnum++);
                    if (obj instanceof String)
                        cell.setCellValue((String)obj);
                    else if (obj instanceof Integer)
                        cell.setCellValue((Integer)obj);
                    else if (obj instanceof SimpleDateFormat)
                        cell.setCellValue(obj.toString());

                }
            }

            workbook.write(out);
            out.close();
            LogSystem.info(logger, "" + ":: MetadataCreateLegacy", "Excel created successfully...");

        } catch (FileNotFoundException e) {
            LogSystem.error(logger, "" + ":: MetadataCreateLegacy", "FileNotFoundException", e);
        } catch (IOException e) {
            LogSystem.error(logger, "" + ":: MetadataCreateLegacy", "IOException", e);
        }
    }


    public void createMetadata(String date) {
        CloudBlobContainer containerLegacy = azureApi.createBlobContainer("", legacyConnection, date);
        CloudBlobContainer containerLegacyBK = azureApi.createBlobContainer("", legacyConnection, "666");
        File excelLegacy;
        CloudBlob azureExcelLegacy;
        CloudBlob azureExcelLegacyBK;
        try{
            azureExcelLegacy = containerLegacy.getBlockBlobReference(date +".xlsx");
            azureExcelLegacyBK = containerLegacyBK.getBlockBlobReference(date +"-BK.xlsx");
            azureExcelLegacy.getProperties().setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            excelLegacy = File.createTempFile("legacy", "tmp");
            metadataDao.createLogMetadata(date,"INIT");
            LogSystem.infoString(logger, "Init Process", "Process", "");

            LogSystem.infoString(logger, "GetMetadata archivos", "archivos", "INIT");

            DateTime dateTime = new DateTime(new Date());
            DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String whereDate = dateFormat.format(dateTimeWithZone.toDate());

            List<AzureMetadata> data = metadataDao.getMetadataByCodigoPedido(whereDate);

            LogSystem.infoString(logger, "GetMetadata archivos", "archivos", "FIN");

            metadataCreateLegacy(excelLegacy, data);

            azureExcelLegacy.uploadFromFile(excelLegacy.getAbsolutePath());
            azureExcelLegacyBK.uploadFromFile(excelLegacy.getAbsolutePath());
            metadataDao.createLogMetadata(date,"OK");

        }catch (Exception e){
            LogSystem.infoString(logger, "Error al crear metadata","date",date);
            metadataDao.createLogMetadata(date,"ERROR");
        }
    }

    public  void reprocesar(String date){
        CloudBlobContainer containerLegacy = azureApi.createBlobContainer("", legacyConnection, date);
        metadataDao.createLogReproccess(date,"INIT");
        try{
            String whereUpdated="";
            for (ListBlobItem fileUnproccessed : containerLegacy.listBlobs()){
                String blobName = blobNameFromUri(fileUnproccessed.getUri());
                if(!blobName.substring(blobName.length()-3,blobName.length()).equals("gsm")
                && !blobName.substring(blobName.length()-3,blobName.length()).equals("pdf")){
                    continue;
                }
                String codigo;
                if (blobName.charAt(blobName.length()-6)=='_'){
                    codigo = blobName.substring(0,blobName.length()-6);
                }else{
                    codigo = blobName.substring(0,blobName.length()-4);
                }
                whereUpdated+=("'"+codigo+"'"+",");
            }
            if (!whereUpdated.equals("")){
                whereUpdated=whereUpdated.substring(0,whereUpdated.length()-1);
                metadataDao.updateNotProccesed(whereUpdated);
            }

            metadataDao.createLogReproccess(date,"OK");

        }catch (Exception e){
           LogSystem.infoString(logger, "Error al updateNotProccessed","date",date);
            metadataDao.createLogReproccess(date,"ERROR");
        }
    }


    private static String blobNameFromUri(URI uri) {
        String path = uri.getPath();
        String[] splits = path.split("/", 3);
        return splits[2];
    }
}
