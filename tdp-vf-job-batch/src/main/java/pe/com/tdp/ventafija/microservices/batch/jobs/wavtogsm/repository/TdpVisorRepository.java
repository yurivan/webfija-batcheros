package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean.TdpVisor;

import java.util.List;

public interface TdpVisorRepository extends JpaRepository<TdpVisor, String> {

    List<TdpVisor> findAllByCodigoPedidoAndDni(String codigoPedido, String dni);

    TdpVisor findOneByIdVisor(String idVisor);

    List<TdpVisor> findAllByDepartamentoAndProvinciaAndDistritoAndTipoDocumentoAndDniAndNombreProductoAndOperacionComercial(String departamento, String provincia, String distrito, String tipoDocumento, String dni, String nombreProducto, String operacionComercial);

    @Query(value = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_visor WHERE tipo_documento = ?1 AND dni = ?2 AND UPPER(operacion_comercial) IN ?3 AND fecha_grabacion > now() - INTERVAL '2 month' AND UPPER(estado_solicitud) != 'CAIDA'", nativeQuery = true)
    List<TdpVisor> findAllByTipoDocumentoAndDni(String tipoDocumento, String dni, List<String> operacionComercial);
}
