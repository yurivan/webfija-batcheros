package pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureMetadata;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureRequest;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.mapper.AzureMetadataRowMapper;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import javax.sql.DataSource;
import java.util.List;

@Repository
public class AzureMetadataDao {
    private static final Logger logger = LogManager.getLogger(AzureMetadataDao.class);

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public AzureMetadataDao(DataSource ds) {
        super();
        jdbcTemplate = new JdbcTemplate(ds);
    }

    public AzureMetadata getMetadata(AzureRequest request) {
        LogSystem.info(logger, request.getOrderId() + ":: GetMetadata", "Inicio");
        AzureMetadata metadata = null;

        String query = "SELECT " +
                "'file' AS \"ID_GRABACION\", " +
                "VIS.id_visor AS \"ORDER_ID\", " +
                "CUS.docNumber AS \"DNI\", " +
                "VIS.codigo_pedido AS \"IDENTIFICADOR_UNICO\", " +
                "CASE WHEN COALESCE(VEN.canal, 'VACIO') = '' THEN 'VACIO' ELSE COALESCE(VEN.canal, 'VACIO') END AS \"CANAL_VENTA\", " +
                "CASE WHEN COALESCE(CUS.customerPhone, 'VACIO') = '' THEN 'VACIO' ELSE COALESCE(CUS.customerPhone, 'VACIO') END AS \"NUMERO_CELULAR\", " + //Nro Cel Cliente
                "CASE WHEN COALESCE(ORD.migrationPhone, 'VACIO') = '' THEN 'VACIO' ELSE COALESCE(ORD.migrationPhone, 'VACIO') END AS \"NUMERO_LINEA\", " +
                "VIS.fecha_grabacion AS \"FECHA_DOCUMENTO\", " +
                "'APPVENTAFIJA' AS \"NOMBRE_PROVEEDOR\", " +
                "COALESCE(ORD.campaign, '') AS \"NOMBRE_CAMPANIA\", " +
                "CUS.docNumber AS \"NUMERO_DOCUMENTO\", " +
                "CASE WHEN REPLACE(CONCAT(CUS.firstName, ' ', CUS.lastName1, ' ', CUS.lastName2), ' ', '') = '' THEN 'VACIO' ELSE CONCAT(CUS.firstName, ' ', CUS.lastName1, ' ', CUS.lastName2) END AS \"NOMBRE_CLIENTE\", " +
                "'MOVIL' AS \"NEGOCIO\", " +
                "UPPER(ODET.disableWhitePage) AS \"DESAFILIACION_DE_PAGINAS_BLANCAS\", " +
                "UPPER(ODET.enableDigitalInvoice) AS \"AFILIACION_FACTURA_DIGITAL\", " +
                "UPPER(ODET.dataProtection) AS \"PROTECCION_DE_DATOS\", " +
                "'' AS \"DETALLE_PROVEEDOR\", " +
                "'AUDIO' AS \"TIPO_ARCHIVO\", " +
                "AZU.status AS \"STATUS\" " +
                "FROM ibmx_a07e6d02edaf552.tdp_visor VIS " +
                "LEFT JOIN ibmx_a07e6d02edaf552.order ORD ON ORD.ID = VIS.id_visor " +
                "LEFT JOIN ibmx_a07e6d02edaf552.customer CUS ON CUS.ID = ORD.customerID " +
                "LEFT JOIN ibmx_a07e6d02edaf552.tdp_sales_agent VEN ON VEN.codATIS = ORD.userID " +
                "LEFT JOIN ibmx_a07e6d02edaf552.order_detail ODET ON ODET.ORDERID = VIS.id_visor " +
                "LEFT JOIN ibmx_a07e6d02edaf552.azure_request AZU ON AZU.order_id = VIS.id_visor " +
                "WHERE VIS.id_visor = ? " +
                "LIMIT 1";

        try {
            LogSystem.infoQuery(logger, "getMetadata", "query", query.replace("?", "'" + request.getOrderId().replace("_motorizado", "") + "'"));

            Object search[] = {request.getOrderId().replace("_motorizado", "")};
            List<AzureMetadata> result = jdbcTemplate.query(query, search, new AzureMetadataRowMapper());
            if (result != null && !result.isEmpty()) {
                metadata = result.get(0);
            }
        } catch (Exception e) {
            LogSystem.error(logger, request.getOrderId() + ":: GetMetadata", e.getMessage(), e);
        }
        LogSystem.info(logger, request.getOrderId() + ":: GetMetadata", "Fin");
        return metadata;
    }

    public void setMetaData(AzureRequest request, String msg, String response, String code) {
        request.setMsg(null);
        String query = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events "
                + "(event_datetime, tag, username, msg, orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp, sourceAppVersion, result) "
                + "VALUES (CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            LogSystem.infoQuery(logger, "setMetaData", "query", query);

            JSONObject objRequest = new JSONObject(request);
            jdbcTemplate.update(query, "SERVICE_CALL", request.getUsername(), msg, request.getOrderId().replace("_motorizado", ""), request.getDocnumber(), "AUDIOAZURE", request.getServiceUrl(), objRequest.toString(), response, "AZURE", "1.0", code);

        } catch (Exception e) {
            logger.error("Error setMetaData Dao", e);
        }
    }

    public List<AzureMetadata> getMetadataByCodigoPedido(String date){
        LogSystem.info(logger, date + ":: GetMetadata", "Inicio");
        List<AzureMetadata> metadata=null;
        String query = "SELECT DISTINCT\n" +
                "\tfiles.archivo AS \"ID_GRABACION\",\n" +
                "VIS.id_visor AS \"ORDER_ID\", " +
                "CUS.docNumber AS \"DNI\", " +
                "\tVIS.codigo_pedido AS IDENTIFICADOR_UNICO,\n" +
                "CASE\n" +
                "\t\tWHEN COALESCE ( VEN.canal, 'VACIO' ) = '' THEN\n" +
                "\t\t'VACIO' ELSE COALESCE ( VEN.canal, 'VACIO' ) \n" +
                "\tEND AS CANAL_VENTA,\n" +
                "CASE\n" +
                "\t\tWHEN COALESCE ( CUS.customerPhone, 'VACIO' ) = '' THEN\n" +
                "\t\t'VACIO' ELSE COALESCE ( CUS.customerPhone, 'VACIO' ) \n" +
                "\tEND AS NUMERO_CELULAR,\n" +
                "CASE\n" +
                "\t\tWHEN COALESCE ( ORD.migrationPhone, 'VACIO' ) = '' THEN\n" +
                "\t\t'VACIO' ELSE COALESCE ( ORD.migrationPhone, 'VACIO' ) \n" +
                "\tEND AS NUMERO_LINEA,\n" +
                "\tVIS.fecha_grabacion AS FECHA_DOCUMENTO,\n" +
                "\t'APPVENTAFIJA' AS NOMBRE_PROVEEDOR,\n" +
                "\tCOALESCE ( ORD.campaign, '' ) AS NOMBRE_CAMPANIA,\n" +
                "\tCUS.docNumber AS NUMERO_DOCUMENTO,\n" +
                "CASE\n" +
                "\t\tWHEN REPLACE ( CONCAT ( CUS.firstName, ' ', CUS.lastName1, ' ', CUS.lastName2 ), ' ', '' ) = '' THEN\n" +
                "\t\t'VACIO' ELSE CONCAT ( CUS.firstName, ' ', CUS.lastName1, ' ', CUS.lastName2 ) \n" +
                "\tEND AS NOMBRE_CLIENTE,\n" +
                "\t'MOVIL' AS NEGOCIO,\n" +
                "\tUPPER ( ODET.disableWhitePage ) AS DESAFILIACION_DE_PAGINAS_BLANCAS,\n" +
                "\tUPPER ( ODET.enableDigitalInvoice ) AS AFILIACION_FACTURA_DIGITAL,\n" +
                "\tUPPER ( ODET.dataProtection ) AS PROTECCION_DE_DATOS \n," +
                "'' AS \"DETALLE_PROVEEDOR\", " +
                "AZU.status AS \"STATUS\" " +
                "FROM\n" +
                "\t((\n" +
                "\t\tSELECT DISTINCT\n" +
                "\t\t\tSPLIT_PART( serviceresponse, '\"', 6 ) AS archivo,\n" +
                "\t\t\torderid \n" +
                "\t\tFROM\n" +
                "\t\t\tibmx_a07e6d02edaf552.service_call_events \n" +
                "\t\tWHERE\n" +
                "\t\t\tservicecode = 'AUDIOAZURE' \n" +
                "\t\t\tAND msg = 'OK' \n" +
                "\t\t\tAND serviceresponse LIKE '{\"archivos\":{%.pdf%' \n" +
                "\t\t\tAND event_datetime > '"+date+" 00:00:00' \n" +
                "\t\t\tAND event_datetime < '"+date+" 23:59:59' \n" +
                "\t\t\t) UNION ALL\n" +
                "\t\t(\n" +
                "\t\tSELECT DISTINCT\n" +
                "\t\t\tSPLIT_PART( serviceresponse, '\"', 10 ) AS archivo,\n" +
                "\t\t\torderid \n" +
                "\t\tFROM\n" +
                "\t\t\tibmx_a07e6d02edaf552.service_call_events \n" +
                "\t\tWHERE\n" +
                "\t\t\tservicecode = 'AUDIOAZURE' \n" +
                "\t\t\tAND msg = 'OK' \n" +
                "\t\t\tAND serviceresponse LIKE'{\"archivos\":{%.gsm%' \n" +
                "\t\t\tAND event_datetime > '"+date+" 00:00:00' \n" +
                "\t\tAND event_datetime < '"+date+" 23:59:59' \n" +
                "\t)) files\n" +
                "\tJOIN ibmx_a07e6d02edaf552.tdp_visor VIS ON VIS.id_visor = files.orderid  \n" +
                "\tLEFT JOIN ibmx_a07e6d02edaf552.order ORD ON ORD.ID = VIS.id_visor  \n" +
                "\tLEFT JOIN ibmx_a07e6d02edaf552.customer CUS ON CUS.ID = ORD.customerID  \n" +
                "\tLEFT JOIN ibmx_a07e6d02edaf552.tdp_sales_agent VEN ON VEN.codATIS = ORD.userID  \n" +
                "\tLEFT JOIN ibmx_a07e6d02edaf552.order_detail ODET ON ODET.ORDERID = VIS.id_visor " +
                "LEFT JOIN ibmx_a07e6d02edaf552.azure_request AZU ON AZU.order_id = VIS.id_visor ";

        try {
            metadata = jdbcTemplate.query(query,new AzureMetadataRowMapper());
        } catch (Exception e) {
            LogSystem.error(logger, date + ":: GetMetadata", e.getMessage(), e);
        }
        LogSystem.info(logger, date + ":: GetMetadata", "Fin");
        return metadata;
    }


    public void createLogMetadata(String date,String msg) {
        String query = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events "
                + "(event_datetime, tag, username, msg, orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp, sourceAppVersion, result) "
                + "VALUES (CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            LogSystem.infoQuery(logger, "setMetaData", "query", query);
            jdbcTemplate.update(query, "SERVICE_CALL", "METADATA", msg,"","", "METADATA", "",date,msg, "AZURE", "1.0",msg);

        } catch (Exception e) {
            logger.error("Error setMetaData Dao", e);
        }
    }
    public void createLogReproccess(String date,String msg) {
        String query = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events "
                + "(event_datetime, tag, username, msg, orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp, sourceAppVersion, result) "
                + "VALUES (CURRENT_TIMESTAMP, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            LogSystem.infoQuery(logger, "setMetaData", "query", query);
            jdbcTemplate.update(query, "SERVICE_CALL", "REPROCCESS", msg,"","", "REPROCCESS", "",date,msg, "AZURE", "1.0",msg);

        } catch (Exception e) {
            logger.error("Error setMetaData Dao", e);
        }
    }


    public void updateNotProccesed(String where) {
        String query = "UPDATE ibmx_a07e6d02edaf552.azure_request SET status = 'PE' " +
                        "WHERE order_id IN (" +
                        "   SELECT id_visor" +
                        "   FROM ibmx_a07e6d02edaf552.tdp_visor" +
                        "   WHERE codigo_pedido IN ("+where+")" +
                        ")";

        try {
            LogSystem.info(logger, "updateNotProccessed",  query);
            jdbcTemplate.update(query);
        } catch (Exception e) {
            logger.error("Error updateNotProccessed", e);
        }
    }
}
