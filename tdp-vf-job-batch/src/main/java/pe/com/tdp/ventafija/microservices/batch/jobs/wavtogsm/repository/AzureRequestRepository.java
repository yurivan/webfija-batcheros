package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean.AzureRequest;

import java.util.List;

public interface AzureRequestRepository extends JpaRepository<AzureRequest, Integer> {

    List<AzureRequest> findByOrderId(String orderId);

    AzureRequest findOneByOrderId(String orderId);

}