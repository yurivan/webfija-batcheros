package pe.com.tdp.ventafija.microservices.batch.jobs;

public class RequestProcessorException extends Exception {
    private static final long serialVersionUID = 1L;

    public RequestProcessorException(String message) {
        super(message);
    }

    public RequestProcessorException(String message, Throwable cause) {
        super(message, cause);
    }

}
