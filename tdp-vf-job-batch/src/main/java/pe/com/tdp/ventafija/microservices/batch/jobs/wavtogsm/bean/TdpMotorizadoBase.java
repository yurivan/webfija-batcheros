package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean;

import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "tdp_motorizado_base", schema = "ibmx_a07e6d02edaf552")
public class TdpMotorizadoBase {

    @Id
    @Column(name = "id")
    public Integer id;
    @Column(name = "codigopedido")
    public String codigoPedido;
    @Column(name = "codmotorizado")
    public String codMotorizado;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "fecharegistro")
    public Date fechaRegistro;
    @Column(name = "tipdoccliente")
    public String tipDocCliente;
    @Column(name = "numdoccliente")
    public String numDocCliente;
    @Column(name = "nombrecliente")
    public String nombreCliente;
    @Column(name = "movilcliente")
    public String movilCliente;
    @Column(name = "biometria")
    public Integer biometria;
    @Column(name = "estado")
    public String estado;
    @Column(name = "motivorechazo")
    public String motivoRechazo;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "registeredtime")
    public Date registeredTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "updatetime")
    public Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getCodMotorizado() {
        return codMotorizado;
    }

    public void setCodMotorizado(String codMotorizado) {
        this.codMotorizado = codMotorizado;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getTipDocCliente() {
        return tipDocCliente;
    }

    public void setTipDocCliente(String tipDocCliente) {
        this.tipDocCliente = tipDocCliente;
    }

    public String getNumDocCliente() {
        return numDocCliente;
    }

    public void setNumDocCliente(String numDocCliente) {
        this.numDocCliente = numDocCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getMovilCliente() {
        return movilCliente;
    }

    public void setMovilCliente(String movilCliente) {
        this.movilCliente = movilCliente;
    }

    public Integer getBiometria() {
        return biometria;
    }

    public void setBiometria(Integer biometria) {
        this.biometria = biometria;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getMotivoRechazo() {
        return motivoRechazo;
    }

    public void setMotivoRechazo(String motivoRechazo) {
        this.motivoRechazo = motivoRechazo;
    }

    public Date getRegisteredTime() {
        return registeredTime;
    }

    public void setRegisteredTime(Date registeredTime) {
        this.registeredTime = registeredTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}