package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean.AzureRequest;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean.TdpMotorizadoBase;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean.TdpVisor;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.dao.WavtogsmDao;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.repository.*;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.connection.Firebase;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.encryption.PGPEncryptionHelper;
import pe.com.tdp.ventafija.microservices.common.util.FileUtils;
import pe.com.tdp.ventafija.microservices.common.util.Ziphelper;
import pe.com.telefonica.everis.common.google.GoogleApi;
import pe.com.telefonica.everis.common.google.GoogleApiConfig;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class WavtogsmRequestProcessor {

    private static final Logger logger = LogManager.getLogger(WavtogsmRequestProcessor.class);

    private DateTimeZone dateTimeZone = DateTimeZone.forID("America/Lima");

    @Autowired
    private WavtogsmDao wavtogsmDao;
    @Autowired
    private TdpVisorRepository tdpVisorRepository;
    @Autowired
    private TdpMotorizadoBaseRepository tdpMotorizadoBaseRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private AzureRequestRepository azureRequestRepository;

    @Autowired
    private Firebase firebase;
    @Autowired
    private GoogleStorageService googleStorageService;

    @Value("${google.api.auth.projectid}")
    private String googleProjectId;
    @Value("${google.api.storage.bucket}")
    private String googleBucket;
    @Value("${google.api.auth.credential}")
    private String googleCredential;
    @Value("${google.api.auth.applicationname}")
    private String googleApplicationName;

    @Value("${azure.max.day.without.audio}")
    private Integer maxDayWithoutAudio;

    @Bean
    public GoogleStorageService googleStorageService() throws IOException {
        return new GoogleStorageService();
    }

    @Bean
    public GoogleApiConfig googleApiConfig() {
        return new GoogleApiConfig(googleBucket, googleCredential, googleApplicationName, googleProjectId);
    }

    @Bean
    public GoogleApi googleApi() {
        return new GoogleApi(googleApiConfig());
    }

    @Bean
    public RestTemplate restTemplate() {
        Integer seconds = (int) Duration.ofSeconds(2).toMillis();
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();
        httpRequestFactory.setConnectionRequestTimeout(seconds);
        httpRequestFactory.setConnectTimeout(seconds);
        httpRequestFactory.setReadTimeout(seconds);

        return new RestTemplate(httpRequestFactory);
    }

    public String[] process() {
        LogSystem.info(logger, "Process", "Inicio");
        String[] process = new String[2];

        List<SaleApiFirebaseResponseBody> orderList = wavtogsmDao.getOrderListForAudioProcess();
        LogSystem.infoArray(logger, "process", "orderList", orderList);

        for (SaleApiFirebaseResponseBody idOrder : orderList) {
            LogSystem.infoObject(logger, "process", "idOrder", idOrder);
            try {
                LogSystem.infoString(logger, idOrder.getId() + ":: process", "status", processAudio(idOrder));
            } catch (Exception ex) {
                LogSystem.error(logger, "process", "ex", ex);
            }
        }

        DateTimeZone dateTimeZone = DateTimeZone.forID("America/Lima");
        DateTime dateTime = new DateTime(new Date());
        DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);
        process[0] = String.valueOf(dateTimeWithZone.toDate());
        process[1] = "Finished: SUCCESS";
        LogSystem.info(logger, "Process", "Fin");
        return process;
    }

    public String processAudio(SaleApiFirebaseResponseBody saleApiFirebaseResponseBody) {
        LogSystem.info(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", "Inicio");

        ServiceCallEvent event = new ServiceCallEvent();
        String orderId = saleApiFirebaseResponseBody.getId();

        if (orderId.indexOf("motorizado") > 0) {
            try {
                TdpVisor tdpVisor = tdpVisorRepository.findOneByIdVisor(orderId.replace("_motorizado", ""));
                List<TdpMotorizadoBase> tdpMotorizadoBaseList = tdpMotorizadoBaseRepository.findAllByCodigoPedidoAndNumDocCliente(tdpVisor.getCodigoPedido(), tdpVisor.getDni());
                if (tdpMotorizadoBaseList.size() > 0) {
                    saleApiFirebaseResponseBody.setVendor_id(tdpMotorizadoBaseList.get(0).getCodMotorizado());
                    saleApiFirebaseResponseBody.setClient_doc_number(tdpMotorizadoBaseList.get(0).getNumDocCliente());
                }
            } catch (Exception motorizado) {
                motorizado.printStackTrace();
            }
        }

        // Estados de tranformación de audios
        //    0   Por procesar
        //    1   Procesado
        //    2   Procesando
        //    3   Error
        String statusAudio = orderRepository.loadStatusAudioById(orderId.replace("_motorizado", ""));
        LogSystem.infoString(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", "statusAudio", statusAudio);

        if (statusAudio == null && orderId.indexOf("_motorizado") > 0) {
            LogSystem.info(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", "Fin");
            return "-1 PENDIENTE ORDEN";
        }
        /*if (statusAudio != null && Integer.parseInt(statusAudio) == 0) {
            response.setResponseCode(OrderConstants.PROCESS_AUDIO_RESPONSE_CODE_ERROR);
            response.setResponseData(OrderConstants.PROCESS_AUDIO_RESPONSE_DATA_POR_PROCESAR);
            response.setResponseMessage("00 POR PROCESAR");
        } else*/
        if (statusAudio != null && Integer.parseInt(statusAudio) == 1) {
            LogSystem.info(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", "Fin");
            return "01 PROCESADO";
            //} else if (statusAudio != null && Integer.parseInt(statusAudio) == 2) {
            //    LogSystem.info(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", "Fin");
            //    return "02 PROCESANDO";
        }/* else if (statusAudio != null && Integer.parseInt(statusAudio) == 3) {
            LogSystem.info(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", "Fin");
            return "03 ERROR";
        }*/

        String statusAudioNew = "2";
        if (!statusAudio.equalsIgnoreCase("2"))
            wavtogsmDao.updateStatusAudio(orderId, statusAudioNew);

        String orderStatusAudio = "0";

        JSONObject firebaseOriginal = new JSONObject();
        firebaseOriginal.put("audio", String.format("%s/audios/%s.zip", orderId, orderId));
        firebaseOriginal.put("images", String.format("%s/images/%s.pdf", orderId, orderId));

        event.setDocNumber(saleApiFirebaseResponseBody.getClient_doc_number());
        event.setUsername(saleApiFirebaseResponseBody.getVendor_id());
        event.setOrderId(orderId);
        event.setResult("OK");
        event.setServiceCode("WAVTOGSM");
        event.setServiceRequest(firebaseOriginal.toString());
        event.setMsg("OK");
        event.setServiceUrl("https://tefappfijaffmpeg-dev.mybluemix.net/api/v1/gsm");

        String response = null;
        try {
            response = audioReview(orderId);
            if (response == null) {
                LogSystem.infoString(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", "response", "Audio o Imagen no encontrada");

                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                c.add(Calendar.DAY_OF_MONTH, maxDayWithoutAudio);
                Date minDate = c.getTime();
                if (saleApiFirebaseResponseBody.getRegisteredtime().before(minDate)) {
                    response = "";
                    orderStatusAudio = "7";
                    event.setResult("ERROR");
                    event.setMsg("No se encontró Audio o Imagen, esta consulta superó el límite de 30 días. Se procede a colocar como AUSENTE");
                } else {
                    LogSystem.info(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", "Fin");
                    return "02 EN ESPERA DE AUDIO O IMAGEN";
                }
            } else if (response.equalsIgnoreCase("ERROR_500")) {
                orderStatusAudio = "6";
                event.setResult("ERROR");
                event.setMsg("ERROR 500 - TimeOut");
            } else {
                orderStatusAudio = "1";
            }
        } catch (Exception e) {
            LogSystem.error(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", e.getMessage(), e);
            event.setResult("ERROR");
            event.setMsg("ERROR");
            event.setServiceResponse("Error al procesar: " + e.getMessage());
            orderStatusAudio = "3";
        }
        event.setServiceResponse(response);

        wavtogsmDao.updateStatusAudio(orderId, orderStatusAudio);
        wavtogsmDao.registerEvent(new ServiceCallEvent(event));

        LogSystem.info(logger, saleApiFirebaseResponseBody.getId() + ":: ProcessAudio", "Fin");
        return response.equalsIgnoreCase("ERROR_500") ? "06: ERROR 500" : "01 PROCESADO";
    }

    public String audioReview(String orderId) {
        LogSystem.info(logger, orderId + ":: AudioReview", "Inicio");

        PGPEncryptionHelper pgpDecryptionHelper = new PGPEncryptionHelper(new ByteArrayInputStream(firebase.getPgpPrivateKeyBytes()), firebase.getPgpPassphrase());
        PGPEncryptionHelper pgpDecryptionHelper2 = new PGPEncryptionHelper(new ByteArrayInputStream(firebase.getPgpPrivateKeyBytes()), firebase.getPgpPassphrase());

        final String AUDIO_VALUE = "audio";
        final String IMAGE_VALUE = "image";

        final String audiosFirebase = String.format("%s/audios/%s.zip", orderId, orderId);
        final String imagesFirebase = String.format("%s/images/%s.pdf", orderId, orderId);

        String audioWavName = orderId + ".wav";
        File audioZipFirebase = null;
        File imagesPdfFirebase = null;
        File imagesPdfDescrypt = null;
        File audioZipDescrypt = null;
        File audioWav = null;
        File audioGsm = null;
        File audioGsmZip = null;

        JSONObject response = new JSONObject();
        JSONObject imagesResponse = new JSONObject();
        imagesResponse.put("firebase", String.format("%s/images/%s.pdf", orderId, orderId));
        imagesResponse.put("azure", String.format("%s/images/%s-original.pdf", orderId, orderId));
        imagesResponse.put("encontrado", "");
        imagesResponse.put("desencriptado", "");
        imagesResponse.put("subido", "");

        JSONObject audioResponse = new JSONObject();
        audioResponse.put("firebase", String.format("%s/audios/%s.zip", orderId, orderId));
        audioResponse.put("azure", String.format("%s/audios/%s_gsm.zip", orderId, orderId));
        audioResponse.put("encontrado", "");
        audioResponse.put("desencriptado", "");
        audioResponse.put("descomprimido", "");
        audioResponse.put("wavtogsm", "");
        audioResponse.put("comprimido", "");
        audioResponse.put("subido", "");

        response.put("images", imagesResponse);
        response.put("audio", audioResponse);

        try {
            // Descargar imagen de Firebase
            imagesPdfFirebase = googleStorageService.retrieveFile(imagesFirebase, orderId, IMAGE_VALUE);
            // Descargar audio de Firebase
            audioZipFirebase = googleStorageService.retrieveFile(audiosFirebase, orderId, AUDIO_VALUE);

            if (audioZipFirebase == null || imagesPdfFirebase == null) {

                // Eliminar ambos archivos en caso uno sea null
                if (!deleteFile(audioZipFirebase))
                    LogSystem.infoString(logger, orderId + ":: AudioReview", "audioZipFirebase: " + audioZipFirebase.getAbsolutePath(), "Delete failed!!!");
                if (!deleteFile(imagesPdfFirebase))
                    LogSystem.infoString(logger, orderId + ":: AudioReview", "imagesPdfFirebase: " + imagesPdfFirebase.getAbsolutePath(), "Delete failed!!!");

                LogSystem.info(logger, orderId + ":: AudioReview", "Fin");
                return null;
            }

            LogSystem.infoString(logger, orderId + ":: AudioReview", "audioZipFirebase", audioZipFirebase.getAbsolutePath());
            LogSystem.infoString(logger, orderId + ":: AudioReview", "imagesPdfFirebase", imagesPdfFirebase.getAbsolutePath());


            /**
             * Inicio Proceso de PDF
             */
            imagesResponse.put("encontrado", "SI");
            // Desencriptar PDF cifrado
            try {
                imagesPdfDescrypt = pgpDecryptionHelper.decrypt(imagesPdfFirebase, orderId);
                imagesResponse.put("desencriptado", "SI");
            } catch (Exception e) {
                // En caso de error asumir que el pdf está desencriptado
                imagesPdfDescrypt = imagesPdfFirebase;
                imagesResponse.put("desencriptado", "NO");
            }
            LogSystem.infoString(logger, orderId + ":: AudioReview", "imagesPdfDescrypt", "desencriptado");

            // Eliminar PDF cifrado
            if (!deleteFile(imagesPdfFirebase))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "imagesPdfFirebase: " + imagesPdfFirebase.getAbsolutePath(), "Delete failed!!!");

            // Enviar PDF desencriptado
            googleStorageService.sendFile(imagesPdfDescrypt, orderId + "/images/" + orderId + "-original.pdf", "application/pdf");
            LogSystem.infoString(logger, orderId + ":: AudioReview", "imagesPdfDescrypt", "Subido al Firebase");
            imagesResponse.put("subido", "SI");
            // Eliminar PDF desencriptado
            if (!deleteFile(imagesPdfDescrypt))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "imagesPdfDescrypt: " + imagesPdfDescrypt.getAbsolutePath(), "Delete failed!!!");

            /**
             * Inicio Proceso de Audio
             */
            audioResponse.put("encontrado", "SI");
            // Desencriptar zip cifrado de audio
            try {
                audioZipDescrypt = pgpDecryptionHelper2.decrypt(audioZipFirebase, orderId);
                audioResponse.put("desencriptado", "SI");
            } catch (Exception e) {
                // En caso de error asumir que el pdf está desencriptado
                audioZipDescrypt = audioZipFirebase;
                audioResponse.put("desencriptado", "NO");
            }
            LogSystem.infoString(logger, orderId + ":: AudioReview", "audioZipDescrypt", "Desencriptado");
            // Eliminar zip cifrado de audio
            if (!deleteFile(audioZipFirebase))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "audioZipFirebase: " + audioZipFirebase.getAbsolutePath(), "Delete failed!!!");

            // Obtener el audio WAV del zip
            Ziphelper ziphelper = new Ziphelper();
            audioWav = ziphelper.retriveFileFromZip(audioWavName, audioZipDescrypt);
            audioResponse.put("descomprimido", "SI");
            // Eliminar zip de audio
            if (!deleteFile(audioZipDescrypt))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "audioZipDescrypt: " + audioZipDescrypt.getAbsolutePath(), "Delete failed!!!");

            // Convertir audio Wav a Gsm
            audioGsm = resolverAudio(audioWav, orderId);
            if (audioGsm == null) {

                if (!deleteFile(audioWav))
                    LogSystem.infoString(logger, orderId + ":: AudioReview", "audioWav: " + audioWav.getAbsolutePath(), "Delete failed!!!");

                audioResponse.put("wavtogsm", "NO");
                LogSystem.info(logger, orderId + ":: AudioReview", "Fin");
                response.put("images", imagesResponse);
                response.put("audio", audioResponse);
                //return response.toString();
                return "ERROR_500";
            } else {
                audioResponse.put("wavtogsm", "SI");
                // Eliminar audio Wav
                if (!deleteFile(audioWav))
                    LogSystem.infoString(logger, orderId + ":: AudioReview", "audioWav: " + audioWav.getAbsolutePath(), "Delete failed!!!");

                // Comprimir a zip audio Gsm
                audioGsmZip = FileUtils.zip(audioGsm, String.format("%s.gsm", orderId));
                audioResponse.put("comprimido", "SI");
                // Eliminar audio Gsm
                if (!deleteFile(audioGsm))
                    LogSystem.infoString(logger, orderId + ":: AudioReview", "audioGsm: " + audioGsm.getAbsolutePath(), "Delete failed!!!");
                // Enviar audio gsm zip
                googleStorageService.sendZipFile(audioGsmZip, orderId);
                audioResponse.put("subido", "SI");
                LogSystem.infoString(logger, orderId + ":: AudioReview", "audioGsmZip", "Subido al Firebase");
                // Eliminar audio gsm zip
                if (!deleteFile(audioGsmZip))
                    LogSystem.infoString(logger, orderId + ":: AudioReview", "audioGsmZip: " + audioGsmZip.getAbsolutePath(), "Delete failed!!!");
            }

            /* Actualizar estado de Tabla AzureRequest */
            List<AzureRequest> lista = azureRequestRepository.findByOrderId(orderId);
            for (AzureRequest item : lista) {
                item.setEncryted("OK");
                wavtogsmDao.updateAzure(item.getOrderId());
            }

            response.put("images", imagesResponse);
            response.put("audio", audioResponse);
        } catch (Exception e) {
            LogSystem.error(logger, orderId + ":: AudioReview", "Exception " + e.getMessage(), e);

            if (!deleteFile(imagesPdfFirebase))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "Exception - imagesPdfFirebase: " + imagesPdfFirebase.getAbsolutePath(), "Delete failed!!!");
            if (!deleteFile(imagesPdfDescrypt))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "Exception - imagesPdfDescrypt: " + imagesPdfDescrypt.getAbsolutePath(), "Delete failed!!!");

            if (!deleteFile(audioZipFirebase))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "Exception - audioZipFirebase: " + audioZipFirebase.getAbsolutePath(), "Delete failed!!!");
            if (!deleteFile(audioZipDescrypt))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "Exception - audioZipDescrypt: " + audioZipDescrypt.getAbsolutePath(), "Delete failed!!!");
            if (!deleteFile(audioWav))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "Exception - audioWav: " + audioWav.getAbsolutePath(), "Delete failed!!!");
            if (!deleteFile(audioGsm))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "Exception - audioGsm: " + audioGsm.getAbsolutePath(), "Delete failed!!!");
            if (!deleteFile(audioGsmZip))
                LogSystem.infoString(logger, orderId + ":: AudioReview", "Exception - audioGsmZip: " + audioGsmZip.getAbsolutePath(), "Delete failed!!!");
        }

        LogSystem.info(logger, orderId + ":: AudioReview", "Fin");
        return response.toString();
    }

    private boolean deleteFile(File file) {
        if (file != null && file.exists() && !file.isDirectory())
            return file.delete();
        else
            return true;
    }

    private File resolverAudio(File a, String orderId) {
        LogSystem.info(logger, orderId + ":: ResolverAudio", "Inicio");
        File out = null;
        Long pesoAudio = a.length();
        LogSystem.infoString(logger, orderId + ":: ResolverAudio", "File", "Peso: " + (a.length() / 1000 / 1000));
        //if (pesoAudio > 1024 * 1000 * 1000) {
        if (pesoAudio > 100 * 1000 * 1000) {
            LogSystem.infoString(logger, orderId + ":: ResolverAudio", "File a", "Peso excedido a 100 MB: " + a.length());
            LogSystem.info(logger, orderId + ":: ResolverAudio", "Fin");
            return out;
        }

        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        FileSystemResource value = new FileSystemResource(a);
        map.add("file", value);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(map, headers);
        RestTemplate restTemplate = restTemplate();
        ResponseEntity<byte[]> response = null;

        try {
            // Pendiente pasar este endpoint a la tabla de parametros....
            response = restTemplate.exchange("https://tefappfijaffmpeg-dev.mybluemix.net/api/v1/gsm", HttpMethod.POST, requestEntity, byte[].class);
        } catch (Exception e) {
            LogSystem.error(logger, orderId + ":: ResolverAudio", "RestTemplate Exception: " + e.getMessage(), e);
        }
        try {
            out = File.createTempFile("audio" + new Date().getTime(), ".gsm");
            org.apache.commons.io.FileUtils.writeByteArrayToFile(out, response.getBody());
        } catch (Exception e) {
            LogSystem.error(logger, orderId + ":: ResolverAudio", "writeByteArrayToFile Exception: " + e.getMessage(), e);
            out = null;
        }
        LogSystem.info(logger, orderId + ":: ResolverAudio", "Fin");
        return out;
    }

}
