package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.bean.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.dao.mapper.WavtogsmRequestRowMapper;
import pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.repository.OrderRepository;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@Repository
public class WavtogsmDao {
    private static final Logger logger = LogManager.getLogger(WavtogsmDao.class);

    @Autowired
    private OrderRepository orderRepository;
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public WavtogsmDao(DataSource ds) {
        super();
        jdbcTemplate = new JdbcTemplate(ds);
    }

    public List<SaleApiFirebaseResponseBody> getOrderListForAudioProcess() {
        List<SaleApiFirebaseResponseBody> idOrderList = new ArrayList<>();
        String query = "SELECT order_id, id, userid, docnumber, registeredtime "
                + "FROM ("
                + "SELECT azu.order_id, ord.id, ord.userid, cus.docnumber, ord.registeredtime "
                + "FROM ibmx_a07e6d02edaf552.azure_request azu "
                + "LEFT JOIN ibmx_a07e6d02edaf552.order ord ON ord.id = replace(azu.order_id, '_motorizado', '') "
                + "LEFT JOIN ibmx_a07e6d02edaf552.customer cus ON cus.id = ord.customerid "
                //+ "WHERE azu.order_id = '-LRbo6GWBwnBZ6ASQhhC' "
                + "WHERE azu.status != 'OK' "
                + "AND coalesce(azu.encrypted, '') != 'OK' "
                + "AND ord.appcode != 'WEBVF' "
                + "AND ord.status = 'EFECTIVO' "
                + "AND ord.statusaudio NOT IN ('1', '4' ,'7') "
                + "ORDER BY ord.registrationdate DESC "
                + "LIMIT 5000 "
                + ") AZURE "
                + "ORDER BY RANDOM() "
                + "LIMIT 10 ";

        try {
            LogSystem.infoQuery(logger, "getOrderListForAudioProcess", "query", query);

            List<SaleApiFirebaseResponseBody> result = jdbcTemplate.query(query, new WavtogsmRequestRowMapper());
            if (result != null && !result.isEmpty()) {
                idOrderList = result;
            }

        } catch (Exception e) {
            LogSystem.error(logger, "getOrderListForAudioProcess", "Error setServiceCallEvents Dao", e);
        }

        return idOrderList;
    }

    public void updateStatusAudio(String orderId, String statusAudio) {
        LogSystem.info(logger, orderId + ":: UpdateStatusAudio", "Inicio");
        String query = "update ibmx_a07e6d02edaf552.order set statusAudio = ?, lastUpdateTime = now() where id = ?";

        try {
            jdbcTemplate.update(query, statusAudio, orderId);
            LogSystem.infoQuery(logger, orderId + ":: UpdateStatusAudio", "query", String.format("update ibmx_a07e6d02edaf552.order set statusAudio = '%s', lastUpdateTime = now() where id = '%s'", statusAudio, orderId));
        } catch (Exception e) {
            LogSystem.error(logger, orderId + ":: UpdateStatusAudio", e.getMessage(), e);
        }
        LogSystem.info(logger, orderId + ":: UpdateStatusAudio", "Fin");
    }

    public void updateAzure(String orderId) {
        LogSystem.info(logger, orderId + ":: UpdateAzure", "Inicio");
        String query = "update ibmx_a07e6d02edaf552.azure_request set encrypted = 'OK', last_update = (now() AT TIME ZONE 'America/Lima') where order_id = ?";

        try {
            LogSystem.infoString(logger, orderId + ":: UpdateAzure", "query", "update ibmx_a07e6d02edaf552.azure_request set encrypted = 'OK', last_update = (now() AT TIME ZONE 'America/Lima') where order_id = '" + orderId + "'");
            jdbcTemplate.update(query, orderId);
        } catch (Exception e) {
            LogSystem.error(logger, orderId + ":: UpdateAzure", e.getMessage(), e);
        }
        LogSystem.info(logger, orderId + ":: UpdateAzure", "Fin");
    }

    public void registerEvent(ServiceCallEvent event) {
        LogSystem.info(logger, event.getOrderId() + ":: RegisterEvent", "Inicio");
        try {
            String query = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events "
                    + "(event_datetime, tag, username, msg, orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp, sourceAppVersion, result) "
                    + "VALUES (CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?,?)";

            jdbcTemplate.update(query,
                    "SERVICE_CALL",
                    event.getUsername(),
                    event.getMsg(),
                    event.getOrderId().replace("_motorizado", ""),
                    event.getDocNumber(),
                    event.getServiceCode(),
                    event.getServiceUrl(),
                    event.getServiceRequest(),
                    event.getServiceResponse(),
                    "AZURE",
                    "1.0",
                    event.getResult());

        } catch (Exception e) {
            LogSystem.error(logger, event.getOrderId() + ":: RegisterEvent", e.getMessage(), e);
        }
        LogSystem.info(logger, event.getOrderId() + ":: RegisterEvent", "Fin");
    }

}
