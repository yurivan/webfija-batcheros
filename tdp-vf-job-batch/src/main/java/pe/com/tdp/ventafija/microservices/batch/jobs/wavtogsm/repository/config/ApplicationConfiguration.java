package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.repository.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConfiguration {
	@Value("${watson.api.speech2text.username}")
	private String watsonUsername;
	@Value("${watson.api.speech2text.password}")
	private String watsonPassword;
	@Value("${watson.api.speech2text.model}")
	private String watsonModel;
	
	@Value("${twilio.api.sms.username}")
	private String twilioUsername;
	@Value("${twilio.api.sms.password}")
	private String twilioPassword;
	@Value("${twilio.api.sms.phonenumbersender}")
	private String twilioPhoneNumberSender;
	
	@Value("${google.api.storage.bucket}")
	private String googleStorageBucket;
	@Value("${google.api.auth.credential}")
	private String googleApiJsonCredential;
	@Value("${google.api.auth.applicationname}")
	private String googleApiApplicationName;
	@Value("${google.api.auth.projectid}")
	private String googleApiProjectId;

	public String getWatsonUsername() {
		return watsonUsername;
	}

	public void setWatsonUsername(String watsonUsername) {
		this.watsonUsername = watsonUsername;
	}

	public String getWatsonPassword() {
		return watsonPassword;
	}

	public void setWatsonPassword(String watsonPassword) {
		this.watsonPassword = watsonPassword;
	}

	public String getWatsonModel() {
		return watsonModel;
	}

	public void setWatsonModel(String watsonModel) {
		this.watsonModel = watsonModel;
	}

	public String getTwilioUsername() {
		return twilioUsername;
	}

	public void setTwilioUsername(String twilioUsername) {
		this.twilioUsername = twilioUsername;
	}

	public String getTwilioPassword() {
		return twilioPassword;
	}

	public void setTwilioPassword(String twilioPassword) {
		this.twilioPassword = twilioPassword;
	}

	public String getGoogleStorageBucket() {
		return googleStorageBucket;
	}

	public void setGoogleStorageBucket(String googleStorageBucket) {
		this.googleStorageBucket = googleStorageBucket;
	}

	public String getGoogleApiJsonCredential() {
		return googleApiJsonCredential;
	}

	public void setGoogleApiJsonCredential(String googleApiJsonCredential) {
		this.googleApiJsonCredential = googleApiJsonCredential;
	}

	public String getGoogleApiApplicationName() {
		return googleApiApplicationName;
	}

	public void setGoogleApiApplicationName(String googleApiApplicationName) {
		this.googleApiApplicationName = googleApiApplicationName;
	}

	public String getTwilioPhoneNumberSender() {
		return twilioPhoneNumberSender;
	}

	public void setTwilioPhoneNumberSender(String twilioPhoneNumberSender) {
		this.twilioPhoneNumberSender = twilioPhoneNumberSender;
	}

	public String getGoogleApiProjectId() {
		return googleApiProjectId;
	}

	public void setGoogleApiProjectId(String googleApiProjectId) {
		this.googleApiProjectId = googleApiProjectId;
	}

}
