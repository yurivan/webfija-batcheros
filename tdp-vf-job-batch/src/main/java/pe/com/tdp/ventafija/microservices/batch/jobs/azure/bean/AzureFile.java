package pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean;

public class AzureFile {

    private String audio;
    private String image;

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
