package pe.com.tdp.ventafija.microservices.batch.jobs.azure.config;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import pe.com.tdp.ventafija.microservices.batch.jobs.RequestProcessorSkipPolicy;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.AzureRequestProcessor;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.AzureRequestProcessorListener;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.AzureRequestProcessorScheduler;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureRequest;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureRequestStatus;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.AzureMetadataDao;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.AzureRequestDao;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.mapper.AzureRequestRowMapper;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.service.AzureMetadataService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.telefonica.everis.common.azure.AzureApi;
import pe.com.telefonica.everis.common.google.GoogleApi;
import pe.com.telefonica.everis.common.google.GoogleApiConfig;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
public class BatchConfig {

    private static final Logger logger = LogManager.getLogger(BatchConfig.class);

    @Autowired
    private DataSource dataSource;
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;
    @Autowired
    private AzureRequestDao azureRequestDao;
    @Autowired
    private AzureMetadataDao azureMetadataDao;

    @Value("${azure.api.connection.legacy}")
    private String azureLegacyConnection;
    @Value("${azure.api.connection.nolegacy}")
    private String azureNoLegacyConnection;

    @Value("${google.api.auth.projectid}")
    private String googleProjectId;
    @Value("${google.api.storage.bucket}")
    private String googleBucket;
    @Value("${google.api.auth.credential}")
    private String googleCredential;
    @Value("${google.api.auth.applicationname}")
    private String googleApplicationName;


    @Value("${azure.upload.image}")
    private Boolean uploadImage;
    @Value("${azure.file.extension.2}")
    private String fileExtension2;
    @Value("${azure.file.extension.3}")
    private String fileExtension3;

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new ResourcelessTransactionManager();
    }

    @Bean
    public JobRepository jobRepository() throws Exception {
        return new MapJobRepositoryFactoryBean(transactionManager()).getObject();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        SyncTaskExecutor executor = new SyncTaskExecutor();
        return executor;
    }

    @Bean
    public AzureRequestProcessorScheduler azureRequestProcessorScheduler() {
        return new AzureRequestProcessorScheduler();
    }

    @Bean
    public SimpleJobLauncher jobLauncher() throws Exception {
        SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        simpleJobLauncher.setJobRepository(jobRepository());
        simpleJobLauncher.setTaskExecutor(taskExecutor());
        return simpleJobLauncher;
    }

    @Bean
    public JdbcCursorItemReader<AzureRequest> reader() {
        String sql = "select order_id, order_status, registration_date, status, userid, docNumber, appcode, whatsapp " +
                "FROM (" +
                "select distinct azu.order_id, vis.estado_solicitud as order_status, ord.registeredtime as registration_date, now() as last_update, azu.status, COALESCE(ord.userid, '') as userid, vis.dni as docNumber, COALESCE(ord.appcode, 'VISOR') as appcode, ord.whatsapp  " +
                "from ibmx_a07e6d02edaf552.azure_request azu " +
                "left join ibmx_a07e6d02edaf552.order ord on ord.id = replace(azu.order_id, '_motorizado', '') " +
                "left join ibmx_a07e6d02edaf552.tdp_visor vis on vis.id_visor = replace(azu.order_id, '_motorizado', '') " +
                //"where azu.order_id = '-p32p64e79clyj69xeep' "+
                "where azu.status = '%s' " +
                "and azu.encrypted = '%s' " +
                "and COALESCE(vis.codigo_pedido, '') != '' " +
                "ORDER BY ord.registeredtime ASC " +
                "LIMIT 1000) foo " +
                "ORDER BY RANDOM() " +
                "LIMIT 20";

        sql = String.format(sql, AzureRequestStatus.PENDIENTE.getCode(), AzureRequestStatus.OK.getCode());

        LogSystem.infoString(logger, "reader", "sql", sql);

        JdbcCursorItemReader<AzureRequest> reader = new JdbcCursorItemReader<>();
        reader.setDataSource(dataSource);
        reader.setSql(sql);
        reader.setRowMapper(new AzureRequestRowMapper());
        reader.setFetchSize(5);
        return reader;
    }

    @Bean
    public JdbcBatchItemWriter<AzureRequest> writer() {
        JdbcBatchItemWriter<AzureRequest> writer = new JdbcBatchItemWriter<>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<AzureRequest>());
        writer.setSql("update ibmx_a07e6d02edaf552.azure_request set status = :status, last_update = :lastUpdate, msg = :msg, processing_time = :processingTime where order_id = :orderId");
        writer.setDataSource(dataSource);

        LogSystem.infoString(logger, "writer", "writer", "update ibmx_a07e6d02edaf552.azure_request set status = :status, last_update = :lastUpdate, msg = :msg, processing_time = :processingTime where order_id = :orderId");
        return writer;
    }

    @Bean
    public GoogleApiConfig googleApiConfig() {
        return new GoogleApiConfig(googleBucket, googleCredential, googleApplicationName, googleProjectId);
    }

    @Bean
    public GoogleApi googleStorageService() {
        return new GoogleApi(googleApiConfig());
    }

    @Bean
    public AzureApi azureApi() {
        return new AzureApi();
    }

    @Bean
    public AzureRequestProcessor processor(GoogleApi googleStorageService, AzureMetadataDao azureMetadataDao,
                                           AzureApi azureApi) {
        AzureRequestProcessor processor = new AzureRequestProcessor(googleStorageService, azureApi, azureMetadataDao, azureLegacyConnection, fileExtension2, fileExtension3, uploadImage);
        return processor;
    }
    @Bean
    public AzureMetadataService service(AzureMetadataDao azureMetadataDao,
                                        AzureApi azureApi) {
        AzureMetadataService service = new AzureMetadataService(azureApi, azureMetadataDao, azureLegacyConnection, fileExtension2, fileExtension3, uploadImage);
        return service;
    }

    @Bean
    public Job azureRequestProcessor(Step sendAzureStep) throws Exception {
        return jobBuilderFactory.get("azureRequestProcessor").incrementer(new RunIdIncrementer())
                .repository(jobRepository()).flow(sendAzureStep).end().build();
    }

    @Bean
    public SkipPolicy azureRequestProcessorSkipPolicy() {
        return new RequestProcessorSkipPolicy();
    }

    @Bean
    public ItemProcessListener<AzureRequest, AzureRequest> azureRequestProcessorListener() {
        return (ItemProcessListener<AzureRequest, AzureRequest>) new AzureRequestProcessorListener(azureRequestDao, azureMetadataDao);
    }

    @Bean
    public Step sendAzureStep(AzureRequestProcessor processor) {
        return stepBuilderFactory.get("send2Azure")
                .<AzureRequest, AzureRequest>chunk(0)
                .reader(reader())
                .processor(processor)
                .faultTolerant()
                .skipPolicy(azureRequestProcessorSkipPolicy())
                .listener(azureRequestProcessorListener())
                .writer(writer()).build();
    }
}
