package pe.com.tdp.ventafija.microservices.batch.jobs.auditoria;

import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import com.microsoft.azure.storage.blob.ListBlobItem;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.batch.jobs.auditoria.bean.EmailResponseBody;
import pe.com.tdp.ventafija.microservices.batch.jobs.auditoria.service.Mailing;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.telefonica.everis.common.azure.AzureApi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class AuditoriaRequestProcessor {

    private static final Logger logger = LogManager.getLogger(AuditoriaRequestProcessor.class);

    private DateTimeZone dateTimeZone = DateTimeZone.forID("America/Lima");

    //@Value("${azure.api.connection.legados29}")
    @Value("${azure.api.connection.legacy}")
    private String legacyConnection;

    @Value("${auditoria.good.custodia}")
    private String wordsGoodCustodia;
    @Value("${auditoria.error.custodia}")
    private String wordsErrorCustodia;

    //@Value("jhair.lozano@vasslatam.com,smtorrest@grupodelaware.com")
    @Value("${auditoria.emails}")
    private String auditoriaEmails;

    private AzureApi azureApi;

    @Autowired
    Mailing mailing;

    @Bean
    public AzureApi azureApi() {
        return new AzureApi();
    }

    public String[] process() {
        LogSystem.info(logger, "Process", "Inicio");
        String[] process = new String[2];
        azureApi = azureApi();

        try {
            processMetadata(getDate());
        } catch (Exception e) {
            LogSystem.error(logger, "Process", "Exception", e);
        }
        DateTime dateTime = new DateTime(new Date());
        DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);
        process[0] = String.valueOf(dateTimeWithZone.toDate());
        process[1] = "Finished: SUCCESS";
        LogSystem.info(logger, "Process", "Fin");
        return process;
    }

    private String getDate() {

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DAY_OF_MONTH, -1);
        Date minDate = c.getTime();

        DateTime dateTime = new DateTime(minDate);
        DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String azureDate = dateFormat.format(dateTimeWithZone.toDate());
        return azureDate;
    }

    private void processMetadata(String azureDate)
            throws URISyntaxException, StorageException, IOException {
        LogSystem.info(logger, "ProcessMetadata", "Inicio");

        String excelExtension = ".xlsx";
        String excelFilename = azureDate + excelExtension;

        CloudBlobContainer containerLegacy = azureApi.createBlobContainer("", legacyConnection, azureDate);
        for (ListBlobItem blobItem : containerLegacy.listBlobs()) {
            String blobName = blobNameFromUri(blobItem.getUri());
            LogSystem.infoString(logger, "ProcessMetadata", "blobName", blobName);
            if (blobName.endsWith(".xlsx") || blobName.endsWith(".xls")) {
                excelFilename = blobName;
            }
        }
        CloudBlob azureExcelLegacy = containerLegacy.getBlockBlobReference(excelFilename);
        azureExcelLegacy.getProperties().setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

        if (azureExcelLegacy.exists()) {
            LogSystem.infoString(logger, "ProcessMetadata", "azureExcelLegacy", "Existe");
            File excelLegacy = File.createTempFile("legacy", "tmp");
            azureExcelLegacy.downloadToFile(excelLegacy.getAbsolutePath());

            if (excelLegacy != null) {
                LogSystem.infoString(logger, "ProcessMetadata", "excelLegacy", "Creado: " + excelLegacy.getAbsolutePath());
                readMetadata(excelLegacy, azureDate);
                if (excelLegacy.delete())
                    LogSystem.infoString(logger, "ProcessMetadata", "excelLegacy", "Eliminado: " + excelLegacy.getAbsolutePath());
            }
        } else {
            LogSystem.infoString(logger, "ProcessMetadata", "azureExcelLegacy", "No existe");
        }

        LogSystem.info(logger, "ProcessMetadata", "Inicio");
    }


    private void readMetadata(File excelFile, String azureDate) {
        LogSystem.info(logger, "ReadMetadata", "Inicio");

        try (FileInputStream fis = new FileInputStream(excelFile);
             XSSFWorkbook workbook = new XSSFWorkbook(fis)) {
            XSSFSheet sheet = workbook.getSheet("Metadata");

            Integer errorCustodia = 0;
            Integer goodCustodia = 0;

            int lastRow = sheet.getLastRowNum();
            LogSystem.infoString(logger, "ReadMetadata", "sheet", lastRow + " registros");

            String htmlFiles = "<tr>" +
                    "    <td>" +
                    "        <p style='padding: 8px 0px; line-height:normal; text-align: center;'>" +
                    "            [CODE_CUSTODIA]" +
                    "        </p>" +
                    "    </td>" +
                    "    <td>" +
                    "        <p style='padding: 8px 0px; line-height:normal; text-align: center;'>" +
                    "            [FILE_CUSTODIA]" +
                    "        </p>" +
                    "    </td>" +
                    "    <td>" +
                    "        <p style='padding: 8px 0px; line-height:normal; text-align: center;'>" +
                    "            [STATE_CUSTODIA]" +
                    "        </p>" +
                    "    </td>" +
                    "</tr>";

            String file_custodia = "";
            for (int i = 1; i <= lastRow; i++) {
                XSSFRow row = sheet.getRow(i);
                XSSFCell cell_0 = row.getCell(0);
                XSSFCell cell_1 = row.getCell(1);
                XSSFCell cell_14 = row.getCell(14);
                String archivoCustodia = cell_0.getStringCellValue();
                String codigoCustodia = "" + (int) cell_1.getNumericCellValue();
                String estadoCustodia = cell_14.getStringCellValue();
                LogSystem.infoString(logger, "ReadMetadata", archivoCustodia + codigoCustodia, estadoCustodia);

                List<String> listGoodCustodia = new ArrayList<String>(Arrays.asList(wordsGoodCustodia.split(",")));
                if (listGoodCustodia.contains(estadoCustodia.trim())) {
                    goodCustodia++;
                } else {
                    errorCustodia++;
                    htmlFiles = htmlFiles
                            .replace("[CODE_CUSTODIA]", codigoCustodia)
                            .replace("[FILE_CUSTODIA]", archivoCustodia)
                            .replace("[STATE_CUSTODIA]", estadoCustodia);
                    file_custodia += htmlFiles;
                }
            }

            LogSystem.infoString(logger, "ReadMetadata", "goodCustodia", goodCustodia + " correctos encontrados");
            LogSystem.infoString(logger, "ReadMetadata", "errorCustodia", errorCustodia + " errores encontrados");

            if (errorCustodia > 0) {
                String emailHTML = "";
                try {
                    String path = "/auditoria/index.html";

                    File file = new File(new URI(getClass().getResource(path).toString()));

                    Document htmlFile = Jsoup.parse(file, "UTF-8");


                    DateTime dateTime = new DateTime(new Date());
                    DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);

                    emailHTML = htmlFile.toString()
                            .replace("[NRO_REGISTROS]", "" + (goodCustodia + errorCustodia))
                            .replace("[NRO_CORRECTOS]", "" + goodCustodia)
                            .replace("[NRO_ERRORES]", "" + errorCustodia)
                            .replace("[BLOB_CONTAINER]", azureDate)
                            .replace("[FECHA_HORA]", String.valueOf(dateTimeWithZone.toDate()))
                            .replace("[LIST_FILES]", file_custodia);
                    LogSystem.infoString(logger, "ReadMetadata", "emailHTML", emailHTML);
                } catch (Exception e) {
                    LogSystem.error(logger, "ReadMetadata", "Exception - emailHTML", e);
                }

                if (auditoriaEmails != null && !auditoriaEmails.equals("")) {
                    LogSystem.infoString(logger, "ReadMetadata", "auditoriaEmails", auditoriaEmails);
                    List<String> listEmails = new ArrayList<String>(Arrays.asList(auditoriaEmails.split(",")));
                    for (String email : listEmails) {
                        LogSystem.infoString(logger, "ReadMetadata", "email", email);
                        EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, emailHTML, "Custodia de archivo a Azure");

                        LogSystem.infoString(logger, "ReadMetadata", email + ":: getAggregatesSendEmail", "" + sendExpressResult.getAggregatesSendEmail());
                        LogSystem.infoString(logger, "ReadMetadata", email + ":: getTimestamp", sendExpressResult.getTimestamp());
                    }
                } else {
                    LogSystem.infoString(logger, "ReadMetadata", "auditoriaEmails", "La venta no cuenta con CORREO");
                }

            }
        } catch (FileNotFoundException fileNotFoundException) {
            LogSystem.error(logger, "ReadMetadata", "FileNotFoundException", fileNotFoundException);
        } catch (IOException iOException) {
            LogSystem.error(logger, "ReadMetadata", "IOException", iOException);
        } catch (Exception exception) {
            LogSystem.error(logger, "ReadMetadata", "Exception", exception);
        }
        LogSystem.info(logger, "ReadMetadata", "Fin");

    }

    public static String blobNameFromUri(URI uri) {
        String path = uri.getPath();

        // We remove the container name from the path
        // The 3 magic number cames from the fact if path is /container/path/to/myfile
        // First occurrence is empty "/"
        // Second occurrence is "container
        // Last part contains "path/to/myfile" which is what we want to get
        String[] splits = path.split("/", 3);

        // We return the remaining end of the string
        return splits[2];
    }

}