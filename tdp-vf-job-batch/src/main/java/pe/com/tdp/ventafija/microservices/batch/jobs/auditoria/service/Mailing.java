package pe.com.tdp.ventafija.microservices.batch.jobs.auditoria.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.batch.jobs.auditoria.bean.*;

@Component
public class Mailing {

    @Value("${tdp.mail.emblue.sendmailexpresss.actionId}")
    private String emblueSendExpressActionId;

    @Value("${tdp.mail.emblue.sendexpress.uri}")
    private String emblueSendExpressUri;

    @Value("${tdp.mail.emblue.user}")
    private String emblueUser;

    @Value("${tdp.mail.emblue.password}")
    private String embluePassword;

    @Value("${tdp.mail.emblue.token}")
    private String emblueToken;

    @Value("${tdp.mail.emblue.authenticate.uri}")
    private String emblueAutenticateUri;

    @Value("${tdp.mail.emblue.checkconexion.uri}")
    private String emblueCheckConexionUri;

    private String emailToken = "";

    public EmailResponseBody sendMailExpress(String email, String message, String subject) {
        emailToken = getToken();

        EmailRequestBody request = new EmailRequestBody();
        request.setEmail(email);
        request.setActionId(emblueSendExpressActionId);
        request.setMessage(message);
        request.setToken(emailToken);
        request.setSubject(subject);

        EmailClient<EmailRequestBody, EmailResponseBody> emailSendExpress = new EmailClient<>(emblueSendExpressUri);
        ClientResult<EmailResponseBody> sendExpressResponse = emailSendExpress.sendRequest(request);
        return sendExpressResponse.getResult();
    }

    private String getToken() {
        if (emailToken.equals("")) emailToken = authenticate();

        Boolean result = checkConection(emailToken);
        if (!result) emailToken = authenticate();
        return emailToken;
    }

    private String authenticate() {
        AuthenticateRequestBody authenticateRequest = new AuthenticateRequestBody();
        authenticateRequest.setUser(emblueUser);
        authenticateRequest.setPass(embluePassword);
        authenticateRequest.setToken(emblueToken);

        EmailClient<AuthenticateRequestBody, EmailResponseBody> emailAuthenticate = new EmailClient<>(emblueAutenticateUri);
        ClientResult<EmailResponseBody> authenticateResponse = emailAuthenticate.sendRequest(authenticateRequest);

        String tokenToSend = authenticateResponse.getResult().getToken();
        return tokenToSend;
    }

    private Boolean checkConection(String tokenToSend) {
        CheckConexionRequestBody checkConexionRequest = new CheckConexionRequestBody();
        checkConexionRequest.setToken(tokenToSend);

        EmailClient<CheckConexionRequestBody, EmailResponseBody> emailCheckConexion = new EmailClient<>(emblueCheckConexionUri);
        ClientResult<EmailResponseBody> checkConexionResponse = emailCheckConexion.sendRequest(checkConexionRequest);

        EmailResponseBody checkConexionResult = checkConexionResponse.getResult();
        return (checkConexionResult != null) ? checkConexionResult.getResult() : false;
    }


}

