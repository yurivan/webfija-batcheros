package pe.com.tdp.ventafija.microservices.batch.jobs.azure;

import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.CloudBlob;
import com.microsoft.azure.storage.blob.CloudBlobContainer;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import org.springframework.batch.item.ItemProcessor;
import pe.com.tdp.ventafija.microservices.batch.jobs.RequestProcessorException;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.*;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.AzureMetadataDao;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.telefonica.everis.common.azure.AzureApi;
import pe.com.telefonica.everis.common.azure.AzureApiException;
import pe.com.telefonica.everis.common.google.GoogleApi;

import java.io.*;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AzureRequestProcessor implements ItemProcessor<AzureRequest, AzureRequest> {

    private static final Logger logger = LogManager.getLogger(AzureRequestProcessorScheduler.class);

    private DateTimeZone dateTimeZone = DateTimeZone.forID("America/Lima");

    private GoogleApi gss;
    private AzureApi azureApi;
    private AzureMetadataDao metadataDao;
    private String legacyConnection;

    private String fileExtension2;
    private String fileExtension3;
    private Boolean uploadImage;

    private JSONObject metadataLegacy;

    public AzureRequestProcessor(GoogleApi googleStorageService, AzureApi azureApi, AzureMetadataDao metadataDao,
                                 String legacyConnection, String fileExtension2, String fileExtension3, Boolean uploadImage) {
        super();
        this.gss = googleStorageService;
        this.metadataDao = metadataDao;
        this.azureApi = azureApi;

        this.legacyConnection = legacyConnection;
        this.fileExtension2 = fileExtension2;
        this.fileExtension3 = fileExtension3;
        this.uploadImage = uploadImage;
    }

    @Override
    public AzureRequest process(AzureRequest item) throws Exception {
        LogSystem.info(logger, item.getOrderId() + ":: Process", "Inicio");
        LogSystem.infoObject(logger, item.getOrderId() + ":: Process", "item", item);

        Date start, stop;
        start = new Date();

        // Obtener el Blob Container destino
        String azureDate = getDate();
        LogSystem.infoString(logger, item.getOrderId() + ":: Process", "azureDate", azureDate);

        // Obtener el Storage destino
        LogSystem.infoString(logger, item.getOrderId() + ":: Process", "destination", legacyConnection);
        item.setServiceUrl(legacyConnection);

        // Lectura de la Metadata
        AzureMetadata metadata = metadataDao.getMetadata(item);

        LogSystem.infoObject(logger, item.getOrderId() + ":: Process", "metadata", metadata);

        // Validación de la metadata, en caso de no encontrar será por datos insuficientes
        if (metadata == null) {
            LogSystem.info(logger, item.getOrderId() + ":: Process", "Fin");
            throw new RequestProcessorException(setAzureResponse(1, "No se encontró metadata", new JSONObject(), "", ""));
        }
        if (metadata.getId() == null || "".equals(metadata.getId().trim())) {
            LogSystem.info(logger, item.getOrderId() + ":: Process", "Fin");
            throw new RequestProcessorException(setAzureResponse(1, "Pedido sin codigo pedido", new JSONObject(), "", ""));
        }

        if(metadata.getAzureStatus().trim().equalsIgnoreCase(AzureRequestStatus.OK.getCode())){
            LogSystem.info(logger, item.getOrderId() + ":: Pedido ya procesado", "Fin");
            LogSystem.info(logger, item.getOrderId() + ":: Process", "Fin");
            return  item;
        }

        String imagenFilename = "";
        // Validar si está habilitado Azure para subir imagen
        if (uploadImage) {
            // Validar si es APP o WEB
            if (item.getAppcode().equalsIgnoreCase("APPVF")) {
                try {
                    imagenFilename = sendPdf(item, metadata, azureDate, fileExtension3);
                } catch (RequestProcessorException e) {
                    LogSystem.info(logger, item.getOrderId() + ":: Process", "Fin");
                    throw new RequestProcessorException(setAzureResponse(3, e.getMessage(), new JSONObject(), "", ""));
                }
            } else {
                // Validar si tiene el campo Whatsapp
                if (item.getWhatsApp() != null) {
                    // Validar si esta venta es 1:PDF o otro:GSM
                    if (item.getWhatsApp() == 1) {
                        try {
                            imagenFilename = sendPdf(item, metadata, azureDate, fileExtension2);
                        } catch (RequestProcessorException e) {
                            LogSystem.info(logger, item.getOrderId() + ":: Process", "Fin");
                            throw new RequestProcessorException(setAzureResponse(3, e.getMessage(), new JSONObject(), "", ""));
                        }
                    }
                }
            }
        }

        String audioFilename = "";
        try {
            // Validar si tiene el campo Whatsapp
            if (item.getWhatsApp() != null) {
                // Validar si esta venta es diferente a 1:PDF
                if (item.getWhatsApp() != 1) {
                    audioFilename = sendAudio(item, metadata, azureDate);
                } else {
                    if (!uploadImage) {

                        DateTime dateTime = new DateTime(new Date());
                        DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);

                        stop = new Date();

                        item.setLastUpdate(dateTimeWithZone.toDate());
                        item.setStatus(AzureRequestStatus.WHATSAPP.getCode());
                        item.setProcessingTime(stop.getTime() - start.getTime());

                        metadataDao.setMetaData(item, "Venta PDF aún no activa para Azure", "Venta PDF aún no activa para Azure", "OK");

                        LogSystem.infoString(logger, item.getOrderId() + ":: Process", "item", "Venta PDF aún no activa para Azure");
                        LogSystem.infoObject(logger, item.getOrderId() + ":: Process", "item", item);
                        LogSystem.info(logger, item.getOrderId() + ":: Process", "Fin");
                        return item;
                    }
                }
            } else {
                audioFilename = sendAudio(item, metadata, azureDate);
            }
        } catch (RequestProcessorException e) {

            LogSystem.info(logger, item.getOrderId() + ":: Process", "Fin");
            throw new RequestProcessorException(setAzureResponse(3, e.getMessage(), new JSONObject(), "", ""));
        }
        LogSystem.infoString(logger, item.getOrderId() + ":: Process", "audioFilename", audioFilename);

        //processMetadata(item, metadata, azureDate, audioFilename);
        //processMetadata(item, metadata, azureDate, imagenFilename)
        // ;
        item.setStatus(AzureRequestStatus.OK.getCode());

        DateTime dateTime = new DateTime(new Date());
        DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);
        item.setLastUpdate(dateTimeWithZone.toDate());

        stop = new Date();

        item.setProcessingTime(stop.getTime() - start.getTime());

        metadataDao.setMetaData(item, "OK", setAzureResponse(0, "OK", metadataLegacy, audioFilename, imagenFilename), "OK");

        LogSystem.infoObject(logger, item.getOrderId() + ":: Process", "item", item);
        LogSystem.info(logger, item.getOrderId() + ":: Process", "Fin");
        return item;
    }

    private String getDate() {

        DateTime dateTime = new DateTime(new Date());
        DateTime dateTimeWithZone = dateTime.withZone(dateTimeZone);
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String azureDate = dateFormat.format(dateTimeWithZone.toDate());
        return azureDate;
    }

    private String sendPdf(AzureRequest item, AzureMetadata metadata, String azureDate, String fileNameExtension) throws RequestProcessorException {
        LogSystem.info(logger, item.getOrderId() + ":: SendPdf", "Inicio");

        String imagenExtension = ".pdf";
        String imagenFilename = metadata.getId() + fileNameExtension + imagenExtension;
        String imagenFirebase = String.format("%s/images/%s-original.pdf", item.getOrderId(), item.getOrderId());
        File imagenFile = null;

        try {
            // Validar si el archivo ya existe
            CloudBlobContainer containerLegacy = azureApi.createBlobContainer(item.getOrderId(), legacyConnection, azureDate);
            CloudBlob azureExcelLegacy = containerLegacy.getBlockBlobReference(imagenFilename);

            if (azureExcelLegacy.exists()) {
                LogSystem.infoString(logger, item.getOrderId() + ":: SendPdf", "azureExcelLegacy", "Ya existe");
                LogSystem.info(logger, item.getOrderId() + ":: SendPdf", "Fin");
                return imagenFilename;
            }
        } catch (Exception e) {
            LogSystem.error(logger, item.getOrderId() + ":: SendPdf", "CloudBlobContainer Exception", e);
        }

        try {
            // Descargar el archivo de Firebase
            imagenFile = gss.retrieveFile(item.getOrderId(), imagenFirebase);

            // Subir el archivo a Azure
            azureApi.upload(item.getOrderId(), imagenFile, imagenFilename, legacyConnection, azureDate);
        } catch (IOException e) {
            LogSystem.error(logger, item.getOrderId() + ":: SendPdf", "Imagen no encontrada - IOException", e);
            LogSystem.info(logger, item.getOrderId() + ":: SendPdf", "Fin");
            throw new RequestProcessorException("Imagen no encontrada", e);
        } catch (AzureApiException e) {
            LogSystem.error(logger, item.getOrderId() + ":: SendPdf", "Error subir imagen - AzureApiException", e);
            LogSystem.info(logger, item.getOrderId() + ":: SendPdf", "Fin");
            throw new RequestProcessorException("Error subir imagen", e);
        } finally {
            if (imagenFile != null) {
                imagenFile.delete();
            }
        }

        LogSystem.info(logger, item.getOrderId() + ":: SendPdf", "Fin");
        return imagenFilename;
    }

    private String sendAudio(AzureRequest item, AzureMetadata metadata, String azureDate) throws RequestProcessorException {
        LogSystem.info(logger, item.getOrderId() + ":: SendAudio", "Inicio");

        String audioExtension = ".gsm";
        String audioFilename = metadata.getId() + fileExtension2 + audioExtension;

        try {
            CloudBlobContainer containerLegacy = azureApi.createBlobContainer(item.getOrderId(), legacyConnection, azureDate);
            CloudBlob azureExcelLegacy = containerLegacy.getBlockBlobReference(audioFilename);

            if (azureExcelLegacy.exists()) {
                LogSystem.infoString(logger, item.getOrderId() + ":: SendAudio", "azureExcelLegacy", "Ya existe");
                LogSystem.info(logger, item.getOrderId() + ":: SendAudio", "Fin");
                return audioFilename;
            }
        } catch (Exception e) {
            LogSystem.error(logger, item.getOrderId() + ":: SendAudio", "CloudBlobContainer Exception", e);
        }
        String zipFirebase = String.format("%s/audios/%s_gsm.zip", item.getOrderId(), item.getOrderId());
        String audioFirebase = item.getOrderId() + audioExtension;
        File audioFile = null;

        try {
            // Descargar el archivo de Firebase y descomprimir el audio
            audioFile = gss.retriveFileFromZip(item.getOrderId(), zipFirebase, audioFirebase);

            if (audioFile == null) {
                LogSystem.error(logger, item.getOrderId() + ":: SendAudio", "No se encontro audio", new RequestProcessorException("No se encontro audio"));
                LogSystem.info(logger, item.getOrderId() + ":: SendAudio", "Fin");
                throw new RequestProcessorException("No se encontro audio");
            }

            // Subir el archivo a Azure
            azureApi.upload(item.getOrderId(), audioFile, audioFilename, legacyConnection, azureDate);
        } catch (AzureApiException e) {
            LogSystem.error(logger, "sendAudio", "Error subir audio", e);
            LogSystem.info(logger, "sendAudio", "Fin");
            throw new RequestProcessorException("Error subir audio", e);
        } finally {
            if (audioFile != null) {
                audioFile.delete();
            }
        }
        LogSystem.info(logger, item.getOrderId() + ":: SendAudio", "Fin");
        return audioFilename;
    }

    public String setAzureResponse(Integer code, String message, JSONObject metadata, String audio, String image) {

        AzureFile azureFile = new AzureFile();
        azureFile.setAudio(audio);
        azureFile.setImage(image);

        AzureResponse azureResponse = new AzureResponse();
        azureResponse.setResponseCode(code);
        azureResponse.setResponseMessage(message);
        azureResponse.setArchivos(azureFile);

        JSONObject response = new JSONObject(azureResponse);
        response.put("metadata", metadata);

        return response.toString();
    }
}
