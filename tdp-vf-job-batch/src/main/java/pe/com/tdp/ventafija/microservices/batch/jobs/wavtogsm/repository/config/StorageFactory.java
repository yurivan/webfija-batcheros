package pe.com.tdp.ventafija.microservices.batch.jobs.wavtogsm.repository.config;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.storage.Storage;
import com.google.api.services.storage.StorageScopes;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collection;

public class StorageFactory {
	  private static Storage instance = null;
	  static String credentialLocation;
	  static String applicationName;
	  
	  public static void setMeta (String credentials, String application) {
		  credentialLocation =  credentials;
		  applicationName = application;
	  }

	  public static synchronized Storage getService() throws IOException, GeneralSecurityException {
	    if (instance == null) {
	      instance = buildService();
	    }
	    return instance;
	  }

	  private static Storage buildService() throws IOException, GeneralSecurityException {
	    HttpTransport transport = GoogleNetHttpTransport.newTrustedTransport();
	    JsonFactory jsonFactory = new JacksonFactory();
	    GoogleCredential credential = GoogleCredential.fromStream(new FileInputStream(credentialLocation));

	    if (credential.createScopedRequired()) {
	      Collection<String> scopes = StorageScopes.all();
	      credential = credential.createScoped(scopes);
	    }

	    return new Storage.Builder(transport, jsonFactory, credential)
	    		.setApplicationName(applicationName)
	    		.build();
	  }
	}
