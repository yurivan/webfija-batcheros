package pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureRequest;

import javax.sql.DataSource;

@Repository
public class AzureRequestDao {
    private static final Logger logger = LogManager.getLogger(AzureRequestDao.class);
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public AzureRequestDao(DataSource ds) {
        super();
        jdbcTemplate = new JdbcTemplate(ds);
    }

    public void update(AzureRequest request) {
        JSONObject objRequest = new JSONObject(request);
        String update = "update ibmx_a07e6d02edaf552.azure_request set status = ?, last_update = ?, msg = ? where order_id = ?";
        logger.info("update => request: " + objRequest);
        logger.info("update => update: " + update);
        jdbcTemplate.update(update, request.getStatus(), request.getLastUpdate(), request.getMsg(), request.getOrderId());
    }
}
