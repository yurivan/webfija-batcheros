package pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean;

public class AzureResponse {

    private Integer responseCode;
    private String metadata;
    private String responseMessage;
    private AzureFile archivos;

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public AzureFile getArchivos() {
        return archivos;
    }

    public void setArchivos(AzureFile archivos) {
        this.archivos = archivos;
    }
}
