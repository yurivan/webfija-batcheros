package pe.com.tdp.ventafija.microservices.batch.jobs.azure.dao.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.com.tdp.ventafija.microservices.batch.jobs.azure.bean.AzureMetadata;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AzureMetadataRowMapper implements RowMapper<AzureMetadata> {

    @Override
    public AzureMetadata mapRow(ResultSet rs, int rowNum) throws SQLException {
        AzureMetadata metadata = new AzureMetadata();
        metadata.setBusiness(rs.getString("NEGOCIO"));
        metadata.setCampaignName(rs.getString("NOMBRE_CAMPANIA"));
        metadata.setChannel(rs.getString("CANAL_VENTA"));
        metadata.setCustomerName(rs.getString("NOMBRE_CLIENTE"));
        metadata.setDataProtection(rs.getString("PROTECCION_DE_DATOS"));
        metadata.setDisableWhitePage(rs.getString("DESAFILIACION_DE_PAGINAS_BLANCAS"));
        metadata.setDocumentDate(rs.getTimestamp("FECHA_DOCUMENTO"));
        metadata.setDocumentNumber(rs.getString("NUMERO_DOCUMENTO"));
        metadata.setEnableDigitalInvoice(rs.getString("AFILIACION_FACTURA_DIGITAL"));
        metadata.setDni(rs.getString("DNI"));
        metadata.setId(rs.getString("IDENTIFICADOR_UNICO"));
        metadata.setPhoneNumber(rs.getString("NUMERO_CELULAR"));
        metadata.setProviderDetail(rs.getString("DETALLE_PROVEEDOR"));
        metadata.setProviderName(rs.getString("NOMBRE_PROVEEDOR"));
        metadata.setTelephoneNumber(rs.getString("NUMERO_LINEA"));
        metadata.setOrderId(rs.getString("ORDER_ID"));
        metadata.setAzureStatus(rs.getString("STATUS"));
        metadata.setFilename(rs.getString("ID_GRABACION"));

        return metadata;
    }

}
