package pe.com.tdp.ventafija.microservices.batch.jobs;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

public class RequestProcessorSkipPolicy implements SkipPolicy {

    @Override
    public boolean shouldSkip(Throwable t, int skipCount) throws SkipLimitExceededException {
        if (t instanceof RequestProcessorException) {
            return true;
        }
        return false;
    }

}
