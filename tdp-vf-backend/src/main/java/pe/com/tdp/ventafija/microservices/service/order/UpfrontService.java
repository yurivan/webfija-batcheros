package pe.com.tdp.ventafija.microservices.service.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.dao.impl.OrderRepositoryImpl;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.exception.ApplicationException;
import pe.com.tdp.ventafija.microservices.common.services.HDECService;
import pe.com.tdp.ventafija.microservices.domain.dto.PagoEfectivoNotificacionRequest;

@Service
public class UpfrontService {
    @Autowired
    private OrderService orderService;
    @Autowired
    private HDECService hdecService;
    @Autowired
    private OrderRepositoryImpl orderRepository;
    private static final Logger logger = LogManager.getLogger();

    public boolean updateStatusCIP(PagoEfectivoNotificacionRequest penr) {
        int n = 0;
        String estado = "";
        String motivo = "";
        String ordenVenta = "";
        String status = "";
        boolean flag=false;
        if (penr.getCip() != null) {
            if (penr.getCip().getIdEstado() != null) {
                if (penr.getCip().getIdEstado() == 23) {
                    estado = "PENDIENTE";
                    motivo = "CIP PAGADO";
                } else if (penr.getCip().getIdEstado() == 21) {
                    estado = "CAIDA";
                    motivo = "CIP EXPIRADO";
                } else if (penr.getCip().getIdEstado() == 25) {
                    estado = "CAIDA";
                    motivo = "CIP ELIMINADO";
                }

                ordenVenta = orderRepository.getOrderEstado(penr.getCip().getIdOrdenPago()).getOrdenVenta();
                status = orderRepository.getOrderEstado(penr.getCip().getIdOrdenPago()).getEstado();

                if (status.equals("PENDIENTE-PAGO")) {
                    if (penr.getCip().getIdEstado() == 21 || penr.getCip().getIdEstado() == 23 || penr.getCip().getIdEstado() == 25) {
                        flag=orderRepository.updateEstadoMotivo(estado,motivo,penr.getCip().getIdOrdenPago());
                    }
                    if (penr.getCip().getIdEstado() == 23) {
                        try {
                            //orderService.sendToHDEC(ordenVenta);
                            hdecService.sendDirectToHDEC(ordenVenta);
                        } catch (ApiClientException | ClientException e) {
                            if (e.getMessage() != null && e.getMessage().contains("Error: CODIGO_UNICO ya existe")) {
                                logger.info("Ya existe el codigo unico, el flujo continuara con el proceso");
                            } else {
                                logger.info("Error diferente al codigo unico en hdec, se detendra el flujo");
                            }
                        } catch (ApplicationException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return flag;
    }
}
