package pe.com.tdp.ventafija.microservices.domain.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportVentasRequest {

    @JsonProperty("socio")
    private String socio;
    @JsonProperty("campania")
    private String campania;
    @JsonProperty("opComercial")
    private String opComercial;

    @JsonProperty("canal")
    private String canal;
    @JsonProperty("region")
    private String region;
    @JsonProperty("zonal")
    private String zonal;
    @JsonProperty("ptoVenta")
    private String ptoVenta;

    @JsonProperty("fechaInicio")
    private String fechaInicio;
    @JsonProperty("fechaFinal")
    private String fechaFinal;

    @JsonProperty("codeFilter")
    private String codeFilter;


    public String getSocio() {
        return socio;
    }

    public void setSocio(String socio) {
        this.socio = socio;
    }

    public String getCampania() {
        return campania;
    }

    public void setCampania(String campania) {
        this.campania = campania;
    }

    public String getOpComercial() {
        return opComercial;
    }

    public void setOpComercial(String opComercial) {
        this.opComercial = opComercial;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZonal() {
        return zonal;
    }

    public void setZonal(String zonal) {
        this.zonal = zonal;
    }

    public String getPtoVenta() {
        return ptoVenta;
    }

    public void setPtoVenta(String ptoVenta) {
        this.ptoVenta = ptoVenta;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public String getCodeFilter() {
        return codeFilter;
    }

    public void setCodeFilter(String codeFilter) {
        this.codeFilter = codeFilter;
    }
}
