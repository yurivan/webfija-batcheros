package pe.com.tdp.ventafija.microservices.domain.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.pagoefectivo.service.ws.BEGenRequest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class PeticionPagoEfectivoDAO {

    private static final Logger logger = LogManager.getLogger();

    /**
     * Loads the data to be send to pago efectivo
     *
     * @param id to load from db
     * @return BEGenRequest
     */
    public BEGenRequest loadPagoEfectivoData(Integer id) {
        BEGenRequest data = null;
        try (Connection con = Database.datasource().getConnection()) {logger.info("555555552222222");
            PreparedStatement query = con.prepareStatement("select 'status', order_id, idmoneda, total, metodospago, codservicio,"
                    + "codtransaccion, emailcomercio, fechaaexpirar, usuarioid, dataadicional, usuarionombre, usuarioapellidos,"
                    + "usuariolocalidad,usuarioprovincia,usuariopais,usuarioalias,usuariotipodoc,usuarionumerodoc,usuarioemail,"
                    + "conceptopago,detalles,paramsurl,paramsemail from ibmx_a07e6d02edaf552.peticion_pago_efectivo where id = ?");

            query.setInt(1, id);
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                String status = rs.getString(1);
                String orderId = rs.getString(2);
                String idmoneda = rs.getString(3);
                String total = rs.getString(4);
                String metodospago = rs.getString(5);
                String codservicio = rs.getString(6);
                String codtransaccion = rs.getString(7);
                String emailcomercio = rs.getString(8);
                String fechaaexpirar = rs.getString(9);
                String usuarioid = rs.getString(10);
                String dataadicional = rs.getString(11);
                String usuarionombre = rs.getString(12);
                String usuarioapellidos = rs.getString(13);
                String usuariolocalidad = rs.getString(14);
                String usuarioprovincia = rs.getString(15);
                String usuariopais = rs.getString(16);
                String usuarioalias = rs.getString(17);
                String usuariotipodoc = rs.getString(18);
                String usuarionumerodoc = rs.getString(19);
                String usuarioemail = rs.getString(20);
                String conceptopago = rs.getString(21);
                String detalles = rs.getString(22);
                String paramsurl = rs.getString(23);
                String paramsemail = rs.getString(24);

                data = new BEGenRequest.Builder().setCod_servicio(codservicio)
                        .setConcepto_pago(conceptopago)
                        .setData_adicional(dataadicional)
                        .setEmail_comercio(emailcomercio)
                        .setFecha_expirar(fechaaexpirar)
                        .setMedio_pago(metodospago)
                        .setMoneda(idmoneda)
                        .setMonto(total)
                        .setNumero_orden(codtransaccion)
                        .setUsuario_alias(usuarioalias)
                        .setUsuario_apellidos(usuarioapellidos)
                        .setUsuario_localidad(usuariolocalidad)
                        .setUsuario_provincia(usuarioprovincia)
                        .setUsuario_email(usuarioemail)
                        .setUsuario_id(usuarioid)
                        .setUsuario_nombre(usuarionombre)
                        .setUsuario_numerodocumento(usuarionumerodoc)
                        .setUsuario_pais(usuariopais)
                        .setUsuario_tipodocumento(usuariotipodoc)
                        .build();
            }
        } catch (SQLException e) {
            logger.error("Error query.", e);
        }

        return data;
    }

    public void actualizarPagoEfectivoData(String id, String status, String CIP) {
        try (Connection con = Database.datasource().getConnection()) {
            PreparedStatement stm = con.prepareStatement("update ibmx_a07e6d02edaf552.peticion_pago_efectivo set status = ?, codpago = ? where order_id = ?");
            stm.setString(1, status);
            stm.setString(2, CIP);
            stm.setString(3, id);
            stm.executeUpdate();
        } catch (SQLException e) {
            logger.error("Error getMetadata DAO.", e);
        }
    }

}
