package pe.com.tdp.ventafija.microservices.domain.order.dto;

import java.util.Date;

public class AudioReviewResults {
	private int countAccepted;
	private int expectedCountAccepted;
	private Date now;
	private String orderId;
	private String transcript;

	public int getCountAccepted() {
		return countAccepted;
	}

	public void setCountAccepted(int countAccepted) {
		this.countAccepted = countAccepted;
	}

	public int getExpectedCountAccepted() {
		return expectedCountAccepted;
	}

	public void setExpectedCountAccepted(int expectedCountAccepted) {
		this.expectedCountAccepted = expectedCountAccepted;
	}

	public Date getNow() {
		return now;
	}

	public void setNow(Date now) {
		this.now = now;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTranscript() {
		return transcript;
	}

	public void setTranscript(String transcript) {
		this.transcript = transcript;
	}
}
