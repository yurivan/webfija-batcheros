package pe.com.tdp.ventafija.microservices.domain.order.parser;

import pe.com.tdp.ventafija.microservices.common.util.enums.AprobacionEnum;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.domain.order.entity.OrderDetail;

import java.math.BigDecimal;
import java.util.Date;

public class OrderDetailFirebaseParser implements FirebaseParser<OrderDetail> {

    @Override
    public OrderDetail parse(SaleApiFirebaseResponseBody objFirebase) {
        OrderDetail orderDetail = new OrderDetail();
        BigDecimal initCoordinateX = objFirebase.getLatitude_sales() == null ? BigDecimal.ZERO : new BigDecimal(objFirebase.getLatitude_sales());
        BigDecimal initCoordinateY = objFirebase.getLongitude_sales() == null ? BigDecimal.ZERO : new BigDecimal(objFirebase.getLongitude_sales());
        orderDetail.setInitCoordinateX(initCoordinateX);
        orderDetail.setInitCoordinateY(initCoordinateY);

        /* Condiciones de Venta */
        // Enviar contratos por email
        orderDetail.setSendContracts(objFirebase.getShipping_contracts_for_email() != null && objFirebase.getShipping_contracts_for_email() ? AprobacionEnum.SI.getLabel() : AprobacionEnum.NO.getLabel());
        // Pack Verde
        orderDetail.setEnableDigitalInvoice(objFirebase.getAffiliation_electronic_invoice() != null && objFirebase.getAffiliation_electronic_invoice() ? AprobacionEnum.SI.getLabel() : AprobacionEnum.NO.getLabel());
        orderDetail.setPublicationWhitePages(objFirebase.getPublication_white_pages_guide() != null && objFirebase.getPublication_white_pages_guide() ? AprobacionEnum.SI.getLabel() : AprobacionEnum.NO.getLabel());
        orderDetail.setDisableWhitePage(objFirebase.getDisaffiliation_white_pages_guide() != null && objFirebase.getDisaffiliation_white_pages_guide() ? AprobacionEnum.SI.getLabel() : AprobacionEnum.NO.getLabel());
        // Protección de datos
        orderDetail.setDataProtection(objFirebase.getAffiliation_data_protection() != null && objFirebase.getAffiliation_data_protection() ? AprobacionEnum.SI.getLabel() : AprobacionEnum.NO.getLabel());
        // Filtro web parental
        orderDetail.setParentalProtection(objFirebase.getParental_protection() != null && objFirebase.getParental_protection() ? AprobacionEnum.SI.getLabel() : AprobacionEnum.NO.getLabel());
        // Filtro debito automatico
        orderDetail.setAutomaticDebit(objFirebase.getAutomatic_debit() != null && objFirebase.getAutomatic_debit() ? AprobacionEnum.SI.getLabel() : AprobacionEnum.NO.getLabel());
        // Fecha y Hora de condiciones de Venta
        orderDetail.setConditionsFecha(objFirebase.getDatetime_sales_condition());
        /* Condiciones de Venta */

        orderDetail.setDepartment(objFirebase.getDepartment());
        orderDetail.setProvince(objFirebase.getProvince());
        orderDetail.setDistrict(objFirebase.getDistrict());
        orderDetail.setAddress(objFirebase.getAddress());
        orderDetail.setAddressComplement(objFirebase.getAddress_additional_info()!=null ?
                (objFirebase.getAddress_additional_info().equalsIgnoreCase("") ? objFirebase.getAddress_referencia() : objFirebase.getAddress_additional_info()): "");
        orderDetail.setRegisteredTime(new Date());
        orderDetail.setAltasTV(AprobacionEnum.NO.getLabel());
        orderDetail.setWinBackDiscount(" ");
        orderDetail.setHasRecording(AprobacionEnum.NO.getLabel());
        orderDetail.setInputDocMode(objFirebase.getInput_doc_mode());

        orderDetail.setExpertoCode(objFirebase.getOffering_expert_code());

        // Automatizador GIS
        orderDetail.setMsx_cbr_voi_ges_in(objFirebase.getMsx_cbr_voi_ges_in());
        orderDetail.setMsx_cbr_voi_gis_in(objFirebase.getMsx_cbr_voi_gis_in());
        orderDetail.setMsx_ind_snl_gis_cd(objFirebase.getMsx_ind_snl_gis_cd());
        orderDetail.setMsx_ind_gpo_gis_cd(objFirebase.getMsx_ind_gpo_gis_cd());
        orderDetail.setCod_ind_sen_cms(objFirebase.getCod_ind_sen_cms());
        orderDetail.setCod_cab_cms(objFirebase.getCod_cab_cms());
        orderDetail.setCod_fac_tec_cd(objFirebase.getCod_fac_tec_cd());
        orderDetail.setPs_adm_dep_1("16732");
        orderDetail.setCod_stc("");
        orderDetail.setCobre_blq_vta(objFirebase.getCobre_blq_vta());
        orderDetail.setCobre_blq_trm(objFirebase.getCobre_blq_trm());

        // Automatizador NORMALIZADOR
        orderDetail.setTip_cal_ati_cd(objFirebase.getAddress_tipoVia());
        orderDetail.setNom_cal_ds(objFirebase.getAddress_nombreVia());
        orderDetail.setNum_cal_nu(objFirebase.getAddress_numeroPuerta1());
        orderDetail.setCod_cnj_hbl_cd("");
        orderDetail.setTip_cnj_hbl_cd(objFirebase.getAddress_tipoUrbanizacion());
        orderDetail.setNom_cnj_hbl_no(objFirebase.getAddress_nombreUrbanizacion());


        //orderDetail.setPri_tip_cmp_cd();
        //orderDetail.setPri_cmp_dir_ds();


        return orderDetail;
    }

}
