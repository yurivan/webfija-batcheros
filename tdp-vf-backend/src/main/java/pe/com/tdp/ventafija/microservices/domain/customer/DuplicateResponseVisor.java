package pe.com.tdp.ventafija.microservices.domain.customer;

public class DuplicateResponseVisor {
  private String visorID;
  private String requestStatus;
  private String productName;
  private String orderCode;
  private String recordingDate;
  private String commercialOperation;

  public String getVisorID() {
    return visorID;
  }

  public void setVisorID(String visorID) {
    this.visorID = visorID;
  }

  public String getRequestStatus() {
    return requestStatus;
  }

  public void setRequestStatus(String requestStatus) {
    this.requestStatus = requestStatus;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getOrderCode() {
    return orderCode;
  }

  public void setOrderCode(String orderCode) {
    this.orderCode = orderCode;
  }

  public String getRecordingDate() {
    return recordingDate;
  }

  public void setRecordingDate(String recordingDate) {
    this.recordingDate = recordingDate;
  }

  public String getCommercialOperation() {
    return commercialOperation;
  }

  public void setCommercialOperation(String commercialOperation) {
    this.commercialOperation = commercialOperation;
  }

}
