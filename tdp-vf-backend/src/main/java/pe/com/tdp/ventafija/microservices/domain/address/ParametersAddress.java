package pe.com.tdp.ventafija.microservices.domain.address;

public class ParametersAddress {

    private String auxiliar;
    private String domain;
    private String category;
    private String element;
    private String strValue;
    private String strfilter;

    public String getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(String strValue) {
        this.strValue = strValue;
    }

    public String getStrfilter() {
        return strfilter;
    }

    public void setStrfilter(String strfilter) {
        this.strfilter = strfilter;
    }

}
