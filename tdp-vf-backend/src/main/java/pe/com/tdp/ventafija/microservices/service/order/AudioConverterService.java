package pe.com.tdp.ventafija.microservices.service.order;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;
import net.bramp.ffmpeg.probe.FFmpegProbeResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Date;

@Service
public class AudioConverterService {
	private static final Logger logger = LogManager.getLogger(AudioConverterService.class);
	private static final String GSM_FORMAT = "gsm";
	@Value("${ffmpeg.ffmpeg.exe.path}")
	private String ffmpegPath;
	@Value("${ffmpeg.ffmprobe.exe.path}")
	private String ffprobePath;




	public File convertToGsm (File inputFile) {
		logger.info("iniciando conversion de audio");
		File outputFile = null;
		File interFile = null;
		try {

			/*
			FFmpeg ffmpeg = new FFmpeg(ffmpegPath);
			FFprobe ffprobe = new FFprobe(ffprobePath);
			*/

/*
			ClassLoader classLoader = getClass().getClassLoader();
			File ffmpegFile = new File(classLoader.getResource(ffmpegPath).getFile());
			File ffprobeFile = new File(classLoader.getResource(ffprobePath).getFile());

			FFmpeg ffmpeg = new FFmpeg(ffmpegFile.getAbsolutePath());
			FFprobe ffprobe = new FFprobe(ffprobeFile.getAbsolutePath());
*/
		//	FFmpeg ffmpeg = new FFmpeg(ffmpegPath);
		//	FFprobe ffprobe = new FFprobe(ffprobePath);

		//	FFmpegProbeResult probeResult = ffprobe.probe(inputFile.getCanonicalPath());
			interFile = File.createTempFile("audio"+new Date().getTime(), ".wav");
			outputFile = File.createTempFile("audio"+new Date().getTime(), ".gsm");

			Runtime.getRuntime().exec(
					new String[] {
							"/opt/ffmpeg/ffmpeg",
							"-i",
							inputFile.getAbsolutePath(),
							"-vn",
							"-ar",
							"8000",
							"-ac",
							"1",
							"-ab",
							"13k",
							"-f",
							"gsm",
							outputFile.getAbsolutePath()
					}
			);

			/*FFmpegBuilder interBuilder = new FFmpegBuilder()
					.setInput(probeResult)
					.overrideOutputFiles(true)
					.addOutput(interFile.getCanonicalPath())
					.setAudioSampleRate(8_000)
					.setFormat("wav")
					.done();

			FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
			executor.createJob(interBuilder).run();

			FFmpegProbeResult probeResult2 = ffprobe.probe(interFile.getCanonicalPath());
			FFmpegBuilder builder = new FFmpegBuilder()
					.setInput(probeResult2)
					.overrideOutputFiles(true)

					.addOutput(outputFile.getCanonicalPath())
					.setAudioChannels(1)
					.setAudioBitRate(13_000)
					.setFormat(GSM_FORMAT)

					.done();

			executor.createJob(builder).run();
			*/
		} catch (IOException e) {
			logger.error("Error de conversion", e);
		} finally {
			if (interFile != null) {
				interFile.delete();
			}
		}

		logger.info("audio convertido:" + outputFile);
		return outputFile;
	}
}
