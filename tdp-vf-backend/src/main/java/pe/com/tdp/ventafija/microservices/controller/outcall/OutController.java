package pe.com.tdp.ventafija.microservices.controller.outcall;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.common.constant.CustomerConstants;
import pe.com.tdp.ventafija.microservices.common.domain.dto.SaleFileReport;
import pe.com.tdp.ventafija.microservices.common.domain.dto.SalesReport;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.order.OutRequestDetalle;
import pe.com.tdp.ventafija.microservices.domain.order.OutResponseDetalle;
import pe.com.tdp.ventafija.microservices.domain.order.VentaAudioRequest;
import pe.com.tdp.ventafija.microservices.domain.outcall.AudioRequest;
import pe.com.tdp.ventafija.microservices.repository.product.ProductDAO;
import pe.com.tdp.ventafija.microservices.service.order.OrderService;
import pe.com.tdp.ventafija.microservices.service.outcall.OutCallService;

import java.util.List;

@RestController
@RequestMapping(value = "/outcall")
public class OutController {

    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private OutCallService service;

    @Autowired
    private OrderService orderService;

    @Autowired
    private ProductDAO productDAO;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/aprobar", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response approveAudio(@RequestBody AudioRequest request) {
        LogVass.startController(logger, "GetAudioRequest", request);
        Response response = new Response();
        try {
            response=service.getApproveAudio(request);
        }catch (Exception e){
            logger.error("Error GetAudioRequest Controller.", e);
        }
        LogVass.finishController(logger, "GetApproveAudio", response);
        return response;
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/caida", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response failAudio(@RequestBody AudioRequest request) {
        LogVass.startController(logger, "GetAudioRequest", request);
        Response response = new Response();
        try {
            response=service.getFailAudio(request);
        }catch (Exception e){
            logger.error("Error GetAudioRequest Controller.", e);
        }
        LogVass.finishController(logger, "GetFailAudio", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/bandejaVentaArchivos", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<SaleFileReport>> getBandejaVentasAudio(@RequestBody VentaAudioRequest ventaAudioRequest) {
        Response<List<SaleFileReport>> response = new Response<>();
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
        response.setResponseData(

                orderService.getBandejaVentasArchivos(ventaAudioRequest.getCodigoVendedor(),
                         ventaAudioRequest.getFechaInicio(), ventaAudioRequest.getFechaFin())

        );
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/detalleVentaArchivo", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<OutResponseDetalle> getDetalleVentaArchivo(@RequestBody OutRequestDetalle request) {

        Response<OutResponseDetalle> response = new Response<>();
        try {
            OutResponseDetalle outResponseDetalle = productDAO.filterDetalleOut(request);
            response.setResponseData(outResponseDetalle);
            response.setResponseCode("0");
            response.setResponseMessage("getDetalleVentaArchivo ok");
        } catch (Exception e) {
            logger.error("Error getDetalleVentaArchivo Controller", e);
            response.setResponseCode("1");
            response.setResponseMessage("getDetalleVentaArchivo Error");
        }

        LogVass.finishController(logger, "detalleVentaArchivo", response);
        return response;
    }

}