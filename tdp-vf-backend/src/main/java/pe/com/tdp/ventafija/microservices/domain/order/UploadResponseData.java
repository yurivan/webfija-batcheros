package pe.com.tdp.ventafija.microservices.domain.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class UploadResponseData {
	@JsonInclude(Include.NON_NULL)
	private String numero;

	@JsonInclude(Include.NON_NULL)
	private String nombreArchivo;

	@JsonInclude(Include.NON_NULL)
	private String mensaje;
	
	@JsonInclude(Include.NON_NULL)
	private Boolean satisfactorio;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Boolean getSatisfactorio() {
		return satisfactorio;
	}

	public void setSatisfactorio(Boolean satisfactorio) {
		this.satisfactorio = satisfactorio;
	}
	
	

}
