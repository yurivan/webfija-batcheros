package pe.com.tdp.ventafija.microservices.domain.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_automatizador_cabecera", schema = "ibmx_a07e6d02edaf552")
public class TdpAutomatizadorCabecera {

    @Id
    @Column(name = "cabecera_id")
    private Integer cabeceraId;
    @Column(name = "cabecera")
    private String cabecera;
    @Column(name = "oficina")
    private String oficina;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "departamento")
    private String departamento;
    @Column(name = "tvtech")
    private String tvtech;

    public Integer getCabeceraId() {
        return cabeceraId;
    }

    public void setCabeceraId(Integer cabeceraId) {
        this.cabeceraId = cabeceraId;
    }

    public String getCabecera() {
        return cabecera;
    }

    public void setCabecera(String cabecera) {
        this.cabecera = cabecera;
    }

    public String getOficina() {
        return oficina;
    }

    public void setOficina(String oficina) {
        this.oficina = oficina;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getTvtech() {
        return tvtech;
    }

    public void setTvtech(String tvtech) {
        this.tvtech = tvtech;
    }
}
