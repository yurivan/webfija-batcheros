package pe.com.tdp.ventafija.microservices.domain.cip;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data {

    @JsonProperty("cip")
    private Integer cip;
    @JsonProperty("currency")
    private String currency;
    @JsonProperty("amount")
    private Integer amount;
    @JsonProperty("transactionCode")
    private String transactionCode;
    @JsonProperty("dateExpiry")
    private String dateExpiry;
    @JsonProperty("cipUrl")
    private String cipUrl;

    @JsonProperty("cip")
    public Integer getCip() {
        return cip;
    }

    @JsonProperty("cip")
    public void setCip(Integer cip) {
        this.cip = cip;
    }

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("amount")
    public Integer getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @JsonProperty("transactionCode")
    public String getTransactionCode() {
        return transactionCode;
    }

    @JsonProperty("transactionCode")
    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    @JsonProperty("dateExpiry")
    public String getDateExpiry() {
        return dateExpiry;
    }

    @JsonProperty("dateExpiry")
    public void setDateExpiry(String dateExpiry) {
        this.dateExpiry = dateExpiry;
    }

    @JsonProperty("cipUrl")
    public String getCipUrl() {
        return cipUrl;
    }

    @JsonProperty("cipUrl")
    public void setCipUrl(String cipUrl) {
        this.cipUrl = cipUrl;
    }
}