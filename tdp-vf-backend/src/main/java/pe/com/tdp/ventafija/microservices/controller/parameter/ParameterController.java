package pe.com.tdp.ventafija.microservices.controller.parameter;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.order.UserInfoRequest;

@RestController
public class ParameterController {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private ParametersRepository parametersRepository;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/tiposdocumento", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public ResponseEntity<List<Parameters>> getTiposDocumento(@RequestBody UserInfoRequest userInfoRequest) {
        LogVass.startController(logger, "GetTiposDocumento", userInfoRequest);
        // Variable utilizada para diferenciar entre la APP y la WEB
        String tipoAplicacion = "HERRAMIENTACONFIGURABLE" + userInfoRequest.getTipoAplicacion();
        // Pendiente al pasar las constantes la categoria "TIPOSDOCUMENTO"... y controlar errores... x el momento siempre se retorna HttpStatus.OK
        List<Parameters> parameters = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(tipoAplicacion, "TIPOSDOCUMENTO");

        ResponseEntity<List<Parameters>> response = new ResponseEntity<List<Parameters>>(parameters, HttpStatus.OK);
        LogVass.finishController(logger, "GetTiposDocumento", response);
        return response;
    }
}
