package pe.com.tdp.ventafija.microservices.domain.order;

import java.util.List;

public class ConectionRequest {
	private List<ConectionData> lstConectionData;

	public List<ConectionData> getLstConectionData() {
		return lstConectionData;
	}

	public void setLstConectionData(List<ConectionData> lstConectionData) {
		this.lstConectionData = lstConectionData;
	}
}
