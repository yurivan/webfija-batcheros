package pe.com.tdp.ventafija.microservices.domain.order;

public class OutRequestDetalle {
    private String order_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}
