package pe.com.tdp.ventafija.microservices;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.connection.Firebase;
import pe.com.tdp.ventafija.microservices.common.util.DatabasePropertiesSource;
import pe.com.tdp.ventafija.microservices.common.web.context.VentaFijaContextPersistenceFilter;
import pe.com.tdp.ventafija.microservices.common.web.context.VentaFijaTokenValidatorFilter;


@SpringBootApplication
public class EverisUserTdpApplication {
  private static final Logger logger = LogManager.getLogger();
  private static final String USER_SERVICE_CODE = "USER";

  @Value("${firebase.api.auth.key}")
  private String firebaseApiAuthKey;

  @Value("${firebase.api.salesdetail.url}")
  private String firebaseApiSalesdetailUrl;

  @Bean
  public static PropertySourcesPlaceholderConfigurer properties() {
    final PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
    DatabasePropertiesSource propertiesSource = new DatabasePropertiesSource(Database.datasource());
    Map<String, Object> props = propertiesSource.loadProperties();
    System.out.print(props);
    pspc.setIgnoreResourceNotFound(true);
    pspc.setIgnoreUnresolvablePlaceholders(true);
    Properties propps = new Properties();
    propps.putAll(props);
    pspc.setProperties(propps);
    return pspc;
  }

  @Bean
  public Filter log4jMvcFilter () {
    return new Log4JMvcFilter();
  }

  @Bean
  public Filter ventaFijaContextPersistenceFilter() {
    return new VentaFijaContextPersistenceFilter(USER_SERVICE_CODE);
  }
  
  @Bean
  public Filter ventaFijaTokenValidatorFilter() {
    return new VentaFijaTokenValidatorFilter();
  }
  
  @Bean
  public FilterRegistrationBean someFilterRegistration() {
      FilterRegistrationBean registration = new FilterRegistrationBean();
      registration.setFilter(log4jMvcFilter());
      registration.addUrlPatterns("*");
      registration.setName("log4jMvcFilter");
      registration.setOrder(1);
      return registration;
  }

  @Bean
  public TaskExecutor taskExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(5);
    executor.setMaxPoolSize(10);
    executor.setQueueCapacity(25);
    return executor;
  }

  @Bean
  public Firebase firebase () throws IOException {
    InputStream privateIn = EverisUserTdpApplication.class.getClassLoader().getResourceAsStream("private.gpg");
    String pgpPassphrase = "3v3r1s2017.";
    return new Firebase(firebaseApiAuthKey, firebaseApiSalesdetailUrl, privateIn, pgpPassphrase);
  }


  public static void main(String[] args) {
    SpringApplication.run(EverisUserTdpApplication.class, args);
    java.util.TimeZone.setDefault( java.util.TimeZone.getTimeZone("GMT-5") );
    logger.info("user service running");

  }
  
  @PostConstruct
  void started() {
      TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
  }

}