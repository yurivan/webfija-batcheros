package pe.com.tdp.ventafija.microservices.domain.product;

import java.util.List;

public class SvaResponseData {

	private List<SvaResponseDataSva> equipment;
	private List<SvaResponseDataSva> sva;

	public List<SvaResponseDataSva> getEquipment() {
		return equipment;
	}

	public void setEquipment(List<SvaResponseDataSva> equipment) {
		this.equipment = equipment;
	}

	public List<SvaResponseDataSva> getSva() {
		return sva;
	}

	public void setSva(List<SvaResponseDataSva> sva) {
		this.sva = sva;
	}

}
