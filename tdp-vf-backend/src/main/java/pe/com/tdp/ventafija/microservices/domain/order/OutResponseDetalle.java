package pe.com.tdp.ventafija.microservices.domain.order;

public class OutResponseDetalle {
    private String clientNombre;
    private String clienteLugarNacimiento;
    private String clientFechaNacimiento;
    private String clientNumeroTelefono;
    private String clientTipoDocumento;
    private String clientNumeroDoc;

    private String clientProducDepa;
    private String clientProducProvincia;
    private String clientProducDistrito;
    private String clientProducDireccion;
    private String clientProducReferencia;

    private String clientProducNombre;
    private String clientProducTecnologia;
    private String clientProducBeneficios;
    private String clientProducIncluye;
    private String clientProducEquipamento;
    private String clientProducModo;
    private String clientProducPrecioRegular;
    private String clientProducPrecioPromocional;


    private String clientSvaNombre1;
    private String clientSvaPrecio1;

    private String clientSvaNombre2;
    private String clientSvaPrecio2;

    private String clientSvaNombre3;
    private String clientSvaPrecio3;

    private String clientSvaNombre4;
    private String clientSvaPrecio4;

    private String clientSvaNombre5;
    private String clientSvaPrecio5;

    private String clientSvaNombre6;
    private String clientSvaPrecio6;

    private String clientSvaNombre7;
    private String clientSvaPrecio7;

    private String clientSvaNombre8;
    private String clientSvaPrecio8;

    private String clientSvaNombre9;
    private String clientSvaPrecio9;

    private String clientSvaNombre10;
    private String clientSvaPrecio10;


    private String clientProducPrecioPromocionalTotal;
    private String clientProducPrecioRegularTotal;

    private String clientTratamientoDatos;
    private String clientReciboDigital;
    private String clientFiltroParental;



    public String getClientNombre() {
        return clientNombre;
    }

    public void setClientNombre(String clientNombre) {
        this.clientNombre = clientNombre;
    }

    public String getClienteLugarNacimiento() {
        return clienteLugarNacimiento;
    }

    public void setClienteLugarNacimiento(String clienteLugarNacimiento) {
        this.clienteLugarNacimiento = clienteLugarNacimiento;
    }

    public String getClientFechaNacimiento() {
        return clientFechaNacimiento;
    }

    public void setClientFechaNacimiento(String clientFechaNacimiento) {
        this.clientFechaNacimiento = clientFechaNacimiento;
    }

    public String getClientNumeroTelefono() {
        return clientNumeroTelefono;
    }

    public void setClientNumeroTelefono(String clientNumeroTelefono) {
        this.clientNumeroTelefono = clientNumeroTelefono;
    }

    public String getClientTipoDocumento() {
        return clientTipoDocumento;
    }

    public void setClientTipoDocumento(String clientTipoDocumento) {
        this.clientTipoDocumento = clientTipoDocumento;
    }

    public String getClientNumeroDoc() {
        return clientNumeroDoc;
    }

    public void setClientNumeroDoc(String clientNumeroDoc) {
        this.clientNumeroDoc = clientNumeroDoc;
    }

    public String getClientProducDepa() {
        return clientProducDepa;
    }

    public void setClientProducDepa(String clientProducDepa) {
        this.clientProducDepa = clientProducDepa;
    }

    public String getClientProducProvincia() {
        return clientProducProvincia;
    }

    public void setClientProducProvincia(String clientProducProvincia) {
        this.clientProducProvincia = clientProducProvincia;
    }

    public String getClientProducDistrito() {
        return clientProducDistrito;
    }

    public void setClientProducDistrito(String clientProducDistrito) {
        this.clientProducDistrito = clientProducDistrito;
    }

    public String getClientProducDireccion() {
        return clientProducDireccion;
    }

    public void setClientProducDireccion(String clientProducDireccion) {
        this.clientProducDireccion = clientProducDireccion;
    }

    public String getClientProducReferencia() {
        return clientProducReferencia;
    }

    public void setClientProducReferencia(String clientProducReferencia) {
        this.clientProducReferencia = clientProducReferencia;
    }

    public String getClientProducNombre() {
        return clientProducNombre;
    }

    public void setClientProducNombre(String clientProducNombre) {
        this.clientProducNombre = clientProducNombre;
    }

    public String getClientProducTecnologia() {
        return clientProducTecnologia;
    }

    public void setClientProducTecnologia(String clientProducTecnologia) {
        this.clientProducTecnologia = clientProducTecnologia;
    }

    public String getClientProducBeneficios() {
        return clientProducBeneficios;
    }

    public void setClientProducBeneficios(String clientProducBeneficios) {
        this.clientProducBeneficios = clientProducBeneficios;
    }

    public String getClientProducIncluye() {
        return clientProducIncluye;
    }

    public void setClientProducIncluye(String clientProducIncluye) {
        this.clientProducIncluye = clientProducIncluye;
    }

    public String getClientProducEquipamento() {
        return clientProducEquipamento;
    }

    public void setClientProducEquipamento(String clientProducEquipamento) {
        this.clientProducEquipamento = clientProducEquipamento;
    }

    public String getClientProducModo() {
        return clientProducModo;
    }

    public void setClientProducModo(String clientProducModo) {
        this.clientProducModo = clientProducModo;
    }

    public String getClientProducPrecioRegular() {
        return clientProducPrecioRegular;
    }

    public void setClientProducPrecioRegular(String clientProducPrecioRegular) {
        this.clientProducPrecioRegular = clientProducPrecioRegular;
    }

    public String getClientProducPrecioPromocional() {
        return clientProducPrecioPromocional;
    }

    public void setClientProducPrecioPromocional(String clientProducPrecioPromocional) {
        this.clientProducPrecioPromocional = clientProducPrecioPromocional;
    }

    public String getClientSvaNombre1() {
        return clientSvaNombre1;
    }

    public void setClientSvaNombre1(String clientSvaNombre1) {
        this.clientSvaNombre1 = clientSvaNombre1;
    }

    public String getClientSvaPrecio1() {
        return clientSvaPrecio1;
    }

    public void setClientSvaPrecio1(String clientSvaPrecio1) {
        this.clientSvaPrecio1 = clientSvaPrecio1;
    }

    public String getClientSvaNombre2() {
        return clientSvaNombre2;
    }

    public void setClientSvaNombre2(String clientSvaNombre2) {
        this.clientSvaNombre2 = clientSvaNombre2;
    }

    public String getClientSvaPrecio2() {
        return clientSvaPrecio2;
    }

    public void setClientSvaPrecio2(String clientSvaPrecio2) {
        this.clientSvaPrecio2 = clientSvaPrecio2;
    }

    public String getClientSvaNombre3() {
        return clientSvaNombre3;
    }

    public void setClientSvaNombre3(String clientSvaNombre3) {
        this.clientSvaNombre3 = clientSvaNombre3;
    }

    public String getClientSvaPrecio3() {
        return clientSvaPrecio3;
    }

    public void setClientSvaPrecio3(String clientSvaPrecio3) {
        this.clientSvaPrecio3 = clientSvaPrecio3;
    }

    public String getClientSvaNombre4() {
        return clientSvaNombre4;
    }

    public void setClientSvaNombre4(String clientSvaNombre4) {
        this.clientSvaNombre4 = clientSvaNombre4;
    }

    public String getClientSvaPrecio4() {
        return clientSvaPrecio4;
    }

    public void setClientSvaPrecio4(String clientSvaPrecio4) {
        this.clientSvaPrecio4 = clientSvaPrecio4;
    }

    public String getClientSvaNombre5() {
        return clientSvaNombre5;
    }

    public void setClientSvaNombre5(String clientSvaNombre5) {
        this.clientSvaNombre5 = clientSvaNombre5;
    }

    public String getClientSvaPrecio5() {
        return clientSvaPrecio5;
    }

    public void setClientSvaPrecio5(String clientSvaPrecio5) {
        this.clientSvaPrecio5 = clientSvaPrecio5;
    }

    public String getClientSvaNombre6() {
        return clientSvaNombre6;
    }

    public void setClientSvaNombre6(String clientSvaNombre6) {
        this.clientSvaNombre6 = clientSvaNombre6;
    }

    public String getClientSvaPrecio6() {
        return clientSvaPrecio6;
    }

    public void setClientSvaPrecio6(String clientSvaPrecio6) {
        this.clientSvaPrecio6 = clientSvaPrecio6;
    }

    public String getClientSvaNombre7() {
        return clientSvaNombre7;
    }

    public void setClientSvaNombre7(String clientSvaNombre7) {
        this.clientSvaNombre7 = clientSvaNombre7;
    }

    public String getClientSvaPrecio7() {
        return clientSvaPrecio7;
    }

    public void setClientSvaPrecio7(String clientSvaPrecio7) {
        this.clientSvaPrecio7 = clientSvaPrecio7;
    }

    public String getClientSvaNombre8() {
        return clientSvaNombre8;
    }

    public void setClientSvaNombre8(String clientSvaNombre8) {
        this.clientSvaNombre8 = clientSvaNombre8;
    }

    public String getClientSvaPrecio8() {
        return clientSvaPrecio8;
    }

    public void setClientSvaPrecio8(String clientSvaPrecio8) {
        this.clientSvaPrecio8 = clientSvaPrecio8;
    }

    public String getClientSvaNombre9() {
        return clientSvaNombre9;
    }

    public void setClientSvaNombre9(String clientSvaNombre9) {
        this.clientSvaNombre9 = clientSvaNombre9;
    }

    public String getClientSvaPrecio9() {
        return clientSvaPrecio9;
    }

    public void setClientSvaPrecio9(String clientSvaPrecio9) {
        this.clientSvaPrecio9 = clientSvaPrecio9;
    }

    public String getClientSvaNombre10() {
        return clientSvaNombre10;
    }

    public void setClientSvaNombre10(String clientSvaNombre10) {
        this.clientSvaNombre10 = clientSvaNombre10;
    }

    public String getClientSvaPrecio10() {
        return clientSvaPrecio10;
    }

    public void setClientSvaPrecio10(String clientSvaPrecio10) {
        this.clientSvaPrecio10 = clientSvaPrecio10;
    }

    public String getClientProducPrecioPromocionalTotal() {
        return clientProducPrecioPromocionalTotal;
    }

    public void setClientProducPrecioPromocionalTotal(String clientProducPrecioPromocionalTotal) {
        this.clientProducPrecioPromocionalTotal = clientProducPrecioPromocionalTotal;
    }

    public String getClientProducPrecioRegularTotal() {
        return clientProducPrecioRegularTotal;
    }

    public void setClientProducPrecioRegularTotal(String clientProducPrecioRegularTotal) {
        this.clientProducPrecioRegularTotal = clientProducPrecioRegularTotal;
    }

    public String getClientTratamientoDatos() {
        return clientTratamientoDatos;
    }

    public void setClientTratamientoDatos(String clientTratamientoDatos) {
        this.clientTratamientoDatos = clientTratamientoDatos;
    }

    public String getClientReciboDigital() {
        return clientReciboDigital;
    }

    public void setClientReciboDigital(String clientReciboDigital) {
        this.clientReciboDigital = clientReciboDigital;
    }

    public String getClientFiltroParental() {
        return clientFiltroParental;
    }

    public void setClientFiltroParental(String clientFiltroParental) {
        this.clientFiltroParental = clientFiltroParental;
    }
}
