package pe.com.tdp.ventafija.microservices.domain.profile;

public class LoginConvergenciaResponse {

    private boolean enable;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

}
