package pe.com.tdp.ventafija.microservices.controller.address;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.address.*;
import pe.com.tdp.ventafija.microservices.service.address.AddressService;

import java.util.List;

@RestController
public class AddressController {

    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private AddressService service;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/address/geocodificardireccion", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<NormalizerResponse>> getNormalizer(@RequestBody NormalizerRequest request) {
        LogVass.startController(logger, "GetNormalizer", request);
        Response<List<NormalizerResponse>> response = new Response<>();
        try {
            response = service.getAddresses(request);
        } catch (Exception e) {
            logger.error("Error GetAddresses Controller.", e);
        }
        LogVass.finishController(logger, "GetNormalizer", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/address/geocodificardireccionv2", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<NormalizerResponse>> getNormalizerV2(@RequestBody NormalizerRequest request) {
        LogVass.startController(logger, "GetNormalizerVer2", request);
        Response<List<NormalizerResponse>> response = new Response<>();
        try {
            response = service.getAddressesV2(request);
        } catch (Exception e) {
            logger.error("Error GetAddresses V2 Controller.", e);
        }
        LogVass.finishController(logger, "GetNormalizerVer2", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/address/validateAddress", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<AddressValidateResponse> getMessageValidationAddress(@RequestBody AddressValidateRequest request) {
        LogVass.startController(logger, "GetMessageValidationAddress", request);
        Response<AddressValidateResponse> response = new Response<>();
        try {
            response = service.getMessageValidationAddress(request);
        } catch (Exception e) {
            logger.error("Error GetMessageValidationAddress Controller.", e);
        }
        LogVass.finishController(logger, "GetMessageValidationAddress", response);
        return response;
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/address/validateAtis", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response getValidateAtis(@RequestBody AddressValidateAtisRequest request) {
        LogVass.startController(logger, "GetValidateAtis", request);
        Response response = new Response();
        try {
            response = service.getMessageAtis(request);

            if (response == null) {
                response.setResponseCode("00");
                response.setResponseMessage("Normalizador - Error al usar validación ATIS, intente de nuevo.");
            }
        } catch (Exception e) {
            logger.error("Error GetValidateAtis Controller.", e);
            response.setResponseCode("00");
            response.setResponseMessage("Normalizador - Error al usar validación ATIS, intente de nuevo.");

        }

        LogVass.finishController(logger, "GetValidateAtis", response);
        return response;
    }



    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/address/config", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ConfigAddressResponse> getConfigAddress(@RequestBody ConfigAddressRequest request) {
        LogVass.startController(logger, "getConfigAddress", request);
        Response<ConfigAddressResponse> response = new Response<>();
        try {
            response = service.getAddressConfig(request);
        } catch (Exception e) {
            logger.error("Error GetAddresses Controller.", e);
        }
        LogVass.finishController(logger, "GetNormalizer", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/address/getCoordenadaXY", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<CoordenadasXYResponse>> getCoordenadas(@RequestBody CoordenadasXYRequest request){
        LogVass.startController(logger, "getCoordenadasXY", request);
        Response<List<CoordenadasXYResponse>> response = new Response<>();
        try {
            response = service.getCoordenadaXY(request);
        } catch (Exception e) {
            logger.error("Error GetAddresses Controller.", e);
        }
        LogVass.finishController(logger, "getCoordenadasXY", response);
        return response;
    }
}