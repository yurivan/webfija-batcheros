package pe.com.tdp.ventafija.microservices.constants.user;

public class UserConstants {
  
  public static final String LOGIN_RESPONSE_CODE_OK="0";
  public static final String LOGIN_RESPONSE_CODE_ERROR="1";
  public static final String RESET_TRUE="1";
  public static final String RESET_FALSE="0";
  
  public static final String REGISTER_RESPONSE_CODE_OK="0";
  public static final String REGISTER_RESPONSE_DATA="0 - Registro Exitoso";
  public static final String REGISTER_RESPONSE_CODE_ERROR="1";
  public static final String REGISTER_RESPONSE_MESSAGE="No se pudo realizar el registro del vendedor.";

  public static final String UPDATE_RESPONSE_CODE_OK="0";
  public static final String UPDATE_RESPONSE_DATA="0 - Cambio Exitoso.";
  public static final int UPDATE_UNABLE_TIME = 3600;
  
  private UserConstants(){
  }

}
