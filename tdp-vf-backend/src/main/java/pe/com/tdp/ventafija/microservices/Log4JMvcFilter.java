package pe.com.tdp.ventafija.microservices;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.slf4j.MDC;

public class Log4JMvcFilter implements Filter {

  @Override
  public void init(FilterConfig filterConfig) throws ServletException {
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    try {
      MDC.put("ipconnection", "127.0.0.1");
      MDC.put("hostname", "127.0.0.1");
      MDC.put("connectiontype", "MOVIL");
      chain.doFilter(request, response);
    } finally {
      MDC.remove("ipconnection");
      MDC.remove("hostname");
      MDC.remove("connectiontype");
    }
  }

  @Override
  public void destroy() {
  }

}
