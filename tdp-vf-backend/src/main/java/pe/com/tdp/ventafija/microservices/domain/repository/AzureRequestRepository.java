package pe.com.tdp.ventafija.microservices.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.tdp.ventafija.microservices.domain.order.entity.AzureRequest;

public interface AzureRequestRepository extends JpaRepository<AzureRequest, Integer> {

    List<AzureRequest> findByOrderId(String orderId);

    AzureRequest findOneByOrderId(String orderId);

}