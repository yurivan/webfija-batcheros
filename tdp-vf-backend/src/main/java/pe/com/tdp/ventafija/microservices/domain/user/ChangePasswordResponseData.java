package pe.com.tdp.ventafija.microservices.domain.user;

public class ChangePasswordResponseData {
  private String pwd;
  private String previousPwd;
  private int unableTime;
  private String resetPwd;

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }

  public String getPreviousPwd() {
    return previousPwd;
  }

  public void setPreviousPwd(String previousPwd) {
    this.previousPwd = previousPwd;
  }

  public int getUnableTime() {
    return unableTime;
  }

  public void setUnableTime(int unableTime) {
    this.unableTime = unableTime;
  }

  public String getResetPwd() {
    return resetPwd;
  }

  public void setResetPwd(String resetPwd) {
    this.resetPwd = resetPwd;
  }

}
