package pe.com.tdp.ventafija.microservices.domain.product.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_channels", schema="ibmx_a07e6d02edaf552")
public class TdpChannels {
	@Id
	@Column(name = "id")
	public Integer id;
	@Column(name = "channelatis")
	private String channelatis;
	@Column(name = "groupchannel")
	private String groupchannel;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getChannelatis() {
		return channelatis;
	}

	public void setChannelatis(String channelatis) {
		this.channelatis = channelatis;
	}

	public String getGroupchannel() {
		return groupchannel;
	}

	public void setGroupchannel(String groupchannel) {
		this.groupchannel = groupchannel;
	}
}
