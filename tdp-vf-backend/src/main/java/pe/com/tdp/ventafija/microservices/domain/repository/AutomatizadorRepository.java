package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.domain.automatizador.entity.Automatizador;

@Repository
public interface AutomatizadorRepository extends JpaRepository<Automatizador, Integer>, AutomatizadorRepositoryCustom {

}
