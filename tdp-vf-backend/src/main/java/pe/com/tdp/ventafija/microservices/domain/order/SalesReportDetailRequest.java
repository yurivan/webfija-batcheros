package pe.com.tdp.ventafija.microservices.domain.order;

public class SalesReportDetailRequest {

	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
