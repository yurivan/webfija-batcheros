package pe.com.tdp.ventafija.microservices.domain.product;

public class CampaignResponse {

    private String name;
    private Integer entityIdentifier;
    private Integer locationIdentifier;

    public CampaignResponse() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEntityIdentifier() {
        return entityIdentifier;
    }

    public void setEntityIdentifier(Integer entityIdentifier) {
        this.entityIdentifier = entityIdentifier;
    }

    public Integer getLocationIdentifier() {
        return locationIdentifier;
    }

    public void setLocationIdentifier(Integer locationIdentifier) {
        this.locationIdentifier = locationIdentifier;
    }
}
