package pe.com.tdp.ventafija.microservices.domain.configuration;

public class CanalEntidadRequest {

    private String  canal;

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }
}
