package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.User;

public interface UserRepository extends JpaRepository<User, String> {

    User findOneByIdAndStatus(String Id, Character status);
    User findOneByIdAndPwdAndStatus(String Id, String pwd, Character status);
}
