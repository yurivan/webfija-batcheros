package pe.com.tdp.ventafija.microservices.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ChangePasswordRequest {

	private String codAtis;
	private String pwd;
	private String newpwd;
	private String imei;
	@JsonIgnore
	private String previousPwd;
	@JsonIgnore
	private String resetPwd;

	public String getCodAtis() {
		return codAtis;
	}

	public void setCodAtis(String codAtis) {
		this.codAtis = codAtis;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getNewpwd() {
		return newpwd;
	}

	public void setNewpwd(String newpwd) {
		this.newpwd = newpwd;
	}

	public String getImei() {
    return imei;
  }

  public void setImei(String imei) {
    this.imei = imei;
  }

  public String getPreviousPwd() {
		return previousPwd;
	}

	public void setPreviousPwd(String previousPwd) {
		this.previousPwd = previousPwd;
	}

  public String getResetPwd() {
    return resetPwd;
  }

  public void setResetPwd(String resetPwd) {
    this.resetPwd = resetPwd;
  }

}
