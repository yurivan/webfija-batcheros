package pe.com.tdp.ventafija.microservices.service.product;

import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;
import pe.com.tdp.ventafija.microservices.domain.FilterResponse;
import pe.com.tdp.ventafija.microservices.domain.product.OffersRequest2;

/**
 * Service de Campanias
 *
 */
public interface CampaniaFilterService {

    /**
     * Obtiene el filtro de campanias para aplicar en la ficha de productos
     * @param requestOffers objeto que contiene los datos de consulta
     * @return filtro para aplicar en la ficha de productos
     */
    public FilterResponse getFilter(OffersRequest2 requestOffers);
}
