package pe.com.tdp.ventafija.microservices.repository.ftth;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.address.CoordenadasXYRequest;
import pe.com.tdp.ventafija.microservices.domain.address.CoordenadasXYResponse;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class FtthDao {

    private static final Logger logger = LogManager.getLogger();
    @Value("${datasource.query.select}")
    private String MY_SQL_QUERY_WHERE;
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;

    @Autowired
    private DataSource datasource;


    public List<CoordenadasXYResponse> getftth(CoordenadasXYRequest request) {

        List<CoordenadasXYResponse> list = null;

        try (Connection con = Database.datasource().getConnection()) {

            String query = "SELECT "+
                    "pointx,pointy,CONCAT(direccion,' ',numero,' ',"+
                    "NULLIF(CONCAT('Mz ',mz),'Mz 0'),' ',NULLIF(CONCAT('Lt ',lote),'Lt 0')"+
                    ",CONCAT('DPTO ',dpto),' ',CONCAT('BLK ',bloque))"+
                    " AS direccion"+
                    " FROM tdp_ftth WHERE departamento = '"+request.getDepartamento()+"'"+
                                                    "AND provincia = '"+request.getProvincia()+"'"+
                                                    "AND distrito = '"+request.getDistrito()+"'";
            logger.info(query);

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQuery(logger, stmt.toString());
                ResultSet rs = stmt.executeQuery();
                list = new ArrayList<>();
                while (rs.next()) {
                    CoordenadasXYResponse response = new CoordenadasXYResponse();
                    response.setCoordenadaX(rs.getString("pointx"));
                    response.setCoordenadaY(rs.getString("pointy"));
                    response.setDireccion(rs.getString("direccion"));
                    list.add(response);
                }
            }
        } catch (Exception e) {
            logger.info("Error readOferFlujo DAO.", e);
        }
        return list;
    }
}
