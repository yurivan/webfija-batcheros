package pe.com.tdp.ventafija.microservices.domain.order;

public class ConectionData {
	private String codAtis;
	private String firebaseId;
	private String documentNumber;
	private String modulo;
	private String mensaje;
	private String time;
	private String latitude_sales;
	private String longitude_sales;
	private String sourceApp;
	private String sourceAppVersion;

	public String getCodAtis() {
		return codAtis;
	}

	public void setCodAtis(String codAtis) {
		this.codAtis = codAtis;
	}

	public String getFirebaseId() {
		return firebaseId;
	}

	public void setFirebaseId(String firebaseId) {
		this.firebaseId = firebaseId;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getModulo() {
		return modulo;
	}

	public void setModulo(String modulo) {
		this.modulo = modulo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getLatitude_sales() {
		return latitude_sales;
	}

	public void setLatitude_sales(String latitude_sales) {
		this.latitude_sales = latitude_sales;
	}

	public String getLongitude_sales() {
		return longitude_sales;
	}

	public void setLongitude_sales(String longitude_sales) {
		this.longitude_sales = longitude_sales;
	}

	public String getSourceApp() {
		return sourceApp;
	}

	public void setSourceApp(String sourceApp) {
		this.sourceApp = sourceApp;
	}

	public String getSourceAppVersion() {
		return sourceAppVersion;
	}

	public void setSourceAppVersion(String sourceAppVersion) {
		this.sourceAppVersion = sourceAppVersion;
	}

	@Override
	public String toString() {
		return "ConectionData [codAtis=" + codAtis + ", firebaseId=" + firebaseId + ", documentNumber=" + documentNumber
				+ ", modulo=" + modulo + ", mensaje=" + mensaje + ", time=" + time + ", latitude_sales="
				+ latitude_sales + ", longitude_sales=" + longitude_sales + ", sourceApp=" + sourceApp
				+ ", sourceAppVersion=" + sourceAppVersion + "]";
	}
}
