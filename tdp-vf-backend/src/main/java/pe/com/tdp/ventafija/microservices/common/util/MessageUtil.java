package pe.com.tdp.ventafija.microservices.common.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.domain.message.MessageEntities;
import pe.com.tdp.ventafija.microservices.domain.message.TdpErrorMessageServiceEntities;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpErrorMessageServiceRepository;

import java.util.HashMap;
import java.util.List;

/**
 * Funciones para obtener mensajes de error o éxito según el servicio llamado
 *
 * @author Jhair Lozano
 * @date 2018-03-06
 */
@Service
public class MessageUtil {

    @Autowired
    public TdpErrorMessageServiceRepository tdpErrorMessageServiceRepository;
    @Autowired
    private MessageUtil messageUtil;

    /**
     * Función para obtener datos los mensajes según el servicio llamado
     *
     * @return HashMap<String                               ,                                                               MessageEntities>
     * @author Jhair Lozano
     * @date 2018-03-06
     * @request String domain, String category
     */
    public HashMap<String, MessageEntities> getMessages(String service) {
        List<TdpErrorMessageServiceEntities> objParameter = tdpErrorMessageServiceRepository.findByServiceOrderByOrdenAsc(service);

        HashMap<String, MessageEntities> messageList = new HashMap<String, MessageEntities>();
        for (TdpErrorMessageServiceEntities obj : objParameter) {
            MessageEntities mensaje = new MessageEntities("" + obj.getResponseCode(), obj.getResponseMessage());
            messageList.put(obj.getServiceKey(), mensaje);
        }
        return messageList;
    }

    // Sprint 11 - se interpreta mensaje de error HDEC... pendiente pasarlo a bd
    public String getMensajeErrorHDECInterpretado(Exception e, String mensajeAdicional) {
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("VENTA");
        if (e instanceof ClientException) {
            if (((ClientException) e).getEvent() == null) {
                // Si es que event es null significa que es un error en los datos enviados a HDEC
                return mensajeAdicional + " " + messageList.get("errorHdecDatosIncorrectos").getMessage();
            } else {
                // Si es que event es distinto de null significa que es un error en el lado del servidor HDEC
                //return "Ocurrio un error al subir audio. El servicio de HDEC no esta disponible. Por favor intente nuevamente.";
                return mensajeAdicional + " " + messageList.get("mensajeRegistrarPreVentaErrorHdec").getMessage();
            }

        } else {
            return mensajeAdicional + " Por favor intente nuevamente.";
        }
    }
}
