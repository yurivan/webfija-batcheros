package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpSalesAgent;

public interface TdpSalesAgentRepository extends JpaRepository<TdpSalesAgent, String> {

    TdpSalesAgent findOneByCodigoAtis(String codigoAtis);

}
