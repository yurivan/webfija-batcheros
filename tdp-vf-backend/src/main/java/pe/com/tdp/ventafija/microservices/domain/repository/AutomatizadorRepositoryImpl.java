package pe.com.tdp.ventafija.microservices.domain.repository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class AutomatizadorRepositoryImpl implements AutomatizadorRepositoryCustom {

    private static final Logger logger = LogManager.getLogger();

    @PersistenceContext
    private EntityManager em;


}
