package pe.com.tdp.ventafija.microservices.domain.customer.scoring;

public class CustomerScoringRequest {
	private String ApellidoPaterno;
	private String ApellidoMaterno;
	private String Nombres;
	private String TipoDeDocumento;
	private String NumeroDeDocumento;
	private String Departamento;
	private String Provincia;
	private String Distrito;
	private String Ubigeo;
	private String CodVendedor;
	private String Zonal;
	private String Canal;

	public String getApellidoPaterno() {
		return ApellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		ApellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return ApellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		ApellidoMaterno = apellidoMaterno;
	}

	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	public String getTipoDeDocumento() {
		return TipoDeDocumento;
	}

	public void setTipoDeDocumento(String tipoDeDocumento) {
		TipoDeDocumento = tipoDeDocumento;
	}

	public String getNumeroDeDocumento() {
		return NumeroDeDocumento;
	}

	public void setNumeroDeDocumento(String numeroDeDocumento) {
		NumeroDeDocumento = numeroDeDocumento;
	}

	public String getDepartamento() {
		return Departamento;
	}

	public void setDepartamento(String departamento) {
		Departamento = departamento;
	}

	public String getProvincia() {
		return Provincia;
	}

	public void setProvincia(String provincia) {
		Provincia = provincia;
	}

	public String getDistrito() {
		return Distrito;
	}

	public void setDistrito(String distrito) {
		Distrito = distrito;
	}

	public String getUbigeo() {
		return Ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		Ubigeo = ubigeo;
	}

	public String getCodVendedor() {
		return CodVendedor;
	}

	public void setCodVendedor(String codVendedor) {
		CodVendedor = codVendedor;
	}

	public String getZonal() {
		return Zonal;
	}

	public void setZonal(String zonal) {
		Zonal = zonal;
	}

	public String getCanal() {
		return Canal;
	}

	public void setCanal(String canal) {
		Canal = canal;
	}

	@Override
	public String toString() {
		return "CustomerScoringRequest [ApellidoPaterno=" + ApellidoPaterno + ", ApellidoMaterno=" + ApellidoMaterno
				+ ", Nombres=" + Nombres + ", TipoDeDocumento=" + TipoDeDocumento + ", NumeroDeDocumento="
				+ NumeroDeDocumento + ", Departamento=" + Departamento + ", Provincia=" + Provincia + ", Distrito="
				+ Distrito + ", Ubigeo=" + Ubigeo + ", CodVendedor=" + CodVendedor + ", Zonal=" + Zonal
				+ ", Canal=" + Canal + "]";
	}
}
