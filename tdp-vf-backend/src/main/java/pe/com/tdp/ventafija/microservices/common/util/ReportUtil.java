package pe.com.tdp.ventafija.microservices.common.util;

import net.sf.jasperreports.engine.JRParameter;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.jasperreports.JasperReportsXlsView;

import java.util.HashMap;
import java.util.Map;

public class ReportUtil {

    public static ModelAndView getModelAndView(ApplicationContext applicationContext, Object dataSource, String reportName, String reportType) {
        JasperReportsXlsView view = new JasperReportsXlsView();
        view.setUrl("classpath:/reports/" + reportName + ".jrxml");
        view.setApplicationContext(applicationContext);

        Map<String, Object> params = new HashMap<String, Object>();

        params.put("datasource", dataSource);
        params.put("TIPO_REPORTE", reportType);
        params.put(JRParameter.IS_IGNORE_PAGINATION, Boolean.TRUE);

        return new ModelAndView(view, params);
    }
}
