package pe.com.tdp.ventafija.microservices.domain.product;

import pe.com.tdp.ventafija.microservices.domain.customer.scoring.CustomerScoringResponse;

public class OffersRequest {

	private String documentType;
	private String documentNumber;
	private String department;
	private String province;
	private String district;
	private String ubigeoCode;
	private String atisSellerCode;
	private String coordinateX;
	private String coordinateY;
	private String phoneNumber;
	private String zonal;
	private String prodTypeCode;
	private String legacyCode;
	private CustomerScoringResponse scoringResponse;

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getUbigeoCode() {
		return ubigeoCode;
	}

	public void setUbigeoCode(String ubigeoCode) {
		this.ubigeoCode = ubigeoCode;
	}

	public String getAtisSellerCode() {
		return atisSellerCode;
	}

	public void setAtisSellerCode(String atisSellerCode) {
		this.atisSellerCode = atisSellerCode;
	}

	public String getCoordinateX() {
		return coordinateX;
	}

	public void setCoordinateX(String coordinateX) {
		this.coordinateX = coordinateX;
	}

	public String getCoordinateY() {
		return coordinateY;
	}

	public void setCoordinateY(String coordinateY) {
		this.coordinateY = coordinateY;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getZonal() {
		return zonal;
	}

	public void setZonal(String zonal) {
		this.zonal = zonal;
	}

	public String getProdTypeCode() {
		return prodTypeCode;
	}

	public void setProdTypeCode(String prodTypeCode) {
		this.prodTypeCode = prodTypeCode;
	}

	public String getLegacyCode() {
		return legacyCode;
	}

	public void setLegacyCode(String legacyCode) {
		this.legacyCode = legacyCode;
	}

	public CustomerScoringResponse getScoringResponse() {
		return scoringResponse;
	}

	public void setScoringResponse(CustomerScoringResponse scoringResponse) {
		this.scoringResponse = scoringResponse;
	}

}
