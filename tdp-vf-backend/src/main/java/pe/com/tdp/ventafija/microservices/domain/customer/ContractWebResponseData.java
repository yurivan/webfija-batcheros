package pe.com.tdp.ventafija.microservices.domain.customer;

import java.util.List;

public class ContractWebResponseData {

	private List<ContractWebResponseDataParagraph> contractWeb;

	public List<ContractWebResponseDataParagraph> getContractWeb() {
		return contractWeb;
	}

	public void setContractWeb(List<ContractWebResponseDataParagraph> contractWeb) {
		this.contractWeb = contractWeb;
	}
}
