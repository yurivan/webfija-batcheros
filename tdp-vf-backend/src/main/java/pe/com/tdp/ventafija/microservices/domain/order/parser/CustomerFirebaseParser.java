package pe.com.tdp.ventafija.microservices.domain.order.parser;

import java.util.Date;

import pe.com.tdp.ventafija.microservices.common.domain.entity.Customer;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;

public class CustomerFirebaseParser implements FirebaseParser<Customer> {

    @Override
    public Customer parse(SaleApiFirebaseResponseBody objFirebase) {
        Customer customer = new Customer(objFirebase.getClient_document(), objFirebase.getClient_doc_number());
        customer.setEmail(objFirebase.getClient_email());
        customer.setCustomerPhone(objFirebase.getClient_mobile_phone());
        customer.setCustomerPhone2(objFirebase.getClient_secondary_phone());
        customer.setFirstName(objFirebase.getClient_names());
        customer.setLastName1(objFirebase.getClient_lastname());
        customer.setLastName2(objFirebase.getClient_mother_lastname());

        // Sprint 7... verificamos datos adicionales
        Date birthdate = (Date) objFirebase.getAdditionalProperties().get("birthdate");
        String nationality = (String) objFirebase.getAdditionalProperties().get("nationality");

        if (birthdate == null)
            birthdate = (Date) objFirebase.getClient_birth_date();
        if (nationality == null)
            nationality = objFirebase.getClient_nationality();

        /* Sprint 12 - Guardar por defecto Peru como nacionalidad */
        if (nationality == null)
            nationality = "PE";
        /* Sprint 12 */

        customer.setBirthDate(birthdate);
        customer.setNationality(nationality);

        //Spring 24 RUC
        if(customer.getDocType().equals("RUC")){
            customer.setDoctyperrll(objFirebase.getClient_doctype_rrll());
            customer.setDocnumberrrll(objFirebase.getClient_numdoc_rrll());
            customer.setFullnamerrll(objFirebase.getClient_name_rrll());
        }

        return customer;
    }

}
