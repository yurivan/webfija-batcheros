package pe.com.tdp.ventafija.microservices.domain.configuration;

public class WhatsApp {

    private boolean onOff;
    private String canal;

    public boolean isOnOff() {
        return onOff;
    }

    public void setOnOff(boolean onOff) {
        this.onOff = onOff;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }
}
