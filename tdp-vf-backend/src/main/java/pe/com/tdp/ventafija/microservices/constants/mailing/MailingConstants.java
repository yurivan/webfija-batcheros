package pe.com.tdp.ventafija.microservices.constants.mailing;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MailingConstants {

    private static Integer SOLICITADO = 1;
    private static Integer REGISTRADO = 2;
    private static Integer TECNICO_ASIGNADO = 3;
    private static Integer INSTALADO = 4;

    private static Integer EXITO = 1;
    private static Integer RETRASO = 2;
    private static Integer CAIDA = 0;

    @Value("${tdp.mail.emblue.authenticate.uri}")
    private String emailAutenticateUri;

    @Value("${tdp.mail.emblue.checkconexion.uri}")
    private String emailCheckConexionUri;

    @Value("${tdp.mail.emblue.sendexpress.uri}")
    private String emailSendExpressUri;

    @Value("${tdp.mail.emblue.user}")
    private String emailUser;

    @Value("${tdp.mail.emblue.password}")
    private String emailPassword;

    @Value("${tdp.mail.emblue.token}")
    private String emailToken;

    @Value("${tdp.mail.emblue.sendmailexpresss.actionId}")
    private String emailSendExpressActionId;

    //@Value("alvin.puma@smart-home.com.pe|diana.ch.n.26@gmail.com|felix.vargas.vasslatam@gmail.com")
    //@Value("felix.vargas369@gmail.com|hugo.ayarza@vasslatam.com|hayarza2018@gmail.com")
    @Value("xmanu2015@gmail.com")
    private String receivers;

    @Value("${tdp.mail.traceability.subject.requested}")
    private String subjectSolicitado;

    @Value("${tdp.mail.traceability.subject.registered}")
    private String subjectRegistrado;

    @Value("${tdp.mail.traceability.subject.techassigned}")
    private String subjectAsignacionTecnica;

    @Value("${tdp.mail.traceability.subject.installed}")
    private String subjectInstalado;

    @Value("${tdp.mail.traceability.subject.comercialdev}")
    private String subjectDevueltaComercial;

    @Value("${tdp.mail.traceability.subject.technicaldev}")
    private String subjectDevueltaTecnica;

    @Value("${tdp.mail.traceability.subject.dc48}")
    private String subjectDC48;

    @Value("${tdp.mail.traceability.subject.registered_down}")
    private String subjectRegistradoCaida;

    @Value("${tdp.mail.traceability.subject.registered_delay}")
    private String subjectRegistradoRetraso;

    @Value("${tdp.mail.traceability.subject.techassigned_down}")
    private String subjectAsignacionTecnicaCaida;

    @Value("${tdp.mail.traceability.subject.techassigned_delay}")
    private String subjectAsignacionTecnicaRetraso;

    @Value("${tdp.mail.traceability.subject.installed_delay}")
    private String subjectInstaladoRetraso;

    @Value("${tdp.mail.traceability.subject.recoveryordercode}")
    private String subjectRecuperarCodigo;

    @Value("${tdp.traceability.enable.mailing}")
    private Boolean enableMailing;

    @Value("${tdp.traceability.enable.delayasign}")
    private Boolean enableDelayAsign;

    @Value("${tdp.traceability.enable.dropdtdc}")
    private Boolean enableDropDTDC;

    @Value("${tdp.traceability.enable.dc48}")
    private Boolean enableDC48;

    @Value("${tdp.traceability.time.rescheduledenable}")
    private String timeRescheduledEnable;

    @Value("${tdp.traceability.enablefilterbydistrict}")
    private Boolean enableFilterByDistrict;

    @Value("${tdp.traceability.districtsinfilter}")
    private String districtsInFilter;

    public MailingConstants(){
		/*emailAutenticateUri = "http://api.embluemail.com/Services/Emblue3Service.svc/json/Authenticate";
		emailCheckConexionUri = "http://api.embluemail.com/Services/Emblue3Service.svc/json/CheckConnection";
		emailSendExpressUri = "http://api.embluemail.com/Services/Emblue3Service.svc/json/sendMailExpress";
		emailUser = "developer@movistarplayperu.com";
		emailPassword = "Developer_movistarplayperu1";
		emailToken = "sUZJ0eb5-yQpgE-wRMcz-mzZFpYjg1v";
		emailSendExpressActionId = "537839";
		receivers = "felix.vargas@vasslatam.com";
		subjectSolicitado = "HEMOS RECIBIDO TU PEDIDO";
		subjectRegistrado = "TU PEDIDO HA SIDO REGISTRADO";*/
    }

    public String getEmailAutenticateUri() {
        return emailAutenticateUri;
    }

    public void setEmailAutenticateUri(String emailAutenticateUri) {
        this.emailAutenticateUri = emailAutenticateUri;
    }

    public String getEmailCheckConexionUri() {
        return emailCheckConexionUri;
    }

    public void setEmailCheckConexionUri(String emailCheckConexionUri) {
        this.emailCheckConexionUri = emailCheckConexionUri;
    }

    public String getEmailSendExpressUri() {
        return emailSendExpressUri;
    }

    public void setEmailSendExpressUri(String emailSendExpressUri) {
        this.emailSendExpressUri = emailSendExpressUri;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public String getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(String emailToken) {
        this.emailToken = emailToken;
    }

    public String getEmailSendExpressActionId() {
        return emailSendExpressActionId;
    }

    public void setEmailSendExpressActionId(String emailSendExpressActionId) {
        this.emailSendExpressActionId = emailSendExpressActionId;
    }

    public String getReceivers() {
        return receivers;
    }

    public void setReceivers(String receivers) {
        this.receivers = receivers;
    }

    public String getSubjectSolicitado() {
        return subjectSolicitado;
    }

    public void setSubjectSolicitado(String subjectSolicitado) {
        this.subjectSolicitado = subjectSolicitado;
    }

    public String getSubjectRegistrado() {
        return subjectRegistrado;
    }

    public void setSubjectRegistrado(String subjectRegistrado) {
        this.subjectRegistrado = subjectRegistrado;
    }

    public String getSubjectAsignacionTecnica() {
        return subjectAsignacionTecnica;
    }

    public void setSubjectAsignacionTecnica(String subjectAsignacionTecnica) {
        this.subjectAsignacionTecnica = subjectAsignacionTecnica;
    }

    public String getSubjectInstalado() {
        return subjectInstalado;
    }

    public void setSubjectInstalado(String subjectInstalado) {
        this.subjectInstalado = subjectInstalado;
    }

    public String getSubjectDevueltaComercial() {
        return subjectDevueltaComercial;
    }

    public void setSubjectDevueltaComercial(String subjectDevueltaComercial) {
        this.subjectDevueltaComercial = subjectDevueltaComercial;
    }

    public String getSubjectDevueltaTecnica() {
        return subjectDevueltaTecnica;
    }

    public void setSubjectDevueltaTecnica(String subjectDevueltaTecnica) {
        this.subjectDevueltaTecnica = subjectDevueltaTecnica;
    }

    public String getSubjectDC48() {
        return subjectDC48;
    }

    public void setSubjectDC48(String subjectDC48) {
        this.subjectDC48 = subjectDC48;
    }

    public String getSubjectRegistradoCaida() {
        return subjectRegistradoCaida;
    }

    public void setSubjectRegistradoCaida(String subjectRegistradoCaida) {
        this.subjectRegistradoCaida = subjectRegistradoCaida;
    }

    public String getSubjectRegistradoRetraso() {
        return subjectRegistradoRetraso;
    }

    public void setSubjectRegistradoRetraso(String subjectRegistradoRetraso) {
        this.subjectRegistradoRetraso = subjectRegistradoRetraso;
    }

    public String getSubjectAsignacionTecnicaCaida() {
        return subjectAsignacionTecnicaCaida;
    }

    public void setSubjectAsignacionTecnicaCaida(String subjectAsignacionTecnicaCaida) {
        this.subjectAsignacionTecnicaCaida = subjectAsignacionTecnicaCaida;
    }

    public String getSubjectAsignacionTecnicaRetraso() {
        return subjectAsignacionTecnicaRetraso;
    }

    public void setSubjectAsignacionTecnicaRetraso(String subjectAsignacionTecnicaRetraso) {
        this.subjectAsignacionTecnicaRetraso = subjectAsignacionTecnicaRetraso;
    }

    public String getSubjectInstaladoRetraso() {
        return subjectInstaladoRetraso;
    }

    public void setSubjectInstaladoRetraso(String subjectInstaladoRetraso) {
        this.subjectInstaladoRetraso = subjectInstaladoRetraso;
    }

    public String getSubjectRecuperarCodigo() {
        return subjectRecuperarCodigo;
    }

    public void setSubjectRecuperarCodigo(String subjectRecuperarCodigo) {
        this.subjectRecuperarCodigo = subjectRecuperarCodigo;
    }

    public String getTimeRescheduledEnable() {
        return timeRescheduledEnable;
    }

    public void setTimeRescheduledEnable(String timeRescheduledEnable) {
        this.timeRescheduledEnable = timeRescheduledEnable;
    }

    public Boolean getEnableFilterByDistrict() {
        return enableFilterByDistrict;
    }

    public void setEnableFilterByDistrict(Boolean enableFilterByDistrict) {
        this.enableFilterByDistrict = enableFilterByDistrict;
    }

    public String getDistrictsInFilter() {
        return districtsInFilter;
    }

    public void setDistrictsInFilter(String districtsInFilter) {
        this.districtsInFilter = districtsInFilter;
    }

    public Boolean getEnableMailing() {
        return enableMailing;
    }

    public void setEnableMailing(Boolean enableMailing) {
        this.enableMailing = enableMailing;
    }

    public Boolean getEnableDelayAsign() {
        return enableDelayAsign;
    }

    public void setEnableDelayAsign(Boolean enableDelayAsign) {
        this.enableDelayAsign = enableDelayAsign;
    }

    public Boolean getEnableDropDTDC() {
        return enableDropDTDC;
    }

    public void setEnableDropDTDC(Boolean enableDropDTDC) {
        this.enableDropDTDC = enableDropDTDC;
    }

    public Boolean getEnableDC48() {
        return enableDC48;
    }

    public void setEnableDC48(Boolean enableDC48) {
        this.enableDC48 = enableDC48;
    }
}
