package pe.com.tdp.ventafija.microservices.domain.product;

public class OutOffersPlantaRequest {
    private String phoneOrCms;
    private String legacyCode;
    private String commercialOperation;
    //Sprint 25 calculadora
    private String psprincipal;
    private String productDescription;

    public String getPhoneOrCms() {
        return phoneOrCms;
    }

    public void setPhoneOrCms(String phoneOrCms) {
        this.phoneOrCms = phoneOrCms;
    }

    public String getLegacyCode() {
        return legacyCode;
    }

    public void setLegacyCode(String legacyCode) {
        this.legacyCode = legacyCode;
    }

    public String getCommercialOperation() {
        return commercialOperation;
    }

    public void setCommercialOperation(String commercialOperation) {
        this.commercialOperation = commercialOperation;
    }

    public String getPsprincipal() { return psprincipal; }

    public void setPsprincipal(String psprincipal) { this.psprincipal = psprincipal; }

    public String getProductDescription() { return productDescription; }

    public void setProductDescription(String productDescription) {this.productDescription = productDescription;}

}
