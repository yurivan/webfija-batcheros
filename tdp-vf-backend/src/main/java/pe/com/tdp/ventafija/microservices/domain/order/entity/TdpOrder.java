package pe.com.tdp.ventafija.microservices.domain.order.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import pe.com.tdp.ventafija.microservices.domain.address.ConfigAddressRequest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "tdp_order", schema = "ibmx_a07e6d02edaf552")
public class TdpOrder {

    @Id
    @Column(name = "order_id")
    private String orderId;
    @Column(name = "order_fecha_hora")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    private Date orderFechaHora;
    @Column(name = "order_operation_commercial")
    private String orderOperationCommercial;
    @Column(name = "order_operation_number")
    private Integer orderOperationNumber;
    @Column(name = "order_experto")
    private String orderExperto;
    @Column(name = "order_gps_x")
    private BigDecimal orderGpsX;
    @Column(name = "order_gps_y")
    private BigDecimal orderGpsY;
    @Column(name = "order_parque_telefono")
    private String orderParqueTelefono;
    @Column(name = "order_gis_xdsl")
    private String orderGisXdsl;
    @Column(name = "order_gis_hfc")
    private String orderGisHfc;
    @Column(name = "order_gis_cable")
    private String orderGisCable;
    @Column(name = "order_gis_gpon")
    private String orderGisGpon;
    @Column(name = "order_gis_ntlt")
    private String orderGisNtlt;
    @Column(name = "order_call_tipo")
    private String orderCallTipo;
    @Column(name = "order_call_desc")
    private String orderCallDesc;
    @Column(name = "order_modelo")
    private String orderModelo;
    @Column(name = "order_origen")
    private String orderOrigen;
    @Column(name = "affiliation_electronic_invoice")
    private String affiliationElectronicInvoice;
    @Column(name = "affiliation_data_protection")
    private String affiliationDataProtection;
    @Column(name = "affiliation_parental_protection")
    private String affiliationParentalProtection;
    @Column(name = "disaffiliation_white_pages_guide")
    private String disaffiliationWhitePagesGuide;
    @Column(name = "order_contrato")
    private String orderContrato;
    @Column(name = "order_grabacion")
    private String orderGrabacion;
    @Column(name = "order_modalidad_acept")
    private String orderModalidadAcept;
    @Column(name = "order_cip")
    private String orderCip;
    @Column(name = "order_stc6i")
    private String orderStc6i;
    @Column(name = "user_atis")
    private String userAtis;
    @Column(name = "user_fuerza_venta")
    private String userFuerzaVenta;
    @Column(name = "user_canal_codigo")
    private String userCanalCodigo;
    @Column(name = "user_cms_punto_venta")
    private Integer userCmsPuntoVenta;
    @Column(name = "user_cms")
    private Integer userCms;
    @Column(name = "user_nombre")
    private String userNombre;
    @Column(name = "user_dni")
    private String userDni;
    @Column(name = "user_telefono")
    private String userTelefono;
    @Column(name = "user_entidad")
    private String userEntidad;
    @Column(name = "user_region")
    private String userRegion;
    @Column(name = "user_zonal")
    private String userZonal;
    @Column(name = "client_tipo_doc")
    private String clientTipoDoc;
    @Column(name = "client_numero_doc")
    private String clientNumeroDoc;
    @Column(name = "client_ruc_digit_ctrl")
    private String clientRucDigitCtrl;
    @Column(name = "client_ruc_actividad")
    private Integer clientRucActividad;
    @Column(name = "client_ruc_subactividad")
    private Integer clientRucSubactividad;
    @Column(name = "client_nombre")
    private String clientNombre;
    @Column(name = "client_apellido_paterno")
    private String clientApellidoPaterno;
    @Column(name = "client_apellido_materno")
    private String clientApellidoMaterno;
    @Column(name = "client_nacimiento")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    private Date clientNacimiento;
    @Column(name = "client_sexo")
    private String clientSexo;
    @Column(name = "client_civil")
    private String clientCivil;
    @Column(name = "client_email")
    private String clientEmail;
    @Column(name = "client_nationality")
    private String clientNationality;
    @Column(name = "client_telefono_1_area")
    private String clientTelefono1Area;
    @Column(name = "client_telefono_1")
    private String clientTelefono1;
    @Column(name = "client_telefono_2_area")
    private String clientTelefono2Area;
    @Column(name = "client_telefono_2")
    private String clientTelefono2;
    @Column(name = "client_cms_codigo")
    private Integer clientCmsCodigo;
    @Column(name = "client_cms_servicio")
    private String clientCmsServicio;
    @Column(name = "client_cms_factura")
    private Integer clientCmsFactura;
    @Column(name = "product_codigo")
    private String productCodigo;
    @Column(name = "product_ps")
    private String productPs;
    @Column(name = "product_precio_normal")
    private BigDecimal productPrecioNormal;
    @Column(name = "product_precio_promo")
    private BigDecimal productPrecioPromo;
    @Column(name = "product_precio_promo_mes")
    private Integer productPrecioPromoMes;
    @Column(name = "product_internet_vel")
    private Integer productInternetVel;
    @Column(name = "product_internet_promo")
    private Integer productInternetPromo;
    @Column(name = "product_internet_promo_tiempo")
    private Integer productInternetPromoTiempo;
    @Column(name = "product_campaing")
    private String productCampaing;
    @Column(name = "product_type")
    private String productType;
    @Column(name = "product_category")
    private String productCategory;
    @Column(name = "product_pay_method")
    private String productPayMethod;
    @Column(name = "product_pay_price")
    private BigDecimal productPayPrice;
    @Column(name = "product_pay_return_month")
    private Integer productPayReturnMonth;
    @Column(name = "product_pay_return_period")
    private String productPayReturnPeriod;
    @Column(name = "product_cost_install")
    private BigDecimal productCostInstall;
    @Column(name = "product_cost_install_month")
    private Integer productCostInstallMonth;
    @Column(name = "product_tec_inter")
    private String productTecInter;
    @Column(name = "product_tec_tv")
    private String productTecTv;
    @Column(name = "product_tv_senal")
    private String productTvSenal;
    @Column(name = "product_equip_tv")
    private String productEquipTv;
    @Column(name = "product_equip_inter")
    private String productEquipInter;
    @Column(name = "product_equip_line")
    private String productEquipLine;
    @Column(name = "product_tipo_reg")
    private String productTipoReg;
    @Column(name = "product_senal")
    private String productSenal;
    @Column(name = "product_cabecera")
    private String productCabecera;
    @Column(name = "product_ps_admin_1")
    private Integer productPsAdmin1;
    @Column(name = "product_ps_admin_2")
    private Integer productPsAdmin2;
    @Column(name = "product_ps_admin_3")
    private Integer productPsAdmin3;
    @Column(name = "product_ps_admin_4")
    private Integer productPsAdmin4;
    @Column(name = "sva_code_1")
    private String svaCode1;
    @Column(name = "sva_codigo_1")
    private String svaCodigo1;
    @Column(name = "sva_nombre_1")
    private String svaNombre1;
    @Column(name = "sva_precio_1")
    private BigDecimal svaPrecio1;
    @Column(name = "sva_cantidad_1")
    private Integer svaCantidad1;
    @Column(name = "sva_code_2")
    private String svaCode2;
    @Column(name = "sva_codigo_2")
    private String svaCodigo2;
    @Column(name = "sva_nombre_2")
    private String svaNombre2;
    @Column(name = "sva_precio_2")
    private BigDecimal svaPrecio2;
    @Column(name = "sva_cantidad_2")
    private Integer svaCantidad2;
    @Column(name = "sva_code_3")
    private String svaCode3;
    @Column(name = "sva_codigo_3")
    private String svaCodigo3;
    @Column(name = "sva_nombre_3")
    private String svaNombre3;
    @Column(name = "sva_precio_3")
    private BigDecimal svaPrecio3;
    @Column(name = "sva_cantidad_3")
    private Integer svaCantidad3;
    @Column(name = "sva_code_4")
    private String svaCode4;
    @Column(name = "sva_codigo_4")
    private String svaCodigo4;
    @Column(name = "sva_nombre_4")
    private String svaNombre4;
    @Column(name = "sva_precio_4")
    private BigDecimal svaPrecio4;
    @Column(name = "sva_cantidad_4")
    private Integer svaCantidad4;
    @Column(name = "sva_code_5")
    private String svaCode5;
    @Column(name = "sva_codigo_5")
    private String svaCodigo5;
    @Column(name = "sva_nombre_5")
    private String svaNombre5;
    @Column(name = "sva_precio_5")
    private BigDecimal svaPrecio5;
    @Column(name = "sva_cantidad_5")
    private Integer svaCantidad5;
    @Column(name = "sva_code_6")
    private String svaCode6;
    @Column(name = "sva_codigo_6")
    private String svaCodigo6;
    @Column(name = "sva_nombre_6")
    private String svaNombre6;
    @Column(name = "sva_precio_6")
    private BigDecimal svaPrecio6;
    @Column(name = "sva_cantidad_6")
    private Integer svaCantidad6;
    @Column(name = "sva_code_7")
    private String svaCode7;
    @Column(name = "sva_codigo_7")
    private String svaCodigo7;
    @Column(name = "sva_nombre_7")
    private String svaNombre7;
    @Column(name = "sva_precio_7")
    private BigDecimal svaPrecio7;
    @Column(name = "sva_cantidad_7")
    private Integer svaCantidad7;
    @Column(name = "sva_code_8")
    private String svaCode8;
    @Column(name = "sva_codigo_8")
    private String svaCodigo8;
    @Column(name = "sva_nombre_8")
    private String svaNombre8;
    @Column(name = "sva_precio_8")
    private BigDecimal svaPrecio8;
    @Column(name = "sva_cantidad_8")
    private Integer svaCantidad8;
    @Column(name = "sva_code_9")
    private String svaCode9;
    @Column(name = "sva_codigo_9")
    private String svaCodigo9;
    @Column(name = "sva_nombre_9")
    private String svaNombre9;
    @Column(name = "sva_precio_9")
    private BigDecimal svaPrecio9;
    @Column(name = "sva_cantidad_9")
    private Integer svaCantidad9;
    @Column(name = "sva_code_10")
    private String svaCode10;
    @Column(name = "sva_codigo_10")
    private String svaCodigo10;
    @Column(name = "sva_nombre_10")
    private String svaNombre10;
    @Column(name = "sva_precio_10")
    private BigDecimal svaPrecio10;
    @Column(name = "sva_cantidad_10")
    private Integer svaCantidad10;
    @Column(name = "address_departamento_cod")
    private String addressDepartamentoCod;
    @Column(name = "address_departamento")
    private String addressDepartamento;
    @Column(name = "address_provincia_cod")
    private String addressProvinciaCod;
    @Column(name = "address_provincia")
    private String addressProvincia;
    @Column(name = "address_distrito_cod")
    private String addressDistritoCod;
    @Column(name = "address_distrito")
    private String addressDistrito;
    @Column(name = "address_principal")
    private String addressPrincipal;
    @Column(name = "address_calle_atis")
    private String addressCalleAtis;
    @Column(name = "address_calle_nombre")
    private String addressCalleNombre;
    @Column(name = "address_calle_numero")
    private String addressCalleNumero;
    @Column(name = "address_cchh_codigo")
    private Integer addressCchhCodigo;
    @Column(name = "address_cchh_tipo")
    private String addressCchhTipo;
    @Column(name = "address_cchh_nombre")
    private String addressCchhNombre;
    @Column(name = "address_cchh_comp_tip_1")
    private String addressCchhCompTip1;
    @Column(name = "address_cchh_comp_nom_1")
    private String addressCchhCompNom1;
    @Column(name = "address_cchh_comp_tip_2")
    private String addressCchhCompTip2;
    @Column(name = "address_cchh_comp_nom_2")
    private String addressCchhCompNom2;
    @Column(name = "address_cchh_comp_tip_3")
    private String addressCchhCompTip3;
    @Column(name = "address_cchh_comp_nom_3")
    private String addressCchhCompNom3;
    @Column(name = "address_cchh_comp_tip_4")
    private String addressCchhCompTip4;
    @Column(name = "address_cchh_comp_nom_4")
    private String addressCchhCompNom4;
    @Column(name = "address_cchh_comp_tip_5")
    private String addressCchhCompTip5;
    @Column(name = "address_cchh_comp_nom_5")
    private String addressCchhCompNom5;
    @Column(name = "address_via_comp_tip_1")
    private String addressViaCompTip1;
    @Column(name = "address_via_comp_nom_1")
    private String addressViaCompNom1;
    @Column(name = "address_via_comp_tip_2")
    private String addressViaCompTip2;
    @Column(name = "address_via_comp_nom_2")
    private String addressViaCompNom2;
    @Column(name = "address_via_comp_tip_3")
    private String addressViaCompTip3;
    @Column(name = "address_via_comp_nom_3")
    private String addressViaCompNom3;
    @Column(name = "address_via_comp_tip_4")
    private String addressViaCompTip4;
    @Column(name = "address_via_comp_nom_4")
    private String addressViaCompNom4;
    @Column(name = "address_via_comp_tip_5")
    private String addressViaCompTip5;
    @Column(name = "address_via_comp_nom_5")
    private String addressViaCompNom5;
    @Column(name = "address_referencia")
    private String addressReferencia;
    @Column(name = "auditoria_create")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    private Date auditoriaCreate;
    @Column(name = "auditoria_modify")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    private Date auditoriaModify;
    @Column(name = "automatizador_ok")
    private Integer automatizadorOK;
    @Column(name = "fecha_pago_cms_atis")
    private Date fechaPagoCmsAtis;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getOrderFechaHora() {
        return orderFechaHora;
    }

    public void setOrderFechaHora(Date orderFechaHora) {
        this.orderFechaHora = orderFechaHora;
    }

    public String getOrderOperationCommercial() {
        return orderOperationCommercial;
    }

    public void setOrderOperationCommercial(String orderOperationCommercial) {
        this.orderOperationCommercial = orderOperationCommercial;
    }

    public Integer getOrderOperationNumber() {
        return orderOperationNumber;
    }

    public void setOrderOperationNumber(Integer orderOperationNumber) {
        this.orderOperationNumber = orderOperationNumber;
    }

    public String getOrderExperto() {
        return orderExperto;
    }

    public void setOrderExperto(String orderExperto) {
        this.orderExperto = orderExperto;
    }

    public BigDecimal getOrderGpsX() {
        return orderGpsX;
    }

    public void setOrderGpsX(BigDecimal orderGpsX) {
        this.orderGpsX = orderGpsX;
    }

    public BigDecimal getOrderGpsY() {
        return orderGpsY;
    }

    public void setOrderGpsY(BigDecimal orderGpsY) {
        this.orderGpsY = orderGpsY;
    }

    public String getOrderParqueTelefono() {
        return orderParqueTelefono;
    }

    public void setOrderParqueTelefono(String orderParqueTelefono) {
        this.orderParqueTelefono = orderParqueTelefono;
    }

    public String getOrderGisXdsl() {
        return orderGisXdsl;
    }

    public void setOrderGisXdsl(String orderGisXdsl) {
        this.orderGisXdsl = orderGisXdsl;
    }

    public String getOrderGisHfc() {
        return orderGisHfc;
    }

    public void setOrderGisHfc(String orderGisHfc) {
        this.orderGisHfc = orderGisHfc;
    }

    public String getOrderGisCable() {
        return orderGisCable;
    }

    public void setOrderGisCable(String orderGisCable) {
        this.orderGisCable = orderGisCable;
    }

    public String getOrderGisGpon() {
        return orderGisGpon;
    }

    public void setOrderGisGpon(String orderGisGpon) {
        this.orderGisGpon = orderGisGpon;
    }

    public String getOrderGisNtlt() {
        return orderGisNtlt;
    }

    public void setOrderGisNtlt(String orderGisNtlt) {
        this.orderGisNtlt = orderGisNtlt;
    }

    public String getOrderCallTipo() {
        return orderCallTipo;
    }

    public void setOrderCallTipo(String orderCallTipo) {
        this.orderCallTipo = orderCallTipo;
    }

    public String getOrderCallDesc() {
        return orderCallDesc;
    }

    public void setOrderCallDesc(String orderCallDesc) {
        this.orderCallDesc = orderCallDesc;
    }

    public String getOrderModelo() {
        return orderModelo;
    }

    public void setOrderModelo(String orderModelo) {
        this.orderModelo = orderModelo;
    }

    public String getOrderOrigen() {
        return orderOrigen;
    }

    public void setOrderOrigen(String orderOrigen) {
        this.orderOrigen = orderOrigen;
    }

    public String getAffiliationElectronicInvoice() {
        return affiliationElectronicInvoice;
    }

    public void setAffiliationElectronicInvoice(String affiliationElectronicInvoice) {
        this.affiliationElectronicInvoice = affiliationElectronicInvoice;
    }

    public String getAffiliationDataProtection() {
        return affiliationDataProtection;
    }

    public void setAffiliationDataProtection(String affiliationDataProtection) {
        this.affiliationDataProtection = affiliationDataProtection;
    }

    public String getAffiliationParentalProtection() {
        return affiliationParentalProtection;
    }

    public void setAffiliationParentalProtection(String affiliationParentalProtection) {
        this.affiliationParentalProtection = affiliationParentalProtection;
    }

    public String getDisaffiliationWhitePagesGuide() {
        return disaffiliationWhitePagesGuide;
    }

    public void setDisaffiliationWhitePagesGuide(String disaffiliationWhitePagesGuide) {
        this.disaffiliationWhitePagesGuide = disaffiliationWhitePagesGuide;
    }

    public String getOrderContrato() {
        return orderContrato;
    }

    public void setOrderContrato(String orderContrato) {
        this.orderContrato = orderContrato;
    }

    public String getOrderGrabacion() {
        return orderGrabacion;
    }

    public void setOrderGrabacion(String orderGrabacion) {
        this.orderGrabacion = orderGrabacion;
    }

    public String getOrderModalidadAcept() {
        return orderModalidadAcept;
    }

    public void setOrderModalidadAcept(String orderModalidadAcept) {
        this.orderModalidadAcept = orderModalidadAcept;
    }

    public String getOrderCip() {
        return orderCip;
    }

    public void setOrderCip(String orderCip) {
        this.orderCip = orderCip;
    }

    public String getOrderStc6i() {
        return orderStc6i;
    }

    public void setOrderStc6i(String orderStc6i) {
        this.orderStc6i = orderStc6i;
    }

    public String getUserAtis() {
        return userAtis;
    }

    public void setUserAtis(String userAtis) {
        this.userAtis = userAtis;
    }

    public String getUserFuerzaVenta() {
        return userFuerzaVenta;
    }

    public void setUserFuerzaVenta(String userFuerzaVenta) {
        this.userFuerzaVenta = userFuerzaVenta;
    }

    public String getUserCanalCodigo() {
        return userCanalCodigo;
    }

    public void setUserCanalCodigo(String userCanalCodigo) {
        this.userCanalCodigo = userCanalCodigo;
    }

    public Integer getUserCmsPuntoVenta() {
        return userCmsPuntoVenta;
    }

    public void setUserCmsPuntoVenta(Integer userCmsPuntoVenta) {
        this.userCmsPuntoVenta = userCmsPuntoVenta;
    }

    public Integer getUserCms() {
        return userCms;
    }

    public void setUserCms(Integer userCms) {
        this.userCms = userCms;
    }

    public String getUserNombre() {
        return userNombre;
    }

    public void setUserNombre(String userNombre) {
        this.userNombre = userNombre;
    }

    public String getUserDni() {
        return userDni;
    }

    public void setUserDni(String userDni) {
        this.userDni = userDni;
    }

    public String getUserTelefono() {
        return userTelefono;
    }

    public void setUserTelefono(String userTelefono) {
        this.userTelefono = userTelefono;
    }

    public String getUserEntidad() {
        return userEntidad;
    }

    public void setUserEntidad(String userEntidad) {
        this.userEntidad = userEntidad;
    }

    public String getUserRegion() {
        return userRegion;
    }

    public void setUserRegion(String userRegion) {
        this.userRegion = userRegion;
    }

    public String getUserZonal() {
        return userZonal;
    }

    public void setUserZonal(String userZonal) {
        this.userZonal = userZonal;
    }

    public String getClientTipoDoc() {
        return clientTipoDoc;
    }

    public void setClientTipoDoc(String clientTipoDoc) {
        this.clientTipoDoc = clientTipoDoc;
    }

    public String getClientNumeroDoc() {
        return clientNumeroDoc;
    }

    public void setClientNumeroDoc(String clientNumeroDoc) {
        this.clientNumeroDoc = clientNumeroDoc;
    }

    public String getClientRucDigitCtrl() {
        return clientRucDigitCtrl;
    }

    public void setClientRucDigitCtrl(String clientRucDigitCtrl) {
        this.clientRucDigitCtrl = clientRucDigitCtrl;
    }

    public Integer getClientRucActividad() {
        return clientRucActividad;
    }

    public void setClientRucActividad(Integer clientRucActividad) {
        this.clientRucActividad = clientRucActividad;
    }

    public Integer getClientRucSubactividad() {
        return clientRucSubactividad;
    }

    public void setClientRucSubactividad(Integer clientRucSubactividad) {
        this.clientRucSubactividad = clientRucSubactividad;
    }

    public String getClientNombre() {
        return clientNombre;
    }

    public void setClientNombre(String clientNombre) {
        this.clientNombre = clientNombre;
    }

    public String getClientApellidoPaterno() {
        return clientApellidoPaterno;
    }

    public void setClientApellidoPaterno(String clientApellidoPaterno) {
        this.clientApellidoPaterno = clientApellidoPaterno;
    }

    public String getClientApellidoMaterno() {
        return clientApellidoMaterno;
    }

    public void setClientApellidoMaterno(String clientApellidoMaterno) {
        this.clientApellidoMaterno = clientApellidoMaterno;
    }

    public Date getClientNacimiento() {
        return clientNacimiento;
    }

    public void setClientNacimiento(Date clientNacimiento) {
        this.clientNacimiento = clientNacimiento;
    }

    public String getClientSexo() {
        return clientSexo;
    }

    public void setClientSexo(String clientSexo) {
        this.clientSexo = clientSexo;
    }

    public String getClientCivil() {
        return clientCivil;
    }

    public void setClientCivil(String clientCivil) {
        this.clientCivil = clientCivil;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientNationality() {
        return clientNationality;
    }

    public void setClientNationality(String clientNationality) {
        this.clientNationality = clientNationality;
    }

    public String getClientTelefono1Area() {
        return clientTelefono1Area;
    }

    public void setClientTelefono1Area(String clientTelefono1Area) {
        this.clientTelefono1Area = clientTelefono1Area;
    }

    public String getClientTelefono1() {
        return clientTelefono1;
    }

    public void setClientTelefono1(String clientTelefono1) {
        this.clientTelefono1 = clientTelefono1;
    }

    public String getClientTelefono2Area() {
        return clientTelefono2Area;
    }

    public void setClientTelefono2Area(String clientTelefono2Area) {
        this.clientTelefono2Area = clientTelefono2Area;
    }

    public String getClientTelefono2() {
        return clientTelefono2;
    }

    public void setClientTelefono2(String clientTelefono2) {
        this.clientTelefono2 = clientTelefono2;
    }

    public Integer getClientCmsCodigo() {
        return clientCmsCodigo;
    }

    public void setClientCmsCodigo(Integer clientCmsCodigo) {
        this.clientCmsCodigo = clientCmsCodigo;
    }

    public String getClientCmsServicio() {
        return clientCmsServicio;
    }

    public void setClientCmsServicio(String clientCmsServicio) {
        this.clientCmsServicio = clientCmsServicio;
    }

    public Integer getClientCmsFactura() {
        return clientCmsFactura;
    }

    public void setClientCmsFactura(Integer clientCmsFactura) {
        this.clientCmsFactura = clientCmsFactura;
    }

    public String getProductCodigo() {
        return productCodigo;
    }

    public void setProductCodigo(String productCodigo) {
        this.productCodigo = productCodigo;
    }

    public String getProductPs() {
        return productPs;
    }

    public void setProductPs(String productPs) {
        this.productPs = productPs;
    }

    public BigDecimal getProductPrecioNormal() {
        return productPrecioNormal;
    }

    public void setProductPrecioNormal(BigDecimal productPrecioNormal) {
        this.productPrecioNormal = productPrecioNormal;
    }

    public BigDecimal getProductPrecioPromo() {
        return productPrecioPromo;
    }

    public void setProductPrecioPromo(BigDecimal productPrecioPromo) {
        this.productPrecioPromo = productPrecioPromo;
    }

    public Integer getProductPrecioPromoMes() {
        return productPrecioPromoMes;
    }

    public void setProductPrecioPromoMes(Integer productPrecioPromoMes) {
        this.productPrecioPromoMes = productPrecioPromoMes;
    }

    public Integer getProductInternetVel() {
        return productInternetVel;
    }

    public void setProductInternetVel(Integer productInternetVel) {
        this.productInternetVel = productInternetVel;
    }

    public Integer getProductInternetPromo() {
        return productInternetPromo;
    }

    public void setProductInternetPromo(Integer productInternetPromo) {
        this.productInternetPromo = productInternetPromo;
    }

    public Integer getProductInternetPromoTiempo() {
        return productInternetPromoTiempo;
    }

    public void setProductInternetPromoTiempo(Integer productInternetPromoTiempo) {
        this.productInternetPromoTiempo = productInternetPromoTiempo;
    }

    public String getProductCampaing() {
        return productCampaing;
    }

    public void setProductCampaing(String productCampaing) {
        this.productCampaing = productCampaing;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductPayMethod() {
        return productPayMethod;
    }

    public void setProductPayMethod(String productPayMethod) {
        this.productPayMethod = productPayMethod;
    }

    public BigDecimal getProductPayPrice() {
        return productPayPrice;
    }

    public void setProductPayPrice(BigDecimal productPayPrice) {
        this.productPayPrice = productPayPrice;
    }

    public Integer getProductPayReturnMonth() {
        return productPayReturnMonth;
    }

    public void setProductPayReturnMonth(Integer productPayReturnMonth) {
        this.productPayReturnMonth = productPayReturnMonth;
    }

    public String getProductPayReturnPeriod() {
        return productPayReturnPeriod;
    }

    public void setProductPayReturnPeriod(String productPayReturnPeriod) {
        this.productPayReturnPeriod = productPayReturnPeriod;
    }

    public BigDecimal getProductCostInstall() {
        return productCostInstall;
    }

    public void setProductCostInstall(BigDecimal productCostInstall) {
        this.productCostInstall = productCostInstall;
    }

    public Integer getProductCostInstallMonth() {
        return productCostInstallMonth;
    }

    public void setProductCostInstallMonth(Integer productCostInstallMonth) {
        this.productCostInstallMonth = productCostInstallMonth;
    }

    public String getProductTecInter() {
        return productTecInter;
    }

    public void setProductTecInter(String productTecInter) {
        this.productTecInter = productTecInter;
    }

    public String getProductTecTv() {
        return productTecTv;
    }

    public void setProductTecTv(String productTecTv) {
        this.productTecTv = productTecTv;
    }

    public String getProductTvSenal() {
        return productTvSenal;
    }

    public void setProductTvSenal(String productTvSenal) {
        this.productTvSenal = productTvSenal;
    }

    public String getProductEquipTv() {
        return productEquipTv;
    }

    public void setProductEquipTv(String productEquipTv) {
        this.productEquipTv = productEquipTv;
    }

    public String getProductEquipInter() {
        return productEquipInter;
    }

    public void setProductEquipInter(String productEquipInter) {
        this.productEquipInter = productEquipInter;
    }

    public String getProductEquipLine() {
        return productEquipLine;
    }

    public void setProductEquipLine(String productEquipLine) {
        this.productEquipLine = productEquipLine;
    }

    public String getProductTipoReg() {
        return productTipoReg;
    }

    public void setProductTipoReg(String productTipoReg) {
        this.productTipoReg = productTipoReg;
    }

    public String getProductSenal() {
        return productSenal;
    }

    public void setProductSenal(String productSenal) {
        this.productSenal = productSenal;
    }

    public String getProductCabecera() {
        return productCabecera;
    }

    public void setProductCabecera(String productCabecera) {
        this.productCabecera = productCabecera;
    }

    public Integer getProductPsAdmin1() {
        return productPsAdmin1;
    }

    public void setProductPsAdmin1(Integer productPsAdmin1) {
        this.productPsAdmin1 = productPsAdmin1;
    }

    public Integer getProductPsAdmin2() {
        return productPsAdmin2;
    }

    public void setProductPsAdmin2(Integer productPsAdmin2) {
        this.productPsAdmin2 = productPsAdmin2;
    }

    public Integer getProductPsAdmin3() {
        return productPsAdmin3;
    }

    public void setProductPsAdmin3(Integer productPsAdmin3) {
        this.productPsAdmin3 = productPsAdmin3;
    }

    public Integer getProductPsAdmin4() {
        return productPsAdmin4;
    }

    public void setProductPsAdmin4(Integer productPsAdmin4) {
        this.productPsAdmin4 = productPsAdmin4;
    }

    public String getSvaCode1() {
        return svaCode1;
    }

    public void setSvaCode1(String svaCode1) {
        this.svaCode1 = svaCode1;
    }

    public String getSvaCodigo1() {
        return svaCodigo1;
    }

    public void setSvaCodigo1(String svaCodigo1) {
        this.svaCodigo1 = svaCodigo1;
    }

    public String getSvaNombre1() {
        return svaNombre1;
    }

    public void setSvaNombre1(String svaNombre1) {
        this.svaNombre1 = svaNombre1;
    }

    public BigDecimal getSvaPrecio1() {
        return svaPrecio1;
    }

    public void setSvaPrecio1(BigDecimal svaPrecio1) {
        this.svaPrecio1 = svaPrecio1;
    }

    public Integer getSvaCantidad1() {
        return svaCantidad1;
    }

    public void setSvaCantidad1(Integer svaCantidad1) {
        this.svaCantidad1 = svaCantidad1;
    }

    public String getSvaCode2() {
        return svaCode2;
    }

    public void setSvaCode2(String svaCode2) {
        this.svaCode2 = svaCode2;
    }

    public String getSvaCodigo2() {
        return svaCodigo2;
    }

    public void setSvaCodigo2(String svaCodigo2) {
        this.svaCodigo2 = svaCodigo2;
    }

    public String getSvaNombre2() {
        return svaNombre2;
    }

    public void setSvaNombre2(String svaNombre2) {
        this.svaNombre2 = svaNombre2;
    }

    public BigDecimal getSvaPrecio2() {
        return svaPrecio2;
    }

    public void setSvaPrecio2(BigDecimal svaPrecio2) {
        this.svaPrecio2 = svaPrecio2;
    }

    public Integer getSvaCantidad2() {
        return svaCantidad2;
    }

    public void setSvaCantidad2(Integer svaCantidad2) {
        this.svaCantidad2 = svaCantidad2;
    }

    public String getSvaCode3() {
        return svaCode3;
    }

    public void setSvaCode3(String svaCode3) {
        this.svaCode3 = svaCode3;
    }

    public String getSvaCodigo3() {
        return svaCodigo3;
    }

    public void setSvaCodigo3(String svaCodigo3) {
        this.svaCodigo3 = svaCodigo3;
    }

    public String getSvaNombre3() {
        return svaNombre3;
    }

    public void setSvaNombre3(String svaNombre3) {
        this.svaNombre3 = svaNombre3;
    }

    public BigDecimal getSvaPrecio3() {
        return svaPrecio3;
    }

    public void setSvaPrecio3(BigDecimal svaPrecio3) {
        this.svaPrecio3 = svaPrecio3;
    }

    public Integer getSvaCantidad3() {
        return svaCantidad3;
    }

    public void setSvaCantidad3(Integer svaCantidad3) {
        this.svaCantidad3 = svaCantidad3;
    }

    public String getSvaCode4() {
        return svaCode4;
    }

    public void setSvaCode4(String svaCode4) {
        this.svaCode4 = svaCode4;
    }

    public String getSvaCodigo4() {
        return svaCodigo4;
    }

    public void setSvaCodigo4(String svaCodigo4) {
        this.svaCodigo4 = svaCodigo4;
    }

    public String getSvaNombre4() {
        return svaNombre4;
    }

    public void setSvaNombre4(String svaNombre4) {
        this.svaNombre4 = svaNombre4;
    }

    public BigDecimal getSvaPrecio4() {
        return svaPrecio4;
    }

    public void setSvaPrecio4(BigDecimal svaPrecio4) {
        this.svaPrecio4 = svaPrecio4;
    }

    public Integer getSvaCantidad4() {
        return svaCantidad4;
    }

    public void setSvaCantidad4(Integer svaCantidad4) {
        this.svaCantidad4 = svaCantidad4;
    }

    public String getSvaCode5() {
        return svaCode5;
    }

    public void setSvaCode5(String svaCode5) {
        this.svaCode5 = svaCode5;
    }

    public String getSvaCodigo5() {
        return svaCodigo5;
    }

    public void setSvaCodigo5(String svaCodigo5) {
        this.svaCodigo5 = svaCodigo5;
    }

    public String getSvaNombre5() {
        return svaNombre5;
    }

    public void setSvaNombre5(String svaNombre5) {
        this.svaNombre5 = svaNombre5;
    }

    public BigDecimal getSvaPrecio5() {
        return svaPrecio5;
    }

    public void setSvaPrecio5(BigDecimal svaPrecio5) {
        this.svaPrecio5 = svaPrecio5;
    }

    public Integer getSvaCantidad5() {
        return svaCantidad5;
    }

    public void setSvaCantidad5(Integer svaCantidad5) {
        this.svaCantidad5 = svaCantidad5;
    }

    public String getSvaCode6() {
        return svaCode6;
    }

    public void setSvaCode6(String svaCode6) {
        this.svaCode6 = svaCode6;
    }

    public String getSvaCodigo6() {
        return svaCodigo6;
    }

    public void setSvaCodigo6(String svaCodigo6) {
        this.svaCodigo6 = svaCodigo6;
    }

    public String getSvaNombre6() {
        return svaNombre6;
    }

    public void setSvaNombre6(String svaNombre6) {
        this.svaNombre6 = svaNombre6;
    }

    public BigDecimal getSvaPrecio6() {
        return svaPrecio6;
    }

    public void setSvaPrecio6(BigDecimal svaPrecio6) {
        this.svaPrecio6 = svaPrecio6;
    }

    public Integer getSvaCantidad6() {
        return svaCantidad6;
    }

    public void setSvaCantidad6(Integer svaCantidad6) {
        this.svaCantidad6 = svaCantidad6;
    }

    public String getSvaCode7() {
        return svaCode7;
    }

    public void setSvaCode7(String svaCode7) {
        this.svaCode7 = svaCode7;
    }

    public String getSvaCodigo7() {
        return svaCodigo7;
    }

    public void setSvaCodigo7(String svaCodigo7) {
        this.svaCodigo7 = svaCodigo7;
    }

    public String getSvaNombre7() {
        return svaNombre7;
    }

    public void setSvaNombre7(String svaNombre7) {
        this.svaNombre7 = svaNombre7;
    }

    public BigDecimal getSvaPrecio7() {
        return svaPrecio7;
    }

    public void setSvaPrecio7(BigDecimal svaPrecio7) {
        this.svaPrecio7 = svaPrecio7;
    }

    public Integer getSvaCantidad7() {
        return svaCantidad7;
    }

    public void setSvaCantidad7(Integer svaCantidad7) {
        this.svaCantidad7 = svaCantidad7;
    }

    public String getSvaCode8() {
        return svaCode8;
    }

    public void setSvaCode8(String svaCode8) {
        this.svaCode8 = svaCode8;
    }

    public String getSvaCodigo8() {
        return svaCodigo8;
    }

    public void setSvaCodigo8(String svaCodigo8) {
        this.svaCodigo8 = svaCodigo8;
    }

    public String getSvaNombre8() {
        return svaNombre8;
    }

    public void setSvaNombre8(String svaNombre8) {
        this.svaNombre8 = svaNombre8;
    }

    public BigDecimal getSvaPrecio8() {
        return svaPrecio8;
    }

    public void setSvaPrecio8(BigDecimal svaPrecio8) {
        this.svaPrecio8 = svaPrecio8;
    }

    public Integer getSvaCantidad8() {
        return svaCantidad8;
    }

    public void setSvaCantidad8(Integer svaCantidad8) {
        this.svaCantidad8 = svaCantidad8;
    }

    public String getSvaCode9() {
        return svaCode9;
    }

    public void setSvaCode9(String svaCode9) {
        this.svaCode9 = svaCode9;
    }

    public String getSvaCodigo9() {
        return svaCodigo9;
    }

    public void setSvaCodigo9(String svaCodigo9) {
        this.svaCodigo9 = svaCodigo9;
    }

    public String getSvaNombre9() {
        return svaNombre9;
    }

    public void setSvaNombre9(String svaNombre9) {
        this.svaNombre9 = svaNombre9;
    }

    public BigDecimal getSvaPrecio9() {
        return svaPrecio9;
    }

    public void setSvaPrecio9(BigDecimal svaPrecio9) {
        this.svaPrecio9 = svaPrecio9;
    }

    public Integer getSvaCantidad9() {
        return svaCantidad9;
    }

    public void setSvaCantidad9(Integer svaCantidad9) {
        this.svaCantidad9 = svaCantidad9;
    }

    public String getSvaCode10() {
        return svaCode10;
    }

    public void setSvaCode10(String svaCode10) {
        this.svaCode10 = svaCode10;
    }

    public String getSvaCodigo10() {
        return svaCodigo10;
    }

    public void setSvaCodigo10(String svaCodigo10) {
        this.svaCodigo10 = svaCodigo10;
    }

    public String getSvaNombre10() {
        return svaNombre10;
    }

    public void setSvaNombre10(String svaNombre10) {
        this.svaNombre10 = svaNombre10;
    }

    public BigDecimal getSvaPrecio10() {
        return svaPrecio10;
    }

    public void setSvaPrecio10(BigDecimal svaPrecio10) {
        this.svaPrecio10 = svaPrecio10;
    }

    public Integer getSvaCantidad10() {
        return svaCantidad10;
    }

    public void setSvaCantidad10(Integer svaCantidad10) {
        this.svaCantidad10 = svaCantidad10;
    }

    public String getAddressDepartamentoCod() {
        return addressDepartamentoCod;
    }

    public void setAddressDepartamentoCod(String addressDepartamentoCod) {
        this.addressDepartamentoCod = addressDepartamentoCod;
    }

    public String getAddressDepartamento() {
        return addressDepartamento;
    }

    public void setAddressDepartamento(String addressDepartamento) {
        this.addressDepartamento = addressDepartamento;
    }

    public String getAddressProvinciaCod() {
        return addressProvinciaCod;
    }

    public void setAddressProvinciaCod(String addressProvinciaCod) {
        this.addressProvinciaCod = addressProvinciaCod;
    }

    public String getAddressProvincia() {
        return addressProvincia;
    }

    public void setAddressProvincia(String addressProvincia) {
        this.addressProvincia = addressProvincia;
    }

    public String getAddressDistritoCod() {
        return addressDistritoCod;
    }

    public void setAddressDistritoCod(String addressDistritoCod) {
        this.addressDistritoCod = addressDistritoCod;
    }

    public String getAddressDistrito() {
        return addressDistrito;
    }

    public void setAddressDistrito(String addressDistrito) {
        this.addressDistrito = addressDistrito;
    }

    public String getAddressPrincipal() {
        return addressPrincipal;
    }

    public void setAddressPrincipal(String addressPrincipal) {
        this.addressPrincipal = addressPrincipal;
    }

    public String getAddressCalleAtis() {
        return addressCalleAtis;
    }

    public void setAddressCalleAtis(String addressCalleAtis) {
        this.addressCalleAtis = addressCalleAtis;
    }

    public String getAddressCalleNombre() {
        return addressCalleNombre;
    }

    public void setAddressCalleNombre(String addressCalleNombre) {
        this.addressCalleNombre = addressCalleNombre;
    }

    public String getAddressCalleNumero() {
        return addressCalleNumero;
    }

    public void setAddressCalleNumero(String addressCalleNumero) {
        this.addressCalleNumero = addressCalleNumero;
    }

    public Integer getAddressCchhCodigo() {
        return addressCchhCodigo;
    }

    public void setAddressCchhCodigo(Integer addressCchhCodigo) {
        this.addressCchhCodigo = addressCchhCodigo;
    }

    public String getAddressCchhTipo() {
        return addressCchhTipo;
    }

    public void setAddressCchhTipo(String addressCchhTipo) {
        this.addressCchhTipo = addressCchhTipo;
    }

    public String getAddressCchhNombre() {
        return addressCchhNombre;
    }

    public void setAddressCchhNombre(String addressCchhNombre) {
        this.addressCchhNombre = addressCchhNombre;
    }

    public String getAddressCchhCompTip1() {
        return addressCchhCompTip1;
    }

    public void setAddressCchhCompTip1(String addressCchhCompTip1) {
        this.addressCchhCompTip1 = addressCchhCompTip1;
    }

    public String getAddressCchhCompNom1() {
        return addressCchhCompNom1;
    }

    public void setAddressCchhCompNom1(String addressCchhCompNom1) {
        this.addressCchhCompNom1 = addressCchhCompNom1;
    }

    public String getAddressCchhCompTip2() {
        return addressCchhCompTip2;
    }

    public void setAddressCchhCompTip2(String addressCchhCompTip2) {
        this.addressCchhCompTip2 = addressCchhCompTip2;
    }

    public String getAddressCchhCompNom2() {
        return addressCchhCompNom2;
    }

    public void setAddressCchhCompNom2(String addressCchhCompNom2) {
        this.addressCchhCompNom2 = addressCchhCompNom2;
    }

    public String getAddressCchhCompTip3() {
        return addressCchhCompTip3;
    }

    public void setAddressCchhCompTip3(String addressCchhCompTip3) {
        this.addressCchhCompTip3 = addressCchhCompTip3;
    }

    public String getAddressCchhCompNom3() {
        return addressCchhCompNom3;
    }

    public void setAddressCchhCompNom3(String addressCchhCompNom3) {
        this.addressCchhCompNom3 = addressCchhCompNom3;
    }

    public String getAddressCchhCompTip4() {
        return addressCchhCompTip4;
    }

    public void setAddressCchhCompTip4(String addressCchhCompTip4) {
        this.addressCchhCompTip4 = addressCchhCompTip4;
    }

    public String getAddressCchhCompNom4() {
        return addressCchhCompNom4;
    }

    public void setAddressCchhCompNom4(String addressCchhCompNom4) {
        this.addressCchhCompNom4 = addressCchhCompNom4;
    }

    public String getAddressCchhCompTip5() {
        return addressCchhCompTip5;
    }

    public void setAddressCchhCompTip5(String addressCchhCompTip5) {
        this.addressCchhCompTip5 = addressCchhCompTip5;
    }

    public String getAddressCchhCompNom5() {
        return addressCchhCompNom5;
    }

    public void setAddressCchhCompNom5(String addressCchhCompNom5) {
        this.addressCchhCompNom5 = addressCchhCompNom5;
    }

    public String getAddressViaCompTip1() {
        return addressViaCompTip1;
    }

    public void setAddressViaCompTip1(String addressViaCompTip1) {
        this.addressViaCompTip1 = addressViaCompTip1;
    }

    public String getAddressViaCompNom1() {
        return addressViaCompNom1;
    }

    public void setAddressViaCompNom1(String addressViaCompNom1) {
        this.addressViaCompNom1 = addressViaCompNom1;
    }

    public String getAddressViaCompTip2() {
        return addressViaCompTip2;
    }

    public void setAddressViaCompTip2(String addressViaCompTip2) {
        this.addressViaCompTip2 = addressViaCompTip2;
    }

    public String getAddressViaCompNom2() {
        return addressViaCompNom2;
    }

    public void setAddressViaCompNom2(String addressViaCompNom2) {
        this.addressViaCompNom2 = addressViaCompNom2;
    }

    public String getAddressViaCompTip3() {
        return addressViaCompTip3;
    }

    public void setAddressViaCompTip3(String addressViaCompTip3) {
        this.addressViaCompTip3 = addressViaCompTip3;
    }

    public String getAddressViaCompNom3() {
        return addressViaCompNom3;
    }

    public void setAddressViaCompNom3(String addressViaCompNom3) {
        this.addressViaCompNom3 = addressViaCompNom3;
    }

    public String getAddressViaCompTip4() {
        return addressViaCompTip4;
    }

    public void setAddressViaCompTip4(String addressViaCompTip4) {
        this.addressViaCompTip4 = addressViaCompTip4;
    }

    public String getAddressViaCompNom4() {
        return addressViaCompNom4;
    }

    public void setAddressViaCompNom4(String addressViaCompNom4) {
        this.addressViaCompNom4 = addressViaCompNom4;
    }

    public String getAddressViaCompTip5() {
        return addressViaCompTip5;
    }

    public void setAddressViaCompTip5(String addressViaCompTip5) {
        this.addressViaCompTip5 = addressViaCompTip5;
    }

    public String getAddressViaCompNom5() {
        return addressViaCompNom5;
    }

    public void setAddressViaCompNom5(String addressViaCompNom5) {
        this.addressViaCompNom5 = addressViaCompNom5;
    }

    public String getAddressReferencia() {
        return addressReferencia;
    }

    public void setAddressReferencia(String addressReferencia) {
        this.addressReferencia = addressReferencia;
    }

    public Date getAuditoriaCreate() {
        return auditoriaCreate;
    }

    public void setAuditoriaCreate(Date auditoriaCreate) {
        this.auditoriaCreate = auditoriaCreate;
    }

    public Date getAuditoriaModify() {
        return auditoriaModify;
    }

    public void setAuditoriaModify(Date auditoriaModify) {
        this.auditoriaModify = auditoriaModify;
    }

    public Integer getAutomatizadorOK() {
        return automatizadorOK;
    }

    public void setAutomatizadorOK(Integer automatizadorOK) {
        this.automatizadorOK = automatizadorOK;
    }

    public Date getFechaPagoCmsAtis() {
        return fechaPagoCmsAtis;
    }

    public void setFechaPagoCmsAtis(Date fechaPagoCmsAtis) {
        this.fechaPagoCmsAtis = fechaPagoCmsAtis;
    }

    public TdpOrder updateAddress(ConfigAddressRequest request) {
        Date fechaActual=new Date();
        this.setAddressCalleAtis(request.getAddress_calle_atis());
        this.setAddressPrincipal(request.getAddress_principal());
        this.setAddressCalleAtis(request.getAddress_calle_atis());
        this.setAddressCalleNombre(request.getAddress_calle_nombre());
        this.setAddressCalleNumero(request.getAddress_calle_numero());
        this.setAddressCchhTipo(request.getAddress_cchh_tipo());
        this.setAddressCchhNombre(request.getAddress_cchh_nombre());
        this.setAddressCchhCompTip1(request.getAddress_cchh_comp_tip_1());
        this.setAddressCchhCompNom1(request.getAddress_cchh_comp_nom_1());
        this.setAddressCchhCompTip2(request.getAddress_cchh_comp_tip_2());
        this.setAddressCchhCompNom2(request.getAddress_cchh_comp_nom_2());
        this.setAddressCchhCompTip3(request.getAddress_cchh_comp_tip_3());
        this.setAddressCchhCompNom3(request.getAddress_cchh_comp_nom_3());
        this.setAddressCchhCompTip4(request.getAddress_cchh_comp_tip_4());
        this.setAddressCchhCompNom4(request.getAddress_cchh_comp_nom_4());
        this.setAddressCchhCompTip5(request.getAddress_cchh_comp_tip_5());
        this.setAddressCchhCompNom5(request.getAddress_cchh_comp_nom_5());
        this.setAddressViaCompTip1(request.getAddress_via_comp_tip_1());
        this.setAddressViaCompNom1(request.getAddress_via_comp_nom_1());
        this.setAddressViaCompTip2(request.getAddress_via_comp_tip_2());
        this.setAddressViaCompNom2(request.getAddress_via_comp_nom_2());
        this.setAddressViaCompTip3(request.getAddress_via_comp_tip_3());
        this.setAddressViaCompNom3(request.getAddress_via_comp_nom_3());
        this.setAddressViaCompTip4(request.getAddress_via_comp_tip_4());
        this.setAddressViaCompNom4(request.getAddress_via_comp_nom_4());
        this.setAddressViaCompTip5(request.getAddress_via_comp_tip_5());
        this.setAddressViaCompNom5(request.getAddress_via_comp_nom_5());
        this.setAddressReferencia(request.getAddress_referencia());
        this.setAuditoriaModify(fechaActual);
        return this;
    }
}
