package pe.com.tdp.ventafija.microservices.domain.dto;

public class PagoEfectivoNotificacionResponse {
    private String codigo;
    private String respuesta;

    public PagoEfectivoNotificacionResponse() {
        super();
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}

