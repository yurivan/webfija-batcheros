package pe.com.tdp.ventafija.microservices.domain.outcall;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AudioRequest {
    @JsonProperty("order_id")
    private String order_id;

    @JsonProperty("user_id")
    private String user_id;

    @JsonProperty("name")
    private String name;

    @JsonProperty("canal_atis")
    private String canal_atis;

    @JsonProperty("extension")
    private String extension;

    @JsonProperty("address_direccion")
    private String address_direccion;

    @JsonProperty("address_referencia")
    private String address_referencia;

    @JsonProperty("motivo_estado")
    private String motivo_estado;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCanal_atis() {
        return canal_atis;
    }

    public void setCanal_atis(String canal_atis) {
        this.canal_atis = canal_atis;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getAddress_direccion() {
        return address_direccion;
    }

    public void setAddress_direccion(String address_direccion) {
        this.address_direccion = address_direccion;
    }

    public String getAddress_referencia() {
        return address_referencia;
    }

    public void setAddress_referencia(String address_referencia) {
        this.address_referencia = address_referencia;
    }
}
