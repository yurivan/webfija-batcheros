package pe.com.tdp.ventafija.microservices.domain.user;

public class LoginResponseData {

    private String name;
    private String lastName;
    private String document;
    private String entity;
    private String phone;
    private String resetPwd;
    /*-----Campos de perfil----*/
    private String channel;
    private String groupPermission;
    private String group;

    // Propiedades necesarias para campanias
    private String sellerSegment;
    private String sellerChannelAtis;
    private String campaigns;

    // Sprint 12 - datos canal y entidad equivalentes para campanias
    private String sellerChannelEquivalentCampaign;
    private String sellerEntityEquivalentCampaign;

    private String niveles;

    //Sprint 27 - dato para separar los flujos - dochoaro
    private String typeFlujoTipificacion;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getResetPwd() {
        return resetPwd;
    }

    public void setResetPwd(String resetPwd) {
        this.resetPwd = resetPwd;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getGroupPermission() {
        return groupPermission;
    }

    public void setGroupPermission(String groupPermission) {
        this.groupPermission = groupPermission;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getSellerSegment() {
        return sellerSegment;
    }

    public void setSellerSegment(String sellerSegment) {
        this.sellerSegment = sellerSegment;
    }

    public String getSellerChannelAtis() {
        return sellerChannelAtis;
    }

    public void setSellerChannelAtis(String sellerChannelAtis) {
        this.sellerChannelAtis = sellerChannelAtis;
    }

    public String getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(String campaigns) {
        this.campaigns = campaigns;
    }

    public String getSellerChannelEquivalentCampaign() {
        return sellerChannelEquivalentCampaign;
    }

    public void setSellerChannelEquivalentCampaign(String sellerChannelEquivalentCampaign) {
        this.sellerChannelEquivalentCampaign = sellerChannelEquivalentCampaign;
    }

    public String getSellerEntityEquivalentCampaign() {
        return sellerEntityEquivalentCampaign;
    }

    public void setSellerEntityEquivalentCampaign(String sellerEntityEquivalentCampaign) {
        this.sellerEntityEquivalentCampaign = sellerEntityEquivalentCampaign;
    }

    public String getNiveles() {
        return niveles;
    }

    public void setNiveles(String niveles) {
        this.niveles = niveles;
    }

    public String getTypeFlujoTipificacion() { return typeFlujoTipificacion;
    }

    public void setTypeFlujoTipificacion(String typeFlujoTipificacion) { this.typeFlujoTipificacion = typeFlujoTipificacion; }
}
