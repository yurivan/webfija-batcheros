package pe.com.tdp.ventafija.microservices.domain.order;

public class OrderVisor {
    private String id;
    private String name;
    private String document;
    private String codigoPedido;
    private String producto;
    private String tecnologia;
    private String estado;
    private String idtransaccion;
    private String codigoAtis;
    private String anio;
    private String mes;
    private String dia;
    private String hora;
    private String estadocolor;
    private String tipoCaida;
    private String motivoCaida;
    private String telefonoAsignado;
    private String telefonoCliente;
    private String telefonoCliente2;
    private String pagoAdelantado;
    private String direccion;
    private String fVenta;
    private String precio;
    private String preciopromocional;
    private String operacioncomercial;
    private String tipolinea;
    private String descuento;
    private String periodopromocional;
    private String facturaelectronica;
    private String paginasblancas;

    private int decosSmart;
    private int decosHD;
    private int decosDVR;
    private String codigocip;
    private String estadocip;
    private String debitoAuto;
    private String tratamientDatos;
    private String webParental;
    private String reciboElect;
    private String offline;
    private String ordenTrabajo;
    private String svaNombre1;
    private String svaCantidad1;
    private String svaNombre2;
    private String svaCantidad2;
    private String svaNombre3;
    private String svaCantidad3;
    private String svaNombre4;
    private String svaCantidad4;
    private String svaNombre5;
    private String svaCantidad5;
    private String svaNombre6;
    private String svaCantidad6;
    private String svaNombre7;
    private String svaCantidad7;
    private String svaNombre8;
    private String svaCantidad8;
    private String svaNombre9;
    private String svaCantidad9;
    private String svaNombre10;
    private String svaCantidad10;

    public String getOrdenTrabajo() {
        return ordenTrabajo;
    }

    public void setOrdenTrabajo(String ordenTrabajo) {
        this.ordenTrabajo = ordenTrabajo;
    }

   public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getTecnologia() {
        return tecnologia;
    }

    public void setTecnologia(String tecnologia) {
        this.tecnologia = tecnologia;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdtransaccion() {
        return idtransaccion;
    }

    public void setIdtransaccion(String idtransaccion) {
        this.idtransaccion = idtransaccion;
    }

    public String getCodigoAtis() {
        return codigoAtis;
    }

    public void setCodigoAtis(String codigoAtis) {
        this.codigoAtis = codigoAtis;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getEstadocolor() {
        return estadocolor;
    }

    public void setEstadocolor(String estadocolor) {
        this.estadocolor = estadocolor;
    }

    public String getTipoCaida() {
        return tipoCaida;
    }

    public void setTipoCaida(String tipoCaida) {
        this.tipoCaida = tipoCaida;
    }

    public String getMotivoCaida() {
        return motivoCaida;
    }

    public void setMotivoCaida(String motivoCaida) {
        this.motivoCaida = motivoCaida;
    }

    public String getTelefonoAsignado() {
        return telefonoAsignado;
    }

    public void setTelefonoAsignado(String telefonoAsignado) {
        this.telefonoAsignado = telefonoAsignado;
    }

    public String getTelefonoCliente() {
        return telefonoCliente;
    }

    public void setTelefonoCliente(String telefonoCliente) {
        this.telefonoCliente = telefonoCliente;
    }

    public String getPagoAdelantado() {
        return pagoAdelantado;
    }

    public void setPagoAdelantado(String pagoAdelantado) {
        this.pagoAdelantado = pagoAdelantado;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getfVenta() {
        return fVenta;
    }

    public void setfVenta(String fVenta) {
        this.fVenta = fVenta;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getPreciopromocional() {
        return preciopromocional;
    }

    public void setPreciopromocional(String preciopromocional) {
        this.preciopromocional = preciopromocional;
    }

    public String getOperacioncomercial() {
        return operacioncomercial;
    }

    public void setOperacioncomercial(String operacioncomercial) {
        this.operacioncomercial = operacioncomercial;
    }

    public String getTipolinea() {
        return tipolinea;
    }

    public void setTipolinea(String tipolinea) {
        this.tipolinea = tipolinea;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getPeriodopromocional() {
        return periodopromocional;
    }

    public void setPeriodopromocional(String periodopromocional) {
        this.periodopromocional = periodopromocional;
    }

    public String getFacturaelectronica() {
        return facturaelectronica;
    }

    public void setFacturaelectronica(String facturaelectronica) {
        this.facturaelectronica = facturaelectronica;
    }

    public String getPaginasblancas() {
        return paginasblancas;
    }

    public void setPaginasblancas(String paginasblancas) {
        this.paginasblancas = paginasblancas;
    }

    public int getDecosSmart() {
        return decosSmart;
    }

    public void setDecosSmart(int decosSmart) {
        this.decosSmart = decosSmart;
    }

    public int getDecosHD() {
        return decosHD;
    }

    public void setDecosHD(int decosHD) {
        this.decosHD = decosHD;
    }

    public int getDecosDVR() {
        return decosDVR;
    }

    public void setDecosDVR(int decosDVR) {
        this.decosDVR = decosDVR;
    }

    public String getCodigocip() {
        return codigocip;
    }

    public void setCodigocip(String codigocip) {
        this.codigocip = codigocip;
    }

    public String getEstadocip() {
        return estadocip;
    }

    public void setEstadocip(String estadocip) {
        this.estadocip = estadocip;
    }

    public String getDebitoAuto() {
        return debitoAuto;
    }

    public void setDebitoAuto(String debitoAuto) {
        this.debitoAuto = debitoAuto;
    }

    public String getTratamientDatos() {
        return tratamientDatos;
    }

    public void setTratamientDatos(String tratamientDatos) {
        this.tratamientDatos = tratamientDatos;
    }

    public String getWebParental() {
        return webParental;
    }

    public void setWebParental(String webParental) {
        this.webParental = webParental;
    }

    public String getReciboElect() {
        return reciboElect;
    }

    public void setReciboElect(String reciboElect) {
        this.reciboElect = reciboElect;
    }

    public String getOffline() {
        return offline;
    }

    public void setOffline(String offline) {
        this.offline = offline;
    }

    public String getTelefonoCliente2() {
        return telefonoCliente2;
    }

    public void setTelefonoCliente2(String telefonoCliente2) {
        this.telefonoCliente2 = telefonoCliente2;
    }

    public String getSvaNombre1() {
        return svaNombre1;
    }

    public void setSvaNombre1(String svaNombre1) {
        this.svaNombre1 = svaNombre1;
    }

    public String getSvaCantidad1() {
        return svaCantidad1;
    }

    public void setSvaCantidad1(String svaCantidad1) {
        this.svaCantidad1 = svaCantidad1;
    }

    public String getSvaNombre2() {
        return svaNombre2;
    }

    public void setSvaNombre2(String svaNombre2) {
        this.svaNombre2 = svaNombre2;
    }

    public String getSvaCantidad2() {
        return svaCantidad2;
    }

    public void setSvaCantidad2(String svaCantidad2) {
        this.svaCantidad2 = svaCantidad2;
    }

    public String getSvaNombre3() {
        return svaNombre3;
    }

    public void setSvaNombre3(String svaNombre3) {
        this.svaNombre3 = svaNombre3;
    }

    public String getSvaCantidad3() {
        return svaCantidad3;
    }

    public void setSvaCantidad3(String svaCantidad3) {
        this.svaCantidad3 = svaCantidad3;
    }

    public String getSvaNombre4() {
        return svaNombre4;
    }

    public void setSvaNombre4(String svaNombre4) {
        this.svaNombre4 = svaNombre4;
    }

    public String getSvaCantidad4() {
        return svaCantidad4;
    }

    public void setSvaCantidad4(String svaCantidad4) {
        this.svaCantidad4 = svaCantidad4;
    }

    public String getSvaNombre5() {
        return svaNombre5;
    }

    public void setSvaNombre5(String svaNombre5) {
        this.svaNombre5 = svaNombre5;
    }

    public String getSvaCantidad5() {
        return svaCantidad5;
    }

    public void setSvaCantidad5(String svaCantidad5) {
        this.svaCantidad5 = svaCantidad5;
    }

    public String getSvaNombre6() {
        return svaNombre6;
    }

    public void setSvaNombre6(String svaNombre6) {
        this.svaNombre6 = svaNombre6;
    }

    public String getSvaCantidad6() {
        return svaCantidad6;
    }

    public void setSvaCantidad6(String svaCantidad6) {
        this.svaCantidad6 = svaCantidad6;
    }

    public String getSvaNombre7() {
        return svaNombre7;
    }

    public void setSvaNombre7(String svaNombre7) {
        this.svaNombre7 = svaNombre7;
    }

    public String getSvaCantidad7() {
        return svaCantidad7;
    }

    public void setSvaCantidad7(String svaCantidad7) {
        this.svaCantidad7 = svaCantidad7;
    }

    public String getSvaNombre8() {
        return svaNombre8;
    }

    public void setSvaNombre8(String svaNombre8) {
        this.svaNombre8 = svaNombre8;
    }

    public String getSvaCantidad8() {
        return svaCantidad8;
    }

    public void setSvaCantidad8(String svaCantidad8) {
        this.svaCantidad8 = svaCantidad8;
    }

    public String getSvaNombre9() {
        return svaNombre9;
    }

    public void setSvaNombre9(String svaNombre9) {
        this.svaNombre9 = svaNombre9;
    }

    public String getSvaCantidad9() {
        return svaCantidad9;
    }

    public void setSvaCantidad9(String svaCantidad9) {
        this.svaCantidad9 = svaCantidad9;
    }

    public String getSvaNombre10() {
        return svaNombre10;
    }

    public void setSvaNombre10(String svaNombre10) {
        this.svaNombre10 = svaNombre10;
    }

    public String getSvaCantidad10() {
        return svaCantidad10;
    }

    public void setSvaCantidad10(String svaCantidad10) {
        this.svaCantidad10 = svaCantidad10;
    }
}
