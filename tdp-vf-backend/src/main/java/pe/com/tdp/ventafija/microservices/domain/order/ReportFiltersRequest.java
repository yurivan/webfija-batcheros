package pe.com.tdp.ventafija.microservices.domain.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportFiltersRequest {

    @JsonProperty("codatis")
    private String codatis;
    @JsonProperty("niveles")
    private String niveles;

    @JsonProperty("codatis")
    public String getCodatis() {
        return codatis;
    }

    @JsonProperty("codatis")
    public void setCodatis(String codatis) {
        this.codatis = codatis;
    }

    @JsonProperty("niveles")
    public String getNiveles() {
        return niveles;
    }

    @JsonProperty("niveles")
    public void setNiveles(String niveles) {
        this.niveles = niveles;
    }
}
