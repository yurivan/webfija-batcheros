package pe.com.tdp.ventafija.microservices.service.customer;

import com.google.api.client.json.Json;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.arpu.CalculadoraClient;
import pe.com.tdp.ventafija.microservices.common.clients.arpu.OffersApiCalculadoraArpuRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.arpu.OffersApiCalculadoraArpuResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoResponseBodyOperacion;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoResponseBodyOperacionDetalle;
import pe.com.tdp.ventafija.microservices.common.connection.Firebase;
import pe.com.tdp.ventafija.microservices.common.constant.CustomerConstants;
import pe.com.tdp.ventafija.microservices.common.constant.LegadosConstants;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.dao.OrderRepository;
import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.dao.TdpServiceClientRepository;
import pe.com.tdp.ventafija.microservices.common.dao.TdpServicePedidoRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Order;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.domain.entity.TdpServiceClient;
import pe.com.tdp.ventafija.microservices.common.domain.message.MessageEntities;
import pe.com.tdp.ventafija.microservices.common.services.ExpertoService;
import pe.com.tdp.ventafija.microservices.common.services.LegadoService;
import pe.com.tdp.ventafija.microservices.common.services.ParametersService;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.MessageUtil;
import pe.com.tdp.ventafija.microservices.common.util.RandomString;
import pe.com.tdp.ventafija.microservices.domain.Parameter;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.customer.*;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.CustomerScoringRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.CustomerScoringResponse;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.OperacionComercial;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.OperacionComercialDetalle;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.domain.message.TdpErrorMessageServiceEntities;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpCatalogOutPlanta;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpVisor;
import pe.com.tdp.ventafija.microservices.domain.repository.CustomerRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpCatalogOutPlantaRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpErrorMessageServiceRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpVisorRepository;
import pe.com.tdp.ventafija.microservices.repository.customer.CustomerDAO;
import pe.com.tdp.ventafija.microservices.repository.product.ProductDAO;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class CustomerService {

    private static final Logger logger = LogManager.getLogger(CustomerService.class);
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ParametersRepository parametersRepository;
    @Autowired
    private ParametersService parametersService;
    @Autowired
    private TdpServiceClientRepository tdpServiceClientRepository;
    @Autowired
    private TdpServicePedidoRepository tdpServicePedidoRepository;
    @Autowired
    private TdpVisorRepository tdpVisorRepository;
    @Autowired
    private TdpErrorMessageServiceRepository tdpErrorMessageServiceRepository;
    @Autowired
    private TdpCatalogOutPlantaRepository tdpCatalogOutPlantaRepository;

    @Autowired
    private MessageUtil messageUtil;
    private ApiHeaderConfig apiHeaderConfig;
    private CustomerDAO dao;
    private Firebase firebase;
    private ServiceCallEventsService serviceCallEventsService;
    private ExpertoService expertoService;
    private LegadoService legadoService;

    @Autowired
    private ProductDAO daoProduct;

    @Value("${identity.validation.parents.options}")
    private Long identityValidationParentsOptions;
    //Variables
    @Value("${custom.mensajes.contrato.ErrorGenerico}")
    private String mensajeContratoErrorGenerico;
    @Value("${custom.mensajes.contrato.ErrorCarga}")
    private String mensajeContratoErrorCarga;
    @Value("${custom.mensajes.validDupli.ErrorCmsAtis}")
    private String mensajeValidDupliErrorCmsAtis;
    @Value("${custom.mensajes.validDupli.ErrorGener}")
    private String mensajeValidDupliErrorGener;
    @Value("${custom.mensajes.validReniec.ErrorDniNoVali}")
    private String mensajeValidReniecErrorDniNoVali;
    @Value("${custom.mensajes.validReniec.ErrorReniec}")
    private String mensajeValidReniecErrorReniec;
    @Value("${custom.mensajes.validReniec.ErrorGener}")
    private String mensajeValidReniecErrorGener;
    @Value("${custom.mensajes.parque.ErrorParqueCmsAtis}")
    private String mensajeParqueErrorParqueCmsAtis;
    @Value("${custom.mensajes.parque.ErrorGener}")
    private String mensajeParqueErrorGener;

    @Value("${customer.mensaje.opcion.seleccione.padre}")
    private String opcionSeleccionePadre;
    @Value("${customer.mensaje.opcion.seleccione.madre}")
    private String opcionSeleccioneMadre;

    @Value("${tdp.api.max.retry}")
    private Integer maxRetry;

    @Value("${visor.operacioncomercial.altapura}")
    private String scoringOperacionComercial;
    @Value("${scoring.stop.estados.atis}")
    private String scoringEstadoStopAtis;
    @Value("${scoring.stop.estados.cms}")
    private String scoringEstadoStopCms;

    // Detener contingencia stados de ATIS
    @Value("${contingencia.stop.estados.atis}")
    private String contingenciaStopEstadoAtis;
    // Detener contingencia stados de ATIS
    @Value("${contingencia.stop.estados.cms}")
    private String contingenciaStopEstadoCms;

    @Value("${experto.ruc.20}")
    private String expertoRuc20;

    //Sprint 24 RUC Scoring
    @Value("${experto.ruc.20.data}")
    private String expertoRuc20Data;

    //Sprint 28 Scoring Contingencia
    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerService(ApiHeaderConfig apiHeaderConfig, CustomerDAO dao, Firebase firebase,
                           ServiceCallEventsService serviceCallEventsService, ExpertoService expertoService,
                           LegadoService legadoService) {
        super();
        this.apiHeaderConfig = apiHeaderConfig;
        this.dao = dao;
        this.firebase = firebase;
        this.serviceCallEventsService = serviceCallEventsService;
        this.expertoService = expertoService;
        this.legadoService = legadoService;
    }

    public Response<ContractResponseData> readContract(ContractRequest request) {

        logger.info("Inicio ReadContract APP Service");
        Response<ContractResponseData> response = new Response<>();
        List<ContractResponseParameter> list = new ArrayList<>();
        List<String> contractList = new ArrayList<>();
        List<String> speechList = new ArrayList<>();
        ContractResponseData responseData = new ContractResponseData();

        try {
            SaleApiFirebaseResponseBody objFirebase = firebase.loadFirebaseObject(request.getOrderId());
            //LogVass.serviceResponseObject(logger, "ReadContract APP", "objFirebase", objFirebase);

            if (objFirebase == null) {
                response.setResponseCode("2");
                response.setResponseMessage("Error Firebase.");
                logger.info("Fin ReadContract Service");
                return response;
            } else {
                list = dao.readContractFlujo(objFirebase.getType());
                //LogVass.serviceResponseArray(logger, "ReadContract APP", "list", list);

                if (list.isEmpty()) {
                    response.setResponseCode("1");
                    //Agrega Error del APP
                    response.setResponseMessage(mensajeContratoErrorGenerico);
                    logger.info("Fin ReadContract Service");
                    return response;
                } else {
                    try {
                        if (objFirebase.getProduct_id() != null) {
                            List<String> lstInformationCatalog = dao.readInformationCatalog(objFirebase.getProduct_id());
                            //LogVass.serviceResponseArray(logger, "ReadContract APP", "lstInformationCatalog", lstInformationCatalog);
                            if (lstInformationCatalog.size() > 0) {
                                objFirebase.setProduct_install_cost(lstInformationCatalog.get(0) != null ? new BigDecimal(lstInformationCatalog.get(0)) : null);
                                objFirebase.setProduct_financing_cost(lstInformationCatalog.get(1) != null ? new BigDecimal(lstInformationCatalog.get(1)) : null);
                                objFirebase.setProduct_financing_month(lstInformationCatalog.get(2) != null ? Integer.parseInt(lstInformationCatalog.get(2)) : null);
                                objFirebase.setProm_speed(lstInformationCatalog.get(3) != null ? Integer.parseInt(lstInformationCatalog.get(3)) : null);
                                objFirebase.setPeriod_prom_speed(lstInformationCatalog.get(4) != null ? Integer.parseInt(lstInformationCatalog.get(4)) : null);
                                objFirebase.setProduct_equipamiento_linea(lstInformationCatalog.get(5).replace("-", ""));
                                objFirebase.setProduct_equipamiento_internet(lstInformationCatalog.get(6).replace("-", ""));
                                objFirebase.setProduct_equipamiento_tv(lstInformationCatalog.get(7).replace("-", ""));
                            }
                            if (objFirebase.getProduct_cash_price() != null && objFirebase.getProduct_return_month() != null) {
                                if (BigDecimal.valueOf(objFirebase.getProduct_return_month()).compareTo(BigDecimal.valueOf(0)) > 0) {
                                    objFirebase.setPrice_flat(objFirebase.getProduct_cash_price().divide(BigDecimal.valueOf(objFirebase.getProduct_return_month())));
                                } else {
                                    objFirebase.setPrice_flat(objFirebase.getProduct_cash_price());
                                }
                            }
                        }
                    } catch (Exception e) {
                        logger.error("ReadContract APP => lstInformationCatalog(0): null");
                    }

                    Field[] objFields = SaleApiFirebaseResponseBody.class.getDeclaredFields();
                    //LogVass.serviceResponseObject(logger, "ReadContract APP", "objFields", objFields);

                    StringBuilder svaCode = new StringBuilder();
                    if (objFirebase.getSva() != null) {
                        for (int i = 0; i < objFirebase.getSva().size(); i++) {
                            String[] sva = objFirebase.getSva().get(i).split("-");
                            if (Double.parseDouble(sva[1]) > 0.0) {
                                if (svaCode.toString().trim().length() > 0) {
                                    svaCode.append(",");
                                }
                                svaCode.append(sva[0]);
                            }
                        }
                    }
                    //logger.info("ReadContract => svaCode: " + svaCode);

                    List<String[]> svaContract = new ArrayList<>();
                    List<String[]> svaSpeech = new ArrayList<>();
                    if (CustomerConstants.PARAMETRO_FLUJO_ALTA.equalsIgnoreCase(objFirebase.getType())) {
                        svaContract = dao.getContractSVA(CustomerConstants.PARAMETRO_FLUJO_ALTA, CustomerConstants.PARAMETRO_CONTRACT, svaCode.toString());
                        //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaContract", svaContract);
                        svaSpeech = dao.getContractSVA(CustomerConstants.PARAMETRO_FLUJO_ALTA, CustomerConstants.PARAMETRO_SPEECH, svaCode.toString());
                        //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaSpeech", svaSpeech);
                    } else if (CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(objFirebase.getType())) {
                        svaContract = dao.getContractSVA(CustomerConstants.PARAMETRO_FLUJO_MIGRACION, CustomerConstants.PARAMETRO_CONTRACT, svaCode.toString());
                        //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaContract", svaContract);
                        svaSpeech = dao.getContractSVA(CustomerConstants.PARAMETRO_FLUJO_MIGRACION, CustomerConstants.PARAMETRO_SPEECH, svaCode.toString());
                        //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaSpeech", svaSpeech);
                    } else if (CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())) {
                        svaContract = dao.getContractSVA(CustomerConstants.PARAMETRO_FLUJO_SVA, CustomerConstants.PARAMETRO_CONTRACT, svaCode.toString());
                        //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaContract", svaContract);
                        svaSpeech = dao.getContractSVA(CustomerConstants.PARAMETRO_FLUJO_SVA, CustomerConstants.PARAMETRO_SPEECH, svaCode.toString());
                        //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaSpeech", svaSpeech);
                    }

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i) != null) {
                            //JSONObject ooobj = new JSONObject(list.get(i));
                            //logger.info("ReadContract APP => ooobj: " + ooobj);
                            if (list.get(i).getCategory() != null) {
                                for (int j = 0; j < objFields.length; j++) {
                                    objFields[j].setAccessible(true);
                                    if (objFields[j].getName().equalsIgnoreCase(list.get(i).getCategory())) {
                                        if (list.get(i).getDomain() != null) {
                                            if (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain())) {
                                                if (list.get(i).getElement() != null) {
                                                    String field = String.valueOf(objFields[j].get(objFirebase));
                                                    if (field != null) {
                                                        if (field.equalsIgnoreCase(list.get(i).getElement())) {
                                                            contractList.add(list.get(i).getStrValue());
                                                            //logger.info("ReadContract APP => contractList => list.get(i).getCategory(): " + list.get(i).getCategory());
                                                            //logger.info("ReadContract APP => contractList => list.get(i).getStrValue(): " + list.get(i).getStrValue());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (objFields[j].getName().equalsIgnoreCase(list.get(i).getCategory())) {
                                        if (list.get(i).getDomain() != null) {
                                            if (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain())) {
                                                if (list.get(i).getElement() != null) {
                                                    String field = String.valueOf(objFields[j].get(objFirebase));
                                                    if (field != null) {
                                                        if (field.equalsIgnoreCase(list.get(i).getElement())) {
                                                            speechList.add(list.get(i).getStrValue());
                                                            //logger.info("ReadContract APP => speechList => list.get(i).getCategory(): " + list.get(i).getCategory());
                                                            //logger.info("ReadContract APP => speechList => list.get(i).getStrValue(): " + list.get(i).getStrValue());
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement() == null &&
                                        (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    contractList.add(list.get(i).getStrValue());
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {
                                    if (list.get(i).getElement() != null) {
                                        String[] elementos = list.get(i).getElement().split(",");
                                        for (int j = 0; j < elementos.length; j++) {
                                            for (int k = 0; k < svaContract.size(); k++) {
                                                if (svaContract.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                                        && svaContract.get(k)[2] != null
                                                        && svaContract.get(k)[0].equalsIgnoreCase(elementos[j])
                                                        && svaContract.get(k)[2].equalsIgnoreCase(list.get(i).getCampo())
                                                        && (list.get(i).getStrValue()).contains(svaContract.get(k)[2])) {

                                                    contractList.add(svaContract.get(k)[1]);
                                                } else if (!svaContract.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                                        && svaContract.get(k)[0].equalsIgnoreCase(elementos[j])) {

                                                    contractList.add(svaContract.get(k)[1]);
                                                }
                                            }
                                        }
                                    }
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_ALTA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement() != null && list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_has_discount())) &&
                                        CustomerConstants.PARAMETRO_FLUJO_ALTA.equals(objFirebase.getType()) &&
                                        (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    contractList.add(list.get(i).getStrValue());
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_MIGRACION.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_has_discount())) &&
                                        CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(objFirebase.getType()) &&
                                        CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain())) {

                                    contractList.add(list.get(i).getStrValue());
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_LEGACY.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement().equals(String.valueOf(objFirebase.getLegacy_code())) &&
                                        CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(objFirebase.getType()) &&
                                        CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain())) {

                                    contractList.add(list.get(i).getStrValue());
                                }

                                /* Velocidad Promociona Speed Internet - No Aplica para SVA */
                                if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())
                                        && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SPEED.equalsIgnoreCase(list.get(i).getCategory())
                                        && list.get(i).getElement() != null
                                        && list.get(i).getElement().equals(String.valueOf(objFirebase.getProm_speed() > 0))
                                        && (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    contractList.add(list.get(i).getStrValue());
                                }
                                /* Velocidad Promociona Speed Internet */

                                /* Equipamientos - No Aplica para SVA */
                                if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())
                                        && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_EQUIPMENTS.equalsIgnoreCase(list.get(i).getCategory())
                                        && list.get(i).getElement() != null
                                        && list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_equipamiento_linea() != null))
                                        && (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    contractList.add(list.get(i).getStrValue());
                                }
                                /* Equipamientos */

                                /* Costo de Instalación - No Aplica para SVA */
                                if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())
                                        && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_PRODUCTO.equalsIgnoreCase(list.get(i).getCategory())
                                        && list.get(i).getElement() != null
                                        && list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_financing_month() != null ? objFirebase.getProduct_financing_month() > 0 : false))
                                        && (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    contractList.add(list.get(i).getStrValue());
                                }
                                /* Costo de Instalación */

                                if (!CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement() == null &&
                                        (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    speechList.add(list.get(i).getStrValue());
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    String[] elementos = list.get(i).getElement().split(",");
                                    for (int j = 0; j < elementos.length; j++) {
                                        for (int k = 0; k < svaSpeech.size(); k++) {
                                            if (svaSpeech.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                                    && svaSpeech.get(k)[2] != null
                                                    && svaSpeech.get(k)[0].equalsIgnoreCase(elementos[j])
                                                    && svaSpeech.get(k)[2].equalsIgnoreCase(list.get(i).getCampo())
                                                    && (list.get(i).getStrValue()).contains(svaSpeech.get(k)[2])) {

                                                speechList.add(svaSpeech.get(k)[1]);
                                            } else if (!svaSpeech.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                                    && svaSpeech.get(k)[0].equalsIgnoreCase(elementos[j])) {

                                                speechList.add(svaSpeech.get(k)[1]);
                                            }
                                        }
                                    }
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_ALTA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement() != null && list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_has_discount())) &&
                                        CustomerConstants.PARAMETRO_FLUJO_ALTA.equals(objFirebase.getType()) &&
                                        (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    speechList.add(list.get(i).getStrValue());
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_MIGRACION.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_has_discount())) &&
                                        CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(objFirebase.getType()) &&
                                        CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain())) {

                                    speechList.add(list.get(i).getStrValue());
                                }
                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_LEGACY.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement().equals(String.valueOf(objFirebase.getLegacy_code())) &&
                                        CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(objFirebase.getType()) &&
                                        CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain())) {

                                    speechList.add(list.get(i).getStrValue());
                                }

                                /* Velocidad Promociona Speed Internet - No Aplica para SVA */
                                if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())
                                        && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SPEED.equalsIgnoreCase(list.get(i).getCategory())
                                        && list.get(i).getElement() != null
                                        && list.get(i).getElement().equals(String.valueOf(objFirebase.getProm_speed() > 0))
                                        && (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    speechList.add(list.get(i).getStrValue());
                                }
                                /* Velocidad Promociona Speed Internet */

                                /* Equipamientos - No Aplica para SVA */
                                if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())
                                        && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_EQUIPMENTS.equalsIgnoreCase(list.get(i).getCategory())
                                        && list.get(i).getElement() != null
                                        && list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_equipamiento_linea() != null))
                                        && (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    speechList.add(list.get(i).getStrValue());
                                }
                                /* Equipamientos */

                                /* Costo de Instalación - No Aplica para SVA */
                                if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())
                                        && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_PRODUCTO.equalsIgnoreCase(list.get(i).getCategory())
                                        && list.get(i).getElement() != null
                                        && list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_financing_month() != null ? objFirebase.getProduct_financing_month() > 0 : false))
                                        && (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                                    speechList.add(list.get(i).getStrValue());
                                }
                                /* Costo de Instalación */
                            }
                        }
                    }

                    //LogVass.serviceResponseArray(logger, "ReadContract APP", "contractList", contractList);
                    //LogVass.serviceResponseArray(logger, "ReadContract APP", "speechList", speechList);

                    logger.info("Contract APP: " + contractList.size());
                    logger.info("Speech APP: " + speechList.size());
                    List<Parameter> valuesParameterList = dao.getParameter("VALUES");
                    //LogVass.serviceResponseArray(logger, "ReadContract APP", "valuesParameterList", valuesParameterList);

                    for (int i = 0; i < contractList.size(); i++) {
                        for (int j = 0; j < objFields.length; j++) {
                            objFields[j].setAccessible(true);
                            if (Arrays
                                    .asList(CustomerConstants.PARAMETRO_PRODUCTO_1, CustomerConstants.PARAMETRO_PRODUCTO_2,
                                            CustomerConstants.PARAMETRO_PRODUCTO_3, CustomerConstants.PARAMETRO_PRODUCTO_4,
                                            CustomerConstants.PARAMETRO_PRODUCTO_5)
                                    .contains(objFields[j].getName())) {
                                contractList.set(i,
                                        contractList.get(i).replace(
                                                getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                String.valueOf(
                                                        new BigDecimal(String.valueOf(objFields[j].get(objFirebase)))
                                                                .setScale(2, RoundingMode.HALF_EVEN))));
                            } else {
                                if (Arrays
                                        .asList(CustomerConstants.PARAMETRO_PRODUCTO_14)
                                        .contains(objFields[j].getName())) {
                                    if (contractList.get(i) != null && contractList.get(i).indexOf(getDescriptionByElement(objFields[j].getName(), valuesParameterList)) > 0) {
                                        String equipamientos = "";
                                        int cont1 = 0;
                                        for (int k1 = 0; k1 < objFields.length; k1++) {
                                            if (Arrays
                                                    .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_16)
                                                    .contains(objFields[k1].getName())) {
                                                if (objFields[k1].get(objFirebase) != null && !String.valueOf(objFields[k1].get(objFirebase)).equals("")) {
                                                    cont1++;
                                                }
                                            }
                                        }
                                        int cont2 = 0;
                                        for (int k2 = 0; k2 < objFields.length; k2++) {
                                            if (Arrays
                                                    .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_16)
                                                    .contains(objFields[k2].getName())) {
                                                if (objFields[k2].get(objFirebase) != null && !String.valueOf(objFields[k2].get(objFirebase)).equals("")) {
                                                    if (cont1 == 1) {
                                                        equipamientos = String.valueOf(objFields[k2].get(objFirebase));
                                                    } else {
                                                        if (cont1 == 2) {
                                                            equipamientos = equipamientos + (cont2 == 0 ? "" : " y ") + String.valueOf(objFields[k2].get(objFirebase));
                                                            cont2++;
                                                        } else {
                                                            if (cont1 == 3) {
                                                                equipamientos = equipamientos + (cont2 == 0 ? "" : (cont2 == 1 ? " , " : " y ")) + String.valueOf(objFields[k2].get(objFirebase));
                                                                cont2++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        contractList.set(i,
                                                contractList.get(i).replace(
                                                        getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                        equipamientos));

                                    }
                                } else {
                                    contractList.set(i,
                                            contractList.get(i).replace(
                                                    getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                    String.valueOf(objFields[j].get(objFirebase))));
                                }
                                //logger.info("ReadContract APP : contractList.get(" + i + "):" + getDescriptionByElement(objFields[j].getName(), valuesParameterList) + "|" + objFields[j].get(objFirebase));
                            }
                        }
                        contractList.set(i,
                                contractList.get(i)
                                        .replace(CustomerConstants.PARAMETRO_ANIO,
                                                ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                        .format(DateTimeFormatter.ofPattern("yyyy")))
                                        .replace(CustomerConstants.PARAMETRO_DIA,
                                                ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                        .format(DateTimeFormatter.ofPattern("d")))
                                        .replace(CustomerConstants.PARAMETRO_MES,
                                                ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                        .format(DateTimeFormatter.ofPattern("MMMM")
                                                                .withLocale(new Locale("es", "PE")))));
                    }
                    for (int i = 0; i < speechList.size(); i++) {
                        for (int j = 0; j < objFields.length; j++) {
                            objFields[j].setAccessible(true);
                            if (Arrays.asList(CustomerConstants.PARAMETRO_PRODUCTO_6, CustomerConstants.PARAMETRO_PRODUCTO_7,
                                    CustomerConstants.PARAMETRO_PRODUCTO_8, CustomerConstants.PARAMETRO_PRODUCTO_9,
                                    CustomerConstants.PARAMETRO_PRODUCTO_10)
                                    .contains(objFields[j].getName())) {
                                speechList.set(i,
                                        speechList.get(i).replace(
                                                getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                String.valueOf(objFields[j].get(objFirebase)).replace("", " ").trim()));
                            } else if (Arrays
                                    .asList(CustomerConstants.PARAMETRO_PRODUCTO_1, CustomerConstants.PARAMETRO_PRODUCTO_2,
                                            CustomerConstants.PARAMETRO_PRODUCTO_3, CustomerConstants.PARAMETRO_PRODUCTO_4,
                                            CustomerConstants.PARAMETRO_PRODUCTO_5)
                                    .contains(objFields[j].getName())) {
                                speechList.set(i,
                                        speechList.get(i).replace(
                                                getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                decimalSpeechParser(String.valueOf(objFields[j].get(objFirebase)))));
                            } else if (CustomerConstants.PARAMETRO_PRODUCTO_11.equals(objFields[j].getName())
                                    || CustomerConstants.PARAMETRO_PRODUCTO_12.equals(objFields[j].getName())
                                    || CustomerConstants.PARAMETRO_PRODUCTO_13.equals(objFields[j].getName())) {
                                speechList.set(i,
                                        speechList.get(i).replace(
                                                getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                String.valueOf(objFields[j].get(objFirebase)).toLowerCase()));
                            } else {
                                if (Arrays
                                        .asList(CustomerConstants.PARAMETRO_PRODUCTO_14)
                                        .contains(objFields[j].getName())) {
                                    if (speechList.get(i) != null && speechList.get(i).indexOf(getDescriptionByElement(objFields[j].getName(), valuesParameterList)) > 0) {
                                        String equipamientos = "";
                                        int cont1 = 0;
                                        for (int k1 = 0; k1 < objFields.length; k1++) {
                                            if (Arrays
                                                    .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_16)
                                                    .contains(objFields[k1].getName())) {
                                                if (objFields[k1].get(objFirebase) != null && !String.valueOf(objFields[k1].get(objFirebase)).equals("")) {
                                                    cont1++;
                                                }
                                            }
                                        }
                                        int cont2 = 0;
                                        for (int k2 = 0; k2 < objFields.length; k2++) {
                                            if (Arrays
                                                    .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_16)
                                                    .contains(objFields[k2].getName())) {
                                                if (objFields[k2].get(objFirebase) != null && !String.valueOf(objFields[k2].get(objFirebase)).equals("")) {
                                                    if (cont1 == 1) {
                                                        equipamientos = String.valueOf(objFields[k2].get(objFirebase));
                                                    } else {
                                                        if (cont1 == 2) {
                                                            equipamientos = equipamientos + (cont2 == 0 ? "" : " y ") + String.valueOf(objFields[k2].get(objFirebase));
                                                            cont2++;
                                                        } else {
                                                            if (cont1 == 3) {
                                                                equipamientos = equipamientos + (cont2 == 0 ? "" : (cont2 == 1 ? " , " : " y ")) + String.valueOf(objFields[k2].get(objFirebase));
                                                                cont2++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        speechList.set(i,
                                                speechList.get(i).replace(
                                                        getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                        equipamientos));

                                    }

                                } else {
                                    speechList.set(i,
                                            speechList.get(i).replace(
                                                    getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                    String.valueOf(objFields[j].get(objFirebase))));
                                }
                            }
                            //logger.info("ReadContract APP : speechList.get(" + i + "):" + getDescriptionByElement(objFields[j].getName(), valuesParameterList) + "|" + objFields[j].get(objFirebase));
                        }
                        speechList.set(i,
                                speechList.get(i)
                                        .replace(CustomerConstants.PARAMETRO_ANIO,
                                                ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                        .format(DateTimeFormatter.ofPattern("yyyy")))
                                        .replace(CustomerConstants.PARAMETRO_DIA,
                                                ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                        .format(DateTimeFormatter.ofPattern("d")))
                                        .replace(CustomerConstants.PARAMETRO_MES,
                                                ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                        .format(DateTimeFormatter.ofPattern("MMMM")
                                                                .withLocale(new Locale("es", "PE")))));
                    }

                    for (int c = 0; c < contractList.size(); c++) {
                        if (contractList.get(c).toLowerCase().indexOf("no aplica") > 0 || contractList.get(c).toLowerCase().indexOf("null") > 0) {
                            //logger.info("APP remove contractlist= "+contractList.get(c));
                            contractList.remove(c);
                        }
                    }
                    for (int c = 0; c < speechList.size(); c++) {
                        if (speechList.get(c).toLowerCase().indexOf("no aplica") > 0 || speechList.get(c).toLowerCase().indexOf("null") > 0) {
                            //logger.info("APP remove speechList= "+speechList.get(c));
                            speechList.remove(c);
                        }
                    }

                    responseData.setContract(contractList);
                    responseData.setSpeech(speechList);
                    response.setResponseCode("0");
                    response.setResponseData(responseData);
                }
            }
            logger.info("Fin Contract APP Service");
        } catch (Exception e) {
            logger.info("Error ReadContract Service ", e);
            response.setResponseCode("0");
            //Agrega Error en carga de contrato
            response.setResponseMessage(mensajeContratoErrorGenerico);
            logger.info("Fin ReadContract Service");
            return response;
        }
        //System.out.println("Fin ReadContract APP Service");
        return response;
    }

    /**
     * @param request
     * @return
     */
    public Response<ContractWebResponseData> readContractWeb(ContractWebRequest request) {

        Response<ContractWebResponseData> response = new Response<>();
        List<ContractResponseParameter> list = new ArrayList<>();
        List<ContractWebResponseDataParagraph> contractList = new ArrayList<>();
        ContractWebResponseData responseData = new ContractWebResponseData();

        SaleApiFirebaseResponseBody objFirebase = new SaleApiFirebaseResponseBody();

        if (CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(request.getType())) {
            objFirebase.setProduct_payment_method(request.getSelectedOffering().getPaymentMethod());
            objFirebase.setProduct_cash_price(request.getSelectedOffering().getCashPrice());
        } else {
            objFirebase.setProduct_name(request.getSelectedOffering().getProductName());
            objFirebase.setProduct_has_discount(request.getSelectedOffering().getDiscount() != null ? (request.getSelectedOffering().getDiscount().equalsIgnoreCase("si") ? true : false) : false);
            objFirebase.setProduct_financing_month(request.getSelectedOffering().getFinancingMonth());
            objFirebase.setProduct_return_month(request.getSelectedOffering().getReturnMonth());
            objFirebase.setProduct_return_period(request.getSelectedOffering().getReturnPeriod());
            objFirebase.setProduct_has_equipment(request.getSelectedOffering().getEquipmentType() != null ? (request.getSelectedOffering().getEquipmentType().equalsIgnoreCase("si") ? true : false) : false);
            objFirebase.setProduct_equipment_type(request.getEquipmentName());
            objFirebase.setProduct_line_type(request.getSelectedOffering().getLineType());
            objFirebase.setProduct_commercial_operation(request.getSelectedOffering().getCommercialOperation());
            objFirebase.setProduct_payment_method(request.getSelectedOffering().getPaymentMethod());
            objFirebase.setOffering_expert_code(request.getSelectedOffering().getExpertCode());
            objFirebase.setProduct_prom_price(request.getSelectedOffering().getPromPrice());
            objFirebase.setProduct_install_cost(request.getSelectedOffering().getInstallCost());
            objFirebase.setProduct_price(request.getSelectedOffering().getPrice());
            objFirebase.setProduct_financing_cost(request.getSelectedOffering().getFinancingCost());
            objFirebase.setProduct_cash_price(request.getSelectedOffering().getCashPrice());
            objFirebase.setAtis_migration_phone(request.getProduct().getPhone());
            objFirebase.setAddress(request.getProduct().getAddress());
            objFirebase.setAddress_additional_info(request.getProduct().getAdditionalAddress());
            objFirebase.setMonth_period(String.valueOf(request.getSelectedOffering().getMonthPeriod()));
            objFirebase.setLegacy_code(request.getProduct().getParkType());

        }

        List<String> svaList = new ArrayList<>();
        if (request.getSva() != null) {
            for (ContractWebRequestSva sva : request.getSva()) {
                // Sprint 10 - Para alta pura: si es que el costo del sva es 0 (ya esta incluido en el precio total del producto) entonces no debemos mostrar contrato para ese sva
                if (!"0".equals(sva.getCost())) {
                    svaList.add(String.valueOf(sva.getId()));
                }
            }
        }

        objFirebase.setSva(svaList);
        objFirebase.setClient_document(request.getCustomer().getDocumentType());
        objFirebase.setClient_doc_number(request.getCustomer().getDocumentNumber());
        objFirebase.setClient_email(request.getCustomer().getEmail());
        objFirebase.setClient_has_email(request.getCustomer().getEmail() != null ? (request.getCustomer().getEmail().equalsIgnoreCase("") ? false : true) : false);
        objFirebase.setClient_lastname(request.getCustomer().getLastName1());
        objFirebase.setClient_mother_lastname(request.getCustomer().getLastName2());
        objFirebase.setClient_mobile_phone(request.getCustomer().getMobilePhone());
        objFirebase.setClient_names(request.getCustomer().getFirstName());
        objFirebase.setClient_secondary_phone(request.getCustomer().getTelephone());
        objFirebase.setType(request.getType());

        objFirebase.setVendor_id(request.getUser().getUserId());

        if (objFirebase != null) {
            try {
                logger.info("Inicio Contract WEB Service");
                list = dao.readContractFlujo(request.getType());
                //LogVass.serviceResponseArray(logger, "ReadContract WEB", "readContractFlujoList", list);
                if (list.isEmpty()) {
                    response.setResponseCode("1");
                    response.setResponseMessage("No existen valores");
                } else {
                    try {
                        if (request.getSelectedOffering().getId() > 0) {
                            List<String> lstInformationCatalog = dao.readInformationCatalog("" + request.getSelectedOffering().getId());
                            //LogVass.serviceResponseArray(logger, "ReadContract WEB", "lstInformationCatalog", lstInformationCatalog);
                            if (lstInformationCatalog.size() > 0) {
                                objFirebase.setProduct_install_cost(lstInformationCatalog.get(0) != null ? new BigDecimal(lstInformationCatalog.get(0)) : null);
                                objFirebase.setProduct_financing_cost(lstInformationCatalog.get(1) != null ? new BigDecimal(lstInformationCatalog.get(1)) : null);
                                objFirebase.setProduct_financing_month(lstInformationCatalog.get(2) != null ? Integer.parseInt(lstInformationCatalog.get(2)) : null);
                                objFirebase.setProm_speed(lstInformationCatalog.get(3) != null ? Integer.parseInt(lstInformationCatalog.get(3)) : null);
                                objFirebase.setPeriod_prom_speed(lstInformationCatalog.get(4) != null ? Integer.parseInt(lstInformationCatalog.get(4)) : null);
                                objFirebase.setProduct_equipamiento_linea(lstInformationCatalog.get(5).replace("-", ""));
                                objFirebase.setProduct_equipamiento_internet(lstInformationCatalog.get(6).replace("-", ""));
                                objFirebase.setProduct_equipamiento_tv(lstInformationCatalog.get(7).replace("-", ""));
                            }
                            if (objFirebase.getProduct_cash_price() != null && objFirebase.getProduct_return_month() != null) {
                                if (BigDecimal.valueOf(objFirebase.getProduct_return_month()).compareTo(BigDecimal.valueOf(0)) > 0) {
                                    objFirebase.setPrice_flat(objFirebase.getProduct_cash_price().divide(BigDecimal.valueOf(objFirebase.getProduct_return_month())));
                                } else {
                                    objFirebase.setPrice_flat(objFirebase.getProduct_cash_price());
                                }
                            }
                        }
                    } catch (Exception e) {
                        logger.error("ReadContract WEB => lstInformationCatalog(0): null" + e);
                    }

                    //LogVass.serviceResponseObject(logger, "ReadContract WEB", "objFirebase", objFirebase);

                    Field[] objFields = SaleApiFirebaseResponseBody.class.getDeclaredFields();
                    //LogVass.serviceResponseObject(logger, "ReadContract WEB", "objFields", objFields);

                    StringBuilder svaCode = new StringBuilder();
                    for (int i = 0; i < objFirebase.getSva().size(); i++) {
                        svaCode.append(objFirebase.getSva().get(i));
                        if (i < objFirebase.getSva().size() - 1) {
                            svaCode.append(",");
                        }
                    }
                    //logger.info("SvaCode:" + svaCode);

                    List<String[]> svaContract = new ArrayList<>();
                    if (CustomerConstants.PARAMETRO_FLUJO_ALTA.equalsIgnoreCase(request.getType())) {
                        svaContract = dao.getContractSVA(CustomerConstants.PARAMETRO_FLUJO_ALTA, CustomerConstants.PARAMETRO_CONTRACT, svaCode.toString());
                    } else {
                        if (CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(request.getType())) {
                            svaContract = dao.getContractSVA(CustomerConstants.PARAMETRO_FLUJO_MIGRACION, CustomerConstants.PARAMETRO_CONTRACT, svaCode.toString());
                        } else {
                            if (CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(request.getType())) {
                                svaContract = dao.getContractSVA(CustomerConstants.PARAMETRO_FLUJO_SVA, CustomerConstants.PARAMETRO_CONTRACT, svaCode.toString());
                            }
                        }
                    }

                    if (svaContract != null && !svaContract.isEmpty()) {
                        logger.info("Lista svaContract: " + svaContract.size());
                    }

                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i) != null) {
                            if (list.get(i).getCategory() != null) {
                                for (int j = 0; j < objFields.length; j++) {
                                    objFields[j].setAccessible(true);
                                    if (objFields[j].getName().equalsIgnoreCase(list.get(i).getCategory())) {
                                        if (list.get(i).getElement() != null) {
                                            String field = String.valueOf(objFields[j].get(objFirebase));
                                            if (field != null) {
                                                if (field.equalsIgnoreCase(list.get(i).getElement())) {
                                                    contractList.add(new ContractWebResponseDataParagraph(list.get(i).getStrValue(), list.get(i).getCategory()));
                                                    //logger.info("ReadContract WEB => contractList => list.get(i).getCategory(): " + list.get(i).getCategory());
                                                    //logger.info("ReadContract WEB => contractList => list.get(i).getStrValue(): " + list.get(i).getStrValue());
                                                }
                                            }
                                        }
                                    }
                                }

                                if (!CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement() == null) {

                                    contractList.add(new ContractWebResponseDataParagraph(list.get(i).getStrValue(), list.get(i).getCategory()));
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory())) {
                                    if (list.get(i).getElement() != null) {
                                        String[] elementos = list.get(i).getElement().split(",");
                                        for (int j = 0; j < elementos.length; j++) {
                                            for (int k = 0; k < svaContract.size(); k++) {
                                                if (svaContract.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                                        && svaContract.get(k)[2] != null
                                                        && svaContract.get(k)[0].equalsIgnoreCase(elementos[j])
                                                        && svaContract.get(k)[2].equalsIgnoreCase(list.get(i).getCampo())
                                                        && (list.get(i).getStrValue()).contains(svaContract.get(k)[2])) {

                                                    contractList.add(new ContractWebResponseDataParagraph(svaContract.get(k)[1], list.get(i).getCategory()));
                                                } else if (!svaContract.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                                        && svaContract.get(k)[0].equalsIgnoreCase(elementos[j])) {

                                                    contractList.add(new ContractWebResponseDataParagraph(svaContract.get(k)[1], list.get(i).getCategory()));
                                                }
                                            }
                                        }
                                    }
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_ALTA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement() != null && list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_has_discount())) &&
                                        CustomerConstants.PARAMETRO_FLUJO_ALTA.equals(objFirebase.getType())) {

                                    contractList.add(new ContractWebResponseDataParagraph(list.get(i).getStrValue(), list.get(i).getCategory()));
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_MIGRACION.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_has_discount())) &&
                                        CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(objFirebase.getType())) {

                                    contractList.add(new ContractWebResponseDataParagraph(list.get(i).getStrValue(), list.get(i).getCategory()));
                                }

                                if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_LEGACY.equalsIgnoreCase(list.get(i).getCategory()) &&
                                        list.get(i).getElement().equals(String.valueOf(objFirebase.getLegacy_code())) &&
                                        CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(objFirebase.getType())) {

                                    contractList.add(new ContractWebResponseDataParagraph(list.get(i).getStrValue(), list.get(i).getCategory()));
                                }

                                /* Velocidad Promociona Speed Internet - No Aplica para SVA */
                                if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())
                                        && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SPEED.equalsIgnoreCase(list.get(i).getCategory())
                                        && list.get(i).getElement() != null
                                        && list.get(i).getElement().equals(String.valueOf(objFirebase.getProm_speed() > 0))) {

                                    contractList.add(new ContractWebResponseDataParagraph(list.get(i).getStrValue(), list.get(i).getCategory()));
                                }
                                /* Velocidad Promociona Speed Internet */

                                /* Equipamientos - No Aplica para SVA */
                                if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())
                                        && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_EQUIPMENTS.equalsIgnoreCase(list.get(i).getCategory())
                                        && list.get(i).getElement() != null
                                        && list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_equipamiento_linea() != null))) {

                                    contractList.add(new ContractWebResponseDataParagraph(list.get(i).getStrValue(), list.get(i).getCategory()));
                                }
                                /* Equipamientos */

                                /* Costo de Instalación - No Aplica para SVA */
                                if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(objFirebase.getType())
                                        && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_PRODUCTO.equalsIgnoreCase(list.get(i).getCategory())
                                        && list.get(i).getElement() != null
                                        && list.get(i).getElement().equals(String.valueOf(objFirebase.getProduct_financing_month() != null ? objFirebase.getProduct_financing_month() > 0 : false))) {

                                    contractList.add(new ContractWebResponseDataParagraph(list.get(i).getStrValue(), list.get(i).getCategory()));
                                }
                                /* Costo de Instalación */

                            }
                        }
                    }

                    logger.info("Contract WEB: " + contractList.size());

                    List<Parameter> valuesParameterList = dao.getParameter("VALUES");

                    for (int i = 0; i < contractList.size(); i++) {
                        for (int j = 0; j < objFields.length; j++) {
                            objFields[j].setAccessible(true);

                            if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(request.getType())
                                    && Arrays
                                    .asList(CustomerConstants.PARAMETRO_PRODUCTO_1, CustomerConstants.PARAMETRO_PRODUCTO_2,
                                            CustomerConstants.PARAMETRO_PRODUCTO_3, CustomerConstants.PARAMETRO_PRODUCTO_4,
                                            CustomerConstants.PARAMETRO_PRODUCTO_5)
                                    .contains(objFields[j].getName())) {
                                contractList.set(i, new ContractWebResponseDataParagraph(
                                        contractList.get(i).getParagraph().replace(
                                                getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                String.valueOf(
                                                        new BigDecimal(String.valueOf(objFields[j].get(objFirebase)))
                                                                .setScale(2, RoundingMode.HALF_EVEN))),
                                        contractList.get(i).getCategory()));
                            } else if (CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(request.getType())
                                    && Arrays.asList(CustomerConstants.PARAMETRO_PRODUCTO_5).contains(objFields[j].getName())) {
                                contractList.set(i, new ContractWebResponseDataParagraph(
                                        contractList.get(i).getParagraph().replace(
                                                getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                String.valueOf(
                                                        new BigDecimal(String.valueOf(objFields[j].get(objFirebase)))
                                                                .setScale(2, RoundingMode.HALF_EVEN))),
                                        contractList.get(i).getCategory()));
                            } else {
                                if (Arrays
                                        .asList(CustomerConstants.PARAMETRO_PRODUCTO_14)
                                        .contains(objFields[j].getName())) {
                                    if (contractList.get(i) != null && contractList.get(i).getParagraph() != null && contractList.get(i).getParagraph().indexOf(getDescriptionByElement(objFields[j].getName(), valuesParameterList)) > 0) {
                                        String equipamientos = "";
                                        int cont1 = 0;
                                        for (int k1 = 0; k1 < objFields.length; k1++) {
                                            if (Arrays
                                                    .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_16)
                                                    .contains(objFields[k1].getName())) {
                                                if (objFields[k1].get(objFirebase) != null && !String.valueOf(objFields[k1].get(objFirebase)).equals("")) {
                                                    cont1++;
                                                }
                                            }
                                        }
                                        int cont2 = 0;
                                        for (int k2 = 0; k2 < objFields.length; k2++) {
                                            if (Arrays
                                                    .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                            CustomerConstants.PARAMETRO_PRODUCTO_16)
                                                    .contains(objFields[k2].getName())) {
                                                if (objFields[k2].get(objFirebase) != null && !String.valueOf(objFields[k2].get(objFirebase)).equals("")) {
                                                    if (cont1 == 1) {
                                                        equipamientos = String.valueOf(objFields[k2].get(objFirebase));
                                                    } else {
                                                        if (cont1 == 2) {
                                                            equipamientos = equipamientos + (cont2 == 0 ? "" : " y ") + String.valueOf(objFields[k2].get(objFirebase));
                                                            cont2++;
                                                        } else {
                                                            if (cont1 == 3) {
                                                                equipamientos = equipamientos + (cont2 == 0 ? "" : (cont2 == 1 ? " , " : " y ")) + String.valueOf(objFields[k2].get(objFirebase));
                                                                cont2++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        contractList.set(i, new ContractWebResponseDataParagraph(
                                                contractList.get(i).getParagraph().replace(
                                                        getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                        equipamientos),
                                                contractList.get(i).getCategory()));

                                    }
                                } else {
                                    contractList.set(i, new ContractWebResponseDataParagraph(
                                            contractList.get(i).getParagraph().replace(
                                                    getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                    String.valueOf(objFields[j].get(objFirebase))),
                                            contractList.get(i).getCategory()));
                                }
                            }
                            /*System.out.println(
                                    "Contract: " + getDescriptionByElement(objFields[j].getName(), valuesParameterList)
                                            + "|" + objFields[j].get(objFirebase));*/
                        }
                        contractList.set(i,
                                new ContractWebResponseDataParagraph(
                                        contractList.get(i).getParagraph()
                                                .replace(CustomerConstants.PARAMETRO_ANIO,
                                                        ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                                .format(DateTimeFormatter.ofPattern("yyyy")))
                                                .replace(CustomerConstants.PARAMETRO_DIA,
                                                        ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                                .format(DateTimeFormatter.ofPattern("d")))
                                                .replace(CustomerConstants.PARAMETRO_MES,
                                                        ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                                .format(DateTimeFormatter.ofPattern("MMMM")
                                                                        .withLocale(new Locale("es", "PE")))),
                                        contractList.get(i).getCategory()));
                    }

                    for (int c = 0; c < contractList.size(); c++) {
                        if (contractList.get(c).getParagraph().toLowerCase().indexOf("no aplica") > 0 || contractList.get(c).getParagraph().toLowerCase().indexOf("null") > 0) {
                            //logger.info("WEB remove contractlist= "+contractList.get(c).getParagraph());
                            contractList.remove(c);
                        }
                    }

                    responseData.setContractWeb(contractList);
                    response.setResponseCode("0");
                    response.setResponseData(responseData);
                }
                logger.info("Fin Contract WEB Service");
            } catch (Exception e) {
                logger.info("Error Contract Service", e);
                response.setResponseCode("0");
                response.setResponseMessage("Ocurrio un error.");
                return response;
            }
        }
        return response;
    }

    private String decimalSpeechParser(String decimal) {

        int integerPart = new BigDecimal(decimal).setScale(2, RoundingMode.HALF_EVEN).setScale(0, RoundingMode.FLOOR)
                .intValue();
        int fractionPart = new BigDecimal(decimal).setScale(2, RoundingMode.HALF_EVEN)
                .subtract(new BigDecimal(integerPart)).movePointRight(2).intValue();

        String decimalString;
        String stringSolesPlus = " soles con ";
        String stringSolPlus = " sol con ";
        String stringCentimos = " c\u00e9ntimos";
        String stringCentimo = " c\u00e9ntimo";
        if (integerPart == 1 && fractionPart == 1) {
            decimalString = integerPart + stringSolPlus + fractionPart + stringCentimo;
        } else if (integerPart == 1 && fractionPart != 1) {
            decimalString = integerPart + stringSolPlus + fractionPart + stringCentimos;
        } else if (integerPart != 1 && fractionPart == 1) {
            decimalString = integerPart + stringSolesPlus + fractionPart + stringCentimo;
        } else {
            decimalString = integerPart + stringSolesPlus + fractionPart + stringCentimos;
        }

        return decimalString;
    }

    /**
     * @param request
     * @return
     */
    public Response<List<ServicesResponseData>> readServices(ServicesRequest request) {
        logger.info("Inicio ReadServices Service");

        Response<List<ServicesResponseData>> response = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("PARQUE");
        LogVass.serviceResponseHashMap(logger, "PARQUE", "messageList", messageList);
        //End--Code for call Service

        try {
            List<ServicesResponseData> responseData = new ArrayList<>();

            List<TdpServiceClient> objCms = legadoService.getParqueAtis(request.getDocumentType(), request.getDocumentNumber(),request.getId());
            for (TdpServiceClient obj : objCms) {
                ServicesResponseData responseDataCms = new ServicesResponseData();
                responseDataCms.setParkType("CMS");
                responseDataCms.setSubscriber(obj.getClientCms());
                responseDataCms.setServiceCode(obj.getProductService());
                responseDataCms.setStatus(obj.getServiceEstado());
                responseDataCms.setSourceType(obj.getProductOrigen());
                responseDataCms.setCoordinateX("" + obj.getCoordenadaX());
                responseDataCms.setCoordinateY("" + obj.getCoordenadaY());
                responseDataCms.setDepartment(obj.getDepartamento());
                responseDataCms.setProvince(obj.getProvincia());
                responseDataCms.setDistrict(obj.getDistrito());
                responseDataCms.setAddress(obj.getDireccion());
                responseDataCms.setProductCode(obj.getProductPs());
                responseDataCms.setProductDescription(obj.getProductoDescripcion());
                responseData.add(responseDataCms);
            }

            List<TdpServiceClient> objAtis = legadoService.getParqueAtis(request.getDocumentType(), request.getDocumentNumber(),request.getId());
            for (TdpServiceClient obj : objAtis) {
                ServicesResponseData responseDataAtis = new ServicesResponseData();
                responseDataAtis.setParkType("ATIS");
                responseDataAtis.setPhone(obj.getProductService());
                responseDataAtis.setPsprincipal(obj.getProductPs());
                responseDataAtis.setPslinea(obj.getProductLinea());
                responseDataAtis.setPsinternet(obj.getProductInternet());
                responseDataAtis.setPstv(obj.getProductTv());
                responseDataAtis.setSourceType(obj.getProductOrigen());
                responseDataAtis.setDepartment(obj.getDepartamento());
                responseDataAtis.setProvince(obj.getProvincia());
                responseDataAtis.setDistrict(obj.getDistrito());
                responseDataAtis.setAddress(obj.getDireccion());
                responseDataAtis.setUbigeoCode(obj.getDepartamentoCode() + obj.getProvinciaCode() + obj.getDistritoCode());
                responseDataAtis.setProductDescription(obj.getProductoDescripcion());
                responseDataAtis.setStatus(obj.getServiceEstado());
                responseData.add(responseDataAtis);
            }
            response.setResponseCode("0");
            response.setResponseData(responseData);
            logger.info("Fin ReadServices Service");
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorApp").getCode());
            response.setResponseMessage(messageList.get("errorApp").getMessage());
            logger.error("Error ReadServices Service", e);
            return response;
        }

        return response;
    }

    private String getDescriptionByElement(String element, List<Parameter> cmssParameterList) {
        String description = "SIN VALOR";

        for (Parameter parameter : cmssParameterList) {
            if (element.equals(parameter.getElement())) {
                description = parameter.getStrValue();
            }
        }

        return description;
    }

    public Response<GetInfoResponseData> getInfo(GetInfoRequest request) throws Exception {
        logger.info("Inicio getInfo Service");

        Response<GetInfoResponseData> response = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("INFORMACION CLIENTE");
        LogVass.serviceResponseHashMap(logger, "getInfo", "messageList", messageList);
        //End--Code for call Service

        try {
            //ObjectMapper mapper = new ObjectMapper();
            //logger.info(mapper.writeValueAsString(request));
            GetInfoResponseData responseData = dao.getInfo(request);
            if (responseData != null) {
                response.setResponseCode("0");
                response.setResponseData(responseData);
            } else {
                response.setResponseCode(messageList.get("notDNI").getCode());
                response.setResponseMessage(messageList.get("notDNI").getMessage());
            }
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorApp").getCode());
            response.setResponseMessage(messageList.get("errorApp").getMessage());
            logger.error("Error getInfo Service", e);
            return response;
        }

        logger.info("Fin getInfo Service");
        return response;
    }

    public Boolean existsInParqueAtis(String documentType, String documentNumber, String productName, String
            department, String province, String district) {
        Boolean result = false;
        try {
            List<TdpServiceClient> tdpServiceClientAtis = legadoService.getParqueAtis(documentType, documentNumber,"");

            for (TdpServiceClient obj : tdpServiceClientAtis) {
                if (department.toLowerCase().equals(obj.getDepartamento().toLowerCase())
                        && province.toLowerCase().equals(obj.getProvincia().toLowerCase())
                        && district.toLowerCase().equals(obj.getDistrito().toLowerCase())) {

                    String productNameCatalog = daoProduct.getProductNameByProductCode(obj.getProductPs());
                    if (productNameCatalog == null) continue;

                    if (productName.toLowerCase().equals(productNameCatalog.toLowerCase())) {
                        result = true;
                        break;
                    }
                }
                //}
            }

            return result;
        } catch (Exception e) {
            return result;
        }
    }

    public Boolean existsInParqueCms(String documentType, String documentNumber, String productName, String
            department, String province, String district) {
        Boolean result = false;
        try {
            List<TdpServiceClient> tdpServiceClientCms = legadoService.getParqueCms(documentType, documentNumber,"");

            for (TdpServiceClient obj : tdpServiceClientCms) {
                String departmentNew = obj.getDepartamento();
                String provinceNew = obj.getProvincia();
                String districtNew = obj.getDistrito();
                if (department.toLowerCase().equals(departmentNew.toLowerCase())
                        && province.toLowerCase().equals(provinceNew.toLowerCase())
                        && district.toLowerCase().equals(districtNew.toLowerCase())) {

                    String productCMS = obj.getProductoDescripcion();

                    if (productName.toLowerCase().equals(productCMS.toLowerCase())) {
                        result = true;
                        break;
                    }
                }
            }

            return result;
        } catch (Exception e) {
            return result;
        }
    }

    /**
     * @param request
     * @return
     */
    public Response<String> validateDuplicateForCategory(DuplicateForCategoryRequest request) {

        logger.info("Inicio ValidateDuplicateForCategory Service.");
        Response<String> response = new Response<>();
        //Begin--Code for call Service

        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("DUPLICADOS");
        LogVass.serviceResponseHashMap(logger, "ValidateDuplicate", "messageList", messageList);
        //End--Code for call Service
        try {
            // Realizamos la equivalencia para Duplicados
            String codigoTipoDocumentoEquivalencia = parametersService.equivalenceDocument("DUPLICATE",  request.getDocumentType().toUpperCase());
            request.setDocumentType(codigoTipoDocumentoEquivalencia);

            String productName = request.getProductName();
            Boolean isSVA = false;
            if (productName.split("\\|").length > 1) isSVA = true;

            logger.info("ENTRA A LOGICA DE DUPLICADOS");
            DuplicateResponseVisor responseVisor = dao.getVisorForCategory(request, isSVA);
            LogVass.serviceResponseObject(logger, "ValidateDuplicateForCategory", "responseVisor", responseVisor);
            if (!isSVA) {
                logger.info("ENTRA A ALTA PURA");
                if (existsInParqueAtis(request.getDocumentType(), request.getDocumentNumber(), request.getProductName(),
                        request.getDepartmentName(), request.getProvinceName(), request.getDistrictName())) {
                    response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                    response.setResponseMessage(CustomerConstants.DUPLICATE_PRODUCT_ATIS_MESSAGE
                            .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                    request.getProductName()));
                    response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                    return response;
                }
                logger.info("NO EXISTE EN PARQUE ATIS");
                if (existsInParqueCms(request.getDocumentType(), request.getDocumentNumber(), request.getProductName(),
                        request.getDepartmentName(), request.getProvinceName(), request.getDistrictName())) {
                    response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                    response.setResponseMessage(CustomerConstants.DUPLICATE_PRODUCT_ATIS_MESSAGE
                            .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                    request.getProductName()));
                    response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                    return response;
                }
                logger.info("NO EXISTE EN PARQUE CMS");

                if (responseVisor != null) {
                    logger.info("SE OBTIENE LA DATA DE VISOR"+responseVisor.toString());
                    logger.info("SE ENCONTRO UNA VENTA ANTES DE 30 DIAS DIFERENTE A CAIDA CON SSTADO "+responseVisor.getRequestStatus());
                    String status="";
                    if(responseVisor.getRequestStatus().equals("INGRESADO")){

                        Order order = orderRepository.findOne(responseVisor.getVisorID());

                        if(order.getServiceType().equals("ATIS")){
                            status=legadoService.getEstadoAtisCodeWithTry(responseVisor.getOrderCode());
                        }else{
                            status=legadoService.getEstadoCmsCodeWithTry(responseVisor.getOrderCode());
                        }

                        logger.info("SE OBTUVO EL ESTADO "+status);

                        if(status==null){
                            response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                            response.setResponseMessage(CustomerConstants.DUPLICATE_PRODUCT_ATIS_MESSAGE
                                    .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                            request.getProductName()));
                            response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                        }else{
                            List<String> l_status = new ArrayList<>();
                            l_status.add("TE");l_status.add("CG");l_status.add("05");l_status.add("04");
                            if(!l_status.contains(status)){
                                response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                                response.setResponseMessage(CustomerConstants.DUPLICATE_PRODUCT_ATIS_MESSAGE
                                        .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                                request.getProductName()));
                                response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                            }else{
                                response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
                                response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
                            }
                        }
                    }else{
                        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                        response.setResponseMessage(CustomerConstants.DUPLICATE_PRODUCT_ATIS_MESSAGE
                                .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                        request.getProductName()));
                        response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                    }
                    return response;
                }else{
                    logger.info("NO SE ENCONTRO UNA VENTA ANTES DE 30 DIAS DIFERENTE A CAIDA");
                }

            }

            String requestStatus = responseVisor == null || responseVisor.getRequestStatus() == null ? null : responseVisor.getRequestStatus().toUpperCase();
            if (Arrays.asList(Constants.DUPLICATE_STATUS_FOLLOW, Constants.DUPLICATE_STATUS_PENDING, Constants.DUPLICATE_STATUS_IN_PROCESS, Constants.DUPLICATE_STATUS_EMPTY).contains(requestStatus)) {
                response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                if (responseVisor.getOrderCode() != null) {
                    response.setResponseMessage(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE
                            .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                    responseVisor.getProductName())
                            .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE,
                                    responseVisor.getRecordingDate()));
                } else {
                    response.setResponseMessage(CustomerConstants.DUPLICATE_PRODUCT_RESPONSE_MESSAGE
                            .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                    responseVisor.getProductName())
                            .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE,
                                    responseVisor.getRecordingDate()));
                }
                response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
            } else if (responseVisor == null || Constants.DUPLICATE_STATUS_DOWN.equalsIgnoreCase(requestStatus)) {
                response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
                response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
            } else if (Constants.DUPLICATE_STATUS_JOINED.equalsIgnoreCase(requestStatus) && responseVisor.getOrderCode() != null && !isSVA) {
                String estadoApi = "";
                String servicio = "";
                if (responseVisor.getOrderCode().length() == Constants.DUPLICATE_ORDER_CODE_CMS_1 || responseVisor.getOrderCode().length() == Constants.DUPLICATE_ORDER_CODE_CMS_2) {

                    servicio = "CMSR";
                    estadoApi = legadoService.getEstadoCmsCodeWithTry(responseVisor.getOrderCode());

                } else if (responseVisor.getOrderCode().length() == Constants.DUPLICATE_ORDER_CODE_ATIS) {

                    servicio = "ATIS";
                    estadoApi = legadoService.getEstadoAtisCodeWithTry(responseVisor.getOrderCode());

                } else {
                    response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
                    response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
                    logger.info("Fin ValidateDuplicateForCategory Service.");
                    return response;
                }

                if (estadoApi == null || estadoApi.equals("")) {
                    response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                    response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);

                    TdpErrorMessageServiceEntities errorMessage = tdpErrorMessageServiceRepository.findByServiceAndServiceKey("DUPLICADOS", "atiscmsdown");
                    String message = errorMessage.getResponseMessage();

                    response.setResponseMessage(message);
                    return response;
                } else if (estadoApi.equals(Constants.DUPLICATE_ATIS_SUS_DUE)) {
                    response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                    response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                    response.setResponseMessage(CustomerConstants.DUPLICATE_RESPONSE_SUS_DUE_MESSAGE);
                    return response;
                } else if (estadoApi.equals(Constants.DUPLICATE_ATIS_SUS_APC)) {
                    response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                    response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                    response.setResponseMessage(CustomerConstants.DUPLICATE_RESPONSE_SUS_APC_MESSAGE);
                    return response;
                }
                if (estadoApi.equals(Constants.DUPLICATE_ATIS_SUS)) {
                    response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                    response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                    response.setResponseMessage(CustomerConstants.DUPLICATE_RESPONSE_SUS_MESSAGE);
                    return response;
                } else {

                    List<Parameters> estadosParamStopList = parametersRepository.findByDomainAndCategoryAndStrfilter("TRANSLATE", servicio, "STOP");
                    List<String> estadosStopList = new ArrayList<>();
                    for (Parameters obj : estadosParamStopList) estadosStopList.add(obj.getElement());

                    if (/*Arrays.asList(
                        Constants.DUPLICATE_CMS_NEW,
                        Constants.DUPLICATE_CMS_PENDING,
                        Constants.DUPLICATE_CMS_COMPLETED,

                        Constants.DUPLICATE_ATIS_TERMINATE,
                        Constants.DUPLICATE_ATIS_PENDING,
                        Constants.DUPLICATE_ATIS_PENDING_APROVE,
                        //Constants.DUPLICATE_ATIS_CANCELED,
                        Constants.DUPLICATE_ATIS_PENDING_VALIDATE,
                        Constants.DUPLICATE_ATIS_CONFIGURED,
                        Constants.DUPLICATE_ATIS_FINALIZED,
                        //Constants.DUPLICATE_ATIS_ERROR,
                        Constants.DUPLICATE_ATIS_ACTIVE)*/
                            estadosStopList.contains(estadoApi)) {
                        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                        if (responseVisor.getOrderCode() != null) {
                            response.setResponseMessage(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE
                                    .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                            responseVisor.getProductName())
                                    .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE,
                                            responseVisor.getRecordingDate()));
                        } else {
                            response.setResponseMessage(CustomerConstants.DUPLICATE_PRODUCT_RESPONSE_MESSAGE
                                    .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                            responseVisor.getProductName())
                                    .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE,
                                            responseVisor.getRecordingDate()));
                        }
                        response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                    } else {
                        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
                        response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
                    }
                }
            } else if (Constants.DUPLICATE_STATUS_JOINED.equalsIgnoreCase(requestStatus) && isSVA) {

                if (responseVisor.getProductName().equals(Constants.DUPLICATE_SVA_DECO)) {
                    response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
                    response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
                } else {
                    response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
                    response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_ERROR);
                    response.setResponseMessage(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE
                            .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME,
                                    responseVisor.getProductName())
                            .replace(CustomerConstants.DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE,
                                    responseVisor.getRecordingDate()));
                }

            } else {
                response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
                response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
            }
        } catch (Exception e) {
            //response.setResponseCode(messageList.get("errorAtisCms").getCode());
            //response.setResponseMessage(messageList.get("errorAtisCms").getMessage());
            logger.error("Error ValidateDuplicateForCategory Service.", e);
            logger.info("Fin ValidateDuplicateForCategory Service.");
            return response;
        }/* catch (Exception e) {
            response.setResponseCode(messageList.get("errorApp").getCode());
            response.setResponseMessage(messageList.get("errorApp").getMessage());
            logger.error("Error ValidateDuplicateForCategory Service.", e);
            logger.info("Fin ValidateDuplicateForCategory Service.");
            return response;
        }*/
        logger.info("Fin ValidateDuplicateForCategory Service.");
        return response;
    }

    /**
     * @param request
     * @return
     */
    public Response<List<ServicesWebResponseData>> readServicesWeb(ServicesRequest request) {
        logger.info("Inicio ReadServicesWeb Service");

        Response<List<ServicesWebResponseData>> response = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("PARQUE");
        LogVass.serviceResponseHashMap(logger, "PARQUE", "messageList", messageList);
        //End--Code for call Service

        List<String> codes = new ArrayList<String>();

        try {
            if (noPermitirRUC(request.getDocumentType(), request.getDocumentNumber())) {
                response.setResponseCode("1");
                response.setResponseMessage(expertoRuc20);
                return response;
            }

            List<ServicesWebResponseData> responseData = new ArrayList<>();

            /* Búsqueda de PARQUE CMS */
            List<TdpServiceClient> objCms = legadoService.getParqueCms(request.getDocumentType(), request.getDocumentNumber(), request.getId());
            for (TdpServiceClient obj : objCms) {
                ServicesWebResponseData responseDataCms = new ServicesWebResponseData();
                responseDataCms.setParkType(LegadosConstants.PARQUE_CMS);
                responseDataCms.setSubscriber(obj.getClientCms());
                responseDataCms.setServiceCode(obj.getProductService());
                responseDataCms.setStatus(obj.getServiceEstado());
                responseDataCms.setSourceType(obj.getProductOrigen());
                responseDataCms.setCoordinateX("" + obj.getCoordenadaX());
                responseDataCms.setCoordinateY("" + obj.getCoordenadaY());
                responseDataCms.setDepartment(obj.getDepartamentoCode() + " " + obj.getDepartamento());
                responseDataCms.setProvince(obj.getProvinciaCode() + " " + obj.getProvincia());
                responseDataCms.setDistrict(obj.getDistritoCode() + " " + obj.getDistrito());
                responseDataCms.setAddress(obj.getDireccion());
                responseDataCms.setProductCode(obj.getProductPs());
                responseDataCms.setProductDescription(obj.getProductoDescripcion());
                responseData.add(responseDataCms);
            }
            /* Búsqueda de PARQUE CMS */

            /* Búsqueda de PARQUE ATIS */
            List<TdpServiceClient> objAtis = legadoService.getParqueAtis(request.getDocumentType(), request.getDocumentNumber(), request.getId());
            for (TdpServiceClient obj : objAtis) {
                ServicesWebResponseData responseDataAtis = new ServicesWebResponseData();
                responseDataAtis.setParkType(LegadosConstants.PARQUE_ATIS);
                responseDataAtis.setPhone(obj.getProductService());
                responseDataAtis.setPsprincipal(obj.getProductPs());
                responseDataAtis.setPslinea(obj.getProductLinea());
                responseDataAtis.setPsinternet(obj.getProductInternet());
                responseDataAtis.setPstv(obj.getProductTv());
                responseDataAtis.setSourceType(obj.getProductOrigen());
                responseDataAtis.setDepartment(obj.getDepartamento());
                responseDataAtis.setProvince(obj.getProvincia());
                responseDataAtis.setDistrict(obj.getDistrito());
                responseDataAtis.setAddress(obj.getDireccion());
                responseDataAtis.setUbigeoCode(obj.getDepartamentoCode() + obj.getProvinciaCode() + obj.getDistritoCode());
                responseDataAtis.setProductDescription(obj.getProductoDescripcion());
                responseDataAtis.setStatus(obj.getServiceEstado());
                responseData.add(responseDataAtis);
            }
            /* Búsqueda de PARQUE ATIS */

            OffersApiCalculadoraArpuRequestBody apiCalculadoraArpuRequestBody = new OffersApiCalculadoraArpuRequestBody();
            apiCalculadoraArpuRequestBody.setDocumento(request.getDocumentNumber());

            ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalc = null;
            List<OffersApiCalculadoraArpuResponseBody> lstObjCalculadora = null;
            try {
                objCalc = getCalculadora(apiCalculadoraArpuRequestBody, request.getId());
                lstObjCalculadora = (List<OffersApiCalculadoraArpuResponseBody>) objCalc.getBodyOut();
            } catch (Exception e) {
                lstObjCalculadora = new ArrayList<>();
            }

            if (lstObjCalculadora != null && !lstObjCalculadora.isEmpty()) {
                for (ServicesWebResponseData objParque : responseData) {
                    for (OffersApiCalculadoraArpuResponseBody obj : lstObjCalculadora) {
                        if (objParque.getParkType().equals("CMS")
                                && objParque.getServiceCode().equals(obj.getServicio())
                                || (objParque.getParkType().equals("ATIS")
                                && objParque.getPhone().equals(obj.getTelefono()))) {
                            objParque.setProductDescription(obj.getPaqueteOrigen());
                            objParque.setNumDecos(obj.getCantidadDecos());
                            objParque.setRentaTotal(obj.getRentaTotal());
                            objParque.setVelInter(obj.getInternet());
                            break;
                        }
                    }
                }
            }

            response.setResponseCode("0");
            response.setResponseData(responseData);
            logger.info("Fin readServicesWeb");
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorApp").getCode());
            response.setResponseMessage(messageList.get("errorApp").getMessage());
            logger.info("Error readServicesWeb", e);
            return response;
        }

        return response;
    }

    public Response<CustomerScoringResponse> scoring(CustomerScoringRequest request) throws ClientException {
        logger.info("Inicio Scoring Service");
        Response<CustomerScoringResponse> response = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("POLITICA COMERCIAL");
        LogVass.serviceResponseHashMap(logger, "scoring", "messageList", messageList);
        //End--Code for call Service
        try {
            // Equivalencias de tipos documento en Equifax
            String documentType = request.getTipoDeDocumento().toUpperCase();
            String documentNumber = request.getNumeroDeDocumento();

            String requestScoringDiario = "";
            String canal = request.getCanal() == null || request.getCanal().equalsIgnoreCase("") ? "" : request.getCanal();

            requestScoringDiario = customerRepository.getResponseScoring(documentType,documentNumber,request.getUbigeo(),canal);


            // Realizamos la equivalencia para ParqueATIS
            String codigoTipoDocumentoEquivalencia = parametersService.equivalenceDocument("EQUIFAX", documentType);
            /*
            Integer maxService = 3;
            try {
                Parameters parametersService = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "POLICY_PARAMETER", "max_amount_services");
                maxService = parametersService.getStrValue() == null || parametersService.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(parametersService.getStrValue());
            } catch (Exception parameter) {

            }

            List<String> operacionComercial = new ArrayList<>();
            //operacionComercial.add("ALTA PURA");
            //operacionComercial.add("ALT");
            String[] operacionComercialList = scoringOperacionComercial.split(",");
            for (String oc : operacionComercialList) {
                operacionComercial.add(oc);
            }

            List<TdpVisor> tdpVisorList = tdpVisorRepository.findAllByTipoDocumentoAndDni(documentType, documentNumber, operacionComercial);
            LogVass.serviceResponseArray(logger, "scoring", "tdpVisorList", tdpVisorList);

            if (tdpVisorList == null) {
                tdpVisorList = new ArrayList<>();
            } else {
                List<TdpVisor> tdpVisorListTmp = new ArrayList<>();
                for (TdpVisor tdpVisor : tdpVisorList) {
                    if (tdpVisor.getCodigoPedido() == null) {
                        tdpVisorListTmp.add(tdpVisor);
                    } else {
                        String estadoApi = "";
                        if (tdpVisor.getCodigoPedido().length() == Constants.DUPLICATE_ORDER_CODE_CMS_1 || tdpVisor.getCodigoPedido().length() == Constants.DUPLICATE_ORDER_CODE_CMS_2) {
                            TdpServicePedido tdoTdpServicePedido = legadoService.getEstadoCms(tdpVisor.getCodigoPedido());
                            estadoApi = tdoTdpServicePedido == null ? null : tdoTdpServicePedido.getPedidoEstadoCode();
                            if (estadoApi == null || estadoApi.equalsIgnoreCase("") || scoringEstadoStopCms.indexOf("|" + estadoApi + "|") < 0) {
                                tdpVisorListTmp.add(tdpVisor);
                            }
                        } else if (tdpVisor.getCodigoPedido().length() == Constants.DUPLICATE_ORDER_CODE_ATIS) {
                            TdpServicePedido tdoTdpServicePedido = legadoService.getEstadoAtis(tdpVisor.getCodigoPedido());
                            estadoApi = tdoTdpServicePedido == null ? null : tdoTdpServicePedido.getPedidoEstadoCode();
                            if (estadoApi == null || estadoApi.equalsIgnoreCase("") || scoringEstadoStopAtis.indexOf("|" + estadoApi + "|") < 0) {
                                tdpVisorListTmp.add(tdpVisor);
                            }
                        } else {
                            tdpVisorListTmp.add(tdpVisor);
                        }
                    }
                }
                tdpVisorList = tdpVisorListTmp;
            }

            List<TdpServiceClient> tdpServiceClientList = new ArrayList<>();
            List<TdpServiceClient> tdpServiceClientAtis = legadoService.getParqueAtis(documentType, documentNumber);
            List<TdpServiceClient> tdpServiceClientCms = legadoService.getParqueCms(documentType, documentNumber);

            if (tdpServiceClientAtis == null) {
                tdpServiceClientAtis = new ArrayList<>();
            }
            if (tdpServiceClientCms == null) {
                tdpServiceClientCms = new ArrayList<>();
            }

            List<TdpServiceClient> tdpServiceClientListTmp = new ArrayList<>();
            for (TdpServiceClient serviceType : tdpServiceClientAtis) {
                if (Arrays.asList(
                        CustomerConstants.PARQUE_ATIS_ACTIVE,
                        CustomerConstants.PARQUE_ATIS_SUSPENDIDO
                ).contains(serviceType.getServiceEstadoCode()))
                    tdpServiceClientListTmp.add(serviceType);
            }

            for (TdpServiceClient serviceType : tdpServiceClientCms) {
                if (Arrays.asList(
                        CustomerConstants.PARQUE_CMS_ACTIVE,
                        CustomerConstants.PARQUE_CMS_SUSPENDIDO
                ).contains(serviceType.getServiceEstadoCode()))
                    tdpServiceClientListTmp.add(serviceType);
            }
            tdpServiceClientList = tdpServiceClientListTmp;

            Integer cantParque = tdpServiceClientList.size();
            Integer cantVisor = tdpVisorList.size();
            Integer cantService = cantParque + cantVisor;
            if (cantService >= maxService) {
                response.setResponseCode(messageList.get("maxServices").getCode());
                response.setResponseMessage(messageList.get("maxServices").getMessage().replace("[PARQUE]", "" + cantParque).replace("[VISOR]", "" + cantVisor));
                return response;
            }
            */
            //Sprint 22 - Creamos Id de Venta
            String orderId = "";
            if (VentaFijaContextHolder.getContext().getServiceCallEvent().getSourceApp() != null && !VentaFijaContextHolder.getContext().getServiceCallEvent().getSourceApp().equalsIgnoreCase("APPVF")) {
                orderId = generateWebOrderId();
            } else {
                orderId = VentaFijaContextHolder.getContext().getServiceCallEvent().getOrderId() != null ? VentaFijaContextHolder.getContext().getServiceCallEvent().getOrderId() : "ERROR";
            }

            if (noPermitirRUC(documentType, documentNumber) || (requestScoringDiario != null && !requestScoringDiario.equalsIgnoreCase(""))) { //En caso sea RUC Sacamos la data de un response de BD
                ApiResponse<OffersApiExpertoResponseBody> objExperto = null;
                if(!documentType.equalsIgnoreCase("RUC") && (requestScoringDiario != null && !requestScoringDiario.equalsIgnoreCase(""))){
                    objExperto = expertoService.scoringRucData(requestScoringDiario, orderId, "OTROS");
                }
                else{
                    objExperto = expertoService.scoringRucData(expertoRuc20Data, orderId, "RUC");
                }
                List<OperacionComercial> result = new ArrayList<>();
                for (OffersApiExpertoResponseBodyOperacion operacion : objExperto.getBodyOut().getOperacionComercial()) {
                    List<OperacionComercialDetalle> detalle = new ArrayList<>();
                    if (operacion.getDetalle() != null) {
                        for (OffersApiExpertoResponseBodyOperacionDetalle expertoDetalle : operacion.getDetalle()) {
                            OperacionComercialDetalle det = new OperacionComercialDetalle(expertoDetalle.getCodProd(), expertoDetalle.getDesProd(), expertoDetalle.getCuotaFin(), expertoDetalle.getMesesFin(), expertoDetalle.getPagoAdelantado(), expertoDetalle.getNroDecosAdic());
                            detalle.add(det);
                        }
                    }

                    OperacionComercial op = new OperacionComercial(operacion.getCodOpe(), operacion.getRenta().replace("S/.", "").replace(" ", ""), operacion.getSegmento(), operacion.getContadorDetalle(), detalle);
                    result.add(op);
                }

                CustomerScoringResponse responseBody = new CustomerScoringResponse(result);

                responseBody.setAccion(objExperto.getBodyOut().getAccion());
                responseBody.setCodConsulta(objExperto.getBodyOut().getCodConsulta());
                responseBody.setDeudaAtis(objExperto.getBodyOut().getDeudaAtis());
                responseBody.setDeudaCms(objExperto.getBodyOut().getDeudaCms());

                responseBody.setOrderId(orderId);

                response.setResponseData(responseBody);
                response.setResponseCode("0");
                response.setResponseMessage("DATA_RUC");
            }
            else{
            ApiResponse<OffersApiExpertoResponseBody> objExperto = null;
            OffersApiExpertoRequestBody body = new OffersApiExpertoRequestBody();
            body.setApellidoMaterno(request.getApellidoMaterno());
            body.setApellidoPaterno(request.getApellidoPaterno());
            body.setCodVendedor(request.getCodVendedor());
            body.setDepartamento(request.getDepartamento());
            body.setDistrito(request.getDistrito());
            body.setNombres(request.getNombres());
            body.setNumeroDeDocumento(request.getNumeroDeDocumento());
            body.setProvincia(request.getProvincia());
            body.setTipoDeDocumento(codigoTipoDocumentoEquivalencia);
            body.setUbigeo(request.getUbigeo());
            body.setZonal(request.getZonal());

            for(int cont = 1; cont <= 3; cont ++){
                try {
                    objExperto = expertoService.scoring(body,orderId);
                    break;
                } catch (ClientException e) {
                    if(cont == 3){
                        logger.error("Error Scoring Service. Scoring.", e);
                        response.setResponseCode("0");
                        response.setResponseData(getResponseOnScoringError(orderId));
                        return response;
                    }

                }
            }


            if (Constants.RESPONSE.equals(objExperto.getHeaderOut().getMsgType())) {
                List<OperacionComercial> result = new ArrayList<>();
                for (OffersApiExpertoResponseBodyOperacion operacion : objExperto.getBodyOut().getOperacionComercial()) {
                    List<OperacionComercialDetalle> detalle = new ArrayList<>();
                    if (operacion.getDetalle() != null) {
                        for (OffersApiExpertoResponseBodyOperacionDetalle expertoDetalle : operacion.getDetalle()) {
                            OperacionComercialDetalle det = new OperacionComercialDetalle(expertoDetalle.getCodProd(), expertoDetalle.getDesProd(), expertoDetalle.getCuotaFin(), expertoDetalle.getMesesFin(), expertoDetalle.getPagoAdelantado(), expertoDetalle.getNroDecosAdic());
                            detalle.add(det);
                        }
                    }

                    OperacionComercial op = new OperacionComercial(operacion.getCodOpe(), operacion.getRenta().replace("S/.", "").replace(" ", ""), operacion.getSegmento(), operacion.getContadorDetalle(), detalle);
                    result.add(op);
                }

                CustomerScoringResponse responseBody = new CustomerScoringResponse(result);

                responseBody.setAccion(objExperto.getBodyOut().getAccion());
                responseBody.setCodConsulta(objExperto.getBodyOut().getCodConsulta());
                responseBody.setDeudaAtis(objExperto.getBodyOut().getDeudaAtis());
                responseBody.setDeudaCms(objExperto.getBodyOut().getDeudaCms());

                responseBody.setOrderId(orderId);

                response.setResponseCode("0");
                response.setResponseData(responseBody);

                Gson objJson = new Gson();

                String responseBodyScoring = objJson.toJson(objExperto);

                responseBodyScoring = toPrettyFormat(responseBodyScoring);

                if(documentType != null && documentNumber != null && request.getUbigeo() != null && responseBodyScoring != "")
                customerRepository.insertResponseScoring(documentType,documentNumber,request.getUbigeo(),responseBodyScoring,canal);

            } else {
                response.setResponseCode("0");
                response.setResponseData(getResponseOnScoringError(orderId));
            }

            }
        } catch (Exception e) {
            logger.error("Error Scoring Service.", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
        }

        logger.info("Fin Scoring Service");
        return response;
    }

    public static String toPrettyFormat(String jsonString)
    {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(jsonString).getAsJsonObject();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String prettyJson = gson.toJson(json);

        return prettyJson;
    }

    public CustomerScoringResponse getResponseOnScoringError(String orderId) {

        List<OperacionComercial> result = new ArrayList<>();
        List<OperacionComercialDetalle> detalle = new ArrayList<>();

        List<Map<String, String>> mapList = dao.getDefaultItemsCatalog();
        for (Map<String, String> map : mapList) {
            String productType = (map.get("producttype") != null) ? map.get("producttype") : "SIN DESCRIPCIÓN";
            String cashPrice = (map.get("cashprice") != null && !map.get("cashprice").equals("")) ? map.get("cashprice") : "0.00";
            String codProduct = (map.get("prodcategorycode") != null && !map.get("prodcategorycode").equals("")) ? map.get("prodcategorycode") : "000";

            if (Integer.parseInt(codProduct) > 200) continue;

            OperacionComercialDetalle det = new OperacionComercialDetalle(codProduct,
                    productType, (float) 0.0, 0,
                    Float.parseFloat(cashPrice), 0);
            detalle.add(det);
        }

        OperacionComercial op = new OperacionComercial("ALTA PURA", "700", "C2", 6, detalle);
        result.add(op);

        List<OperacionComercialDetalle> detalle2 = new ArrayList<>();
        OperacionComercial op2 = new OperacionComercial("ALTA COMPONENTE", "0", "C2", 0, detalle2);
        result.add(op2);

        CustomerScoringResponse responseBody = new CustomerScoringResponse(result);

        responseBody.setAccion("");
        responseBody.setCodConsulta("N/A");
        responseBody.setDeudaAtis("0");
        responseBody.setDeudaCms("0");
        responseBody.setOrderId(orderId);
        return responseBody;
    }

    private ApiResponse<OffersApiCalculadoraArpuResponseBody> getCalculadora(OffersApiCalculadoraArpuRequestBody
                                                                                     apiCalculadoraArpuRequestBody,
                                                                             String orderId)
            throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getFfttCalculadoraConsultarArpuUri())
                .setApiId(apiHeaderConfig.getFfttCalculadoraConsultarArpuApiId())
                .setApiSecret(apiHeaderConfig.getFttCalculadoraConsultarArpuApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_AVVE)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_AVVE).build();
        CalculadoraClient calculadoraClient = new CalculadoraClient(config);
        ClientResult<ApiResponse<OffersApiCalculadoraArpuResponseBody>> result = calculadoraClient.post(apiCalculadoraArpuRequestBody);
        ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalculadora = result.getResult();

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
        result.getEvent().setOrderId(orderId);
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }

        return objCalculadora;
    }

    /**
     * @param request
     * @return
     */
    public Response<ContractSalesConditionResponseData> readSaleConditions(ContractWebRequest request) {
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("CONDICIONES VENTA");
        LogVass.serviceResponseHashMap(logger, "GetSalesCondition", "messageList", messageList);
        //End--Code for call Service
        //verificamos si alguno de los svas es deco smart... para obligar a ingresar correo
        boolean hasDecoSmart = false;
        if (request.getSva() != null) {
            for (ContractWebRequestSva sva : request.getSva()) {
                if ("DSHD".equals(sva.getCode())) {
                    hasDecoSmart = true;
                    break;
                }
            }
        }

        //PREGUNTA: APLICA A MIGRAS Y ALTAS NUEVAS??..... verificamos si es que se eligio tipo producto duo o trio (tipo producto 3 o 4) para mostrar condiciones adicionales (caso filtro parental)
        boolean hasFiltroParental = false;

        if ("3".equals(request.getSelectedOffering().getProductTypeCode()) || "4".equals(request.getSelectedOffering().getProductTypeCode())) {
            if (request.getSelectedOffering().getCommercialOperation() != null) {
                //Si es que getCommercialOperation() es null entonces es migra con solo sva... para tal caso no aplica el filtro parental
                hasFiltroParental = true;
            }
        }

        //si es que se solicito deco smart entonces se debe mostrar condicion adicional
        Response<ContractSalesConditionResponseData> response = new Response<>();
        try {
            List<ContractResponseParameter> condicionesContrato = dao.readSaleConditions(request);

            List<ContractResponseParameter> condicionesFinal = new ArrayList<ContractResponseParameter>();

            for (ContractResponseParameter condition : condicionesContrato) {
                if ("FILTROWEBPARENTAL".equals(condition.getCategory())) {
                    if (hasFiltroParental) {
                        condicionesFinal.add(condition);
                    }
                } else {
                    condicionesFinal.add(condition);
                }
            }
            ContractSalesConditionResponseData responseData = new ContractSalesConditionResponseData();
            responseData.setConditions(condicionesFinal);
            responseData.setDecoSmart(hasDecoSmart);
            responseData.setFiltroParental(hasFiltroParental);

            response.setResponseCode("0");
            response.setResponseData(responseData);
            logger.info("Fin readSalesConditions");

        } catch (Exception e) {
            logger.info("Error Contract Service", e);
            response.setResponseCode(messageList.get("errorSalesCondition").getCode());
            response.setResponseMessage(messageList.get("errorSalesCondition").getMessage());

        }

        return response;
    }

    public Response<Integer> actualizaContactos(ContractWebRequestCustomer request) {
        logger.info("Inicio actualizaContactos Service");
        Response<Integer> responseData = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("INFORMACION CLIENTE");
        LogVass.serviceResponseHashMap(logger, "getInfo", "messageList", messageList);
        //End--Code for call Service

        try {

            Integer rpta = dao.actualizarContactos(request);
            responseData.setResponseData(rpta);
            LogVass.serviceResponseObject(logger, "actualizaContactos", "responseData", responseData);

        } catch (Exception e) {
            responseData.setResponseCode(messageList.get("errorUpdateCustomer").getCode());
            responseData.setResponseMessage(messageList.get("errorUpdateCustomer").getMessage());
            logger.error("Error actualizaContactos Service", e);
            logger.info("Fin actualizaContactos Service");
            return responseData;
        }
        logger.info("Fin actualizaContactos Service");
        return responseData;
    }

    public Response<Integer> updateContactTdpOrder(UpdTelfTdpOrderResponseData request) {
        logger.info("Inicio updateContactTdpOrder Service");
        Response<Integer> responseData = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("INFORMACION CLIENTE/UPDATE DATOS DE VENTA");
        LogVass.serviceResponseHashMap(logger, "getInfo", "messageList", messageList);
        //End--Code for call Service

        try {
            Integer rpta = dao.updateContactTdpOrder(request);
            Integer rpta1 = dao.updateContactTdpVisor(request);
            if(rpta1 == rpta){
                responseData.setResponseData(rpta);
                LogVass.serviceResponseObject(logger, "updateContactTdpOrder", "responseData", responseData);
            }

            TdpVisor orden = tdpVisorRepository.findOneByIdVisor(request.getOrder_id());

            if (orden.getEstadoSolicitud().equals("SINFINALIZAR")){
                orden.setEstadoSolicitud("PENDIENTE");
                tdpVisorRepository.save(orden);
            }else if(orden.getEstadoSolicitud().equals("SINFINALIZAR-OFFLINE")){
                orden.setEstadoSolicitud("OFFLINE");
                tdpVisorRepository.save(orden);
            }

        } catch (Exception e) {
            responseData.setResponseCode(messageList.get("errorUpdateCustomer").getCode());
            responseData.setResponseMessage(messageList.get("errorUpdateCustomer").getMessage());
            logger.error("Error actualizaContactos Service", e);
            logger.info("Fin actualizaContactos Service");
            return responseData;
        }
        logger.info("Fin actualizaContactos Service");
        return responseData;
    }

    /**
     * @param request
     * @return
     */
    public Response<List<ServicesWebResponseData>> readOutServicesWeb(OutServicesRequest request) {
        logger.info("Inicio readOutServicesWeb Service");

        Response<List<ServicesWebResponseData>> response = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("PARQUE PLANTA OUT");
        LogVass.serviceResponseHashMap(logger, "PARQUE PLANTA OUT", "messageList", messageList);
        //End--Code for call Service

        List<String> codes = new ArrayList<String>();

        try {
            List<ServicesWebResponseData> responseData = new ArrayList<>();

            TdpCatalogOutPlanta tdpCatalogOutPlanta = tdpCatalogOutPlantaRepository.findByPhoneOrCms(request.getPhoneOrCms());

            if (tdpCatalogOutPlanta!=null){
                String documentType = tdpCatalogOutPlanta.getTipo_documento();
                String documentNumber = tdpCatalogOutPlanta.getNumero_documento();

            /* Búsqueda de PARQUE CMS */
                List<TdpServiceClient> objCms = legadoService.getParqueCms(documentType, documentNumber,"");
                for (TdpServiceClient obj : objCms) {
                    if(obj.getProductService().equalsIgnoreCase(request.getPhoneOrCms())){
                        ServicesWebResponseData responseDataCms = new ServicesWebResponseData();
                        responseDataCms.setParkType(LegadosConstants.PARQUE_CMS);
                        responseDataCms.setSubscriber(obj.getClientCms());
                        responseDataCms.setServiceCode(obj.getProductService());
                        responseDataCms.setStatus(obj.getServiceEstado());
                        responseDataCms.setSourceType(obj.getProductOrigen());
                        responseDataCms.setCoordinateX("" + obj.getCoordenadaX());
                        responseDataCms.setCoordinateY("" + obj.getCoordenadaY());
                        responseDataCms.setDepartment(obj.getDepartamentoCode() + " " + obj.getDepartamento());
                        responseDataCms.setProvince(obj.getProvinciaCode() + " " + obj.getProvincia());
                        responseDataCms.setDistrict(obj.getDistritoCode() + " " + obj.getDistrito());
                        responseDataCms.setAddress(obj.getDireccion());
                        responseDataCms.setProductCode(obj.getProductPs());
                        responseDataCms.setProductDescription(obj.getProductoDescripcion());
                        responseData.add(responseDataCms);
                        break;
                    }
                }
            /* Búsqueda de PARQUE CMS */

            /* Búsqueda de PARQUE ATIS */
                if(responseData.isEmpty()){
                    List<TdpServiceClient> objAtis = legadoService.getParqueAtis(documentType, documentNumber,"");
                    for (TdpServiceClient obj : objAtis) {
                        if(obj.getProductService().equalsIgnoreCase(request.getPhoneOrCms())){
                            ServicesWebResponseData responseDataAtis = new ServicesWebResponseData();
                            responseDataAtis.setParkType(LegadosConstants.PARQUE_ATIS);
                            responseDataAtis.setPhone(obj.getProductService());
                            responseDataAtis.setPsprincipal(obj.getProductPs());
                            responseDataAtis.setPslinea(obj.getProductLinea());
                            responseDataAtis.setPsinternet(obj.getProductInternet());
                            responseDataAtis.setPstv(obj.getProductTv());
                            responseDataAtis.setSourceType(obj.getProductOrigen());
                            responseDataAtis.setDepartment(obj.getDepartamento());
                            responseDataAtis.setProvince(obj.getProvincia());
                            responseDataAtis.setDistrict(obj.getDistrito());
                            responseDataAtis.setAddress(obj.getDireccion());
                            responseDataAtis.setUbigeoCode(obj.getDepartamentoCode() + obj.getProvinciaCode() + obj.getDistritoCode());
                            responseDataAtis.setProductDescription(obj.getProductoDescripcion());
                            responseDataAtis.setStatus(obj.getServiceEstado());
                            responseData.add(responseDataAtis);
                            break;
                        }
                    }
                }

            /* Búsqueda de PARQUE ATIS */
                if(!responseData.isEmpty()){
                    OffersApiCalculadoraArpuRequestBody apiCalculadoraArpuRequestBody = new OffersApiCalculadoraArpuRequestBody();
                    apiCalculadoraArpuRequestBody.setDocumento(documentNumber);

                    ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalc = null;
                    List<OffersApiCalculadoraArpuResponseBody> lstObjCalculadora = null;
                    try {
                        objCalc = getCalculadora(apiCalculadoraArpuRequestBody, "");
                        lstObjCalculadora = (List<OffersApiCalculadoraArpuResponseBody>) objCalc.getBodyOut();
                    } catch (Exception e) {
                        lstObjCalculadora = new ArrayList<>();
                    }

                    if (lstObjCalculadora != null && !lstObjCalculadora.isEmpty()) {
                        for (ServicesWebResponseData objParque : responseData) {
                            for (OffersApiCalculadoraArpuResponseBody obj : lstObjCalculadora) {
                                if (objParque.getParkType().equals("CMS")
                                        && objParque.getServiceCode().equals(obj.getServicio())
                                        || (objParque.getParkType().equals("ATIS")
                                        && objParque.getPhone().equals(obj.getTelefono()))) {
                                    objParque.setProductDescription(obj.getPaqueteOrigen());
                                    objParque.setNumDecos(obj.getCantidadDecos());
                                    objParque.setRentaTotal(obj.getRentaTotal());
                                    objParque.setVelInter(obj.getInternet());
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            response.setResponseCode("0");
            response.setResponseData(responseData);
            logger.info("Fin readOutServicesWeb");
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorApp").getCode());
            response.setResponseMessage(messageList.get("errorApp").getMessage());
            logger.info("Error readOutServicesWeb", e);
            return response;
        }

        return response;
    }

    private boolean noPermitirRUC(String tipoDocument, String numberDocument) {
        if (tipoDocument != null && tipoDocument.equalsIgnoreCase("RUC")) {
            if (numberDocument != null) {
                if (numberDocument.substring(0, 2).equalsIgnoreCase("20")
                        || numberDocument.substring(0, 2).equalsIgnoreCase("15")
                        || numberDocument.substring(0, 2).equalsIgnoreCase("17")) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private String generateWebOrderId() {
        RandomString randomString = new RandomString(19);
        String orderId = "-" + randomString.nextString();
        return orderId;
    }
}
