package pe.com.tdp.ventafija.microservices.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ResetPasswordRequest {

    private String dni;
    private String codAtis;
    private String imei;
    private String pwd;
    private String repeatPwd;
    @JsonIgnore
    private boolean flagReset;
    @JsonIgnore
    private String phone;
    @JsonIgnore
    private String resetPwd;
    @JsonIgnore
    private String previousPwd;

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCodAtis() {
        return codAtis;
    }

    public void setCodAtis(String codAtis) {
        this.codAtis = codAtis;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getRepeatPwd() {
        return repeatPwd;
    }

    public void setRepeatPwd(String repeatPwd) {
        this.repeatPwd = repeatPwd;
    }

    public boolean isFlagReset() {
        return flagReset;
    }

    public void setFlagReset(boolean flagReset) {
        this.flagReset = flagReset;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getResetPwd() {
        return resetPwd;
    }

    public void setResetPwd(String resetPwd) {
        this.resetPwd = resetPwd;
    }

    public String getPreviousPwd() {
        return previousPwd;
    }

    public void setPreviousPwd(String previousPwd) {
        this.previousPwd = previousPwd;
    }
}
