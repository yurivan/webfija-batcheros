package pe.com.tdp.ventafija.microservices.service.order;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import pe.com.tdp.ventafija.microservices.common.clients.Cip.GenerarCip;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.ThirdpartyPagoEfectivoClient;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.connection.Firebase;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.dao.OrderRepository;
import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.domain.dto.HDECResponse;
import pe.com.tdp.ventafija.microservices.common.domain.dto.SaleFileReport;
import pe.com.tdp.ventafija.microservices.common.domain.dto.SalesReport;
import pe.com.tdp.ventafija.microservices.common.domain.dto.UserOrderResponse;
import pe.com.tdp.ventafija.microservices.common.domain.entity.*;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.encryption.PGPEncryptionHelper;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.exception.ApplicationException;
import pe.com.tdp.ventafija.microservices.common.exception.DataValidationException;
import pe.com.tdp.ventafija.microservices.common.logging.ApplicationLogMarker;
import pe.com.tdp.ventafija.microservices.common.services.*;
import pe.com.tdp.ventafija.microservices.common.util.*;
import pe.com.tdp.ventafija.microservices.common.util.enums.OrderStatusEnum;
import pe.com.tdp.ventafija.microservices.common.util.enums.UploadWebFileResponse;
import pe.com.tdp.ventafija.microservices.domain.Cell;
import pe.com.tdp.ventafija.microservices.domain.Dates;
import pe.com.tdp.ventafija.microservices.domain.Parameter;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipRequest;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipResponse;
import pe.com.tdp.ventafija.microservices.domain.configuration.Agendamiento;
import pe.com.tdp.ventafija.microservices.domain.configuration.InitialResponse;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.domain.order.*;
import pe.com.tdp.ventafija.microservices.domain.order.dto.PagoEfectivoConsultaRequest;
import pe.com.tdp.ventafija.microservices.domain.order.dto.PagoEfectivoConsultaResponse;
import pe.com.tdp.ventafija.microservices.domain.order.dto.WebOrderDto;
import pe.com.tdp.ventafija.microservices.domain.order.entity.*;
import pe.com.tdp.ventafija.microservices.domain.order.parser.FirebaseParser;
import pe.com.tdp.ventafija.microservices.domain.order.parser.OrderFirebaseParser;
import pe.com.tdp.ventafija.microservices.domain.repository.*;
import pe.com.tdp.ventafija.microservices.repository.address.AddressDao;
import pe.com.tdp.ventafija.microservices.service.automatizador.AutomatizadorService;
import pe.com.tdp.ventafija.microservices.service.customer.CustomerService;
import pe.pagoefectivo.PagoEfectivo;
import pe.pagoefectivo.service.ConsultarCIPService;
import pe.pagoefectivo.service.ws.BEWSConsultarRequest;
import pe.pagoefectivo.service.ws.BEWSConsultarResponse;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.swing.text.NumberFormatter;
import java.io.*;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

import static pe.com.tdp.ventafija.microservices.common.util.ConvertTimeStamp.convertStringToTimestamp;
import static pe.com.tdp.ventafija.microservices.common.util.ConvertTimeStamp_.convertStringToTimestamp_;

@Service
public class OrderService {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderDetailRepository orderDetailRepository;
    @Autowired
    private CustomerService customerService;

    // Sprint 5 - Service de SalesAgent
    @Autowired
    private TdpSalesAgentRepository tdpSalesAgentRepository;
    @Autowired
    private TdpVisorRepository tdpVisorRepository;
    @Autowired
    private AzureRequestRepository azureRequestRepository;
    @Autowired
    private TdpCatalogRepository tdpCatalogRepository;
    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private OrderCustomerService orderCustomerService;
    // @Autowired
    // private HdecClientConfig hdecClientConfig;
    @Autowired
    private ApiHeaderConfig apiHeaderConfig;
    @Autowired
    private ParametersRepository parametersRepository;
    @Autowired
    private GoogleStorageService googleStorageService;
    @Autowired
    private WatsonService watsonService;
    @Autowired
    private Messages messages;
    @Autowired
    private AudioConverterService audioConverterService;
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private PeticionPagoEfectivoService peticionPagoEfectivoService;
    @Autowired
    private SvaService svaService;

    @Autowired
    private Firebase firebase;

    @Autowired
    private LegadoService legadoService;

    //20180129
    @Autowired
    private HDECService hdecService;

    @Autowired
    private HDECRetryService hdecRetryService;

    @Autowired
    private ServiceCallEventsService serviceCallEventsService;

    @Autowired
    private AutomatizadorService automatizadorService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private TdpOrderRepository tdpOrderRepository;

    @Value("${pe.api.code.service}")
    private String pagoEfectivoCodigoServicio;
    @Value("${pe.api.expiration.days}")
    private Integer pagoEfectivoExpirationDays;

    @Value("${pe.api.paymentmode.condition}")
    private String pagoEfectivoPaymentModeCondition;
    @Value("${pe.api.invalid.producttypecode}")
    private String pagoEfectivoInvalidProductTypeCode;

    // Sprint 4 - parametro de inicio de vigencia de upfront
    @Value("${tdp.vigencia.upfront.inicio}")
    private String flagUpfront;

    // valores para Pago Efectivo
    @Value("${pe.api.key.private}")
    private String privateKeyLocation;
    @Value("${pe.api.key.public}")
    private String publicKeyLocation;
    @Value("${pe.api.url.cripto}")
    private String cryptoUrl;
    @Value("${pe.api.url.service}")
    private String servicesUrl;
    @Value("${pe.api.code.service}")
    private String serviceCode;
    @Value("${pe.api.proxy.hostname}")
    private String proxyHostname;
    @Value("${pe.api.proxy.port}")
    private String proxyPort;
    @Value("${pe.api.proxy.user}")
    private String proxyUser;
    @Value("${pe.api.proxy.password}")
    private String proxyPassword;
    @Value("${pe.api.thirdparty.url}")
    private String pagoEfectivoUrl;
    @Value("${pe.api.thirdparty.cons.url}")
    private String consultaPagoEfectivoUrl;

    @Value("${pe.api.restthirdparty.cons.url}")
    private String consultaRestPagoEfectivoUrl;

    // Sprint 7 - Se cargan codigos de tipos de documento equivalencias en HDEC y EQUIFAX
    @Value("${tipodocumento.equivalencia.hdec.dni}")
    private String codigoDNIEquivalenteHDEC;

    @Value("${tipodocumento.equivalencia.hdec.cex}")
    private String codigoCEXEquivalenteHDEC;

    @Value("${tipodocumento.equivalencia.hdec.pas}")
    private String codigoPASEquivalenteHDEC;

    @Value("${tipodocumento.equivalencia.hdec.ruc}")
    private String codigoRUCEquivalenteHDEC;

    @Autowired
    private AddressDao addressDao;

    private static final String ESTADO_SOLICITUD_REGISTRADO = "REGISTRADO";
    private static final String ESTADO_SOLICITUD_INGRESADO = "INGRESADO";
    private static final List<Integer> LONG_CMS = Arrays.asList(7, 8);
    private static final List<Integer> LONG_ATIS = Arrays.asList(9);

    InitialResponse initialResponse = new InitialResponse();

    // Zona Horaria obligatoria para fechas
    DateTimeZone dtZone = DateTimeZone.forID("America/Lima");

    @Transactional
    public boolean save(String firebaseId, String codigoAplicacion, String sourceProductName) throws DataValidationException, Exception {
        Date timestamp = new Date();
        logger.info(String.format("%s saving order", timestamp.getTime()));
        boolean result = false;
        SaleApiFirebaseResponseBody objFirebase = firebase.loadFirebaseObject(firebaseId);
        if (objFirebase != null) {
            String type = objFirebase.getType() != null ? objFirebase.getType().trim() : objFirebase.getType();
            if (!Constants.TYPE_SVA.equals(type)) {
                // Sprint 3 - sabemos que este metodo ya sube el audio (por la app)... entonces seteamos el valor seguimientoPosteriorAudio=false
                save(firebaseId, objFirebase, null, false, codigoAplicacion);
            } else {
                WebOrderDto webOrderDto = new WebOrderDto();

                webOrderDto.setProduct(new WebOrderDto.Product());
                webOrderDto.getProduct().setProductType(objFirebase.getProduct_type());
                webOrderDto.getProduct().setProductName(objFirebase.getProduct_name());
                webOrderDto.getProduct().setCampaign(objFirebase.getProduct_campaign());
                webOrderDto.getProduct().setProductCategory(objFirebase.getProduct_category());
                webOrderDto.getProduct().setProductCode(objFirebase.getProduct_code());

                webOrderDto.setSourceProductName(sourceProductName);

                saveSva(firebaseId, objFirebase, webOrderDto, false, codigoAplicacion);
            }
            // save(firebaseId, objFirebase);
            result = true;
        }

        logger.info(String.format("%s order saved", timestamp.getTime()));
        return result;
    }

    public SaleApiFirebaseResponseBody objFirebase(String firebaseId) {
        SaleApiFirebaseResponseBody objFirebase = firebase.loadFirebaseObject(firebaseId);
        return objFirebase;
    }

    public SaleApiFirebaseResponseBody computeWebOrder(WebOrderDto webOrderDto) {
        if (webOrderDto == null || webOrderDto.getCustomer() == null) {
            return null;
        }
        SaleApiFirebaseResponseBody objFirebase = new SaleApiFirebaseResponseBody();
        // Sprint 7... propiedades adicionales: birthdate y nationality
        objFirebase.getAdditionalProperties().put("birthdate", webOrderDto.getCustomer().getBirthDate());
        objFirebase.getAdditionalProperties().put("nationality", webOrderDto.getCustomer().getNationality());

        if (webOrderDto.getCmsServiceCode() != null)
            objFirebase.setCms_service_code(webOrderDto.getCmsServiceCode());
        if (webOrderDto.getCmsCustomer() != null)
            objFirebase.setCms_customer(webOrderDto.getCmsCustomer());

        objFirebase.setVendor_id(webOrderDto.getUserId());
        objFirebase.setClient_doc_number(webOrderDto.getCustomer().getDocumentNumber());
        objFirebase.setClient_document(webOrderDto.getCustomer().getDocumentType());
        objFirebase.setClient_email(webOrderDto.getCustomer().getEmail());
        objFirebase.setClient_lastname(webOrderDto.getCustomer().getLastName1());
        objFirebase.setClient_mother_lastname(webOrderDto.getCustomer().getLastName2());
        objFirebase.setClient_names(webOrderDto.getCustomer().getFirstName());
        objFirebase.setClient_mobile_phone(webOrderDto.getCustomer().getMobilePhone());
        objFirebase.setClient_secondary_phone(webOrderDto.getCustomer().getTelephone());
        objFirebase.setAtis_migration_phone(webOrderDto.getPhone());
        objFirebase.setDepartment(webOrderDto.getDepartment());
        objFirebase.setProvince(webOrderDto.getProvince());
        objFirebase.setDistrict(webOrderDto.getDistrict());
        objFirebase.setAddress(webOrderDto.getAddress());
        objFirebase.setLatitude_installation(webOrderDto.getLatitudeInstallation());
        objFirebase.setLongitude_installation(webOrderDto.getLongitudeInstallation());
        objFirebase.setAddress_additional_info(webOrderDto.getAddressAdditionalInfo());
        objFirebase.setType(webOrderDto.getType());
        objFirebase.setProduct_equipment_type(webOrderDto.getEquipmentName());
        if (webOrderDto.getProduct() != null) {
            objFirebase.setProduct_id(webOrderDto.getProduct().getId());
            objFirebase.setOffering_expert_code(webOrderDto.getProduct().getExpertCode());
            objFirebase.setProduct_type(webOrderDto.getProduct().getProductType());

            // Automatizador GIS
            objFirebase.setMsx_cbr_voi_ges_in(webOrderDto.getProduct().getMsx_cbr_voi_ges_in());
            objFirebase.setMsx_cbr_voi_gis_in(webOrderDto.getProduct().getMsx_cbr_voi_gis_in());
            objFirebase.setMsx_ind_snl_gis_cd(webOrderDto.getProduct().getMsx_ind_snl_gis_cd());
            objFirebase.setMsx_ind_gpo_gis_cd(webOrderDto.getProduct().getMsx_ind_gpo_gis_cd());
            objFirebase.setCod_ind_sen_cms(webOrderDto.getProduct().getCod_ind_sen_cms());
            objFirebase.setCod_cab_cms(webOrderDto.getProduct().getCod_cab_cms());
            objFirebase.setCod_fac_tec_cd(webOrderDto.getProduct().getCod_fac_tec_cd());
            objFirebase.setCobre_blq_vta(webOrderDto.getProduct().getCobre_blq_vta());
            objFirebase.setCobre_blq_trm(webOrderDto.getProduct().getCobre_blq_trm());
        }
        if (webOrderDto != null && webOrderDto.getCampaniaSeleccionada() != null && !webOrderDto.getCampaniaSeleccionada().isEmpty()) {
            objFirebase.setProduct_campaign(webOrderDto.getCampaniaSeleccionada());
        }
        objFirebase.setSva(webOrderDto.getSva());
        objFirebase.setModified(new Date().getTime());

        // Condiciones de Venta
        objFirebase.setShipping_contracts_for_email(webOrderDto.isShippingContractsForEmail());
        objFirebase.setAffiliation_electronic_invoice(webOrderDto.isAffiliationElectronicInvoice());
        objFirebase.setDisaffiliation_white_pages_guide(webOrderDto.isDisaffiliationWhitePagesGuide());
        objFirebase.setPublication_white_pages_guide(webOrderDto.isPublicationWhitePagesGuide());
        objFirebase.setAffiliation_data_protection(webOrderDto.isAffiliationDataProtection());
        objFirebase.setParental_protection(webOrderDto.isParentalProtection());
        objFirebase.setAutomatic_debit(webOrderDto.isAutomaticDebit());
        objFirebase.setDatetime_sales_condition(webOrderDto.getDatetimeSalesCondition());

        objFirebase.setReason_close_sale(webOrderDto.getCancelDescription());
        objFirebase.setOriginOrder(webOrderDto.getOriginOrder());

        if (webOrderDto.getType() != null) {
            if (!objFirebase.getType().trim().equals(Constants.TYPE_SVA)) {
                if (objFirebase.getProduct_id() != null) {
                    TdpCatalog tdpCatalog = tdpCatalogRepository.findOne(Integer.parseInt(objFirebase.getProduct_id()));
                    if (tdpCatalog != null) {
                        objFirebase.setProduct_name(tdpCatalog.getProductName());
                        objFirebase.setProduct_has_discount("SI".equalsIgnoreCase(tdpCatalog.getDiscount()));
                        objFirebase.setProduct_price(tdpCatalog.getPrice());
                        objFirebase.setProduct_prom_price(tdpCatalog.getPromPrice());
                        objFirebase.setProduct_financing_cost(tdpCatalog.getFinancingCost());
                        objFirebase.setProduct_financing_month(tdpCatalog.getFinancingMonth());
                        objFirebase.setProduct_install_cost(tdpCatalog.getInstallCost());
                        objFirebase.setProduct_return_period(tdpCatalog.getReturnPeriod());
                        objFirebase.setProduct_has_equipment("si".equalsIgnoreCase(tdpCatalog.getEquipmentType()));
                        objFirebase.setProduct_line_type(tdpCatalog.getLineType());
                        objFirebase.setProduct_cash_price(tdpCatalog.getCashPrice());
                        objFirebase.setProduct_commercial_operation(tdpCatalog.getCommercialOperation());
                        objFirebase.setProduct_payment_method(tdpCatalog.getPaymentMethod());
                    }
                }
            } else {
                if (webOrderDto.getProduct() != null) {
                    objFirebase.setProduct_name(webOrderDto.getProduct().getProductName());
                    objFirebase.setProduct_payment_method(webOrderDto.getProduct().getPaymentMethod());
                    objFirebase.setProduct_cash_price(new BigDecimal(webOrderDto.getProduct().getCashPrice()));
                    objFirebase.setProduct_price(new BigDecimal(webOrderDto.getProduct().getPrice()));
                }
                objFirebase.setProduct_commercial_operation(Constants.COMMERCIAL_OPE_SVA);
            }
        }
        objFirebase.setAddress_referencia(webOrderDto.getAddress_referencia());
        objFirebase.setAddress_ubigeoGeocodificado(webOrderDto.getAddress_ubigeoGeocodificado());
        objFirebase.setAddress_descripcionUbigeo(webOrderDto.getAddress_descripcionUbigeo());
        objFirebase.setAddress_direccionGeocodificada(webOrderDto.getAddress_direccionGeocodificada());
        objFirebase.setAddress_tipoVia(webOrderDto.getAddress_tipoVia());
        objFirebase.setAddress_nombreVia(webOrderDto.getAddress_nombreVia());
        objFirebase.setAddress_numeroPuerta1(webOrderDto.getAddress_numeroPuerta1());
        objFirebase.setAddress_numeroPuerta2(webOrderDto.getAddress_numeroPuerta2());
        objFirebase.setAddress_cuadra(webOrderDto.getAddress_cuadra());
        objFirebase.setAddress_tipoInterior(webOrderDto.getAddress_tipoInterior());
        objFirebase.setAddress_numeroInterior(webOrderDto.getAddress_numeroInterior());
        objFirebase.setAddress_piso(webOrderDto.getAddress_piso());
        objFirebase.setAddress_tipoVivienda(webOrderDto.getAddress_tipoVivienda());
        objFirebase.setAddress_nombreVivienda(webOrderDto.getAddress_nombreVivienda());
        objFirebase.setAddress_tipoUrbanizacion(webOrderDto.getAddress_tipoUrbanizacion());
        objFirebase.setAddress_nombreUrbanizacion(webOrderDto.getAddress_nombreUrbanizacion());
        objFirebase.setAddress_manzana(webOrderDto.getAddress_manzana());
        objFirebase.setAddress_lote(webOrderDto.getAddress_lote());
        objFirebase.setAddress_kilometro(webOrderDto.getAddress_kilometro());
        objFirebase.setAddress_nivelConfianza(webOrderDto.getAddress_nivelConfianza());

        objFirebase.setClient_doctype_rrll(webOrderDto.getCustomer().getTipoDocumentoRrll());
        objFirebase.setClient_numdoc_rrll(webOrderDto.getCustomer().getNumeroDocumentoRrll());
        objFirebase.setClient_name_rrll(webOrderDto.getCustomer().getNombreCompletoRrll());

        return objFirebase;
    }

    private String generateWebOrderId() {
        RandomString randomString = new RandomString(19);
        String orderId = "-" + randomString.nextString();
        return orderId;
    }

    @Transactional
    public boolean updatePending(WebOrderDto.Offline offline, String orderId) throws ApplicationException {
        if (offline == null) {
            return false;
        }

        TdpVisor orden = tdpVisorRepository.findOneByIdVisor(orderId);

        if (orden== null) return false;

        //Sprint27 Offline
        if(offline != null && offline.getFlag()!=null && offline.getBody()!=null && offline.getFlag().contains("7")){
            orderRepository.updateFlagOffline(orderId, offline.toString());

        }

        return true;

    }

    // Sprint 7
    @Transactional
    public boolean savePending(WebOrderDto webOrderDto) throws ApplicationException {
        if (webOrderDto == null) {
            return false;
        }

        orderRepository.updateWhatsapp(webOrderDto.getWhatsapp(), webOrderDto.getOrderId());

        Agendamiento agendamiento = new Agendamiento();
        //método para realizar captura de campos para la validación
        agendamiento = consulAgen();
        String depaList = agendamiento.getDepartamento();
        String tecList = agendamiento.getTecnologia();
        String canalList = agendamiento.getCanal();

        AgenVal validarExistencia = null;

        validarExistencia = orderRepository.updateAgentPendienteVisor(webOrderDto.getOrderId(), canalList, depaList, tecList);

        TdpVisor orden = tdpVisorRepository.findOneByIdVisor(webOrderDto.getOrderId());

        //Sprint27 Offline
        if(webOrderDto.getOffline() != null && webOrderDto.getOffline().getFlag() != null && webOrderDto.getOffline().getBody() != null && webOrderDto.getOffline().getFlag().contains("7")){
            orderRepository.updateFlagOffline(webOrderDto.getOrderId(),webOrderDto.getOffline().toString());
        }

        if(orden.getEstadoSolicitud().equals("OFFLINE")){
            orden.setEstadoAnterior("PENDIENTE");
            tdpVisorRepository.save(orden);
            return true;
        }else{
            if (!orden.getEstadoSolicitud().equals("SINFINALIZAR") && !orden.getEstadoSolicitud().equals("SINFINALIZAR-OFFLINE")){
                return orderRepository.updateEstadoVisor("PENDIENTE", webOrderDto.getOrderId());
            }else{
                return true;
            }
        }
    }

    public Agendamiento consulAgen() {

        Agendamiento agendamiento = new Agendamiento();

        List<Parameters> parametersAgendamientoList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("AGENDAMIENTO", "VAL_AREA");
        List<String> listaAgendamiento = new ArrayList<>();

        for (Parameters items : parametersAgendamientoList) {
            listaAgendamiento.add(items.getStrValue());
        }

        agendamiento.setDepartamento(listaAgendamiento.get(0));
        agendamiento.setCanal(listaAgendamiento.get(1));
        agendamiento.setTecnologia(listaAgendamiento.get(2));

        initialResponse.setAgendamiento(agendamiento);

        return agendamiento;
    }

    @Transactional
    public Order save(WebOrderDto webOrderDto, String codigoAplicacion) throws ApplicationException {//Mijail
        if (webOrderDto == null) {
            return null;
        }
        String orderId = webOrderDto.getOrderId();
        if (webOrderDto.getOrderId() == null || webOrderDto.getOrderId().equalsIgnoreCase("")) {
            orderId = generateWebOrderId();
        }

        SaleApiFirebaseResponseBody objFirebase = computeWebOrder(webOrderDto);
        objFirebase.setSuccess(true);
        objFirebase.setReason_close_sale("");
        logger.info(String.format("registrando order web con ID=%s", orderId));
        Order order;

        // Sprint 7... si es que el perfil del vendedor es TIENDA entonces no hay seguimiento de audio
        boolean seguimientoPosteriorAudio = !"PRESENCIAL".equals(webOrderDto.getTipificacion());
        boolean grabarEnAzure = !"PRESENCIAL".equals(webOrderDto.getTipificacion());

        if (!objFirebase.getType().trim().equals(Constants.TYPE_SVA)) {
            // Sprint 3 - sabemos que este metodo SI deja pendiente la subida de audio... entonces seteamos el valor seguimientoPosteriorAudio=true
            order = save(orderId, objFirebase, webOrderDto, seguimientoPosteriorAudio, codigoAplicacion);
        } else {
            // Sprint 3 - sabemos que este metodo SI deja pendiente la subida de audio... entonces seteamos el valor seguimientoPosteriorAudio=true
            order = saveSva(orderId, objFirebase, webOrderDto, seguimientoPosteriorAudio, codigoAplicacion);
        }

        this.update(orderId, objFirebase, grabarEnAzure, webOrderDto.getNombrePuntoVenta(), webOrderDto.getEntidad());

        return order;
    }

    @Transactional
    public Order save(String firebaseId, SaleApiFirebaseResponseBody objFirebase, WebOrderDto webOrderDto, boolean seguimientoPosteriorAudio, String codigoAplicacion) throws DataValidationException {

        if (objFirebase == null) {
            return null;
        }
        Customer customer = orderCustomerService.saveOrUpdate(objFirebase);

        FirebaseParser<Order> parser = new OrderFirebaseParser();
        Order order = parser.parse(objFirebase);

        TdpCatalog tdpCatalog = null;
        if (NumberUtils.isNumber(objFirebase.getProduct_id())) {
            //throw new DataValidationException(MessageConstants.ORDER_PRODUCT_DATA_NOT_FOUND);
            tdpCatalog = tdpCatalogRepository.findOne(Integer.parseInt(objFirebase.getProduct_id()));
        }
        if (tdpCatalog != null) {
            order.setServiceType(tdpCatalog.getTipoRegistro());
            order.setProductCategory(tdpCatalog.getProductCategory());
            order.setProductType(tdpCatalog.getProductType());
            order.setProductName(tdpCatalog.getProductName());
            //order.setCampaign(tdpCatalog.getCampaign());
            // Sprint 10 - seteamos la campania seleccionada para el caso de ventas en la web
            if (webOrderDto != null && webOrderDto.getCampaniaSeleccionada() != null && !webOrderDto.getCampaniaSeleccionada().isEmpty()) {
                order.setCampaign(webOrderDto.getCampaniaSeleccionada());
            } else {
                //sino solo seteamos el valor original.. para no afectar la app
                //order.setCampaign(tdpCatalog.getCampaign());
                order.setCampaign(order.getCampaign());
            }
            order.setMonthPeriod(tdpCatalog.getMonthPeriod());

            order.setProductType(tdpCatalog.getProductType());
            order.setProductCode(tdpCatalog.getProductCode());
            order.setCommercialOperation(tdpCatalog.getCommercialOperation());
            order.setEquipmentDecoType(tdpCatalog.getEquipmentType());

            /* Sprint 10 */
            order.setPromoSpeed(tdpCatalog.getPromoSpeed());
            order.setPeriodoPromoSpeed(tdpCatalog.getPeriodoPromoSpeed());
            order.setEquipamientoLinea(tdpCatalog.getEquipamientoLinea());
            order.setEquipamientoInternet(tdpCatalog.getEquipamientoInternet());
            order.setEquipamientoTv(tdpCatalog.getEquipamientoTv());
            /* Sprint 10 */

        }
        /*else {
            throw new DataValidationException(MessageConstants.ORDER_PRODUCT_DATA_NOT_FOUND);
        }*/

        order.setCustomer(customer);
        order.setId(firebaseId);

        order.setStatus(OrderStatusEnum.PRE_EFECTIVO.getCode());

        if (order.getRegisteredTime() == null) {
            DateTime dt = new DateTime(new Date());
            DateTime dtus = dt.withZone(dtZone);
            order.setRegisteredTime(dtus.toDate());
        }


        // Sprint 3 - seteamos estado del audio como pendiente... es posible que solo utilicemos el campo "status"
        if (seguimientoPosteriorAudio) {
            //seteamos el valor 1:PENDIENTE-AUDIO... luego cuando se suba el audio se actualizara a 2:AUDIO-INGRESADO
            order.setStatusAudio("1");
        } else {
            //sino seteamos el valor 0:INGRESADO POR OTRO MEDIO
            order.setStatusAudio("0");
        }

        TdpSalesAgent tdpSalesAgent = tdpSalesAgentRepository.findOneByCodigoAtis(objFirebase.getVendor_id());

        if (!codigoAplicacion.equalsIgnoreCase("APPVF")) {
            if (tdpSalesAgent != null) {
                if (tdpSalesAgent.getType_flujo_tipificacion() != null
                        && tdpSalesAgent.getType_flujo_tipificacion().equalsIgnoreCase("REMOTO OUT")) {
                    order.setStatusAudio("8");
                }
            }
        }
        order.setAppcode(codigoAplicacion);

        order.setPaquetizacion("NO");
        if (objFirebase.getType() != null && objFirebase.getType().equalsIgnoreCase("M")) {
            if (objFirebase.getCms_customer() != null && objFirebase.getCms_service_code() != null) {
                order.setPaquetizacion("SI");
            }
        }

        /* se setea, evitar nullpointerexception si webOrderDto es null */
        if (webOrderDto != null) order.setFlagFtth(webOrderDto.getFlagFtth());

        if (webOrderDto!=null) order.setOffline(webOrderDto.getOffline().toString());

        order.setCanal(tdpSalesAgent.getCanal());

        if (order.getAppcode().equalsIgnoreCase("APPVF") && objFirebase!=null && objFirebase.getOffline()!=null
                && (objFirebase.getOffline().contains("flag") || objFirebase.getOffline().contains("body"))){

            WebOrderDto.Offline offlineObj = (new Gson()).fromJson(objFirebase.getOffline(), WebOrderDto.Offline.class);
            order.setOffline(offlineObj.toString());
        }

        //System.out.println(webOrderDto.getOffline().toString());

        orderRepository.save(order);

        orderDetailService.save(objFirebase, order, tdpCatalog);

        return order;
    }

    @Transactional
    public Order saveSva(String firebaseId, SaleApiFirebaseResponseBody objFirebase, WebOrderDto webOrderDto, boolean seguimientoPosteriorAudio, String codigoAplicacion)
            throws DataValidationException {
        if (objFirebase == null) {
            return null;
        }
        Customer customer = orderCustomerService.saveOrUpdate(objFirebase);

        FirebaseParser<Order> parser = new OrderFirebaseParser();
        Order order = parser.parse(objFirebase);

        if (webOrderDto.getProduct() != null) {
            order.setProductType(webOrderDto.getProduct().getProductType());
            order.setProductName(webOrderDto.getProduct().getProductName());
            order.setCampaign(webOrderDto.getProduct().getCampaign());
            order.setProductCategory(webOrderDto.getProduct().getProductCategory());
            order.setProductCode(webOrderDto.getProduct().getProductCode());
        }

        order.setCommercialOperation(Constants.COMMERCIAL_OPE_SVA);

        // order.setMonthPeriod(tdpCatalog.getMonthPeriod());

        order.setCustomer(customer);
        order.setId(firebaseId);

        order.setStatus(OrderStatusEnum.PRE_EFECTIVO.getCode());

        order.setSourceProductName(webOrderDto.getSourceProductoName());

        // Sprint 3 - seteamos estado del audio como pendiente... es posible que solo utilicemos el campo "status"
        if (seguimientoPosteriorAudio) {
            //seteamos el valor 1:PENDIENTE-AUDIO... luego cuando se suba el audio se actualizara a 2:AUDIO-INGRESADO
            order.setStatusAudio("1");
        } else {
            //sino seteamos el valor 0:INGRESADO POR OTRO MEDIO
            order.setStatusAudio("0");
        }

        if (!codigoAplicacion.equalsIgnoreCase("APPVF")) {
            TdpSalesAgent tdpSalesAgent = tdpSalesAgentRepository.findOneByCodigoAtis(objFirebase.getVendor_id());
            if (tdpSalesAgent != null) {
                if (tdpSalesAgent.getType_flujo_tipificacion() != null
                        && tdpSalesAgent.getType_flujo_tipificacion().equalsIgnoreCase("REMOTO OUT")) {
                    order.setStatusAudio("8");
                }
            }
        }

        order.setAppcode(codigoAplicacion);
        if (order.getRegisteredTime() == null) {
            DateTime dt = new DateTime(new Date());
            DateTime dtus = dt.withZone(dtZone);
            order.setRegisteredTime(dtus.toDate());
            //order.setRegisteredTime(new Date());
        }

        if (webOrderDto!=null && webOrderDto.getOffline()!=null) order.setOffline(webOrderDto.getOffline().toString());

        if (order.getAppcode().equalsIgnoreCase("APPVF") && objFirebase!=null && objFirebase.getOffline()!=null
                && (objFirebase.getOffline().contains("flag") || objFirebase.getOffline().contains("body"))){

            WebOrderDto.Offline offlineObj = (new Gson()).fromJson(objFirebase.getOffline(), WebOrderDto.Offline.class);
            order.setOffline(offlineObj.toString());
        }

        orderRepository.save(order);
        orderDetailService.save(objFirebase, order, null);

        return order;
    }


    @Transactional
    public Order cancel(WebOrderDto webOrderDto, String codigoAplicacion) throws ApplicationException {
        if (webOrderDto == null) {
            return null;
        }
        Order order = new Order();
        try {
            // Sprint 7... verificamos si es que viene un orderId entonces se tiene que eliminar esa venta...
            // queda pendiente optimizar este flujo
            if (webOrderDto.getOrderId() != null) {
                orderDetailRepository.deleteByOrderId(webOrderDto.getOrderId());
                try {
                    orderRepository.delete(webOrderDto.getOrderId());
                } catch (Exception e) {
                    // si es que no existe la venta en bd entonces continuamos el flujo
                }
            }
            // Y continuamos con la generacion de la venta con otro id

            String orderId = generateWebOrderId();
            SaleApiFirebaseResponseBody objFirebase = computeWebOrder(webOrderDto);
            if (objFirebase.getType() != null && objFirebase.getType().trim().equals(Constants.TYPE_SVA)) {
                order = saveSva(orderId, objFirebase, webOrderDto, false, codigoAplicacion);
            } else {
                order = saveForCancel(objFirebase, orderId, codigoAplicacion);
            }
            order.setCancelDescription(webOrderDto.getCancelDescription());
            String orderStatus = OrderStatusEnum.NO_EFECTIVO.getCode();
            orderRepository.updateStatus(orderStatus, order.getCancelDescription(), new Date(), order.getId());
        } catch (DataValidationException e) {
            logger.error("error de validacion", e);
        }
        return order;
    }

    @Transactional
    public boolean update(String firebaseId) throws ApplicationException {
        boolean result = false;
        SaleApiFirebaseResponseBody objFirebase = firebase.loadFirebaseObject(firebaseId);

        if (objFirebase != null) {
            // Sprint 5 - al parecer este metodo es usado por la app... entonces no tienen punto de venta ni entidad
            // enviamos estos datos como null para que se obligue a buscar en base datos (solo para la app)
            //update(firebaseId, objFirebase, false);
            update(firebaseId, objFirebase, true, null, null);
            result = true;
        }
        return result;
    }


    @Transactional
    public void update(String firebaseId, SaleApiFirebaseResponseBody objFirebase, boolean grabarEnAzure,
                       String nombrePuntoVenta, String entidad) throws ApplicationException {
        Order order = orderRepository.findOne(firebaseId);
        if ((order == null || objFirebase == null)
                || !OrderStatusEnum.PRE_EFECTIVO.getCode().equals(order.getStatus())) {
            throw new ApplicationException(MessageConstants.ORDER_NOT_SAVED);
        }

        String orderStatus = OrderStatusEnum.NO_EFECTIVO.getCode();
        if (objFirebase.getSuccess()) {
            // System.out.println("lgomez: se va actualizar a efectivo -
            // orderId"+ order.getId());

            orderStatus = OrderStatusEnum.EFECTIVO.getCode();
        }


        //glazaror se comenta el updateStatus ya que genera error
        //orderRepository.updateStatus(orderStatus, objFirebase.getReason_close_sale(), new Date(), firebaseId);
        //@Query("update Order o set o.status = ?1, o.cancelDescription = ?2, o.lastUpdateTime = ?3 where o.id = ?4")
        order.setStatus(orderStatus);
        order.setCancelDescription(objFirebase.getReason_close_sale());

        DateTime dt = new DateTime(new Date());
        DateTime dtus = dt.withZone(dtZone);
        order.setLastUpdateTime(dtus.toDate());

        // Sprint 5 - se adicionan campos adicionales para la venta
        if (nombrePuntoVenta == null || entidad == null) {
            //si es que nombrePuntoVenta o entidad son nulos entonces buscamos en base datos estos valores (caso de app movil)
            String codigoAtis = order.getUser().getId();
            TdpSalesAgent salesAgent = tdpSalesAgentRepository.findOne(codigoAtis);
            if (salesAgent != null) {
                nombrePuntoVenta = salesAgent.getNombrePuntoVenta();
                entidad = salesAgent.getEntidad();
            }
        }
        order.setNombrePuntoVenta(nombrePuntoVenta);
        order.setEntidad(entidad);

        orderRepository.save(order);

        if (grabarEnAzure) {
            // sprint 3 - el registro de pendiente en la tabla azure_request debe realizarse en la nueva opcion de subida de audios individual/masivo
            AzureRequest azureRequest = new AzureRequest();
            azureRequest.setLastUpdate(dtus.toDate());
            azureRequest.setOrderId(firebaseId);
            azureRequest.setOrderStatus("PENDIENTE");
            azureRequest.setRegistrationDate(dtus.toDate());
            azureRequest.setStatus("PE");
            azureRequestRepository.save(azureRequest);
        }


        try {
            // TODO: actualizar datos
            MDC.put("commercialoperation", order.getCommercialOperation());
            MDC.put("clienttype", "cascader");
            MDC.put("servicetype", "cascader");

            if (objFirebase.getSuccess()) {
                logger.info(ApplicationLogMarker.TRANSACTION, "Venta actualizada");
            }
        } finally {
            MDC.remove("commercialoperation");
            MDC.remove("clienttype");
            MDC.remove("servicetype");
        }
    }

    @Transactional
    public void cancel(String firebaseId, String codigoAplicacion) throws Exception {
        logger.info("Inicio Cancel Service");
        SaleApiFirebaseResponseBody objFirebase = firebase.loadFirebaseObject(firebaseId);
        if (objFirebase == null) {
            logger.info("Cancel => objFirebase: null");
            return;
        }
        try {
            saveForCancel(objFirebase, firebaseId, codigoAplicacion);
            String orderStatus = OrderStatusEnum.PRE_EFECTIVO.getCode();
            if (objFirebase.getSuccess() != null && !objFirebase.getSuccess()) {
                orderStatus = OrderStatusEnum.NO_EFECTIVO.getCode();
            }
            logger.info("Cancel => orderStatus: " + orderStatus);

            orderRepository.updateStatus(orderStatus, objFirebase.getReason_close_sale(), new Date(), firebaseId);
        } catch (Exception e) {
            logger.error("Error Cancel Service", e);
        }
        logger.info("Fin Cancel Service");
    }

    public Order saveForCancel(SaleApiFirebaseResponseBody objFirebase, String orderId, String codigoAplicacion) {
        logger.info("Inicio saveForCancel Service");
        FirebaseParser<Order> parser = new OrderFirebaseParser();
        Order order = parser.parse(objFirebase);
        try {
            LogVass.serviceResponseObject(logger, "saveForCancel", "order", order);
            TdpCatalog tdpCatalog = null;
            Customer customer = orderCustomerService.saveOrUpdate(objFirebase);
            LogVass.serviceResponseObject(logger, "saveForCancel", "customer", customer);
            if (objFirebase.getProduct_id() != null) {
                tdpCatalog = tdpCatalogRepository.findOne(Integer.parseInt(objFirebase.getProduct_id()));
                if (tdpCatalog != null) {
                    order.setProductCategory(tdpCatalog.getProductCategory());
                    order.setProductType(tdpCatalog.getProductType());
                    order.setProductName(tdpCatalog.getProductName());
                    //order.setCampaign(tdpCatalog.getCampaign());
                    order.setMonthPeriod(tdpCatalog.getMonthPeriod());

                    order.setProductType(tdpCatalog.getProductType());
                    order.setCommercialOperation(tdpCatalog.getCommercialOperation());
                    order.setEquipmentDecoType(tdpCatalog.getEquipmentType());

                    /* Sprint 10 */
                    order.setPromoSpeed(tdpCatalog.getPromoSpeed());
                    order.setPeriodoPromoSpeed(tdpCatalog.getPeriodoPromoSpeed());
                    order.setEquipamientoLinea(tdpCatalog.getEquipamientoLinea());
                    order.setEquipamientoInternet(tdpCatalog.getEquipamientoInternet());
                    order.setEquipamientoTv(tdpCatalog.getEquipamientoTv());
                    /* Sprint 10 */
                }
            }
            order.setCustomer(customer);
            order.setId(orderId);

            /* Agregando el Origen de APLICACIÓN AL CANCELAR VENTA*/
            if (codigoAplicacion != null && !codigoAplicacion.trim().equalsIgnoreCase(""))
                order.setAppcode(codigoAplicacion);

            order.setStatus(OrderStatusEnum.PRE_EFECTIVO.getCode());

            if (order.getRegisteredTime() == null) {
                DateTime dt = new DateTime(new Date());
                DateTime dtus = dt.withZone(dtZone);
                order.setRegisteredTime(dtus.toDate());
            }
            LogVass.serviceResponseObject(logger, "saveForCancel", "order", order);
            orderRepository.save(order);

            orderDetailService.save(objFirebase, order, tdpCatalog);
        } catch (Exception e) {
            logger.info("Error saveForCancel Service", e);
        }
        logger.info("Fin saveForCancel Service");
        return order;
    }

    // Sprint 4 - por defecto se graba con el estado PENDIENTE en el VISOR
    @Transactional
    public void migrateVisor(String orderId) {
        orderRepository.spMigrateVisor(orderId, "PENDIENTE");
    }

    @Transactional
    public void migrateVisor(String orderId, String status) {
        //sprint 3 - ya no se usara procedimiento almacenado
        orderRepository.spMigrateVisor(orderId, status);
        //tdpVisorRepository
    }

    public List<SalesReport> reporteVentas(Integer numDays, String vendorId, String idTransaccion, String filterDni, String status) {
        logger.info("Inicio reporteVentas Service");
        List<SalesReport> response = orderRepository
                //.loadReporteVentaData(DateUtils.convertDateString(DateUtils.removeDays(new Date(), numDays)), vendorId); //sprint 3
                .loadReporteVentaData(DateUtils.removeDays(new Date(), numDays), vendorId, idTransaccion, filterDni, status); //sprint 3
        logger.info("Fin reporteVentas Service");
        return response;
    }

    //método reporte venta
    @Transactional
    public SalesReport reporteVenta(String orderId, String cip) {
        logger.info("Inicio ReporteVenta Service");
        SalesReport salesReport = null;

        try {
            salesReport = orderRepository.loadSalesReportByOrderId(orderId);
            LogVass.serviceResponseObject(logger, "ReporteVenta", "SalesReport", salesReport);
            if (salesReport == null) {
                logger.info("Fin ReporteVenta Service");
                return null;
            }

            String estadoSolicitud = salesReport.getStatusRequest();
            String codigoPedido = salesReport.getRequestCode();

            if (cip != null) {
                String docNumber = salesReport.getDni();
                PagoEfectivoConsultaResponse pagoEfectivoConsultaResponse = consultarCIP(cip, orderId, docNumber);
                LogVass.serviceResponseObject(logger, "ReporteVenta", "PagoEfectivoConsultaResponse", pagoEfectivoConsultaResponse);
                if (pagoEfectivoConsultaResponse != null) {
                    salesReport.setNumeroCIP(pagoEfectivoConsultaResponse.getNumeroCIP());
                    salesReport.setEstadoCIP(pagoEfectivoConsultaResponse.getEstadoCIP());
                } else {
                    salesReport.setNumeroCIP("");
                    salesReport.setEstadoCIP("");
                }
            } else {
                salesReport.setNumeroCIP("");
                salesReport.setEstadoCIP("");
            }


            if (ESTADO_SOLICITUD_INGRESADO.equals(estadoSolicitud) && codigoPedido != null) {
                String estadoAtisCms = null;
                int codigoPedidoTamanio = codigoPedido.trim().length();

                if (LONG_CMS.contains(codigoPedidoTamanio)) { // CMS
                    TdpServicePedido tdpServicePedido = legadoService.getEstadoCms(codigoPedido);
                    estadoAtisCms = tdpServicePedido.getPedidoEstado();
                } else if (LONG_ATIS.contains(codigoPedidoTamanio)) { // ATIS
                    TdpServicePedido tdpServicePedido = legadoService.getEstadoAtis(codigoPedido);
                    estadoAtisCms = tdpServicePedido.getPedidoEstado();
                }
                salesReport.setCmsAtisStatus(estadoAtisCms);
            }

        } catch (Exception e) {
            logger.error("Error ReporteVenta Service. ", e);
        }
        logger.info("Fin ReporteVenta Service");
        return salesReport;
    }

    public List<SaleFileReport> getBandejaVentasArchivos(String codigoSupervisor, Date fechaInicio, Date fechaFin) {
        String hour_minute = customerRepository.getValorParametro("carga_audio_hora_restriccion");
        return orderRepository
                .loadBandejaVentasArchivo(codigoSupervisor, fechaInicio, fechaFin,hour_minute);
    }

    public String audioReview(String orderId) throws ApplicationException {
        logger.info(String.format("Inicio AudioReview Service: %s", orderId));
        String requestJson = "";

        final String AUDIO_VALUE = "audio";
        final String IMAGE_VALUE = "image";

        final String audiosFirebase = String.format("%s/audios/%s.zip", orderId, orderId);
        final String imagesFirebase = String.format("%s/images/%s.pdf", orderId, orderId);

        PGPEncryptionHelper pgpDecryptionHelper = new PGPEncryptionHelper(new ByteArrayInputStream(firebase.getPgpPrivateKeyBytes()), firebase.getPgpPassphrase());
        PGPEncryptionHelper pgpDecryptionHelper2 = new PGPEncryptionHelper(new ByteArrayInputStream(firebase.getPgpPrivateKeyBytes()), firebase.getPgpPassphrase());
        String audioWavName = orderId + ".wav";
        File audioZipFirebase = null;
        File imagesPdfFirebase = null;
        File imagesPdfDescrypt = null;
        File audioZipDescrypt = null;
        File audioWav = null;
        File audioGsm = null;
        File audioGsmZip = null;

        JSONObject response = new JSONObject();
        JSONObject imagesResponse = new JSONObject();
        imagesResponse.put("firebase", String.format("%s/images/%s.pdf", orderId, orderId));
        imagesResponse.put("azure", String.format("%s/images/%s-original.pdf", orderId, orderId));
        imagesResponse.put("encontrado", "");
        imagesResponse.put("desencriptado", "");
        imagesResponse.put("subido", "");

        JSONObject audioResponse = new JSONObject();
        audioResponse.put("firebase", String.format("%s/audios/%s.zip", orderId, orderId));
        audioResponse.put("azure", String.format("%s/audios/%s_gsm.zip", orderId, orderId));
        audioResponse.put("encontrado", "");
        audioResponse.put("desencriptado", "");
        audioResponse.put("descomprimido", "");
        audioResponse.put("wavtogsm", "");
        audioResponse.put("comprimido", "");
        audioResponse.put("subido", "");

        response.put("images", imagesResponse);
        response.put("audio", audioResponse);

        try {
            // Descargar audio de Firebase
            audioZipFirebase = googleStorageService.retrieveFile(audiosFirebase, orderId, AUDIO_VALUE);
            logger.info("AudioReview => audioZipFirebase: " + audioZipFirebase.getAbsolutePath());
            // Descargar imagen de Firebase
            imagesPdfFirebase = googleStorageService.retrieveFile(imagesFirebase, orderId, IMAGE_VALUE);
            logger.info("AudioReview => imagesPdfFirebase: " + imagesPdfFirebase.getAbsolutePath());

            if (imagesPdfFirebase != null) {
                imagesResponse.put("encontrado", "SI");

                // Desencriptar PDF cifrado
                try {
                    imagesPdfDescrypt = pgpDecryptionHelper.decrypt(imagesPdfFirebase, orderId);
                    imagesResponse.put("desencriptado", "SI");
                } catch (Exception e) {
                    // En caso de error asumir que el pdf está desencriptado
                    imagesPdfDescrypt = imagesPdfFirebase;
                    imagesResponse.put("desencriptado", "NO");
                }
                logger.info("AudioReview => imagesPdfDescrypt: desencriptado");
                // Eliminar PDF cifrado
                if (!imagesPdfFirebase.delete())
                    logger.info("Delete on imagesPdfFirebase: " + imagesPdfFirebase.getAbsolutePath() + " failed!");

                // Enviar PDF desencriptado
                googleStorageService.sendFile(imagesPdfDescrypt, orderId + "/images/" + orderId + "-original.pdf", "application/pdf");
                logger.info("AudioReview => imagesPdfDescrypt: Subido al Firebase");
                imagesResponse.put("subido", "SI");
                // Eliminar PDF desencriptado
                if (!imagesPdfDescrypt.delete())
                    logger.info("Delete on imagesPdfDescrypt: " + imagesPdfDescrypt.getAbsolutePath() + " failed!");

            } else {
                imagesResponse.put("encontrado", "NO");
                imagesResponse.put("desencriptado", "NO");
                imagesResponse.put("subido", "NO");
            }

            if (audioZipFirebase != null) {
                audioResponse.put("encontrado", "SI");

                // Desencriptar zip cifrado de audio
                try {
                    audioZipDescrypt = pgpDecryptionHelper2.decrypt(audioZipFirebase, orderId);
                    audioResponse.put("desencriptado", "SI");
                } catch (Exception e) {
                    // En caso de error asumir que el pdf está desencriptado
                    audioZipDescrypt = audioZipFirebase;
                    audioResponse.put("desencriptado", "NO");
                }
                logger.info("AudioReview => audioZipDescrypt: desencriptado");
                // Eliminar zip cifrado de audio
                if (!audioZipFirebase.delete())
                    logger.info("Delete on Audio: " + audioZipFirebase.getAbsolutePath() + " failed!");

                // Obtener el audio WAV del zip
                Ziphelper ziphelper = new Ziphelper();
                audioWav = ziphelper.retriveFileFromZip(audioWavName, audioZipDescrypt);
                audioResponse.put("descomprimido", "SI");
                // Eliminar zip de audio
                if (!audioZipDescrypt.delete())
                    logger.info("Delete on audioZipDescrypt : " + audioZipDescrypt.getAbsolutePath() + " failed!");

                // Convertir audio Wav a Gsm
                audioGsm = resolverAudio(audioWav);
                audioResponse.put("wavtogsm", "SI");
                // Eliminar audio Wav
                if (!audioWav.delete())
                    logger.info("Delete on audioWav: " + audioWav.getAbsolutePath() + " failed!");

                // Comprimir a zip audio Gsm
                audioGsmZip = FileUtils.zip(audioGsm, String.format("%s.gsm", orderId));
                audioResponse.put("comprimido", "SI");
                // Eliminar audio Gsm
                if (!audioGsm.delete())
                    logger.info("Delete on audioGsm: " + audioGsm.getAbsolutePath() + " failed!");
                // Enviar audio gsm zip
                googleStorageService.sendZipFile(audioGsmZip, orderId);
                audioResponse.put("subido", "SI");
                logger.info("AudioReview => audioGsmZip: Subido al Firebase");
                // Eliminar audio gsm zip
                if (!audioGsmZip.delete())
                    logger.info("Delete on audioGsmZip: " + audioGsmZip.getAbsolutePath() + " failed!");

                /* Actualizar estado de Tabla AzureRequest */
                List<AzureRequest> lista = azureRequestRepository.findByOrderId(orderId);
                for (AzureRequest item : lista) {
                    item.setEncryted("OK");
                    azureRequestRepository.save(item);
                }

            } else {
                audioResponse.put("encontrado", "NO");
                audioResponse.put("desencriptado", "NO");
                audioResponse.put("descomprimido", "NO");
                audioResponse.put("wavtogsm", "NO");
                audioResponse.put("comprimido", "NO");
                audioResponse.put("subido", "NO");
            }

            response.put("images", imagesResponse);
            response.put("audio", audioResponse);
        } catch (Exception e) {
            logger.error("Error AudioReview Service", e);
            if (audioZipDescrypt != null && audioZipDescrypt.exists() && !audioZipDescrypt.isDirectory()) {
                audioZipDescrypt.delete();
            }
            if (imagesPdfDescrypt != null && imagesPdfDescrypt.exists() && !imagesPdfDescrypt.isDirectory()) {
                imagesPdfDescrypt.delete();
            }
            if (audioWav != null && audioWav.exists() && !audioWav.isDirectory()) {
                audioWav.delete();
            }
            if (audioGsm != null && audioGsm.exists() && !audioGsm.isDirectory()) {
                audioGsm.delete();
            }
            if (audioGsmZip != null && audioGsmZip.exists() && !audioGsmZip.isDirectory()) {
                audioGsmZip.delete();
            }
            if (audioZipFirebase != null && audioZipFirebase != null && audioZipFirebase.exists() && !audioZipFirebase.isDirectory()) {
                audioZipFirebase.delete();
            }
            if (imagesPdfFirebase != null && imagesPdfFirebase != null && imagesPdfFirebase.exists() && !imagesPdfFirebase.isDirectory()) {
                imagesPdfFirebase.delete();
            }

            response.put("images", imagesResponse);
            response.put("audio", audioResponse);
            throw new ApplicationException(e);
        }

        logger.info(String.format("Fin AudioReview Service: %s", orderId));
        return response.toString();
    }

    // Sprint 5 - conversion wav a gsm
    private File resolverAudio(File a) throws ApplicationException {
        LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
        FileSystemResource value = new FileSystemResource(a);
        map.add("file", value);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(map, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<byte[]> response = null;

        try {
            // Pendiente pasar este endpoint a la tabla de parametros....
            response = restTemplate.exchange("https://tefappfijaffmpeg-dev.mybluemix.net/api/v1/gsm", HttpMethod.POST, requestEntity, byte[].class);

        } catch (Exception e) {
            throw new ApplicationException(e);
        }

        File out = null;
        try {
            out = File.createTempFile("audio" + new Date().getTime(), ".gsm");
            org.apache.commons.io.FileUtils.writeByteArrayToFile(out, response.getBody());
            return out;

        } catch (IOException e) {
            throw new ApplicationException(e);
        }
    }


    @Transactional
    public void updateReviewData(ServiceCallEvent event, String orderId, String statusAudio) {
        logger.info("Inicio UpdateReviewData Service");
        logger.info("" + orderId + " => " + statusAudio);
        orderRepository.updateStatusAudioOrder(statusAudio, orderId);
        registerEvent(event);
        logger.info("Fin UpdateReviewData Service");
    }

    @Transactional
    public void updateStatusAudio(String orderId, String statusAudio) {
        logger.info("Inicio UpdateStatusAudio Service");
        orderRepository.updateStatusAudioOrder(statusAudio, orderId);
        logger.info("Fin UpdateStatusAudio Service");
    }

    @Transactional
    public List<String> getIdsForPreprocess() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.MONTH, 11);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        List<String> list = new ArrayList<>();
        List<Order> orders = orderRepository.findByRegistrationDateAfterAndStatus(calendar.getTime(),
                OrderStatusEnum.EFECTIVO.getCode());
        for (Order order : orders) {
            list.add(order.getId());
        }
        return list;
    }

    public boolean isPending(String orderId) throws ApplicationException {
        String orderStatus = orderRepository.loadStatusById(orderId);
        if (orderStatus == null) {
            throw new ApplicationException(MessageConstants.ORDER_NOT_SAVED);
        }
        return OrderStatusEnum.PRE_EFECTIVO.getCode().equals(orderRepository.loadStatusById(orderId));
    }

    @Transactional
    public PeticionPagoEfectivo registrarPeticionCIP(String orderId) throws URISyntaxException, ApiClientException {
        logger.info("llamando a pago efectivo con el orderId " + orderId);
        Order order = orderRepository.findOne(orderId);

        if (pagoEfectivoPaymentModeCondition != null
                && !pagoEfectivoPaymentModeCondition.equalsIgnoreCase(order.getPaymentMode())) {
            return null;
        }

        if (pagoEfectivoInvalidProductTypeCode.equalsIgnoreCase(order.getProductType())) {
            return null;
        } else if (Constants.COMMERCIAL_OPE_SVA.equalsIgnoreCase(order.getCommercialOperation())) {
            if (pagoEfectivoInvalidProductTypeCode.equalsIgnoreCase(order.getProductCategory())) {
                return null;
            }
        }

        PeticionPagoEfectivo ppe = new PeticionPagoEfectivo();
        ppe.setCodServicio(pagoEfectivoCodigoServicio);
        ppe.setCodTransaccion(orderId);
        ppe.setConceptoPago(messages.get(MessageConstants.ORDER_PAGO_EFECTIVO_CONCEPTO_PAGO));
        ppe.setEmailComercio("");
        // ppe.setFechaAExpirar(DateUtils.format("dd/MM/yyyy hh:mm:ss",
        // DateUtils.addDays(new Date(), pagoEfectivoExpirationDays)));
        ppe.setFechaAExpirar(DateUtils.format("dd/MM/yyyy", DateUtils.addDays(new Date(), pagoEfectivoExpirationDays))
                + " 22:00:00");
        ppe.setIdMoneda("1");
        ppe.setMetodosPago("1");
        ppe.setOrderId(orderId);
        ppe.setStatus("PE"); // status pendiente

        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("0.00", dfs);
        NumberFormatter nf = new NumberFormatter(decimalFormat);
        try {
            ppe.setTotal(nf.valueToString(order.getCashPrice()));
        } catch (ParseException e) {
            ppe.setTotal(order.getCashPrice().toString());
        }

        ppe.setUsuarioApellidos(String.format("%s %s",
                order.getCustomer().getLastName1() == null ? "" : order.getCustomer().getLastName1(),
                order.getCustomer().getLastName2() == null ? "" : order.getCustomer().getLastName2()));
        ppe.setUsuarioNombre(order.getCustomer().getFirstName());
        ppe.setUsuarioTipoDoc(order.getCustomer().getDocType());
        ppe.setUsuarioNumeroDoc(order.getCustomer().getDocNumber());
        ppe.setUsuarioEmail(order.getCustomer().getEmail());
        ppe.setUsuarioId(order.getUser().getId() + "");
        return peticionPagoEfectivoService.nuevaPeticion(ppe);
    }

    public PeticionPagoEfectivo findCodigoPagoEfectivo(String orderId) {
        return peticionPagoEfectivoService.findByOrderId(orderId);
    }

    public List<UserOrderResponse> getUserOrder(String id, int numDays) {
        return orderRepository.spGetUserOrders(id, numDays);
    }

    public File multipartToFile(MultipartFile multipart) throws IllegalStateException, IOException {
        File convFile = new File(multipart.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(multipart.getBytes());
        fos.close();
        return convFile;
    }

    // Sprint 3 - ahora el metodo upload es transaccional
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Throwable.class)
    public UploadWebFileResponse uploadWebFile(MultipartFile file, String orderId, String sourceApp, String versionApp) throws Exception {
        InputStream in = file.getInputStream();
        UploadWebFileResponse response = null;
        if (in == null) {
            throw new FileNotFoundException("The file uploaded can not be read");
        }
        File zipGsm = null;
        try {
            logger.info("subiendo archivo web");
            String originalName = file.getOriginalFilename();
            boolean correctExtensionPDF = originalName.toLowerCase().endsWith(".pdf");
            if (correctExtensionPDF) {
                zipGsm = multipartToFile(file);
                googleStorageService.sendFile(zipGsm,
                        String.format("%s/images/%s-original.pdf", orderId, orderId),
                        "application/pdf");
            } else {
                zipGsm = FileUtils.zip(in, String.format("%s.gsm", orderId));
                googleStorageService.sendZipFile(zipGsm, orderId);
            }
            //sprint 3 - se actualiza el estado de subida de audio de la venta
            Order order = orderRepository.findOne(orderId);
            if (correctExtensionPDF) {
                order.setWhatsapp(1);
            }

           // 2:AUDIO INGRESADO Y PENDIENTE PARA QUE LO PROCESE EL BATCHERO
            order.setStatusAudio("2");
            orderRepository.save(order);

            setContextVentaData(order.getCustomer().getDocNumber(), order.getUser().getId(), sourceApp, versionApp);

            //if ("1".equals(flagUpfront) && (new BigDecimal("0.0").compareTo(order.getCashPrice()) != 0)) {
            // VERIFICAMOS, EL FLAGUPFRONT, SI LA VENTA ES AL CONTADO Y EL TIPO DE PRODUCTO
            if (!getVisorStatus(order).equals("PENDIENTE")) {
                return UploadWebFileResponse.UPLOAD_OK;
            }

            if (order.getCommercialOperation() != null) {

                String type = "";
                if (order.getCommercialOperation().equalsIgnoreCase("alta pura")) type = OrderConstants.ALTA_PURA;
                else if (order.getCommercialOperation().equalsIgnoreCase("svas")) type = OrderConstants.ALTA_SVA;
                else type = OrderConstants.MIGRACION;

                boolean success = automatizadorService.preRegisterSaleWeb(order.getId(), type);
                if (success) {
                    response = UploadWebFileResponse.UPLOAD_OK;
                    automatizadorService.setOrderInAutomatizador(order.getId());
                } else {
                    //20180129... ahora el envio a HDEC es al momento de subir audio
                    //sendDirectToHDEC(orderId);

                    Integer duplicado = addressDao._consultarDuplicado(order.getId());

                    if(duplicado.equals(0)){
                        HDECResponse hdecResponse = hdecRetryService.sendToHDECWithRetry(orderId);
                        if (HDECResponse.SATISFACTORIO.equals(hdecResponse)) {
                            response = UploadWebFileResponse.UPLOAD_OK;
                        } else if (HDECResponse.ERROR_CONTROLADO.equals(hdecResponse)) {
                            orderRepository.updateEstadoVisor("ENVIANDO", orderId);
                            response = UploadWebFileResponse.UPLOAD_ENVIANDO_HDEC;
                        } else {
                            orderRepository.updateEstadoVisor("CAIDA", orderId);
                            response = UploadWebFileResponse.UPLOAD_ERROR;
                        }
                    }

                    //consultar duplicado en automatizador
                    if(duplicado.equals(1)){
                        logger.error("Error Duplicado en Automatizador", duplicado);
                        response = UploadWebFileResponse.UPLOAD_OK;
                    }
                    if(duplicado.equals(2)){
                        orderRepository.updateEstadoVisor("ENVIANDO_AUTOMATIZADOR", orderId);
                        //response = UploadWebFileResponse.UPLOAD_ENVIANDO_HDEC;
                        logger.error("Error de Bus", duplicado);
                    }
                }

            } else {
                //20180129... ahora el envio a HDEC es al momento de subir audio
                //sendDirectToHDEC(orderId);
                HDECResponse hdecResponse = hdecRetryService.sendToHDECWithRetry(orderId);
                if (HDECResponse.SATISFACTORIO.equals(hdecResponse)) {
                    response = UploadWebFileResponse.UPLOAD_OK;
                } else if (HDECResponse.ERROR_CONTROLADO.equals(hdecResponse)) {
                    orderRepository.updateEstadoVisor("ENVIANDO", orderId);
                    response = UploadWebFileResponse.UPLOAD_ENVIANDO_HDEC;
                } else {
                    orderRepository.updateEstadoVisor("CAIDA", orderId);
                    response = UploadWebFileResponse.UPLOAD_ERROR;
                }
            }

            registrarAzure(orderId);

        } catch (Exception e) {
            logger.error("error enviar zip", e);
            throw e;
        } finally {
            // Se mueve al bloque finally
            //Eliminando zipGsm
            if (zipGsm != null) {
                zipGsm.delete();
            }
        }
        return response;
    }

    private void setContextVentaData(String numeroDocumentoCliente, String codigoVendedor, String sourceApp, String versionApp) {
        VentaFijaContext context = VentaFijaContextHolder.getContext();
        context.getServiceCallEvent().setDocNumber(numeroDocumentoCliente);
        context.getServiceCallEvent().setUsername(codigoVendedor);
        if (sourceApp != null) {
            context.getServiceCallEvent().setSourceApp(sourceApp);
        }
        if (versionApp != null) {
            context.getServiceCallEvent().setSourceAppVersion(versionApp);
        }
    }

    public void registerEvent(ServiceCallEvent event) {
        try {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events (event_datetime, tag, username,msg,"
                    + "orderId,docNumber,serviceCode,serviceUrl,serviceRequest,serviceResponse,sourceApp,"
                    + "sourceAppVersion,result) "
                    + "VALUES (CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?,?)";//glazaror se adecua sentencia insert para postgresql
            javax.persistence.Query q = em.createNativeQuery(sql);

            q.setParameter(1, "SERVICE_CALL");
            q.setParameter(2, event.getUsername());
            q.setParameter(3, event.getMsg());
            q.setParameter(4, event.getOrderId());
            q.setParameter(5, event.getDocNumber());
            q.setParameter(6, event.getServiceCode());
            q.setParameter(7, event.getServiceUrl());
            q.setParameter(8, event.getServiceRequest());
            q.setParameter(9, event.getServiceResponse());
            q.setParameter(10, event.getSourceApp());
            q.setParameter(11, event.getSourceAppVersion());
            q.setParameter(12, event.getResult());

            q.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert", e);
        }
    }

    public Map<String, SaleApiFirebaseResponseBody> loadByFirebaseIds(List<String> ids) {
        Map<String, SaleApiFirebaseResponseBody> response = new HashMap<>();
        for (String id : ids) {
            SaleApiFirebaseResponseBody fb = firebase.loadFirebaseObject(id);
            response.put(id, fb);
        }
        return response;
    }

    @Transactional
    public boolean looseConnection(List<ConectionData> lstConectionData, Map<String, SaleApiFirebaseResponseBody> firebaseData) throws Exception {
        Date start = new Date();

        for (ConectionData conectionData : lstConectionData) {
            String fbId = conectionData.getFirebaseId();
            if (fbId != null && !"".equals(fbId.trim())) {
                SaleApiFirebaseResponseBody objFirebase = firebaseData.get(conectionData.getFirebaseId());
                conectionData.setLatitude_sales(objFirebase.getLatitude_sales() == null ? null : objFirebase.getLatitude_sales().toString());
                conectionData.setLongitude_sales(objFirebase.getLongitude_sales() == null ? null : objFirebase.getLongitude_sales().toString());
//				try {
//					save(fbId, objFirebase);
//				} catch (Exception e) {
//					logger.error("No se pudo registrar la venta para el log", e);
//				}
            }
            System.out.println(conectionData);
            registerLoosConection(conectionData);
        }

        Date stop = new Date();
        logger.trace(String.format("Demoro: %f", (start.getTime() - stop.getTime()) * 1f / 1000));
        return true;
    }

    public void registerLoosConection(ConectionData conectionData) {
        try {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events" +
                    "(event_datetime, tag, username, msg, orderId, docNumber,"
                    + "serviceCode, sourceApp, sourceAppVersion, result, app_module, latitude_sales, longitude_sales)"
                    + "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            javax.persistence.Query q = em.createNativeQuery(sql);


            String sDate = conectionData.getTime();
            Date dateParse = new SimpleDateFormat("yyyyMMdd HH:mm:ss").parse(sDate);

            q.setParameter(1, dateParse);
            q.setParameter(2, "SERVICE_LOG");
            q.setParameter(3, conectionData.getCodAtis());
            q.setParameter(4, conectionData.getMensaje());
            q.setParameter(5, conectionData.getFirebaseId());
            q.setParameter(6, conectionData.getDocumentNumber());
            q.setParameter(7, "NET_ERROR");
            q.setParameter(8, conectionData.getSourceApp());
            q.setParameter(9, conectionData.getSourceAppVersion());
            q.setParameter(10, "OK");
            q.setParameter(11, conectionData.getModulo());
            q.setParameter(12, conectionData.getLatitude_sales());
            q.setParameter(13, conectionData.getLongitude_sales());

            q.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert", e);
        }
    }

    @Transactional
    public void registrarAzure(String orderId) {

        DateTime dt = new DateTime(new Date());
        DateTime dtus = dt.withZone(dtZone);

        AzureRequest azureRequest = new AzureRequest();
        azureRequest.setLastUpdate(dtus.toDate());
        azureRequest.setOrderId(orderId);
        azureRequest.setOrderStatus("PENDIENTE");
        azureRequest.setRegistrationDate(dtus.toDate());
        azureRequest.setStatus("PE");//Se vuelve a XX 20180102
        azureRequest.setEncryted("OK");
        azureRequestRepository.save(azureRequest);

    }

    public List<SalesReport> getReporteVenta(String codigoVendedor, String codigoEstado, Date fechaInicio, Date fechaFin) {
        return orderRepository.loadBandejaVentas(codigoVendedor, codigoEstado, fechaInicio, fechaFin);
    }

    //Sprint 3
    public List<SalesReport> getBandejaVentasAudio(String nombrePuntoVenta, String entidad, String codigoEstado, String codigoVendedor, String buscarPorVendedor, String dniCliente, Date fechaInicio, Date fechaFin, String filtroRestringeHoras) {
        String hour_minute = customerRepository.getValorParametro("carga_audio_hora_restriccion");
        return orderRepository
                .loadBandejaVentasAudio(nombrePuntoVenta, entidad, codigoEstado, codigoVendedor, buscarPorVendedor, dniCliente, fechaInicio, fechaFin, filtroRestringeHoras,hour_minute);
    }

    //Sprint 4
    public String deleteCerosLeft(String cadena) {

        if (cadena != null && !cadena.equals("")) {
            int count = 0;

            for (char c : cadena.toCharArray()) {
                if (c == '0') {
                    count++;
                } else {
                    break;
                }
            }

            cadena = cadena.substring(count);

        } else {
            return "";
        }

        return cadena;

    }

    // Sprint 4
    public String getVisorStatus(Order order) {
        if ("1".equals(flagUpfront)) {
            // Verificamos si el tipo de producto y operacion, ejem: MONO TV y si es operacion SVAS y MONO TV no deberian
            // generar CIP y enviarse directamente a HDEC
            if (pagoEfectivoInvalidProductTypeCode.equalsIgnoreCase(order.getProductType())) {
                return "PENDIENTE";
            } else if (Constants.COMMERCIAL_OPE_SVA.equalsIgnoreCase(order.getCommercialOperation())) {
                if (pagoEfectivoInvalidProductTypeCode.equalsIgnoreCase(order.getProductCategory())) {
                    return "PENDIENTE";
                }
            }
            // Si es que UPFRONT ya esta vigente entonces evaluamos si es la venta es con pago adelantado o no
            // para retornar PENDIENTE o PENDIENTE-PAGO como estado
            return new BigDecimal("0.0").compareTo(order.getCashPrice()) == 0 ? "PENDIENTE" : "PENDIENTE-PAGO";

        } else {
            // Si es que UPFRONT aun no esta vigente entonces siempre retornamos "PENDIENTE"
            return "PENDIENTE";
        }
    }

    public String genCIP(Integer id) {

        ThirdpartyPagoEfectivoClient client = new ThirdpartyPagoEfectivoClient(pagoEfectivoUrl, "", "");
        Map<String, String> data = new HashMap<>();
        data.put("id", id + "");
        String cip = null;
        try {
            Map<String, String> response = client.sendData(data);
            cip = response.get("responseData");
        } catch (ApiClientException e) {
            logger.error("Error :(", e);
        }
        logger.info("cip generado " + cip);
        return cip;
    }

    public PagoEfectivoConsultaResponse consultarBackupCIP(String numeroCip, String orderId, String docNumber) {

        PagoEfectivo PE = new PagoEfectivo.Builder()
                .setPrivateKey(privateKeyLocation)
                .setPublicKey(publicKeyLocation)
                .setCryptoUrl(cryptoUrl)
                .setServicesUrl(servicesUrl)
                .setCodigoServicio(serviceCode)
                .setHostnameProxy(proxyHostname)
                .setPortProxy(proxyPort)
                .setUserProxy(proxyUser)
                .setPasswordProxy(proxyPassword)
                .build();
        PagoEfectivoConsultaRequest rq = new PagoEfectivoConsultaRequest();
        rq.setNumeroCIP(numeroCip);
        rq.setOrderId(orderId);
        rq.setDocNumber(docNumber);
        PagoEfectivoConsultaResponse rs = getConsultaPagoEfectivo(rq, PE);

        return rs;
    }

    public PagoEfectivoConsultaResponse consultarCIP(String cip, String orderId, String docNumber) {

        PagoEfectivoConsultaResponse consultaPagoEfectivoReport = null;
        PagoEfectivoConsultaRequest pagoEfectivoConsultaRequest = null;

        try {
            ServiceCallEvent event = VentaFijaContextHolder.getContext().getServiceCallEvent();
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.setContentType(MediaType.APPLICATION_JSON);
            requestHeaders.set("x-ibm-client-id", "");
            requestHeaders.set("x-ibm-client-secret", "");
            requestHeaders.set("X_HTTP_USUARIO", event.getUsername());
            requestHeaders.set("X_HTTP_ORDERID", event.getOrderId());
            requestHeaders.set("X_HTTP_DOCIDENT", event.getDocNumber());
            requestHeaders.set("X_HTTP_APPSOURCE", event.getSourceApp());
            requestHeaders.set("X_HTTP_APPVERSION", event.getSourceAppVersion());

            pagoEfectivoConsultaRequest = new PagoEfectivoConsultaRequest();
            pagoEfectivoConsultaRequest.setNumeroCIP(cip);
            pagoEfectivoConsultaRequest.setOrderId(orderId);
            pagoEfectivoConsultaRequest.setDocNumber(docNumber);

            RestTemplate restTemplate = new RestTemplate();

            HttpEntity<PagoEfectivoConsultaRequest> httpEntity = new HttpEntity<PagoEfectivoConsultaRequest>(pagoEfectivoConsultaRequest, requestHeaders);

            logger.info("consultaPagoEfectivoUrl: " + consultaPagoEfectivoUrl);

            PagoEfectivoConsultaResponse response = restTemplate.postForObject(consultaPagoEfectivoUrl,
                    httpEntity,
                    PagoEfectivoConsultaResponse.class);

            if (response != null) {
                consultaPagoEfectivoReport = new PagoEfectivoConsultaResponse();
                if (response.getNumeroCIP() != null) {
                    consultaPagoEfectivoReport.setNumeroCIP(response.getNumeroCIP());
                    logger.info("numeroCIP= " + response.getNumeroCIP());
                } else {
                    consultaPagoEfectivoReport.setNumeroCIP("");
                    logger.info("numeroCIP= ");
                }
                if (response.getIdEstadoCIP() != null) {
                    consultaPagoEfectivoReport.setIdEstadoCIP(response.getIdEstadoCIP());
                    logger.info("idEstadoCIP= " + response.getIdEstadoCIP());
                } else {
                    consultaPagoEfectivoReport.setIdEstadoCIP("");
                    logger.info("idEstadoCIP= ");
                }
                if (response.getEstadoCIP() != null) {
                    consultaPagoEfectivoReport.setEstadoCIP(response.getEstadoCIP());
                    logger.info("estadoCIP= " + response.getEstadoCIP());
                } else {
                    consultaPagoEfectivoReport.setEstadoCIP("");
                    logger.info("estadoCIP= ");
                }

            }
        } catch (Exception e) {
            logger.error("Error :(", e);
        }
        return consultaPagoEfectivoReport;
    }

    public PagoEfectivoConsultaResponse getConsultaPagoEfectivo(PagoEfectivoConsultaRequest request, PagoEfectivo PE) {

        ServiceCallEvent event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
        event.setServiceUrl(PE.getServicesUrl());
        event.setOrderId(request.getOrderId());
        event.setDocNumber(request.getDocNumber());
        ObjectMapper mapper = new ObjectMapper();

        PagoEfectivoConsultaResponse rs = null;

        String requestJSON;
        try {
            requestJSON = mapper.writeValueAsString(request);
        } catch (Exception e) {
            requestJSON = "N/A";
        }

        event.setServiceCode("CONSULTAPAGOEFECTIVO");
        event.setServiceRequest(requestJSON);

        String responseJSON = null;
        BEWSConsultarRequest bEWSConsultarRequest = null;
        BEWSConsultarResponse bEWSConsultarResponse = null;
        try {
            ConsultarCIPService service = new ConsultarCIPService(PE);

            bEWSConsultarRequest = new BEWSConsultarRequest();
            bEWSConsultarRequest.setNumeroCIP(request.getNumeroCIP());

            bEWSConsultarResponse = service.consultar(bEWSConsultarRequest);

            if (bEWSConsultarResponse != null) {
                rs = new PagoEfectivoConsultaResponse();
                if (bEWSConsultarResponse.getNumeroCIP() != null) {
                    rs.setNumeroCIP(bEWSConsultarResponse.getNumeroCIP());
                } else {
                    rs.setNumeroCIP("");
                }
                if (bEWSConsultarResponse.getIdEstadoCIP() != null) {
                    rs.setIdEstadoCIP(bEWSConsultarResponse.getIdEstadoCIP());
                } else {
                    rs.setIdEstadoCIP("");
                }
                if (bEWSConsultarResponse.getEstadoCIP() != null) {
                    rs.setEstadoCIP(bEWSConsultarResponse.getEstadoCIP());
                } else {
                    rs.setEstadoCIP("");
                }
            }

            responseJSON = "N/A";
            try {
                responseJSON = mapper.writeValueAsString(rs);
            } catch (Exception e) {
            }

            event.setMsg("OK");
            event.setResult("OK");

        } catch (Exception e) {
            logger.error("Error :/", e);
            event.setMsg(e.getMessage());
            event.setResult("ERROR");
            responseJSON = "N/A";
        }
        event.setServiceResponse(responseJSON);
        serviceCallEventsService.registerEvent(event);

        return rs;
    }

    public Response<List<TdpVisorResposeData>> getConsultaHogar(TdpVisorRequest request) {
        Response<List<TdpVisorResposeData>> response = new Response<>();
        List<TdpVisorResposeData> tdpVisorResposeDataList = new ArrayList<>();

        List<TdpVisor> lista = tdpVisorRepository.findAllByDepartamentoAndProvinciaAndDistritoAndTipoDocumentoAndDniAndNombreProductoAndOperacionComercial(
                request.getDepartamento(), request.getProvincia(), request.getDistrito(),
                request.getTipoDocumento(), request.getDni(),
                request.getNombreProducto(), request.getOperacionComercial());

        if (lista != null) {
            for (TdpVisor items : lista) {
                TdpVisorResposeData item = new TdpVisorResposeData();
                tdpVisorResposeDataList.add(item.cargaDatos(items));
            }
        }

        response.setResponseCode("01");
        response.setResponseData(tdpVisorResposeDataList);
        return response;
    }

    @Transactional
    public Response<ReportResponse> getReportHistorico(ReportRequest request) throws ApplicationException {
        logger.info("Inicio Initial getReport Service");
        Response<ReportResponse> responseData = new Response<>();
        ReportResponse reportResponse = new ReportResponse();
        try {
            if (request.getDateStart().length() < 11) request.setDateStart(request.getDateStart() + " 00:00:00");

            if (request.getDateEnd().length() < 11) request.setDateEnd(request.getDateEnd() + " 23:59:59");


            List<Object[]> listObjects = orderRepository.getReport(request.getCodAtis(),
                    convertStringToTimestamp_(request.getDateStart()),
                    convertStringToTimestamp_(request.getDateEnd()));

            List<OrderVisor> listOrderVisor = new ArrayList<>();

            for (Object[] obj : listObjects) {
                OrderVisor orderVisor = transformToOrderVisor(obj);
                listOrderVisor.add(orderVisor);
            }

            if ("0".equals(flagUpfront)) {
                for (OrderVisor o : listOrderVisor) {
                    String codigoCip = o.getCodigocip();
                    if (codigoCip != null && !codigoCip.isEmpty()) {
                        PagoEfectivoConsultaResponse pagoEfectivoConsultaResponse = consultarCIP(o.getCodigocip(), o.getId(), o.getDocument());

                        if (pagoEfectivoConsultaResponse != null) {
                            o.setEstadocip(pagoEfectivoConsultaResponse.getEstadoCIP());
                        }
                    }
                }
            }

            List<Object[]> listObjectsColorMes = orderRepository.getColoresMes();
            List<Color> listColoresMes = new ArrayList<>();
            for (Object[] obj : listObjectsColorMes) {
                Color colorMes = transformToColor(obj);
                listColoresMes.add(colorMes);
            }

            List<Object[]> listObjectsColorEstado = orderRepository.getColoresEstado();
            List<Estado> listColoresEstado = new ArrayList<>();
            for (Object[] obj : listObjectsColorEstado) {
                Estado colorEstado = transformToEstado(obj);
                listColoresEstado.add(colorEstado);
            }

            listOrderVisor = getFilteredList(listOrderVisor, request.getState());

            OrderVisor mesActual = null;
            int nclientes = 0;
            int indice = 0;
            List<Mes> mesList = new ArrayList<>();
            // Obtenemos los distintos meses
            List<String> meses = getMeses(listOrderVisor);

            for (String mes : meses) {
                System.out.println("--> mes " + mes);
                Mes objMes = new Mes();
                nclientes = 0;

                List<Dia> diasList = new ArrayList<>();
                // Obtenemos los distintos dias
                List<String> dias = getDias(listOrderVisor, mes);
                for (String dia : dias) {
                    System.out.println("--> dia " + dia);
                    Dia objDia = new Dia();
                    objDia.setName(dia);
                    // Poblamos la lista
                    List<OrderVisor> listas = new ArrayList<>();
                    for (OrderVisor item : listOrderVisor) {
                        if (item.getMes().equalsIgnoreCase(mes) && item.getDia().equals(dia)) {
                            /*OrderVisor lista = new OrderVisor();
                            lista.setId(item.getId());
                            lista.setProducto(item.getProducto());
                            //lista.setEstado(item.getEstado());
                            //lista.setEstado(getEstadoOrderVisor(listToa,listCaida,item.getCodigoPedido(),item.getEstado() ));
                            lista.setEstado(item.getEstado());
                            lista.setAnio(item.getAnio());
                            lista.setMes(item.getMes());
                            lista.setDia(item.getDia());
                            lista.setHora(item.getHora());
                            lista.setCodigoPedido(item.getCodigoPedido());
                            //lista.setEstadocolor(getColorEstado(listColoresEstado,item.getEstado()));
                            lista.setEstadocolor(item.getEstadocolor());
                            lista.setName(item.getName());
                            lista.setDocument(item.getDocument());
                            lista.setTecnologia(item.getTecnologia());
                            lista.setCodigoAtis(item.getCodigoAtis());
                            lista.setIdtransaccion(item.getIdtransaccion());

                            listas.add(lista);*/

                            listas.add(item);
                            mesActual = item;
                            nclientes++;
                        }
                    }
                    objDia.setLista(listas);
                    diasList.add(objDia);
                }

                indice++;
                objMes.setDias(diasList);
                objMes.setOrder(String.valueOf(indice));
                objMes.setAnio(mesActual.getAnio());
                objMes.setMes(mes);
                objMes.setName(getNombreMes(mes));
                objMes.setColor(getColorMes(listColoresMes, mes));

                objMes.setClientestotal(String.valueOf(nclientes));
                objMes.setEstados(getlistColoresEstado(listOrderVisor, mes, listColoresEstado));
                mesList.add(objMes);
            }

            reportResponse.setMeses(mesList);
            responseData.setResponseData(reportResponse);

        } catch (Exception e) {
            logger.error("Error getReport Service", e);
        }

        return responseData;
    }

    private OrderVisor transformToOrderVisor(Object[] obj) throws Exception {
        OrderVisor item = new OrderVisor();

        try {
            item.setId(obj[0] != null ? obj[0].toString() : "");
            item.setName(obj[1] != null ? obj[1].toString() : "");
            item.setDocument(obj[2] != null ? obj[2].toString() : "");
            item.setCodigoPedido(obj[3] != null ? obj[3].toString() : "");
            item.setProducto(obj[4] != null ? obj[4].toString() : "");
            item.setEstado(obj[5] != null ? obj[5].toString() : "");
            item.setIdtransaccion(obj[6] != null ? obj[6].toString() : "");
            item.setCodigoAtis(obj[7] != null ? obj[7].toString() : "");
            item.setAnio(obj[8] != null ? obj[8].toString() : "");
            item.setMes(obj[9] != null ? obj[9].toString() : "");
            item.setDia(obj[10] != null ? obj[10].toString() : "");
            item.setHora(obj[11] != null ? obj[11].toString() : "");

            if (item.getEstado().equals("CAIDA")) {
                item.setTipoCaida("Caida en Registro");
                item.setMotivoCaida(obj[12] != null ? obj[12].toString() : "");
            } else {
                item.setTipoCaida("");
                item.setMotivoCaida("");
            }

            item.setTelefonoCliente(obj[13] != null ? obj[13].toString() : "");
            item.setTelefonoAsignado(obj[14] != null ? obj[14].toString() : "");
            item.setDireccion(obj[15] != null ? obj[15].toString() : "");

            item.setPrecio(obj[16] != null ? obj[16].toString() : "");
            item.setPreciopromocional(obj[17] != null ? obj[17].toString() : "");
            //item.setTecnologia("Inter:"+obj[25].toString() +" / "+ " TV: "+obj[26].toString() );

            Double precio = null;
            try {
                precio = Double.parseDouble(item.getPrecio());
            } catch (Exception ex) {
            }
            Double preciopromo = null;
            try {
                preciopromo = Double.parseDouble(item.getPreciopromocional());
            } catch (Exception ex) {
            }

            DecimalFormat df = new DecimalFormat("#.##");

            item.setOperacioncomercial(obj[18] != null ? obj[18].toString() : "");//TIPO DE INGRESO

            if (obj[19].toString().equalsIgnoreCase("Contado")) {
                item.setPagoAdelantado("S/" + obj[20].toString() + "  CIP dura 30 días");
            } else {
                item.setPagoAdelantado("");
            }
            item.setTipolinea("");

            if (precio != null && preciopromo != null && !precio.equals(preciopromo))
                item.setDescuento(String.valueOf(df.format(precio - preciopromo)));
            else item.setDescuento("");

            item.setPeriodopromocional(obj[21] != null ? obj[21].toString() : "");

            if (!obj[22].toString().equals("")) {
                item.setFacturaelectronica(obj[22].toString());
            } else {
                item.setFacturaelectronica("No");
            }

            if (obj[23].toString().toUpperCase().equals("SI")) {
                item.setPaginasblancas("No");
            } else {
                item.setPaginasblancas("Si");
            }

            item.setfVenta(obj[24] != null ? obj[24].toString() : "");

            String tecInternet = obj[25] != null ? obj[25].toString().trim() : "";
            String tecTv = obj[26] != null ? obj[26].toString().trim() : "";
            if (tecInternet.equalsIgnoreCase("No Aplica")) tecInternet = "";
            if (tecTv.equalsIgnoreCase("No Aplica") || tecTv.equalsIgnoreCase("NA")) tecTv = "";
            String tecnologia = "";
            if (!tecInternet.equals("")) tecnologia = tecInternet;
            else if (!tecTv.equals("")) tecnologia = tecTv;

            item.setTecnologia(tecnologia);

            item.setDecosSmart(obj[27]!=null ? Integer.parseInt(obj[27].toString().trim()) : 0);
            item.setDecosHD(obj[28]!=null ? Integer.parseInt(obj[28].toString().trim()) : 0);
            item.setDecosDVR(obj[29]!=null ? Integer.parseInt(obj[29].toString().trim()) : 0);

            item.setCodigocip(obj[30] != null ? deleteCerosLeft(obj[30].toString()) : "");
            String estadoCip="";
            // Si no hay codigo cip no se debe mostrar el estado cip tampoco
            if(!item.getCodigocip().isEmpty()){
                estadoCip = (obj[31] != null ? obj[31].toString() : "");
                if (estadoCip!= null){
                    switch (estadoCip){
                        case "PE":
                        case "EN":
                            estadoCip = "Pendiente de Pago";
                            break;
                        case "PA":
                            estadoCip = "Pagado";
                            break;
                        case "EX":
                            estadoCip = "Expirado";
                            break;
                        case "EL":
                            estadoCip = "Eliminado";
                    }
                }
            }
            item.setEstadocip(estadoCip);

            item.setTipolinea(obj[32]!=null ? obj[32].toString() : "");
            item.setDebitoAuto(obj[33]!=null ? obj[33].toString() : "");
            item.setTratamientDatos(obj[34]!=null ? obj[34].toString() : "");
            item.setWebParental(obj[35]!=null ? obj[35].toString() : "");
            item.setReciboElect(obj[36]!=null ? obj[36].toString() : "");

            item.setOffline(obj[37]!=null ? obj[37].toString() : "");

            item.setTelefonoCliente2(obj[38] != null ? obj[38].toString() : "");
            item.setOrdenTrabajo(obj[39] != null ? obj[39].toString() : "");
            item.setSvaNombre1(obj[40] != null ? obj[40].toString() : "");
            item.setSvaCantidad1(obj[41] != null ? obj[41].toString() : "");
            item.setSvaNombre2(obj[42] != null ? obj[42].toString() : "");
            item.setSvaCantidad2(obj[43] != null ? obj[43].toString() : "");
            item.setSvaNombre3(obj[44] != null ? obj[44].toString() : "");
            item.setSvaCantidad3(obj[45] != null ? obj[45].toString() : "");
            item.setSvaNombre4(obj[46] != null ? obj[46].toString() : "");
            item.setSvaCantidad4(obj[47] != null ? obj[47].toString() : "");
            item.setSvaNombre5(obj[48] != null ? obj[48].toString() : "");
            item.setSvaCantidad5(obj[49] != null ? obj[49].toString() : "");
            item.setSvaNombre6(obj[50] != null ? obj[50].toString() : "");
            item.setSvaCantidad6(obj[51] != null ? obj[51].toString() : "");
            item.setSvaNombre7(obj[52] != null ? obj[52].toString() : "");
            item.setSvaCantidad7(obj[53] != null ? obj[53].toString() : "");
            item.setSvaNombre8(obj[54] != null ? obj[54].toString() : "");
            item.setSvaCantidad8(obj[55] != null ? obj[55].toString() : "");
            item.setSvaNombre9(obj[56] != null ? obj[56].toString() : "");
            item.setSvaCantidad9(obj[57] != null ? obj[57].toString() : "");
            item.setSvaNombre10(obj[58] != null ? obj[58].toString() : "");
            item.setSvaCantidad10(obj[59] != null ? obj[59].toString() : "");

        } catch (Exception e) {
            logger.error("Error transformToOrderVisor", e);
        }
        return item;
    }

    private Color transformToColor(Object[] obj) throws Exception {
        Color item = new Color();
        try {
            item.setElement(obj[0] != null ? obj[0].toString() : "");
            item.setValue(obj[1] != null ? obj[1].toString() : "");
        } catch (Exception e) {
            logger.error("Error transformToColor", e);
        }
        return item;
    }

    private Estado transformToEstado(Object[] obj) throws Exception {
        Estado item = new Estado();
        try {
            item.setName(obj[0] != null ? obj[0].toString() : "");
            item.setColor(obj[1] != null ? obj[1].toString() : "");
            item.setCantidad(obj[2] != null ? obj[2].toString() : "");
        } catch (Exception e) {
            logger.error("Error transformToEstado", e);
        }
        return item;
    }

    private ToaCaida transformToToa(Object[] obj) throws Exception {
        ToaCaida item = new ToaCaida();
        try {
            item.setCodigo(obj[0] != null ? obj[0].toString() : "");
            item.setEstado(obj[1] != null ? obj[1].toString() : "");
        } catch (Exception e) {
            logger.error("Error transformToToa", e);
        }
        return item;
    }

    private ToaCaida transformToCaida(Object[] obj) throws Exception {
        ToaCaida item = new ToaCaida();
        try {
            item.setCodigo(obj[0] != null ? obj[0].toString() : "");
            item.setEstado(obj[1] != null ? obj[1].toString() : "");
            item.setTipo(obj[2] != null ? obj[2].toString() : "");
        } catch (Exception e) {
            logger.error("Error transformToCaida", e);
        }
        return item;
    }


    private List<String> getMeses(List<OrderVisor> listOrderVisor) {
        List<String> listMeses = new ArrayList<>();
        List<String> meses = new ArrayList<>();

        HashSet<String> hs = new HashSet<>();
        for (OrderVisor orderVisor : listOrderVisor) {
            meses.add(orderVisor.getMes());
        }

        hs.addAll(meses);
        listMeses.addAll(hs);

        return listMeses;
    }

    private List<String> getDias(List<OrderVisor> listOrderVisor, String mes) {
        List<String> listDias = new ArrayList<>();
        List<Integer> lista = new ArrayList<>();
        List<String> dias = new ArrayList<>();

        HashSet<String> hs = new HashSet<>();
        for (OrderVisor orderVisor : listOrderVisor) {
            if (orderVisor.getMes().equalsIgnoreCase(mes)) {
                dias.add(orderVisor.getDia());
            }
        }

        hs.addAll(dias);
        listDias.addAll(hs);

        for (String object : listDias) {
            lista.add(Integer.parseInt(object));
        }
        Collections.sort(lista);
        listDias.clear();
        for (Integer object : lista) {
            listDias.add(String.valueOf(object));
        }

        return listDias;
    }

    public String getNombreMes(String n) {
        String mes = "";
        switch (n) {
            case "1":
                mes = "Enero";
                break;
            case "2":
                mes = "Febrero";
                break;
            case "3":
                mes = "Marzo";
                break;
            case "4":
                mes = "Abril";
                break;
            case "5":
                mes = "Mayo";
                break;
            case "6":
                mes = "Junio";
                break;
            case "7":
                mes = "Julio";
                break;
            case "8":
                mes = "Agosto";
                break;
            case "9":
                mes = "Setiembre";
                break;
            case "10":
                mes = "Octubre";
                break;
            case "11":
                mes = "Noviembre";
                break;
            case "12":
                mes = "Diciembre";
                break;
        }
        return mes;
    }

    private String getColorMes(List<Color> lista, String clave) {
        String color = "";

        for (Color item : lista) {
            if (item.getElement().equalsIgnoreCase(clave)) {
                color = item.getValue();
            }
        }

        return color;
    }

    private String getColorEstado(List<Estado> lista, String estado) {
        String color = "";

        for (Estado item : lista) {
            if (item.getName().equalsIgnoreCase(estado)) {
                color = item.getColor();
            }
        }

        return color;
    }

    public List<Estado> getlistColoresEstado(List<OrderVisor> lista, String mes, List<Estado> estados) {
        List<String> listaAllEstadosMeses = new ArrayList<>();
        List<Estado> listaEstados = new ArrayList<>();

        for (OrderVisor item : lista) {
            if (item.getMes().equals(mes)) {
                listaAllEstadosMeses.add(item.getEstado());
            }
            item.setEstadocolor(getColorEstado(estados, item.getEstado()));
        }

        for (Estado item : estados) {
            Estado estado = new Estado();
            estado.setName(item.getName());
            estado.setColor(item.getColor());
            estado.setCantidad(String.valueOf(Collections.frequency(listaAllEstadosMeses, item.getName())));
            listaEstados.add(estado);
        }

        return listaEstados;
    }

    private String getEstadoOrderVisor(List<ToaCaida> listToa, List<ToaCaida> listCaida, String code) {
        List<String> filteredListToa = new ArrayList<>();
        List<ToaCaida> filteredListcaida = new ArrayList<>();
        String estadoToa = "";
        String estadoCaida = "";
        String tipoCaida = "";
        String motivoCaida = "";

        for (ToaCaida item : listToa) {
            if (item.getCodigo().equals(code)) {
                filteredListToa.add(item.getEstado());
            }
        }

        if (Collections.frequency(filteredListToa, "IN_TOA") > 0 && Collections.frequency(filteredListToa, "WO_COMPLETED") == 0) {
            estadoToa = "EN PROCESO";
        }

        if (Collections.frequency(filteredListToa, "WO_COMPLETED") > 0) {
            return "INSTALADO";
        }

        for (ToaCaida item : listCaida) {
            if (item.getCodigo().equals(code)) {
                filteredListcaida.add(item);
            }
        }

        for (ToaCaida item : filteredListcaida) {
            //ESTADO        TIPO CAIDA              MOTIVO
            //RED_ASIGN // CAIDA EN ASINGNACION  // RETRASO EXESIVO
            //DEV_COM_CL ||  DEV_COM_NO || DEV_TEC // CAIDA INSTALACION // DEV_COM_CL(A PEDIDO DEL CLIENTE) DEV_COM_NO(CLIENTE NO CONTACTADO) DEV_TEC(NO HUBO FACILIDADES TECNICAS)

            if ((item.getTipo().equals("DEV_TEC") || item.getTipo().equals("DEV_COM_CL") || item.getTipo().equals("DEV_COM_NO")) && item.getEstado() == "true") {
                estadoCaida = "CAIDA";
                tipoCaida = "CAIDA EN INSTALACION";
                if (item.getTipo().equals("DEV_TEC")) {
                    motivoCaida = "NO HUBO FACILIDADES TECNICAS";
                } else if (item.getTipo().equals("DEV_COM_CL")) {
                    motivoCaida = "A PEDIDO DEL CLIENTE";
                } else if (item.getTipo().equals("DEV_COM_NO")) {
                    motivoCaida = "CLIENTE NO CONTACTADO";
                }

                break;
            } else if (item.getTipo().equals("RET_ASIGN")) {
                estadoCaida = "CAIDA";
                tipoCaida = "CAIDA EN ASIGNACION";
                motivoCaida = "RETRASO EXCESIVO";
                break;
            }
        }

        if (estadoCaida.equalsIgnoreCase("")) {
            return estadoToa;
        } else {

            return estadoCaida + "-" + tipoCaida + "-" + motivoCaida;
        }

    }

    private List<OrderVisor> getFilteredList(List<OrderVisor> list, String state) throws Exception {
        List<String> listCommon = new ArrayList<>();
        List<OrderVisor> listIngresadoCaida = new ArrayList<>();
        List<OrderVisor> listFinal = new ArrayList<>();
        try {
            for (OrderVisor obj : list) {
                if (obj.getEstado().equals("INGRESADO")) {
                    listCommon.add(obj.getCodigoPedido());
                    listIngresadoCaida.add(obj);
                }
            }

            List<Object[]> listObjectsToa = new ArrayList<>();
            if (listCommon.size() > 0)
                listObjectsToa = orderRepository.getPedidosEstadosToa(listCommon);

            List<ToaCaida> listToa = new ArrayList<>();
            for (Object[] obj : listObjectsToa) {
                ToaCaida toaCaida = transformToToa(obj);
                listToa.add(toaCaida);
            }


            List<Object[]> listObjectsCaida = new ArrayList<>();
            if (listCommon.size() > 0)
                listObjectsCaida = orderRepository.getPedidosEnCaida(listCommon);

            List<ToaCaida> listCaida = new ArrayList<>();
            for (Object[] obj : listObjectsCaida) {
                ToaCaida toaCaida = transformToCaida(obj);
                listCaida.add(toaCaida);
            }


            for (OrderVisor item : listIngresadoCaida) {
                String estado = "";
                estado = getEstadoOrderVisor(listToa, listCaida, item.getCodigoPedido());
                if (estado != "") {
                    for (OrderVisor items : list) {
                        if (item.getCodigoPedido().equals(items.getCodigoPedido())) {
                            String[] parts = estado.split("-");
                            String part1estado = parts[0]; // 123
                            String part2tipo = parts.length > 2 ? parts[1] : "";
                            String part3motivo = parts.length > 2 ? parts[2] : "";
                            items.setEstado(part1estado);
                            items.setTipoCaida(part2tipo);
                            items.setMotivoCaida(part3motivo);
                        }
                    }
                }
            }

            if (state != null) {
                switch (state) {
                    case "0":
                        listFinal = list;
                        break;
                    case "1":
                        for (OrderVisor obj : list)
                            if (obj.getEstado().equals("SOLICITADO"))
                                listFinal.add(obj);
                        break;
                    case "2":
                        for (OrderVisor obj : list)
                            if (obj.getEstado().equals("INGRESADO"))
                                listFinal.add(obj);
                        break;
                    case "3":
                        for (OrderVisor obj : list)
                            if (obj.getEstado().equals("EN PROCESO"))
                                listFinal.add(obj);
                        break;
                    case "4":
                        for (OrderVisor obj : list)
                            if (obj.getEstado().equals("INSTALADO"))
                                listFinal.add(obj);
                        break;
                    case "5":
                        for (OrderVisor obj : list)
                            if (obj.getEstado().equals("CAIDA"))
                                listFinal.add(obj);
                        break;
                }
            } else {
                listFinal = list;
            }

        } catch (Exception e) {
            logger.error("Error getFilteredList", e);
        }

        return listFinal;
    }


    //@Transactional
    public Response<ReportResponse2> getReportTracking(CodAtisRequest request) throws ApplicationException {
        logger.info("Inicio Initial getReport Service");
        Response<ReportResponse2> responseData = new Response<>();
        ReportResponse2 reportResponse = new ReportResponse2();
        try {

            Date fecha = new Date();

            String fechaInicio = fechaInicioFin(fecha, true);// "2018-07-01";
            String fechaFin = fechaInicioFin(fecha, false);//  "2018-07-30";

            List<Object[]> listObjects = orderRepository.getReport(request.getCodAtis(),
                    convertStringToTimestamp(fechaInicio),
                    convertStringToTimestamp(fechaFin ));

            List<OrderVisor> listOrderVisor = new ArrayList<>();

            TdpSalesAgent tdpSalesAgent = tdpSalesAgentRepository.findOneByCodigoAtis(request.getCodAtis());

            for (Object[] obj : listObjects) {

                if(!tdpSalesAgent.getType_flujo_tipificacion().toUpperCase().equals("PRESENCIAL") &&
                        (obj[60].toString().equals("1") || obj[60].toString().equals("8"))
                        && obj[5].toString().equals("SOLICITADO"))
                    continue;

                OrderVisor orderVisor = transformToOrderVisor(obj);
                listOrderVisor.add(orderVisor);

            }

            if ("0".equals(flagUpfront)) {
                for (OrderVisor o: listOrderVisor) {
                    String codigoCip = o.getCodigocip();
                    if (codigoCip != null && !codigoCip.isEmpty()) {
                        PagoEfectivoConsultaResponse pagoEfectivoConsultaResponse = consultarCIP(o.getCodigocip(), o.getId(), o.getDocument());

                        if (pagoEfectivoConsultaResponse != null) {
                            o.setEstadocip(pagoEfectivoConsultaResponse.getEstadoCIP());
                        }
                    }
                }
            }

            List<Object[]> listObjectsColorEstado = orderRepository.getColoresEstado();

            listOrderVisor = getFilteredList(listOrderVisor, null);

            SimpleDateFormat formatMes = new SimpleDateFormat("MM");
            Integer mes = Integer.valueOf(formatMes.format(fecha));

            SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
            String anio = formatAnio.format(fecha);

            reportResponse.setMes(getNombreMes(mes.toString()) + " " + anio);
            List<Estados> listEstados = listEstados(listOrderVisor, listObjectsColorEstado);
            reportResponse.setEstados(listEstados);
            reportResponse.setClientesTotales(listOrderVisor.size());

            responseData.setResponseData(reportResponse);

        } catch (Exception e) {
            logger.error("Error getReport Service", e);
        }
        return responseData;
    }

    private List<Estados> listEstados(List<OrderVisor> listOrderVisor, List<Object[]> listColores) {

        List<Estados> estados = new ArrayList<>();
        List<Estados> estadosColor = new ArrayList<>();
        List<List<OrderVisor>> lisOrderVisor = new ArrayList<>();
        List<OrderVisor> listSolicitado = new ArrayList<>();
        List<OrderVisor> listIngreso = new ArrayList<>();
        List<OrderVisor> listEnProceso = new ArrayList<>();
        List<OrderVisor> listIntalado = new ArrayList<>();
        List<OrderVisor> listCaida = new ArrayList<>();
        List<OrderVisor> listOffline = new ArrayList<>();

        for (OrderVisor item : listOrderVisor) {

            if (item.getEstado().equals("SOLICITADO")) {
                listSolicitado.add(item);
            }
            if (item.getEstado().equals("INGRESADO")) {
                listIngreso.add(item);
            }
            if (item.getEstado().equals("EN PROCESO")) {
                listEnProceso.add(item);
            }
            if (item.getEstado().equals("INSTALADO")) {
                listIntalado.add(item);
            }
            if (item.getEstado().equals("CAIDA")) {
                listCaida.add(item);
            }
            if (item.getEstado().equals("OFFLINE")) {
                listOffline.add(item);
            }
        }

        for (Object[] obj : listColores) {
            Estados colorEstado = new Estados();
            colorEstado.setNombre(obj[0].toString());
            estadosColor.add(colorEstado);
        }

        lisOrderVisor.add(listSolicitado);
        lisOrderVisor.add(listIngreso);
        lisOrderVisor.add(listEnProceso);
        lisOrderVisor.add(listIntalado);
        lisOrderVisor.add(listCaida);
        lisOrderVisor.add(listOffline);
        for (int i = 0; estadosColor.size() > i; i++) {
            Estados add = new Estados();
            add.setNombre(estadosColor.get(i).getNombre());
            estadosColor.get(i).getNombre();
            add.setLista(lisOrderVisor.get(i));
            add.setCantidad(lisOrderVisor.get(i).size());
            add.setColor(colorEstado(listColores, estadosColor.get(i).getNombre()));
            cargaColorInterno(lisOrderVisor.get(i), add.getColor());
            estados.add(add);
        }
        return estados;
    }

    private void cargaColorInterno(List<OrderVisor> orderVisors, String color) {
        if (orderVisors.size() > 0) {
            for (OrderVisor item : orderVisors) {
                item.setEstadocolor(color);
            }
        }
    }

    private String colorEstado(List<Object[]> listColores, String nombre) {
        String color = "";
        for (Object[] obj : listColores) {
            if (obj[0].toString().equals(nombre)) {
                //add.setCantidad(nsolicitado);
                color = (obj[1].toString());
                break;
            }
        }
        return color;
    }

    private static String fechaInicioFin(Date fecha, boolean esInicio) {
        SimpleDateFormat formatAnio = new SimpleDateFormat("yyyy");
        String anio = formatAnio.format(fecha);

        SimpleDateFormat formatMes = new SimpleDateFormat("MM");
        String mes = formatMes.format(fecha);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        Integer actual = esInicio ? calendar.getActualMinimum(calendar.DAY_OF_MONTH) : calendar.getActualMaximum(calendar.DAY_OF_MONTH);
        String dia = actual.toString();
        String fechaProcesada = anio + "-" + mes + "-" + dia;
        return fechaProcesada; // Devuelve el objeto Date con los nuevos días añadidos
    }

    @Transactional
    public Response<RecuperoResponse> generarRecuperoVenta(RecuperoRequest request) {
        Response<RecuperoResponse> response = new Response<>();
        RecuperoResponse recupero = new RecuperoResponse();

        switch (request.getMotivoCaida()) {
            case "SIN TELEFONO DE CONTACTO":
                customerRepository.updateCustomerPhone(request.getTelefono(), request.getDocnumber());
                tdpOrderRepository.updateTdpOrderTelefono(request.getTelefono(),request.getOrderId());
                tdpVisorRepository.updateTelefonoAndIdVisor(request.getTelefono(), request.getOrderId());

                orderRepository.updateFlagRecuperoOrder(request.getOrderId());
                break;
            case "DIRECCION INCOMPLETA/INCORRECTA":
                tdpVisorRepository.updateDireccionTdpVisor(request.getDireccion(), request.getOrderId());
                orderDetailRepository.updateAddressOrderDetail(request.getDireccion(),request.getOrderId());
                tdpOrderRepository.updateTdpOrderAddress(request.getDireccion(),request.getOrderId());

                orderRepository.updateFlagRecuperoOrder(request.getOrderId());
                break;
            case "DEUDA EN ATIS/CMS":
                tdpOrderRepository.updateTdpOrderFechaPagoCmsAtis(request.getFechaPago(), request.getOrderId());
                tdpVisorRepository.updateStateTdpVisor(request.getOrderId());

                orderRepository.updateFlagRecuperoOrder(request.getOrderId());
                break;
            case "SUPERO MAXIMO DE DECOS PERMITIDOS":
                orderDetailRepository.updateDecosSvaOrderDetail(request.getSvaSmart(), request.getSvaHd(), request.getSvaDvr(), request.getOrderId());
                tdpVisorRepository.updateStateTdpVisor(request.getOrderId());

                orderRepository.updateFlagRecuperoOrder(request.getOrderId());
                break;
        }

        recupero.setEstado("OK");
        recupero.setIdOrder(request.getOrderId());
        recupero.setMotivo(request.getMotivoCaida());
        recupero.setRespuesta("Esta venta esta en proceso de recupero, revisar la venta durante 2 horas");
        response.setResponseData(recupero);

        return response;
    }

    public SalesReportTypeResponse obtieneFiltrosReporte(SalesReportTypeRequest request) {
        logger.info("ini metodo: obtieneFiltrosReporte");
        logger.info("---->>> body request.getReportTypeId: " + request.getReportTypeId());
        SalesReportTypeResponse response = new SalesReportTypeResponse();
        List<Object[]> filterObjects = orderRepository.getFiltrosReporte(request.getReportTypeId());
        List<Cell> filterRS = new ArrayList<>();
        List<String> filterRSL = new ArrayList<>();

        if (filterObjects != null) {
            for (Object[] obj : filterObjects) {
                //logger.info("ini metodo: transformToParameter");
                Cell filterRow = transformToCell(obj);
                //logger.info("fin metodo: transformToParameter");
                if (filterRow != null) {
                    //logger.info("filterRow.getRow = "+filterRow.getRow());
                    //logger.info("filterRow.getColumn = "+filterRow.getColumn());
                    if (filterRow.getRow().equals("FILTRO")) {
                        filterRSL.add(filterRow.getColumn());
                    } else {
                        filterRS.add(filterRow);
                    }
                }
            }
        }
        System.out.println("Tamaño filterRSL: " + filterRS.size());
        System.out.println("Tamaño filterRS: " + filterRSL.size());
        response.setActiveFilterList(filterRSL);
        response.setActiveFilterData(filterRS);
        response.setReporteVentaList(null);

        return response;
    }

    public SalesReportTypeResponse obtieneData(SalesReportTypeRequest request) {

        logger.info("ini metodo: obtieneFiltrosReporte");
        logger.info("---->>> body request.getReportTypeId: " + request.getReportTypeId());
        SalesReportTypeResponse response = new SalesReportTypeResponse();
        List<Cell> dataRS = new ArrayList<>();

        List<Object[]> dataObjects = orderRepository.
                getDataReporte(
                        request.getReportTypeId()
                        //null,
                        //null
                        //,null
                );

        if (dataObjects != null) {
            logger.info("ini metodo: transformToCell");
            for (Object[] obj : dataObjects) {

                Cell cell = transformToCell(obj);
                if (cell != null) {
                    logger.info("cell.getRow: " + cell.getRow());
                    logger.info("cell.getColumn: " + cell.getColumn());
                    logger.info("cell.getValue: " + cell.getValue());
                    dataRS.add(cell);
                }
            }
        }

        Parameter dtv = new Parameter();
        dtv.setStrValue("OPERACION COMERCIAL");
        dtv.setElement(request.getReportTypeId());
        List<Parameter> dtvList = new ArrayList<>();
        dtvList.add(dtv);

        ReporteVenta rv = new ReporteVenta();
        rv.setDataReporteventaList(dataRS);
        rv.setDetalleReporteVentaList(dtvList);

        List<ReporteVenta> rvList = new ArrayList<>();
        rvList.add(rv);

        response.setActiveFilterList(null);
        response.setActiveFilterData(null);
        response.setReporteVentaList(rvList);

        return response;
    }

    private Cell transformToCell(Object[] obj) {

        Cell item = new Cell();

        try {
            item.setRow(obj[0] != null ? obj[0].toString() : "");
            item.setColumn(obj[1] != null ? obj[1].toString() : "");
            item.setValue(obj[2] != null ? obj[2].toString() : "");
        } catch (Exception e) {
            logger.error("Error transformToCell", e);
            return null;
        }
        return item;
    }

    public ReportFiltersResponse getFiltersReports(ReportFiltersRequest request) {

        logger.info("Ingreso del metodo getFiltersReports");
        logger.info("Codigo atis: " + request.getCodatis() + "--- nivel: " + request.getNiveles());
        ReportFiltersResponse response = new ReportFiltersResponse();

        String canales = getCanales(request);
        String socios = getSocio(request);
        String regiones = getRegion(request);
        String zonales = getZonal(request);
        String ptosVenta = getPtoVenta(request);

        HashMap<String,List<Dates>> data;

        DateTimeZone zone = DateTimeZone.forID("America/Lima");
        DateTime dt = new DateTime(zone);
        String formattedTime=dt.getYear() +"-"+
                            String.format("%02d" , dt.getMonthOfYear())+"-"+String.format("%02d" , dt.getDayOfMonth());

        switch (request.getNiveles()) {
            case "5"://SOCIO
                data=getData(canales,"-1",regiones,zonales,ptosVenta);
                response.setCanal(data.get("canales"));
                response.setRegion(data.get("regiones"));
                response.setZonal(data.get("zonales"));
                response.setPtoVenta(data.get("ptoVenta"));
                //FECHAS
                response.setFechaInicio(formattedTime);
                response.setFechaFinal(formattedTime);
                break;

            case "3"://COORDINADOR
                data=getData(canales,socios,regiones,zonales,ptosVenta);
                response.setCanal(data.get("canales"));
                response.setRegion(data.get("regiones"));
                response.setZonal(data.get("zonales"));
                response.setPtoVenta(data.get("ptoVenta"));
                response.setSocio(data.get("socio"));
                response.setCampania(getCampania(canales));
                response.setOpComercial(getOpComercial(canales));
                //FECHAS
                response.setFechaInicio(formattedTime);
                response.setFechaFinal(formattedTime);  break;
            case "4"://SUPERVISOR
                response.setFechaInicio(formattedTime);
                response.setFechaFinal(formattedTime);
                break;
            default:
                response = null;
                break;
        }

        return response;
    }

    private HashMap<String,List<Dates>> getData(String canales,String socios,String regiones,String zonales,String ptosVenta){

        HashMap<String,List<Dates>> data = new HashMap<>();

        data.put("canales",new ArrayList<>());
        data.put("socio",new ArrayList<>());
        data.put("regiones",new ArrayList<>());
        data.put("zonales",new ArrayList<>());
        data.put("ptoVenta",new ArrayList<>());

        try (Connection con = Database.datasource().getConnection()) {
            String query = "SELECT distinct canal,entidad,zona,zonal,nompuntoventa FROM ibmx_a07e6d02edaf552.tdp_sales_agent " +
                    "where niveles='1' "+
                    (canales.equals("")?" AND canal IS NOT NULL ":(" AND canal in ("+canales+")")) +
                    (regiones.equals("")?" AND zona IS NOT NULL ":(" AND zona in ("+regiones+")")) +
                    (zonales.equals("")?" AND zonal IS NOT NULL ":(" AND zonal in ("+zonales+")")) +
                    (ptosVenta.equals("")?" AND nompuntoventa IS NOT NULL ":(" AND nompuntoventa in ("+ptosVenta+")")) +
                    ((socios.equals("")||socios.equals("-1"))?" AND entidad IS NOT NULL ":(" AND entidad in ("+socios+")")) +
                    " order by canal,entidad,zona,zonal,nompuntoventa";
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    String canal = rs.getString("canal");
                    String socio = rs.getString("entidad");
                    String region = rs.getString("zona");
                    String zonal = rs.getString("zonal");
                    String nompuntoventa = rs.getString("nompuntoventa");

                    if (data.get("canales").stream().filter(t -> t.getCode().equals("'"+canal+"'")).
                            collect(Collectors.toList()).size()==0){
                        Dates dates = new Dates();
                        dates.setCode("'"+canal+"'");
                        dates.setName(canal);
                        dates.setCodeHereditary("");
                        data.get("canales").add(dates);
                    }
                    if (data.get("socio").stream().filter(t -> (t.getCode().equals("'"+socio+"'"))).
                            collect(Collectors.toList()).size()==0){
                        Dates dates = new Dates();
                        dates.setCode("'"+socio+"'");
                        dates.setName(socio);
                        dates.setCodeHereditary("'"+canal+"'");
                        data.get("socio").add(dates);
                    }else{
                        Dates copy= data.get("socio").stream().filter(t -> (t.getCode().equals("'"+socio+"'"))).
                                collect(Collectors.toList()).get(0);
                        if(copy.getCodeHereditary().indexOf("'"+canal+"'")==-1){
                            copy.setCodeHereditary(copy.getCodeHereditary()+",'"+canal+"'");
                            data.get("socio").stream().filter(t -> (t.getCode().equals("'"+socio+"'"))).
                                    collect(Collectors.toList()).set(0,copy);
                        }
                    }
                    if (data.get("regiones").stream().filter(t -> (t.getCode().equals("'"+region+"'") )).
                            collect(Collectors.toList()).size()==0){
                        Dates dates = new Dates();
                        dates.setCode("'"+region+"'");
                        dates.setName(region);
                        if (socios.equals("-1")){
                            dates.setCodeHereditary("'"+canal+"'");
                        }else{
                            dates.setCodeHereditary("'"+socio+"'");
                        }
                        data.get("regiones").add(dates);
                    }else{
                        Dates copy= data.get("regiones").stream().filter(t -> (t.getCode().equals("'"+region+"'"))).
                                collect(Collectors.toList()).get(0);
                        String parent;
                        if (socios.equals("-1")){
                            parent=canal;
                        }else{
                            parent=socio;
                        }
                        if(copy.getCodeHereditary().indexOf("'"+parent+"'")==-1){
                            copy.setCodeHereditary(copy.getCodeHereditary()+",'"+parent+"'");
                            data.get("regiones").stream().filter(t -> (t.getCode().equals("'"+region+"'"))).
                                    collect(Collectors.toList()).set(0,copy);
                        }
                    }
                    if (data.get("zonales").stream().filter(t -> t.getCode().equals("'"+zonal+"'")
                                                        &&  t.getCodeHereditary().equals("'"+region+"'")).
                            collect(Collectors.toList()).size()==0){
                        Dates dates = new Dates();
                        dates.setCode("'"+zonal+"'");
                        dates.setName(zonal);
                        dates.setCodeHereditary("'"+region+"'");
                        data.get("zonales").add(dates);
                    }
                    if (data.get("ptoVenta").stream().filter(t -> t.getCode().equals("'"+nompuntoventa+"'")
                                                              &&  t.getCodeHereditary().equals("'"+zonal+"'")).
                            collect(Collectors.toList()).size()==0){
                        Dates dates = new Dates();
                        dates.setCode("'"+nompuntoventa+"'");
                        dates.setName(nompuntoventa);
                        dates.setCodeHereditary("'"+zonal+"'");
                        data.get("ptoVenta").add(dates);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getData.", e);
        }
        return data;
    }

    private List<Dates> getOpComercial(String canales) {
        List<Dates> data = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct commercialoperation from ibmx_a07e6d02edaf552.tdp_catalog ";
            if (!canales.equals("")){
                query+="where (upper(canal) in (SELECT distinct canalequivcampania " +
                        "FROM ibmx_a07e6d02edaf552.tdp_sales_agent WHERE canal IN ("+canales+")) or upper(canal) = 'TODOS')";
            }

            try (PreparedStatement ps = con.prepareStatement(query)) {
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Dates dta = new Dates();
                    dta.setName(rs.getString(1));
                    dta.setCode("'"+rs.getString(1)+"'");
                    dta.setCodeHereditary("");
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getOpComercial.", e);
        }

        return data;
    }

    private List<Dates> getCampania(String canales) {
        List<Dates> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "SELECT DISTINCT" +
                            " upper(campaign)" +
                            " FROM" +
                            " ibmx_a07e6d02edaf552.order ";
            if (!canales.equals("")){
                query+=" WHERE canal IN ("+canales+")";
            }
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    if (rs.getString(1)!=null) {
                        Dates dta = new Dates();
                        dta.setName(rs.getString(1));
                        dta.setCode("'" + rs.getString(1) + "'");
                        dta.setCodeHereditary("");
                        data.add(dta);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getOpComercial.", e);
        }
        return data;
    }

    private String getCanales(ReportFiltersRequest request) {
        String canales = "";
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select canal from ibmx_a07e6d02edaf552.tdp_sales_agent " +
                    "where codatis = ?";
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, request.getCodatis());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    canales = rs.getString("canal");
                    if (canales.equals("TODOS")){
                        canales="";
                    }else{
                        String[] temp = canales.split(",");
                        canales = "";
                        for (int i =0 ; i < temp.length;i++){
                            canales =  canales + "'" + temp[i] + "',";
                        }
                        canales = canales.substring(0, canales.length() - 1);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getCanales.", e);
        }
        return canales;
    }

    private String getSocio(ReportFiltersRequest request) {
        String socio = "";
        try (Connection con = Database.datasource().getConnection()) {
            String query = "SELECT entidad FROM ibmx_a07e6d02edaf552.tdp_sales_agent WHERE codatis = ?";
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, request.getCodatis());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    socio = rs.getString("entidad");
                    if (socio.equals("TODOS")){
                        socio="";
                    }else{
                        String[] temp = socio.split(",");
                        socio = "";
                        for (int i =0 ; i < temp.length;i++){
                            socio = socio+ "'" + temp[i] + "',";
                        }
                        socio = socio.substring(0, socio.length() - 1);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getSocio.", e);
        }
        return socio;
    }

    private String getRegion(ReportFiltersRequest request) {
        String socio = "";
        try (Connection con = Database.datasource().getConnection()) {
            String query = "SELECT zona FROM ibmx_a07e6d02edaf552.tdp_sales_agent WHERE codatis = ?";
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, request.getCodatis());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    socio = rs.getString("zona");
                    if (socio.equals("TODOS")){
                        socio="";
                    }else{
                        String[] temp = socio.split(",");
                        socio = "";
                        for (int i =0 ; i < temp.length;i++){
                            socio = socio + "'" + temp[i] + "',";
                        }
                        socio = socio.substring(0, socio.length() - 1);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getRegion.", e);
        }
        return socio;
    }

    private String getZonal(ReportFiltersRequest request ) {
        String socio = "";
        try (Connection con = Database.datasource().getConnection()) {
            String query = "SELECT zonal FROM ibmx_a07e6d02edaf552.tdp_sales_agent WHERE codatis = ?";
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, request.getCodatis());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    socio = rs.getString("zonal");
                    if (socio.equals("TODOS")){
                        socio="";
                    }else{
                        String[] temp = socio.split(",");
                        socio = "";
                        for (int i =0 ; i < temp.length;i++){
                            socio =  socio + "'" + temp[i] + "',";
                        }
                        socio = socio.substring(0, socio.length() - 1);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getZonal.", e);
        }
        return socio;
    }

    private String getPtoVenta(ReportFiltersRequest request) {
        String socio = "";
        try (Connection con = Database.datasource().getConnection()) {
            String query = "SELECT nompuntoventa FROM ibmx_a07e6d02edaf552.tdp_sales_agent WHERE codatis = ?";
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, request.getCodatis());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    socio = rs.getString("nompuntoventa");
                    if (socio.equals("TODOS")){
                        socio="";
                    }else{
                        String[] temp = socio.split(",");
                        socio = "";
                        for (int i =0 ; i < temp.length;i++){
                            socio =   socio + "'" + temp[i] + "',";
                        }
                        socio = socio.substring(0, socio.length() - 1);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getPtoVenta.", e);
        }
        return socio;
    }

    public SalesReportTypeResponse getDate(ReportVentasRequest request) {
        logger.info("Ingreso del metodo getFiltersReports");
        SalesReportTypeResponse response = new SalesReportTypeResponse();
        List<Cell> data = new ArrayList<>();
        ReporteVenta report = new ReporteVenta();
        List<ReporteVenta> reportes = new ArrayList<>();

        switch (request.getCodeFilter()) {
            case "1":
                List<Cell> preventa = getPreventa(request);
                for (Cell dta: preventa){
                    data.add(dta);
                }
                /*List<Cell> venta = getVenta(request);
                for (Cell dta: venta){
                    data.add(dta);
                }*/
                List<Cell> ingresado = getIngresadoPedido(request);
                for (Cell dta: ingresado){
                    data.add(dta);
                }
                /*List<Cell> caida = getCaidaSinPedido(request);
                for (Cell dta: caida){
                    data.add(dta);
                }
                List<Cell> instalado = getInstalado(request);
                for (Cell dta: instalado){
                    data.add(dta);
                }
                List<Cell> cancelado = getCancelado(request);
                for (Cell dta: cancelado){
                    data.add(dta);
                }*/
                report.setDataReporteventaList(data);
                reportes.add(report);
                response.setReporteVentaList(reportes);
                break;
            case "2":
                List<Cell> opComercial = getTrazabilidad(request);
                report.setDataReporteventaList(opComercial);
                reportes.add(report);
                response.setReporteVentaList(reportes);
                break;
            case "3":
                List<Cell> preventaProducto = getPreventaProducto(request);
                for (Cell dta: preventaProducto){
                    data.add(dta);
                }

                /*List<Cell> ventaProducto = getVentaProducto(request);
                for (Cell dta: ventaProducto){
                    data.add(dta);
                }*/

                List<Cell> ingresadoProducto = getIngresadoPedidoProducto(request);
                for (Cell dta: ingresadoProducto){
                    data.add(dta);
                }

                /*List<Cell> caidaProducto = getCaidaSinPedidoProducto(request);
                for (Cell dta: caidaProducto){
                    data.add(dta);
                }

                List<Cell> instaladoProducto = getInstaladoProducto(request);
                for (Cell dta: instaladoProducto){
                    data.add(dta);
                }

                List<Cell> canceladoProducto = getCanceladoProducto(request);
                for (Cell dta: canceladoProducto){
                    data.add(dta);
                }*/
                report.setDataReporteventaList(data);
                reportes.add(report);
                response.setReporteVentaList(reportes);
                break;
            case "4":
                List<Cell> preventaSocio = getPreventaSocio(request);
                for (Cell dta: preventaSocio){
                    data.add(dta);
                }

                /*List<Cell> ventaSocio = getVentaSocio(request);
                for (Cell dta: ventaSocio){
                    data.add(dta);
                }*/

                List<Cell> ingresadoSocio = getIngresadoPedidoSocio(request);
                for (Cell dta: ingresadoSocio){
                    data.add(dta);
                }

                /*List<Cell> caidaSocio = getCaidaSinPedidoSocio(request);
                for (Cell dta: caidaSocio){
                    data.add(dta);
                }

                List<Cell> instaladoSocio = getInstaladoSocio(request);
                for (Cell dta: instaladoSocio){
                    data.add(dta);
                }

                List<Cell> canceladoSocio = getCanceladoSocio(request);
                for (Cell dta: canceladoSocio){
                    data.add(dta);
                }*/
                report.setDataReporteventaList(data);
                reportes.add(report);
                response.setReporteVentaList(reportes);
                break;
        }

        return response;
    }

    public SalesReportExportResponseBody getDataExport(ReportVentasRequest request){
        String hour_minute = customerRepository.getValorParametro("carga_audio_hora_restriccion");
        long tiempoTotal = 0;
        String array[];
        Date fechaServidor = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String fechaServConvertida = sdf.format(fechaServidor);
        Integer hora = 0,minuto = 0;

        try{
            array = hour_minute.split(":");
            hora = Integer.parseInt(array[0]);
            minuto = Integer.parseInt(array[1]);
            if(hora > 0 || minuto > 0){
                tiempoTotal = TimeUnit.HOURS.toMinutes(hora) + minuto;
            }

        }catch (Exception e){
            tiempoTotal = 0;
        }

        logger.info("Ingreso del metodo de exportar");
        List<SalesReportExportResponse> listado = new ArrayList<>();
        SalesReportExportResponseBody bodyResponse = new SalesReportExportResponseBody();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct on (a.id_visor) a.* from (SELECT distinct " +
                    "id_visor, " +
                    "o.appcode,v.id_grabacion,o.campaign,UPPER(v.operacion_comercial),v.tipo_documento,v.dni, " +
                    "v.cliente,C.birthdate,C.customerphone,C.customerphone2,C.email,o.cmscustomer,tdo.user_region, " +
                    "tdo.user_zonal,o.canal,UPPER(o.entidad),o.nompuntoventa,tsa.codpuntoventa,user_nombre AS nombrevendedor, " +
                    "o.userid,tsa.codcms,tsa.segmento,v.tipo_producto,v.sub_producto,v.nombre_producto,v.codigo_pedido, " +
                    "tdo.product_tec_inter,tdo.product_tec_tv,TO_CHAR(o.registrationdate,'dd/mm/YYYY') Fecha_Venta, " +
                    "TO_CHAR(o.registrationdate,'HH:mm:ss') Hora_Venta,TO_CHAR(v.fecha_registrado,'dd/mm/YYYY') Fecha_Registro, " +
                    "TO_CHAR(v.fecha_registrado,'HH:mm:ss') Hora_Registro,estado_solicitud as estado_venta, " +
                    "(CASE WHEN  toa.status_toa = 'IN_TOA' THEN 'En Proceso' WHEN  toa.status_toa is null " +
                    " or toa.status_toa in ('WO_COMPLETED','WO_INIT') THEN '-' ELSE '--' END) estado_toa, " +
                    "motivo_estado,o.paymentmode, tdo.order_experto,pe.codpago,pe.total," +
                    "(CASE pe.status WHEN 'PE' THEN 'Generando' WHEN 'PA' THEN 'Pagado' WHEN 'EX' THEN " +
                    " 'Expirado' WHEN 'EN' THEN 'Generado' ELSE '-' END) status, " +
                    "tdo.product_precio_normal,tdo.product_precio_promo, " +
                    "tdo.order_gps_x,tdo.order_gps_y,tdo.address_principal,tdo.address_referencia,tdo.address_distrito, " +
                    "tdo.address_provincia,tdo.address_departamento,od.sendContracts envio_contrato, " +
                    "tdo.affiliation_data_protection as ProteccionDatos,tdo.affiliation_electronic_invoice AS rec_dig, " +
                    "tdo.affiliation_parental_protection as ControlParenta,od.automaticdebit as debito_auto, " +
                    "tdo.client_nationality,tdo.product_equip_tv,tdo.product_equip_inter,tdo.product_equip_line, " +
                    "tdo.product_cost_install,tdo.product_cost_install_month, " +
                    "tdo.order_parque_telefono,tdo.client_cms_codigo,v.statuslegado,v.desclegado, " +
                    "od.svaline,od.blocktv,od.svainternet, " +
                    "fecha_equipo_tecnico as asignacion_tec, fecha_instalado, " +
                    "(CASE WHEN o.statusaudio = '1' and " + tiempoTotal + "= 0 THEN 'Pendiente de Subida' WHEN " +
                    "o.statusaudio = '1' and (extract(epoch from (cast('"+ fechaServConvertida +"' as timestamp) - registrationDate) / 60 ) <= " +
                    tiempoTotal + ") THEN 'Pendiente de Subida' " +
                    "WHEN o.statusaudio = '1' and (extract(epoch from (cast('"+ fechaServConvertida +"' as timestamp) - registrationDate) / 60 ) > " +
                    tiempoTotal + ") THEN 'Carga de Audio Expirada' WHEN o.statusaudio = '2' THEN 'Audio Cargado' " +
                    " WHEN o.statusaudio not in('1','2') THEN '-' ELSE '-' END) as restringehoras ";

                    if(request.getCodeFilter().equalsIgnoreCase("1")){
                        query += ",UPPER(v.operacion_comercial) as tipo ";
                    }
                    else if(request.getCodeFilter().equalsIgnoreCase("2")){
                        query += ",'-' as tipo ";
                    }
                    else if(request.getCodeFilter().equalsIgnoreCase("3")){
                        query += ",UPPER(producttype) as tipo ";
                    }
                    else if(request.getCodeFilter().equalsIgnoreCase("4")){
                        query += ",UPPER(o.entidad) as tipo ";
                    }

                    query += " , tdo.product_tipo_reg ";
                    query += "FROM ibmx_a07e6d02edaf552.tdp_visor v JOIN ibmx_a07e6d02edaf552.ORDER o ON v.id_visor = o.ID " +
                    "JOIN ibmx_a07e6d02edaf552.tdp_order tdo ON tdo.order_id = v.id_visor " +
                    "LEFT JOIN ibmx_a07e6d02edaf552.peticion_pago_efectivo pe ON pe.order_id = v.id_visor " +
                    "JOIN ibmx_a07e6d02edaf552.customer C ON C.ID = o.customerid " +
                    "LEFT JOIN ibmx_a07e6d02edaf552.tdp_sales_agent tsa ON tsa.codatis = o.userid " +
                    "JOIN ibmx_a07e6d02edaf552.order_detail od ON o.ID = od.orderid " +
                    "LEFT JOIN ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido and v.codigo_pedido != '' " +
                    "where v.id_visor like '-%' and  v.operacion_comercial <> '' ";

            if (request.getSocio() != null) {
                query = query + "and upper(o.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(o.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(tdo.user_region) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(tdo.user_zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(o.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59') a " +
                    "where a.estado_toa != '--'" +
                    "order by 1";;

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {

                    SalesReportExportResponse obj = new SalesReportExportResponse();
                    String estado_toa = "";

                    obj.setCodigo_venta(rs.getString(1));
                    obj.setSistema_venta(rs.getString(2));
                    obj.setId_grabacion(rs.getString(3));
                    obj.setCampania(rs.getString(4));
                    obj.setOperacion_comercial(rs.getString(5));
                    obj.setTipo_doc(rs.getString(6));
                    obj.setNum_doc(rs.getString(7));
                    obj.setNom_cli(rs.getString(8));
                    obj.setFec_nac(rs.getString(9));
                    obj.setTelefono1(rs.getString(10));
                    obj.setTelefono2(rs.getString(11));
                    obj.setEmail(rs.getString(12));
                    obj.setTel_fij_planta(rs.getString(63));
                    obj.setCod_cms_planta(rs.getString(62));
                    obj.setCod_cli_cms(rs.getString(13));
                    obj.setRegion(rs.getString(14));
                    obj.setZonal(rs.getString(15));
                    obj.setCanal(rs.getString(16));
                    obj.setEntidad(rs.getString(17));
                    obj.setPunto_de_venta(rs.getString(18));
                    obj.setCod_punto_de_venta(rs.getString(19));
                    obj.setNom_vendedor(rs.getString(20));
                    obj.setCod_atis(rs.getString(21));
                    obj.setCod_cms(rs.getString(22));
                    obj.setSegmento(rs.getString(23));
                    obj.setTipo_producto(rs.getString(24));
                    obj.setSub_producto(rs.getString(25));
                    obj.setNom_producto(rs.getString(26));
                    obj.setCodigo_pedido(rs.getString(27));
                    obj.setTecnologia_internet(rs.getString(28));
                    obj.setTecnologia_tv(rs.getString(29));
                    obj.setFec_venta(rs.getString(30));
                    obj.setHora_venta(rs.getString(31));
                    obj.setFec_registro(rs.getString(32));
                    obj.setHora_registro(rs.getString(33));
                    obj.setEstado_venta(rs.getString(34));
                    obj.setEstado_legados(rs.getString(65));
                    obj.setMotivo_estado(rs.getString(36));

                    if(rs.getString(70) != null){
                        estado_toa = "Instalado";
                    }
                    else if(rs.getString(69) != null){
                        estado_toa = "Asignacion Tecnica";
                    }
                    else{
                        estado_toa = rs.getString(35);
                    }
                    obj.setEstado_toa(estado_toa);
                    obj.setModalidad_pago(rs.getString(37));
                    obj.setCod_experto(rs.getString(38));
                    obj.setCod_cip(rs.getString(39));
                    obj.setMonto_cip(rs.getString(40));
                    obj.setEstado_cip(rs.getString(41));
                    obj.setRenta_reg(rs.getString(42));
                    obj.setRenta_prom(rs.getString(43));
                    obj.setCoord_x(rs.getString(44));
                    obj.setCoord_y(rs.getString(45));
                    obj.setDireccion(rs.getString(46));
                    obj.setReferencia(rs.getString(47));
                    obj.setDistrito(rs.getString(48));
                    obj.setProvincia(rs.getString(49));
                    obj.setDepartamento(rs.getString(50));
                    obj.setSvas_linea_vendido(rs.getString(66));
                    obj.setSvas_tv_vendido(rs.getString(67));
                    obj.setSvas_inter_vendido(rs.getString(68));
                    obj.setEnvio_contrato(rs.getString(51));
                    obj.setProteccion_datos(rs.getString(52));
                    obj.setRecibo_dig(rs.getString(53));
                    obj.setFiltro_parental(rs.getString(54));
                    obj.setDebito_automatico(rs.getString(55));
                    obj.setNacionalidad(rs.getString(56));
                    obj.setEquipamiento_tv(rs.getString(57));
                    obj.setEquipamiento_internet(rs.getString(58));
                    obj.setEquipamiento_linea(rs.getString(59));
                    obj.setCosto_instalacion(rs.getString(60));
                    obj.setCuotas_costo_instalacion(rs.getString(61));
                    obj.setEstado_audio(rs.getString(71));
                    obj.setTipo_reporte(rs.getString(72));
                    obj.setProduct_tipo_reg(rs.getString(73));

                    listado.add(obj);
                }
            }
            logger.info("Fin de consulta a BD. Total de Registros: " + listado.size());
        } catch (Exception e) {
            logger.error("Error consultar Exportar Dashboard.", e);
        }

        bodyResponse.setReporteVentas(listado);
        return bodyResponse;
    }
    private List<Cell> getPreventa(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select a.op_comercial,sum(CASE WHEN a.estado_solicitud != '' then a.cantidad end ) preventa, " +
                    "sum(CASE WHEN a.estado_solicitud in ('INGRESADO','ENVIANDO','EN PROCESO','CAIDA','SEGUIMIENTO') " +
                    "then a.cantidad end ) venta,sum(CASE WHEN a.estado_solicitud = 'INGRESADO' then a.cantidad end ) ingresado from ( " +
                    "select upper(v.operacion_comercial) OP_COMERCIAL,upper(v.estado_solicitud) estado_solicitud, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v inner join ibmx_a07e6d02edaf552.order o " +
                    "on v.id_visor = o.id left join ibmx_a07e6d02edaf552.tdp_sales_agent sa " +
                    "on o.userid = sa.codatis " +
                    "join ibmx_a07e6d02edaf552.tdp_order tdo ON tdo.order_id = v.id_visor " +
                    "where v.id_visor like '-%' " +
                    " and  v.operacion_comercial <> '' ";

            if (request.getSocio() != null) {
                query = query + "and upper(o.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(o.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(tdo.user_region) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(tdo.user_zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(o.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(v.operacion_comercial),upper(v.estado_solicitud) " +
                    ") a group by a.op_comercial " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    Cell dta2 = new Cell();
                    Cell dta3 = new Cell();

                    dta.setRow(rs.getString(1));
                    dta.setColumn("Pre-Venta");
                    dta.setValue(rs.getString(2));

                    dta2.setRow(rs.getString(1));
                    dta2.setColumn("Venta");
                    dta2.setValue(rs.getString(3));

                    dta3.setRow(rs.getString(1));
                    dta3.setColumn("Ingresado con Pedido");
                    dta3.setValue(rs.getString(4));

                    data.add(dta);
                    data.add(dta2);
                    data.add(dta3);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getPreventa.", e);
        }
        return data;
    }

    private List<Cell> getVenta(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select upper(v.operacion_comercial) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v inner join ibmx_a07e6d02edaf552.order o " +
                    "on v.id_visor = o.id inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa " +
                    "on o.userid = sa.codatis where v.id_visor like '-%' and v.estado_solicitud " +
                    "in ('INGRESADO') and  v.operacion_comercial <> '' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(v.operacion_comercial) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Venta");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }

        } catch (Exception e) {
            logger.error("Error consultar getVenta.", e);
        }
        return data;
    }

    private List<Cell> getIngresadoPedido(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(v.operacion_comercial) OP_COMERCIAL, count(*) CANTIDAD, " +
                    "(CASE toa.status_toa WHEN 'WO_SUSPEND' THEN 'Caida sin Pedido' " +
                    "WHEN 'WO_COMPLETED' THEN 'Instalado' WHEN 'WO_CANCEL' THEN 'Cancelado' END) ESTADO2 " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v inner join ibmx_a07e6d02edaf552.order o " +
                    "on v.id_visor = o.id left join ibmx_a07e6d02edaf552.tdp_sales_agent sa " +
                    "on o.userid = sa.codatis " +
                    "join ibmx_a07e6d02edaf552.tdp_order tdo ON tdo.order_id = v.id_visor " +
                    "inner join ibmx_a07e6d02edaf552.toa_status toa " +
                    "on toa.peticion = v.codigo_pedido where v.id_visor like '-%' " +
                    "and toa.status_toa not in('IN_TOA','WO_INIT','WO_CANCEL') and  v.operacion_comercial <> '' ";

            if (request.getSocio() != null) {
                query = query + "and upper(o.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(o.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(tdo.user_region) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(tdo.user_zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(o.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(v.operacion_comercial),toa.status_toa " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setValue(rs.getString(2));
                    dta.setColumn(rs.getString(3));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getCaidaSinPedido(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(v.operacion_comercial) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v inner join ibmx_a07e6d02edaf552.order o " +
                    "on v.id_visor = o.id inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa " +
                    "on o.userid = sa.codatis inner join ibmx_a07e6d02edaf552.toa_status toa " +
                    "on toa.peticion = v.codigo_pedido " +
                    "WHERE\n" +
                    "\tv.id_visor LIKE '-%' \n" +
                    "\tAND v.fecha_grabacion >='"+request.getFechaInicio()+" 00:00:00'\n" +
                    "\tAND v.fecha_grabacion <= '"+request.getFechaFinal()+" 23:59:59' \n" +
                    "\tAND toa.status_toa = 'WO_SUSPEND' \n" +
                    "\tAND v.operacion_comercial <> '' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query +  " group by upper(v.operacion_comercial) ";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Caida sin Pedido");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getInstalado(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(v.operacion_comercial) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v inner join ibmx_a07e6d02edaf552.order o " +
                    "on v.id_visor = o.id inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa " +
                    "on o.userid = sa.codatis inner join ibmx_a07e6d02edaf552.toa_status toa " +
                    "on toa.peticion = v.codigo_pedido where v.id_visor like '-%' " +
                    "and toa.status_toa = 'WO_COMPLETED' and  v.operacion_comercial <> '' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(v.operacion_comercial) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Instalado");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getCancelado(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(v.operacion_comercial) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v inner join ibmx_a07e6d02edaf552.order o " +
                    "on v.id_visor = o.id inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa " +
                    "on o.userid = sa.codatis inner join ibmx_a07e6d02edaf552.toa_status toa " +
                    "on toa.peticion = v.codigo_pedido where v.id_visor like '-%' " +
                    "and toa.status_toa = 'WO_CANCEL' and  v.operacion_comercial <> '' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(v.operacion_comercial) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Instalado");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getTrazabilidad(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select upper(v.operacion_comercial) OP_COMERCIAL, " +
                    "upper(v.estado_solicitud) ESTADO_TRZ, " +
                    "count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "where v.id_visor like '-%' and v.estado_solicitud <> '' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(v.operacion_comercial),upper(v.estado_solicitud) " +
                    "order by 1 ";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn(rs.getString(2));
                    dta.setValue(rs.getString(3));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getTrazabilidad.", e);
        }
        return data;
    }

    private List<Cell> getPreventaProducto(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select a.producttype,sum(CASE WHEN a.estado_solicitud != '' then a.cantidad end ) preventa, " +
                    "sum(CASE WHEN a.estado_solicitud in ('INGRESADO','ENVIANDO','EN PROCESO','CAIDA','SEGUIMIENTO') " +
                    "then a.cantidad end ) venta,sum(CASE WHEN a.estado_solicitud = 'INGRESADO' then a.cantidad end ) ingresado from ( " +
                    "select upper(producttype) producttype, " +
                    "upper(v.estado_solicitud) estado_solicitud, count(*) CANTIDAD "  +
                    "from ibmx_a07e6d02edaf552.tdp_visor v inner join ibmx_a07e6d02edaf552.order o " +
                    "on v.id_visor = o.id inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa " +
                    "on o.userid = sa.codatis where v.id_visor like '-%' " +
                    " ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(producttype),upper(v.estado_solicitud) " +
                    ") a group by a.producttype " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    Cell dta2 = new Cell();
                    Cell dta3 = new Cell();

                    dta.setRow(rs.getString(1));
                    dta.setColumn("Pre-Venta");
                    dta.setValue(rs.getString(2));

                    dta2.setRow(rs.getString(1));
                    dta2.setColumn("Venta");
                    dta2.setValue(rs.getString(3));

                    dta3.setRow(rs.getString(1));
                    dta3.setColumn("Ingresado con Pedido");
                    dta3.setValue(rs.getString(4));

                    data.add(dta);
                    data.add(dta2);
                    data.add(dta3);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getPreventa.", e);
        }
        return data;
    }

    private List<Cell> getVentaProducto(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select upper(o.producttype) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "where v.id_visor like '-%' and v.estado_solicitud in ('INGRESADO') ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(o.producttype) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Venta");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getVenta.", e);
        }
        return data;
    }

    private List<Cell> getIngresadoPedidoProducto(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(o.producttype) OP_COMERCIAL, count(*) CANTIDAD, " +
                    "(CASE toa.status_toa WHEN 'WO_SUSPEND' THEN 'Caida sin Pedido' " +
                    "WHEN 'WO_COMPLETED' THEN 'Instalado' WHEN 'WO_CANCEL' THEN 'Cancelado' END) ESTADO2 " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "inner join ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido " +
                    "where v.id_visor like '-%' and toa.status_toa not in('IN_TOA','WO_INIT','WO_CANCEL') ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(o.producttype),toa.status_toa " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn(rs.getString(3));
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getCaidaSinPedidoProducto(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(o.producttype) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "inner join ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido " +
                    "where v.id_visor like '-%' and toa.status_toa = 'WO_SUSPEND' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(o.producttype) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Caida sin Pedido");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getInstaladoProducto(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(o.producttype) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "inner join ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido " +
                    "where v.id_visor like '-%' and toa.status_toa = 'WO_COMPLETED' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(o.producttype) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Instalado");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getCanceladoProducto(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(o.producttype) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "left join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "join ibmx_a07e6d02edaf552.tdp_order tdo ON tdo.order_id = v.id_visor " +
                    "inner join ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido " +
                    "where v.id_visor like '-%' and toa.status_toa = 'WO_CANCEL' ";

            if (request.getSocio() != null) {
                query = query + "and upper(o.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(o.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(tdo.user_region) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(tdo.user_zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(o.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(o.producttype) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Instalado");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getPreventaSocio(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select a.entidad,sum(CASE WHEN a.estado_solicitud != '' then a.cantidad end ) preventa, " +
                    "sum(CASE WHEN a.estado_solicitud in ('INGRESADO','ENVIANDO','EN PROCESO','CAIDA','SEGUIMIENTO') " +
                    "then a.cantidad end ) venta,sum(CASE WHEN a.estado_solicitud = 'INGRESADO' then a.cantidad end ) ingresado from ( " +
                    "select upper(sa.entidad) entidad, count(*) CANTIDAD, " +
                    "upper(v.estado_solicitud) estado_solicitud " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v inner join ibmx_a07e6d02edaf552.order o " +
                    "on v.id_visor = o.id left join ibmx_a07e6d02edaf552.tdp_sales_agent sa " +
                    "on o.userid = sa.codatis " +
                    "join ibmx_a07e6d02edaf552.tdp_order tdo ON tdo.order_id = v.id_visor " +
                    " where v.id_visor like '-%' " +
                    " ";

            if (request.getSocio() != null) {
                query = query + "and upper(o.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(o.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(tdo.user_region) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(tdo.user_zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(o.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(sa.entidad),upper(v.estado_solicitud) " +
                    ") a group by a.entidad " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    Cell dta2 = new Cell();
                    Cell dta3 = new Cell();

                    dta.setRow(rs.getString(1));
                    dta.setColumn("Pre-Venta");
                    dta.setValue(rs.getString(2));

                    dta2.setRow(rs.getString(1));
                    dta2.setColumn("Venta");
                    dta2.setValue(rs.getString(3));

                    dta3.setRow(rs.getString(1));
                    dta3.setColumn("Ingresado con Pedido");
                    dta3.setValue(rs.getString(4));

                    data.add(dta);
                    data.add(dta2);
                    data.add(dta3);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getPreventa.", e);
        }
        return data;
    }

    private List<Cell> getVentaSocio(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select upper(sa.entidad) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "where v.id_visor like '-%' and v.estado_solicitud in ('INGRESADO') ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(sa.entidad) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Venta");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getVenta.", e);
        }
        return data;
    }

    private List<Cell> getIngresadoPedidoSocio(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(sa.entidad) OP_COMERCIAL, count(*) CANTIDAD, " +
                    "(CASE toa.status_toa WHEN 'WO_SUSPEND' THEN 'Caida sin Pedido' " +
                    "WHEN 'WO_COMPLETED' THEN 'Instalado' WHEN 'WO_CANCEL' THEN 'Cancelado' END) ESTADO2 " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "inner join ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido " +
                    "where v.id_visor like '-%' and toa.status_toa not in('IN_TOA','WO_INIT','WO_CANCEL') ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(sa.entidad),toa.status_toa " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn(rs.getString(3));
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getCaidaSinPedidoSocio(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(sa.entidad) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "inner join ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido " +
                    "where v.id_visor like '-%' and toa.status_toa = 'WO_SUSPEND' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(sa.entidad) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Caida sin Pedido");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getInstaladoSocio(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(sa.entidad) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "inner join ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido " +
                    "where v.id_visor like '-%' and toa.status_toa = 'WO_COMPLETED' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(sa.entidad) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Instalado");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    private List<Cell> getCanceladoSocio(ReportVentasRequest request) {
        List<Cell> data = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "select distinct upper(sa.entidad) OP_COMERCIAL, count(*) CANTIDAD " +
                    "from ibmx_a07e6d02edaf552.tdp_visor v " +
                    "inner join ibmx_a07e6d02edaf552.order o on v.id_visor = o.id " +
                    "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on o.userid = sa.codatis " +
                    "inner join ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido " +
                    "where v.id_visor like '-%' and toa.status_toa = 'WO_CANCEL' ";

            if (request.getSocio() != null) {
                query = query + "and upper(sa.entidad) IN ("+request.getSocio()+") ";
            }

            if (request.getCampania() != null) {
                query = query + "and upper(o.campaign) IN ("+request.getCampania()+") ";
            }

            if (request.getOpComercial() != null) {
                query = query + "and upper(v.operacion_comercial) IN ("+request.getOpComercial()+") ";
            }

            if(request.getCanal()!=null){
                query = query + "and upper(sa.canal) IN ("+request.getCanal()+") ";
            }

            if(request.getRegion()!=null){
                query = query + "and upper(sa.zona) IN ("+request.getRegion()+") ";
            }

            if(request.getZonal()!=null){
                query = query + "and upper(sa.zonal) IN ("+request.getZonal()+") ";
            }

            if(request.getPtoVenta()!=null){
                query = query + "and upper(sa.nompuntoventa) IN ("+request.getPtoVenta()+") ";
            }

            query = query + "and v.fecha_grabacion between '"+request.getFechaInicio()+" 00:00:00' " +
                    "and '"+request.getFechaFinal()+" 23:59:59' " +
                    "group by upper(sa.entidad) " +
                    "order by 1";

            try (PreparedStatement ps = con.prepareStatement(query)) {
                //ps.setString(1, channel.getName());
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Cell dta = new Cell();
                    dta.setRow(rs.getString(1));
                    dta.setColumn("Instalado");
                    dta.setValue(rs.getString(2));
                    data.add(dta);
                }
            }
        } catch (Exception e) {
            logger.error("Error consultar getIngresadoPedido.", e);
        }
        return data;
    }

    public GenerarCipResponse generarCIP(GenerarCipRequest cipRequest, String token) {
        GenerarCipResponse response = new GenerarCipResponse();
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(consultaRestPagoEfectivoUrl)
                .build();
        GenerarCip generarCip = new GenerarCip(config);
        ClientResult<GenerarCipResponse> result = generarCip.postCIP(cipRequest, token);
        result.getEvent().setOrderId(cipRequest.getTransactionCode());

        if (!result.isSuccess()) {
            serviceCallEventsService.registerEvent(result.getEvent());
            response = result.getResult();
            //throw new TimeoutException(result.getE().getMessage());
        } else {
            serviceCallEventsService.registerEvent(result.getEvent());
            peticionPagoEfectivoService.actualizarPagoEfectivo(result.getResult().getData().getTransactionCode() + "", result.getResult().getData().getCip() + "");
            response = result.getResult();
        }
        return response;
    }


}