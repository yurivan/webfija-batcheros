package pe.com.tdp.ventafija.microservices.service.product;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.arpu.CalculadoraClient;
import pe.com.tdp.ventafija.microservices.common.clients.arpu.OffersApiCalculadoraArpuRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.arpu.OffersApiCalculadoraArpuResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.avve.AvveClient;
import pe.com.tdp.ventafija.microservices.common.clients.avve.OffersApiAvveRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.avve.OffersApiAvveResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseHeader;
import pe.com.tdp.ventafija.microservices.common.clients.experto.*;
import pe.com.tdp.ventafija.microservices.common.clients.gis.*;
import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.dao.ServiceCallEventsDao;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.domain.message.MessageEntities;
import pe.com.tdp.ventafija.microservices.common.services.ParametersService;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.*;
import pe.com.tdp.ventafija.microservices.constants.product.ProductConstants;
import pe.com.tdp.ventafija.microservices.constants.user.UserConstants;
import pe.com.tdp.ventafija.microservices.domain.FilterResponse;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.agendamiento.AgendamientRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.CustomerScoringResponse;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.OperacionComercial;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.OperacionComercialDetalle;
import pe.com.tdp.ventafija.microservices.domain.gis.GisUbigeo;
import pe.com.tdp.ventafija.microservices.domain.order.entity.*;
import pe.com.tdp.ventafija.microservices.domain.product.*;
import pe.com.tdp.ventafija.microservices.domain.repository.*;
import pe.com.tdp.ventafija.microservices.domain.user.ConfigParameterResponseData;
import pe.com.tdp.ventafija.microservices.repository.customer.CustomerDAO;
import pe.com.tdp.ventafija.microservices.repository.gis.GisDAO;
import pe.com.tdp.ventafija.microservices.repository.product.ProductDAO;

import java.util.*;

@Service
public class ProductService {
    private static final Logger logger = LogManager.getLogger();
    private static final String DOUBLE_MESSAGE = "%s - %s";

    private static boolean byPassServiciosCaidos = false;

    @Autowired
    private ProductDAO dao;
    @Autowired
    private CustomerDAO daoCustomer;

    //Aquí iniciamos con contingencia GIS by Valdemar
    @Autowired
    private GisDAO daoGis;


    @Autowired
    private ServiceCallEventsDao serviceCallEventsDao;
    @Autowired
    private ApiHeaderConfig apiHeaderConfig;
    @Autowired
    private ParametersRepository parametersRepository;
    @Autowired
    private ParametersService parametersService;
    @Autowired
    private TdpSalesAgentRepository tdpSalesAgentRepository;
    @Autowired
    private TdpChannelsRepository tdpChannelsRepository;
    @Autowired
    private TdpCatalogRepository tdpCatalogRepository;
    @Autowired
    private MessageUtil messageUtil;

    @Autowired
    private TdpAutomatizadorCabeceraRepository tdpAutomatizadorCabeceraRepository;

    @Value("${tdp.api.max.retry}")
    private Integer maxRetry;

    // Variables
    @Value("${product.mensajes.oferta.ErrorAvve}")
    private String mensajeOfertaErrorAvve;
    @Value("${product.mensajes.oferta.ErrorExperto}")
    private String mensajeOfertaErrorExperto;
    @Value("${product.mensajes.oferta.ErrorAvveExperto}")
    private String mensajeOfertaErrorAvveExperto;
    @Value("${product.mensajes.oferta.ErrConnAvveExperto}")
    private String mensajeOfertaErrConnAvveExperto;
    @Value("${product.mensajes.oferta.ErrorGener}")
    private String mensajeOfertaErrorGener;
    @Value("${product.mensajes.sva.SinDatosBD}")
    private String mensajeSvaSinDatosBD;
    @Value("${product.mensajes.sva.ErrorGener}")
    private String mensajeSvaErrorGener;
    @Value("${product.offering.arpu.MaxDecos}")
    private String cantidadMaximaDecos;
    @Value("${product.offering.arpu.errorCalculadora}")
    private String errorCalculadora;
    //@Value("${product.offering.arpu.clienteNoEncontrado}")
    //private String clienteNoEncontrado;

    //Gis
    @Value("${tdp.api.gis.consultagis.uri}")
    private String uriConsultaGis;
    @Value("${tdp.api.gis.consultagis.apiid}")
    private String apiidConsultaGis;
    @Value("${tdp.api.gis.consultagis.apisecret}")
    private String apisecretConsultaGis;

    @Autowired
    private ServiceCallEventsService serviceCallEventsService;

    @Autowired
    private GisFilterServiceImpl gisFilterService;

    @Autowired
    private CampaniaFilterService campaniaFilterService;

    @Autowired
    private TdpCatalogOutRepository tdpCatalogOutRepository;

    // Sprint 13 - Equivalencias de tipos documento en Equifax
    @Value("${tipodocumento.equivalencia.equifax.dni}")
    private String codigoDNIEquivalenteEquifax;

    @Value("${tipodocumento.equivalencia.equifax.cex}")
    private String codigoCEXEquivalenteEquifax;

    @Value("${tipodocumento.equivalencia.equifax.pas}")
    private String codigoPASEquivalenteEquifax;

    @Value("${tipodocumento.equivalencia.equifax.ruc}")
    private String codigoRUCEquivalenteEquifax;

    @Value("${tipodocumento.equivalencia.equifax.otros}")
    private String codigoOTROSEquivalenteEquifax;


    public Response saveAgendamiento(AgendamientRequest request) {
        Response response = new Response();
        try {
            dao.agendamientoSave(request);
            response.setResponseMessage("Actualizado Correctamente");
        } catch (Exception e) {
            response.setResponseMessage("Error Offering Service");
            logger.info("Error Offering Service", e);
            return response;
        }


        return response;
    }

    public Response<List<TdpAgendamientoCupones>> showCalendario() {
        Response <List<TdpAgendamientoCupones>>  response = new Response();

        try {
            //dao.agendamientoSave();
            List<TdpAgendamientoCupones> list = dao.calendarioShow();
            response.setResponseData(list);
            response.setResponseMessage("Success calendario_agendamiento");

        } catch (Exception e) {
            response.setResponseMessage("Error calendario_agendamiento Service");
            logger.info("Error show Service", e);
            return response;
        }


        return response;
    }

    public Response<List<OffersResponseData>> readOffers(OffersRequest request) {
        Response<List<OffersResponseData>> response = new Response<>();
        int countRetryExpert = 0;
        boolean foundExpert = false;
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("PRODUCTO");
        LogVass.serviceResponseHashMap(logger, "GetProducto", "messageList", messageList);
        //End--Code for call Service

        try {
            logger.info("Inicio Offers Service");
            List<OffersResponseData> listOffers = new ArrayList<>();
            List<OffersResponseData> responseData = new ArrayList<>();
            OffersApiAvveRequestBody apiAvveRequestBody = new OffersApiAvveRequestBody();
            if (request.getPhoneNumber() != null && !Constants.EMPTY.equals(request.getPhoneNumber())) {
                apiAvveRequestBody.setTelefono(request.getPhoneNumber());
            } else {
                apiAvveRequestBody.setCoordenadaX(request.getCoordinateX());
                apiAvveRequestBody.setCoordenadaY(request.getCoordinateY());
            }

            ApiResponse<OffersApiAvveResponseBody> objAvve = getAvve(apiAvveRequestBody);

            if ((Constants.RESPONSE).equals(objAvve.getHeaderOut().getMsgType())) {
                objAvve.getBodyOut().setVelocidadMax(objAvve.getBodyOut().getVelocidadMax()
                        .replace(ProductConstants.OFFERS_API_AVVE_REQUEST_MAX_SPEED_FIX, Constants.EMPTY));
                objAvve.getBodyOut()
                        .setTipoSenal(ProductConstants.OFFERS_TIPO_SENAL(objAvve.getBodyOut().getTipoSenal()));
            }
            if (request.getScoringResponse() != null) {
                if (!Constants.RESPONSE_ACTION.equals(request.getScoringResponse().getAccion())) {
                    if (!request.getScoringResponse().getDeudaAtis().equals("0") || !request.getScoringResponse().getDeudaCms().equals("0")) {
                        response.setResponseCode("4");
                        response.setResponseMessage("El cliente no tiene oferta porque tiene deuda.");
                        logger.info("El cliente no tiene oferta porque tiene deuda.");
                        return response;
                    }
                }
                listOffers = dao.readOffers(request, objAvve, request.getScoringResponse());
                logger.info("Offers BD:" + listOffers.size());
                for (OffersResponseData rd : listOffers) {
                    for (OperacionComercial operation : request.getScoringResponse().getResult()) {
                        if (operation.getContadorDetalle() > 0) {
                            for (OperacionComercialDetalle detail : operation.getDetalle()) {
                                if (rd.getPrice() <= Integer.parseInt(Converter.fixPrice(operation.getRenta()))
                                        && rd.getCommercialOperation().equalsIgnoreCase(operation.getCodOpe())
                                        && rd.getProductCategoryCode().equals(detail.getCodProd())
                                        && rd.getCashPrice() == detail.getPagoAdelantado()) {
                                    responseData.add(rd);
                                }
                            }
                        }
                    }
                }
                logger.info("Offers Response:" + responseData.size());
                response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_OK);
                response.setResponseData(responseData);
            } else if (!(Constants.RESPONSE).equals(objAvve.getHeaderOut().getMsgType())
                    && request.getScoringResponse() == null) {
                response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
                response.setResponseMessage(mensajeOfertaErrorAvve);

            } else {
                response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
                response.setResponseMessage(mensajeOfertaErrorAvveExperto);
            }
            logger.info("Fin Offering Service.");
        } catch (ClientException e) {
            response.setResponseCode(messageList.get("readOffersFail").getCode());
            response.setResponseMessage(messageList.get("readOffersFail").getMessage());
            //response.setResponseCode("2");
            //response.setResponseMessage(String.format(mensajeOfertaErrConnAvveExperto, e.getEvent().getServiceCode()));
            logger.info("Error Offering Service.", e);
            return response;
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            //response.setResponseCode("3");
            //response.setResponseMessage(mensajeOfertaErrorGener);
            logger.info("Error Offering Service", e);
            return response;
        }

        return response;
    }

    private void generarConsultaDefaultSvasMigracion(SvaV2Request request) {
        StringBuilder consultaBuilder = new StringBuilder();
        appendSvas(request.getMigracionSvasBloqueTV(), consultaBuilder);
        appendSvas(request.getMigracionSvasInternet(), consultaBuilder);
        appendSvas(request.getMigracionSvasLinea(), consultaBuilder);

        if (consultaBuilder.length() > 0) {
            String[] svasAll = consultaBuilder.toString().split(",");
            String svasAllConsulta = generateConsultaSVA(svasAll);
            request.setMigracionSvasAll(svasAllConsulta);
        }
    }


    private void generarConsultaDefaultSvasAltaPura(SvaRequest request) {
        StringBuilder consultaBuilder = new StringBuilder();
        appendSvas(request.getAltaPuraSvasBloqueTV(), consultaBuilder);
        appendSvas(request.getAltaPuraSvasInternet(), consultaBuilder);
        appendSvas(request.getAltaPuraSvasLinea(), consultaBuilder);

        if (consultaBuilder.length() > 0) {
            String[] svasAll = consultaBuilder.toString().split(",");
            String svasAllConsulta = generateConsultaSVA(svasAll);
            request.setAltaPuraSvasAll(svasAllConsulta);
        }
    }

    private void appendSvas(String svas, StringBuilder consultaBuilder) {
        if (consultaBuilder.toString().length() > 2)
            consultaBuilder.append(",");
        if (svas != null && !svas.isEmpty() && !"-".equals(svas)) {
            consultaBuilder.append(svas);
        }
    }

    private String generateConsultaSVA(String[] svas) {
        StringBuilder builder = new StringBuilder();
        for (String sva : svas) {
            if (!"-".equals(sva) && !"".equals(sva)) {
                builder.append("'").append(sva).append("',");
            }
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
            return builder.toString();
        }
        return "";
    }

    public Response<SvaResponseData> readSVA(SvaRequest request) {
        logger.info("Inicio readSVA Service.");
        Response<SvaResponseData> response = new Response<>();
        SvaResponseData responseData = new SvaResponseData();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("SVA");
        LogVass.serviceResponseHashMap(logger, "GetSVA", "messageList", messageList);
        //End--Code for call Service
        Integer smartMb = 0;
        try {
            smartMb = Integer.parseInt(messageList.get("smartMb").getMessage());
        } catch (Exception eMb) {
            logger.error("Error Complements Service.", eMb);
        }

        String productCodeSVA = "";
        try {
            productCodeSVA = dao.getProductProductCodeById(Integer.parseInt(request.getProductId()));
        } catch (Exception e) {
            logger.error(e);
        }

        int maxDecosExtras;

        if (!productCodeSVA.isEmpty())
            maxDecosExtras = (productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL) || productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) ? 1 : 2;
        else
            maxDecosExtras = 2;

        try {
            /* Sprint 10 */
            TdpCatalog tdpCatalog = null;
            if (request.getProductId() != null) {
                tdpCatalog = tdpCatalogRepository.findOne(Integer.parseInt(request.getProductId()));
            }
            if (tdpCatalog != null) {
                request.setAltaPuraSvasBloqueTV(tdpCatalog.getProductBloqueTv().replace(" ", ""));
                request.setAltaPuraSvasLinea(tdpCatalog.getProductSvaLinea().replace(" ", ""));
                request.setAltaPuraSvasInternet(tdpCatalog.getProductSvaInternet().replace(" ", ""));
            }
            generarConsultaDefaultSvasAltaPura(request);
            /* Sprint 10 */
            List<SvaResponseDataSva> list = dao.readSVA(request);

            //List <String> temp = new ArrayList<String>();
            List<SvaResponseDataSva> temp = new ArrayList<>();

            int in = 0;

            String[] newNombre = new String[20];

            for (SvaResponseDataSva item : list) {
                if (item.getBloquePadre() == null && item.isClickPadre()) {
                    temp.add(item);
                    if (item.isAltaPuraDefaultSVA()) {
                        newNombre[in] = item.getUnit();
                        in++;
                        System.out.println(in);
                    }
                }
            }
            for (SvaResponseDataSva item : temp) {
                if (item.getBloquePadre() != null) {
                    for (String newItem : newNombre) {
                        if (item.getUnit() == newItem) {
                            item.setClickPadre(true);
                        } else {
                            item.setClickPadre(false);
                        }
                    }
                }
            }


            if (!list.isEmpty()) {
                if (!productCodeSVA.isEmpty()) {
                    if (productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL) || productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) {
                        int contador = 0;
                        Iterator<SvaResponseDataSva> listIterable = list.iterator();
                        while (listIterable.hasNext()) {
                            SvaResponseDataSva obj = listIterable.next();
                            if (obj.getCode().equals(ProductConstants.CODE_BLOQUE_PRODUCTO) ||
                                    obj.getCode().equals(ProductConstants.CODE_BLOQUE_TV)
                            ) {
                                listIterable.remove();
                            }
                        }
                    }
                }
            }
            List<SvaResponseDataSva> equipmentList = new ArrayList<>();
            List<SvaResponseDataSva> svaList = new ArrayList<>();
            if (list.isEmpty()) {
                //response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_ERROR);
                responseData.setEquipment(equipmentList);
                responseData.setSva(svaList);

                response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_OK);
                response.setResponseData(responseData);
                //response.setResponseMessage(mensajeSvaSinDatosBD);
            } else {

                if (tdpCatalog != null) {
                    List<SvaResponseDataSva> svaListTmp = new ArrayList<>();
                    for (SvaResponseDataSva svaObject : list) {
                        if (svaObject.getCode().equals("DVR")) {
                            if (tdpCatalog.getTvTech().equalsIgnoreCase("CATV")) {
                                svaListTmp.add(svaObject);
                            }
                        } else if (svaObject.getCode().equals("DSHD")) {
                            if (tdpCatalog.getInternetSpeed() >= smartMb) {
                                svaListTmp.add(svaObject);
                            }
                        } else {
                            svaListTmp.add(svaObject);
                        }
                    }
                    list = svaListTmp;
                }

                String productCode = "";

                if (!productCodeSVA.isEmpty()) {
                    if (!productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL)
                            || !productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) {
                        for (SvaResponseDataSva svaObject : list) {
                            if ("BP".equalsIgnoreCase(svaObject.getCode()) && svaObject.isAltaPuraDefaultSVA()) {
                                productCode += svaObject.getUnit().replace("+", ",") + ",";
                            }
                        }
                        if (!productCode.equalsIgnoreCase("")) {
                            String[] nameProduct = productCode.replace("BLOQUE", "").split(",");

                            List<SvaResponseDataSva> svaListTmp = new ArrayList<>();
                            for (SvaResponseDataSva svaObject : list) {

                                if ("BTV".equalsIgnoreCase(svaObject.getCode())) {
                                    boolean isExiste = false;
                                    for (String obj : nameProduct) {
                                        if (svaObject.getUnit().indexOf(obj.trim()) >= 0) {
                                            isExiste = true;
                                        }
                                    }
                                    if (!isExiste) {
                                        svaListTmp.add(svaObject);
                                    }
                                } else {
                                    svaListTmp.add(svaObject);
                                }

                            }
                            list = svaListTmp;
                        }

                        productCode = "";
                        for (SvaResponseDataSva svaObject : list) {
                            if ("BTV".equalsIgnoreCase(svaObject.getCode()) && svaObject.isAltaPuraDefaultSVA()) {
                                productCode += svaObject.getUnit().replace("+", ",").replace("NEW", "") + ",";
                            }
                        }
                        if (!productCode.equalsIgnoreCase("")) {
                            String[] nameProduct = productCode.replace("BLOQUE", "").split(",");

                            List<SvaResponseDataSva> svaListTmp = new ArrayList<>();
                            for (SvaResponseDataSva svaObject : list) {
                                if ("BP".equalsIgnoreCase(svaObject.getCode())) {
                                    boolean isExiste = false;
                                    for (String obj : nameProduct) {
                                        if (svaObject.getUnit().indexOf(obj.trim()) >= 0) {
                                            isExiste = true;
                                        }
                                    }
                                    if (!isExiste) {
                                        svaListTmp.add(svaObject);
                                    }
                                } else {
                                    svaListTmp.add(svaObject);
                                }
                            }
                            list = svaListTmp;
                        }

                    }
                }

                for (SvaResponseDataSva svaObject : list) {
                    if ("EQUIPAMIENTO".equals(svaObject.getCode())) {
                        equipmentList.add(svaObject);
                    } else {
                        if ((svaObject.getCode().equals("DSHD") || svaObject.getCode().equals("DHD") || svaObject.getCode().equals("DVR")) && Integer.parseInt(svaObject.getUnit()) > maxDecosExtras)
                            continue;
                        svaList.add(svaObject);
                    }
                }

                responseData.setEquipment(equipmentList);
                responseData.setSva(svaList);

                response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_OK);
                response.setResponseData(responseData);
            }
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            logger.info("Fin Complements Service.");
            logger.error("Error Complements Service.", e);
            return response;
        }
        logger.info("Fin Complements Service.");

        return response;
    }

    public Response<SvaResponseData> readOutSVA(SvaOutRequest svaOutRequest) {
        logger.info("Inicio readSVA Service.");

        SvaRequest request = new SvaRequest();
        request.setProductCode(svaOutRequest.getProductCode());
        request.setProductId(svaOutRequest.getProductId());
        request.setApplicationCode(svaOutRequest.getApplicationCode());
        request.setAltaPuraSvasLinea(svaOutRequest.getAltaPuraSvasLinea());
        request.setAltaPuraSvasBloqueTV(svaOutRequest.getAltaPuraSvasBloqueTV());
        request.setAltaPuraSvasInternet(svaOutRequest.getAltaPuraSvasInternet());
        request.setAltaPuraSvasAll(svaOutRequest.getAltaPuraSvasAll());

        Response<SvaResponseData> response = new Response<>();
        SvaResponseData responseData = new SvaResponseData();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("SVA");
        LogVass.serviceResponseHashMap(logger, "GetSVA", "messageList", messageList);
        //End--Code for call Service
        Integer smartMb = 0;
        try {
            smartMb = Integer.parseInt(messageList.get("smartMb").getMessage());
        } catch (Exception eMb) {
            logger.error("Error Complements Service.", eMb);
        }

        String productCodeSVA = "";
        try {
            productCodeSVA = dao.getProductProductCodeById(Integer.parseInt(request.getProductId()));
        } catch (Exception e) {
            logger.error(e);
        }

        int maxDecosExtras;

        if (!productCodeSVA.isEmpty())
            maxDecosExtras = (productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL) || productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) ? 1 : 2;
        else
            maxDecosExtras = 2;

        try {
            /* Sprint 10 */
            TdpCatalog tdpCatalog = null;
//            if (request.getProductId() != null) {
//                tdpCatalog = tdpCatalogRepository.findOne(Integer.parseInt(request.getProductId()));
//            }
//            if (tdpCatalog != null) {
//                request.setAltaPuraSvasBloqueTV(tdpCatalog.getProductBloqueTv().replace(" ", ""));
//                request.setAltaPuraSvasLinea(tdpCatalog.getProductSvaLinea().replace(" ", ""));
//                request.setAltaPuraSvasInternet(tdpCatalog.getProductSvaInternet().replace(" ", ""));
//            }
            tdpCatalog = new TdpCatalog();

            tdpCatalog.setProductCode(svaOutRequest.getProductCode());
            tdpCatalog.setInternetSpeed(svaOutRequest.getInternetSpeed());
            tdpCatalog.setTvTech(svaOutRequest.getTvTech());

            generarConsultaDefaultSvasAltaPura(request);
            //request.setAltaPuraSvasAll("'"+request.getAltaPuraSvasAll()+"'");
            /* Sprint 10 */
            List<SvaResponseDataSva> list = dao.readSVA(request);

            //List <String> temp = new ArrayList<String>();
            List<SvaResponseDataSva> temp = new ArrayList<>();

            int in = 0;

            String[] newNombre = new String[20];

            for (SvaResponseDataSva item : list) {
                if (item.getBloquePadre() == null && item.isClickPadre()) {
                    temp.add(item);
                    if (item.isAltaPuraDefaultSVA()) {
                        newNombre[in] = item.getUnit();
                        in++;
                        System.out.println(in);
                    }
                }
            }
            for (SvaResponseDataSva item : temp) {
                if (item.getBloquePadre() != null) {
                    for (String newItem : newNombre) {
                        if (item.getUnit() == newItem) {
                            item.setClickPadre(true);
                        } else {
                            item.setClickPadre(false);
                        }
                    }
                }
            }


            if (!list.isEmpty()) {
                if (!productCodeSVA.isEmpty()) {
                    if (productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL) || productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) {
                        int contador = 0;
                        Iterator<SvaResponseDataSva> listIterable = list.iterator();
                        while (listIterable.hasNext()) {
                            SvaResponseDataSva obj = listIterable.next();
                            if (obj.getCode().equals(ProductConstants.CODE_BLOQUE_PRODUCTO) ||
                                    obj.getCode().equals(ProductConstants.CODE_BLOQUE_TV)
                                    ) {
                                listIterable.remove();
                            }
                        }
                    }
                }
            }
            List<SvaResponseDataSva> equipmentList = new ArrayList<>();
            List<SvaResponseDataSva> svaList = new ArrayList<>();
            if (list.isEmpty()) {
                //response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_ERROR);
                responseData.setEquipment(equipmentList);
                responseData.setSva(svaList);
                response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_OK);
                response.setResponseData(responseData);
                //response.setResponseMessage(mensajeSvaSinDatosBD);
            } else {
                if (tdpCatalog != null) {
                    List<SvaResponseDataSva> svaListTmp = new ArrayList<>();
                    for (SvaResponseDataSva svaObject : list) {
                        if (svaObject.getCode().equals("DVR")) {
                            if (tdpCatalog.getTvTech().equalsIgnoreCase("CATV")) {
                                svaListTmp.add(svaObject);
                            }
                        } else if (svaObject.getCode().equals("DSHD")) {
                            if (tdpCatalog.getInternetSpeed() >= smartMb) {
                                svaListTmp.add(svaObject);
                            }
                        } else {
                            svaListTmp.add(svaObject);
                        }
                    }
                    list = svaListTmp;
                }

                String productCode = "";

                if (!productCodeSVA.isEmpty()) {
                    if (!productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL)
                            || !productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) {
                        for (SvaResponseDataSva svaObject : list) {
                            if ("BP".equalsIgnoreCase(svaObject.getCode()) && svaObject.isAltaPuraDefaultSVA()) {
                                productCode += svaObject.getUnit().replace("+", ",") + ",";
                            }
                        }
                        if (!productCode.equalsIgnoreCase("")) {
                            String[] nameProduct = productCode.replace("BLOQUE", "").split(",");

                            List<SvaResponseDataSva> svaListTmp = new ArrayList<>();
                            for (SvaResponseDataSva svaObject : list) {

                                if ("BTV".equalsIgnoreCase(svaObject.getCode())) {
                                    boolean isExiste = false;
                                    for (String obj : nameProduct) {
                                        if (svaObject.getUnit().indexOf(obj.trim()) >= 0) {
                                            isExiste = true;
                                        }
                                    }
                                    if (!isExiste) {
                                        svaListTmp.add(svaObject);
                                    }
                                } else {
                                    svaListTmp.add(svaObject);
                                }

                            }
                            list = svaListTmp;
                        }

                        productCode = "";
                        for (SvaResponseDataSva svaObject : list) {
                            if ("BTV".equalsIgnoreCase(svaObject.getCode()) && svaObject.isAltaPuraDefaultSVA()) {
                                productCode += svaObject.getUnit().replace("+", ",").replace("NEW", "") + ",";
                            }
                        }
                        if (!productCode.equalsIgnoreCase("")) {
                            String[] nameProduct = productCode.replace("BLOQUE", "").split(",");

                            List<SvaResponseDataSva> svaListTmp = new ArrayList<>();
                            for (SvaResponseDataSva svaObject : list) {
                                if ("BP".equalsIgnoreCase(svaObject.getCode())) {
                                    boolean isExiste = false;
                                    for (String obj : nameProduct) {
                                        if (svaObject.getUnit().indexOf(obj.trim()) >= 0) {
                                            isExiste = true;
                                        }
                                    }
                                    if (!isExiste) {
                                        svaListTmp.add(svaObject);
                                    }
                                } else {
                                    svaListTmp.add(svaObject);
                                }
                            }
                            list = svaListTmp;
                        }

                    }
                }

                for (SvaResponseDataSva svaObject : list) {
                    if ("EQUIPAMIENTO".equals(svaObject.getCode())) {
                        equipmentList.add(svaObject);
                    } else {
                        if ((svaObject.getCode().equals("DSHD") || svaObject.getCode().equals("DHD") || svaObject.getCode().equals("DVR")) && Integer.parseInt(svaObject.getUnit()) > maxDecosExtras)
                            continue;
                        svaList.add(svaObject);
                    }
                }

                responseData.setEquipment(equipmentList);
                responseData.setSva(svaList);

                response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_OK);
                response.setResponseData(responseData);
            }
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            logger.info("Fin Complements Service.");
            logger.error("Error Complements Service.", e);
            return response;
        }
        logger.info("Fin Complements Service.");

        return response;
    }


    public Response<List<ConfigParameterResponseData>> numDecos() {
        logger.info("Inicio NumDecos Service");

        Response<List<ConfigParameterResponseData>> response = new Response<>();
        List<ConfigParameterResponseData> configParameterResponseDataList = new ArrayList<>();

        configParameterResponseDataList = dao.numDecos();
        LogVass.serviceResponseObject(logger, "LoadUserDataDetails", "configParameterResponseData", configParameterResponseDataList);
        response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
        response.setResponseData(configParameterResponseDataList);

        logger.info("Fin NumDecos Service");
        return response;
    }

    public Response<SvaV2ResponseData> readSVA1_2(SvaV2Request request) {
        logger.info("Inicio ReadSVA1_2 Service.");
        Response<SvaV2ResponseData> response = new Response<>();
        SvaV2ResponseData responseData = new SvaV2ResponseData();

        SvaRequest SvaRequest = new SvaRequest();
        SvaRequest.setProductCode(request.getProductType());
        SvaRequest.setApplicationCode(request.getApplicationCode());
        try {
            List<SvaV2ResponseDataSva> listV2 = new ArrayList<SvaV2ResponseDataSva>();
            SvaV2ResponseDataSva svaV2ResponseDataSva = null;

            List<SvaResponseDataSva> list = dao.readSVA(SvaRequest);
            LogVass.serviceResponseArray(logger, "ReadSVA1_2", "list", list);

            /* Sprint 10 */
            TdpCatalog tdpCatalog = null;
            if (request.getProductId() != null) {
                tdpCatalog = tdpCatalogRepository.findOne(Integer.parseInt(request.getProductId()));
            }
            if (tdpCatalog != null) {
                List<SvaResponseDataSva> svaListTmp = new ArrayList<>();
                for (SvaResponseDataSva svaObject : list) {
                    if (svaObject.getCode().equals("DVR")) {
                        if (tdpCatalog.getTvTech().equalsIgnoreCase("CATV")) {
                            svaListTmp.add(svaObject);
                        }
                    } else if (svaObject.getCode().equals("DSHD")) {
                        if (tdpCatalog.getInternetSpeed() >= 30) {
                            svaListTmp.add(svaObject);
                        }
                    } else {
                        svaListTmp.add(svaObject);
                    }
                }
                list = svaListTmp;
            }
            /* Sprint 10 */

            for (SvaResponseDataSva svaResponseDataSva : list) {
                svaV2ResponseDataSva = new SvaV2ResponseDataSva();
                svaV2ResponseDataSva.setCode(svaResponseDataSva.getCode());
                svaV2ResponseDataSva.setDescription(svaResponseDataSva.getDescription());
                svaV2ResponseDataSva.setType(svaResponseDataSva.getType());
                svaV2ResponseDataSva.setUnit(svaResponseDataSva.getUnit());
                svaV2ResponseDataSva.setCost(Double.parseDouble(svaResponseDataSva.getCost()));
                svaV2ResponseDataSva.setPaymentDescription(svaResponseDataSva.getPaymentDescription());
                svaV2ResponseDataSva.setId(svaResponseDataSva.getId());
                svaV2ResponseDataSva.setFinancedAmount(svaResponseDataSva.getFinancingMonth());
                svaV2ResponseDataSva.setMaxDecos(cantidadMaximaDecos);
                listV2.add(svaV2ResponseDataSva);
            }
            LogVass.serviceResponseArray(logger, "ReadSVA1_2", "listV2", listV2);

            List<SvaV2ResponseDataSva> equipmentList = new ArrayList<>();
            List<SvaV2ResponseDataSva> svaList = new ArrayList<>();
            if (listV2.isEmpty()) {
                response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_ERROR);
                response.setResponseMessage(ProductConstants.SVA_RESPONSE_MESSAGE);
            } else {
                for (SvaV2ResponseDataSva svaObject : listV2) {
                    if ("EQUIPAMIENTO".equals(svaObject.getCode())) {
                        equipmentList.add(svaObject);
                    } else {
                        svaList.add(svaObject);
                    }
                }

                responseData.setEquipment(equipmentList);
                responseData.setSva(svaList);

                response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_OK);
                response.setResponseData(responseData);
            }
        } catch (Exception e) {
            response.setResponseCode("-2");
            response.setResponseMessage("Ocurrio un error.");
            logger.error("Error readSVA1_2 Service.", e);
            logger.info("Fin ReadSVA1_2 Service.");
            return response;
        }
        logger.info("Fin ReadSVA1_2 Service.");
        return response;
    }

    public Response<SvaV2ResponseData> readSVAv2(SvaV2Request request) {
        logger.info("Inicio ReadSVAv2 Service.");
        Response<SvaV2ResponseData> response = new Response<>();
        SvaV2ResponseData responseData = new SvaV2ResponseData();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("SVA");
        LogVass.serviceResponseHashMap(logger, "GetSVA", "messageList", messageList);
        HashMap<String, MessageEntities> messageArpu = messageUtil.getMessages("ARPU");
        LogVass.serviceResponseHashMap(logger, "GetArpu", "messageArpu", messageArpu);
        //End--Code for call Service

        Integer smartMb = 0;
        try {
            smartMb = Integer.parseInt(messageList.get("smartMb").getMessage());
        } catch (Exception eMb) {
            logger.error("Error Complements Service.", eMb);
        }

        try {
            OffersApiCalculadoraArpuRequestBody offersApiCalculadoraArpuRequestBody = new OffersApiCalculadoraArpuRequestBody();

            if (request.getLegacyCode().equals("ATIS")) {
                offersApiCalculadoraArpuRequestBody.setTelefono(request.getServiceCode());
            } else if (request.getLegacyCode().equals("CMS")) {
                offersApiCalculadoraArpuRequestBody.setTelefono("-" + request.getServiceCode());
            }

            ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalculadora;
            try {
                objCalculadora = getCalculadora(offersApiCalculadoraArpuRequestBody);
            } catch (ClientException e) {
                response.setResponseCode(messageArpu.get("errorArpu").getCode());
                response.setResponseMessage(messageArpu.get("errorArpu").getMessage());
                logger.error("ErrorClient ReadOffers2 Service.", e.getMessage());
                logger.info("Fin ReadOffers2 Service");
                Response<SvaV2ResponseData> data = new Response<>();
                data =readSVACalculadora(request);
                logger.info("Fin ReadOffers2 Service: "+data);
                List <SvaV2ResponseDataSva> listSva = validarSVA(data.getResponseData().getSva());
                data.getResponseData().setSva(listSva);
                data.getResponseData().setOffLine(true);
                //response = cargaSvaV2(data.getResponseData().getSva(), request);
                return data;// AGREGAR
            }

            String productCodeSVA = "";
            if (request.getProductCode() != null)
                productCodeSVA = request.getProductCode().toUpperCase();
            List<OffersApiCalculadoraArpuResponseBody> lstObjCalculadora = new ArrayList<OffersApiCalculadoraArpuResponseBody>();
            if (objCalculadora.getBodyOut() != null) {
                lstObjCalculadora = (List<OffersApiCalculadoraArpuResponseBody>) objCalculadora.getBodyOut();

                // Sòlo para pruebas, luego comentar.
                /*
                if (!productCodeSVA.equalsIgnoreCase("")) {
                    if (ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL.equals(request.getProductCode().toUpperCase())
                            || ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL.equals(request.getProductCode().toUpperCase())) {
                        OffersApiCalculadoraArpuResponseBody objItemCalculadora = new OffersApiCalculadoraArpuResponseBody();
                        objItemCalculadora.setTelefono(lstObjCalculadora.get(0).getTelefono());
                        objItemCalculadora.setServicio(lstObjCalculadora.get(0).getServicio());
                        objItemCalculadora.setPaquetePsOrigen(lstObjCalculadora.get(0).getPaquetePsOrigen());
                        objItemCalculadora.setPaqueteOrigen(lstObjCalculadora.get(0).getPaqueteOrigen());
                        objItemCalculadora.setCantidadDecos("1");
                        objItemCalculadora.setRentaTotal(lstObjCalculadora.get(0).getRentaTotal());
                        objItemCalculadora.setInternet(lstObjCalculadora.get(0).getInternet());
                        objItemCalculadora.setPrioridad(lstObjCalculadora.get(0).getPrioridad());
                        objItemCalculadora.setSVAs("21933");
                        objItemCalculadora.setProductoDestinoNombre("Deco Smart HD Alquiler");
                        objItemCalculadora.setCodigoOferta("207");
                        lstObjCalculadora.add(objItemCalculadora);

                        objItemCalculadora = new OffersApiCalculadoraArpuResponseBody();
                        objItemCalculadora.setTelefono(lstObjCalculadora.get(0).getTelefono());
                        objItemCalculadora.setServicio(lstObjCalculadora.get(0).getServicio());
                        objItemCalculadora.setPaquetePsOrigen(lstObjCalculadora.get(0).getPaquetePsOrigen());
                        objItemCalculadora.setPaqueteOrigen(lstObjCalculadora.get(0).getPaqueteOrigen());
                        objItemCalculadora.setCantidadDecos("1");
                        objItemCalculadora.setRentaTotal(lstObjCalculadora.get(0).getRentaTotal());
                        objItemCalculadora.setInternet(lstObjCalculadora.get(0).getInternet());
                        objItemCalculadora.setPrioridad(lstObjCalculadora.get(0).getPrioridad());
                        objItemCalculadora.setSVAs("21931");
                        objItemCalculadora.setProductoDestinoNombre("Deco HD Alquiler");
                        objItemCalculadora.setCodigoOferta("207");
                        lstObjCalculadora.add(objItemCalculadora);

                        objItemCalculadora = new OffersApiCalculadoraArpuResponseBody();
                        objItemCalculadora.setTelefono(lstObjCalculadora.get(0).getTelefono());
                        objItemCalculadora.setServicio(lstObjCalculadora.get(0).getServicio());
                        objItemCalculadora.setPaquetePsOrigen(lstObjCalculadora.get(0).getPaquetePsOrigen());
                        objItemCalculadora.setPaqueteOrigen(lstObjCalculadora.get(0).getPaqueteOrigen());
                        objItemCalculadora.setCantidadDecos("1");
                        objItemCalculadora.setRentaTotal(lstObjCalculadora.get(0).getRentaTotal());
                        objItemCalculadora.setInternet(lstObjCalculadora.get(0).getInternet());
                        objItemCalculadora.setPrioridad(lstObjCalculadora.get(0).getPrioridad());
                        objItemCalculadora.setSVAs("20449");
                        objItemCalculadora.setProductoDestinoNombre("Deco HD DVR Alquiler");
                        objItemCalculadora.setCodigoOferta("207");
                        lstObjCalculadora.add(objItemCalculadora);
                    }

                }
                */
                LogVass.serviceResponseArray(logger, "ReadSVAv2", "lstObjCalculadora", lstObjCalculadora);
            } else {
                String clienteNoEncontrado = messageArpu.get("sinOfertasArpu").getMessage();
                response.setResponseCode(messageArpu.get("sinOfertasArpu").getCode());
                response.setResponseMessage(clienteNoEncontrado);

                logger.info("ReadOffers2 => " + clienteNoEncontrado);
                logger.info("Fin ReadSVAv2 Service.");
                response.getResponseData().setOffLine(false);
                return response;
            }

            List<String> listaId = new ArrayList<>();
            Double maxDecos = 6.0;
            Double maxDecosByFloor = 6.0;

            try {
                Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "POLICY_PARAMETER", "max_amount_decoders_by_floor");
                maxDecos = Double.parseDouble(objParameter.getStrValue());
                if ((ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL).equals(productCodeSVA)
                        || ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL.equals(productCodeSVA)) {
                    maxDecosByFloor = 3.0;
                } else {
                    maxDecosByFloor = Double.parseDouble(objParameter.getStrValue());
                }

            } catch (Exception e) {
                logger.error("Error ReadSVAv2 Service. - Parameters.", e);
            }
            Double arpu = 0.0;
            Double velInter = 0.0;

            for (OffersApiCalculadoraArpuResponseBody offersApiCalculadoraArpuResponseBody : lstObjCalculadora) {
                try {
                    arpu = Double.parseDouble(offersApiCalculadoraArpuResponseBody.getRentaTotal());
                } catch (Exception ex) {
                    arpu = 0.0;
                }
                try {
                    Double decos = 0.0;
                    if (offersApiCalculadoraArpuResponseBody.getCantidadDecos() != null) {
                        decos = Double.parseDouble(offersApiCalculadoraArpuResponseBody.getCantidadDecos());
                    }
                    maxDecos = maxDecosByFloor - decos;
                    if (maxDecos < 0) {
                        maxDecos = 0.0;
                    }
                } catch (Exception ex) {
                    maxDecos = 0.0;
                }
                try {
                    velInter = Double.parseDouble(offersApiCalculadoraArpuResponseBody.getInternet());
                } catch (Exception ex) {
                    velInter = 0.0;
                }
                if (request.getProductCode() != null && request.getProductCode().length() > 0) {
                    //Migraciones
                    if ((request.getProductCode().equals(offersApiCalculadoraArpuResponseBody.getProductoDestinoPs())
                            && offersApiCalculadoraArpuResponseBody.getSVAs() != null)
                            || (offersApiCalculadoraArpuResponseBody.getSVAs() != null
                            && offersApiCalculadoraArpuResponseBody.getProductoDestinoPs() == null)) {
                        listaId.add(offersApiCalculadoraArpuResponseBody.getSVAs());
                    }
                } else {
                    //SVA puro
                    if ((request.getProductCode() == null || request.getProductCode().length() <= 0)
                            && offersApiCalculadoraArpuResponseBody.getSVAs() != null
                            && offersApiCalculadoraArpuResponseBody.getProductoDestinoPs() == null) {
                        listaId.add(offersApiCalculadoraArpuResponseBody.getSVAs());
                    }
                }
            }
            LogVass.serviceResponseArray(logger, "ReadSVAv2", "listaId", listaId);

            /* Sprint 10 */
            TdpCatalog tdpCatalog = null;
            if (request.getProductId() != null) {
                tdpCatalog = tdpCatalogRepository.findOne(Integer.parseInt(request.getProductId()));
            }
            if (tdpCatalog != null) {
                request.setMigracionSvasBloqueTV(tdpCatalog.getProductBloqueTv().replace(" ", ""));
                request.setMigracionSvasLinea(tdpCatalog.getProductSvaLinea().replace(" ", ""));
                request.setMigracionSvasInternet(tdpCatalog.getProductSvaInternet().replace(" ", ""));
            }
            generarConsultaDefaultSvasMigracion(request);

            //lista de SVAs de la bd
            List<SvaV2ResponseDataSva> listaSvas = dao.getSvaByProductCode(listaId, maxDecos, request, "NORMAL");
            LogVass.serviceResponseArray(logger, "ReadSVAv2", "listaSvas", listaSvas);

            if (tdpCatalog != null) {
                productCodeSVA = tdpCatalog.getProductCode();
                List<SvaV2ResponseDataSva> svaListTmp = new ArrayList<>();
                for (SvaV2ResponseDataSva svaObject : listaSvas) {
                    if (svaObject.getCode().equals("DVR")) {
                        if (tdpCatalog.getTvTech().equalsIgnoreCase("CATV")) {
                            svaListTmp.add(svaObject);
                        }
                    } else if (svaObject.getCode().equals("DSHD")) {
                        if (tdpCatalog.getInternetSpeed() >= smartMb) {
                            svaListTmp.add(svaObject);
                        }
                    } else {
                        svaListTmp.add(svaObject);
                    }
                }
                listaSvas = svaListTmp;
            }

            if (!listaSvas.isEmpty()) {
                if (!productCodeSVA.isEmpty()) {
                    if (productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL) || productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) {
                        Iterator<SvaV2ResponseDataSva> listIterable = listaSvas.iterator();
                        while (listIterable.hasNext()) {
                            SvaV2ResponseDataSva obj = listIterable.next();
                            if (obj.getCode().equals(ProductConstants.CODE_BLOQUE_PRODUCTO) ||
                                    obj.getCode().equals(ProductConstants.CODE_BLOQUE_TV)
                            ) {
                                listIterable.remove();
                            }
                        }
                    }
                }
            }

            if (request.getSvaMigracionesUnit() != null && request.getSvaMigracionesCode() != null) {
                String codeMigraciones = "BP";
                String productCode = request.getSvaMigracionesUnit().replace("+", ",").replace("NEW", "") + ",";
                if (request.getSvaMigracionesCode().equalsIgnoreCase("BP")) {
                    codeMigraciones = "BTV";
                    productCode = request.getSvaMigracionesUnit().replace("+", ",") + ",";
                }
                if (!productCode.equalsIgnoreCase("")) {
                    String[] nameProduct = productCode.replace("BLOQUE", "").split(",");

                    List<SvaV2ResponseDataSva> svaListTmp = new ArrayList<>();
                    for (SvaV2ResponseDataSva svaObject : listaSvas) {
                        if (codeMigraciones.equalsIgnoreCase(svaObject.getCode())) {
                            boolean isExiste = false;
                            for (String obj : nameProduct) {
                                if (svaObject.getUnit().indexOf(obj.trim()) >= 0) {
                                    isExiste = true;
                                }
                            }
                            if (!isExiste) {
                                svaListTmp.add(svaObject);
                            }
                        } else {
                            svaListTmp.add(svaObject);
                        }
                    }
                    listaSvas = svaListTmp;
                }
            }
            /* Sprint 10 */

            String applicationCode = "WEBVF";
            if (request.getApplicationCode() != null) {
                applicationCode = request.getApplicationCode();
            }
            List<SvaV2ResponseDataSva> listEquipamiento = dao.getEquipamientoDecos(request.getProductType(), applicationCode);
            LogVass.serviceResponseArray(logger, "ReadSVAv2", "listEquipamiento", listEquipamiento);
            //listaSvas.addAll(listEquipamiento);

            String idSVA = "|";
            for (SvaV2ResponseDataSva svaV2ResponseDataSva : listaSvas) {
                svaV2ResponseDataSva.setArpu(arpu);
                svaV2ResponseDataSva.setMaxDecos(maxDecos.toString());
                svaV2ResponseDataSva.setVelInter(velInter.toString());
                idSVA += svaV2ResponseDataSva.getId() + "|";
            }
            LogVass.serviceResponseArray(logger, "ReadSVAv2", "listaSvas", listaSvas);

            List<SvaV2ResponseDataSva> equipmentList = new ArrayList<>();
            //List<SvaV2ResponseDataSva> svaList = new ArrayList<>();
            //if (listaSvas.isEmpty()) {
            //    response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_ERROR);
            //    response.setResponseMessage(ProductConstants.SVA_RESPONSE_MESSAGE);
            //} else {
            for (SvaV2ResponseDataSva svaObject : listEquipamiento) {
                if ("EQUIPAMIENTO".equals(svaObject.getCode())) {
                    equipmentList.add(svaObject);
                } else {
                    if (idSVA.indexOf("|" + svaObject.getId() + "|") < -1)
                        if (request.getProductCode() != null && request.getProductCode().length() > 0) {
                            if (Double.parseDouble(svaObject.getUnit()) <= maxDecos) {
                                listaSvas.add(svaObject);
                            }
                        }
                }
            }

            responseData.setEquipment(equipmentList);
            responseData.setSva(listaSvas);
            responseData.setOffLine(false);
            response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_OK);
            response.setResponseData(responseData);

        } catch (Exception e) {
            logger.error("Error ReadSVAv2 Service => Exception: ", e);
            response.setResponseCode(messageList.get("errorSVA").getCode());
            response.setResponseMessage(messageList.get("errorSVA").getMessage());
            //response.setResponseCode("-2");
            //response.setResponseMessage("SVA no disponible temporalmente. Intente nuevamente.");
            logger.info("Fin ReadSVAv2 Service.");
            return response;
        }
        logger.info("Fin ReadSVAv2 Service.");

        return response;
    }

    private Response<SvaV2ResponseData> cargaSvaV2(List<SvaResponseDataSva> sva, SvaV2Request  sadd) {

        Response<SvaV2ResponseData> response = new Response<>();
        SvaV2ResponseData data = new SvaV2ResponseData();
        List<SvaV2ResponseDataSva> list = new ArrayList<>();
        for (SvaResponseDataSva dat :sva){
            SvaV2ResponseDataSva item = new SvaV2ResponseDataSva();

            item.setId(dat.getId());
            item.setCode(dat.getCode());
            item.setDescription(dat.getDescription());
            item.setType(dat.getType());
            item.setUnit(dat.getUnit());
            item.setCost(Double.parseDouble(dat.getCost()));
            item.setPaymentDescription(dat.getPaymentDescription());
            item.setFinancingMonth(dat.getFinancingMonth());
            item.setHasEquipment("");
            item.setMaxDecos("");
            item.setArpu(2.00);//preio de producto actual
            item.setVelInter("");//velociad de inter
            item.setFinancedAmount(0.00);
            item.setFinancedQuotas(0);
            item.setProductCode(" ");//cod del sva a ofrecer
            item.setAltaPuraDefaultSVA(dat.isAltaPuraDefaultSVA());
            item.setBloquePadre(dat.getBloquePadre());
            item.setClickPadre(dat.isClickPadre());

            list.add(item);
        }
        data.setSva(list);
        response.setResponseData(data);
        return response;
    }


    public Response<SvaV2ResponseData> readOutSVAv2(SvaOutV2Request svaOutV2Request) {

        logger.info("Inicio readOutSVAv2 Service.");

        SvaV2Request request = new SvaV2Request();
        request.setProductCode(svaOutV2Request.getProductCode());
        request.setProductId(svaOutV2Request.getProductId());
        request.setServiceCode(svaOutV2Request.getServiceCode());
        request.setProductType(svaOutV2Request.getProductType());
        request.setLegacyCode(svaOutV2Request.getLegacyCode());
        request.setApplicationCode(svaOutV2Request.getApplicationCode());
        request.setSvaMigracionesUnit(svaOutV2Request.getSvaMigracionesUnit());
        request.setSvaMigracionesCode(svaOutV2Request.getSvaMigracionesCode());
        request.setMigracionSvasAll(svaOutV2Request.getMigracionSvasAll());
        request.setMigracionSvasBloqueTV(svaOutV2Request.getMigracionSvasBloqueTV());
        request.setMigracionSvasLinea(svaOutV2Request.getMigracionSvasLinea());
        request.setMigracionSvasInternet(svaOutV2Request.getMigracionSvasInternet());
        request.setSellerChannelEquivalentCampaign(svaOutV2Request.getSellerChannelEquivalentCampaign());

        Response<SvaV2ResponseData> response = new Response<>();
        SvaV2ResponseData responseData = new SvaV2ResponseData();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("SVA");
        LogVass.serviceResponseHashMap(logger, "GetSVA", "messageList", messageList);
        HashMap<String, MessageEntities> messageArpu = messageUtil.getMessages("ARPU");
        LogVass.serviceResponseHashMap(logger, "GetArpu", "messageArpu", messageArpu);
        //End--Code for call Service

        Integer smartMb = 0;
        try {
            smartMb = Integer.parseInt(messageList.get("smartMb").getMessage());
        } catch (Exception eMb) {
            logger.error("Error Complements Service.", eMb);
        }

        try {
            OffersApiCalculadoraArpuRequestBody offersApiCalculadoraArpuRequestBody = new OffersApiCalculadoraArpuRequestBody();

            if (request.getLegacyCode().equals("ATIS")) {
                offersApiCalculadoraArpuRequestBody.setTelefono(request.getServiceCode());
            } else if (request.getLegacyCode().equals("CMS")) {
                offersApiCalculadoraArpuRequestBody.setTelefono("-" + request.getServiceCode());
            }
            //sprint 25 offline Calculadora
            ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalculadora;
            try {
                objCalculadora = getCalculadora(offersApiCalculadoraArpuRequestBody);
            } catch (ClientException e) {
                response.setResponseCode(messageArpu.get("errorArpu").getCode());
                response.setResponseMessage(messageArpu.get("errorArpu").getMessage());
                logger.error("ErrorClient ReadOffers2 Service.", e.getMessage());
                logger.info("Fin ReadOffers2 Service");
                Response<SvaV2ResponseData> data = new Response<>();
                data =readSVACalculadora(request);
                logger.info("Fin ReadOffers2 Service: "+data);
                List <SvaV2ResponseDataSva> listSva = validarSVA(data.getResponseData().getSva());
                data.getResponseData().setSva(listSva);
                data.getResponseData().setOffLine(true);
                //response = cargaSvaV2(data.getResponseData().getSva(), request);
                return data;// AGREGAR
            }

            String productCodeSVA = "";
            //sprint 25 offline Calculadora
            if (request.getProductCode() != null)
                productCodeSVA = request.getProductCode().toUpperCase();
            List<OffersApiCalculadoraArpuResponseBody> lstObjCalculadora = new ArrayList<OffersApiCalculadoraArpuResponseBody>();


            if (objCalculadora == null || objCalculadora.getBodyOut() == null) {
                lstObjCalculadora=contingenciaCalculadora(offersApiCalculadoraArpuRequestBody.getTelefono());

            } else {
                if(objCalculadora != null || objCalculadora.getBodyOut() != null){
                lstObjCalculadora = (List<OffersApiCalculadoraArpuResponseBody>) objCalculadora.getBodyOut();
                    for( OffersApiCalculadoraArpuResponseBody item : lstObjCalculadora){
                        if(item.getPaqueteOrigen() == null){
                            if(item.getPaquetePsOrigen() == null){
                                if(item.getProductoDestinoPs() == null){
                                    if(item.getProductoDestinoNombre() == null){
                                        lstObjCalculadora=contingenciaCalculadora(offersApiCalculadoraArpuRequestBody.getTelefono());
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                // Sòlo para pruebas, luego comentar.
                /*
                if (!productCodeSVA.equalsIgnoreCase("")) {
                    if (ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL.equals(request.getProductCode().toUpperCase())
                            || ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL.equals(request.getProductCode().toUpperCase())) {
                        OffersApiCalculadoraArpuResponseBody objItemCalculadora = new OffersApiCalculadoraArpuResponseBody();
                        objItemCalculadora.setTelefono(lstObjCalculadora.get(0).getTelefono());
                        objItemCalculadora.setServicio(lstObjCalculadora.get(0).getServicio());
                        objItemCalculadora.setPaquetePsOrigen(lstObjCalculadora.get(0).getPaquetePsOrigen());
                        objItemCalculadora.setPaqueteOrigen(lstObjCalculadora.get(0).getPaqueteOrigen());
                        objItemCalculadora.setCantidadDecos("1");
                        objItemCalculadora.setRentaTotal(lstObjCalculadora.get(0).getRentaTotal());
                        objItemCalculadora.setInternet(lstObjCalculadora.get(0).getInternet());
                        objItemCalculadora.setPrioridad(lstObjCalculadora.get(0).getPrioridad());
                        objItemCalculadora.setSVAs("21933");
                        objItemCalculadora.setProductoDestinoNombre("Deco Smart HD Alquiler");
                        objItemCalculadora.setCodigoOferta("207");
                        lstObjCalculadora.add(objItemCalculadora);

                        objItemCalculadora = new OffersApiCalculadoraArpuResponseBody();
                        objItemCalculadora.setTelefono(lstObjCalculadora.get(0).getTelefono());
                        objItemCalculadora.setServicio(lstObjCalculadora.get(0).getServicio());
                        objItemCalculadora.setPaquetePsOrigen(lstObjCalculadora.get(0).getPaquetePsOrigen());
                        objItemCalculadora.setPaqueteOrigen(lstObjCalculadora.get(0).getPaqueteOrigen());
                        objItemCalculadora.setCantidadDecos("1");
                        objItemCalculadora.setRentaTotal(lstObjCalculadora.get(0).getRentaTotal());
                        objItemCalculadora.setInternet(lstObjCalculadora.get(0).getInternet());
                        objItemCalculadora.setPrioridad(lstObjCalculadora.get(0).getPrioridad());
                        objItemCalculadora.setSVAs("21931");
                        objItemCalculadora.setProductoDestinoNombre("Deco HD Alquiler");
                        objItemCalculadora.setCodigoOferta("207");
                        lstObjCalculadora.add(objItemCalculadora);

                        objItemCalculadora = new OffersApiCalculadoraArpuResponseBody();
                        objItemCalculadora.setTelefono(lstObjCalculadora.get(0).getTelefono());
                        objItemCalculadora.setServicio(lstObjCalculadora.get(0).getServicio());
                        objItemCalculadora.setPaquetePsOrigen(lstObjCalculadora.get(0).getPaquetePsOrigen());
                        objItemCalculadora.setPaqueteOrigen(lstObjCalculadora.get(0).getPaqueteOrigen());
                        objItemCalculadora.setCantidadDecos("1");
                        objItemCalculadora.setRentaTotal(lstObjCalculadora.get(0).getRentaTotal());
                        objItemCalculadora.setInternet(lstObjCalculadora.get(0).getInternet());
                        objItemCalculadora.setPrioridad(lstObjCalculadora.get(0).getPrioridad());
                        objItemCalculadora.setSVAs("20449");
                        objItemCalculadora.setProductoDestinoNombre("Deco HD DVR Alquiler");
                        objItemCalculadora.setCodigoOferta("207");
                        lstObjCalculadora.add(objItemCalculadora);
                    }

                }
                */
                LogVass.serviceResponseArray(logger, "ReadSVAv2", "lstObjCalculadora", lstObjCalculadora);
            }

            List<String> listaId = new ArrayList<>();
            Double maxDecos = 6.0;
            Double maxDecosByFloor = 6.0;

            try {
                Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "POLICY_PARAMETER", "max_amount_decoders_by_floor");
                maxDecos = Double.parseDouble(objParameter.getStrValue());
                if ((ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL).equals(productCodeSVA)
                        || ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL.equals(productCodeSVA)) {
                    maxDecosByFloor = 3.0;
                } else {
                    maxDecosByFloor = Double.parseDouble(objParameter.getStrValue());
                }

            } catch (Exception e) {
                logger.error("Error ReadSVAv2 Service. - Parameters.", e);
            }
            Double arpu = 0.0;
            Double velInter = 0.0;

            for (OffersApiCalculadoraArpuResponseBody offersApiCalculadoraArpuResponseBody : lstObjCalculadora) {
                try {
                    arpu = Double.parseDouble(offersApiCalculadoraArpuResponseBody.getRentaTotal());
                } catch (Exception ex) {
                    arpu = 0.0;
                }
                try {
                    Double decos = 0.0;
                    if (offersApiCalculadoraArpuResponseBody.getCantidadDecos() != null) {
                        decos = Double.parseDouble(offersApiCalculadoraArpuResponseBody.getCantidadDecos());
                    }
                    maxDecos = maxDecosByFloor - decos;
                    if (maxDecos < 0) {
                        maxDecos = 0.0;
                    }
                } catch (Exception ex) {
                    maxDecos = 0.0;
                }
                try {
                    velInter = Double.parseDouble(offersApiCalculadoraArpuResponseBody.getInternet());
                } catch (Exception ex) {
                    velInter = 0.0;
                }
                if (request.getProductCode() != null && request.getProductCode().length() > 0) {
                    //Migraciones
                    if ((request.getProductCode().equals(offersApiCalculadoraArpuResponseBody.getProductoDestinoPs())
                            && offersApiCalculadoraArpuResponseBody.getSVAs() != null)
                            || (offersApiCalculadoraArpuResponseBody.getSVAs() != null
                            && offersApiCalculadoraArpuResponseBody.getProductoDestinoPs() == null)) {
                        listaId.add(offersApiCalculadoraArpuResponseBody.getSVAs());
                    }
                } else {
                    //SVA puro
                    if ((request.getProductCode() == null || request.getProductCode().length() <= 0)
                            && offersApiCalculadoraArpuResponseBody.getSVAs() != null
                            && offersApiCalculadoraArpuResponseBody.getProductoDestinoPs() == null) {
                        //listaId.add(offersApiCalculadoraArpuResponseBody.getSVAs());
                    }
                }
            }
            LogVass.serviceResponseArray(logger, "ReadSVAv2", "listaId", listaId);

            /* Sprint 10 */
            TdpCatalog tdpCatalog = null;
//            if (request.getProductId() != null) {
//                tdpCatalog = tdpCatalogRepository.findOne(Integer.parseInt(request.getProductId()));
//            }
//            if (tdpCatalog != null) {
//                request.setMigracionSvasBloqueTV(tdpCatalog.getProductBloqueTv().replace(" ", ""));
//                request.setMigracionSvasLinea(tdpCatalog.getProductSvaLinea().replace(" ", ""));
//                request.setMigracionSvasInternet(tdpCatalog.getProductSvaInternet().replace(" ", ""));
//            }

            String[] listaSvaPropios = request.getMigracionSvasBloqueTV().split(",");
            for (int indSvas = 0; indSvas < listaSvaPropios.length; indSvas++) {
                listaId.add(listaSvaPropios[indSvas]);
            }

            // que no sea FLUJO SVA PURO
            if(request.getProductCode() != null && request.getProductCode().length() > 0)
                generarConsultaDefaultSvasMigracion(request);

            if (request.getProductId() != null){
            tdpCatalog = new TdpCatalog();
            tdpCatalog.setProductCode(svaOutV2Request.getProductCode());
            tdpCatalog.setTvTech(svaOutV2Request.getTvTech());
            tdpCatalog.setInternetSpeed(svaOutV2Request.getInternetSpeed());

            }


            //lista de SVAs de la bd
            List<SvaV2ResponseDataSva> listaSvas = dao.getSvaByProductCode(listaId, maxDecos, request, "OUT");
            LogVass.serviceResponseArray(logger, "ReadSVAv2", "listaSvas", listaSvas);

            if (tdpCatalog != null) {
                productCodeSVA = (tdpCatalog.getProductCode()==null?"":tdpCatalog.getProductCode());
                List<SvaV2ResponseDataSva> svaListTmp = new ArrayList<>();
                for (SvaV2ResponseDataSva svaObject : listaSvas) {
                    if (svaObject.getCode().equals("DVR")) {
                        if (tdpCatalog.getTvTech().equalsIgnoreCase("CATV")) {
                            svaListTmp.add(svaObject);
                        }
                    } else if (svaObject.getCode().equals("DSHD")) {
                        if (tdpCatalog.getInternetSpeed() >= smartMb) {
                            svaListTmp.add(svaObject);
                        }
                    } else {
                        svaListTmp.add(svaObject);
                    }
                }
                listaSvas = svaListTmp;
            }

            if (!listaSvas.isEmpty()) {
                if (!productCodeSVA.isEmpty()) {
                    if (productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL) || productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) {
                        Iterator<SvaV2ResponseDataSva> listIterable = listaSvas.iterator();
                        while (listIterable.hasNext()) {
                            SvaV2ResponseDataSva obj = listIterable.next();
                            if (obj.getCode().equals(ProductConstants.CODE_BLOQUE_PRODUCTO) ||
                                    obj.getCode().equals(ProductConstants.CODE_BLOQUE_TV)
                                    ) {
                                listIterable.remove();
                            }
                        }
                    }
                }
            }

            if (request.getSvaMigracionesUnit() != null && request.getSvaMigracionesCode() != null) {
                String codeMigraciones = "BP";
                String productCode = request.getSvaMigracionesUnit().replace("+", ",").replace("NEW", "") + ",";
                if (request.getSvaMigracionesCode().equalsIgnoreCase("BP")) {
                    codeMigraciones = "BTV";
                    productCode = request.getSvaMigracionesUnit().replace("+", ",") + ",";
                }
                if (!productCode.equalsIgnoreCase("")) {
                    String[] nameProduct = productCode.replace("BLOQUE", "").split(",");

                    List<SvaV2ResponseDataSva> svaListTmp = new ArrayList<>();
                    for (SvaV2ResponseDataSva svaObject : listaSvas) {
                        if (codeMigraciones.equalsIgnoreCase(svaObject.getCode())) {
                            boolean isExiste = false;
                            for (String obj : nameProduct) {
                                if (svaObject.getUnit().indexOf(obj.trim()) >= 0) {
                                    isExiste = true;
                                }
                            }
                            if (!isExiste) {
                                svaListTmp.add(svaObject);
                            }
                        } else {
                            svaListTmp.add(svaObject);
                        }
                    }
                    listaSvas = svaListTmp;
                }
            }
            /* Sprint 10 */

            String applicationCode = "WEBVF";
            if (request.getApplicationCode() != null) {
                applicationCode = request.getApplicationCode();
            }
            List<SvaV2ResponseDataSva> listEquipamiento = dao.getEquipamientoDecos(request.getProductType(), applicationCode);
            LogVass.serviceResponseArray(logger, "ReadSVAv2", "listEquipamiento", listEquipamiento);
            //listaSvas.addAll(listEquipamiento);

            String idSVA = "|";
            for (SvaV2ResponseDataSva svaV2ResponseDataSva : listaSvas) {
                svaV2ResponseDataSva.setArpu(arpu);
                svaV2ResponseDataSva.setMaxDecos(maxDecos.toString());
                svaV2ResponseDataSva.setVelInter(velInter.toString());
                idSVA += svaV2ResponseDataSva.getId() + "|";
            }
            LogVass.serviceResponseArray(logger, "ReadSVAv2", "listaSvas", listaSvas);

            List<SvaV2ResponseDataSva> equipmentList = new ArrayList<>();
            //List<SvaV2ResponseDataSva> svaList = new ArrayList<>();
            //if (listaSvas.isEmpty()) {
            //    response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_ERROR);
            //    response.setResponseMessage(ProductConstants.SVA_RESPONSE_MESSAGE);
            //} else {
            for (SvaV2ResponseDataSva svaObject : listEquipamiento) {
                if ("EQUIPAMIENTO".equals(svaObject.getCode())) {
                    equipmentList.add(svaObject);
                } else {
                    if (idSVA.indexOf("|" + svaObject.getId() + "|") < -1)
                        if (request.getProductCode() != null && request.getProductCode().length() > 0) {
                            if (Double.parseDouble(svaObject.getUnit()) <= maxDecos) {
                                listaSvas.add(svaObject);
                            }
                        }
                }
            }

            responseData.setEquipment(equipmentList);
            responseData.setSva(listaSvas);

            response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_OK);
            response.setResponseData(responseData);

        } catch (Exception e) {
            logger.error("Error ReadSVAv2 Service => Exception: ", e);
            response.setResponseCode(messageList.get("errorSVA").getCode());
            response.setResponseMessage(messageList.get("errorSVA").getMessage());
            //response.setResponseCode("-2");
            //response.setResponseMessage("SVA no disponible temporalmente. Intente nuevamente.");
            logger.info("Fin ReadSVAv2 Service.");
            return response;
        }
        logger.info("Fin ReadSVAv2 Service.");
        return response;
    }

    private List <SvaV2ResponseDataSva> validarSVA(List <SvaV2ResponseDataSva> datos) {
        List <SvaV2ResponseDataSva> list= null;
        for(SvaV2ResponseDataSva item :datos){
           if(list == null){
               list = new ArrayList<>();
               list.add(item);
           }
            SvaV2ResponseDataSva dat= list.get(list.size() -1 );
               if(!dat.getUnit().equals(item.getUnit()) && dat.getCost() != item.getCost()){
                   list.add(item);
               }
           }
        return list;
    }


    public Response<List<OffersResponseData2>> readOffers2(OffersRequest2 request) {
        logger.info("Inicio ReadOffers2 Service");
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("PRODUCTO");
        LogVass.serviceResponseHashMap(logger, "GetProduct", "messageList", messageList);
        HashMap<String, MessageEntities> messageArpu = messageUtil.getMessages("ARPU");
        LogVass.serviceResponseHashMap(logger, "GetArpu", "messageArpu", messageArpu);
        //End--Code for call Service
        Response<List<OffersResponseData2>> response = new Response<>();
        try {
            List<OffersResponseData2> listOffers = new ArrayList<>();
            List<OffersResponseData2> responseData = new ArrayList<>();
            // Variable para el bypass de AVVE en Migraciones.
            boolean byPassAvve = false;

            // Inicio del Experto
            ApiResponse<OffersApiExpertoResponseBody> objExperto = null;

            // Si es alta no tiene Tipo de Producto: prodTypeCode
            if (request.getProdTypeCode() != null && request.getProdTypeCode().length() <= 0) {
                OffersApiExpertoRequestBody apiExpertoRequestBody = new OffersApiExpertoRequestBody();
                apiExpertoRequestBody.setApellidoPaterno(Constants.DOES_NOT_APPLY);
                apiExpertoRequestBody.setApellidoMaterno(Constants.DOES_NOT_APPLY);
                apiExpertoRequestBody.setNombres(Constants.DOES_NOT_APPLY);

                // Realizamos la equivalencia para ParqueATIS
                String codigoTipoDocumentoEquivalencia = parametersService.equivalenceDocument("EQUIFAX",  request.getDocumentType().toUpperCase());

                apiExpertoRequestBody.setTipoDeDocumento(codigoTipoDocumentoEquivalencia);
                apiExpertoRequestBody.setNumeroDeDocumento(request.getDocumentNumber());
                apiExpertoRequestBody.setDepartamento(request.getDepartment());
                apiExpertoRequestBody.setProvincia(request.getProvince());
                apiExpertoRequestBody.setDistrito(request.getDistrict());

                // Valida el código Legacy: legacyCode
                if (Constants.EMPTY.equals(request.getLegacyCode()) || request.getLegacyCode() == null) {
                    apiExpertoRequestBody.setUbigeo(request.getUbigeoCode());
                } else {
                    OffersRequest offersRequest = convertOffering2ToOffering(request);
                    apiExpertoRequestBody.setUbigeo(dao.getUbigeo(offersRequest));
                    logger.info("ReadOffers2 => Nuevo Ubigeo: " + apiExpertoRequestBody.getUbigeo());
                }

                apiExpertoRequestBody.setCodVendedor(request.getAtisSellerCode());
                apiExpertoRequestBody.setZonal("");

                if (request.getScoringResponse() == null) {
                    objExperto = getExperto(apiExpertoRequestBody);

                    JSONObject jsonExperto = new JSONObject(objExperto);
                    logger.info("ReadOffers2 => scoring: {}");
                    logger.info("ReadOffers2 => objExperto: " + jsonExperto);
                } else {
                    objExperto = new ApiResponse<OffersApiExpertoResponseBody>();

                    OffersApiExpertoResponseBody offersApiExpertoResponseBody = new OffersApiExpertoResponseBody();
                    CustomerScoringResponse scoring = request.getScoringResponse();
                    List<OffersApiExpertoResponseBodyOperacion> operacionComercial = new ArrayList<OffersApiExpertoResponseBodyOperacion>();
                    OffersApiExpertoResponseBodyOperacion responseBodyOperacion = null;
                    List<OffersApiExpertoResponseBodyOperacionDetalle> lstBdyOperacionDetalle = null;
                    OffersApiExpertoResponseBodyOperacionDetalle bodyOperacionDetalle = null;
                    ApiResponseHeader header = new ApiResponseHeader();

                    offersApiExpertoResponseBody.setCodConsulta(scoring.getCodConsulta());
                    offersApiExpertoResponseBody.setAccion(scoring.getAccion());
                    offersApiExpertoResponseBody.setDeudaAtis(scoring.getDeudaAtis());
                    offersApiExpertoResponseBody.setDeudaCms(scoring.getDeudaCms());


                    for (OperacionComercial operacionComercialTmp : scoring.getResult()) {
                        boolean entro = false;

                        responseBodyOperacion = new OffersApiExpertoResponseBodyOperacion();
                        lstBdyOperacionDetalle = new ArrayList<OffersApiExpertoResponseBodyOperacionDetalle>();
                        responseBodyOperacion.setCodOpe(operacionComercialTmp.getCodOpe());
                        responseBodyOperacion.setRenta(operacionComercialTmp.getRenta());
                        responseBodyOperacion.setSegmento(operacionComercialTmp.getSegmento());
                        responseBodyOperacion.setContadorDetalle(operacionComercialTmp.getContadorDetalle());

                        for (OperacionComercialDetalle bodyOperacionDetalletmp : operacionComercialTmp.getDetalle()) {
                            entro = true;
                            bodyOperacionDetalle = new OffersApiExpertoResponseBodyOperacionDetalle();
                            bodyOperacionDetalle.setCodProd(bodyOperacionDetalletmp.getCodProd());
                            bodyOperacionDetalle.setCuotaFin(bodyOperacionDetalletmp.getCuotaFin());
                            bodyOperacionDetalle.setDesProd(bodyOperacionDetalletmp.getDesProd());
                            bodyOperacionDetalle.setMesesFin(bodyOperacionDetalletmp.getMesesFin());
                            bodyOperacionDetalle.setNroDecosAdic(bodyOperacionDetalletmp.getNroDecosAdic());
                            bodyOperacionDetalle.setPagoAdelantado(bodyOperacionDetalletmp.getPagoAdelantado());
                            lstBdyOperacionDetalle.add(bodyOperacionDetalle);
                        }
                        if (entro) {
                            responseBodyOperacion.setDetalle(lstBdyOperacionDetalle);
                        }
                        operacionComercial.add(responseBodyOperacion);
                    }
                    offersApiExpertoResponseBody.setOperacionComercial(operacionComercial);

                    objExperto.setBodyOut(offersApiExpertoResponseBody);

                    if (scoring != null) {
                        header.setMsgType(Constants.RESPONSE);
                        objExperto.setHeaderOut(header);
                    }

                    JSONObject jsonScoring = new JSONObject(scoring);
                    logger.info("ReadOffers2 => scoring: " + jsonScoring);
                    JSONObject jsonExperto = new JSONObject(objExperto);
                    logger.info("ReadOffers2 => objExperto: " + jsonExperto);
                }

                if ((Constants.RESPONSE).equals(objExperto.getHeaderOut().getMsgType())) {
                    if (!(Constants.RESPONSE_ACTION).equals(objExperto.getBodyOut().getAccion())) {
                        if ((!objExperto.getBodyOut().getDeudaAtis().equals("0") || !objExperto.getBodyOut().getDeudaCms().equals("0"))) {
                            response.setResponseCode("4");
                            response.setResponseMessage("El cliente no tiene oferta porque tiene deuda.");

                            logger.info("ReadOffers2 => El cliente no tiene oferta porque tiene deuda.");
                            logger.info("Fin ReadOffers2 Service.");
                            return response;
                        }
                    }

                } else {

                    response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
                    String errorMessageExperto = mensajeOfertaErrorAvveExperto;// String.format(mensajeOfertaErrConnAvveExperto, "interconnect");
                    String errorDetailExperto = "";

                    if (objExperto.getBodyOut().getClientException() != null) {
                        if (objExperto.getBodyOut().getClientException().getAppDetail() != null) {
                            errorDetailExperto = objExperto.getBodyOut().getClientException().getAppDetail().getExceptionAppMessage();
                        } else {
                            errorDetailExperto = objExperto.getBodyOut().getClientException().getExceptionDetail();
                        }
                    }
                    if (objExperto.getBodyOut().getServerException() != null) {
                        if (objExperto.getBodyOut().getServerException().getAppDetail() != null) {
                            errorDetailExperto = objExperto.getBodyOut().getServerException().getAppDetail().getExceptionAppMessage();
                        } else {
                            errorDetailExperto = objExperto.getBodyOut().getServerException().getExceptionDetail();
                        }
                    }
                    response.setResponseMessage(String.format(DOUBLE_MESSAGE, errorMessageExperto, errorDetailExperto));

                    logger.info("ReadOffers2 => " + String.format(DOUBLE_MESSAGE, errorMessageExperto, errorDetailExperto));
                    logger.info("Fin ReadOffers2 Service.");

                    return response;
                }
            } else {
                byPassAvve = dao.readByPassAvve();
                logger.info("ReadOffers2 => byPassAvve: " + byPassAvve);
            }
            // Fin del experto

            OffersApiGisRequestBody apiGisRequestBody = new OffersApiGisRequestBody();
            if (request.getPhoneNumber() != null && !Constants.EMPTY.equals(request.getPhoneNumber())) {
                apiGisRequestBody.setNum_NUM_TLF(request.getPhoneNumber());
            } else {
                apiGisRequestBody.setNum_COD_CDX(request.getCoordinateX());
                apiGisRequestBody.setNum_COD_CDY(request.getCoordinateY());
            }

            ApiResponse<OffersApiGisResponseBody> objApiGisResponseBody = new ApiResponse<OffersApiGisResponseBody>();
            OffersGisResponse offersGisResponse = null;
            if (byPassAvve) {
                ApiResponseHeader headerOut = new ApiResponseHeader();
                headerOut.setMsgType(Constants.RESPONSE);
                objApiGisResponseBody.setHeaderOut(headerOut);
            } else {
                // Obtener parámetro de número de intentos
                Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("APICONNECT", "URI", "tdp.api.gis");
                byPassServiciosCaidos = Boolean.parseBoolean(objParameter.getStrValue());


                /**
                 *  Metodo que servirá para guardar data de futura contingencia en Gis by Valdemar
                 */
                daoGis.saveGisDireccion(request.getOrderId(),request.getDepartment(),request.getProvince(),request.getDistrict(),request.getCoordinateX(),request.getCoordinateY());


                objApiGisResponseBody = getGis(apiGisRequestBody, request.getOrderId(),request.getFlagFtth());
               /* boolean consultarContingencia = daoGis.consultarContingencia(request.getOrderId());

                if(consultarContingencia){
                    response.setFlag_offiline(1);
                }else{
                    response.setFlag_offiline(0);
                }*/

                LogVass.serviceResponseObject(logger, "ReadOffers2", "objApiGisResponseBody", objApiGisResponseBody);
                offersGisResponse = getFromGis(objApiGisResponseBody, ProductConstants.PARAMETRO_GIS_OPERACION_ALTA_PURA);
                if (objApiGisResponseBody.getHeaderOut() != null) {
                    if (objApiGisResponseBody.getHeaderOut().getMsgType() != null) {
                        if ((Constants.RESPONSE).equals(objApiGisResponseBody.getHeaderOut().getMsgType())) {
                            if (offersGisResponse.getVelocidadMax() != null) {
                                int sizeVelocidad = offersGisResponse.getVelocidadMax().length;
                                String[] velocidadMax = new String[sizeVelocidad];
                                for (int i = 0; i < sizeVelocidad; i++) {
                                    if (offersGisResponse.getVelocidadMax()[i] != null) {
                                        velocidadMax[i] = offersGisResponse.getVelocidadMax()[i].replace(ProductConstants.OFFERS_API_AVVE_REQUEST_MAX_SPEED_FIX, Constants.EMPTY);
                                    }
                                }
                                offersGisResponse.setVelocidadMax(velocidadMax);
                            }
                            if (offersGisResponse.getTipoSenal() != null) {
                                int sizeSignal = offersGisResponse.getTipoSenal().length;
                                String[] tipoSignal = new String[sizeSignal];
                                for (int i = 0; i < sizeSignal; i++) {
                                    if (offersGisResponse.getTipoSenal()[i] != null) {
                                        tipoSignal[i] = ProductConstants.OFFERS_TIPO_SENAL(offersGisResponse.getTipoSenal()[i]);
                                    }
                                }
                                offersGisResponse.setTipoSenal(tipoSignal);
                            }
                        }
                    }
                }
            }
            LogVass.serviceResponseObject(logger, "ReadOffers2", "offersGisResponse", offersGisResponse);

            if ((Constants.RESPONSE).equals(objApiGisResponseBody.getHeaderOut().getMsgType())) {// &&(Constants.RESPONSE).equals(objExperto.getHeaderOut().getMsgType())
                // modificando por gis... inicialmente vamos a usar el objeto objApiGisResponseBody directamente... pendiente el ajuste de manolo para q sea parametrizable

                WSSIGFFTT_FFTT_AVEResult result = null;
                String filtrosGis = "";
                String MSX_CBR_VOI_GES_IN = "";
                String MSX_CBR_VOI_GIS_IN = "";
                String MSX_IND_SNL_GIS_CD = "";
                String MSX_IND_GPO_GIS_CD = "";
                //String COD_IND_SEN_CMS = "";
                //String COD_CAB_CMS = "";
                String COD_FAC_TEC_CD = "";
                String COBRE_BLQ_TRM = "";
                String COBRE_BLQ_VTA = "";
                String DTH_TEC = "";
                String DTH_BLQ = "";
                String ZAR_TEC = "";
                String ZAR_BLQ = "";
                String COOR_X = "";
                String COOR_Y = "";

                boolean yesHFC = false, yesGPON = false;

                if (objApiGisResponseBody.getBodyOut() != null) {
                    result = objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult();

                    //para pruebas --> solo DTH
                    //result.setCOAXIAL_TEC("NO");

                    // Consulta de Cobertura XDSL - Cobertura XDSL: S=Si tiene Cobertura, N=No tiene cobertura
                    if (result.getXDSL_TEC() != null) {
                        if (result.getXDSL_TEC().toUpperCase().equalsIgnoreCase("SI")) {
                            MSX_CBR_VOI_GES_IN = "S";
                        } else {
                            MSX_CBR_VOI_GES_IN = "N";
                        }
                    } else {
                        MSX_CBR_VOI_GES_IN = "N";
                    }

                    // Indicador GPON - Cobertura GPON: N=Sin Cobertura, H=titne Cobertura HFC, G= Tiene cobertura GPON
                    if (result.getGPON_TEC() == null) {
                        MSX_IND_GPO_GIS_CD = "N";
                    }

                    // Consulta de Cobertura HFC - Cobertura HFC: S=Si tiene Cobertura, N=No tiene cobertura
                    if (result.getHFC_TEC() != null) {
                        if (result.getHFC_TEC().toUpperCase().equalsIgnoreCase("SI")) {
                            MSX_CBR_VOI_GIS_IN = "S";
                            MSX_IND_GPO_GIS_CD = "H";
                            yesHFC = true;
                        } else {
                            MSX_CBR_VOI_GIS_IN = "N";
                        }
                    } else {
                        MSX_CBR_VOI_GIS_IN = "N";
                    }

                    // Indicador GPON - Cobertura GPON: N=Sin Cobertura, H=titne Cobertura HFC, G= Tiene cobertura GPON
                    if (result.getGPON_TEC() != null) {
                        if (result.getGPON_TEC().toUpperCase().equalsIgnoreCase("SI")) {
                            //MSX_IND_GPO_GIS_CD = "G";
                            //MSX_IND_GPO_GIS_CD = yesHFC ? "A" : "G";

                            if(yesHFC){
                                MSX_IND_GPO_GIS_CD = "A";
                            }else{
                            MSX_IND_GPO_GIS_CD = "G";
                                MSX_CBR_VOI_GIS_IN = "S";
                            }

                            yesGPON = true;
                        }
                    }

                    // Tipo Señal CABLE - Tipo de Señal: 1= 100% Digital, 2 =Parcialmente Digital, 3 = Analógico
                    if (result.getCOAXIAL_SEN() != null) {
                        if (result.getCOAXIAL_SEN().toUpperCase().equalsIgnoreCase("DIGITAL")) {
                            MSX_IND_SNL_GIS_CD = "1";
                        } else if (result.getCOAXIAL_SEN().toUpperCase().equalsIgnoreCase("PARCIAL")) {
                            MSX_IND_SNL_GIS_CD = "2";
                        } else if (result.getCOAXIAL_SEN().toUpperCase().equalsIgnoreCase("ANALOGICO")) {
                            MSX_IND_SNL_GIS_CD = "3";
                        }
                    } else {
                        MSX_IND_SNL_GIS_CD = "0";
                    }

                    // Nodo, Troncal, Lex, Tap
                    COD_FAC_TEC_CD = result.getCOAXIAL_TRM();
                    String price = (objExperto != null ? String.valueOf(getTopPrice(objExperto)) : String.valueOf(9999999));
                    filtrosGis = gisFilterService.getFilter(result, price);

                    COBRE_BLQ_TRM = result.getCOBRE_BLQ_TRM();
                    COBRE_BLQ_VTA = result.getCOBRE_BLQ_VTA();
                    DTH_TEC = result.getDTH_TEC();
                    DTH_BLQ = result.getDTH_BLQ();
                    ZAR_TEC = result.getZAR_TEC();
                    ZAR_BLQ = result.getZAR_BLQ();
                    COOR_X = result.getCOOR_X();
                    COOR_Y = result.getCOOR_Y();
                }

                // modificando por campanias
                //request.setSellerSegment("NPP");
                //request.setSellerChannel("ONLINE");
                //request.setSellerEntity("PLAZA VEA");

                if (request.getSellerChannelEquivalentCampaign() == null) {
                    // Verificamos que se tengan cargados los valores equivalentes de campania: canal y entidad
                    // Para el caso de la app es posible que no se encuentren cargados estos valores por lo tanto consultamos de bd
                    TdpSalesAgent salesAgent = tdpSalesAgentRepository.findOne(request.getAtisSellerCode());
                    request.setSellerChannelEquivalentCampaign(salesAgent.getCanalEquivalenciaCampania());
                    request.setSellerEntityEquivalentCampaign(salesAgent.getEntidadEquivalenciaCampania());
                }

                FilterResponse filterResponse = campaniaFilterService.getFilter(request);
                String filtrosCampanias = filterResponse.getWhereQuery();
                String columnsCampanias = filterResponse.getColumnsSelected();


                List<TdpAutomatizadorCabecera> automatizadorCabecera = new ArrayList<TdpAutomatizadorCabecera>();
                automatizadorCabecera = tdpAutomatizadorCabeceraRepository.findAllByDepartamento(request.getDepartment().toUpperCase());
                listOffers = dao.readOffers2(request, offersGisResponse, filtrosGis, filtrosCampanias, columnsCampanias, objExperto, automatizadorCabecera);
                LogVass.serviceResponseArray(logger, "ReadOffers2", "listOffers", listOffers);

                /* Para migración el ProdTypeCode es diferente de null, caso contrario es Alta Pura */
                if (request.getProdTypeCode() != null && request.getProdTypeCode().length() > 0) {
                    // Migraciones

                    OffersApiCalculadoraArpuRequestBody offersApiCalculadoraArpuRequestBody = new OffersApiCalculadoraArpuRequestBody();
                    if (request.getLegacyCode().equals("ATIS")) {
                        offersApiCalculadoraArpuRequestBody.setTelefono(request.getPhoneNumber());
                    } else if (request.getLegacyCode().equals("CMS")) {
                        offersApiCalculadoraArpuRequestBody.setTelefono("-" + request.getProductSvcCode());
                    }

                    /* Consulta al ApiConnect de Calculadora - En caso de error se retorna un mensaje personalizado al usuario */
                    //sprint 25 offline Calculadora
                    ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalculadora;
                    try {
                        objCalculadora = getCalculadora(offersApiCalculadoraArpuRequestBody);
                    } catch (ClientException e) {
                        response.setResponseCode(messageArpu.get("errorArpu").getCode());
                        response.setResponseMessage("CalculadoraOffLine");
                        logger.error("ErrorClient ReadOffers2 Service.", e.getMessage());
                        logger.info("Fin ReadOffers2 Service");
                        objCalculadora=null;
                    }
                    /* Consulta al ApiConnect de Calculadora*/
                    //sprint 25 offline Calculadora
                    List<OffersApiCalculadoraArpuResponseBody> lstObjCalculadora = null;
                    if (objCalculadora == null || objCalculadora.getBodyOut() == null) {
                        lstObjCalculadora=contingenciaCalculadora(offersApiCalculadoraArpuRequestBody.getTelefono(), request);
                    } else {
                        if(objCalculadora != null || objCalculadora.getBodyOut() != null){
                        lstObjCalculadora = (List<OffersApiCalculadoraArpuResponseBody>) objCalculadora.getBodyOut();
                            for( OffersApiCalculadoraArpuResponseBody item : lstObjCalculadora){
                                if(item.getPaqueteOrigen() == null){
                                    if(item.getPaquetePsOrigen() == null){
                                        if(item.getProductoDestinoPs() == null){
                                            if(item.getProductoDestinoNombre() == null){
                                                lstObjCalculadora=contingenciaCalculadora(offersApiCalculadoraArpuRequestBody.getTelefono(), request);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        /*
                        request.setProductSvcCode(null);

                        String clienteNoEncontrado = messageArpu.get("sinOfertasArpu").getMessage();
                        response.setResponseCode(messageArpu.get("sinOfertasArpu").getCode());
                        response.setResponseMessage(clienteNoEncontrado);

                        logger.info("ReadOffers2 => " + clienteNoEncontrado);
                        logger.info("Fin ReadOffers2 Service.");
                        return response;*/
                    }
                    LogVass.serviceResponseArray(logger, "ReadOffers2", "lstObjCalculadora", lstObjCalculadora);

                    List<OffersResponseData2> lstOfertaFinal = new ArrayList<>();
                    OffersResponseData2 ofertaFinal = null;
                    Map<String, List<OffersResponseData2>> mapOffer = new HashMap<String, List<OffersResponseData2>>();
                    List<String> lstSvas = new ArrayList<String>();
                    Map<String, SvaV2ResponseDataSva> mapObjSvas = new HashMap<String, SvaV2ResponseDataSva>();

                    // --- Sprint 8: Migraciones --- //
                    List<OffersResponseData2> listOffers_tmp = new ArrayList<>(); // Lista temporal para almacenar ofertas
                    // Recorremos la lista de ofertas y añadimos sólo las ofertas que tienen el mismo código de producto y código promocional traido por Calculadora
                    for (OffersResponseData2 offers : listOffers) {
                        for (OffersApiCalculadoraArpuResponseBody objCalculadoraTmp : lstObjCalculadora) {
                            String productCategoryCode = offers.getProductCategoryCode();
                            String productCode = offers.getProductCode();
                            String productInternetTech = offers.getInternetTech().toUpperCase();
                            String destinoPs = objCalculadoraTmp.getProductoDestinoPs();
                            String codigoOferta = objCalculadoraTmp.getCodigoOferta();
                            String internetTech = objCalculadoraTmp.getMontoFinanciando() == null ? "" : objCalculadoraTmp.getMontoFinanciando().toUpperCase();

                            if (productCode.equalsIgnoreCase(destinoPs)
                                    && productCategoryCode.equalsIgnoreCase(codigoOferta)
                                    && (productInternetTech.equalsIgnoreCase(internetTech) || internetTech.equalsIgnoreCase(""))) {
                                listOffers_tmp.add(offers);
                                break;
                            }
                        }
                    }
                    listOffers = listOffers_tmp; // Reemplazar por lista temporal
                    LogVass.serviceResponseArray(logger, "ReadOffers2", "listOffers", listOffers);
                    listOffers_tmp = new ArrayList<>(); // Limpiamos lista

                    // Recorremos la lista de ofertas y añadimos sólo las primeras ofertas que traemos, las cuáles están priorizadas según tecnologia de internet
                    String internetTech = "";
                    for (OffersResponseData2 offers : listOffers) {
                        String codProductoInternetTech = "" + offers.getProductCode();
                        if (!codProductoInternetTech.equalsIgnoreCase(internetTech)) {
                            internetTech = codProductoInternetTech;
                            listOffers_tmp.add(offers);
                        }
                    }
                    listOffers = listOffers_tmp; // Reemplazar por lista temporal
                    LogVass.serviceResponseArray(logger, "ReadOffers2", "listOffers", listOffers);
                    // --- Sprint 8: Migraciones  --- //

                    for (OffersResponseData2 offers : listOffers) {
                        List<OffersResponseData2> tmp = mapOffer.get(offers.getProductCode());
                        if (tmp == null) {
                            tmp = new ArrayList<>();
                        }
                        tmp.add(offers);
                        mapOffer.put(offers.getProductCode(), tmp);
                    }

                    //obteniendo la lista de productCode
                    for (OffersApiCalculadoraArpuResponseBody objCalculadoraTmpSva : lstObjCalculadora) {
                        if (objCalculadoraTmpSva.getSVAs() != null && !("").equals(objCalculadoraTmpSva.getSVAs())) {
                            lstSvas.add(objCalculadoraTmpSva.getSVAs());
                        }
                    }

                    Double maxDecos = 6.0;
                    try {
                        Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "POLICY_PARAMETER", "max_amount_decoders_by_floor");
                        maxDecos = Double.parseDouble(objParameter.getStrValue());
                    } catch (Exception e) {
                        logger.entry("Error ReadOffers2 Service. - Parameters.", e);
                    }

                    SvaV2Request requestSva = new SvaV2Request();
                    requestSva.setMigracionSvasBloqueTV("");
                    requestSva.setMigracionSvasLinea("");
                    requestSva.setMigracionSvasInternet("");
                    generarConsultaDefaultSvasMigracion(requestSva);

                    requestSva.setProductCode("666");
                    //lista de SVAs de la bd
                    List<SvaV2ResponseDataSva> listaSvas = dao.getSvaByProductCode(lstSvas, maxDecos, requestSva, "NORMAL");
                    JSONArray jsonSva = new JSONArray(listaSvas);
                    logger.info("ReadOffers2 => listaSvas (" + jsonSva.length() + "): " + jsonSva.toString());

                    for (SvaV2ResponseDataSva svaV2ResponseDataSva : listaSvas) {
                        if (mapObjSvas.get(svaV2ResponseDataSva.getProductCode())==null){
                            mapObjSvas.put(svaV2ResponseDataSva.getProductCode(), svaV2ResponseDataSva);
                        }

                    }

                    for (OffersApiCalculadoraArpuResponseBody objCalculadoraTmp : lstObjCalculadora) {
                        //logger.trace("Analizando oferta de calculadora {} - {}", objCalculadoraTmp.getProductoDestinoPs(), objCalculadoraTmp.getProductoDestinoNombre());
                        String offerKey = objCalculadoraTmp.getProductoDestinoPs();
                        logger.info("ReadOffers2 => AVVE mapOffer.get (" + offerKey + ")");
                        List<OffersResponseData2> dbOffers = mapOffer.get(offerKey);
                        JSONArray jsonDBOffers = new JSONArray(dbOffers);
                        logger.info("ReadOffers2 => AVVE dbOffers (" + jsonDBOffers.length() + "): " + jsonDBOffers.toString());
                        if (dbOffers != null && !dbOffers.isEmpty()) {
                            for (OffersResponseData2 dbOferta : dbOffers) {
                                //agregando sprint 11 cambios
                                OffersResponseData2 oferta = new OffersResponseData2(dbOferta);
                                if (oferta.getProductCategoryCode() == null || !oferta.getProductCategoryCode().equals(objCalculadoraTmp.getCodigoOferta())) {
                                    logger.info("ReadOffers2 => Oferta rechazada por que el codigo de oferta no es correcto");
                                    continue;
                                }
                                try {
                                    oferta.setRentatotal(Double.parseDouble(objCalculadoraTmp.getRentaTotal()));

                                    Double decos = 0.0;
                                    if (objCalculadoraTmp.getCantidadDecos() != null) {
                                        decos = Double.parseDouble(objCalculadoraTmp.getCantidadDecos());
                                    }
                                    oferta.setCantidaddecos(maxDecos - decos);

                                    oferta.setInternet(Double.parseDouble(objCalculadoraTmp.getInternet()));
                                } catch (Exception ex) {
                                    oferta.setRentatotal(-1);
                                    oferta.setCantidaddecos(-1);
                                    oferta.setInternet(-1);
                                }
                                Double montoFinanciado = 0.0;
                                //if (objCalculadoraTmp.getMontoFinanciando() != null) {
                                //    montoFinanciado = Double.parseDouble(objCalculadoraTmp.getMontoFinanciando());
                                oferta.setFinancingCost(montoFinanciado);
                                //}
                                oferta.setProductName(objCalculadoraTmp.getProductoDestinoNombre());


                                SvaV2ResponseDataSva sva = mapObjSvas.get(objCalculadoraTmp.getSVAs());



                                if (objCalculadoraTmp.getSVAs() == null) {
                                    lstOfertaFinal.add(oferta);
                                } else if (sva != null) {
                                    if (objCalculadoraTmp.getCuotasFinanciado() != null) {
                                        sva.setFinancedQuotas(Integer.parseInt(objCalculadoraTmp.getCuotasFinanciado()));
                                    }
                                    if (objCalculadoraTmp.getRentaTotal() != null) {
                                        sva.setArpu(Double.parseDouble(objCalculadoraTmp.getRentaTotal()));
                                    }
                                    if (montoFinanciado > 0) {
                                        sva.setHasEquipment("1");
                                    } else {
                                        sva.setHasEquipment("0");
                                    }
                                    //oferta.getAltaPuraSvasBloqueTV();
                                    //oferta.getAltaPuraSvasInternet();
                                    //oferta.getAltaPuraSvasLinea();
                                    //String getproductCode = sva.getProductCode();

                                    String svaTv = oferta.getAltaPuraSvasBloqueTV();
                                    String svaInternet = oferta.getAltaPuraSvasInternet();
                                    String svaLinea = oferta.getAltaPuraSvasLinea();

                                    if (!"-".equals(svaLinea)) {
                                        String[] arraySvaLinea = svaLinea.split(",");
                                        for (String svaLineaCode : arraySvaLinea) {
                                            if (sva.getProductCode().equals(svaLineaCode)) {
                                                sva.setCost(0);
                                                break;
                                            }
                                        }
                                    }

                                    if (!"-".equals(svaInternet)) {
                                        String[] arraySvaInternet = svaInternet.split(",");
                                        for (String svaInternetCode : arraySvaInternet) {
                                            if (sva.getProductCode().equals(svaInternetCode)) {
                                                sva.setCost(0);
                                                break;
                                            }
                                        }
                                    }

                                    if (!"-".equals(svaTv)) {
                                        String[] arraySvaTv = svaTv.split(",");
                                        for (String svaTvCode : arraySvaTv) {
                                            if (sva.getProductCode().equals(svaTvCode)) {
                                                sva.setCost(0);
                                                break;
                                            }
                                        }
                                    }
                                    //System.out.println(svaLinea);
                                    //System.out.println(svaTv);
                                    //System.out.println(svaInternet);
                                    //System.out.println(getproductCode);

                                    if (sva.getProductCode().equals(svaLinea) || sva.getProductCode().equals(svaTv) || sva.getProductCode().equals(svaInternet)) {
                                        sva.setCost(0);
                                    }

                                    oferta.setSva(sva);

                                    lstOfertaFinal.add(oferta);
                                } else {
                                    logger.info("ReadOffers2 => No se encuentra el SVA de la Oferta");
                                }
                            }
                        } else {
                            logger.info("ReadOffers2 => Oferta no tiene facilidades tecnicas");
                        }
                    }

                    response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_OK);
                    response.setResponseData(lstOfertaFinal);
                } else {
                    // Alta pura

                    //obtenemos solo los productos con mas afinidad de entidad y ubicacion
                    //sprint 11 - ya no es necesario filtrar x java... el filtrado se realiza en el mismo query
                    //listOffers = getProductosPorAfinidadEntidadYUbicacion(listOffers);

                    String internetTipoProductoAgregado = "";
                    String campaniaAgregada = "";
                    int internetVelocidadAgregado = -1;

                    //Para alta Pura, la señal depende de las facilidades - Logica automatizador
                    if(yesHFC || yesGPON) MSX_IND_SNL_GIS_CD = "1";
                    boolean consultarContingencia = daoGis.consultarContingencia(request.getOrderId());


                    // Mantenemos en el response solo los productos de mejor tecnologia
                    // Pendiente verificar si podemos reutilizar el metodo de dejar los productos de mejor tecnologia usado en migras
                    for (OffersResponseData2 rd : listOffers) {

                        //para pruebas automatizador
                        //if(!"digital".equalsIgnoreCase(rd.getTvSignal()) && !"no aplica".equalsIgnoreCase(rd.getTvSignal())) continue;

                        for (OffersApiExpertoResponseBodyOperacion operation : objExperto.getBodyOut().getOperacionComercial()) {
                            if (operation.getContadorDetalle() > 0) {
                                for (OffersApiExpertoResponseBodyOperacionDetalle detail : operation.getDetalle()) {
                                    if (rd.getPrice() <= Integer.parseInt(Converter.fixPrice(operation.getRenta()))
                                            && rd.getCommercialOperation().equalsIgnoreCase(operation.getCodOpe())
                                            && rd.getProductCategoryCode().equals(detail.getCodProd())
                                            && rd.getCashPrice() == detail.getPagoAdelantado()) {

                                        rd.setMsx_cbr_voi_ges_in(MSX_CBR_VOI_GES_IN);
                                        rd.setMsx_cbr_voi_gis_in(MSX_CBR_VOI_GIS_IN);
                                        rd.setMsx_ind_snl_gis_cd(MSX_IND_SNL_GIS_CD);
                                        rd.setMsx_ind_gpo_gis_cd(MSX_IND_GPO_GIS_CD);
                                        rd.setCod_fac_tec_cd(COD_FAC_TEC_CD);
                                        rd.setCobre_blq_vta(COBRE_BLQ_VTA);
                                        rd.setCobre_blq_trm(COBRE_BLQ_TRM);
                                        if(consultarContingencia){
                                            rd.setFlag_offline(true);
                                        }else{
                                            rd.setFlag_offline(false);
                                        }

//                                        rd.setDth_tec(DTH_TEC);
//                                        rd.setDth_blq(DTH_BLQ);
//                                        rd.setZar_tec(ZAR_TEC);
//                                        rd.setZar_blq(ZAR_BLQ);
//                                        rd.setCoor_x(COOR_X);
//                                        rd.setCoor_y(COOR_Y);

                                        //4: DUO BA
                                        //3: TRIO
                                        //2: MONO BA
                                        //5: DUO TV
                                        if ("4".equals(rd.getProductTypeCode()) || "3".equals(rd.getProductTypeCode()) || "2".equals(rd.getProductTypeCode()) || "5".equals(rd.getProductTypeCode())) {
                                            if (!(internetTipoProductoAgregado.equals(rd.getProductTypeCode()) && rd.getInternetSpeed() == internetVelocidadAgregado && campaniaAgregada.equals(rd.getCampaign()))) {
                                                //if (!internetTipoProductoAgregado.equals(rd.getProductTypeCode()) && rd.getInternetSpeed() != internetVelocidadAgregado) {
                                                campaniaAgregada = rd.getCampaign();
                                                internetTipoProductoAgregado = rd.getProductTypeCode();
                                                internetVelocidadAgregado = rd.getInternetSpeed();
                                                responseData.add(rd);
                                            }
                                        } else {
                                            // Para los otros tipos de producto simplemente adicionamos a la lista final
                                            responseData.add(rd);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    JSONArray jsonData = new JSONArray(responseData);
                    logger.info("ReadOffers2 => responseData (" + responseData.size() + "): " + jsonData.toString());
                    response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_OK);
                    response.setResponseData(responseData);
                }

            } else {//if (!(Constants.RESPONSE).equals(objAvve.getHeaderOut().getMsgType())) {
                response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
                String errorDetailAvve = "";

                if (objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().getClientException() != null) {
                    if (objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().getClientException().getAppDetail() != null) {
                        errorDetailAvve = objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().getClientException().getAppDetail().getExceptionAppMessage();
                    } else {
                        errorDetailAvve = objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().getClientException().getExceptionDetail();
                    }
                }
                if (objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().getServerException() != null) {
                    if (objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().getServerException().getAppDetail() != null) {
                        errorDetailAvve = objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().getServerException().getAppDetail().getExceptionAppMessage();
                    } else {
                        errorDetailAvve = objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().getServerException().getExceptionDetail();
                    }
                }
                response.setResponseMessage(String.format(DOUBLE_MESSAGE, mensajeOfertaErrorAvve, errorDetailAvve));
            }
        } catch (ClientException e) {
            if (e.getMessage() != null && e.getMessage().equals("El proveedor no responde.")) {
                response.setResponseCode(messageList.get("errorArpu").getCode());
                response.setResponseMessage(messageList.get("errorArpu").getMessage());
                logger.error("ErrorClient ReadOffers2 Service.", e.getMessage());
            } else {
                response.setResponseCode(messageList.get("readOffersFail").getCode());
                response.setResponseMessage(messageList.get("readOffersFail").getMessage());
                logger.error("ErrorClient ReadOffers2 Service.", e);
            }
            logger.info("Fin ReadOffers2 Service");
            return response;
        } catch (Exception e) {
            logger.error("Error Offering Service", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());

            logger.info("Fin ReadOffers2 Service");
            return response;
        }
        logger.info("Fin ReadOffers2 Service");

        if(response.getResponseMessage()!=null && response.getResponseMessage().equals("CalculadoraOffLine")){
            response.setResponseData(cargaOffLineCalculadora(response.getResponseData()));
        }

        return response;
    }

    private List<OffersResponseData2> cargaOffLineCalculadora(List<OffersResponseData2> responseData) {
        List<OffersResponseData2> listaLimpia = new ArrayList<>();

        //Forma número 1 (Uso de Maps).
        Map<Integer, OffersResponseData2> mapListOffering = new HashMap<Integer, OffersResponseData2>(responseData.size());
        for(OffersResponseData2 p : responseData) {
            mapListOffering.put(p.getId(), p);
        }
        //Agrego cada elemento del map a una nueva lista y muestro cada elemento.
        System.out.println("Lista sin repetidos:");
        for(Map.Entry<Integer, OffersResponseData2> p : mapListOffering.entrySet()) {
            listaLimpia.add(p.getValue());
            System.out.println(p.getValue());
        }

        return listaLimpia;
    }

    public Response<GisResponse> readGis(OffersApiGisRequestBody request) {
        logger.info("Inicio readGis Service");
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("PRODUCTO");
        LogVass.serviceResponseHashMap(logger, "readGis", "messageList", messageList);

        Response<GisResponse> response = new Response<>();
        try {

            ApiResponse<OffersApiGisResponseBody> objApiGisResponseBody = new ApiResponse<OffersApiGisResponseBody>();
            try {
                OffersApiGisRequestBody apiGisRequestBody = new OffersApiGisRequestBody();
                apiGisRequestBody.setNum_COD_CDX(request.getNum_COD_CDX());
                apiGisRequestBody.setNum_COD_CDY(request.getNum_COD_CDY());

                objApiGisResponseBody = getGis(apiGisRequestBody, "",false);
            } catch (ClientException e) {
                response.setResponseCode(messageList.get("readOffersFail").getCode());
                response.setResponseMessage(messageList.get("readOffersFail").getMessage());
                e.printStackTrace();
            }

            WSSIGFFTT_FFTT_AVEResult result = null;
            String MSX_CBR_VOI_GES_IN = "";
            String MSX_CBR_VOI_GIS_IN = "";
            String MSX_IND_SNL_GIS_CD = "";
            String MSX_IND_GPO_GIS_CD = "";
            //String COD_IND_SEN_CMS = "";
            //String COD_CAB_CMS = "";
            String COD_FAC_TEC_CD = "";
            String COBRE_BLQ_TRM = "";
            String COBRE_BLQ_VTA = "";
            String DTH_TEC = "";
            String DTH_BLQ = "";
            String ZAR_TEC = "";
            String ZAR_BLQ = "";
            String COOR_X = "";
            String COOR_Y = "";

            if (objApiGisResponseBody.getBodyOut() != null) {
                result = objApiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult();

                // Consulta de Cobertura XDSL - Cobertura XDSL: S=Si tiene Cobertura, N=No tiene cobertura
                if (result.getXDSL_TEC() != null) {
                    if (result.getXDSL_TEC().toUpperCase().equalsIgnoreCase("SI")) {
                        MSX_CBR_VOI_GES_IN = "S";
                    } else {
                        MSX_CBR_VOI_GES_IN = "N";
                    }
                } else {
                    MSX_CBR_VOI_GES_IN = "N";
                }

                // Indicador GPON - Cobertura GPON: N=Sin Cobertura, H=titne Cobertura HFC, G= Tiene cobertura GPON
                if (result.getGPON_TEC() == null) {
                    MSX_IND_GPO_GIS_CD = "N";
                }

                // Consulta de Cobertura HFC - Cobertura HFC: S=Si tiene Cobertura, N=No tiene cobertura
                if (result.getHFC_TEC() != null) {
                    if (result.getHFC_TEC().toUpperCase().equalsIgnoreCase("SI")) {
                        MSX_CBR_VOI_GIS_IN = "S";
                        MSX_IND_GPO_GIS_CD = "H";
                    } else {
                        MSX_CBR_VOI_GIS_IN = "N";
                    }
                } else {
                    MSX_CBR_VOI_GIS_IN = "N";
                }

                // Indicador GPON - Cobertura GPON: N=Sin Cobertura, H=titne Cobertura HFC, G= Tiene cobertura GPON
                if (result.getGPON_TEC() != null) {
                    if (result.getGPON_TEC().toUpperCase().equalsIgnoreCase("SI")) {
                        MSX_IND_GPO_GIS_CD = "G";
                    }
                }

                // Tipo Señal CABLE - Tipo de Señal: 1= 100% Digital, 2 =Parcialmente Digital, 3 = Analógico
                if (result.getCOAXIAL_SEN() != null) {
                    if (result.getCOAXIAL_SEN().toUpperCase().equalsIgnoreCase("DIGITAL")) {
                        MSX_IND_SNL_GIS_CD = "1";
                    } else if (result.getCOAXIAL_SEN().toUpperCase().equalsIgnoreCase("PARCIAL")) {
                        MSX_IND_SNL_GIS_CD = "2";
                    } else if (result.getCOAXIAL_SEN().toUpperCase().equalsIgnoreCase("ANALOGICO")) {
                        MSX_IND_SNL_GIS_CD = "3";
                    }
                } else {
                    MSX_IND_SNL_GIS_CD = "0";
                }

                // Nodo, Troncal, Lex, Tap
                COD_FAC_TEC_CD = result.getCOAXIAL_TRM();
                //String price = (objExperto != null ? String.valueOf(getTopPrice(objExperto)) : String.valueOf(9999999));
                // filtrosGis = gisFilterService.getFilter(result, price);

                COBRE_BLQ_TRM = result.getCOBRE_BLQ_TRM();
                COBRE_BLQ_VTA = result.getCOBRE_BLQ_VTA();
                DTH_TEC = result.getDTH_TEC();
                DTH_BLQ = result.getDTH_BLQ();
                ZAR_TEC = result.getZAR_TEC();
                ZAR_BLQ = result.getZAR_BLQ();
                COOR_X = result.getCOOR_X();
                COOR_Y = result.getCOOR_Y();

                result.getXDSL_VEL_TRM();

                GisResponse gisResponse = new GisResponse();

                gisResponse.setMsx_cbr_voi_ges_in(MSX_CBR_VOI_GES_IN);
                gisResponse.setMsx_cbr_voi_gis_in(MSX_CBR_VOI_GIS_IN);
                gisResponse.setMsx_ind_snl_gis_cd(MSX_IND_SNL_GIS_CD);
                gisResponse.setMsx_ind_gpo_gis_cd(MSX_IND_GPO_GIS_CD);
                gisResponse.setCod_fac_tec_cd(COD_FAC_TEC_CD);
                gisResponse.setCobre_blq_vta(COBRE_BLQ_VTA);
                gisResponse.setCobre_blq_trm(COBRE_BLQ_TRM);

                response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_OK);
                response.setResponseData(gisResponse);
            }
        } catch (Exception e) {
            logger.error("Error readGis Service", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());

            logger.info("Fin readGis Service");
            return response;
        }
        logger.info("Fin readGis Service");
        return response;
    }

    //Sprint 10 - campanias
    private List<OffersResponseData2> getProductosPorAfinidadEntidadYUbicacion(List<OffersResponseData2> offers) {
        Integer entidadAfinidad = 0;
        Integer ubicacionAfinidad = 0;
        if (offers != null && !offers.isEmpty()) {
            OffersResponseData2 initialOffer = offers.get(0);
            entidadAfinidad = initialOffer.getAfinidadEntidad();
            ubicacionAfinidad = initialOffer.getAfinidadUbicacion();
        }
        List<OffersResponseData2> finalOffers = new ArrayList<OffersResponseData2>();
        for (OffersResponseData2 offer : offers) {
            if (offer.getAfinidadEntidad().equals(entidadAfinidad) && offer.getAfinidadUbicacion().equals(ubicacionAfinidad)) {
                finalOffers.add(offer);
            } else {
                break;
            }
        }
        return finalOffers;
    }


    public double getTopPrice(ApiResponse<OffersApiExpertoResponseBody> objExperto) {
        List<OffersApiExpertoResponseBodyOperacion> lista = objExperto.getBodyOut().getOperacionComercial();
        double topPrice = 0.0;

        for (OffersApiExpertoResponseBodyOperacion d : lista) {
            int renta = Integer.parseInt(Converter.fixPrice(d.getRenta()));

            if (renta > topPrice) {
                topPrice = renta;
            }
        }

        return topPrice;
    }

    private OffersRequest convertOffering2ToOffering(OffersRequest2 offersRequest2) {
        OffersRequest offersRequest = new OffersRequest();
        offersRequest.setAtisSellerCode(offersRequest2.getAtisSellerCode());
        offersRequest.setCoordinateX(offersRequest2.getCoordinateX());
        offersRequest.setCoordinateY(offersRequest2.getCoordinateY());
        offersRequest.setDepartment(offersRequest2.getDepartment());
        offersRequest.setDistrict(offersRequest2.getDistrict());
        offersRequest.setDocumentNumber(offersRequest.getDocumentNumber());
        offersRequest.setDocumentType(offersRequest2.getDocumentType());
        offersRequest.setLegacyCode(offersRequest2.getLegacyCode());
        offersRequest.setPhoneNumber(offersRequest2.getPhoneNumber());
        offersRequest.setProdTypeCode(offersRequest2.getProdTypeCode());
        offersRequest.setProvince(offersRequest2.getProvince());
        offersRequest.setUbigeoCode(offersRequest2.getUbigeoCode());
        offersRequest.setZonal(offersRequest2.getZonal());

        return offersRequest;

    }

    private ApiResponse<OffersApiAvveResponseBody> getAvve(OffersApiAvveRequestBody apiAvveRequestBody)
            throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getFfttaaveConsultaFfttUri())
                .setApiId(apiHeaderConfig.getFfttaaveConsultarFfttApiId())
                .setApiSecret(apiHeaderConfig.getFfttaaveConsultarFfttApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_AVVE)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_AVVE).build();
        AvveClient avveClient = new AvveClient(config);
        ClientResult<ApiResponse<OffersApiAvveResponseBody>> result = avveClient.post(apiAvveRequestBody);
        ApiResponse<OffersApiAvveResponseBody> objAvve = result.getResult();
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }

        return objAvve;
    }

    private ApiResponse<OffersApiGisResponseBody>
    getGis(OffersApiGisRequestBody request, String orderId,Boolean flagFtth) throws ClientException {

        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(uriConsultaGis)
                .setApiId(apiidConsultaGis)
                .setApiSecret(apisecretConsultaGis)
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_GIS)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_GIS).build();
        GisClient gisClient = new GisClient(config);
        ClientResult<ApiResponse<OffersApiGisResponseBody>> result = gisClient.post(request);

         ApiResponse<OffersApiGisResponseBody> objGis;

        ApiResponse<OffersApiGisResponseBody> objG = new ApiResponse<>();

        /**
         *  Metodo que servirá para guardar data de futura contingencia en Gis by Valdemar
         */
        if(result.isSuccess()){
            objGis = result.getResult();
        }else {
            objGis = new ApiResponse<>();
        }

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
        result.getEvent().setOrderId(orderId);
        serviceCallEventsService.registerEvent(result.getEvent());
        OffersApiGisResponseBody gisResponseBody = new OffersApiGisResponseBody();

        /**
         *  Metodo que servirá para guardar data de futura contingencia en Gis by Valdemar
         */
        if (!result.isSuccess()) {
            /*BYPASS GIS */
          if (byPassServiciosCaidos) {
            result.setE(null);

            ApiResponseHeader headerOut = new ApiResponseHeader();

            headerOut.setOriginator("PE:TDP:WebPorta:WebPorta");
            headerOut.setDestination("PE:TDP:APPVENTASFIJAX:APPVENTASFIJA");
            headerOut.setExecId("550e8400-e29b-41d4-a716-446655440003");
            headerOut.setTimestamp("2019-01-23T00:5153.201-05:00");
            headerOut.setMsgType("RESPONSE");
            objGis.setHeaderOut(headerOut);

            GisUbigeo gisUbigeo = daoGis.readGisforUbigeoFilter(orderId);

            //filtrar por xy más cercanos en los últimos 7 días.
            //  List<GisUbigeo> gisUbigeos = daoGis.listaUbigeos();
            WSSIGFFTT_FFTT_AVEResult wssigfftt_fftt_aveResult = daoGis.readGisforUbigeo(gisUbigeo);


            if(objGis.getBodyOut() == null){

                WSSIGFFTT_FFTT_AVEResult wssigfftt_fftt_aveResult2 = daoGis.readGisTecnologíaDefault();

                wssigfftt_fftt_aveResult = wssigfftt_fftt_aveResult2;


            }

              wssigfftt_fftt_aveResult.setFlag_offiline(true);
              daoGis.saveContingencia(orderId,1);

            gisResponseBody.setwSSIGFFTT_FFTT_AVEResult(wssigfftt_fftt_aveResult);
            //result.setResult(objG);
            System.out.println(objGis);

             objGis.setBodyOut(gisResponseBody);
             if(objGis.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().getCOAXIAL_TRM() == null){
                 objGis = null;
             }
         } else {
                throw result.getE();
            }
        }else{
               gisResponseBody = objGis.getBodyOut();

        }

//        objGis.getBodyOut().getwSSIGFFTT_FFTT_AVEResult().setCOBRE_BLQ_VTA("SI");

        WSSIGFFTT_FFTT_AVEResult wssigfftt_fftt_aveResult = gisResponseBody.getwSSIGFFTT_FFTT_AVEResult();

        if (wssigfftt_fftt_aveResult.getXDSL_TEC() != null && wssigfftt_fftt_aveResult.getXDSL_TEC().equalsIgnoreCase("SI")) {
            if (wssigfftt_fftt_aveResult.getCOBRE_BLQ_VTA() == null || wssigfftt_fftt_aveResult.getCOBRE_BLQ_VTA().equalsIgnoreCase("SI"))
                wssigfftt_fftt_aveResult.setXDSL_TEC("NO");
        }

        if(flagFtth!=null && flagFtth){
            wssigfftt_fftt_aveResult.setGPON_TEC("SI");
            wssigfftt_fftt_aveResult.setGPON_VEL("500MB");
        }



        /**
         *  Metodo que servirá para guardar data de futura contingencia en Gis by Valdemar
         */
        //Sí recibimos alguna respuesta del servicio lo guardamos
        if (result.isSuccess()) {
            daoGis.saveGisStateTecnology(orderId,gisResponseBody.getwSSIGFFTT_FFTT_AVEResult());
            wssigfftt_fftt_aveResult.setFlag_offiline(false);

        }




        return objGis;

    }

    private WSSIGFFTT_FFTT_AVEResult getNullsToEmptyFromGIS(WSSIGFFTT_FFTT_AVEResult objectGisResult) {

        WSSIGFFTT_FFTT_AVEResult respuesta = new WSSIGFFTT_FFTT_AVEResult();
        // 1 COBRE_TEC
        if (objectGisResult.getCOBRE_TEC() != null) {
            respuesta.setCOBRE_TEC(objectGisResult.getCOBRE_TEC());
        } else {
            respuesta.setCOBRE_TEC("");
        }
        // 2 COBRE_SAT
        if (objectGisResult.getCOBRE_SAT() != null) {
            respuesta.setCOBRE_SAT(objectGisResult.getCOBRE_SAT());
        } else {
            respuesta.setCOBRE_SAT("");
        }
        // 3 COBRE_TRM
        if (objectGisResult.getCOBRE_TRM() != null) {
            respuesta.setCOBRE_TRM(objectGisResult.getCOBRE_TRM());
        } else {
            respuesta.setCOBRE_TRM("");
        }
        // 4 COBRE_TRM
        if (objectGisResult.getCOBRE_BLQ_VTA() != null) {
            respuesta.setCOBRE_BLQ_VTA(objectGisResult.getCOBRE_BLQ_VTA());
        } else {
            respuesta.setCOBRE_BLQ_VTA("");
        }
        // 3 COBRE_TRM
        if (objectGisResult.getCOBRE_BLQ_TRM() != null) {
            respuesta.setCOBRE_BLQ_TRM(objectGisResult.getCOBRE_BLQ_TRM());
        } else {
            respuesta.setCOBRE_BLQ_TRM("");
        }
        // 5 COAXIAL_TEC
        if (objectGisResult.getCOAXIAL_TEC() != null) {
            respuesta.setCOAXIAL_TEC(objectGisResult.getCOAXIAL_TEC());
        } else {
            respuesta.setCOAXIAL_TEC("");
        }
        // 6 COAXIAL_SEN
        if (objectGisResult.getCOAXIAL_SEN() != null) {
            respuesta.setCOAXIAL_SEN(objectGisResult.getCOAXIAL_SEN());
        } else {
            respuesta.setCOAXIAL_SEN("");
        }
        // 7 COAXIAL_TRM
        if (objectGisResult.getCOAXIAL_TRM() != null) {
            respuesta.setCOAXIAL_TRM(objectGisResult.getCOAXIAL_TRM());
        } else {
            respuesta.setCOAXIAL_TRM("");
        }
        // 8 HFC_TEC
        if (objectGisResult.getHFC_TEC() != null) {
            respuesta.setHFC_TEC(objectGisResult.getHFC_TEC());
        } else {
            respuesta.setHFC_TEC("");
        }
        // 9 HFC_VEL
        if (objectGisResult.getHFC_VEL() != null) {
            respuesta.setHFC_VEL(objectGisResult.getHFC_VEL());
        } else {
            respuesta.setHFC_VEL("");
        }
        // 10 GPON_TEC
        if (objectGisResult.getGPON_TEC() != null) {
            respuesta.setGPON_TEC(objectGisResult.getGPON_TEC());
        } else {
            respuesta.setGPON_TEC("");
        }
        // 11 GPON_VEL
        if (objectGisResult.getGPON_VEL() != null) {
            respuesta.setGPON_VEL(objectGisResult.getGPON_VEL());
        } else {
            respuesta.setGPON_VEL("");
        }
        // 12 GPON_TRM
        if (objectGisResult.getGPON_TRM() != null) {
            respuesta.setGPON_TRM(objectGisResult.getGPON_TRM());
        } else {
            respuesta.setGPON_TRM("");
        }
        // 13 XDSL_TEC
        if (objectGisResult.getXDSL_TEC() != null) {
            respuesta.setXDSL_TEC(objectGisResult.getXDSL_TEC());
        } else {
            respuesta.setXDSL_TEC("");
        }

        // 14 XDSL_SAT
        if (objectGisResult.getXDSL_SAT() != null) {
            respuesta.setXDSL_SAT(objectGisResult.getXDSL_SAT());
        } else {
            respuesta.setXDSL_SAT("");
        }

        // 15 XDSL_VEL_TLF
        if (objectGisResult.getXDSL_VEL_TLF() != null) {
            respuesta.setXDSL_VEL_TLF(objectGisResult.getXDSL_VEL_TLF());
        } else {
            respuesta.setXDSL_VEL_TLF("");
        }

        // 16 XDSL_VEL_TRM
        if (objectGisResult.getXDSL_VEL_TRM() != null) {
            respuesta.setXDSL_VEL_TRM(objectGisResult.getXDSL_VEL_TRM());
        } else {
            respuesta.setXDSL_VEL_TRM("");
        }

        // 17 EXC_CABLE_HFC
        if (objectGisResult.getEXC_CABLE_HFC() != null) {
            respuesta.setEXC_CABLE_HFC(objectGisResult.getEXC_CABLE_HFC());
        } else {
            respuesta.setEXC_CABLE_HFC("");
        }

        // 18 EXC_CABLE_ACO
        if (objectGisResult.getEXC_CABLE_ACO() != null) {
            respuesta.setEXC_CABLE_ACO(objectGisResult.getEXC_CABLE_ACO());
        } else {
            respuesta.setEXC_CABLE_ACO("");
        }

        // 19 EDF_NOCABLE_EXISTE
        if (objectGisResult.getEDF_NOCABLE_EXISTE() != null) {
            respuesta.setEDF_NOCABLE_EXISTE(objectGisResult.getEDF_NOCABLE_EXISTE());
        } else {
            respuesta.setEDF_NOCABLE_EXISTE("");
        }

        // 20 EDF_NOCABLE_METROS
        if (objectGisResult.getEDF_NOCABLE_METROS() != null) {
            respuesta.setEDF_NOCABLE_METROS(objectGisResult.getEDF_NOCABLE_METROS());
        } else {
            respuesta.setEDF_NOCABLE_METROS("");
        }

        // 21 PST_APOYO
        if (objectGisResult.getPST_APOYO() != null) {
            respuesta.setPST_APOYO(objectGisResult.getPST_APOYO());
        } else {
            respuesta.setPST_APOYO("");
        }

        // 22 NAP_TEC
        if (objectGisResult.getNAP_TEC() != null) {
            respuesta.setNAP_TEC(objectGisResult.getNAP_TEC());
        } else {
            respuesta.setNAP_TEC("");
        }

        // 23 S4G_TEC
        if (objectGisResult.getS4G_TEC() != null) {
            respuesta.setS4G_TEC(objectGisResult.getS4G_TEC());
        } else {
            respuesta.setS4G_TEC("");
        }

        // 24 EST_BASE
        if (objectGisResult.getEST_BASE() != null) {
            respuesta.setEST_BASE(objectGisResult.getEST_BASE());
        } else {
            respuesta.setEST_BASE("");
        }

        // 25 CIR_DIGITAL
        if (objectGisResult.getCIR_DIGITAL() != null) {
            respuesta.setCIR_DIGITAL(objectGisResult.getCIR_DIGITAL());
        } else {
            respuesta.setCIR_DIGITAL("");
        }

        // 26 DTH_TEC
        if (objectGisResult.getDTH_TEC() != null) {
            respuesta.setDTH_TEC(objectGisResult.getDTH_TEC());
        } else {
            respuesta.setDTH_TEC("");
        }

        // 27 DTH_BLQ
        if (objectGisResult.getDTH_BLQ() != null) {
            respuesta.setDTH_BLQ(objectGisResult.getDTH_BLQ());
        } else {
            respuesta.setDTH_BLQ("");
        }

        // 28 ZAR_TEC
        if (objectGisResult.getZAR_TEC() != null) {
            respuesta.setZAR_TEC(objectGisResult.getZAR_TEC());
        } else {
            respuesta.setZAR_TEC("");
        }

        // 29 ZAR_BLQ
        if (objectGisResult.getZAR_BLQ() != null) {
            respuesta.setZAR_BLQ(objectGisResult.getZAR_BLQ());
        } else {
            respuesta.setZAR_BLQ("");
        }

        // 30 COOR_X
        if (objectGisResult.getCOOR_X() != null) {
            respuesta.setCOOR_X(objectGisResult.getCOOR_X());
        } else {
            respuesta.setCOOR_X("");
        }

        // 31 COOR_Y
        if (objectGisResult.getCOOR_Y() != null) {
            respuesta.setCOOR_Y(objectGisResult.getCOOR_Y());
        } else {
            respuesta.setCOOR_Y("");
        }

        // 32 WS_STATUS
        if (objectGisResult.getWS_STATUS() != null) {
            respuesta.setWS_STATUS(objectGisResult.getWS_STATUS());
        } else {
            respuesta.setWS_STATUS("");
        }

        // 33 WS_ERROR
        if (objectGisResult.getWS_ERROR() != null) {
            respuesta.setWS_ERROR(objectGisResult.getWS_ERROR());
        } else {
            respuesta.setWS_ERROR("");
        }

        return respuesta;
    }

    private OffersGisResponse getFromGis(ApiResponse<OffersApiGisResponseBody> apiGisResponseBody, String operacionComercial) {
        OffersGisResponse objGis = new OffersGisResponse();

        try {

            if (apiGisResponseBody != null) {

                if (apiGisResponseBody.getBodyOut() != null) {

                    if (apiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult() != null) {

                        WSSIGFFTT_FFTT_AVEResult wSSIGFFTT_FFTT_AVEResult = getNullsToEmptyFromGIS(apiGisResponseBody.getBodyOut().getwSSIGFFTT_FFTT_AVEResult());
                        WSSIGFFTT_FFTT_AVEResult wSSIGFFTT_FFTT_AVEResult_2 = wSSIGFFTT_FFTT_AVEResult;
                        wSSIGFFTT_FFTT_AVEResult_2.setWS_ERROR("");
                        wSSIGFFTT_FFTT_AVEResult_2.setWS_STATUS("");
                        JSONObject jasonBean = new JSONObject(wSSIGFFTT_FFTT_AVEResult_2);

                        String cadena = jasonBean.toString().substring(1, jasonBean.toString().length() - 1);

                        String[] lista = cadena.split(",");
                        String[] campos = new String[lista.length];
                        String[] valores = new String[lista.length];
                        //logger.info("cadena: " + cadena);

                        for (int i = 0; i < lista.length; i++) {
                            String[] lista2 = lista[i].split(":");
                            campos[i] = lista2[0].substring(1, lista2[0].length() - 1);
                            valores[i] = lista2[1].equalsIgnoreCase("null") ? "" : lista2[1].substring(1, lista2[1].length() - 1);
                            //logger.info("campos[" + i + "]: " + campos[i]);
                            //logger.info("valores[" + i + "]: " + valores[i]);
                        }

                        List<OfferResponseParameter> list = dao.readOferFromGIS();

                        String[] listInternet = new String[list.size()];
                        String[] listTv = new String[list.size()];
                        String[] listOperacion = new String[list.size()];
                        String[] listSpeed = new String[list.size()];
                        String[] listSignal = new String[list.size()];
                        int novalorSpeed = -1;
                        int novalorSignal = -1;
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i) != null) {
                                JSONObject ooobj = new JSONObject(list.get(i));
                                logger.info("ReadOfferFromGis => ooobj: " + ooobj);

                                if (list.get(i).getCategory() != null) {

                                    if (!list.get(i).getCategory().equals("")) {

                                        if (list.get(i).getCategory().equalsIgnoreCase(operacionComercial)) {

                                            listOperacion[i] = list.get(i).getCategory();
                                        }

                                    } else {
                                        listOperacion[i] = "";
                                    }
                                }

                                if (list.get(i).getAuxiliar() != null) {

                                    if (list.get(i).getAuxiliar().equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_TI)) {

                                        if (list.get(i).getCategory() != null) {

                                            if (!list.get(i).getCategory().equals("")) {

                                                if (!list.get(i).getCategory().equalsIgnoreCase(operacionComercial)) {

                                                    novalorSpeed = i;

                                                }
                                            }
                                        }

                                        if (list.get(i).getElement() != null) {

                                            boolean cumple = true;
                                            String[] elementos = list.get(i).getElement().split(",");

                                            for (int j = 0; j < elementos.length; j++) {

                                                for (int k = 0; k < campos.length; k++) {

                                                    if (campos[k] != null) {

                                                        if (elementos[j].equalsIgnoreCase(campos[k])) {

                                                            if (list.get(i).getStrfilter() != null) {

                                                                String[] filter = list.get(i).getStrfilter().split(",");

                                                                for (int l = 0; l < filter.length; l++) {

                                                                    String[] flag = filter[j].split(":");

                                                                    if (flag[0].equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_FLAG)) {
                                                                        if (!flag[1].equalsIgnoreCase(valores[k])) {
                                                                            cumple = false;
                                                                        }
                                                                        listSpeed[i] = "";
                                                                    } else {
                                                                        if (flag[0].equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_VELOCIDAD)) {
                                                                            String operacion = "";
                                                                            if (flag[1].equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_OPERACION_ALTA_PURA.substring(0, 1))) {
                                                                                operacion = ProductConstants.PARAMETRO_GIS_OPERACION_ALTA_PURA;
                                                                            } else {
                                                                                if (flag[1].equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_OPERACION_MIGRACION.substring(0, 1))) {
                                                                                    operacion = ProductConstants.PARAMETRO_GIS_OPERACION_MIGRACION;
                                                                                }
                                                                            }
                                                                            if (operacion.equalsIgnoreCase(operacionComercial)) {
                                                                                listSpeed[i] = valores[k];
                                                                            }

                                                                        }
                                                                    }

                                                                }

                                                            }
                                                        }

                                                    }
                                                }
                                            }

                                            if (cumple && (i != novalorSpeed)) {
                                                listInternet[i] = list.get(i).getStrValue();
                                            } else {
                                                listInternet[i] = "";
                                            }
                                        }
                                    }

                                    if (list.get(i).getAuxiliar().equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_TTV)) {

                                        if (list.get(i).getElement() != null) {

                                            boolean cumple = true;
                                            String[] elementos = list.get(i).getElement().split(",");

                                            for (int j = 0; j < elementos.length; j++) {

                                                for (int k = 0; k < campos.length; k++) {

                                                    if (campos[k] != null) {

                                                        if (elementos[j].equalsIgnoreCase(campos[k])) {

                                                            if (list.get(i).getStrfilter() != null) {

                                                                String[] filter = list.get(i).getStrfilter().split(",");

                                                                for (int l = 0; l < filter.length; l++) {

                                                                    String[] flag = filter[j].split(":");

                                                                    if (flag[0].equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_FLAG)) {
                                                                        if (!flag[1].equalsIgnoreCase(valores[k])) {
                                                                            cumple = false;
                                                                        }
                                                                        listSignal[i] = "";
                                                                    } else {
                                                                        if (flag[0].equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_SEÑAL)) {

                                                                            String flagSignal = "";

                                                                            if (flag[1].equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_SEÑAL_DIGITAL.substring(0, 1))) {
                                                                                flagSignal = ProductConstants.PARAMETRO_GIS_SEÑAL_DIGITAL;
                                                                            } else {
                                                                                if (flag[1].equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_SEÑAL_ANALOGICO.substring(0, 1))) {
                                                                                    flagSignal = ProductConstants.PARAMETRO_GIS_SEÑAL_ANALOGICO;
                                                                                }
                                                                            }

                                                                            if (flagSignal.equalsIgnoreCase(valores[k])) {
                                                                                listSignal[i] = valores[k];
                                                                            }

                                                                            if (list.get(i).getCategory() != null) {

                                                                                if (!list.get(i).getCategory().equals("")) {

                                                                                    if (!list.get(i).getCategory().equalsIgnoreCase(valores[k])) {

                                                                                        novalorSignal = i;
                                                                                    }
                                                                                }
                                                                            }

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            if (cumple && (i != novalorSignal)) {
                                                listTv[i] = list.get(i).getStrValue();
                                            } else {
                                                listTv[i] = "";
                                            }

                                        }

                                    }

                                }

                            }
                        }

                        boolean existeProducto = false;
                        boolean existenn = false;
                        for (String ti : listInternet) {
                            if (ti != null) {
                                if (!ti.equals("")) {
                                    if (!ti.equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_NINGUN_PRODUCTO)) {
                                        existeProducto = true;
                                    } else {
                                        existenn = true;
                                    }
                                }
                            }
                        }

                        for (String ttv : listTv) {
                            if (ttv != null) {
                                if (!ttv.equals("")) {
                                    if (!ttv.equalsIgnoreCase(ProductConstants.PARAMETRO_GIS_NINGUN_PRODUCTO)) {
                                        existeProducto = true;
                                    } else {
                                        existenn = true;
                                    }
                                }
                            }
                        }
                        if (!existeProducto && existenn) {
                            String[] listanoaplicati = new String[1];
                            listanoaplicati[0] = ProductConstants.PARAMETRO_GIS_NO_APLICA;
                            objGis.setTecInternet(listanoaplicati);
                        } else {
                            objGis.setTecInternet(listInternet);
                        }
                        if (!existeProducto && existenn) {
                            String[] listanoaplicattv = new String[1];
                            listanoaplicattv[0] = ProductConstants.PARAMETRO_GIS_NO_APLICA;
                            objGis.setTecTV(listanoaplicattv);
                        } else {
                            objGis.setTecTV(listTv);
                        }
                        objGis.setOperacion(listOperacion);
                        objGis.setVelocidadMax(listSpeed);
                        objGis.setTipoSenal(listSignal);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error getFromGis Service.", e);
        }

        return objGis;
    }

    private ApiResponse<OffersApiCalculadoraArpuResponseBody> getCalculadora(OffersApiCalculadoraArpuRequestBody apiCalculadoraArpuRequestBody)
            throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getFfttCalculadoraConsultarArpuUri())
                .setApiId(apiHeaderConfig.getFfttCalculadoraConsultarArpuApiId())
                .setApiSecret(apiHeaderConfig.getFttCalculadoraConsultarArpuApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_AVVE)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_AVVE).build();
        CalculadoraClient calculadoraClient = new CalculadoraClient(config);
        ClientResult<ApiResponse<OffersApiCalculadoraArpuResponseBody>> result = calculadoraClient.post(apiCalculadoraArpuRequestBody);
        ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalculadora = result.getResult();
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }

        return objCalculadora;
    }

    //sprint 25 offline Calculadora
    private List<OffersApiCalculadoraArpuResponseBody> contingenciaCalculadoraOutOffers(String telefono, OutOffersPlantaRequest request) {

        List<OffersApiCalculadoraArpuResponseBody> data =
                serviceCallEventsDao.consultaCalculadora(telefono,request.getPsprincipal(),request.getProductDescription());

        return data;
    }

    private List<OffersApiCalculadoraArpuResponseBody> contingenciaCalculadora(String telefono) {

        List<OffersApiCalculadoraArpuResponseBody> data =serviceCallEventsDao.consultaCalculadora(telefono);

        return data;
    }

    private List<OffersApiCalculadoraArpuResponseBody> contingenciaCalculadora(String telefono, OffersRequest2 requesCalculadora) {

        List<OffersApiCalculadoraArpuResponseBody> data =
                serviceCallEventsDao.consultaCalculadora(telefono,requesCalculadora.getPsprincipal(),requesCalculadora.getProductDescription());

        return data;
    }


    private ApiResponse<OffersApiExpertoResponseBody> getExperto(OffersApiExpertoRequestBody apiExpertoRequestBody) {
        try {
            ClientConfig config = new ClientConfig.ClientConfigBuilder()
                    .setUrl(apiHeaderConfig.getExpertoObtenerExpertoUri())
                    .setApiId(apiHeaderConfig.getExpertoObtenerExpertoApiId())
                    .setApiSecret(apiHeaderConfig.getExpertoObtenerExpertoApiSecret())
                    .setOperation(Constants.API_REQUEST_HEADER_OPERATION_EXPERTO)
                    .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_EXPERTO).build();
            ExpertoClient expertoClient = new ExpertoClient(config);
            ClientResult<ApiResponse<OffersApiExpertoResponseBody>> result = expertoClient.post(apiExpertoRequestBody);
            ApiResponse<OffersApiExpertoResponseBody> objExperto = result.getResult();
            serviceCallEventsService.registerEvent(result.getEvent());

            if (!result.isSuccess()) {
                return getResponseOnScoringError();
            }

            if (!Constants.RESPONSE.equals(objExperto.getHeaderOut().getMsgType())) {
                return getResponseOnScoringError();
            }
            return objExperto;
        } catch (Exception ex) {
            return getResponseOnScoringError();
        }
    }

    public ApiResponse<OffersApiExpertoResponseBody> getResponseOnScoringError() {

        ApiResponse<OffersApiExpertoResponseBody> objExpertoError = new ApiResponse<OffersApiExpertoResponseBody>();
        OffersApiExpertoResponseBody obj = new OffersApiExpertoResponseBody();

        obj.setAccion("CLIENTE NUEVO");
        obj.setCodConsulta("N/A");
        obj.setDeudaAtis("0");
        obj.setDeudaCms("0");

        List<OffersApiExpertoResponseBodyOperacion> operacionComercial = new ArrayList<OffersApiExpertoResponseBodyOperacion>();
        OffersApiExpertoResponseBodyOperacion operacionComercialObj = new OffersApiExpertoResponseBodyOperacion();
        operacionComercialObj.setCodOpe("ALTA PURA");
        operacionComercialObj.setRenta("S/. 700");
        operacionComercialObj.setSegmento("C2");
        operacionComercialObj.setContadorDetalle(6);

        List<OffersApiExpertoResponseBodyOperacionDetalle> detalle = new ArrayList<OffersApiExpertoResponseBodyOperacionDetalle>();

        List<Map<String, String>> mapList = daoCustomer.getDefaultItemsCatalog();
        for (Map<String, String> map : mapList) {
            String productType = (map.get("producttype") != null) ? map.get("producttype") : "SIN DESCRIPCIÓN";
            String cashPrice = (map.get("cashprice") != null && !map.get("cashprice").equals("")) ? map.get("cashprice") : "0.00";
            String codProduct = (map.get("prodcategorycode") != null && !map.get("prodcategorycode").equals("")) ? map.get("prodcategorycode") : "000";

            //if(Integer.parseInt(codProduct) > 200) continue;

            OffersApiExpertoResponseBodyOperacionDetalle det = new OffersApiExpertoResponseBodyOperacionDetalle();
            det.setCodProd(codProduct);
            det.setDesProd(productType);
            det.setCuotaFin((float) 0.0);
            det.setMesesFin(0);
            det.setPagoAdelantado(Float.parseFloat(cashPrice));
            det.setNroDecosAdic(0);

            detalle.add(det);
        }

        operacionComercialObj.setDetalle(detalle);
        operacionComercial.add(operacionComercialObj);


        OffersApiExpertoResponseBodyOperacion operacionComercialObj2 = new OffersApiExpertoResponseBodyOperacion();
        operacionComercialObj2.setCodOpe("ALTA COMPONENTE");
        operacionComercialObj2.setRenta("0");
        operacionComercialObj2.setSegmento("C2");
        operacionComercialObj2.setContadorDetalle(0);
        List<OffersApiExpertoResponseBodyOperacionDetalle> detalle2 = new ArrayList<OffersApiExpertoResponseBodyOperacionDetalle>();
        operacionComercialObj2.setDetalle(detalle2);
        operacionComercial.add(operacionComercialObj2);


        obj.setOperacionComercial(operacionComercial);
        objExpertoError.setBodyOut(obj);

        ApiResponseHeader header = new ApiResponseHeader();
        header.setMsgType("RESPONSE");
        header.setDestination("");
        header.setExecId("");
        header.setOriginator("");
        header.setTimestamp("");

        objExpertoError.setHeaderOut(header);

        return objExpertoError;
    }

    private String logObject(Object o) {
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        try {
            json = mapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            json = "Error while logging object";
        }
        return json;
    }

    public List<String> getOrderListForAudioProcess(String hoy) {
        logger.info("Inicio getOrderList Service");
        List<String> orderList = dao.getOrderListForAudioProcess(hoy);
        //LogVass.serviceResponseObject(logger, "LoadUserDataDetails", "configParameterResponseData", configParameterResponseDataList);
        logger.info("Fin getOrderList Service");
        return orderList;
    }

//    public Response<List<OffersResponseData2>> readOutOffers(OutOffersRequest request) {
//        Response<List<OffersResponseData2>> response = new Response<>();
//        List<OffersResponseData2> listOut = new ArrayList<>();
//
//        for(int i=0;listOut.size()<1;i++){
//            listOut.add(cargarOutList());
//        }
//
//        response.setResponseData(listOut);
//        response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_OK);
//        return response;
//    }

    public Response<List<OutOffersResponseData>> readOutOffers(OutOffersRequest request) {
        Response<List<OutOffersResponseData>> response = new Response<>();
        try {

            List<TdpCatalogOut> tdpCatalogOutList = tdpCatalogOutRepository.findAllByTipoDocumentoAndDni(request.getDocumentType(), request.getDocumentNumber());

            // response.setResponseData(cargarOutBIList(tdpCatalogOutList));
            response.setResponseData(cargarOutListPrueba());

            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_OK);
        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            // ejemplo: cuando falta un dato obligatorio
            logger.error("Error outOffering Controller.", arrayIndexOutOfBoundsException);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Estructura de datos incorrecta");
        } catch (NullPointerException npe) {
            // ejemplo: cuando no trae el precio y se quiere transformar a double
            logger.error("Error outOffering Controller.", npe);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Estructura de datos incorrecta");
        } catch (NumberFormatException nfe) {
            // ejemplo: cuando no trae el precio y se quiere transformar a double
            logger.error("Error outOffering Controller.", nfe);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Estructura de datos incorrecta");
        } catch (Exception e) {
            logger.error("Error outOffering Controller.", e);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("PRODUCTO OUT no está disponible temporalmente. Intente en unos minutos.");
        }

        return response;
    }

    public Response<List<OutOffersResponseData>> readOutOffersCapta(OutOffersRequest request) {
        Response<List<OutOffersResponseData>> response = new Response<>();
        try {

            List<OutOffersResponseData> tdpCatalogOutList = dao.readOffersOutCapta(request);

            // response.setResponseData(cargarOutBIList(tdpCatalogOutList));
            response.setResponseData(tdpCatalogOutList);

            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_OK);
        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            // ejemplo: cuando falta un dato obligatorio
            logger.error("Error outOfferingCapta Controller.", arrayIndexOutOfBoundsException);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Estructura de datos incorrecta");
        } catch (NullPointerException npe) {
            // ejemplo: cuando no trae el precio y se quiere transformar a double
            logger.error("Error outOfferingCapta Controller.", npe);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Estructura de datos incorrecta");
        } catch (NumberFormatException nfe) {
            // ejemplo: cuando no trae el precio y se quiere transformar a double
            logger.error("Error outOfferingCapta Controller.", nfe);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Estructura de datos incorrecta");
        } catch (Exception e) {
            logger.error("Error outOfferingCapta Controller.", e);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("PRODUCTO OUT CAPTA no está disponible temporalmente. Intente en unos minutos.");
        }

        return response;
    }

    public Response<List<OutOffersResponseData>> readOutOffersOutPlanta(OutOffersPlantaRequest request) {
        Response<List<OutOffersResponseData>> response = new Response<>();
        try {

            // Se leen los productos de planta y se cruza con ficha producto, además se obtienen los svas de los productos
            List<String> lstSvas = new ArrayList<String>();
            List<OutOffersResponseData> tdpCatalogOutList = dao.readOffersOutPlanta(request, lstSvas);
            // Se crea un Map con la lista de productos y el productCode como llave para cada lista
            Map<String, List<OutOffersResponseData>> mapOffer = new HashMap<String, List<OutOffersResponseData>>();

            for (OutOffersResponseData offers : tdpCatalogOutList) {
                List<OutOffersResponseData> tmp = mapOffer.get(offers.getProductCode());
                if (tmp == null) {
                    tmp = new ArrayList<>();
                }
                tmp.add(offers);
                mapOffer.put(offers.getProductCode(), tmp);
                offers.setAltaPuraSvasNameBloqueTV(dao.readSVANames(offers.getAltaPuraSvasBloqueTV()));
            }

            /* Consulta al ApiConnect de Calculadora - En caso de error se retorna un mensaje personalizado al usuario */
            OffersApiCalculadoraArpuRequestBody offersApiCalculadoraArpuRequestBody = new OffersApiCalculadoraArpuRequestBody();
            if (request.getLegacyCode().equalsIgnoreCase("ATIS")) {
                offersApiCalculadoraArpuRequestBody.setTelefono(request.getPhoneOrCms());
            } else if (request.getLegacyCode().equalsIgnoreCase("CMS")) {
                offersApiCalculadoraArpuRequestBody.setTelefono("-" + request.getPhoneOrCms());
            }
            ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalculadora = null;
            HashMap<String, MessageEntities> messageArpu = messageUtil.getMessages("ARPU");
            LogVass.serviceResponseHashMap(logger, "GetArpu", "messageArpu", messageArpu);
            //sprint 25 offline Calculadora
            try {
                objCalculadora = getCalculadora(offersApiCalculadoraArpuRequestBody);
            } catch (ClientException e) {
                //response.setResponseCode(messageArpu.get("errorArpu").getCode());
                //response.setResponseMessage(messageArpu.get("errorArpu").getMessage());
                logger.error("ErrorClient readOutOffersOutPlanta Service al obtener calculadora", e.getMessage());
                //return response;
                objCalculadora=null;
            }

            /* Se obtiene el body de calculadora*/
            //sprint 25 offline Calculadora
            List<OffersApiCalculadoraArpuResponseBody> lstObjCalculadora = null;
            if (objCalculadora == null || objCalculadora.getBodyOut() == null) {
                lstObjCalculadora=contingenciaCalculadoraOutOffers(offersApiCalculadoraArpuRequestBody.getTelefono(), request);
            } else {
                if(objCalculadora != null || objCalculadora.getBodyOut() != null){
                lstObjCalculadora = (List<OffersApiCalculadoraArpuResponseBody>) objCalculadora.getBodyOut();
                    for( OffersApiCalculadoraArpuResponseBody item : lstObjCalculadora){
                        if(item.getPaqueteOrigen() == null){
                            if(item.getPaquetePsOrigen() == null){
                                if(item.getProductoDestinoPs() == null){
                                    if(item.getProductoDestinoNombre() == null){
                                        lstObjCalculadora=contingenciaCalculadoraOutOffers(offersApiCalculadoraArpuRequestBody.getTelefono(), request);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            LogVass.serviceResponseArray(logger, "readOutOffersOutPlanta", "lstObjCalculadora", lstObjCalculadora);

            // se obtiene la cantidad de decos de parameters
            double maxDecos = 6.0;
            try {
                Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "POLICY_PARAMETER", "max_amount_decoders_by_floor");
                maxDecos = Double.parseDouble(objParameter.getStrValue());
            } catch (Exception e) {
                logger.entry("Error readOutOffersOutPlanta Service. - Parameters.", e);
            }

            for(OutOffersResponseData oferta : tdpCatalogOutList){

                for (OffersApiCalculadoraArpuResponseBody objCalculadoraTmp : lstObjCalculadora) {
                    if(objCalculadoraTmp.getProductoDestinoPs()!=null && !objCalculadoraTmp.getProductoDestinoPs().isEmpty()){
                        if(objCalculadoraTmp.getProductoDestinoPs().equals(oferta.getProductCode())){
                            try {
                                //oferta.setRentatotal(Double.parseDouble(objCalculadoraTmp.getRentaTotal()));
                                Double decos = 0.0;
                                if (objCalculadoraTmp.getCantidadDecos() != null) {
                                    decos = Double.parseDouble(objCalculadoraTmp.getCantidadDecos());
                                }
                                oferta.setCantidaddecos(maxDecos - decos);

                                oferta.setInternet(Double.parseDouble(objCalculadoraTmp.getInternet()));
                            } catch (Exception ex) {
                                oferta.setRentatotal(-1);
                                oferta.setCantidaddecos(-1);
                                oferta.setInternet(-1);
                            }
                            break;
                        }
                    }
                }
            }
            response.setResponseData(tdpCatalogOutList);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_OK);

        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
            // ejemplo: cuando falta un dato obligatorio
            logger.error("Error readOffersOutPlanta Controller.", arrayIndexOutOfBoundsException);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Estructura de datos incorrecta");
        } catch (NullPointerException npe) {
            // ejemplo: cuando no trae el precio y se quiere transformar a double
            logger.error("Error readOffersOutPlanta Controller.", npe);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Estructura de datos incorrecta");
        } catch (NumberFormatException nfe) {
            // ejemplo: cuando no trae el precio y se quiere transformar a double
            logger.error("Error readOffersOutPlanta Controller.", nfe);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Estructura de datos incorrecta");
        } catch (Exception e) {
            logger.error("Error readOffersOutPlanta Controller.", e);
            response.setResponseCode(ProductConstants.OFFERS_RESPONSE_CODE_ERROR);
            response.setResponseMessage("PRODUCTO OUT PLANTA no está disponible temporalmente. Intente en unos minutos.");
        }

        return response;
    }

    private List<OutOffersResponseData> cargarOutBIList(List<TdpCatalogOut> tdpCatalogOutList) {

        List<OutOffersResponseData> listOut = new ArrayList<>();

        if (tdpCatalogOutList.size() > 0) {
            ///////////////////////// OFERTA 1
            // en el jpa se discrimino VALIDAR GIS y SOLO OFRECER MONO DTH
            TdpCatalogOut tdpCatalogOut = tdpCatalogOutList.get(0);
            OutOffersResponseData oferta1 = new OutOffersResponseData();

            oferta1.setProductName(tdpCatalogOut.getNom_prod_oferta());
            String nomDescuentoOferta1 = (tdpCatalogOut.getNom_descuento_oferta() == null) ? ""
                    : tdpCatalogOut.getNom_descuento_oferta();
            oferta1.setDiscount(nomDescuentoOferta1.isEmpty() ? "No" : "Si");
            oferta1.setPrice(Double.parseDouble(tdpCatalogOut.getRenta_destino()));

            // '2 primeros meses a S/.119.90' ---> ['119.90','2']
            String[] nomDescuentoOferta1Split = nomDescuentoOferta1.split(" primeros meses a S/.");
            // verificamos el otro posible valor
            if (nomDescuentoOferta1.contains(" primeros meses S/.")) {
                nomDescuentoOferta1Split = nomDescuentoOferta1.split(" primeros meses S/.");
            }
            if (nomDescuentoOferta1Split.length > 1) {
                oferta1.setPromPrice(Double.parseDouble(nomDescuentoOferta1Split[1]));
                oferta1.setMonthPeriod(Integer.parseInt(nomDescuentoOferta1Split[0]));
            }


            String nomPromocionOferta1 = (tdpCatalogOut.getNom_promocion_oferta() == null) ? ""
                    : tdpCatalogOut.getNom_promocion_oferta();
            oferta1.setEquipmentType(nomPromocionOferta1.isEmpty() ? "No" : "Si");

            oferta1.setInternetTech(tdpCatalogOut.getTx_cross_oferta1());
            // '2 deco HD + modem + telefono|  | 30MB x 3 meses' --> ['2 deco HD + modem + telefono','  ',' 30MB x 3 meses']
            String[] nomPromocionOferta1Split = nomPromocionOferta1.split("\\|");
            // '2 deco HD + modem + telefono' --> ['2 deco HD',' modem','telefono']
            String[] equipamientos = nomPromocionOferta1Split[0].split(" \\+ ");

            if (equipamientos.length > 2)
                oferta1.setEquipamientoLinea(equipamientos[2]);
            if (equipamientos.length > 1)
                oferta1.setEquipamientoInternet(equipamientos[1]);
            // '2 deco HD ' -->  ['2','HD']
            String[] equipamientoTV = equipamientos[0].split(" deco ");
            if (equipamientoTV.length > 1)
                oferta1.setEquipamientoTv(equipamientoTV[0] + equipamientoTV[1]);

            if (nomPromocionOferta1Split.length > 2 && !nomPromocionOferta1Split[2].trim().isEmpty()) {
                // ' 30MB x 3 meses' --> [' 30','3 meses']
                String[] velocidadxMesesOferta1 = nomPromocionOferta1Split[2].split("MB x ");
                String velocidadOferta1 = velocidadxMesesOferta1[0].replace(" ", "");
                oferta1.setPromoInternetSpeed(Integer.parseInt(velocidadOferta1));
                String mesesOferta1 = velocidadxMesesOferta1[1].replace(" meses", "");
                oferta1.setPeriodoPromoInternetSpeed(Integer.parseInt(mesesOferta1));
            }

            oferta1.setCustomerName(tdpCatalogOut.getTx_nombre());

            oferta1.setDepartamento(tdpCatalogOut.getDepartamento());
            // para que en el front que provincia y distrito solo cuando el departemento sea LIMA, pq no hay provincia para el resto
            if ("LIMA".equals(tdpCatalogOut.getDepartamento())) {
                oferta1.setProvincia(tdpCatalogOut.getLima_prov());
                oferta1.setDistrito(tdpCatalogOut.getDistrito());
                oferta1.setDireccion(tdpCatalogOut.getTx_direccion());
            }


            listOut.add(oferta1);

            ///////////////////////// OFERTA 2

            // 'Segunda Oferta: DUO 6MB  | Precio:89.90'
            String tx_otros1 = tdpCatalogOut.getTx_otros1();
            if (tx_otros1 != null && !tx_otros1.isEmpty()) {
                String[] tx_otros1Split = tx_otros1.split("\\|");
                String[] productOferta2 = tx_otros1Split[0].split(":");
                String productNameOferta2 = productOferta2[1].trim();
                if (!"".equals(productNameOferta2)) {
                    OutOffersResponseData oferta2 = new OutOffersResponseData();

                    oferta2.setProductName(productNameOferta2);

                    // 'Segunda Oferta: 89.90 | Descuento: 2 primeros meses a S/.69.90 | 2 deco HD + modem + telefono |  | Promoción: 30MB x 3 meses'
                    String tx_otros2 = tdpCatalogOut.getTx_otros2();
                    String[] tx_otros2Split = tx_otros2.split("\\|");

                    String priceOferta2 = tx_otros2Split[0].split(":")[1];
                    oferta2.setPrice(Double.parseDouble(priceOferta2.trim()));

                    String descuentoOferta2 = tx_otros2Split[1].split(":")[1].trim();
                    if (!descuentoOferta2.isEmpty()) {
                        oferta2.setDiscount("Si");
                        String[] descuentoOferta2Split;

                        descuentoOferta2Split = descuentoOferta2.split(" primeros meses a S/.");
                        // verificamos el otro posible valor
                        if (descuentoOferta2.contains(" primeros meses S/.")) {
                            descuentoOferta2Split = descuentoOferta2.split(" primeros meses S/.");
                        }
                        if (descuentoOferta2Split.length > 1) {
                            oferta2.setPromPrice(Double.parseDouble(descuentoOferta2Split[1]));
                            oferta2.setMonthPeriod(Integer.parseInt(descuentoOferta2Split[0]));
                        }

                    } else {
                        oferta2.setDiscount("No");
                    }

                    String equipamientoOferta2 = tx_otros2Split[2].trim();
                    if (!equipamientoOferta2.isEmpty()) {
                        String[] equipamientoOferta2Split = equipamientoOferta2.split(" \\+ ");
                        if (equipamientoOferta2Split.length > 2) {
                            oferta2.setEquipamientoLinea(equipamientoOferta2Split[2]);
                        }
                        if (equipamientoOferta2Split.length > 1) {
                            oferta2.setEquipamientoInternet(equipamientoOferta2Split[1]);
                        }
                        String[] equipamientoTVOferta2 = equipamientoOferta2Split[0].split(" deco ");
                        oferta2.setEquipamientoTv(equipamientoTVOferta2[0] + equipamientoTVOferta2[1]);
                    }

                    String[] promocionOferta2Split = tx_otros2Split[4].split(":");

                    if (promocionOferta2Split.length > 1 && !promocionOferta2Split[1].trim().isEmpty()) {
                        String[] velocidadxMesesOferta2 = promocionOferta2Split[1].split("MB x ");
                        String velocidadOferta2 = velocidadxMesesOferta2[0].replace(" ", "");
                        oferta2.setPromoInternetSpeed(Integer.parseInt(velocidadOferta2));
                        String mesesOferta2 = velocidadxMesesOferta2[1].replace(" meses", "");
                        oferta2.setPeriodoPromoInternetSpeed(Integer.parseInt(mesesOferta2));
                    }

                    oferta2.setCustomerName(tdpCatalogOut.getTx_nombre());

                    oferta2.setDepartamento(tdpCatalogOut.getDepartamento());
                    // para que en el front que provincia y distrito solo cuando el departemento sea LIMA, pq no hay provincia para el resto
                    if ("LIMA".equals(tdpCatalogOut.getDepartamento())) {
                        oferta2.setProvincia(tdpCatalogOut.getLima_prov());
                        oferta2.setDistrito(tdpCatalogOut.getDistrito());
                        oferta2.setDireccion(tdpCatalogOut.getTx_direccion());
                    }

                    listOut.add(oferta2);
                }
            }
        }

        return listOut;

    }

    private List<OutOffersResponseData> cargarOutListPrueba() {
        List<OutOffersResponseData> lista = new ArrayList<>();

        OutOffersResponseData item = new OutOffersResponseData();
        item.setId(155);
        item.setCommercialOperation("Alta Pura");
        item.setSegmento(null);
        item.setCanal("OUT");
        item.setEntidad(null);
        item.setProvincia("LIMA");
        item.setDistrito("LIMA");
        item.setCampaign("CIUDAD SITIADA");
        item.setCampaingCode(null);
        item.setPriority(null);
        item.setProductTypeCode("3");
        item.setProductType("Trio");
        item.setProductCategoryCode("102");
        item.setProductCategory("Trio CATV");
        item.setProductCode("22975");
        item.setProductName("Trio Plano Local 200 Mbps Estandar Digital HD");
        item.setTipoRegistro(null);
        item.setDiscount("Si");
        item.setPrice(539.9);
        item.setPromPrice(509.9);
        item.setMonthPeriod(2);
        item.setInstallCost(0);
        item.setLineType("Libre");
        item.setPaymentMethod("Financiado");
        item.setCashPrice(0);
        item.setFinancingCost(0);
        item.setFinancingMonth(0);
        item.setEquipmentType("Si");
        item.setReturnMonth(0);
        item.setReturnPeriod("No Aplica");
        item.setInternetSpeed(200);
        item.setPromoInternetSpeed(0);
        item.setPeriodoPromoInternetSpeed(0);
        item.setInternetTech("HFC");
        item.setTvSignal("Digital");
        item.setTvTech("CATV");
        item.setEquipamientoLinea("Equipo Telefonico");
        item.setEquipamientoInternet("Modem Smart Wifi");
        item.setEquipamientoTv("2HD");
        item.setExpertCode("10002000154069602");
        item.setRentatotal(0);
        item.setCantidaddecos(0);
        item.setInternet(0);
        item.setCategory("Alta Pura");
        item.setTargetType("ATIS");
        item.setSva(null);
        item.setAfinidadEntidad(2);
        item.setAfinidadUbicacion(1);
        item.setAltaPuraSvasBloqueTV("21148");
        item.setAltaPuraSvasLinea("-");
        item.setAltaPuraSvasInternet("-");
        item.setMsx_cbr_voi_ges_in("S");
        item.setMsx_cbr_voi_gis_in("S");
        item.setMsx_ind_snl_gis_cd("3");
        item.setMsx_ind_gpo_gis_cd("H");
        item.setCod_ind_sen_cms("C");
        item.setCod_cab_cms("01");
        item.setCod_fac_tec_cd("JUR0300706*JUR0300702*JUR0300502*JUR0300501*JUR0300705");
        item.setCobre_blq_vta("NO");
        item.setCobre_blq_trm(null);
        item.setCustomerName("MANUEL CUSIHUAMAN GUERRA");
        item.setDepartamento("LIMA");

        lista.add(item);

        OutOffersResponseData item2 = new OutOffersResponseData();
        item2.setId(1829);
        item2.setCommercialOperation("Alta Pura");
        item2.setSegmento(null);
        item2.setCanal(null);
        item2.setEntidad(null);
        item2.setProvincia(null);
        item2.setDistrito(null);
        item2.setCampaign("MASIVA");
        item2.setCampaingCode(null);
        item2.setPriority(null);
        item2.setProductTypeCode("3");
        item2.setProductType("Trio");
        item2.setProductCategoryCode("205");
        item2.setProductCategory("Completa con Linea y BA");
        item2.setProductCode("23146");
        item2.setProductName("Trio Plano Local 8 Mbps Estandar Digital");
        item2.setTipoRegistro(null);
        item2.setDiscount("No");
        item2.setPrice(149.9);
        item2.setPromPrice(149.9);
        item2.setMonthPeriod(0);
        item2.setInstallCost(0);
        item2.setLineType("Libre");
        item2.setPaymentMethod("Financiado");
        item2.setCashPrice(0);
        item2.setFinancingCost(0);
        item2.setFinancingMonth(0);
        item2.setEquipmentType("Si");
        item2.setReturnMonth(0);
        item2.setReturnPeriod("No Aplica");
        item2.setInternetSpeed(8);
        item2.setPromoInternetSpeed(120);
        item2.setPeriodoPromoInternetSpeed(3);
        item2.setInternetTech("HFC");
        item2.setTvSignal("Digital");
        item2.setTvTech("CATV");
        item2.setEquipamientoLinea("Equipo Telefonico");
        item2.setEquipamientoInternet("Modem Docsis");
        item2.setEquipamientoTv("");
        item2.setExpertCode("");
        item2.setRentatotal(0);
        item2.setCantidaddecos(0);
        item2.setInternet(0);
        item2.setCategory("Alta Pura");
        item2.setTargetType("ATIS");
        item2.setSva(null);
        item2.setAfinidadEntidad(2);
        item2.setAfinidadUbicacion(2);
        item2.setAltaPuraSvasBloqueTV("-");
        item2.setAltaPuraSvasLinea("-");
        item2.setAltaPuraSvasInternet("-");
        item2.setMsx_cbr_voi_ges_in("S");
        item2.setMsx_cbr_voi_gis_in("S");
        item2.setMsx_ind_snl_gis_cd("3");
        item2.setMsx_ind_gpo_gis_cd("H");
        item2.setCod_ind_sen_cms("C");
        item2.setCod_cab_cms("01");
        item2.setCod_fac_tec_cd("LIR0260904*LIR0260907*LIR0260901*LIR0260903*LIR0260905");
        item2.setCobre_blq_vta("NO");
        item2.setCobre_blq_trm(null);
        item2.setCustomerName("MANUEL CUSIHUAMAN GUERRA");
        item2.setDepartamento("LIMA");

        lista.add(item2);
        return lista;
    }

    public Response<OffersGisResponse> readGisOut(OffersApiGisRequestBody request) throws ClientException {

        Response<OffersGisResponse> response = new Response<>();
        ApiResponse<OffersApiGisResponseBody> objApiGisResponseBody = new ApiResponse<OffersApiGisResponseBody>();
        OffersGisResponse offersGisResponse = null;

        ApiResponseHeader headerOut = new ApiResponseHeader();
        headerOut.setMsgType(Constants.RESPONSE);
        objApiGisResponseBody.setHeaderOut(headerOut);

        // Obtener parámetro de número de intentos
        Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("APICONNECT", "URI", "tdp.api.gis");
        byPassServiciosCaidos = Boolean.parseBoolean(objParameter.getStrValue());

        objApiGisResponseBody = getGis(request, "",false);
        LogVass.serviceResponseObject(logger, "ReadOffers2", "objApiGisResponseBody", objApiGisResponseBody);
        offersGisResponse = getFromGis(objApiGisResponseBody, ProductConstants.PARAMETRO_GIS_OPERACION_ALTA_PURA);
        if (objApiGisResponseBody.getHeaderOut() != null) {
            if (objApiGisResponseBody.getHeaderOut().getMsgType() != null) {
                if ((Constants.RESPONSE).equals(objApiGisResponseBody.getHeaderOut().getMsgType())) {
                    if (offersGisResponse.getVelocidadMax() != null) {
                        int sizeVelocidad = offersGisResponse.getVelocidadMax().length;
                        String[] velocidadMax = new String[sizeVelocidad];
                        for (int i = 0; i < sizeVelocidad; i++) {
                            if (offersGisResponse.getVelocidadMax()[i] != null) {
                                velocidadMax[i] = offersGisResponse.getVelocidadMax()[i].replace(ProductConstants.OFFERS_API_AVVE_REQUEST_MAX_SPEED_FIX, Constants.EMPTY);
                            }
                        }
                        offersGisResponse.setVelocidadMax(velocidadMax);
                    }
                    if (offersGisResponse.getTipoSenal() != null) {
                        int sizeSignal = offersGisResponse.getTipoSenal().length;
                        String[] tipoSignal = new String[sizeSignal];
                        for (int i = 0; i < sizeSignal; i++) {
                            if (offersGisResponse.getTipoSenal()[i] != null) {
                                tipoSignal[i] = ProductConstants.OFFERS_TIPO_SENAL(offersGisResponse.getTipoSenal()[i]);
                            }
                        }
                        offersGisResponse.setTipoSenal(tipoSignal);
                    }
                }
            }
        }
        response.setResponseData(offersGisResponse);
        return response;
    }


    public Response<SvaV2ResponseData> readSVACalculadora(SvaV2Request request) {
        logger.info("Inicio readSVACalculadora :");
        Response<SvaV2ResponseData> response = new Response<>();
        SvaV2ResponseData responseData = new SvaV2ResponseData();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("SVA");
        LogVass.serviceResponseHashMap(logger, "GetSVA", "messageList", messageList);
        //End--Code for call Service
        Integer smartMb = 6;
        try {
            smartMb = Integer.parseInt(messageList.get("smartMb").getMessage());
        } catch (Exception eMb) {
            logger.error("Error Complements Service.", eMb);
        }

        String productCodeSVA = "";
        try {
            productCodeSVA = dao.getProductProductCodeById(Integer.parseInt(request.getProductId()));
        } catch (Exception e) {
            logger.error(e);
        }

        int maxDecosExtras;

        if (!productCodeSVA.isEmpty())
            maxDecosExtras = (productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL) || productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) ? 1 : 2;
        else
            maxDecosExtras = 2;

        try {
            /* Sprint 10 */
            TdpCatalog tdpCatalog = null;
            if (request.getProductId() != null) {
                tdpCatalog = tdpCatalogRepository.findOne(Integer.parseInt(request.getProductId()));
            }
            /*if (tdpCatalog != null) {
                request.setAltaPuraSvasBloqueTV(tdpCatalog.getProductBloqueTv().replace(" ", ""));
                request.setAltaPuraSvasLinea(tdpCatalog.getProductSvaLinea().replace(" ", ""));
                request.setAltaPuraSvasInternet(tdpCatalog.getProductSvaInternet().replace(" ", ""));
            }
            generarConsultaDefaultSvasAltaPura(request);*/

            /* Sprint 10 */
            List<SvaV2ResponseDataSva> list = dao.readSVAv2(request);

            //List <String> temp = new ArrayList<String>();
            List<SvaV2ResponseDataSva> temp = new ArrayList<>();

            int in = 0;

            String[] newNombre = new String[20];

            for (SvaV2ResponseDataSva item : list) {
                if (item.getBloquePadre() == null && item.isClickPadre()) {
                    temp.add(item);
                    if (item.isAltaPuraDefaultSVA()) {
                        newNombre[in] = item.getUnit();
                        in++;
                        System.out.println(in);
                    }
                }
            }
            for (SvaV2ResponseDataSva item : temp) {
                if (item.getBloquePadre() != null) {
                    for (String newItem : newNombre) {
                        if (item.getUnit() == newItem) {
                            item.setClickPadre(true);
                        } else {
                            item.setClickPadre(false);
                        }
                    }
                }
            }


            if (!list.isEmpty()) {
                if (!productCodeSVA.isEmpty()) {
                    if (productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL) || productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) {
                        int contador = 0;
                        Iterator<SvaV2ResponseDataSva> listIterable = list.iterator();
                        while (listIterable.hasNext()) {
                            SvaV2ResponseDataSva obj = listIterable.next();
                            if (obj.getCode().equals(ProductConstants.CODE_BLOQUE_PRODUCTO) ||
                                    obj.getCode().equals(ProductConstants.CODE_BLOQUE_TV)
                                    ) {
                                listIterable.remove();
                            }
                        }
                    }
                }
            }
            List<SvaV2ResponseDataSva> equipmentList = new ArrayList<>();
            List<SvaV2ResponseDataSva> svaList = new ArrayList<>();
            if (list.isEmpty()) {
                //response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_ERROR);
                responseData.setEquipment(equipmentList);
                responseData.setSva(svaList);

                response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_OK);
                response.setResponseData(responseData);
                //response.setResponseMessage(mensajeSvaSinDatosBD);
            } else {

                if (tdpCatalog != null) {
                    List<SvaV2ResponseDataSva> svaListTmp = new ArrayList<>();
                    for (SvaV2ResponseDataSva svaObject : list) {
                        if (svaObject.getCode().equals("DVR")) {
                            if (tdpCatalog.getTvTech().equalsIgnoreCase("CATV")) {
                                svaListTmp.add(svaObject);
                            }
                        } else if (svaObject.getCode().equals("DSHD")) {
                            if (tdpCatalog.getInternetSpeed() >= smartMb) {
                                svaListTmp.add(svaObject);
                            }
                        } else {
                            svaListTmp.add(svaObject);
                        }
                    }
                    list = svaListTmp;
                }

                String productCode = "";

                if (!productCodeSVA.isEmpty()) {
                    if (!productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_HOGAR_DIGITAL)
                            || !productCodeSVA.equals(ProductConstants.PARAMETRO_MONO_TV_SATELITAL_HOGAR_DIGITAL)) {
                        for (SvaV2ResponseDataSva svaObject : list) {
                            if ("BP".equalsIgnoreCase(svaObject.getCode()) && svaObject.isAltaPuraDefaultSVA()) {
                                productCode += svaObject.getUnit().replace("+", ",") + ",";
                            }
                        }
                        if (!productCode.equalsIgnoreCase("")) {
                            String[] nameProduct = productCode.replace("BLOQUE", "").split(",");

                            List<SvaV2ResponseDataSva> svaListTmp = new ArrayList<>();
                            for (SvaV2ResponseDataSva svaObject : list) {

                                if ("BTV".equalsIgnoreCase(svaObject.getCode())) {
                                    boolean isExiste = false;
                                    for (String obj : nameProduct) {
                                        if (svaObject.getUnit().indexOf(obj.trim()) >= 0) {
                                            isExiste = true;
                                        }
                                    }
                                    if (!isExiste) {
                                        svaListTmp.add(svaObject);
                                    }
                                } else {
                                    svaListTmp.add(svaObject);
                                }

                            }
                            list = svaListTmp;
                        }

                        productCode = "";
                        for (SvaV2ResponseDataSva svaObject : list) {
                            if ("BTV".equalsIgnoreCase(svaObject.getCode()) && svaObject.isAltaPuraDefaultSVA()) {
                                productCode += svaObject.getUnit().replace("+", ",").replace("NEW", "") + ",";
                            }
                        }
                        if (!productCode.equalsIgnoreCase("")) {
                            String[] nameProduct = productCode.replace("BLOQUE", "").split(",");

                            List<SvaV2ResponseDataSva> svaListTmp = new ArrayList<>();
                            for (SvaV2ResponseDataSva svaObject : list) {
                                if ("BP".equalsIgnoreCase(svaObject.getCode())) {
                                    boolean isExiste = false;
                                    for (String obj : nameProduct) {
                                        if (svaObject.getUnit().indexOf(obj.trim()) >= 0) {
                                            isExiste = true;
                                        }
                                    }
                                    if (!isExiste) {
                                        svaListTmp.add(svaObject);
                                    }
                                } else {
                                    svaListTmp.add(svaObject);
                                }
                            }
                            list = svaListTmp;
                        }

                    }
                }

                for (SvaV2ResponseDataSva svaObject : list) {
                    if ("EQUIPAMIENTO".equals(svaObject.getCode())) {
                        equipmentList.add(svaObject);
                    } else {
                        if ((svaObject.getCode().equals("DSHD") || svaObject.getCode().equals("DHD") || svaObject.getCode().equals("DVR")) && Integer.parseInt(svaObject.getUnit()) > maxDecosExtras)
                            continue;
                        svaList.add(svaObject);
                    }
                }

                responseData.setEquipment(equipmentList);
                responseData.setSva(svaList);

                response.setResponseCode(ProductConstants.SVA_RESPONSE_CODE_OK);
                response.setResponseData(responseData);
            }
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            logger.info("Fin Complements Service.");
            logger.error("Error Complements Service.", e);
            return response;
        }
        logger.info("Fin Complements Service.");

        return response;
    }

    public Response<SvaV2ResponseData> addSva(SvaV2Request request) {
        Response<SvaV2ResponseData> data = new Response<>();
        data =readSVACalculadora(request);
        logger.info("Fin ReadOffers2 Service: "+data);
        List <SvaV2ResponseDataSva> listSva = validarSVA(data.getResponseData().getSva());
        data.getResponseData().setSva(listSva);
        data.getResponseData().setOffLine(true);
        //response = cargaSvaV2(data.getResponseData().getSva(), request);
        return data;// AGREGAR
    }
}
