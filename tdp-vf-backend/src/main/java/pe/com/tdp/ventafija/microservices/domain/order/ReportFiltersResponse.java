package pe.com.tdp.ventafija.microservices.domain.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import pe.com.tdp.ventafija.microservices.domain.Dates;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportFiltersResponse {

    @JsonProperty("socio")
    private List<Dates> socio;
    @JsonProperty("campania")
    private List<Dates> campania;
    @JsonProperty("opComercial")
    private List<Dates> opComercial;

    @JsonProperty("canal")
    private List<Dates> canal;
    @JsonProperty("region")
    private List<Dates> region;
    @JsonProperty("zonal")
    private List<Dates> zonal;
    @JsonProperty("ptoVenta")
    private List<Dates> ptoVenta;

    @JsonProperty("fechaInicio")
    private String fechaInicio;
    @JsonProperty("fechaFinal")
    private String fechaFinal;


    public List<Dates> getSocio() {
        return socio;
    }

    public void setSocio(List<Dates> socio) {
        this.socio = socio;
    }

    public List<Dates> getCampania() {
        return campania;
    }

    public void setCampania(List<Dates> campania) {
        this.campania = campania;
    }

    public List<Dates> getOpComercial() {
        return opComercial;
    }

    public void setOpComercial(List<Dates> opComercial) {
        this.opComercial = opComercial;
    }

    public List<Dates> getCanal() {
        return canal;
    }

    public void setCanal(List<Dates> canal) {
        this.canal = canal;
    }

    public List<Dates> getRegion() {
        return region;
    }

    public void setRegion(List<Dates> region) {
        this.region = region;
    }

    public List<Dates> getZonal() {
        return zonal;
    }

    public void setZonal(List<Dates> zonal) {
        this.zonal = zonal;
    }

    public List<Dates> getPtoVenta() {
        return ptoVenta;
    }

    public void setPtoVenta(List<Dates> ptoVenta) {
        this.ptoVenta = ptoVenta;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }
}
