package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.com.tdp.ventafija.microservices.domain.product.entity.TdpChannels;

import java.util.List;

public interface TdpChannelsRepository extends JpaRepository<TdpChannels, Integer> {
    List<TdpChannels> findByChannelatis(String channelatis);
}
