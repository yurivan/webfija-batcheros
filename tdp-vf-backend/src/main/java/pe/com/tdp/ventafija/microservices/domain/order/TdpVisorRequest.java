package pe.com.tdp.ventafija.microservices.domain.order;

public class TdpVisorRequest {

   private String departamento;
   private String provincia;
   private String distrito;
   private String  tipoDocumento;
   private String dni;
   private String nombreProducto;
   private String operacionComercial;

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public String getOperacionComercial() {
        return operacionComercial;
    }

    public void setOperacionComercial(String operacionComercial) {
        this.operacionComercial = operacionComercial;
    }
}
