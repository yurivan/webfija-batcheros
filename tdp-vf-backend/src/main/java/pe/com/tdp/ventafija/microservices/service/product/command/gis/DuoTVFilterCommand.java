package pe.com.tdp.ventafija.microservices.service.product.command.gis;

import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;

import java.util.Map;

@Component("filterDuoTV")
public class DuoTVFilterCommand implements GisFilterCommand {
    // Filtro para DUO TV (5)
    //private static String DUOTV_INTERGPON_HFC_FILTER =    " (OFERTA.targetData = '5' AND CAT.tvTech = '%s' AND CAT.internetTech = 'No Aplica')";
    private static String DUOTV_INTERGPON_HFC_FILTER =    " (OFERTA.targetData = '5' AND CAT.tvTech = '%s' AND CAT.internetTech = '%s')";
    private static String DUOTV_INTERADSL_FILTER =        " (OFERTA.targetData = '5' AND CAT.tvTech = '%s' AND CAT.internetTech = 'ADSL')";
    private static String DUOTV_DEFAULT_FILTER =          " (OFERTA.targetData = '5' AND CAT.tvTech = '%s' AND CAT.internetTech = 'No Aplica')";

    @Override
    public void execute(WSSIGFFTT_FFTT_AVEResult resultGis, Map<String, String> variables, StringBuilder queryBuilder, StringBuilder columnsBuilder) {
        String hasTecnologiaGPON = resultGis.getGPON_TEC();
        String velocidadGPON = resultGis.getGPON_VEL();

        String hasTecnologiaHFC = resultGis.getHFC_TEC();
        String velocidadHFC = resultGis.getHFC_VEL();

        String hasTecnologiaADSL = resultGis.getXDSL_TEC();
        String velocidadADSL = resultGis.getXDSL_VEL_TRM();
        String hasTecnologiaADSLSaturado = resultGis.getXDSL_SAT();

        String hasTecnologiaCoaxial = resultGis.getCOAXIAL_TEC();

        if ("SI".equals(hasTecnologiaGPON)) {
            String filtro = String.format(DUOTV_INTERGPON_HFC_FILTER, "CATV", "FTTH");
            queryBuilder.append(filtro + " OR ");
        }else if ("SI".equals(hasTecnologiaHFC)) {
            String filtro = String.format(DUOTV_INTERGPON_HFC_FILTER, "CATV", "HFC");
            queryBuilder.append(filtro + " OR ");
        }else if ("SI".equals(hasTecnologiaADSL)) {
            // Para el caso de ADSL la tecnologia de television depende de la tecnologia coaxial (COAXIAL_TEC)
            // Si es que COAXIAL_TEC = 'SI' entonces tecnologia television = 'CATV'
            // Si es que COAXIAL_TEC = 'NO' entonces tecnologia television = 'DTH'
            String tecnologiaTelevision = "SI".equals(hasTecnologiaCoaxial) ? "CATV" : "DTH";
            String filtro = String.format(DUOTV_INTERADSL_FILTER, tecnologiaTelevision);
            queryBuilder.append(filtro + " OR ");
        }
        // DUOS TV (5): Si es que no hay tecnologia GPON ni HFC ni ADSL buscamos prooductos dependiendo de la existencia de tecnologia COAXIAL
        if ("NO".equals(hasTecnologiaGPON) && "NO".equals(hasTecnologiaHFC) && "NO".equals(hasTecnologiaADSL)) {
            // Filtramos por tecnologia internet = 'No Aplica'

            // Determinamos la tecnologia television basados en la tecnologia coaxial
            if ("SI".equals(hasTecnologiaCoaxial)) {
                String filtro = String.format(DUOTV_DEFAULT_FILTER, "CATV");
                queryBuilder.append(filtro + " OR ");
            } else if ("NO".equals(hasTecnologiaCoaxial)) {
                String filtro = String.format(DUOTV_DEFAULT_FILTER, "DTH");
                queryBuilder.append(filtro + " OR ");
            }
        }
    }
}
