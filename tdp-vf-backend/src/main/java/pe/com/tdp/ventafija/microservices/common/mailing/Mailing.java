package pe.com.tdp.ventafija.microservices.common.mailing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.constants.mailing.MailingConstants;

@Component
public class Mailing {

    @Autowired
    MailingConstants constants;

    String emailToken = "";

    public EmailResponseBody sendMailExpress(String email, String message, String subject){
        emailToken = getToken();

        EmailRequestBody request = new EmailRequestBody();
        request.setEmail(email);
        request.setActionId(constants.getEmailSendExpressActionId());
        request.setMessage(message);
        request.setToken(emailToken);
        request.setSubject(subject);

        EmailClient<EmailRequestBody, EmailResponseBody> emailSendExpress = new EmailClient<>(constants.getEmailSendExpressUri());
        ClientResult<EmailResponseBody> sendExpressResponse = emailSendExpress.sendRequest(request);
        return sendExpressResponse.getResult();
    }

    private String getToken(){
        if(emailToken.equals("")) emailToken = authenticate();

        Boolean result = checkConection(emailToken);
        if(!result) emailToken = authenticate();
        return emailToken;
    }

    private String authenticate(){
        AuthenticateRequestBody authenticateRequest = new AuthenticateRequestBody();
        authenticateRequest.setUser(constants.getEmailUser());
        authenticateRequest.setPass(constants.getEmailPassword());
        authenticateRequest.setToken(constants.getEmailToken());

        EmailClient<AuthenticateRequestBody, EmailResponseBody> emailAuthenticate = new EmailClient<>(constants.getEmailAutenticateUri());
        ClientResult<EmailResponseBody> authenticateResponse = emailAuthenticate.sendRequest(authenticateRequest);

        String tokenToSend = authenticateResponse.getResult().getToken();
        return tokenToSend;
    }

    private Boolean checkConection(String tokenToSend){
        CheckConexionRequestBody checkConexionRequest = new CheckConexionRequestBody();
        checkConexionRequest.setToken(tokenToSend);

        EmailClient<CheckConexionRequestBody, EmailResponseBody> emailCheckConexion = new EmailClient<>(constants.getEmailCheckConexionUri());
        ClientResult<EmailResponseBody> checkConexionResponse = emailCheckConexion.sendRequest(checkConexionRequest);

        EmailResponseBody checkConexionResult = checkConexionResponse.getResult();
        return (checkConexionResult != null) ? checkConexionResult.getResult() : false;
    }



}

