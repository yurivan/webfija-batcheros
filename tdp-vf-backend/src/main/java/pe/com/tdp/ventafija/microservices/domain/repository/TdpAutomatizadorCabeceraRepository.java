package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpAutomatizadorCabecera;

import java.util.List;

public interface TdpAutomatizadorCabeceraRepository extends JpaRepository<TdpAutomatizadorCabecera, Integer> {

    List<TdpAutomatizadorCabecera> findAllByDepartamento(String departamento);
}
