package pe.com.tdp.ventafija.microservices.domain.order.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class WebOrderDto {
    private String userId;
    private String department;
    private String province;
    private String district;
    private String address;
    private Double latitudeInstallation;
    private Double longitudeInstallation;
    private String addressAdditionalInfo;
    private String equipmentName;
    private String phone;
    private List<String> sva;
    private Customer customer;
    private Product product;
    private String type;
    private String cancelDescription;

    /* Condiciones de Venta */
    private boolean shippingContractsForEmail;
    private boolean affiliationElectronicInvoice;
    private boolean disaffiliationWhitePagesGuide;
    private boolean publicationWhitePagesGuide;
    private boolean affiliationDataProtection;
    private boolean parentalProtection;
    private boolean automaticDebit;
    private String datetimeSalesCondition;
    /* Condiciones de Venta */

    private String originOrder;

    // Sprint 6 Agregar Origen de venta
    private String codigoAplicacion;

    // Sprint 7 Agregar id de venta
    private String orderId;

    // Sprint 9 - Se adiciona campo para diferenciar por producto al cual pertenece el SVA (solo flujo SVA)
    private String sourceProductName;

    // Sprint 10
    private String campaniaSeleccionada;

    // Sprint 5 - propiedades para el punto de venta: nombrePuntoVenta y entidad
    private String nombrePuntoVenta;
    private String entidad;

    // Automatizador NORMALIZADOR
    private String address_referencia;
    private String address_ubigeoGeocodificado;
    private String address_descripcionUbigeo;
    private String address_direccionGeocodificada;
    private String address_tipoVia;
    private String address_nombreVia;
    private String address_numeroPuerta1;
    private String address_numeroPuerta2;
    private String address_cuadra;
    private String address_tipoInterior;
    private String address_numeroInterior;
    private String address_piso;
    private String address_tipoVivienda;
    private String address_nombreVivienda;
    private String address_tipoUrbanizacion;
    private String address_nombreUrbanizacion;
    private String address_manzana;
    private String address_lote;
    private String address_kilometro;
    private String address_nivelConfianza;

    // Anthony - Sprint 15
    private String serviceType;
    private String cmsServiceCode;
    private String cmsCustomer;
    private Integer whatsapp;

    private Boolean flagFtth;
    private Offline offline;



    //Sprint 27 - tipificacion dochoaro
    private String tipificacion;

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }


    public String getCmsServiceCode() {
        return cmsServiceCode;
    }

    public void setCmsServiceCode(String cmsServiceCode) {
        this.cmsServiceCode = cmsServiceCode;
    }

    public String getSourceProductName() {
        return sourceProductName;
    }

    public String getCmsCustomer() {
        return cmsCustomer;
    }

    public void setCmsCustomer(String cmsCustomer) {
        this.cmsCustomer = cmsCustomer;
    }

    public String getSourceProductoName() {
        return sourceProductName;
    }

    public void setSourceProductName(String sourceProductName) {
        this.sourceProductName = sourceProductName;
    }

    public String getCodigoAplicacion() {
        return codigoAplicacion;
    }

    public void setCodigoAplicacion(String codigoAplicacion) {
        this.codigoAplicacion = codigoAplicacion;
    }

    public String getOriginOrder() {
        return originOrder;
    }

    public void setOriginOrder(String originOrder) {
        this.originOrder = originOrder;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLatitudeInstallation() {
        return latitudeInstallation;
    }

    public void setLatitudeInstallation(Double latitudeInstallation) {
        this.latitudeInstallation = latitudeInstallation;
    }

    public Double getLongitudeInstallation() {
        return longitudeInstallation;
    }

    public void setLongitudeInstallation(Double longitudeInstallation) {
        this.longitudeInstallation = longitudeInstallation;
    }

    public String getAddressAdditionalInfo() {
        return addressAdditionalInfo;
    }

    public void setAddressAdditionalInfo(String addressAdditionalInfo) {
        this.addressAdditionalInfo = addressAdditionalInfo;
    }

    public List<String> getSva() {
        return sva;
    }

    public void setSva(List<String> sva) {
        this.sva = sva;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getCampaniaSeleccionada() {
        return campaniaSeleccionada;
    }

    public void setCampaniaSeleccionada(String campaniaSeleccionada) {
        this.campaniaSeleccionada = campaniaSeleccionada;
    }

    public Offline getOffline() {
        return offline;
    }

    public void setOffline(Offline offline) {
        this.offline = offline;
    }

    public static class Product {
        private String id;
        private String expertCode;
        private String productName;
        private String productType;
        private String paymentMethod;
        private String cashPrice;
        private String campaign;
        private String productCategory;
        private String price;
        private String productCode;

        // Automatizador GIS
        private String msx_cbr_voi_ges_in;
        private String msx_cbr_voi_gis_in;
        private String msx_ind_snl_gis_cd;
        private String msx_ind_gpo_gis_cd;
        private String cod_ind_sen_cms;
        private String cod_cab_cms;
        private String cod_fac_tec_cd;

        //Sprint 15 - Anthony

        private String parkType;
        private String subscriber;
        private String serviceCode;

        private String cobre_blq_vta;
        private String cobre_blq_trm;


        public Product() {
        }

        public String getCampaign() {
            return campaign;
        }

        public void setCampaign(String campaign) {
            this.campaign = campaign;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getExpertCode() {
            return expertCode;
        }

        public void setExpertCode(String expertCode) {
            this.expertCode = expertCode;
        }

        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductType() {
            return productType;
        }

        public void setProductType(String productType) {
            this.productType = productType;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getCashPrice() {
            return cashPrice;
        }

        public void setCashPrice(String cashPrice) {
            this.cashPrice = cashPrice;
        }

        public String getProductCategory() {
            return productCategory;
        }

        public void setProductCategory(String productCategory) {
            this.productCategory = productCategory;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getProductCode() {
            return productCode;
        }

        public void setProductCode(String productCode) {
            this.productCode = productCode;
        }

        public String getMsx_cbr_voi_ges_in() {
            return msx_cbr_voi_ges_in;
        }

        public void setMsx_cbr_voi_ges_in(String msx_cbr_voi_ges_in) {
            this.msx_cbr_voi_ges_in = msx_cbr_voi_ges_in;
        }

        public String getMsx_cbr_voi_gis_in() {
            return msx_cbr_voi_gis_in;
        }

        public void setMsx_cbr_voi_gis_in(String msx_cbr_voi_gis_in) {
            this.msx_cbr_voi_gis_in = msx_cbr_voi_gis_in;
        }

        public String getMsx_ind_snl_gis_cd() {
            return msx_ind_snl_gis_cd;
        }

        public void setMsx_ind_snl_gis_cd(String msx_ind_snl_gis_cd) {
            this.msx_ind_snl_gis_cd = msx_ind_snl_gis_cd;
        }

        public String getMsx_ind_gpo_gis_cd() {
            return msx_ind_gpo_gis_cd;
        }

        public void setMsx_ind_gpo_gis_cd(String msx_ind_gpo_gis_cd) {
            this.msx_ind_gpo_gis_cd = msx_ind_gpo_gis_cd;
        }

        public String getCod_ind_sen_cms() {
            return cod_ind_sen_cms;
        }

        public void setCod_ind_sen_cms(String cod_ind_sen_cms) {
            this.cod_ind_sen_cms = cod_ind_sen_cms;
        }

        public String getCod_cab_cms() {
            return cod_cab_cms;
        }

        public void setCod_cab_cms(String cod_cab_cms) {
            this.cod_cab_cms = cod_cab_cms;
        }

        public String getCod_fac_tec_cd() {
            return cod_fac_tec_cd;
        }

        public void setCod_fac_tec_cd(String cod_fac_tec_cd) {
            this.cod_fac_tec_cd = cod_fac_tec_cd;
        }

        public String getParkType() {
            return parkType;
        }

        public void setParkType(String parkType) {
            this.parkType = parkType;
        }

        public String getSubscriber() {
            return subscriber;
        }

        public void setSubscriber(String subscriber) {
            this.subscriber = subscriber;
        }

        public String getServiceCode() {
            return serviceCode;
        }

        public void setServiceCode(String serviceCode) {
            this.serviceCode = serviceCode;
        }

        public String getCobre_blq_vta() {
            return cobre_blq_vta;
        }

        public void setCobre_blq_vta(String cobre_blq_vta) {
            this.cobre_blq_vta = cobre_blq_vta;
        }

        public String getCobre_blq_trm() {
            return cobre_blq_trm;
        }

        public void setCobre_blq_trm(String cobre_blq_trm) {
            this.cobre_blq_trm = cobre_blq_trm;
        }
    }

    public boolean isDisaffiliationWhitePagesGuide() {
        return disaffiliationWhitePagesGuide;
    }

    public void setDisaffiliationWhitePagesGuide(boolean disaffiliationWhitePagesGuide) {
        this.disaffiliationWhitePagesGuide = disaffiliationWhitePagesGuide;
    }

    public boolean isAffiliationElectronicInvoice() {
        return affiliationElectronicInvoice;
    }

    public void setAffiliationElectronicInvoice(boolean affiliationElectronicInvoice) {
        this.affiliationElectronicInvoice = affiliationElectronicInvoice;
    }

    public boolean isAffiliationDataProtection() {
        return affiliationDataProtection;
    }

    public void setAffiliationDataProtection(boolean affiliationDataProtection) {
        this.affiliationDataProtection = affiliationDataProtection;
    }

    public boolean isPublicationWhitePagesGuide() {
        return publicationWhitePagesGuide;
    }

    public void setPublicationWhitePagesGuide(boolean publicationWhitePagesGuide) {
        this.publicationWhitePagesGuide = publicationWhitePagesGuide;
    }

    public boolean isShippingContractsForEmail() {
        return shippingContractsForEmail;
    }

    public void setShippingContractsForEmail(boolean shippingContractsForEmail) {
        this.shippingContractsForEmail = shippingContractsForEmail;
    }

    public String getCancelDescription() {
        return cancelDescription;
    }

    public void setCancelDescription(String cancelDescription) {
        this.cancelDescription = cancelDescription;
    }

    public class Customer {
        private String documentNumber;
        private String documentType;
        private String lastName1;
        private String lastName2;
        private String firstName;
        private String email;
        private String mobilePhone;
        private String telephone;

        // Sprint 7... propiedades adicionales: birthDate, nationality
        private Date birthDate;
        private String nationality;

        //Sprint 24 RUC
        private String tipoDocumentoRrll;
        private String numeroDocumentoRrll;
        private String nombreCompletoRrll;


        public String getDocumentNumber() {
            return documentNumber;
        }

        public void setDocumentNumber(String documentNumber) {
            this.documentNumber = documentNumber;
        }

        public String getDocumentType() {
            return documentType;
        }

        public void setDocumentType(String documentType) {
            this.documentType = documentType;
        }

        public String getLastName1() {
            return lastName1;
        }

        public void setLastName1(String lastName1) {
            this.lastName1 = lastName1;
        }

        public String getLastName2() {
            return lastName2;
        }

        public void setLastName2(String lastName2) {
            this.lastName2 = lastName2;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobilePhone() {
            return mobilePhone;
        }

        public void setMobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
        }

        public String getTelephone() {
            return telephone;
        }

        public void setTelephone(String telephone) {
            this.telephone = telephone;
        }

        public Date getBirthDate() {
            return birthDate;
        }

        public void setBirthDate(Date birthDate) {
            this.birthDate = birthDate;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }

        //Sprint 24 RUC
        public String getTipoDocumentoRrll() {
            return tipoDocumentoRrll;
        }
        public void setTipoDocumentoRrll(String tipoDocumentoRrll) {
            this.tipoDocumentoRrll = tipoDocumentoRrll;
        }

        public String getNumeroDocumentoRrll() {
            return numeroDocumentoRrll;
        }
        public void setNumeroDocumentoRrll(String numeroDocumentoRrll) {
            this.numeroDocumentoRrll = numeroDocumentoRrll;
        }

        public String getNombreCompletoRrll() {
            return nombreCompletoRrll;
        }
        public void setNombreCompletoRrll(String nombreCompletoRrll) {
            this.nombreCompletoRrll = nombreCompletoRrll;
        }
    }

    public String getNombrePuntoVenta() {
        return nombrePuntoVenta;
    }

    public void setNombrePuntoVenta(String nombrePuntoVenta) {
        this.nombrePuntoVenta = nombrePuntoVenta;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public boolean isParentalProtection() {
        return parentalProtection;
    }

    public void setParentalProtection(boolean parentalProtection) {
        this.parentalProtection = parentalProtection;
    }

    public boolean isAutomaticDebit() {
        return automaticDebit;
    }

    public void setAutomaticDebit(boolean automaticDebit) {
        this.automaticDebit = automaticDebit;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAddress_referencia() {
        return address_referencia;
    }

    public void setAddress_referencia(String address_referencia) {
        this.address_referencia = address_referencia;
    }

    public String getAddress_ubigeoGeocodificado() {
        return address_ubigeoGeocodificado;
    }

    public void setAddress_ubigeoGeocodificado(String address_ubigeoGeocodificado) {
        this.address_ubigeoGeocodificado = address_ubigeoGeocodificado;
    }

    public String getAddress_descripcionUbigeo() {
        return address_descripcionUbigeo;
    }

    public void setAddress_descripcionUbigeo(String address_descripcionUbigeo) {
        this.address_descripcionUbigeo = address_descripcionUbigeo;
    }

    public String getAddress_direccionGeocodificada() {
        return address_direccionGeocodificada;
    }

    public void setAddress_direccionGeocodificada(String address_direccionGeocodificada) {
        this.address_direccionGeocodificada = address_direccionGeocodificada;
    }

    public String getAddress_tipoVia() {
        return address_tipoVia;
    }

    public void setAddress_tipoVia(String address_tipoVia) {
        this.address_tipoVia = address_tipoVia;
    }

    public String getAddress_nombreVia() {
        return address_nombreVia;
    }

    public void setAddress_nombreVia(String address_nombreVia) {
        this.address_nombreVia = address_nombreVia;
    }

    public String getAddress_numeroPuerta1() {
        return address_numeroPuerta1;
    }

    public void setAddress_numeroPuerta1(String address_numeroPuerta1) {
        this.address_numeroPuerta1 = address_numeroPuerta1;
    }

    public String getAddress_numeroPuerta2() {
        return address_numeroPuerta2;
    }

    public void setAddress_numeroPuerta2(String address_numeroPuerta2) {
        this.address_numeroPuerta2 = address_numeroPuerta2;
    }

    public String getAddress_cuadra() {
        return address_cuadra;
    }

    public void setAddress_cuadra(String address_cuadra) {
        this.address_cuadra = address_cuadra;
    }

    public String getAddress_tipoInterior() {
        return address_tipoInterior;
    }

    public void setAddress_tipoInterior(String address_tipoInterior) {
        this.address_tipoInterior = address_tipoInterior;
    }

    public String getAddress_numeroInterior() {
        return address_numeroInterior;
    }

    public void setAddress_numeroInterior(String address_numeroInterior) {
        this.address_numeroInterior = address_numeroInterior;
    }

    public String getAddress_piso() {
        return address_piso;
    }

    public void setAddress_piso(String address_piso) {
        this.address_piso = address_piso;
    }

    public String getAddress_tipoVivienda() {
        return address_tipoVivienda;
    }

    public void setAddress_tipoVivienda(String address_tipoVivienda) {
        this.address_tipoVivienda = address_tipoVivienda;
    }

    public String getAddress_nombreVivienda() {
        return address_nombreVivienda;
    }

    public void setAddress_nombreVivienda(String address_nombreVivienda) {
        this.address_nombreVivienda = address_nombreVivienda;
    }

    public String getAddress_tipoUrbanizacion() {
        return address_tipoUrbanizacion;
    }

    public void setAddress_tipoUrbanizacion(String address_tipoUrbanizacion) {
        this.address_tipoUrbanizacion = address_tipoUrbanizacion;
    }

    public String getAddress_nombreUrbanizacion() {
        return address_nombreUrbanizacion;
    }

    public void setAddress_nombreUrbanizacion(String address_nombreUrbanizacion) {
        this.address_nombreUrbanizacion = address_nombreUrbanizacion;
    }

    public String getAddress_manzana() {
        return address_manzana;
    }

    public void setAddress_manzana(String address_manzana) {
        this.address_manzana = address_manzana;
    }

    public String getAddress_lote() {
        return address_lote;
    }

    public void setAddress_lote(String address_lote) {
        this.address_lote = address_lote;
    }

    public String getAddress_kilometro() {
        return address_kilometro;
    }

    public void setAddress_kilometro(String address_kilometro) {
        this.address_kilometro = address_kilometro;
    }

    public String getAddress_nivelConfianza() {
        return address_nivelConfianza;
    }

    public void setAddress_nivelConfianza(String address_nivelConfianza) {
        this.address_nivelConfianza = address_nivelConfianza;
    }

    public String getDatetimeSalesCondition() {
        return datetimeSalesCondition;
    }

    public void setDatetimeSalesCondition(String datetimeSalesCondition) {
        this.datetimeSalesCondition = datetimeSalesCondition;
    }

    public Integer getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(Integer whatsapp) {
        this.whatsapp = whatsapp;
    }
    public Boolean getFlagFtth() {
        return this.flagFtth;
    }

    public void setFlagFtth(Boolean flagFtth) {
        this.flagFtth = flagFtth;
    }


    public class Offline {

        private String flag;
        private HashMap<String,String> body;

        public  String getFlag(){
            return this.flag;
        }
        public  void  setFlag(String flag){
            this.flag = flag;
        }
        public HashMap<String,String> getBody(){
            return this.body;
        }
        public  void  setBody(HashMap<String,String> body){
            this.body = body;
        }

        @Override
        public String toString(){
            String texto = "flag : " + this.flag;
            Iterator<String> it = this.getBody().keySet().iterator();
            while (it.hasNext()){
                String key = it.next();
                texto = texto + "; "+ key + " : " + this.getBody().get(key);
            }
            return texto;
        }
    }
    public String getTipificacion() { return tipificacion; }

    public void setTipificacion(String tipificacion) { this.tipificacion = tipificacion; }
}


