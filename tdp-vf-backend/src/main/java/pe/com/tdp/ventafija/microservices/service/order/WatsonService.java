package pe.com.tdp.ventafija.microservices.service.order;

import com.ibm.watson.developer_cloud.http.HttpMediaType;
import com.ibm.watson.developer_cloud.http.ServiceCall;
import com.ibm.watson.developer_cloud.speech_to_text.v1.SpeechToText;
import com.ibm.watson.developer_cloud.speech_to_text.v1.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.services.config.ApplicationConfiguration;


import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Service
public class WatsonService {
    private static final Logger logger = LogManager.getLogger(WatsonService.class);
    @Autowired
    private ApplicationConfiguration applicationConfiguration;

    public static final String AUDIO_WAV = "audio/wav";

    /**
     * request Watson service to process the audio and return the transcript
     *
     * @param audio       to be analyzed
     * @param contentType of the audio
     * @return null when not able to process, the transcript otherwise
     */
    public String processAudio(File audio, String contentType) {
        logger.info("Inicio ProcessAudio Service.");
        String result = null;
        logger.info("ProcessAudio => audio: " + audio);
        logger.info("ProcessAudio => contentType: " + contentType);
        logger.info("ProcessAudio => user: " + applicationConfiguration.getWatsonUsername());
        logger.info("ProcessAudio => password: " + applicationConfiguration.getWatsonPassword());
        logger.info("ProcessAudio => model: " + applicationConfiguration.getWatsonModel());

        try {
            if (audio != null) {
                logger.info("ProcessAudio => audio.length(): " + audio.length());

                Map<String, String> headers = new HashMap<String, String>();
                headers.put("X-Watson-Learning-Opt-Out", "true");

                SpeechToText service = new SpeechToText();
                service.setUsernameAndPassword(applicationConfiguration.getWatsonUsername(), applicationConfiguration.getWatsonPassword());
                service.setDefaultHeaders(headers);
                //File audio = new File("src/test/resources/sample1.wav");

                // Sprint 5 - Se adiciona el model (language) en el builder
                RecognizeOptions options = new RecognizeOptions.Builder()
                        .contentType(HttpMediaType.AUDIO_WAV).model(applicationConfiguration.getWatsonModel())
                        .build();

                SpeechResults transcript = service.recognize(audio, options).execute();
                //System.out.println(transcript);
                result = transcript.toString();
				/*
				SpeechToText service = new SpeechToText();
				service.setEndPoint("https://stream.watsonplatform.net/speech-to-text/api");
				service.setUsernameAndPassword(applicationConfiguration.getWatsonUsername(), applicationConfiguration.getWatsonPassword());
				RecognizeOptions options = new RecognizeOptions.Builder().contentType(contentType).model(applicationConfiguration.getWatsonModel()).build();

				ServiceCall<SpeechResults> serviceCall = service.recognize(audio, options);
				SpeechResults speechResults = serviceCall.execute();
				StringBuilder transcript = new StringBuilder();
				
				if (speechResults != null) {
					for (Transcript t : speechResults.getResults()) {
						for (SpeechAlternative alt : t.getAlternatives()) {
							transcript.append(alt.getTranscript());
						}
					}
				}
				
				result = transcript.toString();
				*/


            }
        } catch (Exception e) {
            logger.error("Error ProcessAudio service.", e);
        }
        logger.info("Fin ProcessAudio Service.");
        return result;
    }
}
