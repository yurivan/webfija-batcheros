package pe.com.tdp.ventafija.microservices.domain.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "tdp_catalog", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema)
public class TdpCatalog {

    @Id
    @Column(name = "id")
    public Integer id;
    @Column(name = "commercialoperation")
    private String commercialOperation;
    @Column(name = "campaign")
    private String campaign;
    @Column(name = "campaingcode")
    private String campaingCode;
    @Column(name = "priority")
    private Integer priority;
    @Column(name = "prodtypecode")
    private String prodTypeCode;
    @Column(name = "producttype")
    private String productType;
    @Column(name = "prodcategorycode")
    private String prodCategoryCode;
    @Column(name = "productcategory")
    private String productCategory;
    @Column(name = "productcode")
    private String productCode;
    @Column(name = "productname")
    private String productName;
    @Column(name = "bloquetv")
    private String productBloqueTv;
    @Column(name = "svalinea")
    private String productSvaLinea;
    @Column(name = "svainternet")
    private String productSvaInternet;
    @Column(name = "tiporegistro")
    private String tipoRegistro;
    @Column(name = "discount")
    private String discount;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "promprice")
    private BigDecimal promPrice;
    @Column(name = "monthperiod")
    private Integer monthPeriod;
    @Column(name = "installcost")
    private BigDecimal installCost;
    @Column(name = "linetype")
    private String lineType;
    @Column(name = "paymentmethod")
    private String paymentMethod;
    @Column(name = "cashprice")
    private BigDecimal cashPrice;
    @Column(name = "financingcost")
    private BigDecimal financingCost;
    @Column(name = "financingmonth")
    private Integer financingMonth;
    @Column(name = "equipmenttype")
    private String equipmentType;
    @Column(name = "returnmonth")
    private Integer returnMonth;
    @Column(name = "returnperiod")
    private String returnPeriod;
    @Column(name = "internetspeed")
    private Integer internetSpeed;
    @Column(name = "promspeed")
    private Integer promoSpeed;
    @Column(name = "periodpromspeed")
    private Integer periodoPromoSpeed;
    @Column(name = "internettech")
    private String internetTech;
    @Column(name = "tvsignal")
    private String tvSignal;
    @Column(name = "tvtech")
    private String tvTech;
    @Column(name = "equiplinea")
    private String equipamientoLinea;
    @Column(name = "equipinternet")
    private String equipamientoInternet;
    @Column(name = "equiptv")
    private String equipamientoTv;
    @Column(name = "product_codigo")
    private String productCodigo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCommercialOperation() {
        return commercialOperation;
    }

    public void setCommercialOperation(String commercialOperation) {
        this.commercialOperation = commercialOperation;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getCampaingCode() {
        return campaingCode;
    }

    public void setCampaingCode(String campaingCode) {
        this.campaingCode = campaingCode;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getProdTypeCode() {
        return prodTypeCode;
    }

    public void setProdTypeCode(String prodTypeCode) {
        this.prodTypeCode = prodTypeCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProdCategoryCode() {
        return prodCategoryCode;
    }

    public void setProdCategoryCode(String prodCategoryCode) {
        this.prodCategoryCode = prodCategoryCode;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductBloqueTv() {
        return productBloqueTv;
    }

    public void setProductBloqueTv(String productBloqueTv) {
        this.productBloqueTv = productBloqueTv;
    }

    public String getProductSvaLinea() {
        return productSvaLinea;
    }

    public void setProductSvaLinea(String productSvaLinea) {
        this.productSvaLinea = productSvaLinea;
    }

    public String getProductSvaInternet() {
        return productSvaInternet;
    }

    public void setProductSvaInternet(String productSvaInternet) {
        this.productSvaInternet = productSvaInternet;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPromPrice() {
        return promPrice;
    }

    public void setPromPrice(BigDecimal promPrice) {
        this.promPrice = promPrice;
    }

    public Integer getMonthPeriod() {
        return monthPeriod;
    }

    public void setMonthPeriod(Integer monthPeriod) {
        this.monthPeriod = monthPeriod;
    }

    public BigDecimal getInstallCost() {
        return installCost;
    }

    public void setInstallCost(BigDecimal installCost) {
        this.installCost = installCost;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public BigDecimal getCashPrice() {
        return cashPrice;
    }

    public void setCashPrice(BigDecimal cashPrice) {
        this.cashPrice = cashPrice;
    }

    public BigDecimal getFinancingCost() {
        return financingCost;
    }

    public String getTipoRegistro() {
        return tipoRegistro;
    }

    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public void setFinancingCost(BigDecimal financingCost) {
        this.financingCost = financingCost;
    }

    public Integer getFinancingMonth() {
        return financingMonth;
    }

    public void setFinancingMonth(Integer financingMonth) {
        this.financingMonth = financingMonth;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public Integer getReturnMonth() {
        return returnMonth;
    }

    public void setReturnMonth(Integer returnMonth) {
        this.returnMonth = returnMonth;
    }

    public String getReturnPeriod() {
        return returnPeriod;
    }

    public void setReturnPeriod(String returnPeriod) {
        this.returnPeriod = returnPeriod;
    }

    public Integer getInternetSpeed() {
        return internetSpeed;
    }

    public void setInternetSpeed(Integer internetSpeed) {
        this.internetSpeed = internetSpeed;
    }

    public Integer getPromoSpeed() {
        return promoSpeed;
    }

    public void setPromoSpeed(Integer promoSpeed) {
        this.promoSpeed = promoSpeed;
    }

    public Integer getPeriodoPromoSpeed() {
        return periodoPromoSpeed;
    }

    public void setPeriodoPromoSpeed(Integer periodoPromoSpeed) {
        this.periodoPromoSpeed = periodoPromoSpeed;
    }

    public String getInternetTech() {
        return internetTech;
    }

    public void setInternetTech(String internetTech) {
        this.internetTech = internetTech;
    }

    public String getTvSignal() {
        return tvSignal;
    }

    public void setTvSignal(String tvSignal) {
        this.tvSignal = tvSignal;
    }

    public String getTvTech() {
        return tvTech;
    }

    public void setTvTech(String tvTech) {
        this.tvTech = tvTech;
    }

    public String getEquipamientoLinea() {
        return equipamientoLinea;
    }

    public void setEquipamientoLinea(String equipamientoLinea) {
        this.equipamientoLinea = equipamientoLinea;
    }

    public String getEquipamientoInternet() {
        return equipamientoInternet;
    }

    public void setEquipamientoInternet(String equipamientoInternet) {
        this.equipamientoInternet = equipamientoInternet;
    }

    public String getEquipamientoTv() {
        return equipamientoTv;
    }

    public void setEquipamientoTv(String equipamientoTv) {
        this.equipamientoTv = equipamientoTv;
    }

    public String getProductCodigo() {
        return productCodigo;
    }

    public void setProductCodigo(String productCodigo) {
        this.productCodigo = productCodigo;
    }
}