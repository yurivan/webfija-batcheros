package pe.com.tdp.ventafija.microservices.domain.customer;

public class ContractResponseParameter {
    private String category;
    private String element;
    private String strValue;
    private String domain;
    private String campo;
    private int auxiliar;
    private String strfilter;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(String strValue) {
        this.strValue = strValue;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public int getAuxiliar() {
        return auxiliar;
    }

    public void setAuxiliar(int auxiliar) {
        this.auxiliar = auxiliar;
    }

    public String getStrfilter() {
        return strfilter;
    }

    public void setStrfilter(String strfilter) {
        this.strfilter = strfilter;
    }
}
