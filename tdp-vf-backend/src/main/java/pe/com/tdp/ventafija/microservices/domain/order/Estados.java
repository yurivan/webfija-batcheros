package pe.com.tdp.ventafija.microservices.domain.order;

import java.util.List;

public class Estados {
    private String nombre;
    private String color;
    private Integer cantidad;
    private List<OrderVisor> lista;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public List<OrderVisor> getLista() {
        return lista;
    }

    public void setLista(List<OrderVisor> lista) {
        this.lista = lista;
    }
}
