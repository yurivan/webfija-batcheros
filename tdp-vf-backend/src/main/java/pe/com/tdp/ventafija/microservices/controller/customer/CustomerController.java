package pe.com.tdp.ventafija.microservices.controller.customer;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Date;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import pe.com.tdp.ventafija.microservices.common.clients.customer.*;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.domain.message.MessageEntities;
import pe.com.tdp.ventafija.microservices.common.services.CustomerDataService;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.MessageUtil;
import pe.com.tdp.ventafija.microservices.common.util.RandomString;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.customer.*;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.CustomerScoringRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.CustomerScoringResponse;
import pe.com.tdp.ventafija.microservices.domain.repository.CustomerRepository;
import pe.com.tdp.ventafija.microservices.service.customer.CustomerService;

@RestController
public class CustomerController {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private CustomerService customerService;

    @Autowired
    private MessageUtil messageUtil;

    //Mijail
    //private CustomerDataService customerMetodo;
    @Autowired
    private CustomerDataService customerMetodo;

    //Sprint24 RUC
    @Autowired
    private CustomerRepository customerRepository;

    @Value("${product.mensajes.oferta.ErrorExperto}")
    private String mensajeOfertaErrorExperto;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/contract", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ContractResponseData> contract(@RequestBody ContractRequest request) {
        LogVass.startController(logger, "Contract", request);
        Response<ContractResponseData> response = new Response<>();

        try {
            response = customerService.readContract(request);
        } catch (Exception e) {
            logger.error("Error Contract Controller", e);
        }
        LogVass.finishController(logger, "Contract", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/web/contract", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ContractWebResponseData> contractWeb(@RequestBody ContractWebRequest request) {
        LogVass.startController(logger, "contractWeb", request);
        Response<ContractWebResponseData> response = new Response<>();

        try {
            response = customerService.readContractWeb(request);
        } catch (Exception e) {
            logger.error("Error contractWeb Controller", e);
        }
        LogVass.finishController(logger, "contractWeb", response);

        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = {"/customer/web/services"}, method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<ServicesWebResponseData>> servicesWeb(@RequestBody ServicesRequest request) {
        LogVass.startController(logger, "ServicesWeb", request);

        Response<List<ServicesWebResponseData>> response = new Response<>();
        try {
            response = customerService.readServicesWeb(request);
        } catch (Exception e) {
            logger.error("Error ServicesWeb Controller", e);
        }
        LogVass.finishController(logger, "ServicesWeb", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/getInfo", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<GetInfoResponseData> getInfo(@RequestBody GetInfoRequest request) {
        LogVass.startController(logger, "GetInfo", request);
        Response<GetInfoResponseData> response = new Response<>();

        try {
            response = customerService.getInfo(request);
        } catch (Exception e) {
            logger.error("Error getInfo Controller", e);
        }

        LogVass.finishController(logger, "GetInfo", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/validateCategoryActive", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> validateCategoryActive(@RequestBody DuplicateForCategoryRequest request) {
        LogVass.startController(logger, "ValidateCategoryActive", request);

        Response<String> response = new Response<>();
        try {
            response = customerService.validateDuplicateForCategory(request);
        } catch (Exception e) {
            logger.error("Error ValidateCategoryActive Controller. ", e);
        }
        LogVass.finishController(logger, "ValidateCategoryActive", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/orderid", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> getOrderId(){
        Response<String> response =new Response<>();
        RandomString randomString = new RandomString(19);
        String orderId = "-" + randomString.nextString();
        response.setResponseMessage(orderId);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/scoring", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<CustomerScoringResponse> scoring(@RequestBody CustomerScoringRequest request) {
        LogVass.startController(logger, "Scoring", request);
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("EXPERTO");
        LogVass.serviceResponseHashMap(logger, "GetExperto", "messageList", messageList);
        //End--Code for call Service

        Response<CustomerScoringResponse> response = new Response<>();
        try {
            response = customerService.scoring(request);
        } catch (Exception e) {
            logger.error("Error Scoring Controller.", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
        }
        LogVass.finishController(logger, "Scoring", response);
        return response;
    }

    // Sprint 6
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/web/salecondition", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ContractSalesConditionResponseData> getSaleCondition(@RequestBody ContractWebRequest request) {
        LogVass.startController(logger, "getSaleCondition", request);
        Response<ContractSalesConditionResponseData> response = new Response<>();

        try {
            response = customerService.readSaleConditions(request);
        } catch (Exception e) {
            logger.error("Error contractWeb Controller", e);
        }
        LogVass.finishController(logger, "getSaleCondition", response);

        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/web/contactos", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Integer> actualizarContactos(@RequestBody ContractWebRequestCustomer request) {
        LogVass.startController(logger, "actualizarContactos", request);
        Response<Integer> response = new Response<>();

        try {
            response = customerService.actualizaContactos(request);
        } catch (Exception e) {
            logger.error("Error contactosWeb Controller", e);
        }
        LogVass.finishController(logger, "actualizarContactos", response);

        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/web/updateTdpOrder", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Integer> updateContactTdpOrder(@RequestBody UpdTelfTdpOrderResponseData request) {
        LogVass.startController(logger, "updateContactTdpOrder", request);
        Response<Integer> response = new Response<>();

        try {
            response = customerService.updateContactTdpOrder(request);
        } catch (Exception e) {
            logger.error("Error updateContactTdpOrder Controller", e);
        }
        LogVass.finishController(logger, "updateContactTdpOrder", response);

        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = {"/customer/out/web/services"}, method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<ServicesWebResponseData>> outServicesWeb(@RequestBody OutServicesRequest request) {
        LogVass.startController(logger, "ServicesOutWeb", request);

        Response<List<ServicesWebResponseData>> response = new Response<>();
        try {
            response = customerService.readOutServicesWeb(request);
        } catch (Exception e) {
            logger.error("Error ServicesWeb Controller", e);
        }
        LogVass.finishController(logger, "ServicesOutWeb", response);
        return response;
    }

    //Sprint 24
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "customer/getCustomerDue", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<CustomerDueResponseData> consultaDataCustomer(@RequestBody CustomerDataRequestBody request) {
        LogVass.startController(logger, "consultaDataCustomer", request);
        //logger.info("login basic auth");
        Response<CustomerDueResponseData> response = new Response<>();
        CustomerLineRequestBody codCliRequest = new CustomerLineRequestBody();
        List<CustomerDueResponseData> dataResponseList = new ArrayList<>();

        CustomerDueResponseData customerDue =  new CustomerDueResponseData();
        SimpleDateFormat f =  new SimpleDateFormat("yyyy-MM-dd");
        Double deudaGeneral = 0.0;
        Integer contLineas = 0;
        Date ultFecha = new Date();
        Date ultFechaEmi = new Date();
        Date fecAct = new Date();

        String errorMsg = "",cuentas = "";

        String servicioActivo=customerMetodo.getEstadoServicio();

        if(servicioActivo.equals("true")){
            String[] topeDeuda = customerMetodo.getTopeDeuda().split(",");
            customerDue.setTopeDeudaAlta(topeDeuda[0]);
            customerDue.setTopeDeudaPark(topeDeuda[1]);
            CustomerDataResponseBody customerResponse;
            try{
                customerResponse = customerMetodo.getConsultaCustomer(request).getBodyOut();
                for(CustomerDetailResponseBody x : customerResponse.getRespuesta() ){
                    if(!x.getCodCli().equals("0") && x.getCodCli() != null){
                        customerDue.setCodCustomer(x.getCodCli());
                        customerDue.setApePatCustomer(x.getPriApeCli().trim());
                        customerDue.setApeMatCustomer(x.getSegApeCli().trim());
                        customerDue.setNombreCustomer(x.getNomCli().trim());
                        customerDue.setTipDocCustomer(x.getTipDoc().trim());
                        customerDue.setNumDocCustomer(x.getNumDocCli().trim());
                        break;
                    }
                }

                if(customerDue.getCodCustomer() != null && !customerDue.getCodCustomer().equals("")){
                    codCliRequest.setCodCliente(customerDue.getCodCustomer());
                    //LogVass.serviceResponse(logger,"Data Request Cliente: ",codCliRequest);
                    CustomerLineResponseBody dataResult = customerMetodo.getLineasCustomer(codCliRequest).getBodyOut();

                    for(CustomerLineDetailResponseBody z: dataResult.getLineas()){

                        if(z.getCodCtaCd().equals("0")){
                            break;
                        }
                        if(z.getCodCtaCd() != null && !z.getCodCtaCd().equals("") && !z.getCodCtaCd().equals("0")){
                            Double deuda = 0.0;
                            CustomerReceiptsRequestBody deudaRequest = new CustomerReceiptsRequestBody();
                            deudaRequest.setCodCuenta(z.getCodCtaCd());
                            deudaRequest.setCodCliente(customerDue.getCodCustomer());

                            //LogVass.serviceResponse(logger,"Data Request Linea: ",deudaRequest);
                            CustomerReceiptsResponseBody deudaResult = customerMetodo.getDeudaCustomer(deudaRequest).getBodyOut();

                            for (CustomerReceiptsDetailResponseBody y: deudaResult.getRecibos()) {

                                if(y.getCodTipDoc() != null && !y.getIndEstDoc().equals("C")){

                                    Date fechaEmi,fechaVenc;
                                    if(y.getIndEstDoc().equals("P")){
                                        try{
                                            fechaEmi = f.parse(y.getFecEmiDoc() == null ? "" : y.getFecEmiDoc());
                                            fechaVenc = f.parse(y.getFecVnc() == null ? "" : y.getFecVnc());

                                            if(fechaVenc.before(fecAct)){
                                                deuda += Double.parseDouble(y.getMtoTotIm() == null ? "0.0" : y.getMtoTotIm());
                                                if(ultFecha.after(fechaVenc)){
                                                    ultFecha = fechaVenc;
                                                }
                                                if(ultFechaEmi.after(fechaEmi)){
                                                    ultFechaEmi = fechaEmi;
                                                }
                                            }

                                        }catch (Exception e){
                                            deuda += Double.parseDouble(y.getMtoTotIm() == null ? "0.0" : y.getMtoTotIm());
                                        }
                                    }
                                }
                                else{
                                    break;
                                }

                            }

                            if(deuda > 0){
                                deudaGeneral += deuda;
                                contLineas++;
                                if(cuentas.isEmpty()){
                                    cuentas = z.getCodCtaCd();
                                }
                                else{
                                    cuentas += ',' + z.getCodCtaCd();
                                }
                            }

                        }

                    }

                    customerDue.setDeudaCuenta(deudaGeneral.toString());
                    customerDue.setTotLineas(contLineas.toString());
                    customerDue.setCuentaCustomer(cuentas);
                    customerDue.setFecReciboVencidoUlt(ultFecha.before(fecAct) ? f.format(ultFecha) : "");
                    customerDue.setFecReciboEmitidoUlt((ultFechaEmi.before(fecAct) ? f.format(ultFechaEmi) : ""));
                    response.setResponseMessage(contLineas > 0 ? "SUCCESS" : "SIN_DEUDA");
                    response.setResponseCode("0");
                    response.setResponseData(customerDue);
                }
                else{
                    response.setResponseCode("-2");
                    response.setResponseMessage("NOT_FOUND");
                }

            }catch (TimeoutException te){
                response.setResponseMessage(customerMetodo.getMessageError() + "|" + "ERROR : " + te.getMessage());
                response.setResponseCode("-1");
                //logger.error("Error ServicesWebDeuda Controller", e.getStackTrace());
                return response;
            }
            catch (Exception e){
                response.setResponseMessage(customerMetodo.getMessageError() + "|" + "ERROR : " + e.getMessage());
                response.setResponseCode("-1");
                //logger.error("Error ServicesWebDeuda Controller", e.getStackTrace());
                return response;
            }

        }
        else if(servicioActivo.equals("false")) {
            response.setResponseCode("-3");
            response.setResponseMessage("SERVICE_INACTIVED");
            response.setResponseData(null);
        }
        LogVass.finishController(logger, "consultaDataCustomer", response);
        return response;
    }

    //Sprint 24 RUC
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "customer/consultaRuc", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<CustomerRucResponseData> consultaRuc(@RequestBody ConsultaRucDataRequestBody request){
        LogVass.startController(logger, "ServiceValidaRuc", request);
        ConsultaRucDataResponseBody consultaRuc = new ConsultaRucDataResponseBody();
        Response<CustomerRucResponseData> response = new Response<>();
        ApiResponse<ConsultaRucDataResponseBody> consultaRucResponseBody;
        CustomerRucResponseData consultaRucData = new CustomerRucResponseData();
        String consultaRucSwitch = consultaParametro("tdp.api.consultaruc.activo");
        try{
            if(!consultaRucSwitch.equals("") && consultaRucSwitch.equals("false")){
                consultaRuc = proccessRucData(request.getNumDoc());
            }else{
                for (Integer cont = 1; cont < 4; cont ++){
                    consultaRucResponseBody = customerMetodo.getDataRuc(request);
                    if(cont == 3){
                        String msjOffline = consultaParametro("tdp.api.consultaruc.msjoffline");
                        response.setResponseCode("-2");
                        response.setResponseMessage("FLUJO_OFFLINE|" + msjOffline);
                        return response;
                    }
                    if(consultaRucResponseBody != null && consultaRucResponseBody.getBodyOut() != null && !consultaRucResponseBody.getBodyOut().getNumero().equals("")){
                        consultaRuc = consultaRucResponseBody.getBodyOut();
                        break;
                    }
                    }
            }            
            if(consultaRuc != null && consultaRuc.getNumero() != null){
                String domicilio = "";

                consultaRucData.setRazonSocial(consultaRuc.getRazSocial());
                consultaRucData.setUbigeo(consultaRuc.getUbigeo());
                consultaRucData.setCondDomicilio(consultaRuc.getConDomicilio());
                consultaRucData.setEstContrib(consultaRuc.getEstContribuyente());

                if(!consultaRuc.getTipVia().equals("-")){domicilio += consultaRuc.getTipVia() + " ";}
                if(!consultaRuc.getNomVia().equals("-") && !consultaRuc.getTipVia().equals("-")){domicilio += consultaRuc.getNomVia() + " ";}
                if(!consultaRuc.getCodZona().equals("-")){domicilio += consultaRuc.getCodZona() + " ";}
                if(!consultaRuc.getTipZona().equals("-") && !consultaRuc.getCodZona().equals("-")){domicilio += consultaRuc.getTipZona() + " ";}
                if(!consultaRuc.getNumero().equals("-")){domicilio += "Nro. " + consultaRuc.getNumero() + " ";}
                if(!consultaRuc.getInterior().equals("-")){domicilio += "Int. " + consultaRuc.getInterior() + " ";}
                if(!consultaRuc.getDepartamento().equals("-")){domicilio += "Dpto. " + consultaRuc.getDepartamento() + " ";}
                if(!consultaRuc.getManzana().equals("-")){domicilio += "Mz. " + consultaRuc.getManzana() + " ";}
                if(!consultaRuc.getLote().equals("-")){domicilio += "Lt. " + consultaRuc.getLote() + " ";}
                domicilio = domicilio.trim();

                consultaRucData.setDireccion(domicilio);
                response.setResponseData(consultaRucData);
                response.setResponseCode("0");
                response.setResponseMessage("OK");
            }else{
                response.setResponseCode("-1");
                response.setResponseMessage("ERROR CONSULTA RUC");
            }

        }catch (Exception e){
            response.setResponseCode("-1");
            response.setResponseMessage("ERROR");
            return response;
        }
        LogVass.finishController(logger, "ServiceValidaRuc", response);
        return response;
    }

    //Sprint 24 RUC
    //@CrossOrigin(origins = "*")
    //@RequestMapping(value = "customer/getParametro", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public String consultaParametro(String parametro){
        //Response<String> response = new Response<>();
        /*response.setResponseData(customerRepository.getValorParametro(parametro));
        response.setResponseCode("0");
        response.setResponseMessage("");*/
        String response = customerRepository.getValorParametro(parametro);

        return response;
    }

    private ConsultaRucDataResponseBody proccessRucData(String nroRuc){
        ConsultaRucDataResponseBody objRucResponse = new ConsultaRucDataResponseBody();
        List<Object[]> objListaRuc = new ArrayList<>();

        objListaRuc = customerRepository.getDataRuc(nroRuc);

        if(objListaRuc != null && objListaRuc.size() > 0){
            for (Object[] row : objListaRuc) {
                objRucResponse.setNumero(row[0] == null ? "" : row[0].toString());
                objRucResponse.setRazSocial(row[1] == null ? "" : row[1].toString());
                objRucResponse.setEstContribuyente(row[2] == null ? "" : row[2].toString());
                objRucResponse.setConDomicilio(row[3] == null ? "" : row[3].toString());
                objRucResponse.setUbigeo(row[4] == null ? "" : row[4].toString());
                objRucResponse.setTipVia(row[5] == null ? "" : row[5].toString());
                objRucResponse.setNomVia(row[6] == null ? "" : row[6].toString());
                objRucResponse.setCodZona(row[7] == null ? "" : row[7].toString());
                objRucResponse.setTipZona(row[8] == null ? "" : row[8].toString());
                objRucResponse.setNumero(row[9] == null ? "" : row[9].toString());
                objRucResponse.setInterior(row[10] == null ? "" : row[10].toString());
                objRucResponse.setLote(row[11] == null ? "" : row[11].toString());
                objRucResponse.setDepartamento(row[12] == null ? "" : row[12].toString());
                objRucResponse.setManzana(row[13] == null ? "" : row[13].toString());
                objRucResponse.setKilometro(row[14] == null ? "" : row[14].toString());
                break;
            }
            
        }

        if(objRucResponse.getNumero() != null){
            return objRucResponse;
        }else {
            return null;
        }


    }
}
