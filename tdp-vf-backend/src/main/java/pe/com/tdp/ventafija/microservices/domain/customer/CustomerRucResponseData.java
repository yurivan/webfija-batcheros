package pe.com.tdp.ventafija.microservices.domain.customer;

public class CustomerRucResponseData {

    private String razonSocial;
    private String estContrib;
    private String condDomicilio;
    private String ubigeo;
    private String direccion;

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getEstContrib() {
        return estContrib;
    }

    public void setEstContrib(String estContrib) {
        this.estContrib = estContrib;
    }

    public String getCondDomicilio() {
        return condDomicilio;
    }

    public void setCondDomicilio(String condDomicilio) {
        this.condDomicilio = condDomicilio;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
}
