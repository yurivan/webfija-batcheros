package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpCatalogOut;

import java.util.List;

public interface TdpCatalogOutRepository extends JpaRepository<TdpCatalogOut, Integer> {
    @Query(value = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_catalog_out WHERE tipo_documento = ?1 AND numero_documento = ?2 AND tx_otros1 ilike 'Segunda%' AND nom_prod_oferta not similar to '%(VALIDAR GIS|SOLO OFRECER MONO DTH)%'  LIMIT 1", nativeQuery = true)
    List<TdpCatalogOut> findAllByTipoDocumentoAndDni(String tipoDocumento, String dni);
}
