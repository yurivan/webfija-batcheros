package pe.com.tdp.ventafija.microservices.controller.product;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.common.clients.gis.OffersApiGisRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.gis.OffersGisResponse;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.agendamiento.AgendamientRequest;
import pe.com.tdp.ventafija.microservices.domain.order.OutRequestDetalle;
import pe.com.tdp.ventafija.microservices.domain.order.OutResponseDetalle;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpAgendamientoCupones;
import pe.com.tdp.ventafija.microservices.domain.product.*;
import pe.com.tdp.ventafija.microservices.domain.user.ConfigParameterResponseData;
import pe.com.tdp.ventafija.microservices.repository.product.ProductDAO;
import pe.com.tdp.ventafija.microservices.service.product.ProductService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
public class ProductController {

    private static final Logger logger = LogManager.getLogger();

    @Autowired
    private ProductService service;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/complements", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SvaResponseData> complements(@RequestBody SvaRequest request) {
        LogVass.startController(logger, "Complements", request);

        Response<SvaResponseData> response = new Response<>();
        try {
            response = service.readSVA(request);
        } catch (Exception e) {
            logger.error("Error Complements Controller.", e);
        }
        LogVass.finishController(logger, "Complements", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/outcall/complements", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SvaResponseData> outComplements(@RequestBody SvaOutRequest request) {
        LogVass.startController(logger, "outComplements", request);

        Response<SvaResponseData> response = new Response<>();
        try {
            response = service.readOutSVA(request);
        } catch (Exception e) {
            logger.error("Error outComplements Controller.", e);
        }
        LogVass.finishController(logger, "outComplements", response);
        return response;
    }
//strat Agendamiento Svae
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/agendamiento", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response agendamiento(@RequestBody AgendamientRequest request) {
        LogVass.startController(logger, "agendamiento", request);


        Response response = new Response<>();
        try {
            //response = service.readSVA(request);
            response = service.saveAgendamiento(request);
        } catch (Exception e) {
            logger.error("Error agendamiento Controller.", e);
        }
        LogVass.finishController(logger, "agendamiento", response);
        return response;
    }
//fin Agendamiento

    //strat Agendamiento Svae
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/calendario/agendamiento", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response <List<TdpAgendamientoCupones>> calendario() {

        Response <List<TdpAgendamientoCupones>> response = new Response<>();
        try {
             response= service.showCalendario();

        } catch (Exception e) {
            logger.error("Error agendamiento Controller.", e);
        }
        LogVass.finishController(logger, "agendamiento", response);

        return response;
    }
//fin Agendamiento

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/v2/complements", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SvaV2ResponseData> complementsV2(@RequestBody SvaV2Request request) {
        LogVass.startController(logger, "ComplementsV2", request);

        Response<SvaV2ResponseData> response = new Response<>();
        try {
            response = service.readSVAv2(request);
        } catch (Exception e) {
            logger.error("Error ComplementsV2 Controller.", e);
        }
        LogVass.finishController(logger, "ComplementsV2", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/outcall/v2/complements", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SvaV2ResponseData> outComplementsV2(@RequestBody SvaOutV2Request request) {
        LogVass.startController(logger, "ComplementsV2", request);

        Response<SvaV2ResponseData> response = new Response<>();
        try {
            response = service.readOutSVAv2(request);
        } catch (Exception e) {
            logger.error("Error outComplementsV2 Controller.", e);
        }
        LogVass.finishController(logger, "outComplementsV2", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/numdecos", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<ConfigParameterResponseData>> numDecosApp() {
        logger.info("Inicio NumDecosApp Controller");

        Response<List<ConfigParameterResponseData>> response = new Response<>();
        try {
            response = service.numDecos();
        } catch (Exception e) {
            logger.error("Error NumDecos Controller.", e);
        }
        logger.info("Fin NumDecosApp Controller");
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/v2/offering", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<OffersResponseData2>> offering2(@RequestBody OffersRequest2 request) {
        LogVass.startController(logger, "Offering2", request);
        logger.error("SE ENVIO AL OFFERING "+request.getFlagFtth());
        Response<List<OffersResponseData2>> response = new Response<>();
        try {
            response = service.readOffers2(request);
        } catch (Exception e) {
            logger.error("Error Offering2 Controller.", e);
        }
        LogVass.finishController(logger, "Offering2", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/outcall/offering", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<OutOffersResponseData>> outOffering(@RequestBody OutOffersRequest request) {
        LogVass.startController(logger, "Offering2", request);

        Response<List<OutOffersResponseData>> response = new Response<>();
        try {
            response = service.readOutOffers(request);
        } catch (Exception e) {
            logger.error("Error outOffering Controller.", e);
        }
        LogVass.finishController(logger, "Offering2", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/outcall/offeringcapta", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<OutOffersResponseData>> outOfferingCapta(@RequestBody OutOffersRequest request) {
        LogVass.startController(logger, "Offering2", request);

        Response<List<OutOffersResponseData>> response = new Response<>();
        try {
            response = service.readOutOffersCapta(request);
        } catch (Exception e) {
            logger.error("Error outOffering Controller.", e);
        }
        LogVass.finishController(logger, "Offering2", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/outcall/offeringplanta", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<OutOffersResponseData>> outOfferingPlanta(@RequestBody OutOffersPlantaRequest request) {
        LogVass.startController(logger, "Offering2", request);

        Response<List<OutOffersResponseData>> response = new Response<>();
        try {
            response = service.readOutOffersOutPlanta(request);
        } catch (Exception e) {
            logger.error("Error outOffering Controller.", e);
        }
        LogVass.finishController(logger, "Offering2", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/outcall/gis", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<GisResponse> outGis(@RequestBody OffersApiGisRequestBody request) {
        LogVass.startController(logger, "outGis", request);

        Response<GisResponse> response = new Response<>();
        try {
            response = service.readGis(request);
        } catch (Exception e) {
            logger.error("Error outGis Controller.", e);
        }
        LogVass.finishController(logger, "outGis", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/out/gis", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<OffersGisResponse> gisOut(@RequestBody OffersApiGisRequestBody request) {
        LogVass.startController(logger, "gisOut", request);

        Response<OffersGisResponse> response = new Response<>();
        try {
            response = service.readGisOut(request);
        } catch (Exception e) {
            logger.error("Error gisOut Controller.", e);
        }
        LogVass.finishController(logger, "gisOut", response);
        return response;
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/product/add/sva", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SvaV2ResponseData> addSva(@RequestBody SvaV2Request request) {
        LogVass.startController(logger, "ComplementsV2", request);

        Response<SvaV2ResponseData> response = new Response<>();
        try {
            response = service.addSva(request);
        } catch (Exception e) {
            logger.error("Error ComplementsV2 Controller.", e);
        }
        LogVass.finishController(logger, "ComplementsV2", response);
        return response;
    }
}
