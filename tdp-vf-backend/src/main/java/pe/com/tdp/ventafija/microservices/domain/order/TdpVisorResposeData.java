package pe.com.tdp.ventafija.microservices.domain.order;

import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpVisor;

public class TdpVisorResposeData {

    private String IdVisor;
    private String codigoPedido;
    private String cliente;
    private String estadoSolicitud;


    public String getIdVisor() {
        return IdVisor;
    }

    public void setIdVisor(String idVisor) {
        IdVisor = idVisor;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }



    public TdpVisorResposeData cargaDatos(TdpVisor items) {

        this.setIdVisor(items.getIdVisor());
        this.setCodigoPedido(items.getCodigoPedido());
        this.setCliente(items.getIdVisor());
        this.setEstadoSolicitud(items.getIdVisor());

        return this;
    }
}
