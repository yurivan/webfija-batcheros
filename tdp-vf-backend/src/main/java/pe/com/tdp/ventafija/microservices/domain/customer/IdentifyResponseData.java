package pe.com.tdp.ventafija.microservices.domain.customer;

import java.util.List;

public class IdentifyResponseData {

  private String lastName;
  private String motherLastName;
  private String names;
  private String departmentBirthLocation;
  private String provinceBirthLocation;
  private String districtBirthLocation;
  private String birthdate;
  private String fatherName;
  private String motherName;
  private String expeditionDate;
  private String restriction;
  private List<String> motherNameAlternatives;
  private List<String> fatherNameAlternatives;

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getMotherLastName() {
    return motherLastName;
  }

  public void setMotherLastName(String motherLastName) {
    this.motherLastName = motherLastName;
  }

  public String getNames() {
    return names;
  }

  public void setNames(String names) {
    this.names = names;
  }

  public String getDepartmentBirthLocation() {
    return departmentBirthLocation;
  }

  public void setDepartmentBirthLocation(String departmentBirthLocation) {
    this.departmentBirthLocation = departmentBirthLocation;
  }

  public String getProvinceBirthLocation() {
    return provinceBirthLocation;
  }

  public void setProvinceBirthLocation(String provinceBirthLocation) {
    this.provinceBirthLocation = provinceBirthLocation;
  }

  public String getDistrictBirthLocation() {
    return districtBirthLocation;
  }

  public void setDistrictBirthLocation(String districtBirthLocation) {
    this.districtBirthLocation = districtBirthLocation;
  }

  public String getBirthdate() {
    return birthdate;
  }

  public void setBirthdate(String birthdate) {
    this.birthdate = birthdate;
  }

  public String getFatherName() {
    return fatherName;
  }

  public void setFatherName(String fatherName) {
    this.fatherName = fatherName;
  }

  public String getMotherName() {
    return motherName;
  }

  public void setMotherName(String motherName) {
    this.motherName = motherName;
  }

  public String getExpeditionDate() {
    return expeditionDate;
  }

  public void setExpeditionDate(String expeditionDate) {
    this.expeditionDate = expeditionDate;
  }

  public String getRestriction() {
    return restriction;
  }

  public void setRestriction(String restriction) {
    this.restriction = restriction;
  }

public List<String> getMotherNameAlternatives() {
	return motherNameAlternatives;
}

public void setMotherNameAlternatives(List<String> motherNameAlternatives) {
	this.motherNameAlternatives = motherNameAlternatives;
}

public List<String> getFatherNameAlternatives() {
	return fatherNameAlternatives;
}

public void setFatherNameAlternatives(List<String> fatherNameAlternatives) {
	this.fatherNameAlternatives = fatherNameAlternatives;
}

}
