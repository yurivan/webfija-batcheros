package pe.com.tdp.ventafija.microservices.domain.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_catalog_capta_out", schema = "ibmx_a07e6d02edaf552")
public class TdpCatalogOutCapta {
    @Id
    @Column(name = "id")
    public Integer id;
    @Column(name="cod_accion")
    private String cod_accion;
    @Column(name="tx_nombre")
    private String tx_nombre;
    @Column(name="numero_telefono")
    private String numero_telefono;
    @Column(name="tipo_documento")
    private String tipo_documento;
    @Column(name="numero_documento")
    private String numero_documento;
    @Column(name="departamento")
    private String departamento;
    @Column(name="provincia")
    private String provincia;
    @Column(name="distrito")
    private String distrito;
    @Column(name="tx_direccion")
    private String tx_direccion;
    @Column(name="ps_oferta2")
    private String ps_oferta2;

    public String getCod_accion() {
        return cod_accion;
    }

    public void setCod_accion(String cod_accion) {
        this.cod_accion = cod_accion;
    }

    public String getTx_nombre() {
        return tx_nombre;
    }

    public void setTx_nombre(String tx_nombre) {
        this.tx_nombre = tx_nombre;
    }

    public String getNumero_telefono() {
        return numero_telefono;
    }

    public void setNumero_telefono(String numero_telefono) {
        this.numero_telefono = numero_telefono;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getNumero_documento() {
        return numero_documento;
    }

    public void setNumero_documento(String numero_documento) {
        this.numero_documento = numero_documento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getTx_direccion() {
        return tx_direccion;
    }

    public void setTx_direccion(String tx_direccion) {
        this.tx_direccion = tx_direccion;
    }

    public String getPs_oferta2() {
        return ps_oferta2;
    }

    public void setPs_oferta2(String ps_oferta2) {
        this.ps_oferta2 = ps_oferta2;
    }
}

