package pe.com.tdp.ventafija.microservices.domain.order;

public class SalesReportRequest {
	private String vendorId;
	private Integer numDays;
	private String idTransaccion;
	private String filterDni;
	private String status;

	public String getFilterDni() {
		return filterDni;
	}

	public void setFilterDni(String filterDni) {
		this.filterDni = filterDni;
	}

	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public Integer getNumDays() {
		return numDays;
	}

	public void setNumDays(Integer numDays) {
		this.numDays = numDays;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
