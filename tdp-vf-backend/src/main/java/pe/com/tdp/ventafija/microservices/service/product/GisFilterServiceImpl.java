package pe.com.tdp.ventafija.microservices.service.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;
import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.service.product.command.gis.GisFilterCommand;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GisFilterServiceImpl implements GisFilterService {

    @Autowired
    @Qualifier("filterDuoBA")
    private GisFilterCommand filterDuoBA;

    @Autowired
    @Qualifier("filterDuoTV")
    private GisFilterCommand filterDuoTV;

    @Autowired
    @Qualifier("filterMonoBA")
    private GisFilterCommand filterMonoBA;

    @Autowired
    @Qualifier("filterMonoLinea")
    private GisFilterCommand filterMonoLinea;

    @Autowired
    @Qualifier("filterMonoTV")
    private GisFilterCommand filterMonoTV;

    @Autowired
    @Qualifier("filterTrio")
    private GisFilterCommand filterTrio;

    @Autowired
    private ParametersRepository parametersRepository;

    @Override
    public String getFilter(WSSIGFFTT_FFTT_AVEResult resultGis, String price) {
        // Evaluamos el flag de test para ejecutar en modo de pruebas (casuisticas)
        Parameters parameter = parametersRepository.findOneByDomainAndCategoryAndElement("TEST", "PARAMETER", "TESTGIST");
        if ("SI".equals(parameter.getStrValue())) {
            // Entonces ejecutamos en modo test... procedemos a leer las variables response gis de prueba
            loadTestParameters(resultGis);
        }

        cleanResultGis(resultGis);
        StringBuilder queryBuilder = new StringBuilder();
        Map<String, String> variables = new HashMap<String, String>();
        variables.put("PRICE", price);
        filterMonoLinea.execute(resultGis, variables, queryBuilder, null);
        filterTrio.execute(resultGis, variables, queryBuilder, null);
        filterDuoBA.execute(resultGis, variables, queryBuilder, null);
        filterDuoTV.execute(resultGis, variables, queryBuilder, null);
        filterMonoBA.execute(resultGis, variables, queryBuilder, null);
        filterMonoTV.execute(resultGis, variables, queryBuilder, null);



        return queryBuilder.toString();
    }

    private void cleanResultGis(WSSIGFFTT_FFTT_AVEResult resultGis) {
        //Quitamos el texto MB de la velocidad en caso existiera
        String velocidadGpon = resultGis.getGPON_VEL();
        velocidadGpon = cleanVelocidad(velocidadGpon);

        String velocidadHfc = resultGis.getHFC_VEL();
        velocidadHfc = cleanVelocidad(velocidadHfc);

        String velocidadXdsl = resultGis.getXDSL_VEL_TRM();
        velocidadXdsl = cleanVelocidad(velocidadXdsl);

        resultGis.setGPON_VEL(velocidadGpon);
        resultGis.setHFC_VEL(velocidadHfc);
        resultGis.setXDSL_VEL_TRM(velocidadXdsl);

        // Si alguna de las tecnologias GPON_TEC, HFC_TEC, XDSL_TEC, COBRE_TEC, COAXIAL_TEC son NULL entonces las consideramos como un 'NO'
        if (resultGis.getGPON_TEC() == null) {
            resultGis.setGPON_TEC("NO");
        }
        if (resultGis.getHFC_TEC() == null) {
            resultGis.setHFC_TEC("NO");
        }
        if (resultGis.getXDSL_TEC() == null) {
            resultGis.setXDSL_TEC("NO");
        }
        if (resultGis.getCOBRE_TEC() == null) {
            resultGis.setCOBRE_TEC("NO");
        }
        if (resultGis.getCOAXIAL_TEC() == null) {
            resultGis.setCOAXIAL_TEC("NO");
        }

        // Si alguno de los datos COBRE_SAT, XDSL_SAT son nulos entonces los consideramos como un 'SI'
        if (resultGis.getCOBRE_SAT() == null) {
            resultGis.setCOBRE_SAT("SI");
        }
        if (resultGis.getXDSL_SAT() == null) {
            resultGis.setXDSL_SAT("SI");
        }

        // Si alguna de las velocidades GPON_VEL, HFC_VEL, XDSL_VEL_TRM son nulos entonces las consideramos como 0

    }

    private String cleanVelocidad(String velocidad) {
        if (velocidad != null) {
            int indexOf = velocidad.indexOf("MB");
            if (indexOf > -1) {
                velocidad = velocidad.substring(0, indexOf);
            }
        }
        return velocidad;
    }

    private void loadTestParameters(WSSIGFFTT_FFTT_AVEResult resultGis) {
        List<Parameters> parameters = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("TEST", "PARAMETER");
        for (Parameters gisParameter : parameters) {
            if ("COAXIAL_TEC".equals(gisParameter.getElement())) {
                resultGis.setCOAXIAL_TEC(gisParameter.getStrValue());
            } else if ("COBRE_SAT".equals(gisParameter.getElement())) {
                resultGis.setCOBRE_SAT(gisParameter.getStrValue());
            } else if ("COBRE_TEC".equals(gisParameter.getElement())) {
                resultGis.setCOBRE_TEC(gisParameter.getStrValue());
            } else if ("GPON_TEC".equals(gisParameter.getElement())) {
                resultGis.setGPON_TEC(gisParameter.getStrValue());
            } else if ("GPON_VEL".equals(gisParameter.getElement())) {
                resultGis.setGPON_VEL(gisParameter.getStrValue());
            } else if ("HFC_TEC".equals(gisParameter.getElement())) {
                resultGis.setHFC_TEC(gisParameter.getStrValue());
            } else if ("HFC_VEL".equals(gisParameter.getElement())) {
                resultGis.setHFC_VEL(gisParameter.getStrValue());
            } else if ("HFC_VEL".equals(gisParameter.getElement())) {
                resultGis.setHFC_VEL(gisParameter.getStrValue());
            } else if ("XDSL_SAT".equals(gisParameter.getElement())) {
                resultGis.setXDSL_SAT(gisParameter.getStrValue());
            } else if ("XDSL_TEC".equals(gisParameter.getElement())) {
                resultGis.setXDSL_TEC(gisParameter.getStrValue());
            } else if ("XDSL_VEL_TRM".equals(gisParameter.getElement())) {
                resultGis.setXDSL_VEL_TRM(gisParameter.getStrValue());
            }
        }
    }
}
