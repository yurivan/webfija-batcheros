package pe.com.tdp.ventafija.microservices.domain.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecuperoRequest {

    @JsonProperty("orderId")
    private String orderId;
    @JsonProperty("motivoCaida")
    private String motivoCaida;
    @JsonProperty("fechaPago")
    private Date fechaPago;
    @JsonProperty("direccion")
    private String direccion;
    @JsonProperty("telefono")
    private String telefono;
    @JsonProperty("dvr")
    private int svaDvr;
    @JsonProperty("hd")
    private int svaHd;
    @JsonProperty("smart")
    private int svaSmart;
    @JsonProperty("dni")
    private String docnumber;

    @JsonProperty("orderId")
    public String getOrderId() {
        return orderId;
    }

    @JsonProperty("orderId")
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @JsonProperty("motivoCaida")
    public String getMotivoCaida() {
        return motivoCaida;
    }

    @JsonProperty("motivoCaida")
    public void setMotivoCaida(String motivoCaida) {
        this.motivoCaida = motivoCaida;
    }

    @JsonProperty("fechaPago")
    public Date getFechaPago() {
        return fechaPago;
    }

    @JsonProperty("fechaPago")
    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    @JsonProperty("direccion")
    public String getDireccion() {
        return direccion;
    }

    @JsonProperty("direccion")
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @JsonProperty("telefono")
    public String getTelefono() {
        return telefono;
    }

    @JsonProperty("telefono")
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @JsonProperty("dvr")
    public int getSvaDvr() {
        return svaDvr;
    }

    @JsonProperty("dvr")
    public void setSvaDvr(int svaDvr) {
        this.svaDvr = svaDvr;
    }

    @JsonProperty("hd")
    public int getSvaHd() {
        return svaHd;
    }

    @JsonProperty("hd")
    public void setSvaHd(int svaHd) {
        this.svaHd = svaHd;
    }

    @JsonProperty("smart")
    public int getSvaSmart() {
        return svaSmart;
    }

    @JsonProperty("smart")
    public void setSvaSmart(int svaSmart) {
        this.svaSmart = svaSmart;
    }

    @JsonProperty("dni")
    public String getDocnumber() {
        return docnumber;
    }

    @JsonProperty("dni")
    public void setDocnumber(String docnumber) {
        this.docnumber = docnumber;
    }
}
