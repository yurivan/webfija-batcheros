package pe.com.tdp.ventafija.microservices.domain.profile;

public class ParameterResponse {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
