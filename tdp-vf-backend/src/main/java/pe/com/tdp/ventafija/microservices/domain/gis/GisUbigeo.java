package pe.com.tdp.ventafija.microservices.domain.gis;

public class GisUbigeo {
    private String departamento;
    private String provincia;
    private String distrito;
    private String latitud;
    private String longitud;
    private int flag_update;
    private String id;

    private int metros;



    public GisUbigeo() {
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public int getFlag_update() {
        return flag_update;
    }

    public void setFlag_update(int flag_update) {
        this.flag_update = flag_update;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMetros() {
        return metros;
    }

    public void setMetros(int metros) {
        this.metros = metros;
    }
}
