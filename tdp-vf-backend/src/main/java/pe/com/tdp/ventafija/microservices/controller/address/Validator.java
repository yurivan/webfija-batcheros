package pe.com.tdp.ventafija.microservices.controller.address;

import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.domain.address.ConfigAddressRequest;
import pe.com.tdp.ventafija.microservices.domain.address.ConfigAddressResponse;

@Service
public class Validator {

    public ConfigAddressResponse validarAddressConfig(ConfigAddressRequest request) {
        ConfigAddressResponse data= new ConfigAddressResponse();
        String msj="";
        if (request.getAddress_calle_numero().length() <= 6){
            msj="Address_calle_numero excede en la longitud permitida / ";
        }
        if (request.getAddress_calle_atis().length() <= 3){
            msj=msj+"Address_calle_atis excede en la longitud permitida ";
        }
        data.setMensaje(msj);
        return data;
    }
}
