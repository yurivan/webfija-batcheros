package pe.com.tdp.ventafija.microservices.controller.order;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.CachedIntrospectionResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.constant.CustomerConstants;
import pe.com.tdp.ventafija.microservices.common.domain.dto.HDECResponse;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Order;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.exception.ApplicationException;
import pe.com.tdp.ventafija.microservices.common.exception.DataValidationException;
import pe.com.tdp.ventafija.microservices.common.mailing.EmailResponseBody;
import pe.com.tdp.ventafija.microservices.common.mailing.Mailing;
import pe.com.tdp.ventafija.microservices.common.services.HDECRetryService;
import pe.com.tdp.ventafija.microservices.common.services.HDECService;
import pe.com.tdp.ventafija.microservices.common.util.DateUtils;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.MessageUtil;
import pe.com.tdp.ventafija.microservices.common.util.OrderConstants;
import pe.com.tdp.ventafija.microservices.constants.mailing.MailingConstants;
import pe.com.tdp.ventafija.microservices.constants.user.UserConstants;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipRequest;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipResponse;
import pe.com.tdp.ventafija.microservices.domain.order.*;
import pe.com.tdp.ventafija.microservices.domain.order.dto.WebOrderDto;
import pe.com.tdp.ventafija.microservices.domain.order.entity.PeticionPagoEfectivo;
import pe.com.tdp.ventafija.microservices.repository.address.AddressDao;
import pe.com.tdp.ventafija.microservices.service.automatizador.AutomatizadorService;
import pe.com.tdp.ventafija.microservices.service.order.CargaAudioMasivaService;
import pe.com.tdp.ventafija.microservices.service.order.OrderService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeoutException;


@RestController
@RequestMapping(value = "/order/web")
public class OrderWebController {
    private static final Logger logger = LogManager.getLogger(OrderWebController.class);
    @Autowired
    private OrderService orderService;
    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private CargaAudioMasivaService cargaAudioMasivaService;

    // Sprint 12 - refactorizando codigo
    @Autowired
    private HDECService hdecService;

    @Autowired
    private HDECRetryService hdecRetryService;

    @Autowired
    private AutomatizadorService automatizadorService;

    @Autowired
    private MessageUtil messageUtil;

    @Autowired
    Mailing mailing;

    @Autowired
    MailingConstants mailingConstants;

    @Autowired
    private AddressDao addressDao;

    @Value("${order.mensajes.guardar.NoSePuedeRegistrar}")
    private String mensajeGuardarNoSePuedeRegistrar;
    @Value("${order.mensajes.registrarPreVenta.ErrorHdec}")
    private String mensajeRegistrarPreVentaErrorHdec;
    @Value("${order.mensajes.save.errorGenerico}")
    private String mensajeSaveErrorGenerico;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Map<String, String>> saveOrder(@RequestBody WebOrderDto saveOrderRequest, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        Response<Map<String, String>> response = new Response<>();
        logger.info("info saveOrder", saveOrderRequest);
        // se esta aumentando 1 dia a la fech nacimiento pq se estaba asignando con 1 dia de retraso
        saveOrderRequest.getCustomer().setBirthDate(DateUtils.addDays(saveOrderRequest.getCustomer().getBirthDate(), 1));
        // Sprint 6 - Añadir Codigo de Aplicación
        response = responseCIP(saveOrderRequest, codigoAplicacion);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/get", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Map<String, String>> createCIP(@RequestBody WebOrderDto saveOrderRequest, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        Response<Map<String, String>> response = new Response<>();
        logger.info("info saveOrderRequest", saveOrderRequest);
        // Sprint 6 - Añadir Codigo de Aplicación
        response = responseCIP(saveOrderRequest, codigoAplicacion);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/savepending", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Map<String, String>> savePendingOrder(@RequestBody WebOrderDto saveOrderRequest, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        Response<Map<String, String>> response = new Response<>();
        logger.info("info savePendingOrder", saveOrderRequest);

        String responseCode = CustomerConstants.DUPLICATE_RESPONSE_CODE_OK;
        Map<String, String> responseData = new HashMap<>();
        String responseMessage = "";

        try {
            orderService.savePending(saveOrderRequest);
            responseData.put("resultado", "ok");
            responseMessage = "ok";
        } catch (Exception e) {
            logger.error("Error savePendingOrder", e);
            responseCode = CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR;
            responseMessage = mensajeSaveErrorGenerico;
        }
        response.setResponseCode(responseCode);
        response.setResponseData(responseData);
        response.setResponseMessage(responseMessage);
        return response;
    }

    public Response<Map<String, String>> responseCIP(WebOrderDto saveOrderRequest, String codigoAplicacion) {
        Response<Map<String, String>> response = new Response<>();
        String responseCode = CustomerConstants.DUPLICATE_RESPONSE_CODE_OK;
        Map<String, String> responseData = new HashMap<>();
        String responseMessage = "";
        String orderId = "";
        Boolean validadorAutomatizador = false;
        try {
            Order order = orderService.save(saveOrderRequest, codigoAplicacion);
            orderId = order.getId();

            // Sprint 4 - Implementacion de requerimiento UPFRONT - si es que la venta requiere pago adelantado entonces ya no se envia la venta a TGESTIONA (HDEC)
            // La venta sera enviada a TGESTIONA cuando el comercio notifique que la venta ya ha sido pagada...
            // Ahora solo las ventas que sean financiadas seran enviadas a TGESTIONA automaticamente al cerrar la venta
            // Pendiente pasar a constantes el status...
            String visorStatus = orderService.getVisorStatus(order);
            // 20180129... El envio de la venta a HDEC ahora sera en la subida de audio...
            // Los vendedores con perfil TIENDA no suben audio... por lo tanto enviamos directamente a HDEC
            /*if ("PENDIENTE".equals(visorStatus)) {
                orderService.sendDirectToHDEC(orderId);
			}*/

            boolean esPerfilTienda = "PRESENCIAL".equals(saveOrderRequest.getTipificacion());
            boolean isOffline = false;
            final boolean  witherror ;
            String flag = saveOrderRequest.getOffline().getFlag();

            if (flag!=null){
                if(flag.contains("1") || flag.contains("6") || flag.contains("7")){
                    isOffline = true;
                }
            }

            System.out.println("el flag es "+isOffline);

            //Adicionar cip rest - dochoaro - generar cip
            String cip = "";
            GenerarCipResponse cipResponse = new GenerarCipResponse();
            GenerarCipRequest cipRequest = new GenerarCipRequest();
            PeticionPagoEfectivo ppe = orderService.registrarPeticionCIP(orderId);
            if (ppe != null) {
                cipRequest.setCurrency("PEN");
                cipRequest.setAmount(ppe.getTotal());
                cipRequest.setTransactionCode(saveOrderRequest.getOrderId());
                cipRequest.setAdminEmail("lincoln.morales@telefonica.com");
                cipRequest.setDateExpiry(parseStringBornDmY(ppe.getFechaAExpirar()));
                cipRequest.setPaymentConcept("Movistar");
                cipRequest.setAdditionalData("Generando online");
                cipRequest.setUserEmail(saveOrderRequest.getCustomer().getEmail() != null || saveOrderRequest.getCustomer().getEmail() != "" ? saveOrderRequest.getCustomer().getEmail() : "Nulo@nulo.com");
                cipRequest.setUserName(saveOrderRequest.getCustomer().getFirstName());
                cipRequest.setUserLastName(saveOrderRequest.getCustomer().getLastName1() + " " + saveOrderRequest.getCustomer().getLastName2());
                cipRequest.setUserUbigeo("150115");
                cipRequest.setUserCountry("PERU");
                String documenttype;
                switch (saveOrderRequest.getCustomer().getDocumentNumber()) {
                    case "DNI":
                        documenttype = "DNI";
                        break;
                    case "CEX":
                        documenttype = "NAN";
                        break;
                    case "PAS":
                        documenttype = "PAS";
                        break;
                    case "RUC":
                        documenttype = "NAN";
                        break;
                    case "Otros":
                        documenttype = "NAN";
                        break;
                    default:
                        documenttype = "NAN";
                        break;
                }

                cipRequest.setUserDocumentType(documenttype);
                cipRequest.setUserDocumentNumber(saveOrderRequest.getCustomer().getDocumentNumber());
                cipRequest.setUserCodeCountry("+51");
                cipRequest.setUserPhone(saveOrderRequest.getCustomer().getMobilePhone() != null || saveOrderRequest.getCustomer().getMobilePhone() != "" ? saveOrderRequest.getCustomer().getMobilePhone() : "999999999");
                String token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJNT1YiLCJqdGkiOiIxYjUxYmJjYi0wYmQ5LTQwMmUtYTRlOC1lYWIwNTQ2N2I3N2MiLCJuYW1laWQiOiIxMDQiLCJleHAiOjE1ODAwMTIxMDF9.2TVKKT-X7jFiWx-_7DJUZBMYkknrsNY1WX1tqipubAw";
                cipResponse = orderService.generarCIP(cipRequest, token);
                cip = cipResponse != null ? cipResponse.getData().getCip() + "" : "ERROR_CIP";

                if(saveOrderRequest.getType() != null &&
                        (saveOrderRequest.getType().equalsIgnoreCase("A") ||
                                saveOrderRequest.getType().equalsIgnoreCase("S"))) {

                    if(cip!= null){
                        boolean success = automatizadorService.preRegisterSaleWeb2(order.getId(), saveOrderRequest.getType());
                        logger.error("El envío a Automatizador es: ", success);
                    }

                }

                //cip = orderService.genCIP(id);
                //cip = cip == null ? "ERROR_CIP" : cip;
            }

            if(isOffline || (cip!= null && cip.equalsIgnoreCase("ERROR_CIP"))) {
                visorStatus = "OFFLINE";
            }else{
                if ("PENDIENTE".equals(visorStatus) && esPerfilTienda) {
                    List<String> automatizadorAux = automatizadorService.automatizadorExtraValidacion();
                   // int automatizadorSegmentacion = Integer.parseInt(automatizadorAux.get(0));

                    String[] segmentacionCanalSplit = automatizadorAux.get(1).split(",");
                    boolean automatizadorSegmentacionSwitch = Boolean.parseBoolean(automatizadorAux.get(2));

                    if(saveOrderRequest.getType() != null &&
                            (saveOrderRequest.getType().equalsIgnoreCase("A") || saveOrderRequest.getType().equalsIgnoreCase("S")
                                    || saveOrderRequest.getType().equalsIgnoreCase("M"))){
                        String type = "";
                        if(saveOrderRequest.getType().equalsIgnoreCase("A")) type = OrderConstants.ALTA_PURA;
                        else if(saveOrderRequest.getType().equalsIgnoreCase("S")) type = OrderConstants.ALTA_SVA;
                        else type = OrderConstants.MIGRACION;

                        //Enviar a automatizador
                        boolean success = automatizadorService.preRegisterSaleWeb(order.getId(), type);

                    if(!success){
                        Integer duplicado = addressDao._consultarDuplicado(order.getId());


                        if(duplicado.equals(0)){
                            HDECResponse hdecResponse = hdecRetryService.sendToHDECWithRetry(orderId);
                            if (HDECResponse.ERROR_CONTROLADO.equals(hdecResponse)) {
                                visorStatus = "ENVIANDO";
                            }
                        }
                         //consultar duplicado en automatizador
                        if(duplicado.equals(1)){
                          logger.error("Error Duplicado en Automatizador", duplicado);
                        }

                        if(duplicado.equals(2)){
                            logger.error("Error de Bus", duplicado);
                            visorStatus = "ENVIANDO_AUTOMATIZADOR";

                        }

                    }else{
                        automatizadorService.setOrderInAutomatizador(order.getId());
                        validadorAutomatizador = true;
                    }

                }else{
                    HDECResponse hdecResponse = hdecRetryService.sendToHDECWithRetry(orderId);
                    if (HDECResponse.ERROR_CONTROLADO.equals(hdecResponse)) {
                        visorStatus = "ENVIANDO";
                    }
                }

                    // Sprint 11 - Si es que ocurre un error (codigo 4002 o 4003) al enviar a HDEC entonces dejamos continuar el flujo de la venta con estado "ENVIANDO"
                /*try {
                    // Sprint 12 - reutilizamos metodo hdec de HDECService
                    hdecService.sendDirectToHDEC(orderId);
                    //orderService.sendDirectToHDEC(orderId);
                } catch (ClientException e) {
                    String serviceResponse = e.getMessage();
                    //if (serviceResponse != null && (serviceResponse.contains(HDECConstants.ERROR_4002) || serviceResponse.contains(HDECConstants.ERROR_4003) || serviceResponse.contains(HDECConstants.ERROR_500))) {
                    if (serviceResponse != null && serviceResponse.contains(HDECConstants.ERROR_4003)) {
                        visorStatus = "ENVIANDO";
                    } else {
                        throw e;
                    }
                }*/
                }
            }

            final String visorStatusFinal = visorStatus;
            String orderIdFinal = orderId;
            //Runnable task = () -> {

            if (esPerfilTienda){
                orderService.migrateVisor(orderIdFinal, visorStatusFinal.equalsIgnoreCase("PENDIENTE-PAGO") ? "PENDIENTE" : visorStatusFinal);
            }else{
                if (visorStatusFinal.equals("OFFLINE")){
                    orderService.migrateVisor(orderIdFinal, "SINFINALIZAR-OFFLINE");
                }else {
                    orderService.migrateVisor(orderIdFinal, "SINFINALIZAR");
                }
            }
            //};


            //taskExecutor.execute(task);
            //cip = orderService.deleteCerosLeft(cip);
            responseData.put("cip", cip);
            responseData.put("orderId", orderId);
            responseData.put("estadoHDEC", visorStatus);
            responseMessage = "cip";

        } catch (DataValidationException e) {
            logger.error("Error al guardar web", e);
            responseCode = CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR;
            responseMessage = mensajeGuardarNoSePuedeRegistrar;
        } catch (ApiClientException | ClientException e) {
            logger.error("Error al llamar HDC", e);
            responseCode = CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR;

            String mensajeError = messageUtil.getMensajeErrorHDECInterpretado(e, "");
            responseMessage = mensajeError;

            if (e.getMessage() != null && e.getMessage().contains("Error: CODIGO_UNICO ya existe")) {
                logger.info("Ya existe el codigo unico, el flujo continuara con el proceso");
            } else {
                if(validadorAutomatizador){
                    orderService.migrateVisor(orderId, "PENDIENTE");
                }else{
                    logger.info("Error diferente al codigo unico en hdec, se detendra el flujo");
                    if (orderId != null && !orderId.equals(""))
                        orderService.migrateVisor(orderId, "CAIDA");
                }
            }

        } catch (ApplicationException e) {
            logger.error("Error al llamar HDC", e);
            responseCode = CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR;
            responseMessage = mensajeGuardarNoSePuedeRegistrar;
        } catch (URISyntaxException e) {
            logger.error("Error Pago efectivo", e);
            responseCode = CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR;
            responseMessage = "ERROR_CIP";//mensajeSaveErrorGenerico;
        }
        response.setResponseCode(responseCode);
        response.setResponseData(responseData);
        response.setResponseMessage(responseMessage);
        return response;
    }

    private void sendAutomaticDebitMail(String email) {
        String emailHTML = "";
        try {
            String path = "/static/template/index.html";
            //File file = new File(getClass().getResource(path).getFile());

            File file = new File(new URI(getClass().getResource(path).toString()));

            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            logger.error("Error sendAutomaticDebitMail", e);
        }

        if (email != null && !email.equals("")) {
            EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, emailHTML, "Afiliación a Débito Automático - Fija");// mailingConstants.getSubjectSolicitado());

            logger.info("Result ->");
            logger.info("Email: " + email);
            logger.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
            logger.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
            //if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
        } else {
            logger.info("La venta no cuenta con CORREO");
        }

    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/upload", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> upload(@RequestParam("file") MultipartFile file,
                                   @RequestParam("orderId") String orderId) throws IOException {
        if (file == null) {
            throw new FileNotFoundException("The file uploaded can not be read");
        }
        if (!file.getOriginalFilename().toLowerCase().endsWith(".gsm")) {
            throw new IllegalArgumentException("Debe ingresar registrar un archivo GSM. " + file.getOriginalFilename());
        }
        Response<String> response = new Response<>();
        try {
            // ojo... las variables WEBVF y 1.0 deberian ser enviadas desde el frontend... queda pendiente
            orderService.uploadWebFile(file, orderId, "WEBVF", "1.0");
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
            response.setResponseMessage("Archivo registrado exitosamente");

        } catch (Exception e) {
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Ocurrio un error al subir archivo de audio. Por favor intente nuevamente");
        }

        //Sprint 3 - el registro en azure ahora va en el metodo orderService.uploadWebFile
        //orderService.registrarAzure(orderId);
        return response;
    }

    //sprint 3 - upload masivo de audios
    //solo se recibe una lista de archivos de audios
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/uploadmasivo", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<UploadResponseData>> uploadMasivo(@RequestParam("file") MultipartFile[] file) throws IOException {
		/*if (file == null) {
			throw new FileNotFoundException("The file uploaded can not be read");
		}
		if (!file.getOriginalFilename().toLowerCase().endsWith(".gsm")) {
			throw new IllegalArgumentException("Debe ingresar registrar un archivo GSM. "+file.getOriginalFilename());
		}
		orderService.uploadWebFile(file.getInputStream(), orderId);*/

        // ojo... las variables WEBVF y 1.0 deberian ser enviadas desde el frontend... queda pendiente
        List<UploadResponseData> responseList = cargaAudioMasivaService.uploadWebFileMasivo(file, "WEBVF", "1.0");

        Response<List<UploadResponseData>> response = new Response<>();
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
        response.setResponseMessage("Archivo registrado exitosamente");
        response.setResponseData(responseList);
        //orderService.registrarAzure(orderId);

        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/cancel", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> cancel(@RequestBody WebOrderDto saveOrderRequest, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        Response<String> response = new Response<>();
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
        try {
            orderService.cancel(saveOrderRequest, codigoAplicacion);
            response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
            response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
        } catch (Exception e) {
            logger.error("Error del sistema :/", e);
            response.setResponseData(mensajeSaveErrorGenerico);
        }
        return response;
    }

    @ExceptionHandler(IOException.class)
    public Response<String> handleIOExcpetion(IOException ex) {
        Response<String> response = new Response<>();
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
        response.setResponseMessage("Error al procesar la peticion");
        logger.error("Error al procesar la peticion", ex);
        return response;
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public Response<String> handleIllegalArgumentException(IllegalArgumentException ex) {
        Response<String> response = new Response<>();
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
        response.setResponseMessage(ex.getMessage());
        logger.error("Error al procesar la peticion", ex);
        return response;
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/report", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ReportResponse> reportePedido(@RequestBody ReportRequest request) {
        Response<ReportResponse> response = new Response<>();
        try {
            response = orderService.getReportHistorico(request);
        } catch (Exception e) {
            logger.error("Error reportePedido Controller", e);
        }
        LogVass.finishController(logger, "reportePedido", response);
        return response;
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/report2", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ReportResponse2> reportePedido2(@RequestBody CodAtisRequest request) {

        Response<ReportResponse2> response = new Response<>();

        try {
            response = orderService.getReportTracking(request);
        } catch (Exception e) {
            logger.error("Error reportePedido Controller", e);
        }

        LogVass.finishController(logger, "reportePedido", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/recupero", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<RecuperoResponse> recuperoCaida(@RequestBody RecuperoRequest request) {
        Response<RecuperoResponse> response = new Response<>();

        try {
            response = orderService.generarRecuperoVenta(request);
            response.setResponseMessage("01");
            response.setResponseMessage("Servicio Ok");
        } catch (Exception e) {
            response.setResponseMessage("00");
            response.setResponseMessage("Error del Servicios - intente nuevamente");
            logger.error("Error recuperoCaida Controller", e);
        }

        return response;
    }

    public static String parseStringBornDmY(String dateBorn) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Date date = format.parse(dateBorn);
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            return formateador.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }


}
