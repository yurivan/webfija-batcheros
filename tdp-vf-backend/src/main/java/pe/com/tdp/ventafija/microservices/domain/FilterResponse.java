package pe.com.tdp.ventafija.microservices.domain;

public class FilterResponse {
    private String whereQuery;
    private String columnsSelected;

    public FilterResponse(String whereQuery, String columnsSelected) {
        this.whereQuery = whereQuery;
        this.columnsSelected = columnsSelected;
    }

    public String getWhereQuery() {
        return whereQuery;
    }

    public void setWhereQuery(String whereQuery) {
        this.whereQuery = whereQuery;
    }

    public String getColumnsSelected() {
        return columnsSelected;
    }

    public void setColumnsSelected(String columnsSelected) {
        this.columnsSelected = columnsSelected;
    }
}
