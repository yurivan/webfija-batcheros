package pe.com.tdp.ventafija.microservices.domain.order.entity;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Customer;

public class CustomerValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Customer.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
//		ValidationUtils.rejectIfEmpty(errors, "customerPhone", "customer.customerPhone.empty");
	}

}
