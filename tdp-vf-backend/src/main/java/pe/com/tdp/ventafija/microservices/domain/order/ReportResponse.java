package pe.com.tdp.ventafija.microservices.domain.order;

import java.util.List;

public class ReportResponse {

    private List<Mes> meses;

    public List<Mes> getMeses() { return this.meses; }

    public void setMeses(List<Mes> meses) { this.meses = meses; }


}
