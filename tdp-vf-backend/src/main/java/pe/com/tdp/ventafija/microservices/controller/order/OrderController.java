package pe.com.tdp.ventafija.microservices.controller.order;

import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.constant.CustomerConstants;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.dao.OrderRepository;
import pe.com.tdp.ventafija.microservices.common.domain.dto.HDECResponse;
import pe.com.tdp.ventafija.microservices.common.domain.dto.SalesReport;
import pe.com.tdp.ventafija.microservices.common.domain.dto.UserOrderResponse;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Order;
import pe.com.tdp.ventafija.microservices.common.domain.message.MessageEntities;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.exception.ApplicationException;
import pe.com.tdp.ventafija.microservices.common.exception.DataValidationException;
import pe.com.tdp.ventafija.microservices.common.mailing.EmailResponseBody;
import pe.com.tdp.ventafija.microservices.common.mailing.Mailing;
import pe.com.tdp.ventafija.microservices.common.services.GoogleStorageService;
import pe.com.tdp.ventafija.microservices.common.services.HDECRetryService;
import pe.com.tdp.ventafija.microservices.common.services.config.ApplicationConfiguration;
import pe.com.tdp.ventafija.microservices.common.util.*;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipRequest;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipResponse;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.domain.motorizado.entity.TdpMotorizadoBase;
import pe.com.tdp.ventafija.microservices.domain.order.*;
import pe.com.tdp.ventafija.microservices.domain.order.dto.WebOrderDto;
import pe.com.tdp.ventafija.microservices.domain.order.entity.PeticionPagoEfectivo;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpVisor;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpMotorizadoBaseRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpVisorRepository;
import pe.com.tdp.ventafija.microservices.repository.address.AddressDao;
import pe.com.tdp.ventafija.microservices.service.automatizador.AutomatizadorService;
import pe.com.tdp.ventafija.microservices.service.order.AudioConverterService;
import pe.com.tdp.ventafija.microservices.service.order.Messages;
import pe.com.tdp.ventafija.microservices.service.order.OrderService;
import pe.com.tdp.ventafija.microservices.service.product.ProductService;

import java.io.File;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/order")
public class OrderController {

    private static final Logger logger = LogManager.getLogger(OrderController.class);
    @Autowired
    private TdpMotorizadoBaseRepository tdpMotorizadoBaseRepository;
    @Autowired
    private TdpVisorRepository tdpVisorRepository;
    @Autowired
    private OrderService orderService;
    @Autowired
    private ProductService productService;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private AudioConverterService audioConverterService;
    @Autowired
    private GoogleStorageService googleStorageService;
    @Autowired
    public ApplicationConfiguration applicationConfiguration;
    @Autowired
    private TaskExecutor taskExecutor;
    @Autowired
    private Messages messages;
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private AutomatizadorService automatizadorService;

    @Autowired
    private HDECRetryService hdecRetryService;

    @Autowired
    Mailing mailing;

    @Autowired
    private AddressDao addressDao;

    //Variables
    @Value("${order.mensajes.guardar.NoSePuedeRegistrar}")
    private String mensajeGuardarNoSePuedeRegistrar;
    @Value("${order.mensajes.registrarPreVenta.ErrorHdec}")
    private String mensajeRegistrarPreVentaErrorHdec;
    @Value("${order.mensajes.save.errorGenerico}")
    private String mensajeSaveErrorGenerico;

    @RequestMapping(value = "/firebase", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public SaleApiFirebaseResponseBody readFirebase(@RequestBody SaveOrderRequest saveOrderRequest) {
        LogVass.startController(logger, "readFirebase", saveOrderRequest);
        SaleApiFirebaseResponseBody saleApiFirebaseResponseBody = null;
        try {
            saleApiFirebaseResponseBody = orderService.objFirebase(saveOrderRequest.getFirebaseId());
        } catch (Exception e) {
            logger.error("Error ReadFirebase Controller", e);
        }
        LogVass.finishController(logger, "ReadFirebase", saleApiFirebaseResponseBody);
        return saleApiFirebaseResponseBody;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> saveOrder(@RequestBody SaveOrderRequest saveOrderRequest, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        LogVass.startController(logger, "SaveOrder", saveOrderRequest);
        final String username = MDC.get("username");
        Response<String> response = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("VENTA");
        LogVass.serviceResponseHashMap(logger, "GetOrder", "messageList", messageList);
        //End--Code for call Service
        // Runnable task = () -> {
        try {
            MDC.put("username", username);
            orderService.save(saveOrderRequest.getFirebaseId(), codigoAplicacion, saveOrderRequest.getSourceProductName());

            response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
            response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
            logger.info("Se registro en la tabla Order con estado PRE EFECTIVO");
            LogVass.finishController(logger, "SaveOrder", response);
            return response;
        } catch (DataValidationException e) {
            logger.error("error de validacion", e);
            response.setResponseCode(messageList.get("errorSaveOrder").getCode());
            //Guardar no se puede registrar
            response.setResponseMessage(messageList.get("errorSaveOrder").getMessage());
            LogVass.finishController(logger, "SaveOrder", response);
            return response;
        } catch (Exception e) {
            logger.error("error ", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            LogVass.finishController(logger, "SaveOrder", response);
            return response;
        } finally {
            MDC.remove("username");
        }
        // };
        // taskExecutor.execute(task);

        /*
         * Response<String> response = new Response<>();
         * response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK
         * );
         * response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK
         * ); return response;
         */
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> updateOrder(@RequestBody SaveOrderRequest saveOrderRequest) {
        LogVass.startController(logger, "UpdateOrder", saveOrderRequest);
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("VENTA");
        LogVass.serviceResponseHashMap(logger, "GetOrder", "messageList", messageList);
        //End--Code for call Service
        String orderId = saveOrderRequest.getFirebaseId();
        Response<String> response = new Response<>();
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_ERROR);
        try {
            if (!orderService.isPending(orderId)) {
                logger.info("orden ya registrada, no se realiza acciones");
                response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
                response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
                PeticionPagoEfectivo ppe = orderService.findCodigoPagoEfectivo(orderId);
                if (ppe != null) {
                    SaleApiFirebaseResponseBody objFirebase = orderService.objFirebase(orderId);
                    GenerarCipRequest cipRequest = new GenerarCipRequest();
                    GenerarCipResponse cipResponse = new GenerarCipResponse();
                    String cip = ppe.getCodPago();
                    if (cip == null) {
                        cipRequest.setCurrency("PEN");
                        cipRequest.setAmount(ppe.getTotal());
                        cipRequest.setTransactionCode(orderId);
                        cipRequest.setAdminEmail("lincoln.morales@telefonica.com");
                        cipRequest.setDateExpiry(parseStringBornDmY(ppe.getFechaAExpirar()));
                        cipRequest.setPaymentConcept("Movistar");
                        cipRequest.setAdditionalData("Generando online");
                        cipRequest.setUserEmail(objFirebase.getClient_email() != null || objFirebase.getClient_email() != "" ? objFirebase.getClient_email() : "Nulo@nulo.com");
                        cipRequest.setUserName(objFirebase.getClient_names());
                        cipRequest.setUserLastName(objFirebase.getClient_lastname() + " " + objFirebase.getClient_mother_lastname());
                        cipRequest.setUserUbigeo("150115");
                        cipRequest.setUserCountry("PERU");
                        String documenttype;
                        switch (objFirebase.getClient_document()) {
                            case "DNI":
                                documenttype = "DNI";
                                break;
                            case "CEX":
                                documenttype = "NAN";
                                break;
                            case "PAS":
                                documenttype = "PAS";
                                break;
                            case "RUC":
                                documenttype = "NAN";
                                break;
                            case "Otros":
                                documenttype = "NAN";
                                break;
                            default:
                                documenttype = "NAN";
                                break;
                        }
                        cipRequest.setUserDocumentType(documenttype);
                        cipRequest.setUserDocumentNumber(objFirebase.getClient_doc_number());
                        cipRequest.setUserCodeCountry("+51");
                        cipRequest.setUserPhone(objFirebase.getClient_mobile_phone() != null || objFirebase.getClient_mobile_phone() != "" ? objFirebase.getClient_mobile_phone() : "999999999");
                        String token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJNT1YiLCJqdGkiOiIxYjUxYmJjYi0wYmQ5LTQwMmUtYTRlOC1lYWIwNTQ2N2I3N2MiLCJuYW1laWQiOiIxMDQiLCJleHAiOjE1ODAwMTIxMDF9.2TVKKT-X7jFiWx-_7DJUZBMYkknrsNY1WX1tqipubAw";
                        cipResponse = orderService.generarCIP(cipRequest, token);
                        cip = cipResponse != null ? cipResponse.getData().getCip() + "" : "ERROR_CIP";

                        if (objFirebase.getType() != null &&
                                (objFirebase.getType().equalsIgnoreCase("A") ||
                                        objFirebase.getType().equalsIgnoreCase("S"))){

                            if(cip!= null){
                                boolean success = automatizadorService.preRegisterSaleWeb2(orderId, objFirebase.getType());
                                logger.error("El envío a Automatizador es: ", success);
                            }

                        }
                    }
                    //cip = cip == null ? "ERROR_CIP" : cip;
                    response.setResponseMessage(cip);
                }
                return response;
            }

            // Sprint 4 - Implementacion de requerimiento UPFRONT - si es que la venta requiere pago adelantado entonces ya no se envia la venta a TGESTIONA (HDEC)
            // La venta sera enviada a TGESTIONA cuando el comercio notifique que la venta ya ha sido pagada...
            // Ahora solo las ventas que sean financiadas seran enviadas a TGESTIONA automaticamente al cerrar la venta
            // Pendiente pasar a constantes el status...
            String visorStatus = "PENDIENTE";
            try {
                String cip = "";
                GenerarCipRequest cipRequest = new GenerarCipRequest();
                GenerarCipResponse cipResponse = new GenerarCipResponse();
                SaleApiFirebaseResponseBody objFirebase = orderService.objFirebase(orderId);
                if (orderService.update(orderId)) {
                    logger.info("Se va a proceder a registrar la peticion del cip");
                    PeticionPagoEfectivo ppe = orderService.registrarPeticionCIP(orderId);

                    if (ppe != null) {
                        cipRequest.setCurrency("PEN");
                        cipRequest.setAmount(ppe.getTotal());
                        cipRequest.setTransactionCode(orderId);
                        cipRequest.setAdminEmail("lincoln.morales@telefonica.com");
                        cipRequest.setDateExpiry(parseStringBornDmY(ppe.getFechaAExpirar()));
                        cipRequest.setPaymentConcept("Movistar");
                        cipRequest.setAdditionalData("Generando online");
                        cipRequest.setUserEmail(objFirebase.getClient_email() != null || objFirebase.getClient_email() != "" ? objFirebase.getClient_email() : "Nulo@nulo.com");
                        cipRequest.setUserName(objFirebase.getClient_names());
                        cipRequest.setUserLastName(objFirebase.getClient_lastname() + " " + objFirebase.getClient_mother_lastname());
                        cipRequest.setUserUbigeo("150115");
                        cipRequest.setUserCountry("PERU");
                        String documenttype;
                        switch (objFirebase.getClient_document()) {
                            case "DNI":
                                documenttype = "DNI";
                                break;
                            case "CEX":
                                documenttype = "NAN";
                                break;
                            case "PAS":
                                documenttype = "PAS";
                                break;
                            case "RUC":
                                documenttype = "NAN";
                                break;
                            case "Otros":
                                documenttype = "NAN";
                                break;
                            default:
                                documenttype = "NAN";
                                break;
                        }
                        cipRequest.setUserDocumentType(documenttype);
                        cipRequest.setUserDocumentNumber(objFirebase.getClient_doc_number());
                        cipRequest.setUserCodeCountry("+51");
                        cipRequest.setUserPhone(objFirebase.getClient_mobile_phone() != null || objFirebase.getClient_mobile_phone() != "" ? objFirebase.getClient_mobile_phone() : "999999999");
                        String token = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJNT1YiLCJqdGkiOiIxYjUxYmJjYi0wYmQ5LTQwMmUtYTRlOC1lYWIwNTQ2N2I3N2MiLCJuYW1laWQiOiIxMDQiLCJleHAiOjE1ODAwMTIxMDF9.2TVKKT-X7jFiWx-_7DJUZBMYkknrsNY1WX1tqipubAw";
                        cipResponse = orderService.generarCIP(cipRequest, token);
                        cip = cipResponse != null ? cipResponse.getData().getCip() + "" : "ERROR_CIP";
                        //cip = orderService.genCIP(id);
                        //cip = cip == null ? "ERROR_CIP" : cip;

                        if (objFirebase.getType() != null &&
                                (objFirebase.getType().equalsIgnoreCase("A") ||
                                  objFirebase.getType().equalsIgnoreCase("S"))){

                            if(cip!= null){
                                boolean success = automatizadorService.preRegisterSaleWeb2(orderId, objFirebase.getType());
                                logger.error("El envío a Automatizador es: ", success);
                            }

                        }

                    }

                    String finalVisorStatus = visorStatus;
                    //Runnable task = () -> {

                     orderService.migrateVisor(saveOrderRequest.getFirebaseId(), finalVisorStatus);

                    //};
                    //taskExecutor.execute(task);

                } else {
                    throw new ApplicationException(MessageConstants.ORDER_NOT_SAVED);
                }
                //cip = orderService.deleteCerosLeft(cip);
                response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
                response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
                response.setResponseMessage(cip);

                Order objOrder = new Order();
                objOrder.setCashPrice(objFirebase.getProduct_cash_price());
                objOrder.setProductCategory(objFirebase.getProduct_category());
                objOrder.setProductType(objFirebase.getProduct_type());
                objOrder.setCommercialOperation(objFirebase.getProduct_commercial_operation());

                boolean isOffline=false;

                if(objFirebase.getOffline()!=null) {
                    String offlineStr = objFirebase.getOffline();
                    if(offlineStr.contains("flag") && offlineStr.contains("body")){
                        WebOrderDto.Offline offlineObj = (new Gson()).fromJson(offlineStr, WebOrderDto.Offline.class);
                        if(offlineObj.getFlag()!=null && (offlineObj.getFlag().contains("1") || offlineObj.getFlag().contains("6") || offlineObj.getFlag().contains("7"))){
                            isOffline = true;
                        }
                    }

                }



                visorStatus = orderService.getVisorStatus(objOrder);

                if(isOffline) {
                    visorStatus = "OFFLINE";
                    orderRepository.updateEstadoVisor(visorStatus, orderId);
                }

                if ("PENDIENTE".equals(visorStatus)) {

                    if (objFirebase.getType() != null && (objFirebase.getType().equalsIgnoreCase("A") ||
                            objFirebase.getType().equalsIgnoreCase("S") || objFirebase.getType().equalsIgnoreCase("M"))) {
                        String type = "";
                        if (objFirebase.getType().equalsIgnoreCase("A")) type = OrderConstants.ALTA_PURA;
                        else if (objFirebase.getType().equalsIgnoreCase("S")) type = OrderConstants.ALTA_SVA;
                        else type = OrderConstants.MIGRACION;
                        //Enviar a automatizador
                        boolean success = automatizadorService.preRegisterSaleWeb(orderId, type);

                        if (!success) {
                            Integer duplicado = addressDao._consultarDuplicado(orderId);
                            //consultar duplicado en automatizador

                            if(duplicado.equals(0)){
                                HDECResponse hdecResponse = hdecRetryService.sendToHDECWithRetry(orderId);
                                if (HDECResponse.ERROR_CONTROLADO.equals(hdecResponse)) {
                                    visorStatus = "ENVIANDO";
                                    orderRepository.updateEstadoVisor(visorStatus, orderId);
                                }
                            }

                            if(duplicado.equals(1)){
                                logger.error("Error Duplicado en Automatizador", duplicado);
                            }
                            if(duplicado.equals(2)){
                                visorStatus = "ENVIANDO_AUTOMATIZADOR";
                                orderRepository.updateEstadoVisor(visorStatus, orderId);
                                logger.error("Error de Bus", duplicado);
                            }

                        } else {
                            automatizadorService.setOrderInAutomatizador(orderId);
                        }
                    } else {
                        HDECResponse hdecResponse = hdecRetryService.sendToHDECWithRetry(orderId);
                        if (HDECResponse.ERROR_CONTROLADO.equals(hdecResponse)) {
                            visorStatus = "ENVIANDO";
                            orderRepository.updateEstadoVisor(visorStatus, orderId);
                        }
                    }


                    //orderService.sendToHDEC(orderId);
                }
                if (objFirebase.getAutomatic_debit()) {
                    sendAutomaticDebitMail(objFirebase.getClient_email());
                }
            } catch (ApiClientException | ClientException e) {
                if (e.getMessage() != null && e.getMessage().contains("Error: CODIGO_UNICO ya existe")) {
                    logger.info("Ya existe el codigo unico, el flujo continuara con el proceso");
                } else {
                    logger.info("Error diferente al codigo unico en hdec, se detendra el flujo");
                    orderRepository.updateEstadoVisor("CAIDA", orderId);
                    throw e;
                }

            }

        } catch (ApiClientException | ClientException e) {
            logger.error("Error al hacer peticion al cliente", e);

            if (e instanceof ClientException) {
                if (((ClientException) e).getEvent() == null) {
                    // Si es que event es null significa que es un error en los datos enviados a HDEC
                    response.setResponseCode(messageList.get("errorHdecDatosIncorrectos").getCode());
                    response.setResponseMessage(messageList.get("errorHdecDatosIncorrectos").getMessage());
                } else {
                    // Si es que event es distinto de null significa que es un error en el lado del servidor HDEC
                    //return "Ocurrio un error al subir audio. El servicio de HDEC no esta disponible. Por favor intente nuevamente.";
                    response.setResponseCode(messageList.get("mensajeRegistrarPreVentaErrorHdec").getCode());
                    response.setResponseMessage(messageList.get("mensajeRegistrarPreVentaErrorHdec").getMessage());
                }
            } else {
                response.setResponseCode(messageList.get("mensajeRegistrarPreVentaErrorHdec").getCode());
                response.setResponseMessage(messageList.get("mensajeRegistrarPreVentaErrorHdec").getMessage());
            }
        } catch (ApplicationException e) {
            logger.error("Error al actualizar el registro", e);
            response.setResponseCode(messageList.get("errorSaveOrder").getCode());
            response.setResponseMessage(messageList.get("errorSaveOrder").getMessage());
        } catch (Exception e) {
            logger.error("Error en algo", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
        }

        LogVass.finishController(logger, "UpdateOrder", response);
        return response;
    }

    @RequestMapping(value = "/updatepending", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> updatePending(@RequestBody SaveOrderRequest saveOrderRequest) {


        Response<String> response = new Response<>();

        SaleApiFirebaseResponseBody objFirebase = orderService.objFirebase(saveOrderRequest.getFirebaseId());

        String jsonOFFLINE = objFirebase.getOffline();

        WebOrderDto.Offline offline = (new Gson()).fromJson(jsonOFFLINE, WebOrderDto.Offline.class);;

        if (!offline.getBody().toString().contains("emailvendedor") && !offline.getFlag().contains("7")) {
            response.setResponseCode("-1");
            return response;
        }

        //CIP
        orderRepository.updateEstadoVisor("OFFLINE", saveOrderRequest.getFirebaseId());

        try {
            boolean updated = orderService.updatePending(offline, saveOrderRequest.getFirebaseId());
            response.setResponseCode(updated?"0":"-1");
            ;
        } catch (Exception e) {
            logger.error("Error savePendingOrder", e);
            response.setResponseCode("-1");
        }

        return response;
    }

    private void sendAutomaticDebitMail(String email) {
        String emailHTML = "";
        try {
            String path = "/static/template/index.html";
            //File file = new File(getClass().getResource(path).getFile());

            File file = new File(new URI(getClass().getResource(path).toString()));

            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            logger.error("Error sendAutomaticDebitMail", e);
        }

        if (email != null && !email.equals("")) {
            EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, emailHTML, "Afiliación a Débito Automático - Fija");// mailingConstants.getSubjectSolicitado());

            logger.info("Result ->");
            logger.info("Email: " + email);
            logger.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
            logger.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
            //if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
        } else {
            logger.info("La venta no cuenta con CORREO");
        }

    }

    //procesamiento masivo de audios
    @RequestMapping(value = "/processaudiomasive", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> processAudioMasive(@RequestBody String fecha) {
        logger.info("Inicio ProcessAudioMasive Controller");
        logger.info("REQUEST => " + fecha);

        List<String> orderList = productService.getOrderListForAudioProcess(fecha);
        LogVass.serviceResponseArray(logger, "processAudioMasive", "orderList", orderList);

        //Runnable task = () -> {
        for (String idOrder : orderList) {
            logger.info("ProcessAudioMasive => idOrder: " + idOrder);
            try {
                SaveOrderRequest saveOrderRequest = new SaveOrderRequest();
                saveOrderRequest.setFirebaseId(idOrder);
                Response<String> responseInd = processAudio(saveOrderRequest);
                logger.info(responseInd.getResponseMessage());
            } catch (Exception ex) {
                logger.error("Error ProcessAudioMasive.", ex);
            }
        }
        //};
        //taskExecutor.execute(task);

        Response<String> response = new Response<>();
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
        response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
        response.setResponseMessage("PROCESADO");
        LogVass.finishController(logger, "ProcessAudioMasive", response);
        return response;
    }

    //No tiene mensaje de incidencia
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/processaudio", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> processAudio(@RequestBody SaveOrderRequest saveOrderRequest) {
        LogVass.startController(logger, "ProcessAudio", saveOrderRequest);

        Response<String> response = new Response<>();
        ServiceCallEvent event = VentaFijaContextHolder.getContext().getServiceCallEvent();

        String orderId = saveOrderRequest.getFirebaseId();

        // Validar que exista en Firebase el idVenta y sus carpetas
        SaleApiFirebaseResponseBody objFirebase = orderService.objFirebase(orderId);

        if (orderId.indexOf("motorizado") > 0) {
            try {
                TdpVisor tdpVisor = tdpVisorRepository.findOneByIdVisor(orderId.replace("_motorizado", ""));
                List<TdpMotorizadoBase> tdpMotorizadoBaseList = tdpMotorizadoBaseRepository.findAllByCodigoPedidoAndNumDocCliente(tdpVisor.getCodigoPedido(), tdpVisor.getDni());
                if (tdpMotorizadoBaseList.size() > 0) {
                    objFirebase = new SaleApiFirebaseResponseBody();
                    objFirebase.setVendor_id(tdpMotorizadoBaseList.get(0).getCodMotorizado());
                    objFirebase.setClient_doc_number(tdpMotorizadoBaseList.get(0).getNumDocCliente());
                }
            } catch (Exception motorizado) {
                motorizado.printStackTrace();
            }
        }

        String statusAudio = orderRepository.loadStatusAudioById(orderId.replace("_motorizado", ""));
        /* Estados de tranformación de audios
            0   Por procesar
            1   Procesado
            2   Procesando
            3   Error
        */

        if ((statusAudio == null || objFirebase == null) && orderId.indexOf("_motorizado") > 0) {
            response.setResponseCode(OrderConstants.PROCESS_AUDIO_RESPONSE_CODE_OK);
            response.setResponseData(OrderConstants.PROCESS_AUDIO_RESPONSE_DATA_PENDIENTE);
            response.setResponseMessage("-1 PENDIENTE ORDEN");
            LogVass.finishController(logger, "ProcessAudio", response);
            return response;
        }
        /*if (statusAudio != null && Integer.parseInt(statusAudio) == 0) {
            response.setResponseCode(OrderConstants.PROCESS_AUDIO_RESPONSE_CODE_ERROR);
            response.setResponseData(OrderConstants.PROCESS_AUDIO_RESPONSE_DATA_POR_PROCESAR);
            response.setResponseMessage("00 POR PROCESAR");
        } else*/
        if (statusAudio != null && Integer.parseInt(statusAudio) == 1) {
            response.setResponseCode(OrderConstants.PROCESS_AUDIO_RESPONSE_CODE_OK);
            response.setResponseData(OrderConstants.PROCESS_AUDIO_RESPONSE_DATA_PROCESADO);
            response.setResponseMessage("01 PROCESADO");
            LogVass.finishController(logger, "ProcessAudio", response);
            return response;
        } else if (statusAudio != null && Integer.parseInt(statusAudio) == 2) {
            response.setResponseCode(OrderConstants.PROCESS_AUDIO_RESPONSE_CODE_OK);
            response.setResponseData(OrderConstants.PROCESS_AUDIO_RESPONSE_DATA_PROCESANDO);
            response.setResponseMessage("02 PROCESANDO");
            LogVass.finishController(logger, "ProcessAudio", response);
            return response;
        } else if (statusAudio != null && Integer.parseInt(statusAudio) == 3) {
            response.setResponseCode(OrderConstants.PROCESS_AUDIO_RESPONSE_CODE_OK);
            response.setResponseData(OrderConstants.PROCESS_AUDIO_RESPONSE_DATA_ERROR);
            response.setResponseMessage("03 ERROR");
            return response;
        }

        String statusAudioNew = "2";
        orderService.updateStatusAudio(orderId, statusAudioNew);

        String orderStatusAudio = "0";
        try {
            JSONObject firebaseOriginal = new JSONObject();
            firebaseOriginal.put("audio", String.format("%s/audios/%s.zip", orderId, orderId));
            firebaseOriginal.put("images", String.format("%s/images/%s.pdf", orderId, orderId));

            event.setDocNumber(objFirebase.getClient_doc_number());
            event.setUsername(objFirebase.getVendor_id());
            event.setOrderId(orderId);
            event.setResult("OK");
            event.setServiceCode("WAVTOGSM");
            event.setServiceRequest(firebaseOriginal.toString());
            event.setMsg("OK");
            event.setServiceUrl("https://tefappfijaffmpeg-dev.mybluemix.net/api/v1/gsm");
            event.setServiceResponse(orderService.audioReview(orderId));

            orderStatusAudio = "1";
        } catch (Exception e) {
            event.setResult("ERROR");
            event.setMsg("ERROR");
            event.setServiceResponse("Error al procesar: " + e.getMessage());
            logger.error("Error ProcessAudio Controller", e);
            orderStatusAudio = "3";
        }
        orderService.updateReviewData(new ServiceCallEvent(event), orderId, orderStatusAudio);

        response.setResponseCode(OrderConstants.PROCESS_AUDIO_RESPONSE_CODE_OK);
        response.setResponseData(OrderConstants.PROCESS_AUDIO_RESPONSE_DATA_PROCESADO);
        response.setResponseMessage("01 PROCESADO");
        LogVass.finishController(logger, "ProcessAudio", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/cancel", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> cancel(@RequestBody SaveOrderRequest saveOrderRequest, @RequestHeader(name = "X_HTTP_APPSOURCE") String codigoAplicacion) {
        LogVass.startController(logger, "Cancel", saveOrderRequest);
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("VENTA");
        LogVass.serviceResponseHashMap(logger, "Cancel", "messageList", messageList);
        //End--Code for call Service
        Response<String> response = new Response<>();
        response.setResponseCode(messageList.get("errorGeneric").getCode());
        try {
            orderService.cancel(saveOrderRequest.getFirebaseId(), codigoAplicacion);
            response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
            response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
        } catch (Exception e) {
            logger.error("Error Cancel Controller", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
        }
        LogVass.finishController(logger, "Cancel", response);
        return response;
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/salesReport", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<SalesReport>> salesReportFilter(@RequestBody SalesReportRequest salesReportRequest) {
        LogVass.startController(logger, "SalesReport", salesReportRequest);
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("VENTA");
        LogVass.serviceResponseHashMap(logger, "GetOrder", "messageList", messageList);
        //End--Code for call Service
        Response<List<SalesReport>> response = new Response<>();
        try {
            response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);

            response.setResponseData(orderService.reporteVentas(salesReportRequest.getNumDays(), salesReportRequest.getVendorId(), salesReportRequest.getIdTransaccion(), salesReportRequest.getFilterDni(), salesReportRequest.getStatus()));
        } catch (Exception e) {
            logger.error("Error SalesReport Controller", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
        }
        LogVass.finishController(logger, "SalesReport", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/salesReport/detail", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SalesReport> salesReport(@RequestBody SalesReportDetailRequest salesReportDetailRequest) {
        LogVass.startController(logger, "SalesReport/detail", salesReportDetailRequest);
        Response<SalesReport> response = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("VENTA");
        LogVass.serviceResponseHashMap(logger, "GetOrder", "messageList", messageList);
        //End--Code for call Service
        try {
            String cip = null;
            PeticionPagoEfectivo ppe = orderService.findCodigoPagoEfectivo(salesReportDetailRequest.getOrderId());
            if (ppe != null) {
                cip = ppe.getCodPago();
            }
            response.setResponseData(orderService.reporteVenta(salesReportDetailRequest.getOrderId(), cip));
            response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
        } catch (Exception e) {
            logger.error("Error SalesReport/detail Controller", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
        }
        LogVass.finishController(logger, "SalesReport/detail", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/audioreview", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> audio(@RequestBody AudioReviewRequest audioReviewRequest) {
        LogVass.startController(logger, "Audioreview", audioReviewRequest);
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("AUDIO");
        LogVass.serviceResponseHashMap(logger, "GetAudio", "messageList", messageList);
        //End--Code for call Service
        Response<String> response = new Response<>();
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
        response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);

        try {
            orderService.audioReview(audioReviewRequest.getFirebaseId());
        } catch (Exception e) {
            logger.error("Error al procesar algo", e);
            response.setResponseMessage(messages.get(e.getMessage()));
            response.setResponseCode(messageList.get("audioProccess").getCode());
            response.setResponseData(messageList.get("audioProccess").getMessage());
        }

        LogVass.finishController(logger, "Audioreview", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/salesReport/count", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<UserOrderResponse>> getUserOrders(@RequestBody UserRequest userRequest) {
        String userId = userRequest.getVendorId();
        int numDays = userRequest.getNumDays();
        Response<List<UserOrderResponse>> response = new Response<>();
        List<UserOrderResponse> listUserOrders = orderService.getUserOrder(userId, numDays);
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("VENTA");
        LogVass.serviceResponseHashMap(logger, "GetVenta", "messageList", messageList);
        //End--Code for call Service
        try {
            logger.info("Inicio getUserOrders Controller");
            if (listUserOrders != null && listUserOrders.size() > 0) {
                response.setResponseCode(OrderConstants.PARAMETER_COD_RESPUESTA_OK);
                response.setResponseData(listUserOrders);
            } else {
                response.setResponseCode(messageList.get("salesCount").getCode());
                response.setResponseMessage(messageList.get("salesCount").getMessage());
            }

            logger.info("Fin getUserOrders Controller");
        } catch (Exception e) {
            logger.info("Error getUserOrders Controller", e);
        }

        return response;
    }

    @RequestMapping(value = "/looseConnection", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> looseConnection(@RequestBody ConectionRequest conectionRequest) {
        Response<String> response = new Response<>();
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("VENTA");
        LogVass.serviceResponseHashMap(logger, "GetVenta", "messageList", messageList);
        //End--Code for call Service
        try {
            if (conectionRequest != null && conectionRequest.getLstConectionData() != null && !conectionRequest.getLstConectionData().isEmpty()) {
                List<String> ids = conectionRequest.getLstConectionData().stream().filter(d -> d != null && d.getFirebaseId() != null && !"".equals(d.getFirebaseId().trim())).map(ConectionData::getFirebaseId).collect(Collectors.toList());
                System.out.println(ids);
                taskExecutor.execute(() -> {
                    Map<String, SaleApiFirebaseResponseBody> data = orderService.loadByFirebaseIds(ids);
                    try {
                        orderService.looseConnection(conectionRequest.getLstConectionData(), data);
                    } catch (Exception e) {
                        logger.error("Error al registrar log de conexiones", e);
                    }
                });
            }

            response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
            response.setResponseData(CustomerConstants.DUPLICATE_RESPONSE_DATA_OK);
            logger.info("Se registro en la tabla Order con estado PRE EFECTIVO");
            return response;
        } catch (Exception e) {
            logger.error("error ", e);
            response.setResponseCode(messageList.get("looseConnection").getCode());
            response.setResponseMessage(messageList.get("looseConnection").getMessage());
            return response;
        }
    }

    //Sprint 3 - Consulta de ventas para la bandeja de subida de audios
    //deberia usarse get en vez de post... pendiente
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/bandejaVentaAudio", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<SalesReport>> getBandejaVentasAudio(@RequestBody VentaAudioRequest ventaAudioRequest) {
        Response<List<SalesReport>> response = new Response<>();
        response.setResponseCode(CustomerConstants.DUPLICATE_RESPONSE_CODE_OK);
        response.setResponseData(

                orderService.getBandejaVentasAudio(ventaAudioRequest.getNombrePuntoVenta(), ventaAudioRequest.getEntidad(),
                        ventaAudioRequest.getCodigoEstado(), ventaAudioRequest.getCodigoVendedor(), ventaAudioRequest.getBuscarPorVendedor(),
                        ventaAudioRequest.getDniCliente(), ventaAudioRequest.getFechaInicio(), ventaAudioRequest.getFechaFin(), ventaAudioRequest.getFiltroRestringeHoras())

        );
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/consultaHogar", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<TdpVisorResposeData>> getConsultaHogar(@RequestBody TdpVisorRequest request) {
        LogVass.startController(logger, "GetConsultaHogar", request);
        Response<List<TdpVisorResposeData>> response = new Response<>();

        try {
            response = orderService.getConsultaHogar(request);
        } catch (Exception e) {
            logger.error("Error GetConsultaHogar Controller", e);
            response.setResponseCode("00");
            response.setResponseMessage("Error al traer consulta");
        }

        LogVass.finishController(logger, "GetConsultaHogar", response);
        return response;
    }

    public static String parseStringBornDmY(String dateBorn) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Date date = format.parse(dateBorn);
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
            return formateador.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}