package pe.com.tdp.ventafija.microservices.common.util;

import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequestHeader;

import java.util.Locale;

//import pe.com.tdp.ventafija.microservices.domain.ApiRequestHeader;

public class ApiRequestHeaderGenerator {

    public static ApiRequestHeader get(String API_REQUEST_HEADER_OPERATION, String API_REQUEST_HEADER_DESTINATION) {
        ApiRequestHeader apiRequestHeader = new ApiRequestHeader();
        apiRequestHeader.setCountry(Constants.API_REQUEST_HEADER_COUNTRY);
        apiRequestHeader.setLang(Constants.API_REQUEST_HEADER_LANG);
        apiRequestHeader.setEntity(Constants.API_REQUEST_HEADER_ENTITY);
        apiRequestHeader.setSystem(Constants.API_REQUEST_HEADER_SYSTEM);
        apiRequestHeader.setSubsystem(Constants.API_REQUEST_HEADER_SUBSYSTEM);
        apiRequestHeader.setOriginator(Constants.API_REQUEST_HEADER_ORIGINATOR);
        apiRequestHeader.setSender(Constants.API_REQUEST_HEADER_SENDER);
        apiRequestHeader.setUserId(Constants.API_REQUEST_HEADER_USER_ID);
        apiRequestHeader.setWsId(Constants.API_REQUEST_HEADER_WS_ID);
        apiRequestHeader.setWsIp(Constants.API_REQUEST_HEADER_WS_IP);
        apiRequestHeader.setOperation(API_REQUEST_HEADER_OPERATION);
        apiRequestHeader.setDestination(API_REQUEST_HEADER_DESTINATION);
        apiRequestHeader.setExecId(Constants.API_REQUEST_HEADER_EXEC_ID);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));
        apiRequestHeader.setTimestamp(sdf.format(new java.util.Date()) + "-05:00");
        apiRequestHeader.setMsgType(Constants.API_REQUEST_HEADER_MSG_TYPE);

        return apiRequestHeader;
    }
}
