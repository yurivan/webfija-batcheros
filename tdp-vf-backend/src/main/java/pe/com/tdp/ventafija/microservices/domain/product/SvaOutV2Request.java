package pe.com.tdp.ventafija.microservices.domain.product;

public class SvaOutV2Request {
    private String productCode;
    private String productId;
    private String serviceCode;
    private String productType;
    private String legacyCode;
    private String applicationCode;
    private String svaMigracionesUnit;
    private String svaMigracionesCode;

    private String migracionSvasAll;

    private String migracionSvasBloqueTV;
    private String migracionSvasLinea;
    private String migracionSvasInternet;

    private String tvTech;
    private int internetSpeed;

    // Se adiciona
    private String sellerChannelEquivalentCampaign;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getLegacyCode() {
        return legacyCode;
    }

    public void setLegacyCode(String legacyCode) {
        this.legacyCode = legacyCode;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public String getSvaMigracionesUnit() {
        return svaMigracionesUnit;
    }

    public void setSvaMigracionesUnit(String svaMigracionesUnit) {
        this.svaMigracionesUnit = svaMigracionesUnit;
    }

    public String getSvaMigracionesCode() {
        return svaMigracionesCode;
    }

    public void setSvaMigracionesCode(String svaMigracionesCode) {
        this.svaMigracionesCode = svaMigracionesCode;
    }

    public String getMigracionSvasAll() {
        return migracionSvasAll;
    }

    public void setMigracionSvasAll(String migracionSvasAll) {
        this.migracionSvasAll = migracionSvasAll;
    }

    public String getMigracionSvasBloqueTV() {
        return migracionSvasBloqueTV;
    }

    public void setMigracionSvasBloqueTV(String migracionSvasBloqueTV) {
        this.migracionSvasBloqueTV = migracionSvasBloqueTV;
    }

    public String getMigracionSvasLinea() {
        return migracionSvasLinea;
    }

    public void setMigracionSvasLinea(String migracionSvasLinea) {
        this.migracionSvasLinea = migracionSvasLinea;
    }

    public String getMigracionSvasInternet() {
        return migracionSvasInternet;
    }

    public void setMigracionSvasInternet(String migracionSvasInternet) {
        this.migracionSvasInternet = migracionSvasInternet;
    }

    public String getSellerChannelEquivalentCampaign() {
        return sellerChannelEquivalentCampaign;
    }

    public void setSellerChannelEquivalentCampaign(String sellerChannelEquivalentCampaign) {
        this.sellerChannelEquivalentCampaign = sellerChannelEquivalentCampaign;
    }

    public String getTvTech() {
        return tvTech;
    }

    public void setTvTech(String tvTech) {
        this.tvTech = tvTech;
    }

    public int getInternetSpeed() {
        return internetSpeed;
    }

    public void setInternetSpeed(int internetSpeed) {
        this.internetSpeed = internetSpeed;
    }
}
