package pe.com.tdp.ventafija.microservices.domain.configuration;

public class ArgumentariosRequest {

    private String  perfil;

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }
}
