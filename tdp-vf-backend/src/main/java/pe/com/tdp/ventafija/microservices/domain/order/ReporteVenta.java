package pe.com.tdp.ventafija.microservices.domain.order;

import pe.com.tdp.ventafija.microservices.domain.Cell;
import pe.com.tdp.ventafija.microservices.domain.Parameter;

import java.util.List;

public class ReporteVenta {

    private List<Cell> dataReporteventaList;
    private List<Parameter> detalleReporteVentaList;

    public List<Cell> getDataReporteventaList() {
        return dataReporteventaList;
    }

    public void setDataReporteventaList(List<Cell> dataReporteventaList) {
        this.dataReporteventaList = dataReporteventaList;
    }

    public List<Parameter> getDetalleReporteVentaList() {
        return detalleReporteVentaList;
    }

    public void setDetalleReporteVentaList(List<Parameter> detalleReporteVentaList) {
        this.detalleReporteVentaList = detalleReporteVentaList;
    }
}
