package pe.com.tdp.ventafija.microservices.repository.address;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.clients.address.GeocodedAddress;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.constants.product.ProductConstants;
import pe.com.tdp.ventafija.microservices.domain.address.ParametersAddress;
import pe.com.tdp.ventafija.microservices.domain.automatizador.NormalizadorModelVO;


import javax.sql.DataSource;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Repository
public class AddressDao {
    private static final Logger logger = LogManager.getLogger();
    @Value("${datasource.query.select}")
    private String MY_SQL_QUERY_WHERE;
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;

    @Autowired
    private DataSource datasource;


    public List<ParametersAddress> readTypeViaAndUrb() {

        List<ParametersAddress> list = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {

            String query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "auxiliar,domain,category,element,strValue,strfilter")
                    .replace(Constants.MY_SQL_TABLE, "parameters")
                    .replace(Constants.MY_SQL_WHERE, "domain='" + ProductConstants.PARAMETRO_GEOCODIFICAR_DIRECCION + "'");

            if (!query.equals("")) {

                try (PreparedStatement stmt = con.prepareStatement(query)) {
                    LogVass.daoQuery(logger, stmt.toString());

                    ResultSet rs = stmt.executeQuery();
                    while (rs.next()) {
                        ParametersAddress response = new ParametersAddress();
                        response.setAuxiliar(rs.getString("auxiliar"));
                        response.setDomain(rs.getString("domain"));
                        response.setCategory(rs.getString("category"));
                        response.setElement(rs.getString("element"));
                        response.setStrValue(rs.getString("strValue"));
                        response.setStrfilter(rs.getString("strfilter"));
                        list.add(response);
                    }
                }
            }
        } catch (Exception e) {
            logger.info("Error readOferFlujo DAO.", e);
        }
        return list;
    }

    public String getNumberNivelConfianza() {

        List<ParametersAddress> list = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {

            String query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "auxiliar,domain,category,element,strValue,strfilter")
                    .replace(Constants.MY_SQL_TABLE, "parameters")
                    .replace(Constants.MY_SQL_WHERE, "element='tdp.api.geocodificardireccion.getnivelconfianza'");

            if (!query.equals("")) {

                try (PreparedStatement stmt = con.prepareStatement(query)) {
                    LogVass.daoQuery(logger, stmt.toString());

                    ResultSet rs = stmt.executeQuery();
                    while (rs.next()) {
                        ParametersAddress response = new ParametersAddress();
                        response.setAuxiliar(rs.getString("auxiliar"));
                        response.setDomain(rs.getString("domain"));
                        response.setCategory(rs.getString("category"));
                        response.setElement(rs.getString("element"));
                        response.setStrValue(rs.getString("strValue"));
                        response.setStrfilter(rs.getString("strfilter"));
                        list.add(response);
                    }
                }
            }
        } catch (Exception e) {
            logger.info("Error readOferFlujo DAO.", e);
        }
        return list.get(0).getStrValue();
    }



    public void saveNormalizador(String id_transaccion, Boolean normalizador, GeocodedAddress objGD, String tipo){

        String tipo_urbanizacion = objGD.getTipoUrbanizacion();
        String nombre_urbanizacion = objGD.getNombreUrbanizacion();
        String nombreVia = objGD.getNombreVia();
        String tipoVia = objGD.getTipoVia();

        String manzana = objGD.getManzana();
        String lote = objGD.getLote();
        String cuadra = objGD.getCuadra();

        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.contingencia_normalizador " +
                    "(id_transaccion, normalizador,tipo_urbanizacion,nombre_urbanizacion,nombreVia,tipoVia,manzana,lote,fecha,cuadra,tipo) "
                    + "VALUES (?, ?,?,?,?,?,?,?,current_timestamp,?,?)";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, id_transaccion);
            ps.setBoolean(2, true);
            ps.setString(3, tipo_urbanizacion);
            ps.setString(4, nombre_urbanizacion);
            ps.setString(5, nombreVia);
            ps.setString(6, tipoVia);

            ps.setString(7, manzana);
            ps.setString(8, lote);
            ps.setString(9, cuadra);
            ps.setString(10, tipo);

            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert saveNormalizador ubigeo", e);
        }

    }



    public Boolean goToNormalizador(String id_transaccion){
        Boolean response = false;

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.contingencia_normalizador " +
                    "where id_transaccion = " + "'" + id_transaccion + "'" + " and normalizador = 'true' limit 1";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    ContingenciaNormalizador obj = new ContingenciaNormalizador();
                    obj.setId_transaccion(rs.getString("id_transaccion"));
                    obj.setNormalizador(rs.getBoolean("normalizador"));

                    response = obj.getNormalizador();

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error leer contingencia_normalizador ", e);
        }

        return response;

    }

    public NormalizadorModelVO showNormalizador(String id_transaccion){
        NormalizadorModelVO response = new NormalizadorModelVO();

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.contingencia_normalizador " +
                    "where id_transaccion = " + "'" + id_transaccion + "'" + " and normalizador = 'true' ORDER BY id desc LIMIT 1";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    NormalizadorModelVO obj = new NormalizadorModelVO();

                    obj.setId_transaccion(rs.getString("id_transaccion"));
                    obj.setNormalizador(rs.getBoolean("normalizador"));

                    obj.setTipo_urbanizacion(rs.getString("tipo_urbanizacion"));
                    obj.setNombre_urbanizacion(rs.getString("nombre_urbanizacion"));
                    obj.setNombreVia(rs.getString("nombreVia"));
                    obj.setTipoVia(rs.getString("tipoVia"));
                    obj.setManzana(rs.getString("manzana"));
                    obj.setLote(rs.getString("lote"));
                    obj.setCuadra(rs.getString("cuadra"));

                    obj.setFecha(rs.getTimestamp("fecha"));
                    obj.setTipo(rs.getString("tipo"));

                    response = obj;

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error leer contingencia_normalizador ", e);
        }

        return response;

    }

    public void insertAuto(String order_id){
        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.automatizador_order " +
                    "(order_id,duplicado_auto, flag_exits) "
                    + "VALUES (?, ?, ?)";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, order_id);
            ps.setString(2, "");
            ps.setInt(3, 0);

            //ps.setString(6, sdf.format(new Date()) + "-05:00");

            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insertAuto", e);
        }

    }


    public Boolean _exitsVentaVisor(String id_transaccion){
        Boolean response = false;

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.automatizador_order " +
                    "where order_id = " + "'" + id_transaccion + "'";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    ContingenciaNormalizador obj = new ContingenciaNormalizador();
                    obj.setId_transaccion(rs.getString("order_id"));
                    response = true;

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error _exitsVentaVisor", e);
        }

        return response;

    }


    public void _registerVentaVisor( String id_visor,String mensaje_duplicado){
        try (Connection con = datasource.getConnection()) {
            String sql = "update ibmx_a07e6d02edaf552.automatizador_order " +
                    "set flag_exits = '1', duplicado_auto = ? where order_id = ?";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, mensaje_duplicado);
            ps.setString(2, id_visor);

            ps.executeUpdate();



        }   catch (Exception e) {
            e.printStackTrace();
            logger.error("Error _registerVentaVisor", e);
        }
    }

    public void _registerDuplicado( String id_visor){
        try (Connection con = datasource.getConnection()) {
            String sql = "update ibmx_a07e6d02edaf552.tdp_order " +
                    "set duplicado_auto = 'ERR-0004' where order_id = ?";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, id_visor);

            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error update _registerDuplicado", e);
        }
    }


    public int _consultarDuplicado(String id_transaccion){
        Integer response = 0;

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.automatizador_order " +
                    "where order_id = " + "'" + id_transaccion + "'";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    String text = rs.getString("duplicado_auto");
                    if(text.equalsIgnoreCase("ERR-0004")){
                        response = 1;
                    }
                    if(text.equalsIgnoreCase("ERR_BUS")){
                        response = 2;
                    }
                }
            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error _consultarDuplicado", e);
        }

        return response;

    }

    /*
    public Boolean _consultarDuplicado(String id_transaccion){
        Boolean response = false;

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.automatizador_order " +
                    "where order_id = " + "'" + id_transaccion + "' and duplicado_auto = 'ERR-0004'";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    ContingenciaNormalizador obj = new ContingenciaNormalizador();
                    obj.setId_transaccion(rs.getString("order_id"));
                    response = true;

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error _consultarDuplicado", e);
        }

        return response;

    }*/

    public String getPayMethod(String id_transaccion){
        String response = "";

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT paymentmode from ibmx_a07e6d02edaf552.order " +
                    "where id = " + "'" + id_transaccion + "'";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    String paymethod = "";

                    paymethod = rs.getString("paymentmode");

                    response = paymethod;

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error leer Metodo de pago ", e);
        }

        return response;

    }




    public String autoDepaCmsPunto(String codAtis, String departamento){
        String response = "0";

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT "+departamento+" from ibmx_a07e6d02edaf552.tdp_sales_agent " +
                    "where codatis = " + "'" + codAtis + "'";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();

                while (rs.next()) {
                    response = rs.getString(departamento);
                    if(response == null){
                        response = "0";
                    }
                    if(response != null ){
                       if(response.isEmpty()){
                           response = "0";
                       }
                    }
                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error autoDepaCmsPunto", e);
        }

        return response;

    }


}
