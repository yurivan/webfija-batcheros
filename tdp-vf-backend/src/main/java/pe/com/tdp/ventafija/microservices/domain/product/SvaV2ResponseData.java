package pe.com.tdp.ventafija.microservices.domain.product;

import java.util.List;

public class SvaV2ResponseData {
	private List<SvaV2ResponseDataSva> equipment;
	private List<SvaV2ResponseDataSva> sva;
	private boolean offLine;
	
	public List<SvaV2ResponseDataSva> getEquipment() {
		return equipment;
	}
	public void setEquipment(List<SvaV2ResponseDataSva> equipment) {
		this.equipment = equipment;
	}
	public List<SvaV2ResponseDataSva> getSva() {
		return sva;
	}
	public void setSva(List<SvaV2ResponseDataSva> sva) {
		this.sva = sva;
	}
	public boolean isOffLine() {return offLine; }
	public void setOffLine(boolean offLine) {this.offLine = offLine;}
}
