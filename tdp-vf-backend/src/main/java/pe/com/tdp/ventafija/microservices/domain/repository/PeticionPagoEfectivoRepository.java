package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import pe.com.tdp.ventafija.microservices.domain.order.entity.PeticionPagoEfectivo;

public interface PeticionPagoEfectivoRepository extends JpaRepository<PeticionPagoEfectivo, Integer> {
	
	public PeticionPagoEfectivo findByOrderId(String orderId);


}
