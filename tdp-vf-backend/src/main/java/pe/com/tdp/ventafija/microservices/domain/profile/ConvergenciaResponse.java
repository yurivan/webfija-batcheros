package pe.com.tdp.ventafija.microservices.domain.profile;

public class ConvergenciaResponse {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
