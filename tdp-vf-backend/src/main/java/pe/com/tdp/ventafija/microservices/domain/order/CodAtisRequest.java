package pe.com.tdp.ventafija.microservices.domain.order;

public class CodAtisRequest {

    private String codAtis;

    public String getCodAtis() {
        return codAtis;
    }

    public void setCodAtis(String codAtis) {
        this.codAtis = codAtis;
    }
}
