package pe.com.tdp.ventafija.microservices.domain.order;

public enum ActiveFilters {

    F_CANAL("F_CANAL"),
    F_SOCIO("F_SOCIO"),
    F_CAMPANIA("F_CAMPANIA"),
    F_OPERACIONCOMERCIAL("F_OPERACIONCOMERCIAL"),
    F_ZONA("F_ZONA"),
    F_ZONAL("F_ZONAL"),
    F_PUNTOVENTA("F_PUNTOVENTA")
    ;

    private final String text;

    ActiveFilters(String text) {
        this.text = text;
    }
}
