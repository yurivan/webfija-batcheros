package pe.com.tdp.ventafija.microservices.service.product.command.campania;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.controller.customer.CustomerController;
import pe.com.tdp.ventafija.microservices.domain.product.OffersRequest2;
import pe.com.tdp.ventafija.microservices.service.customer.CustomerService;

import java.util.Map;

@Component("filterCampaign")
public class GenericCampaignFilterCommand implements CampaignFilterCommand {

    @Autowired
    CustomerController objCustomer;

    private static final String WHERE_FILTER =
            " AND CAT.SEGMENTO IN ('%s', '%s') " +
            " AND CATCANAL.VALUE IN ('%s', 'TODOS') " +
            " AND CATENTIDAD.VALUE IN ('-', '%s') " +
            " AND CATUBICACION.VALUE IN ('TODAS LAS PROVINCIAS-TODO','%s-TODO','%s')";

    private static final String COLUMN_FILTER =
            " CATCANAL.VALUE AS canal, " +
                    " CATCAMPANA.VALUE AS campania, " +
                    " CATENTIDAD.VALUE AS entidad, " +
                    " CATUBICACION.VALUE AS ubicacion, " +
                    " CAT.bloquetv, " +
                    " CAT.svalinea, " +
                    " CAT.svainternet, " +
                    " CASE WHEN (CATENTIDAD.VALUE = '-') THEN 2 " +
                    " WHEN (CATENTIDAD.VALUE = '%s') THEN 1 " +
                    " END entidad_afinidad, " +
                    " " +
                    " CASE WHEN (CATUBICACION.VALUE = 'TODAS LAS PROVINCIAS-TODO') THEN 3 " +
                    " WHEN (CATUBICACION.VALUE = '%s-TODO') THEN 2 " +
                    " WHEN (CATUBICACION.VALUE = '%s') THEN 1 " +
                    " END ubicacion_afinidad ";

    @Override
    public void execute(OffersRequest2 offersRequest, Map<String, String> variables, StringBuilder whereBuilder, StringBuilder columnsBuilder) {
        String segmento = offersRequest.getSellerSegment();
        String canal = offersRequest.getSellerChannelEquivalentCampaign();
        String entidad = offersRequest.getSellerEntityEquivalentCampaign();
        String provincia = StringUtils.stripAccents(offersRequest.getProvince().toUpperCase());
        String distrito = StringUtils.stripAccents(offersRequest.getDistrict().toUpperCase());

        String filtroSegmento1 = "";
        String filtroSegmento2 = "";
        String filtroCanal = "";
        String filtroEntidad = "";
        String filtroUbicacionProvincia = "";
        String filtroUbicacionProvinciaDistrito = "";

        //por el momento las equivalencias de segmento, canal y entidad estaran en duro
        //pendiente pasar a tabla parameters
        String filtroSegmentoCatalog = objCustomer.consultaParametro("tdp.filtro.segmento.catalog");
        String fitroSegmentoVendedor = objCustomer.consultaParametro("tdp.filtro.segmento.vendendor");

        if(filtroSegmentoCatalog.indexOf(',') != -1){
            String cadena[] = filtroSegmentoCatalog.split(",");
            String nuevoFiltro = "";
            for(int cont = 0;cont < cadena.length;cont++){

                if (cadena[cont].contains(segmento)) {
                    filtroSegmento1 = segmento;
                    filtroSegmento2 +=  cadena[cont] + "','";
                }
                //nuevoFiltro += "'" + cadena[cont] + "',";
            }
            filtroSegmento2 = filtroSegmento2.substring(0,filtroSegmento2.length()-3);

            //filtroSegmentoCatalog = nuevoFiltro;
        }

        //1. Filtro para segmento
        /*
        if (segmento.contains(fitroSegmentoVendedor)) {
            filtroSegmento1 = segmento;
            filtroSegmento2 = filtroSegmentoCatalog;

        } else if ("NPP".equals(segmento)) {
            filtroSegmento1 = "RESIDENCIAL - NPP";
            filtroSegmento2 = "RESIDENCIAL - NPP";
        }
*/
        //2. Filtro para canal
        filtroCanal = canal;

        //3. Filtro entidad
        filtroEntidad = entidad;

        //4. Filtro ubicacion
        provincia = provincia.replace("Ñ", "N");
        distrito = distrito.replace("Ñ", "N");

        filtroUbicacionProvincia = provincia;
        filtroUbicacionProvinciaDistrito = provincia + "-" + distrito;

        //generamos el filtro
        String filtro = String.format(WHERE_FILTER, filtroSegmento1, filtroSegmento2, filtroCanal, filtroEntidad, filtroUbicacionProvincia, filtroUbicacionProvinciaDistrito);
        whereBuilder.append(filtro);

        //generamos las columnas adicionales seleccionadas
        String columns = String.format(COLUMN_FILTER, filtroEntidad, filtroUbicacionProvincia, filtroUbicacionProvinciaDistrito);
        columnsBuilder.append(columns);
    }
}
