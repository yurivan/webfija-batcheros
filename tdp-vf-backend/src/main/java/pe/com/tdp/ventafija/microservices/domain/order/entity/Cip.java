package pe.com.tdp.ventafija.microservices.domain.order.entity;


public class Cip {
    private String idOrdenPago;
    private Integer idCliente;
    private Integer idEstado;
    private Integer idServicio;
    private Integer idMoneda;
    private String numeroOrdenPago;
    private String orderIdComercio;
    private double total;
    private String merchantID;
    private String fechaEmision;
    private String fechaCancelado;
    private String fechaConciliado;
    private String fechaAnulado;
    private String fechaExpirado;
    private String fechaEliminado;
    private String tiempoExpiracion;
    private String estaConciliado;
    private String mailComercio;
    private String usuarioID;
    private String dataAdicional;
    private String usuarioNombre;
    private String usuarioApellidos;
    private String usuarioAlias;
    private String usuarioEmail;
    private String usuarioDomicilio;
    private String usuarioLocalidad;
    private String usuarioProvincia;
    private String usuarioPais;
    private Detalles detalles;

    public Cip() {
        super();
    }

    public String getIdOrdenPago() {
        return idOrdenPago;
    }

    public void setIdOrdenPago(String idOrdenPago) {
        this.idOrdenPago = idOrdenPago;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public Integer getIdEstado() {
        return idEstado;
    }

    public void setIdEstado(Integer idEstado) {
        this.idEstado = idEstado;
    }

    public Integer getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Integer idServicio) {
        this.idServicio = idServicio;
    }

    public Integer getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(Integer idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getNumeroOrdenPago() {
        return numeroOrdenPago;
    }

    public void setNumeroOrdenPago(String numeroOrdenPago) {
        this.numeroOrdenPago = numeroOrdenPago;
    }

    public String getOrderIdComercio() {
        return orderIdComercio;
    }

    public void setOrderIdComercio(String orderIdComercio) {
        this.orderIdComercio = orderIdComercio;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public String getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(String fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public String getFechaCancelado() {
        return fechaCancelado;
    }

    public void setFechaCancelado(String fechaCancelado) {
        this.fechaCancelado = fechaCancelado;
    }

    public String getFechaConciliado() {
        return fechaConciliado;
    }

    public void setFechaConciliado(String fechaConciliado) {
        this.fechaConciliado = fechaConciliado;
    }

    public String getFechaAnulado() {
        return fechaAnulado;
    }

    public void setFechaAnulado(String fechaAnulado) {
        this.fechaAnulado = fechaAnulado;
    }

    public String getFechaExpirado() {
        return fechaExpirado;
    }

    public void setFechaExpirado(String fechaExpirado) {
        this.fechaExpirado = fechaExpirado;
    }

    public String getFechaEliminado() {
        return fechaEliminado;
    }

    public void setFechaEliminado(String fechaEliminado) {
        this.fechaEliminado = fechaEliminado;
    }

    public String getTiempoExpiracion() {
        return tiempoExpiracion;
    }

    public void setTiempoExpiracion(String tiempoExpiracion) {
        this.tiempoExpiracion = tiempoExpiracion;
    }

    public String getEstaConciliado() {
        return estaConciliado;
    }

    public void setEstaConciliado(String estaConciliado) {
        this.estaConciliado = estaConciliado;
    }

    public String getMailComercio() {
        return mailComercio;
    }

    public void setMailComercio(String mailComercio) {
        this.mailComercio = mailComercio;
    }

    public String getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(String usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getDataAdicional() {
        return dataAdicional;
    }

    public void setDataAdicional(String dataAdicional) {
        this.dataAdicional = dataAdicional;
    }

    public String getUsuarioNombre() {
        return usuarioNombre;
    }

    public void setUsuarioNombre(String usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }

    public String getUsuarioApellidos() {
        return usuarioApellidos;
    }

    public void setUsuarioApellidos(String usuarioApellidos) {
        this.usuarioApellidos = usuarioApellidos;
    }

    public String getUsuarioAlias() {
        return usuarioAlias;
    }

    public void setUsuarioAlias(String usuarioAlias) {
        this.usuarioAlias = usuarioAlias;
    }

    public String getUsuarioEmail() {
        return usuarioEmail;
    }

    public void setUsuarioEmail(String usuarioEmail) {
        this.usuarioEmail = usuarioEmail;
    }

    public String getUsuarioDomicilio() {
        return usuarioDomicilio;
    }

    public void setUsuarioDomicilio(String usuarioDomicilio) {
        this.usuarioDomicilio = usuarioDomicilio;
    }

    public String getUsuarioLocalidad() {
        return usuarioLocalidad;
    }

    public void setUsuarioLocalidad(String usuarioLocalidad) {
        this.usuarioLocalidad = usuarioLocalidad;
    }

    public String getUsuarioProvincia() {
        return usuarioProvincia;
    }

    public void setUsuarioProvincia(String usuarioProvincia) {
        this.usuarioProvincia = usuarioProvincia;
    }

    public String getUsuarioPais() {
        return usuarioPais;
    }

    public void setUsuarioPais(String usuarioPais) {
        this.usuarioPais = usuarioPais;
    }

    public Detalles getDetalles() {
        return detalles;
    }

    public void setDetalles(Detalles detalles) {
        this.detalles = detalles;
    }
}
