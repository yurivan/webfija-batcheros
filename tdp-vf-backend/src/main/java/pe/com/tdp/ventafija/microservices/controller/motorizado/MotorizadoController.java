package pe.com.tdp.ventafija.microservices.controller.motorizado;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.customer.ContractResponseData;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.domain.motorizado.MotorizadoRequestData;
import pe.com.tdp.ventafija.microservices.domain.motorizado.entity.TdpMotorizadoBase;
import pe.com.tdp.ventafija.microservices.service.motorizado.MotorizadoService;

import java.util.List;


@RestController
@RequestMapping(value = "/motorizado")
public class MotorizadoController {
    private static final Logger logger = LogManager.getLogger();

    @Autowired
    MotorizadoService motorizadoService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/bandejaMotorizado", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<TdpMotorizadoBase>> consultaMotorizado(@RequestBody MotorizadoRequestData request) {
        LogVass.startController(logger, "ConsultaMotorizado", request);

        Response<List<TdpMotorizadoBase>> response = new Response<>();
        try {
            response = motorizadoService.bandejaDelivery(request.getCodmotorizado());
        } catch (Exception e) {
            logger.error("Error ConsultaMotorizado Controller", e);
        }
        LogVass.finishController(logger, "ConsultaMotorizado", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/resumenVenta", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SaleApiFirebaseResponseBody> resumenVenta(@RequestBody MotorizadoRequestData request) {
        LogVass.startController(logger, "ResumenVenta", request);

        Response<SaleApiFirebaseResponseBody> response = new Response<>();

        try {
            response = motorizadoService.resumenVenta(request);
        } catch (Exception e) {
            logger.error("Error ResumenVenta Controller", e);
        }
        LogVass.finishController(logger, "ResumenVenta", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/contractVenta", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ContractResponseData> contractVenta(@RequestBody MotorizadoRequestData request) {
        LogVass.startController(logger, "ContractVenta", request);

        Response<ContractResponseData> response = new Response<>();

        try {
            response = motorizadoService.contractVenta(request);
        } catch (Exception e) {
            logger.error("Error ContractVenta Controller", e);
        }
        LogVass.finishController(logger, "ContractVenta", response);
        return response;
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/cancelVenta", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SaleApiFirebaseResponseBody> cancelVenta(@RequestBody MotorizadoRequestData request) {
        LogVass.startController(logger, "CancelVenta", request);

        Response<SaleApiFirebaseResponseBody> response = new Response<>();

        try {
            response = motorizadoService.cancelVenta(request);
        } catch (Exception e) {
            logger.error("Error CancelVenta Controller", e);
        }
        LogVass.finishController(logger, "CancelVenta", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/closeVenta", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SaleApiFirebaseResponseBody> closeVenta(@RequestBody MotorizadoRequestData request) {
        LogVass.startController(logger, "CloseVenta", request);

        Response<SaleApiFirebaseResponseBody> response = new Response<>();

        try {
            response = motorizadoService.closeVenta(request);
        } catch (Exception e) {
            logger.error("Error CloseVenta Controller", e);
        }
        LogVass.finishController(logger, "CloseVenta", response);
        return response;
    }
}
