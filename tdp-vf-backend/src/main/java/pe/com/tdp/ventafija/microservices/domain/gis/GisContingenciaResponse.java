package pe.com.tdp.ventafija.microservices.domain.gis;

import com.fasterxml.jackson.annotation.JsonProperty;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseBodyFault;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//@Entity
//@Table(name = "tdp_gis_contingencia", schema = "ibmx_a07e6d02edaf552")
public class GisContingenciaResponse {

    // @Id
    // @Column
    private Integer id;
    //  @Column(name = "id_transaccion")
    private String id_transaccion;
    // @Column(name = "departamento")
    private String departamento;
    // @Column(name = "provincia")
    private String provincia;
    // @Column(name = "distrito")
    private String distrito;
    // @Column(name = "flg_update")
    private int flg_update;
    //  @Column(name = "fecha_register")
    private String fecha_register;


    //  @JsonProperty("COBRE_TEC")
    private String COBRE_TEC;
    // @JsonProperty("COBRE_SAT")
    private String COBRE_SAT;
    //@JsonProperty("COBRE_TRM")
    private String COBRE_TRM;
    //@JsonProperty("COAXIAL_TEC")
    private String COAXIAL_TEC;
    // @JsonProperty("COBRE_BLQ_VTA")
    private String COBRE_BLQ_VTA;
    //  @JsonProperty("COBRE_BLQ_TRM")
    private String COBRE_BLQ_TRM;
    //  @JsonProperty("COAXIAL_SEN")
    private String COAXIAL_SEN;
    //  @JsonProperty("COAXIAL_TRM")
    private String COAXIAL_TRM;
    // @JsonProperty("HFC_TEC")
    private String HFC_TEC;
    // @JsonProperty("HFC_VEL")
    private String HFC_VEL;
    // @JsonProperty("GPON_TEC")
    private String GPON_TEC;
    // @JsonProperty("GPON_VEL")
    private String GPON_VEL;
    // @JsonProperty("GPON_TRM")
    private String GPON_TRM;
    // @JsonProperty("XDSL_TEC")
    private String XDSL_TEC;
    //  @JsonProperty("XDSL_SAT")
    private String XDSL_SAT;
    //  @JsonProperty("XDSL_VEL_TLF")
    private String XDSL_VEL_TLF;
    // @JsonProperty("XDSL_VEL_TRM")
    private String XDSL_VEL_TRM;
    // @JsonProperty("EXC_CABLE_HFC")
    private String EXC_CABLE_HFC;
    // @JsonProperty("EXC_CABLE_ACO")
    private String EXC_CABLE_ACO;
    // @JsonProperty("EDF_NOCABLE_EXISTE")
    private String EDF_NOCABLE_EXISTE;
    // @JsonProperty("EDF_NOCABLE_METROS")
    private String EDF_NOCABLE_METROS;
    // @JsonProperty("PST_APOYO")
    private String PST_APOYO;
    // @JsonProperty("NAP_TEC")
    private String NAP_TEC;
    // @JsonProperty("S4G_TEC")
    private String S4G_TEC;
    //  @JsonProperty("EST_BASE")
    private String EST_BASE;
    // @JsonProperty("CIR_DIGITAL")
    private String CIR_DIGITAL;
    // @JsonProperty("DTH_TEC")
    private String DTH_TEC;
    //@JsonProperty("DTH_BLQ")
    private String DTH_BLQ;
    // @JsonProperty("ZAR_TEC")
    private String ZAR_TEC;
    // @JsonProperty("ZAR_BLQ")
    private String ZAR_BLQ;
    // @JsonProperty("COOR_X")
    private String COOR_X;
    // @JsonProperty("COOR_Y")
    private String COOR_Y;
    // @JsonProperty("WS_STATUS")
    private String WS_STATUS;
    // @JsonProperty("WS_ERROR")
    private String WS_ERROR;
    //@JsonProperty("ClientException")
    private ApiResponseBodyFault ClientException;
    // @JsonProperty("ServerException")
    private ApiResponseBodyFault ServerException;


    public GisContingenciaResponse() {
    }

    public GisContingenciaResponse(Integer id, String id_transaccion, String departamento, String provincia, String distrito, int flg_update, String fecha_register, String COBRE_TEC, String COBRE_SAT, String COBRE_TRM, String COAXIAL_TEC, String COBRE_BLQ_VTA, String COBRE_BLQ_TRM, String COAXIAL_SEN, String COAXIAL_TRM, String HFC_TEC, String HFC_VEL, String GPON_TEC, String GPON_VEL, String GPON_TRM, String XDSL_TEC, String XDSL_SAT, String XDSL_VEL_TLF, String XDSL_VEL_TRM, String EXC_CABLE_HFC, String EXC_CABLE_ACO, String EDF_NOCABLE_EXISTE, String EDF_NOCABLE_METROS, String PST_APOYO, String NAP_TEC, String s4G_TEC, String EST_BASE, String CIR_DIGITAL, String DTH_TEC, String DTH_BLQ, String ZAR_TEC, String ZAR_BLQ, String COOR_X, String COOR_Y, String WS_STATUS, String WS_ERROR, ApiResponseBodyFault clientException, ApiResponseBodyFault serverException) {
        this.id = id;
        this.id_transaccion = id_transaccion;
        this.departamento = departamento;
        this.provincia = provincia;
        this.distrito = distrito;
        this.flg_update = flg_update;
        this.fecha_register = fecha_register;
        this.COBRE_TEC = COBRE_TEC;
        this.COBRE_SAT = COBRE_SAT;
        this.COBRE_TRM = COBRE_TRM;
        this.COAXIAL_TEC = COAXIAL_TEC;
        this.COBRE_BLQ_VTA = COBRE_BLQ_VTA;
        this.COBRE_BLQ_TRM = COBRE_BLQ_TRM;
        this.COAXIAL_SEN = COAXIAL_SEN;
        this.COAXIAL_TRM = COAXIAL_TRM;
        this.HFC_TEC = HFC_TEC;
        this.HFC_VEL = HFC_VEL;
        this.GPON_TEC = GPON_TEC;
        this.GPON_VEL = GPON_VEL;
        this.GPON_TRM = GPON_TRM;
        this.XDSL_TEC = XDSL_TEC;
        this.XDSL_SAT = XDSL_SAT;
        this.XDSL_VEL_TLF = XDSL_VEL_TLF;
        this.XDSL_VEL_TRM = XDSL_VEL_TRM;
        this.EXC_CABLE_HFC = EXC_CABLE_HFC;
        this.EXC_CABLE_ACO = EXC_CABLE_ACO;
        this.EDF_NOCABLE_EXISTE = EDF_NOCABLE_EXISTE;
        this.EDF_NOCABLE_METROS = EDF_NOCABLE_METROS;
        this.PST_APOYO = PST_APOYO;
        this.NAP_TEC = NAP_TEC;
        S4G_TEC = s4G_TEC;
        this.EST_BASE = EST_BASE;
        this.CIR_DIGITAL = CIR_DIGITAL;
        this.DTH_TEC = DTH_TEC;
        this.DTH_BLQ = DTH_BLQ;
        this.ZAR_TEC = ZAR_TEC;
        this.ZAR_BLQ = ZAR_BLQ;
        this.COOR_X = COOR_X;
        this.COOR_Y = COOR_Y;
        this.WS_STATUS = WS_STATUS;
        this.WS_ERROR = WS_ERROR;
        ClientException = clientException;
        ServerException = serverException;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getId_transaccion() {
        return id_transaccion;
    }

    public void setId_transaccion(String id_transaccion) {
        this.id_transaccion = id_transaccion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public int getFlg_update() {
        return flg_update;
    }

    public void setFlg_update(int flg_update) {
        this.flg_update = flg_update;
    }

    public String getFecha_register() {
        return fecha_register;
    }

    public void setFecha_register(String fecha_register) {
        this.fecha_register = fecha_register;
    }

    public String getCOBRE_TEC() {
        return COBRE_TEC;
    }

    public void setCOBRE_TEC(String COBRE_TEC) {
        this.COBRE_TEC = COBRE_TEC;
    }

    public String getCOBRE_SAT() {
        return COBRE_SAT;
    }

    public void setCOBRE_SAT(String COBRE_SAT) {
        this.COBRE_SAT = COBRE_SAT;
    }

    public String getCOBRE_TRM() {
        return COBRE_TRM;
    }

    public void setCOBRE_TRM(String COBRE_TRM) {
        this.COBRE_TRM = COBRE_TRM;
    }

    public String getCOAXIAL_TEC() {
        return COAXIAL_TEC;
    }

    public void setCOAXIAL_TEC(String COAXIAL_TEC) {
        this.COAXIAL_TEC = COAXIAL_TEC;
    }

    public String getCOBRE_BLQ_VTA() {
        return COBRE_BLQ_VTA;
    }

    public void setCOBRE_BLQ_VTA(String COBRE_BLQ_VTA) {
        this.COBRE_BLQ_VTA = COBRE_BLQ_VTA;
    }

    public String getCOBRE_BLQ_TRM() {
        return COBRE_BLQ_TRM;
    }

    public void setCOBRE_BLQ_TRM(String COBRE_BLQ_TRM) {
        this.COBRE_BLQ_TRM = COBRE_BLQ_TRM;
    }

    public String getCOAXIAL_SEN() {
        return COAXIAL_SEN;
    }

    public void setCOAXIAL_SEN(String COAXIAL_SEN) {
        this.COAXIAL_SEN = COAXIAL_SEN;
    }

    public String getCOAXIAL_TRM() {
        return COAXIAL_TRM;
    }

    public void setCOAXIAL_TRM(String COAXIAL_TRM) {
        this.COAXIAL_TRM = COAXIAL_TRM;
    }

    public String getHFC_TEC() {
        return HFC_TEC;
    }

    public void setHFC_TEC(String HFC_TEC) {
        this.HFC_TEC = HFC_TEC;
    }

    public String getHFC_VEL() {
        return HFC_VEL;
    }

    public void setHFC_VEL(String HFC_VEL) {
        this.HFC_VEL = HFC_VEL;
    }

    public String getGPON_TEC() {
        return GPON_TEC;
    }

    public void setGPON_TEC(String GPON_TEC) {
        this.GPON_TEC = GPON_TEC;
    }

    public String getGPON_VEL() {
        return GPON_VEL;
    }

    public void setGPON_VEL(String GPON_VEL) {
        this.GPON_VEL = GPON_VEL;
    }

    public String getGPON_TRM() {
        return GPON_TRM;
    }

    public void setGPON_TRM(String GPON_TRM) {
        this.GPON_TRM = GPON_TRM;
    }

    public String getXDSL_TEC() {
        return XDSL_TEC;
    }

    public void setXDSL_TEC(String XDSL_TEC) {
        this.XDSL_TEC = XDSL_TEC;
    }

    public String getXDSL_SAT() {
        return XDSL_SAT;
    }

    public void setXDSL_SAT(String XDSL_SAT) {
        this.XDSL_SAT = XDSL_SAT;
    }

    public String getXDSL_VEL_TLF() {
        return XDSL_VEL_TLF;
    }

    public void setXDSL_VEL_TLF(String XDSL_VEL_TLF) {
        this.XDSL_VEL_TLF = XDSL_VEL_TLF;
    }

    public String getXDSL_VEL_TRM() {
        return XDSL_VEL_TRM;
    }

    public void setXDSL_VEL_TRM(String XDSL_VEL_TRM) {
        this.XDSL_VEL_TRM = XDSL_VEL_TRM;
    }

    public String getEXC_CABLE_HFC() {
        return EXC_CABLE_HFC;
    }

    public void setEXC_CABLE_HFC(String EXC_CABLE_HFC) {
        this.EXC_CABLE_HFC = EXC_CABLE_HFC;
    }

    public String getEXC_CABLE_ACO() {
        return EXC_CABLE_ACO;
    }

    public void setEXC_CABLE_ACO(String EXC_CABLE_ACO) {
        this.EXC_CABLE_ACO = EXC_CABLE_ACO;
    }

    public String getEDF_NOCABLE_EXISTE() {
        return EDF_NOCABLE_EXISTE;
    }

    public void setEDF_NOCABLE_EXISTE(String EDF_NOCABLE_EXISTE) {
        this.EDF_NOCABLE_EXISTE = EDF_NOCABLE_EXISTE;
    }

    public String getEDF_NOCABLE_METROS() {
        return EDF_NOCABLE_METROS;
    }

    public void setEDF_NOCABLE_METROS(String EDF_NOCABLE_METROS) {
        this.EDF_NOCABLE_METROS = EDF_NOCABLE_METROS;
    }

    public String getPST_APOYO() {
        return PST_APOYO;
    }

    public void setPST_APOYO(String PST_APOYO) {
        this.PST_APOYO = PST_APOYO;
    }

    public String getNAP_TEC() {
        return NAP_TEC;
    }

    public void setNAP_TEC(String NAP_TEC) {
        this.NAP_TEC = NAP_TEC;
    }

    public String getS4G_TEC() {
        return S4G_TEC;
    }

    public void setS4G_TEC(String s4G_TEC) {
        S4G_TEC = s4G_TEC;
    }

    public String getEST_BASE() {
        return EST_BASE;
    }

    public void setEST_BASE(String EST_BASE) {
        this.EST_BASE = EST_BASE;
    }

    public String getCIR_DIGITAL() {
        return CIR_DIGITAL;
    }

    public void setCIR_DIGITAL(String CIR_DIGITAL) {
        this.CIR_DIGITAL = CIR_DIGITAL;
    }

    public String getDTH_TEC() {
        return DTH_TEC;
    }

    public void setDTH_TEC(String DTH_TEC) {
        this.DTH_TEC = DTH_TEC;
    }

    public String getDTH_BLQ() {
        return DTH_BLQ;
    }

    public void setDTH_BLQ(String DTH_BLQ) {
        this.DTH_BLQ = DTH_BLQ;
    }

    public String getZAR_TEC() {
        return ZAR_TEC;
    }

    public void setZAR_TEC(String ZAR_TEC) {
        this.ZAR_TEC = ZAR_TEC;
    }

    public String getZAR_BLQ() {
        return ZAR_BLQ;
    }

    public void setZAR_BLQ(String ZAR_BLQ) {
        this.ZAR_BLQ = ZAR_BLQ;
    }

    public String getCOOR_X() {
        return COOR_X;
    }

    public void setCOOR_X(String COOR_X) {
        this.COOR_X = COOR_X;
    }

    public String getCOOR_Y() {
        return COOR_Y;
    }

    public void setCOOR_Y(String COOR_Y) {
        this.COOR_Y = COOR_Y;
    }

    public String getWS_STATUS() {
        return WS_STATUS;
    }

    public void setWS_STATUS(String WS_STATUS) {
        this.WS_STATUS = WS_STATUS;
    }

    public String getWS_ERROR() {
        return WS_ERROR;
    }

    public void setWS_ERROR(String WS_ERROR) {
        this.WS_ERROR = WS_ERROR;
    }

    public ApiResponseBodyFault getClientException() {
        return ClientException;
    }

    public void setClientException(ApiResponseBodyFault clientException) {
        ClientException = clientException;
    }

    public ApiResponseBodyFault getServerException() {
        return ServerException;
    }

    public void setServerException(ApiResponseBodyFault serverException) {
        ServerException = serverException;
    }
}
