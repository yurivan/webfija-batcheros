package pe.com.tdp.ventafija.microservices.service.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import pe.com.tdp.ventafija.microservices.common.dao.OrderRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Order;
import pe.com.tdp.ventafija.microservices.common.util.MessageUtil;
import pe.com.tdp.ventafija.microservices.common.util.enums.UploadWebFileResponse;
import pe.com.tdp.ventafija.microservices.domain.order.UploadResponseData;
import pe.com.tdp.ventafija.microservices.domain.repository.CustomerRepository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class CargaAudioMasivaService {
	
	private static final Logger logger = LogManager.getLogger();
    
	@Autowired
    private OrderRepository orderRepository;
	
	@Autowired
	private OrderService orderService;

    @Autowired
    private MessageUtil messageUtil;

    @Autowired
    private CustomerRepository customerRepository;
    
	//sprint 3 - upload masivo
    public List<UploadResponseData> uploadWebFileMasivo(MultipartFile[] files, String sourceApp, String versionApp) throws IOException {
        List<UploadResponseData> responseList = new ArrayList<UploadResponseData>();
        long tiempoTotalRestrinccion = 0;
        Integer tiempoTotalConvertido = 0;
        if (files == null) {
            throw new FileNotFoundException("The file uploaded can not be read");
        }
        try {
            logger.info("subiendo archivo web masivo");
            //1. Obtenemos los nombres de los archivos
            List<String> fileNames = new ArrayList<String>();
            Map<String, MultipartFile> filesAllMap = new HashMap<String, MultipartFile>();
            Map<String, MultipartFile> filesFoundMap = new HashMap<String, MultipartFile>();

            try{
                String hour_minute = customerRepository.getValorParametro("carga_audio_hora_restriccion");
                String array[] = hour_minute.split(":");
                Integer hora = Integer.parseInt(array[0]);
                Integer minuto = Integer.parseInt(array[1]);
                tiempoTotalRestrinccion = TimeUnit.HOURS.toMinutes(hora) + minuto;
            }catch (Exception e){
                tiempoTotalRestrinccion = 0;
            }

            tiempoTotalConvertido = (int)tiempoTotalRestrinccion;
            // En este barrido realizamos las primeras validaciones
            int numeroOrden = 1;
            for (MultipartFile file : files) {
                String originalName = file.getOriginalFilename();

                String fileName = originalName.substring(0, originalName.indexOf("."));
                boolean correctExtensionGSM = originalName.toLowerCase().endsWith(".gsm");
                boolean correctExtensionPDF = originalName.toLowerCase().endsWith(".pdf");
                // Verificamos lo siguiente
                // 1. Que la extensión del archivo sea .GSM
                // 2. Que el tamaño del archivo sea mayor que 0
                UploadResponseData uploadResponseData = null;
                if (!correctExtensionGSM && !correctExtensionPDF) {
                    uploadResponseData = getUploadResponseData("" + numeroOrden, originalName, "Archivo debe tener extensi\u00f3n .GSM", false);

                } else if (file.getSize() == 0) {
                    uploadResponseData = getUploadResponseData("" + numeroOrden, originalName, "Tama\u00f1o del archivo debe ser mayor a 0", false);

                } else {
                    uploadResponseData = getUploadResponseData("" + numeroOrden, originalName, "", false);
                }
                Date fechaServidor = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                String fechaServConvertida = sdf.format(fechaServidor);

                String estaActivo = orderRepository.getOrderValidaAudio(fileName,tiempoTotalConvertido,fechaServConvertida);

                if(!estaActivo.equalsIgnoreCase("SI")){
                    uploadResponseData.setMensaje("HORA DE CARGA DE AUDIO EXCEDIDA");
                }
                filesAllMap.put(fileName, file);
                fileNames.add(fileName);
                responseList.add(uploadResponseData);
                numeroOrden++;
            }

            //2. Verificamos que los nombres tengan una venta asociada
            List<Order> orders = orderRepository.findAll(fileNames);
            for (Order o : orders) {
                // Si es que el estado del audio se encuentra 1:PENDIENTE entonces se debe adicionar como pendiente para subir archivo
                if (o.getStatusAudio().equals("1")) {
                    filesFoundMap.put(o.getId(), filesAllMap.get(o.getId()));
                }
            }

            //3. Por cada uno de los archivos que tienen asociada una venta registramos en firebase
            int index = 0;
            for (String fileName : fileNames) {
                UploadResponseData uploadResponseData = responseList.get(index);
                MultipartFile file = filesFoundMap.get(fileName);

                if (uploadResponseData.getMensaje().isEmpty()) {
                    if (file == null) {
                        //si es que el archivo no tiene asociado una venta entonces notificamos un error
                        uploadResponseData.setMensaje("Nombre de archivo no coincide con el id de ninguna venta pendiente de subir audio");
                        uploadResponseData.setSatisfactorio(false);
                    } else {
                        //si es que el archivo si tiene asociado una venta entonces notificamos la subida a firebase
                        try {
                            UploadWebFileResponse uploadResponse = orderService.uploadWebFile(file, fileName, sourceApp, versionApp);
                        	if (UploadWebFileResponse.UPLOAD_OK.equals(uploadResponse)) {
                                uploadResponseData.setMensaje("Archivo de audio fue cargado correctamente");
                            } else if (UploadWebFileResponse.UPLOAD_ENVIANDO_HDEC.equals(uploadResponse)) {
                                uploadResponseData.setMensaje("Archivo de audio fue cargado correctamente. Servicio del back no responde, se reintentar\u00e1 el env\u00edo a HDEC para asegurar su venta");
                            }else if (UploadWebFileResponse.UPLOAD_ERROR.equals(uploadResponse)) {
                                uploadResponseData.setMensaje("Ocurrio un error en la trama enviada al Back. Se cancela la venta automáticamente.");
                            }

                            uploadResponseData.setSatisfactorio(true);
                        } catch (Exception e) {
                            logger.error("error al subir el archivo de audio con id: " + fileName, e);
                            String mensajeError = "Ocurrio un error al subir audio. Por favor reintente en unos minutos";
                            String originalName = file.getOriginalFilename();
                            boolean correctExtensionPDF = originalName.toLowerCase().endsWith(".pdf");
                            if (correctExtensionPDF) {
                                mensajeError = messageUtil.getMensajeErrorHDECInterpretado(e, "Ocurrio un error al subir PDF.");
                            }
                            uploadResponseData.setMensaje(mensajeError);
                            uploadResponseData.setSatisfactorio(false);
                        }
                    }
                }

                index++;
            }
            return responseList;

  			/*zipGsm = FileUtils.zip(in, String.format("%s.gsm", orderId));
              googleStorageService.sendZipFile(zipGsm, orderId);*/
        } catch (Exception e) {
            logger.error("error enviar zip", e);
            throw e;
        }
    }
    
    private UploadResponseData getUploadResponseData(String numero, String nombreArchivo, String mensaje, boolean satisfactorio) {
        UploadResponseData uploadResponseData = new UploadResponseData();
        uploadResponseData.setNumero(numero);
        uploadResponseData.setNombreArchivo(nombreArchivo);
        uploadResponseData.setMensaje(mensaje);
        uploadResponseData.setSatisfactorio(satisfactorio);
        return uploadResponseData;
    }
}
