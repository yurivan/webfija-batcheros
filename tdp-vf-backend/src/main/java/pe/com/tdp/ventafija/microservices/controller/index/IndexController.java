package pe.com.tdp.ventafija.microservices.controller.index;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@RestController
public class IndexController implements ErrorController {

    private static final String PATH = "/error";

    @RequestMapping(value = PATH)
    public String error() {
        final HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        final String ip = request.getRemoteAddr();
        return "Tracked action: Your IP address (" + ip +") has been reported and logged as suspiscious. Legal actions might be taken. Powered by Telefonica.";
    }

    @Override
    public String getErrorPath() {
        return PATH;
    }
}