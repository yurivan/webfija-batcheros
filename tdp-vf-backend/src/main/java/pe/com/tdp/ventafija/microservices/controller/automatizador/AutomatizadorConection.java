package pe.com.tdp.ventafija.microservices.controller.automatizador;

import com.google.gson.JsonArray;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.util.IOUtils;
import org.json.JSONArray;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.connection.Api;
import pe.com.tdp.ventafija.microservices.common.encryption.PGPEncryptionHelper;
import pe.com.tdp.ventafija.microservices.domain.automatizador.DetalleEstadoSolicitud;
import pe.com.tdp.ventafija.microservices.domain.automatizador.EstadoSolicitudRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@Service
public class AutomatizadorConection {
    private static final Logger logger = LogManager.getLogger();

    private static String urlDetail="https://api.us.apiconnect.ibmcloud.com/telefonica-del-peru-development/terd/ws-octopus/consultationStateOfSaleInAutomotiveWeb?COD_VTA_APP_CD=120";



    public DetalleEstadoSolicitud loadAutomatizador (String codVTA) {
        logger.info("load firebase object "+codVTA);
        if (codVTA == null) {
            return null;
        }

        URI uri;
        DetalleEstadoSolicitud estadoSolicitud = new DetalleEstadoSolicitud();
        String autoConsultaObj = null;
        Map<String, String> headers;
        headers=cargarHeaders();
        try {
            uri = new URI(urlDetail.replace("{?id}", codVTA));
            logger.info(uri);
            autoConsultaObj = Api.jerseyGET(uri,headers);
            System.out.println(autoConsultaObj);

            if (autoConsultaObj != null) {
                estadoSolicitud = estadoSolicitud.CargarDetalle(autoConsultaObj);
            }

        } catch (URISyntaxException e) {
            logger.error("Error when creating firebase service", e);
        }catch (Exception e) {
            logger.error("Error al desencriptar", e);
        }
        logger.info("loaded object: "+autoConsultaObj);
        return estadoSolicitud;
    }

    public Map<String, String> cargarHeaders(){
        Map<String, String> headers = new HashMap<>();
        headers.put("X-IBM-Client-Id", "c02a3410-1a23-4e3a-b812-b06f8886e004");
        headers.put("X-IBM-Client-Secret", "F7mW4pU3gC7gM7hI3fQ1dU0gX1dX8tV4yJ2wE1sW3eT0aY0oD3");
        headers.put("country", "PE");
        headers.put("lang", "es");
        headers.put("entity", "TDP");
        headers.put("system", "STC");
        headers.put("subsystem", "CRM");
        headers.put("originator", "PE:TDP:STC:CRM");
        headers.put("sender", "OracleServiceBus");
        headers.put("userId", "USERSTC");
        headers.put("wsId", "SistemSTC");
        headers.put("wsIp", "10.10.10.10");
        headers.put("operation", "consultation");
        headers.put("destination", "PE:ZYTRUST:PORTALZT:GATEWAY");
        headers.put("pid", "cba3ea2e-582e-4c07-b34e-618b7b2d5b0a");
        headers.put("execId", "cba3ea2e-582e-4c07-b34e-618b7b2d5b0a");
        headers.put("msgId", "cba3ea2e-582e-4c07-b34e-618b7b2d5b0a");
        headers.put("timestamp", "2018-08-13T15:28:21.988-05:00");
        headers.put("msgType", "REQUEST");
        return headers;
    }

}
