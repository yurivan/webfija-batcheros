package pe.com.tdp.ventafija.microservices.controller.reniec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.customer.BiometricRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.BiometricResponseData;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyResponseData;
import pe.com.tdp.ventafija.microservices.service.reniec.ReniecService;

@RestController
public class ReniecController {

    private static final Logger logger = LogManager.getLogger();

    @Autowired
    private ReniecService reniecService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/identify", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<IdentifyResponseData> identify(@RequestBody IdentifyRequest request) {
        LogVass.startController(logger, "Identify", request);

        Response<IdentifyResponseData> response = new Response<>();
        try {
            response = reniecService.identify(request);
        } catch (Exception e) {
            logger.error("Error Identify Controller", e);
        }
        LogVass.finishController(logger, "Identify", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/customer/biometric", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<BiometricResponseData> biometric(@RequestBody BiometricRequest request) {
        LogVass.startController(logger, "Biometric", request);

        Response<BiometricResponseData> response = new Response<>();
        try {
            response = reniecService.biometric(request);
        } catch (Exception e) {
            logger.error("Error Biometric Controller", e);
        }
        LogVass.finishController(logger, "Biometric", response);
        return response;
    }
}
