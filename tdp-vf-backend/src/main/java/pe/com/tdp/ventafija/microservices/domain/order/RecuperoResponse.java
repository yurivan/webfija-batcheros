package pe.com.tdp.ventafija.microservices.domain.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class RecuperoResponse {
    @JsonProperty("estado")
    private String estado;
    @JsonProperty("idOrder")
    private String idOrder;
    @JsonProperty("motivo")
    private String motivo;
    @JsonProperty("respuesta")
    private String respuesta;

    @JsonProperty("estado")
    public String getEstado() {
        return estado;
    }

    @JsonProperty("estado")
    public void setEstado(String estado) {
        this.estado = estado;
    }

    @JsonProperty("motivo")
    public String getMotivo() {
        return motivo;
    }

    @JsonProperty("motivo")
    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    @JsonProperty("idOrder")
    public String getIdOrder() {
        return idOrder;
    }

    @JsonProperty("idOrder")
    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    @JsonProperty("respuesta")
    public String getRespuesta() {
        return respuesta;
    }

    @JsonProperty("respuesta")
    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
