package pe.com.tdp.ventafija.microservices.domain.customer;

import java.util.List;

// Sprint 6
public class ContractSalesConditionResponseData {

	private List<ContractResponseParameter> conditions;
	private boolean filtroParental;
	private boolean decoSmart;
	
	public List<ContractResponseParameter> getConditions() {
		return conditions;
	}
	
	public void setConditions(List<ContractResponseParameter> conditions) {
		this.conditions = conditions;
	}

	public boolean isFiltroParental() {
		return filtroParental;
	}

	public void setFiltroParental(boolean filtroParental) {
		this.filtroParental = filtroParental;
	}

	public boolean isDecoSmart() {
		return decoSmart;
	}

	public void setDecoSmart(boolean decoSmart) {
		this.decoSmart = decoSmart;
	}
	
}
