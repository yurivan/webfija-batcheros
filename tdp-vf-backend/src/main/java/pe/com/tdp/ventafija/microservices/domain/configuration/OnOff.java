package pe.com.tdp.ventafija.microservices.domain.configuration;

public class OnOff {

    private String hourStart;
    private String hourEnd;
    private String message;
    //private Long dateStart;
    //private Long dateEnd;


    public String getHourStart() {
        return hourStart;
    }

    public void setHourStart(String hourStart) {
        this.hourStart = hourStart;
    }

    public String getHourEnd() {
        return hourEnd;
    }

    public void setHourEnd(String hourEnd) {
        this.hourEnd = hourEnd;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
