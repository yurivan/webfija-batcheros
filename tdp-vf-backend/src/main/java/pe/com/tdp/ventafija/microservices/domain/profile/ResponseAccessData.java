package pe.com.tdp.ventafija.microservices.domain.profile;

public class ResponseAccessData {

    private int id;
    private String name;
    private int nivel;
    private boolean f_venta;
    private boolean f_bandeja;
    private boolean f_recupero;
    private boolean f_reptrack;
    private boolean f_repventa;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public boolean isF_venta() {
        return f_venta;
    }

    public void setF_venta(boolean f_venta) {
        this.f_venta = f_venta;
    }

    public boolean isF_bandeja() {
        return f_bandeja;
    }

    public void setF_bandeja(boolean f_bandeja) {
        this.f_bandeja = f_bandeja;
    }

    public boolean isF_recupero() {
        return f_recupero;
    }

    public void setF_recupero(boolean f_recupero) {
        this.f_recupero = f_recupero;
    }

    public boolean isF_reptrack() {
        return f_reptrack;
    }

    public void setF_reptrack(boolean f_reptrack) {
        this.f_reptrack = f_reptrack;
    }

    public boolean isF_repventa() {
        return f_repventa;
    }

    public void setF_repventa(boolean f_repventa) {
        this.f_repventa = f_repventa;
    }
}
