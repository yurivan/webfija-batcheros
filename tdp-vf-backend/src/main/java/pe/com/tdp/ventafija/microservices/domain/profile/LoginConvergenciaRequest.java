package pe.com.tdp.ventafija.microservices.domain.profile;

public class LoginConvergenciaRequest {

    private String pto_venta;

    public String getPto_venta() {
        return pto_venta;
    }

    public void setPto_venta(String pto_venta) {
        this.pto_venta = pto_venta;
    }
}
