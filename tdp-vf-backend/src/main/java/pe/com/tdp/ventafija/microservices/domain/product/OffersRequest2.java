package pe.com.tdp.ventafija.microservices.domain.product;

import pe.com.tdp.ventafija.microservices.domain.customer.scoring.CustomerScoringResponse;

public class OffersRequest2 {

	private String documentType;
	private String documentNumber;
	private String department;
	private String province;
	private String district;
	private String ubigeoCode;
	private String atisSellerCode;
	private String coordinateX;
	private String coordinateY;
	private String phoneNumber;
	private String zonal;
	private String prodTypeCode;
	private String legacyCode;
	private String productSvcCode;
	private CustomerScoringResponse scoringResponse;
	private String applicationCode;

	//Sprint 10 - se adicionan 3 datos en la consulta: segmento, canal y entidad
	private String sellerSegment;
	private String sellerChannel;
	private String sellerEntity;

	//Sprint 12 - se adicionan 2 datos para el canal y entidad equivalentes de campanias
	private String sellerChannelEquivalentCampaign;
	private String sellerEntityEquivalentCampaign;

	//Sprint 22 - se adiciona orderId
	private String orderId;
	private Boolean flagFtth;

	//Sprint 25 calculadora
	private String psprincipal;
	private String productDescription;

	public Boolean getFlagFtth() {
		return this.flagFtth;
	}

	public void setFlagFtth(Boolean flagFtth) {
		this.flagFtth = flagFtth;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getUbigeoCode() {
		return ubigeoCode;
	}

	public void setUbigeoCode(String ubigeoCode) {
		this.ubigeoCode = ubigeoCode;
	}

	public String getAtisSellerCode() {
		return atisSellerCode;
	}

	public void setAtisSellerCode(String atisSellerCode) {
		this.atisSellerCode = atisSellerCode;
	}

	public String getCoordinateX() {
		return coordinateX;
	}

	public void setCoordinateX(String coordinateX) {
		this.coordinateX = coordinateX;
	}

	public String getCoordinateY() {
		return coordinateY;
	}

	public void setCoordinateY(String coordinateY) {
		this.coordinateY = coordinateY;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getZonal() {
		return zonal;
	}

	public void setZonal(String zonal) {
		this.zonal = zonal;
	}

	public String getProdTypeCode() {
		return prodTypeCode;
	}

	public void setProdTypeCode(String prodTypeCode) {
		this.prodTypeCode = prodTypeCode;
	}

	public String getLegacyCode() {
		return legacyCode;
	}

	public void setLegacyCode(String legacyCode) {
		this.legacyCode = legacyCode;
	}

	public String getProductSvcCode() {
		return productSvcCode;
	}

	public void setProductSvcCode(String productSvcCode) {
		this.productSvcCode = productSvcCode;
	}

	public CustomerScoringResponse getScoringResponse() {
		return scoringResponse;
	}

	public void setScoringResponse(CustomerScoringResponse scoringResponse) {
		this.scoringResponse = scoringResponse;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getSellerSegment() {
		return sellerSegment;
	}

	public void setSellerSegment(String sellerSegment) {
		this.sellerSegment = sellerSegment;
	}

	public String getSellerChannel() {
		return sellerChannel;
	}

	public void setSellerChannel(String sellerChannel) {
		this.sellerChannel = sellerChannel;
	}

	public String getSellerEntity() {
		return sellerEntity;
	}

	public void setSellerEntity(String sellerEntity) {
		this.sellerEntity = sellerEntity;
	}

	public String getSellerChannelEquivalentCampaign() {
		return sellerChannelEquivalentCampaign;
	}

	public void setSellerChannelEquivalentCampaign(String sellerChannelEquivalentCampaign) {
		this.sellerChannelEquivalentCampaign = sellerChannelEquivalentCampaign;
	}

	public String getSellerEntityEquivalentCampaign() {
		return sellerEntityEquivalentCampaign;
	}

	public void setSellerEntityEquivalentCampaign(String sellerEntityEquivalentCampaign) {
		this.sellerEntityEquivalentCampaign = sellerEntityEquivalentCampaign;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getPsprincipal() {
		return psprincipal;
	}

	public void setPsprincipal(String psprincipal) {
		this.psprincipal = psprincipal;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

}
