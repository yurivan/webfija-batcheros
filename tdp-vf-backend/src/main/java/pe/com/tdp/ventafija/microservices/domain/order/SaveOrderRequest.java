package pe.com.tdp.ventafija.microservices.domain.order;

public class SaveOrderRequest {

	private String firebaseId;

	private String sourceProductName;

	public String getFirebaseId() {
		return firebaseId;
	}

	public void setFirebaseId(String firebaseId) {
		this.firebaseId = firebaseId;
	}

	public String getSourceProductName() {
		return sourceProductName;
	}

	public void setSourceProductName(String sourceProductName) {
		this.sourceProductName = sourceProductName;
	}
	
}
