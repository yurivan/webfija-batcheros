package pe.com.tdp.ventafija.microservices.service.configuration;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.dao.ArgumentariosRepository;
import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Argumentarios;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.domain.message.MessageEntities;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.MessageUtil;
import pe.com.tdp.ventafija.microservices.domain.Parameter;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.configuration.*;
import pe.com.tdp.ventafija.microservices.service.customer.CustomerService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class ConfigurationService {
    private static final Logger logger = LogManager.getLogger(CustomerService.class);

    private DateTimeZone dtZone = DateTimeZone.forID("America/Lima");

    @Autowired
    private ParametersRepository parametersRepository;

    @Autowired
    private MessageUtil messageUtil;

    @Autowired
    private ArgumentariosRepository argumentariosRepository;

    public Response<InitialResponse> initial() {
        logger.info("Inicio Initial Service");
        Response<InitialResponse> responseData = new Response<>();
        InitialResponse initialResponse = new InitialResponse();

        try {
            List<Parameters> parametersAgendamientoList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("AGENDAMIENTO", "VAL_AREA");
            List<String> listaAgendamiento = new ArrayList<>();

            for (Parameters items : parametersAgendamientoList) {
                listaAgendamiento.add(items.getStrValue());
            }

            Agendamiento agendamiento = new Agendamiento();
            agendamiento.setDepartamento(listaAgendamiento.get(0));
            agendamiento.setCanal(listaAgendamiento.get(1));
            agendamiento.setTecnologia(listaAgendamiento.get(2));

            initialResponse.setAgendamiento(agendamiento);

            // Obtener la lista de números de teléfonos
            List<Parameters> lista = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("BLACKLIST", "PHONE");
            // Transformar la lista obtenida a una String
            List<String> phone = new ArrayList<>();
            for (Parameters items : lista) {
                phone.add(items.getStrValue());
            }
            // Agregar la lista String de teléfonos al objeto BlackList
            BlackList blackList = new BlackList();
            blackList.setPhone(phone);

            // Obtener el parámetro de normalizador
            Parameters parametersNormalizador = parametersRepository.findOneByDomainAndCategoryAndElement("REINTENTOS", "NORMALIZADOR", "normalizador.reintenos.javascript");
            String javascript = parametersNormalizador.getStrValue() == null ? "3" : parametersNormalizador.getStrValue();

            // Almacenar la lista negra en la variable de blacklist
            initialResponse.setBlackList(blackList);
            // Almacenar la variable de javascript en normalizador
            initialResponse.setNormalizador(javascript);

            //
            List<Parameters> list = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("ORDERREPORT", "ESTADOS");
            List<String> state = new ArrayList<>();

            for (Parameters items : list) {
                state.add(items.getStrValue());
            }

            ReportStates reportStates = new ReportStates();
            reportStates.setState(state);
            initialResponse.setReportStates(reportStates);

            Parameters hourOn = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "AUTH_PARAMETER", "sale_hour_start");
            Parameters hourOff = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "AUTH_PARAMETER", "sale_hour_end");
            OnOff onOff = new OnOff();
            onOff.setHourStart(hourOn.getStrValue());
            onOff.setHourEnd(hourOff.getStrValue());

            DateFormat sdf = new SimpleDateFormat("HH:mm");
            DateFormat df = new SimpleDateFormat("hh:mm aa");
            Date hourOnDate = sdf.parse(onOff.getHourStart());
            Date hourOffDate = sdf.parse(onOff.getHourEnd());

            HashMap<String, MessageEntities> messageList = messageUtil.getMessages("PRENDIDO Y APAGADO DE VENTAS");
            LogVass.serviceResponseHashMap(logger, "initial", "messageList", messageList);
            //String message = "<b>Hola [NOMBRE],</b> recuerda que las ventas sólo pueden ser realizadas durante el horario de gestión<br>De [HORA_INICIO] a [HORA_FIN]"
            String message = messageList.get("errorOnOff").getMessage()
                    .replace("[HORA_INICIO]", df.format(hourOnDate))
                    .replace("[HORA_FIN]", df.format(hourOffDate));

            onOff.setMessage(message);
            initialResponse.setOnOff(onOff);

            WhatsApp whatsApp = new WhatsApp();
            List<Parameters> whatsAppOnOff = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("WHATSAPP", "SWITCH");
            for (Parameters parameters : whatsAppOnOff) {
                if (parameters.getElement().equalsIgnoreCase("whatsapp.contract")) {
                    whatsApp.setOnOff(Boolean.parseBoolean(parameters.getStrValue()));
                }
                if (parameters.getElement().equalsIgnoreCase("whatsapp.canales")) {
                    whatsApp.setCanal(parameters.getStrValue());
                }
            }
            initialResponse.setWhatsApp(whatsApp);

            responseData.setResponseData(initialResponse);

        } catch (Exception e) {
            logger.error("Error Initial Service", e);
            initialResponse.setBlackList(new BlackList());
            initialResponse.setNormalizador("3");
        }
        logger.info("Fin Initial Service");
        return responseData;
    }

    public Response<String> fecha() {
        logger.info("Inicio Fecha Service");
        Response<String> responseData = new Response<>();

        try {
            DateTime dt = new DateTime(new Date());
            DateTime dtus = dt.withZone(dtZone);
            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            responseData.setResponseCode("01");
            responseData.setResponseData(formato.format(dtus.toDate()));
        } catch (Exception e) {
            responseData.setResponseCode("00");
            responseData.setResponseData("");
        }
        logger.info("Fin Fecha Service");
        return responseData;
    }

    public Response<List<Argumentarios>> argumentarios(ArgumentariosRequest perfil) {
        Response<List<Argumentarios>> data = new Response<>();
        List<Argumentarios>  listArgumentario= null;
        List<Argumentarios>  listArgumentarioComp = null;
        String general="GENERAL";
        String nivel="COMPLETO";
        String call="CALL CENTER";

        listArgumentarioComp =argumentariosRepository.findByNivel(nivel);

        if (perfil.getPerfil().equals("CALL CENTER")){

            listArgumentario = argumentariosRepository.findByCanal(perfil.getPerfil());

        }else {
            listArgumentario = argumentariosRepository.findByCanal(general);
        }

        listArgumentarioComp.addAll(listArgumentario);
        data.setResponseData(listArgumentarioComp);
        return data;
    }


    public Response<Parameters> canalentidad(CanalEntidadRequest canal) {
        String category="FICHAPRODUCTO.CANAL";
        Response<Parameters> data = new Response<>();
        Parameters canalEntidad= new Parameters();
        List<Parameters> listCanal =null;

        listCanal = parametersRepository.findByCategory(category);

        for (Parameters item : listCanal){
            if(item.getStrValue().contains(canal.getCanal())){
                canalEntidad=item;
            }
        }


        if (canalEntidad.getId() != null){
            data.setResponseData(canalEntidad);
        }else{
            canalEntidad = canalEntidad.CargaDefault();
            data.setResponseData(canalEntidad);
            data.setResponseMessage("DEFAULT");
        }

        return data;
    }
}
