package pe.com.tdp.ventafija.microservices.domain.outcall;

public class OrderOutCall {
    private String statusaudio;
    private String entidad;
    private String nompuntoventa;
    private String id;

    public String getStatusaudio() {
        return statusaudio;
    }

    public void setStatusaudio(String statusaudio) {
        this.statusaudio = statusaudio;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getNompuntoventa() {
        return nompuntoventa;
    }

    public void setNompuntoventa(String nompuntoventa) {
        this.nompuntoventa = nompuntoventa;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
