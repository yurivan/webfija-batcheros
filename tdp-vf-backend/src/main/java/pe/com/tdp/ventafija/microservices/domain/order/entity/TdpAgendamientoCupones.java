package pe.com.tdp.ventafija.microservices.domain.order.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.persistence.Column;
import javax.persistence.Id;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TdpAgendamientoCupones {

    @Id
    @Column(name = "cupones_id")
    private Integer cupones_id;

    @Column(name = "cupones_franja")
    private String cupones_franja;

    @Column(name = "cupones_fecha")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    private Date cupones_fecha;

    @Column(name = "cupones")
    private Integer cupones;

    @Column(name = "cupones_utilizados")
    private Integer cupones_utilizados;

    @Column(name = "auditoria_create")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    private Date auditoria_create;

    @Column(name = "auditoria_modify")
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    private Date auditoria_modify;

    public Integer getCupones_id() {
        return cupones_id;
    }

    public void setCupones_id(Integer cupones_id) {
        this.cupones_id = cupones_id;
    }

    public String getCupones_franja() {
        return cupones_franja;
    }

    public void setCupones_franja(String cupones_franja) {
        this.cupones_franja = cupones_franja;
    }

    public Date getCupones_fecha() {
        return cupones_fecha;
    }

    public void setCupones_fecha(Date cupones_fecha) {
        this.cupones_fecha = cupones_fecha;
    }

    public Integer getCupones() {
        return cupones;
    }

    public void setCupones(Integer cupones) {
        this.cupones = cupones;
    }

    public Integer getCupones_utilizados() {
        return cupones_utilizados;
    }

    public void setCupones_utilizados(Integer cupones_utilizados) {
        this.cupones_utilizados = cupones_utilizados;
    }

    public Date getAuditoria_create() {
        return auditoria_create;
    }

    public void setAuditoria_create(Date auditoria_create) {
        this.auditoria_create = auditoria_create;
    }

    public Date getAuditoria_modify() {
        return auditoria_modify;
    }

    public void setAuditoria_modify(Date auditoria_modify) {
        this.auditoria_modify = auditoria_modify;
    }
}
