package pe.com.tdp.ventafija.microservices.domain.customer;

public class BiometricResponseData {

    /* Datos del Cliente */
    private String customerName;
    private String customerFatherLastName;
    private String customerMotherLastName;
    private String customerBirthDate;

    /* Datos de la biometría */
    private int validationTransactionID;
    private String validationResult;
    private String idScoringQuery;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerFatherLastName() {
        return customerFatherLastName;
    }

    public void setCustomerFatherLastName(String customerFatherLastName) {
        this.customerFatherLastName = customerFatherLastName;
    }

    public String getCustomerMotherLastName() {
        return customerMotherLastName;
    }

    public void setCustomerMotherLastName(String customerMotherLastName) {
        this.customerMotherLastName = customerMotherLastName;
    }

    public String getCustomerBirthDate() {
        return customerBirthDate;
    }

    public void setCustomerBirthDate(String customerBirthDate) {
        this.customerBirthDate = customerBirthDate;
    }

    public int getValidationTransactionID() {
        return validationTransactionID;
    }

    public void setValidationTransactionID(int validationTransactionID) {
        this.validationTransactionID = validationTransactionID;
    }

    public String getValidationResult() {
        return validationResult;
    }

    public void setValidationResult(String validationResult) {
        this.validationResult = validationResult;
    }

    public String getIdScoringQuery() {
        return idScoringQuery;
    }

    public void setIdScoringQuery(String idScoringQuery) {
        this.idScoringQuery = idScoringQuery;
    }
}
