package pe.com.tdp.ventafija.microservices.domain.customer;

import java.math.BigDecimal;

public class ContractWebRequestOffering {
	private String productCode;
	private String productName;
	private String discount;
	private BigDecimal price;
	private BigDecimal promPrice;
	private int monthPeriod;
	private BigDecimal financingCost;
	private int financingMonth;
	private BigDecimal installCost;
	private int returnMonth;
	private String returnPeriod;
	private String equipmentType;
	private String lineType;
	private BigDecimal cashPrice;
	private int internetSpeed;
	private String productTypeCode;
	private String productType;
	private String commercialOperation;
	private String paymentMethod;
	private String campaign;
	private String internetTech;
	private String tvSignal;
	private String tvTech;
	private String productCategoryCode;
	private String productCategory;
	private String expertCode;
	private int id;
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getDiscount() {
		return discount;
	}
	public void setDiscount(String discount) {
		this.discount = discount;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getPromPrice() {
		return promPrice;
	}
	public void setPromPrice(BigDecimal promPrice) {
		this.promPrice = promPrice;
	}
	public int getMonthPeriod() {
		return monthPeriod;
	}
	public void setMonthPeriod(int monthPeriod) {
		this.monthPeriod = monthPeriod;
	}
	public BigDecimal getFinancingCost() {
		return financingCost;
	}
	public void setFinancingCost(BigDecimal financingCost) {
		this.financingCost = financingCost;
	}
	public int getFinancingMonth() {
		return financingMonth;
	}
	public void setFinancingMonth(int financingMonth) {
		this.financingMonth = financingMonth;
	}
	public BigDecimal getInstallCost() {
		return installCost;
	}
	public void setInstallCost(BigDecimal installCost) {
		this.installCost = installCost;
	}
	public int getReturnMonth() {
		return returnMonth;
	}
	public void setReturnMonth(int returnMonth) {
		this.returnMonth = returnMonth;
	}
	public String getReturnPeriod() {
		return returnPeriod;
	}
	public void setReturnPeriod(String returnPeriod) {
		this.returnPeriod = returnPeriod;
	}
	public String getEquipmentType() {
		return equipmentType;
	}
	public void setEquipmentType(String equipmentType) {
		this.equipmentType = equipmentType;
	}
	public String getLineType() {
		return lineType;
	}
	public void setLineType(String lineType) {
		this.lineType = lineType;
	}
	public BigDecimal getCashPrice() {
		return cashPrice;
	}
	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}
	public int getInternetSpeed() {
		return internetSpeed;
	}
	public void setInternetSpeed(int internetSpeed) {
		this.internetSpeed = internetSpeed;
	}
	public String getProductTypeCode() {
		return productTypeCode;
	}
	public void setProductTypeCode(String productTypeCode) {
		this.productTypeCode = productTypeCode;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getCommercialOperation() {
		return commercialOperation;
	}
	public void setCommercialOperation(String commercialOperation) {
		this.commercialOperation = commercialOperation;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getCampaign() {
		return campaign;
	}
	public void setCampaign(String campaign) {
		this.campaign = campaign;
	}
	public String getInternetTech() {
		return internetTech;
	}
	public void setInternetTech(String internetTech) {
		this.internetTech = internetTech;
	}
	public String getTvSignal() {
		return tvSignal;
	}
	public void setTvSignal(String tvSignal) {
		this.tvSignal = tvSignal;
	}
	public String getTvTech() {
		return tvTech;
	}
	public void setTvTech(String tvTech) {
		this.tvTech = tvTech;
	}
	public String getProductCategoryCode() {
		return productCategoryCode;
	}
	public void setProductCategoryCode(String productCategoryCode) {
		this.productCategoryCode = productCategoryCode;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	public String getExpertCode() {
		return expertCode;
	}
	public void setExpertCode(String expertCode) {
		this.expertCode = expertCode;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
