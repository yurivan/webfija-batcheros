package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.com.tdp.ventafija.microservices.domain.motorizado.entity.TdpMotorizadoBase;

import java.util.List;

public interface TdpMotorizadoBaseRepository extends JpaRepository<TdpMotorizadoBase, String> {

    List<TdpMotorizadoBase> findAllByCodMotorizadoAndEstado(String codMotorizado, String estado);

    TdpMotorizadoBase findOneByCodMotorizadoAndCodigoPedidoAndNumDocCliente(String codMotorizado, String codigoPedido, String numDocCliente);

    List<TdpMotorizadoBase> findAllByCodigoPedidoAndNumDocCliente(String codigoPedido, String numDocCliente);
}
