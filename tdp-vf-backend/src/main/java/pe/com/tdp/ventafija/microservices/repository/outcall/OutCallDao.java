package pe.com.tdp.ventafija.microservices.repository.outcall;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.outcall.OrderOutCall;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@Repository
public class OutCallDao {
    private static final Logger logger = LogManager.getLogger();
    @Value("${datasource.query.select}")
    private String MY_SQL_QUERY_WHERE;
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;

    @Autowired
    private DataSource datasource;

    public OrderOutCall getDateOrderToId(String order_id) {

        OrderOutCall dateOrder = new OrderOutCall();

        try (Connection con = Database.datasource().getConnection()) {

            String query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "statusaudio,entidad,nompuntoventa,id")
                    .replace(Constants.MY_SQL_TABLE, "order")
                    .replace(Constants.MY_SQL_WHERE, "id='" + order_id + "'");

            if (!query.equals("")) {

                try (PreparedStatement stmt = con.prepareStatement(query)) {
                    LogVass.daoQuery(logger, stmt.toString());

                    ResultSet rs = stmt.executeQuery();
                    while (rs.next()) {
                        dateOrder.setStatusaudio(rs.getString("statusaudio"));
                        dateOrder.setEntidad(rs.getString("entidad"));
                        dateOrder.setNompuntoventa(rs.getString("nompuntoventa"));
                        dateOrder.setId(rs.getString("id"));
                    }
                }
            }
        } catch (Exception e) {
            logger.info("Error getDateOrderToId DAO.", e);
        }
        return dateOrder;
    }


}
