package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pe.com.tdp.ventafija.microservices.domain.message.TdpErrorMessageServiceEntities;

import java.util.List;

public interface TdpErrorMessageServiceRepository extends JpaRepository<TdpErrorMessageServiceEntities, Integer> {

	List<TdpErrorMessageServiceEntities> findByServiceOrderByOrdenAsc(String service);

	TdpErrorMessageServiceEntities findByServiceAndServiceKey(String service, String serviceKey);
}
