package pe.com.tdp.ventafija.microservices.service.product.command.gis;

import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;

import java.util.Map;

@Component("filterMonoTV")
public class MonoTVFilterCommand implements GisFilterCommand {
    // Filtro para MONO TV (6)
    private static String MONOTV_FILTER = " (OFERTA.targetData = '6' AND CAT.tvTech = '%s')";

    @Override
    public void execute(WSSIGFFTT_FFTT_AVEResult resultGis, Map<String, String> variables, StringBuilder queryBuilder, StringBuilder columnsBuilder) {
        String hasTecnologiaCoaxial = resultGis.getCOAXIAL_TEC();

        String tecnologiaTelevision = "SI".equals(hasTecnologiaCoaxial) ? "CATV" : "DTH";
        String filtro = String.format(MONOTV_FILTER, tecnologiaTelevision);
        queryBuilder.append(filtro);
    }
}
