package pe.com.tdp.ventafija.microservices.domain.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class SvaV2ResponseDataSva {

    private int id;
    private String code;
    private String description;
    @JsonInclude(Include.NON_NULL)
    private String type;
    private String unit;
    private double cost;
    private String paymentDescription;
    private int financingMonth;
    private String hasEquipment;
    private String maxDecos;
    private double arpu;
    private String velInter;
    private double financedAmount;
    private int financedQuotas;
    private String productCode;

    private boolean altaPuraDefaultSVA;

    private String bloquePadre;
    private boolean isClickPadre;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getPaymentDescription() {
        return paymentDescription;
    }

    public void setPaymentDescription(String paymentDescription) {
        this.paymentDescription = paymentDescription;
    }

    public int getFinancingMonth() {
        return financingMonth;
    }

    public void setFinancingMonth(int financingMonth) {
        this.financingMonth = financingMonth;
    }

    public String getHasEquipment() {
        return hasEquipment;
    }

    public void setHasEquipment(String hasEquipment) {
        this.hasEquipment = hasEquipment;
    }

    public String getMaxDecos() {
        return maxDecos;
    }

    public void setMaxDecos(String maxDecos) {
        this.maxDecos = maxDecos;
    }

    public double getArpu() {
        return arpu;
    }

    public void setArpu(double arpu) {
        this.arpu = arpu;
    }

    public String getVelInter() {
        return velInter;
    }

    public void setVelInter(String velInter) {
        this.velInter = velInter;
    }

    public double getFinancedAmount() {
        return financedAmount;
    }

    public void setFinancedAmount(double financedAmount) {
        this.financedAmount = financedAmount;
    }

    public int getFinancedQuotas() {
        return financedQuotas;
    }

    public void setFinancedQuotas(int financedQuotas) {
        this.financedQuotas = financedQuotas;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public boolean isAltaPuraDefaultSVA() {
        return altaPuraDefaultSVA;
    }

    public void setAltaPuraDefaultSVA(boolean altaPuraDefaultSVA) {
        this.altaPuraDefaultSVA = altaPuraDefaultSVA;
    }

    public String getBloquePadre() {
        return bloquePadre;
    }

    public void setBloquePadre(String bloquePadre) {
        this.bloquePadre = bloquePadre;
    }

    public boolean isClickPadre() {
        return isClickPadre;
    }

    public void setClickPadre(boolean clickPadre) {
        isClickPadre = clickPadre;
    }
}