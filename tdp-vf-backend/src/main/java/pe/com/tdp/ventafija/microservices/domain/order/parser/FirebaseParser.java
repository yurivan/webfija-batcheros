package pe.com.tdp.ventafija.microservices.domain.order.parser;

import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;

public interface FirebaseParser<T> {

	T parse(SaleApiFirebaseResponseBody firebaseObj);
}
