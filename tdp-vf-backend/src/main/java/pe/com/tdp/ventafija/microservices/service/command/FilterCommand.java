package pe.com.tdp.ventafija.microservices.service.command;

import java.util.Map;

public interface FilterCommand<T> {
    public void execute(T object, Map<String, String> variables, StringBuilder whereBuilder, StringBuilder columnsBuilder);
}
