package pe.com.tdp.ventafija.microservices.domain.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_catalog_out", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema)
public class TdpCatalogOut {
    @Id
    @Column(name = "id_catalog_out")
    public Integer id_catalog_out;
    @Column(name="nom_accion")
    private String nom_accion;
    @Column(name="cod_prod_oferta")
    private String cod_prod_oferta;
    @Column(name="nom_prod_oferta")
    private String nom_prod_oferta;
    @Column(name="nom_descuento_oferta")
    private String nom_descuento_oferta;
    @Column(name="nom_promocion_oferta")
    private String nom_promocion_oferta;
    @Column(name="renta_origen")
    private String renta_origen;
    @Column(name="salto_prod_oferta")
    private String salto_prod_oferta;
    @Column(name="renta_destino")
    private String renta_destino;
    @Column(name="renta_destino_deco")
    private String renta_destino_deco;
    @Column(name="tx_nombre")
    private String tx_nombre;
    @Column(name="numero_telefono")
    private String numero_telefono;
    @Column(name="celular")
    private String celular;
    @Column(name="tipo_documento")
    private String tipo_documento;
    @Column(name="numero_documento")
    private String numero_documento;
    @Column(name="tipo_persona")
    private String tipo_persona;
    @Column(name="local_comercial")
    private String local_comercial;
    @Column(name="tx_nombre_decisor")
    private String tx_nombre_decisor;
    @Column(name="segmento")
    private String segmento;
    @Column(name="ciclo")
    private String ciclo;
    @Column(name="lima_prov")
    private String lima_prov;
    @Column(name="departamento")
    private String departamento;
    @Column(name="distrito")
    private String distrito;
    @Column(name="tx_direccion")
    private String tx_direccion;
    @Column(name="nse")
    private String nse;
    @Column(name="tenencia")
    private String tenencia;
    @Column(name="tenencia_adicional")
    private String tenencia_adicional;
    @Column(name="renta_excesos")
    private String renta_excesos;
    @Column(name="cluster")
    private String cluster;
    @Column(name="propension")
    private String propension;
    @Column(name="tx_otros1")
    private String tx_otros1;
    @Column(name="tx_otros2")
    private String tx_otros2;
    @Column(name="tx_otros3")
    private String tx_otros3;
    @Column(name="tx_otros4")
    private String tx_otros4;
    @Column(name="tx_otros5")
    private String tx_otros5;
    @Column(name="cd_cross_oferta1")
    private String cd_cross_oferta1;
    @Column(name="tx_cross_oferta1")
    private String tx_cross_oferta1;
    @Column(name="cd_cross_oferta2")
    private String cd_cross_oferta2;
    @Column(name="tx_cross_oferta2")
    private String tx_cross_oferta2;
    @Column(name="cd_cross_oferta3")
    private String cd_cross_oferta3;
    @Column(name="tx_cross_oferta3")
    private String tx_cross_oferta3;
    @Column(name="cd_cross_oferta4")
    private String cd_cross_oferta4;
    @Column(name="tx_cross_oferta4")
    private String tx_cross_oferta4;
    @Column(name="tipo_archivo")
    private String tipo_archivo;
    @Column(name="fecha")
    private String fecha;

    public Integer getId_catalog_out() {
        return id_catalog_out;
    }

    public void setId_catalog_out(Integer id_catalog_out) {
        this.id_catalog_out = id_catalog_out;
    }

    public String getNom_accion() {
        return nom_accion;
    }

    public void setNom_accion(String nom_accion) {
        this.nom_accion = nom_accion;
    }

    public String getCod_prod_oferta() {
        return cod_prod_oferta;
    }

    public void setCod_prod_oferta(String cod_prod_oferta) {
        this.cod_prod_oferta = cod_prod_oferta;
    }

    public String getNom_prod_oferta() {
        return nom_prod_oferta;
    }

    public void setNom_prod_oferta(String nom_prod_oferta) {
        this.nom_prod_oferta = nom_prod_oferta;
    }

    public String getNom_descuento_oferta() {
        return nom_descuento_oferta;
    }

    public void setNom_descuento_oferta(String nom_descuento_oferta) {
        this.nom_descuento_oferta = nom_descuento_oferta;
    }

    public String getNom_promocion_oferta() {
        return nom_promocion_oferta;
    }

    public void setNom_promocion_oferta(String nom_promocion_oferta) {
        this.nom_promocion_oferta = nom_promocion_oferta;
    }

    public String getRenta_origen() {
        return renta_origen;
    }

    public void setRenta_origen(String renta_origen) {
        this.renta_origen = renta_origen;
    }

    public String getSalto_prod_oferta() {
        return salto_prod_oferta;
    }

    public void setSalto_prod_oferta(String salto_prod_oferta) {
        this.salto_prod_oferta = salto_prod_oferta;
    }

    public String getRenta_destino() {
        return renta_destino;
    }

    public void setRenta_destino(String renta_destino) {
        this.renta_destino = renta_destino;
    }

    public String getRenta_destino_deco() {
        return renta_destino_deco;
    }

    public void setRenta_destino_deco(String renta_destino_deco) {
        this.renta_destino_deco = renta_destino_deco;
    }

    public String getTx_nombre() {
        return tx_nombre;
    }

    public void setTx_nombre(String tx_nombre) {
        this.tx_nombre = tx_nombre;
    }

    public String getNumero_telefono() {
        return numero_telefono;
    }

    public void setNumero_telefono(String numero_telefono) {
        this.numero_telefono = numero_telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getNumero_documento() {
        return numero_documento;
    }

    public void setNumero_documento(String numero_documento) {
        this.numero_documento = numero_documento;
    }

    public String getTipo_persona() {
        return tipo_persona;
    }

    public void setTipo_persona(String tipo_persona) {
        this.tipo_persona = tipo_persona;
    }

    public String getLocal_comercial() {
        return local_comercial;
    }

    public void setLocal_comercial(String local_comercial) {
        this.local_comercial = local_comercial;
    }

    public String getTx_nombre_decisor() {
        return tx_nombre_decisor;
    }

    public void setTx_nombre_decisor(String tx_nombre_decisor) {
        this.tx_nombre_decisor = tx_nombre_decisor;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getCiclo() {
        return ciclo;
    }

    public void setCiclo(String ciclo) {
        this.ciclo = ciclo;
    }

    public String getLima_prov() {
        return lima_prov;
    }

    public void setLima_prov(String lima_prov) {
        this.lima_prov = lima_prov;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getTx_direccion() {
        return tx_direccion;
    }

    public void setTx_direccion(String tx_direccion) {
        this.tx_direccion = tx_direccion;
    }

    public String getNse() {
        return nse;
    }

    public void setNse(String nse) {
        this.nse = nse;
    }

    public String getTenencia() {
        return tenencia;
    }

    public void setTenencia(String tenencia) {
        this.tenencia = tenencia;
    }

    public String getTenencia_adicional() {
        return tenencia_adicional;
    }

    public void setTenencia_adicional(String tenencia_adicional) {
        this.tenencia_adicional = tenencia_adicional;
    }

    public String getRenta_excesos() {
        return renta_excesos;
    }

    public void setRenta_excesos(String renta_excesos) {
        this.renta_excesos = renta_excesos;
    }

    public String getCluster() {
        return cluster;
    }

    public void setCluster(String cluster) {
        this.cluster = cluster;
    }

    public String getPropension() {
        return propension;
    }

    public void setPropension(String propension) {
        this.propension = propension;
    }

    public String getTx_otros1() {
        return tx_otros1;
    }

    public void setTx_otros1(String tx_otros1) {
        this.tx_otros1 = tx_otros1;
    }

    public String getTx_otros2() {
        return tx_otros2;
    }

    public void setTx_otros2(String tx_otros2) {
        this.tx_otros2 = tx_otros2;
    }

    public String getTx_otros3() {
        return tx_otros3;
    }

    public void setTx_otros3(String tx_otros3) {
        this.tx_otros3 = tx_otros3;
    }

    public String getTx_otros4() {
        return tx_otros4;
    }

    public void setTx_otros4(String tx_otros4) {
        this.tx_otros4 = tx_otros4;
    }

    public String getTx_otros5() {
        return tx_otros5;
    }

    public void setTx_otros5(String tx_otros5) {
        this.tx_otros5 = tx_otros5;
    }

    public String getCd_cross_oferta1() {
        return cd_cross_oferta1;
    }

    public void setCd_cross_oferta1(String cd_cross_oferta1) {
        this.cd_cross_oferta1 = cd_cross_oferta1;
    }

    public String getTx_cross_oferta1() {
        return tx_cross_oferta1;
    }

    public void setTx_cross_oferta1(String tx_cross_oferta1) {
        this.tx_cross_oferta1 = tx_cross_oferta1;
    }

    public String getCd_cross_oferta2() {
        return cd_cross_oferta2;
    }

    public void setCd_cross_oferta2(String cd_cross_oferta2) {
        this.cd_cross_oferta2 = cd_cross_oferta2;
    }

    public String getTx_cross_oferta2() {
        return tx_cross_oferta2;
    }

    public void setTx_cross_oferta2(String tx_cross_oferta2) {
        this.tx_cross_oferta2 = tx_cross_oferta2;
    }

    public String getCd_cross_oferta3() {
        return cd_cross_oferta3;
    }

    public void setCd_cross_oferta3(String cd_cross_oferta3) {
        this.cd_cross_oferta3 = cd_cross_oferta3;
    }

    public String getTx_cross_oferta3() {
        return tx_cross_oferta3;
    }

    public void setTx_cross_oferta3(String tx_cross_oferta3) {
        this.tx_cross_oferta3 = tx_cross_oferta3;
    }

    public String getCd_cross_oferta4() {
        return cd_cross_oferta4;
    }

    public void setCd_cross_oferta4(String cd_cross_oferta4) {
        this.cd_cross_oferta4 = cd_cross_oferta4;
    }

    public String getTx_cross_oferta4() {
        return tx_cross_oferta4;
    }

    public void setTx_cross_oferta4(String tx_cross_oferta4) {
        this.tx_cross_oferta4 = tx_cross_oferta4;
    }

    public String getTipo_archivo() {
        return tipo_archivo;
    }

    public void setTipo_archivo(String tipo_archivo) {
        this.tipo_archivo = tipo_archivo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
