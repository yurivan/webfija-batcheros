package pe.com.tdp.ventafija.microservices.domain.order;

public class UserRequest {

	private String vendorId;
	
	private int numDays;
	
	public UserRequest() {}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public int getNumDays() {
		return numDays;
	}

	public void setNumDays(int numDays) {
		this.numDays = numDays;
	}
	
	
}
