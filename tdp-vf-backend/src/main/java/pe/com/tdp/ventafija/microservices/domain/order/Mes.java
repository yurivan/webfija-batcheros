package pe.com.tdp.ventafija.microservices.domain.order;

import java.util.List;

public class Mes {
    private String order;
    private String anio;
    private String mes;
    private String name;
    private String color;
    private String clientestotal;
    private List<Estado> estados;
    private List<Dia> dias;

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getAnio() {
        return anio;
    }

    public void setAnio(String anio) {
        this.anio = anio;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getClientestotal() {
        return clientestotal;
    }

    public void setClientestotal(String clientestotal) {
        this.clientestotal = clientestotal;
    }

    public List<Estado> getEstados() {
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public List<Dia> getDias() {
        return dias;
    }

    public void setDias(List<Dia> dias) {
        this.dias = dias;
    }
}
