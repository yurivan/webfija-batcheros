package pe.com.tdp.ventafija.microservices.domain.order;

import pe.com.tdp.ventafija.microservices.domain.Cell;
import pe.com.tdp.ventafija.microservices.domain.Parameter;

import java.util.List;

public class SalesReportTypeResponse {

    private List<String> activeFilterList;
    private List<Cell> activeFilterData;
    private List<ReporteVenta> reporteVentaList;

    public List<Cell> getActiveFilterData() { return activeFilterData; }

    public void setActiveFilterData(List<Cell> activeFilterData) { this.activeFilterData = activeFilterData; }

    public List<String> getActiveFilterList() {
        return activeFilterList;
    }

    public void setActiveFilterList(List<String> activeFilterList) {
        this.activeFilterList = activeFilterList;
    }

    public List<ReporteVenta> getReporteVentaList() {
        return reporteVentaList;
    }

    public void setReporteVentaList(List<ReporteVenta> reporteVentaList) {
        this.reporteVentaList = reporteVentaList;
    }
}
