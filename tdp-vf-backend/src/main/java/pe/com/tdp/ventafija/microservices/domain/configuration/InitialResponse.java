package pe.com.tdp.ventafija.microservices.domain.configuration;

public class InitialResponse {

    private BlackList blackList;
    private String normalizador;
    private ReportStates reportStates;
    private Agendamiento agendamiento;
    private OnOff onOff;
    private WhatsApp whatsApp;

    public Agendamiento getAgendamiento() {
        return agendamiento;
    }

    public void setAgendamiento(Agendamiento agendamiento) {
        this.agendamiento = agendamiento;
    }

    public BlackList getBlackList() {
        return blackList;
    }

    public void setBlackList(BlackList blackList) {
        this.blackList = blackList;
    }

    public String getNormalizador() {
        return normalizador;
    }

    public void setNormalizador(String normalizador) {
        this.normalizador = normalizador;
    }

    public ReportStates getReportStates() {
        return reportStates;
    }

    public void setReportStates(ReportStates reportStates) {
        this.reportStates = reportStates;
    }

    public OnOff getOnOff() {
        return onOff;
    }

    public void setOnOff(OnOff onOff) {
        this.onOff = onOff;
    }

    public WhatsApp getWhatsApp() {
        return whatsApp;
    }

    public void setWhatsApp(WhatsApp whatsApp) {
        this.whatsApp = whatsApp;
    }
}
