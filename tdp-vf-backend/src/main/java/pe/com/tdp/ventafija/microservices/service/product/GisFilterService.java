package pe.com.tdp.ventafija.microservices.service.product;

import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;

/**
 * Service de facilidades tecnicas GIS
 *
 */
public interface GisFilterService {

    /**
     * Obtiene el filtro para aplicar en la ficha de productos basado facilidades tecnicas basadas en el response de GIS
     * @param resultGis response de facilidades tecnicas de GIS
     * @param price precio filtro
     * @return filtro para aplicar en la ficha de productos
     */
    public String getFilter(WSSIGFFTT_FFTT_AVEResult resultGis, String price);
}
