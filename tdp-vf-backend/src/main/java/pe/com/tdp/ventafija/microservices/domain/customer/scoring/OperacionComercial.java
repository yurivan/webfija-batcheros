package pe.com.tdp.ventafija.microservices.domain.customer.scoring;

import java.util.ArrayList;
import java.util.List;

public class OperacionComercial {
	private String CodOpe;
	private String Renta;
	private String Segmento;
	private float ContadorDetalle;
	private List<OperacionComercialDetalle> detalle;

	public OperacionComercial() {
		super();
		detalle = new ArrayList<>();
	}

	public OperacionComercial(String codOpe, String renta, String segmento, float contadorDetalle,
			List<OperacionComercialDetalle> detalle) {
		super();
		CodOpe = codOpe;
		Renta = renta;
		Segmento = segmento;
		ContadorDetalle = contadorDetalle;
		this.detalle = detalle;
	}

	public String getCodOpe() {
		return CodOpe;
	}

	public void setCodOpe(String codOpe) {
		CodOpe = codOpe;
	}

	public String getRenta() {
		return Renta;
	}

	public void setRenta(String renta) {
		Renta = renta;
	}

	public String getSegmento() {
		return Segmento;
	}

	public void setSegmento(String segmento) {
		Segmento = segmento;
	}

	public float getContadorDetalle() {
		return ContadorDetalle;
	}

	public void setContadorDetalle(float contadorDetalle) {
		ContadorDetalle = contadorDetalle;
	}

	public List<OperacionComercialDetalle> getDetalle() {
		return detalle;
	}

	public void setDetalle(List<OperacionComercialDetalle> detalle) {
		this.detalle = detalle;
	}

}
