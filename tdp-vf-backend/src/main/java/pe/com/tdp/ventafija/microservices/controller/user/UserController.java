package pe.com.tdp.ventafija.microservices.controller.user;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.LoginUtil;
import pe.com.tdp.ventafija.microservices.constants.user.UserConstants;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.profile.ConvergenciaRequest;
import pe.com.tdp.ventafija.microservices.domain.profile.ConvergenciaResponse;
import pe.com.tdp.ventafija.microservices.domain.profile.LoginConvergenciaResponse;
import pe.com.tdp.ventafija.microservices.domain.user.*;
import pe.com.tdp.ventafija.microservices.service.user.UserService;

import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private UserService userService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/login", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<LoginResponseToken> login(@RequestBody LoginRequest request) {
        LogVass.startController(logger, "Login", request);

        Response<LoginResponseToken> responseData = new Response<>();
        LoginResponseToken responseToken = new LoginResponseToken();
        try {
            Response<LoginResponseData> responseLogin  = userService.login(request);

            if (UserConstants.LOGIN_RESPONSE_CODE_OK.equals(responseLogin.getResponseCode())) {

                Map<String, Object> payload = new HashMap<String, Object>();
                payload.put("username", request.getCodAtis());

                //entonces generamos el token
                Response<LoginConvergenciaResponse> response = new Response<>();
                try {
                    response =  userService.getLoginConvergencia(responseLogin.getResponseData().getGroup());
                }catch (Exception e) {
                    logger.error("Error getLoginConvergencia Controller", e);
                }
                if(responseLogin.getResponseData().getSellerChannelEquivalentCampaign().equalsIgnoreCase("RETAIL") && response.getResponseCode().equals("0")){
                    String token = LoginUtil.generarToken(payload);
                    responseToken.setToken(token);
                }else{
                    responseToken.setToken("");
                }

                responseToken.setResponseData(responseLogin.getResponseData());
                responseData.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
                responseData.setResponseData(responseToken);

            } else {
                responseData.setResponseCode(responseLogin.getResponseCode());
                responseData.setResponseMessage(responseLogin.getResponseMessage());
            }

        } catch (Exception e) {
            logger.error("Error Login Controller", e);
        }
        LogVass.finishController(logger, "Login", responseData);
        return responseData;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/register", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> register(@RequestBody RegisterRequest request) {
        LogVass.startController(logger, "Register", request);

        Response<String> response = new Response<>();
        try {
            response = userService.register(request);
        } catch (Exception e) {
            logger.error("Error Register Controller", e);
        }
        LogVass.finishController(logger, "Register", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/changePassword", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> changePassword(@RequestBody ChangePasswordRequest request) {
        LogVass.startController(logger, "ChangePassword", request);

        Response<String> response = new Response<>();
        try {
            response = userService.changePassword(request);
        } catch (Exception e) {
            logger.error("Error ChangePassword Controller", e);
        }
        LogVass.finishController(logger, "ChangePassword", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/reset", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> resetPassword(@RequestBody ResetPasswordRequest request) {
        LogVass.startController(logger, "ResetPassword", request);

        Response<String> response = new Response<>();
        try {
            response = userService.resetPassword(request);
        } catch (Exception e) {
            logger.error("Error ResetPassword Controller", e);
        }
        LogVass.finishController(logger, "ResetPassword", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/health", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public ResponseEntity<String> health() {
        logger.info("health");
        return ResponseEntity.status(HttpStatus.OK).body("{OK}");
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/bauth", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public ResponseEntity<Map<String, String>> basicAuth(@RequestHeader(value = "Authorization") String authorization) {
        logger.info("login basic auth");
        Map<String, String> response = new HashMap<>();
        try {
            if (userService.login(authorization)) {
                logger.info("login OK");
                response.put("result", "Ok");
                return ResponseEntity.ok(response);
            } else {
                logger.info("login FAIL");
                response.put("result", "Forbidden");
            }
        } catch (Exception e) {
            logger.info("login ERROR");
            response.put("result", "Error");
        }
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/generarToken", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ConvergenciaResponse> generarToken(@RequestBody ConvergenciaRequest request) {
        logger.info("Inicio generarToken Controller");
        //logger.info("REQUEST => {\"authorization\":\"" + authorization + "\"}");

        Response<ConvergenciaResponse> response = new Response<>();
        try {
            response = userService.guardarToken(request);
        } catch (Exception e) {
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.error("Error generarToken Controller", e);
        }
        LogVass.finishController(logger, "generarToken", response);
        return response;
    }

}
