package pe.com.tdp.ventafija.microservices.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Dates {

    @JsonProperty("code")
    private String code;
    @JsonProperty("name")
    private String name;
    @JsonProperty("codeHereditary")
    private String codeHereditary;

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("codeHereditary")
    public String getCodeHereditary() {
        return codeHereditary;
    }

    @JsonProperty("codeHereditary")
    public void setCodeHereditary(String codeHereditary) {
        this.codeHereditary = codeHereditary;
    }
}
