package pe.com.tdp.ventafija.microservices.domain.customer;

public class DuplicateRequest {

  private String documentType;
  private String documentNumber;

  public String getDocumentType() {
    return documentType;
  }

  public void setDocumentType(String documentType) {
    this.documentType = documentType;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public void setDocumentNumber(String documentNumber) {
    this.documentNumber = documentNumber;
  }

}
