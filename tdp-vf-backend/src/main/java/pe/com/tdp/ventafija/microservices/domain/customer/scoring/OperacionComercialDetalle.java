package pe.com.tdp.ventafija.microservices.domain.customer.scoring;

public class OperacionComercialDetalle {
	private String CodProd;
	private String DesProd;
	private float CuotaFin;
	private float MesesFin;
	private float PagoAdelantado;
	private float NroDecosAdic;

	public OperacionComercialDetalle() {
		super();
	}

	public OperacionComercialDetalle(String codProd, String desProd, float cuotaFin, float mesesFin,
			float pagoAdelantado, float nroDecosAdic) {
		super();
		CodProd = codProd;
		DesProd = desProd;
		CuotaFin = cuotaFin;
		MesesFin = mesesFin;
		PagoAdelantado = pagoAdelantado;
		NroDecosAdic = nroDecosAdic;
	}

	public String getCodProd() {
		return CodProd;
	}

	public void setCodProd(String codProd) {
		CodProd = codProd;
	}

	public String getDesProd() {
		return DesProd;
	}

	public void setDesProd(String desProd) {
		DesProd = desProd;
	}

	public float getCuotaFin() {
		return CuotaFin;
	}

	public void setCuotaFin(float cuotaFin) {
		CuotaFin = cuotaFin;
	}

	public float getMesesFin() {
		return MesesFin;
	}

	public void setMesesFin(float mesesFin) {
		MesesFin = mesesFin;
	}

	public float getPagoAdelantado() {
		return PagoAdelantado;
	}

	public void setPagoAdelantado(float pagoAdelantado) {
		PagoAdelantado = pagoAdelantado;
	}

	public float getNroDecosAdic() {
		return NroDecosAdic;
	}

	public void setNroDecosAdic(float nroDecosAdic) {
		NroDecosAdic = nroDecosAdic;
	}
}
