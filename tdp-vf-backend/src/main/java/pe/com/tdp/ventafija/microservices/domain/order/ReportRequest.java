package pe.com.tdp.ventafija.microservices.domain.order;

public class ReportRequest {
    private String state;
    private String dateStart;
    private String dateEnd;
    private String codAtis;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getCodAtis() {
        return codAtis;
    }

    public void setCodAtis(String codAtis) {
        this.codAtis = codAtis;
    }
}
