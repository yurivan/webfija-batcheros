package pe.com.tdp.ventafija.microservices.domain.address;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AddressValidateResponse {

    @JsonProperty("isValidAddressComplete")
    private boolean isValidAddressComplete;
    @JsonProperty("isValidAddressCompleteText")
    private ErrorMessage isValidAddressCompleteText;

    @JsonProperty("isValidAddressComplete")
    public boolean isValidAddressComplete() {
        return isValidAddressComplete;
    }

    @JsonProperty("isValidAddressComplete")
    public void setValidAddressComplete(boolean validAddressComplete) {
        isValidAddressComplete = validAddressComplete;
    }

    @JsonProperty("isValidAddressCompleteText")
    public ErrorMessage getIsValidAddressCompleteText() {
        return isValidAddressCompleteText;
    }

    @JsonProperty("isValidAddressCompleteText")
    public void setIsValidAddressCompleteText(ErrorMessage isValidAddressCompleteText) {
        this.isValidAddressCompleteText = isValidAddressCompleteText;
    }
}
