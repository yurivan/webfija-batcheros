package pe.com.tdp.ventafija.microservices.domain.order;

import pe.com.tdp.ventafija.microservices.domain.Parameter;

import java.util.List;

public class SalesReportTypeRequest {

    private String reportTypeId;

    private List<Parameter> filters;

    public String getReportTypeId() {
        return reportTypeId;
    }

    public void setReportTypeId(String reportTypeId) {
        this.reportTypeId = reportTypeId;
    }

    public List<Parameter> getFilters() {
        return filters;
    }

    public void setFilters(List<Parameter> filters) {
        this.filters = filters;
    }
}
