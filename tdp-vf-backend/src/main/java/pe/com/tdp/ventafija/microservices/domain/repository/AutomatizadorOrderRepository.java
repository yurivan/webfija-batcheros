package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.common.domain.entity.AutomatizadorOrder;

public interface AutomatizadorOrderRepository extends JpaRepository<AutomatizadorOrder, String> {


    /*
    @Modifying
    @Query("update Customer c set c.customerPhone = ?1 where c.docNumber = ?2")
    void updateCustomerPhone(String phone, String docnumber);*/
    @Transactional
    @Modifying
    @Query("update AutomatizadorOrder ao set ao.flag_exits = '1', ao.duplicado_auto = ?1 where ao.order_id = ?2")
    void updateAutomatizadorOrder(String duplicado, String order_id);

    @Modifying
    @Query(value = "insert into AutomatizadorOrder (order_id, duplicado_auto, flag_exits) values (:order_id, '', 1)",
            nativeQuery = true)
    void insertAutomatizadorOrder(@Param("order_id") String order_id);

}
