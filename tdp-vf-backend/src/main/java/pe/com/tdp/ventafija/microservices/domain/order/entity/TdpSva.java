package pe.com.tdp.ventafija.microservices.domain.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "tdp_sva", schema = "ibmx_a07e6d02edaf552")
public class TdpSva {

    @Id
    @Column(name = "id")
    private Integer id;
    @Column(name = "prodtypecode")
    private String prodTypeCode;
    @Column(name = "code")
    private String code;
    @Column(name = "description")
    private String description;
    @Column(name = "unit")
    private String unit;
    @Column(name = "cost")
    private BigDecimal cost;
    @Column(name = "productcode")
    private String productCode;
    @Column(name = "sva_codigo")
    private String sva_Codigo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProdTypeCode() {
        return prodTypeCode;
    }

    public void setProdtypecode(String prodTypeCode) {
        this.prodTypeCode = prodTypeCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getSva_Codigo() {
        return sva_Codigo;
    }

    public void setSva_Codigo(String sva_Codigo) {
        this.sva_Codigo = sva_Codigo;
    }
}
