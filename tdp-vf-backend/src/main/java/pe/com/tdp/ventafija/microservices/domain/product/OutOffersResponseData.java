package pe.com.tdp.ventafija.microservices.domain.product;

import pe.com.tdp.ventafija.microservices.domain.customer.ContractWebRequestCustomer;

public class OutOffersResponseData {
    private int id;
    private String commercialOperation;
    private String segmento;
    private String canal;
    private String entidad;
    private String provincia;
    private String distrito;
    private String campaign;
    private String campaingCode;
    private Integer priority;
    private String productTypeCode;
    private String productType;
    private String productCategoryCode;
    private String productCategory;
    private String productCode;
    private String productName;
    //private String bloqueTv;
    //private String svaLinea;
    //private String svaInternet;
    private String tipoRegistro;
    private String discount;
    private double price;
    private double promPrice;
    private int monthPeriod;
    private double installCost;
    private String lineType;
    private String paymentMethod;
    private double cashPrice;
    private double financingCost;
    private int financingMonth;
    private String equipmentType;
    private int returnMonth;
    private String returnPeriod;
    private int internetSpeed;
    /* Sprint 10 */
    private Integer promoInternetSpeed;
    private Integer periodoPromoInternetSpeed;
    /* Sprint 10 */
    private String internetTech;
    private String tvSignal;
    private String tvTech;
    /* Sprint 10 */
    private String equipamientoLinea;
    private String equipamientoInternet;
    private String equipamientoTv;
    /* Sprint 10 */

    private String expertCode;
    private double rentatotal;
    private double cantidaddecos;
    private double internet;

    private String category;
    private String targetType;
    private SvaV2ResponseDataSva sva;

    // Sprint 10 - campos de afinidad por entidad y ubicacion... los que tienen menor valor tienen mas afinidad
    private Integer afinidadEntidad;
    private Integer afinidadUbicacion;

    // Sprint 21 - Para OUT se utiliza altaPuraSvasBloqueTV como los id SVA que viene por defecto en la base para migras
    private String altaPuraSvasBloqueTV;
    // los nombres de los id altaPuraSvasBloqueTV
    private String altaPuraSvasNameBloqueTV;
    private String altaPuraSvasLinea;
    private String altaPuraSvasInternet;


    private String msx_cbr_voi_ges_in;
    private String msx_cbr_voi_gis_in;
    private String msx_ind_snl_gis_cd;
    private String msx_ind_gpo_gis_cd;
    private String cod_ind_sen_cms;
    private String cod_cab_cms;
    private String cod_fac_tec_cd;

    private String cobre_blq_vta;
    private String cobre_blq_trm;
//    private String dth_tec;
//    private String dth_blq;
//    private String zar_tec;
//    private String zar_blq;
//    private String coor_x;
//    private String coor_y;

    private String customerName;
    private String tipoDocumento;
    private String numeroDocumento;
    private String departamento;
    private String direccion;
    private String numeroTelefono;
    private String celular;
    private String celular2;

    public OutOffersResponseData() {
        super();
    }

    public OutOffersResponseData(OutOffersResponseData oferta) {
        super();
        this.id = oferta.id;
        this.commercialOperation = oferta.commercialOperation;
        this.campaign = oferta.campaign;
        this.productTypeCode = oferta.productTypeCode;
        this.productType = oferta.productType;
        this.productCategoryCode = oferta.productCategoryCode;
        this.productCategory = oferta.productCategory;
        this.productCode = oferta.productCode;
        this.productName = oferta.productName;
        this.tipoRegistro = oferta.tipoRegistro;
        this.discount = oferta.discount;
        this.price = oferta.price;
        this.promPrice = oferta.promPrice;
        this.monthPeriod = oferta.monthPeriod;
        this.installCost = oferta.installCost;
        this.lineType = oferta.lineType;
        this.paymentMethod = oferta.paymentMethod;
        this.cashPrice = oferta.cashPrice;
        this.financingCost = oferta.financingCost;
        this.financingMonth = oferta.financingMonth;
        this.equipmentType = oferta.equipmentType;
        this.returnMonth = oferta.returnMonth;
        this.returnPeriod = oferta.returnPeriod;
        this.internetSpeed = oferta.internetSpeed;
        this.promoInternetSpeed = oferta.promoInternetSpeed;
        this.periodoPromoInternetSpeed = oferta.periodoPromoInternetSpeed;
        this.internetTech = oferta.internetTech;
        this.tvSignal = oferta.tvSignal;
        this.tvTech = oferta.tvTech;
        this.equipamientoLinea = oferta.equipamientoLinea;
        this.equipamientoInternet = oferta.equipamientoInternet;
        this.equipamientoTv = oferta.equipamientoTv;
        this.expertCode = oferta.expertCode;
        this.rentatotal = oferta.rentatotal;
        this.cantidaddecos = oferta.cantidaddecos;
        this.internet = oferta.internet;
        this.category = oferta.category;
        this.targetType = oferta.targetType;
        this.sva = oferta.sva;

        // Sprint 11
        this.altaPuraSvasBloqueTV = oferta.getAltaPuraSvasBloqueTV();
        this.altaPuraSvasLinea = oferta.getAltaPuraSvasLinea();
        this.altaPuraSvasInternet=oferta.getAltaPuraSvasInternet();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCommercialOperation() {
        return commercialOperation;
    }

    public void setCommercialOperation(String commercialOperation) {
        this.commercialOperation = commercialOperation;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public String getCampaingCode() {
        return campaingCode;
    }

    public void setCampaingCode(String campaingCode) {
        this.campaingCode = campaingCode;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getProductTypeCode() {
        return productTypeCode;
    }

    public void setProductTypeCode(String productTypeCode) {
        this.productTypeCode = productTypeCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getProductCategoryCode() {
        return productCategoryCode;
    }

    public void setProductCategoryCode(String productCategoryCode) {
        this.productCategoryCode = productCategoryCode;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTipoRegistro() {
        return tipoRegistro;
    }

    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPromPrice() {
        return promPrice;
    }

    public void setPromPrice(double promPrice) {
        this.promPrice = promPrice;
    }

    public int getMonthPeriod() {
        return monthPeriod;
    }

    public void setMonthPeriod(int monthPeriod) {
        this.monthPeriod = monthPeriod;
    }

    public double getInstallCost() {
        return installCost;
    }

    public void setInstallCost(double installCost) {
        this.installCost = installCost;
    }

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public double getCashPrice() {
        return cashPrice;
    }

    public void setCashPrice(double cashPrice) {
        this.cashPrice = cashPrice;
    }

    public double getFinancingCost() {
        return financingCost;
    }

    public void setFinancingCost(double financingCost) {
        this.financingCost = financingCost;
    }

    public int getFinancingMonth() {
        return financingMonth;
    }

    public void setFinancingMonth(int financingMonth) {
        this.financingMonth = financingMonth;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public int getReturnMonth() {
        return returnMonth;
    }

    public void setReturnMonth(int returnMonth) {
        this.returnMonth = returnMonth;
    }

    public String getReturnPeriod() {
        return returnPeriod;
    }

    public void setReturnPeriod(String returnPeriod) {
        this.returnPeriod = returnPeriod;
    }

    public int getInternetSpeed() {
        return internetSpeed;
    }

    public void setInternetSpeed(int internetSpeed) {
        this.internetSpeed = internetSpeed;
    }

    public Integer getPromoInternetSpeed() {
        return promoInternetSpeed;
    }

    public void setPromoInternetSpeed(Integer promoInternetSpeed) {
        this.promoInternetSpeed = promoInternetSpeed;
    }

    public Integer getPeriodoPromoInternetSpeed() {
        return periodoPromoInternetSpeed;
    }

    public void setPeriodoPromoInternetSpeed(Integer periodoPromoInternetSpeed) {
        this.periodoPromoInternetSpeed = periodoPromoInternetSpeed;
    }

    public String getInternetTech() {
        return internetTech;
    }

    public void setInternetTech(String internetTech) {
        this.internetTech = internetTech;
    }

    public String getTvSignal() {
        return tvSignal;
    }

    public void setTvSignal(String tvSignal) {
        this.tvSignal = tvSignal;
    }

    public String getTvTech() {
        return tvTech;
    }

    public void setTvTech(String tvTech) {
        this.tvTech = tvTech;
    }

    public String getEquipamientoLinea() {
        return equipamientoLinea;
    }

    public void setEquipamientoLinea(String equipamientoLinea) {
        this.equipamientoLinea = equipamientoLinea;
    }

    public String getEquipamientoInternet() {
        return equipamientoInternet;
    }

    public void setEquipamientoInternet(String equipamientoInternet) {
        this.equipamientoInternet = equipamientoInternet;
    }

    public String getEquipamientoTv() {
        return equipamientoTv;
    }

    public void setEquipamientoTv(String equipamientoTv) {
        this.equipamientoTv = equipamientoTv;
    }

    public String getExpertCode() {
        return expertCode;
    }

    public void setExpertCode(String expertCode) {
        this.expertCode = expertCode;
    }

    public double getRentatotal() {
        return rentatotal;
    }

    public void setRentatotal(double rentatotal) {
        this.rentatotal = rentatotal;
    }

    public double getCantidaddecos() {
        return cantidaddecos;
    }

    public void setCantidaddecos(double cantidaddecos) {
        this.cantidaddecos = cantidaddecos;
    }

    public double getInternet() {
        return internet;
    }

    public void setInternet(double internet) {
        this.internet = internet;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public SvaV2ResponseDataSva getSva() {
        return sva;
    }

    public void setSva(SvaV2ResponseDataSva sva) {
        this.sva = sva;
    }

    public Integer getAfinidadEntidad() {
        return afinidadEntidad;
    }

    public void setAfinidadEntidad(Integer afinidadEntidad) {
        this.afinidadEntidad = afinidadEntidad;
    }

    public Integer getAfinidadUbicacion() {
        return afinidadUbicacion;
    }

    public void setAfinidadUbicacion(Integer afinidadUbicacion) {
        this.afinidadUbicacion = afinidadUbicacion;
    }

    public String getAltaPuraSvasBloqueTV() {
        return altaPuraSvasBloqueTV;
    }

    public void setAltaPuraSvasBloqueTV(String altaPuraSvasBloqueTV) {
        this.altaPuraSvasBloqueTV = altaPuraSvasBloqueTV;
    }

    public String getAltaPuraSvasNameBloqueTV() {
        return altaPuraSvasNameBloqueTV;
    }

    public void setAltaPuraSvasNameBloqueTV(String altaPuraSvasNameBloqueTV) {
        this.altaPuraSvasNameBloqueTV = altaPuraSvasNameBloqueTV;
    }

    public String getAltaPuraSvasLinea() {
        return altaPuraSvasLinea;
    }

    public void setAltaPuraSvasLinea(String altaPuraSvasLinea) {
        this.altaPuraSvasLinea = altaPuraSvasLinea;
    }

    public String getAltaPuraSvasInternet() {
        return altaPuraSvasInternet;
    }

    public void setAltaPuraSvasInternet(String altaPuraSvasInternet) {
        this.altaPuraSvasInternet = altaPuraSvasInternet;
    }

    public String getMsx_cbr_voi_ges_in() {
        return msx_cbr_voi_ges_in;
    }

    public void setMsx_cbr_voi_ges_in(String msx_cbr_voi_ges_in) {
        this.msx_cbr_voi_ges_in = msx_cbr_voi_ges_in;
    }

    public String getMsx_cbr_voi_gis_in() {
        return msx_cbr_voi_gis_in;
    }

    public void setMsx_cbr_voi_gis_in(String msx_cbr_voi_gis_in) {
        this.msx_cbr_voi_gis_in = msx_cbr_voi_gis_in;
    }

    public String getMsx_ind_snl_gis_cd() {
        return msx_ind_snl_gis_cd;
    }

    public void setMsx_ind_snl_gis_cd(String msx_ind_snl_gis_cd) {
        this.msx_ind_snl_gis_cd = msx_ind_snl_gis_cd;
    }

    public String getMsx_ind_gpo_gis_cd() {
        return msx_ind_gpo_gis_cd;
    }

    public void setMsx_ind_gpo_gis_cd(String msx_ind_gpo_gis_cd) {
        this.msx_ind_gpo_gis_cd = msx_ind_gpo_gis_cd;
    }

    public String getCod_ind_sen_cms() {
        return cod_ind_sen_cms;
    }

    public void setCod_ind_sen_cms(String cod_ind_sen_cms) {
        this.cod_ind_sen_cms = cod_ind_sen_cms;
    }

    public String getCod_cab_cms() {
        return cod_cab_cms;
    }

    public void setCod_cab_cms(String cod_cab_cms) {
        this.cod_cab_cms = cod_cab_cms;
    }

    public String getCod_fac_tec_cd() {
        return cod_fac_tec_cd;
    }

    public void setCod_fac_tec_cd(String cod_fac_tec_cd) {
        this.cod_fac_tec_cd = cod_fac_tec_cd;
    }

    public String getCobre_blq_vta() {
        return cobre_blq_vta;
    }

    public void setCobre_blq_vta(String cobre_blq_vta) {
        this.cobre_blq_vta = cobre_blq_vta;
    }

    public String getCobre_blq_trm() {
        return cobre_blq_trm;
    }

    public void setCobre_blq_trm(String cobre_blq_trm) {
        this.cobre_blq_trm = cobre_blq_trm;
    }

//    public String getDth_tec() {
//        return dth_tec;
//    }
//
//    public void setDth_tec(String dth_tec) {
//        this.dth_tec = dth_tec;
//    }
//
//    public String getDth_blq() {
//        return dth_blq;
//    }
//
//    public void setDth_blq(String dth_blq) {
//        this.dth_blq = dth_blq;
//    }
//
//    public String getZar_tec() {
//        return zar_tec;
//    }
//
//    public void setZar_tec(String zar_tec) {
//        this.zar_tec = zar_tec;
//    }
//
//    public String getZar_blq() {
//        return zar_blq;
//    }
//
//    public void setZar_blq(String zar_blq) {
//        this.zar_blq = zar_blq;
//    }
//
//    public String getCoor_x() {
//        return coor_x;
//    }
//
//    public void setCoor_x(String coor_x) {
//        this.coor_x = coor_x;
//    }
//
//    public String getCoor_y() {
//        return coor_y;
//    }
//
//    public void setCoor_y(String coor_y) {
//        this.coor_y = coor_y;
//    }


    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCelular2() {
        return celular2;
    }

    public void setCelular2(String celular2) {
        this.celular2 = celular2;
    }
}
