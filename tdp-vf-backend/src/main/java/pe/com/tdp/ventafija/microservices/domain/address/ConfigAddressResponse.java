package pe.com.tdp.ventafija.microservices.domain.address;

public class ConfigAddressResponse {

    //00 - ok   01 - error
    private String codConsulta;
    //messaje a mostrar
    private String mensaje;

    public String getCodConsulta() {
        return codConsulta;
    }

    public void setCodConsulta(String codConsulta) {
        this.codConsulta = codConsulta;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
