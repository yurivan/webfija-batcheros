package pe.com.tdp.ventafija.microservices.domain.order;

import java.util.List;

public class SalesReportExportResponseBody {

    private List<SalesReportExportResponse> reporteVentas;

    public List<SalesReportExportResponse> getReporteVentas() {
        return reporteVentas;
    }

    public void setReporteVentas(List<SalesReportExportResponse> reporteVentas) {
        this.reporteVentas = reporteVentas;
    }
}
