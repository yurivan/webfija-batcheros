package pe.com.tdp.ventafija.microservices.domain.message;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_error_message_service", schema = "ibmx_a07e6d02edaf552")
public class TdpErrorMessageServiceEntities {
    @Id
    @Column
    private Integer id;
    @Column(name = "service")
    private String service;
    @Column(name = "orden")
    private Integer orden;
    @Column(name = "descriptionkey")
    private String descriptionKey;
    @Column(name = "servicekey")
    private String serviceKey;
    @Column(name = "responsemessage")
    private String responseMessage;
    @Column(name = "responsecode")
    private Integer responseCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public String getDescriptionKey() {
        return descriptionKey;
    }

    public void setDescriptionKey(String descriptionKey) {
        this.descriptionKey = descriptionKey;
    }

    public String getServiceKey() {
        return serviceKey;
    }

    public void setServiceKey(String serviceKey) {
        this.serviceKey = serviceKey;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer responseCode) {
        this.responseCode = responseCode;
    }
}
