package pe.com.tdp.ventafija.microservices.domain.order.entity;

public class Detalles {

    private Detalle detalle;

    public Detalles() {
        super();
    }

    public Detalle getDetalle() {
        return detalle;
    }

    public void setDetalle(Detalle detalle) {
        this.detalle = detalle;
    }
}
