package pe.com.tdp.ventafija.microservices.domain.profile;

public class NivelRequest {
    private int nivel;
    private String canal;

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getCanal() { return canal; }

    public void setCanal(String canal) { this.canal = canal; }
}
