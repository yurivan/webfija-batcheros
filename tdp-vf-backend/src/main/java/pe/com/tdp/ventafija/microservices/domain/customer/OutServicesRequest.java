package pe.com.tdp.ventafija.microservices.domain.customer;

public class OutServicesRequest {
    private String phoneOrCms;

    public String getPhoneOrCms() {
        return phoneOrCms;
    }

    public void setPhoneOrCms(String phoneOrCms) {
        this.phoneOrCms = phoneOrCms;
    }
}

