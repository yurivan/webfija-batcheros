package pe.com.tdp.ventafija.microservices.domain.customer;

public class BiometricRequest {

    /* Variables de entrada del cliente */
    private String customerDocumentType;
    private String customerDocument;
    private String customerIdTransaction;

    /* Variables de entrada del vendedor */
    private String sellerDocument;
    private String sellerChannel;

    public String getCustomerDocumentType() {
        return customerDocumentType;
    }

    public void setCustomerDocumentType(String customerDocumentType) {
        this.customerDocumentType = customerDocumentType;
    }

    public String getCustomerDocument() {
        return customerDocument;
    }

    public void setCustomerDocument(String customerDocument) {
        this.customerDocument = customerDocument;
    }

    public String getCustomerIdTransaction() {
        return customerIdTransaction;
    }

    public void setCustomerIdTransaction(String customerIdTransaction) {
        this.customerIdTransaction = customerIdTransaction;
    }

    public String getSellerDocument() {
        return sellerDocument;
    }

    public void setSellerDocument(String sellerDocument) {
        this.sellerDocument = sellerDocument;
    }

    public String getSellerChannel() {
        return sellerChannel;
    }

    public void setSellerChannel(String sellerChannel) {
        this.sellerChannel = sellerChannel;
    }
}
