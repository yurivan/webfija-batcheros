package pe.com.tdp.ventafija.microservices.service.outcall;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.dao.OrderRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Order;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.MessageUtil;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpOrder;
import pe.com.tdp.ventafija.microservices.domain.outcall.AudioRequest;
import pe.com.tdp.ventafija.microservices.domain.outcall.OrderOutCall;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpOrderRepository;
import pe.com.tdp.ventafija.microservices.repository.outcall.OutCallDao;
import pe.com.tdp.ventafija.microservices.service.order.OrderService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Service
public class OutCallService {

    @Autowired
    private MessageUtil messageUtil;

    @Autowired
    private ServiceCallEventsService serviceCallEventsService;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();

    @Autowired
    private OrderService orderService;

    @Autowired
    private OutCallDao outCallDao;

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private TdpOrderRepository tdpOrderRepository;

    @Autowired
    private OrderRepository orderRepository;


    @Transactional
    public Response getApproveAudio(AudioRequest request) {
        Response response = new Response();
        try {
            orderService.updateStatusAudio(request.getOrder_id(), "1");
            updateAddressTdpOrder(request.getAddress_direccion(),request.getAddress_referencia(),request.getOrder_id());
            OrderOutCall getDateOrder = outCallDao.getDateOrderToId(request.getOrder_id());
            insertFileStatusLog(request, getDateOrder);
            response.setResponseCode("0");
            response.setResponseMessage("Audio Aprobado Correctamente");
        } catch (Exception e) {
            response.setResponseCode("1");
            response.setResponseMessage("getApproveAudio Error");
        }
        return response;
    }

    @Transactional
    public Response getFailAudio(AudioRequest request) {
        Response response = new Response();
        try {
            orderService.updateStatusAudio(request.getOrder_id(), "2");
            OrderOutCall getDateOrder = outCallDao.getDateOrderToId(request.getOrder_id());
            insertFileStatusLog(request, getDateOrder);
            Order order = orderRepository.findByFileOrder(request.getOrder_id());
            TdpOrder tdpOrder = tdpOrderRepository.findOneByOrderId(request.getOrder_id());
            updateTdpVisor(request, getDateOrder, order, tdpOrder);

            response.setResponseCode("0");
            response.setResponseMessage("Audio dado de baja Correctamente.");
        } catch (Exception e) {
            response.setResponseCode("1");
            response.setResponseMessage("getFailAudio Error");
        }
        return response;
    }

    private void updateTdpVisor(AudioRequest request, OrderOutCall dateOrder, Order order, TdpOrder tdpOrder) {
        try {
            String sql = "UPDATE ibmx_a07e6d02edaf552.tdp_visor SET estado_solicitud = ? WHERE id_visor = ?";
            javax.persistence.Query q = em.createNativeQuery(sql);
            q.setParameter(1, "CAIDA");
            q.setParameter(2, request.getOrder_id());

            q.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert", e);
        }
    }


    public void insertFileStatusLog(AudioRequest request, OrderOutCall dateOrder) {
        try {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.file_status_log (status, user_id, name, canal_atis, fecha_hora, "
                    + "entidad, punto_venta, order_id, extension) "
                    + "VALUES (?, ?, ?, ?, CURRENT_TIMESTAMP, ?, ?, ?, ?)";
            javax.persistence.Query q = em.createNativeQuery(sql);
            q.setParameter(1, dateOrder.getStatusaudio());
            q.setParameter(2, request.getUser_id());
            q.setParameter(3, request.getName());
            q.setParameter(4, request.getCanal_atis());
            q.setParameter(5, dateOrder.getEntidad());
            q.setParameter(6, dateOrder.getNompuntoventa());
            q.setParameter(7, dateOrder.getId());
            q.setParameter(8, request.getExtension());

            q.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert", e);
        }
    }

    @Transactional
    public void updateAddressTdpOrder(String direccion, String referencia, String id) {
        logger.info("Inicio updateAddressTdpOrder Service");
        tdpOrderRepository.updateAddressTdpOrder(direccion, referencia, id);
        logger.info("Fin updateAddressTdpOrder Service");
    }

}
