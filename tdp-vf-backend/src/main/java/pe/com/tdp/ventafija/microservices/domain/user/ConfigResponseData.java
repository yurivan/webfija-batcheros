package pe.com.tdp.ventafija.microservices.domain.user;

import java.util.List;

public class ConfigResponseData {

	private LoginResponseData user;
	private List<ConfigParameterResponseData> parameter;

	public LoginResponseData getUser() {
		return user;
	}

	public void setUser(LoginResponseData user) {
		this.user = user;
	}

	public List<ConfigParameterResponseData> getParameter() {
		return parameter;
	}

	public void setParameter(List<ConfigParameterResponseData> parameter) {
		this.parameter = parameter;
	}

}
