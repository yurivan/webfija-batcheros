package pe.com.tdp.ventafija.microservices.domain.customer.scoring;

import java.util.List;

public class CustomerScoringResponse {
	private String codConsulta;
	private String accion;
	private String deudaAtis;
	private String deudaCms;
	private List<OperacionComercial> result;

	private String orderId;

	public CustomerScoringResponse() {
		super();
	}

	public CustomerScoringResponse(List<OperacionComercial> result) {
		super();
		this.result = result;
	}

	public List<OperacionComercial> getResult() {
		return result;
	}

	public void setResult(List<OperacionComercial> result) {
		this.result = result;
	}

	public String getCodConsulta() {
		return codConsulta;
	}

	public void setCodConsulta(String codConsulta) {
		this.codConsulta = codConsulta;
	}

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getDeudaAtis() {
		return deudaAtis;
	}

	public void setDeudaAtis(String deudaAtis) {
		this.deudaAtis = deudaAtis;
	}

	public String getDeudaCms() {
		return deudaCms;
	}

	public void setDeudaCms(String deudaCms) {
		this.deudaCms = deudaCms;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
}
