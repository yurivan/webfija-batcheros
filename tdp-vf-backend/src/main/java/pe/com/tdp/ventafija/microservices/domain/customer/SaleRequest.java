package pe.com.tdp.ventafija.microservices.domain.customer;

public class SaleRequest {

  private String appId;
  private String campaign;
  private String productType;
  private String productCategory;
  private String productName;
  private String internetTech;
  private String paymentMethod;
  private String commercialOperation;
  private String documentNumber;
  private String phone;
  private String firstName;
  private String lastName;
  private String channel;
  private String entity;
  private String codATIS;
  private String codCMS;
  private String department;
  private String province;
  private String district;

  public String getAppId() {
    return appId;
  }

  public void setAppId(String appId) {
    this.appId = appId;
  }

  public String getCampaign() {
    return campaign;
  }

  public void setCampaign(String campaign) {
    this.campaign = campaign;
  }

  public String getProductType() {
    return productType;
  }

  public void setProductType(String productType) {
    this.productType = productType;
  }

  public String getProductCategory() {
    return productCategory;
  }

  public void setProductCategory(String productCategory) {
    this.productCategory = productCategory;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getInternetTech() {
    return internetTech;
  }

  public void setInternetTech(String internetTech) {
    this.internetTech = internetTech;
  }

  public String getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public String getCommercialOperation() {
    return commercialOperation;
  }

  public void setCommercialOperation(String commercialOperation) {
    this.commercialOperation = commercialOperation;
  }

  public String getDocumentNumber() {
    return documentNumber;
  }

  public void setDocumentNumber(String documentNumber) {
    this.documentNumber = documentNumber;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public String getEntity() {
    return entity;
  }

  public void setEntity(String entity) {
    this.entity = entity;
  }

  public String getCodATIS() {
    return codATIS;
  }

  public void setCodATIS(String codATIS) {
    this.codATIS = codATIS;
  }

  public String getCodCMS() {
    return codCMS;
  }

  public void setCodCMS(String codCMS) {
    this.codCMS = codCMS;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getDistrict() {
    return district;
  }

  public void setDistrict(String district) {
    this.district = district;
  }

}
