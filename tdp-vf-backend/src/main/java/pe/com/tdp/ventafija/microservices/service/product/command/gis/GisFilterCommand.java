package pe.com.tdp.ventafija.microservices.service.product.command.gis;


import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;
import pe.com.tdp.ventafija.microservices.service.command.FilterCommand;

public interface GisFilterCommand extends FilterCommand<WSSIGFFTT_FFTT_AVEResult> {

}
