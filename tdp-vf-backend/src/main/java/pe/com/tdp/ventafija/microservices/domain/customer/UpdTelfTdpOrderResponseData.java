package pe.com.tdp.ventafija.microservices.domain.customer;

public class UpdTelfTdpOrderResponseData {

   private String order_id;
   private ContractWebRequestCustomer customer;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public ContractWebRequestCustomer getCustomer() {
        return customer;
    }

    public void setCustomer(ContractWebRequestCustomer customer) {
        this.customer = customer;
    }
}

