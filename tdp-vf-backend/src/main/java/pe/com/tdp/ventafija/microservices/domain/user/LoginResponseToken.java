package pe.com.tdp.ventafija.microservices.domain.user;


//glazaror
public class LoginResponseToken {

	private String token;
	private LoginResponseData responseData;

	public LoginResponseData getResponseData() {
		return responseData;
	}

	public void setResponseData(LoginResponseData responseData) {
		this.responseData = responseData;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	
	
}
