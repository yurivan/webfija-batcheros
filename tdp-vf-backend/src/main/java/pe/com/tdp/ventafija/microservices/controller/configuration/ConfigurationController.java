package pe.com.tdp.ventafija.microservices.controller.configuration;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Argumentarios;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.configuration.ArgumentariosRequest;
import pe.com.tdp.ventafija.microservices.domain.configuration.CanalEntidadRequest;
import pe.com.tdp.ventafija.microservices.domain.configuration.InitialResponse;
import pe.com.tdp.ventafija.microservices.service.configuration.ConfigurationService;

import java.util.List;

@RestController
public class ConfigurationController {

    private static final Logger logger = LogManager.getLogger();

    @Autowired
    private ConfigurationService configurationService;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/config/initial", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<InitialResponse> initial() {
        Response<InitialResponse> response = new Response<>();
        try {
            response = configurationService.initial();
        } catch (Exception e) {
            logger.error("Error Initial Controller", e);
        }
        LogVass.finishController(logger, "Initial", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/config/fecha", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> fechaServidor() {
        Response<String> response = new Response<>();
        try {
            response = configurationService.fecha();
        } catch (Exception e) {
            logger.error("Error FechaServidor Controller", e);
        }
        LogVass.finishController(logger, "FechaServidor", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/config/argumentarios", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<List<Argumentarios>> argumentarios(@RequestBody ArgumentariosRequest request) {
        Response<List<Argumentarios>> response = new Response<>();
        try {
            response = configurationService.argumentarios(request);
        } catch (Exception e) {
            logger.error("Error Initial Controller", e);
        }
        LogVass.finishController(logger, "Initial", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/config/canalEntidad", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<Parameters> canalentidad(@RequestBody CanalEntidadRequest request) {
        Response<Parameters> response = new Response<>();

        response = configurationService.canalentidad(request);

        LogVass.finishController(logger, "Initial", response);
        return response;
    }
}
