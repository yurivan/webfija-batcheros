package pe.com.tdp.ventafija.microservices.domain.order;

import java.util.List;

public class Dia {
    private String name;
    private List<OrderVisor> lista;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<OrderVisor> getLista() {
        return lista;
    }

    public void setLista(List<OrderVisor> lista) {
        this.lista = lista;
    }
}
