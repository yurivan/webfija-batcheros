package pe.com.tdp.ventafija.microservices.domain.repository;

import org.hibernate.annotations.SQLInsert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Customer;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {
	
	Long countByDocTypeAndDocNumber(String docType, String docNumber);
	
	@Query("select c.id from Customer c where c.docType = ?1 and c.docNumber = ?2")
	Long loadIdByDocTypeAndDocNumber(String docType, String docNumber);

    @Modifying
    @Query("update Customer c set c.customerPhone = ?1 where c.docNumber = ?2")
    void updateCustomerPhone(String phone, String docnumber);

    @Query(value = "select strvalue" +
            " from ibmx_a07e6d02edaf552.parameters " +
            " where  domain = 'CONFIG' AND element = ?1", nativeQuery = true)
    String getValorParametro(String parametro);

    @Query(value = "select num_ruc," +
            "raz_social," +
            "est_contr," +
            "cond_dom," +
            "ubigeo," +
            "tipo_via," +
            "nom_via," +
            "cod_zona," +
            "tip_zona," +
            "numero," +
            "interior," +
            "lote," +
            "dpto," +
            "manzana," +
            "kilometro" +
            " from ibmx_a07e6d02edaf552.ruc_sunat " +
            " where  num_ruc = ?1" +
            " limit 1", nativeQuery = true)
    List<Object[]> getDataRuc(String nroruc);

    @Transactional
    @Modifying
    @Query(value = "insert into ibmx_a07e6d02edaf552.experto_response" +
            "(tip_doc,num_doc,ubigeo,body_response,fecha_consulta,canal) " +
            "values (?1,?2,?3,?4,NOW(),?5)"
            , nativeQuery = true)
    public int insertResponseScoring(String docType, String docNumber, String ubigeo, String bodyResponse, String canal);

    @Query(value = "select body_response from ibmx_a07e6d02edaf552.experto_response " +
            "where tip_doc = ?1 and " +
            "num_doc = ?2 and " +
            "ubigeo = ?3 and " +
            "cast(fecha_consulta as date) = cast(NOW() as date) and " +
            "canal = ?4"
            , nativeQuery = true)
    String getResponseScoring(String docType, String docNumber, String ubigeo, String canal);

    @Query(value="select CASE WHEN COUNT(*) > 0 THEN 'SI' ELSE 'NO' END from " +
            "ibmx_a07e6d02edaf552.customer where docnumber = ?1",nativeQuery = true)
    String getExiste(String dni);
}
