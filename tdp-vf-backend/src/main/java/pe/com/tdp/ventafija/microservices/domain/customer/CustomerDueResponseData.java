package pe.com.tdp.ventafija.microservices.domain.customer;

public class CustomerDueResponseData {

    private String codCustomer;
    private String cuentaCustomer;
    private String nombreCustomer;
    private String apePatCustomer;
    private String apeMatCustomer;
    private String tipDocCustomer;
    private String numDocCustomer;
    private String totLineas;
    private String fecReciboEmitidoUlt;
    private String fecReciboVencidoUlt;
    private String deudaCuenta;
    private String topeDeudaAlta;
    private String topeDeudaPark;

    public String getCodCustomer() {
        return codCustomer;
    }

    public void setCodCustomer(String codCustomer) {
        this.codCustomer = codCustomer;
    }

    public String getCuentaCustomer() {
        return cuentaCustomer;
    }

    public void setCuentaCustomer(String cuentaCustomer) {
        this.cuentaCustomer = cuentaCustomer;
    }

    public String getFecReciboEmitidoUlt() {
        return fecReciboEmitidoUlt;
    }

    public String getNombreCustomer() {
        return nombreCustomer;
    }

    public void setNombreCustomer(String nombreCustomer) {
        this.nombreCustomer = nombreCustomer;
    }

    public String getApePatCustomer() {
        return apePatCustomer;
    }

    public void setApePatCustomer(String apePatCustomer) {
        this.apePatCustomer = apePatCustomer;
    }

    public String getApeMatCustomer() {
        return apeMatCustomer;
    }

    public void setApeMatCustomer(String apeMatCustomer) {
        this.apeMatCustomer = apeMatCustomer;
    }

    public String getTipDocCustomer() {
        return tipDocCustomer;
    }

    public void setTipDocCustomer(String tipDocCustomer) {
        this.tipDocCustomer = tipDocCustomer;
    }

    public String getNumDocCustomer() {
        return numDocCustomer;
    }

    public void setNumDocCustomer(String numDocCustomer) {
        this.numDocCustomer = numDocCustomer;
    }

    public String getTotLineas() {
        return totLineas;
    }

    public void setTotLineas(String totLineas) {
        this.totLineas = totLineas;
    }

    public void setFecReciboEmitidoUlt(String fecReciboEmitidoUlt) {
        this.fecReciboEmitidoUlt = fecReciboEmitidoUlt;
    }

    public String getFecReciboVencidoUlt() {
        return fecReciboVencidoUlt;
    }

    public void setFecReciboVencidoUlt(String fecReciboVencidoUlt) {
        this.fecReciboVencidoUlt = fecReciboVencidoUlt;
    }

    public String getDeudaCuenta() {
        return deudaCuenta;
    }

    public void setDeudaCuenta(String deudaCuenta) {
        this.deudaCuenta = deudaCuenta;
    }

    public String getTopeDeudaAlta() { return topeDeudaAlta; }

    public void setTopeDeudaAlta(String topeDeudaAlta) { this.topeDeudaAlta = topeDeudaAlta; }

    public String getTopeDeudaPark() { return topeDeudaPark; }

    public void setTopeDeudaPark(String topeDeudaPark) { this.topeDeudaPark = topeDeudaPark; }
}
