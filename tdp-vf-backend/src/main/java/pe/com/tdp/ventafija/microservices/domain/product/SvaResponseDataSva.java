package pe.com.tdp.ventafija.microservices.domain.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class SvaResponseDataSva {

	private String code;
	private String description;
	@JsonInclude(Include.NON_NULL)
	private String type;
	private String unit;
	private String cost;
	@JsonIgnore
	private String maxValue;
	private String paymentDescription;
	private int id;
	private int financingMonth;

	private String bloquePadre;
	private boolean isClickPadre;

	// Sprint 10 - sva por default
	private boolean altaPuraDefaultSVA;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(String maxValue) {
		this.maxValue = maxValue;
	}

	public String getPaymentDescription() {
		return paymentDescription;
	}

	public void setPaymentDescription(String paymentDescription) {
		this.paymentDescription = paymentDescription;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFinancingMonth() {
		return financingMonth;
	}

	public void setFinancingMonth(int financingMonth) {
		this.financingMonth = financingMonth;
	}

	public String getBloquePadre() {
		return bloquePadre;
	}

	public void setBloquePadre(String bloquePadre) {
		this.bloquePadre = bloquePadre;
	}

	public boolean isClickPadre() {
		return isClickPadre;
	}

	public void setClickPadre(boolean clickPadre) {
		isClickPadre = clickPadre;
	}

	public boolean isAltaPuraDefaultSVA() {
		return altaPuraDefaultSVA;
	}

	public void setAltaPuraDefaultSVA(boolean altaPuraDefaultSVA) {
		this.altaPuraDefaultSVA = altaPuraDefaultSVA;
	}
}