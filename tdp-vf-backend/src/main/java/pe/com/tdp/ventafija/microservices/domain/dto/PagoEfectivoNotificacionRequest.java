package pe.com.tdp.ventafija.microservices.domain.dto;

import pe.com.tdp.ventafija.microservices.domain.order.entity.Cip;

public class PagoEfectivoNotificacionRequest {
    private String idResSolPago;
    private String codTrans;
    private String token;
    private String metodoPago;
    private String codigo;
    private String fecha;
    private String estado;
    private String paramsUrl;
    private Cip cip;
    private String trans;

    public PagoEfectivoNotificacionRequest() {
        super();
    }

    public String getIdResSolPago() {
        return idResSolPago;
    }

    public void setIdResSolPago(String idResSolPago) {
        this.idResSolPago = idResSolPago;
    }

    public String getCodTrans() {
        return codTrans;
    }

    public void setCodTrans(String codTrans) {
        this.codTrans = codTrans;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMetodoPago() {
        return metodoPago;
    }

    public void setMetodoPago(String metodoPago) {
        this.metodoPago = metodoPago;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getParamsUrl() {
        return paramsUrl;
    }

    public void setParamsUrl(String paramsUrl) {
        this.paramsUrl = paramsUrl;
    }

    public Cip getCip() {
        return cip;
    }

    public void setCip(Cip cip) {
        this.cip = cip;
    }

    public String getTrans() {
        return trans;
    }

    public void setTrans(String trans) {
        this.trans = trans;
    }
}
