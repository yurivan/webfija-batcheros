package pe.com.tdp.ventafija.microservices.domain.agendamiento;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AgendamientRequest {


    @JsonProperty("id_visor")
    private String id_visor;
    @JsonProperty("agentClientName")
    private String agentClientName;
    @JsonProperty("agentClientTel")
    private String agentClientTel;
    @JsonProperty("agentClientFrangaHoraria")
    private String agentClientFrangaHoraria;

    @JsonProperty("agentClientFechaSeleccionada")
    private String agentClientFechaSeleccionada;

    @JsonProperty("cupones_id")
    private int cupones_id;

    @JsonProperty("cupones_utilizados")
    private int cupones_utilizados;




    public AgendamientRequest() {
    }

    @JsonProperty("id_visor")
    public String getOrderId() {
        return id_visor;
    }

    @JsonProperty("id_visor")
    public void setOrderId(String orderId) {
        this.id_visor = orderId;
    }

    @JsonProperty("agentClientName")
    public String getAgentClientName() {
        return agentClientName;
    }
    @JsonProperty("agentClientName")
    public void setAgentClientName(String agentClientName) {
        this.agentClientName = agentClientName;
    }
    @JsonProperty("agentClientTel")
    public String getAgentClientTel() {
        return agentClientTel;
    }
    @JsonProperty("agentClientTel")
    public void setAgentClientTel(String agentClientTel) {
        this.agentClientTel = agentClientTel;
    }
    @JsonProperty("agentClientFrangaHoraria")
    public String getAgentClientFrangaHoraria() {
        return agentClientFrangaHoraria;
    }
    @JsonProperty("agentClientFrangaHoraria")
    public void setAgentClientFrangaHoraria(String agentClientFrangaHoraria) {
        this.agentClientFrangaHoraria = agentClientFrangaHoraria;
    }

    @JsonProperty("agentClientFechaSeleccionada")
    public String getAgentClientFechaSeleccionada() {
        return agentClientFechaSeleccionada;
    }
    @JsonProperty("agentClientFechaSeleccionada")
    public void setAgentClientFechaSeleccionada(String agentClientFechaSeleccionada) {
        this.agentClientFechaSeleccionada = agentClientFechaSeleccionada;
    }

    @JsonProperty("cupones_id")
    public int getId_cupon() {
        return cupones_id;
    }
    @JsonProperty("cupones_id")
    public void setId_cupon(int id_cupon) {
        this.cupones_id = id_cupon;
    }
    @JsonProperty("cupones_utilizados")
    public int getCuponesUtilizadoActualizado() {
        return cupones_utilizados;
    }
    @JsonProperty("cupones_utilizados")
    public void setCuponesUtilizadoActualizado(int cuponesUtilizadoActualizado) {
        this.cupones_utilizados = cuponesUtilizadoActualizado;
    }
}
