package pe.com.tdp.ventafija.microservices.domain.product;

public class GisResponse {

    public GisResponse() {
        super();
    }

    private String msx_cbr_voi_ges_in;
    private String msx_cbr_voi_gis_in;
    private String msx_ind_snl_gis_cd;
    private String msx_ind_gpo_gis_cd;
    private String cod_fac_tec_cd;
    private String cod_cab_cms;
    private String cobre_blq_vta;
    private String cobre_blq_trm;

    public String getMsx_cbr_voi_ges_in() {
        return msx_cbr_voi_ges_in;
    }

    public void setMsx_cbr_voi_ges_in(String msx_cbr_voi_ges_in) {
        this.msx_cbr_voi_ges_in = msx_cbr_voi_ges_in;
    }

    public String getMsx_cbr_voi_gis_in() {
        return msx_cbr_voi_gis_in;
    }

    public void setMsx_cbr_voi_gis_in(String msx_cbr_voi_gis_in) {
        this.msx_cbr_voi_gis_in = msx_cbr_voi_gis_in;
    }

    public String getMsx_ind_snl_gis_cd() {
        return msx_ind_snl_gis_cd;
    }

    public void setMsx_ind_snl_gis_cd(String msx_ind_snl_gis_cd) {
        this.msx_ind_snl_gis_cd = msx_ind_snl_gis_cd;
    }

    public String getMsx_ind_gpo_gis_cd() {
        return msx_ind_gpo_gis_cd;
    }

    public void setMsx_ind_gpo_gis_cd(String msx_ind_gpo_gis_cd) {
        this.msx_ind_gpo_gis_cd = msx_ind_gpo_gis_cd;
    }

    public String getCod_fac_tec_cd() {
        return cod_fac_tec_cd;
    }

    public void setCod_fac_tec_cd(String cod_fac_tec_cd) {
        this.cod_fac_tec_cd = cod_fac_tec_cd;
    }

    public String getCod_cab_cms() {
        return cod_cab_cms;
    }

    public void setCod_cab_cms(String cod_cab_cms) {
        this.cod_cab_cms = cod_cab_cms;
    }

    public String getCobre_blq_vta() {
        return cobre_blq_vta;
    }

    public void setCobre_blq_vta(String cobre_blq_vta) {
        this.cobre_blq_vta = cobre_blq_vta;
    }

    public String getCobre_blq_trm() {
        return cobre_blq_trm;
    }

    public void setCobre_blq_trm(String cobre_blq_trm) {
        this.cobre_blq_trm = cobre_blq_trm;
    }
}