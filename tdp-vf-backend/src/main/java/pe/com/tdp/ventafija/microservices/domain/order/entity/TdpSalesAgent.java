package pe.com.tdp.ventafija.microservices.domain.order.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_sales_agent", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema)
public class TdpSalesAgent {

    @Id
    @Column(name = "codatis")
    public String codigoAtis;
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "apepaterno")
    private String apePaterno;
    @Column(name = "apematerno")
    private String apeMaterno;
    @Column(name = "dni")
    private String dni;
    @Column(name = "codcms")
    private String codCms;
    @Column(name = "email")
    private String email;
    @Column(name = "canal")
    private String canal;
    @Column(name = "entidad")
    private String entidad;
    @Column(name = "codpuntoventa")
    private String codPuntoVenta;
    @Column(name = "nompuntoventa")
    private String nombrePuntoVenta;
    @Column(name = "zona")
    private String zona;
    @Column(name = "zonal")
    private String zonal;
    @Column(name = "canalatis")
    private String canalAtis;
    @Column(name = "segmento")
    private String segmento;
    @Column(name = "canalequivcampania")
    private String canalEquivalenciaCampania;
    @Column(name = "entidadequivcampania")
    private String entidadEquivalenciaCampania;

    @Column(name = "userFuerzaVenta")
    private String userFuerzaVenta;

    @Column(name = "user_canal_codigo")
    private String user_canal_codigo;

    @Column(name = "type_flujo_tipificacion")
    private String type_flujo_tipificacion;

    public String getCodigoAtis() {
        return codigoAtis;
    }

    public void setCodigoAtis(String codigoAtis) {
        this.codigoAtis = codigoAtis;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApePaterno() {
        return apePaterno;
    }

    public void setApePaterno(String apePaterno) {
        this.apePaterno = apePaterno;
    }

    public String getApeMaterno() {
        return apeMaterno;
    }

    public void setApeMaterno(String apeMaterno) {
        this.apeMaterno = apeMaterno;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCodCms() {
        return codCms;
    }

    public void setCodCms(String codCms) {
        this.codCms = codCms;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getCodPuntoVenta() {
        return codPuntoVenta;
    }

    public void setCodPuntoVenta(String codPuntoVenta) {
        this.codPuntoVenta = codPuntoVenta;
    }

    public String getNombrePuntoVenta() {
        return nombrePuntoVenta;
    }

    public void setNombrePuntoVenta(String nombrePuntoVenta) {
        this.nombrePuntoVenta = nombrePuntoVenta;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getZonal() {
        return zonal;
    }

    public void setZonal(String zonal) {
        this.zonal = zonal;
    }

    public String getCanalAtis() {
        return canalAtis;
    }

    public void setCanalAtis(String canalAtis) {
        this.canalAtis = canalAtis;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getCanalEquivalenciaCampania() {
        return canalEquivalenciaCampania;
    }

    public void setCanalEquivalenciaCampania(String canalEquivalenciaCampania) {
        this.canalEquivalenciaCampania = canalEquivalenciaCampania;
    }

    public String getEntidadEquivalenciaCampania() {
        return entidadEquivalenciaCampania;
    }

    public void setEntidadEquivalenciaCampania(String entidadEquivalenciaCampania) {
        this.entidadEquivalenciaCampania = entidadEquivalenciaCampania;
    }

    public String getUserFuerzaVenta() {
        return userFuerzaVenta;
    }

    public void setUserFuerzaVenta(String userFuerzaVenta) {
        this.userFuerzaVenta = userFuerzaVenta;
    }

    public String getUser_canal_codigo() {
        return user_canal_codigo;
    }

    public void setUser_canal_codigo(String user_canal_codigo) {
        this.user_canal_codigo = user_canal_codigo;
    }

    public String getType_flujo_tipificacion() {
        return type_flujo_tipificacion;
    }

    public void setType_flujo_tipificacion(String type_flujo_tipificacion) {
        this.type_flujo_tipificacion = type_flujo_tipificacion;
    }
}
