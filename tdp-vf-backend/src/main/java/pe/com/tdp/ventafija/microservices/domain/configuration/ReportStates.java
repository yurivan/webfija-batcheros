package pe.com.tdp.ventafija.microservices.domain.configuration;

import java.util.List;

public class ReportStates {
    private List<String> state;

    public List<String> getState() {
        return state;
    }

    public void setState(List<String> state) {
        this.state = state;
    }
}
