package pe.com.tdp.ventafija.microservices.domain.user;

import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.tdp.ventafija.microservices.domain.ApiResponseBodyFault;

public class RegisterApiSmsResponseBody {

  @JsonProperty("Respuesta")
  private String Respuesta;
  @JsonProperty("ClientException")
  private ApiResponseBodyFault ClientException;
  @JsonProperty("ServerException")
  private ApiResponseBodyFault ServerException;

  public String getRespuesta() {
    return Respuesta;
  }

  public void setRespuesta(String respuesta) {
    Respuesta = respuesta;
  }

  public ApiResponseBodyFault getClientException() {
    return ClientException;
  }

  public void setClientException(ApiResponseBodyFault clientException) {
    ClientException = clientException;
  }

  public ApiResponseBodyFault getServerException() {
    return ServerException;
  }

  public void setServerException(ApiResponseBodyFault serverException) {
    ServerException = serverException;
  }
}
