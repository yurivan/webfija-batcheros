package pe.com.tdp.ventafija.microservices.domain.customer;

public class ContractWebResponseDataParagraph {

	private String paragraph;
	private String category;

	public ContractWebResponseDataParagraph(String paragraph,String category) {
		this.paragraph=paragraph;
		this.category=category;
	}

	public String getParagraph() {
		return paragraph;
	}

	public void setParagraph(String paragraph) {
		this.paragraph = paragraph;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
