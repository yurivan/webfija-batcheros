package pe.com.tdp.ventafija.microservices.service.product.command.gis;

import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;

import java.util.Map;

@Component("filterDuoBA")
public class DuoBAFilterCommand implements GisFilterCommand {
    // Filtro para DUO BA (4)
    private static String DUOBA_FILTER = " (OFERTA.targetData = '4' AND CAT.internetTech = '%s' AND CAT.internetSpeed <= %s)";

    @Override
    public void execute(WSSIGFFTT_FFTT_AVEResult resultGis, Map<String, String> variables, StringBuilder queryBuilder, StringBuilder columnsBuilder) {
        String hasTecnologiaGPON = resultGis.getGPON_TEC();
        String velocidadGPON = resultGis.getGPON_VEL();

        String hasTecnologiaHFC = resultGis.getHFC_TEC();
        String velocidadHFC = resultGis.getHFC_VEL();

        String hasTecnologiaADSL = resultGis.getXDSL_TEC();
        String velocidadADSL = resultGis.getXDSL_VEL_TRM();
        String hasTecnologiaADSLSaturado = resultGis.getXDSL_SAT();

        // Evaluamos DUOS BA (4)
        if ("SI".equals(hasTecnologiaGPON)) {
            String filtro = String.format(DUOBA_FILTER, "FTTH", velocidadGPON);
            queryBuilder.append(filtro + " OR ");
        }else if ("SI".equals(hasTecnologiaHFC)) {
            String filtro = String.format(DUOBA_FILTER, "HFC", velocidadHFC);
            queryBuilder.append(filtro + " OR ");
        }else if ("SI".equals(hasTecnologiaADSL) && "NO".equals(hasTecnologiaADSLSaturado)) {
            String filtro = String.format(DUOBA_FILTER, "ADSL", velocidadADSL);
            queryBuilder.append(filtro + " OR ");
        }
    }
}
