package pe.com.tdp.ventafija.microservices.service.product.command.campania;

import pe.com.tdp.ventafija.microservices.domain.product.OffersRequest2;
import pe.com.tdp.ventafija.microservices.service.command.FilterCommand;

public interface CampaignFilterCommand extends FilterCommand<OffersRequest2> {

}
