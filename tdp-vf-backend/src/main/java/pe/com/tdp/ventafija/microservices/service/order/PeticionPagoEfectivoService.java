package pe.com.tdp.ventafija.microservices.service.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.domain.order.entity.PeticionPagoEfectivo;
import pe.com.tdp.ventafija.microservices.domain.repository.PeticionPagoEfectivoDAO;
import pe.com.tdp.ventafija.microservices.domain.repository.PeticionPagoEfectivoRepository;
import pe.pagoefectivo.service.ws.BEGenRequest;

@Service
public class PeticionPagoEfectivoService {
	private static final Logger logger = LogManager.getLogger(PeticionPagoEfectivoService.class);
	@Autowired
	private PeticionPagoEfectivoRepository pagoEfectivoRepository;
	@Autowired
	private PeticionPagoEfectivoDAO dao;

	@Transactional
	public PeticionPagoEfectivo nuevaPeticion (PeticionPagoEfectivo peticion) {
		logger.info("registrar peticion pago efectivo");
		peticion.setStatus("PE");
		PeticionPagoEfectivo ppe = pagoEfectivoRepository.save(peticion);
		logger.info("fin registro peticion pago efectivo");
		return ppe;
	}
	
	public PeticionPagoEfectivo findByOrderId (String orderId) {
		return pagoEfectivoRepository.findByOrderId(orderId);
	}

	public BEGenRequest loadPagoEfectivo (Integer id) {

		BEGenRequest rq = dao.loadPagoEfectivoData(id);

		return rq;
	}

	public void actualizarPagoEfectivo (String id, String codPago) {

		dao.actualizarPagoEfectivoData(id, "EN", codPago);

	}
}
