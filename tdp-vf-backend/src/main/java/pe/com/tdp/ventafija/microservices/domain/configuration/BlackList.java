package pe.com.tdp.ventafija.microservices.domain.configuration;

import java.util.List;

public class BlackList {

    private List<String> phone;

    public List<String> getPhone() {
        return phone;
    }

    public void setPhone(List<String> phone) {
        this.phone = phone;
    }
}
