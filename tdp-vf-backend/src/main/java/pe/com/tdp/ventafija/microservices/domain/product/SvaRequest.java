package pe.com.tdp.ventafija.microservices.domain.product;

public class SvaRequest {

    private String productCode;
    private String productId;
    private String applicationCode;
    // Sprint 10 - svas por default: svasBloqueTV, svasLinea, svasInternet
    private String altaPuraSvasBloqueTV;
    private String altaPuraSvasLinea;
    private String altaPuraSvasInternet;

    private String altaPuraSvasAll;
    private String tvTech;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getApplicationCode() {
        return applicationCode;
    }

    public void setApplicationCode(String applicationCode) {
        this.applicationCode = applicationCode;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getAltaPuraSvasBloqueTV() {
        return altaPuraSvasBloqueTV;
    }

    public void setAltaPuraSvasBloqueTV(String altaPuraSvasBloqueTV) {
        this.altaPuraSvasBloqueTV = altaPuraSvasBloqueTV;
    }

    public String getAltaPuraSvasLinea() {
        return altaPuraSvasLinea;
    }

    public void setAltaPuraSvasLinea(String altaPuraSvasLinea) {
        this.altaPuraSvasLinea = altaPuraSvasLinea;
    }

    public String getAltaPuraSvasInternet() {
        return altaPuraSvasInternet;
    }

    public void setAltaPuraSvasInternet(String altaPuraSvasInternet) {
        this.altaPuraSvasInternet = altaPuraSvasInternet;
    }

    public String getAltaPuraSvasAll() {
        return altaPuraSvasAll;
    }

    public void setAltaPuraSvasAll(String altaPuraSvasAll) {
        this.altaPuraSvasAll = altaPuraSvasAll;
    }

    public String getTvTech() {
        return tvTech;
    }

    public void setTvTech(String tvTech) {
        this.tvTech = tvTech;
    }
}
