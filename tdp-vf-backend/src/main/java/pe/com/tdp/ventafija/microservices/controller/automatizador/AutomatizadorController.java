package pe.com.tdp.ventafija.microservices.controller.automatizador;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.common.connection.Api;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.automatizador.DetalleEstadoSolicitud;
import pe.com.tdp.ventafija.microservices.domain.automatizador.EstadoSolicitudRequest;
import pe.com.tdp.ventafija.microservices.domain.automatizador.EstadoSolicitudResponse;
import pe.com.tdp.ventafija.microservices.service.automatizador.AutomatizadorService;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RestController
public class AutomatizadorController {

    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private AutomatizadorService automatizadorService;

    @Autowired
    private AutomatizadorConection autoConec;
    private String urlDetail;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/automatizador/informaestado", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public ResponseEntity<EstadoSolicitudResponse> informaEstadoSolicitud(@RequestBody EstadoSolicitudRequest estadoSolicitudRequest) {
        LogVass.startController(logger, "InformaEstadoSolicitud", estadoSolicitudRequest);

        ResponseEntity<EstadoSolicitudResponse> response = null;

        EstadoSolicitudResponse estadoSolicitudResponse = new EstadoSolicitudResponse();
        if (estadoSolicitudRequest == null) {
            estadoSolicitudResponse.setCodErrCd("ERR – 0001");
            estadoSolicitudResponse.setObsErrDs("Request Vacio");
            response = new ResponseEntity<EstadoSolicitudResponse>(estadoSolicitudResponse, HttpStatus.NOT_FOUND);
            LogVass.finishController(logger, "InformaEstadoSolicitud", response);
            return response;
        } else {
            boolean creado = automatizadorService.createAutomatizador(estadoSolicitudRequest);
            if (creado) {
                estadoSolicitudResponse.setCodErrCd("ERR – 0000");
                estadoSolicitudResponse.setObsErrDs("Update OK");
                response = new ResponseEntity<EstadoSolicitudResponse>(estadoSolicitudResponse, HttpStatus.OK);
                LogVass.finishController(logger, "InformaEstadoSolicitud", response);
                return response;
            }
        }
        estadoSolicitudResponse.setCodErrCd("ERR – 0001");
        estadoSolicitudResponse.setObsErrDs("Update ERROR");
        response = new ResponseEntity<EstadoSolicitudResponse>(estadoSolicitudResponse, HttpStatus.NOT_FOUND);
        LogVass.finishController(logger, "InformaEstadoSolicitud", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/automatizador/registersale", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public ResponseEntity<EstadoSolicitudResponse> registerSale(@RequestBody EstadoSolicitudRequest estadoSolicitudRequest) {
        LogVass.startController(logger, "RegistrarPedido", estadoSolicitudRequest);

        ResponseEntity<EstadoSolicitudResponse> response = null;

        //automatizadorService.testRegisterSale();

        LogVass.finishController(logger, "RegistrarPedido", response);
        return response;
    }


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/automatizador/getConsulta", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public ResponseEntity<DetalleEstadoSolicitud> basicAuth() {
        logger.info("login basic auth");
        ResponseEntity<DetalleEstadoSolicitud> response = null;
        DetalleEstadoSolicitud data=null;
        String authorization="";
        try{
            String parameter="";
            data = autoConec.loadAutomatizador(authorization);

        }catch (Exception e){
                logger.info("error : "+ e);
        }

        return response;
    }


}
