package pe.com.tdp.ventafija.microservices.service.reniec;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientBIOConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.bio.BioClient;
import pe.com.tdp.ventafija.microservices.common.clients.bio.ValidateCustomerIdentityRequestData;
import pe.com.tdp.ventafija.microservices.common.clients.bio.ValidateCustomerIdentityResponseData;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseBIO;
import pe.com.tdp.ventafija.microservices.common.clients.dto.bio.CharacteristicList;
import pe.com.tdp.ventafija.microservices.common.clients.dto.bio.PersonDetails;
import pe.com.tdp.ventafija.microservices.common.clients.dto.bio.Status;
import pe.com.tdp.ventafija.microservices.common.clients.reniec.IdentifyApiReniecRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.reniec.IdentifyApiReniecResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.reniec.ReniecClient;
import pe.com.tdp.ventafija.microservices.common.connection.Firebase;
import pe.com.tdp.ventafija.microservices.common.domain.message.MessageEntities;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.MessageUtil;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.customer.BiometricRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.BiometricResponseData;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyResponseData;
import pe.com.tdp.ventafija.microservices.repository.reniec.ReniecDAO;

import java.text.Normalizer;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


@Service
public class ReniecService {
    private static final Logger logger = LogManager.getLogger(ReniecService.class);

    @Autowired
    private MessageUtil messageUtil;

    private ApiHeaderConfig apiHeaderConfig;
    private Firebase firebase;
    private ReniecDAO reniecDAO;
    private ServiceCallEventsService serviceCallEventsService;

    /* Valores para RENIEC método identify */
    @Value("${identity.validation.parents.options}")
    private Long identityValidationParentsOptions;
    @Value("${customer.mensaje.opcion.seleccione.padre}")
    private String opcionSeleccionePadre;
    @Value("${customer.mensaje.opcion.seleccione.madre}")
    private String opcionSeleccioneMadre;
    @Value("${custom.mensajes.validReniec.ErrorReniec}")
    private String mensajeValidReniecErrorReniec;
    @Value("${custom.mensajes.validReniec.ErrorGener}")
    private String mensajeValidReniecErrorGener;

    @Autowired
    public ReniecService(ApiHeaderConfig apiHeaderConfig,
                         Firebase firebase,
                         ReniecDAO reniecDAO,
                         ServiceCallEventsService serviceCallEventsService) {
        super();
        this.apiHeaderConfig = apiHeaderConfig;
        this.firebase = firebase;
        this.reniecDAO = reniecDAO;
        this.serviceCallEventsService = serviceCallEventsService;
    }

    /**
     * Función utilizada para obtener datos de RENIEC
     *
     * @return Response<IdentifyResponseData>
     * @request IdentifyRequest
     */
    public Response<IdentifyResponseData> identify(IdentifyRequest request) {
        logger.info("Inicio Identify Service");
        //Begin--Code for call Service
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("RENIEC");
        LogVass.serviceResponseHashMap(logger, "GetOrder", "messageList", messageList);
        //End--Code for call Service
        Response<IdentifyResponseData> response = new Response<>();
        try {
            /* Validar RUC 10 */
            if (request.getDocumentNumber() != null && request.getDocumentNumber().length() > 8) {
                request.setDocumentNumber(request.getDocumentNumber().substring(2, 10));
            }

            IdentifyApiReniecRequestBody apiReniecRequestBody = new IdentifyApiReniecRequestBody();
            apiReniecRequestBody.setNumDNI(request.getDocumentNumber());

            ApiResponse<IdentifyApiReniecResponseBody> objReniec = getReniec(apiReniecRequestBody, request.getId());
            LogVass.serviceResponseObject(logger, "Identify", "objReniec", objReniec);

            if ((Constants.RESPONSE).equals(objReniec.getHeaderOut().getMsgType())) {
                IdentifyResponseData responseData = new IdentifyResponseData();
                responseData.setLastName(objReniec.getBodyOut().getApePaterno());
                responseData.setMotherLastName(objReniec.getBodyOut().getApeMaterno() == null ? "" : objReniec.getBodyOut().getApeMaterno());
                responseData.setNames(objReniec.getBodyOut().getNombres());
                responseData.setDepartmentBirthLocation(objReniec.getBodyOut().getNacUbiDepartamento());
                responseData.setProvinceBirthLocation(objReniec.getBodyOut().getNacUbiProvincia());
                responseData.setDistrictBirthLocation(objReniec.getBodyOut().getNacUbiDistrito());
                responseData.setBirthdate(objReniec.getBodyOut().getFecNacimiento());
                responseData.setFatherName(objReniec.getBodyOut().getNomPadre() == null ? "" : objReniec.getBodyOut().getNomPadre());
                responseData.setMotherName(objReniec.getBodyOut().getNomMadre() == null ? "" : objReniec.getBodyOut().getNomMadre());
                responseData.setExpeditionDate(objReniec.getBodyOut().getFecExpedicion());
                responseData.setRestriction(objReniec.getBodyOut().getRestriccion());

                List<String> listaPadre = reniecDAO.getNames(responseData.getFatherName(), Constants.GENDER_MAS, identityValidationParentsOptions);
                LogVass.serviceResponseArray(logger, "Identify", "listaPadre", listaPadre);
                List<String> listaMadre = reniecDAO.getNames(responseData.getMotherName(), Constants.GENDER_FEM, identityValidationParentsOptions);
                LogVass.serviceResponseArray(logger, "Identify", "listaMadre", listaMadre);

                listaMadre.add(ThreadLocalRandom.current().nextInt(0, identityValidationParentsOptions.intValue()), responseData.getMotherName());
                listaPadre.add(ThreadLocalRandom.current().nextInt(0, identityValidationParentsOptions.intValue()), responseData.getFatherName());

                listaPadre.add(0, opcionSeleccionePadre);
                listaMadre.add(0, opcionSeleccioneMadre);

                responseData.setMotherNameAlternatives(listaMadre);
                responseData.setFatherNameAlternatives(listaPadre);

                response.setResponseCode("0");
                response.setResponseData(responseData);
            } else {
            response.setResponseCode(messageList.get("dniFail").getCode());
            response.setResponseMessage(messageList.get("dniFail").getMessage());
            }
        } /*catch (ClientException e) {
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            logger.error("Error Identify Service.", e);
            logger.info("Fin Identify Service");
            return response;
        } */ catch (Exception e) {
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            logger.error("Error Identify Service", e);
            logger.info("Fin Identify Service");
            return response;
        }

        logger.info("Fin Identify Service");
        return response;
    }

    /**
     * Función client para obtener datos de RENIEC
     *
     * @return ApiResponse<IdentifyApiReniecResponseBody>
     * @request IdentifyApiReniecRequestBody
     */
    private ApiResponse<IdentifyApiReniecResponseBody> getReniec(IdentifyApiReniecRequestBody apiRequestBody, String orderId) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getReniecConsultaClienteUri())
                .setApiId(apiHeaderConfig.getReniecConsultarClienteApiId())
                .setApiSecret(apiHeaderConfig.getReniecConsultarClienteApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_RENIEC)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_RENIEC)
                .build();

        ReniecClient reniecClient = new ReniecClient(config);
        ClientResult<ApiResponse<IdentifyApiReniecResponseBody>> result = reniecClient.post(apiRequestBody);
        if ((Constants.RESPONSE).equals(result.getResult().getHeaderOut().getMsgType())) {
            if (result.getResult().getBodyOut().getRestriccion() != null && result.getResult().getBodyOut().getRestriccion().equalsIgnoreCase("A")) {
                result.getResult().getHeaderOut().setMsgType("ERROR");
                result.getResult().getBodyOut().setRestriccion("FALLECIMIENTO");
                result.getEvent().setServiceResponse(result.getEvent().getServiceResponse().replace("\"restriccion\" : \"A\"", "\"restriccion\" : \"FALLECIMIENTO\""));
            }
        }

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
        result.getEvent().setOrderId(orderId);
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

    /**
     * Función para obtener datos de RENIEC BIOMETRICO
     *
     * @return Response<BiometricResponseData>
     * @request BiometricRequest
     */
    public Response<BiometricResponseData> biometric(BiometricRequest biometricRequest) {
        logger.info("Inicio Biometric Service");

        Response<BiometricResponseData> response = new Response<>();
        BiometricResponseData responseData = new BiometricResponseData();

        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("BIOMETRIA");
        LogVass.serviceResponseHashMap(logger, "Biometric", "messageList", messageList);

        try {
            /* Evaluar parametros null */
            if (biometricRequest == null) {
                response.setResponseCode(messageList.get("requestNull").getCode());
                response.setResponseMessage(messageList.get("requestNull").getMessage());
            } else if (biometricRequest.getCustomerDocument() == null || biometricRequest.getCustomerDocument().equalsIgnoreCase("")) {
                response.setResponseCode(messageList.get("documentoVacio").getCode());
                response.setResponseMessage(messageList.get("documentoVacio").getMessage());
            } else if (biometricRequest.getCustomerDocumentType() == null || biometricRequest.getCustomerDocumentType().equalsIgnoreCase("")) {
                response.setResponseCode(messageList.get("tipoDocumentoVacio").getCode());
                response.setResponseMessage(messageList.get("tipoDocumentoVacio").getMessage());
            } else if (biometricRequest.getCustomerIdTransaction() == null || biometricRequest.getCustomerIdTransaction().equalsIgnoreCase("")) {
                response.setResponseCode(messageList.get("idTransactionVacio").getCode());
                response.setResponseMessage(messageList.get("idTransactionVacio").getMessage());
            } else if (biometricRequest.getSellerDocument() == null || biometricRequest.getSellerDocument().equalsIgnoreCase("")) {
                response.setResponseCode(messageList.get("vendedorDniVacio").getCode());
                response.setResponseMessage(messageList.get("vendedorDniVacio").getMessage());
            }
            if (response.getResponseCode() != null && response.getResponseCode().equalsIgnoreCase("1")) {
                logger.info("Fin Biometric Service");
                return response;
            }
            /* Evaluar parametros null */

            /* Declarar las variables de body */
            ValidateCustomerIdentityRequestData validateCustomerIdentityRequestData = new ValidateCustomerIdentityRequestData();
            validateCustomerIdentityRequestData.setDocumentType(biometricRequest.getCustomerDocumentType().toUpperCase());
            validateCustomerIdentityRequestData.setDocumentNumber(biometricRequest.getCustomerDocument());
            validateCustomerIdentityRequestData.setOrderActionType("PR");
            validateCustomerIdentityRequestData.setModalityID("1");
            //validateCustomerIdentityRequestData.setSalesChannel("");
            validateCustomerIdentityRequestData.setMotive("1");
            CharacteristicList characteristicList = new CharacteristicList();
            Status status = new Status();
            status.setCode("idTransaccionGT");
            status.setDescription(biometricRequest.getCustomerIdTransaction());
            characteristicList.setStatus(status);
            validateCustomerIdentityRequestData.setCharacteristicList(characteristicList);

            ApiResponseBIO<ValidateCustomerIdentityResponseData> objBIO = getBIO(validateCustomerIdentityRequestData, biometricRequest);

            if (objBIO.getValidateCustomerIdentityResponseData() == null) {
                response.setResponseCode(messageList.get("errorGeneric").getCode());
                response.setResponseMessage(messageList.get("errorGeneric").getMessage());
                logger.info("objBIO.getValidateCustomerIdentityResponseData(): null");
                logger.info("Fin Biometric Service");
                return response;
            } else {
                responseData.setValidationResult(objBIO.getValidateCustomerIdentityResponseData().getValidationResult());
                responseData.setValidationTransactionID(objBIO.getValidateCustomerIdentityResponseData().getValidationTransactionID());
                responseData.setIdScoringQuery(objBIO.getValidateCustomerIdentityResponseData().getIdScoringQuery());
                if (responseData.getValidationResult().equals("HIT"))
                    responseData = setIndividualInformation(objBIO.getValidateCustomerIdentityResponseData().getPersonDetails(), responseData);
                response.setResponseCode("0");
                response.setResponseData(responseData);
            }
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            logger.error("Error Biometric Service", e);
            logger.info("Fin Biometric Service");
            return response;
        }

        logger.info("Fin Biometric Service");
        return response;
    }

    /**
     * Función para obtener datos de RENIEC BIOMETRICO
     *
     * @return Response<BiometricResponseData>
     * @request BiometricRequest
     */
    private BiometricResponseData setIndividualInformation(PersonDetails personDetails, BiometricResponseData responseData) {
        if (personDetails.getGivenNames() == null || personDetails.getGivenNames().equals("")) {
            responseData.setCustomerName("");
        } else {
            String customerName = Normalizer.normalize(personDetails.getGivenNames().toUpperCase(), Normalizer.Form.NFKD);
            responseData.setCustomerName(customerName.replaceAll("\\p{InCombiningDiacriticalMarks}+", ""));
        }

        String[] lastNames = personDetails.getFamilyNames().split(" ");
        if (lastNames[0].equals("") || lastNames[0].equals(" ")) {
            responseData.setCustomerFatherLastName("SINVALOR");
        } else {
            String fatherLastName = Normalizer.normalize(lastNames[0].toUpperCase(), Normalizer.Form.NFKD);
            responseData.setCustomerFatherLastName(fatherLastName.replaceAll("\\p{InCombiningDiacriticalMarks}+", ""));
        }

        String motherLastName = "";
        for (int i = 1; i < lastNames.length; i++) {
            motherLastName += lastNames[i] + " ";
        }
        String motherLastName2 = Normalizer.normalize(motherLastName.toUpperCase(), Normalizer.Form.NFKD);
        responseData.setCustomerMotherLastName(motherLastName2.replaceAll("\\p{InCombiningDiacriticalMarks}+", ""));
        return responseData;
    }

    /**
     * Función client para obtener datos de RENIEC BIOMETRICO
     *
     * @return ApiResponseBIO<ValidateCustomerIdentityResponseData>
     * @request ValidateCustomerIdentityRequestData & BiometricRequest
     */
    public ApiResponseBIO<ValidateCustomerIdentityResponseData> getBIO(ValidateCustomerIdentityRequestData request, BiometricRequest biometricRequest) throws ClientException {
        ClientBIOConfig config = new ClientBIOConfig.ClientBIOConfigBuilder()
                .setUrl(apiHeaderConfig.getReniecBiometricoClienteUri())
                .setApiId(apiHeaderConfig.getReniecBiometricoClienteApiId())
                .setApiSecret(apiHeaderConfig.getReniecBiometricoClienteApiSecret())
                .setServiceChannel(biometricRequest.getSellerChannel())
                .setUserLogin(biometricRequest.getSellerDocument()).build();
        BioClient bioClient = new BioClient(config);
        ClientResult<ApiResponseBIO<ValidateCustomerIdentityResponseData>> result = bioClient.post(request);
        ApiResponseBIO<ValidateCustomerIdentityResponseData> objBIO = result.getResult();
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return objBIO;
    }
}