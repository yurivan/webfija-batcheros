package pe.com.tdp.ventafija.microservices.domain.order;

public class UserInfoRequest {

	private String tipoAplicacion;

	public String getTipoAplicacion() {
		return tipoAplicacion;
	}

	public void setTipoAplicacion(String tipoAplicacion) {
		this.tipoAplicacion = tipoAplicacion;
	}
	
}
