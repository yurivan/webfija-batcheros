package pe.com.tdp.ventafija.microservices.controller.upfront;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.tdp.ventafija.microservices.domain.dto.PagoEfectivoNotificacionRequest;
import pe.com.tdp.ventafija.microservices.domain.dto.PagoEfectivoNotificacionResponse;
import pe.com.tdp.ventafija.microservices.service.order.UpfrontService;

@RestController
public class UpfrontController {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private UpfrontService upfrontService;


    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/pagoefectivo/urlnotif", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public ResponseEntity<PagoEfectivoNotificacionResponse> notifacionPagoEfectivo(@RequestBody PagoEfectivoNotificacionRequest pagoEfectivoNotificacionRequest) {
        logger.info("Notificacion pagoEfectivoNotificacionRequest=", pagoEfectivoNotificacionRequest);
        PagoEfectivoNotificacionResponse pagoEfectivoNotificacionResponse = new PagoEfectivoNotificacionResponse();
        if (pagoEfectivoNotificacionRequest == null) {
            pagoEfectivoNotificacionResponse.setCodigo("000");
            pagoEfectivoNotificacionResponse.setRespuesta("Request Vacio");
            return new ResponseEntity<PagoEfectivoNotificacionResponse>(pagoEfectivoNotificacionResponse, HttpStatus.NOT_FOUND);
        } else {
            boolean update = upfrontService.updateStatusCIP(pagoEfectivoNotificacionRequest);
            if (update) {
                pagoEfectivoNotificacionResponse.setCodigo("001");
                pagoEfectivoNotificacionResponse.setRespuesta("Update OK");
                return new ResponseEntity<PagoEfectivoNotificacionResponse>(pagoEfectivoNotificacionResponse, HttpStatus.OK);
            }
        }
        pagoEfectivoNotificacionResponse.setCodigo("000");
        pagoEfectivoNotificacionResponse.setRespuesta("Update ERROR");
        return new ResponseEntity<PagoEfectivoNotificacionResponse>(pagoEfectivoNotificacionResponse, HttpStatus.NOT_FOUND);
    }
}
