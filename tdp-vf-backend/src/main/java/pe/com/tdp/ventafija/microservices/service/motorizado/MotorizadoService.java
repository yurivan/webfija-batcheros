package pe.com.tdp.ventafija.microservices.service.motorizado;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.common.dao.OrderRepository;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.constant.CustomerConstants;
import pe.com.tdp.ventafija.microservices.constants.motorizado.MotorizadoConstants;
import pe.com.tdp.ventafija.microservices.domain.Parameter;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.customer.ContractResponseData;
import pe.com.tdp.ventafija.microservices.domain.customer.ContractResponseParameter;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.domain.motorizado.MotorizadoRequestData;
import pe.com.tdp.ventafija.microservices.domain.motorizado.entity.TdpMotorizadoBase;
import pe.com.tdp.ventafija.microservices.domain.order.entity.AzureRequest;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpOrder;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpVisor;
import pe.com.tdp.ventafija.microservices.domain.repository.AzureRequestRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpMotorizadoBaseRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpOrderRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpVisorRepository;
import pe.com.tdp.ventafija.microservices.repository.customer.CustomerDAO;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class MotorizadoService {

    private static final Logger logger = LogManager.getLogger();

    @Value("${operation.comercial.type}")
    private String operacionComercialWithTypeCode;
    @Autowired
    private TdpMotorizadoBaseRepository tdpMotorizadoBaseRepository;
    @Autowired
    private TdpVisorRepository tdpVisorRepository;
    @Autowired
    private TdpOrderRepository tdpOrderRepository;
    @Autowired
    private CustomerDAO customerDAO;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private AzureRequestRepository azureRequestRepository;

    @Autowired
    private ServiceCallEventsService serviceCallEventsService;

    public Response<List<TdpMotorizadoBase>> bandejaDelivery(String codMotorizado) {
        logger.info("Inicio BandejaDelivery Service.");
        Response<List<TdpMotorizadoBase>> response = new Response<>();

        try {
            List<TdpMotorizadoBase> tdpMotorizadoBaseList = tdpMotorizadoBaseRepository.findAllByCodMotorizadoAndEstado(codMotorizado, "PENDIENTE");

            for (TdpMotorizadoBase obj : tdpMotorizadoBaseList) {
                if (obj.getTipDocCliente().equalsIgnoreCase("Otros Extranjeros - Aut. SNM"))
                    obj.setTipDocCliente("OTROS");
            }

            response.setResponseCode("01");
            response.setResponseData(tdpMotorizadoBaseList);
        } catch (Exception e) {
            response.setResponseCode("00");
            response.setResponseMessage("No se encontraron ventas pendientes.");
            logger.error("Error ValidateCategoryActive Controller. ", e);
        }
        logger.info("Fin BandejaDelivery Service.");
        return response;
    }

    public Response<SaleApiFirebaseResponseBody> resumenVenta(MotorizadoRequestData request) {
        logger.info("Inicio ResumenVenta Service.");
        Response<SaleApiFirebaseResponseBody> response = new Response<>();
        response.setResponseCode("01");

        try {
            TdpMotorizadoBase tdpMotorizadoBase = tdpMotorizadoBaseRepository.findOneByCodMotorizadoAndCodigoPedidoAndNumDocCliente(request.getCodmotorizado(), request.getCodigopedido(), request.getDocumentocliente());
            if (tdpMotorizadoBase == null) {
                response.setResponseCode("00");
                response.setResponseMessage("Está venta ha sido reasignada, por favor actualice su bandeja.");
                logger.info("Fin ResumenVenta Service.");
                return response;
            }
            LogVass.serviceResponseObject(logger, "ResumenVenta", "tdpMotorizadoBase", tdpMotorizadoBase);

            if (!tdpMotorizadoBase.getEstado().equalsIgnoreCase(MotorizadoConstants.MOTORIZADO_ESTADO_PENDIENTE)) {
                response.setResponseCode("00");
                response.setResponseMessage("Está venta ya fue iniciada o procesada.");
                logger.info("Fin ResumenVenta Service.");
                return response;
            }

            List<TdpVisor> tdpVisorList = tdpVisorRepository.findAllByCodigoPedidoAndDni(request.getCodigopedido(), request.getDocumentocliente());
            if (tdpVisorList == null || tdpVisorList.size() == 0) {
                response.setResponseCode("00");
                response.setResponseMessage("Está venta no existe, por favor actualice su bandeja.");
                logger.info("Fin ResumenVenta Service.");
                return response;
            }
            LogVass.serviceResponseArray(logger, "ResumenVenta", "tdpVisorList", tdpVisorList);
            TdpVisor tdpVisor = tdpVisorList.get(0);
            LogVass.serviceResponseObject(logger, "ResumenVenta", "tdpVisor", tdpVisor);

            TdpOrder tdpOrder = tdpOrderRepository.findOneByOrderId(tdpVisor.getIdVisor());
            LogVass.serviceResponseObject(logger, "ResumenVenta", "tdpOrder", tdpOrder);
            if (tdpOrder == null) {
                response.setResponseCode("00");
                response.setResponseMessage("No se encuentra registrada la venta, por favor comunicarlo con Soporte.");
                logger.info("Fin ResumenVenta Service.");
                return response;
            }

            SaleApiFirebaseResponseBody saleApiFirebaseResponseBody = new SaleApiFirebaseResponseBody();

            /* Order */
            saleApiFirebaseResponseBody.setId(tdpVisor.getIdVisor() + "_motorizado");
            saleApiFirebaseResponseBody.setProduct_has_discount(true);
            /* Operación Comercial */
            JSONObject tipoVenta = new JSONObject();
            String[] operacionComercialWithType = operacionComercialWithTypeCode.split(",");
            for (String obj : operacionComercialWithType) {
                String[] operationComercial = obj.split("-");
                tipoVenta.put(operationComercial[0], operationComercial[1]);
            }
            /*
            tipoVenta.put("ALTA PURA", "A");
            tipoVenta.put("MIGRACION", "M");
            tipoVenta.put("MIGRACIONES", "M");
            tipoVenta.put("ALTA COMPONENTE", "M");
            tipoVenta.put("ALTA COMPONENTE BA", "M");
            tipoVenta.put("ALTA COMPONENTE TV", "M");
            tipoVenta.put("SVAS", "S");
            tipoVenta.put("SVA", "S");
            */
            String operationComercial = tipoVenta.get(tdpVisor.getOperacionComercial().toUpperCase()).toString();
            saleApiFirebaseResponseBody.setType(operationComercial != null || operationComercial.equalsIgnoreCase("") ? operationComercial : "A");

            /* Cliente */
            saleApiFirebaseResponseBody.setClient_names(tdpVisor.getCliente());
            saleApiFirebaseResponseBody.setClient_lastname("");
            saleApiFirebaseResponseBody.setClient_mother_lastname("");
            saleApiFirebaseResponseBody.setClient_doc_number(tdpVisor.getDni());
            saleApiFirebaseResponseBody.setClient_document(tdpVisor.getTipoDocumento());
            saleApiFirebaseResponseBody.setClient_mobile_phone(tdpVisor.getTelefono());
            saleApiFirebaseResponseBody.setClient_email(tdpOrder.getClientEmail());

            /* Dirección */
            String[] direccion = tdpVisor.getDireccion().split("-");
            String[] direccionSplit = direccion[0].split(",");

            String addressReferencia = "";
            String address = "";
            if (direccion.length == 2)
                addressReferencia = direccion[1];
            if (direccionSplit.length > 0)
                address = direccionSplit[0];
            saleApiFirebaseResponseBody.setAddress(address == null ? "" : address);
            saleApiFirebaseResponseBody.setAddress_referencia(addressReferencia == null ? "" : addressReferencia);

            /* Producto */
            saleApiFirebaseResponseBody.setProduct_name(tdpVisor.getNombreProducto());
            saleApiFirebaseResponseBody.setProduct_type(tdpVisor.getTipoProducto());
            saleApiFirebaseResponseBody.setProduct_category(tdpVisor.getSubProducto());
            saleApiFirebaseResponseBody.setProduct_price(tdpOrder.getProductPrecioNormal());
            saleApiFirebaseResponseBody.setProduct_has_discount(tdpOrder.getProductPrecioPromo() == null || tdpOrder.getProductPrecioPromo() == BigDecimal.ZERO ? false : true);
            saleApiFirebaseResponseBody.setProduct_prom_price(tdpOrder.getProductPrecioPromo());
            saleApiFirebaseResponseBody.setMonth_period(tdpOrder.getProductPrecioPromoMes() == null ? null : "" + tdpOrder.getProductPrecioPromoMes());
            saleApiFirebaseResponseBody.setProduct_install_cost(tdpOrder.getProductCostInstall());
            saleApiFirebaseResponseBody.setProduct_financing_month(tdpOrder.getProductCostInstallMonth());
            saleApiFirebaseResponseBody.setProduct_financing_cost(BigDecimal.ZERO);
            saleApiFirebaseResponseBody.setProm_speed(tdpOrder.getProductInternetPromo());
            saleApiFirebaseResponseBody.setPeriod_prom_speed(tdpOrder.getProductInternetPromoTiempo());
            saleApiFirebaseResponseBody.setProduct_equipamiento_tv(tdpOrder.getProductEquipTv());
            saleApiFirebaseResponseBody.setProduct_equipamiento_internet(tdpOrder.getProductEquipInter());
            saleApiFirebaseResponseBody.setProduct_equipamiento_linea(tdpOrder.getProductEquipLine());
            saleApiFirebaseResponseBody.setProduct_payment_method(tdpOrder.getProductPayMethod());
            saleApiFirebaseResponseBody.setProduct_cash_price(tdpOrder.getProductPayPrice());
            saleApiFirebaseResponseBody.setProduct_return_month(tdpOrder.getProductPayReturnMonth());
            saleApiFirebaseResponseBody.setProduct_return_period(tdpOrder.getProductPayReturnPeriod());
            saleApiFirebaseResponseBody.setProduct_has_equipment(true);

            String sva1 = tdpOrder.getSvaCode1() + "-" + tdpOrder.getSvaNombre1() + "-" + tdpOrder.getSvaCantidad1() + "-" + (tdpOrder.getSvaCantidad1() == null || tdpOrder.getSvaPrecio1() == null ? "" : "" + tdpOrder.getSvaPrecio1().multiply(new BigDecimal(tdpOrder.getSvaCantidad1())));
            String sva2 = tdpOrder.getSvaCode2() + "-" + tdpOrder.getSvaNombre2() + "-" + tdpOrder.getSvaCantidad2() + "-" + (tdpOrder.getSvaCantidad2() == null || tdpOrder.getSvaPrecio2() == null ? "" : "" + tdpOrder.getSvaPrecio2().multiply(new BigDecimal(tdpOrder.getSvaCantidad2())));
            String sva3 = tdpOrder.getSvaCode3() + "-" + tdpOrder.getSvaNombre3() + "-" + tdpOrder.getSvaCantidad3() + "-" + (tdpOrder.getSvaCantidad3() == null || tdpOrder.getSvaPrecio3() == null ? "" : "" + tdpOrder.getSvaPrecio3().multiply(new BigDecimal(tdpOrder.getSvaCantidad3())));
            String sva4 = tdpOrder.getSvaCode4() + "-" + tdpOrder.getSvaNombre4() + "-" + tdpOrder.getSvaCantidad4() + "-" + (tdpOrder.getSvaCantidad4() == null || tdpOrder.getSvaPrecio4() == null ? "" : "" + tdpOrder.getSvaPrecio4().multiply(new BigDecimal(tdpOrder.getSvaCantidad4())));
            String sva5 = tdpOrder.getSvaCode5() + "-" + tdpOrder.getSvaNombre5() + "-" + tdpOrder.getSvaCantidad5() + "-" + (tdpOrder.getSvaCantidad5() == null || tdpOrder.getSvaPrecio5() == null ? "" : "" + tdpOrder.getSvaPrecio5().multiply(new BigDecimal(tdpOrder.getSvaCantidad5())));
            String sva6 = tdpOrder.getSvaCode6() + "-" + tdpOrder.getSvaNombre6() + "-" + tdpOrder.getSvaCantidad6() + "-" + (tdpOrder.getSvaCantidad6() == null || tdpOrder.getSvaPrecio6() == null ? "" : "" + tdpOrder.getSvaPrecio6().multiply(new BigDecimal(tdpOrder.getSvaCantidad6())));
            String sva7 = tdpOrder.getSvaCode7() + "-" + tdpOrder.getSvaNombre7() + "-" + tdpOrder.getSvaCantidad7() + "-" + (tdpOrder.getSvaCantidad7() == null || tdpOrder.getSvaPrecio7() == null ? "" : "" + tdpOrder.getSvaPrecio7().multiply(new BigDecimal(tdpOrder.getSvaCantidad7())));
            String sva8 = tdpOrder.getSvaCode8() + "-" + tdpOrder.getSvaNombre8() + "-" + tdpOrder.getSvaCantidad8() + "-" + (tdpOrder.getSvaCantidad8() == null || tdpOrder.getSvaPrecio8() == null ? "" : "" + tdpOrder.getSvaPrecio8().multiply(new BigDecimal(tdpOrder.getSvaCantidad8())));
            String sva9 = tdpOrder.getSvaCode9() + "-" + tdpOrder.getSvaNombre9() + "-" + tdpOrder.getSvaCantidad9() + "-" + (tdpOrder.getSvaCantidad9() == null || tdpOrder.getSvaPrecio9() == null ? "" : "" + tdpOrder.getSvaPrecio9().multiply(new BigDecimal(tdpOrder.getSvaCantidad9())));
            String sva10 = tdpOrder.getSvaCode10() + "-" + tdpOrder.getSvaNombre10() + "-" + tdpOrder.getSvaCantidad10() + "-" + (tdpOrder.getSvaCantidad10() == null || tdpOrder.getSvaPrecio10() == null ? "" : "" + tdpOrder.getSvaPrecio10().multiply(new BigDecimal(tdpOrder.getSvaCantidad10())));
            String sva = (sva1 == null ? "" : sva1) + ";"
                    + (sva2 == null ? "" : sva2) + ";"
                    + (sva3 == null ? "" : sva3) + ";"
                    + (sva4 == null ? "" : sva4) + ";"
                    + (sva5 == null ? "" : sva5) + ";"
                    + (sva6 == null ? "" : sva6) + ";"
                    + (sva7 == null ? "" : sva7) + ";"
                    + (sva8 == null ? "" : sva8) + ";"
                    + (sva9 == null ? "" : sva9) + ";"
                    + (sva10 == null ? "" : sva10);
            String[] listSva = sva.split(";");

            List<String> svaList = new ArrayList();
            for (String svaObj : listSva) {
                if (!svaObj.equals("") && !svaObj.equalsIgnoreCase("---") && !(svaObj.indexOf("null") >= 0))
                    svaList.add(svaObj);
            }
            saleApiFirebaseResponseBody.setSva(svaList);

            /* Condiciones de Venta */
            saleApiFirebaseResponseBody.setAffiliation_data_protection(tdpOrder.getAffiliationDataProtection().equalsIgnoreCase("Si") ? true : false);
            saleApiFirebaseResponseBody.setAffiliation_electronic_invoice(tdpOrder.getAffiliationElectronicInvoice().equalsIgnoreCase("Si") ? true : false);
            saleApiFirebaseResponseBody.setDisaffiliation_white_pages_guide(tdpOrder.getDisaffiliationWhitePagesGuide().equalsIgnoreCase("Si") ? true : false);
            saleApiFirebaseResponseBody.setShipping_contracts_for_email(tdpOrder.getOrderContrato().equalsIgnoreCase("M") ? true : false);
            saleApiFirebaseResponseBody.setParental_protection(tdpOrder.getAffiliationParentalProtection().equalsIgnoreCase("Si") ? true : false);

            response.setResponseData(saleApiFirebaseResponseBody);

            //tdpMotorizadoBase.setEstado("EN PROCESO");
            //tdpMotorizadoBaseRepository.save(tdpMotorizadoBase);
        } catch (Exception e) {
            response.setResponseCode("00");
            response.setResponseMessage("No se logró encontrar venta.");
            logger.error("Error ResumenVenta Controller", e);
        }
        logger.info("Fin ResumenVenta Service.");
        return response;
    }

    public Response<ContractResponseData> contractVenta(MotorizadoRequestData request) {
        logger.info("Inicio ContractVenta Service.");
        Response<ContractResponseData> response = new Response<>();
        response.setResponseCode("01");

        try {
            TdpMotorizadoBase tdpMotorizadoBase = tdpMotorizadoBaseRepository.findOneByCodMotorizadoAndCodigoPedidoAndNumDocCliente(request.getCodmotorizado(), request.getCodigopedido(), request.getDocumentocliente());
            if (tdpMotorizadoBase == null) {
                response.setResponseCode("00");
                response.setResponseMessage("Está venta ha sido reasignada, por favor actualice su bandeja.");
                logger.info("Fin ContractVenta Service.");
                return response;
            }
            LogVass.serviceResponseObject(logger, "ContractVenta", "tdpMotorizadoBase", tdpMotorizadoBase);

            if (!tdpMotorizadoBase.getEstado().equalsIgnoreCase(MotorizadoConstants.MOTORIZADO_ESTADO_PENDIENTE)) {
                response.setResponseCode("00");
                response.setResponseMessage("Está venta ya fue iniciada o procesada.");
                logger.info("Fin ContractVenta Service.");
                return response;
            }

            List<TdpVisor> tdpVisorList = tdpVisorRepository.findAllByCodigoPedidoAndDni(request.getCodigopedido(), request.getDocumentocliente());
            if (tdpVisorList == null) {
                response.setResponseCode("00");
                response.setResponseMessage("Está venta no existe, por favor actualice su bandeja.");
                logger.info("Fin ContractVenta Service.");
                return response;
            }
            LogVass.serviceResponseArray(logger, "ContractVenta", "tdpVisorList", tdpVisorList);
            TdpVisor tdpVisor = tdpVisorList.get(0);
            LogVass.serviceResponseObject(logger, "ContractVenta", "tdpVisor", tdpVisor);

            TdpOrder tdpOrder = tdpOrderRepository.findOneByOrderId(tdpVisor.getIdVisor());
            LogVass.serviceResponseObject(logger, "ContractVenta", "tdpOrder", tdpOrder);
            if (tdpOrder == null) {
                response.setResponseCode("00");
                response.setResponseMessage("No se encuentra registrada la venta, por favor comunicarlo con Soporte.");
                logger.info("Fin ContractVenta Service.");
                return response;
            }

            SaleApiFirebaseResponseBody saleApiFirebaseResponseBody = new SaleApiFirebaseResponseBody();

            /* Order */
            saleApiFirebaseResponseBody.setId(tdpVisor.getIdVisor());
            saleApiFirebaseResponseBody.setProduct_has_discount(true);
            /* Operación Comercial */
            JSONObject tipoVenta = new JSONObject();
            String[] operacionComercialWithType = operacionComercialWithTypeCode.split(",");
            for (String obj : operacionComercialWithType) {
                String[] operationComercial = obj.split("-");
                tipoVenta.put(operationComercial[0], operationComercial[1]);
            }
            /*
            tipoVenta.put("ALTA PURA", "A");
            tipoVenta.put("MIGRACION", "M");
            tipoVenta.put("MIGRACIONES", "M");
            tipoVenta.put("ALTA COMPONENTE", "M");
            tipoVenta.put("ALTA COMPONENTE BA", "M");
            tipoVenta.put("ALTA COMPONENTE TV", "M");
            tipoVenta.put("SVAS", "S");
            tipoVenta.put("SVA", "S");
            */
            String operationComercial = tipoVenta.get(tdpVisor.getOperacionComercial().toUpperCase()).toString();
            saleApiFirebaseResponseBody.setType(operationComercial != null || operationComercial.equalsIgnoreCase("") ? operationComercial : "A");

            /* Cliente */
            saleApiFirebaseResponseBody.setClient_names(tdpVisor.getCliente());
            saleApiFirebaseResponseBody.setClient_lastname("");
            saleApiFirebaseResponseBody.setClient_mother_lastname("");
            saleApiFirebaseResponseBody.setClient_doc_number(tdpVisor.getDni());
            saleApiFirebaseResponseBody.setClient_document(tdpVisor.getTipoDocumento());
            saleApiFirebaseResponseBody.setClient_mobile_phone(tdpVisor.getTelefono());
            saleApiFirebaseResponseBody.setClient_email(tdpOrder.getClientEmail());

            /* Dirección */
            String[] direccion = tdpVisor.getDireccion().split("-");
            String[] direccionSplit = direccion[0].split(",");

            String addressReferencia = "";
            String address = "";
            if (direccion.length == 2)
                addressReferencia = direccion[1];
            if (direccionSplit.length > 0)
                address = direccionSplit[0];
            saleApiFirebaseResponseBody.setAddress(address == null ? "" : address);
            saleApiFirebaseResponseBody.setAddress_referencia(addressReferencia == null ? "" : addressReferencia);

            /* Producto */
            saleApiFirebaseResponseBody.setProduct_name(tdpVisor.getNombreProducto());
            saleApiFirebaseResponseBody.setProduct_type(tdpVisor.getTipoProducto());
            saleApiFirebaseResponseBody.setProduct_category(tdpVisor.getSubProducto());
            saleApiFirebaseResponseBody.setProduct_price(tdpOrder.getProductPrecioNormal());
            saleApiFirebaseResponseBody.setProduct_has_discount(tdpOrder.getProductPrecioPromo() == null || tdpOrder.getProductPrecioPromo() == BigDecimal.ZERO ? false : true);
            saleApiFirebaseResponseBody.setProduct_prom_price(tdpOrder.getProductPrecioPromo());
            saleApiFirebaseResponseBody.setMonth_period("" + tdpOrder.getProductPrecioPromoMes());
            saleApiFirebaseResponseBody.setProduct_install_cost(tdpOrder.getProductCostInstall());
            saleApiFirebaseResponseBody.setProduct_financing_month(tdpOrder.getProductCostInstallMonth());
            saleApiFirebaseResponseBody.setProduct_financing_cost(BigDecimal.ZERO);
            saleApiFirebaseResponseBody.setProm_speed(tdpOrder.getProductInternetPromo());
            saleApiFirebaseResponseBody.setPeriod_prom_speed(tdpOrder.getProductInternetPromoTiempo());
            saleApiFirebaseResponseBody.setProduct_equipamiento_tv(tdpOrder.getProductEquipTv());
            saleApiFirebaseResponseBody.setProduct_equipamiento_internet(tdpOrder.getProductEquipInter());
            saleApiFirebaseResponseBody.setProduct_equipamiento_linea(tdpOrder.getProductEquipLine());
            saleApiFirebaseResponseBody.setProduct_payment_method(tdpOrder.getProductPayMethod());
            saleApiFirebaseResponseBody.setProduct_cash_price(tdpOrder.getProductPayPrice());
            saleApiFirebaseResponseBody.setProduct_return_month(tdpOrder.getProductPayReturnMonth());
            saleApiFirebaseResponseBody.setProduct_return_period(tdpOrder.getProductPayReturnPeriod());
            saleApiFirebaseResponseBody.setProduct_has_equipment(true);

            String sva1 = tdpOrder.getSvaCode1() + "-" + tdpOrder.getSvaNombre1() + "-" + tdpOrder.getSvaCantidad1() + "-" + (tdpOrder.getSvaCantidad1() == null || tdpOrder.getSvaPrecio1() == null ? "" : "" + tdpOrder.getSvaPrecio1().multiply(new BigDecimal(tdpOrder.getSvaCantidad1())));
            String sva2 = tdpOrder.getSvaCode2() + "-" + tdpOrder.getSvaNombre2() + "-" + tdpOrder.getSvaCantidad2() + "-" + (tdpOrder.getSvaCantidad2() == null || tdpOrder.getSvaPrecio2() == null ? "" : "" + tdpOrder.getSvaPrecio2().multiply(new BigDecimal(tdpOrder.getSvaCantidad2())));
            String sva3 = tdpOrder.getSvaCode3() + "-" + tdpOrder.getSvaNombre3() + "-" + tdpOrder.getSvaCantidad3() + "-" + (tdpOrder.getSvaCantidad3() == null || tdpOrder.getSvaPrecio3() == null ? "" : "" + tdpOrder.getSvaPrecio3().multiply(new BigDecimal(tdpOrder.getSvaCantidad3())));
            String sva4 = tdpOrder.getSvaCode4() + "-" + tdpOrder.getSvaNombre4() + "-" + tdpOrder.getSvaCantidad4() + "-" + (tdpOrder.getSvaCantidad4() == null || tdpOrder.getSvaPrecio4() == null ? "" : "" + tdpOrder.getSvaPrecio4().multiply(new BigDecimal(tdpOrder.getSvaCantidad4())));
            String sva5 = tdpOrder.getSvaCode5() + "-" + tdpOrder.getSvaNombre5() + "-" + tdpOrder.getSvaCantidad5() + "-" + (tdpOrder.getSvaCantidad5() == null || tdpOrder.getSvaPrecio5() == null ? "" : "" + tdpOrder.getSvaPrecio5().multiply(new BigDecimal(tdpOrder.getSvaCantidad5())));
            String sva6 = tdpOrder.getSvaCode6() + "-" + tdpOrder.getSvaNombre6() + "-" + tdpOrder.getSvaCantidad6() + "-" + (tdpOrder.getSvaCantidad6() == null || tdpOrder.getSvaPrecio6() == null ? "" : "" + tdpOrder.getSvaPrecio6().multiply(new BigDecimal(tdpOrder.getSvaCantidad6())));
            String sva7 = tdpOrder.getSvaCode7() + "-" + tdpOrder.getSvaNombre7() + "-" + tdpOrder.getSvaCantidad7() + "-" + (tdpOrder.getSvaCantidad7() == null || tdpOrder.getSvaPrecio7() == null ? "" : "" + tdpOrder.getSvaPrecio7().multiply(new BigDecimal(tdpOrder.getSvaCantidad7())));
            String sva8 = tdpOrder.getSvaCode8() + "-" + tdpOrder.getSvaNombre8() + "-" + tdpOrder.getSvaCantidad8() + "-" + (tdpOrder.getSvaCantidad8() == null || tdpOrder.getSvaPrecio8() == null ? "" : "" + tdpOrder.getSvaPrecio8().multiply(new BigDecimal(tdpOrder.getSvaCantidad8())));
            String sva9 = tdpOrder.getSvaCode9() + "-" + tdpOrder.getSvaNombre9() + "-" + tdpOrder.getSvaCantidad9() + "-" + (tdpOrder.getSvaCantidad9() == null || tdpOrder.getSvaPrecio9() == null ? "" : "" + tdpOrder.getSvaPrecio9().multiply(new BigDecimal(tdpOrder.getSvaCantidad9())));
            String sva10 = tdpOrder.getSvaCode10() + "-" + tdpOrder.getSvaNombre10() + "-" + tdpOrder.getSvaCantidad10() + "-" + (tdpOrder.getSvaCantidad10() == null || tdpOrder.getSvaPrecio10() == null ? "" : "" + tdpOrder.getSvaPrecio10().multiply(new BigDecimal(tdpOrder.getSvaCantidad10())));
            String svas = (sva1 == null ? "" : sva1) + ";"
                    + (sva2 == null ? "" : sva2) + ";"
                    + (sva3 == null ? "" : sva3) + ";"
                    + (sva4 == null ? "" : sva4) + ";"
                    + (sva5 == null ? "" : sva5) + ";"
                    + (sva6 == null ? "" : sva6) + ";"
                    + (sva7 == null ? "" : sva7) + ";"
                    + (sva8 == null ? "" : sva8) + ";"
                    + (sva9 == null ? "" : sva9) + ";"
                    + (sva10 == null ? "" : sva10);
            String[] listSva = svas.split(";");

            List<String> svaList = new ArrayList();
            for (String svaObj : listSva) {
                if (!svaObj.equals("") && !svaObj.equalsIgnoreCase("---") && !(svaObj.indexOf("null") >= 0))
                    svaList.add(svaObj);
            }
            saleApiFirebaseResponseBody.setSva(svaList);

            /* Condiciones de Venta */
            saleApiFirebaseResponseBody.setAffiliation_data_protection(tdpOrder.getAffiliationDataProtection().equalsIgnoreCase("Si") ? true : false);
            saleApiFirebaseResponseBody.setAffiliation_electronic_invoice(tdpOrder.getAffiliationElectronicInvoice().equalsIgnoreCase("Si") ? true : false);
            saleApiFirebaseResponseBody.setDisaffiliation_white_pages_guide(tdpOrder.getDisaffiliationWhitePagesGuide().equalsIgnoreCase("Si") ? true : false);
            saleApiFirebaseResponseBody.setShipping_contracts_for_email(tdpOrder.getOrderContrato().equalsIgnoreCase("M") ? true : false);
            saleApiFirebaseResponseBody.setParental_protection(tdpOrder.getAffiliationParentalProtection().equalsIgnoreCase("Si") ? true : false);

            List<ContractResponseParameter> list = new ArrayList<>();
            List<String> contractList = new ArrayList<>();
            List<String> speechList = new ArrayList<>();
            ContractResponseData responseData = new ContractResponseData();

            list = customerDAO.readContractFlujo(saleApiFirebaseResponseBody.getType());
            LogVass.serviceResponseArray(logger, "ContractVenta", "list", list);

            //saleApiFirebaseResponseBody.setProduct_install_cost(lstInformationCatalog.get(0) != null ? new BigDecimal(lstInformationCatalog.get(0)) : null);
            //saleApiFirebaseResponseBody.setProduct_financing_cost(lstInformationCatalog.get(1) != null ? new BigDecimal(lstInformationCatalog.get(1)) : null);
            //saleApiFirebaseResponseBody.setProduct_financing_month(lstInformationCatalog.get(2) != null ? Integer.parseInt(lstInformationCatalog.get(2)) : null);
            //saleApiFirebaseResponseBody.setProm_speed(lstInformationCatalog.get(3) != null ? Integer.parseInt(lstInformationCatalog.get(3)) : null);
            //saleApiFirebaseResponseBody.setPeriod_prom_speed(lstInformationCatalog.get(4) != null ? Integer.parseInt(lstInformationCatalog.get(4)) : null);
            saleApiFirebaseResponseBody.setProduct_equipamiento_linea(saleApiFirebaseResponseBody.getProduct_equipamiento_linea() == null ? null : saleApiFirebaseResponseBody.getProduct_equipamiento_linea().replace("-", ""));
            saleApiFirebaseResponseBody.setProduct_equipamiento_internet(saleApiFirebaseResponseBody.getProduct_equipamiento_internet() == null ? null : saleApiFirebaseResponseBody.getProduct_equipamiento_internet().replace("-", ""));
            saleApiFirebaseResponseBody.setProduct_equipamiento_tv(saleApiFirebaseResponseBody.getProduct_equipamiento_tv() == null ? null : saleApiFirebaseResponseBody.getProduct_equipamiento_tv().replace("-", ""));
            if (saleApiFirebaseResponseBody.getProduct_cash_price() != null && saleApiFirebaseResponseBody.getProduct_return_month() != null) {
                if (BigDecimal.valueOf(saleApiFirebaseResponseBody.getProduct_return_month()).compareTo(BigDecimal.valueOf(0)) > 0) {
                    saleApiFirebaseResponseBody.setPrice_flat(saleApiFirebaseResponseBody.getProduct_cash_price().divide(BigDecimal.valueOf(saleApiFirebaseResponseBody.getProduct_return_month())));
                } else {
                    saleApiFirebaseResponseBody.setPrice_flat(saleApiFirebaseResponseBody.getProduct_cash_price());
                }
            }

            Field[] objFields = SaleApiFirebaseResponseBody.class.getDeclaredFields();
            //LogVass.serviceResponseObject(logger, "ReadContract APP", "objFields", objFields);

            StringBuilder svaCode = new StringBuilder();
            if (saleApiFirebaseResponseBody.getSva() != null) {
                for (int i = 0; i < saleApiFirebaseResponseBody.getSva().size(); i++) {
                    String[] sva = saleApiFirebaseResponseBody.getSva().get(i).split("-");
                    if (Double.parseDouble(sva[3]) > 0.0) {
                        if (svaCode.toString().trim().length() > 0) {
                            svaCode.append(",");
                        }
                        svaCode.append("'");
                        svaCode.append(sva[1] + sva[3]);
                        svaCode.append("'");
                    }
                }
            }
            //logger.info("ReadContract => svaCode: " + svaCode);

            List<String[]> svaContract = new ArrayList<>();
            List<String[]> svaSpeech = new ArrayList<>();
            if (CustomerConstants.PARAMETRO_FLUJO_ALTA.equalsIgnoreCase(saleApiFirebaseResponseBody.getType())) {
                svaContract = customerDAO.getContractSVAMotorizado(CustomerConstants.PARAMETRO_FLUJO_ALTA, CustomerConstants.PARAMETRO_CONTRACT, svaCode.toString());
                //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaContract", svaContract);
                svaSpeech = customerDAO.getContractSVAMotorizado(CustomerConstants.PARAMETRO_FLUJO_ALTA, CustomerConstants.PARAMETRO_SPEECH, svaCode.toString());
                //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaSpeech", svaSpeech);
            } else if (CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(saleApiFirebaseResponseBody.getType())) {
                svaContract = customerDAO.getContractSVAMotorizado(CustomerConstants.PARAMETRO_FLUJO_MIGRACION, CustomerConstants.PARAMETRO_CONTRACT, svaCode.toString());
                //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaContract", svaContract);
                svaSpeech = customerDAO.getContractSVAMotorizado(CustomerConstants.PARAMETRO_FLUJO_MIGRACION, CustomerConstants.PARAMETRO_SPEECH, svaCode.toString());
                //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaSpeech", svaSpeech);
            } else if (CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(saleApiFirebaseResponseBody.getType())) {
                svaContract = customerDAO.getContractSVAMotorizado(CustomerConstants.PARAMETRO_FLUJO_SVA, CustomerConstants.PARAMETRO_CONTRACT, svaCode.toString());
                //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaContract", svaContract);
                svaSpeech = customerDAO.getContractSVAMotorizado(CustomerConstants.PARAMETRO_FLUJO_SVA, CustomerConstants.PARAMETRO_SPEECH, svaCode.toString());
                //LogVass.serviceResponseArray(logger, "ReadContract APP", "listSvaSpeech", svaSpeech);
            }

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i) != null) {
                    //JSONObject ooobj = new JSONObject(list.get(i));
                    //logger.info("ReadContract APP => ooobj: " + ooobj);
                    if (list.get(i).getCategory() != null) {
                        for (int j = 0; j < objFields.length; j++) {
                            objFields[j].setAccessible(true);
                            if (objFields[j].getName().equalsIgnoreCase(list.get(i).getCategory())) {
                                if (list.get(i).getDomain() != null) {
                                    if (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain())) {
                                        if (list.get(i).getElement() != null) {
                                            String field = String.valueOf(objFields[j].get(saleApiFirebaseResponseBody));
                                            if (field != null) {
                                                if (field.equalsIgnoreCase(list.get(i).getElement())) {
                                                    contractList.add(list.get(i).getStrValue());
                                                    //logger.info("ReadContract APP => contractList => list.get(i).getCategory(): " + list.get(i).getCategory());
                                                    //logger.info("ReadContract APP => contractList => list.get(i).getStrValue(): " + list.get(i).getStrValue());
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (objFields[j].getName().equalsIgnoreCase(list.get(i).getCategory())) {
                                if (list.get(i).getDomain() != null) {
                                    if (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain())) {
                                        if (list.get(i).getElement() != null) {
                                            String field = String.valueOf(objFields[j].get(saleApiFirebaseResponseBody));
                                            if (field != null) {
                                                if (field.equalsIgnoreCase(list.get(i).getElement())) {
                                                    speechList.add(list.get(i).getStrValue());
                                                    //logger.info("ReadContract APP => speechList => list.get(i).getCategory(): " + list.get(i).getCategory());
                                                    //logger.info("ReadContract APP => speechList => list.get(i).getStrValue(): " + list.get(i).getStrValue());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (!CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                list.get(i).getElement() == null &&
                                (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                            contractList.add(list.get(i).getStrValue());
                        }

                        if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {
                            if (list.get(i).getElement() != null) {
                                String[] elementos = list.get(i).getElement().split(",");
                                for (int j = 0; j < elementos.length; j++) {
                                    for (int k = 0; k < svaContract.size(); k++) {
                                        if (svaContract.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                                && svaContract.get(k)[2] != null
                                                && svaContract.get(k)[0].equalsIgnoreCase(elementos[j])
                                                && svaContract.get(k)[2].equalsIgnoreCase(list.get(i).getCampo())
                                                && (list.get(i).getStrValue()).contains(svaContract.get(k)[2])) {

                                            contractList.add(svaContract.get(k)[1]);
                                        } else if (!svaContract.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                                && svaContract.get(k)[0].equalsIgnoreCase(elementos[j])) {

                                            contractList.add(svaContract.get(k)[1]);
                                        }
                                    }
                                }
                            }
                        }

                        if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_ALTA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                list.get(i).getElement() != null && list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getProduct_has_discount())) &&
                                CustomerConstants.PARAMETRO_FLUJO_ALTA.equals(saleApiFirebaseResponseBody.getType()) &&
                                (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                            contractList.add(list.get(i).getStrValue());
                        }

                        if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_MIGRACION.equalsIgnoreCase(list.get(i).getCategory()) &&
                                list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getProduct_has_discount())) &&
                                CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(saleApiFirebaseResponseBody.getType()) &&
                                CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain())) {

                            contractList.add(list.get(i).getStrValue());
                        }

                        if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_LEGACY.equalsIgnoreCase(list.get(i).getCategory()) &&
                                list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getLegacy_code())) &&
                                CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(saleApiFirebaseResponseBody.getType()) &&
                                CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain())) {

                            contractList.add(list.get(i).getStrValue());
                        }

                        /* Velocidad Promociona Speed Internet - No Aplica para SVA */
                        if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(saleApiFirebaseResponseBody.getType())
                                && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SPEED.equalsIgnoreCase(list.get(i).getCategory())
                                && list.get(i).getElement() != null
                                && list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getProm_speed() > 0))
                                && (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                            contractList.add(list.get(i).getStrValue());
                        }
                        /* Velocidad Promociona Speed Internet */

                        /* Equipamientos - No Aplica para SVA */
                        if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(saleApiFirebaseResponseBody.getType())
                                && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_EQUIPMENTS.equalsIgnoreCase(list.get(i).getCategory())
                                && list.get(i).getElement() != null
                                && (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                            contractList.add(list.get(i).getStrValue());
                        }
                        /* Equipamientos */

                        /* Costo de Instalación - No Aplica para SVA */
                        if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(saleApiFirebaseResponseBody.getType())
                                && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_PRODUCTO.equalsIgnoreCase(list.get(i).getCategory())
                                && list.get(i).getElement() != null
                                && list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getProduct_financing_month() != null ? saleApiFirebaseResponseBody.getProduct_financing_month() > 0 : false))
                                && (CustomerConstants.PARAMETRO_CONTRACT.equalsIgnoreCase(list.get(i).getDomain()))) {

                            contractList.add(list.get(i).getStrValue());
                        }
                        /* Costo de Instalación */

                        if (!CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                list.get(i).getElement() == null &&
                                (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                            speechList.add(list.get(i).getStrValue());
                        }

                        if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                            String[] elementos = list.get(i).getElement().split(",");
                            for (int j = 0; j < elementos.length; j++) {
                                for (int k = 0; k < svaSpeech.size(); k++) {
                                    if (svaSpeech.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                            && svaSpeech.get(k)[2] != null
                                            && svaSpeech.get(k)[0].equalsIgnoreCase(elementos[j])
                                            && svaSpeech.get(k)[2].equalsIgnoreCase(list.get(i).getCampo())
                                            && (list.get(i).getStrValue()).contains(svaSpeech.get(k)[2])) {

                                        speechList.add(svaSpeech.get(k)[1]);
                                    } else if (!svaSpeech.get(k)[0].equalsIgnoreCase(CustomerConstants.PARAMETRO_COLUMNA_BTV)
                                            && svaSpeech.get(k)[0].equalsIgnoreCase(elementos[j])) {

                                        speechList.add(svaSpeech.get(k)[1]);
                                    }
                                }
                            }
                        }

                        if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_ALTA.equalsIgnoreCase(list.get(i).getCategory()) &&
                                list.get(i).getElement() != null && list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getProduct_has_discount())) &&
                                CustomerConstants.PARAMETRO_FLUJO_ALTA.equals(saleApiFirebaseResponseBody.getType()) &&
                                (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                            speechList.add(list.get(i).getStrValue());
                        }

                        if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_MIGRACION.equalsIgnoreCase(list.get(i).getCategory()) &&
                                list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getProduct_has_discount())) &&
                                CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(saleApiFirebaseResponseBody.getType()) &&
                                CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain())) {

                            speechList.add(list.get(i).getStrValue());
                        }
                        if (CustomerConstants.PARAMETRO_TIPO_CATEGORIA_LEGACY.equalsIgnoreCase(list.get(i).getCategory()) &&
                                list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getLegacy_code())) &&
                                CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(saleApiFirebaseResponseBody.getType()) &&
                                CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain())) {

                            speechList.add(list.get(i).getStrValue());
                        }

                        /* Velocidad Promociona Speed Internet - No Aplica para SVA */
                        if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(saleApiFirebaseResponseBody.getType())
                                && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SPEED.equalsIgnoreCase(list.get(i).getCategory())
                                && list.get(i).getElement() != null
                                && list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getProm_speed() > 0))
                                && (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                            speechList.add(list.get(i).getStrValue());
                        }
                        /* Velocidad Promociona Speed Internet */

                        /* Equipamientos - No Aplica para SVA */
                        if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(saleApiFirebaseResponseBody.getType())
                                && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_EQUIPMENTS.equalsIgnoreCase(list.get(i).getCategory())
                                && list.get(i).getElement() != null
                                && (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                            speechList.add(list.get(i).getStrValue());
                        }
                        /* Equipamientos */

                        /* Costo de Instalación - No Aplica para SVA */
                        if (!CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(saleApiFirebaseResponseBody.getType())
                                && CustomerConstants.PARAMETRO_TIPO_CATEGORIA_PRODUCTO.equalsIgnoreCase(list.get(i).getCategory())
                                && list.get(i).getElement() != null
                                && list.get(i).getElement().equals(String.valueOf(saleApiFirebaseResponseBody.getProduct_financing_month() != null ? saleApiFirebaseResponseBody.getProduct_financing_month() > 0 : false))
                                && (CustomerConstants.PARAMETRO_SPEECH.equalsIgnoreCase(list.get(i).getDomain()))) {

                            speechList.add(list.get(i).getStrValue());
                        }
                        /* Costo de Instalación */
                    }
                }
            }

            //LogVass.serviceResponseArray(logger, "ReadContract APP", "contractList", contractList);
            //LogVass.serviceResponseArray(logger, "ReadContract APP", "speechList", speechList);

            logger.info("Contract APP: " + contractList.size());
            logger.info("Speech APP: " + speechList.size());
            List<Parameter> valuesParameterList = customerDAO.getParameter("VALUES");
            //LogVass.serviceResponseArray(logger, "ReadContract APP", "valuesParameterList", valuesParameterList);

            for (int i = 0; i < contractList.size(); i++) {
                for (int j = 0; j < objFields.length; j++) {
                    objFields[j].setAccessible(true);
                    if (Arrays
                            .asList(CustomerConstants.PARAMETRO_PRODUCTO_1, CustomerConstants.PARAMETRO_PRODUCTO_2,
                                    CustomerConstants.PARAMETRO_PRODUCTO_3, CustomerConstants.PARAMETRO_PRODUCTO_4,
                                    CustomerConstants.PARAMETRO_PRODUCTO_5)
                            .contains(objFields[j].getName())) {
                        contractList.set(i,
                                contractList.get(i).replace(
                                        getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                        String.valueOf(
                                                new BigDecimal(String.valueOf(objFields[j].get(saleApiFirebaseResponseBody)))
                                                        .setScale(2, RoundingMode.HALF_EVEN))));
                    } else {
                        if (Arrays
                                .asList(CustomerConstants.PARAMETRO_PRODUCTO_14)
                                .contains(objFields[j].getName())) {
                            if (contractList.get(i) != null && contractList.get(i).indexOf(getDescriptionByElement(objFields[j].getName(), valuesParameterList)) > 0) {
                                String equipamientos = "";
                                int cont1 = 0;
                                for (int k1 = 0; k1 < objFields.length; k1++) {
                                    if (Arrays
                                            .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                    CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                    CustomerConstants.PARAMETRO_PRODUCTO_16)
                                            .contains(objFields[k1].getName())) {
                                        if (objFields[k1].get(saleApiFirebaseResponseBody) != null && !String.valueOf(objFields[k1].get(saleApiFirebaseResponseBody)).equals("")) {
                                            cont1++;
                                        }
                                    }
                                }
                                int cont2 = 0;
                                for (int k2 = 0; k2 < objFields.length; k2++) {
                                    if (Arrays
                                            .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                    CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                    CustomerConstants.PARAMETRO_PRODUCTO_16)
                                            .contains(objFields[k2].getName())) {
                                        if (objFields[k2].get(saleApiFirebaseResponseBody) != null && !String.valueOf(objFields[k2].get(saleApiFirebaseResponseBody)).equals("")) {
                                            if (cont1 == 1) {
                                                equipamientos = String.valueOf(objFields[k2].get(saleApiFirebaseResponseBody));
                                            } else {
                                                if (cont1 == 2) {
                                                    equipamientos = equipamientos + (cont2 == 0 ? "" : " y ") + String.valueOf(objFields[k2].get(saleApiFirebaseResponseBody));
                                                    cont2++;
                                                } else {
                                                    if (cont1 == 3) {
                                                        equipamientos = equipamientos + (cont2 == 0 ? "" : (cont2 == 1 ? ", " : " y ")) + String.valueOf(objFields[k2].get(saleApiFirebaseResponseBody));
                                                        cont2++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                contractList.set(i,
                                        contractList.get(i).replace(
                                                getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                equipamientos));

                            }
                        } else {
                            contractList.set(i,
                                    contractList.get(i).replace(
                                            getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                            String.valueOf(objFields[j].get(saleApiFirebaseResponseBody))));
                        }
                        //logger.info("ReadContract APP : contractList.get(" + i + "):" + getDescriptionByElement(objFields[j].getName(), valuesParameterList) + "|" + objFields[j].get(saleApiFirebaseResponseBody));
                    }
                }
                contractList.set(i,
                        contractList.get(i)
                                .replace(CustomerConstants.PARAMETRO_ANIO,
                                        ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                .format(DateTimeFormatter.ofPattern("yyyy")))
                                .replace(CustomerConstants.PARAMETRO_DIA,
                                        ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                .format(DateTimeFormatter.ofPattern("d")))
                                .replace(CustomerConstants.PARAMETRO_MES,
                                        ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                .format(DateTimeFormatter.ofPattern("MMMM")
                                                        .withLocale(new Locale("es", "PE")))));
            }
            for (int i = 0; i < speechList.size(); i++) {
                for (int j = 0; j < objFields.length; j++) {
                    objFields[j].setAccessible(true);
                    if (Arrays.asList(CustomerConstants.PARAMETRO_PRODUCTO_6, CustomerConstants.PARAMETRO_PRODUCTO_7,
                            CustomerConstants.PARAMETRO_PRODUCTO_8, CustomerConstants.PARAMETRO_PRODUCTO_9,
                            CustomerConstants.PARAMETRO_PRODUCTO_10)
                            .contains(objFields[j].getName())) {
                        speechList.set(i,
                                speechList.get(i).replace(
                                        getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                        String.valueOf(objFields[j].get(saleApiFirebaseResponseBody)).replace("", " ").trim()));
                    } else if (Arrays
                            .asList(CustomerConstants.PARAMETRO_PRODUCTO_1, CustomerConstants.PARAMETRO_PRODUCTO_2,
                                    CustomerConstants.PARAMETRO_PRODUCTO_3, CustomerConstants.PARAMETRO_PRODUCTO_4,
                                    CustomerConstants.PARAMETRO_PRODUCTO_5)
                            .contains(objFields[j].getName())) {
                        speechList.set(i,
                                speechList.get(i).replace(
                                        getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                        decimalSpeechParser(String.valueOf(objFields[j].get(saleApiFirebaseResponseBody)))));
                    } else if (CustomerConstants.PARAMETRO_PRODUCTO_11.equals(objFields[j].getName())
                            || CustomerConstants.PARAMETRO_PRODUCTO_12.equals(objFields[j].getName())
                            || CustomerConstants.PARAMETRO_PRODUCTO_13.equals(objFields[j].getName())) {
                        speechList.set(i,
                                speechList.get(i).replace(
                                        getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                        String.valueOf(objFields[j].get(saleApiFirebaseResponseBody)).toLowerCase()));
                    } else {
                        if (Arrays
                                .asList(CustomerConstants.PARAMETRO_PRODUCTO_14)
                                .contains(objFields[j].getName())) {
                            if (speechList.get(i) != null && speechList.get(i).indexOf(getDescriptionByElement(objFields[j].getName(), valuesParameterList)) > 0) {
                                String equipamientos = "";
                                int cont1 = 0;
                                for (int k1 = 0; k1 < objFields.length; k1++) {
                                    if (Arrays
                                            .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                    CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                    CustomerConstants.PARAMETRO_PRODUCTO_16)
                                            .contains(objFields[k1].getName())) {
                                        if (objFields[k1].get(saleApiFirebaseResponseBody) != null && !String.valueOf(objFields[k1].get(saleApiFirebaseResponseBody)).equals("")) {
                                            cont1++;
                                        }
                                    }
                                }
                                int cont2 = 0;
                                for (int k2 = 0; k2 < objFields.length; k2++) {
                                    if (Arrays
                                            .asList(CustomerConstants.PARAMETRO_PRODUCTO_14,
                                                    CustomerConstants.PARAMETRO_PRODUCTO_15,
                                                    CustomerConstants.PARAMETRO_PRODUCTO_16)
                                            .contains(objFields[k2].getName())) {
                                        if (objFields[k2].get(saleApiFirebaseResponseBody) != null && !String.valueOf(objFields[k2].get(saleApiFirebaseResponseBody)).equals("")) {
                                            if (cont1 == 1) {
                                                equipamientos = String.valueOf(objFields[k2].get(saleApiFirebaseResponseBody));
                                            } else {
                                                if (cont1 == 2) {
                                                    equipamientos = equipamientos + (cont2 == 0 ? "" : " y ") + String.valueOf(objFields[k2].get(saleApiFirebaseResponseBody));
                                                    cont2++;
                                                } else {
                                                    if (cont1 == 3) {
                                                        equipamientos = equipamientos + (cont2 == 0 ? "" : (cont2 == 1 ? ", " : " y ")) + String.valueOf(objFields[k2].get(saleApiFirebaseResponseBody));
                                                        cont2++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                speechList.set(i,
                                        speechList.get(i).replace(
                                                getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                                equipamientos));

                            }

                        } else {
                            speechList.set(i,
                                    speechList.get(i).replace(
                                            getDescriptionByElement(objFields[j].getName(), valuesParameterList),
                                            String.valueOf(objFields[j].get(saleApiFirebaseResponseBody))));
                        }
                    }
                    //logger.info("ReadContract APP : speechList.get(" + i + "):" + getDescriptionByElement(objFields[j].getName(), valuesParameterList) + "|" + objFields[j].get(saleApiFirebaseResponseBody));
                }
                speechList.set(i,
                        speechList.get(i)
                                .replace(CustomerConstants.PARAMETRO_ANIO,
                                        ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                .format(DateTimeFormatter.ofPattern("yyyy")))
                                .replace(CustomerConstants.PARAMETRO_DIA,
                                        ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                .format(DateTimeFormatter.ofPattern("d")))
                                .replace(CustomerConstants.PARAMETRO_MES,
                                        ZonedDateTime.now(ZoneId.of("America/Indiana/Indianapolis"))
                                                .format(DateTimeFormatter.ofPattern("MMMM")
                                                        .withLocale(new Locale("es", "PE")))));
            }

            for (int c = 0; c < contractList.size(); c++) {
                if (contractList.get(c).toLowerCase().indexOf("no aplica") > 0 || contractList.get(c).toLowerCase().indexOf("null") > 0) {
                    //logger.info("APP remove contractlist= "+contractList.get(c));
                    contractList.remove(c);
                }
            }
            for (int c = 0; c < speechList.size(); c++) {
                if (speechList.get(c).toLowerCase().indexOf("no aplica") > 0 || speechList.get(c).toLowerCase().indexOf("null") > 0) {
                    //logger.info("APP remove speechList= "+speechList.get(c));
                    speechList.remove(c);
                }
            }

            responseData.setContract(contractList);
            responseData.setSpeech(speechList);
            response.setResponseCode("0");
            response.setResponseData(responseData);
            logger.info("Fin Contract APP Service");

        } catch (Exception e) {
            response.setResponseCode("00");
            response.setResponseMessage("No se logró cargar el contrato de la venta.");
            logger.error("Error ContractVenta Controller", e);
        }
        logger.info("Fin ContractVenta Service.");
        return response;
    }

    public Response<SaleApiFirebaseResponseBody> cancelVenta(MotorizadoRequestData request) {
        logger.info("Inicio CancelVenta Service.");
        Response<SaleApiFirebaseResponseBody> response = new Response<>();
        response.setResponseCode("01");

        try {
            TdpMotorizadoBase tdpMotorizadoBase = tdpMotorizadoBaseRepository.findOneByCodMotorizadoAndCodigoPedidoAndNumDocCliente(request.getCodmotorizado(), request.getCodigopedido(), request.getDocumentocliente());
            if (tdpMotorizadoBase == null) {
                response.setResponseCode("00");
                response.setResponseMessage("Está venta ha sido reasignada, por favor actualice su bandeja.");
                logger.info("Fin CancelVenta Service.");
                return response;
            }
            LogVass.serviceResponseObject(logger, "CancelVenta", "tdpMotorizadoBase", tdpMotorizadoBase);

            tdpMotorizadoBase.setEstado(MotorizadoConstants.MOTORIZADO_ESTADO_RECHAZADO);
            tdpMotorizadoBase.setMotivoRechazo(request.getMotivorechazo());
            DateTime dt = new DateTime(new Date());
            DateTimeZone dtZone = DateTimeZone.forID("America/Lima");
            DateTime dtus = dt.withZone(dtZone);
            tdpMotorizadoBase.setUpdateTime(dtus.toDate());
            tdpMotorizadoBaseRepository.save(tdpMotorizadoBase);

            response.setResponseMessage("PROCEDE");
        } catch (Exception e) {
            response.setResponseCode("00");
            response.setResponseMessage("No se logró cancelar venta.");
            logger.error("Error CancelVenta Controller", e);
        }
        logger.info("Fin CancelVenta Service.");
        return response;
    }

    @Transactional
    public Response<SaleApiFirebaseResponseBody> closeVenta(MotorizadoRequestData request) {
        logger.info("Inicio CloseVenta Service.");
        Response<SaleApiFirebaseResponseBody> response = new Response<>();
        response.setResponseCode("01");

        try {
            TdpMotorizadoBase tdpMotorizadoBase = tdpMotorizadoBaseRepository.findOneByCodMotorizadoAndCodigoPedidoAndNumDocCliente(request.getCodmotorizado(), request.getCodigopedido(), request.getDocumentocliente());
            if (tdpMotorizadoBase == null) {
                response.setResponseCode("02");
                response.setResponseMessage("Está venta ha sido reasignada, por favor actualice su bandeja.");
                logger.info("Fin CloseVenta Service.");
                return response;
            }
            LogVass.serviceResponseObject(logger, "CancelVenta", "tdpMotorizadoBase", tdpMotorizadoBase);

            tdpMotorizadoBase.setEstado(MotorizadoConstants.MOTORIZADO_ESTADO_CONTRATADO);
            tdpMotorizadoBase.setMotivoRechazo(null);
            tdpMotorizadoBase.setBiometria(request.getBiometria());
            DateTime dt = new DateTime(new Date());
            DateTimeZone dtZone = DateTimeZone.forID("America/Lima");
            DateTime dtus = dt.withZone(dtZone);
            tdpMotorizadoBase.setUpdateTime(dtus.toDate());
            tdpMotorizadoBaseRepository.save(tdpMotorizadoBase);

            List<TdpVisor> tdpVisorList = tdpVisorRepository.findAllByCodigoPedidoAndDni(request.getCodigopedido(), request.getDocumentocliente());
            if (tdpVisorList == null) {
                response.setResponseCode("00");
                response.setResponseMessage("Está venta no existe, por favor actualice su bandeja.");
                logger.info("Fin ContractVenta Service.");
                return response;
            }
            LogVass.serviceResponseArray(logger, "ContractVenta", "tdpVisorList", tdpVisorList);
            TdpVisor tdpVisor = tdpVisorList.get(0);
            LogVass.serviceResponseObject(logger, "ContractVenta", "tdpVisor", tdpVisor);

            orderRepository.updateStatusAudioOrder("0", tdpVisor.getIdVisor());

            AzureRequest azure = azureRequestRepository.findOneByOrderId(tdpVisor.getIdVisor() + "_motorizado");
            if (azure == null) {
                azure = new AzureRequest();
            }
            azure.setOrderId(tdpVisor.getIdVisor() + "_motorizado");
            azure.setOrderStatus("INGRESADO");
            azure.setRegistrationDate(dtus.toDate());
            azure.setLastUpdate(dtus.toDate());
            azure.setStatus("PE");
            azureRequestRepository.save(azure);

            response.setResponseMessage("Se ha realizado el cierre de venta exitosamente.");
        } catch (Exception e) {
            response.setResponseCode("02");
            response.setResponseMessage("No se logró cerrar la venta. Por favor volver intentar en unos minutos.");
            logger.error("Error CloseVenta Controller", e);
        }
        logger.info("Fin CloseVenta Service.");
        return response;
    }

    private String getDescriptionByElement(String element, List<Parameter> cmssParameterList) {
        String description = "SIN VALOR";

        for (Parameter parameter : cmssParameterList) {
            if (element.equals(parameter.getElement())) {
                description = parameter.getStrValue();
            }
        }

        return description;
    }

    private String decimalSpeechParser(String decimal) {

        int integerPart = new BigDecimal(decimal).setScale(2, RoundingMode.HALF_EVEN).setScale(0, RoundingMode.FLOOR)
                .intValue();
        int fractionPart = new BigDecimal(decimal).setScale(2, RoundingMode.HALF_EVEN)
                .subtract(new BigDecimal(integerPart)).movePointRight(2).intValue();

        String decimalString;
        String stringSolesPlus = " soles con ";
        String stringSolPlus = " sol con ";
        String stringCentimos = " c\u00e9ntimos";
        String stringCentimo = " c\u00e9ntimo";
        if (integerPart == 1 && fractionPart == 1) {
            decimalString = integerPart + stringSolPlus + fractionPart + stringCentimo;
        } else if (integerPart == 1 && fractionPart != 1) {
            decimalString = integerPart + stringSolPlus + fractionPart + stringCentimos;
        } else if (integerPart != 1 && fractionPart == 1) {
            decimalString = integerPart + stringSolesPlus + fractionPart + stringCentimo;
        } else {
            decimalString = integerPart + stringSolesPlus + fractionPart + stringCentimos;
        }

        return decimalString;
    }
}
