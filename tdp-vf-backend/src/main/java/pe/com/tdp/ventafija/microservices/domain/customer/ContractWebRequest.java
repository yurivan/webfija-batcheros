package pe.com.tdp.ventafija.microservices.domain.customer;

import java.util.List;

public class ContractWebRequest {

	private String status;
	private String equipmentName;
	private ContractWebRequestUser user;
	private ContractWebRequestCustomer customer;
	private ContractWebRequestProduct product;
	private ContractWebRequestOffering selectedOffering;
	private List<ContractWebRequestSva> sva;
	private String type;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ContractWebRequestUser getUser() {
		return user;
	}

	public void setUser(ContractWebRequestUser user) {
		this.user = user;
	}

	public ContractWebRequestCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(ContractWebRequestCustomer customer) {
		this.customer = customer;
	}

	public ContractWebRequestProduct getProduct() {
		return product;
	}

	public void setProduct(ContractWebRequestProduct product) {
		this.product = product;
	}

	public ContractWebRequestOffering getSelectedOffering() {
		return selectedOffering;
	}

	public void setSelectedOffering(ContractWebRequestOffering selectedOffering) {
		this.selectedOffering = selectedOffering;
	}

	public String getEquipmentName() {
		return equipmentName;
	}

	public void setEquipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}

	public List<ContractWebRequestSva> getSva() {
		return sva;
	}

	public void setSva(List<ContractWebRequestSva> sva) {
		this.sva = sva;
	}

}