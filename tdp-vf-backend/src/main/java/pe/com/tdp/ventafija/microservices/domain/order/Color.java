package pe.com.tdp.ventafija.microservices.domain.order;

public class Color {
    private String element;
    private String value;

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
