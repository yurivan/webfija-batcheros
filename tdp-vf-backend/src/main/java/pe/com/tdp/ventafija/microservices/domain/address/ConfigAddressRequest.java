package pe.com.tdp.ventafija.microservices.domain.address;

import java.util.Date;

public class ConfigAddressRequest {

    private String orderId;
    private String address_principal ;
    private String address_calle_atis ;
    private String address_calle_nombre ;
    private String address_calle_numero ;
    private String address_cchh_tipo ;
    private String address_cchh_nombre ;
    private String address_cchh_comp_tip_1 ;
    private String address_cchh_comp_nom_1 ;
    private String address_cchh_comp_tip_2 ;
    private String address_cchh_comp_nom_2 ;
    private String address_cchh_comp_tip_3 ;
    private String address_cchh_comp_nom_3 ;
    private String address_cchh_comp_tip_4 ;
    private String address_cchh_comp_nom_4 ;
    private String address_cchh_comp_tip_5 ;
    private String address_cchh_comp_nom_5 ;
    private String address_via_comp_tip_1 ;
    private String address_via_comp_nom_1 ;
    private String address_via_comp_tip_2 ;
    private String address_via_comp_nom_2 ;
    private String address_via_comp_tip_3 ;
    private String address_via_comp_nom_3 ;
    private String address_via_comp_tip_4 ;
    private String address_via_comp_nom_4 ;
    private String address_via_comp_tip_5;
    private String address_via_comp_nom_5;
    private String address_referencia;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAddress_principal() {
        return address_principal;
    }

    public void setAddress_principal(String address_principal) {
        this.address_principal = address_principal;
    }

    public String getAddress_calle_atis() {
        return address_calle_atis;
    }

    public void setAddress_calle_atis(String address_calle_atis) {
        this.address_calle_atis = address_calle_atis;
    }

    public String getAddress_calle_nombre() {
        return address_calle_nombre;
    }

    public void setAddress_calle_nombre(String address_calle_nombre) {
        this.address_calle_nombre = address_calle_nombre;
    }

    public String getAddress_calle_numero() {
        return address_calle_numero;
    }

    public void setAddress_calle_numero(String address_calle_numero) {
        this.address_calle_numero = address_calle_numero;
    }

    public String getAddress_cchh_tipo() {
        return address_cchh_tipo;
    }

    public void setAddress_cchh_tipo(String address_cchh_tipo) {
        this.address_cchh_tipo = address_cchh_tipo;
    }

    public String getAddress_cchh_nombre() {
        return address_cchh_nombre;
    }

    public void setAddress_cchh_nombre(String address_cchh_nombre) {
        this.address_cchh_nombre = address_cchh_nombre;
    }

    public String getAddress_cchh_comp_tip_1() {
        return address_cchh_comp_tip_1;
    }

    public void setAddress_cchh_comp_tip_1(String address_cchh_comp_tip_1) {
        this.address_cchh_comp_tip_1 = address_cchh_comp_tip_1;
    }

    public String getAddress_cchh_comp_nom_1() {
        return address_cchh_comp_nom_1;
    }

    public void setAddress_cchh_comp_nom_1(String address_cchh_comp_nom_1) {
        this.address_cchh_comp_nom_1 = address_cchh_comp_nom_1;
    }

    public String getAddress_cchh_comp_tip_2() {
        return address_cchh_comp_tip_2;
    }

    public void setAddress_cchh_comp_tip_2(String address_cchh_comp_tip_2) {
        this.address_cchh_comp_tip_2 = address_cchh_comp_tip_2;
    }

    public String getAddress_cchh_comp_nom_2() {
        return address_cchh_comp_nom_2;
    }

    public void setAddress_cchh_comp_nom_2(String address_cchh_comp_nom_2) {
        this.address_cchh_comp_nom_2 = address_cchh_comp_nom_2;
    }

    public String getAddress_cchh_comp_tip_3() {
        return address_cchh_comp_tip_3;
    }

    public void setAddress_cchh_comp_tip_3(String address_cchh_comp_tip_3) {
        this.address_cchh_comp_tip_3 = address_cchh_comp_tip_3;
    }

    public String getAddress_cchh_comp_nom_3() {
        return address_cchh_comp_nom_3;
    }

    public void setAddress_cchh_comp_nom_3(String address_cchh_comp_nom_3) {
        this.address_cchh_comp_nom_3 = address_cchh_comp_nom_3;
    }

    public String getAddress_cchh_comp_tip_4() {
        return address_cchh_comp_tip_4;
    }

    public void setAddress_cchh_comp_tip_4(String address_cchh_comp_tip_4) {
        this.address_cchh_comp_tip_4 = address_cchh_comp_tip_4;
    }

    public String getAddress_cchh_comp_nom_4() {
        return address_cchh_comp_nom_4;
    }

    public void setAddress_cchh_comp_nom_4(String address_cchh_comp_nom_4) {
        this.address_cchh_comp_nom_4 = address_cchh_comp_nom_4;
    }

    public String getAddress_cchh_comp_tip_5() {
        return address_cchh_comp_tip_5;
    }

    public void setAddress_cchh_comp_tip_5(String address_cchh_comp_tip_5) {
        this.address_cchh_comp_tip_5 = address_cchh_comp_tip_5;
    }

    public String getAddress_cchh_comp_nom_5() {
        return address_cchh_comp_nom_5;
    }

    public void setAddress_cchh_comp_nom_5(String address_cchh_comp_nom_5) {
        this.address_cchh_comp_nom_5 = address_cchh_comp_nom_5;
    }

    public String getAddress_via_comp_tip_1() {
        return address_via_comp_tip_1;
    }

    public void setAddress_via_comp_tip_1(String address_via_comp_tip_1) {
        this.address_via_comp_tip_1 = address_via_comp_tip_1;
    }

    public String getAddress_via_comp_nom_1() {
        return address_via_comp_nom_1;
    }

    public void setAddress_via_comp_nom_1(String address_via_comp_nom_1) {
        this.address_via_comp_nom_1 = address_via_comp_nom_1;
    }

    public String getAddress_via_comp_tip_2() {
        return address_via_comp_tip_2;
    }

    public void setAddress_via_comp_tip_2(String address_via_comp_tip_2) {
        this.address_via_comp_tip_2 = address_via_comp_tip_2;
    }

    public String getAddress_via_comp_nom_2() {
        return address_via_comp_nom_2;
    }

    public void setAddress_via_comp_nom_2(String address_via_comp_nom_2) {
        this.address_via_comp_nom_2 = address_via_comp_nom_2;
    }

    public String getAddress_via_comp_tip_3() {
        return address_via_comp_tip_3;
    }

    public void setAddress_via_comp_tip_3(String address_via_comp_tip_3) {
        this.address_via_comp_tip_3 = address_via_comp_tip_3;
    }

    public String getAddress_via_comp_nom_3() {
        return address_via_comp_nom_3;
    }

    public void setAddress_via_comp_nom_3(String address_via_comp_nom_3) {
        this.address_via_comp_nom_3 = address_via_comp_nom_3;
    }

    public String getAddress_via_comp_tip_4() {
        return address_via_comp_tip_4;
    }

    public void setAddress_via_comp_tip_4(String address_via_comp_tip_4) {
        this.address_via_comp_tip_4 = address_via_comp_tip_4;
    }

    public String getAddress_via_comp_nom_4() {
        return address_via_comp_nom_4;
    }

    public void setAddress_via_comp_nom_4(String address_via_comp_nom_4) {
        this.address_via_comp_nom_4 = address_via_comp_nom_4;
    }

    public String getAddress_via_comp_tip_5() {
        return address_via_comp_tip_5;
    }

    public void setAddress_via_comp_tip_5(String address_via_comp_tip_5) {
        this.address_via_comp_tip_5 = address_via_comp_tip_5;
    }

    public String getAddress_via_comp_nom_5() {
        return address_via_comp_nom_5;
    }

    public void setAddress_via_comp_nom_5(String address_via_comp_nom_5) {
        this.address_via_comp_nom_5 = address_via_comp_nom_5;
    }

    public String getAddress_referencia() {
        return address_referencia;
    }

    public void setAddress_referencia(String address_referencia) {
        this.address_referencia = address_referencia;
    }
}
