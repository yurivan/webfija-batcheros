package pe.com.tdp.ventafija.microservices.domain.motorizado;

public class MotorizadoRequestData {

    private String codmotorizado;
    private String codigopedido;
    private String documentocliente;
    private String motivorechazo;
    private Integer biometria;

    public String getCodmotorizado() {
        return codmotorizado;
    }

    public void setCodmotorizado(String codmotorizado) {
        this.codmotorizado = codmotorizado;
    }

    public String getCodigopedido() {
        return codigopedido;
    }

    public void setCodigopedido(String codigopedido) {
        this.codigopedido = codigopedido;
    }

    public String getDocumentocliente() {
        return documentocliente;
    }

    public void setDocumentocliente(String documentocliente) {
        this.documentocliente = documentocliente;
    }

    public String getMotivorechazo() {
        return motivorechazo;
    }

    public void setMotivorechazo(String motivorechazo) {
        this.motivorechazo = motivorechazo;
    }

    public Integer getBiometria() {
        return biometria;
    }

    public void setBiometria(Integer biometria) {
        this.biometria = biometria;
    }
}
