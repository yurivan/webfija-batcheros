package pe.com.tdp.ventafija.microservices.service.automatizador;

import org.apache.http.HttpException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.automatizadorsale.AutomatizadorSaleClient;
import pe.com.tdp.ventafija.microservices.common.clients.automatizadorsale.AutomatizadorSaleRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.automatizadorsale.AutomatizadorSaleResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.dao.CuponesRepository;
import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.dao.TdpUbigeoEquivRepository;
import pe.com.tdp.ventafija.microservices.common.dao.impl.CuponesRepositoryImpl;
import pe.com.tdp.ventafija.microservices.common.domain.entity.*;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.OrderConstants;
import pe.com.tdp.ventafija.microservices.domain.automatizador.EstadoSolicitudRequest;
import pe.com.tdp.ventafija.microservices.domain.automatizador.NormalizadorModelVO;
import pe.com.tdp.ventafija.microservices.domain.automatizador.entity.Automatizador;
import pe.com.tdp.ventafija.microservices.domain.automatizador.entity.AutomatizadorSaleService;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpOrder;
import pe.com.tdp.ventafija.microservices.domain.repository.AutomatizadorOrderRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.AutomatizadorRepository;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpOrderRepository;
import pe.com.tdp.ventafija.microservices.repository.AutomatizadorRequestSaleService;
import pe.com.tdp.ventafija.microservices.repository.address.AddressDao;
import pe.com.tdp.ventafija.microservices.service.order.OrderService;

import javax.xml.ws.http.HTTPException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class AutomatizadorService {

    private static final Logger logger = LogManager.getLogger();

    @Autowired
    private ApiHeaderConfig apiHeaderConfig;
    @Autowired
    private AutomatizadorRepository automatizadorRepository;
    @Autowired
    private TdpOrderRepository tdpOrderRepository;



    @Autowired
    private ServiceCallEventsService serviceCallEventsService;
    @Autowired
    private ParametersRepository parametersRepository;


    @Autowired
    private OrderService orderService;


    @Autowired
    private AutomatizadorRequestSaleService automatizadorRequestSaleService;


    @Autowired
    private CuponesRepository cuponesAutomatizadorRepository;

    @Autowired
    CuponesRepositoryImpl cuponesRepository;

    @Autowired
    private TdpUbigeoEquivRepository tdpUbigeoEquivRepository;
    @Value("${tdp.sw.automatizador.tgestiona}")
    private Boolean enabled;

    @Autowired
    private AddressDao addressDao;

    @Value("${tdp.sw.automatizador.logica.direcciones}")
    private String logicaDireccionesEnabled;

    //Con esto realizaremos parametrizables los errores de buss

    @Value("${tdp.automatizador.errBusParametrizable}")
    private String errBusParametrizable;

    //Sprint24 RUC
    @Autowired
    private AutomatizadorOrderRepository automatizadorOrderRepository;

    public List<String> automatizadorExtraValidacion() {

        List<String> response = new ArrayList<>();

        List<Parameters> enableList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("AUTOMATIZADOR", "EXTRA");
        for (Parameters i : enableList) {
            response.add(i.getStrValue());
        }
        System.out.println(response);


        return response;
    }

    public boolean preRegisterSaleWeb(String orderId, String type) {


        //consultando método de pago
       /* String payMethod = addressDao.getPayMethod(orderId);
        if(payMethod !=null) {
            if (payMethod.equalsIgnoreCase("CONTADO")) {
            return false;
            }
        }*/

        try {
            List<String> automatizadorAux = automatizadorExtraValidacion();

            boolean cupones_enable = Boolean.parseBoolean(automatizadorAux.get(0));
            boolean automatizadorSegmentacionSwitch = Boolean.parseBoolean(automatizadorAux.get(2));


            //cupones por switch
            if (!cupones_enable) {
                return false;
            }
            if (!automatizadorSegmentacionSwitch) {
                return false;
            }

            Boolean dataExiste = Boolean.parseBoolean(automatizadorAux.get(5));

            if (dataExiste) {
                //orderService.migrateVisor(orderId, "PENDIENTE");
            }

        /*List<String> listaAutomatizadorExtra =  automatizadorExtraValidacion();
        System.out.println(listaAutomatizadorExtra);*/


            //validar si paso por normalizador
            boolean normalizador = addressDao.goToNormalizador(orderId);
            boolean _exitsVentaVisor = addressDao._exitsVentaVisor(orderId);
            System.out.println(normalizador);
            System.out.println(normalizador);

            if (_exitsVentaVisor) {
                automatizadorOrderRepository.updateAutomatizadorOrder("ERR-0004", orderId);
                return false;
            }

            Boolean isEnabled = false;
            List<Parameters> enableList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("AUTOMATIZADOR", "ENABLE");
            for (Parameters obj : enableList) {
                if (type.equalsIgnoreCase(OrderConstants.ALTA_PURA) && obj.getElement().equalsIgnoreCase("auto.altapura")
                        && obj.getStrValue().equalsIgnoreCase("true")) {
                    isEnabled = true;
                    break;
                }
                if (type.equalsIgnoreCase(OrderConstants.ALTA_SVA) && obj.getElement().equalsIgnoreCase("auto.altasva")
                        && obj.getStrValue().equalsIgnoreCase("true")) {
                    isEnabled = true;
                    break;
                }
                if (type.equalsIgnoreCase(OrderConstants.MIGRACION) && obj.getElement().equalsIgnoreCase("auto.migracion")
                        && obj.getStrValue().equalsIgnoreCase("true")) {
                    isEnabled = true;
                    break;
                }
            }


            if (!isEnabled) {
                logger.info("Automatizador de encuentra DESACTIVADO.");
                return false;
            }

            TdpOrder tdpOrder = tdpOrderRepository.findOneByOrderId(orderId);
            //script auto cms dinamicos en punto de venta

            String cmsPuntoVentaDepa = addressDao.autoDepaCmsPunto(tdpOrder.getUserAtis(), tdpOrder.getAddressDepartamento());

            if (cmsPuntoVentaDepa != null) {
                if (cmsPuntoVentaDepa.isEmpty()) {
                    cmsPuntoVentaDepa = "0";
                }
            }
            tdpOrder.setUserFuerzaVenta(cmsPuntoVentaDepa);

           //TODO: Aplicar validacion
           /*
           if (tdpOrder.getClientTipoDoc().equalsIgnoreCase("RUC")) {
               if (!switchRuc) {
                   logger.info("Automatizador de encuentra DESACTIVADO para RUC.");
                   return false;
               }
               return false;
           }*/

           //TODO: Aplicar validacion
           if (tdpOrder.getClientTipoDoc().equalsIgnoreCase("RUC")) {
               String rucNum = tdpOrder.getClientNumeroDoc();
               String sub = rucNum.substring(0,2);
               if(sub.equals("20")){
                   return false;
               }
           }

            boolean switchRuc = Boolean.parseBoolean(automatizadorAux.get(3));

            //TODO: Aplicar validacion
            if (tdpOrder.getClientTipoDoc().equalsIgnoreCase("RUC")) {
                if (!switchRuc) {
                    logger.info("Automatizador de encuentra DESACTIVADO para RUC.");
                    return false;
                }else{
                    if(tdpOrder.getClientNumeroDoc().trim().substring(0,2).equals("20")){
                        return false;
                }
            }
                 //   return false;
                }
                //return false;

            boolean isPaquetizacion = false;
            if (tdpOrder.getClientCmsCodigo() != null && tdpOrder.getClientCmsServicio() != null && type.equalsIgnoreCase(OrderConstants.MIGRACION))
                isPaquetizacion = true;

            if (type.equalsIgnoreCase(OrderConstants.MIGRACION) && isPaquetizacion) {
                logger.info("Automatizador de encuentra DESACTIVADO para MIGRACIONES por que no es paquetización");
                return false;
            }


            SimpleDateFormat sdfDatetime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");

            AutomatizadorSaleRequestBody request = new AutomatizadorSaleRequestBody();

            request.setUserFuerzaVenta(tdpOrder.getUserFuerzaVenta() != null ? tdpOrder.getUserFuerzaVenta() : "");
            request.setProductoSenial(tdpOrder.getProductSenal() != null ? tdpOrder.getProductSenal() : "");
            request.setUserNombre(tdpOrder.getUserNombre() != null ? tdpOrder.getUserNombre() : "");
            request.setAddressReferencia(tdpOrder.getAddressReferencia() != null ? tdpOrder.getAddressReferencia() : "");
            request.setOrderCallTipo(tdpOrder.getOrderCallTipo() != null ? tdpOrder.getOrderCallTipo() : "");
            request.setOrderCallDesc(tdpOrder.getOrderCallDesc() != null ? tdpOrder.getOrderCallDesc() : "");
            request.setOrderModelo(tdpOrder.getOrderModelo() != null ? tdpOrder.getOrderModelo() : "");
            request.setUserDNI(tdpOrder.getUserDni() != null ? tdpOrder.getUserDni() : "");
            request.setUserTelefono(tdpOrder.getUserTelefono() != null ? tdpOrder.getUserTelefono() : "");
            request.setOrderContrato(tdpOrder.getOrderContrato() != null ? tdpOrder.getOrderContrato() : "");
            request.setOrderIDGrabacion(tdpOrder.getOrderGrabacion() != null ? tdpOrder.getOrderGrabacion() : "");
            request.setOrderModalidadAcep(tdpOrder.getOrderModalidadAcept() != null ? tdpOrder.getOrderModalidadAcept() : "");
            request.setOrderEntidad(tdpOrder.getUserEntidad() != null ? tdpOrder.getUserEntidad() : "");
            request.setOrderRegion(tdpOrder.getUserRegion() != null ? tdpOrder.getUserRegion() : "");

            request.setOrderCIP(tdpOrder.getOrderCip() != null ? tdpOrder.getOrderCip() : "");
            request.setOrderExperto(tdpOrder.getOrderExperto() != null ? tdpOrder.getOrderExperto() : "");
            request.setOrderZonal(tdpOrder.getUserZonal() != null ? tdpOrder.getUserZonal() : "");
            request.setOrderWebParental(tdpOrder.getAffiliationParentalProtection() != null ? tdpOrder.getAffiliationParentalProtection() : "");
            request.setOrderCodigoSTC6I(tdpOrder.getOrderStc6i() != null ? tdpOrder.getOrderStc6i() : "");

            request.setClienteNacionalidad(tdpOrder.getClientNationality() != null ? tdpOrder.getClientNationality() : "");

            String origenCode = "1";
            if (tdpOrder.getOrderOrigen() != null && tdpOrder.getOrderOrigen().equalsIgnoreCase("APPVF"))
                origenCode = "1";
            if (tdpOrder.getOrderOrigen() != null && tdpOrder.getOrderOrigen().equalsIgnoreCase("WEBVF"))
                origenCode = "2";
            request.setOrderModoVenta(origenCode);

            String cantidadEquipos = getCantidadEquipos(tdpOrder);
            request.setOrderCantidadEquipos(cantidadEquipos);

            request.setOrderGisNTLT(tdpOrder.getOrderGisNtlt() != null ? tdpOrder.getOrderGisNtlt() : "");

            request = SetGeneralAutomatizador(request, tdpOrder, sdfDatetime, isPaquetizacion);

            if (request != null) {
                if (tdpOrder.getProductTipoReg().equalsIgnoreCase("ATIS")) {
                    request = SetATISAutomatizador(request, tdpOrder);
                }

                if (tdpOrder.getProductTipoReg().equalsIgnoreCase("CMS")) {
                    request = SetCMSAutomatizador(request, tdpOrder, sdfDate);
                }
            }

            if (request == null) {
                return false;
            }



           /*
            * Aquí comenzamos con automatizador
            * */


            String[] segmentacionCanalSplit = automatizadorAux.get(1).split(",");
            SimpleDateFormat formatPresentDay = new SimpleDateFormat("yyyy-MM-dd");
            String presentDay = formatPresentDay.format(new Date());
            Boolean autoSend = false;

            for (String i : segmentacionCanalSplit) {
                if (tdpOrder.getUserCanalCodigo().equalsIgnoreCase(i)) {
                    autoSend = true;
                }
            }


            if (!autoSend) {
                return false;
            }

            //Validamos los datos con tabla normalizador
            //y según lo obtenido agregamos lo que falta.

            NormalizadorModelVO normalizadorModelVO = addressDao.showNormalizador(request.getOrderId());

            System.out.println(normalizadorModelVO);
            System.out.println(normalizadorModelVO);

            if(normalizadorModelVO.getTipo() != null){
                if (normalizadorModelVO.getTipo().equalsIgnoreCase("1")) {

                    if (normalizadorModelVO.getTipo_urbanizacion() != null) {
                        if (!normalizadorModelVO.getTipo_urbanizacion().isEmpty()) {
                            if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("URB")) {
                                request.setAddressCCHHTipo("UR");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("POBLADO")) {
                                request.setAddressCCHHTipo("PO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("VILLA MILITAR")) {
                                request.setAddressCCHHTipo("VM");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("URBANIZACION POPULAR")) {
                                request.setAddressCCHHTipo("UP");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("RES")) {
                                request.setAddressCCHHTipo("RS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("PPJJ")) {
                                request.setAddressCCHHTipo("PJ");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("PARCELA")) {
                                request.setAddressCCHHTipo("PA");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("LOT")) {
                                request.setAddressCCHHTipo("LO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("GRUPO")) {
                                request.setAddressCCHHTipo("GR");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("FUNDO")) {
                                request.setAddressCCHHTipo("FU");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COOP")) {
                                request.setAddressCCHHTipo("CV");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDAD UNIVERSITARIA")) {
                                request.setAddressCCHHTipo("CU");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDAD SATELITE")) {
                                request.setAddressCCHHTipo("CS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CH")) {
                                request.setAddressCCHHTipo("CO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COMITE")) {
                                request.setAddressCCHHTipo("CM");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDADELA")) {
                                request.setAddressCCHHTipo("CD");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("BARRIO")) {
                                request.setAddressCCHHTipo("BA");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("ASOC")) {
                                request.setAddressCCHHTipo("AS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("AGR")) {
                                request.setAddressCCHHTipo("AG");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COND")) {
                                request.setAddressCCHHTipo("CDM");
                            }
                            else {
                                request.setAddressCCHHTipo(normalizadorModelVO.getTipo_urbanizacion());
                            }
                        }
                    }

                    if (normalizadorModelVO.getTipoVia() != null) {
                        if (!normalizadorModelVO.getTipoVia().isEmpty()) {
                            if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PSJ")) {
                                request.setAddressCalleAtis("PJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("AUTOPISTA")) {
                                request.setAddressCalleAtis("AU");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("SENDERO")) {
                                request.setAddressCalleAtis("SD");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CUESTA")) {
                                request.setAddressCalleAtis("CT");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CAMINO")) {
                                request.setAddressCalleAtis("CM");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PLAZUELA")) {
                                request.setAddressCalleAtis("PZ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PASEO")) {
                                request.setAddressCalleAtis("PO");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PZ")) {
                                request.setAddressCalleAtis("PL");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PSJ")) {
                                request.setAddressCalleAtis("PJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("OVALO")) {
                                request.setAddressCalleAtis("OV");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("ML")) {
                                request.setAddressCalleAtis("MA");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("GAL")) {
                                request.setAddressCalleAtis("GL");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CALLEJON")) {
                                request.setAddressCalleAtis("CJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CAR")) {
                                request.setAddressCalleAtis("CA");
                            }
                            else if(normalizadorModelVO.getTipoVia().equalsIgnoreCase("COND")) {
                                request.setAddressCalleAtis("CDM");
                            }else {
                                request.setAddressCalleAtis(normalizadorModelVO.getTipoVia());
                            }
                        }
                    }

                    if (normalizadorModelVO.getNombre_urbanizacion() != null) {
                        if (!normalizadorModelVO.getNombre_urbanizacion().isEmpty()) {
                            request.setAddressCCHHNombre(normalizadorModelVO.getNombre_urbanizacion());
                        }
                    }

                    if (normalizadorModelVO.getNombreVia() != null) {
                        if (!normalizadorModelVO.getNombreVia().isEmpty()) {
                            request.setAddressCalleNombre(normalizadorModelVO.getNombreVia());
                        } else {
                            request.setAddressCalleNombre(".");
                        }
                    }

                    if (request.getAddressViaCompTipo1() != null) {
                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("MZ")) {
                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(".");
                        }


                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");
                        }

                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");

                            request.setAddressViaCompTipo3(request.getAddressViaCompTipo3());
                            request.setAddressViaCompNombre3(request.getAddressViaCompNombre3() != null ? request.getAddressViaCompNombre3() : "0");
                        }
                    }

                    if (request.getAddressViaCompTipo1() != null) {
                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3("0");
                        }
                    }

                    if (request.getAddressViaCompTipo2() != null) {

                        if (request.getAddressViaCompTipo2().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(".");

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3("0");
                        }
                    }

                    if (normalizadorModelVO.getManzana() != null) {
                        if (!normalizadorModelVO.getManzana().isEmpty()) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(normalizadorModelVO.getManzana());

                        }

                    }

                    if (normalizadorModelVO.getLote() != null) {
                        if (!normalizadorModelVO.getLote().isEmpty()) {

                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(normalizadorModelVO.getLote());

                        }
                    }

                    if (normalizadorModelVO.getCuadra() != null) {
                        if (!normalizadorModelVO.getCuadra().isEmpty()) {

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3(normalizadorModelVO.getCuadra());

                        }
                    }


                    if (request.getAddressViaCompNombre1() != null) {
                        if (!request.getAddressViaCompNombre1().isEmpty()) {
                            request.setAddressViaCompTipo1("MZ");
                        }

                    }

                    if (request.getAddressViaCompNombre2() != null) {
                        if (!request.getAddressViaCompNombre2().isEmpty()) {
                            request.setAddressViaCompTipo2("LT");
                        }
                    }


                    if (request.getAddressViaCompNombre3() != null) {
                        if (!request.getAddressViaCompNombre3().isEmpty()) {
                            request.setAddressViaCompTipo3("CDR");
                        }
                    }

                    if (request.getAddressViaCompTipo1() == null) {
                        if (request.getAddressViaCompTipo2() != null) {
                            if (!request.getAddressViaCompTipo2().equalsIgnoreCase("MZ")) {
                                request.setAddressViaCompTipo1("MZ");
                                request.setAddressViaCompNombre1(".");
                            }
                        }
                    }

                    if (request.getAddressViaCompTipo2() == null) {
                        if (request.getAddressViaCompTipo1() != null) {
                            if (!request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                                request.setAddressViaCompTipo2("LT");
                                request.setAddressViaCompNombre2(".");
                            }
                        }
                    }


                    if (!normalizador) {
                        if (!type.equalsIgnoreCase(OrderConstants.ALTA_SVA)) {
                            return false;
                        }
                        if ((type.equalsIgnoreCase(OrderConstants.ALTA_SVA) && (tdpOrder.getOrderOperationNumber().equals(2)))) {
                            request = new AutomatizadorSaleRequestBody();

                            request.setClienteNumeroDoc(tdpOrder.getClientNumeroDoc());
                            request.setClienteTipoDoc(tdpOrder.getClientTipoDoc());

                            //TODO: Aplicar validacion
                            if (tdpOrder.getClientTipoDoc() != null) {
                                switch (tdpOrder.getClientTipoDoc().toUpperCase().trim()) {
                                    case "DNI":
                                        request.setClienteTipoDoc("DNI");
                                        break;
                                    case "CE":
                                        request.setClienteTipoDoc("CEX");
                                        break;
                                    case "PAS":
                                        request.setClienteTipoDoc("CEX");
                                        break;
                                    case "RUC":
                                        request.setClienteTipoDoc("RUC");
                                        break;
                                    default:
                                        request.setClienteTipoDoc("NDE");
                                        break;
                                }
                            }


                            request.setUserAtis(tdpOrder.getUserAtis());
                            request.setUserFuerzaVenta(tdpOrder.getUserFuerzaVenta());
                            request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                            request.setProductoSVACodigo1(tdpOrder.getSvaCodigo1());
                            request.setProductoSVACodigo2(tdpOrder.getSvaCodigo2());
                            request.setProductoSVACodigo3(tdpOrder.getSvaCodigo3());
                            request.setProductoSVACodigo4(tdpOrder.getSvaCodigo4());
                            request.setProductoSVACodigo5(tdpOrder.getSvaCodigo5());
                            request.setProductoSVACodigo6(tdpOrder.getSvaCodigo6());
                            request.setProductoSVACodigo7(tdpOrder.getSvaCodigo7());
                            request.setProductoSVACodigo8(tdpOrder.getSvaCodigo8());
                            request.setProductoSVACodigo9(tdpOrder.getSvaCodigo9());
                            request.setProductoSVACodigo10(tdpOrder.getSvaCodigo10());

                            request.setOrderId(tdpOrder.getOrderId());

                            String dateOrderStr = "";
                            Date dateOrder = tdpOrder.getOrderFechaHora();

                            if (dateOrder != null) {
                                dateOrderStr = sdfDatetime.format(dateOrder);
                            }
                            if (dateOrderStr != null) {
                                request.setOrderFecha(dateOrderStr);
                            }

                            request.setOrderParqueTelefono(tdpOrder.getOrderParqueTelefono());
                            request.setOrderOperacionComercial("2");


                            //Agregando Campos necesarios para que automatizador complete sus reportes by Valdemar

                            if (tdpOrder.getClientNombre() != null) {
                                request.setClienteNombre(tdpOrder.getClientNombre());
                            }

                            if (tdpOrder.getClientApellidoPaterno() != null) {
                                if (!tdpOrder.getClientApellidoPaterno().isEmpty()) {
                                    request.setClienteApellidoPaterno(tdpOrder.getClientApellidoPaterno());
                                } else {
                                    request.setClienteApellidoPaterno(".");
                                }
                            } else {
                                request.setClienteApellidoPaterno(".");
                            }

                            if (tdpOrder.getClientApellidoMaterno() != null) {
                                if (!tdpOrder.getClientApellidoMaterno().isEmpty()) {
                                    request.setClienteApellidoMaterno(tdpOrder.getClientApellidoMaterno());
                                } else {
                                    request.setClienteApellidoMaterno(".");
                                }
                            } else {
                                request.setClienteApellidoMaterno(".");
                            }

                            request.setClienteTelefono1(tdpOrder.getClientTelefono1());
                            request.setClienteEmail(tdpOrder.getClientEmail());
                            request.setOrderContrato(tdpOrder.getOrderContrato() != null ? tdpOrder.getOrderContrato() : "");

                            request.setOrderZonal(tdpOrder.getUserZonal() != null ? tdpOrder.getUserZonal() : "");

                            request.setUserDNI(tdpOrder.getUserDni());

                            request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                            request.setOrderModoVenta(origenCode);

                            if (tdpOrder.getUserFuerzaVenta() != null) {
                                request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
                            }

                        }
                    }

                } else if(normalizadorModelVO.getTipo().equalsIgnoreCase("2")){
                    //OCT_TIP_CMP
                    if (tdpOrder.getAddressViaCompTip3() != null) {
                        request.setAddressViaCompTipo3(tdpOrder.getAddressViaCompTip3());
                    }
                    //OCT_TIP_DIR
                    if (tdpOrder.getAddressViaCompNom3() != null) {
                        request.setAddressViaCompNombre3(tdpOrder.getAddressViaCompNom3());

                    }
                    //NOV_TIP_CMP
                    if (tdpOrder.getAddressViaCompTip4() != null) {
                        request.setAddressViaCompTipo4(tdpOrder.getAddressViaCompTip4());
                    }
                    //NOV_TIP_DIR
                    if (tdpOrder.getAddressViaCompNom4() != null) {
                        request.setAddressViaCompNombre4(tdpOrder.getAddressViaCompNom4());
                    }
                    //DCO_TIP_CMP
                    if (tdpOrder.getAddressViaCompTip5() != null) {
                        request.setAddressViaCompTipo5(tdpOrder.getAddressViaCompTip5());
                    }
                    //DCO_TIP_DIR
                    if (tdpOrder.getAddressViaCompNom5() != null) {
                        request.setAddressViaCompNombre5(tdpOrder.getAddressViaCompNom5());
                    }

                }

                else{
                    if (normalizadorModelVO.getTipo_urbanizacion() != null) {
                        if (!normalizadorModelVO.getTipo_urbanizacion().isEmpty()) {
                            if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("URB")) {
                                request.setAddressCCHHTipo("UR");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("POBLADO")) {
                                request.setAddressCCHHTipo("PO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("VILLA MILITAR")) {
                                request.setAddressCCHHTipo("VM");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("URBANIZACION POPULAR")) {
                                request.setAddressCCHHTipo("UP");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("RES")) {
                                request.setAddressCCHHTipo("RS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("PPJJ")) {
                                request.setAddressCCHHTipo("PJ");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("PARCELA")) {
                                request.setAddressCCHHTipo("PA");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("LOT")) {
                                request.setAddressCCHHTipo("LO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("GRUPO")) {
                                request.setAddressCCHHTipo("GR");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("FUNDO")) {
                                request.setAddressCCHHTipo("FU");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COOP")) {
                                request.setAddressCCHHTipo("CV");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDAD UNIVERSITARIA")) {
                                request.setAddressCCHHTipo("CU");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDAD SATELITE")) {
                                request.setAddressCCHHTipo("CS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CH")) {
                                request.setAddressCCHHTipo("CO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COMITE")) {
                                request.setAddressCCHHTipo("CM");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDADELA")) {
                                request.setAddressCCHHTipo("CD");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("BARRIO")) {
                                request.setAddressCCHHTipo("BA");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("ASOC")) {
                                request.setAddressCCHHTipo("AS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("AGR")) {
                                request.setAddressCCHHTipo("AG");
                            } 
                             else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COND")) {
                                request.setAddressCCHHTipo("CDM");
                            }
                            else {
                                request.setAddressCCHHTipo(normalizadorModelVO.getTipo_urbanizacion());
                            }
                        }
                    }

                    if (normalizadorModelVO.getTipoVia() != null) {
                        if (!normalizadorModelVO.getTipoVia().isEmpty()) {
                            if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PSJ")) {
                                request.setAddressCalleAtis("PJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("AUTOPISTA")) {
                                request.setAddressCalleAtis("AU");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("SENDERO")) {
                                request.setAddressCalleAtis("SD");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CUESTA")) {
                                request.setAddressCalleAtis("CT");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CAMINO")) {
                                request.setAddressCalleAtis("CM");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PLAZUELA")) {
                                request.setAddressCalleAtis("PZ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PASEO")) {
                                request.setAddressCalleAtis("PO");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PZ")) {
                                request.setAddressCalleAtis("PL");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PSJ")) {
                                request.setAddressCalleAtis("PJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("OVALO")) {
                                request.setAddressCalleAtis("OV");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("ML")) {
                                request.setAddressCalleAtis("MA");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("GAL")) {
                                request.setAddressCalleAtis("GL");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CALLEJON")) {
                                request.setAddressCalleAtis("CJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CAR")) {
                                request.setAddressCalleAtis("CA");
                            } else if(normalizadorModelVO.getTipoVia().equalsIgnoreCase("COND")) {
                                request.setAddressCalleAtis("CDM");
                            }else {
                                request.setAddressCalleAtis(normalizadorModelVO.getTipoVia());
                            }
                        }
                    }

                    if (normalizadorModelVO.getNombre_urbanizacion() != null) {
                        if (!normalizadorModelVO.getNombre_urbanizacion().isEmpty()) {
                            request.setAddressCCHHNombre(normalizadorModelVO.getNombre_urbanizacion());
                        }
                    }

                    if (normalizadorModelVO.getNombreVia() != null) {
                        if (!normalizadorModelVO.getNombreVia().isEmpty()) {
                            request.setAddressCalleNombre(normalizadorModelVO.getNombreVia());
                        } else {
                            request.setAddressCalleNombre(".");
                        }
                    }

                    if (request.getAddressViaCompTipo1() != null) {
                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("MZ")) {
                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(".");
                        }


                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");
                        }

                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");

                            request.setAddressViaCompTipo3(request.getAddressViaCompTipo3());
                            request.setAddressViaCompNombre3(request.getAddressViaCompNombre3() != null ? request.getAddressViaCompNombre3() : "0");
                        }
                    }

                    if (request.getAddressViaCompTipo1() != null) {
                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3("0");
                        }
                    }

                    if (request.getAddressViaCompTipo2() != null) {

                        if (request.getAddressViaCompTipo2().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(".");

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3("0");
                        }
                    }

                    if (normalizadorModelVO.getManzana() != null) {
                        if (!normalizadorModelVO.getManzana().isEmpty()) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(normalizadorModelVO.getManzana());

                        }

                    }

                    if (normalizadorModelVO.getLote() != null) {
                        if (!normalizadorModelVO.getLote().isEmpty()) {

                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(normalizadorModelVO.getLote());

                        }
                    }

                    if (normalizadorModelVO.getCuadra() != null) {
                        if (!normalizadorModelVO.getCuadra().isEmpty()) {

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3(normalizadorModelVO.getCuadra());

                        }
                    }


                    if (request.getAddressViaCompNombre1() != null) {
                        if (!request.getAddressViaCompNombre1().isEmpty()) {
                            request.setAddressViaCompTipo1("MZ");
                        }

                    }

                    if (request.getAddressViaCompNombre2() != null) {
                        if (!request.getAddressViaCompNombre2().isEmpty()) {
                            request.setAddressViaCompTipo2("LT");
                        }
                    }


                    if (request.getAddressViaCompNombre3() != null) {
                        if (!request.getAddressViaCompNombre3().isEmpty()) {
                            request.setAddressViaCompTipo3("CDR");
                        }
                    }

                    if (request.getAddressViaCompTipo1() == null) {
                        if (request.getAddressViaCompTipo2() != null) {
                            if (!request.getAddressViaCompTipo2().equalsIgnoreCase("MZ")) {
                                request.setAddressViaCompTipo1("MZ");
                                request.setAddressViaCompNombre1(".");
                            }
                        }
                    }

                    if (request.getAddressViaCompTipo2() == null) {
                        if (request.getAddressViaCompTipo1() != null) {
                            if (!request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                                request.setAddressViaCompTipo2("LT");
                                request.setAddressViaCompNombre2(".");
                            }
                        }
                    }


                    if (!normalizador) {
                        if (!type.equalsIgnoreCase(OrderConstants.ALTA_SVA)) {
                            return false;
                        }
                        if ((type.equalsIgnoreCase(OrderConstants.ALTA_SVA) && (tdpOrder.getOrderOperationNumber().equals(2)))) {
                            request = new AutomatizadorSaleRequestBody();

                            request.setClienteNumeroDoc(tdpOrder.getClientNumeroDoc());
                            request.setClienteTipoDoc(tdpOrder.getClientTipoDoc());

                            //TODO: Aplicar validacion
                            if (tdpOrder.getClientTipoDoc() != null) {
                                switch (tdpOrder.getClientTipoDoc().toUpperCase().trim()) {
                                    case "DNI":
                                        request.setClienteTipoDoc("DNI");
                                        break;
                                    case "CE":
                                        request.setClienteTipoDoc("CEX");
                                        break;
                                    case "PAS":
                                        request.setClienteTipoDoc("CEX");
                                        break;
                                    case "RUC":
                                        request.setClienteTipoDoc("RUC");
                                        break;
                                    default:
                                        request.setClienteTipoDoc("NDE");
                                        break;
                                }
                            }

                            request.setUserAtis(tdpOrder.getUserAtis());
                            request.setUserFuerzaVenta(tdpOrder.getUserFuerzaVenta());
                            request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                            request.setProductoSVACodigo1(tdpOrder.getSvaCodigo1());
                            request.setProductoSVACodigo2(tdpOrder.getSvaCodigo2());
                            request.setProductoSVACodigo3(tdpOrder.getSvaCodigo3());
                            request.setProductoSVACodigo4(tdpOrder.getSvaCodigo4());
                            request.setProductoSVACodigo5(tdpOrder.getSvaCodigo5());
                            request.setProductoSVACodigo6(tdpOrder.getSvaCodigo6());
                            request.setProductoSVACodigo7(tdpOrder.getSvaCodigo7());
                            request.setProductoSVACodigo8(tdpOrder.getSvaCodigo8());
                            request.setProductoSVACodigo9(tdpOrder.getSvaCodigo9());
                            request.setProductoSVACodigo10(tdpOrder.getSvaCodigo10());

                            request.setOrderId(tdpOrder.getOrderId());

                            String dateOrderStr = "";
                            Date dateOrder = tdpOrder.getOrderFechaHora();

                            if (dateOrder != null) {
                                dateOrderStr = sdfDatetime.format(dateOrder);
                            }
                            if (dateOrderStr != null) {
                                request.setOrderFecha(dateOrderStr);
                            }

                            request.setOrderParqueTelefono(tdpOrder.getOrderParqueTelefono());
                            request.setOrderOperacionComercial("2");


                            //Agregando Campos necesarios para que automatizador complete sus reportes by Valdemar

                            if (tdpOrder.getClientNombre() != null) {
                                request.setClienteNombre(tdpOrder.getClientNombre());
                            }

                            if (tdpOrder.getClientApellidoPaterno() != null) {
                                if (!tdpOrder.getClientApellidoPaterno().isEmpty()) {
                                    request.setClienteApellidoPaterno(tdpOrder.getClientApellidoPaterno());
                                } else {
                                    request.setClienteApellidoPaterno(".");
                                }
                            } else {
                                request.setClienteApellidoPaterno(".");
                            }

                            if (tdpOrder.getClientApellidoMaterno() != null) {
                                if (!tdpOrder.getClientApellidoMaterno().isEmpty()) {
                                    request.setClienteApellidoMaterno(tdpOrder.getClientApellidoMaterno());
                                } else {
                                    request.setClienteApellidoMaterno(".");
                                }
                            } else {
                                request.setClienteApellidoMaterno(".");
                            }

                            request.setClienteTelefono1(tdpOrder.getClientTelefono1());
                            request.setClienteEmail(tdpOrder.getClientEmail());
                            request.setOrderContrato(tdpOrder.getOrderContrato() != null ? tdpOrder.getOrderContrato() : "");

                            request.setOrderZonal(tdpOrder.getUserZonal() != null ? tdpOrder.getUserZonal() : "");

                            request.setUserDNI(tdpOrder.getUserDni());

                            request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                            request.setOrderModoVenta(origenCode);

                            if (tdpOrder.getUserFuerzaVenta() != null) {
                                request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
                            }


                        }
                    }
                }
            }


            //validar tipo de conjunto habitacional contingencia
       //     Tipo de conjunto habitacional => getAddressCchhTipo
       //     Nombre de conjunto Habitacional =>  getAddressCchhNombre
            request.getAddressCCHHTipo();
            request.getAddressCCHHNombre();

            if(request.getAddressCCHHTipo() != null){
                if(request.getAddressCCHHTipo().isEmpty()){
                    request.setAddressCCHHTipo("");
                    request.setAddressCCHHNombre("");
                }
                if(request.getAddressCCHHTipo().equalsIgnoreCase(".")){
                    request.setAddressCCHHTipo("");
                    request.setAddressCCHHNombre("");
                }
            }



            //Fin validación con tabla normalizador

            String[] erroresBuss = errBusParametrizable.split(",");

            String mensaje_duplicado = "";
            boolean result = false;

            try {
                ApiResponse<AutomatizadorSaleResponseBody> response = registerSale(request);
                if (response != null && response.getHeaderOut() != null && response.getHeaderOut().getMsgType() != null &&
                        response.getHeaderOut().getMsgType().equalsIgnoreCase("RESPONSE")) {
                    result = true;
                    //addressDao._registerDuplicado(orderId);
                    mensaje_duplicado = "OK";
                }

                if (response != null && response.getBodyOut() != null) {
                    if (response.getBodyOut().getServerException() != null) {
                        for (String i : erroresBuss) {
                            int res = Integer.parseInt(i);
                            if (response.getBodyOut().getServerException().getExceptionCode() == res) {
                                mensaje_duplicado = "ERR_BUS";
                            }

                        }
                    }
                }

                if (response != null && response.getHeaderOut() != null && response.getHeaderOut().getMsgType() != null &&
                        response.getHeaderOut().getMsgType().equalsIgnoreCase("ERROR")) {

                    if (response.getBodyOut() != null) {
                        if (response.getBodyOut() != null) {
                            if (response.getBodyOut().getClientException() != null) {

                                if (response.getBodyOut().getClientException().getAppDetail().getExceptionAppCode().equalsIgnoreCase("ERR-0004")) {
                                    //addressDao._registerDuplicado(orderId);
                                    mensaje_duplicado = "ERR-0004";
                                }


                            }
                        }
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                mensaje_duplicado = "ERR_BUS";
            }


            //addressDao._registerVentaVisor(orderId,mensaje_duplicado);
            // if(!mensaje_duplicado.equalsIgnoreCase("ERR-0004")){
            addressDao.insertAuto(orderId);
            // automatizadorOrderRepository.insertAutomatizadorOrder(orderId);
            // }

            if (mensaje_duplicado.equalsIgnoreCase("ERR-0004")) {
                automatizadorOrderRepository.updateAutomatizadorOrder(mensaje_duplicado, orderId);
            }

            if (mensaje_duplicado.equalsIgnoreCase("ERR_BUS")) {
                automatizadorOrderRepository.updateAutomatizadorOrder(mensaje_duplicado, orderId);
                //guardar el request
                AutomatizadorSaleService automatizadorSaleService = poblarRequest(request);

                automatizadorRequestSaleService.save(automatizadorSaleService);

            }


            return result;
        } catch (Exception e) {
            return false;
        }



    }

    public boolean preRegisterSaleWeb2(String orderId, String type) {

        try{
            List<String> automatizadorAux = automatizadorExtraValidacion();

            boolean cupones_enable = Boolean.parseBoolean(automatizadorAux.get(0));
            boolean automatizadorSegmentacionSwitch = Boolean.parseBoolean(automatizadorAux.get(2));




            //cupones por switch
            if(!cupones_enable){
                return false;
            }
            if(!automatizadorSegmentacionSwitch){
                return false;
            }

            Boolean dataExiste = Boolean.parseBoolean(automatizadorAux.get(5));

            if(dataExiste){
                //orderService.migrateVisor(orderId, "PENDIENTE");
            }

        /*List<String> listaAutomatizadorExtra =  automatizadorExtraValidacion();
        System.out.println(listaAutomatizadorExtra);*/



            //validar si paso por normalizador
            boolean normalizador = addressDao.goToNormalizador(orderId);
            boolean _exitsVentaVisor = addressDao._exitsVentaVisor(orderId);
            System.out.println(normalizador);
            System.out.println(normalizador);

            if(_exitsVentaVisor){
                automatizadorOrderRepository.updateAutomatizadorOrder("ERR-0004",orderId);
                return false;
            }

            Boolean isEnabled = false;
            List<Parameters> enableList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("AUTOMATIZADOR", "ENABLE");
            for (Parameters obj : enableList) {
                if (type.equalsIgnoreCase(OrderConstants.ALTA_PURA) && obj.getElement().equalsIgnoreCase("auto.altapura")
                        && obj.getStrValue().equalsIgnoreCase("true")) {
                    isEnabled = true;
                    break;
                }

            }


            if (!isEnabled) {
                logger.info("Automatizador de encuentra DESACTIVADO.");
                return false;
            }


            TdpOrder tdpOrder = tdpOrderRepository.findOneByOrderId(orderId);

            String cmsPuntoVentaDepa = addressDao.autoDepaCmsPunto(tdpOrder.getUserAtis(),tdpOrder.getAddressDepartamento());

            if(cmsPuntoVentaDepa != null){
                if(cmsPuntoVentaDepa.isEmpty()){
                    cmsPuntoVentaDepa = "0";
                }
            }

            tdpOrder.setUserFuerzaVenta(cmsPuntoVentaDepa);

            boolean switchRuc = Boolean.parseBoolean(automatizadorAux.get(3));

            //TODO: Aplicar validacion
            if (tdpOrder.getClientTipoDoc().equalsIgnoreCase("RUC")) {
                String rucNum = tdpOrder.getClientNumeroDoc();
                String sub = rucNum.substring(0,2);
                if(sub.equals("20")){
                    return false;
                }
            }

            boolean isPaquetizacion = false;
            if (tdpOrder.getClientCmsCodigo() != null && tdpOrder.getClientCmsServicio() != null && type.equalsIgnoreCase(OrderConstants.MIGRACION))
                isPaquetizacion = true;

            if (type.equalsIgnoreCase(OrderConstants.MIGRACION) && isPaquetizacion) {
                logger.info("Automatizador de encuentra DESACTIVADO para MIGRACIONES por que no es paquetización");
                return false;
            }




            SimpleDateFormat sdfDatetime = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");

            AutomatizadorSaleRequestBody request = new AutomatizadorSaleRequestBody();

            request.setUserFuerzaVenta(tdpOrder.getUserFuerzaVenta() != null ? tdpOrder.getUserFuerzaVenta() : "");
            request.setProductoSenial(tdpOrder.getProductSenal() != null ? tdpOrder.getProductSenal() : "");
            request.setUserNombre(tdpOrder.getUserNombre() != null ? tdpOrder.getUserNombre() : "");
            request.setAddressReferencia(tdpOrder.getAddressReferencia() != null ? tdpOrder.getAddressReferencia() : "");
            request.setOrderCallTipo(tdpOrder.getOrderCallTipo() != null ? tdpOrder.getOrderCallTipo() : "");
            request.setOrderCallDesc(tdpOrder.getOrderCallDesc() != null ? tdpOrder.getOrderCallDesc() : "");
            request.setOrderModelo(tdpOrder.getOrderModelo() != null ? tdpOrder.getOrderModelo() : "");
            request.setUserDNI(tdpOrder.getUserDni() != null ? tdpOrder.getUserDni() : "");
            request.setUserTelefono(tdpOrder.getUserTelefono() != null ? tdpOrder.getUserTelefono() : "");
            request.setOrderContrato(tdpOrder.getOrderContrato() != null ? tdpOrder.getOrderContrato() : "");
            request.setOrderIDGrabacion(tdpOrder.getOrderGrabacion() != null ? tdpOrder.getOrderGrabacion() : "");
            request.setOrderModalidadAcep(tdpOrder.getOrderModalidadAcept() != null ? tdpOrder.getOrderModalidadAcept() : "");
            request.setOrderEntidad(tdpOrder.getUserEntidad() != null ? tdpOrder.getUserEntidad() : "");
            request.setOrderRegion(tdpOrder.getUserRegion() != null ? tdpOrder.getUserRegion() : "");

            request.setOrderCIP(tdpOrder.getOrderCip() != null ? tdpOrder.getOrderCip() : "");
            request.setOrderExperto(tdpOrder.getOrderExperto() != null ? tdpOrder.getOrderExperto() : "");
            request.setOrderZonal(tdpOrder.getUserZonal() != null ? tdpOrder.getUserZonal() : "");
            request.setOrderWebParental(tdpOrder.getAffiliationParentalProtection() != null ? tdpOrder.getAffiliationParentalProtection() : "");
            request.setOrderCodigoSTC6I(tdpOrder.getOrderStc6i() != null ? tdpOrder.getOrderStc6i() : "");

            request.setClienteNacionalidad(tdpOrder.getClientNationality() != null ? tdpOrder.getClientNationality() : "");

            String origenCode = "1";
            if (tdpOrder.getOrderOrigen() != null && tdpOrder.getOrderOrigen().equalsIgnoreCase("APPVF")) origenCode = "1";
            if (tdpOrder.getOrderOrigen() != null && tdpOrder.getOrderOrigen().equalsIgnoreCase("WEBVF")) origenCode = "2";
            request.setOrderModoVenta(origenCode);

            String cantidadEquipos = getCantidadEquipos(tdpOrder);
            request.setOrderCantidadEquipos(cantidadEquipos);

            request.setOrderGisNTLT(tdpOrder.getOrderGisNtlt() != null ? tdpOrder.getOrderGisNtlt() : "");

            request = SetGeneralAutomatizador(request, tdpOrder, sdfDatetime, isPaquetizacion);

            if (request != null) {
                if (tdpOrder.getProductTipoReg().equalsIgnoreCase("ATIS")) {
                    request = SetATISAutomatizador(request, tdpOrder);
                }

                if (tdpOrder.getProductTipoReg().equalsIgnoreCase("CMS")) {
                    request = SetCMSAutomatizador(request, tdpOrder, sdfDate);
                }
            }

            if (request == null) {
                return false;
            }



            /*
             * Aquí comenzamos con automatizador
             * */


            String[] segmentacionCanalSplit = automatizadorAux.get(1).split(",");
            SimpleDateFormat formatPresentDay = new SimpleDateFormat("yyyy-MM-dd");
            String presentDay = formatPresentDay.format(new Date());
            Boolean autoSend = false;

            for (String i : segmentacionCanalSplit) {
                if (tdpOrder.getUserCanalCodigo().equalsIgnoreCase(i)) {
                    autoSend = true;
                }
            }


            if (!autoSend) {
                return false;
            }

            //Validamos los datos con tabla normalizador
            //y según lo obtenido agregamos lo que falta.

            NormalizadorModelVO normalizadorModelVO = addressDao.showNormalizador(request.getOrderId());

            System.out.println(normalizadorModelVO);
            System.out.println(normalizadorModelVO);


            if(normalizadorModelVO.getTipo() != null){
                if (normalizadorModelVO.getTipo().equalsIgnoreCase("1")) {

                    if (normalizadorModelVO.getTipo_urbanizacion() != null) {
                        if (!normalizadorModelVO.getTipo_urbanizacion().isEmpty()) {
                            if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("URB")) {
                                request.setAddressCCHHTipo("UR");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("POBLADO")) {
                                request.setAddressCCHHTipo("PO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("VILLA MILITAR")) {
                                request.setAddressCCHHTipo("VM");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("URBANIZACION POPULAR")) {
                                request.setAddressCCHHTipo("UP");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("RES")) {
                                request.setAddressCCHHTipo("RS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("PPJJ")) {
                                request.setAddressCCHHTipo("PJ");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("PARCELA")) {
                                request.setAddressCCHHTipo("PA");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("LOT")) {
                                request.setAddressCCHHTipo("LO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("GRUPO")) {
                                request.setAddressCCHHTipo("GR");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("FUNDO")) {
                                request.setAddressCCHHTipo("FU");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COOP")) {
                                request.setAddressCCHHTipo("CV");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDAD UNIVERSITARIA")) {
                                request.setAddressCCHHTipo("CU");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDAD SATELITE")) {
                                request.setAddressCCHHTipo("CS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CH")) {
                                request.setAddressCCHHTipo("CO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COMITE")) {
                                request.setAddressCCHHTipo("CM");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDADELA")) {
                                request.setAddressCCHHTipo("CD");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("BARRIO")) {
                                request.setAddressCCHHTipo("BA");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("ASOC")) {
                                request.setAddressCCHHTipo("AS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("AGR")) {
                                request.setAddressCCHHTipo("AG");
                            } else if(normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COND")) {
                                request.setAddressCCHHTipo("CDM");
                            } else {
                                request.setAddressCCHHTipo(normalizadorModelVO.getTipo_urbanizacion());
                            }
                        }
                    }

                    if (normalizadorModelVO.getTipoVia() != null) {
                        if (!normalizadorModelVO.getTipoVia().isEmpty()) {
                            if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PSJ")) {
                                request.setAddressCalleAtis("PJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("AUTOPISTA")) {
                                request.setAddressCalleAtis("AU");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("SENDERO")) {
                                request.setAddressCalleAtis("SD");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CUESTA")) {
                                request.setAddressCalleAtis("CT");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CAMINO")) {
                                request.setAddressCalleAtis("CM");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PLAZUELA")) {
                                request.setAddressCalleAtis("PZ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PASEO")) {
                                request.setAddressCalleAtis("PO");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PZ")) {
                                request.setAddressCalleAtis("PL");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PSJ")) {
                                request.setAddressCalleAtis("PJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("OVALO")) {
                                request.setAddressCalleAtis("OV");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("ML")) {
                                request.setAddressCalleAtis("MA");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("GAL")) {
                                request.setAddressCalleAtis("GL");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CALLEJON")) {
                                request.setAddressCalleAtis("CJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CAR")) {
                                request.setAddressCalleAtis("CA");
                            }
                            else if(normalizadorModelVO.getTipoVia().equalsIgnoreCase("COND")) {
                                request.setAddressCalleAtis("CDM");
                            }else {
                                request.setAddressCalleAtis(normalizadorModelVO.getTipoVia());
                            }
                        }
                    }

                    if (normalizadorModelVO.getNombre_urbanizacion() != null) {
                        if (!normalizadorModelVO.getNombre_urbanizacion().isEmpty()) {
                            request.setAddressCCHHNombre(normalizadorModelVO.getNombre_urbanizacion());
                        }
                    }

                    if (normalizadorModelVO.getNombreVia() != null) {
                        if (!normalizadorModelVO.getNombreVia().isEmpty()) {
                            request.setAddressCalleNombre(normalizadorModelVO.getNombreVia());
                        } else {
                            request.setAddressCalleNombre(".");
                        }
                    }

                    if (request.getAddressViaCompTipo1() != null) {
                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("MZ")) {
                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(".");
                        }


                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");
                        }

                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");

                            request.setAddressViaCompTipo3(request.getAddressViaCompTipo3());
                            request.setAddressViaCompNombre3(request.getAddressViaCompNombre3() != null ? request.getAddressViaCompNombre3() : "0");
                        }
                    }

                    if (request.getAddressViaCompTipo1() != null) {
                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3("0");
                        }
                    }

                    if (request.getAddressViaCompTipo2() != null) {

                        if (request.getAddressViaCompTipo2().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(".");

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3("0");
                        }
                    }

                    if (normalizadorModelVO.getManzana() != null) {
                        if (!normalizadorModelVO.getManzana().isEmpty()) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(normalizadorModelVO.getManzana());

                        }

                    }

                    if (normalizadorModelVO.getLote() != null) {
                        if (!normalizadorModelVO.getLote().isEmpty()) {

                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(normalizadorModelVO.getLote());

                        }
                    }

                    if (normalizadorModelVO.getCuadra() != null) {
                        if (!normalizadorModelVO.getCuadra().isEmpty()) {

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3(normalizadorModelVO.getCuadra());

                        }
                    }


                    if (request.getAddressViaCompNombre1() != null) {
                        if (!request.getAddressViaCompNombre1().isEmpty()) {
                            request.setAddressViaCompTipo1("MZ");
                        }

                    }

                    if (request.getAddressViaCompNombre2() != null) {
                        if (!request.getAddressViaCompNombre2().isEmpty()) {
                            request.setAddressViaCompTipo2("LT");
                        }
                    }


                    if (request.getAddressViaCompNombre3() != null) {
                        if (!request.getAddressViaCompNombre3().isEmpty()) {
                            request.setAddressViaCompTipo3("CDR");
                        }
                    }

                    if (request.getAddressViaCompTipo1() == null) {
                        if (request.getAddressViaCompTipo2() != null) {
                            if (!request.getAddressViaCompTipo2().equalsIgnoreCase("MZ")) {
                                request.setAddressViaCompTipo1("MZ");
                                request.setAddressViaCompNombre1(".");
                            }
                        }
                    }

                    if (request.getAddressViaCompTipo2() == null) {
                        if (request.getAddressViaCompTipo1() != null) {
                            if (!request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                                request.setAddressViaCompTipo2("LT");
                                request.setAddressViaCompNombre2(".");
                            }
                        }
                    }


                    if (!normalizador) {
                        if (!type.equalsIgnoreCase(OrderConstants.ALTA_SVA)) {
                            return false;
                        }
                        if ((type.equalsIgnoreCase(OrderConstants.ALTA_SVA) && (tdpOrder.getOrderOperationNumber().equals(2)))) {
                            request = new AutomatizadorSaleRequestBody();

                            request.setClienteNumeroDoc(tdpOrder.getClientNumeroDoc());
                            request.setClienteTipoDoc(tdpOrder.getClientTipoDoc());
                            request.setUserAtis(tdpOrder.getUserAtis());
                            request.setUserFuerzaVenta(tdpOrder.getUserFuerzaVenta());
                            request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                            request.setProductoSVACodigo1(tdpOrder.getSvaCodigo1());
                            request.setProductoSVACodigo2(tdpOrder.getSvaCodigo2());
                            request.setProductoSVACodigo3(tdpOrder.getSvaCodigo3());
                            request.setProductoSVACodigo4(tdpOrder.getSvaCodigo4());
                            request.setProductoSVACodigo5(tdpOrder.getSvaCodigo5());
                            request.setProductoSVACodigo6(tdpOrder.getSvaCodigo6());
                            request.setProductoSVACodigo7(tdpOrder.getSvaCodigo7());
                            request.setProductoSVACodigo8(tdpOrder.getSvaCodigo8());
                            request.setProductoSVACodigo9(tdpOrder.getSvaCodigo9());
                            request.setProductoSVACodigo10(tdpOrder.getSvaCodigo10());

                            request.setOrderId(tdpOrder.getOrderId());

                            String dateOrderStr = "";
                            Date dateOrder = tdpOrder.getOrderFechaHora();

                            if (dateOrder != null) {
                                dateOrderStr = sdfDatetime.format(dateOrder);
                            }
                            if (dateOrderStr != null) {
                                request.setOrderFecha(dateOrderStr);
                            }

                            request.setOrderParqueTelefono(tdpOrder.getOrderParqueTelefono());
                            request.setOrderOperacionComercial("2");


                            //Agregando Campos necesarios para que automatizador complete sus reportes by Valdemar

                            if (tdpOrder.getClientNombre() != null) {
                                request.setClienteNombre(tdpOrder.getClientNombre());
                            }

                            if (tdpOrder.getClientApellidoPaterno() != null) {
                                if (!tdpOrder.getClientApellidoPaterno().isEmpty()) {
                                    request.setClienteApellidoPaterno(tdpOrder.getClientApellidoPaterno());
                                } else {
                                    request.setClienteApellidoPaterno(".");
                                }
                            } else {
                                request.setClienteApellidoPaterno(".");
                            }

                            if (tdpOrder.getClientApellidoMaterno() != null) {
                                if (!tdpOrder.getClientApellidoMaterno().isEmpty()) {
                                    request.setClienteApellidoMaterno(tdpOrder.getClientApellidoMaterno());
                                } else {
                                    request.setClienteApellidoMaterno(".");
                                }
                            } else {
                                request.setClienteApellidoMaterno(".");
                            }

                            request.setClienteTelefono1(tdpOrder.getClientTelefono1());
                            request.setClienteEmail(tdpOrder.getClientEmail());
                            request.setOrderContrato(tdpOrder.getOrderContrato() != null ? tdpOrder.getOrderContrato() : "");

                            request.setOrderZonal(tdpOrder.getUserZonal() != null ? tdpOrder.getUserZonal() : "");

                            request.setUserDNI(tdpOrder.getUserDni());

                            request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                            request.setOrderModoVenta(origenCode);

                            if (tdpOrder.getUserFuerzaVenta() != null) {
                                request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
                            }

                        }
                    }

                } else if(normalizadorModelVO.getTipo().equalsIgnoreCase("2")){
                    //OCT_TIP_CMP
                    if (tdpOrder.getAddressViaCompTip3() != null) {
                        request.setAddressViaCompTipo3(tdpOrder.getAddressViaCompTip3());
                    }
                    //OCT_TIP_DIR
                    if (tdpOrder.getAddressViaCompNom3() != null) {
                        request.setAddressViaCompNombre3(tdpOrder.getAddressViaCompNom3());

                    }
                    //NOV_TIP_CMP
                    if (tdpOrder.getAddressViaCompTip4() != null) {
                        request.setAddressViaCompTipo4(tdpOrder.getAddressViaCompTip4());
                    }
                    //NOV_TIP_DIR
                    if (tdpOrder.getAddressViaCompNom4() != null) {
                        request.setAddressViaCompNombre4(tdpOrder.getAddressViaCompNom4());
                    }
                    //DCO_TIP_CMP
                    if (tdpOrder.getAddressViaCompTip5() != null) {
                        request.setAddressViaCompTipo5(tdpOrder.getAddressViaCompTip5());
                    }
                    //DCO_TIP_DIR
                    if (tdpOrder.getAddressViaCompNom5() != null) {
                        request.setAddressViaCompNombre5(tdpOrder.getAddressViaCompNom5());
                    }

                }

                else{
                    if (normalizadorModelVO.getTipo_urbanizacion() != null) {
                        if (!normalizadorModelVO.getTipo_urbanizacion().isEmpty()) {
                            if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("URB")) {
                                request.setAddressCCHHTipo("UR");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("POBLADO")) {
                                request.setAddressCCHHTipo("PO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("VILLA MILITAR")) {
                                request.setAddressCCHHTipo("VM");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("URBANIZACION POPULAR")) {
                                request.setAddressCCHHTipo("UP");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("RES")) {
                                request.setAddressCCHHTipo("RS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("PPJJ")) {
                                request.setAddressCCHHTipo("PJ");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("PARCELA")) {
                                request.setAddressCCHHTipo("PA");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("LOT")) {
                                request.setAddressCCHHTipo("LO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("GRUPO")) {
                                request.setAddressCCHHTipo("GR");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("FUNDO")) {
                                request.setAddressCCHHTipo("FU");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COOP")) {
                                request.setAddressCCHHTipo("CV");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDAD UNIVERSITARIA")) {
                                request.setAddressCCHHTipo("CU");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDAD SATELITE")) {
                                request.setAddressCCHHTipo("CS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CH")) {
                                request.setAddressCCHHTipo("CO");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COMITE")) {
                                request.setAddressCCHHTipo("CM");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("CIUDADELA")) {
                                request.setAddressCCHHTipo("CD");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("BARRIO")) {
                                request.setAddressCCHHTipo("BA");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("ASOC")) {
                                request.setAddressCCHHTipo("AS");
                            } else if (normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("AGR")) {
                                request.setAddressCCHHTipo("AG");

                            }
                            else if(normalizadorModelVO.getTipo_urbanizacion().equalsIgnoreCase("COND")) {
                                request.setAddressCCHHTipo("CDM");
                            }else {
                                request.setAddressCCHHTipo(normalizadorModelVO.getTipo_urbanizacion());
                            }
                        }
                    }

                    if (normalizadorModelVO.getTipoVia() != null) {
                        if (!normalizadorModelVO.getTipoVia().isEmpty()) {
                            if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PSJ")) {
                                request.setAddressCalleAtis("PJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("AUTOPISTA")) {
                                request.setAddressCalleAtis("AU");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("SENDERO")) {
                                request.setAddressCalleAtis("SD");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CUESTA")) {
                                request.setAddressCalleAtis("CT");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CAMINO")) {
                                request.setAddressCalleAtis("CM");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PLAZUELA")) {
                                request.setAddressCalleAtis("PZ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PASEO")) {
                                request.setAddressCalleAtis("PO");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PZ")) {
                                request.setAddressCalleAtis("PL");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("PSJ")) {
                                request.setAddressCalleAtis("PJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("OVALO")) {
                                request.setAddressCalleAtis("OV");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("ML")) {
                                request.setAddressCalleAtis("MA");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("GAL")) {
                                request.setAddressCalleAtis("GL");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CALLEJON")) {
                                request.setAddressCalleAtis("CJ");
                            } else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("CAR")) {
                                request.setAddressCalleAtis("CA");
                            }
                             else if (normalizadorModelVO.getTipoVia().equalsIgnoreCase("COND")) {
                                request.setAddressCalleAtis("CDM");
                            }
                            else {
                                request.setAddressCalleAtis(normalizadorModelVO.getTipoVia());
                            }
                        }
                    }

                    if (normalizadorModelVO.getNombre_urbanizacion() != null) {
                        if (!normalizadorModelVO.getNombre_urbanizacion().isEmpty()) {
                            request.setAddressCCHHNombre(normalizadorModelVO.getNombre_urbanizacion());
                        }
                    }

                    if (normalizadorModelVO.getNombreVia() != null) {
                        if (!normalizadorModelVO.getNombreVia().isEmpty()) {
                            request.setAddressCalleNombre(normalizadorModelVO.getNombreVia());
                        } else {
                            request.setAddressCalleNombre(".");
                        }
                    }

                    if (request.getAddressViaCompTipo1() != null) {
                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("MZ")) {
                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(".");
                        }


                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");
                        }

                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");

                            request.setAddressViaCompTipo3(request.getAddressViaCompTipo3());
                            request.setAddressViaCompNombre3(request.getAddressViaCompNombre3() != null ? request.getAddressViaCompNombre3() : "0");
                        }
                    }

                    if (request.getAddressViaCompTipo1() != null) {
                        if (request.getAddressViaCompTipo1().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(".");

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3("0");
                        }
                    }

                    if (request.getAddressViaCompTipo2() != null) {

                        if (request.getAddressViaCompTipo2().equalsIgnoreCase("CDR")) {

                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(".");

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3("0");
                        }
                    }

                    if (normalizadorModelVO.getManzana() != null) {
                        if (!normalizadorModelVO.getManzana().isEmpty()) {

                            request.setAddressViaCompTipo1("MZ");
                            request.setAddressViaCompNombre1(normalizadorModelVO.getManzana());

                        }

                    }

                    if (normalizadorModelVO.getLote() != null) {
                        if (!normalizadorModelVO.getLote().isEmpty()) {

                            request.setAddressViaCompTipo2("LT");
                            request.setAddressViaCompNombre2(normalizadorModelVO.getLote());

                        }
                    }

                    if (normalizadorModelVO.getCuadra() != null) {
                        if (!normalizadorModelVO.getCuadra().isEmpty()) {

                            request.setAddressViaCompTipo3("CDR");
                            request.setAddressViaCompNombre3(normalizadorModelVO.getCuadra());

                        }
                    }


                    if (request.getAddressViaCompNombre1() != null) {
                        if (!request.getAddressViaCompNombre1().isEmpty()) {
                            request.setAddressViaCompTipo1("MZ");
                        }

                    }

                    if (request.getAddressViaCompNombre2() != null) {
                        if (!request.getAddressViaCompNombre2().isEmpty()) {
                            request.setAddressViaCompTipo2("LT");
                        }
                    }


                    if (request.getAddressViaCompNombre3() != null) {
                        if (!request.getAddressViaCompNombre3().isEmpty()) {
                            request.setAddressViaCompTipo3("CDR");
                        }
                    }

                    if (request.getAddressViaCompTipo1() == null) {
                        if (request.getAddressViaCompTipo2() != null) {
                            if (!request.getAddressViaCompTipo2().equalsIgnoreCase("MZ")) {
                                request.setAddressViaCompTipo1("MZ");
                                request.setAddressViaCompNombre1(".");
                            }
                        }
                    }

                    if (request.getAddressViaCompTipo2() == null) {
                        if (request.getAddressViaCompTipo1() != null) {
                            if (!request.getAddressViaCompTipo1().equalsIgnoreCase("LT")) {
                                request.setAddressViaCompTipo2("LT");
                                request.setAddressViaCompNombre2(".");
                            }
                        }
                    }


                    if (!normalizador) {
                        if (!type.equalsIgnoreCase(OrderConstants.ALTA_SVA)) {
                            return false;
                        }
                        if ((type.equalsIgnoreCase(OrderConstants.ALTA_SVA) && (tdpOrder.getOrderOperationNumber().equals(2)))) {
                            request = new AutomatizadorSaleRequestBody();

                            request.setClienteNumeroDoc(tdpOrder.getClientNumeroDoc());
                            request.setClienteTipoDoc(tdpOrder.getClientTipoDoc());
                            request.setUserAtis(tdpOrder.getUserAtis());
                            request.setUserFuerzaVenta(tdpOrder.getUserFuerzaVenta());
                            request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                            request.setProductoSVACodigo1(tdpOrder.getSvaCodigo1());
                            request.setProductoSVACodigo2(tdpOrder.getSvaCodigo2());
                            request.setProductoSVACodigo3(tdpOrder.getSvaCodigo3());
                            request.setProductoSVACodigo4(tdpOrder.getSvaCodigo4());
                            request.setProductoSVACodigo5(tdpOrder.getSvaCodigo5());
                            request.setProductoSVACodigo6(tdpOrder.getSvaCodigo6());
                            request.setProductoSVACodigo7(tdpOrder.getSvaCodigo7());
                            request.setProductoSVACodigo8(tdpOrder.getSvaCodigo8());
                            request.setProductoSVACodigo9(tdpOrder.getSvaCodigo9());
                            request.setProductoSVACodigo10(tdpOrder.getSvaCodigo10());

                            request.setOrderId(tdpOrder.getOrderId());

                            String dateOrderStr = "";
                            Date dateOrder = tdpOrder.getOrderFechaHora();

                            if (dateOrder != null) {
                                dateOrderStr = sdfDatetime.format(dateOrder);
                            }
                            if (dateOrderStr != null) {
                                request.setOrderFecha(dateOrderStr);
                            }

                            request.setOrderParqueTelefono(tdpOrder.getOrderParqueTelefono());
                            request.setOrderOperacionComercial("2");


                            //Agregando Campos necesarios para que automatizador complete sus reportes by Valdemar

                            if (tdpOrder.getClientNombre() != null) {
                                request.setClienteNombre(tdpOrder.getClientNombre());
                            }

                            if (tdpOrder.getClientApellidoPaterno() != null) {
                                if (!tdpOrder.getClientApellidoPaterno().isEmpty()) {
                                    request.setClienteApellidoPaterno(tdpOrder.getClientApellidoPaterno());
                                } else {
                                    request.setClienteApellidoPaterno(".");
                                }
                            } else {
                                request.setClienteApellidoPaterno(".");
                            }

                            if (tdpOrder.getClientApellidoMaterno() != null) {
                                if (!tdpOrder.getClientApellidoMaterno().isEmpty()) {
                                    request.setClienteApellidoMaterno(tdpOrder.getClientApellidoMaterno());
                                } else {
                                    request.setClienteApellidoMaterno(".");
                                }
                            } else {
                                request.setClienteApellidoMaterno(".");
                            }

                            request.setClienteTelefono1(tdpOrder.getClientTelefono1());
                            request.setClienteEmail(tdpOrder.getClientEmail());
                            request.setOrderContrato(tdpOrder.getOrderContrato() != null ? tdpOrder.getOrderContrato() : "");

                            request.setOrderZonal(tdpOrder.getUserZonal() != null ? tdpOrder.getUserZonal() : "");

                            request.setUserDNI(tdpOrder.getUserDni());

                            request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());

                            request.setOrderModoVenta(origenCode);

                            if (tdpOrder.getUserFuerzaVenta() != null) {
                                request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
                            }


                        }
                    }
                }
            }


            if(request.getAddressCCHHTipo() != null){
                if(request.getAddressCCHHTipo().isEmpty()){
                    request.setAddressCCHHTipo("");
                    request.setAddressCCHHNombre("");
                }
                if(request.getAddressCCHHTipo().equalsIgnoreCase(".")){
                    request.setAddressCCHHTipo("");
                    request.setAddressCCHHNombre("");
                }
            }


            //Fin validación con tabla normalizador

            boolean result = true;

            automatizadorOrderRepository.updateAutomatizadorOrder("ERR_CIP",orderId);
            //guardar el request
            AutomatizadorSaleService automatizadorSaleService = poblarRequest(request);

            automatizadorRequestSaleService.save(automatizadorSaleService);

            return result;
        }catch (Exception e){
            return false;
        }



    }





    public AutomatizadorSaleService poblarRequest(AutomatizadorSaleRequestBody request){
        AutomatizadorSaleService automatizadorSaleService = new AutomatizadorSaleService();

        automatizadorSaleService.setNom_ds(request.getClienteNombre());
        automatizadorSaleService.setPri_ape_ds(request.getClienteApellidoPaterno());
        automatizadorSaleService.setSeg_ape_ds(request.getClienteApellidoMaterno());
        automatizadorSaleService.setNum_doc_cli_cd(request.getClienteNumeroDoc());
        automatizadorSaleService.setDig_ctl_doc_cd(request.getClienteRucDigitoControl());
        automatizadorSaleService.setTip_doc_cd(request.getClienteTipoDoc());
        automatizadorSaleService.setTel_pri_cot_cd(request.getClienteTelefono1());
        automatizadorSaleService.setTel_seg_cot_cd(request.getClienteTelefono2());
        automatizadorSaleService.setCod_act_eco_cd(request.getClienteRucActividadCodigo());
        automatizadorSaleService.setCod_sac_eco_cd(request.getClienteRucSubActividadCodigo());
        automatizadorSaleService.setTip_cal_ati_cd(request.getAddressCalleAtis());
        automatizadorSaleService.setNom_cal_ds(request.getAddressCalleNombre());
        automatizadorSaleService.setNum_cal_nu(request.getAddressCalleNumero());
        automatizadorSaleService.setCod_cnj_hbl_cd(request.getAddressCCHHCodigo());
        automatizadorSaleService.setTip_cnj_hbl_cd(request.getAddressCCHHTipo());
        automatizadorSaleService.setNom_cnj_hbl_no(request.getAddressCCHHNombre());
        automatizadorSaleService.setPri_tip_cmp_cd(request.getAddressCCHHCompTipo1());
        automatizadorSaleService.setPri_cmp_dir_ds(request.getAddressCCHHCompNombre1());
        automatizadorSaleService.setSeg_tip_cmp_cd(request.getAddressCCHHCompTipo2());
        automatizadorSaleService.setSeg_cmp_dir_ds(request.getAddressCCHHCompNombre2());
        automatizadorSaleService.setTrc_tip_cmp_cd(request.getAddressCCHHCompTipo3());
        automatizadorSaleService.setTrc_cmp_dir_ds(request.getAddressCCHHCompNombre3());
        automatizadorSaleService.setCao_tip_cmp_cd(request.getAddressCCHHCompTipo4());
        automatizadorSaleService.setCao_cmp_dir_ds(request.getAddressCCHHCompNombre4());
        automatizadorSaleService.setQui_tip_cmp_cd(request.getAddressCCHHCompTipo5());
        automatizadorSaleService.setQui_cmp_dir_ds(request.getAddressCCHHCompNombre5());
        automatizadorSaleService.setSet_tip_cmp_cd(request.getAddressViaCompTipo1());
        automatizadorSaleService.setSet_cmp_dir_ds(request.getAddressViaCompNombre1());
        automatizadorSaleService.setSpt_tip_cmp_cd(request.getAddressViaCompTipo2());
        automatizadorSaleService.setSpt_cmp_dir_ds(request.getAddressViaCompNombre2());
        automatizadorSaleService.setOct_tip_cmp_cd(request.getAddressViaCompTipo3());
        automatizadorSaleService.setOct_cmp_dir_ds(request.getAddressViaCompNombre3());
        automatizadorSaleService.setNov_tip_cmp_cd(request.getAddressViaCompTipo4());
        automatizadorSaleService.setNov_cmp_dir_ds(request.getAddressViaCompNombre4());
        automatizadorSaleService.setDco_tip_cmp_cd(request.getAddressViaCompTipo5());
        automatizadorSaleService.setDco_cmp_dir_ds(request.getAddressViaCompNombre5());
        automatizadorSaleService.setMsx_cbr_voi_ges_in(request.getOrderGisXDSL());
        automatizadorSaleService.setMsx_cbr_voi_gis_in(request.getOrderGisHFC());
        automatizadorSaleService.setMsx_ind_snl_gis_cd(request.getOrderGisTipoSenial());
        automatizadorSaleService.setMsx_ind_gpo_gis_cd(request.getOrderGisGPON());
        automatizadorSaleService.setCod_fza_ven_cd(request.getUserAtis());
        automatizadorSaleService.setCod_fza_gen_cd(request.getUserFuerzaVenta());
        automatizadorSaleService.setCod_cnl_ven_cd(request.getUserCanalCodigo());
        automatizadorSaleService.setCod_ind_sen_cms(request.getProductoSenial());
        automatizadorSaleService.setCod_cab_cms(request.getProductoCabecera());
        automatizadorSaleService.setId_cod_pro_cd(request.getProductoCodigo());
        automatizadorSaleService.setPs_adm_dep_1(request.getProductoPSAdmin1());
        automatizadorSaleService.setPs_adm_dep_2(request.getProductoPSAdmin2());
        automatizadorSaleService.setPs_adm_dep_3(request.getProductoPSAdmin3());
        automatizadorSaleService.setPs_adm_dep_4(request.getProductoPSAdmin4());
        automatizadorSaleService.setId_cod_sva_cd_1(request.getProductoSVACodigo1());
        automatizadorSaleService.setId_cod_sva_cd_2(request.getProductoSVACodigo2());
        automatizadorSaleService.setId_cod_sva_cd_3(request.getProductoSVACodigo3());
        automatizadorSaleService.setId_cod_sva_cd_4(request.getProductoSVACodigo4());
        automatizadorSaleService.setId_cod_sva_cd_5(request.getProductoSVACodigo5());
        automatizadorSaleService.setId_cod_sva_cd_6(request.getProductoSVACodigo6());
        automatizadorSaleService.setId_cod_sva_cd_7(request.getProductoSVACodigo7());
        automatizadorSaleService.setId_cod_sva_cd_8(request.getProductoSVACodigo8());
        automatizadorSaleService.setId_cod_sva_cd_9(request.getProductoSVACodigo9());
        automatizadorSaleService.setId_cod_sva_cd_10(request.getProductoSVACodigo10());
        automatizadorSaleService.setCodvtappcd(request.getOrderId());
        automatizadorSaleService.setFec_vta_ff(request.getOrderFecha());
        automatizadorSaleService.setNum_ide_tlf(request.getOrderParqueTelefono());
        automatizadorSaleService.setCod_cli_ext_cd(request.getClienteCMSCodigo());
        automatizadorSaleService.setTip_trx_cmr_cd(request.getOrderOperacionComercial());
        automatizadorSaleService.setCod_srv_cms_cd(request.getClienteCMSServicio());
        automatizadorSaleService.setFec_nac_ff(request.getClienteNacimiento());
        automatizadorSaleService.setCod_sex_cd(request.getClienteSexo());
        automatizadorSaleService.setCod_est_civ_cd(request.getClienteCivil());
        automatizadorSaleService.setDes_mai_cli_ds(request.getClienteEmail());
        automatizadorSaleService.setCod_are_te1_cd(request.getClienteTelefono1Area());
        automatizadorSaleService.setCod_are_te2_cd(request.getClienteTelefono2Area());
        automatizadorSaleService.setCod_cic_fac_cd(request.getClienteCMSFactura());
        automatizadorSaleService.setCod_dep_cd(request.getAddressDepartamentoCodigo());
        automatizadorSaleService.setCod_prv_cd(request.getAddressProvinciaCodigo());
        automatizadorSaleService.setCod_dis_cd(request.getAddressDistritoCodigo());
        automatizadorSaleService.setCod_fac_tec_cd(request.getOrderGisNTLT());
        automatizadorSaleService.setNum_cod_x_cd(request.getOrderGPSX());
        automatizadorSaleService.setNum_cod_y_cd(request.getOrderGPSY());
        automatizadorSaleService.setCod_ven_cms_cd(request.getUserCSMPuntoVenta());
        automatizadorSaleService.setCod_sve_cms_cd(request.getUserCMS());
        automatizadorSaleService.setNom_ven_ds(request.getUserNombre());
        automatizadorSaleService.setRef_dir_ent_ds(request.getAddressReferencia());
        automatizadorSaleService.setCod_tip_cal_cd(request.getOrderCallTipo());
        automatizadorSaleService.setDes_tip_cal_cd(request.getOrderCallDesc());
        automatizadorSaleService.setTip_mod_equ_cd(request.getOrderModelo());
        automatizadorSaleService.setNum_doc_ven_cd(request.getUserDNI());
        automatizadorSaleService.setNum_tel_ven_ds(request.getUserTelefono());
        automatizadorSaleService.setInd_env_con_cd(request.getOrderContrato());
        automatizadorSaleService.setInd_id_gra_cd(request.getOrderIDGrabacion());
        automatizadorSaleService.setMod_ace_ven_cd(request.getOrderModalidadAcep());
        automatizadorSaleService.setDet_can_ven_ds(request.getOrderEntidad());
        automatizadorSaleService.setCod_reg_cd(request.getOrderRegion());
        automatizadorSaleService.setCod_cip_cd(request.getOrderCIP());
        automatizadorSaleService.setCod_exp_cd(request.getOrderExperto());
        automatizadorSaleService.setCod_zon_ven_cd(request.getOrderZonal());
        automatizadorSaleService.setInd_web_par_cd(request.getOrderWebParental());
        automatizadorSaleService.setCod_stc(request.getOrderCodigoSTC6I());
        automatizadorSaleService.setDes_nac_ds(request.getClienteNacionalidad());
        automatizadorSaleService.setCod_mod_ven_cd(request.getOrderModoVenta());
        automatizadorSaleService.setCan_equ_ven(request.getOrderCantidadEquipos());

        return automatizadorSaleService;
    }

    //region SeteoGeneral
    public AutomatizadorSaleRequestBody SetGeneralAutomatizador(AutomatizadorSaleRequestBody request,
                                                                TdpOrder tdpOrder,
                                                                SimpleDateFormat sdfDatetime,
                                                                boolean isPaquetizacion) {


        //Se aplicará lógica para evitar campos vacíos en automatizador
        /*
            Tipo de conjunto habitacional => getAddressCchhTipo
            Nombre de conjunto Habitacional =>  getAddressCchhNombre
            Tipo de calle ATIS => getAddressCalleAtis
            Nombre de calle => getAddressCalleNombre
            Número de calle => getAddressCalleNumero
         */
        //aplicar switch de cambio de raúl, sprint 27 cambio de lógica

        List<String> automatizadorAux = automatizadorExtraValidacion();



        if (((tdpOrder.getAddressCchhTipo() != null)
                && (tdpOrder.getAddressCchhNombre() != null)
                && (tdpOrder.getAddressCalleAtis() != null)
                && (tdpOrder.getAddressCalleNombre() != null)
                && (tdpOrder.getAddressCalleNumero() != null)) &&
                ((tdpOrder.getAddressCchhTipo() != "")
                        && (tdpOrder.getAddressCchhNombre() != "")
                        && (tdpOrder.getAddressCalleAtis() != "")
                        && (tdpOrder.getAddressCalleNombre() != "")
                        && (tdpOrder.getAddressCalleNumero() != ""))
                ) {
            System.out.println("Campos completos en la lógica de direcciones, no entra en la contingencia.");
        } else {
            //no tiene conjunto habitacional ni nombre de conjunto habitacional
            if (tdpOrder.getAddressCchhTipo() == null && tdpOrder.getAddressCchhNombre() == null) {
                //tiene tipo de vía(calle) y nombre de vía(calle)
                if (tdpOrder.getAddressCalleAtis() != null && tdpOrder.getAddressCalleNombre() != null) {
                    //no tiene número de puerta
                    if (tdpOrder.getAddressCalleNumero() == null || tdpOrder.getAddressCalleNumero().isEmpty()) {
                        //número de puerta asignale un "."
                        tdpOrder.setAddressCalleNumero("0");
                    }

                }
                //tiene tipo de vía(calle) y pero no nombre de vía(calle)
                else if (tdpOrder.getAddressCalleAtis() != null && tdpOrder.getAddressCalleNombre() == null) {
                    //seteamos nombre vía(calle)
                    tdpOrder.setAddressCalleNombre(".");
                    if (tdpOrder.getAddressCalleNumero() == null || tdpOrder.getAddressCalleNumero().isEmpty()) {
                        //número de puerta asignale un "."
                        tdpOrder.setAddressCalleNumero("0");
                    }

                }
                else if (tdpOrder.getAddressCalleAtis() == null || tdpOrder.getAddressCalleAtis().isEmpty()) {
                    tdpOrder.setAddressCalleAtis("CL");

                    //si se tiene manzana y no se tiene lote se envía en lote un "."
                    if (tdpOrder.getAddressViaCompTip1() != null || tdpOrder.getAddressViaCompTip1() != "") {
                        //si no se tiene lote
                        if (tdpOrder.getAddressViaCompNom2() == null || tdpOrder.getAddressViaCompNom2() == "") {
                            tdpOrder.setAddressViaCompNom2(".");
                        }
                    }
                    //si se tiene lote pero no se tiene manzana se envia en manzana un "."
                    if (tdpOrder.getAddressViaCompTip2() != null || tdpOrder.getAddressViaCompTip2() != "") {
                        if (tdpOrder.getAddressViaCompTip1() == null || tdpOrder.getAddressViaCompTip1() == "") {
                            tdpOrder.setAddressViaCompNom1(".");
                        }
                    }

                }
                //ni no tiene nombre vía (calle)
                if (tdpOrder.getAddressCalleNombre() == null || tdpOrder.getAddressCalleNombre().isEmpty()) {
                    tdpOrder.setAddressCalleNombre(".");
                }
                if (tdpOrder.getAddressCalleNumero() == null || tdpOrder.getAddressCalleNumero().isEmpty()) {
                    tdpOrder.setAddressCalleNumero("0");
                }
                //si tene conjunto habitacional y nombre de conjunto habitacional
            } else if (tdpOrder.getAddressCchhTipo() != null && tdpOrder.getAddressCchhNombre() != null) {
                //tiene tipo de vía(calle) y nombre de vía(calle)
                if (tdpOrder.getAddressCalleAtis() != null && tdpOrder.getAddressCalleNombre() != null) {
                    //no tiene número de puerta
                    if (tdpOrder.getAddressCalleNumero() == null || tdpOrder.getAddressCalleNumero().isEmpty()) {
                        //número de puerta asignale un "."
                        tdpOrder.setAddressCalleNumero("0");
                    }
                } else {
                    //Si no tiene tipo de via (calle)
                    if (tdpOrder.getAddressCalleAtis() == null || tdpOrder.getAddressCalleAtis().isEmpty()) {
                        tdpOrder.setAddressCalleAtis("CL");

                        //si se tiene manzana y no se tiene lote se envía en lote un "."
                        if (tdpOrder.getAddressViaCompTip1() != null || tdpOrder.getAddressViaCompTip1() != "") {
                            //si no se tiene lote
                            if (tdpOrder.getAddressViaCompNom2() == null || tdpOrder.getAddressViaCompNom2() == "") {
                                tdpOrder.setAddressViaCompNom2(".");
                            }
                        }
                        //si se tiene lote pero no se tiene manzana se envia en manzana un "."
                        if (tdpOrder.getAddressViaCompTip2() != null || tdpOrder.getAddressViaCompTip2() != "") {
                            if (tdpOrder.getAddressViaCompTip1() == null || tdpOrder.getAddressViaCompTip1() == "") {
                                tdpOrder.setAddressViaCompNom1(".");
                            }
                        }

                    }
                    //ni no tiene nombre vía (calle)
                    if (tdpOrder.getAddressCalleNombre() == null) {
                        tdpOrder.setAddressCalleNombre(".");
                    }

                    if(tdpOrder.getAddressCalleNombre() != null){
                        if(tdpOrder.getAddressCalleNombre().isEmpty()){
                            tdpOrder.setAddressCalleNombre(".");
                        }
                    }


                    if (tdpOrder.getAddressCalleNumero() == null) {
                        tdpOrder.setAddressCalleNumero("0");
                    }

                    if(tdpOrder.getAddressCalleNumero() != null){
                        if(tdpOrder.getAddressCalleNumero().isEmpty()){
                            tdpOrder.setAddressCalleNumero("0");
                        }
                    }

                }
            }
            if(tdpOrder.getAddressViaCompNom1()!= null){
                if(tdpOrder.getAddressViaCompNom1().isEmpty()){
                    if(tdpOrder.getAddressViaCompNom1().equalsIgnoreCase(".")){
                        tdpOrder.setAddressViaCompTip1("MZ");
                    }
                }
            }

            if(tdpOrder.getAddressViaCompNom2()!= null){
                if(tdpOrder.getAddressViaCompNom2().isEmpty()){
                    if(tdpOrder.getAddressViaCompNom2().equalsIgnoreCase(".")){
                        tdpOrder.setAddressViaCompTip2("LT");
                    }
                }
            }

            if (tdpOrder.getAddressCalleNumero() == null) {

                tdpOrder.setAddressCalleNumero("0");
            }
            if(tdpOrder.getAddressCalleNumero() != null){
                if(tdpOrder.getAddressCalleNumero().isEmpty()){
                    tdpOrder.setAddressCalleNumero("0");
                }
            }

            //ni no tiene nombre vía (calle)
            if (tdpOrder.getAddressCalleNombre() == null) {
                tdpOrder.setAddressCalleNombre(".");
            }

            if(tdpOrder.getAddressCalleNombre() !=null){
                if(tdpOrder.getAddressCalleNombre().isEmpty()){
                    tdpOrder.setAddressCalleNombre(".");
                }
            }
        }


        if (tdpOrder.getClientNombre() != null) {
            request.setClienteNombre(tdpOrder.getClientNombre());
        }

        if (tdpOrder.getClientApellidoPaterno() != null ) {
            if(!tdpOrder.getClientApellidoPaterno().isEmpty()){
                request.setClienteApellidoPaterno(tdpOrder.getClientApellidoPaterno());
            }else{
                request.setClienteApellidoPaterno(".");
            }
        } else {
            request.setClienteApellidoPaterno(".");
        }

        if (tdpOrder.getClientApellidoMaterno() != null) {
            if(!tdpOrder.getClientApellidoMaterno().isEmpty()){
                request.setClienteApellidoMaterno(tdpOrder.getClientApellidoMaterno());
            } else {
                request.setClienteApellidoMaterno(".");
            }
        } else {
            request.setClienteApellidoMaterno(".");
        }

        if (tdpOrder.getClientNumeroDoc() != null) {
            request.setClienteNumeroDoc(tdpOrder.getClientNumeroDoc());
        } else if ((tdpOrder.getClientNumeroDoc() == null)) {
            return null;
        }
        //TODO: Aplicar validacion
        if (tdpOrder.getClientTipoDoc() != null) {
            switch (tdpOrder.getClientTipoDoc().toUpperCase().trim()) {
                case "DNI":
                    request.setClienteTipoDoc("DNI");
                    break;
                case "CE":
                    request.setClienteTipoDoc("CEX");
                    break;
                case "PAS":
                    request.setClienteTipoDoc("CEX");
                    break;
                case "RUC":
                    request.setClienteTipoDoc("RUC");
                    break;
                default:
                    request.setClienteTipoDoc("NDE");
                    break;
            }
        } else {
            return null;
        }

        Boolean telefonoValidate = Boolean.parseBoolean(automatizadorAux.get(6));

        if(telefonoValidate){
            if (tdpOrder.getClientTelefono1() != null) {
                if(!tdpOrder.getClientTelefono1().isEmpty()){
                    request.setClienteTelefono1(tdpOrder.getClientTelefono1());
                } else{
                    return null;
                }
            }else{
                return null;
            }
        }else{
            if (tdpOrder.getClientTelefono1() != null) {
                request.setClienteTelefono1(tdpOrder.getClientTelefono1());
            }
        }


        if (tdpOrder.getClientTelefono2() != null) {
            request.setClienteTelefono2(tdpOrder.getClientTelefono2());
        }

        if (tdpOrder.getAddressCalleAtis() != null) {

            if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("PSJ")){
                request.setAddressCalleAtis("PJ");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("AUTOPISTA")) {
                request.setAddressCalleAtis("AU");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("SENDERO")) {
                request.setAddressCalleAtis("SD");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("CUESTA")) {
                request.setAddressCalleAtis("CT");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("CAMINO")) {
                request.setAddressCalleAtis("CM");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("PLAZUELA")) {
                request.setAddressCalleAtis("PZ");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("PASEO")) {
                request.setAddressCalleAtis("PO");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("PZ")) {
                request.setAddressCalleAtis("PL");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("PSJ")) {
                request.setAddressCalleAtis("PJ");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("OVALO")) {
                request.setAddressCalleAtis("OV");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("ML")) {
                request.setAddressCalleAtis("MA");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("GAL")) {
                request.setAddressCalleAtis("GL");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("CALLEJON")) {
                request.setAddressCalleAtis("CJ");
            }
            else if(tdpOrder.getAddressCalleAtis().equalsIgnoreCase("CAR")) {
                request.setAddressCalleAtis("CA");
            }
            else if (tdpOrder.getAddressCalleAtis().equalsIgnoreCase("COND")) {
                request.setAddressCalleAtis("CDM");
            }
            else{
                request.setAddressCalleAtis(tdpOrder.getAddressCalleAtis());
            }

        }

        if (tdpOrder.getAddressCalleNombre() != null) {
            request.setAddressCalleNombre(tdpOrder.getAddressCalleNombre());
        }

        if (tdpOrder.getAddressCalleNumero() != null) {
            request.setAddressCalleNumero(tdpOrder.getAddressCalleNumero());
        } else {
            request.setAddressCalleNumero("0");
        }

        if (tdpOrder.getAddressCchhTipo() != null) {
            if(!tdpOrder.getAddressCchhTipo().isEmpty()){

                if (tdpOrder.getAddressCchhTipo().equalsIgnoreCase("URB")) {
                    request.setAddressCCHHTipo("UR");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("POBLADO")) {
                    request.setAddressCCHHTipo("PO");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("VILLA MILITAR")) {
                    request.setAddressCCHHTipo("VM");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("URBANIZACION POPULAR")) {
                    request.setAddressCCHHTipo("UP");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("RES")) {
                    request.setAddressCCHHTipo("RS");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("PPJJ")) {
                    request.setAddressCCHHTipo("PJ");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("PARCELA")) {
                    request.setAddressCCHHTipo("PA");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("LOT")) {
                    request.setAddressCCHHTipo("LO");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("GRUPO")) {
                    request.setAddressCCHHTipo("GR");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("FUNDO")) {
                    request.setAddressCCHHTipo("FU");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("COOP")) {
                    request.setAddressCCHHTipo("CV");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("CIUDAD UNIVERSITARIA")) {
                    request.setAddressCCHHTipo("CU");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("CIUDAD SATELITE")) {
                    request.setAddressCCHHTipo("CS");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("CH")) {
                    request.setAddressCCHHTipo("CO");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("COMITE")) {
                    request.setAddressCCHHTipo("CM");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("CIUDADELA")) {
                    request.setAddressCCHHTipo("CD");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("BARRIO")) {
                    request.setAddressCCHHTipo("BA");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("ASOC")) {
                    request.setAddressCCHHTipo("AS");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("AGR")) {
                    request.setAddressCCHHTipo("AG");
                }
                else if(tdpOrder.getAddressCchhTipo().equalsIgnoreCase("COND")) {
                    request.setAddressCCHHTipo("CDM");
                }
                else {
                    request.setAddressCCHHTipo(tdpOrder.getAddressCchhTipo());
                }


            }else{
                request.setAddressCCHHTipo(".");
            }
        }else{
            request.setAddressCCHHTipo(".");
        }

        if (tdpOrder.getAddressCchhNombre() != null) {
            if(!tdpOrder.getAddressCchhNombre().isEmpty()){
                request.setAddressCCHHNombre(tdpOrder.getAddressCchhNombre());
            }else{
                request.setAddressCCHHNombre(".");
            }
        }else{
            request.setAddressCCHHNombre(".");
        }

        if (tdpOrder.getOrderGisCable() != null) {
            request.setOrderGisTipoSenial(tdpOrder.getOrderGisCable());
        } else {
            //return null;
        }

        if (tdpOrder.getOrderGisGpon() != null) {
            if(!tdpOrder.getOrderGisGpon().isEmpty()){
                request.setOrderGisGPON(tdpOrder.getOrderGisGpon());
            } else {
                request.setOrderGisGPON("N");
            }
        } else {
            request.setOrderGisGPON("N");
        }

        if (tdpOrder.getProductCodigo() != null) {
            if(!tdpOrder.getProductCodigo().isEmpty()){
                request.setProductoCodigo(tdpOrder.getProductCodigo());
            }else {
                // return null;
            }
        } else {
            //return null;
        }

        if (tdpOrder.getOrderId() != null) {
            request.setOrderId(tdpOrder.getOrderId());
        } else {
            return null;
        }

        String dateOrderStr = "";
        Date dateOrder = tdpOrder.getOrderFechaHora();
        if (dateOrder != null) {
            dateOrderStr = sdfDatetime.format(dateOrder);
        }
        if (dateOrderStr != null) {
            request.setOrderFecha(dateOrderStr);
        } else {
            return null;
        }

        if (tdpOrder.getOrderOperationNumber() != null) {
            if(tdpOrder.getOrderOperationNumber().equals(0)){
                return null;
            }
            request.setOrderOperacionComercial(tdpOrder.getOrderOperationNumber().toString());
        } else {
            return null;
        }

        if (tdpOrder.getClientCmsServicio() != null) {
            request.setClienteCMSServicio(tdpOrder.getClientCmsServicio());
        }

        TdpUbigeoEquiv ubigeo = tdpUbigeoEquivRepository.getUbigeoAutomatizadorByNames(tdpOrder.getAddressDepartamento(), tdpOrder.getAddressProvincia(), tdpOrder.getAddressDistrito());
        String departamentoCod = "000", provinciaCod = "000", distritoCod = "000";
        if (ubigeo != null) {
            departamentoCod = ubigeo.getDepCod();
            provinciaCod = ubigeo.getProvCod();
            distritoCod = ubigeo.getDisCod();
        }


        request.setAddressDepartamentoCodigo(departamentoCod);
        request.setAddressProvinciaCodigo(provinciaCod);
        request.setAddressDistritoCodigo(distritoCod);

        return request;
    }
    //endregion;

    //region SeteoATIS
    public AutomatizadorSaleRequestBody SetATISAutomatizador(AutomatizadorSaleRequestBody request, TdpOrder tdpOrder) {
        if (tdpOrder.getClientRucDigitCtrl() != null) {
            request.setClienteRucDigitoControl(tdpOrder.getClientRucDigitCtrl());
        }
        if (tdpOrder.getClientRucActividad() != null) {
            request.setClienteRucActividadCodigo(tdpOrder.getClientRucActividad().toString());
        }
        if (tdpOrder.getClientRucSubactividad() != null) {
            request.setClienteRucSubActividadCodigo(tdpOrder.getClientRucSubactividad().toString());
        }

        if (tdpOrder.getAddressCchhCodigo() != null) {
            request.setAddressCCHHCodigo(tdpOrder.getAddressCchhCodigo().toString());

        }
        if (tdpOrder.getAddressCchhCompTip1() != null ) {
            if(!tdpOrder.getAddressCchhCompTip1().isEmpty()){
                request.setAddressCCHHCompTipo1(tdpOrder.getAddressCchhCompTip1());
            }
        }
        if (tdpOrder.getAddressCchhCompNom1() != null ) {
            if(!tdpOrder.getAddressCchhCompNom1().isEmpty()){
                request.setAddressCCHHCompNombre1(tdpOrder.getAddressCchhCompNom1());
            }
        }
        if (tdpOrder.getAddressCchhCompTip2() != null ) {
            if(!tdpOrder.getAddressCchhCompTip2().isEmpty()){
                request.setAddressCCHHCompTipo2(tdpOrder.getAddressCchhCompTip2());
            }
        }
        if (tdpOrder.getAddressCchhCompNom2() != null ) {
            if(!tdpOrder.getAddressCchhCompNom2().isEmpty()){
                request.setAddressCCHHCompNombre2(tdpOrder.getAddressCchhCompNom2());
            }
        }
        if (tdpOrder.getAddressCchhCompTip3() != null) {
            if(!tdpOrder.getAddressCchhCompTip3().isEmpty()){
                request.setAddressCCHHCompTipo3(tdpOrder.getAddressCchhCompTip3());
            }
        }
        if (tdpOrder.getAddressCchhCompNom3() != null ) {
            if(!tdpOrder.getAddressCchhCompNom3().isEmpty()){
                request.setAddressCCHHCompNombre3(tdpOrder.getAddressCchhCompNom3());
            }
        }
        if (tdpOrder.getAddressCchhCompTip4() != null ) {
            if(!tdpOrder.getAddressCchhCompTip4().isEmpty()){
                request.setAddressCCHHCompTipo4(tdpOrder.getAddressCchhCompTip4());
            }
        }
        if (tdpOrder.getAddressCchhCompNom4() != null) {
            if(!tdpOrder.getAddressCchhCompNom4().isEmpty()){
                request.setAddressCCHHCompNombre4(tdpOrder.getAddressCchhCompNom4());
            }
        }
        if (tdpOrder.getAddressCchhCompTip5() != null) {
            if(!tdpOrder.getAddressCchhCompTip5().isEmpty()){
                request.setAddressCCHHCompTipo5(tdpOrder.getAddressCchhCompTip5());
            }
        }
        if (tdpOrder.getAddressCchhCompNom5() != null) {
            if(!tdpOrder.getAddressCchhCompNom5().isEmpty()){
                request.setAddressCCHHCompNombre5(tdpOrder.getAddressCchhCompNom5());
            }
        }
        //TODO: Correccion de duplicado
        //Inicio duplicado


        if (tdpOrder.getAddressViaCompTip1() != null) {
            if(!tdpOrder.getAddressViaCompTip1().isEmpty()){
                request.setAddressViaCompTipo1(tdpOrder.getAddressViaCompTip1());
            }
        }
        if (tdpOrder.getAddressViaCompNom1() != null) {
            if(!tdpOrder.getAddressViaCompNom1().isEmpty()){
                request.setAddressViaCompNombre1(tdpOrder.getAddressViaCompNom1());
            }
        }

        if(tdpOrder.getAddressViaCompTip1() != null){
            if (tdpOrder.getAddressViaCompTip2() != null) {
                if(!tdpOrder.getAddressViaCompTip2().isEmpty()){
                    if(!tdpOrder.getAddressViaCompTip1().equalsIgnoreCase(tdpOrder.getAddressViaCompTip2())){
                        request.setAddressViaCompTipo2(tdpOrder.getAddressViaCompTip2());
                    }
                }
            }

            if (tdpOrder.getAddressViaCompNom2() != null) {
                if(!tdpOrder.getAddressViaCompNom2().isEmpty()){
                    if(!tdpOrder.getAddressViaCompNom1().equalsIgnoreCase(tdpOrder.getAddressViaCompNom2())){
                        request.setAddressViaCompNombre2(tdpOrder.getAddressViaCompNom2());
                    }
                    if(tdpOrder.getAddressViaCompNom2().equalsIgnoreCase(".")){
                        request.setAddressViaCompNombre2(tdpOrder.getAddressViaCompNom2());
                    }
                }
            }
        }


        //OCT_TIP_CMP
        if (tdpOrder.getAddressViaCompTip3() != null) {
//            request.setAddressViaCompTipo3(tdpOrder.getAddressViaCompTip3());
            request.setAddressViaCompTipo3(null);
        }
        //OCT_TIP_DIR
        if (tdpOrder.getAddressViaCompNom3() != null) {
            // request.setAddressViaCompNombre3(tdpOrder.getAddressViaCompNom3());
            request.setAddressViaCompNombre3(null);

        }
        //NOV_TIP_CMP
        if (tdpOrder.getAddressViaCompTip4() != null) {
            //request.setAddressViaCompTipo4(tdpOrder.getAddressViaCompTip4());
            request.setAddressViaCompTipo4(null);
        }
        //NOV_TIP_DIR
        if (tdpOrder.getAddressViaCompNom4() != null) {
            // request.setAddressViaCompNombre4(tdpOrder.getAddressViaCompNom4());
            request.setAddressViaCompNombre4(null);
        }
        //DCO_TIP_CMP
        if (tdpOrder.getAddressViaCompTip5() != null) {
            //request.setAddressViaCompTipo5(tdpOrder.getAddressViaCompTip5());
            request.setAddressViaCompTipo5(null);
        }
        //DCO_TIP_DIR
        if (tdpOrder.getAddressViaCompNom5() != null) {
            // request.setAddressViaCompNombre5(tdpOrder.getAddressViaCompNom5());
            request.setAddressViaCompNombre5(null);
        }

        if (tdpOrder.getClientEmail() != null) {
            request.setClienteEmail(tdpOrder.getClientEmail());
        }

        //Fin duplicado

        if (tdpOrder.getOrderGisXdsl() != null) {
            if(!tdpOrder.getOrderGisXdsl().equalsIgnoreCase("")){
                request.setOrderGisXDSL(tdpOrder.getOrderGisXdsl());
            }else{
                request.setOrderGisXDSL("N");
            }
        } else {
            request.setOrderGisXDSL("N");
        }
        if (tdpOrder.getOrderGisHfc() != null) {
            if(!tdpOrder.getOrderGisHfc().equalsIgnoreCase("")){
                request.setOrderGisHFC(tdpOrder.getOrderGisHfc());
            }else {
                request.setOrderGisHFC("N");
            }
        } else {
            request.setOrderGisHFC("N");
        }

        if (tdpOrder.getUserAtis() != null) {
            if(!tdpOrder.getUserAtis().equalsIgnoreCase("")){
                request.setUserAtis(tdpOrder.getUserAtis());
            }else {
                // return null;
            }
        } else {
            //return null;
        }

        if (tdpOrder.getUserCanalCodigo() != null) {
            if(!tdpOrder.getUserCanalCodigo().isEmpty()){
                request.setUserCanalCodigo(tdpOrder.getUserCanalCodigo());
            }
        } else {
            //  request.setUserCanalCodigo("56");
        }

        if (tdpOrder.getProductPsAdmin1() != null ) {
            request.setProductoPSAdmin1(tdpOrder.getProductPsAdmin1().toString());
        }
        if (tdpOrder.getProductPsAdmin2() != null) {
            request.setProductoPSAdmin2(tdpOrder.getProductPsAdmin2().toString());
        }
        if (tdpOrder.getProductPsAdmin3() != null) {
            request.setProductoPSAdmin3(tdpOrder.getProductPsAdmin3().toString());
        }
        if (tdpOrder.getProductPsAdmin4() != null) {
            request.setProductoPSAdmin4(tdpOrder.getProductPsAdmin4().toString());
        }

        if (tdpOrder.getSvaCodigo1() != null) {
            request.setProductoSVACodigo1(tdpOrder.getSvaCodigo1());
        }
        if (tdpOrder.getSvaCodigo2() != null) {
            request.setProductoSVACodigo2(tdpOrder.getSvaCodigo2());
        }
        if (tdpOrder.getSvaCodigo3() != null) {
            request.setProductoSVACodigo3(tdpOrder.getSvaCodigo3());
        }
        if (tdpOrder.getSvaCodigo4() != null) {
            request.setProductoSVACodigo4(tdpOrder.getSvaCodigo4());
        }
        if (tdpOrder.getSvaCodigo5() != null) {
            request.setProductoSVACodigo5(tdpOrder.getSvaCodigo5());
        }
        if (tdpOrder.getSvaCodigo6() != null) {
            request.setProductoSVACodigo6(tdpOrder.getSvaCodigo6());
        }
        if (tdpOrder.getSvaCodigo7() != null) {
            request.setProductoSVACodigo7(tdpOrder.getSvaCodigo7());
        }
        if (tdpOrder.getSvaCodigo8() != null) {
            request.setProductoSVACodigo8(tdpOrder.getSvaCodigo8());
        }
        if (tdpOrder.getSvaCodigo9() != null) {
            request.setProductoSVACodigo9(tdpOrder.getSvaCodigo9());
        }
        if (tdpOrder.getSvaCodigo10() != null) {
            request.setProductoSVACodigo10(tdpOrder.getSvaCodigo10());
        }

        if (tdpOrder.getOrderParqueTelefono() != null) {
            if(!tdpOrder.getOrderParqueTelefono().isEmpty()){
                request.setOrderParqueTelefono(tdpOrder.getOrderParqueTelefono());
            }
        }
        if (tdpOrder.getClientCmsCodigo() != null ) {

            request.setClienteCMSCodigo(tdpOrder.getClientCmsCodigo().toString());
        }


        String gpsX = tdpOrder.getOrderGpsX() != null ? tdpOrder.getOrderGpsX().toString() : "";
        String gpsY = tdpOrder.getOrderGpsY() != null ? tdpOrder.getOrderGpsY().toString() : "";
        if (gpsX != null && gpsX != "") {
            request.setOrderGPSX(gpsX.length() > 10 ? gpsX.substring(0, 11) : gpsX);
        }
        if (gpsY != null && gpsY != "") {
            request.setOrderGPSY(gpsY.length() > 10 ? gpsY.substring(0, 11) : gpsY);
        }

        if (tdpOrder.getUserFuerzaVenta() != null && String.valueOf(tdpOrder.getUserFuerzaVenta()) != "") {
            request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
        }else {
            //return null;
        }

        if (tdpOrder.getUserCms() != null && String.valueOf(tdpOrder.getUserCms()) != "") {
            request.setUserCMS(String.valueOf(tdpOrder.getUserCms()));
        }else{
            //return null;
        }

        if (tdpOrder.getClientSexo() != null && String.valueOf(tdpOrder.getUserCms()) != "") {
            request.setClienteSexo(tdpOrder.getClientSexo());
        }

        return request;
    }
    //endregion;

    //region SeteoCMS
    public AutomatizadorSaleRequestBody SetCMSAutomatizador(AutomatizadorSaleRequestBody request, TdpOrder tdpOrder, SimpleDateFormat sdfDate) {
        if (tdpOrder.getProductCabecera() != null) {
            if(!tdpOrder.getProductCabecera().isEmpty())
                request.setProductoCabecera(tdpOrder.getProductCabecera());
        } else {
            return null;
        }

        String dateNacimientoStr = "";
        Date dateNacimiento = tdpOrder.getClientNacimiento();
        if (dateNacimiento != null) {
            dateNacimientoStr = sdfDate.format(dateNacimiento);
        }
        if (dateNacimientoStr != null) {
            request.setClienteNacimiento(dateNacimientoStr);
        } else {
            return null;
        }

        if (tdpOrder.getClientSexo() != null) {
            request.setClienteSexo(tdpOrder.getClientSexo());
        }

        request.setClienteCivil("SOLTERO");

        if (tdpOrder.getClientEmail() != null) {
            request.setClienteEmail(tdpOrder.getClientEmail());
        }

        String telefonoArea = "0";
        List<Parameters> telAreaList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc("AUTOMATIZADOR", "TEL_AREA");
        for (Parameters obj : telAreaList) {
            if (toLowerWithouAccent(obj.getElement()).equalsIgnoreCase(toLowerWithouAccent(tdpOrder.getAddressDepartamento()))) {
                telefonoArea = obj.getStrValue();
                break;
            }
        }
        if (telefonoArea != null || telefonoArea != "") {
            request.setClienteTelefono1Area(telefonoArea);
        }

        if (tdpOrder.getClientTelefono2Area() != null) {
            request.setClienteTelefono2Area(tdpOrder.getClientTelefono2Area());
        }

        String codigoFacturacion = "1";
        if (tdpOrder.getProductTecTv() != null && tdpOrder.getProductTecTv().equalsIgnoreCase("CATV"))
            codigoFacturacion = "1";
        if (tdpOrder.getProductTecTv() != null && tdpOrder.getProductTecTv().equalsIgnoreCase("DTH"))
            codigoFacturacion = "5";
        request.setClienteCMSFactura(codigoFacturacion);

        if (tdpOrder.getProductTipoReg().equalsIgnoreCase("CMS") && tdpOrder.getOrderGisNtlt() == null) {
            request.setOrderGisNTLT("VALD361603*MIR1361601*MIR1361607*MIR1361606*MIR1361608");
        }

        String gpsX = tdpOrder.getOrderGpsX() != null ? tdpOrder.getOrderGpsX().toString() : "";
        String gpsY = tdpOrder.getOrderGpsY() != null ? tdpOrder.getOrderGpsY().toString() : "";
        if (gpsX != null && gpsX != "") {
            request.setOrderGPSX(gpsX.length() > 10 ? gpsX.substring(0, 11) : gpsX);
        } else {
            return null;
        }
        if (gpsY != null && gpsY != "") {
            request.setOrderGPSY(gpsY.length() > 10 ? gpsY.substring(0, 11) : gpsY);
        } else {
            return null;
        }

        if (tdpOrder.getUserFuerzaVenta() != null) {
            request.setUserCSMPuntoVenta(String.valueOf(tdpOrder.getUserFuerzaVenta()));
        }
        if (tdpOrder.getUserCms() != null) {
            request.setUserCMS(String.valueOf(tdpOrder.getUserCms()));
        }
        //automatizador agregando validación a cms por datos de gis
        if (tdpOrder.getOrderGisXdsl() != null) {
            if(!tdpOrder.getOrderGisXdsl().equalsIgnoreCase("")){
                request.setOrderGisXDSL(tdpOrder.getOrderGisXdsl());
            }else{
                request.setOrderGisXDSL("N");
            }
        } else {
            request.setOrderGisXDSL("N");
        }
        if (tdpOrder.getOrderGisHfc() != null) {
            if(!tdpOrder.getOrderGisHfc().equalsIgnoreCase("")){
                request.setOrderGisHFC(tdpOrder.getOrderGisHfc());
            }else {
                request.setOrderGisHFC("N");
            }
        } else {
            request.setOrderGisHFC("N");
        }
        //fin automatizador agregando validación a cms por datos de gis
        return request;
    }
    //endregion;

    public void setOrderInAutomatizador(String orderId) {
        try {
            tdpOrderRepository.updateAutomatizadorOkTdpOrder(1, orderId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private String getCantidadEquipos(TdpOrder tdpOrder) {
        Integer cantidad = 0;
        if (tdpOrder.getSvaCode1() != null && (tdpOrder.getSvaCode1().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode1().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode1().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad1();
            } catch (Exception ex) {
            }
        }
        if (tdpOrder.getSvaCode2() != null && (tdpOrder.getSvaCode2().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode2().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode2().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad2();
            } catch (Exception ex) {
            }
        }
        if (tdpOrder.getSvaCode3() != null && (tdpOrder.getSvaCode3().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode3().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode3().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad3();
            } catch (Exception ex) {
            }
        }
        if (tdpOrder.getSvaCode4() != null && (tdpOrder.getSvaCode4().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode4().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode4().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad4();
            } catch (Exception ex) {
            }
        }
        if (tdpOrder.getSvaCode5() != null && (tdpOrder.getSvaCode5().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode5().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode5().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad5();
            } catch (Exception ex) {
            }
        }
        if (tdpOrder.getSvaCode6() != null && (tdpOrder.getSvaCode6().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode6().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode6().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad6();
            } catch (Exception ex) {
            }
        }
        if (tdpOrder.getSvaCode7() != null && (tdpOrder.getSvaCode7().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode7().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode7().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad7();
            } catch (Exception ex) {
            }
        }
        if (tdpOrder.getSvaCode8() != null && (tdpOrder.getSvaCode8().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode8().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode8().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad8();
            } catch (Exception ex) {
            }
        }
        if (tdpOrder.getSvaCode9() != null && (tdpOrder.getSvaCode9().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode9().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode9().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad9();
            } catch (Exception ex) {
            }
        }
        if (tdpOrder.getSvaCode10() != null && (tdpOrder.getSvaCode10().equalsIgnoreCase("DHD") ||
                tdpOrder.getSvaCode10().equalsIgnoreCase("DSHD") || tdpOrder.getSvaCode10().equalsIgnoreCase("DVR"))) {
            try {
                cantidad = cantidad + tdpOrder.getSvaCantidad10();
            } catch (Exception ex) {
            }
        }
        return cantidad.toString();
    }

    private String toLowerWithouAccent(String text) {
        if (text != null)
            return text.trim().toLowerCase().replace("á", "a").replace("é", "e").replace("í", "i").replace("ó", "o").replace("ú", "u");
        else return "";
    }

    private ApiResponse<AutomatizadorSaleResponseBody> registerSale(AutomatizadorSaleRequestBody apiRequestBody) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getAutomatizadorRegisterSaleUri())
                .setApiId(apiHeaderConfig.getAutomatizadorRegisterSaleApiId())
                .setApiSecret(apiHeaderConfig.getAutomatizadorRegisterSaleApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_AUTOMATIZADOR_REGISTER)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_AUTOMATIZADOR)
                .build();

        AutomatizadorSaleClient automatizadorRegisterSaleClient = new AutomatizadorSaleClient(config);
        ClientResult<ApiResponse<AutomatizadorSaleResponseBody>> result = automatizadorRegisterSaleClient.post(apiRequestBody);
        result.getEvent().setOrderId(apiRequestBody.getOrderId());
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

    public boolean createAutomatizador(EstadoSolicitudRequest estadoSolicitudRequest) {
        logger.info("Inicio CreateAutomatizador Service");
        boolean estadoSolicitud = false;

        try {
            Automatizador automatizador = new Automatizador();
            automatizador.setCodvtappcd(estadoSolicitudRequest.getCodVtAppCd());
            automatizador.setCoderrcd(estadoSolicitudRequest.getCodErrCd());
            automatizador.setDeserrds(estadoSolicitudRequest.getDesErrDs());

            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
            Date date = format.parse(estadoSolicitudRequest.getFecHorRecWa());
            automatizador.setFechorrecwa(date);
            automatizador.setCodestvtaapp(estadoSolicitudRequest.getCodEstVtaApp());
            automatizador.setDesestvtaapp(estadoSolicitudRequest.getCodEstVtaApp());
            automatizador.setCodpedaticd(estadoSolicitudRequest.getCodPedAtiCd());
            automatizador.setCodreqeedcd(estadoSolicitudRequest.getCodReqEedCd());
            automatizador.setCodestpedcd(estadoSolicitudRequest.getCodEstPedCd());
            automatizador.setDesestpedcd(estadoSolicitudRequest.getDesEstPedCd());
            automatizador.setFechorreg(estadoSolicitudRequest.getFecHorReg());
            automatizador.setFechoremiped(estadoSolicitudRequest.getFecHorEmiPed());
            automatizador.setCodclicd(estadoSolicitudRequest.getCodCliCd());
            automatizador.setCodctacd(estadoSolicitudRequest.getCodCtaCd());
            automatizador.setCodinslegcd(estadoSolicitudRequest.getCodInsLegCd());
            automatizador.setNumidtlf(estadoSolicitudRequest.getNumIdTlf());
            automatizador.setCodclicms(estadoSolicitudRequest.getCodCliCms());
            automatizador.setCodctacms(estadoSolicitudRequest.getCodCtaCms());
            automatizador.setDesobsvtaapp(estadoSolicitudRequest.getDesObsVtaApp());
            automatizador.setCodidwebex1(estadoSolicitudRequest.getCodIdWebEx1());
            automatizador.setDesidwebex1(estadoSolicitudRequest.getDesIdWebEx1());
            automatizador.setCodestwebex1(estadoSolicitudRequest.getCodEstWebEx1());
            automatizador.setDesestwebex1(estadoSolicitudRequest.getDesEstWebEx1());
            automatizador.setCodidwebex2(estadoSolicitudRequest.getCodIdWebEx2());
            automatizador.setDesidwebex2(estadoSolicitudRequest.getDesIdWebEx2());
            automatizador.setCodestwebex2(estadoSolicitudRequest.getCodEstWebEx2());
            automatizador.setDesestwebex2(estadoSolicitudRequest.getDesEstWebEx2());
            automatizador.setCodidwebex3(estadoSolicitudRequest.getCodIdWebEx3());
            automatizador.setDesidwebex3(estadoSolicitudRequest.getDesIdWebEx3());
            automatizador.setCodestwebex3(estadoSolicitudRequest.getCodEstWebEx3());
            automatizador.setDesestwebex3(estadoSolicitudRequest.getDesEstWebEx3());
            automatizador.setCodidwebex4(estadoSolicitudRequest.getCodIdWebEx4());
            automatizador.setDesidwebex4(estadoSolicitudRequest.getDesIdWebEx4());
            automatizador.setCodestwebex4(estadoSolicitudRequest.getCodEstWebEx4());
            automatizador.setDesestwebex4(estadoSolicitudRequest.getDesEstWebEx4());
            automatizador.setCodidwebex5(estadoSolicitudRequest.getCodIdWebEx5());
            automatizador.setDesidwebex5(estadoSolicitudRequest.getDesIdWebEx5());
            automatizador.setCodestwebex5(estadoSolicitudRequest.getCodEstWebEx5());
            automatizador.setDesestwebex5(estadoSolicitudRequest.getDesEstWebEx5());

            LogVass.serviceResponseObject(logger, "CreateAutomatizador", "automatizador", automatizador);
            automatizadorRepository.save(automatizador);

            estadoSolicitud = true;

        } catch (Exception e) {
            logger.error("Error CreateAutomatizador Service => Exception: ", e);
            estadoSolicitud = false;
        }
        logger.info("Fin CreateAutomatizador Service");
        return estadoSolicitud;
    }


}
