package pe.com.tdp.ventafija.microservices.controller.user;

import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.LoginUtil;
import pe.com.tdp.ventafija.microservices.constants.user.UserConstants;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.dto.WebRegistrationRequestDto;
import pe.com.tdp.ventafija.microservices.domain.order.UserInfoRequest;
import pe.com.tdp.ventafija.microservices.domain.profile.*;
import pe.com.tdp.ventafija.microservices.domain.user.*;
import pe.com.tdp.ventafija.microservices.service.user.UserService;

@RestController
public class UserWebController {
    private static final Logger logger = LogManager.getLogger(UserWebController.class);
    @Autowired
    private UserService userService;

    /**
     * Permite resetear la contraseña a su valor por defecto. Usado cuando el usuario web olvido su contraseña.
     *
     * @author glazaror
     */
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/login/token", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<LoginResponseToken> clientLogin(@RequestParam("username") String username, @RequestParam("password") String password) {
        logger.info("Inicio ClientLogin Controller");
        logger.info("REQUEST => {\"username\":\"" + username + "\",\"password\":\"" + password + "\"}");

        Response<LoginResponseToken> responseData = new Response<LoginResponseToken>();
        LoginResponseToken responseToken = new LoginResponseToken();
        try {
            Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("username", username);
            //payload.put("password", password);

            //primero verificamos que el usuario y password sean correctos
            LoginRequest request = new LoginRequest();
            request.setCodAtis(username);
            request.setPassword(password);
            Response<LoginResponseData> responseLogin = userService.validateLogin(request);
            LogVass.serviceResponseObject(logger, "ClientLogin", "responseLogin", responseLogin);

            //si es que los datos de autenticacion son correctos entonces generamos el login
            if (UserConstants.LOGIN_RESPONSE_CODE_OK.equals(responseLogin.getResponseCode())) {
                //entonces generamos el token
                String token = LoginUtil.generarToken(payload);

                responseToken.setToken(token);
                responseToken.setResponseData(responseLogin.getResponseData());

                responseData.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
                responseData.setResponseData(responseToken);

            } else {
                responseData.setResponseCode(responseLogin.getResponseCode());
                responseData.setResponseMessage(responseLogin.getResponseMessage());
            }
        } catch (Exception e) {
            logger.error("Error ClientLogin Controller", e);
        }
        LogVass.finishController(logger, "ClientLogin", responseData);
        return responseData;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/register", method = RequestMethod.POST)
    public Response<String> registrar(@RequestBody WebRegistrationRequestDto registrationRequest) {
        LogVass.startController(logger, "Registrar", registrationRequest);
        Response<String> response = new Response<>();
        try {
            response = userService.registerWeb(registrationRequest.getTipoDoc(),registrationRequest.getDni(), registrationRequest.getCodAtis(), registrationRequest.getCodCms(), registrationRequest.getPassword());
        } catch (Exception e) {
            logger.error("Error Registrar Controller", e);
            response.setResponseCode(UserConstants.REGISTER_RESPONSE_CODE_ERROR);
            response.setResponseMessage(UserConstants.REGISTER_RESPONSE_MESSAGE);
        }
        LogVass.finishController(logger, "Registrar", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/changePassword", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> changePasswordWeb(@RequestBody ResetPasswordRequest request) {
        LogVass.startController(logger, "ResetPassword", request);

        Response<String> response = new Response<>();
        try {
            response = userService.resetPassword(request);
        } catch (Exception e) {
            logger.error("Error ResetPassword Controller", e);
        }
        LogVass.finishController(logger, "ResetPassword", response);
        return response;
    }

    /**
     *  Permite guardar un log de del usuario e inserta en un log
     *  @author: Valdemar cambienlo ahora pe animales
     * @param request
     * @return
     */

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/saveLog", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> saveLog(@RequestBody LogUserRequest request) {
        LogVass.startController(logger, "saveLog", request);
        Response<String> response = new Response<>();
        try {
             userService.saveLog(request);
             response.setResponseCode("0");
             response.setResponseMessage("saveLog");
             response.setResponseData("OK");
        } catch (Exception e) {
            logger.error("Error saveLog Controller", e);
        }
        LogVass.finishController(logger, "saveLog", response);

        return response;
    }


    /**
     * Permite resetear la contraseña a su valor por defecto. Usado cuando el usuario web olvido su contraseña.
     *
     * @author glazaror
     */
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/reset", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<String> resetPasswordWeb(@RequestBody ResetPasswordRequest request) {
        LogVass.startController(logger, "ResetPasswordWeb", request);
        //logger.info("campos para el resetPasswordRequest codAtis:" + request.getCodAtis() + "|dni:" + request.getDni());
        Response<String> response = new Response<>();
        try {
            //logger.info("Iniciando el proceso de reseteo de password");
            response = userService.resetPasswordWeb(request);
            //logger.info("Fin el proceso de cambio de password");
        } catch (Exception e) {
            logger.error("Error ResetPasswordWeb Controller", e);
        }
        LogVass.finishController(logger, "ResetPasswordWeb", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/info", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ConfigResponseData> loadUserDataFromToken(@RequestHeader("Access-Token") String accessToken) {
        logger.info("Inicio LoadUserDataFromToken Controller");
        logger.info("REQUEST => {\"accessToken\":\"" + accessToken + "\"");
        Response<ConfigResponseData> response = new Response<>();
        try {
            response = userService.loadUserData(accessToken);
        } catch (Exception e) {
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.error("Error LoadUserDataFromToken Controller", e);
        }
        LogVass.finishController(logger, "LoadUserDataFromToken", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/infodetail", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ConfigResponseData> loadUserDataDetailFromToken(@RequestBody UserInfoRequest request, @RequestHeader(value = "Authorization") String authorization) {
        logger.info("Inicio LoadUserDataDetailFromToken Controller");
        logger.info("REQUEST => {\"authorization\":\"" + authorization + "\"}");

        Response<ConfigResponseData> response = new Response<>();
        try {
            response = userService.loadUserDataDetails(authorization, request.getTipoAplicacion());
        } catch (Exception e) {
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.error("Error loadUserDataDetailFromToken Controller", e);
        }
        LogVass.finishController(logger, "LoadUserDataDetailFromToken", response);
        return response;
    }



    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/getAccessByNivel", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ResponseAccessData> getAccessByNivel(@RequestBody NivelRequest request, @RequestHeader(value = "Authorization") String authorization) {
        logger.info("Inicio getAccessByNivel Controller");
        logger.info("REQUEST => {\"authorization\":\"" + authorization + "\"}");

        Response<ResponseAccessData> response = new Response<>();
        try {
            response = userService.getAccessByNivel(request.getNivel(), request.getCanal());
        } catch (Exception e) {
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.error("Error getAccessByNivel Controller", e);
        }
        LogVass.finishController(logger, "getAccessByNivel", response);
        return response;
    }



    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/getLoginConvergencia", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<LoginConvergenciaResponse> getLoginConvergencia(@RequestBody LoginConvergenciaRequest request, @RequestHeader(value = "Authorization") String authorization) {
        logger.info("Inicio getLoginConvergencia Controller");
        logger.info("REQUEST => {\"authorization\":\"" + authorization + "\"}");

        Response<LoginConvergenciaResponse> response = new Response<>();
        try {
            response = userService.getLoginConvergencia(request.getPto_venta());
        } catch (Exception e) {
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.error("Error getLoginConvergencia Controller", e);
        }
        LogVass.finishController(logger, "getLoginConvergencia", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/getReintentosReniec", method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public Response<ParameterResponse> getReintentosReniec(@RequestHeader(value = "Authorization") String authorization) {
         logger.info("Inicio getLoginConvergencia Controller");
         logger.info("REQUEST => {\"authorization\":\"" + authorization + "\"}");
         Response<ParameterResponse> response = new Response<>();
         try {
            response = userService.getReintentosReniec();
         } catch (Exception e) {
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.error("Error getLoginConvergencia Controller", e);
         }
         LogVass.finishController(logger, "getLoginConvergencia", response);
         return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/generarToken", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ConvergenciaResponse> generarToken(@RequestBody ConvergenciaRequest request, @RequestHeader(value = "Authorization") String authorization) {
        logger.info("Inicio generarToken Controller");
        logger.info("REQUEST => {\"authorization\":\"" + authorization + "\"}");

        Response<ConvergenciaResponse> response = new Response<>();
        try {
            response = userService.guardarToken(request);
        } catch (Exception e) {
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.error("Error generarToken Controller", e);
        }
        LogVass.finishController(logger, "generarToken", response);
        return response;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/user/web/desencriptarToken", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ConvergenciaRequest> generarToken(@RequestBody  ConvergenciaResponse request) {
        logger.info("Inicio desencriptarToken Controller");

        Response<ConvergenciaRequest> response = new Response<>();
        try {
            response = userService.desencriptarToken(request);
        } catch (Exception e) {
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.error("Error desencriptarToken Controller", e);
        }
        LogVass.finishController(logger, "desencriptarToken", response);
        return response;
    }
}
