package pe.com.tdp.ventafija.microservices.service.product.command.gis;

import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;

import java.util.Map;

@Component("filterTrio")
public class TrioGisFilterCommand implements GisFilterCommand {
    // Filtro para TRIOS (3)
    private static String TRIO_FILTER = " (OFERTA.targetData = '3' AND CAT.internetTech = '%s' AND CAT.tvTech = '%s' AND CAT.internetSpeed <= %s AND CAT.price <= coalesce(%s, 9999999) )";

    @Override
    public void execute(WSSIGFFTT_FFTT_AVEResult resultGis, Map<String, String> variables, StringBuilder queryBuilder, StringBuilder columnsBuilder) {
        String hasTecnologiaGPON = resultGis.getGPON_TEC();
        String velocidadGPON = resultGis.getGPON_VEL();

        String hasTecnologiaHFC = resultGis.getHFC_TEC();
        String velocidadHFC = resultGis.getHFC_VEL();

        String hasTecnologiaADSL = resultGis.getXDSL_TEC();
        String velocidadADSL = resultGis.getXDSL_VEL_TRM();
        String hasTecnologiaADSLSaturado = resultGis.getXDSL_SAT();
        String hasTecnologiaCoaxial = resultGis.getCOAXIAL_TEC();

        String price = variables.get("PRICE");

        // Evaluamos productos TRIOS (3)
        if ("SI".equals(hasTecnologiaGPON)) {
            // Para el caso de GPON se asume que la tecnologia TV es "CATV"
            String filtro = String.format(TRIO_FILTER, "FTTH", "CATV", velocidadGPON, price);
            queryBuilder.append(filtro + " OR ");
        }else if ("SI".equals(hasTecnologiaHFC)) {
            // Para el caso de HFC se asume que la tecnologia TV es "CATV"
            String filtro = String.format(TRIO_FILTER, "HFC", "CATV", velocidadHFC, price);
            queryBuilder.append(filtro + " OR ");
        }else if ("SI".equals(hasTecnologiaADSL) && "NO".equals(hasTecnologiaADSLSaturado)) {
            // Para el caso de ADSL la tecnologia de television depende de la tecnologia coaxial (COAXIAL_TEC)
            // Si es que COAXIAL_TEC = 'SI' entonces tecnologia television = 'CATV'
            // Si es que COAXIAL_TEC = 'NO' entonces tecnologia television = 'DTH'
            String tecnologiaTelevision = "SI".equals(hasTecnologiaCoaxial) ? "CATV" : "DTH";
            String filtro = String.format(TRIO_FILTER, "ADSL", tecnologiaTelevision, velocidadADSL, price);
            queryBuilder.append(filtro + " OR ");
        }
    }
}
