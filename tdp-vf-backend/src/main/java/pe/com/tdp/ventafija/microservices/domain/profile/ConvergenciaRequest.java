package pe.com.tdp.ventafija.microservices.domain.profile;

import java.sql.Timestamp;

public class ConvergenciaRequest {

    private String tipdoc;
    private String numero_de_doc;
    private String codatis;
    private String canal_de_venta;
    private String punto_de_venta;
    private String asesor_nombre;
    private String canalequivcampania;
    private String entidad;

    public String getTipdoc() {
        return tipdoc;
    }

    public void setTipdoc(String tipdoc) {
        this.tipdoc = tipdoc;
    }

    public String getNumero_de_doc() {
        return numero_de_doc;
    }

    public void setNumero_de_doc(String numero_de_doc) {
        this.numero_de_doc = numero_de_doc;
    }

    public String getCodatis() {
        return codatis;
    }

    public void setCodatis(String codatis) {
        this.codatis = codatis;
    }

    public String getId_transaccion() {
        return (new Timestamp(System.currentTimeMillis())).toString();
    }

    public String getCanal_de_venta() {
        return canal_de_venta;
    }

    public void setCanal_de_venta(String canal_de_venta) {
        this.canal_de_venta = canal_de_venta;
    }

    public String getPunto_de_venta() {
        return punto_de_venta;
    }

    public void setPunto_de_venta(String punto_de_venta) {
        this.punto_de_venta = punto_de_venta;
    }

    public String getAsesor_nombre() {
        return asesor_nombre;
    }

    public void setAsesor_nombre(String asesor_nombre) {
        this.asesor_nombre = asesor_nombre;
    }

    public String getCanalequivcampania() {
        return canalequivcampania;
    }

    public void setCanalequivcampania(String canalequivcampania) {
        this.canalequivcampania = canalequivcampania;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }
}
