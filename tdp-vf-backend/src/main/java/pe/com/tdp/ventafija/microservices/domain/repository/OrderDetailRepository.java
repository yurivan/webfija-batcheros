package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import pe.com.tdp.ventafija.microservices.domain.order.entity.OrderDetail;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, Integer> {

	//@Query("select od.id from OrderDetail od where od.order.id = ?1") //glazaror prueba
	@Query("select od.id from OrderDetail od where od.orderId = ?1") //glazaror prueba
	Long findByOrderId (String orderId);
	
	@Modifying
	//@Query("update OrderDetail od set od.speechText = ?1, od.hasRecording = ?2 where od.order.id = ?3") //glazaror prueba
	@Query("update OrderDetail od set od.speechText = ?1, od.hasRecording = ?2 where od.orderId = ?3") //glazaror prueba
	void updateSpeechText (String speechText, String hasRecording, String orderId);
	
	// Sprint 7
	@Modifying
	void deleteByOrderId (String orderId);

	@Modifying
	@Query(value = "update OrderDetail od set od.decosSD = ?1, od.decoshd = ?2, od.decosDVR = ?3 where od.orderId = ?4")
	void updateDecosSvaOrderDetail(int smart, int hd, int dvr, String orderId);

	@Modifying
	@Query(value = "update OrderDetail od set od.address = ?1 where od.orderId = ?2")
	void updateAddressOrderDetail(String address, String orderId);
}
