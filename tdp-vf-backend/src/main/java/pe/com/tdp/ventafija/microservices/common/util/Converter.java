package pe.com.tdp.ventafija.microservices.common.util;

import java.nio.charset.Charset;

public class Converter {
    public static String fixPrice(String price) {
        return price.replaceFirst("[.]", "").replaceAll("[^0-9.]", "");
    }

    /**
     * Función que elimina acentos y caracteres especiales de
     * una cadena de texto.
     *
     * @param input
     * @return cadena de texto limpia de acentos y caracteres especiales.
     */
    public static String removeAccents(String input) {
        // Cadena de caracteres original a sustituir.
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        System.out.println("original: " + original + " len: " + original.length());
        // Cadena de caracteres ASCII que reemplazarán los originales.
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        System.out.println("ascii: " + ascii + " len: " + ascii.length());
        String output = input;
        for (int i = 0; i < ascii.length(); i++) {
            // Reemplazamos los caracteres especiales.
            Character origin = original.charAt(i);
            Character destiny = ascii.charAt(i);
            output = output.replace(origin, destiny);
        }
        return output;
    }
}
