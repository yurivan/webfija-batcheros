package pe.com.tdp.ventafija.microservices.service.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.domain.FilterResponse;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpSalesAgent;
import pe.com.tdp.ventafija.microservices.domain.product.OffersRequest2;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpSalesAgentRepository;
import pe.com.tdp.ventafija.microservices.service.product.command.campania.CampaignFilterCommand;

@Service
public class CampaniaFilterServiceImpl implements CampaniaFilterService {

    @Autowired
    @Qualifier("filterCampaign")
    private CampaignFilterCommand filterCampaign;

    @Autowired
    private TdpSalesAgentRepository salesAgentRepository;

    @Override
    public FilterResponse getFilter(OffersRequest2 requestOffers) {
        // Verificamos que se tengan cargados los valores equivalentes de campania: canal y entidad
        // Para el caso de la app es posible que no se encuentren cargados estos valores por lo tanto consultamos de bd
        if (requestOffers.getSellerChannelEquivalentCampaign() == null) {
            TdpSalesAgent salesAgent = salesAgentRepository.findOne(requestOffers.getAtisSellerCode());
            requestOffers.setSellerChannelEquivalentCampaign(salesAgent.getCanalEquivalenciaCampania());
            requestOffers.setSellerEntityEquivalentCampaign(salesAgent.getEntidadEquivalenciaCampania());
        }
        StringBuilder queryBuilder = new StringBuilder();
        StringBuilder columnsBuilder = new StringBuilder();
        filterCampaign.execute(requestOffers, null, queryBuilder, columnsBuilder);

        FilterResponse filterResponse = new FilterResponse(queryBuilder.toString(), columnsBuilder.toString());
        return filterResponse;
    }
}
