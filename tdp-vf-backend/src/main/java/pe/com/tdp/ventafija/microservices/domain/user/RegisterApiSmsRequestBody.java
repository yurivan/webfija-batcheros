package pe.com.tdp.ventafija.microservices.domain.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "Mensaje", "TelefonoDestino" })
public class RegisterApiSmsRequestBody {

  // private String Correo;

  private String Mensaje;

  private String TelefonoDestino;

  // @JsonProperty("Correo")
  // public String getCorreo() {
  // return Correo;
  // }
  //
  // public void setCorreo(String correo) {
  // Correo = correo;
  // }
  @JsonProperty("Mensaje")
  public String getMensaje() {
    return Mensaje;
  }

  public void setMensaje(String mensaje) {
    Mensaje = mensaje;
  }

  @JsonProperty("TelefonoDestino")
  public String getTelefonoDestino() {
    return TelefonoDestino;
  }

  public void setTelefonoDestino(String telefonoDestino) {
    TelefonoDestino = telefonoDestino;
  }
}
