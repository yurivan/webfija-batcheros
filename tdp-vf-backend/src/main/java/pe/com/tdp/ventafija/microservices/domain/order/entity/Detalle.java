package pe.com.tdp.ventafija.microservices.domain.order.entity;

public class Detalle {
    private String idDetalleOrdenPago;
    private String conceptoPago;
    private double importe;
    private String tipo_Origen;
    private String cod_Origen;
    private String campo1;
    private String campo2;
    private String campo3;

    public Detalle() {
        super();
    }

    public String getIdDetalleOrdenPago() {
        return idDetalleOrdenPago;
    }

    public void setIdDetalleOrdenPago(String idDetalleOrdenPago) {
        this.idDetalleOrdenPago = idDetalleOrdenPago;
    }

    public String getConceptoPago() {
        return conceptoPago;
    }

    public void setConceptoPago(String conceptoPago) {
        this.conceptoPago = conceptoPago;
    }

    public double getImporte() {
        return importe;
    }

    public void setImporte(double importe) {
        this.importe = importe;
    }

    public String getTipo_Origen() {
        return tipo_Origen;
    }

    public void setTipo_Origen(String tipo_Origen) {
        this.tipo_Origen = tipo_Origen;
    }

    public String getCod_Origen() {
        return cod_Origen;
    }

    public void setCod_Origen(String cod_Origen) {
        this.cod_Origen = cod_Origen;
    }

    public String getCampo1() {
        return campo1;
    }

    public void setCampo1(String campo1) {
        this.campo1 = campo1;
    }

    public String getCampo2() {
        return campo2;
    }

    public void setCampo2(String campo2) {
        this.campo2 = campo2;
    }

    public String getCampo3() {
        return campo3;
    }

    public void setCampo3(String campo3) {
        this.campo3 = campo3;
    }
}
