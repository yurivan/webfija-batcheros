package pe.com.tdp.ventafija.microservices.domain.order;

public class SalesReportExportResponse {
    private String codigo_venta;
    private String sistema_venta;
    private String id_grabacion;
    private String campania;
    private String operacion_comercial;
    private String tipo_doc;
    private String num_doc;
    private String nom_cli;
    private String fec_nac;
    private String telefono1;
    private String telefono2;
    private String email;
    private String tel_fij_planta;
    private String cod_cms_planta;
    private String cod_cli_cms;
    private String region;
    private String zonal;
    private String canal;
    private String entidad;
    private String punto_de_venta;
    private String cod_punto_de_venta;
    private String nom_vendedor;
    private String cod_atis;
    private String cod_cms;
    private String segmento;
    private String tipo_producto;
    private String sub_producto;
    private String nom_producto;
    private String codigo_pedido;
    private String tecnologia_internet;
    private String tecnologia_tv;
    private String fec_venta;
    private String hora_venta;
    private String fec_registro;
    private String hora_registro;
    private String estado_venta;
    private String estado_legados;
    private String motivo_estado;
    private String estado_toa;
    private String modalidad_pago;
    private String cod_experto;
    private String cod_cip;
    private String monto_cip;
    private String estado_cip;
    private String renta_reg;
    private String renta_prom;
    private String coord_x;
    private String coord_y;
    private String direccion;
    private String referencia;
    private String distrito;
    private String provincia;
    private String departamento;
    private String svas_linea_vendido;
    private String svas_tv_vendido;
    private String svas_inter_vendido;
    private String envio_contrato;
    private String proteccion_datos;
    private String recibo_dig;
    private String filtro_parental;
    private String debito_automatico;
    private String nacionalidad;
    private String equipamiento_tv;
    private String equipamiento_internet;
    private String equipamiento_linea;
    private String costo_instalacion;
    private String cuotas_costo_instalacion;
    private String estado_audio;
    private String tipo_reporte;
    private String product_tipo_reg;

    public String getCodigo_venta() {
        return codigo_venta;
    }

    public void setCodigo_venta(String codigo_venta) {
        this.codigo_venta = codigo_venta;
    }

    public String getSistema_venta() {
        return sistema_venta;
    }

    public void setSistema_venta(String sistema_venta) {
        this.sistema_venta = sistema_venta;
    }

    public String getId_grabacion() {
        return id_grabacion;
    }

    public void setId_grabacion(String id_grabacion) {
        this.id_grabacion = id_grabacion;
    }

    public String getCampania() {
        return campania;
    }

    public void setCampania(String campania) {
        this.campania = campania;
    }

    public String getOperacion_comercial() {
        return operacion_comercial;
    }

    public void setOperacion_comercial(String operacion_comercial) {
        this.operacion_comercial = operacion_comercial;
    }

    public String getTipo_doc() {
        return tipo_doc;
    }

    public void setTipo_doc(String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }

    public String getNum_doc() {
        return num_doc;
    }

    public void setNum_doc(String num_doc) {
        this.num_doc = num_doc;
    }

    public String getNom_cli() {
        return nom_cli;
    }

    public void setNom_cli(String nom_cli) {
        this.nom_cli = nom_cli;
    }

    public String getFec_nac() {
        return fec_nac;
    }

    public void setFec_nac(String fec_nac) {
        this.fec_nac = fec_nac;
    }

    public String getTelefono1() {
        return telefono1;
    }

    public void setTelefono1(String telefono1) {
        this.telefono1 = telefono1;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel_fij_planta() {
        return tel_fij_planta;
    }

    public void setTel_fij_planta(String tel_fij_planta) {
        this.tel_fij_planta = tel_fij_planta;
    }

    public String getCod_cms_planta() {
        return cod_cms_planta;
    }

    public void setCod_cms_planta(String cod_cms_planta) {
        this.cod_cms_planta = cod_cms_planta;
    }

    public String getCod_cli_cms() {
        return cod_cli_cms;
    }

    public void setCod_cli_cms(String cod_cli_cms) {
        this.cod_cli_cms = cod_cli_cms;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZonal() {
        return zonal;
    }

    public void setZonal(String zonal) {
        this.zonal = zonal;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getPunto_de_venta() {
        return punto_de_venta;
    }

    public void setPunto_de_venta(String punto_de_venta) {
        this.punto_de_venta = punto_de_venta;
    }

    public String getCod_punto_de_venta() {
        return cod_punto_de_venta;
    }

    public void setCod_punto_de_venta(String cod_punto_de_venta) {
        this.cod_punto_de_venta = cod_punto_de_venta;
    }

    public String getNom_vendedor() {
        return nom_vendedor;
    }

    public void setNom_vendedor(String nom_vendedor) {
        this.nom_vendedor = nom_vendedor;
    }

    public String getCod_atis() {
        return cod_atis;
    }

    public void setCod_atis(String cod_atis) {
        this.cod_atis = cod_atis;
    }

    public String getCod_cms() {
        return cod_cms;
    }

    public void setCod_cms(String cod_cms) {
        this.cod_cms = cod_cms;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setSegmento(String segmento) {
        this.segmento = segmento;
    }

    public String getTipo_producto() {
        return tipo_producto;
    }

    public void setTipo_producto(String tipo_producto) {
        this.tipo_producto = tipo_producto;
    }

    public String getSub_producto() {
        return sub_producto;
    }

    public void setSub_producto(String sub_producto) {
        this.sub_producto = sub_producto;
    }

    public String getNom_producto() {
        return nom_producto;
    }

    public void setNom_producto(String nom_producto) {
        this.nom_producto = nom_producto;
    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public String getTecnologia_internet() {
        return tecnologia_internet;
    }

    public void setTecnologia_internet(String tecnologia_internet) {
        this.tecnologia_internet = tecnologia_internet;
    }

    public String getTecnologia_tv() {
        return tecnologia_tv;
    }

    public void setTecnologia_tv(String tecnologia_tv) {
        this.tecnologia_tv = tecnologia_tv;
    }

    public String getFec_venta() {
        return fec_venta;
    }

    public void setFec_venta(String fec_venta) {
        this.fec_venta = fec_venta;
    }

    public String getHora_venta() {
        return hora_venta;
    }

    public void setHora_venta(String hora_venta) {
        this.hora_venta = hora_venta;
    }

    public String getFec_registro() {
        return fec_registro;
    }

    public void setFec_registro(String fec_registro) {
        this.fec_registro = fec_registro;
    }

    public String getHora_registro() {
        return hora_registro;
    }

    public void setHora_registro(String hora_registro) {
        this.hora_registro = hora_registro;
    }

    public String getEstado_venta() {
        return estado_venta;
    }

    public void setEstado_venta(String estado_venta) {
        this.estado_venta = estado_venta;
    }

    public String getEstado_legados() {
        return estado_legados;
    }

    public void setEstado_legados(String estado_legados) {
        this.estado_legados = estado_legados;
    }

    public String getMotivo_estado() {
        return motivo_estado;
    }

    public void setMotivo_estado(String motivo_estado) {
        this.motivo_estado = motivo_estado;
    }

    public String getEstado_toa() {
        return estado_toa;
    }

    public void setEstado_toa(String estado_toa) {
        this.estado_toa = estado_toa;
    }

    public String getModalidad_pago() {
        return modalidad_pago;
    }

    public void setModalidad_pago(String modalidad_pago) {
        this.modalidad_pago = modalidad_pago;
    }

    public String getCod_experto() {
        return cod_experto;
    }

    public void setCod_experto(String cod_experto) {
        this.cod_experto = cod_experto;
    }

    public String getCod_cip() {
        return cod_cip;
    }

    public void setCod_cip(String cod_cip) {
        this.cod_cip = cod_cip;
    }

    public String getMonto_cip() {
        return monto_cip;
    }

    public void setMonto_cip(String monto_cip) {
        this.monto_cip = monto_cip;
    }

    public String getEstado_cip() {
        return estado_cip;
    }

    public void setEstado_cip(String estado_cip) {
        this.estado_cip = estado_cip;
    }

    public String getRenta_reg() {
        return renta_reg;
    }

    public void setRenta_reg(String renta_reg) {
        this.renta_reg = renta_reg;
    }

    public String getRenta_prom() {
        return renta_prom;
    }

    public void setRenta_prom(String renta_prom) {
        this.renta_prom = renta_prom;
    }

    public String getCoord_x() {
        return coord_x;
    }

    public void setCoord_x(String coord_x) {
        this.coord_x = coord_x;
    }

    public String getCoord_y() {
        return coord_y;
    }

    public void setCoord_y(String coord_y) {
        this.coord_y = coord_y;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getSvas_linea_vendido() {
        return svas_linea_vendido;
    }

    public void setSvas_linea_vendido(String svas_linea_vendido) {
        this.svas_linea_vendido = svas_linea_vendido;
    }

    public String getSvas_tv_vendido() {
        return svas_tv_vendido;
    }

    public void setSvas_tv_vendido(String svas_tv_vendido) {
        this.svas_tv_vendido = svas_tv_vendido;
    }

    public String getSvas_inter_vendido() {
        return svas_inter_vendido;
    }

    public void setSvas_inter_vendido(String svas_inter_vendido) {
        this.svas_inter_vendido = svas_inter_vendido;
    }

    public String getEnvio_contrato() {
        return envio_contrato;
    }

    public void setEnvio_contrato(String envio_contrato) {
        this.envio_contrato = envio_contrato;
    }

    public String getProteccion_datos() {
        return proteccion_datos;
    }

    public void setProteccion_datos(String proteccion_datos) {
        this.proteccion_datos = proteccion_datos;
    }

    public String getRecibo_dig() {
        return recibo_dig;
    }

    public void setRecibo_dig(String recibo_dig) {
        this.recibo_dig = recibo_dig;
    }

    public String getFiltro_parental() {
        return filtro_parental;
    }

    public void setFiltro_parental(String filtro_parental) {
        this.filtro_parental = filtro_parental;
    }

    public String getDebito_automatico() {
        return debito_automatico;
    }

    public void setDebito_automatico(String debito_automatico) {
        this.debito_automatico = debito_automatico;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getEquipamiento_tv() {
        return equipamiento_tv;
    }

    public void setEquipamiento_tv(String equipamiento_tv) {
        this.equipamiento_tv = equipamiento_tv;
    }

    public String getEquipamiento_internet() {
        return equipamiento_internet;
    }

    public void setEquipamiento_internet(String equipamiento_internet) {
        this.equipamiento_internet = equipamiento_internet;
    }

    public String getEquipamiento_linea() {
        return equipamiento_linea;
    }

    public void setEquipamiento_linea(String equipamiento_linea) {
        this.equipamiento_linea = equipamiento_linea;
    }

    public String getCosto_instalacion() {
        return costo_instalacion;
    }

    public void setCosto_instalacion(String costo_instalacion) {
        this.costo_instalacion = costo_instalacion;
    }

    public String getCuotas_costo_instalacion() {
        return cuotas_costo_instalacion;
    }

    public void setCuotas_costo_instalacion(String cuotas_costo_instalacion) {
        this.cuotas_costo_instalacion = cuotas_costo_instalacion;
    }

    public String getEstado_audio() {
        return estado_audio;
    }

    public void setEstado_audio(String estado_audio) {
        this.estado_audio = estado_audio;
    }

    public String getTipo_reporte() {
        return tipo_reporte;
    }

    public void setTipo_reporte(String tipo_reporte) {
        this.tipo_reporte = tipo_reporte;
    }

    public String getProduct_tipo_reg() {
        return product_tipo_reg;
    }

    public void setProduct_tipo_reg(String product_tipo_reg) {
        this.product_tipo_reg = product_tipo_reg;
    }
}
