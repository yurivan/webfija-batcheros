package pe.com.tdp.ventafija.microservices.domain.product;

public class OffersInterpretacionResponse {

    private String[] segmentos;

    private String[] canales;

    private String[] provincias;

    private String[] distritos;

    private String[] campanias;

    private String[] promospeeds;

    private String[] periodopromospeed;

    private String[] tiporegistro;

    private String[] serviciospaquete;


    public String[] getSegmentos() {
        return segmentos;
    }

    public void setSegmentos(String[] segmentos) {
        this.segmentos = segmentos;
    }

    public String[] getCanales() {
        return canales;
    }

    public void setCanales(String[] canales) {
        this.canales = canales;
    }

    public String[] getProvincias() {
        return provincias;
    }

    public void setProvincias(String[] provincias) {
        this.provincias = provincias;
    }

    public String[] getDistritos() {
        return distritos;
    }

    public void setDistritos(String[] distritos) {
        this.distritos = distritos;
    }

    public String[] getCampanias() {
        return campanias;
    }

    public void setCampanias(String[] campanias) {
        this.campanias = campanias;
    }

    public String[] getPromospeeds() {
        return promospeeds;
    }

    public void setPromospeeds(String[] promospeeds) {
        this.promospeeds = promospeeds;
    }

    public String[] getPeriodopromospeed() {
        return periodopromospeed;
    }

    public void setPeriodopromospeed(String[] periodopromospeed) {
        this.periodopromospeed = periodopromospeed;
    }

    public String[] getTiporegistro() {
        return tiporegistro;
    }

    public void setTiporegistro(String[] tiporegistro) {
        this.tiporegistro = tiporegistro;
    }

    public String[] getServiciospaquete() {
        return serviciospaquete;
    }

    public void setServiciospaquete(String[] serviciospaquete) {
        this.serviciospaquete = serviciospaquete;
    }
}
