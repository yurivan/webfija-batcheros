package pe.com.tdp.ventafija.microservices.constants.motorizado;

public class MotorizadoConstants {

    private MotorizadoConstants() {
    }

    public static final String MOTORIZADO_ESTADO_PENDIENTE = "PENDIENTE";
    public static final String MOTORIZADO_ESTADO_RECHAZADO = "RECHAZADO";
    public static final String MOTORIZADO_ESTADO_CONTRATADO = "CONTRATADO";

}
