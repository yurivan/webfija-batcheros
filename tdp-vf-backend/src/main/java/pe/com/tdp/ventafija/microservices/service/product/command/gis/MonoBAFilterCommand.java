package pe.com.tdp.ventafija.microservices.service.product.command.gis;

import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;

import java.util.Map;

@Component("filterMonoBA")
public class MonoBAFilterCommand implements GisFilterCommand {
    // Filtro para MONO BA (2)
    private static String MONOBA_FILTER = " (OFERTA.targetData = '2' AND CAT.internetTech = '%s' AND CAT.internetSpeed <= %s AND CAT.price <= coalesce(%s, 9999999) )";

    @Override
    public void execute(WSSIGFFTT_FFTT_AVEResult resultGis, Map<String, String> variables, StringBuilder queryBuilder, StringBuilder columnsBuilder) {
        String hasTecnologiaGPON = resultGis.getGPON_TEC();
        String velocidadGPON = resultGis.getGPON_VEL();

        String hasTecnologiaHFC = resultGis.getHFC_TEC();
        String velocidadHFC = resultGis.getHFC_VEL();

        String hasTecnologiaADSL = resultGis.getXDSL_TEC();
        String velocidadADSL = resultGis.getXDSL_VEL_TRM();
        String hasTecnologiaADSLSaturado = resultGis.getXDSL_SAT();

        String price = variables.get("PRICE");

        if ("SI".equals(hasTecnologiaGPON)) {
            String filtro = String.format(MONOBA_FILTER, "FTTH", velocidadGPON, price);
            queryBuilder.append(filtro + " OR ");
        }else if ("SI".equals(hasTecnologiaHFC)) {
            String filtro = String.format(MONOBA_FILTER, "HFC", velocidadHFC, price);
            queryBuilder.append(filtro + " OR ");
        }else if ("SI".equals(hasTecnologiaADSL) && "NO".equals(hasTecnologiaADSLSaturado)) {
            String filtro = String.format(MONOBA_FILTER, "ADSL", velocidadADSL, price);
            queryBuilder.append(filtro + " OR ");
        }
    }
}
