package pe.com.tdp.ventafija.microservices.service.user;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.OauthInstrospectClient;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.sms.RegisterApiSmsRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.sms.RegisterApiSmsResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.sms.SMSClient;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.dao.TokensRepository;
import pe.com.tdp.ventafija.microservices.common.dao.LoginConvergenciaRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.LoginConvergencia;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Tokens;
import pe.com.tdp.ventafija.microservices.common.domain.entity.User;
import pe.com.tdp.ventafija.microservices.common.domain.message.MessageEntities;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.*;
import pe.com.tdp.ventafija.microservices.constants.product.ProductConstants;
import pe.com.tdp.ventafija.microservices.constants.user.UserConstants;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpSalesAgent;
import pe.com.tdp.ventafija.microservices.domain.profile.*;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpSalesAgentRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.UserRepository;
import pe.com.tdp.ventafija.microservices.domain.user.*;
import pe.com.tdp.ventafija.microservices.repository.user.UserDAO;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


@Service
public class UserService {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private MessageUtil messageUtil;
    @Autowired
    private UserDAO dao;
    @Autowired
    private ApiHeaderConfig apiHeaderConfig;
    @Autowired
    private ServiceCallEventsService serviceCallEventsService;
    @Autowired
    private TdpSalesAgentRepository tdpSalesAgentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ParametersRepository parametersRepository;
    @Autowired
    private TokensRepository TokensRepository;
    @Autowired
    private LoginConvergenciaRepository LoginConvergenciaRepository;


    @Value("${security.api.service.url}")
    private String securityApiServiceUrl;
    @Value("${security.api.client.id}")
    private String securityApiClientId;
    @Value("${security.api.client.secret}")
    private String securityApiclientSecret;

    // mensajes
    @Value("${user.mensajes.error.errorGenerico}")
    private String mensajeErrorGenerico;
    @Value("${user.mensajes.registro.ErrorSms}")
    private String mensajeRegistroErrorSms;
    @Value("${user.mensajes.changePassword.IntentoCambioPass}")
    private String mensajeChangePasswordIntentoCambioPass;
    @Value("${user.mensajes.changePassword.PassDuplicada}")
    private String mensajeChangePasswordPassDuplicada;
    @Value("${user.mensajes.changePassword.PassNoValida}")
    private String mensajeChangePasswordPassNoValida;
    @Value("${user.mensajes.resetPassword.ErrorSms}")
    private String mensajeResetPasswordErrorSms;

    public boolean login(String basicAuth) {
        byte[] decoded = Base64.decodeBase64(basicAuth.substring(basicAuth.indexOf(' ') + 1));
        LoginRequest loginRequest = new LoginRequest();
        boolean authorized = false;
        try {
            loginRequest.setCodAtis(new String(decoded, "UTF-8").substring(0, new String(decoded, "UTF-8").indexOf(':')));
            loginRequest.setPassword(Cryptography.encryptMd5(new String(decoded, "UTF-8").substring(new String(decoded, "UTF-8").indexOf(':') + 1)));
            LoginResponseData responseData = dao.loginUsernamePassword(loginRequest);
            if (responseData != null) {
                authorized = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return authorized;
    }

    public Response<LoginResponseData> login(LoginRequest request) throws Exception {
        logger.info("Inicio Login Service");
        Response<LoginResponseData> response = new Response<>();

        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("LOGIN");
        LogVass.serviceResponseHashMap(logger, "Login", "messageList", messageList);
        try {
            MDC.put("username", request.getCodAtis());
            request.setPassword(Cryptography.encryptMd5(request.getPassword()));
            /* Bloqueo de login por fraude */
            // Obtener Objeto de usuario
            logger.info("Login => findOneByIdAndStatus: " + request.getCodAtis() + " - 1");
            User objUser = userRepository.findOneByIdAndStatus(request.getCodAtis(), '1');
            if (objUser == null) {
                String mensajeLoginUsuarioNoRegister = messageList.get("usuarioNoRegister").getMessage();
                response.setResponseCode(messageList.get("usuarioNoRegister").getCode());
                response.setResponseMessage(mensajeLoginUsuarioNoRegister);
                registerServiceCallEvent(mensajeLoginUsuarioNoRegister, "LOGIN", request, response, "OK", "login");
                MDC.remove("username");
                logger.info("Fin Login Service");
                return response;
            }
            LogVass.serviceResponseObject(logger, "Login", "objUser", objUser);
            // Número de intentos fallidos hasta el momento
            Integer nroIntentos = (objUser.getNumeroIntentos() == null ? 0 : objUser.getNumeroIntentos());
            // Fecha del último intento fallido
            Date lastAccess = objUser.getUltimoAcceso();
            // Fecha actual
            Date fechaActual = new Date();
            if (nroIntentos != 0) {
                // Obtener parámetro de número de intentos
                Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "AUTH_PARAMETER", "amount_ret");
                Integer numeroMaximoIntentos = Integer.parseInt(objParameter.getStrValue());
                logger.info("Login => numeroMaximoIntentos: " + numeroMaximoIntentos);

                if (nroIntentos >= numeroMaximoIntentos) {
                    // Obtener parámetro de minutos de bloqueo
                    objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "AUTH_PARAMETER", "min_account_lock");
                    Integer minutosBloqueados = Integer.parseInt(objParameter.getStrValue());
                    logger.info("Login => minutosBloqueados: " + minutosBloqueados);
                    // Obtener la fecha más los minutos bloqueados
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastAccess);
                    c.add(Calendar.MINUTE, minutosBloqueados);
                    lastAccess = c.getTime();

                    logger.info("nroIntentos " + nroIntentos);
                    logger.info("lastAccess " + lastAccess);
                    logger.info("fechaActual " + fechaActual);

                    if (lastAccess.after(fechaActual)) {
                        String mensajeLoginUsuarioLock = messageList.get("usuarioLock").getMessage();
                        response.setResponseCode(messageList.get("usuarioLock").getCode());
                        response.setResponseMessage(mensajeLoginUsuarioLock);
                        registerServiceCallEvent(mensajeLoginUsuarioLock, "LOGIN", request, response, "OK", "login");
                        MDC.remove("username");
                        logger.info("Fin Login Service");
                        return response;
                    } else {
                        nroIntentos = 0;
                    }
                }
            }
            /* --- Sprint 6 --- */
            /* --- Sprint 8 --- */
            if (!request.getImei().equalsIgnoreCase(objUser.getImei())) {
                objUser.setImei(request.getImei());
                try {
                    userRepository.save(objUser);
                    registerServiceCallEvent("Nuevo IMEI", "IMEI", request, objUser, "OK", "login");
                } catch (Exception e) {
                    registerServiceCallEvent(e.getMessage(), "IMEI", request, "null", "OK", "login");

                }
            }

            LoginResponseData responseData = null;
            if (request.getPassword().equalsIgnoreCase(objUser.getPwd())) {
                int days = 35;
                try {
                    Parameters outdatedDay = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "AUTH_PARAMETER", "days_reset_pwd");
                    days = Integer.parseInt(outdatedDay.getStrValue());
                    logger.info("outdatedDay => days: " + days);
                } catch (Exception e) {
                    logger.error("Error outdatedDay DAO.", e);
                }
                try {
                    responseData = new LoginResponseData();
                    responseData.setPhone(objUser.getPhone());
                    Date dateLastUpdateTime = objUser.getLastUpdateTime();
                    Calendar c = Calendar.getInstance();
                    c.setTime(dateLastUpdateTime);
                    c.add(Calendar.DAY_OF_YEAR, days);
                    dateLastUpdateTime = c.getTime();
                    if (dateLastUpdateTime.before(fechaActual)) {
                        responseData.setResetPwd(UserConstants.RESET_TRUE);
                    } else {
                        responseData.setResetPwd(objUser.getResetPwd());
                    }

                } catch (Exception e) {
                    logger.error("Error login DAO.", e);
                }
            }
            LogVass.serviceResponseObject(logger, "Login", "responseData", responseData);

            /* --- Sprint 8 --- */
            if (responseData != null) {
                /* --- Sprint 6 --- Resetear número de intentos */
                objUser.setNumeroIntentos(0);
                objUser.setUltimoAcceso(null);
                userRepository.save(objUser);
                /* --- Sprint 6 --- */
                responseData = dao.loginData(request, responseData);
                /**
                 * Filtro de acceso solo para usuarios de APP
                 *
                 * @author Harold
                 * @date 2019-07-08
                 */
                if (responseData.getTypeFlujoTipificacion().equalsIgnoreCase("PRESENCIAL")) {
                LogVass.serviceResponseObject(logger, "Login", "responseData", responseData);
                response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
                response.setResponseData(responseData);

                //logger.info(ApplicationLogMarker.AUTHENTICATION_LOGIN, "Login => responseData OK");
                registerServiceCallEvent("login", "LOGIN", request, response, "OK", "login");
            } else {
                    String mensajeUsuarioPerfilIncorrecto = messageList.get("perfilIncorrecto").getMessage();
                    response.setResponseCode(messageList.get("perfilIncorrecto").getCode());
                    response.setResponseMessage(mensajeUsuarioPerfilIncorrecto);

                    registerServiceCallEvent(mensajeUsuarioPerfilIncorrecto, "LOGIN", request, response, "OK", "login");
                }
            } else {
                /* --- Sprint 6 --- Añadir un nuevo intento */
                if (lastAccess != null) {
                    // Obtener minutos durante la cual si te equivocas en la contraseña se bloqueará tu usuario
                    Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "AUTH_PARAMETER", "min_for_lock");
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastAccess);
                    c.add(Calendar.MINUTE, Integer.parseInt(objParameter.getStrValue()));
                    lastAccess = c.getTime();

                    if (lastAccess.before(new Date())) {
                        nroIntentos = 0;
                    }
                }
                objUser.setNumeroIntentos(nroIntentos + 1);
                objUser.setUltimoAcceso(new Date());
                userRepository.save(objUser);
                /* --- Sprint 6 --- */
                String mensajeLoginUsuarioIncorrecto = messageList.get("usuarioIncorrecto").getMessage();
                response.setResponseCode(messageList.get("usuarioIncorrecto").getCode());
                response.setResponseMessage(mensajeLoginUsuarioIncorrecto);

                registerServiceCallEvent(mensajeLoginUsuarioIncorrecto, "LOGIN", request, response, "OK", "login");
            }
        } catch (Exception e) {
            logger.error("Error Login Service", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            registerServiceCallEvent(e.getMessage(), "LOGIN", request, response, "ERROR", "login");
        } finally {
            MDC.remove("username");
        }
        logger.info("Fin Login Service");
        return response;
    }

    //glazaror
    public Response<LoginResponseData> validateLogin(LoginRequest request) throws Exception {
        logger.info("Inicio ValidateLogin Service");
        Response<LoginResponseData> response = new Response<>();

        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("LOGIN");
        LogVass.serviceResponseHashMap(logger, "ValidateLogin", "messageList", messageList);
        try {
            MDC.put("username", request.getCodAtis());
            request.setPassword(Cryptography.encryptMd5(request.getPassword()));
            /* --- Sprint 6 --- Bloqueo de login por fraude */
            // Obtener Objeto de usuario
            User objUser = userRepository.findOne(request.getCodAtis());
            if (objUser == null) {
                /* --- Sprint 6 --- */
                String mensajeLoginUsuarioNoRegister = messageList.get("usuarioNoRegister").getMessage();
                response.setResponseCode(messageList.get("usuarioNoRegister").getCode());
                response.setResponseMessage(mensajeLoginUsuarioNoRegister);
                registerServiceCallEvent(mensajeLoginUsuarioNoRegister, "LOGIN", request, response, "OK", "login");
                MDC.remove("username");
                logger.info("Fin Login Service");
                return response;
            }
            LogVass.serviceResponseObject(logger, "Login", "objUser", objUser);
            // Número de intentos fallidos hasta el momento
            Integer nroIntentos = (objUser.getNumeroIntentos() == null ? 0 : objUser.getNumeroIntentos());
            // Fecha del último intento fallido
            Date lastAccess = objUser.getUltimoAcceso();
            if (nroIntentos != 0) {
                // Obtener parámetro de número de intentos
                Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "AUTH_PARAMETER", "amount_ret");
                Integer numeroMaximoIntentos = Integer.parseInt(objParameter.getStrValue());
                logger.info("Login => numeroMaximoIntentos: " + numeroMaximoIntentos);

                if (nroIntentos >= numeroMaximoIntentos) {
                    // Fecha actual
                    Date fechaActual = new Date();
                    // Obtener parámetro de minutos de bloqueo
                    objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "AUTH_PARAMETER", "min_account_lock");
                    Integer minutosBloqueados = Integer.parseInt(objParameter.getStrValue());
                    logger.info("Login => minutosBloqueados: " + minutosBloqueados);
                    // Obtener la fecha más los minutos bloqueados
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastAccess);
                    c.add(Calendar.MINUTE, minutosBloqueados);
                    lastAccess = c.getTime();

                    logger.info("nroIntentos " + nroIntentos);
                    logger.info("lastAccess " + lastAccess);
                    logger.info("fechaActual " + fechaActual);

                    if (lastAccess.after(fechaActual)) {
                        String mensajeLoginUsuarioLock = messageList.get("usuarioLock").getMessage();
                        response.setResponseCode(messageList.get("usuarioLock").getCode());
                        response.setResponseMessage(mensajeLoginUsuarioLock);
                        registerServiceCallEvent(mensajeLoginUsuarioLock, "LOGIN", request, response, "OK", "login");
                        MDC.remove("username");
                        logger.info("Fin Login Service");
                        return response;
                    } else {
                        nroIntentos = 0;
                    }
                }
            }
            /* --- Sprint 6 --- */
            LoginResponseData responseData = dao.loginUsernamePassword(request);
            LogVass.serviceResponse(logger, "ValidateLogin", responseData);

            if (responseData != null) {
                /* --- Sprint 6 --- Resetear número de intentos */
                objUser.setNumeroIntentos(0);
                objUser.setUltimoAcceso(null);
                userRepository.save(objUser);
                /* --- Sprint 6 --- */
                responseData = dao.loginData(request, responseData);
                LogVass.serviceResponseObject(logger, "ValidateLogin", "responseData", responseData);
                response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
                response.setResponseData(responseData);

                //logger.info(ApplicationLogMarker.AUTHENTICATION_LOGIN, "login");
                registerServiceCallEvent("login", "LOGIN", request, response, "OK", "login");
            } else {
                /* --- Sprint 6 --- Añadir un nuevo intento */
                if (lastAccess != null) {
                    // Obtener minutos durante la cual si te equivocas en la contraseña se bloqueará tu usuario
                    Parameters objParameter = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG", "AUTH_PARAMETER", "min_for_lock");
                    Calendar c = Calendar.getInstance();
                    c.setTime(lastAccess);
                    c.add(Calendar.MINUTE, Integer.parseInt(objParameter.getStrValue()));
                    lastAccess = c.getTime();

                    if (lastAccess.before(new Date())) {
                        nroIntentos = 0;
                    }
                }
                objUser.setNumeroIntentos(nroIntentos + 1);
                objUser.setUltimoAcceso(new Date());
                userRepository.save(objUser);
                /* --- Sprint 6 --- */
                String mensajeLoginUsuarioIncorrecto = messageList.get("usuarioIncorrecto").getMessage();
                response.setResponseCode(messageList.get("usuarioIncorrecto").getCode());
                response.setResponseMessage(mensajeLoginUsuarioIncorrecto);

                //logger.info(ApplicationLogMarker.AUTHENTICATION_BAD_PASSWD, "failed login");
                registerServiceCallEvent(mensajeLoginUsuarioIncorrecto, "LOGIN", request, response, "OK", "login");
            }
        } catch (Exception e) {
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            registerServiceCallEvent(e.getMessage(), "LOGIN", request, response, "ERROR", "login");
            logger.error("Error ValidateLogin Service", e);
        } finally {
            MDC.remove("username");
        }

        logger.info("Fin ValidateLogin Service");
        return response;
    }

    public Response<String> register(RegisterRequest request) {
        logger.info("Inicio Register Service");
        Response<String> response = new Response<>();

        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("REGISTRAR");
        LogVass.serviceResponseHashMap(logger, "REGISTRAR", "messageList", messageList);
        try {
            if (!request.getPwd().equalsIgnoreCase(request.getRepeatPwd())) {
                response.setResponseCode(messageList.get("usuarioContraseniaNoCoincide").getCode());
                response.setResponseMessage(messageList.get("usuarioContraseniaNoCoincide").getMessage());
                logger.info("Fin Register Service");
                return response;
            }

            boolean existsSalesAgent = true;
            TdpSalesAgent tdpSalesAgent = tdpSalesAgentRepository.findOneByCodigoAtis(request.getCodAtis());
            if (tdpSalesAgent == null)
                existsSalesAgent = false;

            logger.info("Register => existsSalesAgent: " + existsSalesAgent);
            if (existsSalesAgent) {
                User objUser = userRepository.findOne(request.getCodAtis());
                LogVass.serviceResponseObject(logger, "Register", "objUser", objUser);
                Date ahora = new Date();
                if (objUser == null) {
                    objUser = new User();
                    objUser.setId(request.getCodAtis());
                    objUser.setRegisteredTime(ahora);
                    objUser.setPreviousPwd("");
                    objUser.setNumeroIntentos(0);
                    objUser.setUltimoAcceso(null);
                }
                objUser.setPwd(Cryptography.encryptMd5(request.getPwd()));
                objUser.setPhone(request.getPhone());
                objUser.setLastUpdateTime(ahora);
                objUser.setImei(request.getImei());
                objUser.setResetPwd("0");
                objUser.setStatus('1');
                try {
                    userRepository.save(objUser);

                    logger.info("Register => OK");
                    String mensajeUsuarioRegistrado = messageList.get("usuarioRegistrado").getMessage();
                    response.setResponseCode(messageList.get("usuarioRegistrado").getCode());
                    response.setResponseMessage(mensajeUsuarioRegistrado);
                    registerServiceCallEvent(mensajeUsuarioRegistrado, "REGISTER", request, response, "OK", "register");
                } catch (Exception e) {
                    logger.error("Error Register Service => userRepository ", e);
                    String mensajeUsuarioNoSePudoRegistrar = messageList.get("usuarioNoSePudoRegistrar").getMessage();
                    response.setResponseCode(messageList.get("usuarioNoSePudoRegistrar").getCode());
                    response.setResponseMessage(mensajeUsuarioNoSePudoRegistrar);
                    registerServiceCallEvent(mensajeUsuarioNoSePudoRegistrar, "REGISTER", request, response, "OK", "register");
                }
            } else {
                String mensajeUsuarioNoValido = messageList.get("usuarioNoActivoMaestra").getMessage();
                response.setResponseCode(messageList.get("usuarioNoActivoMaestra").getCode());
                response.setResponseMessage(mensajeUsuarioNoValido);
                registerServiceCallEvent(mensajeUsuarioNoValido, "REGISTER", request, response, "OK", "register");
            }
        } catch (Exception e) {
            logger.error("Error Register Service", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            registerServiceCallEvent(e.getMessage(), "REGISTER", request, response, "ERROR", "user/register");

            logger.info("Fin Register Service");
            return response;
        }

        logger.info("Fin Register Service");
        return response;
    }

    public Response<String> resetPassword(ResetPasswordRequest request) {
        logger.info("Inicio ResetPassword Service");
        Response<String> response = new Response<>();

        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("RESTABLECER CONTRASEÑA");
        LogVass.serviceResponseHashMap(logger, "RESTABLECER CONTRASEÑA", "messageList", messageList);
        try {
            if (!request.getPwd().equalsIgnoreCase(request.getRepeatPwd())) {
                response.setResponseCode(messageList.get("resetearContraseniaNoCoincide").getCode());
                response.setResponseMessage(messageList.get("resetearContraseniaNoCoincide").getMessage());
                logger.info("Fin ResetPassword Service");
                return response;
            }

            boolean existsSalesAgent = true;
            TdpSalesAgent tdpSalesAgent = tdpSalesAgentRepository.findOneByCodigoAtis(request.getCodAtis());
            if (tdpSalesAgent == null)
                existsSalesAgent = false;

            logger.info("ResetPassword => existsSalesAgent: " + existsSalesAgent);
            if (existsSalesAgent) {

                boolean existsUser = true;
                User objUser = userRepository.findOne(request.getCodAtis());
                LogVass.serviceResponseObject(logger, "Register", "objUser", objUser);
                if (objUser == null)
                    existsUser = false;

                logger.info("ResetPassword => existsUser: " + existsUser);
                if (existsUser) {
                    Date lastAccess = objUser.getLastUpdateTime();
                    Date fechaActual = new Date();

                    Calendar c = Calendar.getInstance();
                    c.setTime(lastAccess);
                    c.add(Calendar.SECOND, UserConstants.UPDATE_UNABLE_TIME);
                    lastAccess = c.getTime();

                    if (lastAccess.after(fechaActual)) {
                        String mensajeResetPasswordClaveReciente = messageList.get("resetearClaveReciente").getMessage();
                        response.setResponseCode(messageList.get("resetearClaveReciente").getCode());
                        response.setResponseMessage(mensajeResetPasswordClaveReciente);
                        registerServiceCallEvent(mensajeResetPasswordClaveReciente, "PWRESET", request, response, "OK", "register");
                        logger.info("ResetPassword => " + mensajeResetPasswordClaveReciente);
                        logger.info("Fin ResetPassword Service");
                        return response;
                    }
                    String newPassword = Cryptography.encryptMd5(request.getPwd());
                    logger.info("ResetPassword => PASSWORD: " + request.getPwd());

                    if (objUser.getPreviousPwd() != null && !Constants.EMPTY.equals(objUser.getPreviousPwd())) {
                        objUser.setPreviousPwd(objUser.getPwd() + Constants.DELIMITER_VERTICAL_BAR + objUser.getPreviousPwd());
                    } else {
                        objUser.setPreviousPwd(objUser.getPwd());
                    }

                    LinkedList<String> listPreviousPwd = new LinkedList<>(Arrays.asList(objUser.getPreviousPwd().split(Pattern.quote(Constants.DELIMITER_VERTICAL_BAR))));
                    logger.info("ResetPassword => listPreviousPwd: " + listPreviousPwd.size());
                    boolean validPwd = true;
                    for (String oldPwd : listPreviousPwd) {
                        if (newPassword.equals(oldPwd)) {
                            validPwd = false;
                            break;
                        }
                    }
                    if (!validPwd) {
                        String mensajeResetPasswordContrasenaYaExiste = messageList.get("resetearContrasenaYaExiste").getMessage();
                        response.setResponseCode(messageList.get("resetearContrasenaYaExiste").getCode());
                        response.setResponseMessage(mensajeResetPasswordContrasenaYaExiste);
                        registerServiceCallEvent(mensajeResetPasswordContrasenaYaExiste, "PWRESET", request, response, "OK", "register");
                        logger.info("ResetPassword => " + mensajeResetPasswordContrasenaYaExiste);
                    } else {
                        if (listPreviousPwd.size() > Constants.MAXIMUM_PASSWORD_STORAGE) {
                            while (listPreviousPwd.size() > Constants.MAXIMUM_PASSWORD_STORAGE) {
                                listPreviousPwd.removeLast();
                                logger.info("ResetPassword => removeLast: " + listPreviousPwd.getLast());
                            }
                            objUser.setPreviousPwd(String.join(Constants.DELIMITER_VERTICAL_BAR, listPreviousPwd));
                        }

                        objUser.setPwd(newPassword);
                        objUser.setResetPwd("0");
                        //objUser.setImei(request.getImei());
                        objUser.setLastUpdateTime(new Date());
                        try {
                            userRepository.save(objUser);
                            String mensajeResetPasswordOk = messageList.get("resetearOK").getMessage();
                            response.setResponseCode(messageList.get("resetearOK").getCode());
                            response.setResponseMessage(mensajeResetPasswordOk);
                            registerServiceCallEvent(mensajeResetPasswordOk, "PWRESET", request, response, "OK", "register");
                            logger.info("ResetPassword => " + mensajeResetPasswordOk);
                        } catch (Exception e) {
                            String mensajeResetPasswordNoSePudoResetear = messageList.get("resetearNoSePudoResetear").getMessage();
                            response.setResponseCode(messageList.get("resetearNoSePudoResetear").getCode());
                            response.setResponseMessage(mensajeResetPasswordNoSePudoResetear);
                            registerServiceCallEvent(mensajeResetPasswordNoSePudoResetear, "PWRESET", request, response, "ERROR", "register");
                            logger.info("ResetPassword => " + mensajeResetPasswordNoSePudoResetear);

                        }
                    }
                } else {
                    //Reset usuario no existe
                    String mensajeResetPasswordUsuarioNoExiste = messageList.get("resetearUsuarioNoEncontrado").getMessage();
                    response.setResponseCode(messageList.get("resetearUsuarioNoEncontrado").getCode());
                    response.setResponseMessage(mensajeResetPasswordUsuarioNoExiste);
                    registerServiceCallEvent(mensajeResetPasswordUsuarioNoExiste, "PWRESET", request, response, "OK", "register");
                    logger.info("ResetPassword => " + mensajeResetPasswordUsuarioNoExiste);
                }
            } else {
                //Reset vendedor no existe
                String mensajeResetPasswordVendedorNoExiste = messageList.get("resetearNoActivoMaestra").getMessage();
                response.setResponseCode(messageList.get("resetearNoActivoMaestra").getCode());
                response.setResponseMessage(mensajeResetPasswordVendedorNoExiste);
                registerServiceCallEvent(mensajeResetPasswordVendedorNoExiste, "PWRESET", request, response, "OK", "register");
                logger.info("ResetPassword => " + mensajeResetPasswordVendedorNoExiste);
            }

        } catch (
                Exception e)

        {
            logger.error("Error ResetPassword Service.", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            registerServiceCallEvent(e.getMessage(), "PWRESET", request, response, "ERROR", "user/reset");

            logger.info("Fin ResetPassword Service");
            return response;
        }

        logger.info("Fin ResetPassword Service");
        return response;
    }

    public void saveLog(LogUserRequest request) {
        logger.info("Inicio saveLog Service");

        try {
            dao.insertSaveLog(request);

        } catch (Exception e){
            logger.info("error insert "+e);
        }


        logger.info("Fin saveLog Service");

    }


    public Response<String> changePassword(ChangePasswordRequest request) {
        logger.info("Inicio ChangePassword Service");
        Response<String> response = new Response<>();
        //ChangePasswordResponseData responseData;

        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("CAMBIAR CONTRASEÑA");
        LogVass.serviceResponseHashMap(logger, "CAMBIAR CONTRASEÑA", "messageList", messageList);
        try {
            String newPassword = Cryptography.encryptMd5(request.getNewpwd());
            User objUser = userRepository.findOneByIdAndPwdAndStatus(request.getCodAtis(), Cryptography.encryptMd5(request.getPwd()), '1');
            LogVass.serviceResponseObject(logger, "ChangePassword", "objUser", objUser);
            if (objUser != null) {
                Date lastAccess = objUser.getLastUpdateTime();
                Date fechaActual = new Date();

                Calendar c = Calendar.getInstance();
                c.setTime(lastAccess);
                c.add(Calendar.SECOND, UserConstants.UPDATE_UNABLE_TIME);
                lastAccess = c.getTime();
                if (lastAccess.after(fechaActual)) {
                    String mensajeChangePasswordClaveReciente = messageList.get("cambiarClaveReciente").getMessage();
                    response.setResponseCode(messageList.get("cambiarClaveReciente").getCode());
                    response.setResponseMessage(mensajeChangePasswordClaveReciente);
                    registerServiceCallEvent(mensajeChangePasswordClaveReciente, "PWCHANGE", request, response, "OK", "password_change");
                    logger.info("ChangePassword => " + mensajeChangePasswordClaveReciente);
                    logger.info("Fin ChangePassword Service");
                    return response;
                } else {
                    if (objUser.getPreviousPwd() != null && !Constants.EMPTY.equals(objUser.getPreviousPwd())) {
                        objUser.setPreviousPwd(objUser.getPwd() + Constants.DELIMITER_VERTICAL_BAR + objUser.getPreviousPwd());
                    } else {
                        objUser.setPreviousPwd(objUser.getPwd());
                    }

                    LinkedList<String> listPreviousPwd = new LinkedList<>(Arrays.asList(objUser.getPreviousPwd().split(Pattern.quote(Constants.DELIMITER_VERTICAL_BAR))));
                    logger.info("ChangePassword => listPreviousPwd: " + listPreviousPwd.size());
                    boolean validPwd = true;
                    for (String oldPwd : listPreviousPwd) {
                        if (newPassword.equals(oldPwd)) {
                            validPwd = false;
                            break;
                        }
                    }
                    if (!validPwd) {
                        String mensajeChangePasswordContrasenaYaExiste = messageList.get("cambiarContrasenaYaExiste").getMessage();
                        response.setResponseCode(messageList.get("cambiarContrasenaYaExiste").getCode());
                        response.setResponseMessage(mensajeChangePasswordContrasenaYaExiste);
                        registerServiceCallEvent(mensajeChangePasswordContrasenaYaExiste, "PWCHANGE", request, response, "OK", "password_change");
                        logger.info("ChangePassword => " + mensajeChangePasswordContrasenaYaExiste);
                    } else {
                        if (listPreviousPwd.size() > Constants.MAXIMUM_PASSWORD_STORAGE) {
                            while (listPreviousPwd.size() > Constants.MAXIMUM_PASSWORD_STORAGE) {
                                listPreviousPwd.removeLast();
                                logger.info("ChangePassword => removeLast: " + listPreviousPwd.getLast());
                            }
                            objUser.setPreviousPwd(String.join(Constants.DELIMITER_VERTICAL_BAR, listPreviousPwd));
                        }

                        objUser.setPwd(newPassword);
                        objUser.setResetPwd("0");
                        objUser.setLastUpdateTime(new Date());
                        try {
                            userRepository.save(objUser);
                            String mensajeCambiarPasswordOk = messageList.get("cambiarOK").getMessage();
                            response.setResponseCode(messageList.get("cambiarOK").getCode());
                            response.setResponseMessage(mensajeCambiarPasswordOk);
                            registerServiceCallEvent(mensajeCambiarPasswordOk, "PWCHANGE", request, response, "OK", "password_change");
                            logger.info("ChangePassword => " + mensajeCambiarPasswordOk);
                        } catch (Exception e) {
                            String mensajeCambiarPasswordNoSePudoCambiar = messageList.get("cambiarNoSePudoCambiar").getMessage();
                            response.setResponseCode(messageList.get("cambiarNoSePudoCambiar").getCode());
                            response.setResponseMessage(mensajeCambiarPasswordNoSePudoCambiar);
                            registerServiceCallEvent(mensajeCambiarPasswordNoSePudoCambiar, "PWCHANGE", request, response, "ERROR", "password_change");
                            logger.info("ChangePassword => " + mensajeCambiarPasswordNoSePudoCambiar);
                        }
                    }
                }
            } else {
                //No se pudo realizar el cambio de clave
                String mensajeCambiarPasswordVendedorNoExiste = messageList.get("cambiarNoCoincideContrasena").getMessage();
                response.setResponseCode(messageList.get("cambiarNoCoincideContrasena").getCode());
                response.setResponseMessage(mensajeCambiarPasswordVendedorNoExiste);
                registerServiceCallEvent(mensajeCambiarPasswordVendedorNoExiste, "PWCHANGE", request, response, "OK", "password_change");
                logger.info("ResetPassword => " + mensajeCambiarPasswordVendedorNoExiste);
            }
        } catch (Exception e) {
            logger.error("Error ChangePassword Service", e);
            response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseMessage(messageList.get("errorGeneric").getMessage());
            registerServiceCallEvent(e.getMessage(), "PWCHANGE", request, response, "ERROR", "password_change");
            logger.info("Fin ChangePassword Service");
            return response;
        }

        logger.info("Fin ChangePassword Service");
        return response;
    }


    public Response<String> changePasswordWeb(ChangePasswordWebRequest request) {
        logger.info("Inicio ChangePasswordWeb Service");
        Response<String> response = new Response<>();
        ChangePasswordWebResponseData responseData;
        try {
            request.setPwd(Cryptography.encryptMd5(request.getPwd()));
            responseData = dao.validatePasswordWeb(request);
            if (responseData == null) {
                response.setResponseCode("2");
                response.setResponseMessage("No existe el CODIGO ATIS " + request.getCodAtis());
                return response;
            }
            //LogVass.serviceResponseObject(logger, "changePasswordWeb", "responseData", responseData);
            request.setPreviousPwd("");
            request.setNewpwd(Cryptography.encryptMd5(request.getNewpwd()));
            request.setResetPwd(UserConstants.RESET_FALSE);
            dao.changePasswordWeb(request);
            response.setResponseCode(UserConstants.UPDATE_RESPONSE_CODE_OK);
            response.setResponseData(UserConstants.UPDATE_RESPONSE_DATA);
        } catch (Exception e) {
            response.setResponseCode("2");
            response.setResponseMessage("Ocurrio un error.");
            logger.error("Error ChangePasswordWeb Service", e);
            logger.info("Fin ChangePasswordWeb Service");
            return response;
        }
        logger.info("Fin ChangePasswordWeb Service");
        return response;
    }
    public Response<ResponseAccessData> getAccessByNivel( int nivel, String canal){
        logger.info("Inicio getAccessByNivel Service");
        Response<ResponseAccessData> response = new Response<>();
        ResponseAccessData data = new ResponseAccessData();

        try {
            data=dao.getAccessByNivel(nivel, canal);
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
        } catch (Exception e) {
            logger.error("Error getAccessByNivel Service URL servicio", e);
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
        }

        response.setResponseData(data);
        return response;
    }

    public Response<ConfigResponseData> loadUserData(String token) {
        logger.info("Inicio LoadUserData Service");

        Map<String, String> request = new HashMap<>();
        request.put("token_type_hint", "access_token");
        request.put("token", token);
        Response<ConfigResponseData> response = new Response<>();
        LoginResponseData loginResponseData = new LoginResponseData();
        List<ConfigParameterResponseData> configParameterResponseData = new ArrayList<>();
        ConfigResponseData responseData = new ConfigResponseData();
        try {
            OauthInstrospectClient client = new OauthInstrospectClient(securityApiServiceUrl, securityApiClientId, securityApiclientSecret);
            Map<String, String> responseMap = client.sendData(request);
            String username = responseMap.get("username");
            LoginRequest loginRequest = new LoginRequest();
            loginRequest.setCodAtis(username);
            loginResponseData = dao.loginData(loginRequest, loginResponseData);
            configParameterResponseData = dao.configParameter();
            responseData.setUser(loginResponseData);
            responseData.setParameter(configParameterResponseData);
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
            response.setResponseData(responseData);
        } catch (URISyntaxException e) {
            logger.error("Error LoadUserData Service URL servicio AUTH", e);
        } catch (ApiClientException e) {
            logger.error("Error LoadUserData Service consulta HTTP", e);
        }
        logger.info("Fin LoadUserData Service");
        return response;
    }

    public Response<String> registerWeb(String tipoDoc, String dni, String codAtis, String codCms, String password) {
        logger.info("Inicio RegisterWeb Service");
        Response<String> response = new Response<>();
        boolean valid = dao.validateVendor(dni, codAtis, codCms);
        logger.info("RegisterWeb => valid: " + valid);
        if (valid) {
            RegisterRequest registrationRequest = new RegisterRequest(tipoDoc,dni, codAtis);
            try {
                registrationRequest.setPwd(Cryptography.encryptMd5(password));
            } catch (NoSuchAlgorithmException e) {
                logger.error("Error al genera clave", e);
                registrationRequest.setPwd("dfacb5ef30932c35155cab2bdaac5893");
            } // TODO: generate the password and send it to the supervisor so that he can give it to the sales
            //20180105 - Se añadió para complementar el flujo.
            registrationRequest.setResetPwd(UserConstants.RESET_TRUE);
            LogVass.serviceResponseObject(logger, "RegisterWeb", "registrationRequest", registrationRequest);
            dao.registerUser(registrationRequest);
            response.setResponseCode(UserConstants.REGISTER_RESPONSE_CODE_OK);
            response.setResponseData(UserConstants.REGISTER_RESPONSE_DATA);
        } else {
            response.setResponseCode(UserConstants.REGISTER_RESPONSE_CODE_ERROR);
            response.setResponseMessage(UserConstants.REGISTER_RESPONSE_MESSAGE);
        }
        logger.info("Fin RegisterWeb Service");
        return response;
    }

    private ApiResponse<RegisterApiSmsResponseBody> getSMS(RegisterApiSmsRequestBody apiSmsRequestBody) throws
            ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getEnvioSmsEnvioUri())
                .setApiId(apiHeaderConfig.getEnvioSmsEnvioApiId())
                .setApiSecret(apiHeaderConfig.getEnvioSmsEnvioApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION)
                .build();

        SMSClient smsClient = new SMSClient(config);
        ClientResult<ApiResponse<RegisterApiSmsResponseBody>> clientResult = smsClient.post(apiSmsRequestBody);

        ApiResponse<RegisterApiSmsResponseBody> apiResponse = clientResult.getResult();
        serviceCallEventsService.registerEvent(clientResult.getEvent());

        if (!clientResult.isSuccess()) {
            throw clientResult.getE();
        }
        return apiResponse;
    }

    private void registerServiceCallEvent(String msg, String serviceCode, Object serviceRequest, Object
            serviceResponse, String result, String url) {
        ServiceCallEvent event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
        ObjectMapper mapper = new ObjectMapper();
        String requestJson = "N/A";
        String responseJson = "N/A";

        try {
            requestJson = mapper.writeValueAsString(serviceRequest);
        } catch (Exception e) {
        }
        try {
            responseJson = mapper.writeValueAsString(serviceResponse);
        } catch (Exception e) {
        }

        event.setMsg(msg);
        event.setServiceCode(serviceCode);
        event.setServiceRequest(requestJson);
        event.setServiceResponse(responseJson);
        event.setResult(result);
        event.setServiceUrl(url);
        try {
            serviceCallEventsService.registerEvent(event);
        } catch (Exception e) {
            logger.error("error registrando evento", e);
        }
    }

    /**
     * Valida que el dni y codigo atis se encuentren asociados para proceder a restablecer la contraseña
     *
     * @author glazaror
     **/
    public Response<String> resetPasswordWeb(ResetPasswordRequest request) {
        logger.info("Inicio ResetPasswordWeb Service");
        Response<String> response = new Response<>();
        try {

            boolean result = dao.validateVendorByAtis(request.getDni(), request.getCodAtis());
            if (result) {
                dao.resetPasswordWeb(request);
                response.setResponseCode("0");
                response.setResponseMessage("Contraseña restablecida correctamente");
            } else {
                response.setResponseCode("2");
                response.setResponseMessage("El código ATIS no existe o no esta asociado al DNI");
            }
        } catch (Exception e) {
            response.setResponseCode("2");
            response.setResponseMessage("Ocurrio un error.");
            logger.info("Error ResetPasswordWeb Service", e);
        }
        logger.info("Fin ResetPasswordWeb Service");
        return response;
    }

    //Sprint 7... se adiciona parametro tipoAplicacion
    public Response<ConfigResponseData> loadUserDataDetails(String token, String tipoAplicacion) {
        logger.info("Inicio LoadUserDataDetails Service");

        Map<String, String> request = new HashMap<>();
        request.put("token_type_hint", "access_token");
        request.put("token", token);
        Response<ConfigResponseData> response = new Response<>();
        LoginResponseData loginResponseData = new LoginResponseData();
        List<ConfigParameterResponseData> configParameterResponseData = new ArrayList<>();
        ConfigResponseData responseData = new ConfigResponseData();

        Map<String, Object> datosUsuario = LoginUtil.obtenerUsuario(token);
        String username = (String) datosUsuario.get("username");
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setCodAtis(username);
        loginResponseData = dao.loginData(loginRequest, loginResponseData);
        LogVass.serviceResponseObject(logger, "LoadUserDataDetails", "loginResponseData", loginResponseData);

        tipoAplicacion = "HERRAMIENTACONFIGURABLE" + tipoAplicacion;

        configParameterResponseData = dao.configParameter(tipoAplicacion);
        LogVass.serviceResponseObject(logger, "LoadUserDataDetails", "configParameterResponseData", configParameterResponseData);
        responseData.setUser(loginResponseData);
        responseData.setParameter(configParameterResponseData);
        response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
        response.setResponseData(responseData);

        logger.info("Fin LoadUserDataDetails Service");
        return response;
    }

    public Response<ConvergenciaResponse> guardarToken(ConvergenciaRequest req){
        logger.info("Inicio guardarToken Service");
        Response<ConvergenciaResponse> response = new Response<ConvergenciaResponse>();
        ConvergenciaResponse data = new ConvergenciaResponse();
        try {

            TripleDES cod = new TripleDES("mikey");
            String time = req.getId_transaccion();
            String token=cod.harden(req.getTipdoc()
                                    +','+req.getNumero_de_doc()
                                    +','+req.getCodatis()
                                    +','+time
                                    +','+req.getCanal_de_venta()
                                    +','+req.getPunto_de_venta()
                                    +','+req.getAsesor_nombre()
                                    +','+req.getEntidad()
                                    +','+req.getCanalequivcampania()
                                  );
            logger.info(token);
            data.setToken(token);
            Tokens new_token = new Tokens();
            new_token.setToken(token);
            new_token.setCreated_at(time);
            new_token.setExpire_at((new Timestamp(System.currentTimeMillis()+300000)).toString());
            TokensRepository.save(new_token);
            response.setResponseData(data);
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
            logger.info("Exito generarToken Service");
        }catch (Exception e){
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.info("ERROR generarToken Service");
}
        return response;
    }

    public Response<ConvergenciaRequest> desencriptarToken(ConvergenciaResponse req){
        logger.info("Inicio desencriptarToken Service");
        Response<ConvergenciaRequest> response = new Response<ConvergenciaRequest>();
        ConvergenciaRequest resp = new ConvergenciaRequest();
        String text = null;
        String[] data = null;
        try {
            TripleDES cod = new TripleDES("mikey");
            Tokens finded = TokensRepository.findToken(req.getToken());
            logger.info("Se busco el token "+finded.getToken());
            if(finded==null){
                response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
                response.setResponseMessage("El token no existe");
            }else {
                if(finded.getUsed_at()!=null){
                    response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
                    response.setResponseMessage("El token ha sido usado");
                }else{
                    String used_at = resp.getId_transaccion();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    logger.info("Expira"+finded.getExpire_at());
                    logger.info("Son las "+used_at);
                    logger.info(format.parse(finded.getExpire_at()).compareTo(format.parse(used_at)));
                    if (format.parse(finded.getExpire_at()).compareTo(format.parse(used_at))<0){
                        response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
                        response.setResponseMessage("El token ha expirado");
                    }else{
                        finded.setUsed_at(used_at);
                TokensRepository.saveAndFlush(finded);
                text=cod.soften(req.getToken());
                data=text.split(",");
                resp.setTipdoc(data[0]);
                resp.setNumero_de_doc(data[1]);
                resp.setCodatis(data[2]);
                resp.setCanal_de_venta(data[4]);
                resp.setPunto_de_venta(data[5]);
                resp.setAsesor_nombre(data[6]);
                resp.setEntidad(data[7]);
                resp.setCanalequivcampania(data[8]);
                response.setResponseData(resp);
                response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
                    }
                }
            }
            logger.info("Exito desencriptarToken Service");
        }catch (Exception e){
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.info("ERROR desencriptarToken Service");
        }
        return response;
    }

    public Response<LoginConvergenciaResponse> getLoginConvergencia(String pto_venta){
        logger.info("Inicio getLoginConvergencia Service");
        Response<LoginConvergenciaResponse> response = new Response<>();
        LoginConvergenciaResponse res = new LoginConvergenciaResponse();
        try {
            Parameters parameters = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","VALUES","flag_convergencia");

            if(parameters.getStrValue().equals("false")){
                LoginConvergencia finded = LoginConvergenciaRepository.findAccessByPtoVenta(pto_venta);
                if (finded!=null){
                    res.setEnable(finded.isF_enable());
                    response.setResponseData(res);
                    response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);
                }else{
                    response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
                    response.setResponseMessage("No existe la configuracion para este punto de venta");
                }
            }else {
                response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
                response.setResponseMessage("Esta desactivado globalmente");
            }

        }catch (Exception e){
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            logger.info("ERROR getLoginConvergencia Service");
        }
        return response;
    }


    public Response<ParameterResponse> getReintentosReniec(){
        logger.info("Inicio getLoginMT Service");
        Response<ParameterResponse> response = new Response<>();
        ParameterResponse res = new ParameterResponse();
        Parameters parameters = parametersRepository.findOneByDomainAndCategoryAndElement("CONFIG","VALUES","reintentos.reniec");
        if(parameters.getStrValue()==null){
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_ERROR);
            response.setResponseMessage("Esta desactivado globalmente");
        }else {
            res.setValue(parameters.getStrValue());
            response.setResponseData(res);
            response.setResponseCode(UserConstants.LOGIN_RESPONSE_CODE_OK);

        }
        return response;
    }
}

