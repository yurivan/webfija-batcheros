package pe.com.tdp.ventafija.microservices.service.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.NumberUtils;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpSva;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpSvaRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SvaService {
    private static final Logger logger = LogManager.getLogger(SvaService.class);
    @Value("${datasource.query.select}")
    private String MY_SQL_QUERY_WHERE;
    @Autowired
    private TdpSvaRepository tdpSvaRepository;
    @PersistenceContext
    private EntityManager em;

    public TdpSva loadTdpSva(Integer id) {
        return tdpSvaRepository.findOne(id);
    }

    public Map<String, String> loadSva(List<Integer> ids) {
        Map<String, String> result = new HashMap<>();
        try {
            String paramValue = ids.stream().map(Object::toString).collect(Collectors.joining(","));

            String sql = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "s.unit, p.strValue")
                    .replace(Constants.MY_SQL_TABLE, "tdp_sva s left join ibmx_a07e6d02edaf552.parameters p on p.element = s.code and p.domain = 'TRANSLATE' and p.category = 'FB2SVA'")
                    .replace(Constants.MY_SQL_WHERE, "s.id in (" + paramValue + ")");

            LogVass.daoQuery(logger, sql);
            Query q = em.createNativeQuery(sql);
            List<Object[]> queryResult = q.getResultList();
            for (Object[] row : queryResult) {
                String unit = (String) row[0];
                String value = (String) row[1];
                if (value != null) {
                    if (result.get(value) != null && value.equalsIgnoreCase("blockTV")) {
                        result.put(value, result.get(value) + " + " + unit);
                    } else {
                        result.put(value, unit);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error loadSva DAO.", e);
        }
        return result;
    }


    public List<TdpSva> getSva(List<Integer> ids, String idCatalog) {
        List<TdpSva> result = new ArrayList<>();
        try {
            String paramValue = ids.stream().map(Object::toString).collect(Collectors.joining(","));

            String sva = "s.cost";
            if (idCatalog.length() > 2)
                sva = "case when s.productcode in (" + idCatalog + ") then 0.0 else s.cost end";
            String svaIds = "s.id = 0";
            if (paramValue.length() > 0)
                svaIds = "s.id in (" + paramValue + ")";
            String sql = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "s.code, s.description, s.unit, " + sva + " as cost, s.sva_codigo ")
                    .replace(Constants.MY_SQL_TABLE, "tdp_sva s")
                    .replace(Constants.MY_SQL_WHERE, svaIds);

            LogVass.daoQueryDAO(logger, "getSva", "sql", sql);
            Query q = em.createNativeQuery(sql);
            List<Object[]> queryResult = q.getResultList();
            for (Object[] row : queryResult) {
                TdpSva tdpSva = new TdpSva();
                tdpSva.setCode((String) row[0]);
                tdpSva.setDescription(NumberUtils.isNumber((String) row[2]) ? (String) row[1] : (String) row[2]);
                tdpSva.setCost((BigDecimal) row[3]);
                tdpSva.setUnit(NumberUtils.isNumber((String) row[2]) ? (String) row[2] : "1");
                tdpSva.setSva_Codigo((String) row[4]);
                result.add(tdpSva);
            }
        } catch (Exception e) {
            logger.error("Error loadSva DAO.", e);
        }
        return result;
    }
}
