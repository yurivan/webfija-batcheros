package pe.com.tdp.ventafija.microservices.service.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.common.dao.AddressComplementRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.AddressComplement;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Customer;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Order;
import pe.com.tdp.ventafija.microservices.common.domain.entity.User;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.domain.order.entity.*;
import pe.com.tdp.ventafija.microservices.domain.order.parser.FirebaseParser;
import pe.com.tdp.ventafija.microservices.domain.order.parser.OrderDetailFirebaseParser;
import pe.com.tdp.ventafija.microservices.domain.repository.OrderDetailRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpOrderRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpSalesAgentRepository;
import pe.com.tdp.ventafija.microservices.domain.repository.UserRepository;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrderDetailService {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private OrderDetailRepository orderDetailRepository;
    @Autowired
    private TdpOrderRepository tdpOrderRepository;
    @Autowired
    private TdpSalesAgentRepository tdpSalesAgentRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AddressComplementRepository addressComplementRepository;
    @Autowired
    private SvaService svaService;

    private static final String KEY_DECOSSD = "decosSD";
    private static final String KEY_DECOSHD = "decosHD";
    private static final String KEY_DECOSDVR = "decosDVR";
    private static final String KEY_TVBLOCK = "tvBlock";
    private static final String KEY_BLOCKTV = "blockTV";
    private static final String KEY_SVAINTERNET = "svaInternet";
    private static final String KEY_SVALINE = "svaLine";
    private static final String KEY_SVAINTERNET_RWS = "svaInternetRSW";

    private DateTimeZone dtZone = DateTimeZone.forID("America/Lima");

    @Transactional
    public OrderDetail save(SaleApiFirebaseResponseBody objFirebase, Order order, TdpCatalog tdpCatalog) {
        if (objFirebase == null) {
            return null;
        }
        DateTime dt = new DateTime(new Date());
        DateTime dtus = dt.withZone(dtZone);

        FirebaseParser<OrderDetail> parser = new OrderDetailFirebaseParser();
        OrderDetail orderDetail = parser.parse(objFirebase);
        if (orderDetail.getRegisteredTime() == null) {
            orderDetail.setRegisteredTime(dtus.toDate());
        }
        orderDetail.setRecordingID(order.getId());
        orderDetail.setOrderId(order.getId());

        String svaCatalog = "";
        Integer productInternetVel = null;
        if (tdpCatalog != null) {
            String internetTech = tdpCatalog.getInternetTech();
            String tvTech = tdpCatalog.getTvTech();
            productInternetVel = tdpCatalog.getInternetSpeed();

            orderDetail.setInternetTech(internetTech);
            orderDetail.setTvTech(tvTech);
            StringBuilder consultaBuilder = new StringBuilder();
            appendSvas((tdpCatalog.getProductBloqueTv() == null ? "" : tdpCatalog.getProductBloqueTv().replace("-", "")), consultaBuilder);
            appendSvas((tdpCatalog.getProductSvaLinea() == null ? "" : tdpCatalog.getProductSvaLinea().replace("-", "")), consultaBuilder);
            appendSvas((tdpCatalog.getProductSvaInternet() == null ? "" : tdpCatalog.getProductSvaInternet().replace("-", "")), consultaBuilder);

            if (consultaBuilder.length() > 0) {
                String[] svasAll = consultaBuilder.toString().split(",");
                svaCatalog = generateConsultaSVA(svasAll);
            }
        }

        Long id = orderDetailRepository.findByOrderId(order.getId());
        orderDetail.setId(id);

        List<String> svaList = new ArrayList<>();
        if (objFirebase.getSva() != null && !objFirebase.getSva().isEmpty()) {
            List<String> svaListTmp = objFirebase.getSva();

            for (int i = 0; i < svaListTmp.size(); i++) {
                String[] svaObj = svaListTmp.get(i).split("-");
                svaList.add(svaObj[0]);
            }
            Map<String, String> sva = svaService.loadSva(svaList.stream().map(Integer::parseInt).collect(Collectors.toList()));
            orderDetail.setDecosSD(sva.get(KEY_DECOSSD) == null ? null : Integer.parseInt(sva.get(KEY_DECOSSD)));
            orderDetail.setDecoshd(sva.get(KEY_DECOSHD) == null ? null : Integer.parseInt(sva.get(KEY_DECOSHD)));
            orderDetail.setDecosDVR(sva.get(KEY_DECOSDVR) == null ? null : Integer.parseInt(sva.get(KEY_DECOSDVR)));
            orderDetail.setTvBlock(sva.get(KEY_TVBLOCK));
            orderDetail.setBlockTV(sva.get(KEY_BLOCKTV));
            orderDetail.setInternetRsw(sva.get(KEY_SVAINTERNET_RWS) == null ? null : Integer.parseInt(sva.get(KEY_SVAINTERNET_RWS)));
            orderDetail.setSvaInternet(sva.get(KEY_SVAINTERNET));
            orderDetail.setSvaLine(sva.get(KEY_SVALINE));
        }
        orderDetail.setOriginOrder(objFirebase.getOriginOrder());
        orderDetail.setCobre_blq_vta(objFirebase.getCobre_blq_vta());
        orderDetail.setCobre_blq_trm(objFirebase.getCobre_blq_trm());

        orderDetailRepository.save(orderDetail);

        TdpOrder tdpOrder = tdpOrderRepository.findOneByOrderId(order.getId());
        if (tdpOrder == null) {
            tdpOrder = new TdpOrder();
            tdpOrder.setAuditoriaCreate(dtus.toDate());
        }
        tdpOrder.setAuditoriaModify(dtus.toDate());

        /* START Validación del flujo sva*/
        
        if(tdpCatalog != null){
            order.setServiceType(tdpCatalog.getTipoRegistro());
        }
        
        /* FIN Validación del flujo sva */
        
        tdpOrder.setOrderId(order.getId());
        tdpOrder.setOrderFechaHora(dtus.toDate());
        tdpOrder.setOrderOperationCommercial(order.getCommercialOperation());
            Integer operationNumber = 0;
        if (order.getCommercialOperation() != null && order.getServiceType() != null && order.getCampaign() != null) {
            if (order.getCommercialOperation().equalsIgnoreCase("Alta Pura")
                    && order.getServiceType().equalsIgnoreCase("ATIS")
                    && order.getCampaign().toUpperCase().equalsIgnoreCase("CIUDAD SITIADA")) {
                operationNumber = 1;
            } else if (order.getCommercialOperation().equalsIgnoreCase("Alta Pura")
                    && order.getServiceType().equalsIgnoreCase("ATIS")) {
                operationNumber = 1;
            } else if (order.getCommercialOperation().equalsIgnoreCase("Alta Pura")
                    && order.getServiceType().equalsIgnoreCase("CMS")) {
                operationNumber = 6;
            } else if (order.getCommercialOperation().equalsIgnoreCase("SVAS")
                    && order.getServiceType().equalsIgnoreCase("ATIS")) {
                operationNumber = 2;
            } else if (order.getCommercialOperation().equalsIgnoreCase("SVAS")
                    && order.getServiceType().equalsIgnoreCase("CMS")) {
                operationNumber = 7;
            } else if (objFirebase.getType().equalsIgnoreCase("M")) {
                operationNumber = 4;
            } else {
                operationNumber = 0;
            }
        }
        tdpOrder.setOrderOperationNumber(operationNumber);
        tdpOrder.setOrderExperto(orderDetail.getExpertoCode());
        tdpOrder.setOrderGpsX(order.getCoordinateX());
        tdpOrder.setOrderGpsY(order.getCoordinateY());
        tdpOrder.setOrderParqueTelefono(order.getMigrationPhone());
        tdpOrder.setOrderGisXdsl(orderDetail.getMsx_cbr_voi_ges_in());
        tdpOrder.setOrderGisHfc(orderDetail.getMsx_cbr_voi_gis_in());
        tdpOrder.setOrderGisCable(orderDetail.getMsx_ind_snl_gis_cd());
        tdpOrder.setOrderGisGpon(orderDetail.getMsx_ind_gpo_gis_cd());
        tdpOrder.setOrderGisNtlt(orderDetail.getCod_fac_tec_cd());
        tdpOrder.setOrderCallTipo("");
        tdpOrder.setOrderCallDesc("");
        tdpOrder.setOrderModelo("");
        tdpOrder.setOrderOrigen(order.getAppcode());
        tdpOrder.setAffiliationElectronicInvoice(orderDetail.getEnableDigitalInvoice());
        tdpOrder.setAffiliationDataProtection(orderDetail.getDataProtection());
        tdpOrder.setAffiliationParentalProtection(orderDetail.getParentalProtection());
        tdpOrder.setDisaffiliationWhitePagesGuide(orderDetail.getPublicationWhitePages());
        tdpOrder.setOrderContrato(orderDetail.getSendContracts().toUpperCase().equalsIgnoreCase("SI") ? "M" : "F");
        tdpOrder.setOrderGrabacion("S");
        tdpOrder.setOrderModalidadAcept("G");
        //tdpOrder.setOrderCip(String orderCip);
        tdpOrder.setOrderStc6i(orderDetail.getCod_stc());
        try {
            TdpSalesAgent tdpSalesAgent = tdpSalesAgentRepository.findOneByCodigoAtis(order.getUser().getId());
            User user = userRepository.findOne(order.getUser().getId());

            if (tdpSalesAgent != null) {
            tdpOrder.setUserAtis(tdpSalesAgent.getCodigoAtis());
                tdpOrder.setUserFuerzaVenta(tdpSalesAgent.getUserFuerzaVenta());
                tdpOrder.setUserCanalCodigo(tdpSalesAgent.getUser_canal_codigo());
            tdpOrder.setUserCmsPuntoVenta(0);
                tdpOrder.setUserCms(tdpSalesAgent.getCodCms() == null || tdpSalesAgent.getCodCms().isEmpty() ? null : Integer.parseInt(tdpSalesAgent.getCodCms()));
            tdpOrder.setUserNombre(tdpSalesAgent.getNombre() + " " + tdpSalesAgent.getApePaterno() + " " + tdpSalesAgent.getApeMaterno());
            tdpOrder.setUserDni(tdpSalesAgent.getDni());
            }
            if (user != null)
            tdpOrder.setUserTelefono(user.getPhone());
            if (tdpSalesAgent != null) {
            tdpOrder.setUserEntidad(tdpSalesAgent.getEntidad());
            tdpOrder.setUserRegion(tdpSalesAgent.getZona());
            tdpOrder.setUserZonal(tdpSalesAgent.getZonal());
            }
        } catch (Exception sales) {
            sales.printStackTrace();

        }
        Customer customer = order.getCustomer();
        tdpOrder.setClientTipoDoc(customer.getDocType().toUpperCase());
        tdpOrder.setClientNumeroDoc(customer.getDocNumber());

        String RUC_20 = customer.getDocNumber().trim().substring(0,2);
        if(customer.getDocType().trim().equalsIgnoreCase("RUC") &&
                (RUC_20.equals("20") || RUC_20.equals("15") || RUC_20.equals("17"))){
            logger.info(">>>jdcd: RUC 20 o 15 o 17");
            String ultimo_digito_ruc = tdpOrder.getClientNumeroDoc().trim().substring(tdpOrder.getClientNumeroDoc().trim().length() - 1);
            tdpOrder.setClientRucDigitCtrl(ultimo_digito_ruc);
            tdpOrder.setClientApellidoPaterno(".");
            tdpOrder.setClientApellidoMaterno(".");
            tdpOrder.setClientRucActividad(1);
            tdpOrder.setClientRucSubactividad(1);
        }else if(customer.getDocType().trim().equalsIgnoreCase("RUC")) {
            logger.info(">>>jdcd: RUC diferente de 20");
            String ultimo_digito_ruc = tdpOrder.getClientNumeroDoc().trim().substring(tdpOrder.getClientNumeroDoc().trim().length() - 1);
            tdpOrder.setClientRucDigitCtrl(ultimo_digito_ruc);
            tdpOrder.setClientApellidoPaterno(customer.getLastName1());
            tdpOrder.setClientApellidoMaterno(customer.getLastName2());
            tdpOrder.setClientRucActividad(1);
            tdpOrder.setClientRucSubactividad(1);
        }else {
            logger.info(">>>jdcd: No es RUC");
            tdpOrder.setClientRucDigitCtrl("");
            tdpOrder.setClientApellidoPaterno(customer.getLastName1());
            tdpOrder.setClientApellidoMaterno(customer.getLastName2());
            tdpOrder.setClientRucActividad(0);
            tdpOrder.setClientRucSubactividad(0);
        }



        /*tdpOrder.setClientRucActividad(0);
        tdpOrder.setClientRucSubactividad(0);*/
        tdpOrder.setClientNombre(customer.getFirstName());
        /*tdpOrder.setClientApellidoPaterno(customer.getLastName1());
        tdpOrder.setClientApellidoMaterno(customer.getLastName2());*/
        tdpOrder.setClientNacimiento(customer.getBirthDate());
        tdpOrder.setClientSexo(customer.getSex());
        tdpOrder.setClientCivil(customer.getMaritalstatus());
        tdpOrder.setClientEmail(customer.getEmail());
        tdpOrder.setClientNationality(customer.getNationality());
        tdpOrder.setClientTelefono1Area("");
        tdpOrder.setClientTelefono1(customer.getCustomerPhone());
        tdpOrder.setClientTelefono2Area("");
        tdpOrder.setClientTelefono2(customer.getCustomerPhone2());
        tdpOrder.setClientCmsCodigo(order.getCmsCustomer() == null || order.getCmsCustomer().isEmpty() ? null : Integer.parseInt(order.getCmsCustomer()));
        tdpOrder.setClientCmsServicio(order.getCmsServiceCode());
        tdpOrder.setClientCmsFactura(0);
        tdpOrder.setProductPs(order.getProductCode());
        tdpOrder.setProductPrecioNormal(order.getPrice());
        tdpOrder.setProductPrecioPromo(order.getPromPrice());
        tdpOrder.setProductPrecioPromoMes(order.getMonthPeriod());
        tdpOrder.setProductInternetVel(productInternetVel);
        tdpOrder.setProductInternetPromo(order.getPromoSpeed());
        tdpOrder.setProductInternetPromoTiempo(order.getPeriodoPromoSpeed());
        tdpOrder.setProductCampaing(order.getCampaign());
        tdpOrder.setProductType(order.getProductType());
        tdpOrder.setProductCategory(order.getProductCategory());
        tdpOrder.setProductPayMethod(order.getPaymentMode());
        tdpOrder.setProductPayPrice(order.getCashPrice());
        try {
            if (tdpCatalog != null) {
                tdpOrder.setProductCodigo(tdpCatalog.getProductCodigo());
            tdpOrder.setProductPayReturnMonth(tdpCatalog.getReturnMonth());
            tdpOrder.setProductPayReturnPeriod(tdpCatalog.getReturnPeriod());
            tdpOrder.setProductCostInstall(tdpCatalog.getInstallCost());
            tdpOrder.setProductCostInstallMonth(tdpCatalog.getFinancingMonth());
            tdpOrder.setProductTecInter(tdpCatalog.getInternetTech());
            tdpOrder.setProductTecTv(tdpCatalog.getTvTech());
        tdpOrder.setProductTvSenal(tdpCatalog.getTvSignal());
                tdpOrder.setProductEquipTv(tdpCatalog.getEquipamientoTv().replace("-",""));
                tdpOrder.setProductEquipInter(tdpCatalog.getEquipamientoInternet().replace("-",""));
                tdpOrder.setProductEquipLine(tdpCatalog.getEquipamientoLinea().replace("-",""));
            }
        } catch (Exception product) {
            product.printStackTrace();
        }
        tdpOrder.setProductTipoReg(order.getServiceType());
        tdpOrder.setProductSenal(orderDetail.getCod_ind_sen_cms());
        tdpOrder.setProductCabecera(orderDetail.getCod_cab_cms());

        tdpOrder.setProductPsAdmin1(16732);
        tdpOrder.setProductPsAdmin2(0);
        tdpOrder.setProductPsAdmin3(0);
        tdpOrder.setProductPsAdmin4(0);

        List<TdpSva> listSva = svaService.getSva(svaList.stream().map(Integer::parseInt).collect(Collectors.toList()), svaCatalog);
        Integer svaNro = 0;
        for (TdpSva sva : listSva) {
            if (svaNro == 0) {
                tdpOrder.setSvaCode1(sva.getCode());
                tdpOrder.setSvaCodigo1(sva.getSva_Codigo());
                tdpOrder.setSvaNombre1(sva.getDescription());
                tdpOrder.setSvaPrecio1(sva.getCost());
                tdpOrder.setSvaCantidad1(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            } else if (svaNro == 1) {
                tdpOrder.setSvaCode2(sva.getCode());
                tdpOrder.setSvaCodigo2(sva.getSva_Codigo());
                tdpOrder.setSvaNombre2(sva.getDescription());
                tdpOrder.setSvaPrecio2(sva.getCost());
                tdpOrder.setSvaCantidad2(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            } else if (svaNro == 2) {
                tdpOrder.setSvaCode3(sva.getCode());
                tdpOrder.setSvaCodigo3(sva.getSva_Codigo());
                tdpOrder.setSvaNombre3(sva.getDescription());
                tdpOrder.setSvaPrecio3(sva.getCost());
                tdpOrder.setSvaCantidad3(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            } else if (svaNro == 3) {
                tdpOrder.setSvaCode4(sva.getCode());
                tdpOrder.setSvaCodigo4(sva.getSva_Codigo());
                tdpOrder.setSvaNombre4(sva.getDescription());
                tdpOrder.setSvaPrecio4(sva.getCost());
                tdpOrder.setSvaCantidad4(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            } else if (svaNro == 4) {
                tdpOrder.setSvaCode5(sva.getCode());
                tdpOrder.setSvaCodigo5(sva.getSva_Codigo());
                tdpOrder.setSvaNombre5(sva.getDescription());
                tdpOrder.setSvaPrecio5(sva.getCost());
                tdpOrder.setSvaCantidad5(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            } else if (svaNro == 5) {
                tdpOrder.setSvaCode6(sva.getCode());
                tdpOrder.setSvaCodigo6(sva.getSva_Codigo());
                tdpOrder.setSvaNombre6(sva.getDescription());
                tdpOrder.setSvaPrecio6(sva.getCost());
                tdpOrder.setSvaCantidad6(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            } else if (svaNro == 6) {
                tdpOrder.setSvaCode7(sva.getCode());
                tdpOrder.setSvaCodigo7(sva.getSva_Codigo());
                tdpOrder.setSvaNombre7(sva.getDescription());
                tdpOrder.setSvaPrecio7(sva.getCost());
                tdpOrder.setSvaCantidad7(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            } else if (svaNro == 7) {
                tdpOrder.setSvaCode8(sva.getCode());
                tdpOrder.setSvaCodigo8(sva.getSva_Codigo());
                tdpOrder.setSvaNombre8(sva.getDescription());
                tdpOrder.setSvaPrecio8(sva.getCost());
                tdpOrder.setSvaCantidad8(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            } else if (svaNro == 8) {
                tdpOrder.setSvaCode9(sva.getCode());
                tdpOrder.setSvaCodigo9(sva.getSva_Codigo());
                tdpOrder.setSvaNombre9(sva.getDescription());
                tdpOrder.setSvaPrecio9(sva.getCost());
                tdpOrder.setSvaCantidad9(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            } else if (svaNro == 9) {
                tdpOrder.setSvaCode10(sva.getCode());
                tdpOrder.setSvaCodigo10(sva.getSva_Codigo());
                tdpOrder.setSvaNombre10(sva.getDescription());
                tdpOrder.setSvaPrecio10(sva.getCost());
                tdpOrder.setSvaCantidad10(sva.getUnit() == null || sva.getUnit().isEmpty() ? null : Integer.parseInt(sva.getUnit()));
                svaNro++;
            }
        }

        tdpOrder = setComplementos(tdpOrder, objFirebase);

        tdpOrder.setAddressDepartamentoCod("");
        tdpOrder.setAddressDepartamento(orderDetail.getDepartment());
        tdpOrder.setAddressProvinciaCod("");
        tdpOrder.setAddressProvincia(orderDetail.getProvince());
        tdpOrder.setAddressDistritoCod("");
        tdpOrder.setAddressDistrito(orderDetail.getDistrict());
        tdpOrder.setAddressPrincipal(orderDetail.getAddress());
        //tdpOrder.setAddressCalleAtis(orderDetail.getTip_cal_ati_cd());
        //tdpOrder.setAddressCalleNombre(orderDetail.getNom_cal_ds());
        tdpOrder.setAddressCalleNumero(orderDetail.getNum_cal_nu());
        tdpOrder.setAddressCchhCodigo(null);
        //tdpOrder.setAddressCchhTipo(orderDetail.getTip_cnj_hbl_cd());
        //tdpOrder.setAddressCchhNombre(orderDetail.getNom_cnj_hbl_no());
        /*tdpOrder.setAddressCchhCompTip1("");
        tdpOrder.setAddressCchhCompNom1("");
        tdpOrder.setAddressCchhCompTip2("");
        tdpOrder.setAddressCchhCompNom2("");
        tdpOrder.setAddressCchhCompTip3("");
        tdpOrder.setAddressCchhCompNom3("");
        tdpOrder.setAddressCchhCompTip4("");
        tdpOrder.setAddressCchhCompNom4("");
        tdpOrder.setAddressCchhCompTip5("");
        tdpOrder.setAddressCchhCompNom5("");
        tdpOrder.setAddressViaCompTip1("");
        tdpOrder.setAddressViaCompNom1("");
        tdpOrder.setAddressViaCompTip2("");
        tdpOrder.setAddressViaCompNom2("");
        tdpOrder.setAddressViaCompTip3("");
        tdpOrder.setAddressViaCompNom3("");
        tdpOrder.setAddressViaCompTip4("");
        tdpOrder.setAddressViaCompNom4("");
        tdpOrder.setAddressViaCompTip5("");
        tdpOrder.setAddressViaCompNom5("");*/
        tdpOrder.setAddressReferencia(orderDetail.getAddressComplement());

        LogVass.serviceResponseObject(logger, "Save", "tdpOrder", tdpOrder);
        tdpOrderRepository.save(tdpOrder);

        return orderDetail;
    }

    private TdpOrder setComplementos(TdpOrder tdpOrder, SaleApiFirebaseResponseBody objFirebase){
        List<AddressComplement> complementList = addressComplementRepository.findAll();
        List<AddressComplement> tipoCCHHLima = new ArrayList<>();
        List<AddressComplement> tipoCCHHProvincia = new ArrayList<>();
        List<AddressComplement> tipoCCHHComplemento = new ArrayList<>();
        List<AddressComplement> tipoViaLima = new ArrayList<>();
        List<AddressComplement> tipoViaProvincia = new ArrayList<>();
        List<AddressComplement> tipoViaComplemento = new ArrayList<>();

        //COMPLEMENTOS
        for(AddressComplement obj : complementList){
            if("DSCACCHL".equalsIgnoreCase(obj.getTipo())){ tipoCCHHLima.add(obj); continue;}
            if("DSCACCHP".equalsIgnoreCase(obj.getTipo())){ tipoCCHHProvincia.add(obj); continue;}
            if("DSCATCMH".equalsIgnoreCase(obj.getTipo())){ tipoCCHHComplemento.add(obj); continue;}
            if("DSCAVIAL".equalsIgnoreCase(obj.getTipo())){ tipoViaLima.add(obj); continue;}
            if("DSCAVIAP".equalsIgnoreCase(obj.getTipo())){ tipoViaProvincia.add(obj); continue;}
            if("DSCATCMV".equalsIgnoreCase(obj.getTipo())){ tipoViaComplemento.add(obj); continue;}
        }

        String cuadra = objFirebase.getAddress_cuadra();
        String cuadraCode = "CDR";
        if(cuadra != null && !cuadra.equalsIgnoreCase("")){
            if(existsInListByCode(cuadraCode,tipoCCHHComplemento)){
                setComplementoCCHH(cuadraCode, cuadra, tdpOrder);
            }else if(existsInListByCode(cuadraCode,tipoViaComplemento)){
                setComplementoVia(cuadraCode, cuadra, tdpOrder);
            }
        }

        String manzana = objFirebase.getAddress_manzana();
        String manzanaCode = "MZ";
        if(manzana != null && !manzana.equalsIgnoreCase("")){
            if(existsInListByCode(manzanaCode,tipoCCHHComplemento)){
                setComplementoCCHH(manzanaCode, manzana, tdpOrder);
            }else if(existsInListByCode(manzanaCode,tipoViaComplemento)){
                setComplementoVia(manzanaCode, manzana, tdpOrder);
            }
        }

        String lote = objFirebase.getAddress_lote();
        String loteCode = "LT";
        if(lote != null && !lote.equalsIgnoreCase("")){
            if(existsInListByCode(loteCode,tipoCCHHComplemento)){
                setComplementoCCHH(loteCode, lote, tdpOrder);
            }else if(existsInListByCode(loteCode,tipoViaComplemento)){
                setComplementoVia(loteCode, lote, tdpOrder);
            }
        }

        String kilometro = objFirebase.getAddress_kilometro();
        String kilometroCode = "KM";
        if(kilometro != null && !kilometro.equalsIgnoreCase("")){
            if(existsInListByCode(kilometroCode,tipoCCHHComplemento)){
                setComplementoCCHH(kilometroCode, kilometro, tdpOrder);
            }else if(existsInListByCode(kilometroCode,tipoViaComplemento)){
                setComplementoVia(kilometroCode, kilometro, tdpOrder);
            }
        }

        String piso = objFirebase.getAddress_piso();
        String pisoCode = "PI";
        if(piso != null && !piso.equalsIgnoreCase("")){
            if(existsInListByCode(pisoCode,tipoCCHHComplemento)){
                setComplementoCCHH(pisoCode, piso, tdpOrder);
            }else if(existsInListByCode(pisoCode,tipoViaComplemento)){
                setComplementoVia(pisoCode, piso, tdpOrder);
            }
        }

        String nombreViviendaValor = objFirebase.getAddress_nombreVivienda();
        String tipoViviendaCodigo = objFirebase.getAddress_tipoVivienda();
        if(nombreViviendaValor != null && !nombreViviendaValor.equalsIgnoreCase("")){
            if(existsInListByCode(tipoViviendaCodigo,tipoCCHHComplemento)){
                setComplementoCCHH(tipoViviendaCodigo, nombreViviendaValor, tdpOrder);
            }else if(existsInListByCode(tipoViviendaCodigo,tipoViaComplemento)){
                setComplementoVia(tipoViviendaCodigo, nombreViviendaValor, tdpOrder);
            }
        }

        String numeroInteriorValor = objFirebase.getAddress_numeroInterior();
        String tipoInteriorCodigo = objFirebase.getAddress_tipoInterior();
        if(numeroInteriorValor != null && !numeroInteriorValor.equalsIgnoreCase("")){
            if(existsInListByCode(tipoInteriorCodigo,tipoCCHHComplemento)){
                setComplementoCCHH(tipoInteriorCodigo, numeroInteriorValor, tdpOrder);
            }else if(existsInListByCode(tipoInteriorCodigo,tipoViaComplemento)){
                setComplementoVia(tipoInteriorCodigo, numeroInteriorValor, tdpOrder);
            }
        }


        //CONJUNTO HABITACIONAL
        String tipoCCHH = objFirebase.getAddress_tipoUrbanizacion();
        String nombreCCHH = objFirebase.getAddress_nombreUrbanizacion();
        //VIA
        String tipoVia = objFirebase.getAddress_tipoVia();
        String nombreVia = objFirebase.getAddress_nombreVia();

        String provincia = objFirebase.getProvince();
        if(provincia != null && provincia.equalsIgnoreCase("Lima")){
            if(tipoCCHH != null && !tipoCCHH.equals("")){
                if(existsInListByCode(tipoCCHH, tipoCCHHLima)){
                    tdpOrder.setAddressCchhTipo(tipoCCHH);
                    tdpOrder.setAddressCchhNombre(nombreCCHH);
                }else if(existsInListByCode(tipoCCHH, tipoViaLima)){
                    tdpOrder.setAddressCalleAtis(tipoVia);
                    tdpOrder.setAddressCalleNombre(nombreVia);
                }
            }
            if(tipoVia != null && !tipoVia.equals("")){
                if(existsInListByCode(tipoVia, tipoCCHHLima)){
                    tdpOrder.setAddressCchhTipo(tipoCCHH);
                    tdpOrder.setAddressCchhNombre(nombreCCHH);
                }else if(existsInListByCode(tipoVia, tipoViaLima)){
                    tdpOrder.setAddressCalleAtis(tipoVia);
                    tdpOrder.setAddressCalleNombre(nombreVia);
                }
            }
        }else if(provincia != null && !provincia.equalsIgnoreCase("Lima")){
            if(tipoCCHH != null && !tipoCCHH.equals("")){
                if(existsInListByCode(tipoCCHH, tipoCCHHLima)){
                    tdpOrder.setAddressCchhTipo(tipoCCHH);
                    tdpOrder.setAddressCchhNombre(nombreCCHH);
                }else if(existsInListByCode(tipoCCHH, tipoViaLima)){
                    tdpOrder.setAddressCalleAtis(tipoVia);
                    tdpOrder.setAddressCalleNombre(nombreVia);
                }
            }
            if(tipoVia != null && !tipoVia.equals("")){
                if(existsInListByCode(tipoVia, tipoCCHHLima)){
                    tdpOrder.setAddressCchhTipo(tipoCCHH);
                    tdpOrder.setAddressCchhNombre(nombreCCHH);
                }else if(existsInListByCode(tipoVia, tipoViaLima)){
                    tdpOrder.setAddressCalleAtis(tipoVia);
                    tdpOrder.setAddressCalleNombre(nombreVia);
                }
            }
        }

        return tdpOrder;
    }

    private boolean existsInListByCode(String code, List<AddressComplement> items){
        boolean result = false;
        for(AddressComplement obj : items){
            if(code.equalsIgnoreCase(obj.getCodigo())){result = true; break;}
        }
        return result;
    }

    private TdpOrder setComplementoVia(String codigo, String valor, TdpOrder tdpOrder){
        if(tdpOrder.getAddressViaCompNom1() == null || tdpOrder.getAddressViaCompNom1().trim().equals("")){
            tdpOrder.setAddressViaCompTip1(codigo);
            tdpOrder.setAddressViaCompNom1(valor);
        }else if(tdpOrder.getAddressViaCompNom2() == null || tdpOrder.getAddressViaCompNom2().trim().equals("")){
            tdpOrder.setAddressViaCompTip2(codigo);
            tdpOrder.setAddressViaCompNom2(valor);
        }else if(tdpOrder.getAddressViaCompNom3() == null || tdpOrder.getAddressViaCompNom3().trim().equals("")){
            tdpOrder.setAddressViaCompTip3(codigo);
            tdpOrder.setAddressViaCompNom3(valor);
        }else if(tdpOrder.getAddressViaCompNom4() == null || tdpOrder.getAddressViaCompNom4().trim().equals("")){
            tdpOrder.setAddressViaCompTip4(codigo);
            tdpOrder.setAddressViaCompNom4(valor);
        }else if(tdpOrder.getAddressViaCompNom5() == null || tdpOrder.getAddressViaCompNom5().trim().equals("")){
            tdpOrder.setAddressViaCompTip5(codigo);
            tdpOrder.setAddressViaCompNom5(valor);
        }
        return tdpOrder;
    }

    private TdpOrder setComplementoCCHH(String codigo, String valor, TdpOrder tdpOrder){
        if(tdpOrder.getAddressCchhCompNom1() == null || tdpOrder.getAddressCchhCompNom1().trim().equals("")){
            tdpOrder.setAddressCchhCompTip1(codigo);
            tdpOrder.setAddressCchhCompNom1(valor);
        }else if(tdpOrder.getAddressCchhCompNom2() == null || tdpOrder.getAddressCchhCompNom2().trim().equals("")){
            tdpOrder.setAddressCchhCompTip2(codigo);
            tdpOrder.setAddressCchhCompNom2(valor);
        }else if(tdpOrder.getAddressCchhCompNom3() == null || tdpOrder.getAddressCchhCompNom3().trim().equals("")){
            tdpOrder.setAddressCchhCompTip3(codigo);
            tdpOrder.setAddressCchhCompNom3(valor);
        }else if(tdpOrder.getAddressCchhCompNom4() == null || tdpOrder.getAddressCchhCompNom4().trim().equals("")){
            tdpOrder.setAddressCchhCompTip4(codigo);
            tdpOrder.setAddressCchhCompNom4(valor);
        }else if(tdpOrder.getAddressCchhCompNom5() == null || tdpOrder.getAddressCchhCompNom5().trim().equals("")){
            tdpOrder.setAddressCchhCompTip5(codigo);
            tdpOrder.setAddressCchhCompNom5(valor);
        }
        return tdpOrder;
    }

    private String generateConsultaSVA(String[] svas) {
        StringBuilder builder = new StringBuilder();
        for (String sva : svas) {
            if (!"-".equals(sva) && !"".equals(sva)) {
                builder.append("'").append(sva).append("',");
            }
        }
        if (builder.length() > 0) {
            builder.deleteCharAt(builder.length() - 1);
            return builder.toString();
        }
        return "";
    }

    private void appendSvas(String svas, StringBuilder consultaBuilder) {
        if (consultaBuilder.toString().length() > 2)
            consultaBuilder.append(",");
        if (svas != null && !svas.isEmpty() && !"-".equals(svas)) {
            consultaBuilder.append(svas);
        }
    }

}
