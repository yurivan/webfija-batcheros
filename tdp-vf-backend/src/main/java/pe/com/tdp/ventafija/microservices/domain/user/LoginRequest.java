package pe.com.tdp.ventafija.microservices.domain.user;

public class LoginRequest {

	private String codAtis;
	private String password;
	private String imei;

	public String getCodAtis() {
		return codAtis;
	}

	public void setCodAtis(String codAtis) {
		this.codAtis = codAtis;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

  public String getImei() {
    return imei;
  }

  public void setImei(String imei) {
    this.imei = imei;
  }

}
