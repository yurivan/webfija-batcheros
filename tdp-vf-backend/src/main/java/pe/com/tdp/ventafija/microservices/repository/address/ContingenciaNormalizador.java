package pe.com.tdp.ventafija.microservices.repository.address;

import org.springframework.context.annotation.Bean;


public class ContingenciaNormalizador {
    private String id_transaccion;
    private Boolean normalizador;

    public ContingenciaNormalizador() {
    }

    public String getId_transaccion() {
        return id_transaccion;
    }

    public void setId_transaccion(String id_transaccion) {
        this.id_transaccion = id_transaccion;
    }

    public Boolean getNormalizador() {
        return normalizador;
    }

    public void setNormalizador(Boolean normalizador) {
        this.normalizador = normalizador;
    }
}
