package pe.com.tdp.ventafija.microservices.domain.customer;

import java.util.List;

public class ContractResponseData {

	private List<String> contract;
	private List<String> speech;

	public List<String> getContract() {
		return contract;
	}

	public void setContract(List<String> contract) {
		this.contract = contract;
	}

	public List<String> getSpeech() {
		return speech;
	}

	public void setSpeech(List<String> speech) {
		this.speech = speech;
	}

}
