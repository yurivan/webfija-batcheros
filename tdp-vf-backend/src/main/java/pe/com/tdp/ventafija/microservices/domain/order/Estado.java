package pe.com.tdp.ventafija.microservices.domain.order;

public class Estado {
    private String name;
    private String color;
    private String cantidad;

    public Estado() {

    }
    public Estado(String name, String color, String cantidad) {
        super();
        this.name = name;
        this.color = color;
        this.cantidad = cantidad;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }
}
