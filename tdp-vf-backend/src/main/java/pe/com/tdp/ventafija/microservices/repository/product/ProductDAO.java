package pe.com.tdp.ventafija.microservices.repository.product;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.clients.avve.OffersApiAvveResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoResponseBodyOperacion;
import pe.com.tdp.ventafija.microservices.common.clients.gis.OffersGisResponse;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.Converter;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.constants.product.ProductConstants;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.agendamiento.AgendamientRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.CustomerScoringResponse;
import pe.com.tdp.ventafija.microservices.domain.customer.scoring.OperacionComercial;
import pe.com.tdp.ventafija.microservices.domain.order.OutRequestDetalle;
import pe.com.tdp.ventafija.microservices.domain.order.OutResponseDetalle;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpAgendamientoCupones;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpAutomatizadorCabecera;
import pe.com.tdp.ventafija.microservices.domain.product.*;
import pe.com.tdp.ventafija.microservices.domain.user.ConfigParameterResponseData;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class ProductDAO {
    private static final Logger logger = LogManager.getLogger();
    @Value("${datasource.query.select}")
    private String MY_SQL_QUERY_WHERE;
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;

    @Autowired
    private DataSource datasource;

    public Response agendamientoSave (AgendamientRequest request){
        Response response = new Response();

        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd ");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = df.format(new Date());

        try (Connection con = Database.datasource().getConnection()) {

            String update = "UPDATE ibmx_a07e6d02edaf552.tdp_visor " +
                    "SET agent_client_name=? ," +
                    "agent_client_tel=? ," +
                    "agent_client_franga_horaria=? ," +
                    "agent_client_fecha_auditoria_server=? ," +
                    "agent_client_fecha_seleccionada=? " +
                    "WHERE id_visor=?";
            try (PreparedStatement ps = con.prepareStatement(update)) {
                ps.setString(1, request.getAgentClientName());
                ps.setString(2, request.getAgentClientTel());
                ps.setString(3, request.getAgentClientFrangaHoraria());
                ps.setString(4, strDate);
                ps.setString(5, request.getAgentClientFechaSeleccionada());
                ps.setString(6, request.getOrderId());
                ps.executeUpdate();
                response.setResponseMessage("Actualizado Correctamente");
            }


            String updateCupones = "UPDATE ibmx_a07e6d02edaf552.tdp_agendamiento_cupones " +
                    "SET cupones_utilizados=? " +
                    "WHERE cupones_id=?";
            try (PreparedStatement pss = con.prepareStatement(updateCupones)) {
                pss.setInt(1, request.getCuponesUtilizadoActualizado());
                pss.setInt(2, request.getId_cupon());
                pss.executeUpdate();
            }


        } catch (SQLException e) {
            e.printStackTrace();
            response.setResponseMessage("Error al actualizar");

        }
        return response;
    }


    public List <TdpAgendamientoCupones> calendarioShow (){
        List<TdpAgendamientoCupones> list = new ArrayList<>();

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");


        String strDate = df.format(new Date());


        try (Connection con = Database.datasource().getConnection()) {
            String select = "SELECT cupones_id,cupones_franja," +
                    "cupones_fecha,cupones,cupones_utilizados," +
                    "auditoria_create,auditoria_modify " +
                    "FROM ibmx_a07e6d02edaf552.tdp_agendamiento_cupones " +
                    "WHERE cupones_fecha > "+"'"+strDate+"'"+" " +
                    "ORDER BY cupones_fecha, cupones_franja " +
                    "ASC LIMIT 22";

            try (PreparedStatement ps = con.prepareStatement(select)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    TdpAgendamientoCupones obj = new TdpAgendamientoCupones();

                    obj.setCupones_id(rs.getInt("cupones_id"));
                    obj.setCupones_franja(rs.getString("cupones_franja"));
                    obj.setCupones_fecha(rs.getDate("cupones_fecha"));
                    obj.setCupones(rs.getInt("cupones"));
                    obj.setCupones_utilizados(rs.getInt("cupones_utilizados"));
                    obj.setAuditoria_create(rs.getDate("auditoria_create"));
                    obj.setAuditoria_modify(rs.getDate("auditoria_modify"));

                    list.add(obj);

                }

            }


        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Error Cupones.", e);

        }
        return list;
    }


    public List<OffersResponseData> readOffers(OffersRequest request, ApiResponse<OffersApiAvveResponseBody> objAVVE,
                                               CustomerScoringResponse objExperto) {
        List<OffersResponseData> list = new ArrayList<>();

        try (Connection con = datasource.getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL,
                            "DISTINCT OFERTA.category, OFERTA.targetType, CAT.*")
                    .replace(Constants.MY_SQL_TABLE, "TDP_CATALOG CAT, ( SELECT * FROM ibmx_a07e6d02edaf552.CONFIG_OFFERING WHERE sourceApp = 'APPVF' AND domain = 'OFFERS' AND category <> 'SVA' AND sourceData = ? ) OFERTA")
                    .replace(Constants.MY_SQL_WHERE, "CAT.prodTypeCode = OFERTA.targetData " +
                            " AND CAT.commercialOperation = OFERTA.category" +
                            "    AND (CAT.prodCategoryCode = '110' " +
                            "     OR (OFERTA.targetData = '4' AND CAT.internetSpeed <= IFNULL(topSpeed,9999999) AND CAT.internetTech = IFNULL(interTech,CAT.internetTech))" +
                            "         OR (OFERTA.targetData = '5' AND CAT.tvTech = IFNULL(tvTech,CAT.tvTech) AND UCASE(CAT.tvSignal) = IFNULL(UCASE(tvSignal),UCASE(CAT.tvSignal)))" +
                            "         OR (OFERTA.targetData = '6' AND CAT.tvTech = IFNULL(tvTech,CAT.tvTech) AND UCASE(CAT.tvSignal) = IFNULL(UCASE(tvSignal),UCASE(CAT.tvSignal)))" +
                            "         OR (OFERTA.targetData <> '4' AND OFERTA.targetData <> '5' AND OFERTA.targetData <> '6'" +
                            "             AND CAT.price <= IFNULL(?,9999999)" +
                            "             AND CAT.internetSpeed <= IFNULL(?,9999999)" +
                            "             AND CAT.internetTech = IFNULL(?,CAT.internetTech)" +
                            "             AND CAT.tvTech = IFNULL(?,CAT.tvTech)" +
                            "             AND UCASE(CAT.tvSignal) = IFNULL(UCASE(?),UCASE(CAT.tvSignal))" +
                            "            )" +
                            "        )" +
                            " ORDER BY CAT.priority ASC, CAT.price DESC");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getProdTypeCode());
                stmt.setDouble(2, getTopPrice(objExperto.getResult()));
                stmt.setInt(3, Integer.parseInt(objAVVE.getBodyOut().getVelocidadMax()));
                stmt.setString(4, objAVVE.getBodyOut().getTecInternet());
                stmt.setString(5, objAVVE.getBodyOut().getTecTV());
                stmt.setString(6, objAVVE.getBodyOut().getTipoSenal());
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {

                    OffersResponseData obj = new OffersResponseData();
                    obj.setProductCode(rs.getString("productCode"));
                    obj.setProductName(rs.getString("productName"));
                    obj.setDiscount(rs.getString("discount"));
                    obj.setPrice(Double.parseDouble(rs.getString("price")));
                    obj.setPromPrice(Double.parseDouble(rs.getString("promPrice")));
                    obj.setMonthPeriod(Integer.parseInt(rs.getString("monthPeriod")));
                    obj.setFinancingCost(Double.parseDouble(rs.getString("financingCost")));
                    obj.setFinancingMonth(Integer.parseInt(rs.getString("financingMonth")));
                    obj.setInstallCost(Double.parseDouble(rs.getString("installCost")));
                    obj.setReturnMonth(Integer.parseInt(rs.getString("returnMonth")));
                    obj.setReturnPeriod(rs.getString("returnPeriod"));
                    obj.setEquipmentType(rs.getString("equipmentType"));
                    obj.setLineType(rs.getString("lineType"));
                    obj.setInternetSpeed(rs.getInt("internetSpeed"));
                    obj.setCashPrice(Double.parseDouble(rs.getString("cashPrice")));
                    obj.setProductTypeCode(rs.getString("prodTypeCode"));
                    obj.setProductType(rs.getString("productType"));
                    obj.setCommercialOperation(rs.getString("commercialOperation"));
                    obj.setPaymentMethod(rs.getString("paymentMethod"));
                    obj.setCampaign(rs.getString("campaign"));
                    obj.setInternetTech(rs.getString("internetTech"));
                    obj.setTvSignal(rs.getString("tvSignal"));
                    obj.setTvTech(rs.getString("tvTech"));
                    obj.setProductCategoryCode(rs.getString("prodCategoryCode"));
                    obj.setProductCategory(rs.getString("productCategory"));
                    obj.setExpertCode(objExperto.getCodConsulta());
                    obj.setId(rs.getInt("ID"));

                    list.add(obj);

                }
            }
        } catch (Exception e) {
            logger.error("Error Offering DAO.", e);
        }

        return list;

    }

    public double getTopPrice(List<OperacionComercial> lista) {
        double topPrice = 0.0;

        for (OperacionComercial d : lista) {
            int renta = Integer.parseInt(Converter.fixPrice(d.getRenta()));

            if (renta > topPrice) {
                topPrice = renta;
            }
        }
        return topPrice;
    }

    public double getTopPrice(ApiResponse<OffersApiExpertoResponseBody> objExperto) {
        List<OffersApiExpertoResponseBodyOperacion> lista = objExperto.getBodyOut().getOperacionComercial();
        double topPrice = 0.0;

        for (OffersApiExpertoResponseBodyOperacion d : lista) {
            int renta = Integer.parseInt(Converter.fixPrice(d.getRenta()));

            if (renta > topPrice) {
                topPrice = renta;
            }
        }

        return topPrice;
    }

    public List<SvaResponseDataSva> readSVA(SvaRequest request) {

        List<SvaResponseDataSva> list = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {


            String CMS_ATIS = getSistemas(request.getProductCode(), con);
            String TVTECH = request.getTvTech();
            //String TVTECH = "CATV";


            /*String query = "SELECT orden.orden, MIN(SVAT.ID) ID, SVAT.code, SVAT.description, SVAT.type, SVAT.unit, MIN(SVAT.cost) as cost," +
                    " SVAT.paymentdescription, SVAT.financingmonth, SVAT.hasequipment, SVAT.maxdecos, SVAT.arpu, SVAT.velinter, MAX(SVAT.svadefault) as svadefault, par.strvalue , coalesce(par.strfilter, 'true') as bloquePadre" +
                    " FROM (" +
                    " SELECT DISTINCT SVA.ID, SVA.code, SVA.description, SVA.type, TRIM(SVA.unit) unit, " +
                    " CASE WHEN SVA.productcode IN (" + request.getAltaPuraSvasAll() + ") AND SVA.description not like '%PUNTO ADICIONAL%' THEN 0 ELSE SVA.cost END as cost, SVA.paymentDescription, " +
                    " SVA.financingMonth, FALSE AS hasEquipment, 4 as maxDecos, 0 AS arpu, 0 AS velInter, productcode, " +
                    " CASE WHEN SVA.productcode IN (" + request.getAltaPuraSvasAll() + ") AND SVA.description not like '%PUNTO ADICIONAL%' THEN 1 ELSE 0 END AS svadefault" +
                    " FROM ibmx_a07e6d02edaf552.TDP_SVA SVA, " +
                    " (SELECT * FROM ibmx_a07e6d02edaf552.CONFIG_OFFERING WHERE sourceData = ? AND sourceApp = ? AND domain = 'OFFERS' AND category = 'SVA' ) OFERTA " +
                    " WHERE SVA.prodTypeCode = OFERTA.targetData ) SVAT " +
                    " LEFT JOIN ibmx_a07e6d02edaf552.PARAMETERS par on par.domain = 'CONFIGSVA' and par.category = SVAT.code and par.element = SVAT.unit " +
                    " LEFT JOIN (select 1 as orden, 'BLOQUE FULL HD' as sva  union select 2 as code, 'BLOQUE HBO NEW' as sva  union select 3 as code, 'BLOQUE HBO NEW 20' as sva   union select 4 as code, 'BLOQUE FOX+ NEW' as sva  union select 5 as code, 'BLOQUE FOX+ NEW 20' as sva  union select 6 as code, 'BLOQUE HOT PACK' as sva  union select 7 as code, 'BLOQUE HOT PACK 20' as sva  union select 8 as code, 'BLOQUE GOLDEN PREMIER' as sva  union select 9 as code, 'BLOQUE ESTELAR' as sva  union select 10 as orden, 'BP' as sva union select 11 as orden, 'DHD' as sva  union select 12 as orden, 'DSHD' as sva  union select 13 as orden, 'DVR' as sva  union select 14 as orden, 'RSW' as sva  union select 15 as orden, 'SVAI' as sva  union select 16 as orden, 'SVAL' as sva ) orden " +
                    " on orden.sva = SVAT.unit or orden.sva = SVAT.code " +
                    " GROUP BY orden.orden, SVAT.code, SVAT.description, SVAT.type, SVAT.unit, SVAT.paymentdescription, SVAT.financingmonth, SVAT.hasequipment, SVAT.maxdecos, SVAT.arpu, SVAT.velinter, par.strvalue, par.strfilter " +
                    " ORDER BY orden.orden asc, 7 ASC";*/


            String query = "SELECT orden.orden, MIN(SVAT.ID) ID, SVAT.code, SVAT.description, SVAT.type, SVAT.unit, MIN(SVAT.cost) as cost, " +
                    "SVAT.paymentdescription, SVAT.financingmonth, SVAT.hasequipment, SVAT.maxdecos, SVAT.arpu, SVAT.velinter, MAX(SVAT.svadefault) as svadefault, par.strvalue , coalesce(par.strfilter, 'true') as bloquePadre " +
                    " FROM ( " +
                    "SELECT DISTINCT SVA.ID, SVA.code, SVA.description, SVA.type, TRIM(SVA.unit) unit, SVA.comercializable, SVA.sistemas, SVA.tecnologia_sva, " +
                    "CASE WHEN SVA.productcode IN ("+request.getAltaPuraSvasAll()+") " +
                    "AND SVA.comercializable = 'SI' AND SVA.sistemas = '"+ CMS_ATIS+"' AND (SVA.tecnologia_sva = '-' OR SVA.tecnologia_sva = '"+TVTECH+"') " +
                    "AND SVA.description not like '%PUNTO ADICIONAL%' THEN 0 ELSE SVA.cost END as cost, SVA.paymentDescription, " +
                    "SVA.financingMonth, FALSE AS hasEquipment, 4 as maxDecos, 0 AS arpu, 0 AS velInter, productcode, " +
                    "CASE WHEN SVA.productcode IN ("+request.getAltaPuraSvasAll()+") " +
                    "AND SVA.comercializable = 'SI' AND SVA.sistemas = '"+CMS_ATIS+"' AND (SVA.tecnologia_sva = '-' OR SVA.tecnologia_sva = '"+TVTECH+"') " +
                    "AND SVA.description not like '%PUNTO ADICIONAL%' THEN 1 ELSE 0 END AS svadefault " +
                    "FROM ibmx_a07e6d02edaf552.TDP_SVA SVA," +
                    " (SELECT * FROM ibmx_a07e6d02edaf552.CONFIG_OFFERING WHERE sourceData = ? AND sourceApp = ? AND domain = 'OFFERS' AND category = 'SVA' ) OFERTA " +
                    " WHERE SVA.prodTypeCode = OFERTA.targetData AND SVA.comercializable = 'SI' " +
                    " AND SVA.sistemas = '"+ CMS_ATIS+"' " +
                    " AND (sva.code not in('DHD','BTV','DSHD') or (sva.code in('DHD','BTV','DSHD') and (SVA.tecnologia_sva = '-' OR SVA.tecnologia_sva = '"+TVTECH+"')) )" +
                    " ) SVAT " +
                    " LEFT JOIN ibmx_a07e6d02edaf552.PARAMETERS par on par.domain = 'CONFIGSVA' and par.category = SVAT.code and par.element = SVAT.unit " +
                    " LEFT JOIN (select 1 as orden, 'BLOQUE FULL HD' as sva  union select 2 as code, 'BLOQUE HBO NEW' as sva  union select 3 as code, 'BLOQUE HBO NEW 20' as sva   union select 4 as code, 'BLOQUE FOX+ NEW' as sva  union select 5 as code, 'BLOQUE FOX+ NEW 20' as sva  union select 6 as code, 'BLOQUE HOT PACK' as sva  union select 7 as code, 'BLOQUE HOT PACK 20' as sva  union select 8 as code, 'BLOQUE GOLDEN PREMIER' as sva  union select 9 as code, 'BLOQUE ESTELAR' as sva  union select 10 as orden, 'BP' as sva union select 11 as orden, 'DHD' as sva  union select 12 as orden, 'DSHD' as sva  union select 13 as orden, 'DVR' as sva  union select 14 as orden, 'RSW' as sva  union select 15 as orden, 'SVAI' as sva  union select 16 as orden, 'SVAL' as sva ) orden " +
                    "  on orden.sva = SVAT.unit or orden.sva = SVAT.code " +
                    " GROUP BY orden.orden, SVAT.code, SVAT.description, SVAT.type, SVAT.unit, SVAT.paymentdescription, SVAT.financingmonth, SVAT.hasequipment, SVAT.maxdecos, SVAT.arpu, SVAT.velinter, par.strvalue, par.strfilter " +
                    " ORDER BY orden.orden asc, 7 ASC ";






            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getProductCode());
                if (null == request.getApplicationCode()) {
                    stmt.setString(2, "APPVF");
                } else {
                    stmt.setString(2, request.getApplicationCode());
                }

                LogVass.daoQueryDAO(logger, "readSVA", "query", stmt.toString());
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    SvaResponseDataSva obj = new SvaResponseDataSva();
                    obj.setCode(rs.getString("code"));
                    obj.setDescription(rs.getString("description"));
                    obj.setType(rs.getString("type"));
                    obj.setUnit(rs.getString("unit"));
                    obj.setCost(rs.getString("cost"));
                    // obj.setMaxValue(rs.getString("maxValue"));
                    obj.setPaymentDescription(rs.getString("paymentDescription"));
                    obj.setId(rs.getInt("ID"));
                    obj.setFinancingMonth(rs.getInt("financingMonth"));

                    obj.setBloquePadre(rs.getString("strvalue"));
                    obj.setClickPadre(rs.getBoolean("bloquePadre"));

                    // Sprint 10 - sva por default
                    obj.setAltaPuraDefaultSVA((rs.getInt("svadefault") == 1) ? true : false);
                    list.add(obj);
                }
            }
        } catch (Exception e) {
            logger.error("Error readSVA DAO.", e);
        }
        return list;
    }


    public String getSistemas(String prodtypecode, Connection con){
                   String ATIS_CMS ="";
                   String query = "SELECT DISTINCT tiporegistro FROM ibmx_a07e6d02edaf552.tdp_catalog WHERE prodtypecode = '"+prodtypecode+"' ";

                           try (PreparedStatement stmt = con.prepareStatement(query)) {
                            LogVass.daoQuery(logger, stmt.toString());
                            ResultSet rs = stmt.executeQuery();
                            while (rs.next()) {
                                    ATIS_CMS = rs.getString(1);
                                }

                            } catch (Exception e) {
                        logger.error("Error getSistemas ProductDAO" + e);
                    }

                        return ATIS_CMS;

                    }

    public List<ConfigParameterResponseData> numDecos() {

        List<ConfigParameterResponseData> configParameterResponseData = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {

            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "element, strValue")
                    .replace(Constants.MY_SQL_TABLE, "parameters")
                    .replace(Constants.MY_SQL_WHERE, "category = 'POLICY_PARAMETER' ");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQueryDAO(logger, "numDecos", "query", stmt.toString());
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    ConfigParameterResponseData responseData = new ConfigParameterResponseData();
                    responseData.setElement(rs.getString("element"));
                    responseData.setStrValue(rs.getString("strValue"));
                    configParameterResponseData.add(responseData);
                }
            }
        } catch (Exception e) {
            logger.error("Error numDecos DAO.", e);
        }
        return configParameterResponseData;
    }

    public List<SvaV2ResponseDataSva> readSVAv2(SvaV2Request request) {
        List<SvaV2ResponseDataSva> list = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {

            String TVTECH = (request.getTvTech().equals("CATV") || request.getTvTech().equals("DTH"))? request.getTvTech().trim(): "-";

            String query = "SELECT orden.orden, MIN(SVAT.ID) ID, SVAT.code, SVAT.description, SVAT.type, SVAT.unit, MIN(SVAT.cost) as cost," +
                    " SVAT.paymentdescription, SVAT.financingmonth, SVAT.hasequipment, SVAT.maxdecos, SVAT.arpu, SVAT.velinter, MAX(SVAT.svadefault) as svadefault, par.strvalue , coalesce(par.strfilter, 'true') as bloquePadre, SVAT.productcode as productcode " +
                    " FROM (" +
                    " SELECT DISTINCT SVA.ID, SVA.code, SVA.description, SVA.type, TRIM(SVA.unit) unit, " +
                    " CASE WHEN SVA.productcode IN ('-') AND SVA.description not like '%PUNTO ADICIONAL%' THEN 0 ELSE SVA.cost END as cost, SVA.paymentDescription, " +
                    " SVA.financingMonth, FALSE AS hasEquipment, 4 as maxDecos, 0 AS arpu, 0 AS velInter, productcode, " +
                    " CASE WHEN SVA.productcode IN ('-') AND SVA.description not like '%PUNTO ADICIONAL%' THEN 1 ELSE 0 END AS svadefault" +
                    " FROM ibmx_a07e6d02edaf552.TDP_SVA SVA, " +
                    " (SELECT * FROM ibmx_a07e6d02edaf552.CONFIG_OFFERING WHERE sourceData = ? AND sourceApp = ? AND domain = 'OFFERS' AND category = 'SVA' ) OFERTA " +
                    " WHERE SVA.prodTypeCode = OFERTA.targetData " +
                    " AND comercializable = 'SI' ";

            if(TVTECH.equals("-")){
                query += " and trim(SVA.comercializable) = 'SI' and trim(SVA.sistemas) = '"+request.getLegacyCode()+"' and (SVA.code not in('DHD','BTV','DSHD') or (SVA.code in('DHD','BTV','DSHD') and (trim(SVA.tecnologia_sva) = '-' or trim(SVA.tecnologia_sva) = 'CATV'  or trim(SVA" +
                        ".tecnologia_sva) = 'DTH') ))  ";
            }else {
                query +=" and trim(SVA.comercializable) = 'SI' and trim(SVA.sistemas) = '"+request.getLegacyCode()+"' and (SVA.code not in('DHD','BTV','DSHD') or (SVA.code in('DHD','BTV','DSHD') and (trim(SVA.tecnologia_sva) = '-' or trim(SVA.tecnologia_sva) = '"+TVTECH+"' ) )) ";
            }

            query += ") SVAT " +
                    " JOIN ibmx_a07e6d02edaf552.TDP_SVA_ADDITIONAL SVAADD ON (SVAADD.SVA_ID = SVAT.ID AND PARAMETER_ID = 6527 AND SVAADD.VALUE IN ('" + request.getSellerChannelEquivalentCampaign() + "', 'TODOS')) " +
                    " LEFT JOIN ibmx_a07e6d02edaf552.PARAMETERS par on par.domain = 'CONFIGSVA' and par.category = SVAT.code and par.element = SVAT.unit " +
                    " LEFT JOIN (select 1 as orden, 'BLOQUE FULL HD' as sva  union select 2 as code, 'BLOQUE HBO NEW' as sva  union select 3 as code, 'BLOQUE HBO NEW 20' as sva   union select 4 as code, 'BLOQUE FOX+ NEW' as sva  union select 5 as code, 'BLOQUE FOX+ NEW 20' as sva  union select 6 as code, 'BLOQUE HOT PACK' as sva  union select 7 as code, 'BLOQUE HOT PACK 20' as sva  union select 8 as code, 'BLOQUE GOLDEN PREMIER' as sva  union select 9 as code, 'BLOQUE ESTELAR' as sva  union select 10 as orden, 'BP' as sva union select 11 as orden, 'DHD' as sva  union select 12 as orden, 'DSHD' as sva  union select 13 as orden, 'DVR' as sva  union select 14 as orden, 'RSW' as sva  union select 15 as orden, 'SVAI' as sva  union select 16 as orden, 'SVAL' as sva ) orden " +
                    " on orden.sva = SVAT.unit or orden.sva = SVAT.code " +
                    " GROUP BY orden.orden, SVAT.code, SVAT.description, SVAT.type, SVAT.unit, SVAT.paymentdescription, SVAT.financingmonth, SVAT.hasequipment, SVAT.maxdecos, SVAT.arpu, SVAT.velinter, par.strvalue, par.strfilter, SVAT.productcode " +
                    " ORDER BY orden.orden asc, 7 ASC";

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getProductType());
                if (null == request.getApplicationCode()) {
                    stmt.setString(2, "APPVF");
                } else {
                    stmt.setString(2, request.getApplicationCode());
                }

                LogVass.daoQueryDAO(logger, "readSVA", "query", stmt.toString());
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    SvaV2ResponseDataSva obj = new SvaV2ResponseDataSva();
                    obj.setCode(rs.getString("code"));
                    obj.setDescription(rs.getString("description"));
                    obj.setType(rs.getString("type"));
                    obj.setUnit(rs.getString("unit"));
                    obj.setCost(Double.parseDouble(rs.getString("cost")));
                    // obj.setMaxValue(rs.getString("maxValue"));
                    obj.setPaymentDescription(rs.getString("paymentDescription"));
                    obj.setId(rs.getInt("ID"));
                    obj.setFinancingMonth(rs.getInt("financingMonth"));

                    obj.setBloquePadre(rs.getString("strvalue"));
                    obj.setClickPadre(rs.getBoolean("bloquePadre"));
                    obj.setProductCode(rs.getString("productcode"));

                    // Sprint 10 - sva por default
                    obj.setAltaPuraDefaultSVA((rs.getInt("svadefault") == 1) ? true : false);
                    list.add(obj);
                }
            }
        } catch (Exception e) {
            logger.error("Error readSVA DAO.", e);
        }
        return list;

        /*try (Connection con = Database.datasource().getConnection()) {

            String query = MY_SQL_SP.replace(Constants.MY_SQL_SP_NAME, "COMPLEMENT2")
                    .replace(Constants.MY_SQL_PARAMETER, "?,?,?");
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getProductType());
                stmt.setString(2, request.getServiceCode());
                stmt.setString(3, request.getProductCode());

                LogVass.daoQuery(logger, stmt.toString());
                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    SvaV2ResponseDataSva obj = new SvaV2ResponseDataSva();
                    obj.setId(rs.getInt("ID"));
                    obj.setCode(rs.getString("code"));
                    obj.setDescription(rs.getString("description"));
                    obj.setType(rs.getString("type"));
                    obj.setUnit(rs.getString("unit"));
                    obj.setCost(Double.parseDouble(rs.getString("cost")));
                    obj.setPaymentDescription(rs.getString("paymentDescription"));
                    obj.setFinancingMonth(rs.getInt("financingMonth"));
                    obj.setHasEquipment(rs.getString("hasEquipment"));
                    obj.setMaxDecos(rs.getString("maxDecos"));
                    obj.setArpu(Double.parseDouble(rs.getString("arpu")));
                    obj.setVelInter(rs.getString("velInter"));
                    list.add(obj);

                }
            }

        } catch (Exception e) {
            logger.info("Error Complements V2 DAO." + e);
        }

        return list;*/
    }

    public String getUbigeo(OffersRequest request) {

        String ubigeo = "000000";
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "ut.tdpCode")
                    .replace(Constants.MY_SQL_TABLE, "tdp_ubigeo_translate ut")
                    .replace(Constants.MY_SQL_WHERE, "ut.domain = ? AND ut.thirdpartyCode = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getLegacyCode());
                stmt.setString(2, request.getUbigeoCode());
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    ubigeo = rs.getString("tdpCode");
                }
            }
        } catch (Exception e) {
            logger.error("Errog getUbigeo DAO." + e);
        }

        return ubigeo;
    }

    public boolean readByPassAvve() {
        boolean byPassAvve = false;
        try (Connection con = datasource.getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "strvalue")
                    .replace(Constants.MY_SQL_TABLE, "parameters")
                    .replace(Constants.MY_SQL_WHERE, "element = 'tdp.bypass.migraciones.avve' and category = 'PARAMETER'");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    byPassAvve = Boolean.parseBoolean(rs.getString("strvalue"));
                }
            }
        } catch (Exception e) {
            logger.error("Error readByPassAvve DAO.", e);
        }
        return byPassAvve;
    }

    public List<OffersResponseData2> readOffers2(OffersRequest2 request, OffersGisResponse offersGisResponse, String filtrosGIS, String filtrosCampanias, String columnsCampanias, ApiResponse<OffersApiExpertoResponseBody> objExperto, List<TdpAutomatizadorCabecera> automatizadorCabecera) {
        List<OffersResponseData2> list = new ArrayList<>();

        boolean altaPura = false;
        //boolean migra = false;
        try (Connection con = Database.datasource().getConnection()) {
            String query = "";
            if (request.getProductSvcCode() == null || request.getProductSvcCode().length() == 0) {
                altaPura = true;
                query = getQueryOffersOpcion1((ProductConstants.PARAMETRO_VENTA_FIJA_APP.equals(request.getApplicationCode()) ? ProductConstants.PARAMETRO_VENTA_FIJA_APP : ProductConstants.PARAMETRO_VENTA_FIJA_WEB), request.getProdTypeCode(), (objExperto != null ? String.valueOf(getTopPrice(objExperto)) : String.valueOf(9999999)), filtrosGIS, filtrosCampanias, columnsCampanias);
            } else if (offersGisResponse == null) {
                //migra = true;
                query = getQueryOffersOpcion3(request.getSellerChannelEquivalentCampaign());

            } else {
                query = getQueryOffersOpcion2((ProductConstants.PARAMETRO_VENTA_FIJA_APP.equals(request.getApplicationCode()) ? ProductConstants.PARAMETRO_VENTA_FIJA_APP : ProductConstants.PARAMETRO_VENTA_FIJA_WEB), request.getProdTypeCode(), (objExperto != null ? String.valueOf(getTopPrice(objExperto)) : String.valueOf(9999999)), offersGisResponse.getVelocidadMax(), offersGisResponse.getTecInternet(), offersGisResponse.getTecTV(), offersGisResponse.getTipoSenal(), offersGisResponse.getOperacion());
            }

            try (PreparedStatement stmt = con.prepareStatement(query)) {

                logger.info("ReadOffers2 - query: " + query);
                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    OffersResponseData2 obj = new OffersResponseData2();
                    obj.setProductCode(rs.getString("productCode"));
                    obj.setProductName(rs.getString("productName"));
                    obj.setProductCodigo(rs.getString("product_codigo"));
                    obj.setDiscount(rs.getString("discount"));
                    obj.setPrice(Double.parseDouble(rs.getString("price")));
                    obj.setPromPrice(Double.parseDouble(rs.getString("promPrice")));
                    obj.setMonthPeriod(Integer.parseInt(rs.getString("monthPeriod")));
                    obj.setFinancingCost(Double.parseDouble(rs.getString("financingCost")));
                    obj.setFinancingMonth(Integer.parseInt(rs.getString("financingMonth")));
                    obj.setInstallCost(Double.parseDouble(rs.getString("installCost")));
                    obj.setReturnMonth(Integer.parseInt(rs.getString("returnMonth")));
                    obj.setReturnPeriod(rs.getString("returnPeriod"));
                    obj.setEquipmentType(rs.getString("equipmentType"));
                    obj.setLineType(rs.getString("lineType"));
                    obj.setInternetSpeed(rs.getInt("internetSpeed"));
                    obj.setCashPrice(Double.parseDouble(rs.getString("cashPrice")));
                    obj.setProductTypeCode(rs.getString("prodTypeCode"));
                    obj.setProductType(rs.getString("productType"));
                    obj.setCommercialOperation(rs.getString("commercialOperation"));
                    obj.setPaymentMethod(rs.getString("paymentMethod"));
                    obj.setCampaign(rs.getString("campaign"));
                    obj.setInternetTech(rs.getString("internetTech"));
                    obj.setTvSignal(rs.getString("tvSignal"));
                    obj.setTvTech(rs.getString("tvTech"));
                    obj.setProductCategoryCode(rs.getString("prodCategoryCode"));
                    obj.setProductCategory(rs.getString("productCategory"));
                    if (objExperto != null) {
                        obj.setExpertCode(objExperto.getBodyOut().getCodConsulta());
                    }
                    obj.setId(rs.getInt("ID"));
                    obj.setCategory(rs.getString("category"));
                    obj.setTargetType(rs.getString("targetType"));

                    /* Sprint 10 */
                    obj.setPromoInternetSpeed(Integer.parseInt((rs.getString("promspeed") == null ? "0" : rs.getString("promspeed"))));
                    obj.setPeriodoPromoInternetSpeed(Integer.parseInt((rs.getString("periodpromspeed") == null ? "0" : rs.getString("periodpromspeed"))));
                    obj.setEquipamientoLinea((rs.getString("equiplinea") == null ? "" : rs.getString("equiplinea").replace("-", "")));
                    obj.setEquipamientoInternet((rs.getString("equipinternet") == null ? "" : rs.getString("equipinternet").replace("-", "")));
                    obj.setEquipamientoTv((rs.getString("equiptv") == null ? "" : rs.getString("equiptv").replace("-", "")));
                    /* Sprint 10 */
                    if (altaPura) {
                        // Sprint 10 - campos adicionales de afinidad por entidad y ubicacion
                        obj.setAfinidadEntidad(Integer.parseInt((rs.getString("entidad_afinidad"))));
                        obj.setAfinidadUbicacion(Integer.parseInt((rs.getString("ubicacion_afinidad"))));
                        obj.setCampaign(rs.getString("campania"));
                        obj.setTargetType(rs.getString("tiporegistro"));

                    }
                    obj.setAltaPuraSvasBloqueTV(rs.getString("bloquetv"));
                    obj.setAltaPuraSvasLinea(rs.getString("svalinea"));
                    obj.setAltaPuraSvasInternet(rs.getString("svainternet"));

                    /*if (migra) {
                        obj.setAltaPuraSvasBloqueTV(rs.getString("bloquetv"));
                        obj.setAltaPuraSvasLinea(rs.getString("svalinea"));
                        obj.setAltaPuraSvasInternet(rs.getString("svainternet"));
                    }*/

                    switch (rs.getString("tvTech")) {
                        case "CATV":
                            obj.setCod_ind_sen_cms("C");
                            break;
                        case "DTH":
                            obj.setCod_ind_sen_cms("D");
                            break;
                        default:
                            obj.setCod_ind_sen_cms("");
                            break;
                    }

                    obj.setCod_cab_cms("");
                    for (TdpAutomatizadorCabecera cabecera : automatizadorCabecera) {
                        if (obj.getCod_cab_cms().equalsIgnoreCase(""))
                            obj.setCod_cab_cms(cabecera.getCabecera());
                        if (cabecera.getTvtech().equalsIgnoreCase(rs.getString("tvTech"))) {
                            obj.setCod_cab_cms(cabecera.getCabecera());
                        }
                    }
                    // COD_IND_SEN_CMS // CATV -> C, DTH -> D, No aplica -> Vacio
                    // COD_CAB_CMS // CATV -> Cabecera , DTH -> Satelital . PARA CONFIRMAR
                    list.add(obj);
                }
            }
        } catch (Exception e) {
            logger.error("Error ReadOffers2 DAO.", e);
        }
        return list;

    }

    public List<OfferResponseParameter> readOferFromGIS() {

        List<OfferResponseParameter> list = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {

            String query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "auxiliar,domain,category,element,strValue,strfilter")
                    .replace(Constants.MY_SQL_TABLE, "parameters")
                    .replace(Constants.MY_SQL_WHERE, "domain='" + ProductConstants.PARAMETRO_GIS_OFFER + "'");

            if (!query.equals("")) {

                try (PreparedStatement stmt = con.prepareStatement(query)) {
                    LogVass.daoQuery(logger, stmt.toString());

                    ResultSet rs = stmt.executeQuery();
                    while (rs.next()) {
                        OfferResponseParameter response = new OfferResponseParameter();
                        response.setAuxiliar(rs.getString("auxiliar"));
                        response.setDomain(rs.getString("domain"));
                        response.setCategory(rs.getString("category"));
                        response.setElement(rs.getString("element"));
                        response.setStrValue(rs.getString("strValue"));
                        response.setStrfilter(rs.getString("strfilter"));
                        list.add(response);
                    }
                }
            }
        } catch (Exception e) {
            logger.info("Error readOferFlujo DAO.", e);
        }
        return list;
    }

    public String getQueryOffersOpcion1(String source, String prodetypecode, String price, String filtrosGIS, String filtrosCampanias, String columnsCampanias) {
        //targetData:
        // 1 = MONO LINEA (no se deberia hacer validacion)
        // 3 = TRIO       (se debe validar la tecnologia television y tecnologia internet con su velocidad)
        // 2 = MONO BA    (se debe validar tecnologia internet y su velocidad)
        // 4 = DUO BA     (se debe validar tecnologia internet y su velocidad)
        // 5 = DUO TV     (se debe validar solo tecnologia television)
        // 6 = MONO TV    (se debe validar solo tecnologia television)

        // Que productos son de la categoria (prodCategoryCode) '110'?????

        // Deberia usarse parametros binding.... x el momento usamos literales... pendiente optimizacion

        String[] listaOperacion = new String[1];
        listaOperacion[0] = "Alta Pura";

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(
                " WITH FICHA_PROD AS (" +
                        "   SELECT DISTINCT OFERTA.category, OFERTA.targetType, CAT.*, NULL AS sva_code, cat.productName AS productodestinonombre " +
                        "   , PARAMS.STRVALUE AS PRIORIDADTECNOLOGIA, " +

                        columnsCampanias +

                        "   FROM ibmx_a07e6d02edaf552.TDP_CATALOG CAT " +
                        "   INNER JOIN (SELECT * FROM ibmx_a07e6d02edaf552.CONFIG_OFFERING WHERE sourceApp = '" + source + "' AND domain = 'OFFERS' AND category <> 'SVA' AND sourceData = '" + prodetypecode + "') OFERTA ON CAT.prodTypeCode = OFERTA.targetData AND CAT.commercialOperation = OFERTA.category  " +
                        "   LEFT JOIN ibmx_a07e6d02edaf552.PARAMETERS PARAMS ON (PARAMS.DOMAIN = 'CONFIG' AND PARAMS.CATEGORY = 'OFFERING_TECHNOLOGY_PRIORITY' AND PARAMS.ELEMENT = CAT.internetTech) " +

                        "   JOIN ibmx_a07e6d02edaf552.TDP_CATALOG_ADITIONAL CATCANAL ON (CAT.ID = CATCANAL.PRODUCT_ID AND CATCANAL.PARAMETER_ID = 6527)" +
                        "   JOIN ibmx_a07e6d02edaf552.TDP_CATALOG_ADITIONAL CATENTIDAD ON (CAT.ID = CATENTIDAD.PRODUCT_ID AND CATENTIDAD.PARAMETER_ID = 6528) " +
                        "   JOIN ibmx_a07e6d02edaf552.TDP_CATALOG_ADITIONAL CATUBICACION ON (CAT.ID = CATUBICACION.PRODUCT_ID AND CATUBICACION.PARAMETER_ID = 6529) " +
                        "   JOIN ibmx_a07e6d02edaf552.TDP_CATALOG_ADITIONAL CATCAMPANA ON (CAT.ID = CATCAMPANA.PRODUCT_ID AND CATCAMPANA.PARAMETER_ID = 6530) " +

                        "   WHERE " +
                        "       (CAT.prodCategoryCode = '110' " +
                        "           OR ( " +
                        "               CAT.commercialoperation in (" + getCampoCadena(listaOperacion, "CAT.commercialoperation") + ") " +
                        "               AND ( ");


        //filtros por gis
        queryBuilder.append(filtrosGIS);

        queryBuilder.append("           )" +
                "       )" +
                " )");

        //filtros por campanias
        queryBuilder.append(filtrosCampanias)
                //usando tablas temporales AFINIDAD_UBICACION y FICHA_PROD
                .append(" ORDER BY 55, 56, CATCAMPANA.VALUE, CAT.priority ASC, CAT.price DESC, PARAMS.STRVALUE")
                .append(" ), ")
                .append(" AFINIDAD_UBICACION AS (")
                .append("   SELECT ")
                .append("       MIN(UBICACION_AFINIDAD) MIN_UBICACION_AFINIDAD,")
                .append("       CASE WHEN (select STRVALUE from ibmx_a07e6d02edaf552.parameters where element = 'catalog.campanias.AllProv') = 'true' THEN MAX(UBICACION_AFINIDAD) ELSE -1 END MAX_UBICACION_AFINIDAD ")
                .append("   FROM FICHA_PROD")
                .append(" )")
                //no es buena practica usar *... pendiente cambiar
                .append(" SELECT * FROM FICHA_PROD WHERE ubicacion_afinidad IN (")
                .append("       SELECT MIN_UBICACION_AFINIDAD FROM AFINIDAD_UBICACION")
                .append("       UNION ALL")
                .append("       SELECT MAX_UBICACION_AFINIDAD FROM AFINIDAD_UBICACION")
                .append(" )")
                .append(" ORDER BY campania, priority ASC, price DESC, internetspeed desc, PRIORIDADTECNOLOGIA");

        return queryBuilder.toString();
    }

    public String getQueryOffersOpcion1Old(String source, String prodetypecode, String price, String[] listaVelocidad, String[] listainternet, String[] listatv, String[] listaSignal, String[] listaOperacion) {
        String tecnologiaList = getCampoCadena(listainternet, "CAT.internetTech");

        String query = "SELECT DISTINCT OFERTA.category, OFERTA.targetType, CAT.*, NULL AS sva_code, cat.productName AS productodestinonombre " +
                " , PARAMS.STRVALUE " +
                "FROM ibmx_a07e6d02edaf552.TDP_CATALOG CAT " +
                "INNER JOIN (SELECT * FROM ibmx_a07e6d02edaf552.CONFIG_OFFERING WHERE sourceApp = '" + source + "' AND domain = 'OFFERS' AND category <> 'SVA' AND sourceData = '" + prodetypecode + "') OFERTA ON CAT.prodTypeCode = OFERTA.targetData AND CAT.commercialOperation = OFERTA.category  " +
                " LEFT JOIN ibmx_a07e6d02edaf552.PARAMETERS PARAMS ON (PARAMS.DOMAIN = 'CONFIG' AND PARAMS.CATEGORY = 'OFFERING_TECHNOLOGY_PRIORITY' AND PARAMS.ELEMENT = CAT.internetTech) " +
                "WHERE " +
                "   (CAT.prodCategoryCode = '110' " +
                "    OR (OFERTA.targetData = '4' AND CAT.internetSpeed" + getCampoNumerico(listaVelocidad, "CAT.internetSpeed") + " AND CAT.internetTech in (" + tecnologiaList + ") )" +
                "         OR (OFERTA.targetData = '5' AND CAT.tvTech in (" + getCampoCadena(listatv, "CAT.tvTech") + ")  AND CAT.internetTech in (" + tecnologiaList + "))" +
                "         OR (OFERTA.targetData = '6' AND CAT.tvTech in (" + getCampoCadena(listatv, "CAT.tvTech") + ") )" +
                "         OR (     OFERTA.targetData <> '4' " +
                "              AND OFERTA.targetData <> '5' " +
                "              AND OFERTA.targetData <> '6' " +
                "              AND CAT.price <= coalesce(" + price + ", 9999999)" +
                "              AND CAT.internetSpeed" + getCampoNumerico(listaVelocidad, "CAT.internetSpeed") + " " +
                "              AND CAT.internetTech in (" + getCampoCadena(listainternet, "CAT.internetTech") + ") " +
                "              AND CAT.tvTech in (" + getCampoCadena(listatv, "CAT.tvTech") + ") " +
                "              AND CAT.commercialoperation in (" + getCampoCadena(listaOperacion, "CAT.commercialoperation") + ") " +
                "            )" +
                "        )" +
                " ORDER BY CAT.priority ASC, CAT.price DESC, PARAMS.STRVALUE";

        return query;
    }

    public String getQueryOffersOpcion2(String source, String prodetypecode, String price, String[] listaVelocidad, String[] listainternet, String[] listatv, String[] listaSignal, String[] listaOperacion) {

        String query = "SELECT con.category, con.targetType, cat.* " +
                "FROM ibmx_a07e6d02edaf552.tdp_catalog cat " +
                "LEFT JOIN ibmx_a07e6d02edaf552.config_offering con on con.targetdata = cat.prodTypeCode and cat.commercialOperation = con.category " +
                "WHERE cat.cashPrice = 0 " +
                "    and (con.sourceData = '6' or cat.commercialOperation <> 'Alta Pura') " +
                "    and con.sourceApp = '" + source + "' " +
                "    and con.domain = 'OFFERS' " +
                "    and con.category <> 'SVA'" +
                "    and con.sourceData = '" + prodetypecode + "' " +
                "    and (cat.prodCategoryCode = '110'" +
                "         or (con.targetData = '4' and cat.internetSpeed" + getCampoNumerico(listaVelocidad, "CAT.internetSpeed") + " and cat.internetTech in (" + getCampoCadena(listainternet, "CAT.internetTech") + ") )" +
                "         or ((con.targetData = '5' or con.targetData = '6') and cat.tvTech in (" + getCampoCadena(listatv, "CAT.tvTech") + ") )" +
                "         or (con.targetData not in ('4', '5', '6') and CAT.price <= coalesce(" + price + ", 9999999)" +
                "             and CAT.internetSpeed" + getCampoCadena(listaVelocidad, "CAT.internetSpeed") + " " +
                "             and CAT.tvTech in (" + getCampoCadena(listatv, "CAT.tvTech") + ") " +
                "             and CAT.commercialoperation in (" + getCampoCadena(listaOperacion, "CAT.commercialoperation") + ") " +
                "            )" +
                "         )" +
                " order by cat.priority asc, cat.price desc";

        return query;
    }

    public String getQueryOffersOpcion3(String canal) {

        String query = "SELECT cat.commercialoperation as category, case when commercialoperation='Migracion' and cat.prodtypecode = '6' then CAST('CMS' AS varchar(10)) else CAST('ATIS' AS varchar(10)) end as targetType, " +
                "cat.id, cat.commercialoperation, CATCAMPANA.value as campaign, cat.campaingcode, cat.priority, cat.prodtypecode, cat.producttype, cat.prodcategorycode, cat.productcategory, cat.productcode, cat.productname, cat.bloquetv, cat.svalinea, cat.svainternet, cat.discount, cat.price, cat.promprice, cat.monthperiod, cat.installcost, cat.linetype, cat.paymentmethod, cat.cashprice, cat.financingcost, cat.financingmonth, cat.equipmenttype, cat.returnmonth, cat.returnperiod, cat.internetspeed, cat.tvtech, cat.promspeed, cat.periodpromspeed, cat.equiplinea, cat.equipinternet, cat.equiptv, cat.internettech, cat.tvsignal, cat.product_codigo " +
                "FROM ibmx_a07e6d02edaf552.tdp_catalog cat " +
                "JOIN ibmx_a07e6d02edaf552.tdp_catalog_aditional CATCAMPANA ON ( cat.id = CATCAMPANA.product_id AND CATCAMPANA.parameter_id = 6530 ) " +
                "LEFT JOIN (SELECT auxiliar, element FROM ibmx_a07e6d02edaf552.parameters WHERE category = 'OFFERING_TECHNOLOGY_PRIORITY' AND domain = 'CONFIG') tec on tec.element = cat.internettech " +
                "LEFT JOIN ibmx_a07e6d02edaf552.tdp_catalog_aditional canal ON (cat.id = canal.product_id AND canal.parameter_id = 6527) " +
                "WHERE cat.cashPrice = 0 " +
                "and cast(prodcategorycode as int) >= 200 " +
                (canal == null || canal.equalsIgnoreCase("") ? "" : ("and canal.value IN ('" + canal + "', 'TODOS') ")) +
                "order by cat.prodtypecode, cat.productcode, cat.prodcategorycode, cat.priority asc, cat.price desc, tec.auxiliar, case when canal.value = 'TODOS' then 1 else 0 end asc";


        return query;
    }

    public String getCampoCadena(String[] lista, String campo) {

        String respuesta = "";

        for (int i = 0; i < lista.length; i++) {
            if (lista[i] != null) {
                if (!lista[i].equalsIgnoreCase("")) {
                    if (!lista[i].equalsIgnoreCase("null")) {

                        respuesta = respuesta + "coalesce(NULLIF('" + lista[i] + "','')," + campo + "),";

                    }
                }
            }
        }

        if (!respuesta.equals("")) {
            respuesta = respuesta.substring(0, respuesta.length() - 1);
        } else {
            respuesta = " " + campo + " ";
        }

        return respuesta;
    }

    public String getCampoCadenaUPPER(String[] lista, String campo) {

        String respuesta = "";

        for (int i = 0; i < lista.length; i++) {
            if (lista[i] != null) {
                if (!lista[i].equalsIgnoreCase("")) {
                    if (!lista[i].equalsIgnoreCase("null")) {

                        respuesta = respuesta + "coalesce(UPPER(NULLIF('" + lista[i] + "','')),UPPER(" + campo + ")),";

                    }
                }
            }
        }

        if (!respuesta.equals("")) {
            respuesta = respuesta.substring(0, respuesta.length() - 1);
        } else {
            respuesta = " UPPER(" + campo + ") ";
        }

        return respuesta;
    }

    public String getCampoNumerico(String[] lista, String campo) {

        String respuesta = "";

        for (int i = 0; i < lista.length; i++) {
            if (lista[i] != null) {
                if (!lista[i].equalsIgnoreCase("")) {
                    if (!lista[i].equalsIgnoreCase("null")) {

                        respuesta = respuesta + campo + " <= coalesce(NULLIF('" + lista[i] + "',9999999),9999999) AND ";

                    }
                }
            }

        }
        if (!respuesta.equals("")) {
            respuesta = respuesta.substring(campo.length(), respuesta.length() - 4);
        } else {
            respuesta = " <= 9999999";
        }
        return respuesta;
    }

    public List<SvaV2ResponseDataSva> getSvaByProductCode_SINMODIFICAR(List<String> lstProductCode, Double maxDecos, SvaV2Request request, String tipo) {

        if (lstProductCode == null || lstProductCode.isEmpty()) {
            logger.info("getSvaByProductCode => lstProductCode:{}");
            return new ArrayList<SvaV2ResponseDataSva>();
        }

        List<SvaV2ResponseDataSva> lstIdSVAs = new ArrayList<SvaV2ResponseDataSva>();
        SvaV2ResponseDataSva sva = null;
        String productCodes = "";

        try (Connection con = Database.datasource().getConnection()) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < lstProductCode.size(); i++) {
                builder.append("?,");
            }

            /*
            String query = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_sva s " +
                    "WHERE s.productCode in (" + builder.deleteCharAt(builder.length() - 1).toString() + ") " +
                    "and (case when code IN ('DHD', 'DSHD', 'DVR') then cast(unit as numeric ) <= " + maxDecos + " else true end)" +
                    "order by code, cost";
            */


            String query2 = "SELECT MIN(SVAT.ID) ID, SVAT.code, SVAT.description, SVAT.type, SVAT.unit, SVAT.cost," +
                    " SVAT.paymentdescription, SVAT.financingmonth, SVAT.hasequipment, SVAT.maxdecos, SVAT.arpu, SVAT.velinter, SVAT.svadefault, par.strvalue , coalesce(par.strfilter, 'true') as bloquePadre" +
                    " FROM (" +
                    " SELECT DISTINCT SVA.ID, SVA.code, SVA.description, SVA.type, TRIM(SVA.unit) unit, " +
                    " CASE WHEN SVA.productcode IN (" + request.getMigracionSvasAll() + ") AND SVA.description not like '%PUNTO ADICIONAL%' THEN 0 ELSE SVA.cost END as cost, SVA.paymentDescription, " +
                    " SVA.financingMonth, FALSE AS hasEquipment, 4 as maxDecos, 0 AS arpu, 0 AS velInter, productcode, " +
                    " CASE WHEN SVA.productcode IN (" + request.getMigracionSvasAll() + ") AND SVA.description not like '%PUNTO ADICIONAL%' THEN 1 ELSE 0 END AS svadefault" +
                    " FROM ibmx_a07e6d02edaf552.TDP_SVA SVA, " +
                    " (SELECT * FROM ibmx_a07e6d02edaf552.CONFIG_OFFERING WHERE sourceData = ? AND sourceApp = ? AND domain = 'OFFERS' AND category = 'SVA' ) OFERTA " +
                    " WHERE SVA.prodTypeCode = OFERTA.targetData ) SVAT " +
                    " LEFT JOIN ibmx_a07e6d02edaf552.PARAMETERS par on par.domain = 'CONFIGSVA' and par.category = SVAT.code and par.element = SVAT.unit " +
                    " GROUP BY SVAT.code, SVAT.description, SVAT.type, SVAT.unit, SVAT.cost, SVAT.paymentdescription, SVAT.financingmonth, SVAT.hasequipment, SVAT.maxdecos, SVAT.arpu, SVAT.velinter, SVAT.svadefault, par.strvalue,par.strfilter " +
                    " ORDER BY SVAT.CODE DESC, SVAT.COST ASC";

            String query = "SELECT s.id, s.prodtypecode, s.code, s.description, s.unit, par.strvalue, coalesce(par.strfilter, 'true') as bloquePadre, ";

                    if(tipo.equalsIgnoreCase("OUT")){
                        query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ")  THEN 0 ELSE s.cost END as cost, ";
                    }
                    else{
                        query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ") AND s.description not like '%PUNTO ADICIONAL%' THEN 0 ELSE s.cost END as cost, ";
                    }

                    query += "s.paymentdescription, s.type, s.financingmonth, s.productcode, ";

                    if(tipo.equalsIgnoreCase("")){
                        query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ") THEN 1 ELSE 0 END AS svadefault ";
                    }
                    else{
                        query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ") AND s.description not like '%PUNTO ADICIONAL%' THEN 1 ELSE 0 END AS svadefault ";
                    }

                    query += "FROM ibmx_a07e6d02edaf552.tdp_sva s " +
                    "LEFT JOIN ibmx_a07e6d02edaf552.PARAMETERS par on par.domain = 'CONFIGSVA' and par.category = s.code and par.element = s.unit " +
                    "LEFT JOIN (select 1 as orden, 'BLOQUE FULL HD' as sva  union select 2 as code, 'BLOQUE HBO NEW' as sva  union select 3 as code, 'BLOQUE HBO NEW 20' as sva   union select 4 as code, 'BLOQUE FOX+ NEW' as sva  union select 5 as code, 'BLOQUE FOX+ NEW 20' as sva  union select 6 as code, 'BLOQUE HOT PACK' as sva  union select 7 as code, 'BLOQUE HOT PACK 20' as sva  union select 8 as code, 'BLOQUE GOLDEN PREMIER' as sva  union select 9 as code, 'BLOQUE ESTELAR' as sva  union select 10 as orden, 'BP' as sva union select 11 as orden, 'DHD' as sva  union select 12 as orden, 'DSHD' as sva  union select 13 as orden, 'DVR' as sva  union select 14 as orden, 'RSW' as sva  union select 15 as orden, 'SVAI' as sva  union select 16 as orden, 'SVAL' as sva) orden " +
                    "on orden.sva = s.unit or orden.sva = s.code ";

            if (request.getProductCode() == null) {
                // Se aplica el filtro de canal solo para operacion comercial "SVAS"
                // Si es que el productCode es nulo significa que es operacion comercial "SVAS"
                // entonces adicionamos la restriccion
                query += " JOIN ibmx_a07e6d02edaf552.TDP_SVA_ADDITIONAL SVAADD ON (SVAADD.SVA_ID = S.ID AND PARAMETER_ID = 6527 AND SVAADD.VALUE IN ('" + request.getSellerChannelEquivalentCampaign() + "', 'TODOS')) ";
            }
            query += " WHERE s.productCode in (" + builder.deleteCharAt(builder.length() - 1).toString() + ") " +
                    "and (case when s.code IN ('DHD', 'DSHD', 'DVR') then cast(s.unit as numeric ) <= " + maxDecos + " else true end) " +
                    "order by orden.orden asc, s.cost";

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                int index = 1;
                for (Object productCode : lstProductCode) {
                    stmt.setObject(index++, productCode);
                }
                LogVass.daoQueryDAO(logger, "getSvaByProductCode", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    sva = new SvaV2ResponseDataSva();
                    if (rs.getString("id") != null) {
                        sva.setId(Integer.parseInt(rs.getString("id")));
                    }
                    if (rs.getString("code") != null) {
                        sva.setCode(rs.getString("code"));
                    }
                    if (rs.getString("description") != null) {
                        sva.setDescription(rs.getString("description"));
                    }
                    if (rs.getString("type") != null) {
                        sva.setType(rs.getString("type"));
                    }
                    if (rs.getString("unit") != null) {
                        sva.setUnit(rs.getString("unit"));
                    }
                    if (rs.getString("cost") != null) {
                        sva.setCost(Double.parseDouble(rs.getString("cost")));
                    }
                    if (rs.getString("paymentDescription") != null) {
                        sva.setPaymentDescription(rs.getString("paymentDescription"));
                    }
                    if (rs.getString("financingMonth") != null) {
                        sva.setFinancingMonth(Integer.parseInt(rs.getString("financingMonth")));
                    }
                    if (rs.getString("productCode") != null) {
                        sva.setProductCode(rs.getString("productCode"));
                    }
                    if (rs.getString("svadefault") != null) {
                        sva.setAltaPuraDefaultSVA((rs.getInt("svadefault") == 1) ? true : false);
                    }
                    if (rs.getString("strvalue") != null) {
                        sva.setBloquePadre(rs.getString("strvalue"));
                    }

                    sva.setClickPadre(rs.getBoolean("bloquePadre"));

                    if (!isSvaInList(lstIdSVAs, sva) || !tipo.equalsIgnoreCase("OUT"))
                        lstIdSVAs.add(sva);
                }
            }
        } catch (Exception e) {
            logger.error("Error getSvaByProductCode DAO", e);
        }
        return lstIdSVAs;
    }



    public List<SvaV2ResponseDataSva> getSvaByProductCode(List<String> lstProductCode, Double maxDecos, SvaV2Request request, String tipo) {
        System.out.println(">>>getSvaByProductCode:");
        if (lstProductCode == null || lstProductCode.isEmpty()) {
            logger.info("getSvaByProductCode => lstProductCode:{}");
            return new ArrayList<SvaV2ResponseDataSva>();
        }

        List<SvaV2ResponseDataSva> lstIdSVAs = new ArrayList<SvaV2ResponseDataSva>();
        SvaV2ResponseDataSva sva = null;
        String productCodes = "";

        String TVTECH = "";



        try (Connection con = Database.datasource().getConnection()) {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < lstProductCode.size(); i++) {
                builder.append("?,");
            }

            String query ="";

            if(request.getTvTech() != null && !request.getTvTech().equalsIgnoreCase("") && request.getTargetType() != null && !request.getTargetType().equalsIgnoreCase("")){
                
                TVTECH = (request.getTvTech().equals("CATV") || request.getTvTech().equals("DTH"))? request.getTvTech().trim(): "-";
                query = "SELECT s.id, s.prodtypecode, s.code, s.description, s.unit, par.strvalue, coalesce(par.strfilter, 'true') as bloquePadre, s.comercializable, s.sistemas, s.tecnologia_sva, ";

                if(tipo.equalsIgnoreCase("OUT")){
                    query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ")   THEN 0 ELSE s.cost END as cost, ";
                }
                else{
                    query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ")   AND s.description not like '%PUNTO ADICIONAL%' THEN 0 ELSE s.cost END as cost, ";
                }

                query += "s.paymentdescription, s.type, s.financingmonth, s.productcode, ";

                if(tipo.equalsIgnoreCase("")){
                    query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ")   THEN 1 ELSE 0 END AS svadefault ";
                }
                else{
                    query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ")   AND s.description not like '%PUNTO ADICIONAL%' THEN 1 ELSE 0 END AS svadefault ";
                }

                query += "FROM ibmx_a07e6d02edaf552.tdp_sva s " +
                        "LEFT JOIN ibmx_a07e6d02edaf552.PARAMETERS par on par.domain = 'CONFIGSVA' and par.category = s.code and par.element = s.unit " +
                        "LEFT JOIN (select 1 as orden, 'BLOQUE FULL HD' as sva  union select 2 as code, 'BLOQUE HBO NEW' as sva  union select 3 as code, 'BLOQUE HBO NEW 20' as sva   union select 4 as code, 'BLOQUE FOX+ NEW' as sva  union select 5 as code, 'BLOQUE FOX+ NEW 20' as sva  union select 6 as code, 'BLOQUE HOT PACK' as sva  union select 7 as code, 'BLOQUE HOT PACK 20' as sva  union select 8 as code, 'BLOQUE GOLDEN PREMIER' as sva  union select 9 as code, 'BLOQUE ESTELAR' as sva  union select 10 as orden, 'BP' as sva union select 11 as orden, 'DHD' as sva  union select 12 as orden, 'DSHD' as sva  union select 13 as orden, 'DVR' as sva  union select 14 as orden, 'RSW' as sva  union select 15 as orden, 'SVAI' as sva  union select 16 as orden, 'SVAL' as sva) orden " +
                        "on orden.sva = s.unit or orden.sva = s.code ";

                //if (request.getProductCode() == null) {
                    query += " JOIN ibmx_a07e6d02edaf552.TDP_SVA_ADDITIONAL SVAADD ON (SVAADD.SVA_ID = S.ID AND PARAMETER_ID = 6527 AND SVAADD.VALUE IN ('" + request.getSellerChannelEquivalentCampaign() + "', 'TODOS')) ";
                //}

                /*query += " WHERE s.productCode in (" + builder.deleteCharAt(builder.length() - 1).toString() + ") " +
                        " and trim(s.comercializable) = 'SI' and trim(s.sistemas) = 'ATIS' and (trim(s.tecnologia_sva) = '-' or trim(s.tecnologia_sva) = 'CATV' )  "+
                        "and (case when s.code IN ('DHD', 'DSHD', 'DVR') then cast(s.unit as numeric ) <= " + maxDecos + " else true end) " +
                        "order by orden.orden asc, s.cost";*/

                if(TVTECH.equals("-")){
                    //Para (3ra opcion)svs no hay parametro en offert

                    query += " WHERE s.productCode in (" + builder.deleteCharAt(builder.length() - 1).toString() + ") " +
                            " and trim(s.comercializable) = 'SI' and trim(s.sistemas) = '"+request.getTargetType()+"' and (s.code not in('DHD','BTV','DSHD') or (s.code in('DHD','BTV','DSHD') and (trim(s.tecnologia_sva) = '-' or trim(s.tecnologia_sva) = 'CATV'  or trim(s.tecnologia_sva) = 'DTH') ))  "+
                            "and (case when s.code IN ('DHD', 'DSHD', 'DVR') then cast(s.unit as numeric ) <= " + maxDecos + " else true end) " +
                            "order by orden.orden asc, s.cost";

                }else {
                    query += " WHERE s.productCode in (" + builder.deleteCharAt(builder.length() - 1).toString() + ") " +
                            " and trim(s.comercializable) = 'SI' and trim(s.sistemas) = '"+request.getTargetType()+"' and (s.code not in('DHD','BTV','DSHD') or (s.code in('DHD','BTV','DSHD') and (trim(s.tecnologia_sva) = '-' or trim(s.tecnologia_sva) = '"+TVTECH+"' ) )) "+
                            "and (case when s.code IN ('DHD', 'DSHD', 'DVR') then cast(s.unit as numeric ) <= " + maxDecos + " else true end) " +
                            "order by orden.orden asc, s.cost";

                }

            }else{

                /**
                 * Query sin Mmodificación
                 */

                query = "SELECT s.id, s.prodtypecode, s.code, s.description, s.unit, par.strvalue, coalesce(par.strfilter, 'true') as bloquePadre, ";

                if(tipo.equalsIgnoreCase("OUT")){
                    query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ")   THEN 0 ELSE s.cost END as cost, ";
                }
                else{
                    query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ")   AND s.description not like '%PUNTO ADICIONAL%' THEN 0 ELSE s.cost END as cost, ";
                }

                query += "s.paymentdescription, s.type, s.financingmonth, s.productcode, ";

                if(tipo.equalsIgnoreCase("")){
                    query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ")   THEN 1 ELSE 0 END AS svadefault ";
                }
                else{
                    query += "CASE WHEN s.productcode IN (" + request.getMigracionSvasAll() + ")   AND s.description not like '%PUNTO ADICIONAL%' THEN 1 ELSE 0 END AS svadefault ";
                }


                query += "FROM ibmx_a07e6d02edaf552.tdp_sva s " +
                        "LEFT JOIN ibmx_a07e6d02edaf552.PARAMETERS par on par.domain = 'CONFIGSVA' and par.category = s.code and par.element = s.unit " +
                        "LEFT JOIN (select 1 as orden, 'BLOQUE FULL HD' as sva  union select 2 as code, 'BLOQUE HBO NEW' as sva  union select 3 as code, 'BLOQUE HBO NEW 20' as sva   union select 4 as code, 'BLOQUE FOX+ NEW' as sva  union select 5 as code, 'BLOQUE FOX+ NEW 20' as sva  union select 6 as code, 'BLOQUE HOT PACK' as sva  union select 7 as code, 'BLOQUE HOT PACK 20' as sva  union select 8 as code, 'BLOQUE GOLDEN PREMIER' as sva  union select 9 as code, 'BLOQUE ESTELAR' as sva  union select 10 as orden, 'BP' as sva union select 11 as orden, 'DHD' as sva  union select 12 as orden, 'DSHD' as sva  union select 13 as orden, 'DVR' as sva  union select 14 as orden, 'RSW' as sva  union select 15 as orden, 'SVAI' as sva  union select 16 as orden, 'SVAL' as sva) orden " +
                        "on orden.sva = s.unit or orden.sva = s.code ";

                //if (request.getProductCode() == null) {
                    // Se aplica el filtro de canal solo para operacion comercial "SVAS"
                    // Si es que el productCode es nulo significa que es operacion comercial "SVAS"
                    // entonces adicionamos la restriccion
                    query += " JOIN ibmx_a07e6d02edaf552.TDP_SVA_ADDITIONAL SVAADD ON (SVAADD.SVA_ID = S.ID AND PARAMETER_ID = 6527 AND SVAADD.VALUE IN ('" + request.getSellerChannelEquivalentCampaign() + "', 'TODOS')) ";
                //}
                query += " WHERE s.productCode in (" + builder.deleteCharAt(builder.length() - 1).toString() + ") " +
                        "and (case when s.code IN ('DHD', 'DSHD', 'DVR') then cast(s.unit as numeric ) <= " + maxDecos + " else true end) " +
                        "and s.comercializable = 'SI' " +
                        "order by orden.orden asc, s.cost";


            }




            try (PreparedStatement stmt = con.prepareStatement(query)) {
                int index = 1;
                for (Object productCode : lstProductCode) {
                    stmt.setObject(index++, productCode);
                }
                LogVass.daoQueryDAO(logger, "getSvaByProductCode", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    sva = new SvaV2ResponseDataSva();
                    if (rs.getString("id") != null) {
                        sva.setId(Integer.parseInt(rs.getString("id")));
                    }
                    if (rs.getString("code") != null) {
                        sva.setCode(rs.getString("code"));
                    }
                    if (rs.getString("description") != null) {
                        sva.setDescription(rs.getString("description"));
                    }
                    if (rs.getString("type") != null) {
                        sva.setType(rs.getString("type"));
                    }
                    if (rs.getString("unit") != null) {
                        sva.setUnit(rs.getString("unit"));
                    }
                    if (rs.getString("cost") != null) {
                        sva.setCost(Double.parseDouble(rs.getString("cost")));
                    }
                    if (rs.getString("paymentDescription") != null) {
                        sva.setPaymentDescription(rs.getString("paymentDescription"));
                    }
                    if (rs.getString("financingMonth") != null) {
                        sva.setFinancingMonth(Integer.parseInt(rs.getString("financingMonth")));
                    }
                    if (rs.getString("productCode") != null) {
                        sva.setProductCode(rs.getString("productCode"));
                    }
                    if (rs.getString("svadefault") != null) {
                        sva.setAltaPuraDefaultSVA((rs.getInt("svadefault") == 1) ? true : false);
                    }
                    if (rs.getString("strvalue") != null) {
                        sva.setBloquePadre(rs.getString("strvalue"));
                    }

                    sva.setClickPadre(rs.getBoolean("bloquePadre"));

                    if (!isSvaInList(lstIdSVAs, sva) || !tipo.equalsIgnoreCase("OUT"))
                        lstIdSVAs.add(sva);
                }
            }
        } catch (Exception e) {
            logger.error("Error getSvaByProductCode DAO", e);
        }
        return lstIdSVAs;
    }











    private boolean isSvaInList(List<SvaV2ResponseDataSva> listSvas, SvaV2ResponseDataSva sva) {
        for (SvaV2ResponseDataSva svaItem : listSvas) {
            if (svaItem.getProductCode().equalsIgnoreCase(sva.getProductCode())){
                return true;
            }
        }
        return false;
    }

    public List<SvaV2ResponseDataSva> getEquipamientoDecos(String productCode, String applicationCode) {

        List<SvaV2ResponseDataSva> lstIdSVAs = new ArrayList<SvaV2ResponseDataSva>();
        SvaV2ResponseDataSva sva = null;
        String productCodes = "";
        try (Connection con = Database.datasource().getConnection()) {
            String query = "SELECT SVAS.*, 0 " +
                    "FROM ibmx_a07e6d02edaf552.TDP_SVA SVAS " +
                    "INNER JOIN ibmx_a07e6d02edaf552.CONFIG_OFFERING OFERTA ON  SVAS.prodTypeCode = OFERTA.targetData " +
                    "WHERE CODE IN ('EQUIPAMIENTO', 'DHD', 'DSHD') " +
                    "AND sourceApp = ? " +
                    "AND domain = 'OFFERS' " +
                    "AND category = 'SVA' " +
                    "AND sourceData = ? " +
                    "ORDER BY description desc, unit";

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setObject(1, applicationCode);
                stmt.setObject(2, productCode);
                LogVass.daoQueryDAO(logger, "getEquipamientoDecos", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    sva = new SvaV2ResponseDataSva();
                    if (rs.getString("id") != null) {
                        sva.setId(Integer.parseInt(rs.getString("id")));
                    }
                    if (rs.getString("code") != null) {
                        sva.setCode(rs.getString("code"));
                    }
                    if (rs.getString("description") != null) {
                        sva.setDescription(rs.getString("description"));
                    }
                    if (rs.getString("type") != null) {
                        sva.setType(rs.getString("type"));
                    }
                    if (rs.getString("unit") != null) {
                        sva.setUnit(rs.getString("unit"));
                    }
                    if (rs.getString("cost") != null) {
                        sva.setCost(Double.parseDouble(rs.getString("cost")));
                    }
                    if (rs.getString("paymentDescription") != null) {
                        sva.setPaymentDescription(rs.getString("paymentDescription"));
                    }
                    if (rs.getString("financingMonth") != null) {
                        sva.setFinancingMonth(Integer.parseInt(rs.getString("financingMonth")));
                    }
                    if (rs.getString("productCode") != null) {
                        sva.setProductCode(rs.getString("productCode"));
                    }
                    lstIdSVAs.add(sva);
                }
            }
        } catch (Exception e) {
            logger.error("Error al obtener id de sva!", e);
        }
        return lstIdSVAs;
    }

    public String getProductNameByProductCode(String productCode) {
        String productName = null;
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "DISTINCT productname")
                    .replace(Constants.MY_SQL_TABLE, "tdp_catalog")
                    .replace(Constants.MY_SQL_WHERE, "productcode = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, productCode);
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    productName = rs.getString("productname");
                }
            }
        } catch (Exception e) {
            logger.error("Errog getProductNameByProductCode DAO." + e);
        }

        return productName;
    }

    public String getProductProductCodeById(int productId) {
        String productCode = null;
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "productcode")
                    .replace(Constants.MY_SQL_TABLE, "tdp_catalog")
                    .replace(Constants.MY_SQL_WHERE, "id = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setInt(1, productId);
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    productCode = rs.getString("productcode");
                }
            }
        } catch (Exception e) {
            logger.error("Errog getProductNameByProductCode DAO." + e);
        }
        return productCode;
    }

    public String getProductTvTechById(int productId) {
        String productCode = null;
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "tvtech")
                    .replace(Constants.MY_SQL_TABLE, "tdp_catalog")
                    .replace(Constants.MY_SQL_WHERE, "id = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setInt(1, productId);
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    productCode = rs.getString("tvtech");
                }
            }
        } catch (Exception e) {
            logger.error("Errog getProductNameByProductCode DAO." + e);
        }
        return productCode;
    }

    public List<String> getOrderListForAudioProcess(String hoy) {
        List<String> idOrderList = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "ord.id")
                    .replace(Constants.MY_SQL_TABLE, "azure_request azu" +
                            " LEFT JOIN ibmx_a07e6d02edaf552.order ord ON ord.id = azu.order_id")
                    .replace(Constants.MY_SQL_WHERE,
                            "azu.status != 'OK'"
                                    + " AND coalesce(azu.encrypted, '') != 'OK'"
                                    + " AND ord.appcode != 'WEBVF'"
                                    + " AND ord.statusaudio NOT IN ('1', '3', '4', '6')"
                                    + " AND ord.registrationdate < CAST(? AS TIMESTAMP)"
                                    + " ORDER BY ord.registrationdate ASC"
                            //+ " LIMIT 100"
                    );

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, hoy);
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    idOrderList.add(rs.getString("id"));
                }
            }
        } catch (Exception e) {
            logger.error("Errog getProductNameByProductCode DAO." + e);
        }

        return idOrderList;
    }

    public OutResponseDetalle filterDetalleOut(OutRequestDetalle request) {
        OutResponseDetalle outResponseDetalle = new OutResponseDetalle();

        try (Connection con = Database.datasource().getConnection()) {


            String query ="SELECT distinct o.client_nombre,o.client_apellido_paterno,o.client_apellido_materno,o.client_nacimiento, " +
                    "cus.customerphone,o.client_tipo_doc,o.client_numero_doc,v.departamento,v.provincia,v.distrito,v.direccion, " +
                    "o.address_referencia, v.nombre_producto, o.product_tec_inter, o.product_category, o.product_internet_vel, " +
                    "o.product_equip_tv, o.product_equip_inter, o.product_equip_line, o.product_pay_method, " +
                    "o.product_precio_normal, o.product_precio_promo, o.product_precio_promo_mes, " +
                    "o.sva_nombre_1, o.sva_precio_1, o.sva_nombre_2, o.sva_precio_2, o.sva_nombre_3, o.sva_precio_3, " +
                    "o.sva_nombre_4, o.sva_precio_4, o.sva_nombre_5, o.sva_precio_5, o.sva_nombre_6, o.sva_precio_6, " +
                    "o.sva_nombre_7, o.sva_precio_7, o.sva_nombre_8, o.sva_precio_8, o.sva_nombre_9, o.sva_precio_9, " +
                    "o.sva_nombre_10, o.sva_precio_10, o.affiliation_data_protection, o.affiliation_electronic_invoice, " +
                    "o.affiliation_parental_protection " +
                    "FROM ibmx_a07e6d02edaf552.tdp_order o inner join ibmx_a07e6d02edaf552.tdp_visor v on o.order_id = v.id_visor " +
                    "inner join ibmx_a07e6d02edaf552.customer cus on cus.docnumber = v.dni " +
                    "where o.order_id = '"+request.getOrder_id()+"'";


            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQueryDAO(logger, "readSVA", "query", stmt.toString());
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    String nombre = rs.getString("client_nombre")+ " " +
                            rs.getString("client_apellido_paterno")+ " " +
                            rs.getString("client_apellido_materno");

                    // Datos del Cliente
                    outResponseDetalle.setClientNombre(nombre);
                    outResponseDetalle.setClientFechaNacimiento(rs.getString("client_nacimiento"));
                    outResponseDetalle.setClientNumeroTelefono(rs.getString("customerphone"));
                    outResponseDetalle.setClientTipoDocumento(rs.getString("client_tipo_doc"));
                    outResponseDetalle.setClientNumeroDoc(rs.getString("client_numero_doc"));

                    // Lugar de Instalacion
                    outResponseDetalle.setClientProducDepa(rs.getString("departamento"));
                    outResponseDetalle.setClientProducProvincia(rs.getString("provincia"));
                    outResponseDetalle.setClientProducDistrito(rs.getString("distrito"));
                    outResponseDetalle.setClientProducDireccion(rs.getString("direccion"));
                    outResponseDetalle.setClientProducReferencia(rs.getString("address_referencia"));

                    // Producto Contratado
                    outResponseDetalle.setClientProducNombre(rs.getString("nombre_producto"));
                    outResponseDetalle.setClientProducTecnologia(rs.getString("product_tec_inter"));
                    outResponseDetalle.setClientProducBeneficios(rs.getString("product_category"));
                    String productInternetVel = rs.getString("product_internet_vel");
                    String productEquipLine = rs.getString("product_equip_line");
                    String productEquipInter = rs.getString("product_equip_inter");
                    String productEquipTV = rs.getString("product_equip_tv");

                    if (productInternetVel!=null && !productInternetVel.isEmpty()){
                        outResponseDetalle.setClientProducIncluye("Velocidad de Internet " + productInternetVel);
                                //((productEquipInter!=null && productEquipInter.isEmpty()) ? productEquipInter : ""));
                    }

                    String equipamiento = "";

                    if (productEquipLine!=null && !productEquipLine.isEmpty()){
                        equipamiento +=  productEquipLine;
                    }

                    if (productEquipInter!=null && !productEquipInter.isEmpty()){
                        equipamiento += " " + productEquipInter;
                    }

                    if (productEquipTV!=null && !productEquipTV.isEmpty()){
                        equipamiento += " " + productEquipTV;
                    }

                    if (productEquipLine!=null )
                    outResponseDetalle.setClientProducEquipamento(equipamiento);

                    outResponseDetalle.setClientProducModo(rs.getString("product_pay_method"));

                    String productPrecioNormal = rs.getString("product_precio_normal");
                    if (productPrecioNormal!=null)
                        outResponseDetalle.setClientProducPrecioRegular("S/ " + rs.getString("product_precio_normal") + " mensual");

                    String productPrecioPromo = rs.getString("product_precio_promo");
                    if (productPrecioPromo!=null)
                    outResponseDetalle.setClientProducPrecioPromocional("S/ " + rs.getString("product_precio_promo") + " x " +
                            rs.getString("product_precio_promo_mes")+ " meses");

                    String svaPrecio1 = rs.getString("sva_precio_1");
                    String svaPrecio2 = rs.getString("sva_precio_2");
                    String svaPrecio3 = rs.getString("sva_precio_3");
                    String svaPrecio4 = rs.getString("sva_precio_4");
                    String svaPrecio5 = rs.getString("sva_precio_5");
                    String svaPrecio6 = rs.getString("sva_precio_6");
                    String svaPrecio7 = rs.getString("sva_precio_7");
                    String svaPrecio8 = rs.getString("sva_precio_8");
                    String svaPrecio9 = rs.getString("sva_precio_9");
                    String svaPrecio10 = rs.getString("sva_precio_10");

                    outResponseDetalle.setClientSvaNombre1(rs.getString("sva_nombre_1"));
                    outResponseDetalle.setClientSvaPrecio1(svaPrecio1);
                    outResponseDetalle.setClientSvaNombre2(rs.getString("sva_nombre_2"));
                    outResponseDetalle.setClientSvaPrecio2(svaPrecio2);
                    outResponseDetalle.setClientSvaNombre3(rs.getString("sva_nombre_3"));
                    outResponseDetalle.setClientSvaPrecio3(svaPrecio3);
                    outResponseDetalle.setClientSvaNombre4(rs.getString("sva_nombre_4"));
                    outResponseDetalle.setClientSvaPrecio4(svaPrecio4);
                    outResponseDetalle.setClientSvaNombre5(rs.getString("sva_nombre_5"));
                    outResponseDetalle.setClientSvaPrecio5(svaPrecio5);
                    outResponseDetalle.setClientSvaNombre6(rs.getString("sva_nombre_6"));
                    outResponseDetalle.setClientSvaPrecio6(svaPrecio6);
                    outResponseDetalle.setClientSvaNombre7(rs.getString("sva_nombre_7"));
                    outResponseDetalle.setClientSvaPrecio7(svaPrecio7);
                    outResponseDetalle.setClientSvaNombre8(rs.getString("sva_nombre_8"));
                    outResponseDetalle.setClientSvaPrecio8(svaPrecio8);
                    outResponseDetalle.setClientSvaNombre9(rs.getString("sva_nombre_9"));
                    outResponseDetalle.setClientSvaPrecio9(svaPrecio9);
                    outResponseDetalle.setClientSvaNombre10(rs.getString("sva_nombre_10"));
                    outResponseDetalle.setClientSvaPrecio10(svaPrecio10);

                    Double sva_precio1 = (svaPrecio1!=null && !svaPrecio1.isEmpty()) ? Double.parseDouble(svaPrecio1) : 0;
                    Double sva_precio2 = (svaPrecio2!=null && !svaPrecio2.isEmpty()) ? Double.parseDouble(svaPrecio2) : 0;
                    Double sva_precio3 = (svaPrecio3!=null && !svaPrecio3.isEmpty()) ? Double.parseDouble(svaPrecio3) : 0;
                    Double sva_precio4 = (svaPrecio4!=null && !svaPrecio4.isEmpty()) ? Double.parseDouble(svaPrecio4) : 0;
                    Double sva_precio5 = (svaPrecio5!=null && !svaPrecio5.isEmpty()) ? Double.parseDouble(svaPrecio5) : 0;
                    Double sva_precio6 = (svaPrecio6!=null && !svaPrecio6.isEmpty()) ? Double.parseDouble(svaPrecio6) : 0;
                    Double sva_precio7 = (svaPrecio7!=null && !svaPrecio7.isEmpty()) ? Double.parseDouble(svaPrecio7) : 0;
                    Double sva_precio8 = (svaPrecio8!=null && !svaPrecio8.isEmpty()) ? Double.parseDouble(svaPrecio8) : 0;
                    Double sva_precio9 = (svaPrecio9!=null && !svaPrecio9.isEmpty()) ? Double.parseDouble(svaPrecio9) : 0;
                    Double sva_precio10 = (svaPrecio10!=null && !svaPrecio10.isEmpty()) ? Double.parseDouble(svaPrecio10) : 0;
                    Double sumaSvas = sva_precio1 + sva_precio2 + sva_precio3 + sva_precio4 + sva_precio5 + sva_precio6 +
                            sva_precio7 + sva_precio8 + sva_precio9 + sva_precio10;

                    if (productPrecioPromo!=null){
                        Double precioPromocionalTotal = Math.round((Double.parseDouble(productPrecioPromo) + sumaSvas) * 100.0) / 100.0;
                        outResponseDetalle.setClientProducPrecioPromocionalTotal(precioPromocionalTotal.toString());
                    }

                    if (productPrecioNormal!=null){
                        Double precioRegularTotal = Math.round((Double.parseDouble(productPrecioNormal) + sumaSvas) * 100.0) / 100.0;
                        outResponseDetalle.setClientProducPrecioRegularTotal(precioRegularTotal.toString());
                    }


                    // Detalle Recibo
                    outResponseDetalle.setClientTratamientoDatos(rs.getString("affiliation_data_protection"));
                    outResponseDetalle.setClientReciboDigital(rs.getString("affiliation_electronic_invoice"));
                    outResponseDetalle.setClientFiltroParental(rs.getString("affiliation_parental_protection"));

                }
            }
        } catch (Exception e) {
            logger.error("Error filterDetalleOut DAO.", e);
        }
        return outResponseDetalle;
    }

    public List<OutOffersResponseData> readOffersOutCapta(OutOffersRequest request) {
        List<OutOffersResponseData> list = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {
            String query = "( select 0, c.*, co.tx_nombre, co.numero_telefono, co.tipo_documento, co.numero_documento, co.departamento as depa," +
                    " co.provincia as provin, co.distrito as distr, co.tx_direccion" +
                    " from ibmx_a07e6d02edaf552.tdp_catalog_capta_out co" +
                    " left join ibmx_a07e6d02edaf552.tdp_catalog c on c.productcode = co.cod_prod_oferta" +
                    " where " +
                    " co.tipo_documento = '" + request.getDocumentType() + "'" +
                    " and co.numero_documento = '" + request.getDocumentNumber() + "'" +
                    " and c.canal in ('Out','Todos')" +
                    " and c.cashprice = '0.00'" +
                    " and c.internettech = 'HFC'" +
                    " and to_number(c.prodcategorycode,'999') < 200" +
                    " limit 1)" +
                    " union " +
                    "( select 1, c.*, co.tx_nombre, co.numero_telefono, co.tipo_documento, co.numero_documento, co.departamento as depa," +
                    " co.provincia as provin, co.distrito as distr, co.tx_direccion" +
                    " from ibmx_a07e6d02edaf552.tdp_catalog_capta_out co" +
                    " left join ibmx_a07e6d02edaf552.tdp_catalog c on c.productcode = co.ps_oferta2" +
                    " where " +
                    " co.tipo_documento = '" + request.getDocumentType() + "'" +
                    " and co.numero_documento = '" + request.getDocumentNumber() + "'" +
                    " and c.canal in ('Out','Todos')" +
                    " and c.cashprice = '0.00'" +
                    " and c.internettech = 'HFC'" +
                    " and to_number(c.prodcategorycode,'999') < 200" +
                    " limit 1)" +
                    " order by 1" ;

            try (PreparedStatement stmt = con.prepareStatement(query)) {

                logger.info("ReadOffers2 - query: " + query);
                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    OutOffersResponseData obj = new OutOffersResponseData();
                    obj.setProductCode(rs.getString("productCode"));
                    obj.setProductName(rs.getString("productName"));
                    obj.setDiscount(rs.getString("discount"));
                    obj.setPrice(Double.parseDouble(rs.getString("price")));
                    obj.setPromPrice(Double.parseDouble(rs.getString("promPrice")));
                    obj.setMonthPeriod(Integer.parseInt(rs.getString("monthPeriod")));
                    obj.setFinancingCost(Double.parseDouble(rs.getString("financingCost")));
                    obj.setFinancingMonth(Integer.parseInt(rs.getString("financingMonth")));
                    obj.setInstallCost(Double.parseDouble(rs.getString("installCost")));
                    obj.setReturnMonth(Integer.parseInt(rs.getString("returnMonth")));
                    obj.setReturnPeriod(rs.getString("returnPeriod"));
                    obj.setEquipmentType(rs.getString("equipmentType"));
                    obj.setLineType(rs.getString("lineType"));
                    obj.setInternetSpeed(rs.getInt("internetSpeed"));
                    obj.setCashPrice(Double.parseDouble(rs.getString("cashPrice")));
                    obj.setProductTypeCode(rs.getString("prodTypeCode"));
                    obj.setProductType(rs.getString("productType"));
                    obj.setCommercialOperation(rs.getString("commercialOperation"));
                    obj.setPaymentMethod(rs.getString("paymentMethod"));
                    obj.setCampaign(rs.getString("campaign"));
                    obj.setInternetTech(rs.getString("internetTech"));
                    obj.setTvSignal(rs.getString("tvSignal"));
                    obj.setTvTech(rs.getString("tvTech"));
                    obj.setProductCategoryCode(rs.getString("prodCategoryCode"));
                    obj.setProductCategory(rs.getString("productCategory"));
                    obj.setId(rs.getInt("id"));

                    /* Sprint 10 */
                    obj.setPromoInternetSpeed(Integer.parseInt((rs.getString("promspeed") == null ? "0" : rs.getString("promspeed"))));
                    obj.setPeriodoPromoInternetSpeed(Integer.parseInt((rs.getString("periodpromspeed") == null ? "0" : rs.getString("periodpromspeed"))));
                    obj.setEquipamientoLinea((rs.getString("equiplinea") == null ? "" : rs.getString("equiplinea").replace("-", "")));
                    obj.setEquipamientoInternet((rs.getString("equipinternet") == null ? "" : rs.getString("equipinternet").replace("-", "")));
                    obj.setEquipamientoTv((rs.getString("equiptv") == null ? "" : rs.getString("equiptv").replace("-", "")));
                    //obj.setCampaign(rs.getString("campania"));

                    obj.setAltaPuraSvasBloqueTV(rs.getString("bloquetv"));
                    obj.setAltaPuraSvasLinea(rs.getString("svalinea"));
                    obj.setAltaPuraSvasInternet(rs.getString("svainternet"));

                    StringBuffer sb = new StringBuffer();
                    if(obj.getAltaPuraSvasBloqueTV()!=null && !obj.getAltaPuraSvasBloqueTV().isEmpty())
                        sb.append(obj.getAltaPuraSvasBloqueTV());

                    if(obj.getAltaPuraSvasLinea()!=null && !obj.getAltaPuraSvasLinea().isEmpty()){
                        if (sb.length()>0) sb.append(",");
                        sb.append(obj.getAltaPuraSvasLinea());
                    }

                    if(obj.getAltaPuraSvasInternet()!=null && !obj.getAltaPuraSvasInternet().isEmpty()){
                        if (sb.length()>0) sb.append(",");
                        sb.append(obj.getAltaPuraSvasInternet());
                    }

                    obj.setAltaPuraSvasNameBloqueTV(readSVANames(sb.toString()));

                    switch (rs.getString("tvTech")) {
                        case "CATV":
                            obj.setCod_ind_sen_cms("C");
                            break;
                        case "DTH":
                            obj.setCod_ind_sen_cms("D");
                            break;
                        default:
                            obj.setCod_ind_sen_cms("");
                            break;
                    }

                    obj.setCod_cab_cms("");
                    // COD_IND_SEN_CMS // CATV -> C, DTH -> D, No aplica -> Vacio
                    // COD_CAB_CMS // CATV -> Cabecera , DTH -> Satelital . PARA CONFIRMAR
                    obj.setDepartamento(rs.getString("depa"));
                    obj.setProvincia(rs.getString("provin"));
                    obj.setDistrito(rs.getString("distr"));
                    obj.setDireccion(rs.getString("tx_direccion"));
                    obj.setCustomerName(rs.getString("tx_nombre"));
                    obj.setNumeroTelefono(rs.getString("numero_telefono"));
                    list.add(obj);
                }
            }
        } catch (Exception e) {
            logger.error("Error ReadOffers2 DAO.", e);
        }
        return list;

    }

    public List<OutOffersResponseData> readOffersOutPlanta(OutOffersPlantaRequest request, List<String> lstSvas) {
        List<OutOffersResponseData> list = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {
            String query ;

            query = "( select 0, c.*, cp.*, cp.departamento as depa, cp.provincia as provin, cp.distrito as distr" +
                    " from ibmx_a07e6d02edaf552.tdp_catalog_planta_out cp " +
                    " left join ibmx_a07e6d02edaf552.tdp_catalog c on c.productcode = cp.ps_of1 " +
                    " where  cp.numero_telefono = '" + request.getPhoneOrCms() + "'" +
                    " and c.canal in ('Out','Todos') " +
                    " and c.cashprice = '0.00' " +
                    " and c.internettech = 'HFC'" +
                    " and c.commercialoperation = (select p.strvalue from ibmx_a07e6d02edaf552.parameters p where p.domain = 'EQUIVALENCEOUT' " +
                    " and p.category = 'OPERACIONCOMERCIAL' and p.element = cp.campania)" +
                    " and to_number(c.prodcategorycode,'999') >= 200 limit 1 )" +
                    " UNION" +
                    " ( select 1, c.*, cp.*, cp.departamento as depa, cp.provincia as provin, cp.distrito as distr" +
                    " from ibmx_a07e6d02edaf552.tdp_catalog_planta_out cp " +
                    " left join ibmx_a07e6d02edaf552.tdp_catalog c on c.productcode = cp.ps_of2" +
                    " where  cp.numero_telefono = '" + request.getPhoneOrCms() + "'" +
                    " and c.canal in ('Out','Todos') " +
                    " and cashprice = '0.00' " +
                    " and internettech = 'HFC'" +
                    " and c.commercialoperation = (select p.strvalue from ibmx_a07e6d02edaf552.parameters p where p.domain = 'EQUIVALENCEOUT' " +
                    " and p.category = 'OPERACIONCOMERCIAL' and p.element = cp.campania)" +
                    " and to_number(c.prodcategorycode,'999') >= 200 limit 1)" +
                    " order by 1";

            if (request.getCommercialOperation().equalsIgnoreCase("S")){
                query = "select c.*, cp.*, cp.departamento as depa, cp.provincia as provin, cp.distrito as distr" +
                        " from ibmx_a07e6d02edaf552.tdp_catalog_planta_out cp " +
                        " left join ibmx_a07e6d02edaf552.tdp_catalog c on cp.ps_of1 = c.productcode" +
                        " where  cp.numero_telefono = '" + request.getPhoneOrCms() + "'" +
                        " and  cp.ps_of1 = '' and cp.ps_of2 = '' limit 1";
            }

            try (PreparedStatement stmt = con.prepareStatement(query)) {

                logger.info("readOffersOutPlanta - query: " + query);
                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    // Por el momento solo DNI y no RUC porque no hay Servicio SUNAT para corroborar un numero RUC valido
                    if(!rs.getString("tipo_documento").equals("DNI")) break;

                    OutOffersResponseData obj = new OutOffersResponseData();
                    obj.setProductCode(rs.getString("productCode"));
                    obj.setProductName(rs.getString("productName"));
                    String hayDiscount = "No", psStr="", psSvasStr = null, saltoStr = "0", rentaStr = "0", rentaPromocionalStr = "0", rentaPromPeriodoStr = "",
                    velocidadPromocionalStr = null, velocidadPromPeriodoStr=null;

                    if (rs.getRow() == 1 ){
                        psStr = rs.getString("ps_of1");
                        psSvasStr = rs.getString("ps_svas_of1");
                        saltoStr = rs.getString("salto_of1");
                        rentaStr = rs.getString("renta_destino_of1");
                        rentaPromocionalStr = rs.getString("renta_promocional_of1");
                        rentaPromPeriodoStr = rs.getString("renta_prom_periodo_of1");
                        if (rs.getString("velocidad_promocional_of1")!=null){
                            if(rs.getString("velocidad_promocional_of1").length()>3){
                                velocidadPromocionalStr = rs.getString("velocidad_promocional_of1").substring(0, rs.getString("velocidad_promocional_of1").length() -3);
                            } else {
                                velocidadPromocionalStr = rs.getString("velocidad_promocional_of1");
                            }
                            velocidadPromPeriodoStr = rs.getString("velocidad_prom_periodo_of1");
                        }

                    } else if (rs.getRow() == 2 ){
                        psStr = rs.getString("ps_of2");
                        psSvasStr = rs.getString("ps_svas_of2");
                        saltoStr = rs.getString("salto_of2");
                        rentaStr = rs.getString("renta_destino_of2");
                        rentaPromocionalStr = rs.getString("renta_promocional_of2");
                        rentaPromPeriodoStr = rs.getString("renta_prom_periodo_of2");
                        if (rs.getString("velocidad_promocional_of2")!=null){
                            if(rs.getString("velocidad_promocional_of2").length()>3){
                                velocidadPromocionalStr = rs.getString("velocidad_promocional_of2").substring(0, rs.getString("velocidad_promocional_of2").length() -3);
                            } else {
                                velocidadPromocionalStr = rs.getString("velocidad_promocional_of2");
                            }
                            velocidadPromPeriodoStr = rs.getString("velocidad_prom_periodo_of2");
                        }
                    }
                    double renta=0, rentaPromocional = 0, salto =0;
                    if(rentaStr != null && !rentaStr.isEmpty()){
                        renta = Double.parseDouble(rentaStr);
                    }
                    if(rentaPromocionalStr != null && !rentaPromocionalStr.isEmpty()){
                        rentaPromocional = Double.parseDouble(rentaPromocionalStr);
                    }
                    if(renta>0 && rentaPromocional>0 & renta>rentaPromocional){
                        hayDiscount = "Si";
                    }
                    obj.setDiscount(hayDiscount);
                    obj.setPrice(renta);
                    obj.setPromPrice(rentaPromocional);
                    obj.setMonthPeriod(Integer.parseInt(rentaPromPeriodoStr));
                    obj.setFinancingCost(0);
                    obj.setFinancingMonth(0);
                    obj.setInstallCost(0);
                    obj.setReturnMonth(0);
                    obj.setReturnPeriod(rs.getString("returnPeriod"));
                    obj.setEquipmentType(rs.getString("equipmentType"));
                    obj.setLineType(rs.getString("lineType"));
                    obj.setInternetSpeed(rs.getInt("internetSpeed"));
                    if(rs.getString("cashPrice")!=null && !rs.getString("cashPrice").isEmpty())
                    obj.setCashPrice(Double.parseDouble(rs.getString("cashPrice")));
                    obj.setProductTypeCode(rs.getString("prodTypeCode"));
                    obj.setProductType(rs.getString("productType"));
                    obj.setCommercialOperation(rs.getString("commercialOperation"));
                    obj.setPaymentMethod(rs.getString("paymentMethod"));
                    obj.setCampaign(rs.getString("campaign"));
                    obj.setInternetTech(rs.getString("internetTech"));
                    obj.setTvSignal(rs.getString("tvSignal"));
                    obj.setTvTech(rs.getString("tvTech"));
                    obj.setProductCategoryCode(rs.getString("prodCategoryCode"));
                    obj.setProductCategory(rs.getString("productCategory"));
                    obj.setId(rs.getInt("id"));

                    /* Sprint 10 */
                    obj.setPromoInternetSpeed(Integer.parseInt((velocidadPromocionalStr == null ? "0" : velocidadPromocionalStr)));
                    obj.setPeriodoPromoInternetSpeed(Integer.parseInt((velocidadPromPeriodoStr == null ? "0" : velocidadPromPeriodoStr)));
                    obj.setEquipamientoLinea((rs.getString("equiplinea") == null ? "" : rs.getString("equiplinea").replace("-", "")));
                    obj.setEquipamientoInternet((rs.getString("equipinternet") == null ? "" : rs.getString("equipinternet").replace("-", "")));
                    obj.setEquipamientoTv((rs.getString("equiptv") == null ? "" : rs.getString("equiptv").replace("-", "")));
                    //obj.setCampaign(rs.getString("campania"));
                    obj.setAltaPuraSvasBloqueTV(rs.getString("bloquetv"));
                    obj.setAltaPuraSvasLinea(rs.getString("svalinea"));
                    obj.setAltaPuraSvasInternet(rs.getString("svainternet"));

                    if(rs.getString("tvTech")!=null){
                    switch (rs.getString("tvTech")) {
                        case "CATV":
                            obj.setCod_ind_sen_cms("C");
                            break;
                        case "DTH":
                            obj.setCod_ind_sen_cms("D");
                            break;
                        default:
                            obj.setCod_ind_sen_cms("");
                            break;
                    }
                    }


                    obj.setCod_cab_cms("");
                    // COD_IND_SEN_CMS // CATV -> C, DTH -> D, No aplica -> Vacio
                    // COD_CAB_CMS // CATV -> Cabecera , DTH -> Satelital . PARA CONFIRMAR
                    obj.setDepartamento(rs.getString("depa"));
                    obj.setProvincia(rs.getString("provin"));
                    obj.setDistrito(rs.getString("distr"));
                    obj.setDireccion(rs.getString("tx_direccion"));
                    obj.setCustomerName(rs.getString("tx_nombre"));
                    obj.setTipoDocumento(rs.getString("tipo_documento"));
                    obj.setNumeroDocumento(rs.getString("numero_documento"));
                    obj.setNumeroTelefono(rs.getString("numero_telefono"));
                    String[] celulares = rs.getString("celular").split("\\|");
                    if (celulares!= null && celulares.length>0){
                        obj.setCelular(celulares[0]);
                        if (celulares.length>1){
                            obj.setCelular2(celulares[1]);
                        }
                    }
                    if(psSvasStr!=null && !psSvasStr.isEmpty()){
                        String[] listaSvasSplitted = psSvasStr.split("\\|");
                        for (int i = 0; i < listaSvasSplitted.length; i++) {
                            lstSvas.add(listaSvasSplitted[i]);
                        }
                        obj.setAltaPuraSvasBloqueTV(psSvasStr.replace("|",","));
                    }
                    if(saltoStr != null && !saltoStr.isEmpty()){
                        salto = Double.parseDouble(saltoStr);
                    }
                    obj.setRentatotal(renta + salto);

                    list.add(obj);
                }
            }
        } catch (Exception e) {
            logger.error("Error readOffersOutPlanta DAO.", e);
        }
        return list;

    }

    public String readSVANames(String listaSvas) {
        StringBuilder sb = new StringBuilder();

        listaSvas = listaSvas.replace("," ,"','");
        try (Connection con = Database.datasource().getConnection()) {

            String query = "SELECT code, description, unit FROM ibmx_a07e6d02edaf552.tdp_sva WHERE productcode in ('"
                    + listaSvas + "')" ;

            try (PreparedStatement stmt = con.prepareStatement(query)) {

                logger.info("readSVANames - query: " + query);
                ResultSet rs = stmt.executeQuery();
                String nameSva = "";
                while (rs.next()) {
                    SvaV2ResponseDataSva obj = new SvaV2ResponseDataSva();
                    String code = rs.getString("code");

                    if (code == null ) continue;



                    if (!code.equalsIgnoreCase("DHD") && !code.equalsIgnoreCase("DSHD")
                            && !code.equalsIgnoreCase("DVR") && !code.equalsIgnoreCase("RSW")){
                        nameSva = rs.getString("unit");
                    } else {
                        nameSva = rs.getString("description");
                    }
                    if(!sb.toString().contains(nameSva)){
                        if (sb.length()>0)
                            sb.append(",");
                        sb.append(nameSva);
                    }


                }
            }

        } catch (Exception e) {
            logger.info("Error Complements V2 DAO." + e);
        }

        return sb.toString();
    }

}