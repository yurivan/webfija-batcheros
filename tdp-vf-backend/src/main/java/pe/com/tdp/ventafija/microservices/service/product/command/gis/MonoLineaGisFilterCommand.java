package pe.com.tdp.ventafija.microservices.service.product.command.gis;

import org.springframework.stereotype.Component;
import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;

import java.util.Map;

@Component("filterMonoLinea")
public class MonoLineaGisFilterCommand implements GisFilterCommand {
    // Filtro para MONO LINEA (1)
    private static String MONO_LINEA_FILTER =       " (OFERTA.targetData = '1') ";

    @Override
    public void execute(WSSIGFFTT_FFTT_AVEResult resultGis, Map<String, String> variables, StringBuilder queryBuilder, StringBuilder columnsBuilder) {
        String hasTecnologiaCobre = resultGis.getCOBRE_TEC();
        String hasTecnologiaCobreSaturado = resultGis.getCOBRE_SAT();
        // Evaluamos productos MONO LINEA (1)
        if ("SI".equals(hasTecnologiaCobre) && "NO".equals(hasTecnologiaCobreSaturado)) {
            queryBuilder.append(MONO_LINEA_FILTER + " OR ");
        }
    }
}
