package pe.com.tdp.ventafija.microservices.domain.customer;

public class ContractWebRequestProduct {

	private String parkType;
	private String productDescription;
	private String status;
	private String sourceType;
	private String coordinateX;
	private String coordianteY;
	private String department;
	private String province;
	private String district;
	private String address;
	private String ubigeoCode;
	private String phone;
	private String psprincipal;
	private String pslinea;
	private String psinternet;
	private String pstv;
	private String subscriber;
	private String serviceCode;
	private String productCode;
	private String additionalAddress;

	public String getParkType() {
		return parkType;
	}

	public void setParkType(String parkType) {
		this.parkType = parkType;
	}

	public String getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getCoordinateX() {
		return coordinateX;
	}

	public void setCoordinateX(String coordinateX) {
		this.coordinateX = coordinateX;
	}

	public String getCoordianteY() {
		return coordianteY;
	}

	public void setCoordianteY(String coordianteY) {
		this.coordianteY = coordianteY;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUbigeoCode() {
		return ubigeoCode;
	}

	public void setUbigeoCode(String ubigeoCode) {
		this.ubigeoCode = ubigeoCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPsprincipal() {
		return psprincipal;
	}

	public void setPsprincipal(String psprincipal) {
		this.psprincipal = psprincipal;
	}

	public String getPslinea() {
		return pslinea;
	}

	public void setPslinea(String pslinea) {
		this.pslinea = pslinea;
	}

	public String getPsinternet() {
		return psinternet;
	}

	public void setPsinternet(String psinternet) {
		this.psinternet = psinternet;
	}

	public String getPstv() {
		return pstv;
	}

	public void setPstv(String pstv) {
		this.pstv = pstv;
	}

	public String getAdditionalAddress() { return additionalAddress; }

	public void setAdditionalAddress(String additionalAddress) { this.additionalAddress = additionalAddress; }
}
