package pe.com.tdp.ventafija.microservices.domain.automatizador;

import jdk.nashorn.internal.parser.JSONParser;
import org.json.JSONArray;
import org.json.JSONObject;

public class DetalleEstadoSolicitud {

    public String COD_ERR_CD;
    public String DES_ERR_DS;
    public String FEC_HOR_REC_WA;
    public String COD_EST_VTA_APP;
    public String DES_EST_VTA_APP;
    public String COD_PED_ATI_CD;
    public String COD_REQ_PED_CD;
    public String COD_EST_PED_CD;
    public String DES_EST_PED_CD;
    public String FEC_HOR_REG;
    public String FEC_HOR_EMI_PED;
    public String COD_CLI_CD;
    public String COD_CTA_CD;
    public String COD_INS_LEG_CD;
    public String NUM_IDE_TLF;
    public String COD_CLI_CMS;
    public String COD_CTA_CMS;
    public String DES_OBS_VTA_APP;
    public String COD_ID_WEB_EX1;
    public String DES_ID_WEB_EX1;
    public String COD_EST_WEB_EX1;
    public String DES_EST_WEB_EX1;
    public String COD_ID_WEB_EX2;
    public String DES_ID_WEB_EX2;
    public String COD_EST_WEB_EX2;
    public String DES_EST_WEB_EX2;
    public String COD_ID_WEB_EX3;
    public String DES_ID_WEB_EX3;
    public String COD_EST_WEB_EX3;
    public String DES_EST_WEB_EX3;
    public String COD_ID_WEB_EX4;
    public String DES_ID_WEB_EX4;
    public String COD_EST_WEB_EX4;
    public String DES_EST_WEB_EX4;
    public String COD_ID_WEB_EX5;
    public String DES_ID_WEB_EX5;
    public String COD_EST_WEB_EX5;
    public String DES_EST_WEB_EX5;

    public String getCOD_ERR_CD() {
        return COD_ERR_CD;
    }

    public void setCOD_ERR_CD(String COD_ERR_CD) {
        this.COD_ERR_CD = COD_ERR_CD;
    }

    public String getDES_ERR_DS() {
        return DES_ERR_DS;
    }

    public void setDES_ERR_DS(String DES_ERR_DS) {
        this.DES_ERR_DS = DES_ERR_DS;
    }

    public String getFEC_HOR_REC_WA() {
        return FEC_HOR_REC_WA;
    }

    public void setFEC_HOR_REC_WA(String FEC_HOR_REC_WA) {
        this.FEC_HOR_REC_WA = FEC_HOR_REC_WA;
    }

    public String getCOD_EST_VTA_APP() {
        return COD_EST_VTA_APP;
    }

    public void setCOD_EST_VTA_APP(String COD_EST_VTA_APP) {
        this.COD_EST_VTA_APP = COD_EST_VTA_APP;
    }

    public String getDES_EST_VTA_APP() {
        return DES_EST_VTA_APP;
    }

    public void setDES_EST_VTA_APP(String DES_EST_VTA_APP) {
        this.DES_EST_VTA_APP = DES_EST_VTA_APP;
    }

    public String getCOD_PED_ATI_CD() {
        return COD_PED_ATI_CD;
    }

    public void setCOD_PED_ATI_CD(String COD_PED_ATI_CD) {
        this.COD_PED_ATI_CD = COD_PED_ATI_CD;
    }

    public String getCOD_REQ_PED_CD() {
        return COD_REQ_PED_CD;
    }

    public void setCOD_REQ_PED_CD(String COD_REQ_PED_CD) {
        this.COD_REQ_PED_CD = COD_REQ_PED_CD;
    }

    public String getCOD_EST_PED_CD() {
        return COD_EST_PED_CD;
    }

    public void setCOD_EST_PED_CD(String COD_EST_PED_CD) {
        this.COD_EST_PED_CD = COD_EST_PED_CD;
    }

    public String getDES_EST_PED_CD() {
        return DES_EST_PED_CD;
    }

    public void setDES_EST_PED_CD(String DES_EST_PED_CD) {
        this.DES_EST_PED_CD = DES_EST_PED_CD;
    }

    public String getFEC_HOR_REG() {
        return FEC_HOR_REG;
    }

    public void setFEC_HOR_REG(String FEC_HOR_REG) {
        this.FEC_HOR_REG = FEC_HOR_REG;
    }

    public String getFEC_HOR_EMI_PED() {
        return FEC_HOR_EMI_PED;
    }

    public void setFEC_HOR_EMI_PED(String FEC_HOR_EMI_PED) {
        this.FEC_HOR_EMI_PED = FEC_HOR_EMI_PED;
    }

    public String getCOD_CLI_CD() {
        return COD_CLI_CD;
    }

    public void setCOD_CLI_CD(String COD_CLI_CD) {
        this.COD_CLI_CD = COD_CLI_CD;
    }

    public String getCOD_CTA_CD() {
        return COD_CTA_CD;
    }

    public void setCOD_CTA_CD(String COD_CTA_CD) {
        this.COD_CTA_CD = COD_CTA_CD;
    }

    public String getCOD_INS_LEG_CD() {
        return COD_INS_LEG_CD;
    }

    public void setCOD_INS_LEG_CD(String COD_INS_LEG_CD) {
        this.COD_INS_LEG_CD = COD_INS_LEG_CD;
    }

    public String getNUM_IDE_TLF() {
        return NUM_IDE_TLF;
    }

    public void setNUM_IDE_TLF(String NUM_IDE_TLF) {
        this.NUM_IDE_TLF = NUM_IDE_TLF;
    }

    public String getCOD_CLI_CMS() {
        return COD_CLI_CMS;
    }

    public void setCOD_CLI_CMS(String COD_CLI_CMS) {
        this.COD_CLI_CMS = COD_CLI_CMS;
    }

    public String getCOD_CTA_CMS() {
        return COD_CTA_CMS;
    }

    public void setCOD_CTA_CMS(String COD_CTA_CMS) {
        this.COD_CTA_CMS = COD_CTA_CMS;
    }

    public String getDES_OBS_VTA_APP() {
        return DES_OBS_VTA_APP;
    }

    public void setDES_OBS_VTA_APP(String DES_OBS_VTA_APP) {
        this.DES_OBS_VTA_APP = DES_OBS_VTA_APP;
    }

    public String getCOD_ID_WEB_EX1() {
        return COD_ID_WEB_EX1;
    }

    public void setCOD_ID_WEB_EX1(String COD_ID_WEB_EX1) {
        this.COD_ID_WEB_EX1 = COD_ID_WEB_EX1;
    }

    public String getDES_ID_WEB_EX1() {
        return DES_ID_WEB_EX1;
    }

    public void setDES_ID_WEB_EX1(String DES_ID_WEB_EX1) {
        this.DES_ID_WEB_EX1 = DES_ID_WEB_EX1;
    }

    public String getCOD_EST_WEB_EX1() {
        return COD_EST_WEB_EX1;
    }

    public void setCOD_EST_WEB_EX1(String COD_EST_WEB_EX1) {
        this.COD_EST_WEB_EX1 = COD_EST_WEB_EX1;
    }

    public String getDES_EST_WEB_EX1() {
        return DES_EST_WEB_EX1;
    }

    public void setDES_EST_WEB_EX1(String DES_EST_WEB_EX1) {
        this.DES_EST_WEB_EX1 = DES_EST_WEB_EX1;
    }

    public String getCOD_ID_WEB_EX2() {
        return COD_ID_WEB_EX2;
    }

    public void setCOD_ID_WEB_EX2(String COD_ID_WEB_EX2) {
        this.COD_ID_WEB_EX2 = COD_ID_WEB_EX2;
    }

    public String getDES_ID_WEB_EX2() {
        return DES_ID_WEB_EX2;
    }

    public void setDES_ID_WEB_EX2(String DES_ID_WEB_EX2) {
        this.DES_ID_WEB_EX2 = DES_ID_WEB_EX2;
    }

    public String getCOD_EST_WEB_EX2() {
        return COD_EST_WEB_EX2;
    }

    public void setCOD_EST_WEB_EX2(String COD_EST_WEB_EX2) {
        this.COD_EST_WEB_EX2 = COD_EST_WEB_EX2;
    }

    public String getDES_EST_WEB_EX2() {
        return DES_EST_WEB_EX2;
    }

    public void setDES_EST_WEB_EX2(String DES_EST_WEB_EX2) {
        this.DES_EST_WEB_EX2 = DES_EST_WEB_EX2;
    }

    public String getCOD_ID_WEB_EX3() {
        return COD_ID_WEB_EX3;
    }

    public void setCOD_ID_WEB_EX3(String COD_ID_WEB_EX3) {
        this.COD_ID_WEB_EX3 = COD_ID_WEB_EX3;
    }

    public String getDES_ID_WEB_EX3() {
        return DES_ID_WEB_EX3;
    }

    public void setDES_ID_WEB_EX3(String DES_ID_WEB_EX3) {
        this.DES_ID_WEB_EX3 = DES_ID_WEB_EX3;
    }

    public String getCOD_EST_WEB_EX3() {
        return COD_EST_WEB_EX3;
    }

    public void setCOD_EST_WEB_EX3(String COD_EST_WEB_EX3) {
        this.COD_EST_WEB_EX3 = COD_EST_WEB_EX3;
    }

    public String getDES_EST_WEB_EX3() {
        return DES_EST_WEB_EX3;
    }

    public void setDES_EST_WEB_EX3(String DES_EST_WEB_EX3) {
        this.DES_EST_WEB_EX3 = DES_EST_WEB_EX3;
    }

    public String getCOD_ID_WEB_EX4() {
        return COD_ID_WEB_EX4;
    }

    public void setCOD_ID_WEB_EX4(String COD_ID_WEB_EX4) {
        this.COD_ID_WEB_EX4 = COD_ID_WEB_EX4;
    }

    public String getDES_ID_WEB_EX4() {
        return DES_ID_WEB_EX4;
    }

    public void setDES_ID_WEB_EX4(String DES_ID_WEB_EX4) {
        this.DES_ID_WEB_EX4 = DES_ID_WEB_EX4;
    }

    public String getCOD_EST_WEB_EX4() {
        return COD_EST_WEB_EX4;
    }

    public void setCOD_EST_WEB_EX4(String COD_EST_WEB_EX4) {
        this.COD_EST_WEB_EX4 = COD_EST_WEB_EX4;
    }

    public String getDES_EST_WEB_EX4() {
        return DES_EST_WEB_EX4;
    }

    public void setDES_EST_WEB_EX4(String DES_EST_WEB_EX4) {
        this.DES_EST_WEB_EX4 = DES_EST_WEB_EX4;
    }

    public String getCOD_ID_WEB_EX5() {
        return COD_ID_WEB_EX5;
    }

    public void setCOD_ID_WEB_EX5(String COD_ID_WEB_EX5) {
        this.COD_ID_WEB_EX5 = COD_ID_WEB_EX5;
    }

    public String getDES_ID_WEB_EX5() {
        return DES_ID_WEB_EX5;
    }

    public void setDES_ID_WEB_EX5(String DES_ID_WEB_EX5) {
        this.DES_ID_WEB_EX5 = DES_ID_WEB_EX5;
    }

    public String getCOD_EST_WEB_EX5() {
        return COD_EST_WEB_EX5;
    }

    public void setCOD_EST_WEB_EX5(String COD_EST_WEB_EX5) {
        this.COD_EST_WEB_EX5 = COD_EST_WEB_EX5;
    }

    public String getDES_EST_WEB_EX5() {
        return DES_EST_WEB_EX5;
    }

    public void setDES_EST_WEB_EX5(String DES_EST_WEB_EX5) {
        this.DES_EST_WEB_EX5 = DES_EST_WEB_EX5;
    }

    public DetalleEstadoSolicitud CargarDetalle(String autoConsultaObj) {
        JSONArray jsonArr = new JSONArray("["+autoConsultaObj+"]");
        for (int i = 0; i < jsonArr.length(); i++) {
            JSONObject jsonObj = jsonArr.getJSONObject(i);
            System.out.println(jsonObj);
        }
        JSONObject map = jsonArr.getJSONObject(0);
        JSONArray jsonArr1 = new JSONArray("["+map+"]");
        JSONObject objeto = jsonArr1.getJSONObject(0);
        this.setCOD_ERR_CD(objeto.getString("COD_ERR_CD"));
        this.setDES_ERR_DS(objeto.getString("DES_ERR_DS"));
        this.setFEC_HOR_REC_WA(objeto.getString("FEC_HOR_REC_WA"));
        this.setCOD_EST_VTA_APP(objeto.getString("COD_EST_VTA_APP"));
        this.setDES_EST_VTA_APP(objeto.getString("DES_EST_VTA_APP"));
        this.setCOD_PED_ATI_CD(objeto.getString("COD_PED_ATI_CD"));
        this.setCOD_REQ_PED_CD(objeto.getString("COD_REQ_PED_CD"));
        this.setCOD_EST_PED_CD(objeto.getString("COD_EST_PED_CD"));
        this.setDES_EST_PED_CD(objeto.getString("DES_EST_PED_CD"));
        this.setFEC_HOR_REG(objeto.getString("FEC_HOR_REG"));
        this.setFEC_HOR_EMI_PED(objeto.getString("FEC_HOR_EMI_PED"));
        this.setCOD_CLI_CD(objeto.getString("COD_CLI_CD"));
        this.setCOD_CTA_CD(objeto.getString("COD_CTA_CD"));
        this.setCOD_INS_LEG_CD(objeto.getString("COD_INS_LEG_CD"));
        this.setNUM_IDE_TLF(objeto.getString("NUM_IDE_TLF"));
        this.setCOD_CLI_CMS(objeto.getString("COD_CLI_CMS"));
        this.setCOD_CTA_CMS(objeto.getString("COD_CTA_CMS"));
        this.setDES_OBS_VTA_APP(objeto.getString("DES_OBS_VTA_APP"));
        this.setCOD_ID_WEB_EX1(objeto.getString("COD_ID_WEB_EX1"));
        this.setDES_ID_WEB_EX1(objeto.getString("DES_ID_WEB_EX1"));
        this.setCOD_EST_WEB_EX1(objeto.getString("COD_EST_WEB_EX1"));
        this.setDES_EST_WEB_EX1(objeto.getString("DES_EST_WEB_EX1"));
        this.setCOD_ID_WEB_EX2(objeto.getString("COD_ID_WEB_EX2"));
        this.setDES_ID_WEB_EX2(objeto.getString("DES_ID_WEB_EX2"));
        this.setCOD_EST_WEB_EX2(objeto.getString("COD_EST_WEB_EX2"));
        this.setDES_EST_WEB_EX2(objeto.getString("DES_EST_WEB_EX2"));
        this.setCOD_ID_WEB_EX3(objeto.getString("COD_ID_WEB_EX3"));
        this.setDES_ID_WEB_EX3(objeto.getString("DES_ID_WEB_EX3"));
        this.setCOD_EST_WEB_EX3(objeto.getString("COD_EST_WEB_EX3"));
        this.setDES_EST_WEB_EX3(objeto.getString("DES_EST_WEB_EX3"));
        this.setCOD_ID_WEB_EX4(objeto.getString("COD_ID_WEB_EX4"));
        this.setDES_ID_WEB_EX4(objeto.getString("DES_ID_WEB_EX4"));
        this.setCOD_EST_WEB_EX4(objeto.getString("COD_EST_WEB_EX4"));
        this.setDES_EST_WEB_EX4(objeto.getString("DES_EST_WEB_EX4"));
        this.setCOD_ID_WEB_EX5(objeto.getString("COD_ID_WEB_EX5"));
        this.setDES_ID_WEB_EX5(objeto.getString("DES_ID_WEB_EX5"));
        this.setCOD_EST_WEB_EX5(objeto.getString("COD_EST_WEB_EX5"));
        this.setDES_EST_WEB_EX5(objeto.getString("DES_EST_WEB_EX5"));
        return this;
    }
}
