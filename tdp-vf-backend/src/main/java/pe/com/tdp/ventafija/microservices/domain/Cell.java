package pe.com.tdp.ventafija.microservices.domain;

public class Cell {

    private String row;
    private String column;
    private String value;

    public String getRow() {
        return row;
    }
    public void setRow(String row) {
        this.row = row;
    }
    public String getColumn() {
        return column;
    }
    public void setColumn(String column) {
        this.column = column;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
}
