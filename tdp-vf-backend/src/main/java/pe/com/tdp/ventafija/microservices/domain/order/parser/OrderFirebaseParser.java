package pe.com.tdp.ventafija.microservices.domain.order.parser;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Order;
import pe.com.tdp.ventafija.microservices.common.domain.entity.User;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;

import java.math.BigDecimal;
import java.util.Date;

public class OrderFirebaseParser implements FirebaseParser<Order> {
    private static final String SERVICE_TYPE_ATIS = "ATIS";
    private static final String SERVICE_TYPE_CMS = "CMS";

    DateTimeZone dtZone = DateTimeZone.forID("America/Lima");

    @Override
    public Order parse(SaleApiFirebaseResponseBody objFirebase) {
        Order order = new Order();
        order.setPrice(objFirebase.getProduct_price());
        order.setCashPrice(objFirebase.getProduct_cash_price());
        order.setPromPrice(objFirebase.getProduct_prom_price());
        DateTime dt = new DateTime(new Date());
        DateTime dtus = dt.withZone(dtZone);
        order.setRegistrationDate(dtus.toDate());

        if (objFirebase.getLatitude_installation() != null && objFirebase.getLongitude_installation() != null) {
            order.setCoordinateX(new BigDecimal(objFirebase.getLongitude_installation()));
            order.setCoordinateY(new BigDecimal(objFirebase.getLatitude_installation()));
        }
        order.setPaymentMode(objFirebase.getProduct_payment_method());
        order.setLineType(objFirebase.getProduct_line_type());
        order.setMigrationPhone(objFirebase.getAtis_migration_phone());

        order.setFinancingCost(objFirebase.getProduct_financing_cost());
        order.setFinancingMonth(objFirebase.getProduct_financing_month());

        order.setUser(new User());
        //order.getUser().setId(Integer.parseInt(objFirebase.getVendor_id())); //sprint 3
        order.getUser().setId(objFirebase.getVendor_id()); //sprint 3

        if (objFirebase.getType() != null) {
            order.setServiceType(SERVICE_TYPE_ATIS);

            if (!objFirebase.getType().equalsIgnoreCase("A") && (order.getMigrationPhone() == null || order.getMigrationPhone().trim().length() == 0)) {
                order.setServiceType(SERVICE_TYPE_CMS);

                if (objFirebase.getCms_service_code() != null)
                    order.setCmsServiceCode(objFirebase.getCms_service_code());
                if (objFirebase.getCms_customer() != null)
                    order.setCmsCustomer(objFirebase.getCms_customer());

            } else if (objFirebase.getType().equalsIgnoreCase("A") && objFirebase.getProduct_type() != null && objFirebase.getProduct_type().equalsIgnoreCase("Mono TV")) {
                order.setServiceType(SERVICE_TYPE_CMS);
            }
        }

        order.setCampaign(objFirebase.getProduct_campaign());

        order.setFlagFtth((objFirebase!=null)?objFirebase.isFlagFtth():false);

        return order;
    }

}
