package pe.com.tdp.ventafija.microservices.domain.customer;

public class DuplicateResponseSales {
  private String saleId;
  private String saleStatus;
  private String productCode;
  private String saleRegistrationDate;
  private String commercialOperation;

  public String getSaleId() {
    return saleId;
  }

  public void setSaleId(String saleId) {
    this.saleId = saleId;
  }

  public String getSaleStatus() {
    return saleStatus;
  }

  public void setSaleStatus(String saleStatus) {
    this.saleStatus = saleStatus;
  }

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public String getSaleRegistrationDate() {
    return saleRegistrationDate;
  }

  public void setSaleRegistrationDate(String saleRegistrationDate) {
    this.saleRegistrationDate = saleRegistrationDate;
  }

  public String getCommercialOperation() {
    return commercialOperation;
  }

  public void setCommercialOperation(String commercialOperation) {
    this.commercialOperation = commercialOperation;
  }

}
