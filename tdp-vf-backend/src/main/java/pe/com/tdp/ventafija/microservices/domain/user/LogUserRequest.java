package pe.com.tdp.ventafija.microservices.domain.user;

import java.sql.Timestamp;
import java.util.Date;

public class LogUserRequest {

	private String atis;
	private String source;
	private String fecha;

	public String getAtis() {
		return atis;
	}

	public void setAtis(String atis) {
		this.atis = atis;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
}
