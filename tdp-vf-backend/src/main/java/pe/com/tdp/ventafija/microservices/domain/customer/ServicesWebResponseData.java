package pe.com.tdp.ventafija.microservices.domain.customer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ServicesWebResponseData{
	@JsonInclude(Include.NON_NULL)
	private String parkType;
	@JsonInclude(Include.NON_NULL)
	private String subscriber;
	@JsonInclude(Include.NON_NULL)
	private String serviceCode;
	@JsonInclude(Include.NON_NULL)
	private String productCode;
	@JsonInclude(Include.NON_NULL)
	private String productDescription;
	@JsonInclude(Include.NON_NULL)
	private String status;
	@JsonInclude(Include.NON_NULL)
	private String sourceType;
	@JsonInclude(Include.NON_NULL)
	private String coordinateX;
	@JsonInclude(Include.NON_NULL)
	private String coordinateY;
	@JsonInclude(Include.NON_NULL)
	private String department;
	@JsonInclude(Include.NON_NULL)
	private String province;
	@JsonInclude(Include.NON_NULL)
	private String district;
	@JsonInclude(Include.NON_NULL)
	private String address;
	@JsonInclude(Include.NON_NULL)
	private String ubigeoCode;
	@JsonInclude(Include.NON_NULL)
	private String phone;
	@JsonInclude(Include.NON_NULL)
	private String psprincipal;
	@JsonInclude(Include.NON_NULL)
	private String pslinea;
	@JsonInclude(Include.NON_NULL)
	private String psinternet;
	@JsonInclude(Include.NON_NULL)
	private String pstv;
	
	// CALCULATOR INFO
	@JsonInclude(Include.NON_NULL)
	private String codATis;
	@JsonInclude(Include.NON_NULL)
	private String codCms;
//	@JsonInclude(Include.NON_NULL)
//	private String nombrePaquete;
	@JsonInclude(Include.NON_NULL)
	private String numDecos;
	@JsonInclude(Include.NON_NULL)
	private String rentaTotal;
	@JsonInclude(Include.NON_NULL)
	private String velInter;

	public String getParkType() {
		return parkType;
	}

	public void setParkType(String parkType) {
		this.parkType = parkType;
	}

	public String getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(String subscriber) {
		this.subscriber = subscriber;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSourceType() {
		return sourceType;
	}

	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}

	public String getCoordinateX() {
		return coordinateX;
	}

	public void setCoordinateX(String coordinateX) {
		this.coordinateX = coordinateX;
	}

	public String getCoordinateY() {
		return coordinateY;
	}

	public void setCoordinateY(String coordinateY) {
		this.coordinateY = coordinateY;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUbigeoCode() {
		return ubigeoCode;
	}

	public void setUbigeoCode(String ubigeoCode) {
		this.ubigeoCode = ubigeoCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPsprincipal() {
		return psprincipal;
	}

	public void setPsprincipal(String psprincipal) {
		this.psprincipal = psprincipal;
	}

	public String getPslinea() {
		return pslinea;
	}

	public void setPslinea(String pslinea) {
		this.pslinea = pslinea;
	}

	public String getPsinternet() {
		return psinternet;
	}

	public void setPsinternet(String psinternet) {
		this.psinternet = psinternet;
	}

	public String getPstv() {
		return pstv;
	}

	public void setPstv(String pstv) {
		this.pstv = pstv;
	}
	
	// CALCULATOR INFO
	
	public String getCodATis() {
		return codATis;
	}

	public void setCodATis(String codATis) {
		this.codATis = codATis;
	}

	public String getCodCms() {
		return codCms;
	}

	public void setCodCms(String codCms) {
		this.codCms = codCms;
	}

	public String getNumDecos() {
		return numDecos;
	}

	public void setNumDecos(String numDecos) {
		this.numDecos = numDecos;
	}

	public String getRentaTotal() {
		return rentaTotal;
	}

	public void setRentaTotal(String rentaTotal) {
		this.rentaTotal = rentaTotal;
	}

	public String getVelInter() {
		return velInter;
	}

	public void setVelInter(String velInter) {
		this.velInter = velInter;
	}

}
