package pe.com.tdp.ventafija.microservices.domain.gis;

public class GisContingencia {


    private String id_transaccion;
    private int flag_offline;

    public GisContingencia() {
    }

    public String getId_transaccion() {
        return id_transaccion;
    }

    public void setId_transaccion(String id_transaccion) {
        this.id_transaccion = id_transaccion;
    }

    public int getFlag_offline() {
        return flag_offline;
    }

    public void setFlag_offline(int flag_offline) {
        this.flag_offline = flag_offline;
    }
}
