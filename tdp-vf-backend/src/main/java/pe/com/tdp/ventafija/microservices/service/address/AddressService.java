package pe.com.tdp.ventafija.microservices.service.address;

import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.address.*;
import pe.com.tdp.ventafija.microservices.common.clients.address2.*;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.domain.message.MessageEntities;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.AddressConstants;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.MessageUtil;
import pe.com.tdp.ventafija.microservices.controller.address.Validator;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.address.*;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpOrder;
import pe.com.tdp.ventafija.microservices.domain.repository.TdpOrderRepository;
import pe.com.tdp.ventafija.microservices.repository.address.AddressDao;
import pe.com.tdp.ventafija.microservices.repository.ftth.FtthDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class AddressService {

    @Autowired
    private MessageUtil messageUtil;

    //Conexion
    @Value("${tdp.api.geocodificardireccion.consultagd.uri}")
    private String uriConsultaGD;
    @Value("${tdp.api.geocodificardireccion.consultagd.apiid}")
    private String apiidConsultaGD;
    @Value("${tdp.api.geocodificardireccion.consultagd.apisecret}")
    private String apisecretConsultaGD;

    //Sprint 2 - 2019
    @Value("${tdp.api.geocodificardireccion.consultagdv2.uri}")
    private String uriConsultaGDV2;
    @Value("${tdp.api.geocodificardireccion.consultagdv2.apiid}")
    private String apiidConsultaGDV2;
    @Value("${tdp.api.geocodificardireccion.consultagdv2.apisecret}")
    private String apisecretConsultaGDV2;

    //Datos Constantes Request (Guardar en bd)
    @Value("${tdp.api.geocodificardireccion.consultagd.apiuser}")
    private String apiuserConsultaGD;// = "aregalado@telefonica.com.pe";
    @Value("${tdp.api.geocodificardireccion.consultagd.apipass}")
    private String apipasswordConsultaGD;// = "20180206";
    @Value("${tdp.api.geocodificardireccion.consultagd.apiidpaq}")
    private String apiidpaqueteConsultaGD;// = 1102;
    @Value("${tdp.api.geocodificardireccion.consultagd.apicod}")
    private String apicodigounicoConsultaGD;// = "00001";
    @Value("${tdp.api.geocodificardireccion.consultagd.apiuref}")
    private String apiusarreferenciaConsultaGD;// = false;

    @Value("${tdp.api.geocodificardireccion.getnivelconfianza}")
    private String getNivelConfianza;// = 30;

    @Autowired
    private ServiceCallEventsService serviceCallEventsService;
    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger();
    @Autowired
    private AddressDao addressDao;


    @Autowired
    private TdpOrderRepository tdpOrderRepository;
    @Autowired
    private Validator validar;

    public Response<List<NormalizerResponse>> getAddresses(NormalizerRequest request) {
        logger.info("Inicio GetAddresses Service");
        Response<List<NormalizerResponse>> response = new Response<>();
        ApiResponse<BodyOut> addresses;

        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("NORMALIZADOR");
        LogVass.serviceResponseHashMap(logger, "GetAddresses", "messageList", messageList);
        try {
            response.setResponseCode("0");
            BodyIn bodyIn = new BodyIn();
            //Datos entrantes
            bodyIn.setUserName(apiuserConsultaGD);
            bodyIn.setPassword(apipasswordConsultaGD);
            bodyIn.setIdPaquete(Integer.parseInt(apiidpaqueteConsultaGD));
            bodyIn.setCodigoUnico(apicodigounicoConsultaGD);
            bodyIn.setUbigeo(request.getUbigeo());
            bodyIn.setDireccion(request.getDireccion());
            bodyIn.setUsarReferencia(Boolean.parseBoolean(apiusarreferenciaConsultaGD));

            //Consumir el servicio GeocodificarDireccion
            addresses = getGeocodificarDireccion(bodyIn, request.getId());

            //Sprint 26 - Contingencia normalizador
            boolean contingencia = false;
            if (!contingencia) {
                if (addresses.getBodyOut().getGeocodificarDireccionResponse() == null) {
                    response.setResponseCode(messageList.get("error500").getCode());
                    response.setResponseMessage(messageList.get("error500").getMessage());
                } else {
                    //getNivelConfianza = addressDao.getNumberNivelConfianza();
                    //Uso de la Equivalencia
                    if (!addresses.getBodyOut().getGeocodificarDireccionResponse().getGeocodificarDireccionResult().getNumeroRespuestas().equals("0")) {
                        response = getXYAddress(addresses,
                                Integer.parseInt(addresses.getBodyOut().getGeocodificarDireccionResponse().getGeocodificarDireccionResult().getNumeroRespuestas()),
                                request.getUbigeo(),
                                getNivelConfianza,
                                messageList.get("msmNivelConfianzaUp").getCode(),
                                messageList.get("msmNivelConfianzaUp").getMessage(),
                                Integer.parseInt(addresses.getBodyOut().getGeocodificarDireccionResponse().getGeocodificarDireccionResult().getNumeroRespuestas()),
                                request.getDistrito(), request.getProvincia(),
                                messageList.get("msmAddressNotFound").getCode(),
                                messageList.get("msmAddressNotFound").getMessage());
                    } else {
                        response.setResponseCode(messageList.get("direccionNoEncontrada").getCode());
                        response.setResponseMessage(messageList.get("direccionNoEncontrada").getMessage());
                    }
                }
            } else {
                response.setResponseCode(messageList.get("error500").getCode());
                response.setResponseMessage(messageList.get("error500").getMessage());
            }
        } catch (Exception e) {
            //response.setResponseCode(messageList.get("errorGeneric").getCode());
            response.setResponseCode(messageList.get("error500").getCode());
            response.setResponseMessage(messageList.get("error500").getMessage());
            logger.error("Error getAddresses Service", e);
        }
        logger.info("Fin GetAddresses Service");
        return response;
    }

    public Response<List<NormalizerResponse>> getAddressesV2(NormalizerRequest request){
        BodyOutGeoV2 addreses2;
        Response<List<NormalizerResponse>> response = new Response<>();
        BodyInGeoV2 bodyIn2 = new BodyInGeoV2();

        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("NORMALIZADOR");

        bodyIn2.setQuantity_max_resp("0");
        bodyIn2.setCod_unique("9999");
        bodyIn2.setLatitud(request.getLatitud());
        bodyIn2.setLongitud(request.getLongitud());

        try{
            response.setResponseCode("0");
            addreses2 = getGeocodificarDireccionV2(bodyIn2,request.getId());

            if(!addreses2.getNumResponses().equalsIgnoreCase("0")){
                response = getNomAddress(addreses2,
                        messageList.get("msmNivelConfianzaUp").getCode(),
                        messageList.get("msmNivelConfianzaUp").getMessage(),
                        request.getDistrito(), request.getProvincia(),
                        messageList.get("msmAddressNotFound").getCode(),
                        messageList.get("msmAddressNotFound").getMessage(),
                        request.getLongitud(),
                        request.getLatitud());
            }
            else{
                response.setResponseCode(messageList.get("direccionNoEncontrada").getCode());
                response.setResponseMessage(messageList.get("direccionNoEncontrada").getMessage());
            }

        }catch (Exception e){
            response.setResponseCode(messageList.get("error500").getCode());
            response.setResponseMessage(messageList.get("error500").getMessage());
            logger.error("Error getAddresses Service", e);
        }

        return response;
    }

    @Transactional
    public Response<ConfigAddressResponse> getAddressConfig(ConfigAddressRequest request) {
        Response<ConfigAddressResponse> response = new Response<>();
        ConfigAddressResponse data = new ConfigAddressResponse();
        data = validar.validarAddressConfig(request);
        String mjs = data.getMensaje();
        try {
            TdpOrder order = tdpOrderRepository.findOneByOrderId(request.getOrderId());
            order = order.updateAddress(request);
            tdpOrderRepository.save(order);
            data.setCodConsulta("0");
            data.setMensaje(mjs + "Cambios realizados satisfactoriamente");
        } catch (Exception e) {
            logger.error("Error ConfigAddressResponse: " + e);
            response.setResponseCode("1");
            data.setCodConsulta("1");
            data.setMensaje("Los cambios no se realizaron");
            response.setResponseMessage("Los cambios no se realizaron");
        }
        response.setResponseData(data);

        return response;
    }

    private Response<List<NormalizerResponse>> getXYAddress(ApiResponse<BodyOut> addresses, int countAddress,
                                                            String ubigeo,
                                                            String getNivelConfianza,
                                                            String getCodeMsmConfianza,
                                                            String getMessageConfianza,
                                                            int numeroRespuesta,
                                                            String distrito, String provincia,
                                                            String codeAddressOutUbigeo,
                                                            String messageAddressOutUbigeo) {

        LogVass.startController(logger, "GetNormalizer", addresses);

        Response<List<NormalizerResponse>> response = new Response<>();
        Addresses address = null;
        List<Addresses> dataAddress = new ArrayList<>();
        NormalizerResponse normalizerResponse = new NormalizerResponse();
        List<NormalizerResponse> normalizerResponseList = new ArrayList<>();
        List<PuntoGeocodificado> data = addresses.getBodyOut()
                .getGeocodificarDireccionResponse()
                .getGeocodificarDireccionResult()
                .getListOfPuntosGeocodificados()
                .getPuntoGeocodificado();

        int contadorUbigeoDiferente = 0;
        int nivelConfianza = 0;
        for (int i = 0; i < countAddress; i++) {

            if (!data.get(i).getUbigeoGeocodificado().equals(ubigeo)) {
                contadorUbigeoDiferente++;
                continue;
            }

            StringBuilder addressVia = new StringBuilder();
            StringBuilder addressUrb = new StringBuilder();
            String direccionConcatenada = getCastAddress(data.get(i), provincia, addressVia, addressUrb);

            address = new Addresses();
            //TIPO VIAS -- TIPO URBANIZACION
            address.setxPunto(data.get(i).getXGeocodificado());
            address.setyPunto(data.get(i).getYGeocodificado());
            address.setReferencia(provincia.equalsIgnoreCase("LIMA") ? data.get(i).getReferencia() :
                    (!data.get(i).getReferencia().equals("") ? data.get(i).getReferencia() + " " : "") +
                            (!data.get(i).getManzana().equals("") ? "MZ " + data.get(i).getManzana() + " " : "") +
                            (!data.get(i).getLote().equals("") ? "LT " + data.get(i).getLote() : ""));

            address.setUbigeoGeocodificado(data.get(i).getUbigeoGeocodificado());
            address.setDescripcionUbigeo(data.get(i).getDescripcionUbigeo());
            address.setDireccionGeocodificada(data.get(i).getDireccionGeocodificada());
            address.setTipoVia(addressVia.toString());
            address.setNombreVia(data.get(i).getNombreVia());
            address.setNumeroPuerta1(data.get(i).getNumeroPuerta1());
            address.setNumeroPuerta2(data.get(i).getNumeroPuerta2());
            address.setCuadra(data.get(i).getCuadra());
            address.setTipoInterior(data.get(i).getTipoInterior());
            address.setNumeroInterior(data.get(i).getNumeroInterior());
            address.setPiso(data.get(i).getPiso());
            address.setTipoVivienda(data.get(i).getTipoVivienda());
            address.setNombreVivienda(data.get(i).getNombreVivienda());
            address.setTipoUrbanizacion(addressUrb.toString());
            address.setNombreUrbanizacion(data.get(i).getNombreUrbanizacion());
            address.setManzana(data.get(i).getManzana());
            address.setLote(data.get(i).getLote());
            address.setKilometro(data.get(i).getKilometro());
            address.setNivelConfianza(Integer.parseInt(data.get(i).getNivelConfianza()));
            if (Integer.parseInt(data.get(i).getNivelConfianza()) > Integer.parseInt(getNivelConfianza)) {
                nivelConfianza++;
            }
            address.setDireccion(direccionConcatenada);
            dataAddress.add(address);

        }
        normalizerResponse.setCountAddress(numeroRespuesta);
        normalizerResponse.setCountAddressOutUbigeo(contadorUbigeoDiferente);
        normalizerResponse.setAddresses(dataAddress);
        if (contadorUbigeoDiferente == numeroRespuesta) {
            normalizerResponse.setCodeMsmAddressNotFound(codeAddressOutUbigeo);
            messageAddressOutUbigeo = messageAddressOutUbigeo.replace("[NOMBRE_DISTRITO]", distrito);
            normalizerResponse.setMessageMsmAddressNotFoung(messageAddressOutUbigeo);
        }
        if (nivelConfianza > 0) {
            normalizerResponse.setParamNivelConfianza(Integer.parseInt(getNivelConfianza));
            normalizerResponse.setCodeMessageNivelConfianza(getCodeMsmConfianza);
            normalizerResponse.setMessageNivelConfianza(getMessageConfianza);
        }

        if (nivelConfianza > 0 && dataAddress.size() != 0) {
            normalizerResponse.setParamNivelConfianza(Integer.parseInt(getNivelConfianza));
            normalizerResponse.setCodeMessageNivelConfianza(getCodeMsmConfianza);
            normalizerResponse.setMessageNivelConfianza(getMessageConfianza);
        }

        normalizerResponseList.add(normalizerResponse);
        response.setResponseData(normalizerResponseList);
        return response;
    }

    private Response<List<NormalizerResponse>> getNomAddress(BodyOutGeoV2 direcciones,
                                                             String getCodeMsmConfianza,
                                                             String getMessageConfianza,
                                                             String distrito, String provincia,
                                                             String codeAddressOutUbigeo,
                                                             String messageAddressOutUbigeo,
                                                             String coordenadax,
                                                             String coordenaday){
        Response<List<NormalizerResponse>> response = new Response<>();
        Addresses address = null;
        List<Addresses> dataAddress = new ArrayList<>();
        NormalizerResponse normalizerResponse = new NormalizerResponse();
        List<NormalizerResponse> normalizerResponseList = new ArrayList<>();
        List<GeocodInvRespuestaTdpBE> data = new ArrayList<>();

        try{
            int total = Integer.parseInt(direcciones.getNumResponses());
            int nivelConfianza = 0;
            if(total > 0){
                int indice = -1;
                for(ResponseList x : direcciones.getResponseList()){
                    data.add(x.getGeocodInvRespuestaTdpBE());
                }

                for(int i = 0; i < total; i++){
                    GeocodInvRespuestaTdpBE obj = data.get(i);
                    if(i == 0){
                        StringBuilder addressVia = new StringBuilder();
                        StringBuilder addressUrb = new StringBuilder();
                        String direccionConcatenada = getCastAddress2(obj, provincia, addressVia, addressUrb);

                        address = new Addresses();
                        address.setReferencia(provincia.equalsIgnoreCase("LIMA") ? obj.getReference() :
                                (!obj.getReference().equals("") ? obj.getReference() + " " : "") +
                                        (!obj.getBlock().equals("") ? "MZ " + obj.getBlock() + " " : "") +
                                        (!obj.getLot().equals("") ? "LT " + obj.getLot() : ""));

                        address.setUbigeoGeocodificado(obj.getUbigeo());
                        address.setDescripcionUbigeo(obj.getDesUbigeo());
                        address.setDireccionGeocodificada(obj.getAddress());
                        address.setTipoVia(obj.getViaType());
                        address.setNombreVia(obj.getViaName());
                        address.setNumeroPuerta1(obj.getDoorNum());
                        //address.setNumeroPuerta2(obj.getNumeroPuerta2());
                        address.setCuadra(obj.getBlockNum());
                        address.setTipoInterior(obj.getInteriorType());
                        address.setNumeroInterior(obj.getInteriorNum());
                        address.setPiso(obj.getFloor());
                        address.setTipoVivienda(obj.getApartmentType());
                        address.setNombreVivienda(obj.getApartmentName());
                        address.setTipoUrbanizacion(obj.getUrbType());
                        address.setNombreUrbanizacion(obj.getUrbName());
                        address.setManzana(obj.getBlock());
                        address.setLote(obj.getLot());
                        address.setKilometro(obj.getKilometers());
                        address.setNivelConfianza(Integer.parseInt(obj.getPrecisionLevel()));
                        nivelConfianza = Integer.parseInt(obj.getPrecisionLevel());
                        address.setDireccion(obj.getAddress());
                        address.setxPunto(coordenadax);
                        address.setyPunto(coordenaday);
                        dataAddress.add(address);
                    }
                }

                normalizerResponse.setCountAddress(total);
                normalizerResponse.setAddresses(dataAddress);

                if (nivelConfianza > 5) {
                    normalizerResponse.setCodeMsmAddressNotFound(codeAddressOutUbigeo);
                    messageAddressOutUbigeo = messageAddressOutUbigeo.replace("[NOMBRE_DISTRITO]", distrito);
                    normalizerResponse.setMessageMsmAddressNotFoung(messageAddressOutUbigeo);
                    normalizerResponse.setParamNivelConfianza(nivelConfianza);
                }


            }
        }catch (Exception e){

        }

        normalizerResponseList.add(normalizerResponse);
        response.setResponseData(normalizerResponseList);

        return response;
    }

    private String getCastAddress(PuntoGeocodificado puntoGeocodificado, String provincia, StringBuilder refVia, StringBuilder refUrb) {
        String direccion = "";
        List<ParametersAddress> parametersAddresses = addressDao.readTypeViaAndUrb();
        for (ParametersAddress data : parametersAddresses) {
            switch (data.getCategory()) {
                case "Via":
                    if ((!puntoGeocodificado.getTipoVia().equals("")) && data.getElement().equals(puntoGeocodificado.getTipoVia())) {
                        refVia.append(data.getStrValue());
                        //refVia = data.getStrValue();
                    }
                    break;
                case "Urbanizacion":
                    if ((!puntoGeocodificado.getTipoUrbanizacion().equals("")) && data.getElement().equals(puntoGeocodificado.getTipoUrbanizacion())) {
                        refUrb.append(data.getStrValue());
                        //refUrb = data.getStrValue();
                    }
                    break;
            }
        }

        direccion = (refVia.toString().equals("") ? "" : refVia.toString() + " ")
                + (puntoGeocodificado.getNombreVia().equals("") ? "" : puntoGeocodificado.getNombreVia() + " ")
                + (puntoGeocodificado.getNumeroPuerta1().equals("") ? (puntoGeocodificado.getNumeroPuerta2().equals("") ? (puntoGeocodificado.getCuadra().equals("") ? "" : (puntoGeocodificado.getCuadra().equals("0") ? "S/N " : "CDRA " + puntoGeocodificado.getCuadra() + " ")) : puntoGeocodificado.getNumeroPuerta2() + " ") : puntoGeocodificado.getNumeroPuerta1() + " ")
                + (puntoGeocodificado.getTipoInterior().equals("") ? "" : puntoGeocodificado.getTipoInterior() + " ")
                + (puntoGeocodificado.getNumeroInterior().equals("") ? "" : puntoGeocodificado.getNumeroInterior() + " ")
                + (puntoGeocodificado.getPiso().equals("") ? "" : "PISO " + puntoGeocodificado.getPiso() + " ")
                + (puntoGeocodificado.getTipoVivienda().equals("") ? "" : puntoGeocodificado.getTipoVivienda() + " ")
                + (puntoGeocodificado.getNombreVivienda().equals("") ? "" : puntoGeocodificado.getNombreVivienda() + " ")
                + (refUrb.toString().equals("") ? "" : refUrb.toString() + " ")
                + (puntoGeocodificado.getNombreUrbanizacion().equals("") ? "" : puntoGeocodificado.getNombreUrbanizacion() + " ")
                + (provincia.equalsIgnoreCase("LIMA") ? (puntoGeocodificado.getManzana().equals("") ? "" : "MZ " + puntoGeocodificado.getManzana() + " ") : "")
                + (provincia.equalsIgnoreCase("LIMA") ? (puntoGeocodificado.getLote().equals("") ? "" : "LT " + puntoGeocodificado.getLote() + " ") : "")
                + (puntoGeocodificado.getKilometro().equals("") ? "" : "KM " + puntoGeocodificado.getKilometro() + " ");

        return direccion;
    }

    private String getCastAddress2(GeocodInvRespuestaTdpBE puntoGeocodificado, String provincia, StringBuilder refVia, StringBuilder refUrb) {
        String direccion = "";
        List<ParametersAddress> parametersAddresses = addressDao.readTypeViaAndUrb();
        for (ParametersAddress data : parametersAddresses) {
            switch (data.getCategory()) {
                case "Via":
                    if ((!puntoGeocodificado.getViaType().equals("")) && data.getElement().equals(puntoGeocodificado.getViaType())) {
                        refVia.append(data.getStrValue());
                        //refVia = data.getStrValue();
                    }
                    break;
                case "Urbanizacion":
                    if ((!puntoGeocodificado.getUrbType().equals("")) && data.getElement().equals(puntoGeocodificado.getUrbType())) {
                        refUrb.append(data.getStrValue());
                        //refUrb = data.getStrValue();
                    }
                    break;
            }
        }

        direccion = (refVia.toString().equals("") ? "" : refVia.toString() + " ")
                + (puntoGeocodificado.getViaName().equals("") ? "" : puntoGeocodificado.getViaName() + " ")
                + (puntoGeocodificado.getDoorNum().equals("") ? (puntoGeocodificado.getDoorNum().equals("") ? (puntoGeocodificado.getBlockNum().equals("") ? "" : (puntoGeocodificado.getBlockNum().equals("0") ? "S/N " : "CDRA " + puntoGeocodificado.getBlockNum() + " ")) : "" + " ") : puntoGeocodificado.getDoorNum() + " ")
                + (puntoGeocodificado.getInteriorType().equals("") ? "" : puntoGeocodificado.getInteriorType() + " ")
                + (puntoGeocodificado.getInteriorNum().equals("") ? "" : puntoGeocodificado.getInteriorNum() + " ")
                + (puntoGeocodificado.getFloor().equals("") ? "" : "PISO " + puntoGeocodificado.getFloor() + " ")
                + (puntoGeocodificado.getApartmentType().equals("") ? "" : puntoGeocodificado.getApartmentType() + " ")
                + (puntoGeocodificado.getApartmentName().equals("") ? "" : puntoGeocodificado.getApartmentName() + " ")
                + (refUrb.toString().equals("") ? "" : refUrb.toString() + " ")
                + (puntoGeocodificado.getUrbName().equals("") ? "" : puntoGeocodificado.getUrbName() + " ")
                + (provincia.equalsIgnoreCase("LIMA") ? (puntoGeocodificado.getBlock().equals("") ? "" : "MZ " + puntoGeocodificado.getBlock() + " ") : "")
                + (provincia.equalsIgnoreCase("LIMA") ? (puntoGeocodificado.getLot().equals("") ? "" : "LT " + puntoGeocodificado.getLot() + " ") : "")
                + (puntoGeocodificado.getKilometers().equals("") ? "" : "KM " + puntoGeocodificado.getKilometers() + " ");

        return direccion;
    }

    private ApiResponse<BodyOut> getGeocodificarDireccion(BodyIn request, String orderId) throws ClientException {
        String jsonResponse = "";
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(uriConsultaGD)
                .setApiId(apiidConsultaGD)
                .setApiSecret(apisecretConsultaGD)
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_GD)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_GD).build();
        GeocodificarDireccionClient gdc = new GeocodificarDireccionClient(config);
        ClientResult<ApiResponse<BodyOut>> result = gdc.post2(request);
        ApiResponse<BodyOut> objGD = result.getResult();


        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
            result.getEvent().setOrderId(orderId);
        serviceCallEventsService.registerEvent(result.getEvent());
        if (!result.isSuccess()) {
            throw result.getE();
        }
        if (result.isSuccess() && result.getEvent().getOrderId() != null) {

            addressDao.saveNormalizador(result.getEvent().getOrderId(), true,objGD.getBodyOut().getGeocodificarDireccionResponse().getGeocodificarDireccionResult().getGeocodedAddress(),"1");
        }

        boolean normalizador = addressDao.goToNormalizador(result.getEvent().getOrderId());
        System.out.println(normalizador);

        //update estado normalizador solo si es


        return objGD;

    }

    private BodyOutGeoV2 getGeocodificarDireccionV2(BodyInGeoV2 request, String orderId) throws ClientException {
        String jsonResponse = "";
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(uriConsultaGDV2)
                .setApiId(apiidConsultaGDV2)
                .setApiSecret(apisecretConsultaGDV2).build();
        GeocodificarDireccionV2Client gdc = new GeocodificarDireccionV2Client(config);
        ClientResult<String> result = gdc.postGenerico(request);
        BodyOutGeoV2 objGD = new BodyOutGeoV2();

        try{
            objGD = gdc.getResponseGeneric(result.getResult());
        }catch (Exception e){

        }

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
            result.getEvent().setOrderId(orderId);
        serviceCallEventsService.registerEvent(result.getEvent());
        if (!result.isSuccess()) {
            throw result.getE();
        }
        if (result.isSuccess() && result.getEvent().getOrderId() != null) {

           //to do esto es la respuesta
           // objGD.getResponseList().get(0).getGeocodInvRespuestaTdpBE();
            GeocodedAddress direccion = new GeocodedAddress();


            direccion.setTipoUrbanizacion(objGD.getResponseList().get(0).getGeocodInvRespuestaTdpBE().getUrbType());
            direccion.setNombreUrbanizacion(objGD.getResponseList().get(0).getGeocodInvRespuestaTdpBE().getUrbName());
            direccion.setNombreVia(objGD.getResponseList().get(0).getGeocodInvRespuestaTdpBE().getViaName());
            direccion.setTipoVia(objGD.getResponseList().get(0).getGeocodInvRespuestaTdpBE().getViaType());
            direccion.setManzana(objGD.getResponseList().get(0).getGeocodInvRespuestaTdpBE().getBlock());
            direccion.setLote(objGD.getResponseList().get(0).getGeocodInvRespuestaTdpBE().getLot());
            direccion.setCuadra(objGD.getResponseList().get(0).getGeocodInvRespuestaTdpBE().getBlockNum());


           addressDao.saveNormalizador(result.getEvent().getOrderId(), true,direccion,"2");
        }

        boolean normalizador = addressDao.goToNormalizador(result.getEvent().getOrderId());
        System.out.println(normalizador);

        //update estado normalizador solo si es


        return objGD;

    }


    public Response<AddressValidateResponse> getMessageValidationAddress(AddressValidateRequest request) {
        logger.info("Inicio GetMessageValidationAddress Service");
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("NORMALIZADOR");
        LogVass.serviceResponseHashMap(logger, "GetAddresses", "messageList", messageList);

        Response<AddressValidateResponse> response = new Response<>();
        try {
            response = validateAddress(request);
        } catch (Exception e) {
            logger.error("Error getMessageValidationAddress Controller.", e);
        }
        return response;
    }

    private Response<AddressValidateResponse> validateAddress(AddressValidateRequest request) {
        Response<AddressValidateResponse> addressValidate = new Response<>();
        AddressValidateResponse _addressValidate = new AddressValidateResponse();
        ErrorMessage _errorMessage = _validateAddressText(request.getAddress());

        _addressValidate.setValidAddressComplete(_validateAddress(request.getAddress()));
        _addressValidate.setIsValidAddressCompleteText(_errorMessage);
        addressValidate.setResponseData(_addressValidate);


        return addressValidate;
    }


    public ErrorMessage _oErrorMessage(String code) {
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("NORMALIZADOR");
        LogVass.serviceResponseHashMap(logger, "GetAddresses", "messageList", messageList);

        int count = 0;
        ErrorMessage _oErrorMessage[] = new ErrorMessage[7];

        _oErrorMessage[0] = new ErrorMessage(messageList.get("notFoundAddress").getCode() + "", messageList.get("notFoundAddress").getMessage() + "");
        _oErrorMessage[1] = new ErrorMessage(messageList.get("notSpaceAddress").getCode() + "", messageList.get("notSpaceAddress").getMessage() + "");
        _oErrorMessage[2] = new ErrorMessage(messageList.get("notCountAddress").getCode() + "", messageList.get("notCountAddress").getMessage() + "");
        _oErrorMessage[3] = new ErrorMessage(messageList.get("notLTMZAddress").getCode() + "", messageList.get("notLTMZAddress").getMessage() + "");
        _oErrorMessage[4] = new ErrorMessage(messageList.get("notVDAMZAddress").getCode() + "", messageList.get("notVDAMZAddress").getMessage() + "");
        _oErrorMessage[5] = new ErrorMessage(messageList.get("notVIAAddress").getCode() + "", messageList.get("notVIAAddress").getMessage() + "");
        _oErrorMessage[6] = new ErrorMessage(messageList.get("notURBAddress").getCode() + "", messageList.get("notURBAddress").getMessage() + "");

        switch (code) {
            case "0":
                count = 0;
                break;
            case "101":
                count = 1;
                break;
            case "102":
                count = 2;
                break;
            case "103":
                count = 3;
                break;
            case "104":
                count = 4;
                break;
            case "105":
                count = 5;
                break;
            case "106":
                count = 6;
                break;
        }
        return _oErrorMessage[count];
    }

    public boolean _validTipo(String address, String type) {//MZA, BLK, URB, PO, VIA
        boolean flag = false;
        String tokens = "";
        if (!(address == null || address.equals(""))) {
            switch (type) {
                case "MZA":
                    tokens = "MZ|MZA|MANZANA";
                    break;
                case "BLK":
                    tokens = "BLK|BLOCK|BLOK|BLOQUE|BLQ|BLOQ|ED|EDIFICIO|EDIF|TR|TORR|TORRE|ED|EDIFICIO|EDIF";
                    break;
                case "URB":
                    tokens = "URB|URBANIZACION|RES|RESIDENCIAL|AAHH|AH|ASENTAMIENTO HUMANO|ASENT HUMANO|CONDOMINIO|COND|AGRUPACION|AGRUP|SECTOR|SEC|SC|GRUPO|GRP|GR|ETAPA|ET|ZONA|ZN|COOPERATIVA|COPERATIVA|COOP|ASOCIACION|ASOC|CASERIO|CAS|PUEBLO JOVEN|P.JOVEN|PPJJ|COMUNIDAD|COMUNIDA|POBLADO|POB|CASERIO|CAS|CONJUNTO HABITACIONAL|CONJUNTO HAB|C.HABITACIONAL|CCHH|CH|UNIDAD VECINAL|UV|CASERIO|CAS";
                    break;
                case "PO":
                    tokens = "CASERIO|CAS|COMUNIDAD|COMUNIDA|POBLADO|POB";
                    break;
                case "VIA":
                    tokens = "AVENIDA|AVE|AV|CALLE|CALL|CLLE|CLL|CL|PASAJE|PSJ|PJ|CARRETERA|CARR|JIRON|JIRN|JRN|JR|PROLONGACION|PR|MALECON|ML|OVALO|OV|ALAMEDA|ALAM|ALM|CALLEJON";
                    break;
            }
            String[] _validTipo = tokens.split("\\|");
            for (String validTipo : _validTipo) {
                flag = Pattern.compile(".*" + validTipo + "[ \\.].*", Pattern.CASE_INSENSITIVE).matcher(address).matches();
                if (flag) {
                    break;
                }
            }
        }
        return flag;
    }

    public boolean _validateAddress(String address) {
        if (address != null) {
            boolean isPresentTipoVia = _validTipo(address, "VIA");
            boolean isPresentTipoMza = _validTipo(address, "MZA");
            boolean isPresentTipoBlk = _validTipo(address, "BLK");
            boolean isPresentTipoUrb = _validTipo(address, "URB");
            return (isPresentTipoVia || (isPresentTipoUrb && (isPresentTipoBlk || isPresentTipoMza)));
        } else {
            return false;
        }
    }

    public ErrorMessage _validateAddressText(String address) {
        if (address == null || address.equals("")) {
            return _oErrorMessage("101");
        }

        if (address.trim().split(" ").length < 2) {
            return _oErrorMessage("102");
        }

        boolean isPresentTipoVia = _validTipo(address, "VIA");
        boolean isPresentTipoMza = _validTipo(address, "MZA");
        boolean isPresentTipoBlk = _validTipo(address, "BLK");
        boolean isPresentTipoUrb = _validTipo(address, "URB");
        boolean isPresentNumero = Pattern.compile(".*[1-9][0-9]{2,3}( |$).*", Pattern.CASE_INSENSITIVE).matcher(address).matches();

        if (isPresentTipoVia && !isPresentNumero) {
            if (isPresentTipoVia && !isPresentTipoMza && !isPresentTipoUrb) {
                return _oErrorMessage("103");
            }
        }

        if (isPresentTipoUrb && !(isPresentTipoBlk || isPresentTipoMza)) {
            if (!isPresentTipoVia && !isPresentNumero) {
                return _oErrorMessage("104");
            }

            if (isPresentTipoVia && !isPresentTipoMza && !isPresentNumero) {
                return _oErrorMessage("105");
            }
        }

        if (!isPresentTipoUrb && (isPresentTipoBlk || isPresentTipoMza)) {
            if (!isPresentTipoVia) {
                return _oErrorMessage("106");
            }
        }

        return _oErrorMessage("0");
    }

    public Response getMessageAtis(AddressValidateAtisRequest request) {

        Response response = new Response();
        boolean flagNoMostrarCompletarDireccion = true;
        if (flagNoMostrarCompletarDireccion) {
            response.setResponseCode(AddressConstants.VALIDAR_ATIS_CODE_OK);
            response.setResponseMessage(AddressConstants.VALIDAR_ATIS_MESSAGE_OK);
            return response;
        }
        HashMap<String, MessageEntities> messageList = messageUtil.getMessages("NORMALIZADOR");
        LogVass.serviceResponseHashMap(logger, "GetAddresses", "messageList", messageList);


        if (request.getProvincia().equalsIgnoreCase("LIMA")) {

            if (!request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA01").getCode());
                response.setResponseMessage(messageList.get("msmVA01").getMessage());

            } else if (request.getTipoVia().equals("")
                    && !request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA02").getCode());
                response.setResponseMessage(messageList.get("msmVA02").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && !request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA03").getCode());
                response.setResponseMessage(messageList.get("msmVA03").getMessage());

            } else if (!request.getTipoVia().equals("")
                    && !request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA04").getCode());
                response.setResponseMessage(messageList.get("msmVA04").getMessage());

            } else if (!request.getTipoVia().equals("")
                    && !request.getNombreVia().equals("")
                    && !request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA05").getCode());
                response.setResponseMessage(messageList.get("msmVA05").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && !request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA07").getCode());
                response.setResponseMessage(messageList.get("msmVA07").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && !request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && !request.getManzana().equals("")
                    && !request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA08").getCode());
                response.setResponseMessage(messageList.get("msmVA08").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && !request.getNombreUrbanizacion().equals("")
                    && !request.getManzana().equals("")
                    && !request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA09").getCode());
                response.setResponseMessage(messageList.get("msmVA09").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && !request.getTipoUrbanizacion().equals("")
                    && !request.getNombreUrbanizacion().equals("")
                    && !request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA10").getCode());
                response.setResponseMessage(messageList.get("msmVA10").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && !request.getTipoUrbanizacion().equals("")
                    && !request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && !request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA11").getCode());
                response.setResponseMessage(messageList.get("msmVA11").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && !request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA13").getCode());
                response.setResponseMessage(messageList.get("msmVA13").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && !request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA14").getCode());
                response.setResponseMessage(messageList.get("msmVA14").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && !request.getManzana().equals("")
                    && !request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA15").getCode());
                response.setResponseMessage(messageList.get("msmVA15").getMessage());

            } else if (!request.getTipoVia().equals("")
                    && !request.getNombreVia().equals("")
                    && !request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA16").getCode());
                response.setResponseMessage(messageList.get("msmVA16").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && !request.getTipoUrbanizacion().equals("")
                    && !request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA18").getCode());
                response.setResponseMessage(messageList.get("msmVA18").getMessage());

            } else {

                response.setResponseCode(AddressConstants.VALIDAR_ATIS_CODE_OK);
                response.setResponseMessage(AddressConstants.VALIDAR_ATIS_MESSAGE_OK);

            }

        } else {

            if (!request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA19").getCode());
                response.setResponseMessage(messageList.get("msmVA19").getMessage());

            } else if (request.getTipoVia().equals("")
                    && !request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA20").getCode());
                response.setResponseMessage(messageList.get("msmVA20").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && !request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA21").getCode());
                response.setResponseMessage(messageList.get("msmVA21").getMessage());

            } else if (!request.getTipoVia().equals("")
                    && !request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA22").getCode());
                response.setResponseMessage(messageList.get("msmVA22").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && request.getTipoUrbanizacion().equals("")
                    && !request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA25").getCode());
                response.setResponseMessage(messageList.get("msmVA25").getMessage());

            } else if (!request.getTipoVia().equals("")
                    && !request.getNombreVia().equals("")
                    && !request.getTipoUrbanizacion().equals("")
                    && request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA34").getCode());
                response.setResponseMessage(messageList.get("msmVA34").getMessage());

            } else if (request.getTipoVia().equals("")
                    && request.getNombreVia().equals("")
                    && request.getNumeroPuerta1().equals("")
                    && !request.getTipoUrbanizacion().equals("")
                    && !request.getNombreUrbanizacion().equals("")
                    && request.getManzana().equals("")
                    && request.getLote().equals("")) {

                response.setResponseCode(messageList.get("msmVA36").getCode());
                response.setResponseMessage(messageList.get("msmVA36").getMessage());

            } else {

                response.setResponseCode(AddressConstants.VALIDAR_ATIS_CODE_OK);
                response.setResponseMessage(AddressConstants.VALIDAR_ATIS_MESSAGE_OK);

            }

        }

        return response;
    }

    public Response<List<CoordenadasXYResponse>> getCoordenadaXY(CoordenadasXYRequest request) {
        Response<List<CoordenadasXYResponse>> response = new Response<>();
        List<CoordenadasXYResponse> data = new ArrayList<>();
        FtthDao ftth = new FtthDao();
        data = ftth.getftth(request);
        if (data != null) {
            response.setResponseCode("1");
            response.setResponseMessage("Se obtuvieron con exito");
        } else {
            response.setResponseCode("0");
            response.setResponseMessage("Error al obtener los puntos ftth");
        }
        response.setResponseData(data);
        return response;
    }
}
