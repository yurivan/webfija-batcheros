package pe.com.tdp.ventafija.microservices.repository.user;

import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.constants.user.UserConstants;
import pe.com.tdp.ventafija.microservices.domain.profile.ResponseAccessData;
import pe.com.tdp.ventafija.microservices.domain.user.*;

import javax.sql.DataSource;

@Repository
public class UserDAO {
    private static final Logger logger = LogManager.getLogger();

    private static final String PERFIL_TIENDA = "TIENDA";
    @Value("${datasource.query.select}")
    private String MY_SQL_QUERY_WHERE;
    @Value("${datasource.query.insert}")
    private String MY_SQL_INSERT;
    @Value("${datasource.query.update}")
    private String MY_SQL_UPDATE;
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;

    @Autowired
    private DataSource datasource;

    public int outdatedDay(Connection con) {
        int days = 35;
        try {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "strvalue")
                    .replace(Constants.MY_SQL_TABLE, "parameters")
                    .replace(Constants.MY_SQL_WHERE, "element = 'days_reset_pwd' and category = 'AUTH_PARAMETER'");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQueryDAO(logger, "outdatedDay", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    days = Integer.parseInt(rs.getString("strvalue"));
                }
            }
            logger.info("outdatedDay => days: " + days);
        } catch (Exception e) {
            logger.error("Error outdatedDay DAO.", e);
        }
        return days;
    }

    public LoginResponseData login(LoginRequest request) {

        LoginResponseData responseData = null;
        try (Connection con = Database.datasource().getConnection()) {
            int outdatedDays = outdatedDay(con);
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "ID, phone, resetPwd, DATE_PART('day', now() -lastUpdateTime) as outdated")
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_WHERE, "ID = ? AND pwd = ? AND status = '1'");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getCodAtis());
                stmt.setString(2, request.getPassword());
                //stmt.setString(3, request.getImei());
                LogVass.daoQueryDAO(logger, "login", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    responseData = new LoginResponseData();
                    do {
                        responseData.setPhone(rs.getString("phone"));
                        if (rs.getInt("outdated") > outdatedDays) {
                            responseData.setResetPwd(UserConstants.RESET_TRUE);
                        } else {
                            responseData.setResetPwd(rs.getString("resetPwd"));
                        }
                    } while (rs.next());
                }

            }
        } catch (Exception e) {
            logger.error("Error login DAO.", e);
        }
        return responseData;
    }

    public LoginResponseData loginUsernamePassword(LoginRequest request) {

        LoginResponseData responseData = null;
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "ID, phone, resetPwd, DATE_PART('day', now() -lastUpdateTime)  as outdated")
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_WHERE, "ID = ? AND pwd = ? AND status = '1'");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getCodAtis());
                stmt.setString(2, request.getPassword());
                LogVass.daoQueryDAO(logger, "loginUsernamePassword", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                int outdatedDays = outdatedDay(con);
                if (rs.next()) {
                    responseData = new LoginResponseData();
                    do {
                        responseData.setPhone(rs.getString("phone"));
                        /*if (rs.getInt("outdated") > outdatedDays) {
                            responseData.setResetPwd(UserConstants.RESET_TRUE);
                        } else {
                            responseData.setResetPwd(rs.getString("resetPwd"));
                        }*/
                    } while (rs.next());
                }
            }
        } catch (Exception e) {
            logger.error("Error loginUsernamePassword DAO.", e);
        }
        return responseData;
    }

    public LoginResponseData loginData(LoginRequest request, LoginResponseData responseData) {

        try (Connection con = Database.datasource().getConnection()) {
            boolean hasTienda = false;
            boolean profileAssigned = false;
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "s.nombre, s.apePaterno, s.entidad, s.codATIS, u.resetPwd, s.canal, ch.groupChannel, s.nomPuntoVenta, DATE_PART('day', now() -lastUpdateTime)  as outdated, s.dni, s.canalatis, s.segmento, (select array_to_string(array(select campaign from ibmx_a07e6d02edaf552.tdp_catalogxsalesagent where codatis = ?), ', ')) as campanias, s.canalequivcampania, s.entidadequivcampania, s.niveles, s.type_flujo_tipificacion ")
                    .replace(Constants.MY_SQL_TABLE, "tdp_sales_agent s left join ibmx_a07e6d02edaf552.user u on u.id = s.codATIS left join ibmx_a07e6d02edaf552.tdp_channels ch on (ch.channelAtis = s.canal and (ch.groupChannel = '" + PERFIL_TIENDA + "' or ch.groupChannel = s.nomPuntoVenta))")
                    .replace(Constants.MY_SQL_WHERE, "codATIS = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getCodAtis());
                stmt.setString(2, request.getCodAtis());
                LogVass.daoQueryDAO(logger, "loginData", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    String groupChannel = rs.getString("groupChannel");
                    String nomPuntoVenta = rs.getString("nomPuntoVenta");
                    groupChannel = groupChannel == null ? null : groupChannel.trim();
                    nomPuntoVenta = nomPuntoVenta == null ? null : nomPuntoVenta.trim();

                    responseData.setName(rs.getString("nombre"));
                    responseData.setLastName(rs.getString("apePaterno"));
                    responseData.setDocument(rs.getString("dni"));
                    responseData.setEntity(rs.getString("entidad"));
                    responseData.setSellerChannelAtis(rs.getString("canalatis"));
                    responseData.setChannel(rs.getString("canal"));
                    responseData.setSellerSegment(rs.getString("segmento"));

                    responseData.setCampaigns(rs.getString("campanias"));

                    responseData.setGroup(nomPuntoVenta);

                    responseData.setSellerChannelEquivalentCampaign(rs.getString("canalequivcampania"));
                    responseData.setSellerEntityEquivalentCampaign(rs.getString("entidadequivcampania"));
                    responseData.setNiveles(rs.getString("niveles"));
                    responseData.setTypeFlujoTipificacion(rs.getString("type_flujo_tipificacion"));

                    if (groupChannel != null && PERFIL_TIENDA.equalsIgnoreCase(groupChannel)) {
                        hasTienda = true;
                    }
                    if (groupChannel != null && nomPuntoVenta != null) {
                        if (nomPuntoVenta.equalsIgnoreCase(groupChannel)) {
                            responseData.setGroupPermission(groupChannel);
                            profileAssigned = true;
                        }
                    } else if (groupChannel == null && nomPuntoVenta != null) {
                        responseData.setGroupPermission(nomPuntoVenta);
                        profileAssigned = true;
                    }
                }
                if (!profileAssigned && hasTienda) {
                    responseData.setGroupPermission(PERFIL_TIENDA);
                } else if (!profileAssigned) {
                    responseData.setGroupPermission(null);
                }
            }
        } catch (Exception e) {
            logger.error("Error loginData DAO.", e);
        }
        return responseData;
    }

    // Sprint 7... 
    public List<ConfigParameterResponseData> configParameter() {
        return configParameter(null);
    }

    public List<ConfigParameterResponseData> configParameter(String tipoAplicacion) {

        List<ConfigParameterResponseData> configParameterResponseData = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {

            // Sprint 6... se adiciona consulta al parametro de tiempo de expiracion de la sesion
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "element, strValue")
                    .replace(Constants.MY_SQL_TABLE, "parameters")
                    .replace(Constants.MY_SQL_WHERE, "domain = 'CONFIG' AND category = 'WEB' "
                            + " OR (DOMAIN = 'CONFIG' AND CATEGORY = 'AUTH_PARAMETER' AND ELEMENT = 'min_expired_session')");

            // Sprint 7... se adiciona consulta de tipos de documento aplicables por herramienta
            if (tipoAplicacion != null) {
                query += " OR (DOMAIN = ? AND CATEGORY = 'TIPOSDOCUMENTO') ";
            }

            try (PreparedStatement stmt = con.prepareStatement(query)) {

                // Sprint 7... se adiciona consulta de tipos de documento aplicables por herramienta
                if (tipoAplicacion != null) {
                    stmt.setString(1, tipoAplicacion);
                }

                LogVass.daoQueryDAO(logger, "configParameter", "query", stmt.toString());
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    ConfigParameterResponseData responseData = new ConfigParameterResponseData();
                    responseData.setElement(rs.getString("element"));
                    responseData.setStrValue(rs.getString("strValue"));
                    configParameterResponseData.add(responseData);
                }
            }
        } catch (Exception e) {
            logger.error("Error configParameter DAO.", e);
        }
        return configParameterResponseData;
    }

    public boolean validateVendor(String dni, String codAtis) {

        boolean validUser = false;
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "codATIS")
                    .replace(Constants.MY_SQL_TABLE, "tdp_sales_agent")
                    .replace(Constants.MY_SQL_WHERE, "dni = ? AND codATIS = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, dni);
                stmt.setString(2, codAtis);
                LogVass.daoQueryDAO(logger, "validateVendor", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    validUser = true;
                }
            }
        } catch (Exception e) {
            logger.error("Error validateVendor DAO.", e);
        }
        return validUser;
    }

    public ResetPasswordRequest validateUser(ResetPasswordRequest request) {

        request.setFlagReset(false);
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "phone, pwd, previousPwd")
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_WHERE, "ID = ? AND IMEI = ? AND status = '1'");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getCodAtis());
                stmt.setString(2, request.getImei());
                LogVass.daoQueryDAO(logger, "validateUser", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    request.setFlagReset(true);
                    request.setPhone(rs.getString("phone"));
                    request.setPwd(rs.getString("pwd"));
                    request.setPreviousPwd(rs.getString("previousPwd"));
                }
            }
        } catch (Exception e) {
            logger.error("Error validateUser DAO.", e);
        }
        return request;
    }

    public void registerUser(RegisterRequest request) {

        int rowsUpdate = 0;
        try (Connection con = Database.datasource().getConnection()) {
            String updateQuery = MY_SQL_UPDATE
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_COL, "pwd = ?, IMEI = ?, phone = ?, resetPwd = ?, status = '1', lastUpdateTime = now(), dni = ?, cTipoDoc = ?")
                    .replace(Constants.MY_SQL_WHERE, "ID = ?");

            try (PreparedStatement updatePS = con.prepareStatement(updateQuery)) {
                updatePS.setString(1, request.getPwd());
                updatePS.setString(2, request.getImei());
                updatePS.setString(3, request.getPhone());
                updatePS.setString(4, "0");
                updatePS.setString(5, request.getDni());
                updatePS.setString(6, request.getTipoDoc());
                updatePS.setString(7, request.getCodAtis());
                LogVass.daoQueryDAO(logger, "registerUser", "updateQuery", updatePS.toString());
                rowsUpdate = updatePS.executeUpdate();
                logger.info("registerUser => Update: " + rowsUpdate + " rows");
            }

            if (rowsUpdate <= 0) {
                String insertQuery = MY_SQL_INSERT
                        .replace(Constants.MY_SQL_TABLE, "user")
                        .replace(Constants.MY_SQL_COL, "pwd, IMEI, phone, resetPwd, ID, lastUpdateTime")
                        .replace(Constants.MY_SQL_PARAMETER, "?, ?, ?, ?, ?, now()");

                try (PreparedStatement insertPS = con.prepareStatement(insertQuery)) {
                    insertPS.setString(1, request.getPwd());
                    insertPS.setString(2, request.getImei());
                    insertPS.setString(3, request.getPhone());
                    insertPS.setString(4, request.getResetPwd());
                    insertPS.setString(5, request.getCodAtis());
                    LogVass.daoQueryDAO(logger, "registerUser", "insertQuery", insertPS.toString());
                    int rowsInsert = insertPS.executeUpdate();
                    logger.info("registerUser => Insert: " + rowsInsert + " rows");
                }
            }
        } catch (Exception e) {
            logger.error("Error registerUser DAO.", e);
        }

    }

    public ChangePasswordResponseData validatePassword(ChangePasswordRequest request) {

        ChangePasswordResponseData responseData = null;
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "pwd, previousPwd, resetPwd, TRUNC(EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - lastUpdateTime))) as unableTime")
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_WHERE, "ID = ? AND pwd = ? AND IMEI = ? AND status = '1'");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getCodAtis());
                stmt.setString(2, request.getPwd());
                stmt.setString(3, request.getImei());
                LogVass.daoQueryDAO(logger, "validatePassword", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    responseData = new ChangePasswordResponseData();
                    responseData.setPwd(rs.getString("pwd"));
                    responseData.setPreviousPwd(rs.getString("previousPwd"));
                    responseData.setResetPwd(rs.getString("resetPwd"));
                    responseData.setUnableTime(rs.getInt("unableTime"));
                }
            }
        } catch (Exception e) {
            logger.error("Error validatePassword DAO.", e);
        }
        return responseData;
    }

    public void changePassword(ChangePasswordRequest request) {

        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_UPDATE
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_COL, "pwd = ?, previousPwd = ?, resetPwd = ?, lastUpdateTime = now()")
                    .replace(Constants.MY_SQL_WHERE, "ID = ? AND pwd = ? AND IMEI = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getNewpwd());
                stmt.setString(2, request.getPreviousPwd());
                stmt.setString(3, request.getResetPwd());
                stmt.setString(4, request.getCodAtis());
                stmt.setString(5, request.getPwd());
                stmt.setString(6, request.getImei());
                LogVass.daoQueryDAO(logger, "changePassword", "query", stmt.toString());

                int rowsUpdate = stmt.executeUpdate();
                logger.info("changePassword => Update: " + rowsUpdate + " rows");
            }
        } catch (Exception e) {
            logger.error("Error changePassword DAO.", e);
        }

    }

    public void resetPassword(ResetPasswordRequest request) {

        try (Connection con = Database.datasource().getConnection()) {
            String updateQuery = MY_SQL_UPDATE
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_COL, "pwd = ?, IMEI = ?, phone = ?, resetPwd = ?, previousPwd = ?, lastUpdateTime = now()")
                    .replace(Constants.MY_SQL_WHERE, "ID = ?");

            try (PreparedStatement updatePS = con.prepareStatement(updateQuery)) {
                updatePS.setString(1, request.getPwd());
                updatePS.setString(2, request.getImei());
                updatePS.setString(3, request.getPhone());
                updatePS.setString(4, request.getResetPwd());
                updatePS.setString(5, request.getPreviousPwd());
                updatePS.setString(6, request.getCodAtis());
                updatePS.executeUpdate();
                LogVass.daoQueryDAO(logger, "resetPassword", "updateQuery", updatePS.toString());
                int rowsUpdate = updatePS.executeUpdate();
                logger.info("resetPassword => Update: " + rowsUpdate + " rows");
            }
        } catch (Exception e) {
            logger.error("Error resetPassword DAO.", e);
        }
    }


    public ChangePasswordWebResponseData validatePasswordWeb(ChangePasswordWebRequest request) {

        ChangePasswordWebResponseData responseData = null;
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "pwd, previousPwd, resetPwd, TRUNC(EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - lastUpdateTime))) as unableTime") //TRUNC(EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - lastUpdateTime)))
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_WHERE, "ID = ? AND status = '1'");//glazaror se saco el filtro isWeb=1...

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getCodAtis());
               //stmt.setString(2, request.getPwd());
                LogVass.daoQueryDAO(logger, "validatePasswordWeb", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    responseData = new ChangePasswordWebResponseData();
                    responseData.setPwd(rs.getString("pwd"));
                    responseData.setPreviousPwd(rs.getString("previousPwd"));
                    responseData.setResetPwd(rs.getString("resetPwd"));
                    responseData.setUnableTime(rs.getInt("unableTime"));
                }
            }
        } catch (Exception e) {
            logger.error("Error validatePasswordWeb DAO.", e);
        }
        return responseData;
    }

    public void changePasswordWeb(ChangePasswordWebRequest request) {

        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_UPDATE
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_COL, "pwd = ?, previousPwd = ?, lastUpdateTime = CURRENT_TIMESTAMP")// sprint 3
                    .replace(Constants.MY_SQL_WHERE, "ID = ?");//glazaror se quita el filtro isWeb=1

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getNewpwd());
                stmt.setString(2, request.getPreviousPwd());
                //stmt.setString(3, request.getResetPwd());
                stmt.setString(3, request.getCodAtis());
                //stmt.setString(5, request.getPwd());
                LogVass.daoQueryDAO(logger, "changePasswordWeb", "query", stmt.toString());

                int rowsUpdate = stmt.executeUpdate();
                logger.info("changePasswordWeb => Update: " + rowsUpdate + " rows");
            }
        } catch (Exception e) {
            logger.error("Error changePasswordWeb DAO.", e);
        }
    }

    public boolean validateVendor(String dni, String codAtis, String codCms) {
        boolean validUser = false;
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "codATIS")
                    .replace(Constants.MY_SQL_TABLE, "tdp_sales_agent")
                    .replace(Constants.MY_SQL_WHERE, "dni = ? AND codATIS = ?"); // AND appcode = 'W'

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, dni);
                stmt.setString(2, codAtis);
                //stmt.setString(3, codCms);
                LogVass.daoQueryDAO(logger, "validateVendor", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    validUser = true;
                }
            }
        } catch (Exception e) {
            logger.error("Error validateVendor DAO.", e);
        }
        return validUser;
    }

    /**
     * Restablece la contraseña del usuario a la clave por defecto
     *
     * @author glazaror
     * @modify Jhair Lozano
     **/
    public void resetPasswordWeb(ResetPasswordRequest request) {

        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_UPDATE
                    .replace(Constants.MY_SQL_TABLE, "user")
                    .replace(Constants.MY_SQL_COL,
                            "pwd = (select strValue from ibmx_a07e6d02edaf552.parameters where domain = 'LOGIN' and element = 'user.password.default'),"
                                    + "previousPwd = pwd, resetPwd = '1', lastUpdateTime = CURRENT_TIMESTAMP") // sprint 3
                    .replace(Constants.MY_SQL_WHERE, "ID = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getCodAtis());
                LogVass.daoQueryDAO(logger, "resetPasswordWeb", "query", stmt.toString());
                int rowsUpdate = stmt.executeUpdate();
                logger.info("resetPasswordWeb => Update: " + rowsUpdate + " rows");
            }
        } catch (Exception e) {
            logger.error("Error resetPasswordWeb DAO.", e);
        }
    }

    /**
     * Valida que el dni se encuentre asociado al codigo atis del vendedor
     *
     * @author glazaror
     * @modify Jhair Lozano
     **/
    public boolean validateVendorByAtis(String dni, String codAtis) {

        boolean validUser = false;
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "codATIS")
                    .replace(Constants.MY_SQL_TABLE, "tdp_sales_agent")
                    .replace(Constants.MY_SQL_WHERE, "dni = ? AND codATIS = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, dni);
                stmt.setString(2, codAtis);
                LogVass.daoQueryDAO(logger, "validateVendorByAtis", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    validUser = true;
                }
            }
        } catch (Exception e) {
            logger.error("Error validateVendorByAtis DAO.", e);
        }
        return validUser;
    }

    /**
     * Devuelve los permisos de un nivel
     *
     * @author Luis ortega
     * @modify Luis ortega
     **/
    public ResponseAccessData getAccessByNivel(int nivel, String canal) {

        ResponseAccessData access = new ResponseAccessData();
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "*")
                    .replace(Constants.MY_SQL_TABLE, "tdp_profiles")
                    .replace(Constants.MY_SQL_WHERE, "nivel = ? and canal = ?");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setInt(1, nivel);
                stmt.setString(2,canal);
                LogVass.daoQueryDAO(logger, "getAccessByNivel", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    access.setF_venta(rs.getBoolean("f_venta"));
                    access.setF_bandeja(rs.getBoolean("f_bandeja"));
                    access.setF_recupero(rs.getBoolean("f_recupero"));
                    access.setF_reptrack(rs.getBoolean("f_reptrack"));
                    access.setF_repventa(rs.getBoolean("f_repventa"));
                    access.setName(rs.getString("name"));
                    access.setId(rs.getInt("id"));
                }
            }
        } catch (Exception e) {
            logger.error("Error getAccessByNivel DAO.", e);
        }
        return access;
    }



    public void insertSaveLog(LogUserRequest request) throws ParseException {

        Date date = new Date();
        Timestamp ts=new Timestamp(date.getTime());

        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.saveUserLog " +
                    "(atis,sourceApp, fecha) "
                    + "VALUES (?, ?, ?)";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, request.getAtis());
            ps.setString(2, request.getSource());

            ps.setTimestamp(3,ts);

            //ps.setString(6, sdf.format(new Date()) + "-05:00");

            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insertAuto", e);
        }

    }
}
