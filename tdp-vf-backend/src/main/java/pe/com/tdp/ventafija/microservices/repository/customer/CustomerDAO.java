package pe.com.tdp.ventafija.microservices.repository.customer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.constant.CustomerConstants;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.Parameter;
import pe.com.tdp.ventafija.microservices.domain.customer.*;

@Repository
public class CustomerDAO {
    private static final Logger logger = LogManager.getLogger();
    @Value("${datasource.query.select}")
    private String MY_SQL_QUERY_WHERE;
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;
    @Value("${datasource.query.schema}")
    private String MY_SQL_QUERY_SCHEMA;

    // APPVTAFIJA-432: La consulta a visor no debe ser mayor a $customer.duplicados.max.age
    @Value("${customer.dupicados.max.age}")
    private Integer DUPLICADOS_MAX_AGE; // define el tiempo maximo de antiguedad que debe tener un pedido para la validacion de duplicados

    public List<ContractResponseParameter> readContractFlujo(String flujo) {
        List<ContractResponseParameter> list = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {

            String query = "";
            ServiceCallEvent event = VentaFijaContextHolder.getContext().getServiceCallEvent();

            if (event != null) {

                if (event.getSourceApp() != null) {

                    String srtvalue = "";
                    if (event.getSourceApp().equals(CustomerConstants.PARAMETRO_VENTA_FIJA_WEB)) {

                        srtvalue = CustomerConstants.PARAMETRO_COLUMNA_STR_WEB;
                        if (CustomerConstants.PARAMETRO_FLUJO_ALTA.equalsIgnoreCase(flujo)) {
                            query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "campo, domain, category, element, " + srtvalue)
                                    .replace(Constants.MY_SQL_TABLE, "tdp_contract")
                                    .replace(Constants.MY_SQL_WHERE, "type='" + CustomerConstants.PARAMETRO_FLUJO_ALTA + "'" + " and domain='" + CustomerConstants.PARAMETRO_CONTRACT + "'" + " and public ORDER BY auxiliar");
                        } else if (CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(flujo)) {
                            query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "campo, domain, category, element, " + srtvalue)
                                    .replace(Constants.MY_SQL_TABLE, "tdp_contract")
                                    .replace(Constants.MY_SQL_WHERE, "type='" + CustomerConstants.PARAMETRO_FLUJO_MIGRACION + "'" + " and domain='" + CustomerConstants.PARAMETRO_CONTRACT + "'" + " and public ORDER BY auxiliar");
                        } else if (CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(flujo)) {
                            query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "campo, domain, category, element, " + srtvalue)
                                    .replace(Constants.MY_SQL_TABLE, "tdp_contract")
                                    .replace(Constants.MY_SQL_WHERE, "type='" + CustomerConstants.PARAMETRO_FLUJO_SVA + "'" + " and domain='" + CustomerConstants.PARAMETRO_CONTRACT + "'" + " and public ORDER BY auxiliar");
                        }

                    } else if (event.getSourceApp().equals(CustomerConstants.PARAMETRO_VENTA_FIJA_APP)) {

                        srtvalue = CustomerConstants.PARAMETRO_COLUMNA_STR_APP;
                        if (CustomerConstants.PARAMETRO_FLUJO_ALTA.equalsIgnoreCase(flujo)) {
                            query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "campo, domain, category, element, " + srtvalue)
                                    .replace(Constants.MY_SQL_TABLE, "tdp_contract")
                                    .replace(Constants.MY_SQL_WHERE, "type='" + CustomerConstants.PARAMETRO_FLUJO_ALTA + "' and public ORDER BY auxiliar");
                        } else if (CustomerConstants.PARAMETRO_FLUJO_MIGRACION.equalsIgnoreCase(flujo)) {
                            query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "campo, domain, category, element, " + srtvalue)
                                    .replace(Constants.MY_SQL_TABLE, "tdp_contract")
                                    .replace(Constants.MY_SQL_WHERE, "type='" + CustomerConstants.PARAMETRO_FLUJO_MIGRACION + "' and public ORDER BY auxiliar");
                        } else if (CustomerConstants.PARAMETRO_FLUJO_SVA.equalsIgnoreCase(flujo)) {
                            query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "campo, domain, category, element, " + srtvalue)
                                    .replace(Constants.MY_SQL_TABLE, "tdp_contract")
                                    .replace(Constants.MY_SQL_WHERE, "type='" + CustomerConstants.PARAMETRO_FLUJO_SVA + "' and public ORDER BY auxiliar");
                        }
                    }

                    if (!query.equals("")) {

                        try (PreparedStatement stmt = con.prepareStatement(query)) {
                            LogVass.daoQuery(logger, stmt.toString());

                            ResultSet rs = stmt.executeQuery();
                            while (rs.next()) {
                                ContractResponseParameter response = new ContractResponseParameter();
                                response.setCategory(rs.getString("category"));
                                response.setElement(rs.getString("element"));
                                response.setStrValue(rs.getString(srtvalue));
                                response.setDomain(rs.getString("domain"));
                                response.setCampo(rs.getString("campo"));
                                list.add(response);

                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.info("Error readContractFlujo DAO.", e);
        }
        return list;
    }

    public DuplicateResponseVisor getVisor(DuplicateRequest request) {
        DuplicateResponseVisor responseVisor = null;
        try (Connection con = Database.datasource().getConnection()) {
            // APPVTAFIJA-432: La consulta a visor no debe ser mayor a $customer.duplicados.max.age
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL,
                            "ID_VISOR,ESTADO_SOLICITUD,NOMBRE_PRODUCTO,CODIGO_PEDIDO,FECHA_GRABACION,OPERACION_COMERCIAL")
                    .replace(Constants.MY_SQL_TABLE, "tdp_visor")
                    .replace(Constants.MY_SQL_WHERE,
                            "TIPO_DOCUMENTO=? AND DNI=? AND ESTADO_SOLICITUD <> 'NO EFECTIVO' AND datediff(curdate(), date(FECHA_GRABACION)) <= ? ORDER BY FECHA_GRABACION DESC LIMIT 1");
            logger.info("query: {}", query);
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getDocumentType());
                stmt.setString(2, request.getDocumentNumber());
                stmt.setInt(3, DUPLICADOS_MAX_AGE);
                logger.info("DUPLICADOS_MAX_AGE={}", DUPLICADOS_MAX_AGE);

                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    // TODO Verificar null en visor
                    String nombreProducto = rs.getString("NOMBRE_PRODUCTO");
                    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
                    Timestamp fechaGrabacion = rs.getTimestamp("FECHA_GRABACION", calendar);

                    responseVisor = new DuplicateResponseVisor();
                    responseVisor.setVisorID(rs.getString("ID_VISOR"));
                    responseVisor.setRequestStatus(rs.getString("ESTADO_SOLICITUD"));
                    responseVisor.setProductName(nombreProducto == null ? "INDEFINIDO" : nombreProducto);
                    responseVisor.setOrderCode(rs.getString("CODIGO_PEDIDO"));

                    responseVisor.setRecordingDate(fechaGrabacion == null ? "INDEFINIDO" : fechaGrabacion.toString());

                    responseVisor.setCommercialOperation(rs.getString("OPERACION_COMERCIAL"));
                    // list.add(new SimpleDateFormat("dd/MM/yyyy").format(new
                    // Date()));
                }
            }

        } catch (Exception e) {
            logger.info("Exception getVisor DAO.", e);
        }
        return responseVisor;
    }

    public List<String> readInformationCatalog(String productCode) {

        List<String> parameterList = new ArrayList<String>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL,
                            "installcost, financingcost, financingmonth, promspeed, periodpromspeed, equiplinea, equipinternet, equiptv")
                    .replace(Constants.MY_SQL_TABLE, "tdp_catalog")
                    .replace(Constants.MY_SQL_WHERE, "id = ? limit 1");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setInt(1, Integer.parseInt(productCode));
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    parameterList.add(rs.getString("installcost"));
                    parameterList.add(rs.getString("financingcost"));
                    parameterList.add(rs.getString("financingmonth"));
                    parameterList.add(rs.getString("promspeed"));
                    parameterList.add(rs.getString("periodpromspeed"));
                    parameterList.add(rs.getString("equiplinea"));
                    parameterList.add(rs.getString("equipinternet"));
                    parameterList.add(rs.getString("equiptv"));
                }
            }
        } catch (Exception e) {
            logger.error("Error readInformationCatalog DAO." + e);
        }
        return parameterList;
    }

    public List<Map<String, String>> getDefaultItemsCatalog() {

        List<Map<String, String>> detalleList = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL,
                            "distinct producttype, 'Financiado', cashprice, prodcategorycode")
                    .replace(Constants.MY_SQL_TABLE, "tdp_catalog")
                    .replace(Constants.MY_SQL_WHERE, "lower(commercialoperation) = lower(?) AND cashprice = '0.0'");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, "Alta Pura");
                //stmt.setString(2, "Financiado");
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    Map<String, String> obj = new HashMap<>();
                    obj.put("producttype", rs.getString("producttype"));
                    obj.put("cashprice", rs.getString("cashprice"));
                    obj.put("prodcategorycode", rs.getString("prodcategorycode"));
                    detalleList.add(obj);
                }
            }
        } catch (Exception e) {
            logger.error("Error readInformationCatalog DAO." + e);
        }
        return detalleList;
    }

    public List<Parameter> getParameter(String category) {

        List<Parameter> parameterList = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL,
                            "element, strValue")
                    .replace(Constants.MY_SQL_TABLE, "parameters")
                    .replace(Constants.MY_SQL_WHERE, "domain = 'TRANSLATE' AND category = ? ORDER BY element ASC");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, category);
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();

                while (rs.next()) {
                    Parameter parameter = new Parameter();
                    parameter.setElement(rs.getString("element"));
                    parameter.setStrValue(rs.getString("strValue"));
                    parameterList.add(parameter);
                }
            }
        } catch (Exception e) {
            logger.error("Error getParameter DAO." + e);
        }

        return parameterList;
    }

    public List<String[]> getContractSVAMotorizado(String flujo, String domain, String string) {
        List<String[]> contract = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {

            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL,
                            "distinct (case when (s.code = ANY (string_to_array(c.element, ','))) then s.CODE else s.prodTypeCode end) as \"CODE\", c.auxiliar, " +
                                    "REPLACE(REPLACE(REPLACE(c.appvalue, '" + CustomerConstants.PARAMETRO_PLANTILLA_PRODUCTO + "', UPPER(s.unit)), '" + CustomerConstants.PARAMETRO_PLANTILLA_DESCRIPCION + "',s.description), '" + CustomerConstants.PARAMETRO_PLANTILLA_PRECIO_UNITARIO + "', " +
                                    (domain == CustomerConstants.PARAMETRO_SPEECH ? "(REPLACE('' || s.cost, '.', ' soles con ') || ' c\u00e9ntimos')" : " ('' || s.cost)") + ") as decripcion, s.unit")
                    .replace(Constants.MY_SQL_TABLE, "tdp_sva s LEFT JOIN ibmx_a07e6d02edaf552.tdp_contract c ON s.code = ANY (string_to_array(c.element, ',')) OR s.prodTypeCode = ANY (string_to_array(c.element,','))")
                    .replace(Constants.MY_SQL_WHERE, "(s.description || s.cost in (" + (string.equals("") ? "''" : string) + ") or s.unit || s.cost in (" + (string.equals("") ? "''" : string) + ")) " +
                            "AND REPLACE(REPLACE(c.appvalue, '[PRODUCTO]', UPPER(S.unit)), '[description]', UPPER(s.description)) like '%'||UPPER(s.unit)||'%' " +
                            "AND case when s.code != 'BTV' then true else c.campo = s.unit end " +
                            "AND c.domain = '" + domain + "' " +
                            "AND c.category= '" + CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA + "' " +
                            "AND public " +
                            "AND c.type= '" + flujo + "' " +
                            "ORDER BY c.auxiliar");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    String[] lista = new String[3];
                    lista[0] = rs.getString(CustomerConstants.PARAMETRO_COLUMNA_CODE);
                    lista[1] = rs.getString(CustomerConstants.PARAMETRO_COLUMNA_DESCRIPCION);
                    lista[2] = rs.getString(CustomerConstants.PARAMETRO_COLUMNA_UNIT);
                    contract.add(lista);
                }
            }
        } catch (Exception e) {
            logger.error("Error getContract DAO." + e);
        }
        return contract;
    }

    public List<String[]> getContractSVA(String flujo, String domain, String string) {
        List<String[]> contract = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {

            ServiceCallEvent event = VentaFijaContextHolder.getContext().getServiceCallEvent();
            String srtvalue = "";
            if (event != null) {

                if (event.getSourceApp() != null) {

                    if (event.getSourceApp().equals(CustomerConstants.PARAMETRO_VENTA_FIJA_WEB)) {

                        srtvalue = CustomerConstants.PARAMETRO_COLUMNA_STR_WEB;
                    } else if (event.getSourceApp().equals(CustomerConstants.PARAMETRO_VENTA_FIJA_APP)) {

                        srtvalue = CustomerConstants.PARAMETRO_COLUMNA_STR_APP;
                    }
                }
            }
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL,
                            "s.ID, (case when (s.code = ANY (string_to_array(c.element, ','))) then s.CODE else s.prodTypeCode end) as \"CODE\", c.auxiliar, " +
                                    "REPLACE(REPLACE(REPLACE(c." + srtvalue + ", '" + CustomerConstants.PARAMETRO_PLANTILLA_PRODUCTO + "', UPPER(s.unit)), '" + CustomerConstants.PARAMETRO_PLANTILLA_DESCRIPCION + "',s.description), '" + CustomerConstants.PARAMETRO_PLANTILLA_PRECIO_UNITARIO + "', " +
                                    (domain == CustomerConstants.PARAMETRO_SPEECH ? "(REPLACE('' || s.cost, '.', ' soles con ') || ' c\u00e9ntimos')" : " ('' || s.cost)") + ") as decripcion, s.unit")
                    .replace(Constants.MY_SQL_TABLE, "tdp_sva s LEFT JOIN ibmx_a07e6d02edaf552.tdp_contract c ON s.code = ANY (string_to_array(c.element, ',')) OR s.prodTypeCode = ANY (string_to_array(c.element,','))")
                    .replace(Constants.MY_SQL_WHERE, "s.id in (" + (string.equals("") ? "0" : string) + ") " +
                            "AND REPLACE(REPLACE(c." + srtvalue + ", '[PRODUCTO]', UPPER(S.unit)), '[description]', UPPER(s.description)) like '%'||UPPER(s.unit)||'%' " +
                            "AND case when s.code != 'BTV' then true else c.campo = s.unit end " +
                            "AND c.domain = '" + domain + "' " +
                            "AND c.category= '" + CustomerConstants.PARAMETRO_TIPO_CATEGORIA_SVA + "' " +
                            "AND public " +
                            "AND c.type= '" + flujo + "' " +
                            "ORDER BY c.auxiliar");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    String[] lista = new String[3];
                    lista[0] = rs.getString(CustomerConstants.PARAMETRO_COLUMNA_CODE);
                    lista[1] = rs.getString(CustomerConstants.PARAMETRO_COLUMNA_DESCRIPCION);
                    lista[2] = rs.getString(CustomerConstants.PARAMETRO_COLUMNA_UNIT);
                    contract.add(lista);
                }
            }
        } catch (Exception e) {
            logger.error("Error getContract DAO." + e);
        }
        return contract;
    }


    public GetInfoResponseData getInfo(GetInfoRequest request) {
        GetInfoResponseData responseData = new GetInfoResponseData();

        try (Connection con = Database.datasource().getConnection()) {

            // Sprint 7... se adiciona la consulta por tipo documento

            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL, "firstName,lastName1,lastName2,email,customerPhone2,customerPhone,birthdate,nationality")
                    .replace(Constants.MY_SQL_TABLE, "customer").replace(Constants.MY_SQL_WHERE, "docNumber=?");

            if (request.getDocumentType() != null && !request.getDocumentType().isEmpty()) {
                query += " AND UPPER(DOCTYPE) = UPPER(?)";
            }

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getDocumentNumber());
                // Sprint 7... se adiciona la consulta por tipo documento
                if (request.getDocumentType() != null && !request.getDocumentType().isEmpty()) {
                    stmt.setString(2, request.getDocumentType());
                }

                LogVass.daoQuery(logger, stmt.toString());
                ResultSet rs = stmt.executeQuery();
                if (!rs.next()) {
                    responseData = null;
                } else {

                    do {
                        responseData.setFirstName(rs.getString("firstName"));
                        responseData.setLastName1(rs.getString("lastName1"));
                        responseData.setLastName2(rs.getString("lastName2"));
                        responseData.setEmail(rs.getString("email"));
                        responseData.setTelephone(rs.getString("customerPhone2"));
                        responseData.setMobilePhone(rs.getString("customerPhone"));

                        // Sprint 7... se adiciona respuesta de birthdate y nationality
                        responseData.setBirthDate(rs.getDate("birthdate"));
                        responseData.setNationality(rs.getString("nationality"));
                    } while (rs.next());
                }

            }

        } catch (Exception e) {
            logger.error("Error getInfo DAO.", e);
        }

        return responseData;
    }

    // TODO: Validar que para la web requiera el parche APPVTAFIJA-432
    public DuplicateResponseVisor getVisorForCategory(DuplicateForCategoryRequest request, Boolean isSVA) {
        DuplicateResponseVisor responseVisor = null;

        try (Connection con = Database.datasource().getConnection()) {

            String query = "";
            String strProductName = "CASE ";
            String strWhere = "";
            String sourceProductName = "";
            if (isSVA) {

                String[] SVAsplitted = request.getProductName().split("\\|");
                Boolean firstWhere = true;
                //Nombre del Producto Fuente
                sourceProductName = (SVAsplitted[0] != null && !SVAsplitted[0].trim().equals("")) ? SVAsplitted[0].trim() : "";

                //Bloque TV
                if (SVAsplitted[1] != null && !SVAsplitted[1].trim().equals("")) {
                    String toSearch = SVAsplitted[1].trim().replaceAll("\\s", "");
                    strProductName = strProductName + "WHEN lower(REPLACE(OD.tvblock,' ','')) = lower('" + toSearch + "')  THEN OD.tvblock ";
                    String OR = (firstWhere) ? "" : "OR ";
                    strWhere = strWhere + OR + "lower(REPLACE(OD.tvblock,' ','')) = lower('" + toSearch + "') ";
                    if (firstWhere) firstWhere = false;
                }
                //SVA internet
                if (SVAsplitted[2] != null && !SVAsplitted[2].trim().equals("")) {
                    String toSearch = SVAsplitted[2].trim().replaceAll("\\s", "");
                    strProductName = strProductName + "WHEN lower(REPLACE(OD.svainternet,' ','')) = lower('" + toSearch + "')  THEN OD.svainternet ";
                    String OR = (firstWhere) ? "" : "OR ";
                    strWhere = strWhere + OR + "lower(REPLACE(OD.svainternet,' ','')) = lower('" + toSearch + "') ";
                    if (firstWhere) firstWhere = false;
                }
                //SVA linea
                if (SVAsplitted[3] != null && !SVAsplitted[3].trim().equals("")) {
                    String toSearch = SVAsplitted[3].trim().replaceAll("\\s", "");
                    strProductName = strProductName + "WHEN lower(REPLACE(OD.svaline,' ','')) = lower('" + toSearch + "')  THEN OD.svaline ";
                    String OR = (firstWhere) ? "" : "OR ";
                    strWhere = strWhere + OR + "lower(REPLACE(OD.svaline,' ','')) = lower('" + toSearch + "') ";
                    if (firstWhere) firstWhere = false;
                }
                //Decos
                if ((SVAsplitted[4] != null && !SVAsplitted[4].trim().equals("")) || (SVAsplitted[5] != null && !SVAsplitted[5].trim().equals("")) || (SVAsplitted[6] != null && !SVAsplitted[6].trim().equals(""))) {
                    /*String SVAproductName = "";
                    if( SVAsplitted[3] != null && !SVAsplitted[3].trim().equals("") ) SVAproductName = SVAsplitted[3].trim();
                    if( SVAproductName.equals("") ) SVAproductName = SVAsplitted[4].trim();*/
                    strProductName = strProductName + "WHEN OD.decossd is not null OR OD.decoshd is not null OR OD.decosdvr is not null THEN 'DECO' ";
                    String OR = (firstWhere) ? "" : "OR ";
                    strWhere = strWhere + OR + "OD.decossd is not null OR OD.decoshd is not null OR OD.decosdvr is not null ";
                    //if(firstWhere){ firstWhere = false; responseVisor.setSVAProductName(Constants.DUPLICATE_SVA_DECO);}
                    if (firstWhere) firstWhere = false;
                }
                //Bloque Productos
                if (SVAsplitted[7] != null && !SVAsplitted[7].trim().equals("")) {
                    String toSearch = SVAsplitted[7].trim().replaceAll("\\s", "");
                    strProductName = strProductName + "WHEN lower(REPLACE(OD.blocktv,' ','')) = lower('" + toSearch + "')  THEN OD.blocktv ";
                    String OR = (firstWhere) ? "" : "OR ";
                    strWhere = strWhere + OR + "lower(REPLACE(OD.blocktv,' ','')) = lower('" + toSearch + "') ";
                    if (firstWhere) firstWhere = false;
                }

                //Repetidor smart wifi
                if ((SVAsplitted[8] != null && !SVAsplitted[8].trim().equals(""))) {
                    strProductName = strProductName + "WHEN OD.internetrsw is not null THEN 'REPETIDOR SMART WIFI' ";
                    String OR = (firstWhere) ? "" : "OR ";
                    strWhere = strWhere + OR + "OD.internetrsw is not null";
                    if (firstWhere) firstWhere = false;
                }

                strProductName = strProductName + "ELSE '' END NOMBRE_PRODUCTO ";

                query = MY_SQL_QUERY_WHERE
                        .replace(Constants.MY_SQL_COL, "ID_VISOR, ESTADO_SOLICITUD, " + strProductName + " , CODIGO_PEDIDO, FECHA_GRABACION, OPERACION_COMERCIAL")
                        .replace(Constants.MY_SQL_TABLE, "tdp_visor V INNER JOIN " + MY_SQL_QUERY_SCHEMA + ".order_detail OD ON OD.orderid = V.id_visor");
                query = query.replace(Constants.MY_SQL_WHERE, "lower(V.TIPO_DOCUMENTO) = lower(?) AND V.DNI = ? AND  translate(lower(V.departamento), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') = translate(lower(?), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') AND translate(lower(V.provincia), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') = translate(lower(?), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') AND translate(lower(V.distrito), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') = translate(lower(?), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') AND ( " + strWhere + " ) AND lower(REPLACE(V.nombre_producto_fuente,' ','')) = lower(?) AND V.ESTADO_SOLICITUD not in ('NO EFECTIVO','CAIDA') AND V.operacion_comercial = 'SVAS' AND v.fecha_grabacion > (now() - INTERVAL '1 months') ORDER BY V.FECHA_GRABACION DESC LIMIT 1");


            } else {
                query = MY_SQL_QUERY_WHERE
                        .replace(Constants.MY_SQL_COL, "ID_VISOR,ESTADO_SOLICITUD, NOMBRE_PRODUCTO, CODIGO_PEDIDO, FECHA_GRABACION, OPERACION_COMERCIAL")
                        .replace(Constants.MY_SQL_TABLE, "tdp_visor v");
                query = query.replace(Constants.MY_SQL_WHERE, "lower(TIPO_DOCUMENTO) = lower(?) AND DNI = ? AND  translate(lower(departamento), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') = translate(lower(?), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') AND translate(lower(provincia), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') = translate(lower(?), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') AND translate(lower(distrito), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') = translate(lower(?), 'áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ', 'aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC') AND (lower(REPLACE(NOMBRE_PRODUCTO,' ','')) = lower(?) OR lower(REPLACE(SUB_PRODUCTO,' ','')) = lower(?) OR lower(REPLACE(SUB_PRODUCTO_EQUIV,' ','')) = lower(?)) AND ESTADO_SOLICITUD not in ('NO EFECTIVO','CAIDA') AND v.fecha_grabacion > (now() - INTERVAL '1 months') ORDER BY FECHA_GRABACION DESC LIMIT 1");
            }

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                if (isSVA) {
                    stmt.setString(1, request.getDocumentType());
                    stmt.setString(2, request.getDocumentNumber());

                    stmt.setString(3, request.getDepartmentName());
                    stmt.setString(4, request.getProvinceName());
                    stmt.setString(5, request.getDistrictName());
                    stmt.setString(6, sourceProductName.replaceAll("\\s", ""));
                } else {
                    stmt.setString(1, request.getDocumentType());
                    stmt.setString(2, request.getDocumentNumber());

                    stmt.setString(3, request.getDepartmentName());
                    stmt.setString(4, request.getProvinceName());
                    stmt.setString(5, request.getDistrictName());

                    stmt.setString(6, request.getProductName().replaceAll("\\s", ""));
                    stmt.setString(7, request.getProductName().replaceAll("\\s", ""));
                    stmt.setString(8, request.getProductName().replaceAll("\\s", ""));
                }

                LogVass.daoQueryDAO(logger, "getVisorForCategory", "query", stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    String nombreProducto = rs.getString("NOMBRE_PRODUCTO");
                    Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
                    Timestamp fechaGrabacion = rs.getTimestamp("FECHA_GRABACION", calendar);
                    //logger.info("datos de sql " + nombreProducto);
                    responseVisor = new DuplicateResponseVisor();
                    responseVisor.setVisorID(rs.getString("ID_VISOR"));
                    responseVisor.setRequestStatus(rs.getString("ESTADO_SOLICITUD"));
                    responseVisor.setProductName(nombreProducto == null ? "INDEFINIDO" : nombreProducto);
                    responseVisor.setOrderCode(rs.getString("CODIGO_PEDIDO"));

                    responseVisor.setRecordingDate(fechaGrabacion == null ? "INDEFINIDO" : fechaGrabacion.toString());

                    responseVisor.setCommercialOperation(rs.getString("OPERACION_COMERCIAL"));
                }
            }

        } catch (Exception e) {
            logger.error("Error getVisorForCategory DAO.", e);
        }
        return responseVisor;
    }

    public List<ServicesWebResponseData> getDescriptionPark(String codes, List<ServicesWebResponseData> park) {

        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE
                    .replace(Constants.MY_SQL_COL,
                            "id, telefono as codATis, servicio as codCms, paqueteorigen as nombrePaquete, cantidaddecos as numDecos,rentatotal as rentaTotal,internet as velInter")
                    .replace(Constants.MY_SQL_TABLE, "tdp_calculator")
                    .replace(Constants.MY_SQL_WHERE, "FIND_IN_SET(telefono, ?) OR FIND_IN_SET(servicio, ?)");
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, codes);
                stmt.setString(2, codes);
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    for (ServicesWebResponseData responseData : park) {

                        if (responseData.getParkType().equals("CMS") && responseData.getServiceCode().equals(rs.getString("codCms"))) {
                            logger.info("getDescriptionPark CMS");
                            responseData.setCodATis(rs.getString("codATis"));
                            responseData.setCodCms(rs.getString("codCms"));
                            responseData.setProductDescription(rs.getString("nombrePaquete"));
                            responseData.setNumDecos(rs.getString("numDecos"));
                            responseData.setRentaTotal(rs.getString("rentaTotal"));
                            responseData.setVelInter(rs.getString("velInter"));
                        } else if (responseData.getParkType().equals("ATIS") && responseData.getPhone().equals(rs.getString("codATis"))) {
                            logger.info("getDescriptionPark ATIS");
                            responseData.setCodATis(rs.getString("codATis"));
                            responseData.setCodCms(rs.getString("codCms"));
                            responseData.setProductDescription(rs.getString("nombrePaquete"));
                            responseData.setNumDecos(rs.getString("numDecos"));
                            responseData.setRentaTotal(rs.getString("rentaTotal"));
                            responseData.setVelInter(rs.getString("velInter"));
                        }
                    }
                }
            }

        } catch (Exception e) {
            logger.info("Error getDescriptionPark DAO." + e);
        }

        return park;
    }

    // Sprint 6 - condiciones contrato
    public List<ContractResponseParameter> readSaleConditions(ContractWebRequest request) {
        List<ContractResponseParameter> list = new ArrayList<>();

        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_QUERY_WHERE.replace(Constants.MY_SQL_COL, "domain, category, element, strValue, auxiliar, strfilter")
                    .replace(Constants.MY_SQL_TABLE, "parameters")
                    .replace(Constants.MY_SQL_WHERE, "domain = 'CONTRATOCONDICION' ORDER BY auxiliar ASC");

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQuery(logger, stmt.toString());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    ContractResponseParameter response = new ContractResponseParameter();
                    response.setCategory(rs.getString("category"));
                    response.setElement(rs.getString("element"));
                    response.setStrValue(rs.getString("strValue"));
                    response.setDomain(rs.getString("domain"));
                    response.setAuxiliar(rs.getInt("auxiliar"));
                    response.setStrfilter(rs.getString("strfilter"));
                    list.add(response);
                }
            }
        } catch (Exception e) {
            logger.error("Error readContract DAO.", e);
        }

        return list;
    }

    public Integer actualizarContactos(ContractWebRequestCustomer request) {

        Integer response = new Integer(0);
        int rowsUpdate;

        try (Connection con = Database.datasource().getConnection()) {
            String query = null;
            if (request.getMobilePhone() != null && !request.getMobilePhone().trim().isEmpty()) {
                query = "update ibmx_a07e6d02edaf552.customer set customerphone = ?, customerphone2 = ? where docnumber = ?";
            } else {
                query = "update ibmx_a07e6d02edaf552.customer set customerphone2 = ? where docnumber = ?";
            }

            logger.info("telephone= " + request.getTelephone());
            logger.info("documnetnumber= " + request.getDocumentNumber());
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQuery(logger, stmt.toString());
                int columnIndex = 1;
                if (request.getMobilePhone() != null && !request.getMobilePhone().trim().isEmpty()) {
                    stmt.setString(columnIndex++, request.getMobilePhone());
                }
                stmt.setString(columnIndex++, request.getTelephone());
                stmt.setString(columnIndex++, request.getDocumentNumber());

                rowsUpdate = stmt.executeUpdate();
                response = rowsUpdate;
                logger.info("actualizarCustomer=> Update: " + rowsUpdate + " rows");
            }
        } catch (Exception e) {
            logger.error("Error actualizarContactos DAO.", e);
        }

        return response;
    }


    public Integer updateContactTdpOrder(UpdTelfTdpOrderResponseData request) {
        request.getCustomer();
        Integer response = new Integer(0);
        int rowsUpdate;

        try (Connection con = Database.datasource().getConnection()) {
            String query = null;
            if (request.getCustomer().getMobilePhone() != null && !request.getCustomer().getMobilePhone().trim().isEmpty()) {
                query = "update ibmx_a07e6d02edaf552.tdp_order set client_telefono_1 = ?, client_telefono_2 = ? where order_id = ?";
            } else {
                query = "update ibmx_a07e6d02edaf552.tdp_order set client_telefono_1 = ? where order_id = ?";
            }

            logger.info("telephone= " + request.getCustomer().getTelephone());
            logger.info("documnetnumber= " + request.getCustomer().getDocumentNumber());
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQuery(logger, stmt.toString());
                int columnIndex = 1;
                if (request.getCustomer().getMobilePhone() != null && !request.getCustomer().getMobilePhone().trim().isEmpty()) {
                    stmt.setString(columnIndex++, request.getCustomer().getMobilePhone());
                }
                stmt.setString(columnIndex++, request.getCustomer().getTelephone());
                stmt.setString(columnIndex++, request.getOrder_id());

                rowsUpdate = stmt.executeUpdate();
                response = rowsUpdate;
                logger.info("actualizar Tdp_Order=> Update: " + rowsUpdate + " rows");
            }
        } catch (Exception e) {
            logger.error("Error actualizarContactos DAO.", e);
        }

        return response;
    }

    public Integer updateContactTdpVisor(UpdTelfTdpOrderResponseData request) {
        request.getCustomer();
        Integer response = new Integer(0);
        int rowsUpdate;

        try (Connection con = Database.datasource().getConnection()) {
            String query = null;
            if (request.getCustomer().getMobilePhone() != null && !request.getCustomer().getMobilePhone().trim().isEmpty()) {
                query = "update ibmx_a07e6d02edaf552.tdp_visor set telefono = ?, telefono2 = ? where id_visor = ?";
            } else {
                query = "update ibmx_a07e6d02edaf552.tdp_visor set telefono = ? where id_visor = ?";
            }
            logger.info("Query usada Tdp_Visor: " + query);
            logger.info("telephone= " + request.getCustomer().getTelephone());
            logger.info("documnetnumber= " + request.getCustomer().getDocumentNumber());
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQuery(logger, stmt.toString());
                int columnIndex = 1;
                if (request.getCustomer().getMobilePhone() != null && !request.getCustomer().getMobilePhone().trim().isEmpty()) {
                    stmt.setString(columnIndex++, request.getCustomer().getMobilePhone());
                }
                stmt.setString(columnIndex++, request.getCustomer().getTelephone());
                stmt.setString(columnIndex++, request.getOrder_id());

                rowsUpdate = stmt.executeUpdate();
                response = rowsUpdate;
                logger.info("actualizar Tdp_Visor=> Update: " + rowsUpdate + " rows");
            }
        } catch (Exception e) {
            logger.error("Error actualizarTelefono DAO.", e);
        }

        return response;
    }

}
