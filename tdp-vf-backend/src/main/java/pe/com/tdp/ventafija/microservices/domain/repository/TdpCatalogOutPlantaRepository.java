package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpCatalogOutPlanta;

public interface TdpCatalogOutPlantaRepository extends JpaRepository<TdpCatalogOutPlanta, Integer>  {
    @Query(value = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_catalog_planta_out WHERE numero_telefono = ?1 LIMIT 1", nativeQuery = true)
    TdpCatalogOutPlanta findByPhoneOrCms(String phoneOrCms);
}
