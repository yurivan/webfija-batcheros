package pe.com.tdp.ventafija.microservices.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpCatalog;

public interface TdpCatalogRepository extends JpaRepository<TdpCatalog, Integer> {

}
