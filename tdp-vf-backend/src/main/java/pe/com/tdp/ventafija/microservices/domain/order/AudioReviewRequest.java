package pe.com.tdp.ventafija.microservices.domain.order;

public class AudioReviewRequest {

	private String firebaseId;

	public String getFirebaseId() {
		return firebaseId;
	}

	public void setFirebaseId(String firebaseId) {
		this.firebaseId = firebaseId;
	}

}
