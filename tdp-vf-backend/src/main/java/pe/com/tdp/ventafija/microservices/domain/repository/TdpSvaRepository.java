package pe.com.tdp.ventafija.microservices.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpSva;

public interface TdpSvaRepository extends JpaRepository<TdpSva, Integer> {

	List<TdpSva> findByIdIn(List<Integer> ids);
}
