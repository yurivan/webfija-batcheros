package pe.com.tdp.ventafija.microservices.domain.dto;

public class WebRegistrationRequestDto {
  private String tipoDoc;
  private String dni;
  private String codAtis;
  private String codCms;
  private String password;


  public String getTipoDoc() {
    return tipoDoc;
  }

  public void setTipoDoc(String tipoDoc) {
    this.tipoDoc = tipoDoc;
  }

  public String getDni() {
    return dni;
  }

  public void setDni(String dni) {
    this.dni = dni;
  }

  public String getCodAtis() {
    return codAtis;
  }

  public void setCodAtis(String codAtis) {
    this.codAtis = codAtis;
  }

  public String getCodCms() {
    return codCms;
  }

  public void setCodCms(String codCms) {
    this.codCms = codCms;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
