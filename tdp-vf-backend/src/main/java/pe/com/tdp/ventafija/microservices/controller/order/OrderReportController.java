package pe.com.tdp.ventafija.microservices.controller.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pe.com.tdp.ventafija.microservices.common.constant.CustomerConstants;
import pe.com.tdp.ventafija.microservices.common.domain.dto.SalesReport;
import pe.com.tdp.ventafija.microservices.common.util.OrderConstants;
import pe.com.tdp.ventafija.microservices.common.util.ReportUtil;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.order.*;
import pe.com.tdp.ventafija.microservices.service.order.OrderService;

import java.util.List;

@Controller
@RequestMapping(value = "/order/web/report")
public class OrderReportController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private ApplicationContext applicationContext;

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/ventareporte", method = RequestMethod.POST)
    public ModelAndView generarReporteVenta(@RequestBody VentaAudioRequest ventaAudioRequest) {
        List<SalesReport> ventas = orderService
                .getReporteVenta(ventaAudioRequest.getCodigoVendedor(), ventaAudioRequest.getCodigoEstado(), ventaAudioRequest.getFechaInicio(), ventaAudioRequest.getFechaFin());

        return ReportUtil.getModelAndView(applicationContext, ventas, "RPT_VENTAS", "HISTORICO");
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/audioventareporte", method = RequestMethod.POST)
    public ModelAndView generarReporteAudioVenta(@RequestBody VentaAudioRequest ventaAudioRequest) {
        List<SalesReport> ventas = orderService.getBandejaVentasAudio(ventaAudioRequest.getNombrePuntoVenta(), ventaAudioRequest.getEntidad(),
                ventaAudioRequest.getCodigoEstado(), ventaAudioRequest.getCodigoVendedor(), ventaAudioRequest.getBuscarPorVendedor(),
                ventaAudioRequest.getDniCliente(), ventaAudioRequest.getFechaInicio(), ventaAudioRequest.getFechaFin(), ventaAudioRequest.getFiltroRestringeHoras());

        return ReportUtil.getModelAndView(applicationContext, ventas, "RPT_VENTAS", "AUDIO");
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/ventahistoricoreporte", method = RequestMethod.POST)
    public ModelAndView generarReporteHistoricoVenta(@RequestBody SalesReportRequest salesReportRequest) {
        List<SalesReport> ventas = orderService.reporteVentas(salesReportRequest.getNumDays(), salesReportRequest.getVendorId(), salesReportRequest.getIdTransaccion(), salesReportRequest.getFilterDni(), salesReportRequest.getStatus());
        return ReportUtil.getModelAndView(applicationContext, ventas, "RPT_VENTAS", "HISTORICO");
    }

    @CrossOrigin(origins = "*")
    @ResponseBody
    @RequestMapping(value = "/obtenerFiltroReporte", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<ReportFiltersResponse> obtenerFiltrosReporte(@RequestBody ReportFiltersRequest request) {
        /*Response<SalesReportTypeResponse> response = new Response<>();*/
        /*SalesReportTypeResponse data = orderService.obtieneFiltrosReporte(salesReportTypeRequest);
        if(data.getActiveFilterList() != null && data.getActiveFilterData() != null){
            System.out.println("reporteVentaList no nulo");
            response.setResponseCode(OrderConstants.PARAMETER_COD_RESPUESTA_OK);
            response.setResponseData(data);
            return response;
        }*/
        Response<ReportFiltersResponse> response = new Response<>();
        ReportFiltersResponse data = orderService.getFiltersReports(request);

        if (data != null) {
            response.setResponseCode("0");
            response.setResponseMessage("OK");
            response.setResponseData(data);
        } else {
            response.setResponseCode("1");
            response.setResponseMessage("No hay filtros para este vendedor");
        }

        return response;
    }

    @CrossOrigin(origins = "*")
    @ResponseBody
    @RequestMapping(value = "/filtrarReporte", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SalesReportTypeResponse> filtrarReporte(@RequestBody ReportVentasRequest request) {
        /*Response<SalesReportTypeResponse> response = new Response<>();

        SalesReportTypeResponse data = orderService.obtieneData(salesReportTypeRequest);

        if (data.getReporteVentaList() != null) {
            response.setResponseCode(OrderConstants.PARAMETER_COD_RESPUESTA_OK);
            response.setResponseData(data);
            return response;
        }
        return null;*/
        Response<SalesReportTypeResponse> response = new Response<>();
        SalesReportTypeResponse data = orderService.getDate(request);
        if(data != null){
            response.setResponseCode("0");
            response.setResponseMessage("OK");
            response.setResponseData(data);
        }else{
            response.setResponseCode("1");
            response.setResponseMessage("No se encontro datos.");
        }

        return response;
    }

    @CrossOrigin(origins = "*")
    @ResponseBody
    @RequestMapping(value = "/exportReporte", method = RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public Response<SalesReportExportResponseBody> exportarReporte(@RequestBody ReportVentasRequest request) {

        Response<SalesReportExportResponseBody> response = new Response<>();
        SalesReportExportResponseBody data = orderService.getDataExport(request);
        if(data != null && data.getReporteVentas().size() > 0){
            response.setResponseCode("0");
            response.setResponseMessage("OK");
            response.setResponseData(data);
        }else{
            response.setResponseCode("1");
            response.setResponseMessage("No se encontro datos.");
        }

        return response;
    }

}
