package pe.com.tdp.ventafija.microservices.domain.user;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class RegisterRequest {


    private String tipoDoc;
    private String dni;
    private String codAtis;
    private String phone;
    private String imei;
    private String pwd;
    private String repeatPwd;
    @JsonIgnore
    private String resetPwd;
    @JsonIgnore
    private String previousPwd;

    public RegisterRequest() {
    }

    public RegisterRequest(String tipoDoc, String dni, String codAtis) {
        this.dni = dni;
        this.codAtis = codAtis;
        this.tipoDoc = tipoDoc;
    }

    public String getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getResetPwd() {
        return resetPwd;
    }

    public void setResetPwd(String resetPwd) {
        this.resetPwd = resetPwd;
    }

    public String getPreviousPwd() {
        return previousPwd;
    }

    public void setPreviousPwd(String previousPwd) {
        this.previousPwd = previousPwd;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getCodAtis() {
        return codAtis;
    }

    public void setCodAtis(String codAtis) {
        this.codAtis = codAtis;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getRepeatPwd() {
        return repeatPwd;
    }

    public void setRepeatPwd(String repeatPwd) {
        this.repeatPwd = repeatPwd;
    }
}
