package pe.com.tdp.ventafija.microservices.service.order;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import pe.com.tdp.ventafija.microservices.common.connection.Firebase;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Customer;
import pe.com.tdp.ventafija.microservices.common.exception.DataValidationException;
import pe.com.tdp.ventafija.microservices.domain.firebase.SaleApiFirebaseResponseBody;
import pe.com.tdp.ventafija.microservices.domain.order.entity.CustomerValidator;
import pe.com.tdp.ventafija.microservices.domain.order.parser.CustomerFirebaseParser;
import pe.com.tdp.ventafija.microservices.domain.order.parser.FirebaseParser;
import pe.com.tdp.ventafija.microservices.domain.repository.CustomerRepository;

import java.security.SecureRandom;
import java.util.Random;

@Service
public class OrderCustomerService {
    private static final Logger logger = LogManager.getLogger();
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private Firebase firebase;

    @Transactional
    public void saveOrUpdate(String firebaseId) throws DataValidationException {
        SaleApiFirebaseResponseBody objFirebase = firebase.loadFirebaseObject(firebaseId);

        if (objFirebase != null) {
            saveOrUpdate(objFirebase);
        }
    }

    @Transactional
    public Customer saveOrUpdate(SaleApiFirebaseResponseBody objFirebase) throws DataValidationException {

        if (objFirebase == null) {
            return null;
        }

        FirebaseParser<Customer> parser = new CustomerFirebaseParser();

        Customer customer = parser.parse(objFirebase);

        Long id = customerRepository.loadIdByDocTypeAndDocNumber(customer.getDocType().toUpperCase(), customer.getDocNumber());
        // Sprint 4 - verificacion adicional
        //if (id == null) {
        //	id = getCustomerId(customer);
        //}
        customer.setId(id);

        Random rand = new SecureRandom();

        customer.setSex(rand.nextInt(1)==0 ? "M": "F");

        String estadoCivilList = "SCVD";
        customer.setMaritalstatus(String.valueOf(estadoCivilList.charAt(rand.nextInt(estadoCivilList.length()))));


        //DataBinder dataBinder = new DataBinder(customer);
        //dataBinder.setValidator(new CustomerValidator());
        //dataBinder.validate();

        //BindingResult bindingResult = dataBinder.getBindingResult();
        //if (!bindingResult.hasErrors()) {
        String existe = "";
        existe = customerRepository.getExiste(customer.getDocNumber());

        if(customer.getFirstName() == null || ((customer.getCustomerPhone() == null   || customer.getCustomerPhone().equalsIgnoreCase("")) && existe.equalsIgnoreCase("SI"))){
            return customer;
        }
        customerRepository.save(customer);
        //} else {
        //     throw new DataValidationException(bindingResult);
        //}
        return customer;
    }

    // Sprint 4 - en la bd mysql se utilizaba case insensitive a nivel de datos... en esta nueva bd postgresql no se tiene habilitada esta caracteristica
    // Se detecto que la aplicacion graba en la tabla CUSTOMER el tipo de documento "DNI" con mayusculas y otras veces con minusculas...
    // para evitar errores en las consultas se decide (por el momento) consultar con minusculas y mayusculas.
    private Long getCustomerId(Customer customer) {
        Long id = null;
        if ("dni".equals(customer.getDocType())) {
            //entonces buscamos dni en mayusculas
            id = customerRepository.loadIdByDocTypeAndDocNumber("DNI", customer.getDocNumber());
        } else if ("DNI".equals(customer.getDocType())) {
            //entonces buscamos en minusculas
            id = customerRepository.loadIdByDocTypeAndDocNumber("dni", customer.getDocNumber());
        }
        return id;
    }

    @Transactional
    public boolean customerExists(String docType, String docNumber) {
        Long count = customerRepository.countByDocTypeAndDocNumber(docType, docNumber);
        return count > 0;
    }

}
