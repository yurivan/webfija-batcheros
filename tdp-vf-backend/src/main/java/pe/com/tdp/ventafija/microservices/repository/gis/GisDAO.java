package pe.com.tdp.ventafija.microservices.repository.gis;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.gis.WSSIGFFTT_FFTT_AVEResult;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.util.ConvertCoorToMetros;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.gis.GisContingencia;
import pe.com.tdp.ventafija.microservices.domain.gis.GisUbigeo;
import pe.com.tdp.ventafija.microservices.domain.order.entity.TdpAgendamientoCupones;
import pe.com.tdp.ventafija.microservices.domain.product.OffersRequest2;
import pe.com.tdp.ventafija.microservices.domain.product.SvaResponseDataSva;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service
public class GisDAO {


    @Value("${tdp.maximo.metros.gis}")
    private Integer maxRetry;

    @Autowired
    private DataSource datasource;

    private static final Logger logger = LogManager.getLogger();

    /**
     * Se coloca datos de ubigeo (Departamento,Provincia,Distrito) y id_transacción
     * con el fin de hacer una inserción y busqueda específica.
     * @param departamento
     * @param provincia
     * @param distrito
     * @param id_transaccion
     */
    public void saveGisDireccion(String id_transaccion, String departamento,String provincia, String distrito, String x, String y){
        try (Connection con = datasource.getConnection()) {
                String sql = "INSERT INTO ibmx_a07e6d02edaf552.tdp_gis_contingencia " +
                        "(id_transaccion, departamento, provincia, distrito, flg_update, coor_x, coor_y, fecha_register ) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?,CURRENT_TIMESTAMP)";


            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));


                SimpleDateFormat formatPresentDay = new SimpleDateFormat("yyyy-MM-dd");


                PreparedStatement ps = con.prepareStatement(sql);

                ps.setString(1, id_transaccion);
                ps.setString(2, departamento);
                ps.setString(3, provincia);
                ps.setString(4, distrito);
            ps.setInt(5, 0);
            ps.setString(6, y);
            ps.setString(7, x);

                //ps.setString(6, sdf.format(new Date()) + "-05:00");

            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert tdp_gis_contingencia ubigeo", e);
        }

    }






    public void saveGisStateTecnology(String id_transaccion, WSSIGFFTT_FFTT_AVEResult request){
        try (Connection con = datasource.getConnection()) {
            String sql = "update ibmx_a07e6d02edaf552.tdp_gis_contingencia " +
                    "set flg_update = ?, cobre_tec = ?, cobre_sat = ?, cobre_trm = ?, cobre_blq_vta = ?, cobre_blq_trm = ?, coaxial_tec = ?," +
                    "coaxial_sen = ?, coaxial_trm = ?, hfc_tec = ?, hfc_vel = ?, gpon_tec = ?, gpon_vel = ?, gpon_trm = ?, xdsl_tec = ?, " +
                    "xdsl_sat = ?, xdsl_vel_tlf = ?, xdsl_vel_trm = ?, exc_cable_hfc = ?, exc_cable_aco = ?, edf_nocable_existe = ?," +
                    "edf_nocable_metros = ?, pst_apoyo =?, nap_tec = ?, s4g_tec = ?, est_base = ?, cir_digital = ?, ws_status = ?," +
                    "ws_error = ?, dth_tec = ?, dth_blq = ?, zar_tec = ?, zar_blq = ?, coor_x = ?, coor_y = ? where id_transaccion = ?";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setInt(1, 1);
            ps.setString(2, request.getCOBRE_TEC());
            ps.setString(3, request.getCOBRE_SAT());
            ps.setString(4, request.getCOBRE_TRM());
            ps.setString(5, request.getCOBRE_BLQ_VTA());
            ps.setString(6, request.getCOBRE_BLQ_TRM());
            ps.setString(7, request.getCOAXIAL_TEC());
            ps.setString(8, request.getCOAXIAL_SEN());
            ps.setString(9, request.getCOAXIAL_TRM());
            ps.setString(10, request.getHFC_TEC());

            ps.setString(11, request.getHFC_VEL());
            ps.setString(12, request.getGPON_TEC());
            ps.setString(13, request.getGPON_VEL());
            ps.setString(14, request.getGPON_TRM());
            ps.setString(15, request.getXDSL_TEC());
            ps.setString(16, request.getXDSL_SAT());
            ps.setString(17, request.getXDSL_VEL_TLF());
            ps.setString(18, request.getXDSL_VEL_TRM());
            ps.setString(19, request.getEXC_CABLE_HFC());
            ps.setString(20, request.getEXC_CABLE_ACO());
            ps.setString(21, request.getEDF_NOCABLE_EXISTE());
            ps.setString(22, request.getEDF_NOCABLE_METROS());
            ps.setString(23, request.getPST_APOYO());
            ps.setString(24, request.getNAP_TEC());
            ps.setString(25, request.getS4G_TEC());
            ps.setString(26, request.getEST_BASE());
            ps.setString(27, request.getCIR_DIGITAL());
            ps.setString(28, request.getWS_STATUS());
            ps.setString(29, request.getWS_ERROR());

            ps.setString(30, request.getDTH_TEC());
            ps.setString(31, request.getDTH_BLQ());
            ps.setString(32, request.getZAR_TEC());
            ps.setString(33, request.getZAR_BLQ());
            ps.setString(34, request.getCOOR_X());
            ps.setString(35, request.getCOOR_Y());

            ps.setString(36, id_transaccion);


            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert tdp_gis_contingencia status", e);
        }
    }


    public WSSIGFFTT_FFTT_AVEResult readGisforUbigeo(GisUbigeo gisUbigeo){
        WSSIGFFTT_FFTT_AVEResult response = new WSSIGFFTT_FFTT_AVEResult();
       // Double distancia = ConvertCoorToMetros.distance(-12.129054, -77.000581,-12.140342, -76.992779);
        //int xdistancia = distancia.intValue();

        //Limite máximo dinámico


        int maximoLimite = maxRetry;
        List<GisUbigeo> gisUbigeos = listaUbigeos();
        int numMayor = 0;
        String id_transa = "";


        for(int i=0; i<gisUbigeos.size();i++){
            double latitudList = Double.parseDouble(gisUbigeos.get(i).getLatitud());
            double longitudList = Double.parseDouble(gisUbigeos.get(i).getLongitud());

            double latitudCompare = Double.parseDouble(gisUbigeo.getLatitud());
            double longitudCompare = Double.parseDouble(gisUbigeo.getLongitud());

            Double distancia = ConvertCoorToMetros.distance(latitudList,longitudList,latitudCompare,longitudCompare);
            int xdistancia = distancia.intValue();
            if(xdistancia <=maximoLimite){
                gisUbigeos.get(i).setMetros(xdistancia);
            }
            if(numMayor<=gisUbigeos.get(i).getMetros()){
                numMayor = gisUbigeos.get(i).getMetros();
                id_transa = gisUbigeos.get(i).getId();
            }
        }




        //obtener número mayor

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.tdp_gis_contingencia " +
                     "where id_transaccion = " + "'" + id_transa + "'" + " limit 1";


            // PreparedStatement ps = con.prepareStatement(sql);

             //ps.setString(1, id_transaccion);'¿

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    WSSIGFFTT_FFTT_AVEResult obj = new WSSIGFFTT_FFTT_AVEResult();
                    obj.setCOBRE_TEC(rs.getString("cobre_tec"));
                    obj.setCOBRE_SAT(rs.getString("cobre_sat"));
                    obj.setCOBRE_TRM(rs.getString("cobre_trm"));
                    obj.setCOBRE_BLQ_VTA(rs.getString("cobre_blq_vta"));
                    obj.setCOBRE_BLQ_TRM(rs.getString("cobre_blq_trm"));
                    obj.setCOAXIAL_TEC(rs.getString("coaxial_tec"));
                    obj.setCOAXIAL_SEN(rs.getString("coaxial_sen"));
                    obj.setCOAXIAL_TRM(rs.getString("coaxial_trm"));
                    obj.setHFC_TEC(rs.getString("hfc_tec"));
                    obj.setHFC_VEL(rs.getString("hfc_vel"));
                    obj.setGPON_TEC(rs.getString("gpon_tec"));
                    obj.setGPON_VEL(rs.getString("gpon_vel"));
                    obj.setGPON_TRM(rs.getString("gpon_trm"));
                    obj.setXDSL_TEC(rs.getString("xdsl_tec"));
                    obj.setXDSL_SAT(rs.getString("xdsl_sat"));
                    obj.setXDSL_VEL_TLF(rs.getString("xdsl_vel_tlf"));
                    obj.setXDSL_VEL_TRM(rs.getString("xdsl_vel_trm"));
                    obj.setEXC_CABLE_HFC(rs.getString("exc_cable_hfc"));
                    obj.setEXC_CABLE_ACO(rs.getString("exc_cable_aco"));
                    obj.setEDF_NOCABLE_EXISTE(rs.getString("edf_nocable_existe"));
                    obj.setEDF_NOCABLE_METROS(rs.getString("edf_nocable_metros"));
                    obj.setPST_APOYO(rs.getString("pst_apoyo"));
                    obj.setNAP_TEC(rs.getString("nap_tec"));
                    obj.setS4G_TEC(rs.getString("s4g_tec"));
                    obj.setEST_BASE(rs.getString("est_base"));
                    obj.setCIR_DIGITAL(rs.getString("cir_digital"));
                    obj.setWS_STATUS(rs.getString("ws_status"));
                    obj.setWS_ERROR(rs.getString("ws_error"));
                    obj.setDTH_TEC(rs.getString("dth_tec"));
                    obj.setDTH_BLQ(rs.getString("dth_blq"));
                    obj.setZAR_TEC(rs.getString("zar_tec"));
                    obj.setZAR_BLQ(rs.getString("zar_blq"));
                    obj.setCOOR_X(rs.getString("coor_x"));
                    obj.setCOOR_Y(rs.getString("coor_y"));


                    response =obj;

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert tdp_gis_contingencia ubigeo", e);
        }

        return response;

    }



    public List<GisUbigeo> listaUbigeos(){
        List<GisUbigeo> response = new ArrayList<>();

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.tdp_gis_contingencia " +
                    "where flg_update = '1' and fecha_register > (now() - INTERVAL '7 day')  limit 200";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    GisUbigeo obj = new GisUbigeo();
                    obj.setId(rs.getString("id_transaccion"));
                    obj.setLongitud(rs.getString("coor_x"));
                    obj.setLatitud(rs.getString("coor_y"));

                    response.add(obj);

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert tdp_gis_contingencia ubigeo", e);
        }

        return response;

    }

    public void saveContingencia(String id_transaccion, int flag){
        try (Connection con = datasource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.contingencia_gis " +
                    "(id_transaccion, flag_offline) "
                    + "VALUES (?, ?)";


            PreparedStatement ps = con.prepareStatement(sql);

            ps.setString(1, id_transaccion);
            ps.setInt(2, flag);


            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert contingencia_gis ubigeo", e);
        }

    }



    public Boolean consultarContingencia(String id){
        Boolean response = false;

        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.contingencia_gis " +
                    "where id_transaccion = " + "'" + id + "'" + "" +" limit 1";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {

                    GisContingencia obj = new GisContingencia();
                    obj.setId_transaccion(rs.getString("id_transaccion"));
                    obj.setFlag_offline(rs.getInt("flag_offline"));
                    if(obj.getFlag_offline()==1){
                        response = true;
                    }else{
                        response = false;
                    }

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert tdp_gis_contingencia ubigeo", e);
        }

        return response;

    }



    public WSSIGFFTT_FFTT_AVEResult readGisTecnologíaDefault(){

        WSSIGFFTT_FFTT_AVEResult response = new WSSIGFFTT_FFTT_AVEResult();
        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.tdp_gis_contingencia " +
                    "where tecnology_default = '1' limit 1";


            // PreparedStatement ps = con.prepareStatement(sql);

            //ps.setString(1, id_transaccion);'

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    WSSIGFFTT_FFTT_AVEResult obj = new WSSIGFFTT_FFTT_AVEResult();
                    obj.setCOBRE_TEC(rs.getString("cobre_tec"));
                    obj.setCOBRE_SAT(rs.getString("cobre_sat"));
                    obj.setCOBRE_TRM(rs.getString("cobre_trm"));
                    obj.setCOBRE_BLQ_VTA(rs.getString("cobre_blq_vta"));
                    obj.setCOBRE_BLQ_TRM(rs.getString("cobre_blq_trm"));
                    obj.setCOAXIAL_TEC(rs.getString("coaxial_tec"));
                    obj.setCOAXIAL_SEN(rs.getString("coaxial_sen"));
                    obj.setCOAXIAL_TRM(rs.getString("coaxial_trm"));
                    obj.setHFC_TEC(rs.getString("hfc_tec"));
                    obj.setHFC_VEL(rs.getString("hfc_vel"));
                    obj.setGPON_TEC(rs.getString("gpon_tec"));
                    obj.setGPON_VEL(rs.getString("gpon_vel"));
                    obj.setGPON_TRM(rs.getString("gpon_trm"));
                    obj.setXDSL_TEC(rs.getString("xdsl_tec"));
                    obj.setXDSL_SAT(rs.getString("xdsl_sat"));
                    obj.setXDSL_VEL_TLF(rs.getString("xdsl_vel_tlf"));
                    obj.setXDSL_VEL_TRM(rs.getString("xdsl_vel_trm"));
                    obj.setEXC_CABLE_HFC(rs.getString("exc_cable_hfc"));
                    obj.setEXC_CABLE_ACO(rs.getString("exc_cable_aco"));
                    obj.setEDF_NOCABLE_EXISTE(rs.getString("edf_nocable_existe"));
                    obj.setEDF_NOCABLE_METROS(rs.getString("edf_nocable_metros"));
                    obj.setPST_APOYO(rs.getString("pst_apoyo"));
                    obj.setNAP_TEC(rs.getString("nap_tec"));
                    obj.setS4G_TEC(rs.getString("s4g_tec"));
                    obj.setEST_BASE(rs.getString("est_base"));
                    obj.setCIR_DIGITAL(rs.getString("cir_digital"));
                    obj.setWS_STATUS(rs.getString("ws_status"));
                    obj.setWS_ERROR(rs.getString("ws_error"));
                    obj.setDTH_TEC(rs.getString("dth_tec"));
                    obj.setDTH_BLQ(rs.getString("dth_blq"));
                    obj.setZAR_TEC(rs.getString("zar_tec"));
                    obj.setZAR_BLQ(rs.getString("zar_blq"));
                    obj.setCOOR_X(rs.getString("coor_x"));
                    obj.setCOOR_Y(rs.getString("coor_y"));


                    response =obj;

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert tdp_gis_contingencia ubigeo", e);
        }

        return response;

    }



    public GisUbigeo readGisforUbigeoFilter(String orderId){
        GisUbigeo response = new GisUbigeo();
        try (Connection con = datasource.getConnection()) {
            String sql = "SELECT * from ibmx_a07e6d02edaf552.tdp_gis_contingencia " +
                    "where id_transaccion = " + "'" + orderId + "'" + "" +" limit 1";


            // PreparedStatement ps = con.prepareStatement(sql);

            //ps.setString(1, id_transaccion);'¿

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    GisUbigeo obj = new GisUbigeo();
                    obj.setDepartamento(rs.getString("departamento"));
                    obj.setProvincia(rs.getString("provincia"));
                    obj.setDistrito(rs.getString("distrito"));
                    obj.setLatitud(rs.getString("coor_x"));
                    obj.setLongitud(rs.getString("coor_y"));

                    response =obj;

                }

            }

            //ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error insert tdp_gis_contingencia ubigeo", e);
        }

        return response;

    }

}









/*

     Drop Table ibmx_a07e6d02edaf552.tdp_gis_contingencia;
     CREATE TABLE ibmx_a07e6d02edaf552.tdp_gis_contingencia(
     id SERIAL primary key,
     id_transaccion varchar(250) not null,
     departamento varchar not null,
     provincia varchar not null,
     distrito varchar not null
     );

  */