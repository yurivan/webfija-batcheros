package pe.com.tdp.ventafija.microservices.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.domain.automatizador.entity.AutomatizadorSaleService;

@Repository
public interface AutomatizadorRequestSaleService extends JpaRepository<AutomatizadorSaleService, Integer> {

    AutomatizadorSaleService findByCodvtappcd(String codvtappcd);

}