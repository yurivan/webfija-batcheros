package pe.com.tdp.ventafija.microservices.domain.order;

import java.util.Date;

public class VentaAudioRequest {
	private String nombrePuntoVenta;//dato del punto de venta
	private String entidad;//dato del punto de venta
	private String codigoEstado;
	private String codigoVendedor;
	private String buscarPorVendedor;
	private String dniCliente;
	private Date fechaInicio;
	private Date fechaFin;
	private String filtroRestringeHoras;//Sprint 28 Restrincion de Hora de Audios
	
	public String getNombrePuntoVenta() {
		return nombrePuntoVenta;
	}
	
	public void setNombrePuntoVenta(String nombrePuntoVenta) {
		this.nombrePuntoVenta = nombrePuntoVenta;
	}
	
	public String getEntidad() {
		return entidad;
	}
	
	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}
	
	public String getCodigoEstado() {
		return codigoEstado;
	}
	public void setCodigoEstado(String codigoEstado) {
		this.codigoEstado = codigoEstado;
	}
	public String getCodigoVendedor() {
		return codigoVendedor;
	}
	public void setCodigoVendedor(String codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}
	public String getDniCliente() {
		return dniCliente;
	}
	public void setDniCliente(String dniCliente) {
		this.dniCliente = dniCliente;
	}
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}


	public String getBuscarPorVendedor() {
		return buscarPorVendedor;
	}

	public void setBuscarPorVendedor(String buscarPorVendedor) {
		this.buscarPorVendedor = buscarPorVendedor;
	}

	//Sprint 28 Restringe Hora de Audios

	public String getFiltroRestringeHoras() {
		return filtroRestringeHoras;
	}

	public void setFiltroRestringeHoras(String filtroRestringeHoras) {
		this.filtroRestringeHoras = filtroRestringeHoras;
	}
}

