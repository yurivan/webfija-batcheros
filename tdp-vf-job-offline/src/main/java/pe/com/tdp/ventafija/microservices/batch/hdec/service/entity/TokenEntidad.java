package pe.com.tdp.ventafija.microservices.batch.hdec.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_token_entidad", schema = "ibmx_a07e6d02edaf552")
public class TokenEntidad {

    @Id
    @Column(name = "entidad") //=> ID
    private String entidad ;

    @Column(name = "token") //=> ID
    private String token ;

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
