package pe.com.tdp.ventafija.microservices.batch.hdec.bean;

/**
 * Bean que representa una Venta y su estado de envio al BACK (tambien conocido como HDEC)
 * @author      Valdemar
 * @since       1.5
 */
public class VentaLegado {

    private String id;
    private String estado;
    private String codigoPedido;
    private String estadoLegado;
    private String descLegado;
    private String type;
    private String motivoCaida;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEstadoLegado() {
        return estadoLegado;
    }

    public void setEstadoLegado(String estadoLegado) {
        this.estadoLegado = estadoLegado;
    }

    public String getDescLegado() {
        return descLegado;
    }

    public void setDescLegado(String descLegado) {
        this.descLegado = descLegado;
    }

    public String getMotivoCaida() {
        return motivoCaida;
    }

    public void setMotivoCaida(String motivoCaida) {
        this.motivoCaida = motivoCaida;
    }
}
