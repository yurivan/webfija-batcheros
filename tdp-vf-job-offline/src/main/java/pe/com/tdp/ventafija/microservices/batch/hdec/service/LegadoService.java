package pe.com.tdp.ventafija.microservices.batch.hdec.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.batch.config.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.Parameters;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.TdpServicePedido;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.ParametersRepository;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.TdpServicePedidoRepository;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadoatis.ConsultaEstadoAtisClient;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadoatis.DuplicateApiAtisRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadoatis.DuplicateApiAtisResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadocms.ConsultaEstadoCmsClient;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadocms.DuplicateApiCmsRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadocms.DuplicateApiCmsResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.constant.LegadosConstants;
import pe.com.tdp.ventafija.microservices.common.constant.ParametersConstants;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.Constants;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Date;
import java.util.List;

@Service
public class LegadoService {

    private static final Logger logger = LogManager.getLogger(LegadoService.class);
    private DateTimeZone dtZone = DateTimeZone.forID("America/Lima");

    @Autowired
    ParametersRepository parametersRepository;
    @Autowired
    TdpServicePedidoRepository tdpServicePedidoRepository;
    @Autowired
    private ApiHeaderConfig apiHeaderConfig;
    @Autowired
    private DataSource dataSource;

    public TdpServicePedido getEstadoAtis(String codigoPedido) {
        TdpServicePedido tdpServicePedidos = new TdpServicePedido();

        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        try {

            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_ATIS);
            tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

                try {
                    DuplicateApiAtisRequestBody apiRequestBody = new DuplicateApiAtisRequestBody();
                    apiRequestBody.setPeticion(codigoPedido);
                    ApiResponse<DuplicateApiAtisResponseBody> apiResponse = getConsultaEstadoAtis(apiRequestBody);
                    if ((Constants.RESPONSE).equals(apiResponse.getHeaderOut().getMsgType())) {
                        if (apiResponse.getBodyOut().getEstadoPeticion() != null) {
                            if (tdpServicePedidos == null) {
                                tdpServicePedidos = new TdpServicePedido();
                                tdpServicePedidos.setAuditoriaCreate(dateHoy);
                            }
                            tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_ATIS);
                            tdpServicePedidos.setPedidoCodigo(codigoPedido);
                            tdpServicePedidos.setPedidoEstadoCode(apiResponse.getBodyOut().getEstadoPeticion());
                            tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(apiResponse.getBodyOut().getEstadoPeticion(), translateList));

                            tdpServicePedidos.setAuditoriaModify(dateHoy);

                            tdpServicePedidoRepository.save(tdpServicePedidos);
                        }
                    }
                } catch (Exception apiconnect) {
                    logger.error("Error obtenerEstadoAtis Service. apiconnect.", apiconnect);
                }

        } catch (Exception ex) {
            logger.error("Error obtenerEstadoAtis Service.", ex);
        }

        return tdpServicePedidos;
    }

    public TdpServicePedido getEstadoCms(String codigoPedido) {
        TdpServicePedido tdpServicePedidos = new TdpServicePedido();

        boolean consultar = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        try {

            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_CMSR);
            tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

                try {
                    DuplicateApiCmsRequestBody apiRequestBody = new DuplicateApiCmsRequestBody();
                    apiRequestBody.setCodigoRequerimiento(codigoPedido);
                    ApiResponse<DuplicateApiCmsResponseBody> objCms = getConsultaEstadoCms(apiRequestBody);
                    if ((Constants.RESPONSE).equals(objCms.getHeaderOut().getMsgType())) {
                        if (objCms.getBodyOut().getEstadoRequerimiento() != null) {
                            if (tdpServicePedidos == null) {
                                tdpServicePedidos = new TdpServicePedido();
                                tdpServicePedidos.setAuditoriaCreate(dateHoy);
                            }
                            tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_CMS);
                            tdpServicePedidos.setPedidoCodigo(codigoPedido);
                            tdpServicePedidos.setPedidoEstadoCode(objCms.getBodyOut().getEstadoRequerimiento());
                            tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(objCms.getBodyOut().getEstadoRequerimiento(), translateList));

                            tdpServicePedidos.setAuditoriaModify(dateHoy);

                            tdpServicePedidoRepository.save(tdpServicePedidos);
                        }
                    }
                } catch (Exception apiconnect) {
                    logger.error("Error obtenerEstadoCms Service. apiconnect.", apiconnect);
                }
        } catch (Exception ex) {
            logger.error("Error obtenerEstadoCms Service.", ex);
        }

        return tdpServicePedidos;
    }


    private String getDescriptionByElementScoring(String element, List<Parameters> cmssParameterList) {
        String description = "SIN VALOR";

        for (Parameters parameter : cmssParameterList) {
            if (element.equals(parameter.getElement())) {
                description = parameter.getStrValue();
            }
        }

        return description;
    }


    private ApiResponse<DuplicateApiAtisResponseBody> getConsultaEstadoAtis(DuplicateApiAtisRequestBody apiRequestBody) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getAtisConsultaEstadoUri())
                .setApiId(apiHeaderConfig.getAtisConsultarEstadoApiId())
                .setApiSecret(apiHeaderConfig.getAtisConsultarEstadoApiSecret())
                .setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_STATUS_ATIS)
                .setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_ATIS)
                .build();

        ConsultaEstadoAtisClient consultaEstadoAtisClient = new ConsultaEstadoAtisClient(config);
        ClientResult<ApiResponse<DuplicateApiAtisResponseBody>> result = consultaEstadoAtisClient.post(apiRequestBody);
        registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

    private ApiResponse<DuplicateApiCmsResponseBody> getConsultaEstadoCms(DuplicateApiCmsRequestBody apiRequestBody) throws
            ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getCmsConsultaEstadoUri())
                .setApiId(apiHeaderConfig.getCmsConsultarEstadoApiId())
                .setApiSecret(apiHeaderConfig.getCmsConsultarEstadoApiSecret())
                .setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_STATUS_CMS)
                .setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_CMS)
                .build();

        ConsultaEstadoCmsClient consultaEstadoCmsClient = new ConsultaEstadoCmsClient(config);
        ClientResult<ApiResponse<DuplicateApiCmsResponseBody>> result = consultaEstadoCmsClient.post(apiRequestBody);
        registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }


    public void registerEvent(ServiceCallEvent event) {
        try (Connection con = dataSource.getConnection()) {
            String sql = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events (event_datetime, tag, username, msg,"
                    + "orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp,"
                    + "sourceAppVersion, result) "
                    + "VALUES (CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "SERVICE_CALL");
            ps.setString(2, event.getUsername());
            ps.setString(3, event.getMsg());
            ps.setString(4, event.getOrderId());
            ps.setString(5, event.getDocNumber());
            ps.setString(6, "BATCHEROESTADOS");
            ps.setString(7, event.getServiceUrl());
            ps.setString(8, event.getServiceRequest());
            ps.setString(9, event.getServiceResponse());
            ps.setString(10, event.getSourceApp());
            ps.setString(11, event.getSourceAppVersion());
            ps.setString(12, event.getResult());

            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error al registrar evento de llamada a API", e);
        }
    }
}
