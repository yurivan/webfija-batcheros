package pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaAutomatizador;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaLegado;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper del Bean Venta.
 *
 * @author glazaror
 * @since 1.5
 */
public class VentaRowLegadoMapper implements RowMapper<VentaLegado> {

    @Override
    public VentaLegado mapRow(ResultSet rs, int rowNum) throws SQLException {
        VentaLegado ventaLegado = new VentaLegado();
        ventaLegado.setCodigoPedido(rs.getString("CODIGO_PEDIDO"));
        ventaLegado.setEstado(rs.getString("ESTADO_SOLICITUD"));
        ventaLegado.setId(rs.getString("ID_VISOR"));
        ventaLegado.setType(rs.getString("SERVICETYPE"));
        ventaLegado.setMotivoCaida(rs.getString("MOTIVO_ESTADO"));
        return ventaLegado;
    }

}
