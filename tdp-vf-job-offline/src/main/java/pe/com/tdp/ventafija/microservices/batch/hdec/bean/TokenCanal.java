package pe.com.tdp.ventafija.microservices.batch.hdec.bean;

/**
 * Bean que representa una Venta y su estado de envio al BACK (tambien conocido como HDEC)
 * @author      Valdemar
 * @since       1.5
 */
public class TokenCanal {

    private String entidad;
    private String token;

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
