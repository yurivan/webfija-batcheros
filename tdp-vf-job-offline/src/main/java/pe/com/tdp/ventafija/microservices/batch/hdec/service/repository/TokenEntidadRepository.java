package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.LogAutomatizador;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.TokenEntidad;

@Repository
public interface TokenEntidadRepository extends JpaRepository<TokenEntidad, String> {

}