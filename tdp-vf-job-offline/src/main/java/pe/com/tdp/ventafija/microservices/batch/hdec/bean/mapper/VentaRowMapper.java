package pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaAutomatizador;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper del Bean Venta.
 *
 * @author glazaror
 * @since 1.5
 */
public class VentaRowMapper implements RowMapper<VentaAutomatizador> {

    @Override
    public VentaAutomatizador mapRow(ResultSet rs, int rowNum) throws SQLException {
        VentaAutomatizador ventaAutomatizador = new VentaAutomatizador();

        ventaAutomatizador.setId(rs.getString("ID_VISOR"));
        ventaAutomatizador.setEstado(rs.getString("ESTADO_SOLICITUD"));
        ventaAutomatizador.setCodigoPedido(rs.getString("CODIGO_PEDIDO"));
        ventaAutomatizador.setAuto_retry(rs.getInt("AUTO_RETRY"));

        return ventaAutomatizador;
    }

}
