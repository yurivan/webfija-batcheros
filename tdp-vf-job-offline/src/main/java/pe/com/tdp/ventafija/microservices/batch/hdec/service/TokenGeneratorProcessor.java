package pe.com.tdp.ventafija.microservices.batch.hdec.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.TokenCanal;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaLegado;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper.VentaRowLegadoMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper.VentaRowTokenMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.TdpServicePedido;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.TokenEntidad;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.repository.TokenEntidadRepository;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextImpl;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;

@Service
public class TokenGeneratorProcessor {

    private static final Logger logger = LogManager.getLogger(TokenGeneratorProcessor.class);

    @Autowired
    private DataSource dataSource;

    @Autowired
    TokenGeneratorService tokenGeneratorService;

    @Autowired
    TokenEntidadRepository tokenEntidadRepository;

    public void process2() throws Exception{

        JdbcCursorItemReader<TokenCanal> ventasReader = ventasReader();

        ventasReader.open(new ExecutionContext());
        TokenCanal ventaBean;

        while ((ventaBean = ventasReader.read())!=null) {
            startCallContext(ventaBean);
            LogSystem.info(logger, "TokenGeneratorProcessor", "process", "Start");
            String token = tokenGeneratorService.generateUniqueToken();
            TokenEntidad tokenentidad = tokenEntidadRepository.findOne(ventaBean.getEntidad());
            if (tokenentidad==null){
                tokenentidad = new TokenEntidad();
                tokenentidad.setEntidad(ventaBean.getEntidad());
                tokenentidad.setToken(token);
                insertarToken(tokenentidad);
            }else{
                tokenentidad.setToken(token);
                actualizarToken(tokenentidad);
            }
            LogSystem.infoObject(logger, "TokenGeneratorProcessor", "process", "VentaBean", tokenentidad);

        }
        ventasReader.close();

    }

    public JdbcCursorItemReader<TokenCanal> ventasReader() {

        String sql = "SELECT DISTINCT ENTIDAD " +
        "FROM ibmx_a07e6d02edaf552.TDP_SALES_AGENT AGENT ";

        LogSystem.infoQuery(logger, "BatchConfig", "ventasReader", "sql", sql);

        JdbcCursorItemReader<TokenCanal> reader = new JdbcCursorItemReader<>();
        reader.setDataSource(dataSource);
        reader.setSql(sql);
        reader.setRowMapper(new VentaRowTokenMapper());
        reader.setFetchSize(1);
        return reader;
    }


    public void startCallContext(TokenCanal ventaBean) {

        ServiceCallEvent event = new ServiceCallEvent();
        event.setOrderId(ventaBean.getEntidad());
        event.setServiceCode("TOKEN");
        event.setSourceApp("BATCH");
        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);
    }


    public void insertarToken(TokenEntidad ventaBean) {

        try (Connection con = dataSource.getConnection()) {

            String sql = "INSERT INTO ibmx_a07e6d02edaf552.tdp_token_entidad(entidad,token) VALUES " +
                    "('"+ventaBean.getEntidad()+"','"+ventaBean.getToken()+"')";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.execute();
        } catch (Exception e) {
            logger.error("Error al insertar el token", e);
        }
    }
    public void actualizarToken(TokenEntidad ventaBean) {

        try (Connection con = dataSource.getConnection()) {

            String sql = "UPDATE ibmx_a07e6d02edaf552.tdp_token_entidad SET " +
                    "token = '"+ventaBean.getToken()+"' WHERE entidad = '"+ventaBean.getEntidad()+"'";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error al actualizar el token", e);
        }
    }

}
