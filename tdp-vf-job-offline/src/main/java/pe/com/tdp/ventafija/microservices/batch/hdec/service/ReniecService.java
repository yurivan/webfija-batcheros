package pe.com.tdp.ventafija.microservices.batch.hdec.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.reniec.IdentifyApiReniecRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.reniec.IdentifyApiReniecResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.reniec.ReniecClient;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyResponseData;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Service
public class ReniecService {

    private static final Logger logger = LogManager.getLogger(ReniecService.class);

    private String reniecConsultaClienteUri;
    private String reniecConsultarClienteApiId;
    private String reniecConsultarClienteApiSecret;

    @Autowired
    private DataSource dataSource;

    @Autowired
    public ReniecService() {
        super();
    }

    public void setCredentials(String reniecConsultaClienteUri,
                               String reniecConsultarClienteApiId,
                               String reniecConsultarClienteApiSecret){
        this.reniecConsultaClienteUri=reniecConsultaClienteUri;
        this.reniecConsultarClienteApiId=reniecConsultarClienteApiId;
        this.reniecConsultarClienteApiSecret=reniecConsultarClienteApiSecret;
    }

    public Response<IdentifyResponseData> identify(IdentifyRequest request) {

        Response<IdentifyResponseData> response = new Response<>();
        try {
            /* Validar RUC 10 */
            if (request.getDocumentNumber() != null && request.getDocumentNumber().length() > 8) {
                request.setDocumentNumber(request.getDocumentNumber().substring(2, 10));
            }

            IdentifyApiReniecRequestBody apiReniecRequestBody = new IdentifyApiReniecRequestBody();
            apiReniecRequestBody.setNumDNI(request.getDocumentNumber());

            ApiResponse<IdentifyApiReniecResponseBody> objReniec = getReniec(apiReniecRequestBody, request.getId());

            if ((Constants.RESPONSE).equals(objReniec.getHeaderOut().getMsgType())) {
                IdentifyResponseData responseData = new IdentifyResponseData();
                responseData.setLastName(objReniec.getBodyOut().getApePaterno());
                responseData.setMotherLastName(objReniec.getBodyOut().getApeMaterno() == null ? "" : objReniec.getBodyOut().getApeMaterno());
                responseData.setNames(objReniec.getBodyOut().getNombres());
                responseData.setDepartmentBirthLocation(objReniec.getBodyOut().getNacUbiDepartamento());
                responseData.setProvinceBirthLocation(objReniec.getBodyOut().getNacUbiProvincia());
                responseData.setDistrictBirthLocation(objReniec.getBodyOut().getNacUbiDistrito());
                responseData.setBirthdate(objReniec.getBodyOut().getFecNacimiento());
                responseData.setFatherName(objReniec.getBodyOut().getNomPadre() == null ? "" : objReniec.getBodyOut().getNomPadre());
                responseData.setMotherName(objReniec.getBodyOut().getNomMadre() == null ? "" : objReniec.getBodyOut().getNomMadre());
                responseData.setExpeditionDate(objReniec.getBodyOut().getFecExpedicion());
                responseData.setRestriction(objReniec.getBodyOut().getRestriccion());
                response.setResponseCode("0");
                response.setResponseData(responseData);
            }else{
                if (objReniec.getBodyOut().getRestriccion().equals("FALLECIMIENTO")){
                    response.setResponseCode("-2");
                }else{
                    response.setResponseCode("-1");
                }
            }
        }  catch (Exception e) {
            response.setResponseCode("-1");
            logger.error("Error Identify Service", e);
            logger.info("Fin Identify Service");
            return response;
        }

        logger.info("Fin Identify Service");
        return response;
    }

    private ApiResponse<IdentifyApiReniecResponseBody> getReniec(IdentifyApiReniecRequestBody apiRequestBody, String orderId) throws ClientException {

        System.out.println(reniecConsultaClienteUri);
        System.out.println(reniecConsultarClienteApiId);
        System.out.println(reniecConsultarClienteApiSecret);
        System.out.println(Constants.API_REQUEST_HEADER_OPERATION_RENIEC);
        System.out.println(Constants.API_REQUEST_HEADER_DESTINATION_RENIEC);
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(reniecConsultaClienteUri)
                .setApiId(reniecConsultarClienteApiId)
                .setApiSecret(reniecConsultarClienteApiSecret)
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_RENIEC)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_RENIEC)
                .build();

        ReniecClient reniecClient = new ReniecClient(config);
        ClientResult<ApiResponse<IdentifyApiReniecResponseBody>> result = reniecClient.post(apiRequestBody);

        if ((Constants.RESPONSE).equals(result.getResult().getHeaderOut().getMsgType())) {
            if (result.getResult().getBodyOut().getRestriccion() != null && result.getResult().getBodyOut().getRestriccion().equalsIgnoreCase("A")) {
                result.getResult().getHeaderOut().setMsgType("ERROR");
                result.getResult().getBodyOut().setRestriccion("FALLECIMIENTO");
                result.getEvent().setServiceResponse(result.getEvent().getServiceResponse().replace("\"restriccion\" : \"A\"", "\"restriccion\" : \"FALLECIMIENTO\""));
            }
        }

        registerEvent(result.getEvent());

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
            result.getEvent().setOrderId(orderId);
        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }


    public void registerEvent(ServiceCallEvent event) {
        try (Connection con = dataSource.getConnection()) {

            String sql = "INSERT INTO ibmx_a07e6d02edaf552.service_call_events (event_datetime, tag, username, msg,"
                    + "orderId, docNumber, serviceCode, serviceUrl, serviceRequest, serviceResponse, sourceApp,"
                    + "sourceAppVersion, result) "
                    + "VALUES (CURRENT_TIMESTAMP,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "SERVICE_CALL");
            ps.setString(2, event.getUsername());
            ps.setString(3, event.getMsg());
            ps.setString(4, event.getOrderId());
            ps.setString(5, event.getDocNumber());
            ps.setString(6,"OFFLINE_RENIEC");
            ps.setString(7, event.getServiceUrl());
            ps.setString(8, event.getServiceRequest());
            ps.setString(9, event.getServiceResponse());
            ps.setString(10, event.getSourceApp());
            ps.setString(11, event.getSourceAppVersion());
            ps.setString(12, event.getResult());

            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("algo anda mal");
        }
    }
}