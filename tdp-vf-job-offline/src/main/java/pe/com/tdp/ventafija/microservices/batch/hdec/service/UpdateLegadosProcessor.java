package pe.com.tdp.ventafija.microservices.batch.hdec.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaLegado;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper.VentaRowLegadoMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.TdpServicePedido;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextImpl;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class UpdateLegadosProcessor {

    private static final Logger logger = LogManager.getLogger(UpdateLegadosProcessor.class);

    @Autowired
    private DataSource dataSource;

    @Autowired
    LegadoService legadoService;

    @Value("${legados.estados.quitarbolsa}")
    String quitarbolsa;

    @Value("${legados.estados.enproceso}")
    String enproceso;

    @Value("${legados.estados.instalado}")
    String instalado;

    @Value("${legados.estados.caida}")
    String caida;

    public void process2() throws Exception{

        JdbcCursorItemReader<VentaLegado> ventasReader = ventasReader();

        ventasReader.open(new ExecutionContext());
        VentaLegado ventaBean;

        String[] estadosCaida = caida.replace("'","").split(",");
        String[] estadosProceso = enproceso.replace("'","").split(",");
        String[] estadosInstalado = instalado.replace("'","").split(",");


        while ((ventaBean = ventasReader.read())!=null) {
            startCallContext(ventaBean);
            LogSystem.info(logger, "UpdateLegadosProcessor", "process", "Start");
            LogSystem.infoObject(logger, "UpdateLegadosProcessor", "process", "VentaBean", ventaBean);

            TdpServicePedido response;

            if(ventaBean.getType().equals("ATIS")){
                response = legadoService.getEstadoAtis(ventaBean.getCodigoPedido());
            }else{
                response = legadoService.getEstadoCms(ventaBean.getCodigoPedido());
            }
            if (response!=null){
                ventaBean.setEstadoLegado(response.getPedidoEstadoCode());
                ventaBean.setDescLegado(response.getPedidoEstado());
                boolean contains = Arrays.stream(estadosCaida).anyMatch(ventaBean.getEstadoLegado()::equals);
                if (contains){
                    ventaBean.setEstado("CAIDA");
                    ventaBean.setMotivoCaida("Cancelado en sistema "+ventaBean.getType());
                    updateEstadoSolicitud(ventaBean);

                }else{
                   /* boolean contains2 = Arrays.stream(estadosProceso).anyMatch(ventaBean.getEstadoLegado()::equals);
                    boolean contains3 = Arrays.stream(estadosInstalado).anyMatch(ventaBean.getEstadoLegado()::equals);
                    */
                    updateEstadoLegado(ventaBean);
                }

            }
            LogSystem.infoObject(logger, "UpdateLegadosProcessor", "process", "Response Legados", response);
        }
        ventasReader.close();
    }
/*
    boolean existsWoInit(List<ToaStatus> dataToa){
        for (ToaStatus status: dataToa){
            if(status.getStatus_toa().equals("IN_TOA")){
                return true;
            }
        }
        return false;
    }

    boolean exitisWoCompleted(List<ToaStatus> dataToa){
        for (ToaStatus status: dataToa){
            if(status.getStatus_toa().equals("WO_COMPLETED")){
                return true;
            }
        }
        return false;
    }
*/
    public JdbcCursorItemReader<VentaLegado> ventasReader() {

        String sql = "SELECT VIS.ID_VISOR, VIS.CODIGO_PEDIDO,VIS.ESTADO_SOLICITUD,ORD.SERVICETYPE,VIS.MOTIVO_ESTADO " +
                "FROM ibmx_a07e6d02edaf552.TDP_VISOR AS VIS " +
                "INNER JOIN ibmx_a07e6d02edaf552.ORDER AS ORD ON ORD.ID = VIS.ID_VISOR " +
                "WHERE VIS.ESTADO_SOLICITUD = 'INGRESADO' " +
                "AND VIS.CODIGO_PEDIDO <> '' " +
                "AND VIS.CODIGO_PEDIDO IS NOT NULL " +
                //"AND VIS.FECHA_GRABACION >  '2018-10-01 00:00:00' " +
                //"AND VIS.FECHA_GRABACION <  '2018-10-02 00:00:00' " +
                "AND (VIS.STATUSLEGADO IS NULL " +
                "OR VIS.STATUSLEGADO NOT IN ("+quitarbolsa+")"+
                ") " +
                "AND VIS.FECHA_GRABACION >  NOW() - INTERVAL '15 DAYS' " +
                //"AND VIS.ID_VISOR ='-Le5MOro3wjJJ9l0YdVU' " +
                "ORDER BY VIS.FECHA_GRABACION ASC ";
                //"ORDER BY RANDOM() " +
                //"LIMIT 10";

        System.out.println(sql);
        LogSystem.infoQuery(logger, "BatchConfig", "ventasReader", "sql", sql);

        JdbcCursorItemReader<VentaLegado> reader = new JdbcCursorItemReader<>();
        reader.setDataSource(dataSource);
        reader.setSql(sql);
        reader.setRowMapper(new VentaRowLegadoMapper());
        reader.setFetchSize(1);
        return reader;
    }


    public void startCallContext(VentaLegado ventaBean) {

        ServiceCallEvent event = new ServiceCallEvent();
        event.setOrderId(ventaBean.getId());
        event.setServiceCode("UPDATE");
        event.setSourceApp("BATCH");
        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);
    }

    public void updateEstadoSolicitud(VentaLegado ventaBean) {

        try (Connection con = dataSource.getConnection()) {

            String sql = "UPDATE ibmx_a07e6d02edaf552.tdp_visor " +
                    "set estado_solicitud='"+ventaBean.getEstado()+"', " +
                    "statuslegado='"+ventaBean.getEstadoLegado()+"', " +
                    "desclegado='"+ventaBean.getDescLegado()+"', " +
                    "motivo_estado='"+ventaBean.getMotivoCaida()+"' " +
                    "where id_visor = '"+ventaBean.getId()+"' ";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error al registrar evento de llamada a API", e);
        }
    }


    public void updateEstadoLegado(VentaLegado ventaBean) {

        try (Connection con = dataSource.getConnection()) {

            String sql = "UPDATE ibmx_a07e6d02edaf552.tdp_visor " +
                    "set statuslegado='"+ventaBean.getEstadoLegado()+"', " +
                    "desclegado='"+ventaBean.getDescLegado()+"' " +
                    "where id_visor = '"+ventaBean.getId()+"' ";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.executeUpdate();
        } catch (Exception e) {
            logger.error("Error al registrar evento de llamada a API", e);
        }

    }
}
