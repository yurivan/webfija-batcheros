package pe.com.tdp.ventafija.microservices.batch.offline.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.offline.service.entity.Parameters;
import pe.com.tdp.ventafija.microservices.batch.offline.service.entity.PeticionPagoEfectivo;

import java.util.List;

@Repository
public interface PeticionPagoEfectivoRepository extends JpaRepository<PeticionPagoEfectivo, Integer> {

	public PeticionPagoEfectivo findByOrderId(String orderId);


}