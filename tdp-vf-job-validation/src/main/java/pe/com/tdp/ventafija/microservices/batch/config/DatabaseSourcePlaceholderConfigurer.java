package pe.com.tdp.ventafija.microservices.batch.config;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

public class DatabaseSourcePlaceholderConfigurer extends PropertyPlaceholderConfigurer {

    private static final Logger logger = LogManager.getLogger(DatabaseSourcePlaceholderConfigurer.class);

    private static class Parameter {
        private String key;
        private String value;

        public Parameter(String key, String value) {
            super();
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        DataSource dataSource = (DataSource) beanFactory.getBean("dataSource");

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        Object search[] = {"CONFIG"};
        List<Parameter> parameters = jdbcTemplate.query("select element, strValue from ibmx_a07e6d02edaf552.parameters where domain = ?", search, new RowMapper<Parameter>() {
            @Override
            public Parameter mapRow(ResultSet rs, int rowNum) throws SQLException {
                Parameter p = new Parameter(rs.getString("element"), rs.getString("strValue"));
                return p;
            }
        });

        final Properties dbProps = new Properties();
        for (Parameter p : parameters) {
            dbProps.put(p.getKey(), p.getValue());
        }
        setProperties(dbProps);

        JSONObject objDBProps = new JSONObject(dbProps);
        System.out.println("---------------");
        logger.info("postProcessBeanFactory => dbProps: " + objDBProps.toString());
        System.out.println("---------------");

        super.postProcessBeanFactory(beanFactory);
    }

}
