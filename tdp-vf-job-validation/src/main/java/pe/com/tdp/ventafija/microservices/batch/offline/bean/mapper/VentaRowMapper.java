package pe.com.tdp.ventafija.microservices.batch.offline.bean.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.com.tdp.ventafija.microservices.batch.offline.bean.VentaBean;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Mapper del Bean Venta.
 *
 * @author glazaror
 * @since 1.5
 */
public class VentaRowMapper implements RowMapper<VentaBean> {

    @Override
    public VentaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        VentaBean ventaBean = new VentaBean();

        ventaBean.setId(rs.getString("ID_VISOR"));
        ventaBean.setFechaGrabacion(rs.getDate("FECHA_GRABACION"));
        ventaBean.setEstado(rs.getString("ESTADO_SOLICITUD"));
        ventaBean.setDniCliente(rs.getString("DNI"));
        ventaBean.setCodigoVendedor(rs.getString("COD_ATIS"));
        ventaBean.setEstadoAnterior(rs.getString("ESTADO_ANTERIOR"));
        ventaBean.setOffline(rs.getString("OFFLINE"));
        ventaBean.setMotivoCaida(rs.getString("MOTIVO_ESTADO"));

        return ventaBean;
    }

}
