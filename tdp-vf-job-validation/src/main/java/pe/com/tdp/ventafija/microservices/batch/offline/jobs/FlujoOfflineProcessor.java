package pe.com.tdp.ventafija.microservices.batch.offline.jobs;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import pe.com.tdp.ventafija.microservices.batch.offline.bean.VentaBean;
import pe.com.tdp.ventafija.microservices.batch.offline.service.OfflineService;
import pe.com.tdp.ventafija.microservices.batch.offline.service.ReniecService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContext;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextImpl;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

import java.util.HashMap;

/**
 * Implementacion Processor de Spring Batch para la contingencia HDEC.
 * Se evaluan todas las ventas que se encuentran en estado 'ENVIANDO' y que se encuentren en dicho estado
 * un tiempo considerado excesivo (configurado en base datos).
 * Cada venta es enviada nuevamente a HDEC, se manejan los siguientes escenarios:
 * <ul>
 * <li> Si es que la respuesta es SATISFACTORIA o DUPLICADO entonces la venta es actualizada a estado PENDIENTE.
 * <li> Si es que la respuesta es un ERROR entonces se volvera a intentar el envio controlado por un limite de reintentos.
 * <li> Si es que la respuesta es un ERROR y ya se llego al limite de reintentos entonces se actualiza la venta con estado 'CAIDA'.
 * </ul>
 *
 * @author glazaror
 * @since 1.5
 */
public class FlujoOfflineProcessor implements ItemProcessor<VentaBean, VentaBean> {

    private static final Logger logger = LogManager.getLogger(FlujoOfflineProcessor.class);

    private ReniecService reniecService;
    private OfflineService offlineService;

    public FlujoOfflineProcessor(ReniecService reniecService,OfflineService offlineService) {
        this.reniecService = reniecService;
        this.offlineService = offlineService;
    }

    @Override
    public VentaBean process(final VentaBean ventaBean) throws Exception {
        LogSystem.info(logger, "FlujoOfflineProcessor", "process", "Start");
        LogSystem.infoObject(logger, "FlujoOfflineProcessor", "process", "VentaBean", ventaBean);
        System.out.println("VENTA ID: "+ventaBean.getId()+"; OFFLINE: "+ventaBean.getOffline());
        startCallContext(ventaBean);
        int success;

        HashMap<String,String> dataTomaPedido = new HashMap<>();

        String[] body =  ventaBean.getOffline().split(";");
        for (int i = 0; i<body.length; i++){
            String[] par = body[i].split(":");
            dataTomaPedido.put(par[0].trim().toUpperCase(),par[1].trim().toUpperCase());
        }

        success=offlineService.validateOffline(ventaBean.getId(),dataTomaPedido);

        if (success==1){
            if (ventaBean.getEstadoAnterior()==null){
                if(dataTomaPedido.get("FLAG").indexOf('7')>-1){
                    ventaBean.setEstado("PENDIENTE_CIP");
                }else{
                    ventaBean.setEstado("ENVIANDO");
                }
            }else if (ventaBean.getEstadoAnterior().equals("PENDIENTE")){
                if(dataTomaPedido.get("FLAG").indexOf('7')>-1){
                    ventaBean.setEstado("PENDIENTE_CIP");
                }else {
                    ventaBean.setEstado("PENDIENTE");
                }
            }
        }else if(success<0){

            if (success==-1)
                ventaBean.setMotivoCaida("DATOS INCORRECTOS EN RUC");
            if (success==-6)
                ventaBean.setMotivoCaida("DATOS INCORRECTOS EN RENIEC");

            ventaBean.setEstado("CAIDA");
        }
        return ventaBean;
    }


    public void startCallContext(VentaBean ventaBean) {

        ServiceCallEvent event = new ServiceCallEvent();
        event.setUsername(ventaBean.getCodigoVendedor());
        event.setOrderId(ventaBean.getId());
        event.setServiceCode("OFFLINE");
        event.setDocNumber(ventaBean.getDniCliente());
        event.setSourceApp("BATCH");
        VentaFijaContext ctx = new VentaFijaContextImpl();
        ctx.setServiceCallEvent(event);
        VentaFijaContextHolder.setContext(ctx);
    }

    private void endCallContext() {
        VentaFijaContextHolder.clearContext();
    }

}