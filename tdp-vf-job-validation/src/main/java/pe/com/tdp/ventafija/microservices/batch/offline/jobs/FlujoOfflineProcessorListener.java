package pe.com.tdp.ventafija.microservices.batch.offline.jobs;

import org.springframework.batch.core.listener.ItemListenerSupport;
import pe.com.tdp.ventafija.microservices.batch.offline.bean.VentaBean;

/**
 * Implementacion ProcessorListener de Spring Batch para la contingencia HDEC.
 * Implementado para controlar errores.
 * @author      glazaror
 * @since       1.5
 */
public class FlujoOfflineProcessorListener extends ItemListenerSupport<VentaBean, VentaBean> {

    public FlujoOfflineProcessorListener() {
        super();
    }

    @Override
    public void onProcessError(VentaBean item, Exception e) {
        super.onProcessError(item, e);
    }

}
