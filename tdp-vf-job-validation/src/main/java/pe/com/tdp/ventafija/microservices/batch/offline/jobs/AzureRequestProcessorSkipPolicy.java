package pe.com.tdp.ventafija.microservices.batch.offline.jobs;

import org.springframework.batch.core.step.skip.SkipLimitExceededException;
import org.springframework.batch.core.step.skip.SkipPolicy;

public class AzureRequestProcessorSkipPolicy implements SkipPolicy {

	@Override
	public boolean shouldSkip(Throwable t, int skipCount) throws SkipLimitExceededException {
		if (t instanceof AzureRequestProcessorException) {
			return true;
		}
		return false;
	}

}
