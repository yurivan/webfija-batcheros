package pe.com.tdp.ventafija.microservices.batch.config;

import org.springframework.beans.factory.annotation.Value;

public class EmailConfig {

    @Value("${tdp.mail.emblue.authenticate.uri}")
    private String emailAutenticateUri;

    @Value("${tdp.mail.emblue.checkconexion.uri}")
    private String emailCheckConexionUri;

    @Value("${tdp.mail.emblue.sendexpress.uri}")
    private String emailSendExpressUri;

    @Value("${tdp.mail.emblue.user}")
    private String emailUser;

    @Value("${tdp.mail.emblue.password}")
    private String emailPassword;

    @Value("${tdp.mail.emblue.token}")
    private String emailToken;

    @Value("${tdp.mail.emblue.sendmailexpresss.actionId}")
    private String emailSendExpressActionId;

    public EmailConfig(String emailAutenticateUri,
                       String emailCheckConexionUri,
                       String emailSendExpressUri,
                       String emailUser,
                       String emailPassword,
                       String emailToken,
                       String emailSendExpressActionId){

        this.emailAutenticateUri = emailAutenticateUri;
        this.emailCheckConexionUri = emailCheckConexionUri;
        this.emailSendExpressUri = emailSendExpressUri;
        this.emailUser = emailUser;
        this.emailPassword = emailPassword;
        this.emailToken = emailToken;
        this.emailSendExpressActionId = emailSendExpressActionId;

    }

    public String getEmailAutenticateUri() {
        return emailAutenticateUri;
    }

    public void setEmailAutenticateUri(String emailAutenticateUri) {
        this.emailAutenticateUri = emailAutenticateUri;
    }

    public String getEmailCheckConexionUri() {
        return emailCheckConexionUri;
    }

    public void setEmailCheckConexionUri(String emailCheckConexionUri) {
        this.emailCheckConexionUri = emailCheckConexionUri;
    }

    public String getEmailSendExpressUri() {
        return emailSendExpressUri;
    }

    public void setEmailSendExpressUri(String emailSendExpressUri) {
        this.emailSendExpressUri = emailSendExpressUri;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public String getEmailPassword() {
        return emailPassword;
    }

    public void setEmailPassword(String emailPassword) {
        this.emailPassword = emailPassword;
    }

    public String getEmailToken() {
        return emailToken;
    }

    public void setEmailToken(String emailToken) {
        this.emailToken = emailToken;
    }

    public String getEmailSendExpressActionId() {
        return emailSendExpressActionId;
    }

    public void setEmailSendExpressActionId(String emailSendExpressActionId) {
        this.emailSendExpressActionId = emailSendExpressActionId;
    }
}
