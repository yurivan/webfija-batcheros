package pe.com.tdp.ventafija.microservices.batch.config;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.batch.core.ItemProcessListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean;
import org.springframework.batch.core.step.skip.SkipPolicy;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.support.transaction.ResourcelessTransactionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.SyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;
import pe.com.tdp.ventafija.microservices.batch.offline.ContingenciaProcessorScheduler;
import pe.com.tdp.ventafija.microservices.batch.offline.bean.VentaBean;
import pe.com.tdp.ventafija.microservices.batch.offline.bean.mapper.VentaRowMapper;
import pe.com.tdp.ventafija.microservices.batch.offline.jobs.AzureRequestProcessorSkipPolicy;
import pe.com.tdp.ventafija.microservices.batch.offline.jobs.FlujoOfflineProcessor;
import pe.com.tdp.ventafija.microservices.batch.offline.jobs.FlujoOfflineProcessorListener;
import pe.com.tdp.ventafija.microservices.batch.offline.service.OfflineService;
import pe.com.tdp.ventafija.microservices.batch.offline.service.ReniecService;
import pe.com.tdp.ventafija.microservices.batch.util.LogSystem;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing

public class BatchConfig {

    private static final Logger logger = LogManager.getLogger(BatchConfig.class);

    @Autowired
    private DataSource dataSource;
    @Autowired
    private JobBuilderFactory jobBuilderFactory;
    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    public ReniecService reniecService;

    @Autowired
    public OfflineService offlineService;


    @Bean
    public PlatformTransactionManager transactionManager() {
        return new ResourcelessTransactionManager();
    }

    @Bean
    public JobRepository jobRepository() throws Exception {
        return new MapJobRepositoryFactoryBean(transactionManager()).getObject();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        SyncTaskExecutor executor = new SyncTaskExecutor();
        return executor;
    }

    @Bean
    public SimpleJobLauncher jobLauncher() throws Exception {
        SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
        simpleJobLauncher.setJobRepository(jobRepository());
        simpleJobLauncher.setTaskExecutor(taskExecutor());
        return simpleJobLauncher;
    }

    @Bean
    public ContingenciaProcessorScheduler contingenciaProcessorScheduler() {
        return new ContingenciaProcessorScheduler();
    }

    @Bean
    public JdbcCursorItemReader<VentaBean> ventasReader() {
        String sql = "SELECT VIS.ID_VISOR, VIS.FECHA_GRABACION, VIS.ESTADO_SOLICITUD," +
                "VIS.DNI,VIS.ESTADO_ANTERIOR,ORD.OFFLINE , ORD.USERID AS COD_ATIS, VIS.MOTIVO_ESTADO " +
                "FROM ibmx_a07e6d02edaf552.TDP_VISOR AS VIS " +
                "INNER JOIN ibmx_a07e6d02edaf552.ORDER AS ORD ON ORD.ID = VIS.ID_VISOR " +
                "WHERE VIS.ESTADO_SOLICITUD = 'OFFLINE' " +
                "ORDER BY VIS.FECHA_GRABACION ASC " +
                "LIMIT 10";


        LogSystem.infoQuery(logger, "BatchConfig", "ventasReader", "sql", sql);

        JdbcCursorItemReader<VentaBean> reader = new JdbcCursorItemReader<VentaBean>();
        reader.setDataSource(dataSource);
        reader.setSql(sql);
        reader.setRowMapper(new VentaRowMapper());
        reader.setFetchSize(1);
        return reader;
    }

    @Bean
    public JdbcBatchItemWriter<VentaBean> ventasWriter() {
        JdbcBatchItemWriter<VentaBean> writer = new JdbcBatchItemWriter<VentaBean>();
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<VentaBean>());
        writer.setSql("UPDATE ibmx_a07e6d02edaf552.TDP_VISOR set estado_solicitud = :estado, motivo_estado = :motivoCaida where id_visor = :id AND estado_solicitud = 'OFFLINE'");
        writer.setDataSource(dataSource);
        return writer;
    }

    @Bean
    public FlujoOfflineProcessor ventasProcessor() {
        return new FlujoOfflineProcessor(reniecService,offlineService);
    }

    @Bean
    public Job hdecContingenciaProcessor(Step contingenciaHDECStep) throws Exception {
        return jobBuilderFactory.get("FlujoOfflineProcessor").incrementer(new RunIdIncrementer())
                .repository(jobRepository()).flow(contingenciaHDECStep).end().build();
    }

    @Bean
    public SkipPolicy hdecEnvioProcessorSkipPolicy() {
        return new AzureRequestProcessorSkipPolicy();
    }

    @Bean
    public ItemProcessListener<VentaBean, VentaBean> hdecEnvioProcessorListener() {
        return (ItemProcessListener<VentaBean, VentaBean>) new FlujoOfflineProcessorListener();
    }

    @Bean
    public Step contingenciaHDECStep(FlujoOfflineProcessor processor) {
        return stepBuilderFactory.get("FlujoOfflineStep")
                .<VentaBean, VentaBean>chunk(0)
                .reader(ventasReader())
                .processor(ventasProcessor())
                .faultTolerant()
                .skipPolicy(hdecEnvioProcessorSkipPolicy())
                .listener(hdecEnvioProcessorListener())
                .writer(ventasWriter())
                .build();
    }
}
