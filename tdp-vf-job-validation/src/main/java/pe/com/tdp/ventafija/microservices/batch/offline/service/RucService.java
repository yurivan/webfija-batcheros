package pe.com.tdp.ventafija.microservices.batch.offline.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.customer.ConsultaRucCustomer;
import pe.com.tdp.ventafija.microservices.common.clients.customer.ConsultaRucDataRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.customer.ConsultaRucDataResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.customer.CustomerRucResponseData;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

@Service
public class RucService {

    private static final Logger logger = LogManager.getLogger(ReniecService.class);

    private String rucUri;
    private String rucApiId;
    private String rucApiSecret;
    private String rucFlagActivo;
    private String mensajeOffline;

    @Autowired
    public RucService() {
        super();
    }

    public void configParameters(String rucUri,
                               String rucApiId,
                               String rucApiSecret, String rucFlagActivo){
        this.rucUri=rucUri;
        this.rucApiId=rucApiId;
        this.rucApiSecret=rucApiSecret;
        this.rucFlagActivo = rucFlagActivo;
    }

    public String getFlagRuc(){
        return mensajeOffline;
    }

    public Response<CustomerRucResponseData> consultaRuc(ConsultaRucDataRequestBody request,List<Object[]> objData){
        ConsultaRucDataResponseBody consultaRuc = new ConsultaRucDataResponseBody();
        Response<CustomerRucResponseData> response = new Response<>();
        ApiResponse<ConsultaRucDataResponseBody> consultaRucResponseBody;
        CustomerRucResponseData consultaRucData = new CustomerRucResponseData();
        String consultaRucSwitch = rucFlagActivo;//consultaParametro("tdp.api.consultaruc.activo");
        try{
            if(!consultaRucSwitch.equals("") && consultaRucSwitch.equals("false")){
                consultaRuc = proccessRucData(objData);
            }else{
                for (Integer cont = 1; cont < 4; cont ++){
                    consultaRucResponseBody = getDataRuc(request);
                    if(cont == 3){
                        response.setResponseCode("-2");
                        response.setResponseMessage("ERROR");
                        return response;
                    }
                    if(consultaRucResponseBody != null && consultaRucResponseBody.getBodyOut() != null && !consultaRucResponseBody.getBodyOut().getNumero().equals("")){
                        consultaRuc = consultaRucResponseBody.getBodyOut();
                        break;
                    }
                }
            }
            if(consultaRuc != null && consultaRuc.getNumero() != null){
                String domicilio = "";

                consultaRucData.setRazonSocial(consultaRuc.getRazSocial());
                consultaRucData.setUbigeo(consultaRuc.getUbigeo());
                consultaRucData.setCondDomicilio(consultaRuc.getConDomicilio());
                consultaRucData.setEstContrib(consultaRuc.getEstContribuyente());

                if(!consultaRuc.getTipVia().equals("-")){domicilio += consultaRuc.getTipVia() + " ";}
                if(!consultaRuc.getNomVia().equals("-") && !consultaRuc.getTipVia().equals("-")){domicilio += consultaRuc.getNomVia() + " ";}
                if(!consultaRuc.getCodZona().equals("-")){domicilio += consultaRuc.getCodZona() + " ";}
                if(!consultaRuc.getTipZona().equals("-") && !consultaRuc.getCodZona().equals("-")){domicilio += consultaRuc.getTipZona() + " ";}
                if(!consultaRuc.getNumero().equals("-")){domicilio += "Nro. " + consultaRuc.getNumero() + " ";}
                if(!consultaRuc.getInterior().equals("-")){domicilio += "Int. " + consultaRuc.getInterior() + " ";}
                if(!consultaRuc.getDepartamento().equals("-")){domicilio += "Dpto. " + consultaRuc.getDepartamento() + " ";}
                if(!consultaRuc.getManzana().equals("-")){domicilio += "Mz. " + consultaRuc.getManzana() + " ";}
                if(!consultaRuc.getLote().equals("-")){domicilio += "Lt. " + consultaRuc.getLote() + " ";}
                domicilio = domicilio.trim();

                consultaRucData.setDireccion(domicilio);
                response.setResponseData(consultaRucData);
                response.setResponseCode("0");
                response.setResponseMessage("OK");
            }else{
                response.setResponseCode("-1");
                response.setResponseMessage("ERROR CONSULTA RUC");
            }

        }catch (Exception e){
            response.setResponseCode("-1");
            response.setResponseMessage("ERROR");
            return response;
        }
        return response;
    }

    public ApiResponse<ConsultaRucDataResponseBody> getDataRuc(ConsultaRucDataRequestBody apiRequestBody)  throws TimeoutException
    {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(rucUri)
                .setApiId(rucApiId)
                .setApiSecret(rucApiSecret)
                .build();

        ConsultaRucCustomer consultaRucCustomer = new ConsultaRucCustomer(config);
        ApiResponse<ConsultaRucDataResponseBody> objRes = new ApiResponse<>();
        ConsultaRucDataResponseBody objResponse = new ConsultaRucDataResponseBody();
        String request = "";
        if(apiRequestBody.getNumDoc()!= null && !apiRequestBody.getNumDoc().equals("")) {
            request = "numDocumento=" + apiRequestBody.getNumDoc() + "&sisOrigen=" + (apiRequestBody.getSisOri().equals("") ? "1" : apiRequestBody.getSisOri());
        }
        ClientResult<ApiResponse<ConsultaRucDataResponseBody>> result = consultaRucCustomer.get(request);
        if(!result.isSuccess()) {
            objRes.setBodyOut(objResponse);
            result.setResult(objRes);
        }
        return result.getResult();
    }

    private ConsultaRucDataResponseBody proccessRucData(List<Object[]> dataRuc){
        ConsultaRucDataResponseBody objRucResponse = new ConsultaRucDataResponseBody();
        List<Object[]> objListaRuc = dataRuc;

        //objListaRuc = null;//customerRepository.getDataRuc(nroRuc)

        if(objListaRuc != null && objListaRuc.size() > 0){
            for (Object[] row : objListaRuc) {
                objRucResponse.setNumero(row[0] == null ? "" : row[0].toString());
                objRucResponse.setRazSocial(row[1] == null ? "" : row[1].toString());
                objRucResponse.setEstContribuyente(row[2] == null ? "" : row[2].toString());
                objRucResponse.setConDomicilio(row[3] == null ? "" : row[3].toString());
                objRucResponse.setUbigeo(row[4] == null ? "" : row[4].toString());
                objRucResponse.setTipVia(row[5] == null ? "" : row[5].toString());
                objRucResponse.setNomVia(row[6] == null ? "" : row[6].toString());
                objRucResponse.setCodZona(row[7] == null ? "" : row[7].toString());
                objRucResponse.setTipZona(row[8] == null ? "" : row[8].toString());
                objRucResponse.setNumero(row[9] == null ? "" : row[9].toString());
                objRucResponse.setInterior(row[10] == null ? "" : row[10].toString());
                objRucResponse.setLote(row[11] == null ? "" : row[11].toString());
                objRucResponse.setDepartamento(row[12] == null ? "" : row[12].toString());
                objRucResponse.setManzana(row[13] == null ? "" : row[13].toString());
                objRucResponse.setKilometro(row[14] == null ? "" : row[14].toString());
                break;
            }

        }

        if(objRucResponse.getNumero() != null){
            return objRucResponse;
        }else {
            return null;
        }


    }
}
