package pe.com.tdp.ventafija.microservices.batch.offline.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.offline.service.entity.Parameters;

import java.util.List;

@Repository
public interface ParametersRepository extends JpaRepository<Parameters, Integer> {

	Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element);

	@Query(value = "select num_ruc," +
			"raz_social," +
			"est_contr," +
			"cond_dom," +
			"ubigeo," +
			"tipo_via," +
			"nom_via," +
			"cod_zona," +
			"tip_zona," +
			"numero," +
			"interior," +
			"lote," +
			"dpto," +
			"manzana," +
			"kilometro" +
			" from ibmx_a07e6d02edaf552.ruc_sunat " +
			" where  num_ruc = ?1" +
			" limit 1", nativeQuery = true)
	List<Object[]> getDataRuc(String nroruc);

}
