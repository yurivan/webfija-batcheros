package pe.com.tdp.ventafija.microservices.batch.offline.service.entity;


import javax.persistence.*;

@Entity
@Table(name = "peticion_pago_efectivo", schema="ibmx_a07e6d02edaf552") //glazaror se adiciona schema
@SequenceGenerator(name="myseqpagoefectivo", sequenceName="ibmx_a07e6d02edaf552.peticion_pago_efectivo_seq", catalog="ibmx_a07e6d02edaf552", schema="ibmx_a07e6d02edaf552", allocationSize=1)//glazaror

public class PeticionPagoEfectivo {
    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO) //sprint 3
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="myseqpagoefectivo")//sprint 3
            Integer id;
    @Column(name = "status") //sprint 3
            String status;
    @Column(name = "order_id")
    String orderId;
    @Column(name = "idmoneda")
    private String idMoneda;
    @Column(name = "total")
    private String total;
    @Column(name = "metodospago")
    private String metodosPago;
    @Column(name = "codservicio")
    private String codServicio;
    @Column(name = "codtransaccion")
    private String codTransaccion;
    @Column(name = "emailcomercio")
    private String emailComercio;
    @Column(name = "fechaaexpirar")
    private String fechaAExpirar;
    @Column(name = "usuarioid")
    private String usuarioId;
    @Column(name = "dataadicional")
    private String dataAdicional;
    @Column(name = "usuarionombre")
    private String usuarioNombre;
    @Column(name = "usuarioapellidos")
    private String usuarioApellidos;
    @Column(name = "usuariolocalidad")
    private String usuarioLocalidad;
    @Column(name = "usuarioprovincia")
    private String usuarioProvincia;
    @Column(name = "usuariopais")
    private String usuarioPais;
    @Column(name = "usuarioalias")
    private String usuarioAlias;
    @Column(name = "usuariotipodoc")
    private String usuarioTipoDoc;
    @Column(name = "usuarionumerodoc")
    private String usuarioNumeroDoc;
    @Column(name = "usuarioemail")
    private String usuarioEmail;
    @Column(name = "conceptopago")
    private String conceptoPago;
    @Column(name = "detalles")
    private String detalles;
    @Column(name = "paramsurl")
    private String paramsUrl;
    @Column(name = "paramsemail")
    private String pramsEmail;
    @Column(name = "codpago")
    private String codPago;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getIdMoneda() {
        return idMoneda;
    }

    public void setIdMoneda(String idMoneda) {
        this.idMoneda = idMoneda;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getMetodosPago() {
        return metodosPago;
    }

    public void setMetodosPago(String metodosPago) {
        this.metodosPago = metodosPago;
    }

    public String getCodServicio() {
        return codServicio;
    }

    public void setCodServicio(String codServicio) {
        this.codServicio = codServicio;
    }

    public String getCodTransaccion() {
        return codTransaccion;
    }

    public void setCodTransaccion(String codTransaccion) {
        this.codTransaccion = codTransaccion;
    }

    public String getEmailComercio() {
        return emailComercio;
    }

    public void setEmailComercio(String emailComercio) {
        this.emailComercio = emailComercio;
    }

    public String getFechaAExpirar() {
        return fechaAExpirar;
    }

    public void setFechaAExpirar(String fechaAExpirar) {
        this.fechaAExpirar = fechaAExpirar;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getDataAdicional() {
        return dataAdicional;
    }

    public void setDataAdicional(String dataAdicional) {
        this.dataAdicional = dataAdicional;
    }

    public String getUsuarioNombre() {
        return usuarioNombre;
    }

    public void setUsuarioNombre(String usuarioNombre) {
        this.usuarioNombre = usuarioNombre;
    }

    public String getUsuarioApellidos() {
        return usuarioApellidos;
    }

    public void setUsuarioApellidos(String usuarioApellidos) {
        this.usuarioApellidos = usuarioApellidos;
    }

    public String getUsuarioLocalidad() {
        return usuarioLocalidad;
    }

    public void setUsuarioLocalidad(String usuarioLocalidad) {
        this.usuarioLocalidad = usuarioLocalidad;
    }

    public String getUsuarioProvincia() {
        return usuarioProvincia;
    }

    public void setUsuarioProvincia(String usuarioProvincia) {
        this.usuarioProvincia = usuarioProvincia;
    }

    public String getUsuarioPais() {
        return usuarioPais;
    }

    public void setUsuarioPais(String usuarioPais) {
        this.usuarioPais = usuarioPais;
    }

    public String getUsuarioAlias() {
        return usuarioAlias;
    }

    public void setUsuarioAlias(String usuarioAlias) {
        this.usuarioAlias = usuarioAlias;
    }

    public String getUsuarioTipoDoc() {
        return usuarioTipoDoc;
    }

    public void setUsuarioTipoDoc(String usuarioTipoDoc) {
        this.usuarioTipoDoc = usuarioTipoDoc;
    }

    public String getUsuarioNumeroDoc() {
        return usuarioNumeroDoc;
    }

    public void setUsuarioNumeroDoc(String usuarioNumeroDoc) {
        this.usuarioNumeroDoc = usuarioNumeroDoc;
    }

    public String getUsuarioEmail() {
        return usuarioEmail;
    }

    public void setUsuarioEmail(String usuarioEmail) {
        this.usuarioEmail = usuarioEmail;
    }

    public String getConceptoPago() {
        return conceptoPago;
    }

    public void setConceptoPago(String conceptoPago) {
        this.conceptoPago = conceptoPago;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }

    public String getParamsUrl() {
        return paramsUrl;
    }

    public void setParamsUrl(String paramsUrl) {
        this.paramsUrl = paramsUrl;
    }

    public String getPramsEmail() {
        return pramsEmail;
    }

    public void setPramsEmail(String pramsEmail) {
        this.pramsEmail = pramsEmail;
    }

    public String getCodPago() {
        return codPago;
    }

    public void setCodPago(String codPago) {
        this.codPago = codPago;
    }
}
