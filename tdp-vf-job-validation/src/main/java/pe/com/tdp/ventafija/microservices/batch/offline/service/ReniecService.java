package pe.com.tdp.ventafija.microservices.batch.offline.service;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.address.ListOfPuntosGeocodificados;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.reniec.IdentifyApiReniecRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.reniec.IdentifyApiReniecResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.reniec.ReniecClient;
import pe.com.tdp.ventafija.microservices.common.util.*;

import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyRequest;
import pe.com.tdp.ventafija.microservices.domain.customer.IdentifyResponseData;

@Service
public class ReniecService {

    private static final Logger logger = LogManager.getLogger(ReniecService.class);

    private String reniecConsultaClienteUri;
    private String reniecConsultarClienteApiId;
    private String reniecConsultarClienteApiSecret;

    @Autowired
    public ReniecService() {
        super();
    }

    public void setCredentials(String reniecConsultaClienteUri,
                               String reniecConsultarClienteApiId,
                               String reniecConsultarClienteApiSecret){
        this.reniecConsultaClienteUri=reniecConsultaClienteUri;
        this.reniecConsultarClienteApiId=reniecConsultarClienteApiId;
        this.reniecConsultarClienteApiSecret=reniecConsultarClienteApiSecret;
    }

    public Response<IdentifyResponseData> identify(IdentifyRequest request) {

        Response<IdentifyResponseData> response = new Response<>();
        try {
            /* Validar RUC 10 */
            if (request.getDocumentNumber() != null && request.getDocumentNumber().length() > 8) {
                request.setDocumentNumber(request.getDocumentNumber().substring(2, 10));
            }

            IdentifyApiReniecRequestBody apiReniecRequestBody = new IdentifyApiReniecRequestBody();
            apiReniecRequestBody.setNumDNI(request.getDocumentNumber());

            ApiResponse<IdentifyApiReniecResponseBody> objReniec = getReniec(apiReniecRequestBody, request.getId());

            if ((Constants.RESPONSE).equals(objReniec.getHeaderOut().getMsgType())) {
                IdentifyResponseData responseData = new IdentifyResponseData();
                responseData.setLastName(objReniec.getBodyOut().getApePaterno());
                responseData.setMotherLastName(objReniec.getBodyOut().getApeMaterno() == null ? "" : objReniec.getBodyOut().getApeMaterno());
                responseData.setNames(objReniec.getBodyOut().getNombres());
                responseData.setDepartmentBirthLocation(objReniec.getBodyOut().getNacUbiDepartamento());
                responseData.setProvinceBirthLocation(objReniec.getBodyOut().getNacUbiProvincia());
                responseData.setDistrictBirthLocation(objReniec.getBodyOut().getNacUbiDistrito());
                responseData.setBirthdate(objReniec.getBodyOut().getFecNacimiento());
                responseData.setFatherName(objReniec.getBodyOut().getNomPadre() == null ? "" : objReniec.getBodyOut().getNomPadre());
                responseData.setMotherName(objReniec.getBodyOut().getNomMadre() == null ? "" : objReniec.getBodyOut().getNomMadre());
                responseData.setExpeditionDate(objReniec.getBodyOut().getFecExpedicion());
                responseData.setRestriction(objReniec.getBodyOut().getRestriccion());
                response.setResponseCode("0");
                response.setResponseData(responseData);
            }
        }  catch (Exception e) {
            logger.error("Error Identify Service", e);
            logger.info("Fin Identify Service");
            return response;
        }

        logger.info("Fin Identify Service");
        return response;
    }

    private ApiResponse<IdentifyApiReniecResponseBody> getReniec(IdentifyApiReniecRequestBody apiRequestBody, String orderId) throws ClientException {

        System.out.println(reniecConsultaClienteUri);
        System.out.println(reniecConsultarClienteApiId);
        System.out.println(reniecConsultarClienteApiSecret);
        System.out.println(Constants.API_REQUEST_HEADER_OPERATION_RENIEC);
        System.out.println(Constants.API_REQUEST_HEADER_DESTINATION_RENIEC);
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(reniecConsultaClienteUri)
                .setApiId(reniecConsultarClienteApiId)
                .setApiSecret(reniecConsultarClienteApiSecret)
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_RENIEC)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_RENIEC)
                .build();

        ReniecClient reniecClient = new ReniecClient(config);
        ClientResult<ApiResponse<IdentifyApiReniecResponseBody>> result = reniecClient.post(apiRequestBody);

        if ((Constants.RESPONSE).equals(result.getResult().getHeaderOut().getMsgType())) {
            if (result.getResult().getBodyOut().getRestriccion() != null && result.getResult().getBodyOut().getRestriccion().equalsIgnoreCase("A")) {
                result.getResult().getHeaderOut().setMsgType("ERROR");
                result.getResult().getBodyOut().setRestriccion("FALLECIMIENTO");
                result.getEvent().setServiceResponse(result.getEvent().getServiceResponse().replace("\"restriccion\" : \"A\"", "\"restriccion\" : \"FALLECIMIENTO\""));
            }
        }

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
            result.getEvent().setOrderId(orderId);
        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

}