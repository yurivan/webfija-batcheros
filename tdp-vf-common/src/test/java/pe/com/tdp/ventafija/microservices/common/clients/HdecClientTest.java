package pe.com.tdp.ventafija.microservices.common.clients;

public class HdecClientTest {
//	private ApiHeaderConfig config;
//	private ApiClient apiClient;
//	private ApiRequestHeaderFactory factory;
//	private String requestBody = "{\"CodigoUnico\":\"-KZSBbJkKusQpNWySP0K\",\"Campana\":\"Masiva\",\"NombreDeCliente\":\"MARIA ELENA\",\"ApellidoPaterno\":\"ANGULO\",\"ApellidoMaterno\":\"DE PLASENCIA\",\"TipoDeDocumento\":\"dni\",\"NumeroDeDocumento\":\"18086237\",\"TelefonoDeContacto1\":\"948426667\",\"TelefonoDeContacto2\":\"11111111\",\"Email\":\"notiene@gmail.com\",\"EnvioDeContratos\":\"Si\",\"ProteccionDeDatos\":\"Si\",\"CanalDeVenta\":\"NO APLICA\",\"DetalleDeCanal\":\"EXPERTEL\",\"NombreVendedor\":\"MICHAEL\",\"CodVendedorAtis\":\"18516\",\"CodVendedorCms\":\"51302\",\"ZonalDepartamentoVendedor\":\"NO APLICA\",\"RegionProvinciaDistritoVendedor\":\"NO APLICA\",\"TipoDeProducto\":\"Trio\",\"SubProducto\":\"Trio Plano Local 8 Mbps Estandar Digital\",\"Departamento\":\"La Libertad\",\"Provincia\":\"Trujillo\",\"Distrito\":\"Trujillo\",\"Direccion\":\"pasaje :korea mz.O lte:13 urb.monserrate - a espaldas de la sanidad de la policia\",\"OperacionComercial\":\"Alta Pura\",\"TelefonoAMigrar\":\"11111111\",\"DesafiliacionDePaginasBlancas\":\"Si\",\"AfiliacionAFacturaDigital\":\"Si\",\"TecnologiaDeInternet\":\"ADSL\",\"CodigoExperto\":\"0\",\"TieneGrabacion\":\"SI\",\"FechaRegistro\":\"20-12-2016\",\"HoraRegistroWeb\":\"17:16:22\",\"ModalidadDePago\":\"Contado\",\"IdGrabacionNativo\":\"-KZSBbJkKusQpNWySP0K\",\"TecnologiaTelevision\":\"CATV\",\"Paquetizacion\":\"NO\",\"AltasTv\":\"No\",\"TipoEquipamientoDeco\":\"Si\",\"DniVendedor\":\"42690373\",\"TelefonoOrigen\":\"959714516\",\"TipoServicio\":\"ATIS\",\"ClienteCms\":\"1111111\",\"DistritoVendedor\":\"NO APLICA\",\"DecosSd\":\"0\",\"DecosHd\":\"2\",\"DecosDvr\":\"0\",\"BloqueTv\":\"NO APLICA\",\"SvaInternet\":\"\",\"SvaLinea\":\"NO APLICA\",\"DescuentoWinback\":\" \",\"CodigoDeServicioCms\":\"1111111\",\"BloqueProducto\":\"NO APLICA\"}";
//	private String responseBody = "{\"HeaderOut\":{\"originator\":\"ES:BUS:AVVE:AVVE\",\"destination\":\"PE:TDP:APPVENTAS:APPVENTAS\",\"execId\":\"550e8400-e29b-41d4-a716-446655440001\",\"timestamp\":\"2016-12-20T12:23:30.64-05:00\",\"msgType\":\"RESPONSE\"},\"BodyOut\":{\"Mensaje\":\"Cliente Registrado Satisfactoriamente en HDEC\"}}";
//	
//	@Before
//	public void initConfig () throws URISyntaxException {
//		config = new ApiHeaderConfig();
//		config.setApiUri("https://api.us.apiconnect.ibmcloud.com/telefonica-del-peru-production/ter/preventa/registrarPreventa");
//		config.setHdecRegistrarPreVentaApiId("486c5aa3-d9a7-47e1-b39e-bbbff1c1069c");
//		config.setHdecRegistrarPreVentaApiSecret("");
//		config.setCountry("PE");
//		config.setLang("es");
//		config.setEntity("TDP");
//		config.setSystem("APPVENTAS");
//		config.setSubsystem("APPVENTAS");
//		config.setOriginator("PE:TDP:APPVENTAS:APPVENTAS");
//		config.setSender("OracleServiceBus");
//		config.setUserId("AppUser");
//		config.setWsId("BluemixEveris");
//		config.setWsIp("10.11.1.11");
//		config.setOperation("Consulta HDC");
//		config.setDestination("ES:BUS:HDC:HDC");
//		config.setExecId("550e8400-e29b-41d4-a716-446655440001");
//		
//		ApiRequestHeaderFactory factory = new ApiRequestHeaderFactory(config);
//		this.factory = Mockito.spy(new ApiRequestHeaderFactory(config));
//		Mockito.when(this.factory.getHeader()).thenReturn(factory.getHeader());
//		
//		// mock apiClient
//		ApiRequest<HdecApiHdecRequestBody> apiRequest = new ApiRequest<>();
//		ApiRequestHeader headerIn = this.factory.getHeader();
//		headerIn.setOperation(Constants.API_REQUEST_HEADER_OPERATION_HDEC);
//		headerIn.setDestination(Constants.API_REQUEST_HEADER_DESTINATION_HDEC);
//		apiRequest.setHeaderIn(headerIn);
//        
//        apiRequest.setBodyIn(buildHdecRequest());
//        
//		this.apiClient = Mockito.mock(ApiClient.class);
//		URI uri = new URI(config.getApiUri());
//		
//		Mockito.when(apiClient.jerseyPOST(uri, apiRequest, config.getHdecRegistrarPreVentaApiId(), config.getHdecRegistrarPreVentaApiSecret())).thenReturn(responseBody);
//	}
//
//	@Test
//	public void deberiaEnviarDatosAHdec() throws ApiClientException, URISyntaxException {
//		HdecClient client = new HdecClient(config, apiClient, factory);
////		HdecClient client = new HdecClient(config);
//		
//		HdecApiHdecRequestBody request = buildHdecRequest();
//		HdecApiHdecResponseBody response = client.sendData(request);
//	}
//	
//	private HdecApiHdecRequestBody buildHdecRequest () {
//		HdecApiHdecRequestBody request = new HdecApiHdecRequestBody();
//		ObjectMapper mapper = new ObjectMapper();
//		try {
//			request = mapper.readValue(requestBody, new TypeReference<HdecApiHdecRequestBody>() {});
////			request.setCodigoUnico(request.getCodigoUnico()+new java.util.Date().getTime());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return request;
//	}
}
