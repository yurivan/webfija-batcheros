package pe.com.tdp.ventafija.microservices.common.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class NumberUtilsTest {

	@Test
	public void returnsTrueWhenValidNumber () {
		assertTrue(NumberUtils.isNumber("131222222222222222222"));
		assertTrue(NumberUtils.isNumber("1312"));
		assertTrue(NumberUtils.isNumber("   1312   "));
		
		assertFalse(NumberUtils.isNumber("1312asdasas"));
		assertFalse(NumberUtils.isNumber("aaaa1312asdasas"));
		assertFalse(NumberUtils.isNumber("asdasd"));
		assertFalse(NumberUtils.isNumber(null));
	}
}
