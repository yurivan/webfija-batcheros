package pe.com.tdp.ventafija.microservices.common.encryption;

import java.io.File;
import java.io.InputStream;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;

public class PGPEncryptionHelperTest {
	private PGPEncryptionHelper pgpEncryptionHelper;
	private PGPEncryptionHelper pgpDecryptionHelper;
	
	@Before
	public void init () {
		InputStream publicIn = ClassLoader.class.getResourceAsStream("/public.gpg");
		InputStream privateIn = ClassLoader.class.getResourceAsStream("/private.gpg");
		
		pgpEncryptionHelper = new PGPEncryptionHelper(publicIn);
		pgpDecryptionHelper = new PGPEncryptionHelper(privateIn, "3v3r1s2017.");
	}
	
	@Test
	public void shouldEncryptText() throws Exception {
//		String text1 = "Hola mundo :)";
//		String text2 = "Este es un texto muy largo que debe contener caracteres extra\uu00F1os!!'..";
//		
//		String cipherText1 = pgpEncryptionHelper.encrypt(text1);
//		String cipherText2 = pgpEncryptionHelper.encrypt(text2);
//		
//		String plainText1 = pgpDecryptionHelper.decrypt(cipherText1);
//		String plainText2 = pgpDecryptionHelper.decrypt(cipherText2);
//		
//		String plainText = pgpDecryptionHelper.decrypt("-----BEGIN PGP MESSAGE-----\nVersion: BCPG v1.46\n\nhQEMA4DaTYOcGRI6AQf/S+iy65cCg7oM7XRGf/x0tLIdl6KViHTZggH3MuxNcgwC\nrGjL9OLYB7TqqrMjIvbbP+shMFZo6pazt2sx5oBHaExskIkCjhhK3vCG5jzQRCGS\nAeSlZNzYozYxkQrTPxS84ZZyShnDpqr6+M5XnSI51b0lARt+a6fbu5PyfuWy1uwO\nsgWNrNTbvaUEjxJsjsxNqXPaB5ADySQIh6uwOv0//eVik9TWj8bB3eJDoU/88pPD\nH5x9QiR8eBmfRI4WLoPnwQgNzFWbASLR0SEKoDNbxLA1ezle0HjKqH/LnYIMYZ5/\nVbMfTHP40w8Fo3yDk/1MfnJ4URlV4W5Xt+j8bSxt7NLCDwHHKLgqstXhJzULyFRN\nOSq8hhnulqfLOrFXeVVjic+SGKwMt0XGXVhxZ+OWlusVClmksT9ckEuDOeJGyQ0O\ngkS/VuldScqBwmZgIo/NGgZNgOOgu0g98vlVYruabhSEMfmsI70QcTOJjgkgxdzR\nIAAsu9eTEXVrs7HzvLGblCdfrscQiTMn4hcwPb4cxgV0swti9BYHiarKIdo2jcFj\nlO6+tW6cPmvl1qUrRhC2y2mzrdB4ZlRbAiNs4e3dDXkaid/PhlndBcwmNHHNNVWS\nhRSex+Ze0OwaLc8AHSOXVvFkfbBWWzVN3kT5JiEoh0P9nhn1lSAqeCRxZ79APr2g\nRgYjWpCX+ogV7cyZoi+afk4igK/mX69PFwd4c5hS70ssifEBEo89lOEycfgLXGBd\nwXLG4XkYWmtcjslRaFhEas4nabxM1gN8iruVExEpjUy9B8+9hbm6tK1JrN1d0k8T\nq96bvicwt/iG+fZTGoJ11sZpyKUiyV3qZbEGdNwQJgkSdkftojIwPe7nX6qDA9fA\nQCfgiQDnkYEzE39BqKAgP9vGYyDaMFuQpxo8jeEsPmUV+2QqST98I/y54f3dEv4F\nF2TWd8YHx1f5jkaR1T8PTuuBVlZnSwww022tiBLj2Yjw+Lv8Cqyflg4XJq4fp49o\nQzbFx1CavcGLigjU+IKdIVeWcGwD4aBTgUPiPYUOl7g+nXF4FLMD1QBeuOE0FsEC\nsRDBhLu0Y70qh9l8l3ybtN2Ke8Z20h9qaCt3oCXSDiMsvKVALnTSMdmhv+3IPBPn\nTyqcBosm/MDkidJD1oywr/Dw+b+1el8smZODXjs9F27wa5lN951xgW9ih6Y4Dk7c\n6H3e2+kJO5L84sm69+UK6WOy8We2weADrr2ReqAyQRae/fBp47fscnSuGNo2doJK\nJSg/Rjcp5fRPGEXfYY+AVuM1sBzYCgKiTYFmJuIPlnHN\n=xuzk\n-----END PGP MESSAGE-----\n");
//		String s = FileUtils.readFileToString(new File("/ms/filename128.txt"));
//		String plainText = pgpDecryptionHelper.decrypt(s);
//		System.out.println(plainText);
//		
//		Assert.assertEquals(text1, plainText1);
//		Assert.assertEquals(text2, plainText2);
	}

//	@Test
//	public void shouldEncryptFiles() throws Exception {
//		File file1 = new File("/ms/olo.png");
//		File file2 = new File("/ms/audio.mp3");
//		
//		File encFile1 = pgpEncryptionHelper.encrypt(file1);
//		File encFile2 = pgpEncryptionHelper.encrypt(file2);
//		
//		File decFile1 = pgpDecryptionHelper.decrypt(encFile1);
//		File decFile2 = pgpDecryptionHelper.decrypt(encFile2);
//		
//		FileUtils.copyFile(encFile1, new File("/ms/olo-enc.png"));
//		
//		FileUtils.copyFile(decFile1, new File("/ms/olo2.png"));
//		FileUtils.copyFile(decFile2, new File("/ms/audio2.mp3"));
//		
//		Assert.assertTrue(FileUtils.contentEquals(file1, decFile1));
//		Assert.assertTrue(FileUtils.contentEquals(file2, decFile2));
//	}
	
	@Test
	public void shouldDecryptFiles() throws Exception {
//		File file = new File(this.getClass().getClassLoader().getResource("-KpvIN02seP_xcf3ij4k.pgp").getFile());
//		File f = pgpDecryptionHelper.decrypt(file);
//		System.out.println(f);
	}
}
