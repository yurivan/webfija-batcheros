
package pe.com.tdp.ventafija.microservices.common.clients.address2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "geocodInvRespuestaTdpBE"
})
public class ResponseList {

    @JsonProperty("geocodInvRespuestaTdpBE")
    private GeocodInvRespuestaTdpBE geocodInvRespuestaTdpBE;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("geocodInvRespuestaTdpBE")
    public GeocodInvRespuestaTdpBE getGeocodInvRespuestaTdpBE() {
        return geocodInvRespuestaTdpBE;
    }

    @JsonProperty("geocodInvRespuestaTdpBE")
    public void setGeocodInvRespuestaTdpBE(GeocodInvRespuestaTdpBE geocodInvRespuestaTdpBE) {
        this.geocodInvRespuestaTdpBE = geocodInvRespuestaTdpBE;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
