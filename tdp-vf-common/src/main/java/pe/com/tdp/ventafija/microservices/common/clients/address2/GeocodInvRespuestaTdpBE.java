
package pe.com.tdp.ventafija.microservices.common.clients.address2;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "codDist",
        "codDpto",
        "codProvince",
        "desUbigeo",
        "address",
        "kilometers",
        "lot",
        "block",
        "urbName",
        "viaName",
        "apartmentName",
        "blockNum",
        "interiorNum",
        "doorNum",
        "floor",
        "reference",
        "interiorType",
        "urbType",
        "viaType",
        "apartmentType",
        "ubigeo",
        "scope",
        "populatedCenter",
        "indDelivery",
        "indAddress",
        "precisionLevel"
})
public class GeocodInvRespuestaTdpBE {

    @JsonProperty("codDist")
    private String codDist;
    @JsonProperty("codDpto")
    private String codDpto;
    @JsonProperty("codProvince")
    private String codProvince;
    @JsonProperty("desUbigeo")
    private String desUbigeo;
    @JsonProperty("address")
    private String address;
    @JsonProperty("kilometers")
    private String kilometers;
    @JsonProperty("lot")
    private String lot;
    @JsonProperty("block")
    private String block;
    @JsonProperty("urbName")
    private String urbName;
    @JsonProperty("viaName")
    private String viaName;
    @JsonProperty("apartmentName")
    private String apartmentName;
    @JsonProperty("blockNum")
    private String blockNum;
    @JsonProperty("interiorNum")
    private String interiorNum;
    @JsonProperty("doorNum")
    private String doorNum;
    @JsonProperty("floor")
    private String floor;
    @JsonProperty("reference")
    private String reference;
    @JsonProperty("interiorType")
    private String interiorType;
    @JsonProperty("urbType")
    private String urbType;
    @JsonProperty("viaType")
    private String viaType;
    @JsonProperty("apartmentType")
    private String apartmentType;
    @JsonProperty("ubigeo")
    private String ubigeo;
    @JsonProperty("scope")
    private String scope;
    @JsonProperty("populatedCenter")
    private String populatedCenter;
    @JsonProperty("indDelivery")
    private String indDelivery;
    @JsonProperty("indAddress")
    private String indAddress;
    @JsonProperty("precisionLevel")
    private String precisionLevel;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("codDist")
    public String getCodDist() {
        return codDist;
    }

    @JsonProperty("codDist")
    public void setCodDist(String codDist) {
        this.codDist = codDist;
    }

    @JsonProperty("codDpto")
    public String getCodDpto() {
        return codDpto;
    }

    @JsonProperty("codDpto")
    public void setCodDpto(String codDpto) {
        this.codDpto = codDpto;
    }

    @JsonProperty("codProvince")
    public String getCodProvince() {
        return codProvince;
    }

    @JsonProperty("codProvince")
    public void setCodProvince(String codProvince) {
        this.codProvince = codProvince;
    }

    @JsonProperty("desUbigeo")
    public String getDesUbigeo() {
        return desUbigeo;
    }

    @JsonProperty("desUbigeo")
    public void setDesUbigeo(String desUbigeo) {
        this.desUbigeo = desUbigeo;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("kilometers")
    public String getKilometers() {
        return kilometers;
    }

    @JsonProperty("kilometers")
    public void setKilometers(String kilometers) {
        this.kilometers = kilometers;
    }

    @JsonProperty("lot")
    public String getLot() {
        return lot;
    }

    @JsonProperty("lot")
    public void setLot(String lot) {
        this.lot = lot;
    }

    @JsonProperty("block")
    public String getBlock() {
        return block;
    }

    @JsonProperty("block")
    public void setBlock(String block) {
        this.block = block;
    }

    @JsonProperty("urbName")
    public String getUrbName() {
        return urbName;
    }

    @JsonProperty("urbName")
    public void setUrbName(String urbName) {
        this.urbName = urbName;
    }

    @JsonProperty("viaName")
    public String getViaName() {
        return viaName;
    }

    @JsonProperty("viaName")
    public void setViaName(String viaName) {
        this.viaName = viaName;
    }

    @JsonProperty("apartmentName")
    public String getApartmentName() {
        return apartmentName;
    }

    @JsonProperty("apartmentName")
    public void setApartmentName(String apartmentName) {
        this.apartmentName = apartmentName;
    }

    @JsonProperty("blockNum")
    public String getBlockNum() {
        return blockNum;
    }

    @JsonProperty("blockNum")
    public void setBlockNum(String blockNum) {
        this.blockNum = blockNum;
    }

    @JsonProperty("interiorNum")
    public String getInteriorNum() {
        return interiorNum;
    }

    @JsonProperty("interiorNum")
    public void setInteriorNum(String interiorNum) {
        this.interiorNum = interiorNum;
    }

    @JsonProperty("doorNum")
    public String getDoorNum() {
        return doorNum;
    }

    @JsonProperty("doorNum")
    public void setDoorNum(String doorNum) {
        this.doorNum = doorNum;
    }

    @JsonProperty("floor")
    public String getFloor() {
        return floor;
    }

    @JsonProperty("floor")
    public void setFloor(String floor) {
        this.floor = floor;
    }

    @JsonProperty("reference")
    public String getReference() {
        return reference;
    }

    @JsonProperty("reference")
    public void setReference(String reference) {
        this.reference = reference;
    }

    @JsonProperty("interiorType")
    public String getInteriorType() {
        return interiorType;
    }

    @JsonProperty("interiorType")
    public void setInteriorType(String interiorType) {
        this.interiorType = interiorType;
    }

    @JsonProperty("urbType")
    public String getUrbType() {
        return urbType;
    }

    @JsonProperty("urbType")
    public void setUrbType(String urbType) {
        this.urbType = urbType;
    }

    @JsonProperty("viaType")
    public String getViaType() {
        return viaType;
    }

    @JsonProperty("viaType")
    public void setViaType(String viaType) {
        this.viaType = viaType;
    }

    @JsonProperty("apartmentType")
    public String getApartmentType() {
        return apartmentType;
    }

    @JsonProperty("apartmentType")
    public void setApartmentType(String apartmentType) {
        this.apartmentType = apartmentType;
    }

    @JsonProperty("ubigeo")
    public String getUbigeo() {
        return ubigeo;
    }

    @JsonProperty("ubigeo")
    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    @JsonProperty("scope")
    public String getScope() {
        return scope;
    }

    @JsonProperty("scope")
    public void setScope(String scope) {
        this.scope = scope;
    }

    @JsonProperty("populatedCenter")
    public String getPopulatedCenter() {
        return populatedCenter;
    }

    @JsonProperty("populatedCenter")
    public void setPopulatedCenter(String populatedCenter) {
        this.populatedCenter = populatedCenter;
    }

    @JsonProperty("indDelivery")
    public String getIndDelivery() {
        return indDelivery;
    }

    @JsonProperty("indDelivery")
    public void setIndDelivery(String indDelivery) {
        this.indDelivery = indDelivery;
    }

    @JsonProperty("indAddress")
    public String getIndAddress() {
        return indAddress;
    }

    @JsonProperty("indAddress")
    public void setIndAddress(String indAddress) {
        this.indAddress = indAddress;
    }

    @JsonProperty("precisionLevel")
    public String getPrecisionLevel() {
        return precisionLevel;
    }

    @JsonProperty("precisionLevel")
    public void setPrecisionLevel(String precisionLevel) {
        this.precisionLevel = precisionLevel;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
