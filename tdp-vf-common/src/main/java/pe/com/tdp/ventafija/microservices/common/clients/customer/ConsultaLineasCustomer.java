package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.sms.AbstractClientIVR;

import java.io.IOException;

public class ConsultaLineasCustomer extends AbstractClientIVR<CustomerLineRequestBody, CustomerLineResponseBody> {
    @Override
    protected String getServiceCode() {
        return "";
    }

    @Override
    protected ApiResponse<CustomerLineResponseBody> getResponse(String json)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        ApiResponse<CustomerLineResponseBody> objCms = mapper.readValue(json, new TypeReference<ApiResponse<CustomerLineResponseBody>>() {});
        return objCms;
    }

    public ConsultaLineasCustomer(ClientConfig config) {
        super(config);
    }
}
