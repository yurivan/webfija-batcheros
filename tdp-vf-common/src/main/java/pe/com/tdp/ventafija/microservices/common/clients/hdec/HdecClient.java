package pe.com.tdp.ventafija.microservices.common.clients.hdec;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig; 
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.dto.HdecApiHdecRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.HdecApiHdecResponseBody;
 


public class HdecClient  extends AbstractClient<HdecApiHdecRequestBody, HdecApiHdecResponseBody>{

	public HdecClient(ClientConfig config) {
		super(config);
	}
	
	@Override
	protected String getServiceCode() {
		return SERVICE_CODE_HDEC;
	}

	@Override
	protected ApiResponse<HdecApiHdecResponseBody> getResponse(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		ApiResponse<HdecApiHdecResponseBody> objCms = mapper.readValue(json, new TypeReference<ApiResponse<HdecApiHdecResponseBody>>() {});
		return objCms;
	}
	
}
