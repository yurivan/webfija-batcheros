package pe.com.tdp.ventafija.microservices.common.exception;

import org.springframework.validation.BindingResult;

public class DataValidationException extends ApplicationException {
	private static final long serialVersionUID = 1L;
	private BindingResult validationError = null;
	
	public DataValidationException(BindingResult validationError) {
		super();
		this.validationError = validationError;
	}
	
	public DataValidationException (String messageCode) {
		super(messageCode);
	}
	
	public BindingResult getValidationError () {
		return validationError;
	}
}
