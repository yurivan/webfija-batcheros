package pe.com.tdp.ventafija.microservices.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import pe.com.tdp.ventafija.microservices.common.clients.dto.bio.SoapError;

@JsonPropertyOrder({"TefHeaderRes","ValidateCustomerIdentityResponseData"})
public class ApiResponseBIO<E>  extends SoapError {
    
	@JsonProperty("TefHeaderRes")
    private ApiResponseBIOHeader TefHeaderRes;
	@JsonProperty("ValidateCustomerIdentityResponseData")
    private E ValidateCustomerIdentityResponseData;
  
	public ApiResponseBIOHeader getTefHeaderRes() {
		return TefHeaderRes;
	}
	public void setTefHeaderRes(ApiResponseBIOHeader tefHeaderRes) {
		TefHeaderRes = tefHeaderRes;
	}
	
	public E getValidateCustomerIdentityResponseData() {
		return ValidateCustomerIdentityResponseData;
	}
	public void setValidateCustomerIdentityResponseData(E validateCustomerIdentityResponseData) {
		ValidateCustomerIdentityResponseData = validateCustomerIdentityResponseData;
	}

 
}

