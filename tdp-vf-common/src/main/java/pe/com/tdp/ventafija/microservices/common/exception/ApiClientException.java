package pe.com.tdp.ventafija.microservices.common.exception;

public class ApiClientException extends ApplicationException {

	public ApiClientException (String apiMessage) {
		super(apiMessage);
	}
}
