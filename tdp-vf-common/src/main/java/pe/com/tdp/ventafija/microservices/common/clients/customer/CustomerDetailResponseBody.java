package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder({"COD-CLI-CD","NOM-DS","PRI-APE-DS","SEG-APE-DS","COD-TIP-COT-CD","COD-ITM-REF-CD","TIP-CLI-CD",
        "NOM-TIP-CLI-NO","COD-EST-CD","NOM-EST-NO","COD-EMP-CD","NOM-EMP-NO","NUM-DOC-CLI-CD","DIG-CTL-DOC-CD",
        "TIP-DOC-CD","COD-SGM-CLI-CD","NOM-SGM-CLI-NO","COD-SBG-CLI-CD","NOM-SBG-CLI-NO","CLI-GRU-IN","BLO-VIN-HHO-IN",
        "COD-HHO-CD","NUM-IDE-NU","TIP-PRO-CMR-CD","NOM-TIP-PRO-NO","COD-PRO-CMR-CD","FEC-INI-VIG-FF","FEC-FIN-VIG-FF",
        "COD-DIR-CD","TIP-DIR-PRO-CD","COD-DIR-SN","TIP-DIR-CD","FEC-INI-ITV-FF","FEC-FIN-ITV-FF","MOT-EST-CMR-CD",
        "COD-EST-CLI-CD","NOM-MOT-NO","IND-BLO-IN","COD-CTA-CD","COD-CLI-TIT-CD","COD-CTA-TIT-CD"})
public class CustomerDetailResponseBody {

    @JsonProperty("COD-CLI-CD")
    private String codCli;
    @JsonProperty("NOM-DS")
    private String nomCli;
    @JsonProperty("PRI-APE-DS")
    private String priApeCli;
    @JsonProperty("SEG-APE-DS")
    private String segApeCli;
    @JsonProperty("COD-TIP-COT-CD")
    private String codTipCot;
    @JsonProperty("COD-ITM-REF-CD")
    private String codItmRef;
    @JsonProperty("TIP-CLI-CD")
    private String tipCli;
    @JsonProperty("NOM-TIP-CLI-NO")
    private String nomTipCli;
    @JsonProperty("COD-EST-CD")
    private String codEst;
    @JsonProperty("NOM-EST-NO")
    private String nomEst;
    @JsonProperty("COD-EMP-CD")
    private String codEmp;
    @JsonProperty("NOM-EMP-NO")
    private String nomEmp;
    @JsonProperty("NUM-DOC-CLI-CD")
    private String numDocCli;
    @JsonProperty("DIG-CTL-DOC-CD")
    private String digCtlDoc;
    @JsonProperty("TIP-DOC-CD")
    private String tipDoc;
    @JsonProperty("COD-SGM-CLI-CD")
    private String codSgm;
    @JsonProperty("NOM-SGM-CLI-NO")
    private String nomSgm;
    @JsonProperty("COD-SBG-CLI-CD")
    private String codSbg;
    @JsonProperty("NOM-SBG-CLI-NO")
    private String nomSbg;
    @JsonProperty("CLI-GRU-IN")
    private String cliGru;
    @JsonProperty("BLO-VIN-HHO-IN")
    private String bloVin;
    @JsonProperty("COD-HHO-CD")
    private String codHho;
    @JsonProperty("NUM-IDE-NU")
    private String numIdeNu;
    @JsonProperty("TIP-PRO-CMR-CD")
    private String tipProCmr;
    @JsonProperty("NOM-TIP-PRO-NO")
    private String nomTipPro;
    @JsonProperty("COD-PRO-CMR-CD")
    private String codProCmr;
    @JsonProperty("FEC-INI-VIG-FF")
    private String fecIniVig;
    @JsonProperty("FEC-FIN-VIG-FF")
    private String fecFinVig;
    @JsonProperty("COD-DIR-CD")
    private String codDirCd;
    @JsonProperty("TIP-DIR-PRO-CD")
    private String tipDirProCd;
    @JsonProperty("COD-DIR-SN")
    private String codDirSn;
    @JsonProperty("TIP-DIR-CD")
    private String tipDirCd;
    @JsonProperty("FEC-INI-ITV-FF")
    private String fecIniItv;
    @JsonProperty("FEC-FIN-ITV-FF")
    private String fecFinItv;
    @JsonProperty("MOT-EST-CMR-CD")
    private String motEstCmr;
    @JsonProperty("COD-EST-CLI-CD")
    private String codEstCli;
    @JsonProperty("NOM-MOT-NO")
    private String nomMot;
    @JsonProperty("IND-BLO-IN")
    private String indBlo;
    @JsonProperty("COD-CTA-CD")
    private String codCta;
    @JsonProperty("COD-CLI-TIT-CD")
    private String codCliTit;
    @JsonProperty("COD-CTA-TIT-CD")
    private String codCtaTit;

    public String getCodCli() {
        return codCli;
    }

    public void setCodCli(String codCli) {
        this.codCli = codCli;
    }

    public String getNomCli() {
        return nomCli;
    }

    public void setNomCli(String nomCli) {
        this.nomCli = nomCli;
    }

    public String getPriApeCli() {
        return priApeCli;
    }

    public void setPriApeCli(String priApeCli) {
        this.priApeCli = priApeCli;
    }

    public String getSegApeCli() {
        return segApeCli;
    }

    public void setSegApeCli(String segApeCli) {
        this.segApeCli = segApeCli;
    }

    public String getCodTipCot() {
        return codTipCot;
    }

    public void setCodTipCot(String codTipCot) {
        this.codTipCot = codTipCot;
    }

    public String getCodItmRef() {
        return codItmRef;
    }

    public void setCodItmRef(String codItmRef) {
        this.codItmRef = codItmRef;
    }

    public String getTipCli() {
        return tipCli;
    }

    public void setTipCli(String tipCli) {
        this.tipCli = tipCli;
    }

    public String getNomTipCli() {
        return nomTipCli;
    }

    public void setNomTipCli(String nomTipCli) {
        this.nomTipCli = nomTipCli;
    }

    public String getCodEst() {
        return codEst;
    }

    public void setCodEst(String codEst) {
        this.codEst = codEst;
    }

    public String getNomEst() {
        return nomEst;
    }

    public void setNomEst(String nomEst) {
        this.nomEst = nomEst;
    }

    public String getCodEmp() {
        return codEmp;
    }

    public void setCodEmp(String codEmp) {
        this.codEmp = codEmp;
    }

    public String getNomEmp() {
        return nomEmp;
    }

    public void setNomEmp(String nomEmp) {
        this.nomEmp = nomEmp;
    }

    public String getNumDocCli() {
        return numDocCli;
    }

    public void setNumDocCli(String numDocCli) {
        this.numDocCli = numDocCli;
    }

    public String getDigCtlDoc() {
        return digCtlDoc;
    }

    public void setDigCtlDoc(String digCtlDoc) {
        this.digCtlDoc = digCtlDoc;
    }

    public String getTipDoc() {
        return tipDoc;
    }

    public void setTipDoc(String tipDoc) {
        this.tipDoc = tipDoc;
    }

    public String getCodSgm() {
        return codSgm;
    }

    public void setCodSgm(String codSgm) {
        this.codSgm = codSgm;
    }

    public String getNomSgm() {
        return nomSgm;
    }

    public void setNomSgm(String nomSgm) {
        this.nomSgm = nomSgm;
    }

    public String getCodSbg() {
        return codSbg;
    }

    public void setCodSbg(String codSbg) {
        this.codSbg = codSbg;
    }

    public String getNomSbg() {
        return nomSbg;
    }

    public void setNomSbg(String nomSbg) {
        this.nomSbg = nomSbg;
    }

    public String getCliGru() {
        return cliGru;
    }

    public void setCliGru(String cliGru) {
        this.cliGru = cliGru;
    }

    public String getBloVin() {
        return bloVin;
    }

    public void setBloVin(String bloVin) {
        this.bloVin = bloVin;
    }

    public String getCodHho() {
        return codHho;
    }

    public void setCodHho(String codHho) {
        this.codHho = codHho;
    }

    public String getNumIdeNu() {
        return numIdeNu;
    }

    public void setNumIdeNu(String numIdeNu) {
        this.numIdeNu = numIdeNu;
    }

    public String getTipProCmr() {
        return tipProCmr;
    }

    public void setTipProCmr(String tipProCmr) {
        this.tipProCmr = tipProCmr;
    }

    public String getNomTipPro() {
        return nomTipPro;
    }

    public void setNomTipPro(String nomTipPro) {
        this.nomTipPro = nomTipPro;
    }

    public String getCodProCmr() {
        return codProCmr;
    }

    public void setCodProCmr(String codProCmr) {
        this.codProCmr = codProCmr;
    }

    public String getFecIniVig() {
        return fecIniVig;
    }

    public void setFecIniVig(String fecIniVig) {
        this.fecIniVig = fecIniVig;
    }

    public String getFecFinVig() {
        return fecFinVig;
    }

    public void setFecFinVig(String fecFinVig) {
        this.fecFinVig = fecFinVig;
    }

    public String getCodDirCd() {
        return codDirCd;
    }

    public void setCodDirCd(String codDirCd) {
        this.codDirCd = codDirCd;
    }

    public String getTipDirProCd() {
        return tipDirProCd;
    }

    public void setTipDirProCd(String tipDirProCd) {
        this.tipDirProCd = tipDirProCd;
    }

    public String getCodDirSn() {
        return codDirSn;
    }

    public void setCodDirSn(String codDirSn) {
        this.codDirSn = codDirSn;
    }

    public String getTipDirCd() {
        return tipDirCd;
    }

    public void setTipDirCd(String tipDirCd) {
        this.tipDirCd = tipDirCd;
    }

    public String getFecIniItv() {
        return fecIniItv;
    }

    public void setFecIniItv(String fecIniItv) {
        this.fecIniItv = fecIniItv;
    }

    public String getFecFinItv() {
        return fecFinItv;
    }

    public void setFecFinItv(String fecFinItv) {
        this.fecFinItv = fecFinItv;
    }

    public String getMotEstCmr() {
        return motEstCmr;
    }

    public void setMotEstCmr(String motEstCmr) {
        this.motEstCmr = motEstCmr;
    }

    public String getCodEstCli() {
        return codEstCli;
    }

    public void setCodEstCli(String codEstCli) {
        this.codEstCli = codEstCli;
    }

    public String getNomMot() {
        return nomMot;
    }

    public void setNomMot(String nomMot) {
        this.nomMot = nomMot;
    }

    public String getIndBlo() {
        return indBlo;
    }

    public void setIndBlo(String indBlo) {
        this.indBlo = indBlo;
    }

    public String getCodCta() {
        return codCta;
    }

    public void setCodCta(String codCta) {
        this.codCta = codCta;
    }

    public String getCodCliTit() {
        return codCliTit;
    }

    public void setCodCliTit(String codCliTit) {
        this.codCliTit = codCliTit;
    }

    public String getCodCtaTit() {
        return codCtaTit;
    }

    public void setCodCtaTit(String codCtaTit) {
        this.codCtaTit = codCtaTit;
    }
}
