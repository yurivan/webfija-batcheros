package pe.com.tdp.ventafija.microservices.common.clients.experto;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

public class ExpertoClient extends AbstractClient<OffersApiExpertoRequestBody, OffersApiExpertoResponseBody> {

	public ExpertoClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCode() {
		return "EXPERTO";
	}

	@Override
	protected ApiResponse<OffersApiExpertoResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<OffersApiExpertoResponseBody> objExperto = mapper.readValue(json, new TypeReference<ApiResponse<OffersApiExpertoResponseBody>>() {
			});
		return objExperto;
	}
}
