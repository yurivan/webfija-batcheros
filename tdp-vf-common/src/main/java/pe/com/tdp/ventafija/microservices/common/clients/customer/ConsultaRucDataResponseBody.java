package pe.com.tdp.ventafija.microservices.common.clients.customer;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "razSocial",
        "estContribuyente",
        "conDomicilio",
        "ubigeo",
        "tipVia",
        "nomVia",
        "codZona",
        "tipZona",
        "numero",
        "interior",
        "lote",
        "departamento",
        "manzana",
        "kilometro",
        "codError",
        "desError",
        "codTipError",
        "desTipError"
})
public class ConsultaRucDataResponseBody {

    @JsonProperty("razSocial")
    private String razSocial;
    @JsonProperty("estContribuyente")
    private String estContribuyente;
    @JsonProperty("conDomicilio")
    private String conDomicilio;
    @JsonProperty("ubigeo")
    private String ubigeo;
    @JsonProperty("tipVia")
    private String tipVia;
    @JsonProperty("nomVia")
    private String nomVia;
    @JsonProperty("codZona")
    private String codZona;
    @JsonProperty("tipZona")
    private String tipZona;
    @JsonProperty("numero")
    private String numero;
    @JsonProperty("interior")
    private String interior;
    @JsonProperty("lote")
    private String lote;
    @JsonProperty("departamento")
    private String departamento;
    @JsonProperty("manzana")
    private String manzana;
    @JsonProperty("kilometro")
    private String kilometro;
    @JsonProperty("codError")
    private String codError;
    @JsonProperty("desError")
    private String desError;
    @JsonProperty("codTipError")
    private String codTipError;
    @JsonProperty("desTipError")
    private String desTipError;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("razSocial")
    public String getRazSocial() {
        return razSocial;
    }

    @JsonProperty("razSocial")
    public void setRazSocial(String razSocial) {
        this.razSocial = razSocial;
    }

    @JsonProperty("estContribuyente")
    public String getEstContribuyente() {
        return estContribuyente;
    }

    @JsonProperty("estContribuyente")
    public void setEstContribuyente(String estContribuyente) {
        this.estContribuyente = estContribuyente;
    }

    @JsonProperty("conDomicilio")
    public String getConDomicilio() {
        return conDomicilio;
    }

    @JsonProperty("conDomicilio")
    public void setConDomicilio(String conDomicilio) {
        this.conDomicilio = conDomicilio;
    }

    @JsonProperty("ubigeo")
    public String getUbigeo() {
        return ubigeo;
    }

    @JsonProperty("ubigeo")
    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    @JsonProperty("tipVia")
    public String getTipVia() {
        return tipVia;
    }

    @JsonProperty("tipVia")
    public void setTipVia(String tipVia) {
        this.tipVia = tipVia;
    }

    @JsonProperty("nomVia")
    public String getNomVia() {
        return nomVia;
    }

    @JsonProperty("nomVia")
    public void setNomVia(String nomVia) {
        this.nomVia = nomVia;
    }

    @JsonProperty("codZona")
    public String getCodZona() {
        return codZona;
    }

    @JsonProperty("codZona")
    public void setCodZona(String codZona) {
        this.codZona = codZona;
    }

    @JsonProperty("tipZona")
    public String getTipZona() {
        return tipZona;
    }

    @JsonProperty("tipZona")
    public void setTipZona(String tipZona) {
        this.tipZona = tipZona;
    }

    @JsonProperty("numero")
    public String getNumero() {
        return numero;
    }

    @JsonProperty("numero")
    public void setNumero(String numero) {
        this.numero = numero;
    }

    @JsonProperty("interior")
    public String getInterior() {
        return interior;
    }

    @JsonProperty("interior")
    public void setInterior(String interior) {
        this.interior = interior;
    }

    @JsonProperty("lote")
    public String getLote() {
        return lote;
    }

    @JsonProperty("lote")
    public void setLote(String lote) {
        this.lote = lote;
    }

    @JsonProperty("departamento")
    public String getDepartamento() {
        return departamento;
    }

    @JsonProperty("departamento")
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    @JsonProperty("manzana")
    public String getManzana() {
        return manzana;
    }

    @JsonProperty("manzana")
    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @JsonProperty("kilometro")
    public String getKilometro() {
        return kilometro;
    }

    @JsonProperty("kilometro")
    public void setKilometro(String kilometro) {
        this.kilometro = kilometro;
    }

    @JsonProperty("codError")
    public String getCodError() {
        return codError;
    }

    @JsonProperty("codError")
    public void setCodError(String codError) {
        this.codError = codError;
    }

    @JsonProperty("desError")
    public String getDesError() {
        return desError;
    }

    @JsonProperty("desError")
    public void setDesError(String desError) {
        this.desError = desError;
    }

    @JsonProperty("codTipError")
    public String getCodTipError() {
        return codTipError;
    }

    @JsonProperty("codTipError")
    public void setCodTipError(String codTipError) {
        this.codTipError = codTipError;
    }

    @JsonProperty("desTipError")
    public String getDesTipError() {
        return desTipError;
    }

    @JsonProperty("desTipError")
    public void setDesTipError(String desTipError) {
        this.desTipError = desTipError;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}