package pe.com.tdp.ventafija.microservices.common.clients.sms;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequest;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequestHeader;
import pe.com.tdp.ventafija.microservices.common.connection.Api2;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.domain.Cip2.ConsultarCipRequest;
import pe.com.tdp.ventafija.microservices.domain.Cip2.ConsultarCipResponse;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipRequest;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipResponse;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.concurrent.TimeoutException;

public abstract class AbstractClientIVR3<T, R> {
    public static final String SERVICE_CODE_CONSULTA_ESTADO_CMS = "ESTADOCMS";
    public static final String SERVICE_CODE_CONSULTA_ESTADO_ATIS = "ESTADOATIS";
    public static final String SERVICE_CODE_RENIEC = "RENIEC";
    public static final String SERVICE_CODE_BIOMETRIC = "BIOMETRIC";
    public static final String SERVICE_CODE_HDEC = "HDEC";

    protected ClientConfig config;
    protected ServiceCallEvent event;

    protected AbstractClientIVR3(ClientConfig config) {
        super();
        this.config = config;
    }


    public ClientResult<ConsultarCipResponse> postCIP(ConsultarCipRequest body, String token) {
        ClientResult<ConsultarCipResponse> result = new ClientResult<>();

        try {

            String json = doRequestCip(body,token);
            String[] error;
            if(json.substring(0,2).equals("-1")){
                error = json.split("\\$");
                TimeoutException te = new TimeoutException();
                result.setE(new ClientException(event,error[1], te));
                result.setSuccess(false);
                result.setEvent(event);
                return result;
            }

            ConsultarCipResponse cipResponse = getResponseCip(json);
            result.setResult(cipResponse);
            result.setSuccess(true);
        } catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            /*if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse(getEvent().getMsg());
            }
            result.setEvent(event);*/
        }
        return result;
    }





    protected String doRequestCip(ConsultarCipRequest body,String token) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        //getEvent().setServiceRequest(body.toString());
        String jsonSMS = Api2.jerseyPOSTCIP(uri, body, token);
        //getEvent().setServiceCode("CONSULTAR_CIP");
        if(jsonSMS.equals("-1")){
            /*getEvent().setServiceRequest(body.toString());
            getEvent().setServiceResponse("Error de Conexion con el Servicio --> " + uri + "| Data : " + body);
            getEvent().setResult("ERROR");
            getEvent().setMsg("Caida_TimeOut_CIP");*/
            return jsonSMS + "$Error de Conexion con el Servicio --> " + uri;
        }
        /*getEvent().setServiceResponse(jsonSMS);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");*/
        return jsonSMS;
    }

    protected ApiRequest<T> getApiRequest(T body) {
        ApiRequest<T> apiRequest = new ApiRequest<>();
        ApiRequestHeader apiRequestHeader = getHeader(config.getOperation(), config.getDestination());
        apiRequest.setHeaderIn(apiRequestHeader);
        apiRequest.setBodyIn(body);

        String jsonRequest;
        try {
            jsonRequest = new ObjectMapper().writeValueAsString(apiRequest);
        } catch (JsonProcessingException e1) {
            jsonRequest = "N/A";
        }

        getEvent().setServiceRequest(jsonRequest);

        return apiRequest;
    }

    protected ConsultarCipRequest getApiRequest2(ConsultarCipRequest body) {

        String jsonRequest;
        try {
            jsonRequest = new ObjectMapper().writeValueAsString(body);
        } catch (JsonProcessingException e1) {
            jsonRequest = "N/A";
        }

        getEvent().setServiceRequest(jsonRequest);

        return body;
    }

    protected ApiRequestHeader getHeader(String API_REQUEST_HEADER_OPERATION, String API_REQUEST_HEADER_DESTINATION) {
        ApiRequestHeader apiRequestHeader = new ApiRequestHeader();
        apiRequestHeader.setCountry(Constants.API_REQUEST_HEADER_COUNTRY);
        apiRequestHeader.setLang(Constants.API_REQUEST_HEADER_LANG);
        apiRequestHeader.setEntity(Constants.API_REQUEST_HEADER_ENTITY);
        apiRequestHeader.setSystem(Constants.API_REQUEST_HEADER_SYSTEM_IVR);
        apiRequestHeader.setSubsystem(Constants.API_REQUEST_HEADER_SUBSYSTEM_IVR);
        apiRequestHeader.setOriginator(Constants.API_REQUEST_HEADER_ORIGINATOR_IVR);
        apiRequestHeader.setSender(Constants.API_REQUEST_HEADER_SENDER);
        apiRequestHeader.setUserId(Constants.API_REQUEST_HEADER_USER_ID_IVR);
        apiRequestHeader.setWsId(Constants.API_REQUEST_HEADER_WS_ID_IVR);
        apiRequestHeader.setWsIp(Constants.API_REQUEST_HEADER_WS_IP);
        apiRequestHeader.setOperation(API_REQUEST_HEADER_OPERATION);
        apiRequestHeader.setDestination(API_REQUEST_HEADER_DESTINATION);
        apiRequestHeader.setExecId(Constants.API_REQUEST_HEADER_EXEC_ID_IVR);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));
        apiRequestHeader.setTimestamp(sdf.format(new java.util.Date()) + "-05:00");
        apiRequestHeader.setMsgType(Constants.API_REQUEST_HEADER_MSG_TYPE);

        return apiRequestHeader;
    }

    protected ServiceCallEvent getEvent() {
        if (event == null) {
            event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
            event.setServiceUrl(config.getUrl());
            event.setServiceCode(getServiceCode());
        }
        return event;
    }

    protected abstract String getServiceCode();

    protected abstract ConsultarCipResponse getResponseCip(String json) throws Exception;
}
