package pe.com.tdp.ventafija.microservices.common.clients.automatizadorsale;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

import java.io.IOException;

public class AutomatizadorSaleClient extends AbstractClient<AutomatizadorSaleRequestBody, AutomatizadorSaleResponseBody> {

	public AutomatizadorSaleClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCode() {
		return "AUTOMATIZADOR";
	}

	@Override
	protected ApiResponse<AutomatizadorSaleResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<AutomatizadorSaleResponseBody> objAtis = mapper.readValue(json, new TypeReference<ApiResponse<AutomatizadorSaleResponseBody>>() {});
		return objAtis;
	}

}
