package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"COD-CLI-CD","COD-CTA-CD"})
public class CustomerReceiptsRequestBody {

    String codCliente;
    String codCuenta;

    @JsonProperty("COD-CLI-CD")
    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }

    @JsonProperty("COD-CTA-CD")
    public String getCodCuenta() {
        return codCuenta;
    }

    public void setCodCuenta(String codCuenta) {
        this.codCuenta = codCuenta;
    }
}
