package pe.com.tdp.ventafija.microservices.common.context;

import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

public class VentaFijaContextImpl implements VentaFijaContext {
	private static final long serialVersionUID = 1L;
	private  ServiceCallEvent event;

	@Override
	public void setServiceCallEvent(ServiceCallEvent event) {
		this.event = event;
	}

	@Override
	public ServiceCallEvent getServiceCallEvent() {
		return event;
	}

}
