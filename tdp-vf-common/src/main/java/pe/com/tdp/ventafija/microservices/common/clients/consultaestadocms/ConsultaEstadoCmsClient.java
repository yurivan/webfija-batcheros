package pe.com.tdp.ventafija.microservices.common.clients.consultaestadocms;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

public class ConsultaEstadoCmsClient extends AbstractClient<DuplicateApiCmsRequestBody, DuplicateApiCmsResponseBody>{

	@Override
	protected String getServiceCode() {
		return SERVICE_CODE_CONSULTA_ESTADO_CMS;
	}

	@Override
	protected ApiResponse<DuplicateApiCmsResponseBody> getResponse(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		ApiResponse<DuplicateApiCmsResponseBody> objCms = mapper.readValue(json, new TypeReference<ApiResponse<DuplicateApiCmsResponseBody>>() {});
		return objCms;
	}

	public ConsultaEstadoCmsClient(ClientConfig config) {
		super(config);
	}

}
