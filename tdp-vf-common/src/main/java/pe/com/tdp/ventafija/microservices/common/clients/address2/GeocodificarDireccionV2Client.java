package pe.com.tdp.ventafija.microservices.common.clients.address2;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

import java.io.IOException;

public class GeocodificarDireccionV2Client extends AbstractClient<BodyInGeoV2,BodyOutGeoV2> {
    public GeocodificarDireccionV2Client(ClientConfig config) {
        super(config);
    }

    @Override
    protected String getServiceCode() {
        return "NORMALIZADORV2";
    }

    @Override
    protected ApiResponse<BodyOutGeoV2> getResponse(String json) throws JsonParseException, JsonMappingException, IOException{
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json,
                new TypeReference<ApiResponse<BodyOutGeoV2>>() {
                });
    }

    public BodyOutGeoV2 getResponseGeneric(String json) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json,
                new TypeReference<BodyOutGeoV2>() {
                });
    }
}
