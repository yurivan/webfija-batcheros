package pe.com.tdp.ventafija.microservices.common.clients.Cip;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.sms.AbstractClientIVR2;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipRequest;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipResponse;

import java.io.IOException;

public class GenerarCip extends AbstractClientIVR2<GenerarCipRequest, GenerarCipResponse> {
    @Override
    protected String getServiceCode() {
        return "";
    }

    @Override
    protected GenerarCipResponse getResponseCip(String json)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        GenerarCipResponse objCms = mapper.readValue(json, new TypeReference<GenerarCipResponse>() {});
        return objCms;
    }

    public GenerarCip(ClientConfig config) {
        super(config);
    }
}