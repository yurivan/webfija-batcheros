package pe.com.tdp.ventafija.microservices.common.clients.avve;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

public class AvveClient extends AbstractClient<OffersApiAvveRequestBody, OffersApiAvveResponseBody> {

	public AvveClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCode() {
		return "AVVE";
	}

	@Override
	protected ApiResponse<OffersApiAvveResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json, 
	    		new TypeReference<ApiResponse<OffersApiAvveResponseBody>>() {
	    	});
	}

}
