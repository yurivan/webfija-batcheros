package pe.com.tdp.ventafija.microservices.common.clients.hdec;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.dto.HdecApiHdecResponseBody;

import java.io.IOException;

/**
 * Cliente Hdec para simular respuestas con error
 *
 */
public class HdecClientErrorSimulator extends HdecClient {
    private String jsonErrorResponse;

    public HdecClientErrorSimulator(ClientConfig config, String jsonErrorResponse) {
        super(config);
        this.jsonErrorResponse = jsonErrorResponse;
    }

    @Override
    protected ApiResponse<HdecApiHdecResponseBody> getResponse(String json)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        // usamos "jsonErrorResponse" en vez de "json"
        ApiResponse<HdecApiHdecResponseBody> objCms = mapper.readValue(jsonErrorResponse, new TypeReference<ApiResponse<HdecApiHdecResponseBody>>() {});
        return objCms;
    }
}
