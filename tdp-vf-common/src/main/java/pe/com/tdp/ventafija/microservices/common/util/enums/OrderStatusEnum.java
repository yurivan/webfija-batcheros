package pe.com.tdp.ventafija.microservices.common.util.enums;

public enum OrderStatusEnum {
	PRE_EFECTIVO("PRE EFECTIVO"), 
	EFECTIVO("EFECTIVO"), 
	NO_EFECTIVO("NO EFECTIVO"),
	AUDIO_FALLIDO("AUDIO FALLIDO");

	private String code;
	
	private OrderStatusEnum (String code) {
		this.code = code;
	}
	
	public String getCode() {
		return code;
	}
}
