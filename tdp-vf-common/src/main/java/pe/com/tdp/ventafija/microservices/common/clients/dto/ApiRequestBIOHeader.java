package pe.com.tdp.ventafija.microservices.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"userLogin", "serviceChannel", "sessionCode", "application", "idMessage", "ipAddress",
        "functionalityCode", "transactionTimestamp", "serviceName", "version"})
public class ApiRequestBIOHeader {

    private String userLogin;
    private String serviceChannel;
    private String sessionCode;
    private String application;
    private String idMessage;
    private String ipAddress;
    private String functionalityCode;
    private String transactionTimestamp;
    private String serviceName;
    private String version;

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getServiceChannel() {
        return serviceChannel;
    }

    public void setServiceChannel(String serviceChannel) {
        this.serviceChannel = serviceChannel;
    }

    public String getSessionCode() {
        return sessionCode;
    }

    public void setSessionCode(String sessionCode) {
        this.sessionCode = sessionCode;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getIdMessage() {
        return idMessage;
    }

    public void setIdMessage(String idMessage) {
        this.idMessage = idMessage;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getFunctionalityCode() {
        return functionalityCode;
    }

    public void setFunctionalityCode(String functionalityCode) {
        this.functionalityCode = functionalityCode;
    }

    public String getTransactionTimestamp() {
        return transactionTimestamp;
    }

    public void setTransactionTimestamp(String transactionTimestamp) {
        this.transactionTimestamp = transactionTimestamp;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }


}
