package pe.com.tdp.ventafija.microservices.common.clients.parquecms;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "TipoDocumento", "NumeroDocumento" })
public class ServicesApiCmsRequestBody {
  private String TipoDocumento;
  private String NumeroDocumento;

  @JsonProperty("TipoDocumento")
  public String getTipoDocumento() {
    return TipoDocumento;
  }

  public void setTipoDocumento(String tipoDocumento) {
    TipoDocumento = tipoDocumento;
  }

  @JsonProperty("NumeroDocumento")
  public String getNumeroDocumento() {
    return NumeroDocumento;
  }

  public void setNumeroDocumento(String numeroDocumento) {
    NumeroDocumento = numeroDocumento;
  }

}
