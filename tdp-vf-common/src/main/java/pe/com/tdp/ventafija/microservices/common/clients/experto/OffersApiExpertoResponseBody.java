package pe.com.tdp.ventafija.microservices.common.clients.experto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseBodyFault;

public class OffersApiExpertoResponseBody {
	@JsonProperty("CodConsulta")
	private String CodConsulta;
	@JsonProperty("Accion")
	private String Accion;
	@JsonProperty("DeudaAtis")
	private String DeudaAtis;
	@JsonProperty("DeudaCms")
	private String DeudaCms;
	@JsonProperty("OperacionComercial")
	private List<OffersApiExpertoResponseBodyOperacion> OperacionComercial;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;

	public String getCodConsulta() {
		return CodConsulta;
	}

	public void setCodConsulta(String codConsulta) {
		CodConsulta = codConsulta;
	}

	public String getAccion() {
		return Accion;
	}

	public void setAccion(String accion) {
		Accion = accion;
	}

	public String getDeudaAtis() {
		return DeudaAtis;
	}

	public void setDeudaAtis(String deudaAtis) {
		DeudaAtis = deudaAtis;
	}

	public String getDeudaCms() {
		return DeudaCms;
	}

	public void setDeudaCms(String deudaCms) {
		DeudaCms = deudaCms;
	}

	public List<OffersApiExpertoResponseBodyOperacion> getOperacionComercial() {
		return OperacionComercial;
	}

	public void setOperacionComercial(List<OffersApiExpertoResponseBodyOperacion> operacionComercial) {
		OperacionComercial = operacionComercial;
	}

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

}
