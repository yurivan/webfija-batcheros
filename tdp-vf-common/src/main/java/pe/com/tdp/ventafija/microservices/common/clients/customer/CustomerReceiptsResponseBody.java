package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CustomerReceiptsResponseBody {

    @JsonProperty("IND-PGN-IN")
    private String indPgnIn;
    @JsonProperty("V2-TOT-DEU-IM")
    private String totDeu;
    @JsonProperty("V2-COD-RET-CD")
    private String codRet;
    @JsonProperty("CIC-CLI-V2-GR")
    private List<CustomerReceiptsDetailResponseBody> recibos;

    public String getIndPgnIn() {
        return indPgnIn;
    }

    public void setIndPgnIn(String indPgnIn) {
        this.indPgnIn = indPgnIn;
    }

    public String getTotDeu() {
        return totDeu;
    }

    public void setTotDeu(String totDeu) {
        this.totDeu = totDeu;
    }

    public String getCodRet() {
        return codRet;
    }

    public void setCodRet(String codRet) {
        this.codRet = codRet;
    }

    public List<CustomerReceiptsDetailResponseBody> getRecibos() {
        return recibos;
    }

    public void setRecibos(List<CustomerReceiptsDetailResponseBody> recibos) {
        this.recibos = recibos;
    }
}
