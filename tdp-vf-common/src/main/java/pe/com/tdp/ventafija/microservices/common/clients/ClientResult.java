package pe.com.tdp.ventafija.microservices.common.clients;

import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

public class ClientResult<T> {
	private ServiceCallEvent event;
	private T result;
	private boolean success;
	private ClientException e;

	public ClientResult() {
		super();
		this.success = false;
	}

	public ServiceCallEvent getEvent() {
		return event;
	}

	public void setEvent(ServiceCallEvent event) {
		this.event = event;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public ClientException getE() {
		return e;
	}

	public void setE(ClientException e) {
		this.e = e;
	}
}
