package pe.com.tdp.ventafija.microservices.common.clients.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Apellidos", "CiudadId", "CorreoElectronico", "Descripcion", "EstadoPeticionId",
		"FechaCambioEstado", "FechaModificaPeticion", "FechaRegistroPeticion", "IdUsuarioATIS", "IdVendedor",
		"NombreVendedor", "Nombres", "NumeroContacto", "NumeroDocumento", "NumeroPeticion", "ProductoId",
		"PsDescripcionPaquete", "PsMonto", "PsPaquete", "TipoDocumentoId" })
public class UpFrontRequestBody {

	@JsonProperty("Apellidos")
	private String apellidos;
	@JsonProperty("CiudadId")
	private String ciudadId;
	@JsonProperty("CorreoElectronico")
	private String correoElectronico;
	@JsonProperty("Descripcion")
	private String descripcion;
	@JsonProperty("EstadoPeticionId")
	private String estadoPeticionId;
	@JsonProperty("FechaCambioEstado")
	private String fechaCambioEstado;
	@JsonProperty("FechaModificaPeticion")
	private String fechaModificaPeticion;
	@JsonProperty("FechaRegistroPeticion")
	private String fechaRegistroPeticion;
	@JsonProperty("IdUsuarioATIS")
	private String idUsuarioATIS;
	@JsonProperty("IdVendedor")
	private String idVendedor;
	@JsonProperty("NombreVendedor")
	private String nombreVendedor;
	@JsonProperty("Nombres")
	private String nombres;
	@JsonProperty("NumeroContacto")
	private String numeroContacto;
	@JsonProperty("NumeroDocumento")
	private String numeroDocumento;
	@JsonProperty("NumeroPeticion")
	private String numeroPeticion;
	@JsonProperty("ProductoId")
	private String productoId;
	@JsonProperty("PsDescripcionPaquete")
	private String psDescripcionPaquete;
	@JsonProperty("PsMonto")
	private String psMonto;
	@JsonProperty("PsPaquete")
	private String psPaquete;
	@JsonProperty("TipoDocumentoId")
	private String tipoDocumentoId;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The apellidos
	 */
	@JsonProperty("Apellidos")
	public String getApellidos() {
		return apellidos;
	}

	/**
	 * 
	 * @param apellidos
	 *            The Apellidos
	 */
	@JsonProperty("Apellidos")
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	/**
	 * 
	 * @return The ciudadId
	 */
	@JsonProperty("CiudadId")
	public String getCiudadId() {
		return ciudadId;
	}

	/**
	 * 
	 * @param ciudadId
	 *            The CiudadId
	 */
	@JsonProperty("CiudadId")
	public void setCiudadId(String ciudadId) {
		this.ciudadId = ciudadId;
	}

	/**
	 * 
	 * @return The correoElectronico
	 */
	@JsonProperty("CorreoElectronico")
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	/**
	 * 
	 * @param correoElectronico
	 *            The CorreoElectronico
	 */
	@JsonProperty("CorreoElectronico")
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	/**
	 * 
	 * @return The descripcion
	 */
	@JsonProperty("Descripcion")
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * 
	 * @param descripcion
	 *            The Descripcion
	 */
	@JsonProperty("Descripcion")
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * 
	 * @return The estadoPeticionId
	 */
	@JsonProperty("EstadoPeticionId")
	public String getEstadoPeticionId() {
		return estadoPeticionId;
	}

	/**
	 * 
	 * @param estadoPeticionId
	 *            The EstadoPeticionId
	 */
	@JsonProperty("EstadoPeticionId")
	public void setEstadoPeticionId(String estadoPeticionId) {
		this.estadoPeticionId = estadoPeticionId;
	}

	/**
	 * 
	 * @return The fechaCambioEstado
	 */
	@JsonProperty("FechaCambioEstado")
	public String getFechaCambioEstado() {
		return fechaCambioEstado;
	}

	/**
	 * 
	 * @param fechaCambioEstado
	 *            The FechaCambioEstado
	 */
	@JsonProperty("FechaCambioEstado")
	public void setFechaCambioEstado(String fechaCambioEstado) {
		this.fechaCambioEstado = fechaCambioEstado;
	}

	/**
	 * 
	 * @return The fechaModificaPeticion
	 */
	@JsonProperty("FechaModificaPeticion")
	public String getFechaModificaPeticion() {
		return fechaModificaPeticion;
	}

	/**
	 * 
	 * @param fechaModificaPeticion
	 *            The FechaModificaPeticion
	 */
	@JsonProperty("FechaModificaPeticion")
	public void setFechaModificaPeticion(String fechaModificaPeticion) {
		this.fechaModificaPeticion = fechaModificaPeticion;
	}

	/**
	 * 
	 * @return The fechaRegistroPeticion
	 */
	@JsonProperty("FechaRegistroPeticion")
	public String getFechaRegistroPeticion() {
		return fechaRegistroPeticion;
	}

	/**
	 * 
	 * @param fechaRegistroPeticion
	 *            The FechaRegistroPeticion
	 */
	@JsonProperty("FechaRegistroPeticion")
	public void setFechaRegistroPeticion(String fechaRegistroPeticion) {
		this.fechaRegistroPeticion = fechaRegistroPeticion;
	}

	/**
	 * 
	 * @return The idUsuarioATIS
	 */
	@JsonProperty("IdUsuarioATIS")
	public String getIdUsuarioATIS() {
		return idUsuarioATIS;
	}

	/**
	 * 
	 * @param idUsuarioATIS
	 *            The IdUsuarioATIS
	 */
	@JsonProperty("IdUsuarioATIS")
	public void setIdUsuarioATIS(String idUsuarioATIS) {
		this.idUsuarioATIS = idUsuarioATIS;
	}

	/**
	 * 
	 * @return The idVendedor
	 */
	@JsonProperty("IdVendedor")
	public String getIdVendedor() {
		return idVendedor;
	}

	/**
	 * 
	 * @param idVendedor
	 *            The IdVendedor
	 */
	@JsonProperty("IdVendedor")
	public void setIdVendedor(String idVendedor) {
		this.idVendedor = idVendedor;
	}

	/**
	 * 
	 * @return The nombreVendedor
	 */
	@JsonProperty("NombreVendedor")
	public String getNombreVendedor() {
		return nombreVendedor;
	}

	/**
	 * 
	 * @param nombreVendedor
	 *            The NombreVendedor
	 */
	@JsonProperty("NombreVendedor")
	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}

	/**
	 * 
	 * @return The nombres
	 */
	@JsonProperty("Nombres")
	public String getNombres() {
		return nombres;
	}

	/**
	 * 
	 * @param nombres
	 *            The Nombres
	 */
	@JsonProperty("Nombres")
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	/**
	 * 
	 * @return The numeroContacto
	 */
	@JsonProperty("NumeroContacto")
	public String getNumeroContacto() {
		return numeroContacto;
	}

	/**
	 * 
	 * @param numeroContacto
	 *            The NumeroContacto
	 */
	@JsonProperty("NumeroContacto")
	public void setNumeroContacto(String numeroContacto) {
		this.numeroContacto = numeroContacto;
	}

	/**
	 * 
	 * @return The numeroDocumento
	 */
	@JsonProperty("NumeroDocumento")
	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * 
	 * @param numeroDocumento
	 *            The NumeroDocumento
	 */
	@JsonProperty("NumeroDocumento")
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * 
	 * @return The numeroPeticion
	 */
	@JsonProperty("NumeroPeticion")
	public String getNumeroPeticion() {
		return numeroPeticion;
	}

	/**
	 * 
	 * @param numeroPeticion
	 *            The NumeroPeticion
	 */
	@JsonProperty("NumeroPeticion")
	public void setNumeroPeticion(String numeroPeticion) {
		this.numeroPeticion = numeroPeticion;
	}

	/**
	 * 
	 * @return The productoId
	 */
	@JsonProperty("ProductoId")
	public String getProductoId() {
		return productoId;
	}

	/**
	 * 
	 * @param productoId
	 *            The ProductoId
	 */
	@JsonProperty("ProductoId")
	public void setProductoId(String productoId) {
		this.productoId = productoId;
	}

	/**
	 * 
	 * @return The psDescripcionPaquete
	 */
	@JsonProperty("PsDescripcionPaquete")
	public String getPsDescripcionPaquete() {
		return psDescripcionPaquete;
	}

	/**
	 * 
	 * @param psDescripcionPaquete
	 *            The PsDescripcionPaquete
	 */
	@JsonProperty("PsDescripcionPaquete")
	public void setPsDescripcionPaquete(String psDescripcionPaquete) {
		this.psDescripcionPaquete = psDescripcionPaquete;
	}

	/**
	 * 
	 * @return The psMonto
	 */
	@JsonProperty("PsMonto")
	public String getPsMonto() {
		return psMonto;
	}

	/**
	 * 
	 * @param psMonto
	 *            The PsMonto
	 */
	@JsonProperty("PsMonto")
	public void setPsMonto(String psMonto) {
		this.psMonto = psMonto;
	}

	/**
	 * 
	 * @return The psPaquete
	 */
	@JsonProperty("PsPaquete")
	public String getPsPaquete() {
		return psPaquete;
	}

	/**
	 * 
	 * @param psPaquete
	 *            The PsPaquete
	 */
	@JsonProperty("PsPaquete")
	public void setPsPaquete(String psPaquete) {
		this.psPaquete = psPaquete;
	}

	/**
	 * 
	 * @return The tipoDocumentoId
	 */
	@JsonProperty("TipoDocumentoId")
	public String getTipoDocumentoId() {
		return tipoDocumentoId;
	}

	/**
	 * 
	 * @param tipoDocumentoId
	 *            The TipoDocumentoId
	 */
	@JsonProperty("TipoDocumentoId")
	public void setTipoDocumentoId(String tipoDocumentoId) {
		this.tipoDocumentoId = tipoDocumentoId;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}