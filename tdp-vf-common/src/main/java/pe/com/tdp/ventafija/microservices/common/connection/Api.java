package pe.com.tdp.ventafija.microservices.common.connection;

import java.net.URI;
import java.util.Date;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.fasterxml.jackson.core.JsonParser;
import jdk.nashorn.internal.parser.JSONParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import pe.com.tdp.ventafija.microservices.common.logging.ApplicationLogMarker;

/**
 * 
 * @author hruizroj
 *
 */
public class Api {
	private static final Logger logger = LogManager.getLogger();

	private Api() {

	}

	/**
	 * 
	 * @param uri
	 * @return
	 */
	public static String jerseyGET(URI uri) {
		Date timeStamp = new Date();
		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s peticion a %s", timeStamp.getTime(), uri));
		ClientConfig config = new ClientConfig();

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(UriBuilder.fromUri(uri).build());

		String response = target.request().accept(MediaType.TEXT_PLAIN).get(String.class).toString();
		logger.info(response);
		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, response));
		return response;
	}

	public static String jerseyGET(URI uri, Map<String, String> headers) {
		Date timeStamp = new Date();
		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s peticion a %s", timeStamp.getTime(), uri));
		ClientConfig config = new ClientConfig();

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(UriBuilder.fromUri(uri).build());

		Invocation.Builder invocationBuilder = target.request();
		invocationBuilder.header("Content-Type", "application/json");
		for (Map.Entry<String, String> entry : headers.entrySet()) {
			invocationBuilder.header(entry.getKey(), entry.getValue());
		}
		//Response respons = invocationBuilder.get();
		//response.readEntity(EstadoSolicitudRequest.class);
		//String response = respons.toString();
		//JSONArray response = new JSONArray(invocationBuilder.get(String.class).toString());

		String response=invocationBuilder.get(String.class).toString();
		//Object object= invocationBuilder.get(Object.class).toString();
		//JSONArray arrayObj=null;
		//JSONParser jsonParser = null;
		//object=jsonParser.parse(data);
		//arrayObj=(JSONArray) object;
		System.out.println("Json object :: "+response);

		logger.info(response);
		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, response));
		return response;
	}
	
	public static String jerseyPOST (URI uri, Object apiRequest, Map<String, String> headers) {
		Date timeStamp = new Date();
		ObjectMapper mapper = new ObjectMapper();
		String requestJson;
		try {
			requestJson = mapper.writeValueAsString(apiRequest);
			logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s consulta a %s: %s", timeStamp.getTime(), uri, requestJson));
		} catch (JsonProcessingException e) {}
		Client client = ClientBuilder.newClient();
		client.register(new LoggingFeature());
		WebTarget target = client.target(UriBuilder.fromUri(uri).build());

		String json = "";

		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		for (Map.Entry<String, String> entry : headers.entrySet()) {
			invocationBuilder.header(entry.getKey(), entry.getValue());
		}
		invocationBuilder.header("content-type", "application/json");
		Response response = invocationBuilder.post(Entity.entity(apiRequest, MediaType.APPLICATION_JSON), Response.class);
		json = response.readEntity(String.class);

		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, json));
		return json;
	}

	/**
	 * 
	 * @param uri
	 * @param apiRequest
	 * @return
	 */
	public static String jerseyPOST(URI uri, Object apiRequest, String apiId, String apiSecret) {
		Date timeStamp = new Date();
		ObjectMapper mapper = new ObjectMapper();
		String requestJson;
		try {
			requestJson = mapper.writeValueAsString(apiRequest);
			logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s consulta a %s: %s", timeStamp.getTime(), uri, requestJson));
		} catch (JsonProcessingException e) {}
		Client client = ClientBuilder.newClient();
		client.register(new LoggingFeature());
		WebTarget target = client.target(UriBuilder.fromUri(uri).build());

		String json = "";

		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("x-ibm-client-id", apiId);
		invocationBuilder.header("x-ibm-client-secret", apiSecret);
		invocationBuilder.header("content-type", "application/json");
		Response response = invocationBuilder.post(Entity.entity(apiRequest, MediaType.APPLICATION_JSON), Response.class);
		json = response.readEntity(String.class);

		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, json));
		return json;
	}

}
