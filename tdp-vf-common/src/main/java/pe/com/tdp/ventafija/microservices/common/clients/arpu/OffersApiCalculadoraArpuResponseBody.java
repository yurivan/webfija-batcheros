package pe.com.tdp.ventafija.microservices.common.clients.arpu;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseBodyFault;

public class OffersApiCalculadoraArpuResponseBody {

	@JsonInclude(Include.NON_NULL)
	@JsonProperty("Telefono")
	private String telefono;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("Servicio")
	private String servicio;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("PaquetePsOrigen")
	private String paquetePsOrigen;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("PaqueteOrigen")
	private String paqueteOrigen;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("CantidadDecos")
	private String cantidadDecos;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("RentaTotal")
	private String rentaTotal;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("Internet")
	private String internet;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("Prioridad")
	private String prioridad;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("ProductoDestinoPs")
	private String productoDestinoPs;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("SVAs")
	private String SVAs;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("ProductoDestinoNombre")
	private String productoDestinoNombre;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("MontoFinanciando")
	private String montoFinanciando;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("CuotasFinanciado")
	private String cuotasFinanciado;
	@JsonInclude(Include.NON_NULL)
	@JsonProperty("CodigoOferta")
	private String codigoOferta;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;
	
	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getServicio() {
		return servicio;
	}

	public void setServicio(String servicio) {
		this.servicio = servicio;
	}

	public String getPaquetePsOrigen() {
		return paquetePsOrigen;
	}

	public void setPaquetePsOrigen(String paquetePsOrigen) {
		this.paquetePsOrigen = paquetePsOrigen;
	}

	public String getPaqueteOrigen() {
		return paqueteOrigen;
	}

	public void setPaqueteOrigen(String paqueteOrigen) {
		this.paqueteOrigen = paqueteOrigen;
	}

	public String getCantidadDecos() {
		return cantidadDecos;
	}

	public void setCantidadDecos(String cantidadDecos) {
		this.cantidadDecos = cantidadDecos;
	}

	public String getRentaTotal() {
		return rentaTotal;
	}

	public void setRentaTotal(String rentaTotal) {
		this.rentaTotal = rentaTotal;
	}

	public String getInternet() {
		return internet;
	}

	public void setInternet(String internet) {
		this.internet = internet;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getProductoDestinoPs() {
		return productoDestinoPs;
	}

	public void setProductoDestinoPs(String productoDestinoPs) {
		this.productoDestinoPs = productoDestinoPs;
	}

	public String getSVAs() {
		return SVAs;
	}

	public void setSVAs(String sVAs) {
		SVAs = sVAs;
	}

	public String getProductoDestinoNombre() {
		return productoDestinoNombre;
	}

	public void setProductoDestinoNombre(String productoDestinoNombre) {
		this.productoDestinoNombre = productoDestinoNombre;
	}

	public String getMontoFinanciando() {
		return montoFinanciando;
	}

	public void setMontoFinanciando(String montoFinanciando) {
		this.montoFinanciando = montoFinanciando;
	}

	public String getCuotasFinanciado() {
		return cuotasFinanciado;
	}

	public void setCuotasFinanciado(String cuotasFinanciado) {
		this.cuotasFinanciado = cuotasFinanciado;
	}

	public String getCodigoOferta() {
		return codigoOferta;
	}

	public void setCodigoOferta(String codigoOferta) {
		this.codigoOferta = codigoOferta;
	}

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

}
