package pe.com.tdp.ventafija.microservices.common.clients.sms;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

public class SMSClient extends AbstractClient<RegisterApiSmsRequestBody, RegisterApiSmsResponseBody> {

	public SMSClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCode() {
		return "SMS";
	}

	@Override
	protected ApiResponse<RegisterApiSmsResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<RegisterApiSmsResponseBody> apiResponse = mapper.readValue(json,
	            new TypeReference<ApiResponse<RegisterApiSmsResponseBody>>() {
	            });
		return apiResponse;
	}
}
