package pe.com.tdp.ventafija.microservices.common.clients.parquecms;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ServicesApiCmsResponseBodyDetail {

  @JsonProperty("CodigoAbonado")
  private String CodigoAbonado;
  @JsonProperty("CodigoServicio")
  private String CodigoServicio;
  @JsonProperty("Estado")
  private String Estado;
  @JsonProperty("TipoOrigen")
  private String TipoOrigen;
  @JsonProperty("CoordenadaX")
  private String CoordenadaX;
  @JsonProperty("CoordenadaY")
  private String CoordenadaY;
  @JsonProperty("Departamento")
  private String Departamento;
  @JsonProperty("Provincia")
  private String Provincia;
  @JsonProperty("Distrito")
  private String Distrito;
  @JsonProperty("Direccion")
  private String Direccion;
  @JsonProperty("CodigoProducto")
  private String CodigoProducto;
  @JsonProperty("DescripcionProducto")
  private String DescripcionProducto;

  public String getCodigoAbonado() {
    return CodigoAbonado;
  }

  public void setCodigoAbonado(String codigoAbonado) {
    CodigoAbonado = codigoAbonado;
  }

  public String getCodigoServicio() {
    return CodigoServicio;
  }

  public void setCodigoServicio(String codigoServicio) {
    CodigoServicio = codigoServicio;
  }

  public String getEstado() {
    return Estado;
  }

  public void setEstado(String estado) {
    Estado = estado;
  }

  public String getTipoOrigen() {
    return TipoOrigen;
  }

  public void setTipoOrigen(String tipoOrigen) {
    TipoOrigen = tipoOrigen;
  }

  public String getCoordenadaX() {
    return CoordenadaX;
  }

  public void setCoordenadaX(String coordenadaX) {
    CoordenadaX = coordenadaX;
  }

  public String getCoordenadaY() {
    return CoordenadaY;
  }

  public void setCoordenadaY(String coordenadaY) {
    CoordenadaY = coordenadaY;
  }

  public String getDepartamento() {
    return Departamento;
  }

  public void setDepartamento(String departamento) {
    Departamento = departamento;
  }

  public String getProvincia() {
    return Provincia;
  }

  public void setProvincia(String provincia) {
    Provincia = provincia;
  }

  public String getDistrito() {
    return Distrito;
  }

  public void setDistrito(String distrito) {
    Distrito = distrito;
  }

  public String getDireccion() {
    return Direccion;
  }

  public void setDireccion(String direccion) {
    Direccion = direccion;
  }

  public String getCodigoProducto() {
    return CodigoProducto;
  }

  public void setCodigoProducto(String codigoProducto) {
    CodigoProducto = codigoProducto;
  }

  public String getDescripcionProducto() {
    return DescripcionProducto;
  }

  public void setDescripcionProducto(String descripcionProducto) {
    DescripcionProducto = descripcionProducto;
  }


}
