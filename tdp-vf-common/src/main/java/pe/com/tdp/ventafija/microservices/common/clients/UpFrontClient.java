package pe.com.tdp.ventafija.microservices.common.clients;

import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.UpFrontRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.UpFrontResponseBody;
import pe.com.tdp.ventafija.microservices.common.util.Constants;

public class UpFrontClient extends SimpleClientImpl<UpFrontRequestBody, UpFrontResponseBody> {
	
	public UpFrontClient (ApiHeaderConfig config) {
		super(config, config.getRegistrarUpFrontUri(), config.getRegistrarUpFrontApiId(), 
				config.getRegistrarUpFrontApiSecret(), Constants.API_REQUEST_HEADER_OPERATION_UPFRONT, 
				Constants.API_REQUEST_HEADER_DESTINATION_UPFRONT, UpFrontResponseBody.class);
	}

}
