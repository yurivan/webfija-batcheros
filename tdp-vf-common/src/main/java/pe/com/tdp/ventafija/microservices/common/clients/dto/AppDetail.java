package pe.com.tdp.ventafija.microservices.common.clients.dto;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "exceptionAppCode", "exceptionAppMessage" })
public class AppDetail {

	@JsonProperty("exceptionAppCode")
	private String exceptionAppCode;
	@JsonProperty("exceptionAppMessage")
	private String exceptionAppMessage;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The exceptionAppCode
	 */
	@JsonProperty("exceptionAppCode")
	public String getExceptionAppCode() {
		return exceptionAppCode;
	}

	/**
	 * 
	 * @param exceptionAppCode
	 *            The exceptionAppCode
	 */
	@JsonProperty("exceptionAppCode")
	public void setExceptionAppCode(String exceptionAppCode) {
		this.exceptionAppCode = exceptionAppCode;
	}

	/**
	 * 
	 * @return The exceptionAppMessage
	 */
	@JsonProperty("exceptionAppMessage")
	public String getExceptionAppMessage() {
		return exceptionAppMessage;
	}

	/**
	 * 
	 * @param exceptionAppMessage
	 *            The exceptionAppMessage
	 */
	@JsonProperty("exceptionAppMessage")
	public void setExceptionAppMessage(String exceptionAppMessage) {
		this.exceptionAppMessage = exceptionAppMessage;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}