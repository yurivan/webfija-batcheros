package pe.com.tdp.ventafija.microservices.common.clients.dto;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({ "exceptionCategory", "exceptionCode", "exceptionMsg", "exceptionDetail", "exceptionSeverity", "appDetail" })
public class ClientException {

	@JsonProperty("exceptionCategory")
	private String exceptionCategory;
	@JsonProperty("exceptionCode")
	private Integer exceptionCode;
	@JsonProperty("exceptionMsg")
	private String exceptionMsg;
	@JsonProperty("exceptionDetail")
	private String exceptionDetail;
	@JsonProperty("exceptionSeverity")
	private String exceptionSeverity;
	@JsonProperty("appDetail")
	private AppDetail appDetail;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	/**
	 * 
	 * @return The exceptionCategory
	 */
	@JsonProperty("exceptionCategory")
	public String getExceptionCategory() {
		return exceptionCategory;
	}

	/**
	 * 
	 * @param exceptionCategory
	 *            The exceptionCategory
	 */
	@JsonProperty("exceptionCategory")
	public void setExceptionCategory(String exceptionCategory) {
		this.exceptionCategory = exceptionCategory;
	}

	/**
	 * 
	 * @return The exceptionCode
	 */
	@JsonProperty("exceptionCode")
	public Integer getExceptionCode() {
		return exceptionCode;
	}

	/**
	 * 
	 * @param exceptionCode
	 *            The exceptionCode
	 */
	@JsonProperty("exceptionCode")
	public void setExceptionCode(Integer exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	/**
	 * 
	 * @return The exceptionMsg
	 */
	@JsonProperty("exceptionMsg")
	public String getExceptionMsg() {
		return exceptionMsg;
	}

	/**
	 * 
	 * @param exceptionMsg
	 *            The exceptionMsg
	 */
	@JsonProperty("exceptionMsg")
	public void setExceptionMsg(String exceptionMsg) {
		this.exceptionMsg = exceptionMsg;
	}

	/**
	 * 
	 * @return The exceptionDetail
	 */
	@JsonProperty("exceptionDetail")
	public String getExceptionDetail() {
		return exceptionDetail;
	}

	/**
	 * 
	 * @param exceptionDetail
	 *            The exceptionDetail
	 */
	@JsonProperty("exceptionDetail")
	public void setExceptionDetail(String exceptionDetail) {
		this.exceptionDetail = exceptionDetail;
	}

	/**
	 * 
	 * @return The exceptionSeverity
	 */
	@JsonProperty("exceptionSeverity")
	public String getExceptionSeverity() {
		return exceptionSeverity;
	}

	/**
	 * 
	 * @param exceptionSeverity
	 *            The exceptionSeverity
	 */
	@JsonProperty("exceptionSeverity")
	public void setExceptionSeverity(String exceptionSeverity) {
		this.exceptionSeverity = exceptionSeverity;
	}

	/**
	 * 
	 * @return The appDetail
	 */
	@JsonProperty("appDetail")
	public AppDetail getAppDetail() {
		return appDetail;
	}

	/**
	 * 
	 * @param appDetail
	 *            The appDetail
	 */
	@JsonProperty("appDetail")
	public void setAppDetail(AppDetail appDetail) {
		this.appDetail = appDetail;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}