package pe.com.tdp.ventafija.microservices.common.clients.bio;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.AbstractClientBIO;
import pe.com.tdp.ventafija.microservices.common.clients.ClientBIOConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseBIO;

import java.io.IOException;

public class BioClient extends AbstractClientBIO<ValidateCustomerIdentityRequestData, ValidateCustomerIdentityResponseData> {

    public BioClient(ClientBIOConfig config) {
        super(config);
    }

    @Override
    protected String getServiceCode() {
        return "BIOMETRIC";
    }

    @Override
    protected ApiResponseBIO<ValidateCustomerIdentityResponseData> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        ApiResponseBIO<ValidateCustomerIdentityResponseData> objBiometric= mapper.readValue(json, new TypeReference<ApiResponseBIO<ValidateCustomerIdentityResponseData>>() {});
        return objBiometric;
    }

}
