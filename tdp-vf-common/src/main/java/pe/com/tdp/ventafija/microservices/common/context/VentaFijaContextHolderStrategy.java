package pe.com.tdp.ventafija.microservices.common.context;

public interface VentaFijaContextHolderStrategy {
	void clearContext();
	VentaFijaContext getContext();
	void setContext(VentaFijaContext context);
	VentaFijaContext createEmptyContext();
}
