package pe.com.tdp.ventafija.microservices.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DuplicateApiCmsResponseBody {

	@JsonProperty("EstadoRequerimiento")
	private String EstadoRequerimiento;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;

	public String getEstadoRequerimiento() {
		return EstadoRequerimiento;
	}

	public void setEstadoRequerimiento(String estadoRequerimiento) {
		EstadoRequerimiento = estadoRequerimiento;
	}

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

}
