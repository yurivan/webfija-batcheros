package pe.com.tdp.ventafija.microservices.common.util;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Ziphelper {
	
	private static final Logger logger = LogManager.getLogger(Ziphelper.class);


	public File retriveFileFromZip(String audioName, File f) {
		File audio = null;
		ZipFile zip = null;
		///File f = null;

		FileOutputStream fos = null;
		InputStream in = null;
		try {
			logger.info(f.getAbsolutePath());
			zip = new ZipFile(f);

			zip.stream()
					.map(ZipEntry::getName)
					.forEach(System.out::println);

			ZipEntry entry = zip.getEntry(audioName);

			logger.info("entry: " + entry);
			logger.info("audioName: " + audioName);
			logger.info("entry.getName(): "  + entry.getName());

			if (entry != null) {
				in = zip.getInputStream(entry);

				logger.info("in:::");
				logger.info(in);
				logger.info("in:::");

				byte[] buffer = new byte[1024];

				audio = File.createTempFile("audio" + new Date().getTime(), ".tmp");

				int len;
				fos = new FileOutputStream(audio);

				while ((len = in.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
			}

		} catch (IOException e) {
			logger.error("io exp", e);
		} catch (Exception e) {
			logger.error("Exception.", e);
		} finally {
			try {
				if (zip != null)
					zip.close();
				if (fos != null)
					fos.close();
				if (in != null)
					in.close();
				if (f != null) {
					f.delete();
				}
			} catch (IOException e) {
				logger.info("IOException :" + e.getMessage());
			}
		}

		logger.info("audio obtenido: " + audio);
		return audio;
	}
	

}
