package pe.com.tdp.ventafija.microservices.common.clients.avve;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class OffersApiAvveRequestBody {
  @JsonInclude(Include.NON_NULL)
	private String coordenadaX;
  @JsonInclude(Include.NON_NULL)
	private String coordenadaY;
  @JsonInclude(Include.NON_NULL)
	private String telefono;
	
  public String getCoordenadaX() {
    return coordenadaX;
  }
  public void setCoordenadaX(String coordenadaX) {
    this.coordenadaX = coordenadaX;
  }
  public String getCoordenadaY() {
    return coordenadaY;
  }
  public void setCoordenadaY(String coordenadaY) {
    this.coordenadaY = coordenadaY;
  }
  public String getTelefono() {
    return telefono;
  }
  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

}
