package pe.com.tdp.ventafija.microservices.common.clients.automatizadorsale;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "NOM_DS", "PRI_APE_DS", "SEG_APE_DS", "NUM_DOC_CLI_CD","DIG_CTL_DOC_CD",
				"TIP_DOC_CD", "TEL_PRI_COT_CD", "TEL_SEG_COT_CD", "COD_ACT_ECO_CD", "COD_SAC_ECO_CD",
				"TIP_CAL_ATI_CD","NOM_CAL_DS","NUM_CAL_NU","COD_CNJ_HBL_CD","TIP_CNJ_HBL_CD", "NOM_CNJ_HBL_NO",
				"PRI_TIP_CMP_CD","PRI_CMP_DIR_DS","SEG_TIP_CMP_CD","SEG_CMP_DIR_DS","TRC_TIP_CMP_CD","TRC_CMP_DIR_DS",
				"CAO_TIP_CMP_CD","CAO_CMP_DIR_DS","QUI_TIP_CMP_CD","QUI_CMP_DIR_DS","SET_TIP_CMP_CD","SET_CMP_DIR_DS",
				"SPT_TIP_CMP_CD","SPT_CMP_DIR_DS","OCT_TIP_CMP_CD","OCT_CMP_DIR_DS","NOV_TIP_CMP_CD","NOV_CMP_DIR_DS",
				"DCO_TIP_CMP_CD","DCO_CMP_DIR_DS",
				"MSX_CBR_VOI_GES_IN","MSX_CBR_VOI_GIS_IN","MSX_IND_SNL_GIS_CD","MSX_IND_GPO_GIS_CD",
				"COD_FZA_VEN_CD","COD_FZA_GEN_CD","COD_CNL_VEN_CD",
				"COD_IND_SEN_CMS","COD_CAB_CMS","ID_COD_PRO_CD","PS_ADM_DEP_1","PS_ADM_DEP_2","PS_ADM_DEP_3","PS_ADM_DEP_4",
				"ID_COD_SVA_CD_1","ID_COD_SVA_CD_2","ID_COD_SVA_CD_3","ID_COD_SVA_CD_4","ID_COD_SVA_CD_5",
				"ID_COD_SVA_CD_6","ID_COD_SVA_CD_7","ID_COD_SVA_CD_8","ID_COD_SVA_CD_9","ID_COD_SVA_CD_10",
		"COD_VTA_APP_CD","FEC_VTA_FF","NUM_IDE_TLF","COD_CLI_EXT_CD","TIP_TRX_CMR_CD",
		"COD_SRV_CMS_CD","FEC_NAC_FF","COD_SEX_CD","COD_EST_CIV_CD","DES_MAI_CLI_DS",
		"COD_ARE_TE1_CD","COD_ARE_TE2_CD","COD_CIC_FAC_CD","COD_DEP_CD","COD_PRV_CD","COD_DIS_CD",
		"COD_FAC_TEC_CD","NUM_COD_X_CD","NUM_COD_Y_CD","COD_VEN_CMS_CD","COD_SVE_CMS_CD","NOM_VEN_DS",
		"REF_DIR_ENT_DS","COD_TIP_CAL_CD","DES_TIP_CAL_CD","TIP_MOD_EQU_CD","NUM_DOC_VEN_CD","NUM_TEL_VEN_DS",
		"IND_ENV_CON_CD","IND_ID_GRA_CD","MOD_ACE_VEN_CD","DET_CAN_VEN_DS","COD_REG_CD",
		"COD_CIP_CD","COD_EXP_CD","COD_ZON_VEN_CD","IND_WEB_PAR_CD","COD_STC",
		"DES_NAC_DS","COD_MOD_VEN_CD","CAN_EQU_VEN"
				})
public class AutomatizadorSaleRequestBody {
	private String ClienteNombre;
	private String ClienteApellidoPaterno;
	private String ClienteApellidoMaterno;
	private String ClienteNumeroDoc;
	private String ClienteRucDigitoControl;

	private String ClienteTipoDoc;
	private String ClienteTelefono1;
	private String ClienteTelefono2;
    private String ClienteRucActividadCodigo;
	private String ClienteRucSubActividadCodigo;

	private String AddressCalleAtis;
	private String AddressCalleNombre;
	private String AddressCalleNumero;
	private String AddressCCHHCodigo;
	private String AddressCCHHTipo;
	private String AddressCCHHNombre;

	private String AddressCCHHCompTipo1;
	private String AddressCCHHCompNombre1;
	private String AddressCCHHCompTipo2;
	private String AddressCCHHCompNombre2;
	private String AddressCCHHCompTipo3;
	private String AddressCCHHCompNombre3;
	private String AddressCCHHCompTipo4;
	private String AddressCCHHCompNombre4;
	private String AddressCCHHCompTipo5;
	private String AddressCCHHCompNombre5;

	private String AddressViaCompTipo1;
	private String AddressViaCompNombre1;
	private String AddressViaCompTipo2;
	private String AddressViaCompNombre2;
	private String AddressViaCompTipo3;
	private String AddressViaCompNombre3;
	private String AddressViaCompTipo4;
	private String AddressViaCompNombre4;
	private String AddressViaCompTipo5;
	private String AddressViaCompNombre5;

	private String OrderGisXDSL;
	private String OrderGisHFC;
	private String OrderGisTipoSenial;
	private String OrderGisGPON;

	private String UserAtis;
	private String UserFuerzaVenta;
	private String UserCanalCodigo;

	private String ProductoSenial;
	private String ProductoCabecera;
	private String ProductoCodigo;
	private String ProductoPSAdmin1;
	private String ProductoPSAdmin2;
	private String ProductoPSAdmin3;
	private String ProductoPSAdmin4;

	private String ProductoSVACodigo1;
	private String ProductoSVACodigo2;
	private String ProductoSVACodigo3;
	private String ProductoSVACodigo4;
	private String ProductoSVACodigo5;

	private String ProductoSVACodigo6;
	private String ProductoSVACodigo7;
	private String ProductoSVACodigo8;
	private String ProductoSVACodigo9;
	private String ProductoSVACodigo10;

	private String OrderId;
	private String OrderFecha;
	private String OrderParqueTelefono;
	private String ClienteCMSCodigo;
	private String OrderOperacionComercial;

	private String ClienteCMSServicio;
	private String ClienteNacimiento;
	private String ClienteSexo;
	private String ClienteCivil;
	private String ClienteEmail;

	private String ClienteTelefono1Area;
	private String ClienteTelefono2Area;
	private String ClienteCMSFactura;
	private String AddressDepartamentoCodigo;
	private String AddressProvinciaCodigo;
	private String AddressDistritoCodigo;

	private String OrderGisNTLT;
	private String OrderGPSX;
	private String OrderGPSY;
	private String UserCSMPuntoVenta;
	private String UserCMS;
	private String UserNombre;

	private String AddressReferencia;
	private String OrderCallTipo;
	private String OrderCallDesc;
	private String OrderModelo;
	private String UserDNI;
	private String UserTelefono;

	private String OrderContrato;
	private String OrderIDGrabacion;
	private String OrderModalidadAcep;
	private String OrderEntidad;
	private String OrderRegion;

	private String OrderCIP;
	private String OrderExperto;
	private String OrderZonal;
	private String OrderWebParental;
	private String OrderCodigoSTC6I;

	private String ClienteNacionalidad; //"DES_NAC_DS"
	private String OrderModoVenta;  //"COD_MOD_VEN_CD"
	private String OrderCantidadEquipos; //"CAN_EQU_VEN"
			

	@JsonProperty("NOM_DS")
	public String getClienteNombre() {
		return ClienteNombre;
	}

	public void setClienteNombre(String clienteNombre) {
		ClienteNombre = clienteNombre;
	}

	@JsonProperty("PRI_APE_DS")
	public String getClienteApellidoPaterno() {
		return ClienteApellidoPaterno;
	}

	public void setClienteApellidoPaterno(String clienteApellidoPaterno) {
		ClienteApellidoPaterno = clienteApellidoPaterno;
	}

	@JsonProperty("SEG_APE_DS")
	public String getClienteApellidoMaterno() { return ClienteApellidoMaterno; }

	public void setClienteApellidoMaterno(String clienteApellidoMaterno) {
		ClienteApellidoMaterno = clienteApellidoMaterno;
	}

	@JsonProperty("NUM_DOC_CLI_CD")
	public String getClienteNumeroDoc() {
		return ClienteNumeroDoc;
	}

	public void setClienteNumeroDoc(String clienteNumeroDoc) {
		ClienteNumeroDoc = clienteNumeroDoc;
	}

	@JsonProperty("DIG_CTL_DOC_CD")
	public String getClienteRucDigitoControl() {
		return ClienteRucDigitoControl;
	}

	public void setClienteRucDigitoControl(String clienteRucDigitoControl) {
		ClienteRucDigitoControl = clienteRucDigitoControl;
	}

	@JsonProperty("TIP_DOC_CD")
	public String getClienteTipoDoc() {
		return ClienteTipoDoc;
	}

	public void setClienteTipoDoc(String clienteTipoDoc) {
		ClienteTipoDoc = clienteTipoDoc;
	}

	@JsonProperty("TEL_PRI_COT_CD")
	public String getClienteTelefono1() {
		return ClienteTelefono1;
	}

	public void setClienteTelefono1(String clienteTelefono1) {
		ClienteTelefono1 = clienteTelefono1;
	}

	@JsonProperty("TEL_SEG_COT_CD")
	public String getClienteTelefono2() {
		return ClienteTelefono2;
	}

	public void setClienteTelefono2(String clienteTelefono2) {
		ClienteTelefono2 = clienteTelefono2;
	}

	@JsonProperty("COD_ACT_ECO_CD")
	public String getClienteRucActividadCodigo() {
		return ClienteRucActividadCodigo;
	}

	public void setClienteRucActividadCodigo(String clienteRucActividadCodigo) {
		ClienteRucActividadCodigo = clienteRucActividadCodigo;
	}

	@JsonProperty("COD_SAC_ECO_CD")
	public String getClienteRucSubActividadCodigo() {
		return ClienteRucSubActividadCodigo;
	}

	public void setClienteRucSubActividadCodigo(String clienteRucSubActividadCodigo) {
		ClienteRucSubActividadCodigo = clienteRucSubActividadCodigo;
	}

	@JsonProperty("TIP_CAL_ATI_CD")
	public String getAddressCalleAtis() {
		return AddressCalleAtis;
	}

	public void setAddressCalleAtis(String addressCalleAtis) {
		AddressCalleAtis = addressCalleAtis;
	}

	@JsonProperty("NOM_CAL_DS")
	public String getAddressCalleNombre() {
		return AddressCalleNombre;
	}

	public void setAddressCalleNombre(String addressCalleNombre) {
		AddressCalleNombre = addressCalleNombre;
	}

	@JsonProperty("NUM_CAL_NU")
	public String getAddressCalleNumero() {
		return AddressCalleNumero;
	}

	public void setAddressCalleNumero(String addressCalleNumero) {
		AddressCalleNumero = addressCalleNumero;
	}

	@JsonProperty("COD_CNJ_HBL_CD")
	public String getAddressCCHHCodigo() {
		return AddressCCHHCodigo;
	}

	public void setAddressCCHHCodigo(String addressCCHHCodigo) {
		AddressCCHHCodigo = addressCCHHCodigo;
	}

	@JsonProperty("TIP_CNJ_HBL_CD")
	public String getAddressCCHHTipo() {
		return AddressCCHHTipo;
	}

	public void setAddressCCHHTipo(String addressCCHHTipo) {
		AddressCCHHTipo = addressCCHHTipo;
	}

	@JsonProperty("NOM_CNJ_HBL_NO")
	public String getAddressCCHHNombre() {
		return AddressCCHHNombre;
	}

	public void setAddressCCHHNombre(String addressCCHHNombre) {
		AddressCCHHNombre = addressCCHHNombre;
	}

	@JsonProperty("PRI_TIP_CMP_CD")
	public String getAddressCCHHCompTipo1() {
		return AddressCCHHCompTipo1;
	}

	public void setAddressCCHHCompTipo1(String addressCCHHCompTipo1) {
		AddressCCHHCompTipo1 = addressCCHHCompTipo1;
	}

	@JsonProperty("PRI_CMP_DIR_DS")
	public String getAddressCCHHCompNombre1() {
		return AddressCCHHCompNombre1;
	}

	public void setAddressCCHHCompNombre1(String addressCCHHCompNombre1) {
		AddressCCHHCompNombre1 = addressCCHHCompNombre1;
	}

	@JsonProperty("SEG_TIP_CMP_CD")
	public String getAddressCCHHCompTipo2() {
		return AddressCCHHCompTipo2;
	}

	public void setAddressCCHHCompTipo2(String addressCCHHCompTipo2) {
		AddressCCHHCompTipo2 = addressCCHHCompTipo2;
	}

	@JsonProperty("SEG_CMP_DIR_DS")
	public String getAddressCCHHCompNombre2() {
		return AddressCCHHCompNombre2;
	}

	public void setAddressCCHHCompNombre2(String addressCCHHCompNombre2) {
		AddressCCHHCompNombre2 = addressCCHHCompNombre2;
	}

	@JsonProperty("TRC_TIP_CMP_CD")
	public String getAddressCCHHCompTipo3() {
		return AddressCCHHCompTipo3;
	}

	public void setAddressCCHHCompTipo3(String addressCCHHCompTipo3) {
		AddressCCHHCompTipo3 = addressCCHHCompTipo3;
	}

	@JsonProperty("TRC_CMP_DIR_DS")
	public String getAddressCCHHCompNombre3() {
		return AddressCCHHCompNombre3;
	}

	public void setAddressCCHHCompNombre3(String addressCCHHCompNombre3) {
		AddressCCHHCompNombre3 = addressCCHHCompNombre3;
	}

	@JsonProperty("CAO_TIP_CMP_CD")
	public String getAddressCCHHCompTipo4() {
		return AddressCCHHCompTipo4;
	}

	public void setAddressCCHHCompTipo4(String addressCCHHCompTipo4) {
		AddressCCHHCompTipo4 = addressCCHHCompTipo4;
	}

	@JsonProperty("CAO_CMP_DIR_DS")
	public String getAddressCCHHCompNombre4() {
		return AddressCCHHCompNombre4;
	}

	public void setAddressCCHHCompNombre4(String addressCCHHCompNombre4) {
		AddressCCHHCompNombre4 = addressCCHHCompNombre4;
	}

	@JsonProperty("QUI_TIP_CMP_CD")
	public String getAddressCCHHCompTipo5() {
		return AddressCCHHCompTipo5;
	}

	public void setAddressCCHHCompTipo5(String addressCCHHCompTipo5) {
		AddressCCHHCompTipo5 = addressCCHHCompTipo5;
	}

	@JsonProperty("QUI_CMP_DIR_DS")
	public String getAddressCCHHCompNombre5() {
		return AddressCCHHCompNombre5;
	}

	public void setAddressCCHHCompNombre5(String addressCCHHCompNombre5) {
		AddressCCHHCompNombre5 = addressCCHHCompNombre5;
	}

	@JsonProperty("SET_TIP_CMP_CD")
	public String getAddressViaCompTipo1() {
		return AddressViaCompTipo1;
	}

	public void setAddressViaCompTipo1(String addressViaCompTipo1) {
		AddressViaCompTipo1 = addressViaCompTipo1;
	}

	@JsonProperty("SET_CMP_DIR_DS")
	public String getAddressViaCompNombre1() {
		return AddressViaCompNombre1;
	}

	public void setAddressViaCompNombre1(String addressViaCompNombre1) {
		AddressViaCompNombre1 = addressViaCompNombre1;
	}

	@JsonProperty("SPT_TIP_CMP_CD")
	public String getAddressViaCompTipo2() {
		return AddressViaCompTipo2;
	}

	public void setAddressViaCompTipo2(String addressViaCompTipo2) {
		AddressViaCompTipo2 = addressViaCompTipo2;
	}

	@JsonProperty("SPT_CMP_DIR_DS")
	public String getAddressViaCompNombre2() {
		return AddressViaCompNombre2;
	}

	public void setAddressViaCompNombre2(String addressViaCompNombre2) {
		AddressViaCompNombre2 = addressViaCompNombre2;
	}

	@JsonProperty("OCT_TIP_CMP_CD")
	public String getAddressViaCompTipo3() {
		return AddressViaCompTipo3;
	}

	public void setAddressViaCompTipo3(String addressViaCompTipo3) {
		AddressViaCompTipo3 = addressViaCompTipo3;
	}

	@JsonProperty("OCT_CMP_DIR_DS")
	public String getAddressViaCompNombre3() {
		return AddressViaCompNombre3;
	}

	public void setAddressViaCompNombre3(String addressViaCompNombre3) {
		AddressViaCompNombre3 = addressViaCompNombre3;
	}

	@JsonProperty("NOV_TIP_CMP_CD")
	public String getAddressViaCompTipo4() {
		return AddressViaCompTipo4;
	}

	public void setAddressViaCompTipo4(String addressViaCompTipo4) {
		AddressViaCompTipo4 = addressViaCompTipo4;
	}

	@JsonProperty("NOV_CMP_DIR_DS")
	public String getAddressViaCompNombre4() {
		return AddressViaCompNombre4;
	}

	public void setAddressViaCompNombre4(String addressViaCompNombre4) {
		AddressViaCompNombre4 = addressViaCompNombre4;
	}

	@JsonProperty("DCO_TIP_CMP_CD")
	public String getAddressViaCompTipo5() {
		return AddressViaCompTipo5;
	}

	public void setAddressViaCompTipo5(String addressViaCompTipo5) {
		AddressViaCompTipo5 = addressViaCompTipo5;
	}

	@JsonProperty("DCO_CMP_DIR_DS")
	public String getAddressViaCompNombre5() {
		return AddressViaCompNombre5;
	}

	public void setAddressViaCompNombre5(String addressViaCompNombre5) {
		AddressViaCompNombre5 = addressViaCompNombre5;
	}

	@JsonProperty("MSX_CBR_VOI_GES_IN")
	public String getOrderGisXDSL() {
		return OrderGisXDSL;
	}

	public void setOrderGisXDSL(String orderGisXDSL) {
		OrderGisXDSL = orderGisXDSL;
	}

	@JsonProperty("MSX_CBR_VOI_GIS_IN")
	public String getOrderGisHFC() {
		return OrderGisHFC;
	}

	public void setOrderGisHFC(String orderGisHFC) {
		OrderGisHFC = orderGisHFC;
	}

	@JsonProperty("MSX_IND_SNL_GIS_CD")
	public String getOrderGisTipoSenial() {
		return OrderGisTipoSenial;
	}

	public void setOrderGisTipoSenial(String orderGisTipoSenial) {
		OrderGisTipoSenial = orderGisTipoSenial;
	}

	@JsonProperty("MSX_IND_GPO_GIS_CD")
	public String getOrderGisGPON() {
		return OrderGisGPON;
	}

	public void setOrderGisGPON(String orderGisGPON) {
		OrderGisGPON = orderGisGPON;
	}

	@JsonProperty("COD_FZA_VEN_CD")
	public String getUserAtis() {
		return UserAtis;
	}

	public void setUserAtis(String userAtis) {
		UserAtis = userAtis;
	}

	@JsonProperty("COD_FZA_GEN_CD")
	public String getUserFuerzaVenta() {
		return UserFuerzaVenta;
	}

	public void setUserFuerzaVenta(String userFuerzaVenta) {
		UserFuerzaVenta = userFuerzaVenta;
	}

	@JsonProperty("COD_CNL_VEN_CD")
	public String getUserCanalCodigo() {
		return UserCanalCodigo;
	}

	public void setUserCanalCodigo(String userCanalCodigo) {
		UserCanalCodigo = userCanalCodigo;
	}

	@JsonProperty("COD_IND_SEN_CMS")
	public String getProductoSenial() {
		return ProductoSenial;
	}

	public void setProductoSenial(String productoSenial) {
		ProductoSenial = productoSenial;
	}

	@JsonProperty("COD_CAB_CMS")
	public String getProductoCabecera() {
		return ProductoCabecera;
	}

	public void setProductoCabecera(String productoCabecera) {
		ProductoCabecera = productoCabecera;
	}

	@JsonProperty("ID_COD_PRO_CD")
	public String getProductoCodigo() {
		return ProductoCodigo;
	}

	public void setProductoCodigo(String productoCodigo) {
		ProductoCodigo = productoCodigo;
	}

	@JsonProperty("PS_ADM_DEP_1")
	public String getProductoPSAdmin1() {
		return ProductoPSAdmin1;
	}

	public void setProductoPSAdmin1(String productoPSAdmin1) {
		ProductoPSAdmin1 = productoPSAdmin1;
	}

	@JsonProperty("PS_ADM_DEP_2")
	public String getProductoPSAdmin2() {
		return ProductoPSAdmin2;
	}

	public void setProductoPSAdmin2(String productoPSAdmin2) {
		ProductoPSAdmin2 = productoPSAdmin2;
	}

	@JsonProperty("PS_ADM_DEP_3")
	public String getProductoPSAdmin3() {
		return ProductoPSAdmin3;
	}

	public void setProductoPSAdmin3(String productoPSAdmin3) {
		ProductoPSAdmin3 = productoPSAdmin3;
	}

	@JsonProperty("PS_ADM_DEP_4")
	public String getProductoPSAdmin4() {
		return ProductoPSAdmin4;
	}

	public void setProductoPSAdmin4(String productoPSAdmin4) {
		ProductoPSAdmin4 = productoPSAdmin4;
	}

	@JsonProperty("ID_COD_SVA_CD_1")
	public String getProductoSVACodigo1() {
		return ProductoSVACodigo1;
	}

	public void setProductoSVACodigo1(String productoSVACodigo1) {
		ProductoSVACodigo1 = productoSVACodigo1;
	}

	@JsonProperty("ID_COD_SVA_CD_2")
	public String getProductoSVACodigo2() {
		return ProductoSVACodigo2;
	}

	public void setProductoSVACodigo2(String productoSVACodigo2) {
		ProductoSVACodigo2 = productoSVACodigo2;
	}

	@JsonProperty("ID_COD_SVA_CD_3")
	public String getProductoSVACodigo3() {
		return ProductoSVACodigo3;
	}

	public void setProductoSVACodigo3(String productoSVACodigo3) {
		ProductoSVACodigo3 = productoSVACodigo3;
	}

	@JsonProperty("ID_COD_SVA_CD_4")
	public String getProductoSVACodigo4() {
		return ProductoSVACodigo4;
	}

	public void setProductoSVACodigo4(String productoSVACodigo4) {
		ProductoSVACodigo4 = productoSVACodigo4;
	}

	@JsonProperty("ID_COD_SVA_CD_5")
	public String getProductoSVACodigo5() {
		return ProductoSVACodigo5;
	}

	public void setProductoSVACodigo5(String productoSVACodigo5) {
		ProductoSVACodigo5 = productoSVACodigo5;
	}

	@JsonProperty("ID_COD_SVA_CD_6")
	public String getProductoSVACodigo6() {
		return ProductoSVACodigo6;
	}

	public void setProductoSVACodigo6(String productoSVACodigo6) {
		ProductoSVACodigo6 = productoSVACodigo6;
	}

	@JsonProperty("ID_COD_SVA_CD_7")
	public String getProductoSVACodigo7() {
		return ProductoSVACodigo7;
	}

	public void setProductoSVACodigo7(String productoSVACodigo7) {
		ProductoSVACodigo7 = productoSVACodigo7;
	}

	@JsonProperty("ID_COD_SVA_CD_8")
	public String getProductoSVACodigo8() {
		return ProductoSVACodigo8;
	}

	public void setProductoSVACodigo8(String productoSVACodigo8) {
		ProductoSVACodigo8 = productoSVACodigo8;
	}

	@JsonProperty("ID_COD_SVA_CD_9")
	public String getProductoSVACodigo9() {
		return ProductoSVACodigo9;
	}

	public void setProductoSVACodigo9(String productoSVACodigo9) {
		ProductoSVACodigo9 = productoSVACodigo9;
	}

	@JsonProperty("ID_COD_SVA_CD_10")
	public String getProductoSVACodigo10() {
		return ProductoSVACodigo10;
	}

	public void setProductoSVACodigo10(String productoSVACodigo10) {
		ProductoSVACodigo10 = productoSVACodigo10;
	}

	@JsonProperty("COD_VTA_APP_CD")
	public String getOrderId() {
		return OrderId;
	}

	public void setOrderId(String orderId) {
		OrderId = orderId;
	}

	@JsonProperty("FEC_VTA_FF")
	public String getOrderFecha() {
		return OrderFecha;
	}

	public void setOrderFecha(String orderFecha) {
		OrderFecha = orderFecha;
	}

	@JsonProperty("NUM_IDE_TLF")
	public String getOrderParqueTelefono() {
		return OrderParqueTelefono;
	}

	public void setOrderParqueTelefono(String orderParqueTelefono) {
		OrderParqueTelefono = orderParqueTelefono;
	}

	@JsonProperty("COD_CLI_EXT_CD")
	public String getClienteCMSCodigo() {
		return ClienteCMSCodigo;
	}

	public void setClienteCMSCodigo(String clienteCMSCodigo) {
		ClienteCMSCodigo = clienteCMSCodigo;
	}

	@JsonProperty("TIP_TRX_CMR_CD")
	public String getOrderOperacionComercial() {
		return OrderOperacionComercial;
	}

	public void setOrderOperacionComercial(String orderOperacionComercial) {
		OrderOperacionComercial = orderOperacionComercial;
	}

	@JsonProperty("COD_SRV_CMS_CD")
	public String getClienteCMSServicio() {
		return ClienteCMSServicio;
	}

	public void setClienteCMSServicio(String clienteCMSServicio) {
		ClienteCMSServicio = clienteCMSServicio;
	}

	@JsonProperty("FEC_NAC_FF")
	public String getClienteNacimiento() {
		return ClienteNacimiento;
	}

	public void setClienteNacimiento(String clienteNacimiento) {
		ClienteNacimiento = clienteNacimiento;
	}

	@JsonProperty("COD_SEX_CD")
	public String getClienteSexo() {
		return ClienteSexo;
	}

	public void setClienteSexo(String clienteSexo) {
		ClienteSexo = clienteSexo;
	}

	@JsonProperty("COD_EST_CIV_CD")
	public String getClienteCivil() {
		return ClienteCivil;
	}

	public void setClienteCivil(String clienteCivil) {
		ClienteCivil = clienteCivil;
	}

	@JsonProperty("DES_MAI_CLI_DS")
	public String getClienteEmail() {
		return ClienteEmail;
	}

	public void setClienteEmail(String clienteEmail) {
		ClienteEmail = clienteEmail;
	}

	@JsonProperty("COD_ARE_TE1_CD")
	public String getClienteTelefono1Area() {
		return ClienteTelefono1Area;
	}

	public void setClienteTelefono1Area(String clienteTelefono1Area) {
		ClienteTelefono1Area = clienteTelefono1Area;
	}

	@JsonProperty("COD_ARE_TE2_CD")
	public String getClienteTelefono2Area() {
		return ClienteTelefono2Area;
	}

	public void setClienteTelefono2Area(String clienteTelefono2Area) {
		ClienteTelefono2Area = clienteTelefono2Area;
	}

	@JsonProperty("COD_CIC_FAC_CD")
	public String getClienteCMSFactura() {
		return ClienteCMSFactura;
	}

	public void setClienteCMSFactura(String clienteCMSFactura) {
		ClienteCMSFactura = clienteCMSFactura;
	}

	@JsonProperty("COD_DEP_CD")
	public String getAddressDepartamentoCodigo() {
		return AddressDepartamentoCodigo;
	}

	public void setAddressDepartamentoCodigo(String addressDepartamentoCodigo) {
		AddressDepartamentoCodigo = addressDepartamentoCodigo;
	}

	@JsonProperty("COD_PRV_CD")
	public String getAddressProvinciaCodigo() {
		return AddressProvinciaCodigo;
	}

	public void setAddressProvinciaCodigo(String addressProvinciaCodigo) {
		AddressProvinciaCodigo = addressProvinciaCodigo;
	}

	@JsonProperty("COD_DIS_CD")
	public String getAddressDistritoCodigo() {
		return AddressDistritoCodigo;
	}

	public void setAddressDistritoCodigo(String addressDistritoCodigo) {
		AddressDistritoCodigo = addressDistritoCodigo;
	}

	@JsonProperty("COD_FAC_TEC_CD")
	public String getOrderGisNTLT() {
		return OrderGisNTLT;
	}

	public void setOrderGisNTLT(String orderGisNTLT) {
		OrderGisNTLT = orderGisNTLT;
	}

	@JsonProperty("NUM_COD_X_CD")
	public String getOrderGPSX() {
		return OrderGPSX;
	}

	public void setOrderGPSX(String orderGPSX) {
		OrderGPSX = orderGPSX;
	}

	@JsonProperty("NUM_COD_Y_CD")
	public String getOrderGPSY() {
		return OrderGPSY;
	}

	public void setOrderGPSY(String orderGPSY) {
		OrderGPSY = orderGPSY;
	}

	@JsonProperty("COD_VEN_CMS_CD")
	public String getUserCSMPuntoVenta() {
		return UserCSMPuntoVenta;
	}

	public void setUserCSMPuntoVenta(String userCSMPuntoVenta) {
		UserCSMPuntoVenta = userCSMPuntoVenta;
	}

	@JsonProperty("COD_SVE_CMS_CD")
	public String getUserCMS() {
		return UserCMS;
	}

	public void setUserCMS(String userCMS) {
		UserCMS = userCMS;
	}

	@JsonProperty("NOM_VEN_DS")
	public String getUserNombre() {
		return UserNombre;
	}

	public void setUserNombre(String userNombre) {
		UserNombre = userNombre;
	}

	@JsonProperty("REF_DIR_ENT_DS")
	public String getAddressReferencia() {
		return AddressReferencia;
	}

	public void setAddressReferencia(String addressReferencia) {
		AddressReferencia = addressReferencia;
	}

	@JsonProperty("COD_TIP_CAL_CD")
	public String getOrderCallTipo() {
		return OrderCallTipo;
	}

	public void setOrderCallTipo(String orderCallTipo) {
		OrderCallTipo = orderCallTipo;
	}

	@JsonProperty("DES_TIP_CAL_CD")
	public String getOrderCallDesc() {
		return OrderCallDesc;
	}

	public void setOrderCallDesc(String orderCallDesc) {
		OrderCallDesc = orderCallDesc;
	}

	@JsonProperty("TIP_MOD_EQU_CD")
	public String getOrderModelo() {
		return OrderModelo;
	}

	public void setOrderModelo(String orderModelo) {
		OrderModelo = orderModelo;
	}

	@JsonProperty("NUM_DOC_VEN_CD")
	public String getUserDNI() {
		return UserDNI;
	}

	public void setUserDNI(String userDNI) {
		UserDNI = userDNI;
	}

	@JsonProperty("NUM_TEL_VEN_DS")
	public String getUserTelefono() {
		return UserTelefono;
	}

	public void setUserTelefono(String userTelefono) {
		UserTelefono = userTelefono;
	}

	@JsonProperty("IND_ENV_CON_CD")
	public String getOrderContrato() {
		return OrderContrato;
	}

	public void setOrderContrato(String orderContrato) {
		OrderContrato = orderContrato;
	}

	@JsonProperty("IND_ID_GRA_CD")
	public String getOrderIDGrabacion() {
		return OrderIDGrabacion;
	}

	public void setOrderIDGrabacion(String orderIDGrabacion) {
		OrderIDGrabacion = orderIDGrabacion;
	}

	@JsonProperty("MOD_ACE_VEN_CD")
	public String getOrderModalidadAcep() {
		return OrderModalidadAcep;
	}

	public void setOrderModalidadAcep(String orderModalidadAcep) {
		OrderModalidadAcep = orderModalidadAcep;
	}

	@JsonProperty("DET_CAN_VEN_DS")
	public String getOrderEntidad() {
		return OrderEntidad;
	}

	public void setOrderEntidad(String orderEntidad) {
		OrderEntidad = orderEntidad;
	}

	@JsonProperty("COD_REG_CD")
	public String getOrderRegion() {
		return OrderRegion;
	}

	public void setOrderRegion(String orderRegion) {
		OrderRegion = orderRegion;
	}

	@JsonProperty("COD_CIP_CD")
	public String getOrderCIP() {
		return OrderCIP;
	}

	public void setOrderCIP(String orderCIP) {
		OrderCIP = orderCIP;
	}

	@JsonProperty("COD_EXP_CD")
	public String getOrderExperto() {
		return OrderExperto;
	}

	public void setOrderExperto(String orderExperto) {
		OrderExperto = orderExperto;
	}

	@JsonProperty("COD_ZON_VEN_CD")
	public String getOrderZonal() {
		return OrderZonal;
	}

	public void setOrderZonal(String orderZonal) {
		OrderZonal = orderZonal;
	}

	@JsonProperty("IND_WEB_PAR_CD")
	public String getOrderWebParental() {
		return OrderWebParental;
	}

	public void setOrderWebParental(String orderWebParental) {
		OrderWebParental = orderWebParental;
	}

	@JsonProperty("COD_STC")
	public String getOrderCodigoSTC6I() {
		return OrderCodigoSTC6I;
	}

	public void setOrderCodigoSTC6I(String orderCodigoSTC6I) {
		OrderCodigoSTC6I = orderCodigoSTC6I;
	}

	@JsonProperty("DES_NAC_DS")
	public String getClienteNacionalidad() {
		return ClienteNacionalidad;
	}

	public void setClienteNacionalidad(String clienteNacionalidad) {
		ClienteNacionalidad = clienteNacionalidad;
	}

	@JsonProperty("COD_MOD_VEN_CD")
	public String getOrderModoVenta() {
		return OrderModoVenta;
	}

	public void setOrderModoVenta(String orderModoVenta) {
		OrderModoVenta = orderModoVenta;
	}

	@JsonProperty("CAN_EQU_VEN")
	public String getOrderCantidadEquipos() {
		return OrderCantidadEquipos;
	}

	public void setOrderCantidadEquipos(String orderCantidadEquipos) {
		OrderCantidadEquipos = orderCantidadEquipos;
	}
}
