package pe.com.tdp.ventafija.microservices.common.constant;

public class CustomerConstants {

    private CustomerConstants() {
    }

    public static final String ESTADO_SOLICITUD_PENDIENTE = "PENDIENTE";
    public static final String ESTADO_SOLICITUD_CAIDA = "CAIDA";
    public static final String ESTADO_SOLICITUD_GENERANDO_CIP = "GENERANDO_CIP";

    public static final String PARQUE_ATIS_ACTIVE = "IC";
    public static final String PARQUE_ATIS_SUSPENDIDO = "SU";
    public static final String PARQUE_CMS_ACTIVE = "05";
    public static final String PARQUE_CMS_SUSPENDIDO = "04";

    public static final String DUPLICATE_RESPONSE_CODE_OK="00";
    public static final String DUPLICATE_RESPONSE_DATA_OK="PROCEDE";
    public static final String DUPLICATE_RESPONSE_CODE_ERROR="01";
    public static final String DUPLICATE_RESPONSE_DATA_ERROR="NO PROCEDE";
    public static final String DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME="{productoName}";
    public static final String DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE="{recordingDate}";
    public static final String DUPLICATE_RESPONSE_MESSAGE = "Ya cuenta con un "+DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME+" ingresado el "+DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE+".";
    public static final String DUPLICATE_PRODUCT_RESPONSE_MESSAGE = "Usted ya ha solicitado un "+DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME+" registrado el "+DUPLICATE_RESPONSE_MESSAGE_PARAMETER_RECORDING_DATE+".";
    public static final String DUPLICATE_PRODUCT_ATIS_MESSAGE = "Ya cuenta con un "+DUPLICATE_RESPONSE_MESSAGE_PARAMETER_PRODUCT_NAME+".";
    public static final String DUPLICATE_RESPONSE_SUS_DUE_MESSAGE = "Ya cuenta con el producto, pero se le ha suspendido por deuda.";
    public static final String DUPLICATE_RESPONSE_SUS_APC_MESSAGE = "Este producto ha sido suspendido a pedido del cliente.";
    public static final String DUPLICATE_RESPONSE_SUS_MESSAGE = "Este producto se encuentra suspendido.";

    public static final String PARAMETRO_ANIO = "[ANIO]";
    public static final String PARAMETRO_DIA = "[DIA]";
    public static final String PARAMETRO_MES = "[MES]";
    public static final String PARAMETRO_MONTO = "[MONTO]";
    public static final String PARAMETRO_PRECIO = "[PRECIO]";
    public static final String PARAMETRO_PRODUCTO = "[PRODUCTO]";
    public static final String PARAMETRO_DNI_RUC = "[DNI-RUC]";

    //Constantes para duplicado en la web venta fija
    public static final String PARAMETRO_DUO_TV = "5";
    public static final String PARAMETRO_DUO_BA = "4";
    public static final String PARAMETRO_TRIO = "3";
    public static final String PRODUCTO_TRIO = "TRIO";
    public static final String PRODUCTO_DUO = "DUO";
    //Constantes para Contratos
    public static final String PARAMETRO_VENTA_FIJA_WEB = "WEBVF";
    public static final String PARAMETRO_VENTA_FIJA_APP = "APPVF";

    public static final String PARAMETRO_COLUMNA_STR_WEB = "webvalue";
    public static final String PARAMETRO_COLUMNA_STR_APP = "appvalue";

    public static final String PARAMETRO_FLUJO_ALTA = "A";
    public static final String PARAMETRO_FLUJO_MIGRACION = "M";
    public static final String PARAMETRO_FLUJO_SVA = "S";

    public static final String PARAMETRO_CONTRACT = "CONTRACT";
    public static final String PARAMETRO_SPEECH = "SPEECH";

    public static final String PARAMETRO_TIPO_CATEGORIA_SVA = "SVA";
    public static final String PARAMETRO_TIPO_CATEGORIA_ALTA = "product_has_discount_alta_pura";
    public static final String PARAMETRO_TIPO_CATEGORIA_MIGRACION = "product_has_discount_alta_componente_migracion";
    public static final String PARAMETRO_TIPO_CATEGORIA_LEGACY = "legacy_code_alta_componente_migracion";
    public static final String PARAMETRO_TIPO_CATEGORIA_SPEED = "product_has_speed";
    public static final String PARAMETRO_TIPO_CATEGORIA_EQUIPMENTS = "has_equipments";
    public static final String PARAMETRO_TIPO_CATEGORIA_PRODUCTO = "Producto";

    public static final String PARAMETRO_PRODUCTO_1 = "product_prom_price";
    public static final String PARAMETRO_PRODUCTO_2 = "product_install_cost";
    public static final String PARAMETRO_PRODUCTO_3 = "product_price";
    public static final String PARAMETRO_PRODUCTO_4 = "product_financing_cost";
    public static final String PARAMETRO_PRODUCTO_5 = "product_cash_price";
    public static final String PARAMETRO_PRODUCTO_6 = "client_doc_number";
    public static final String PARAMETRO_PRODUCTO_7 = "atis_migration_phone";
    public static final String PARAMETRO_PRODUCTO_8 = "client_mobile_phone";
    public static final String PARAMETRO_PRODUCTO_9 = "client_secondary_phone";
    public static final String PARAMETRO_PRODUCTO_10 = "cms_service_code";
    public static final String PARAMETRO_PRODUCTO_11 = "client_names";
    public static final String PARAMETRO_PRODUCTO_12 = "client_lastname";
    public static final String PARAMETRO_PRODUCTO_13 = "client_mother_lastname";
    public static final String PARAMETRO_PRODUCTO_14 = "product_equipamiento_linea";
    public static final String PARAMETRO_PRODUCTO_15 = "product_equipamiento_internet";
    public static final String PARAMETRO_PRODUCTO_16 = "product_equipamiento_tv";

    public static final String PARAMETRO_PLANTILLA_PRODUCTO = "[PRODUCTO]";
    public static final String PARAMETRO_PLANTILLA_DESCRIPCION = "[description]";
    public static final String PARAMETRO_PLANTILLA_PRECIO_UNITARIO = "[PRECIO_UNITARIO]";
    public static final String PARAMETRO_COLUMNA_DESCRIPCION = "decripcion";
    public static final String PARAMETRO_COLUMNA_CODE = "code";
    public static final String PARAMETRO_COLUMNA_UNIT = "unit";

    public static final String PARAMETRO_COLUMNA_SVAI = "SVAI";
    public static final String PARAMETRO_COLUMNA_SVAL = "SVAL";
    public static final String PARAMETRO_COLUMNA_BTV = "BTV";

    //Sprint 24 Deuda
    public static final String API_REQUEST_HEADER_OPERATION_STATUS_CMS = "consultarValidacionBiometrica";
    public static final String API_REQUEST_HEADER_DESTINATION_CMS = "PE:TDP:CMS:IVR";
}
