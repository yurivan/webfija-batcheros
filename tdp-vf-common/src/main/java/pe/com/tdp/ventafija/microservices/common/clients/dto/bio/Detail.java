package pe.com.tdp.ventafija.microservices.common.clients.dto.bio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ValidateCustomerIdentityFault;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class Detail {

	@JsonProperty("ValidateCustomerIdentityFault")
	private ValidateCustomerIdentityFault validateCustomerIdentityFault;

	@JsonProperty("ValidateCustomerIdentityFault")
	public ValidateCustomerIdentityFault getValidateCustomerIdentityFault() {
		return validateCustomerIdentityFault;
	}

	@JsonProperty("ValidateCustomerIdentityFault")
	public void setValidateCustomerIdentityFault(ValidateCustomerIdentityFault validateCustomerIdentityFault) {
		this.validateCustomerIdentityFault = validateCustomerIdentityFault;
	}
}
