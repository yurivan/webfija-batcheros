package pe.com.tdp.ventafija.microservices.common.clients.parqueatis;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

public class ParqueAtisClient extends AbstractClient<ServicesApiAtisRequestBody, ServicesApiAtisResponseBody> {

	public ParqueAtisClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCode() {
		return "PARQUEATIS";
	}

	@Override
	protected ApiResponse<ServicesApiAtisResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<ServicesApiAtisResponseBody> objAtis = mapper.readValue(json, new TypeReference<ApiResponse<ServicesApiAtisResponseBody>>() {});
		return objAtis;
	}

}
