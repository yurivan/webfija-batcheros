
package pe.com.tdp.ventafija.microservices.common.clients.address2;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "codResponse",
        "idTransaction",
        "responseList",
        "numResponses"
})
public class BodyOutGeoV2 {

    @JsonProperty("codResponse")
    private String codResponse;
    @JsonProperty("idTransaction")
    private String idTransaction;
    @JsonProperty("responseList")
    private List<ResponseList> responseList = null;
    @JsonProperty("numResponses")
    private String numResponses;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("codResponse")
    public String getCodResponse() {
        return codResponse;
    }

    @JsonProperty("codResponse")
    public void setCodResponse(String codResponse) {
        this.codResponse = codResponse;
    }

    @JsonProperty("idTransaction")
    public String getIdTransaction() {
        return idTransaction;
    }

    @JsonProperty("idTransaction")
    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    @JsonProperty("responseList")
    public List<ResponseList> getResponseList() {
        return responseList;
    }

    @JsonProperty("responseList")
    public void setResponseList(List<ResponseList> responseList) {
        this.responseList = responseList;
    }

    @JsonProperty("numResponses")
    public String getNumResponses() {
        return numResponses;
    }

    @JsonProperty("numResponses")
    public void setNumResponses(String numResponses) {
        this.numResponses = numResponses;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
