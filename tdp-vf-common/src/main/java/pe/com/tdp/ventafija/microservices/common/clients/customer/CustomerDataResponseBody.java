package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseBodyFault;

import java.util.List;

@JsonPropertyOrder({"RPO-CLI-QUI-SN","IND-PGN-IN","LIS-CLT-CLI-GR","COD-PRO-CMR-SN","NOM-EST-USR-SN"})
public class CustomerDataResponseBody {

    @JsonProperty("RPO-CLI-QUI-SN")
    private String rpoCli;
    @JsonProperty("IND-PGN-IN")
    private String indPng;
    @JsonProperty("LIS-CLT-CLI-GR")
    private List<CustomerDetailResponseBody> Respuesta;
    @JsonProperty("COD-PRO-CMR-SN")
    private String codProCmrSn;
    @JsonProperty("NOM-EST-USR-SN")
    private String nomEstUsrSn;
    @JsonProperty("ClientException")
    private ApiResponseBodyFault ClientException;
    @JsonProperty("ServerException")
    private ApiResponseBodyFault ServerException;

    public List<CustomerDetailResponseBody> getRespuesta() {
        return Respuesta;
    }

    public void setRespuesta(List<CustomerDetailResponseBody> respuesta) {
        Respuesta = respuesta;
    }

    public ApiResponseBodyFault getClientException() {
        return ClientException;
    }

    public void setClientException(ApiResponseBodyFault clientException) {
        ClientException = clientException;
    }

    public ApiResponseBodyFault getServerException() {
        return ServerException;
    }

    public void setServerException(ApiResponseBodyFault serverException) {
        ServerException = serverException;
    }

    public String getRpoCli() {
        return rpoCli;
    }

    public void setRpoCli(String rpoCli) {
        this.rpoCli = rpoCli;
    }

    public String getIndPng() {
        return indPng;
    }

    public void setIndPng(String indPng) {
        this.indPng = indPng;
    }

    public String getCodProCmrSn() {
        return codProCmrSn;
    }

    public void setCodProCmrSn(String codProCmrSn) {
        this.codProCmrSn = codProCmrSn;
    }

    public String getNomEstUsrSn() {
        return nomEstUsrSn;
    }

    public void setNomEstUsrSn(String nomEstUsrSn) {
        this.nomEstUsrSn = nomEstUsrSn;
    }
}
