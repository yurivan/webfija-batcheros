package pe.com.tdp.ventafija.microservices.common.clients.parquecms;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseBodyFault;

public class ServicesApiCmsResponseBody {
  
  @JsonProperty("DetalleParque")
  private List<ServicesApiCmsResponseBodyDetail> DetalleParque;
  @JsonProperty("ClientException")
  private ApiResponseBodyFault ClientException;
  @JsonProperty("ServerException")
  private ApiResponseBodyFault ServerException;

  public ApiResponseBodyFault getClientException() {
    return ClientException;
  }

  public void setClientException(ApiResponseBodyFault clientException) {
    ClientException = clientException;
  }

  public ApiResponseBodyFault getServerException() {
    return ServerException;
  }

  public void setServerException(ApiResponseBodyFault serverException) {
    ServerException = serverException;
  }

  public List<ServicesApiCmsResponseBodyDetail> getDetalleParque() {
    return DetalleParque;
  }

  public void setDetalleParque(List<ServicesApiCmsResponseBodyDetail> detalleParque) {
    DetalleParque = detalleParque;
  }


}
