package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.sms.AbstractClientIVR;

import java.io.IOException;

public class ConsultaDeudaCustomer extends AbstractClientIVR<CustomerReceiptsRequestBody, CustomerReceiptsResponseBody> {
    @Override
    protected String getServiceCode() {
        return "";
    }

    @Override
    protected ApiResponse<CustomerReceiptsResponseBody> getResponse(String json)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        ApiResponse<CustomerReceiptsResponseBody> objCms = mapper.readValue(json, new TypeReference<ApiResponse<CustomerReceiptsResponseBody>>() {});
        return objCms;
    }

    public ConsultaDeudaCustomer(ClientConfig config) {
        super(config);
    }
}
