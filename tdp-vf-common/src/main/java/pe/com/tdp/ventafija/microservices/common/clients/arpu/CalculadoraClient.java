package pe.com.tdp.ventafija.microservices.common.clients.arpu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

public class CalculadoraClient extends AbstractClient<OffersApiCalculadoraArpuRequestBody, OffersApiCalculadoraArpuResponseBody> {
    public final static String CLIENTE_NO_ENCONTRADO = "2";

    public CalculadoraClient(ClientConfig config) {
        super(config);
    }

    @Override
    protected String getServiceCode() {
        return "ARPU";
    }

    @Override
    protected ApiResponse<OffersApiCalculadoraArpuResponseBody> getResponse(String json) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ApiResponse<OffersApiCalculadoraArpuResponseBody> objCalculadora = null;
        try {
            objCalculadora = mapper.readValue(json,
                    new TypeReference<ApiResponse<List<OffersApiCalculadoraArpuResponseBody>>>() {
                    });
        } catch (JsonMappingException e) {
            objCalculadora = mapper.readValue(json,
                    new TypeReference<ApiResponse<OffersApiCalculadoraArpuResponseBody>>() {
                    });
            if (objCalculadora.getHeaderOut() != null && "ERROR".equals(objCalculadora.getHeaderOut().getMsgType())) {
                objCalculadora.setBodyOut(null);
            } else {
                throw e;
            }
				/*if (CLIENTE_NO_ENCONTRADO.equals(objCalculadora.getBodyOut().getClientException().getAppDetail().getExceptionAppCode())){
					objCalculadora.setBodyOut(null);
				}else if (objCalculadora.getBodyOut() != null && objCalculadora.getBodyOut().getServerException() != null) {
					throw new Exception(objCalculadora.getBodyOut().getServerException().getExceptionDetail());
				} else {
					throw e;
				}
			} else {
				throw e;
			}*/
        }

        return objCalculadora;
    }

}
