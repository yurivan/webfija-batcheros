package pe.com.tdp.ventafija.microservices.common.clients;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequest;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequestHeader;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.dto.DuplicateApiAtisRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.DuplicateApiAtisResponseBody;
import pe.com.tdp.ventafija.microservices.common.connection.Api;
import pe.com.tdp.ventafija.microservices.common.constant.LegadosConstants;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;

public class DuplicateAtisClient implements Client<DuplicateApiAtisRequestBody, DuplicateApiAtisResponseBody> {
	private static final Logger logger = LogManager.getLogger(DuplicateAtisClient.class);
	private ApiHeaderConfig config;

	public DuplicateAtisClient(ApiHeaderConfig config) {
		this.config = config;
	}

	@Override
	public DuplicateApiAtisResponseBody sendData(DuplicateApiAtisRequestBody request) throws ApiClientException {
		URI uri;
		ApiResponse<DuplicateApiAtisResponseBody> objAtis = null;
		try {
			uri = new URI(config.getAtisConsultaEstadoUri());
			ApiRequest<DuplicateApiAtisRequestBody> apiRequest = new ApiRequest<>();

			ObjectMapper mapper = new ObjectMapper();
			ApiRequestHeaderFactory factory = new ApiRequestHeaderFactory(config);
			ApiRequestHeader headerIn = factory.getHeader();
			headerIn.setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_STATUS_ATIS);
			headerIn.setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_ATIS);
			apiRequest.setHeaderIn(headerIn);

			apiRequest.setBodyIn(request);
			
			String requestJson = mapper.writeValueAsString(apiRequest);
			logger.info(requestJson);

			String json = Api.jerseyPOST(uri, apiRequest, config.getAtisConsultarEstadoApiId(), config.getAtisConsultarEstadoApiSecret());
			
			logger.info(json);

			objAtis = mapper.readValue(json,
					new TypeReference<ApiResponse<DuplicateApiAtisResponseBody>>() {
					});
			if (ApiHeaderConfig.MSG_TYPE_RESPONSE.equals(objAtis.getHeaderOut().getMsgType())) {
				logger.info(objAtis.getBodyOut().getEstadoPeticion());
			}
		} catch (URISyntaxException e) {
			logger.error("error uri", e);
		} catch (JsonProcessingException e) {
			logger.error("error json", e);
			throw new ApiClientException("Respuesta incorrecta de servidor");
		} catch (IOException e) {
			logger.error("error io", e);
		}

		return objAtis == null ? null : objAtis.getBodyOut();
	}

}
