package pe.com.tdp.ventafija.microservices.common.clients;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.connection.Api;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseBIO;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequestBIO;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequestBIOHeader;
import pe.com.tdp.ventafija.microservices.common.util.ConstantsBIO;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Locale;

public abstract class AbstractClientBIO<T, R> {

    protected ClientBIOConfig config;
    protected ServiceCallEvent event;

    protected AbstractClientBIO(ClientBIOConfig config) {
        super();
        this.config = config;
    }

    public ClientResult<ApiResponseBIO<R>> post(T body) {
        ClientResult<ApiResponseBIO<R>> result = new ClientResult<>();

        try {
            String json = doRequest(body);
            ApiResponseBIO<R> apiResponse = getResponse(json);
            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {
            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }
        return result;
    }

    protected String doRequest(T body) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyPOST(uri, getApiRequest(body), config.getApiId(), config.getApiSecret());
        getEvent().setServiceResponse(jsonSMS);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");
        return jsonSMS;
    }

    protected ApiRequestBIO<T> getApiRequest(T body) {
        ApiRequestBIO<T> apiRequest = new ApiRequestBIO<>();
        ApiRequestBIOHeader apiRequestHeader = getHeader();
        apiRequest.setTefHeaderReq(apiRequestHeader);
        apiRequest.setValidateCustomerIdentityRequestData(body);

        String jsonRequest;
        try {
            jsonRequest = new ObjectMapper().writeValueAsString(apiRequest);
        } catch (JsonProcessingException e1) {
            jsonRequest = "N/A";
        }

        getEvent().setServiceRequest(jsonRequest);

        return apiRequest;
    }

    protected ApiRequestBIOHeader getHeader() {
        ApiRequestBIOHeader apiRequestBIOHeader = new ApiRequestBIOHeader();
        apiRequestBIOHeader.setUserLogin(config.getUserLogin());
        apiRequestBIOHeader.setServiceChannel(config.getServiceChannel());
        apiRequestBIOHeader.setSessionCode(ConstantsBIO.API_REQUEST_HEADER_SESSION_CODE);
        apiRequestBIOHeader.setApplication(ConstantsBIO.API_REQUEST_HEADER_APPLICATION);
        apiRequestBIOHeader.setIdMessage(ConstantsBIO.API_REQUEST_HEADER_ID_MESSAGE);
        apiRequestBIOHeader.setIpAddress(ConstantsBIO.API_REQUEST_HEADER_IP_ADDRESS);
        apiRequestBIOHeader.setFunctionalityCode(ConstantsBIO.API_REQUEST_HEADER_FUNCIONALITY_CODE);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));
        apiRequestBIOHeader.setTransactionTimestamp(sdf.format(new Date()) + "-05:00");
        apiRequestBIOHeader.setServiceName(ConstantsBIO.API_REQUEST_HEADER_SERVICE_NAME);
        apiRequestBIOHeader.setVersion(ConstantsBIO.API_REQUEST_HEADER_VERSION);

        return apiRequestBIOHeader;
    }

    protected ServiceCallEvent getEvent() {
        if (event == null) {
            event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
            event.setServiceUrl(config.getUrl());
            event.setServiceCode(getServiceCode());
        }
        return event;
    }

    protected abstract String getServiceCode();

    protected abstract ApiResponseBIO<R> getResponse(String json) throws Exception;
}
