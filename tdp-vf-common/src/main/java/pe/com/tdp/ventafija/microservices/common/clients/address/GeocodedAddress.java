package pe.com.tdp.ventafija.microservices.common.clients.address;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeocodedAddress {

    @JsonProperty("ubigeoGeocodificado")
    private String ubigeoGeocodificado;
    @JsonProperty("descripcionUbigeo")
    private String descripcionUbigeo;
    @JsonProperty("direccionGeocodificada")
    private String direccionGeocodificada;
    @JsonProperty("xGeocodificado")
    private String xGeocodificado;
    @JsonProperty("yGeocodificado")
    private String yGeocodificado;
    @JsonProperty("tipoVia")
    private String tipoVia;
    @JsonProperty("nombreVia")
    private String nombreVia;
    @JsonProperty("numeroPuerta1")
    private String numeroPuerta1;
    @JsonProperty("numeroPuerta2")
    private String numeroPuerta2;
    @JsonProperty("cuadra")
    private String cuadra;
    @JsonProperty("tipoInterior")
    private String tipoInterior;
    @JsonProperty("numeroInterior")
    private String numeroInterior;
    @JsonProperty("piso")
    private String piso;
    @JsonProperty("tipoVivienda")
    private String tipoVivienda;
    @JsonProperty("nombreVivienda")
    private String nombreVivienda;
    @JsonProperty("tipoUrbanizacion")
    private String tipoUrbanizacion;
    @JsonProperty("nombreUrbanizacion")
    private String nombreUrbanizacion;
    @JsonProperty("manzana")
    private String manzana;
    @JsonProperty("lote")
    private String lote;
    @JsonProperty("kilometro")
    private String kilometro;
    @JsonProperty("referencia")
    private String referencia;
    @JsonProperty("nivelConfianza")
    private String nivelConfianza;
//    @JsonProperty("extendedInfoPoint")
//    private ExtendedInfoPoint_ extendedInfoPoint;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ubigeoGeocodificado")
    public String getUbigeoGeocodificado() {
        return ubigeoGeocodificado;
    }

    @JsonProperty("ubigeoGeocodificado")
    public void setUbigeoGeocodificado(String ubigeoGeocodificado) {
        this.ubigeoGeocodificado = ubigeoGeocodificado;
    }

    @JsonProperty("descripcionUbigeo")
    public String getDescripcionUbigeo() {
        return descripcionUbigeo;
    }

    @JsonProperty("descripcionUbigeo")
    public void setDescripcionUbigeo(String descripcionUbigeo) {
        this.descripcionUbigeo = descripcionUbigeo;
    }

    @JsonProperty("direccionGeocodificada")
    public String getDireccionGeocodificada() {
        return direccionGeocodificada;
    }

    @JsonProperty("direccionGeocodificada")
    public void setDireccionGeocodificada(String direccionGeocodificada) {
        this.direccionGeocodificada = direccionGeocodificada;
    }

    @JsonProperty("xGeocodificado")
    public String getXGeocodificado() {
        return xGeocodificado;
    }

    @JsonProperty("xGeocodificado")
    public void setXGeocodificado(String xGeocodificado) {
        this.xGeocodificado = xGeocodificado;
    }

    @JsonProperty("yGeocodificado")
    public String getYGeocodificado() {
        return yGeocodificado;
    }

    @JsonProperty("yGeocodificado")
    public void setYGeocodificado(String yGeocodificado) {
        this.yGeocodificado = yGeocodificado;
    }

    @JsonProperty("tipoVia")
    public String getTipoVia() {
        return tipoVia;
    }

    @JsonProperty("tipoVia")
    public void setTipoVia(String tipoVia) {
        this.tipoVia = tipoVia;
    }

    @JsonProperty("nombreVia")
    public String getNombreVia() {
        return nombreVia;
    }

    @JsonProperty("nombreVia")
    public void setNombreVia(String nombreVia) {
        this.nombreVia = nombreVia;
    }

    @JsonProperty("numeroPuerta1")
    public String getNumeroPuerta1() {
        return numeroPuerta1;
    }

    @JsonProperty("numeroPuerta1")
    public void setNumeroPuerta1(String numeroPuerta1) {
        this.numeroPuerta1 = numeroPuerta1;
    }

    @JsonProperty("numeroPuerta2")
    public String getNumeroPuerta2() {
        return numeroPuerta2;
    }

    @JsonProperty("numeroPuerta2")
    public void setNumeroPuerta2(String numeroPuerta2) {
        this.numeroPuerta2 = numeroPuerta2;
    }

    @JsonProperty("cuadra")
    public String getCuadra() {
        return cuadra;
    }

    @JsonProperty("cuadra")
    public void setCuadra(String cuadra) {
        this.cuadra = cuadra;
    }

    @JsonProperty("tipoInterior")
    public String getTipoInterior() {
        return tipoInterior;
    }

    @JsonProperty("tipoInterior")
    public void setTipoInterior(String tipoInterior) {
        this.tipoInterior = tipoInterior;
    }

    @JsonProperty("numeroInterior")
    public String getNumeroInterior() {
        return numeroInterior;
    }

    @JsonProperty("numeroInterior")
    public void setNumeroInterior(String numeroInterior) {
        this.numeroInterior = numeroInterior;
    }

    @JsonProperty("piso")
    public String getPiso() {
        return piso;
    }

    @JsonProperty("piso")
    public void setPiso(String piso) {
        this.piso = piso;
    }

    @JsonProperty("tipoVivienda")
    public String getTipoVivienda() {
        return tipoVivienda;
    }

    @JsonProperty("tipoVivienda")
    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    @JsonProperty("nombreVivienda")
    public String getNombreVivienda() {
        return nombreVivienda;
    }

    @JsonProperty("nombreVivienda")
    public void setNombreVivienda(String nombreVivienda) {
        this.nombreVivienda = nombreVivienda;
    }

    @JsonProperty("tipoUrbanizacion")
    public String getTipoUrbanizacion() {
        return tipoUrbanizacion;
    }

    @JsonProperty("tipoUrbanizacion")
    public void setTipoUrbanizacion(String tipoUrbanizacion) {
        this.tipoUrbanizacion = tipoUrbanizacion;
    }

    @JsonProperty("nombreUrbanizacion")
    public String getNombreUrbanizacion() {
        return nombreUrbanizacion;
    }

    @JsonProperty("nombreUrbanizacion")
    public void setNombreUrbanizacion(String nombreUrbanizacion) {
        this.nombreUrbanizacion = nombreUrbanizacion;
    }

    @JsonProperty("manzana")
    public String getManzana() {
        return manzana;
    }

    @JsonProperty("manzana")
    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    @JsonProperty("lote")
    public String getLote() {
        return lote;
    }

    @JsonProperty("lote")
    public void setLote(String lote) {
        this.lote = lote;
    }

    @JsonProperty("kilometro")
    public String getKilometro() {
        return kilometro;
    }

    @JsonProperty("kilometro")
    public void setKilometro(String kilometro) {
        this.kilometro = kilometro;
    }

    @JsonProperty("referencia")
    public String getReferencia() {
        return referencia;
    }

    @JsonProperty("referencia")
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    @JsonProperty("nivelConfianza")
    public String getNivelConfianza() {
        return nivelConfianza;
    }

    @JsonProperty("nivelConfianza")
    public void setNivelConfianza(String nivelConfianza) {
        this.nivelConfianza = nivelConfianza;
    }

/*    @JsonProperty("extendedInfoPoint")
    public ExtendedInfoPoint_ getExtendedInfoPoint() {
        return extendedInfoPoint;
    }

    @JsonProperty("extendedInfoPoint")
    public void setExtendedInfoPoint(ExtendedInfoPoint_ extendedInfoPoint) {
        this.extendedInfoPoint = extendedInfoPoint;
    }*/

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

