package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponseBodyFault;

import java.util.List;

@JsonPropertyOrder({"COD-CLI-SN","COD-PRO-CMR-SN","IND-PGN-IN","NUM-REG-RCU-NU","TIP-EST-CD","LIS-DAT-CMR-GR"})
public class CustomerLineResponseBody {
    @JsonProperty("COD-CLI-SN")
    private String codCli;
    @JsonProperty("COD-PRO-CMR-SN")
    private String codProCmr;
    @JsonProperty("IND-PGN-IN")
    private String indPgn;
    @JsonProperty("NUM-REG-RCU-NU")
    private String numReg;
    @JsonProperty("TIP-EST-CD")
    private String tipEstCd;
    @JsonProperty("LIS-DAT-CMR-GR")
    private List<CustomerLineDetailResponseBody> Lineas;
    @JsonProperty("ClientException")
    private ApiResponseBodyFault ClientException;
    @JsonProperty("ServerException")
    private ApiResponseBodyFault ServerException;

    public String getCodCli() {
        return codCli;
    }

    public void setCodCli(String codCli) {
        this.codCli = codCli;
    }

    public String getCodProCmr() {
        return codProCmr;
    }

    public void setCodProCmr(String codProCmr) {
        this.codProCmr = codProCmr;
    }

    public String getIndPgn() {
        return indPgn;
    }

    public void setIndPgn(String indPgn) {
        this.indPgn = indPgn;
    }

    public String getNumReg() {
        return numReg;
    }

    public void setNumReg(String numReg) {
        this.numReg = numReg;
    }

    public String getTipEstCd() {
        return tipEstCd;
    }

    public void setTipEstCd(String tipEstCd) {
        this.tipEstCd = tipEstCd;
    }

    public List<CustomerLineDetailResponseBody> getLineas() {
        return Lineas;
    }

    public void setLineas(List<CustomerLineDetailResponseBody> lineas) {
        Lineas = lineas;
    }

    public ApiResponseBodyFault getClientException() {
        return ClientException;
    }

    public void setClientException(ApiResponseBodyFault clientException) {
        ClientException = clientException;
    }

    public ApiResponseBodyFault getServerException() {
        return ServerException;
    }

    public void setServerException(ApiResponseBodyFault serverException) {
        ServerException = serverException;
    }
}
