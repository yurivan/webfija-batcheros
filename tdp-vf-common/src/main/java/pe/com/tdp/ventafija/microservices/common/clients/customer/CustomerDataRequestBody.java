package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"COD-CRI-BUS-CD","TIP-DOC-CLT-SN","DOC-CLI-CLT-SN"})
public class CustomerDataRequestBody {

    private String tipoDoc;
    private String numDoc;
    private String codCriBus;

    @JsonProperty("COD-CRI-BUS-CD")
    public String getCodCriBus() { return codCriBus; }

    @JsonProperty("TIP-DOC-CLT-SN")
    public String getTipoDoc() {
        return tipoDoc;
    }

    @JsonProperty("DOC-CLI-CLT-SN")
    public String getNumDoc() {
        return numDoc;
    }

    public void setTipoDoc(String tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public void setCodCriBus(String codCriBus) { this.codCriBus = codCriBus; }
}