package pe.com.tdp.ventafija.microservices.common.clients.dto.bio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import pe.com.tdp.ventafija.microservices.common.clients.dto.bio.Detail;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SoapError {

    @JsonProperty("faultcode")
    private String faultcode;
    @JsonProperty("faultstring")
    private String faultstring;
    @JsonProperty("faultactor")
    private String faultactor;
    @JsonProperty("detail")
    private Detail detail;

    public String getFaultcode() {
        return faultcode;
    }

    public void setFaultcode(String faultcode) {
        this.faultcode = faultcode;
    }

    public String getFaultstring() {
        return faultstring;
    }

    public void setFaultstring(String faultstring) {
        this.faultstring = faultstring;
    }

    public String getFaultactor() {
        return faultactor;
    }

    public void setFaultactor(String faultactor) {
        this.faultactor = faultactor;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }
}
