package pe.com.tdp.ventafija.microservices.common.util;

public class Constants {

    private Constants() {
    }

    public static final String PARAMETERS_DOMAIN_TRANSLATE = "TRANSLATE";
    public static final String PARAMETERS_CATEGORY_CMSR = "CMSR";
    public static final String PARAMETERS_CATEGORY_ATIS = "ATIS";
    public static final String COMMERCIAL_OPE_SVA = "SVAS";
    public static final String TYPE_SVA = "S";

    public static final String API_REQUEST_HEADER_DESTINATION_UPFRONT = "ES:BUS:UPFRONT:UPFRONT";
    public static final String API_REQUEST_HEADER_DESTINATION_HDEC = "ES:BUS:HDEC:HDEC";
    public static final String API_REQUEST_HEADER_OPERATION_HDEC = "Registro en HDEC";

    public static final String API_REQUEST_HEADER_OPERATION_UPFRONT = "Consulta UPFRONT";
    public static final String API_REQUEST_HEADER_COUNTRY = "PE";
    public static final String API_REQUEST_HEADER_LANG = "es";
    public static final String API_REQUEST_HEADER_ENTITY = "TDP";
    public static final String API_REQUEST_HEADER_SYSTEM = "APPVENTASFIJAX";
    public static final String API_REQUEST_HEADER_SUBSYSTEM = "APPVENTASFIJA";
    public static final String API_REQUEST_HEADER_ORIGINATOR = "PE:TDP:APPVENTASFIJAX:APPVENTASFIJA";
    public static final String API_REQUEST_HEADER_SENDER = "OracleServiceBus";
    public static final String API_REQUEST_HEADER_USER_ID = "AppUser";
    public static final String API_REQUEST_HEADER_WS_ID = "BluemixEveris";
    public static final String API_REQUEST_HEADER_WS_IP = "10.11.11.11";

    //Sprint 24
    public static final String API_REQUEST_HEADER_SYSTEM_IVR = "IVR";
    public static final String API_REQUEST_HEADER_SUBSYSTEM_IVR = "CMS";
    public static final String API_REQUEST_HEADER_ORIGINATOR_IVR = "PE:TDP:IVR:CMS";
    public static final String API_REQUEST_HEADER_USER_ID_IVR = "USERIVR";
    public static final String API_REQUEST_HEADER_WS_ID_IVR = "SistemIVR";
    public static final String API_REQUEST_HEADER_EXEC_ID_IVR = "550e8400-e29b-41d4-a716-446655440000";

    public static final String API_REQUEST_HEADER_OPERATION = "Envia mensaje de texto";
    public static final String API_REQUEST_HEADER_DESTINATION = "ES:BUS:SMS:SMS";
    public static final String API_REQUEST_HEADER_EXEC_ID = "550e8400-e29b-41d4-a716-446655440003";
    public static final String API_REQUEST_HEADER_MSG_TYPE = "REQUEST";

    public static final String DELIMITER_VERTICAL_BAR = "||";
    public static final String EMPTY = "";
    public static final int MAXIMUM_PASSWORD_STORAGE = 5;

//product
    public static final String RESPONSE_ACTION = "CLIENTE NUEVO";
    //public static final String DOES_NOT_APPLY = "NO APLICA";
    public static final String API_REQUEST_HEADER_OPERATION_AVVE = "Consulta FFTT a AAVE";
    public static final String API_REQUEST_HEADER_OPERATION_EXPERTO = "Consulta FFTT a EXPERTO";
    public static final String API_REQUEST_HEADER_DESTINATION_AVVE = "ES:BUS:AVVE:AVVE";
    public static final String API_REQUEST_HEADER_DESTINATION_EXPERTO = "ES:BUS:EXPERTO:EXPERTO";


    //customer
    public static final String API_REQUEST_HEADER_OPERATION_RENIEC = "ConsultaRENIEC";
    public static final String API_REQUEST_HEADER_DESTINATION_RENIEC = "ES:BUS:RENIEC:RENIEC";

    public static final String DOES_NOT_APPLY = "NO APLICA";
    public static final String RESPONSE = "RESPONSE";

    public static final String DUPLICATE_SVA_DECO = "PUNTO ADICIONAL";
    public static final String DUPLICATE_STATUS_DOWN = "CAIDA";
    public static final String DUPLICATE_STATUS_PENDING = "PENDIENTE";
    public static final String DUPLICATE_STATUS_FOLLOW = "SEGUIMIENTO";
    public static final String DUPLICATE_STATUS_IN_PROCESS = "EN PROCESO";
    public static final String DUPLICATE_STATUS_EMPTY = "";
    public static final String DUPLICATE_STATUS_JOINED = "INGRESADO";

    public static final int DUPLICATE_ORDER_CODE_CMS_1 = 7;
    public static final int DUPLICATE_ORDER_CODE_CMS_2 = 8;
    public static final int DUPLICATE_ORDER_CODE_ATIS = 9;

    public static final String DUPLICATE_ATIS_ACTIVE = "IC";
    public static final String DUPLICATE_ATIS_SUS_DUE = "SD";
    public static final String DUPLICATE_ATIS_SUS_APC = "SA";
    public static final String DUPLICATE_ATIS_SUS = "SU";

    public static final String MY_SQL_SP_NAME = "[SP_NAME]";
    public static final String MY_SQL_PARAMETER = "[PARAMETER]";
    public static final String MY_SQL_COL = "[COL]";
    public static final String MY_SQL_TABLE = "[TABLE]";
    public static final String MY_SQL_WHERE = "[WHERE]";

    public static final String GENDER_MAS = "M";
    public static final String GENDER_FEM = "F";

    public static final String API_ERROR_RESPONSE_CODE = "-1";

    public static final String API_REQUEST_HEADER_OPERATION_GIS = "ObtenerScore";
    public static final String API_REQUEST_HEADER_DESTINATION_GIS = "PE:TDP:WebPorta:WebPorta";

    public static final String API_REQUEST_HEADER_OPERATION_GD = "GeocodificarDireccion";
    public static final String API_REQUEST_HEADER_DESTINATION_GD = "PE:TDP:CMS:IVR";

    public static final String API_REQUEST_HEADER_OPERATION_AUTOMATIZADOR_REGISTER = "Registrar pedido";
    public static final String API_REQUEST_HEADER_DESTINATION_AUTOMATIZADOR = "ES:BUS:ATIS:ATIS";
}
