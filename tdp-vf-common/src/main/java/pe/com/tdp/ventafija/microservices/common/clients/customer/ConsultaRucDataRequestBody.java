package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"numDocumento","sisOrigen"})
public class ConsultaRucDataRequestBody {
    private String numDoc;
    private String sisOri;

    @JsonProperty("numDocumento")
    public String getNumDoc() {
        return numDoc;
    }
    @JsonProperty("sisOrigen")
    public String getSisOri() {
        return sisOri;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public void setSisOri(String sisOri) {
        this.sisOri = sisOri;
    }
}
