package pe.com.tdp.ventafija.microservices.common.clients.address;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BodyOut {

    @JsonProperty("GeocodificarDireccionResponse")
    private GeocodificarDireccionResponse geocodificarDireccionResponse;

    @JsonProperty("GeocodificarDireccionResponse")
    public GeocodificarDireccionResponse getGeocodificarDireccionResponse() {
        return geocodificarDireccionResponse;
    }

    @JsonProperty("GeocodificarDireccionResponse")
    public void setGeocodificarDireccionResponse(GeocodificarDireccionResponse geocodificarDireccionResponse) {
        this.geocodificarDireccionResponse = geocodificarDireccionResponse;
    }

}

