package pe.com.tdp.ventafija.microservices.common.clients;

import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

public class ClientException extends Exception {
	private static final long serialVersionUID = 1L;
	private ServiceCallEvent event;

	public ClientException(ServiceCallEvent event, String message, Throwable cause) {
		super(message, cause);
		this.event = event;
	}

	public ClientException(String message) {
		super(message);
	}

	public ClientException(Throwable e) {
		super(e);
	}

	public ServiceCallEvent getEvent() {
		return event;
	}

	public void setEvent(ServiceCallEvent event) {
		this.event = event;
	}

}
