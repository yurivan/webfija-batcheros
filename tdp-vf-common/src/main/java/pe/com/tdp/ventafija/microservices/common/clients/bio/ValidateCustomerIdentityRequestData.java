package pe.com.tdp.ventafija.microservices.common.clients.bio;

import com.fasterxml.jackson.annotation.JsonProperty;
import pe.com.tdp.ventafija.microservices.common.clients.dto.bio.CharacteristicList;

public class ValidateCustomerIdentityRequestData {
	
	@JsonProperty("documentType")
	private String documentType;
	@JsonProperty("documentNumber")
	private String documentNumber;
	@JsonProperty("orderActionType")
	private String orderActionType;
	@JsonProperty("modalityID")
	private String modalityID;
	//@JsonProperty("salesChannel")
    //private String salesChannel;
	@JsonProperty("motive")
    private String motive;
	@JsonProperty("characteristicList")
    private CharacteristicList characteristicList;
    
    
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	public String getDocumentNumber() {
		return documentNumber;
	}
	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}
	public String getOrderActionType() {
		return orderActionType;
	}
	public void setOrderActionType(String orderActionType) {
		this.orderActionType = orderActionType;
	}
	public String getModalityID() {
		return modalityID;
	}
	public void setModalityID(String modalityID) {
		this.modalityID = modalityID;
	}
	/*public String getSalesChannel() {
		return salesChannel;
	}
	public void setSalesChannel(String salesChannel) {
		this.salesChannel = salesChannel;
	}*/
	public String getMotive() {
		return motive;
	}
	public void setMotive(String motive) {
		this.motive = motive;
	}
	public CharacteristicList getCharacteristicList() {
		return characteristicList;
	}
	public void setCharacteristicList(CharacteristicList characteristicList) {
		this.characteristicList = characteristicList;
	}
    
    
    

}
