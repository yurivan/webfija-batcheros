package pe.com.tdp.ventafija.microservices.common.clients.Cip;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.sms.AbstractClientIVR3;
import pe.com.tdp.ventafija.microservices.domain.Cip2.ConsultarCipRequest;
import pe.com.tdp.ventafija.microservices.domain.Cip2.ConsultarCipResponse;
import pe.com.tdp.ventafija.microservices.domain.cip.GenerarCipResponse;

import java.io.IOException;

public class ConsultarCIp extends AbstractClientIVR3<ConsultarCipRequest, ConsultarCipResponse> {
    @Override
    protected String getServiceCode() {
        return "";
    }

    @Override
    protected ConsultarCipResponse getResponseCip(String json)
            throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        ConsultarCipResponse objCms = mapper.readValue(json, new TypeReference<ConsultarCipResponse>() {});
        return objCms;
    }

    public ConsultarCIp(ClientConfig config) {
        super(config);
    }
}
