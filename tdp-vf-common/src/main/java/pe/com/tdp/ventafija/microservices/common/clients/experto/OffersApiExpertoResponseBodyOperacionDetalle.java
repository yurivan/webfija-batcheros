package pe.com.tdp.ventafija.microservices.common.clients.experto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OffersApiExpertoResponseBodyOperacionDetalle {
	@JsonProperty("CodProd")
	private String CodProd;
	@JsonProperty("DesProd")
	private String DesProd;
	@JsonProperty("CuotaFin")
	private float CuotaFin;
	@JsonProperty("MesesFin")
	private float MesesFin;
	@JsonProperty("PagoAdelantado")
	private float PagoAdelantado;
	@JsonProperty("NroDecosAdic")
	private float NroDecosAdic;

	public String getCodProd() {
		return CodProd;
	}

	public void setCodProd(String codProd) {
		CodProd = codProd;
	}

	public String getDesProd() {
		return DesProd;
	}

	public void setDesProd(String desProd) {
		DesProd = desProd;
	}

	public float getCuotaFin() {
		return CuotaFin;
	}

	public void setCuotaFin(float cuotaFin) {
		CuotaFin = cuotaFin;
	}

	public float getMesesFin() {
		return MesesFin;
	}

	public void setMesesFin(float mesesFin) {
		MesesFin = mesesFin;
	}

	public float getPagoAdelantado() {
		return PagoAdelantado;
	}

	public void setPagoAdelantado(float pagoAdelantado) {
		PagoAdelantado = pagoAdelantado;
	}

	public float getNroDecosAdic() {
		return NroDecosAdic;
	}

	public void setNroDecosAdic(float nroDecosAdic) {
		NroDecosAdic = nroDecosAdic;
	}

}
