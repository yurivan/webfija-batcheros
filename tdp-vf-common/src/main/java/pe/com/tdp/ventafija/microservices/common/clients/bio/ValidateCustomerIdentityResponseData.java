package pe.com.tdp.ventafija.microservices.common.clients.bio;

import com.fasterxml.jackson.annotation.JsonProperty;
import pe.com.tdp.ventafija.microservices.common.clients.dto.bio.PersonDetails;

public class ValidateCustomerIdentityResponseData {
	
	@JsonProperty("validationTransactionID")
	private Integer validationTransactionID;
	@JsonProperty("validationResult")
	private String validationResult;
	@JsonProperty("idScoringQuery")
	private String idScoringQuery;
	@JsonProperty("PersonDetails")
	private pe.com.tdp.ventafija.microservices.common.clients.dto.bio.PersonDetails PersonDetails;
	
	public Integer getValidationTransactionID() {
		return validationTransactionID;
	}
	public void setValidationTransactionID(Integer validationTransactionID) {
		this.validationTransactionID = validationTransactionID;
	}
	public String getValidationResult() {
		return validationResult;
	}
	public void setValidationResult(String validationResult) {
		this.validationResult = validationResult;
	}
	public String getIdScoringQuery() {
		return idScoringQuery;
	}
	public void setIdScoringQuery(String idScoringQuery) {
		this.idScoringQuery = idScoringQuery;
	}
	public PersonDetails getPersonDetails() {
		return PersonDetails;
	}
	public void setPersonDetails(PersonDetails personDetails) {
		PersonDetails = personDetails;
	}
	
	

}
