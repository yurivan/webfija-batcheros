package pe.com.tdp.ventafija.microservices.common.util.enums;

public enum AprobacionEnum {
	SI("Si", "S"), NO("No", "N");
	
	private String label;
	private String labelShort;

	private AprobacionEnum (String label, String labelShort) {
		this.label = label;
		this.labelShort = labelShort;
	}
	
	public String getLabel() {
		return label;
	}
	
	public String getLabelShort () {
		return labelShort;
	}
}
