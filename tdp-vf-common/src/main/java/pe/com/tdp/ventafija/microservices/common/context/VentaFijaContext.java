package pe.com.tdp.ventafija.microservices.common.context;

import java.io.Serializable;

import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;

public interface VentaFijaContext extends Serializable {
    void setServiceCallEvent(ServiceCallEvent event);

    ServiceCallEvent getServiceCallEvent();
}
