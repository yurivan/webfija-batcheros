package pe.com.tdp.ventafija.microservices.domain.Cip2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Data {
    @JsonProperty("cip")
    private Integer cip;

    @JsonProperty("cip")
    public Integer getCip() {
        return cip;
    }

    @JsonProperty("cip")
    public void setCip(Integer cip) {
        this.cip = cip;
    }
}
