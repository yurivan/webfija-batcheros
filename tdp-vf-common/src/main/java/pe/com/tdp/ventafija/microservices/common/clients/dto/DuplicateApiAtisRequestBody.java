package pe.com.tdp.ventafija.microservices.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DuplicateApiAtisRequestBody {

	private String Peticion;

	@JsonProperty("Peticion")
	public String getPeticion() {
		return Peticion;
	}

	public void setPeticion(String peticion) {
		Peticion = peticion;
	}

}
