package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"COD-PRO-CMR-CD","NUM-IDE-NU","IDE-PRO-CMR-CD","SBT-PRO-CMR-CD","SBT-PRO-CMR-NO","COD-CLI-CD","COD-CTA-CD",
        "NUM-ICN-LGY-NU","NOM-FAC-DS","PRI-APE-FAC-DS","SEG-APE-FAC-DS","COD-CIC-CD","COD-NAT-CTA-CD","TIP-CAL-ATI-CD",
        "NOM-CAL-DS","NUM-CAL-NU","NOM-PVA-NO","NOM-SLO-NO","DSC-CMP-PRI-DS","TIP-CLI-CD","COD-SGM-CTA-CD","NUM-DOC-CLI-CD",
        "TIP-DOC-CD","FAM-PRO-SER-CD","COD-CAT-PRO-CD"})
public class CustomerLineDetailResponseBody {

    @JsonProperty("COD-PRO-CMR-CD")
    private String codPro;
    @JsonProperty("NUM-IDE-NU")
    private String numIdeNu;
    @JsonProperty("IDE-PRO-CMR-CD")
    private String ideProCmr;
    @JsonProperty("SBT-PRO-CMR-CD")
    private String sbtProCmrCd;
    @JsonProperty("SBT-PRO-CMR-NO")
    private String sbtProCmrNo;
    @JsonProperty("COD-CLI-CD")
    private String codCliCd;
    @JsonProperty("COD-CTA-CD")
    private String codCtaCd;
    @JsonProperty("NUM-ICN-LGY-NU")
    private String numIcnLgyNu;
    @JsonProperty("NOM-FAC-DS")
    private String nomFacDs;
    @JsonProperty("PRI-APE-FAC-DS")
    private String priApeFacDs;
    @JsonProperty("SEG-APE-FAC-DS")
    private String segApeFacDs;
    @JsonProperty("COD-CIC-CD")
    private String codCicCd;
    @JsonProperty("COD-NAT-CTA-CD")
    private String codNatCtaCd;
    @JsonProperty("TIP-CAL-ATI-CD")
    private String tipCalAtiCd;
    @JsonProperty("NOM-CAL-DS")
    private String nomCalDs;
    @JsonProperty("NUM-CAL-NU")
    private String numCalNu;
    @JsonProperty("NOM-PVA-NO")
    private String nomPvaNo;
    @JsonProperty("NOM-SLO-NO")
    private String nomSloNo;
    @JsonProperty("DSC-CMP-PRI-DS")
    private String dscCmpPriDs;
    @JsonProperty("TIP-CLI-CD")
    private String tipCliCd;
    @JsonProperty("COD-SGM-CTA-CD")
    private String codSgmCtaCd;
    @JsonProperty("NUM-DOC-CLI-CD")
    private String numDocCliCd;
    @JsonProperty("TIP-DOC-CD")
    private String tipDocCd;
    @JsonProperty("FAM-PRO-SER-CD")
    private String famProSerCd;
    @JsonProperty("COD-CAT-PRO-CD")
    private String codCatProCd;

    public String getCodPro() {
        return codPro;
    }

    public void setCodPro(String codPro) {
        this.codPro = codPro;
    }

    public String getNumIdeNu() {
        return numIdeNu;
    }

    public void setNumIdeNu(String numIdeNu) {
        this.numIdeNu = numIdeNu;
    }

    public String getIdeProCmr() {
        return ideProCmr;
    }

    public void setIdeProCmr(String ideProCmr) {
        this.ideProCmr = ideProCmr;
    }

    public String getSbtProCmrCd() {
        return sbtProCmrCd;
    }

    public void setSbtProCmrCd(String sbtProCmrCd) {
        this.sbtProCmrCd = sbtProCmrCd;
    }

    public String getSbtProCmrNo() {
        return sbtProCmrNo;
    }

    public void setSbtProCmrNo(String sbtProCmrNo) {
        this.sbtProCmrNo = sbtProCmrNo;
    }

    public String getCodCliCd() {
        return codCliCd;
    }

    public void setCodCliCd(String codCliCd) {
        this.codCliCd = codCliCd;
    }

    public String getCodCtaCd() {
        return codCtaCd;
    }

    public void setCodCtaCd(String codCtaCd) {
        this.codCtaCd = codCtaCd;
    }

    public String getNumIcnLgyNu() {
        return numIcnLgyNu;
    }

    public void setNumIcnLgyNu(String numIcnLgyNu) {
        this.numIcnLgyNu = numIcnLgyNu;
    }

    public String getNomFacDs() {
        return nomFacDs;
    }

    public void setNomFacDs(String nomFacDs) {
        this.nomFacDs = nomFacDs;
    }

    public String getPriApeFacDs() {
        return priApeFacDs;
    }

    public void setPriApeFacDs(String priApeFacDs) {
        this.priApeFacDs = priApeFacDs;
    }

    public String getSegApeFacDs() {
        return segApeFacDs;
    }

    public void setSegApeFacDs(String segApeFacDs) {
        this.segApeFacDs = segApeFacDs;
    }

    public String getCodCicCd() {
        return codCicCd;
    }

    public void setCodCicCd(String codCicCd) {
        this.codCicCd = codCicCd;
    }

    public String getCodNatCtaCd() {
        return codNatCtaCd;
    }

    public void setCodNatCtaCd(String codNatCtaCd) {
        this.codNatCtaCd = codNatCtaCd;
    }

    public String getTipCalAtiCd() {
        return tipCalAtiCd;
    }

    public void setTipCalAtiCd(String tipCalAtiCd) {
        this.tipCalAtiCd = tipCalAtiCd;
    }

    public String getNomCalDs() {
        return nomCalDs;
    }

    public void setNomCalDs(String nomCalDs) {
        this.nomCalDs = nomCalDs;
    }

    public String getNumCalNu() {
        return numCalNu;
    }

    public void setNumCalNu(String numCalNu) {
        this.numCalNu = numCalNu;
    }

    public String getNomPvaNo() {
        return nomPvaNo;
    }

    public void setNomPvaNo(String nomPvaNo) {
        this.nomPvaNo = nomPvaNo;
    }

    public String getNomSloNo() {
        return nomSloNo;
    }

    public void setNomSloNo(String nomSloNo) {
        this.nomSloNo = nomSloNo;
    }

    public String getDscCmpPriDs() {
        return dscCmpPriDs;
    }

    public void setDscCmpPriDs(String dscCmpPriDs) {
        this.dscCmpPriDs = dscCmpPriDs;
    }

    public String getTipCliCd() {
        return tipCliCd;
    }

    public void setTipCliCd(String tipCliCd) {
        this.tipCliCd = tipCliCd;
    }

    public String getCodSgmCtaCd() {
        return codSgmCtaCd;
    }

    public void setCodSgmCtaCd(String codSgmCtaCd) {
        this.codSgmCtaCd = codSgmCtaCd;
    }

    public String getNumDocCliCd() {
        return numDocCliCd;
    }

    public void setNumDocCliCd(String numDocCliCd) {
        this.numDocCliCd = numDocCliCd;
    }

    public String getTipDocCd() {
        return tipDocCd;
    }

    public void setTipDocCd(String tipDocCd) {
        this.tipDocCd = tipDocCd;
    }

    public String getFamProSerCd() {
        return famProSerCd;
    }

    public void setFamProSerCd(String famProSerCd) {
        this.famProSerCd = famProSerCd;
    }

    public String getCodCatProCd() {
        return codCatProCd;
    }

    public void setCodCatProCd(String codCatProCd) {
        this.codCatProCd = codCatProCd;
    }
}
