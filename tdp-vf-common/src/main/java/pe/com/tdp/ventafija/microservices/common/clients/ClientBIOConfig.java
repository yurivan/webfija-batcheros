package pe.com.tdp.ventafija.microservices.common.clients;

public class ClientBIOConfig {
	private String url;
	private String apiId;
	private String apiSecret;
	private String userLogin;
	private String serviceChannel;
	private String sessionCode;
	private String application;
	private String idMessage;
	private String ipAddress;
	private String functionalityCode;
	private String transactionTimestamp;
	private String serviceName;
	private String version;

	public ClientBIOConfig(String url, String apiId, String apiSecret, String userLogin, String serviceChannel, String sessionCode, String application, String idMessage, String ipAddress, String functionalityCode, String transactionTimestamp, String serviceName, String version) {
		super();
		this.url = url; 
		this.apiId = apiId;
		this.apiSecret = apiSecret;
		this.userLogin = userLogin;
		this.serviceChannel = serviceChannel;
		this.sessionCode = sessionCode;
		this.serviceChannel = serviceChannel;
		this.application = application;
		this.idMessage = idMessage;
		this.ipAddress = ipAddress;
		this.functionalityCode =functionalityCode;
		this.transactionTimestamp = transactionTimestamp;
		this.serviceName = serviceName;
		this.version = version;
	}

	
	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getApiId() {
		return apiId;
	}


	public void setApiId(String apiId) {
		this.apiId = apiId;
	}


	public String getApiSecret() {
		return apiSecret;
	}


	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}


	public String getUserLogin() {
		return userLogin;
	}


	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}


	public String getServiceChannel() {
		return serviceChannel;
	}


	public void setServiceChannel(String serviceChannel) {
		this.serviceChannel = serviceChannel;
	}


	public String getSessionCode() {
		return sessionCode;
	}


	public void setSessionCode(String sessionCode) {
		this.sessionCode = sessionCode;
	}


	public String getApplication() {
		return application;
	}


	public void setApplication(String application) {
		this.application = application;
	}


	public String getIdMessage() {
		return idMessage;
	}


	public void setIdMessage(String idMessage) {
		this.idMessage = idMessage;
	}


	public String getIpAddress() {
		return ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public String getFunctionalityCode() {
		return functionalityCode;
	}


	public void setFunctionalityCode(String functionalityCode) {
		this.functionalityCode = functionalityCode;
	}


	public String getTransactionTimestamp() {
		return transactionTimestamp;
	}


	public void setTransactionTimestamp(String transactionTimestamp) {
		this.transactionTimestamp = transactionTimestamp;
	}


	public String getServiceName() {
		return serviceName;
	}


	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}


	public String getVersion() {
		return version;
	}


	public void setVersion(String version) {
		this.version = version;
	}


	public static class ClientBIOConfigBuilder {
		private String url;
		private String apiId;
		private String apiSecret;
		private String userLogin;
		private String serviceChannel;
		private String sessionCode;
		private String application;
		private String idMessage;
		private String ipAddress;
		private String functionalityCode;
		private String transactionTimestamp;
		private String serviceName;
		private String version;
		
		public ClientBIOConfigBuilder() {
			super();
		}

		public ClientBIOConfigBuilder setUrl(String url) {
			this.url = url;
			return this;
		}

		public ClientBIOConfigBuilder setApiId(String apiId) {
			this.apiId = apiId;
			return this;
		}

		public ClientBIOConfigBuilder setApiSecret(String apiSecret) {
			this.apiSecret = apiSecret;
			return this;
		}

		public ClientBIOConfigBuilder setUserLogin(String userLogin) {
			this.userLogin = userLogin;
			return this;
		}

		public ClientBIOConfigBuilder setServiceChannel(String serviceChannel) {
			this.serviceChannel = serviceChannel;
			return this;
		}
		
		public ClientBIOConfigBuilder setSessionCode(String sessionCode) {
			this.sessionCode = sessionCode;
			return this;
		}
		
		public ClientBIOConfigBuilder setApplication(String application) {
			this.application = application;
			return this;
		}
		
		public ClientBIOConfigBuilder setIdMessage(String idMessage) {
			this.idMessage = idMessage;
			return this;
		}
		
		public ClientBIOConfigBuilder setIpAddress(String ipAddress) {
			this.ipAddress = ipAddress;
			return this;
		}
		
		public ClientBIOConfigBuilder setFunctionalityCode(String functionalityCode) {
			this.functionalityCode = functionalityCode;
			return this;
		}
		
		public ClientBIOConfigBuilder setTransactionTimestamp(String transactionTimestamp) {
			this.transactionTimestamp = transactionTimestamp;
			return this;
		}
		
		public ClientBIOConfigBuilder setServiceName(String serviceName) {
			this.serviceName = serviceName;
			return this;
		}
		
		public ClientBIOConfigBuilder setVersion(String version) {
			this.version = version;
			return this;
		}
		
		public ClientBIOConfig build () {
			return new ClientBIOConfig(url, apiId, apiSecret, userLogin, serviceChannel, sessionCode, application, idMessage, ipAddress, functionalityCode, transactionTimestamp, serviceName, version);
		}
	}
}
