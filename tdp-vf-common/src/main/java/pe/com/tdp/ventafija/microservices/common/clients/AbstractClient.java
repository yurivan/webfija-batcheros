package pe.com.tdp.ventafija.microservices.common.clients;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequest;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequestHeader;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.connection.Api;
import pe.com.tdp.ventafija.microservices.common.context.VentaFijaContextHolder;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.util.Constants;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;

public abstract class AbstractClient<T, R> {
    public static final String SERVICE_CODE_CONSULTA_ESTADO_CMS = "ESTADOCMS";
    public static final String SERVICE_CODE_CONSULTA_ESTADO_ATIS = "ESTADOATIS";
    public static final String SERVICE_CODE_RENIEC = "RENIEC";
    public static final String SERVICE_CODE_BIOMETRIC = "BIOMETRIC";
    public static final String SERVICE_CODE_HDEC = "HDEC";

    protected ClientConfig config;
    protected ServiceCallEvent event;

    protected AbstractClient(ClientConfig config) {
        super();
        this.config = config;
    }

    public ClientResult<ApiResponse<R>> post(T body) {
        ClientResult<ApiResponse<R>> result = new ClientResult<>();

        try {

            String json = doRequest(body);

            ApiResponse<R> apiResponse = getResponse(json);

            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse(getEvent().getMsg());
            }
            result.setEvent(event);
        }
        return result;
    }

    public ClientResult<ApiResponse<R>> getJson(String json, String tipo) {
        ClientResult<ApiResponse<R>> result = new ClientResult<>();

        try {
            if(tipo.equalsIgnoreCase("OTROS")){
                getEvent().setServiceCode("EXPERTO_ADICIONAL");
            }
            else{
                getEvent().setServiceCode("EXPERTO_RUC");
            }
            ApiResponse<R> apiResponse = getResponse(json);
            result.setResult(apiResponse);
            result.setSuccess(true);
            getEvent().setServiceResponse(json);
            getEvent().setResult("OK");
            getEvent().setMsg("OK");

        } catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse(getEvent().getMsg());
            }
            result.setEvent(event);
        }
        return result;
    }


    public ClientResult<ApiResponse<R>> post2(T body) {
        ClientResult<ApiResponse<R>> result = new ClientResult<>();

        try {
            String json = doRequest(body);
            String jsonNew = "";
            if (json.contains("$")) {
                json = json + "12345";
                int n = json.length();
                int k = 0;
                for (int i = 0; i < n; i++) {
                    if (json.substring(i, i + 5).equals("12345")) {
                        i = i + 6;
                    } else {
                        if (json.substring(i, i + 5).equals("{\"$\":")) {
                            k = i + 5;
                            jsonNew = jsonNew + "";
                            while (!json.substring(k, k + 1).equals("}")) {
                                jsonNew = jsonNew + json.substring(k, k + 1);
                                k++;
                            }
                            if (json.substring(k, k + 1).equals("}")) {
                                jsonNew = jsonNew + "";
                                i = k;
                            }
                        } else {
                            jsonNew = jsonNew + json.substring(i, i + 1);
                        }
                    }
                }
            } else {
                jsonNew = json;
            }
            ApiResponse<R> apiResponse = getResponse(jsonNew);
            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }
        return result;
    }

    public ClientResult<String> postGenerico(T body){
        ClientResult<String> result = new ClientResult<>();
        try{
            String json = doRequestGeneric(body);

            if(json != ""){
                result.setResult(json);
                result.setSuccess(true);
            }
        }catch (Exception e) {

            getEvent().setResult("ERROR");
            getEvent().setMsg(e.getMessage());

            result.setSuccess(false);
            result.setE(new ClientException(event, e.getMessage(), e));
        } finally {
            if (getEvent().getServiceResponse() == null) {
                getEvent().setServiceResponse("N/A");
            }
            result.setEvent(event);
        }

        return result;
    }

    private R getData(String json)throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json,
                new TypeReference<R>() {
                });
    }


    protected String doRequest(T body) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyPOST(uri, getApiRequest(body), config.getApiId(), config.getApiSecret());
        getEvent().setServiceResponse(jsonSMS);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");
        return jsonSMS;
    }

    protected String doRequestGeneric(T body) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyPOST(uri, body, config.getApiId(), config.getApiSecret());
        getEvent().setServiceResponse(jsonSMS);
        getEvent().setResult("OK");
        getEvent().setMsg("OK");

        String jsonRequest;
        try {
            jsonRequest = new ObjectMapper().writeValueAsString(body);
        } catch (JsonProcessingException e1) {
            jsonRequest = "N/A";
        }
        getEvent().setServiceRequest(jsonRequest);


        return jsonSMS;
    }

    protected ApiRequest<T> getApiRequest(T body) {
        ApiRequest<T> apiRequest = new ApiRequest<>();
        ApiRequestHeader apiRequestHeader = getHeader(config.getOperation(), config.getDestination());
        apiRequest.setHeaderIn(apiRequestHeader);
        apiRequest.setBodyIn(body);

        String jsonRequest;
        try {
            jsonRequest = new ObjectMapper().writeValueAsString(apiRequest);
        } catch (JsonProcessingException e1) {
            jsonRequest = "N/A";
        }

        getEvent().setServiceRequest(jsonRequest);

        return apiRequest;
    }

    protected ApiRequestHeader getHeader(String API_REQUEST_HEADER_OPERATION, String API_REQUEST_HEADER_DESTINATION) {
        ApiRequestHeader apiRequestHeader = new ApiRequestHeader();
        apiRequestHeader.setCountry(Constants.API_REQUEST_HEADER_COUNTRY);
        apiRequestHeader.setLang(Constants.API_REQUEST_HEADER_LANG);
        apiRequestHeader.setEntity(Constants.API_REQUEST_HEADER_ENTITY);
        apiRequestHeader.setSystem(Constants.API_REQUEST_HEADER_SYSTEM);
        apiRequestHeader.setSubsystem(Constants.API_REQUEST_HEADER_SUBSYSTEM);
        apiRequestHeader.setOriginator(Constants.API_REQUEST_HEADER_ORIGINATOR);
        apiRequestHeader.setSender(Constants.API_REQUEST_HEADER_SENDER);
        apiRequestHeader.setUserId(Constants.API_REQUEST_HEADER_USER_ID);
        apiRequestHeader.setWsId(Constants.API_REQUEST_HEADER_WS_ID);
        apiRequestHeader.setWsIp(Constants.API_REQUEST_HEADER_WS_IP);
        apiRequestHeader.setOperation(API_REQUEST_HEADER_OPERATION);
        apiRequestHeader.setDestination(API_REQUEST_HEADER_DESTINATION);
        apiRequestHeader.setExecId(Constants.API_REQUEST_HEADER_EXEC_ID);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));
        apiRequestHeader.setTimestamp(sdf.format(new java.util.Date()) + "-05:00");
        apiRequestHeader.setMsgType(Constants.API_REQUEST_HEADER_MSG_TYPE);

        return apiRequestHeader;
    }

    protected ServiceCallEvent getEvent() {
        if (event == null) {
            event = new ServiceCallEvent(VentaFijaContextHolder.getContext().getServiceCallEvent());
            event.setServiceUrl(config.getUrl());
            event.setServiceCode(getServiceCode());
        }
        return event;
    }

    protected abstract String getServiceCode();

    protected abstract ApiResponse<R> getResponse(String json) throws Exception;

}
