package pe.com.tdp.ventafija.microservices.common.clients.address2;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BodyInGeoV2 {
    @JsonProperty("quantity_max_resp")
    private String quantity_max_resp;

    @JsonProperty("cod_unique")
    private String cod_unique;

    @JsonProperty("latitude")
    private String latitud;

    @JsonProperty("longitude")
    private String longitud;

    public String getQuantity_max_resp() {
        return quantity_max_resp;
    }

    public void setQuantity_max_resp(String quantity_max_resp) {
        this.quantity_max_resp = quantity_max_resp;
    }

    public String getCod_unique() {
        return cod_unique;
    }

    public void setCod_unique(String cod_unique) {
        this.cod_unique = cod_unique;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
}
