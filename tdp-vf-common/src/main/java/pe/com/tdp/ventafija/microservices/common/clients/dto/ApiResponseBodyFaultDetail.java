package pe.com.tdp.ventafija.microservices.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonPropertyOrder({ "exceptionAppCode", "exceptionAppMessage" })
public class ApiResponseBodyFaultDetail {
  @JsonInclude(Include.NON_NULL)
  private String exceptionAppCode;
  @JsonInclude(Include.NON_NULL)
  private String exceptionAppMessage;

  public String getExceptionAppCode() {
    return exceptionAppCode;
  }

  public void setExceptionAppCode(String exceptionAppCode) {
    this.exceptionAppCode = exceptionAppCode;
  }

  public String getExceptionAppMessage() {
    return exceptionAppMessage;
  }

  public void setExceptionAppMessage(String exceptionAppMessage) {
    this.exceptionAppMessage = exceptionAppMessage;
  }

}
