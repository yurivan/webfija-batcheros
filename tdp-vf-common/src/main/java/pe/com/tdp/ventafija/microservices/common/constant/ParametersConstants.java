package pe.com.tdp.ventafija.microservices.common.constant;

public class ParametersConstants {

    private ParametersConstants() {
    }

    public static final String PARAMETERS_DOMAIN_TIMER = "TIMER";
    public static final String PARAMETERS_DOMAIN_TRY = "TRY";
    public static final String PARAMETERS_DOMAIN_TRANSLATE = "TRANSLATE";

    public static final String PARAMETERS_CATEGORY_TRANSLATE_ATIS = "ATIS";
    public static final String PARAMETERS_CATEGORY_TRANSLATE_CMSS = "CMSS";
    public static final String PARAMETERS_CATEGORY_TRANSLATE_CMSR = "CMSR";
}
