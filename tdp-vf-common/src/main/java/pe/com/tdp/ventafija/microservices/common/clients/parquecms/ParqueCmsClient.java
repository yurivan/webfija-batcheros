package pe.com.tdp.ventafija.microservices.common.clients.parquecms;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.AbstractClient;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

public class ParqueCmsClient extends AbstractClient<ServicesApiCmsRequestBody, ServicesApiCmsResponseBody> {

	public ParqueCmsClient(ClientConfig config) {
		super(config);
	}

	@Override
	protected String getServiceCode() {
		return "PARQUECMS";
	}

	@Override
	protected ApiResponse<ServicesApiCmsResponseBody> getResponse(String json) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		ApiResponse<ServicesApiCmsResponseBody> objCms = mapper.readValue(json,
				new TypeReference<ApiResponse<ServicesApiCmsResponseBody>>() {
				});
		return objCms;
	}

}
