package pe.com.tdp.ventafija.microservices.common.clients.experto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"ApellidoPaterno", "ApellidoMaterno", "Nombres", "TipoDeDocumento", "NumeroDeDocumento", "Departamento", "Provincia", "Distrito", "Ubigeo", "CodVendedor", "Zonal"})
public class OffersApiExpertoRequestBody {

	private String ApellidoPaterno;
	private String ApellidoMaterno;
	private String Nombres;
	private String TipoDeDocumento;
	private String NumeroDeDocumento;
	private String Departamento;
	private String Provincia;
	private String Distrito;
	private String Ubigeo;
	private String CodVendedor;
	private String Zonal;

	@JsonProperty("ApellidoPaterno")
	public String getApellidoPaterno() {
		return ApellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		ApellidoPaterno = apellidoPaterno;
	}

	@JsonProperty("ApellidoMaterno")
	public String getApellidoMaterno() {
		return ApellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		ApellidoMaterno = apellidoMaterno;
	}

	@JsonProperty("Nombres")
	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	@JsonProperty("TipoDeDocumento")
	public String getTipoDeDocumento() {
		return TipoDeDocumento;
	}

	public void setTipoDeDocumento(String tipoDeDocumento) {
		TipoDeDocumento = tipoDeDocumento;
	}

	@JsonProperty("NumeroDeDocumento")
	public String getNumeroDeDocumento() {
		return NumeroDeDocumento;
	}

	public void setNumeroDeDocumento(String numeroDeDocumento) {
		NumeroDeDocumento = numeroDeDocumento;
	}

	@JsonProperty("Departamento")
	public String getDepartamento() {
		return Departamento;
	}

	public void setDepartamento(String departamento) {
		Departamento = departamento;
	}

	@JsonProperty("Provincia")
	public String getProvincia() {
		return Provincia;
	}

	public void setProvincia(String provincia) {
		Provincia = provincia;
	}

	@JsonProperty("Distrito")
	public String getDistrito() {
		return Distrito;
	}

	public void setDistrito(String distrito) {
		Distrito = distrito;
	}

	@JsonProperty("Zonal")
	public String getZonal() {
		return Zonal;
	}

	public void setZonal(String zonal) {
		Zonal = zonal;
	}

	@JsonProperty("CodVendedor")
	public String getCodVendedor() {
		return CodVendedor;
	}

	public void setCodVendedor(String codVendedor) {
		CodVendedor = codVendedor;
	}

	@JsonProperty("Ubigeo")
	public String getUbigeo() {
		return Ubigeo;
	}

	public void setUbigeo(String ubigeo) {
		Ubigeo = ubigeo;
	}

}
