package pe.com.tdp.ventafija.microservices.common.util;

public class OrderConstants {

    public static final String PARAMETER_COD_RESPUESTA_OK = "0";
    public static final String PARAMETER_COD_RESPUESTA_ERROR = "0";
    public static final String PARAMETER_DETALLE_RESPUESTA_ERROR = "NO HAY VENTAS DEL USUARIO";

    public static final String ORDEN_RESPONSE_CODE_ERROR = "01";
    public static final String ORDEN_RESPONSE_DATA_ERROR = "Vuelva Intentarlo";

    public static final String PROCESS_AUDIO_RESPONSE_CODE_OK = "00";
    public static final String PROCESS_AUDIO_RESPONSE_CODE_ERROR = "01";
    public static final String PROCESS_AUDIO_RESPONSE_DATA_PENDIENTE = "PENDIENTE ORDEN";
    public static final String PROCESS_AUDIO_RESPONSE_DATA_POR_PROCESAR = "POR PROCESAR";
    public static final String PROCESS_AUDIO_RESPONSE_DATA_ERROR = "ERROR";
    public static final String PROCESS_AUDIO_RESPONSE_DATA_PROCESANDO = "PROCESANDO";
    public static final String PROCESS_AUDIO_RESPONSE_DATA_PROCESADO = "PROCESADO";

    public static final String ALTA_PURA = "A";
    public static final String ALTA_SVA = "S";
    public static final String MIGRACION = "M";


}
