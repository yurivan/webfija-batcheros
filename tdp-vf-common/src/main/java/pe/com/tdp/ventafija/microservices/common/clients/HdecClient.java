package pe.com.tdp.ventafija.microservices.common.clients;

import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.HdecApiHdecRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.HdecApiHdecResponseBody;
import pe.com.tdp.ventafija.microservices.common.connection.ApiClient;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.util.Constants;

public class HdecClient extends SimpleClientImpl<HdecApiHdecRequestBody, HdecApiHdecResponseBody> {

	public HdecClient (ApiHeaderConfig config) {
		super(config, config.getApiUri(), config.getHdecRegistrarPreVentaApiId(), 
				config.getHdecRegistrarPreVentaApiSecret(), Constants.API_REQUEST_HEADER_OPERATION_HDEC, 
				Constants.API_REQUEST_HEADER_DESTINATION_HDEC, HdecApiHdecResponseBody.class);
	}

	public HdecClient (ApiHeaderConfig config, ApiClient apiClient, ApiRequestHeaderFactory factory) {
		super(config, config.getApiUri(), config.getHdecRegistrarPreVentaApiId(), 
				config.getHdecRegistrarPreVentaApiSecret(), Constants.API_REQUEST_HEADER_OPERATION_HDEC, 
				Constants.API_REQUEST_HEADER_DESTINATION_HDEC, HdecApiHdecResponseBody.class, apiClient, factory);
	}

	@Override
	public HdecApiHdecResponseBody sendData(HdecApiHdecRequestBody request) throws ApiClientException {
		HdecApiHdecResponseBody response = super.sendData(request);
		if (response == null) {
			throw new ApiClientException("Error client HDEC");
		}
		
		if (response.getClientException() != null) {
			String exceptionDetail = response.getClientException().getExceptionDetail();
			String appMessage = response.getClientException().getAppDetail() == null ? "Error de cliente HDEC" : response.getClientException().getAppDetail().getExceptionAppMessage();
			throw new ApiClientException(exceptionDetail + " " + appMessage);
		}
		return response;
	}
}
