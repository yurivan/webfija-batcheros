package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"COD-TIP-DOC-CD","NUM-DOC-NU","COD-EMP-EMI-CD","COD-UNI-MED-CD","MTO-IMT-IM","MTO-DOC-IM","MTO-TOT-IM","V2-TIP-MTO-CD",
        "FEC-EMI-DOC-FF","FEC-VNC-FF","FEC-CCL-DOC-FF","IND-EST-DOC-IN"})
public class CustomerReceiptsDetailResponseBody {

    @JsonProperty("COD-TIP-DOC-CD")
    private String codTipDoc;
    @JsonProperty("NUM-DOC-NU")
    private String numDoc;
    @JsonProperty("COD-EMP-EMI-CD")
    private String codEmpEmi;
    @JsonProperty("COD-UNI-MED-CD")
    private String codUniMed;
    @JsonProperty("MTO-IMT-IM")
    private String mtoImtIm;
    @JsonProperty("MTO-DOC-IM")
    private String mtoDocIm;
    @JsonProperty("MTO-TOT-IM")
    private String mtoTotIm;
    @JsonProperty("V2-TIP-MTO-CD")
    private String tipMtoCd;
    @JsonProperty("FEC-EMI-DOC-FF")
    private String fecEmiDoc;
    @JsonProperty("FEC-VNC-FF")
    private String fecVnc;
    @JsonProperty("FEC-CCL-DOC-FF")
    private String fecCclDoc;
    @JsonProperty("IND-EST-DOC-IN")
    private String indEstDoc;

    public String getCodTipDoc() {
        return codTipDoc;
    }

    public void setCodTipDoc(String codTipDoc) {
        this.codTipDoc = codTipDoc;
    }

    public String getNumDoc() {
        return numDoc;
    }

    public void setNumDoc(String numDoc) {
        this.numDoc = numDoc;
    }

    public String getCodEmpEmi() {
        return codEmpEmi;
    }

    public void setCodEmpEmi(String codEmpEmi) {
        this.codEmpEmi = codEmpEmi;
    }

    public String getCodUniMed() {
        return codUniMed;
    }

    public void setCodUniMed(String codUniMed) {
        this.codUniMed = codUniMed;
    }

    public String getMtoImtIm() {
        return mtoImtIm;
    }

    public void setMtoImtIm(String mtoImtIm) {
        this.mtoImtIm = mtoImtIm;
    }

    public String getMtoDocIm() {
        return mtoDocIm;
    }

    public void setMtoDocIm(String mtoDocIm) {
        this.mtoDocIm = mtoDocIm;
    }

    public String getMtoTotIm() {
        return mtoTotIm;
    }

    public void setMtoTotIm(String mtoTotIm) {
        this.mtoTotIm = mtoTotIm;
    }

    public String getTipMtoCd() {
        return tipMtoCd;
    }

    public void setTipMtoCd(String tipMtoCd) {
        this.tipMtoCd = tipMtoCd;
    }

    public String getFecEmiDoc() {
        return fecEmiDoc;
    }

    public void setFecEmiDoc(String fecEmiDoc) {
        this.fecEmiDoc = fecEmiDoc;
    }

    public String getFecVnc() {
        return fecVnc;
    }

    public void setFecVnc(String fecVnc) {
        this.fecVnc = fecVnc;
    }

    public String getFecCclDoc() {
        return fecCclDoc;
    }

    public void setFecCclDoc(String fecCclDoc) {
        this.fecCclDoc = fecCclDoc;
    }

    public String getIndEstDoc() {
        return indEstDoc;
    }

    public void setIndEstDoc(String indEstDoc) {
        this.indEstDoc = indEstDoc;
    }
}
