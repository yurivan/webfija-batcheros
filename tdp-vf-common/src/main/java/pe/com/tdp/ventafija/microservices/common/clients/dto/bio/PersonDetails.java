
package pe.com.tdp.ventafija.microservices.common.clients.dto.bio;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PersonDetails {

	@JsonProperty("maritalStatus")
	private String maritalStatus;
	@JsonProperty("familyNames")
	private String familyNames;
	@JsonProperty("givenNames")
	private String givenNames;
	@JsonProperty("birthDate")
	private String birthDate;

	@JsonProperty("maritalStatus")
	public String getMaritalStatus() {
		return maritalStatus;
	}

	@JsonProperty("maritalStatus")
	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	@JsonProperty("familyNames")
	public String getFamilyNames() {
		return familyNames;
	}

	@JsonProperty("familyNames")
	public void setFamilyNames(String familyNames) {
		this.familyNames = familyNames;
	}

	@JsonProperty("givenNames")
	public String getGivenNames() {
		return givenNames;
	}

	@JsonProperty("givenNames")
	public void setGivenNames(String givenNames) {
		this.givenNames = givenNames;
	}

	@JsonProperty("birthDate")
	public String getBirthDate() {
		return birthDate;
	}

	@JsonProperty("birthDate")
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

}
