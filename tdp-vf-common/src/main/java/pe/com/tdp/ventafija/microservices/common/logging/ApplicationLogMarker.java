package pe.com.tdp.ventafija.microservices.common.logging;

import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

public interface ApplicationLogMarker {

	public static final String REMOTE_CALL_MARKER_NAME = "REMOTECALL";
	public static final String REMOTE_RESPONSE_MARKER_NAME = "REMOTERESPONSE";
	
	public static final String AUTHORIZATION_MARKER_NAME = "AUTHORIZATION";
	
	public static final String AUTHENTICATION_LOGIN_MARKER_NAME = "AUTHENTICATIONLOGIN";
	public static final String AUTHENTICATION_USR_NO_EXISTS_MARKER_NAME = "AUTHENTICATIONUSRNOEXISTS";
	public static final String AUTHENTICATION_BAD_PASSWD_MARKER_NAME = "AUTHENTICATIONBADPASSW";
	
	public static final String TRANSACTION_MARKER_NAME = "TRANSACTION";

	public static final Marker REMOTE_CALL = MarkerManager.getMarker(REMOTE_CALL_MARKER_NAME);
	public static final Marker REMOTE_RESPONSE = MarkerManager.getMarker(REMOTE_RESPONSE_MARKER_NAME);
	
	public static final Marker AUTHORIZATION = MarkerManager.getMarker(AUTHORIZATION_MARKER_NAME);
	
	public static final Marker AUTHENTICATION_LOGIN = MarkerManager.getMarker(AUTHENTICATION_LOGIN_MARKER_NAME);
	public static final Marker AUTHENTICATION_USR_NO_EXISTS = MarkerManager.getMarker(AUTHENTICATION_USR_NO_EXISTS_MARKER_NAME);
	public static final Marker AUTHENTICATION_BAD_PASSWD = MarkerManager.getMarker(AUTHENTICATION_BAD_PASSWD_MARKER_NAME);
	
	public static final Marker TRANSACTION = MarkerManager.getMarker(TRANSACTION_MARKER_NAME);
}
