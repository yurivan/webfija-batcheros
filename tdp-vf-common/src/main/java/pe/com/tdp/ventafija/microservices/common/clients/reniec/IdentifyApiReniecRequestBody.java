package pe.com.tdp.ventafija.microservices.common.clients.reniec;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IdentifyApiReniecRequestBody {

  private String numDNI;

  @JsonProperty("NumDNI")
  public String getNumDNI() {
    return numDNI;
  }

  public void setNumDNI(String numDNI) {
    this.numDNI = numDNI;
  }

}
