package pe.com.tdp.ventafija.microservices.common.clients.customer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


public class CustomerLineRequestBody {

    private String codCliente;

    @JsonProperty("COD-CLI-CD")
    public String getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(String codCliente) {
        this.codCliente = codCliente;
    }
}
