package pe.com.tdp.ventafija.microservices.common.clients;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequest;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiRequestHeader;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.dto.DuplicateApiCmsRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.DuplicateApiCmsResponseBody;
import pe.com.tdp.ventafija.microservices.common.connection.Api;
import pe.com.tdp.ventafija.microservices.common.constant.LegadosConstants;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;

public class DuplicateCmsClient implements Client<DuplicateApiCmsRequestBody, DuplicateApiCmsResponseBody> {
	private static final Logger logger = LogManager.getLogger(DuplicateCmsClient.class);
	
	private ApiHeaderConfig config;
	
	public DuplicateCmsClient (ApiHeaderConfig config) {
		this.config = config;
	}

	@Override
	public DuplicateApiCmsResponseBody sendData(DuplicateApiCmsRequestBody request) throws ApiClientException {
		boolean result = false;
		ApiResponse<DuplicateApiCmsResponseBody> objCms = null;
		try {
			URI uri = new URI(config.getCmsConsultaEstadoUri());
			ApiRequest<DuplicateApiCmsRequestBody> apiRequest = new ApiRequest<>();
			ApiRequestHeaderFactory factory = new ApiRequestHeaderFactory(config);
			ApiRequestHeader headerIn = factory.getHeader();
			headerIn.setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_STATUS_CMS);
			headerIn.setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_CMS);
			apiRequest.setHeaderIn(headerIn);
            
            apiRequest.setBodyIn(request);
            
            ObjectMapper mapper = new ObjectMapper();
            logger.info(mapper.writeValueAsString(apiRequest));
            
            String json = Api.jerseyPOST(uri, apiRequest, config.getCmsConsultarEstadoApiId(), config.getCmsConsultarEstadoApiSecret());
            logger.info(json);
            objCms = mapper.readValue(json,
                    new TypeReference<ApiResponse<DuplicateApiCmsResponseBody>>() {});
                
                if(ApiHeaderConfig.MSG_TYPE_RESPONSE.equals(objCms.getHeaderOut().getMsgType())){
                	logger.info(objCms.getBodyOut().getEstadoRequerimiento());
                }
		} catch (URISyntaxException e) {
			logger.error("error uri", e);
		} catch (JsonProcessingException e) {
			logger.error("error json", e);
			throw new ApiClientException("Respuesta incorrecta de servidor");
		} catch (IOException e) {
			logger.error("error io", e);
		}
		return objCms == null ? null : objCms.getBodyOut();
	}

}
