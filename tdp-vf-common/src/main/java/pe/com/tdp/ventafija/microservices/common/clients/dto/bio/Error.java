package pe.com.tdp.ventafija.microservices.common.clients.dto.bio;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "code", "description", "details" })
public class Error {

	@JsonProperty("code")
	private Code code;
	@JsonProperty("description")
	private String description;
	@JsonProperty("details")
	private Details details;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("code")
	public Code getCode() {
		return code;
	}

	@JsonProperty("code")
	public void setCode(Code code) {
		this.code = code;
	}

	@JsonProperty("description")
	public String getDescription() {
		return description;
	}

	@JsonProperty("description")
	public void setDescription(String description) {
		this.description = description;
	}

	@JsonProperty("details")
	public Details getDetails() {
		return details;
	}

	@JsonProperty("details")
	public void setDetails(Details details) {
		this.details = details;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
