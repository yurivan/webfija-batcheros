package pe.com.tdp.ventafija.microservices.common.clients;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Map;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.logging.ApplicationLogMarker;

public class OauthInstrospectClient implements Client<Map<String, String>, Map<String, String>> {
	private static final Logger logger = LogManager.getLogger(OauthInstrospectClient.class);
	private URI uri;
	private String apiId;
	private String apiSecret;

	public OauthInstrospectClient(String url, String apiId, String apiSecret) throws URISyntaxException {
		this.uri = new URI(url);
		this.apiId = apiId;
		this.apiSecret = apiSecret;
	}

	@Override
	public Map<String, String> sendData(Map<String, String> request) throws ApiClientException {
		Map<String, String> responseMap = null;
		Date timeStamp = new Date();
		logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s peticion a %s", timeStamp.getTime(), uri));
		try {
			MultivaluedMap<String, String> formData = new MultivaluedHashMap<>(request);
			javax.ws.rs.client.Client client = ClientBuilder.newClient();
			WebTarget webTarget = client.target(uri);
			Invocation.Builder invocationBuilder = webTarget.request();
			invocationBuilder.header("x-ibm-client-id", apiId);
			invocationBuilder.header("x-ibm-client-secret", apiSecret);
			invocationBuilder.header("content-type", "application/x-www-form-urlencoded");

			Response response = invocationBuilder.post(Entity.form(formData));
			String json = response.readEntity(String.class);
			logger.info(response);
			ObjectMapper mapper = new ObjectMapper();

			responseMap = mapper.readValue(json, new TypeReference<Map<String, String>>() {
			});
			logger.info(ApplicationLogMarker.REMOTE_CALL, String.format("%s respuesta de %s: %s", timeStamp.getTime(), uri, response));
		} catch (Exception e) {
			logger.error("error al parser data", e);
			throw new ApiClientException("error.consulta.api");
		}

		return responseMap;
	}

}
