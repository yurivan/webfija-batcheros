package pe.com.tdp.ventafija.microservices.common.util.enums;

public enum UploadWebFileResponse {
    UPLOAD_OK,
    UPLOAD_ENVIANDO_HDEC,
    UPLOAD_ERROR;
}
