package pe.com.tdp.ventafija.microservices.common.clients.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApiHeaderConfig {
	public static final String MSG_TYPE_REQUEST = "REQUEST";
	public static final String MSG_TYPE_RESPONSE = "RESPONSE";
	public static final String MSG_TYPE_ERROR = "ERROR";

	@Value("${tdp.api.hdec.uri}")
	private String apiUri;
	
	@Value("${tdp.api.cms.consultaestado.uri}")
	private String cmsConsultaEstadoUri;

	@Value("${tdp.api.atis.consultaestado.uri}")
	private String atisConsultaEstadoUri;

	@Value("${tdp.api.cms.consultaparque.uri}")
	private String cmsConsultaParqueUri;

	@Value("${tdp.api.atis.consultaparque.uri}")
	private String atisConsultaParqueUri;
	
	@Value("${tdp.api.reniec.consultacliente.uri}")
	private String reniecConsultaClienteUri;

	@Value("${tdp.api.ffttaave.consultafftt.uri}")
	private String ffttaaveConsultaFfttUri;

	//@Value("https://api.us.apiconnect.ibmcloud.com/telefonica-del-peru-production/ter/consulta-estrategia-comercial/consultaScoringFex")
	@Value("${tdp.api.experto.obtenerExperto.uri}")
	private String expertoObtenerExpertoUri;

	@Value("${tdp.api.enviosms.envio.uri}")
	private String envioSmsEnvioUri;

	@Value("${tdp.api.upfront.registrar.uri}")
	private String registrarUpFrontUri;

	// header data
	@Value("${tdp.api.hdec.country}")
	private String country;
	@Value("${tdp.api.hdec.lang}")
	private String lang;
	@Value("${tdp.api.hdec.entity}")
	private String entity;
	@Value("${tdp.api.hdec.system}")
	private String system;
	@Value("${tdp.api.hdec.subsystem}")
	private String subsystem;
	@Value("${tdp.api.hdec.originator}")
	private String originator;
	@Value("${tdp.api.hdec.sender}")
	private String sender;
	@Value("${tdp.api.hdec.userid}")
	private String userId;
	@Value("${tdp.api.hdec.wsid}")
	private String wsId;
	@Value("${tdp.api.hdec.wsip}")
	private String wsIp;
	@Value("${tdp.api.hdec.operation}")
	private String operation;
	@Value("${tdp.api.hdec.destination}")
	private String destination;
	@Value("${tdp.api.hdec.execid}")
	private String execId;
	
	@Value("${tdp.api.hdec.registrarpreventa.apiid}")
	private String hdecRegistrarPreVentaApiId;
	@Value("${tdp.api.hdec.registrarpreventa.apisecret}")
	private String hdecRegistrarPreVentaApiSecret;
	
	@Value("${tdp.api.cms.consultaestado.apiid}")
	private String cmsConsultarEstadoApiId;
	@Value("${tdp.api.cms.consultaestado.apisecret}")
	private String cmsConsultarEstadoApiSecret;
	
	@Value("${tdp.api.atis.consultaestado.apiid}")
	private String atisConsultarEstadoApiId;
	@Value("${tdp.api.atis.consultaestado.apisecret}")
	private String atisConsultarEstadoApiSecret;

	@Value("${tdp.api.cms.consultaparque.apiid}")
	private String cmsConsultarParqueApiId;
	@Value("${tdp.api.cms.consultaparque.apisecret}")
	private String cmsConsultarParqueApiSecret;

	@Value("${tdp.api.atis.consultaparque.apiid}")
	private String atisConsultarParqueApiId;
	@Value("${tdp.api.atis.consultaparque.apisecret}")
	private String atisConsultarParqueApiSecret;
	
	@Value("${tdp.api.reniec.consultacliente.apiid}")
	private String reniecConsultarClienteApiId;
	@Value("${tdp.api.reniec.consultacliente.apisecret}")
	private String reniecConsultarClienteApiSecret;
	
	@Value("${tdp.api.ffttaave.consultafftt.apiid}")
	private String ffttaaveConsultarFfttApiId;
	@Value("${tdp.api.ffttaave.consultafftt.apisecret}")
	private String ffttaaveConsultarFfttApiSecret;
	
	@Value("${tdp.api.experto.obtenerExperto.apiid}")
	private String expertoObtenerExpertoApiId;
	@Value("${tdp.api.experto.obtenerExperto.apisecret}")
	private String expertoObtenerExpertoApiSecret;
	
	@Value("${tdp.api.enviosms.envio.apiid}")
	private String envioSmsEnvioApiId;
	@Value("${tdp.api.enviosms.envio.apisecret}")
	private String envioSmsEnvioApiSecret;
	
	@Value("${tdp.api.upfront.registrar.apiid}")
	private String registrarUpFrontApiId;
	@Value("${tdp.api.upfront.registrar.apisecret}")
	private String registrarUpFrontApiSecret;
	
	@Value("${tdp.api.ffttCalculadoraArpu.consultarArpu.uri}")
	private String ffttCalculadoraConsultarArpuUri;
	
	@Value("${tdp.api.ffttCalculadoraArpu.consultarArpu.apiid}")
	private String ffttCalculadoraConsultarArpuApiId;
	
	@Value("${tdp.api.ffttCalculadora.consultarArpu.apisecret}")
	private String fttCalculadoraConsultarArpuApiSecret;


	/* Sprint 8 - Backend de Servicio biometrico */
	@Value("${tdp.api.reniec.validateCustomerIdentity.uri}")
	private String reniecBiometricoClienteUri;
	@Value("${tdp.api.reniec.validateCustomerIdentity.apiid}")
	private String reniecBiometricoClienteApiId;
	@Value("${tdp.api.reniec.validateCustomerIdentity.apisecret}")
	private String reniecBiometricoClienteApiSecret;

	/* Sprint 19 - Backend de Automatizador */
	@Value("${tdp.api.automatizador.registerSale.uri}")
	private String automatizadorRegisterSaleUri;
	@Value("${tdp.api.automatizador.registerSale.apiid}")
	private String automatizadorRegisterSaleApiId;
	@Value("${tdp.api.automatizador.registerSale.apisecret}")
	private String automatizadorRegisterSaleApiSecret;

	//Sprint 24
	@Value("${tdp.api.cms.consultacliente.uri}")
	private String cmsConsultaClienteUri;
	@Value("${tdp.api.cms.consultalinea.uri}")
	private String cmsConsultaLineasUri;
	@Value("${tdp.api.cms.consultarecibo.uri}")
	private String cmsConsultaRecibosUri;
	@Value("${tdp.api.cms.consultacliente.apiid}")
	private String cmsConsultaClienteApiId;
	@Value("${tdp.api.cms.consultacliente.apisecret}")
	private String cmsConsultaClienteApiSecret;
	@Value("${tdp.api.cms.consultacliente.activo}")
	private String cmsConsultaEstadoServicio;

	//Sprint 24 RUC
	@Value("${tdp.api.consultaruc.uri}")
	private String consultaRucUri;
	@Value("${tdp.api.consultaruc.apiid}")
	private String consultaRucApiId;
	@Value("${tdp.api.consultaruc.apisecret}")
	private String consultaRucApiSecret;

	public String getReniecBiometricoClienteUri() {
		return reniecBiometricoClienteUri;
	}

	public String getReniecBiometricoClienteApiId() {
		return reniecBiometricoClienteApiId;
	}

	public String getReniecBiometricoClienteApiSecret() {
		return reniecBiometricoClienteApiSecret;
	}

	/* Sprint 8 - Backend de Servicio biometrico */

	public void setApiUri(String apiUri) {
		this.apiUri = apiUri;
	}

	public String getApiUri() {
		return apiUri;
	}

	public String getCountry() {
		return country;
	}

	public String getLang() {
		return lang;
	}

	public String getEntity() {
		return entity;
	}

	public String getSystem() {
		return system;
	}

	public String getSubsystem() {
		return subsystem;
	}

	public String getOriginator() {
		return originator;
	}

	public String getSender() {
		return sender;
	}

	public String getUserId() {
		return userId;
	}

	public String getWsId() {
		return wsId;
	}

	public String getWsIp() {
		return wsIp;
	}

	public String getOperation() {
		return operation;
	}

	public String getDestination() {
		return destination;
	}

	public String getExecId() {
		return execId;
	}

	public String getCmsConsultaEstadoUri() {
		return cmsConsultaEstadoUri;
	}

	public void setCmsConsultaEstadoUri(String cmsConsultaEstadoUri) {
		this.cmsConsultaEstadoUri = cmsConsultaEstadoUri;
	}

	public String getAtisConsultaEstadoUri() {
		return atisConsultaEstadoUri;
	}

	public void setAtisConsultaEstadoUri(String atisConsultaEstadoUri) {
		this.atisConsultaEstadoUri = atisConsultaEstadoUri;
	}

	public String getHdecRegistrarPreVentaApiId() {
		return hdecRegistrarPreVentaApiId;
	}

	public void setHdecRegistrarPreVentaApiId(String hdecRegistrarPreVentaApiId) {
		this.hdecRegistrarPreVentaApiId = hdecRegistrarPreVentaApiId;
	}

	public String getHdecRegistrarPreVentaApiSecret() {
		return hdecRegistrarPreVentaApiSecret;
	}

	public void setHdecRegistrarPreVentaApiSecret(String hdecRegistrarPreVentaApiSecret) {
		this.hdecRegistrarPreVentaApiSecret = hdecRegistrarPreVentaApiSecret;
	}

	public String getCmsConsultarEstadoApiId() {
		return cmsConsultarEstadoApiId;
	}

	public void setCmsConsultarEstadoApiId(String cmsConsultarEstadoApiId) {
		this.cmsConsultarEstadoApiId = cmsConsultarEstadoApiId;
	}

	public String getCmsConsultarEstadoApiSecret() {
		return cmsConsultarEstadoApiSecret;
	}

	public void setCmsConsultarEstadoApiSecret(String cmsConsultarEstadoApiSecret) {
		this.cmsConsultarEstadoApiSecret = cmsConsultarEstadoApiSecret;
	}

	public String getAtisConsultarEstadoApiId() {
		return atisConsultarEstadoApiId;
	}

	public void setAtisConsultarEstadoApiId(String atisConsultarEstadoApiId) {
		this.atisConsultarEstadoApiId = atisConsultarEstadoApiId;
	}

	public String getAtisConsultarEstadoApiSecret() {
		return atisConsultarEstadoApiSecret;
	}

	public void setAtisConsultarEstadoApiSecret(String atisConsultarEstadoApiSecret) {
		this.atisConsultarEstadoApiSecret = atisConsultarEstadoApiSecret;
	}

	public String getCmsConsultaParqueUri() {
		return cmsConsultaParqueUri;
	}

	public void setCmsConsultaParqueUri(String cmsConsultaParqueUri) {
		this.cmsConsultaParqueUri = cmsConsultaParqueUri;
	}

	public String getAtisConsultarParqueApiId() {
		return atisConsultarParqueApiId;
	}

	public void setAtisConsultarParqueApiId(String atisConsultarParqueApiId) {
		this.atisConsultarParqueApiId = atisConsultarParqueApiId;
	}

	public String getAtisConsultarParqueApiSecret() {
		return atisConsultarParqueApiSecret;
	}

	public void setAtisConsultarParqueApiSecret(String atisConsultarParqueApiSecret) {
		this.atisConsultarParqueApiSecret = atisConsultarParqueApiSecret;
	}

	public String getAtisConsultaParqueUri() {
		return atisConsultaParqueUri;
	}

	public void setAtisConsultaParqueUri(String atisConsultaParqueUri) {
		this.atisConsultaParqueUri = atisConsultaParqueUri;
	}

	public String getCmsConsultarParqueApiId() {
		return cmsConsultarParqueApiId;
	}

	public void setCmsConsultarParqueApiId(String cmsConsultarParqueApiId) {
		this.cmsConsultarParqueApiId = cmsConsultarParqueApiId;
	}

	public String getCmsConsultarParqueApiSecret() {
		return cmsConsultarParqueApiSecret;
	}

	public void setCmsConsultarParqueApiSecret(String cmsConsultarParqueApiSecret) {
		this.cmsConsultarParqueApiSecret = cmsConsultarParqueApiSecret;
	}

	public String getReniecConsultaClienteUri() {
		return reniecConsultaClienteUri;
	}

	public void setReniecConsultaClienteUri(String reniecConsultaClienteUri) {
		this.reniecConsultaClienteUri = reniecConsultaClienteUri;
	}

	public String getReniecConsultarClienteApiId() {
		return reniecConsultarClienteApiId;
	}

	public void setReniecConsultarClienteApiId(String reniecConsultarClienteApiId) {
		this.reniecConsultarClienteApiId = reniecConsultarClienteApiId;
	}

	public String getReniecConsultarClienteApiSecret() {
		return reniecConsultarClienteApiSecret;
	}

	public void setReniecConsultarClienteApiSecret(String reniecConsultarClienteApiSecret) {
		this.reniecConsultarClienteApiSecret = reniecConsultarClienteApiSecret;
	}

	public String getFfttaaveConsultaFfttUri() {
		return ffttaaveConsultaFfttUri;
	}

	public void setFfttaaveConsultaFfttUri(String ffttaaveConsultaFfttUri) {
		this.ffttaaveConsultaFfttUri = ffttaaveConsultaFfttUri;
	}

	public String getFfttaaveConsultarFfttApiId() {
		return ffttaaveConsultarFfttApiId;
	}

	public void setFfttaaveConsultarFfttApiId(String ffttaaveConsultarFfttApiId) {
		this.ffttaaveConsultarFfttApiId = ffttaaveConsultarFfttApiId;
	}

	public String getFfttaaveConsultarFfttApiSecret() {
		return ffttaaveConsultarFfttApiSecret;
	}

	public void setFfttaaveConsultarFfttApiSecret(String ffttaaveConsultarFfttApiSecret) {
		this.ffttaaveConsultarFfttApiSecret = ffttaaveConsultarFfttApiSecret;
	}

	public String getExpertoObtenerExpertoUri() {
		return expertoObtenerExpertoUri;
	}

	public void setExpertoObtenerExpertoUri(String expertoObtenerExpertoUri) {
		this.expertoObtenerExpertoUri = expertoObtenerExpertoUri;
	}

	public String getExpertoObtenerExpertoApiId() {
		return expertoObtenerExpertoApiId;
	}

	public void setExpertoObtenerExpertoApiId(String expertoObtenerExpertoApiId) {
		this.expertoObtenerExpertoApiId = expertoObtenerExpertoApiId;
	}

	public String getExpertoObtenerExpertoApiSecret() {
		return expertoObtenerExpertoApiSecret;
	}

	public void setExpertoObtenerExpertoApiSecret(String expertoObtenerExpertoApiSecret) {
		this.expertoObtenerExpertoApiSecret = expertoObtenerExpertoApiSecret;
	}

	public String getEnvioSmsEnvioUri() {
		return envioSmsEnvioUri;
	}

	public void setEnvioSmsEnvioUri(String envioSmsEnvioUri) {
		this.envioSmsEnvioUri = envioSmsEnvioUri;
	}

	public String getEnvioSmsEnvioApiId() {
		return envioSmsEnvioApiId;
	}

	public void setEnvioSmsEnvioApiId(String envioSmsEnvioApiId) {
		this.envioSmsEnvioApiId = envioSmsEnvioApiId;
	}

	public String getEnvioSmsEnvioApiSecret() {
		return envioSmsEnvioApiSecret;
	}

	public void setEnvioSmsEnvioApiSecret(String envioSmsEnvioApiSecret) {
		this.envioSmsEnvioApiSecret = envioSmsEnvioApiSecret;
	}

	public String getRegistrarUpFrontApiId() {
		return registrarUpFrontApiId;
	}

	public void setRegistrarUpFrontApiId(String registrarUpFrontApiId) {
		this.registrarUpFrontApiId = registrarUpFrontApiId;
	}

	public String getRegistrarUpFrontApiSecret() {
		return registrarUpFrontApiSecret;
	}

	public void setRegistrarUpFrontApiSecret(String registrarUpFrontApiSecret) {
		this.registrarUpFrontApiSecret = registrarUpFrontApiSecret;
	}

	public String getRegistrarUpFrontUri() {
		return registrarUpFrontUri;
	}

	public void setRegistrarUpFrontUri(String registrarUpFrontUri) {
		this.registrarUpFrontUri = registrarUpFrontUri;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public void setEntity(String entity) {
		this.entity = entity;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public void setSubsystem(String subsystem) {
		this.subsystem = subsystem;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setWsId(String wsId) {
		this.wsId = wsId;
	}

	public void setWsIp(String wsIp) {
		this.wsIp = wsIp;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setExecId(String execId) {
		this.execId = execId;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getFfttCalculadoraConsultarArpuUri() {
		return ffttCalculadoraConsultarArpuUri;
	}

	public void setFfttCalculadoraConsultarArpuUri(String ffttCalculadoraConsultarArpuUri) {
		this.ffttCalculadoraConsultarArpuUri = ffttCalculadoraConsultarArpuUri;
	}

	public String getFfttCalculadoraConsultarArpuApiId() {
		return ffttCalculadoraConsultarArpuApiId;
	}

	public void setFfttCalculadoraConsultarArpuApiId(String ffttCalculadoraConsultarArpuApiId) {
		this.ffttCalculadoraConsultarArpuApiId = ffttCalculadoraConsultarArpuApiId;
	}

	public String getFttCalculadoraConsultarArpuApiSecret() {
		return fttCalculadoraConsultarArpuApiSecret;
	}

	public void setFttCalculadoraConsultarArpuApiSecret(String fttCalculadoraConsultarArpuApiSecret) {
		this.fttCalculadoraConsultarArpuApiSecret = fttCalculadoraConsultarArpuApiSecret;
	}

	public String getAutomatizadorRegisterSaleUri() {
		return automatizadorRegisterSaleUri;
	}

	public void setAutomatizadorRegisterSaleUri(String automatizadorRegisterSaleUri) {
		this.automatizadorRegisterSaleUri = automatizadorRegisterSaleUri;
	}

	public String getAutomatizadorRegisterSaleApiId() {
		return automatizadorRegisterSaleApiId;
	}

	public void setAutomatizadorRegisterSaleApiId(String automatizadorRegisterSaleApiId) {
		this.automatizadorRegisterSaleApiId = automatizadorRegisterSaleApiId;
	}

	public String getAutomatizadorRegisterSaleApiSecret() {
		return automatizadorRegisterSaleApiSecret;
	}

	public void setAutomatizadorRegisterSaleApiSecret(String automatizadorRegisterSaleApiSecret) {
		this.automatizadorRegisterSaleApiSecret = automatizadorRegisterSaleApiSecret;
	}

	public String getCmsConsultaClienteUri() {
		return cmsConsultaClienteUri;
	}

	public void setCmsConsultaClienteUri(String cmsConsultaClienteUri) {
		this.cmsConsultaClienteUri = cmsConsultaClienteUri;
	}

	public String getCmsConsultaLineasUri() {
		return cmsConsultaLineasUri;
	}

	public void setCmsConsultaLineasUri(String cmsConsultaLineasUri) {
		this.cmsConsultaLineasUri = cmsConsultaLineasUri;
	}

	public String getCmsConsultaRecibosUri() {
		return cmsConsultaRecibosUri;
	}

	public void setCmsConsultaRecibosUri(String cmsConsultaRecibosUri) {
		this.cmsConsultaRecibosUri = cmsConsultaRecibosUri;
	}

	public String getCmsConsultaClienteApiId() {
		return cmsConsultaClienteApiId;
	}

	public void setCmsConsultaClienteApiId(String cmsConsultaClienteApiId) {
		this.cmsConsultaClienteApiId = cmsConsultaClienteApiId;
	}

	public String getCmsConsultaClienteApiSecret() {
		return cmsConsultaClienteApiSecret;
	}

	public void setCmsConsultaClienteApiSecret(String cmsConsultaClienteApiSecret) {
		this.cmsConsultaClienteApiSecret = cmsConsultaClienteApiSecret;
	}

	public String getCmsConsultaEstadoServicio() {
		return cmsConsultaEstadoServicio;
	}

	public void setCmsConsultaEstadoServicio(String cmsConsultaEstadoServicio) {
		this.cmsConsultaEstadoServicio = cmsConsultaEstadoServicio;
	}

	public String getConsultaRucUri() {
		return consultaRucUri;
	}

	public void setConsultaRucUri(String consultaRucUri) {
		this.consultaRucUri = consultaRucUri;
	}

	public String getConsultaRucApiId() {
		return consultaRucApiId;
	}

	public void setConsultaRucApiId(String consultaRucApiId) {
		this.consultaRucApiId = consultaRucApiId;
	}

	public String getConsultaRucApiSecret() {
		return consultaRucApiSecret;
	}

	public void setConsultaRucApiSecret(String consultaRucApiSecret) {
		this.consultaRucApiSecret = consultaRucApiSecret;
	}
}