package pe.com.tdp.ventafija.microservices.domain.cip;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenerarCipRequest {

    @JsonProperty("currency")
    private String currency;
    @JsonProperty("amount")
    private String amount;
    @JsonProperty("transactionCode")
    private String transactionCode;
    @JsonProperty("adminEmail")
    private String adminEmail;
    @JsonProperty("dateExpiry")
    private String dateExpiry;
    @JsonProperty("paymentConcept")
    private String paymentConcept;
    @JsonProperty("additionalData")
    private String additionalData;
    @JsonProperty("userEmail")
    private String userEmail;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("userLastName")
    private String userLastName;
    @JsonProperty("userUbigeo")
    private String userUbigeo;
    @JsonProperty("userCountry ")
    private String userCountry;
    @JsonProperty("userDocumentType")
    private String userDocumentType;
    @JsonProperty("userDocumentNumber")
    private String userDocumentNumber;
    @JsonProperty("userCodeCountry")
    private String userCodeCountry;
    @JsonProperty("userPhone")
    private String userPhone;

    @JsonProperty("currency")
    public String getCurrency() {
        return currency;
    }

    @JsonProperty("currency")
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @JsonProperty("amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("transactionCode")
    public String getTransactionCode() {
        return transactionCode;
    }

    @JsonProperty("transactionCode")
    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    @JsonProperty("adminEmail")
    public String getAdminEmail() {
        return adminEmail;
    }

    @JsonProperty("adminEmail")
    public void setAdminEmail(String adminEmail) {
        this.adminEmail = adminEmail;
    }

    @JsonProperty("dateExpiry")
    public String getDateExpiry() {
        return dateExpiry;
    }

    @JsonProperty("dateExpiry")
    public void setDateExpiry(String dateExpiry) {
        this.dateExpiry = dateExpiry;
    }

    @JsonProperty("paymentConcept")
    public String getPaymentConcept() {
        return paymentConcept;
    }

    @JsonProperty("paymentConcept")
    public void setPaymentConcept(String paymentConcept) {
        this.paymentConcept = paymentConcept;
    }

    @JsonProperty("additionalData")
    public String getAdditionalData() {
        return additionalData;
    }

    @JsonProperty("additionalData")
    public void setAdditionalData(String additionalData) {
        this.additionalData = additionalData;
    }

    @JsonProperty("userEmail")
    public String getUserEmail() {
        return userEmail;
    }

    @JsonProperty("userEmail")
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @JsonProperty("userName")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("userName")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("userLastName")
    public String getUserLastName() {
        return userLastName;
    }

    @JsonProperty("userLastName")
    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    @JsonProperty("userUbigeo")
    public String getUserUbigeo() {
        return userUbigeo;
    }

    @JsonProperty("userUbigeo")
    public void setUserUbigeo(String userUbigeo) {
        this.userUbigeo = userUbigeo;
    }

    @JsonProperty("userCountry ")
    public String getUserCountry() {
        return userCountry;
    }

    @JsonProperty("userCountry ")
    public void setUserCountry(String userCountry) {
        this.userCountry = userCountry;
    }

    @JsonProperty("userDocumentType")
    public String getUserDocumentType() {
        return userDocumentType;
    }

    @JsonProperty("userDocumentType")
    public void setUserDocumentType(String userDocumentType) {
        this.userDocumentType = userDocumentType;
    }

    @JsonProperty("userDocumentNumber")
    public String getUserDocumentNumber() {
        return userDocumentNumber;
    }

    @JsonProperty("userDocumentNumber")
    public void setUserDocumentNumber(String userDocumentNumber) {
        this.userDocumentNumber = userDocumentNumber;
    }

    @JsonProperty("userCodeCountry")
    public String getUserCodeCountry() {
        return userCodeCountry;
    }

    @JsonProperty("userCodeCountry")
    public void setUserCodeCountry(String userCodeCountry) {
        this.userCodeCountry = userCodeCountry;
    }

    @JsonProperty("userPhone")
    public String getUserPhone() {
        return userPhone;
    }

    @JsonProperty("userPhone")
    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

}
