package pe.com.tdp.ventafija.microservices.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "TefHeaderReq", "ValidateCustomerIdentityRequestData" })
public class ApiRequestBIO<E> {

    private ApiRequestBIOHeader TefHeaderReq;
    private E ValidateCustomerIdentityRequestData;
  
    @JsonProperty("TefHeaderReq")
	public ApiRequestBIOHeader getTefHeaderReq() {
		return TefHeaderReq;
	}
	public void setTefHeaderReq(ApiRequestBIOHeader tefHeaderReq) {
		TefHeaderReq = tefHeaderReq;
	}
	@JsonProperty("ValidateCustomerIdentityRequestData")
	public E getValidateCustomerIdentityRequestData() {
		return ValidateCustomerIdentityRequestData;
	}
	public void setValidateCustomerIdentityRequestData(E validateCustomerIdentityRequestData) {
		ValidateCustomerIdentityRequestData = validateCustomerIdentityRequestData;
	}
  
}
