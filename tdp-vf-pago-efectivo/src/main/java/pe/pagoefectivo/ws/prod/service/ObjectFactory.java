
package pe.pagoefectivo.ws.prod.service;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pe.pagoefectivo.ws.prod.service package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pe.pagoefectivo.ws.prod.service
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GenerarCIPMod1 }
     * 
     */
    public GenerarCIPMod1 createGenerarCIPMod1() {
        return new GenerarCIPMod1();
    }

    /**
     * Create an instance of {@link BEWSGenCIPRequestMod1 }
     * 
     */
    public BEWSGenCIPRequestMod1 createBEWSGenCIPRequestMod1() {
        return new BEWSGenCIPRequestMod1();
    }

    /**
     * Create an instance of {@link ActualizarCIPMod1Response }
     * 
     */
    public ActualizarCIPMod1Response createActualizarCIPMod1Response() {
        return new ActualizarCIPMod1Response();
    }

    /**
     * Create an instance of {@link BEWSActualizaCIPResponseMod1 }
     * 
     */
    public BEWSActualizaCIPResponseMod1 createBEWSActualizaCIPResponseMod1() {
        return new BEWSActualizaCIPResponseMod1();
    }

    /**
     * Create an instance of {@link ConsultarCIPMod1Response }
     * 
     */
    public ConsultarCIPMod1Response createConsultarCIPMod1Response() {
        return new ConsultarCIPMod1Response();
    }

    /**
     * Create an instance of {@link BEWSConsultarCIPResponseMod1 }
     * 
     */
    public BEWSConsultarCIPResponseMod1 createBEWSConsultarCIPResponseMod1() {
        return new BEWSConsultarCIPResponseMod1();
    }

    /**
     * Create an instance of {@link EliminarCIPMod1Response }
     * 
     */
    public EliminarCIPMod1Response createEliminarCIPMod1Response() {
        return new EliminarCIPMod1Response();
    }

    /**
     * Create an instance of {@link BEWSElimCIPResponseMod1 }
     * 
     */
    public BEWSElimCIPResponseMod1 createBEWSElimCIPResponseMod1() {
        return new BEWSElimCIPResponseMod1();
    }

    /**
     * Create an instance of {@link ActualizarCIPMod1 }
     * 
     */
    public ActualizarCIPMod1 createActualizarCIPMod1() {
        return new ActualizarCIPMod1();
    }

    /**
     * Create an instance of {@link BEWSActualizaCIPRequestMod1 }
     * 
     */
    public BEWSActualizaCIPRequestMod1 createBEWSActualizaCIPRequestMod1() {
        return new BEWSActualizaCIPRequestMod1();
    }

    /**
     * Create an instance of {@link ConsultarSolicitudPagov2 }
     * 
     */
    public ConsultarSolicitudPagov2 createConsultarSolicitudPagov2() {
        return new ConsultarSolicitudPagov2();
    }

    /**
     * Create an instance of {@link BEWSConsultarSolicitudRequest }
     * 
     */
    public BEWSConsultarSolicitudRequest createBEWSConsultarSolicitudRequest() {
        return new BEWSConsultarSolicitudRequest();
    }

    /**
     * Create an instance of {@link GenerarCIPMod1Response }
     * 
     */
    public GenerarCIPMod1Response createGenerarCIPMod1Response() {
        return new GenerarCIPMod1Response();
    }

    /**
     * Create an instance of {@link BEWSGenCIPResponseMod1 }
     * 
     */
    public BEWSGenCIPResponseMod1 createBEWSGenCIPResponseMod1() {
        return new BEWSGenCIPResponseMod1();
    }

    /**
     * Create an instance of {@link EliminarCIPMod1 }
     * 
     */
    public EliminarCIPMod1 createEliminarCIPMod1() {
        return new EliminarCIPMod1();
    }

    /**
     * Create an instance of {@link BEWSElimCIPRequestMod1 }
     * 
     */
    public BEWSElimCIPRequestMod1 createBEWSElimCIPRequestMod1() {
        return new BEWSElimCIPRequestMod1();
    }

    /**
     * Create an instance of {@link ConsultarCIPMod1 }
     * 
     */
    public ConsultarCIPMod1 createConsultarCIPMod1() {
        return new ConsultarCIPMod1();
    }

    /**
     * Create an instance of {@link BEWSConsultarCIPRequestMod1 }
     * 
     */
    public BEWSConsultarCIPRequestMod1 createBEWSConsultarCIPRequestMod1() {
        return new BEWSConsultarCIPRequestMod1();
    }

    /**
     * Create an instance of {@link ConsultarSolicitudPagov2Response }
     * 
     */
    public ConsultarSolicitudPagov2Response createConsultarSolicitudPagov2Response() {
        return new ConsultarSolicitudPagov2Response();
    }

    /**
     * Create an instance of {@link BEWSConsultarSolicitudResponse }
     * 
     */
    public BEWSConsultarSolicitudResponse createBEWSConsultarSolicitudResponse() {
        return new BEWSConsultarSolicitudResponse();
    }

}
