
package pe.pagoefectivo.ws.prod.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for BEWSActualizaCIPRequestMod1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BEWSActualizaCIPRequestMod1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodServ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Firma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CIP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FechaExpira" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="InfoRequest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BEWSActualizaCIPRequestMod1", propOrder = {
    "codServ",
    "firma",
    "cip",
    "fechaExpira",
    "infoRequest"
})
public class BEWSActualizaCIPRequestMod1 {

    @XmlElement(name = "CodServ")
    protected String codServ;
    @XmlElement(name = "Firma")
    protected String firma;
    @XmlElement(name = "CIP")
    protected String cip;
    @XmlElement(name = "FechaExpira", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fechaExpira;
    @XmlElement(name = "InfoRequest")
    protected String infoRequest;

    /**
     * Gets the value of the codServ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodServ() {
        return codServ;
    }

    /**
     * Sets the value of the codServ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodServ(String value) {
        this.codServ = value;
    }

    /**
     * Gets the value of the firma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirma() {
        return firma;
    }

    /**
     * Sets the value of the firma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirma(String value) {
        this.firma = value;
    }

    /**
     * Gets the value of the cip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIP() {
        return cip;
    }

    /**
     * Sets the value of the cip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIP(String value) {
        this.cip = value;
    }

    /**
     * Gets the value of the fechaExpira property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFechaExpira() {
        return fechaExpira;
    }

    /**
     * Sets the value of the fechaExpira property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFechaExpira(XMLGregorianCalendar value) {
        this.fechaExpira = value;
    }

    /**
     * Gets the value of the infoRequest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoRequest() {
        return infoRequest;
    }

    /**
     * Sets the value of the infoRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoRequest(String value) {
        this.infoRequest = value;
    }

}
