
package pe.pagoefectivo.ws.pre.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarSolicitudPagov2Result" type="{http://tempuri.org/}BEWSConsultarSolicitudResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarSolicitudPagov2Result"
})
@XmlRootElement(name = "ConsultarSolicitudPagov2Response")
public class ConsultarSolicitudPagov2Response {

    @XmlElement(name = "ConsultarSolicitudPagov2Result")
    protected BEWSConsultarSolicitudResponse consultarSolicitudPagov2Result;

    /**
     * Gets the value of the consultarSolicitudPagov2Result property.
     * 
     * @return
     *     possible object is
     *     {@link BEWSConsultarSolicitudResponse }
     *     
     */
    public BEWSConsultarSolicitudResponse getConsultarSolicitudPagov2Result() {
        return consultarSolicitudPagov2Result;
    }

    /**
     * Sets the value of the consultarSolicitudPagov2Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link BEWSConsultarSolicitudResponse }
     *     
     */
    public void setConsultarSolicitudPagov2Result(BEWSConsultarSolicitudResponse value) {
        this.consultarSolicitudPagov2Result = value;
    }

}
