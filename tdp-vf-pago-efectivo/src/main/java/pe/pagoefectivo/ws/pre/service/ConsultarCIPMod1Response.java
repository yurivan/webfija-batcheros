
package pe.pagoefectivo.ws.pre.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ConsultarCIPMod1Result" type="{http://tempuri.org/}BEWSConsultarCIPResponseMod1" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "consultarCIPMod1Result"
})
@XmlRootElement(name = "ConsultarCIPMod1Response")
public class ConsultarCIPMod1Response {

    @XmlElement(name = "ConsultarCIPMod1Result")
    protected BEWSConsultarCIPResponseMod1 consultarCIPMod1Result;

    /**
     * Gets the value of the consultarCIPMod1Result property.
     * 
     * @return
     *     possible object is
     *     {@link BEWSConsultarCIPResponseMod1 }
     *     
     */
    public BEWSConsultarCIPResponseMod1 getConsultarCIPMod1Result() {
        return consultarCIPMod1Result;
    }

    /**
     * Sets the value of the consultarCIPMod1Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link BEWSConsultarCIPResponseMod1 }
     *     
     */
    public void setConsultarCIPMod1Result(BEWSConsultarCIPResponseMod1 value) {
        this.consultarCIPMod1Result = value;
    }

}
