
package pe.pagoefectivo.ws.pre.crypto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EncryptTextResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "encryptTextResult"
})
@XmlRootElement(name = "EncryptTextResponse")
public class EncryptTextResponse {

    @XmlElement(name = "EncryptTextResult")
    protected String encryptTextResult;

    /**
     * Gets the value of the encryptTextResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEncryptTextResult() {
        return encryptTextResult;
    }

    /**
     * Sets the value of the encryptTextResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEncryptTextResult(String value) {
        this.encryptTextResult = value;
    }

}
