
package pe.pagoefectivo.ws.pre.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="request" type="{http://tempuri.org/}BEWSElimCIPRequestMod1" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "request"
})
@XmlRootElement(name = "EliminarCIPMod1")
public class EliminarCIPMod1 {

    protected BEWSElimCIPRequestMod1 request;

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link BEWSElimCIPRequestMod1 }
     *     
     */
    public BEWSElimCIPRequestMod1 getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link BEWSElimCIPRequestMod1 }
     *     
     */
    public void setRequest(BEWSElimCIPRequestMod1 value) {
        this.request = value;
    }

}
