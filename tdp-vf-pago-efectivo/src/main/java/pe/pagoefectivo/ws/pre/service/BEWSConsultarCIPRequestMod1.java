
package pe.pagoefectivo.ws.pre.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BEWSConsultarCIPRequestMod1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BEWSConsultarCIPRequestMod1">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodServ" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Firma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CIPS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="InfoRequest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BEWSConsultarCIPRequestMod1", propOrder = {
    "codServ",
    "firma",
    "cips",
    "infoRequest"
})
public class BEWSConsultarCIPRequestMod1 {

    @XmlElement(name = "CodServ")
    protected String codServ;
    @XmlElement(name = "Firma")
    protected String firma;
    @XmlElement(name = "CIPS")
    protected String cips;
    @XmlElement(name = "InfoRequest")
    protected String infoRequest;

    /**
     * Gets the value of the codServ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodServ() {
        return codServ;
    }

    /**
     * Sets the value of the codServ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodServ(String value) {
        this.codServ = value;
    }

    /**
     * Gets the value of the firma property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirma() {
        return firma;
    }

    /**
     * Sets the value of the firma property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirma(String value) {
        this.firma = value;
    }

    /**
     * Gets the value of the cips property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIPS() {
        return cips;
    }

    /**
     * Sets the value of the cips property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIPS(String value) {
        this.cips = value;
    }

    /**
     * Gets the value of the infoRequest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoRequest() {
        return infoRequest;
    }

    /**
     * Sets the value of the infoRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoRequest(String value) {
        this.infoRequest = value;
    }

}
