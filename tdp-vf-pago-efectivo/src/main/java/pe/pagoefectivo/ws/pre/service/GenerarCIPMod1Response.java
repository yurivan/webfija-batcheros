
package pe.pagoefectivo.ws.pre.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GenerarCIPMod1Result" type="{http://tempuri.org/}BEWSGenCIPResponseMod1" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "generarCIPMod1Result"
})
@XmlRootElement(name = "GenerarCIPMod1Response")
public class GenerarCIPMod1Response {

    @XmlElement(name = "GenerarCIPMod1Result")
    protected BEWSGenCIPResponseMod1 generarCIPMod1Result;

    /**
     * Gets the value of the generarCIPMod1Result property.
     * 
     * @return
     *     possible object is
     *     {@link BEWSGenCIPResponseMod1 }
     *     
     */
    public BEWSGenCIPResponseMod1 getGenerarCIPMod1Result() {
        return generarCIPMod1Result;
    }

    /**
     * Sets the value of the generarCIPMod1Result property.
     * 
     * @param value
     *     allowed object is
     *     {@link BEWSGenCIPResponseMod1 }
     *     
     */
    public void setGenerarCIPMod1Result(BEWSGenCIPResponseMod1 value) {
        this.generarCIPMod1Result = value;
    }

}
