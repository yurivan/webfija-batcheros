package pe.pagoefectivo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.pagoefectivo.PagoEfectivo;
import pe.pagoefectivo.util.LogSystem;
import pe.pagoefectivo.ws.pre.crypto.WSCrypto;
import pe.pagoefectivo.ws.pre.crypto.WSCryptoSoap;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.nio.file.Files;

public class CifrarService {
    private static final Logger logger = LoggerFactory.getLogger(GenerarCIPService.class);

    private PagoEfectivo pagoEfectivo;
    private URL wsdlLocation;

    public CifrarService(PagoEfectivo pagoEfectivo) {
        this.pagoEfectivo = pagoEfectivo;
        try {
            wsdlLocation = new URL(pagoEfectivo.getCryptoUrl());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public String cifrar(Integer id, String txt) throws IOException {
        LogSystem.info(logger, id + ":: Cifrar", "Inicio");

        SocketAddress addr = new InetSocketAddress(pagoEfectivo.getHostnameProxy(), new Integer(pagoEfectivo.getPortProxy()));
        Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);

        HttpURLConnection uc = (HttpURLConnection) wsdlLocation.openConnection(proxy);

        uc.setDoOutput(true);
        uc.setDoInput(true);
        uc.setRequestProperty("Content-type", "text/xml");
        uc.setRequestProperty("Accept", "text/xml, application/xml");
        uc.setRequestMethod("POST");
        uc.setConnectTimeout(36000);
        uc.setReadTimeout(36000);

        Authenticator.setDefault(new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(pagoEfectivo.getUserProxy(), pagoEfectivo.getPasswordProxy().toCharArray());
            }
        });

        System.setProperty("proxySet", "true");
        System.setProperty("http.proxyHost", pagoEfectivo.getHostnameProxy());
        System.setProperty("http.proxyPort", pagoEfectivo.getPortProxy());
        System.setProperty("http.proxyUser", pagoEfectivo.getUserProxy());
        System.setProperty("http.proxyPassword", pagoEfectivo.getPasswordProxy());

        WSCrypto wscrypto = new WSCrypto(wsdlLocation);
        WSCryptoSoap wscryptoSoap = wscrypto.getWSCryptoSoap();

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(pagoEfectivo.getPublicKey()).getFile());
        byte[] dataPublic = Files.readAllBytes(file.toPath());
        String cifrado = wscryptoSoap.encryptText(txt, dataPublic);

        LogSystem.infoString(logger, id + ":: Cifrar", "cifrado", cifrado);
        LogSystem.info(logger, id + ":: Cifrar", "Fin");
        return cifrado;
    }

    public String decifrar(Integer id, String txt) throws IOException {
        LogSystem.info(logger, id + ":: Decifrar", "Inicio");

        SocketAddress addr = new InetSocketAddress(pagoEfectivo.getHostnameProxy(), new Integer(pagoEfectivo.getPortProxy()));
        Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);

        HttpURLConnection uc = (HttpURLConnection) wsdlLocation.openConnection(proxy);

        uc.setDoOutput(true);
        uc.setDoInput(true);
        uc.setRequestProperty("Content-type", "text/xml");
        uc.setRequestProperty("Accept", "text/xml, application/xml");
        uc.setRequestMethod("POST");
        uc.setConnectTimeout(36000);
        uc.setReadTimeout(36000);

        Authenticator.setDefault(new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(pagoEfectivo.getUserProxy(), pagoEfectivo.getPasswordProxy().toCharArray());
            }
        });

        System.setProperty("proxySet", "true");
        System.setProperty("http.proxyHost", pagoEfectivo.getHostnameProxy());
        System.setProperty("http.proxyPort", pagoEfectivo.getPortProxy());
        System.setProperty("http.proxyUser", pagoEfectivo.getUserProxy());
        System.setProperty("http.proxyPassword", pagoEfectivo.getPasswordProxy());

        WSCrypto wscrypto = new WSCrypto(wsdlLocation);
        WSCryptoSoap wscryptoSoap = wscrypto.getWSCryptoSoap();
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(pagoEfectivo.getPrivateKey()).getFile());
        byte[] dataPrivate = Files.readAllBytes(file.toPath());
        String decifrado = wscryptoSoap.decryptText(txt, dataPrivate);

        LogSystem.infoString(logger, id + ":: Decifrar", "decifrado", decifrado);
        LogSystem.info(logger, id + ":: Decifrar", "Fin");
        return decifrado;
    }

    public String firmar(Integer id, String txt) throws IOException {
        LogSystem.info(logger, id + ":: Firmar", "Inicio");

        SocketAddress addr = new InetSocketAddress(pagoEfectivo.getHostnameProxy(), new Integer(pagoEfectivo.getPortProxy()));
        Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);

        HttpURLConnection uc = (HttpURLConnection) wsdlLocation.openConnection(proxy);

        uc.setDoOutput(true);
        uc.setDoInput(true);
        uc.setRequestProperty("Content-type", "text/xml");
        uc.setRequestProperty("Accept", "text/xml, application/xml");
        uc.setRequestMethod("POST");
        uc.setConnectTimeout(36000);
        uc.setReadTimeout(36000);

        Authenticator.setDefault(new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(pagoEfectivo.getUserProxy(), pagoEfectivo.getPasswordProxy().toCharArray());
            }
        });

        System.setProperty("proxySet", "true");
        System.setProperty("http.proxyHost", pagoEfectivo.getHostnameProxy());
        System.setProperty("http.proxyPort", pagoEfectivo.getPortProxy());
        System.setProperty("http.proxyUser", pagoEfectivo.getUserProxy());
        System.setProperty("http.proxyPassword", pagoEfectivo.getPasswordProxy());

        WSCrypto wscrypto = new WSCrypto(wsdlLocation);
        WSCryptoSoap wscryptoSoap = wscrypto.getWSCryptoSoap();
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(pagoEfectivo.getPrivateKey()).getFile());
        byte[] dataPrivate = Files.readAllBytes(file.toPath());
        String firma = wscryptoSoap.signer(txt, dataPrivate);

        LogSystem.infoString(logger, id + ":: Firmar", "firma", firma);
        LogSystem.info(logger, id + ":: Firmar", "Fin");
        return firma;
    }
}
