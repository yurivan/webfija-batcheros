package pe.pagoefectivo.service;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pe.pagoefectivo.PagoEfectivo;
import pe.pagoefectivo.service.ws.BEWSConsultarRequest;
import pe.pagoefectivo.service.ws.BEWSConsultarResponse;
import pe.pagoefectivo.ws.pre.service.BEWSConsultarCIPRequestMod1;
import pe.pagoefectivo.ws.pre.service.BEWSConsultarCIPResponseMod1;
import pe.pagoefectivo.ws.pre.service.Service;
import pe.pagoefectivo.ws.pre.service.ServiceSoap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.*;

public class ConsultarCIPService {

    private PagoEfectivo pagoEfectivo;
    private CifrarService cifrarService;

    public ConsultarCIPService(PagoEfectivo pagoEfectivo) {
        this.pagoEfectivo = pagoEfectivo;
        this.cifrarService = new CifrarService(pagoEfectivo);
    }

    public BEWSConsultarResponse consultar(BEWSConsultarRequest bEWSConsultarRequest) {

        BEWSConsultarCIPRequestMod1 requestPagoEfectivo = new BEWSConsultarCIPRequestMod1();
        BEWSConsultarCIPResponseMod1 responsePagoEfectivo = null;
        BEWSConsultarResponse consultaResponse = new BEWSConsultarResponse();

        String xml = "";
        String estado = "";
        String mensaje = "";
        String info = "";

        try {

            requestPagoEfectivo.setCodServ(pagoEfectivo.getCodigoServicio());
            requestPagoEfectivo.setFirma(cifrarService.firmar(Integer.parseInt(bEWSConsultarRequest.getNumeroCIP()), bEWSConsultarRequest.getNumeroCIP()));
            requestPagoEfectivo.setCIPS(cifrarService.cifrar(Integer.parseInt(bEWSConsultarRequest.getNumeroCIP()), bEWSConsultarRequest.getNumeroCIP()));
            requestPagoEfectivo.setInfoRequest("");

            SocketAddress addr = new InetSocketAddress(pagoEfectivo.getHostnameProxy(), new Integer(pagoEfectivo.getPortProxy()));
            Proxy proxy = new Proxy(Proxy.Type.HTTP, addr);
            URL url = new URL(pagoEfectivo.getServicesUrl());
            HttpURLConnection uc = (HttpURLConnection)url.openConnection(proxy);
            //HttpURLConnection uc = (HttpURLConnection) url.openConnection();
            //byte[] userPass = (pagoEfectivo.getUserProxy()+":"+pagoEfectivo.getPasswordProxy()).getBytes(StandardCharsets.UTF_8);
            //String basicAuth = Base64.getEncoder().encodeToString(userPass);
            //uc.setRequestProperty("Authorization", basicAuth);
            uc.connect();

            Service service = new Service(url);
            ServiceSoap wscipSoap = service.getServiceSoap();
            responsePagoEfectivo = wscipSoap.consultarCIPMod1(requestPagoEfectivo);

            consultaResponse = new BEWSConsultarResponse();

            if (responsePagoEfectivo != null) {

                estado = responsePagoEfectivo.getEstado();
                mensaje = responsePagoEfectivo.getMensaje();
                info = responsePagoEfectivo.getInfoResponse();

                xml = responsePagoEfectivo.getXML();

                if (estado != null) {
                    System.out.println("estado = " + estado);
                }
                if (mensaje != null) {
                    System.out.println("mensaje = " + mensaje);
                }
                if (info != null) {
                    System.out.println("info = " + info);
                }

                if (xml != null) {

                    xml = cifrarService.decifrar(Integer.parseInt(bEWSConsultarRequest.getNumeroCIP()), responsePagoEfectivo.getXML());
                    DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    Document doc = db.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
                    NodeList nlSolpagos = doc.getElementsByTagName("ConfirSolPagos");

                    if (nlSolpagos.getLength() > 0) {

                        NodeList nlSolpago = ((Element) nlSolpagos.item(0)).getElementsByTagName("ConfirSolPago");

                        if (nlSolpago.getLength() > 0) {

                            NodeList nlCIP = ((Element) nlSolpago.item(0)).getElementsByTagName("CIP");

                            if (nlCIP.getLength() > 0) {

                                NodeList nlIdCIP = ((Element) nlCIP.item(0)).getElementsByTagName("IdOrdenPago");
                                NodeList nlIdCIP2 = ((Element) nlCIP.item(0)).getElementsByTagName("NumeroOrdenPago");

                                System.out.println("Numero CIP= " + nlIdCIP.item(0).getTextContent());
                                System.out.println("NumeroOrdenPago: " + nlIdCIP2.item(0).getTextContent());
                                consultaResponse.setNumeroCIP(nlIdCIP.item(0).getTextContent());

                                NodeList nlEstadoCIP = ((Element) nlCIP.item(0)).getElementsByTagName("IdEstado");

                                if (nlEstadoCIP.item(0) != null) {
                                    if (nlEstadoCIP.item(0).getTextContent() != null) {
                                        System.out.println("Estado CIP= " + nlEstadoCIP.item(0).getTextContent());

                                        if (nlEstadoCIP.item(0).getTextContent().equalsIgnoreCase("21")) {
                                            consultaResponse.setEstadoCIP("Expirado");
                                        }
                                        if (nlEstadoCIP.item(0).getTextContent().equalsIgnoreCase("22")) {
                                            consultaResponse.setEstadoCIP("Pendiente de pago");
                                        }
                                        if (nlEstadoCIP.item(0).getTextContent().equalsIgnoreCase("23")) {
                                            consultaResponse.setEstadoCIP("Pagado");
                                        }
                                        if (nlEstadoCIP.item(0).getTextContent().equalsIgnoreCase("25")) {
                                            consultaResponse.setEstadoCIP("Eliminado");
                                        }

                                        consultaResponse.setIdEstadoCIP(nlEstadoCIP.item(0).getTextContent());
                                    }
                                }

                                NodeList nlFechaCanceladoCIP = ((Element) nlCIP.item(0)).getElementsByTagName("FechaCancelado");
                                System.out.println("Fecha Cancelado CIP= " + nlFechaCanceladoCIP.item(0).getTextContent());
                                consultaResponse.setFechaPagoCIP(nlFechaCanceladoCIP.item(0).getTextContent());

                            }

                        }

                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        return consultaResponse;
    }
}
