package pe.pagoefectivo.service.helper;

import pe.pagoefectivo.service.ws.BEGenRequest;

public class Helper {
	public String convertToXml(BEGenRequest paymentRequest)
    {
        String xml = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
            "<SolPago>" +
                "<IdMoneda>"+ paymentRequest.getMoneda() +"</IdMoneda>" +
                "<Total>" + paymentRequest.getMonto() + "</Total>" +
                "<MetodosPago>" + paymentRequest.getMedio_pago() + "</MetodosPago>" +
                "<CodServicio>" + paymentRequest.getCod_servicio() + "</CodServicio>" +
                "<Codtransaccion>" + paymentRequest.getNumero_orden() + "</Codtransaccion>" +
                "<ConceptoPago>" + paymentRequest.getConcepto_pago() + "</ConceptoPago>" +
                "<EmailComercio>" + paymentRequest.getEmail_comercio() + "</EmailComercio>" +
                "<FechaAExpirar>" + paymentRequest.getFecha_expirar() + "</FechaAExpirar>" +
                "<UsuarioId>" + paymentRequest.getUsuario_id() + "</UsuarioId>" +
                "<UsuarioNombre>" + paymentRequest.getUsuario_nombre() + "</UsuarioNombre>" +
                "<UsuarioApellidos>" + paymentRequest.getUsuario_apellidos() + "</UsuarioApellidos>" +
                "<UsuarioTipoDoc>" + paymentRequest.getUsuario_tipodocumento() + "</UsuarioTipoDoc>" +
                "<UsuarioNumeroDoc>" + paymentRequest.getUsuario_numerodocumento() + "</UsuarioNumeroDoc>" +
                "<UsuarioEmail>" + paymentRequest.getUsuario_email() + "</UsuarioEmail>" +
                "<Detalles>" +
                    "<Detalle>" +
                        "<Cod_Origen>CT</Cod_Origen>" +
                        "<TipoOrigen>TO</TipoOrigen>" +
                        "<ConceptoPago>" + paymentRequest.getConcepto_pago() + "</ConceptoPago>" +
                        "<Importe>" + paymentRequest.getMonto() + "</Importe>" +
                    "</Detalle>" +
                "</Detalles>" +
            "</SolPago>";
        return xml;            
    }

}
