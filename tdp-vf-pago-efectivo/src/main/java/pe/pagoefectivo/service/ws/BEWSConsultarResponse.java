package pe.pagoefectivo.service.ws;

public class BEWSConsultarResponse {

    private String numeroCIP;
    private String idEstadoCIP;
    private String estadoCIP;
    private String fechaPagoCIP;

    public BEWSConsultarResponse(){
        super();
    }

    public String getNumeroCIP() {
        return numeroCIP;
    }

    public void setNumeroCIP(String numeroCIP) {
        this.numeroCIP = numeroCIP;
    }

    public String getIdEstadoCIP() {
        return idEstadoCIP;
    }

    public void setIdEstadoCIP(String idEstadoCIP) {
        this.idEstadoCIP = idEstadoCIP;
    }

    public String getEstadoCIP() {
        return estadoCIP;
    }

    public void setEstadoCIP(String estadoCIP) {
        this.estadoCIP = estadoCIP;
    }

    public String getFechaPagoCIP() {
        return fechaPagoCIP;
    }

    public void setFechaPagoCIP(String fechaPagoCIP) {
        this.fechaPagoCIP = fechaPagoCIP;
    }
}
