package pe.pagoefectivo.service.ws;

public class BEWSConsultarRequest {

    private String numeroCIP;

    public BEWSConsultarRequest(){
        super();
    }

    public String getNumeroCIP() {
        return numeroCIP;
    }

    public void setNumeroCIP(String numeroCIP) {
        this.numeroCIP = numeroCIP;
    }
}
