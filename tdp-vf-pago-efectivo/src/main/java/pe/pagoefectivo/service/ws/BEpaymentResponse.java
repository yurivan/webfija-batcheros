package pe.pagoefectivo.service.ws;

public class BEpaymentResponse {
    public String NumeroCip = "";
    public String IdComercio = "";
    public String Token = "";
    public String Estado = "";
    public String Mensaje = "";
    public String Xml = "";
}
