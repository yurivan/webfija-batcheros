package pe.pagoefectivo.service.ws;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.text.NumberFormatter;

import pe.pagoefectivo.service.ws.enums.MetodoPago;
import pe.pagoefectivo.service.ws.enums.Moneda;

public class BEGenRequest {
	private String moneda = "";
	private String monto = "";
	private String medio_pago = "";
	private String concepto_pago = "";
	private String cod_servicio = "";
	private String numero_orden = "";
	private String email_comercio = "";
	private String fecha_expirar = "";
	private String data_adicional = "";
	private String usuario_id = "";
	private String usuario_nombre = "";
	private String usuario_apellidos = "";
	private String usuario_localidad = "";
	private String usuario_provincia = "";
	private String usuario_pais = "";
	private String usuario_alias = "";
	private String usuario_tipodocumento = "";
	private String usuario_numerodocumento = "";
	private String usuario_email = "";

	protected BEGenRequest(Builder builder) {
		this.moneda = builder.moneda;
		this.monto = builder.monto;
		this.medio_pago = builder.medio_pago;
		this.concepto_pago = builder.concepto_pago;
		this.cod_servicio = builder.cod_servicio;
		this.numero_orden = builder.numero_orden;
		this.email_comercio = builder.email_comercio;
		this.fecha_expirar = builder.fecha_expirar;
		this.data_adicional = builder.data_adicional;
		this.usuario_id = builder.usuario_id;
		this.usuario_nombre = builder.usuario_nombre;
		this.usuario_apellidos = builder.usuario_apellidos;
		this.usuario_localidad = builder.usuario_localidad;
		this.usuario_provincia = builder.usuario_provincia;
		this.usuario_pais = builder.usuario_pais;
		this.usuario_alias = builder.usuario_alias;
		this.usuario_tipodocumento = builder.usuario_tipodocumento;
		this.usuario_numerodocumento = builder.usuario_numerodocumento;
		this.usuario_email = builder.usuario_email;
	}

	public String getMoneda() {
		return moneda;
	}

	public String getMonto() {
		return monto;
	}

	public String getMedio_pago() {
		return medio_pago;
	}

	public String getConcepto_pago() {
		return concepto_pago;
	}

	public String getCod_servicio() {
		return cod_servicio;
	}

	public String getNumero_orden() {
		return numero_orden;
	}

	public String getEmail_comercio() {
		return email_comercio;
	}

	public String getFecha_expirar() {
		return fecha_expirar;
	}

	public String getData_adicional() {
		return data_adicional;
	}

	public String getUsuario_id() {
		return usuario_id;
	}

	public String getUsuario_nombre() {
		return usuario_nombre;
	}

	public String getUsuario_apellidos() {
		return usuario_apellidos;
	}

	public String getUsuario_localidad() {
		return usuario_localidad;
	}

	public String getUsuario_provincia() {
		return usuario_provincia;
	}

	public String getUsuario_pais() {
		return usuario_pais;
	}

	public String getUsuario_alias() {
		return usuario_alias;
	}

	public String getUsuario_tipodocumento() {
		return usuario_tipodocumento;
	}

	public String getUsuario_numerodocumento() {
		return usuario_numerodocumento;
	}

	public String getUsuario_email() {
		return usuario_email;
	}

	public static class Builder {
		private String moneda = "";
		private String monto = "";
		private String medio_pago = "";
		private String concepto_pago = "";
		private String cod_servicio = "";
		private String numero_orden = "";
		private String email_comercio = "";
		private String fecha_expirar = "";
		private String data_adicional = "";
		private String usuario_id = "";
		private String usuario_nombre = "";
		private String usuario_apellidos = "";
		private String usuario_localidad = "";
		private String usuario_provincia = "";
		private String usuario_pais = "";
		private String usuario_alias = "";
		private String usuario_tipodocumento = "";
		private String usuario_numerodocumento = "";
		private String usuario_email = "";

		public String getMoneda() {
			return moneda;
		}

		public Builder setMoneda(Moneda m) {
			this.moneda = m.getCodigo();
			return this;
		}

		public Builder setMoneda(String m) {
			this.moneda = m;
			return this;
		}

		public String getMonto() {
			return monto;
		}

		public Builder setMonto(BigDecimal monto) throws ParseException {
			DecimalFormatSymbols dfs = new DecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			DecimalFormat decimalFormat = new DecimalFormat("0.00", dfs);
			NumberFormatter nf = new NumberFormatter(decimalFormat);
			this.monto = nf.valueToString(monto);
			return this;
		}

		public Builder setMonto(String monto) {
			this.monto = monto;
			return this;
		}

		public String getMedio_pago() {
			return medio_pago;
		}
		
		public Builder setMedio_pago(String metodosPago) {
			this.medio_pago = metodosPago;
			return this;
		}

		public Builder setMedio_pago(List<MetodoPago> metodosPago) {
			this.medio_pago = "";
			int count = 0;
			for (MetodoPago mp : metodosPago) {
				String medioPago = mp.getCodigo();
				if (count > 0) {
					this.medio_pago += ",";
				}
				this.medio_pago += medioPago;
				count++;
			}
			return this;
		}

		public String getConcepto_pago() {
			return concepto_pago;
		}

		public Builder setConcepto_pago(String concepto_pago) {
			this.concepto_pago = concepto_pago;
			return this;
		}

		public String getCod_servicio() {
			return cod_servicio;
		}

		public Builder setCod_servicio(String cod_servicio) {
			this.cod_servicio = cod_servicio;
			return this;
		}

		public String getNumero_orden() {
			return numero_orden;
		}

		public Builder setNumero_orden(String numero_orden) {
			this.numero_orden = numero_orden;
			return this;
		}

		public String getEmail_comercio() {
			return email_comercio;
		}

		public Builder setEmail_comercio(String email_comercio) {
			this.email_comercio = email_comercio;
			return this;
		}

		public String getFecha_expirar() {
			return fecha_expirar;
		}

		public Builder setFecha_expirar(Date fechaAExpirar) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
			this.fecha_expirar = sdf.format(fechaAExpirar);
			return this;
		}

		public Builder setFecha_expirar(String fechaAExpirar) {
			this.fecha_expirar = fechaAExpirar;
			return this;
		}

		public String getData_adicional() {
			return data_adicional;
		}

		public Builder setData_adicional(String data_adicional) {
			this.data_adicional = data_adicional;
			return this;
		}

		public String getUsuario_id() {
			return usuario_id;
		}

		public Builder setUsuario_id(String usuario_id) {
			this.usuario_id = usuario_id;
			return this;
		}

		public String getUsuario_nombre() {
			return usuario_nombre;
		}

		public Builder setUsuario_nombre(String usuario_nombre) {
			this.usuario_nombre = usuario_nombre;
			return this;
		}

		public String getUsuario_apellidos() {
			return usuario_apellidos;
		}

		public Builder setUsuario_apellidos(String usuario_apellidos) {
			this.usuario_apellidos = usuario_apellidos;
			return this;
		}

		public String getUsuario_localidad() {
			return usuario_localidad;
		}

		public Builder setUsuario_localidad(String usuario_localidad) {
			this.usuario_localidad = usuario_localidad;
			return this;
		}

		public String getUsuario_provincia() {
			return usuario_provincia;
		}

		public Builder setUsuario_provincia(String usuario_provincia) {
			this.usuario_provincia = usuario_provincia;
			return this;
		}

		public String getUsuario_pais() {
			return usuario_pais;
		}

		public Builder setUsuario_pais(String usuario_pais) {
			this.usuario_pais = usuario_pais;
			return this;
		}

		public String getUsuario_alias() {
			return usuario_alias;
		}

		public Builder setUsuario_alias(String usuario_alias) {
			this.usuario_alias = usuario_alias;
			return this;
		}

		public String getUsuario_tipodocumento() {
			return usuario_tipodocumento;
		}

		public Builder setUsuario_tipodocumento(String usuario_tipodocumento) {
			this.usuario_tipodocumento = usuario_tipodocumento;
			return this;
		}

		public String getUsuario_numerodocumento() {
			return usuario_numerodocumento;
		}

		public Builder setUsuario_numerodocumento(String usuario_numerodocumento) {
			this.usuario_numerodocumento = usuario_numerodocumento;
			return this;
		}

		public String getUsuario_email() {
			return usuario_email;
		}

		public Builder setUsuario_email(String usuario_email) {
			this.usuario_email = usuario_email;
			return this;
		}
		
		public BEGenRequest build () {
			return new BEGenRequest(this);
		}
	}

}