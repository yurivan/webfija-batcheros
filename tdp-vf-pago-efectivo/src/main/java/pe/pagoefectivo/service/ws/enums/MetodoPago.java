package pe.pagoefectivo.service.ws.enums;

public enum MetodoPago {
	BANCO("1"), MONEDERO_VIRTUAL("2");
	
	private String codigo;
	
	private MetodoPago (String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}
}
