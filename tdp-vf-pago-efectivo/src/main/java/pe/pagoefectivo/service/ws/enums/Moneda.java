package pe.pagoefectivo.service.ws.enums;

public enum Moneda {
	SOLES("1"), DOLARES("2");
	
	private String codigo = "";

	private Moneda (String codigo) {
		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}
}
