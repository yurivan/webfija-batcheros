package pe.pagoefectivo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pe.pagoefectivo.PagoEfectivo;
import pe.pagoefectivo.service.helper.Helper;
import pe.pagoefectivo.service.ws.BEGenRequest;
import pe.pagoefectivo.service.ws.BEpaymentResponse;
import pe.pagoefectivo.util.LogSystem;
import pe.pagoefectivo.ws.pre.service.BEWSGenCIPRequestMod1;
import pe.pagoefectivo.ws.pre.service.BEWSGenCIPResponseMod1;
import pe.pagoefectivo.ws.pre.service.Service;
import pe.pagoefectivo.ws.pre.service.ServiceSoap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;

public class GenerarCIPService {
    private static final Logger logger = LoggerFactory.getLogger(GenerarCIPService.class);

    private PagoEfectivo pagoEfectivo;
    private CifrarService cifrarService;

    public GenerarCIPService(PagoEfectivo pagoEfectivo) {
        this.pagoEfectivo = pagoEfectivo;
        this.cifrarService = new CifrarService(pagoEfectivo);
    }

    public BEpaymentResponse generar(Integer id, BEGenRequest paymentRequest) {
        LogSystem.info(logger, id + ":: Generar", "Inicio");
        BEWSGenCIPRequestMod1 solicitud = new BEWSGenCIPRequestMod1();

        Helper helper = new Helper();
        String xml = helper.convertToXml(paymentRequest);
        xml = xml != null ? xml.trim() : xml;

        BEWSGenCIPResponseMod1 response = new BEWSGenCIPResponseMod1();
        BEpaymentResponse paymentResponse = null;

        LogSystem.infoString(logger, id + ":: Generar", "xml", xml);
        try {
            URL proxyUrl = new URL(String.format("http://%s:%s@%s:%s", pagoEfectivo.getUserProxy(), pagoEfectivo.getPasswordProxy(), pagoEfectivo.getHostnameProxy(), pagoEfectivo.getPortProxy()));
            String userInfo = proxyUrl.getUserInfo();
            String user = userInfo.substring(0, userInfo.indexOf(':'));
            String password = userInfo.substring(userInfo.indexOf(':') + 1);

            HttpURLConnection uc = null;
            System.setProperty("http.proxyHost", proxyUrl.getHost());
            System.setProperty("http.proxyPort", Integer.toString(proxyUrl.getPort()));

            Authenticator.setDefault(new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(user, password.toCharArray());
                }
            });

            solicitud.setCodServ(pagoEfectivo.getCodigoServicio());
            solicitud.setXml(cifrarService.cifrar(id, xml));
            solicitud.setFirma(cifrarService.firmar(id, xml));
            LogSystem.infoObject(logger, id + ":: Generar", "solicitud", solicitud);

            URL url = new URL(pagoEfectivo.getServicesUrl());

            uc = (HttpURLConnection) url.openConnection();
            uc.usingProxy();
            uc.setDoOutput(true);
            uc.setDoInput(true);
            uc.setRequestProperty("Content-type", "text/xml");
            uc.setRequestProperty("Accept", "text/xml, application/xml");
            uc.setRequestMethod("POST");

            Service service = new Service(url);
            ServiceSoap wscipSoap = service.getServiceSoap();
            response = wscipSoap.generarCIPMod1(solicitud);
            LogSystem.infoObject(logger, id + ":: Generar", "response", response);

            paymentResponse = new BEpaymentResponse();

            if (response != null) {
                paymentResponse.Estado = response.getEstado();
                paymentResponse.Mensaje = response.getMensaje();

                xml = response.getXml();
                if (xml != null) {
                    xml = cifrarService.decifrar(id, response.getXml());
                    DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                    Document doc = db.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
                    NodeList nlSolpago = doc.getElementsByTagName("ConfirSolPago");
                    if (nlSolpago.getLength() > 0) {
                        NodeList nlCIP = ((Element) nlSolpago.item(0)).getElementsByTagName("CIP");
                        if (nlCIP.getLength() > 0) {
                            NodeList nlNumeroCIP = ((Element) nlCIP.item(0)).getElementsByTagName("NumeroOrdenPago");
                            paymentResponse.NumeroCip = nlNumeroCIP.item(0).getTextContent();

                            NodeList nlIdComercio = ((Element) nlCIP.item(0)).getElementsByTagName("MerchantID");
                            paymentResponse.IdComercio = nlIdComercio.item(0).getTextContent();
                        }
                        NodeList nlToken = ((Element) nlSolpago.item(0)).getElementsByTagName("Token");
                        paymentResponse.Token = nlToken.item(0).getTextContent();
                    }
                }
            }
        } catch (IOException e) {
            LogSystem.error(logger, id + ":: Generar", "IOException", e);
        } catch (ParserConfigurationException e) {
            LogSystem.error(logger, id + ":: Generar", "ParserConfigurationException", e);
        } catch (SAXException e) {
            LogSystem.error(logger, id + ":: Generar", "SAXException", e);
        }
        LogSystem.info(logger, id + ":: Generar", "Fin");
        return paymentResponse;
    }
}
