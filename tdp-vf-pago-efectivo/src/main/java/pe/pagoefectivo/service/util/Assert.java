package pe.pagoefectivo.service.util;

public abstract class Assert {

	public static void notNull (Object o, String message) {
		if (o == null) {
			throw new IllegalArgumentException(message);
		}
	}
}
