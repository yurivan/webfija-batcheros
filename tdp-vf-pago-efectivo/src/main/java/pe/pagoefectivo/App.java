package pe.pagoefectivo;

import pe.pagoefectivo.service.GenerarCIPService;
import pe.pagoefectivo.service.ws.BEGenRequest;
import pe.pagoefectivo.service.ws.BEpaymentResponse;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

public class App {

	public static void main (String [] args) throws KeyManagementException, NoSuchAlgorithmException, IOException, ParseException {
		PagoEfectivo PE = new PagoEfectivo.Builder()
				.setPrivateKey("/ms/config/MOV_PrivateKey.1pz")
				.setPublicKey("/ms/config/SPE_PublicKey.1pz")
				.setCryptoUrl("https://cip.pre.2b.pagoefectivo.pe/PagoEfectivoWSCrypto/WSCrypto.asmx?WSDL")
				.setServicesUrl("https://cip.pre.2b.pagoefectivo.pe/PagoEfectivoWSGeneralv2/service.asmx?WSDL")
				.setCodigoServicio("MOV")
				.build();
		
		GenerarCIPService service = new GenerarCIPService(PE);
		
//		DecimalFormatSymbols dfs = new DecimalFormatSymbols();
//		dfs.setDecimalSeparator(',');
//		DecimalFormat decimalFormat = new DecimalFormat("0.00", dfs);
//		NumberFormatter nf = new NumberFormatter(decimalFormat);
//		System.out.println(nf.valueToString(new BigDecimal("123.302")));
//		System.out.println(nf.valueToString(new BigDecimal("0.3")));
		BEGenRequest rq = new BEGenRequest.Builder()
				.setMoneda("1")
				.setMonto(new BigDecimal("34"))
				.setMedio_pago("1")
				.setConcepto_pago("CASScER")
				.setCod_servicio("MOV")
				.setNumero_orden("0000047")
				.setEmail_comercio("jvilcayp@everis.com")
				.setFecha_expirar("31/12/2016 17:00:00")
				.setData_adicional("")
				.setUsuario_id("001")
				.setUsuario_nombre("Cascader")
				.setUsuario_apellidos("Cascader")
				.build();
//	    rq.usuario_alias = "Alex";
//	    rq.usuario_tipodocumento = "1";
//	    rq.usuario_numerodocumento = "1";
//	    rq.usuario_email = "alex.castillo@perucomsultores.pe";
		
        BEpaymentResponse rs = service.generar(0, rq);
		System.out.println(rs.NumeroCip);
	}
}
