package pe.pagoefectivo;

import pe.pagoefectivo.service.util.Assert;

public class PagoEfectivo {
	private String codigoServicio;
	private String privateKey;
	private String publicKey;
	private String cryptoUrl;
	private String servicesUrl;
	private String hostnameProxy;
	private String portProxy;
	private String userProxy;
	private String passwordProxy;

	protected PagoEfectivo (Builder builder) {
		this.privateKey = builder.privateKey;
		this.publicKey = builder.publicKey;
		this.cryptoUrl = builder.cryptoUrl;
		this.servicesUrl = builder.servicesUrl;
		this.codigoServicio = builder.codigoServicio;
		this.hostnameProxy = builder.hostnameProxy;
		this.portProxy = builder.portProxy;
		this.userProxy = builder.userProxy;
		this.passwordProxy = builder.passwordProxy;

	}

	public String getPrivateKey() {
		return privateKey;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public String getCryptoUrl() {
		return cryptoUrl;
	}

	public String getServicesUrl() {
		return servicesUrl;
	}

	public String getCodigoServicio() {
		return codigoServicio;
	}

	public String getHostnameProxy() {	return hostnameProxy;	}

	public String getPortProxy() {	return portProxy;	}

	public String getUserProxy() {	return userProxy;	}

	public String getPasswordProxy() {	return passwordProxy; }

	public static class Builder {
		private String codigoServicio;
		private String privateKey;
		private String publicKey;
		private String cryptoUrl;
		private String servicesUrl;
		private String hostnameProxy;
		private String portProxy;
		private String userProxy;
		private String passwordProxy;

		public String getPrivateKey() {
			return privateKey;
		}

		public Builder setPrivateKey(String privateKey) {
			this.privateKey = privateKey;
			return this;
		}

		public String getPublicKey() {
			return publicKey;
		}

		public Builder setPublicKey(String publicKey) {
			this.publicKey = publicKey;
			return this;
		}

		public String getCryptoUrl() {
			return cryptoUrl;
		}

		public Builder setCryptoUrl(String cryptoUrl) {
			this.cryptoUrl = cryptoUrl;
			return this;
		}

		public String getServicesUrl() {
			return servicesUrl;
		}

		public Builder setServicesUrl(String servicesUrl) {
			this.servicesUrl = servicesUrl;
			return this;
		}

		public String getCodigoServicio() {
			return codigoServicio;
		}

		public Builder setCodigoServicio(String codigoServicio) {
			this.codigoServicio = codigoServicio;
			return this;
		}

		public String getHostnameProxy() {	return hostnameProxy;	}


		public Builder setHostnameProxy(String hostnameProxy) {
			this.hostnameProxy = hostnameProxy;
			return this;
		}

		public String getPortProxy() {	return portProxy;	}

		public Builder setPortProxy(String portProxy) {
			this.portProxy = portProxy;
			return this;
		}

		public String getUserProxy() {	return userProxy;	}


		public Builder setUserProxy(String userProxy) {
			this.userProxy = userProxy;
			return this;
		}

		public String getPasswordProxy() {	return passwordProxy;	}

		public Builder setPasswordProxy(String passwordProxy) {
			this.passwordProxy = passwordProxy;
			return this;
		}

		public PagoEfectivo build () {
			Assert.notNull(publicKey, "Parametro public key es obligatorio");
			Assert.notNull(privateKey, "Parametro private key es obligatorio");
			Assert.notNull(codigoServicio, "Parametro codigo de servicio es obligatorio");
			Assert.notNull(cryptoUrl, "Parametro URL WSCrypto es obligatorio");
			Assert.notNull(servicesUrl, "Parametro URL WSServices es obligatorio");
			Assert.notNull(hostnameProxy, "Parametro PROXY Host es obligatorio");
			Assert.notNull(portProxy, "Parametro PROXY Port es obligatorio");
			Assert.notNull(userProxy, "Parametro PROXY User es obligatorio");
			Assert.notNull(passwordProxy, "Parametro PROXY Password es obligatorio");
			return new PagoEfectivo(this);
		}
	}
}
