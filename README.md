# README #

This README file provides basic information to deploy the following applications:

* tdp-vf-backend - RESTful backend for Web and App clients
* tdp-vf-job-batch - Chron job executed by SpringBoot
* tdp-vf-thirdparty - Bypass to El Comercio
* tdp-web-admin-client - tdp-web-venta's front end
* tdp-web-admin-server - Administration web backend
* tdp-web-venta - tdp-vf-backend's front end
* tdp-vf-toa - toa web services
* tdp-vf-tracer - tracer job and Emblue API connection
* tdp-vf-ffmpeg-api - file converter API inside a docker container


All of these are put in a CloudFoundry environment accessed v�a Jenkins.

### Purpose ###

Provide basic references to load up the Java environment for the Landline selling App environment.

### Setting up environment ###

It's recommended to use the Gradle console environment. The validated Gradle version is v3.5, wich is different than v3.5.1.

It's highly recommended to use PostreSQL 9.6 with any database viewer such as NaviCat / PgAdmin 3 / PgAdmin 4

Also, every host should use the following system properties:

Every Host must have these:

* GRADLE_HOME = Findable Folder for the user
* JAVA_HOME = Findable Folder for the user


The following System properties will support the DAO layer

* TDP_FIJA_DB_URL = jdbc:postgresql://xxxx:xxxx/compose?currentSchema=xxxx=false&characterEncoding=utf-8
* TDP_FIJA_DB_DRIVER = org.postgresql.Driver
* TDP_FIJA_DB_PW = GQOYAWZYTAMOWBYC
* TDP_FIJA_DB_USR = admin


### Solution's follow up ###

This repository is linked to a Jenkins server wich provides the deployment scripts for the Testing environment.

The following link will execute an update in the Testing environment (it's execution is programmed to be lauched every 30 minutes if a change in the repo is detected.

APP PUSHES: https://bitbucket.org/vassappfija/tdp-vf-servers


To obtain a follow up for the Backend progress, the following GET method have been set up (note the user and token), which can be executed v�a HTTPie or Postman

* APP LOGGING URI: http://181.224.229.90:8575/job/TDP-FIJA-CI/lastBuild/consoleText
* APP LOGGING AUTH: Basic
* APP LOGGING USER: appfija
* APP LOGGING TOKEN: 841c263bf71685d5e97f28e9f67d5faf

To launch a deployment on Development and Testing Envs v�a API:

* Development: http://181.224.229.90:8575/job/TDP-FIJA-CI/build
* Testing: http://181.224.229.90:8575/job/TDP-FIJA-CI-TESTING/build

Both executions are POST and return 201 Created response



### Who do I talk to? ###

* Javier Vera (javier.vera@vasslatam.com)