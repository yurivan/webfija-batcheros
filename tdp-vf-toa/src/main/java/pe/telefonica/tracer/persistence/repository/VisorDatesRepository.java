package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.VisorByDocDB;
import pe.telefonica.tracer.persistence.VisorDatesDB;

import java.sql.Timestamp;
import java.util.List;

@Transactional()
public interface VisorDatesRepository extends JpaRepository<VisorDatesDB, Long> {
    @Modifying
    @Query("UPDATE VisorDatesDB v SET v.fecha_equipo_tecnico = :techAssignDate WHERE v.codigo_pedido = :requestCode")
    public int updateTechAssignDate(@Param("requestCode") String requestCode, @Param("techAssignDate") Timestamp techAssignDate);

    @Modifying
    @Query("UPDATE VisorDatesDB v SET v.fecha_instalado = :installedDate WHERE v.codigo_pedido = :requestCode")
    public int updateInstalledDate(@Param("requestCode") String requestCode, @Param("installedDate") Timestamp installedDate);
}

