package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TDP_VISOR", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class VisorByRequestCodeDateDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id_visor")
    private String id;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    @Column(name = "fecha_solicitado")
    private Timestamp fecha_solicitado;

    @Column(name = "fecha_registrado")
    private Timestamp fecha_registrado;

    @Column(name = "fecha_equipo_tecnico")
    private Timestamp fecha_equipo_tecnico;

    @Column(name = "fecha_instalado")
    private Timestamp fecha_instalado;

    public VisorByRequestCodeDateDB(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public Timestamp getFecha_solicitado() {
        return fecha_solicitado;
    }

    public void setFecha_solicitado(Timestamp fecha_solicitado) {
        this.fecha_solicitado = fecha_solicitado;
    }

    public Timestamp getFecha_registrado() {
        return fecha_registrado;
    }

    public void setFecha_registrado(Timestamp fecha_registrado) {
        this.fecha_registrado = fecha_registrado;
    }

    public Timestamp getFecha_equipo_tecnico() {
        return fecha_equipo_tecnico;
    }

    public void setFecha_equipo_tecnico(Timestamp fecha_equipo_tecnico) {
        this.fecha_equipo_tecnico = fecha_equipo_tecnico;
    }

    public Timestamp getFecha_instalado() {
        return fecha_instalado;
    }

    public void setFecha_instalado(Timestamp fecha_instalado) {
        this.fecha_instalado = fecha_instalado;
    }

}
