package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.PublicCustomerDataDB;

@Transactional()
public interface PublicCustomerDataRepository extends JpaRepository<PublicCustomerDataDB, Long> {

    @Query(value = "SELECT pc.id, pc.documento, pc.telefono3, pc.telefono4, pc.direccion2, pc.correo2, pc.rango_fecha_inicio, pc.rango_fecha_fin ,pc.insert_update" +
            " FROM ibmx_a07e6d02edaf552.public_customer pc WHERE pc.documento = :documento", nativeQuery = true)
    public PublicCustomerDataDB getPublicCustomerData(@Param("documento") final String documento);

}

