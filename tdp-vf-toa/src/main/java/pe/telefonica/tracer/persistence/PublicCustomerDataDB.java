package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;


@Entity
@Table(name = "PUBLIC_CUSTOMER", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PublicCustomerDataDB {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    @Column(updatable = false, name = "id")
    private Integer id;

    @Column(name = "documento")
    private String documento;

    @Column(name = "telefono3")
    private String telefono3;

    @Column(name = "telefono4")
    private String telefono4;

    @Column(name = "direccion2")
    private String direccion2;

    @Column(name = "correo2")
    private String correo2;

    @Column(name = "rango_fecha_inicio")
    private String rango_fecha_inicio;

    @Column(name = "rango_fecha_fin")
    private String rango_fecha_fin;

    @Column(name = "insert_update")
    private Timestamp insert_update;

    public PublicCustomerDataDB(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getTelefono3() {
        return telefono3;
    }

    public void setTelefono3(String telefono3) {
        this.telefono3 = telefono3;
    }

    public String getTelefono4() {
        return telefono4;
    }

    public void setTelefono4(String telefono4) {
        this.telefono4 = telefono4;
    }

    public String getDireccion2() {
        return direccion2;
    }

    public void setDireccion2(String direccion2) {
        this.direccion2 = direccion2;
    }

    public String getCorreo2() {
        return correo2;
    }

    public void setCorreo2(String correo2) {
        this.correo2 = correo2;
    }

    public String getRango_fecha_inicio() {
        return rango_fecha_inicio;
    }

    public void setRango_fecha_inicio(String rango_fecha_inicio) {
        this.rango_fecha_inicio = rango_fecha_inicio;
    }

    public String getRango_fecha_fin() {
        return rango_fecha_fin;
    }

    public void setRango_fecha_fin(String rango_fecha_fin) {
        this.rango_fecha_fin = rango_fecha_fin;
    }

    public Timestamp getInsert_update() {
        return insert_update;
    }

    public void setInsert_update(Timestamp insert_update) {
        this.insert_update = insert_update;
    }
}
