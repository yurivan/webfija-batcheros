package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TOA_STATUS", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ToaStatusDB {

    @GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id")
    private String id;

    @Column(name = "message_id")
    private String message_id;

    @Column(name = "peticion")
    private String peticion;

    @Column(name = "status_toa")
    private String status_toa;

    @Column(name = "external_id")
    private String external_id;

    @Column(name = "xa_identificador_st")
    private String xa_identificador_st;

    @Column(name = "eta_start_time")
    private String eta_start_time;

    @Column(name = "astatus")
    private String astatus;

    @Column(name = "appt_number")
    private String appt_number;

    @Column(name = "aid")
    private String aid;

    @Column(name = "XA_NUMBER_WORK_ORDER")
    private String XA_NUMBER_WORK_ORDER;

    @Column(name = "XA_NUMBER_SERVICE_ORDER")
    private String XA_NUMBER_SERVICE_ORDER;

    @Column(name = "xa_creation_date")
    private String xa_creation_date;

    @Column(name = "xa_requirement_number")
    private String xa_requirement_number;

    @Column(name = "delivery_window_start")
    private String delivery_window_start;

    @Column(name = "delivery_window_end")
    private String delivery_window_end;

    @Column(name = "date")
    private String date;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "time_slot")
    private String time_slot;

    @Column(name = "end_time")
    private String end_time;

    @Column(name = "start_time")
    private String start_time;

    @Column(name = "time_of_assignment")
    private String time_of_assignment;

    @Column(name = "time_of_booking")
    private String time_of_booking;

    @Column(name = "duration")
    private String duration;

    @Column(name = "eta_end_time")
    private String eta_end_time;

    @Column(name = "pname")
    private String pname;

    @Column(name = "ETA")
    private String ETA;

    @Column(name = "A_COMMENT_TECHNICIAN")
    private String A_COMMENT_TECHNICIAN;

    @Column(name = "A_OBSERVATION")
    private String A_OBSERVATION;

    @Column(name = "XA_SOURCE_SYSTEM")
    private String XA_SOURCE_SYSTEM;

    @Column(name = "A_NETWORK_CHANGE_ELEMENT_LIST")
    private String A_NETWORK_CHANGE_ELEMENT_LIST;

    @Column(name = "A_RECEIVE_PERSON_NAME")
    private String A_RECEIVE_PERSON_NAME;

    @Column(name = "A_RECEIVE_PERSON_ID")
    private String A_RECEIVE_PERSON_ID;

    @Column(name = "A_COMPLETE_REASON_INSTALL")
    private String A_COMPLETE_REASON_INSTALL;

    @Column(name = "A_COMPLETE_CAUSA_REP_STB")
    private String A_COMPLETE_CAUSA_REP_STB;

    @Column(name = "A_COMPLETE_REP_STB")
    private String A_COMPLETE_REP_STB;

    @Column(name = "A_COMPLETE_CAUSA_REP_ADSL")
    private String A_COMPLETE_CAUSA_REP_ADSL;

    @Column(name = "A_COMPLETE_REP_ADSL")
    private String A_COMPLETE_REP_ADSL;

    @Column(name = "A_COMPLETE_CAUSA_REP_SAT")
    private String A_COMPLETE_CAUSA_REP_SAT;

    @Column(name = "A_COMPLETE_REP_SAT")
    private String A_COMPLETE_REP_SAT;

    @Column(name = "A_COMPLETE_CAUSA_REP_CBL")
    private String A_COMPLETE_CAUSA_REP_CBL;

    @Column(name = "A_COMPLETE_REP_CBL")
    private String A_COMPLETE_REP_CBL;

    @Column(name = "A_SUSPEND_REASON")
    private String A_SUSPEND_REASON;

    @Column(name = "XA_CANCEL_REASON")
    private String XA_CANCEL_REASON;

    @Column(name = "insert_date")
    private Timestamp insert_date;

    public ToaStatusDB(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getPeticion() {
        return peticion;
    }

    public void setPeticion(String peticion) {
        this.peticion = peticion;
    }

    public String getStatus_toa() {
        return status_toa;
    }

    public void setStatus_toa(String status_toa) {
        this.status_toa = status_toa;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getXa_identificador_st() {
        return xa_identificador_st;
    }

    public void setXa_identificador_st(String xa_identificador_st) {
        this.xa_identificador_st = xa_identificador_st;
    }

    public String getEta_start_time() {
        return eta_start_time;
    }

    public void setEta_start_time(String eta_start_time) {
        this.eta_start_time = eta_start_time;
    }

    public String getAstatus() {
        return astatus;
    }

    public void setAstatus(String astatus) {
        this.astatus = astatus;
    }

    public String getAppt_number() {
        return appt_number;
    }

    public void setAppt_number(String appt_number) {
        this.appt_number = appt_number;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getXA_NUMBER_WORK_ORDER() {
        return XA_NUMBER_WORK_ORDER;
    }

    public void setXA_NUMBER_WORK_ORDER(String XA_NUMBER_WORK_ORDER) {
        this.XA_NUMBER_WORK_ORDER = XA_NUMBER_WORK_ORDER;
    }

    public String getXA_NUMBER_SERVICE_ORDER() {
        return XA_NUMBER_SERVICE_ORDER;
    }

    public void setXA_NUMBER_SERVICE_ORDER(String XA_NUMBER_SERVICE_ORDER) {
        this.XA_NUMBER_SERVICE_ORDER = XA_NUMBER_SERVICE_ORDER;
    }

    public String getXa_creation_date() {
        return xa_creation_date;
    }

    public void setXa_creation_date(String xa_creation_date) {
        this.xa_creation_date = xa_creation_date;
    }

    public String getXa_requirement_number() {
        return xa_requirement_number;
    }

    public void setXa_requirement_number(String xa_requirement_number) {
        this.xa_requirement_number = xa_requirement_number;
    }

    public String getDelivery_window_start() {
        return delivery_window_start;
    }

    public void setDelivery_window_start(String delivery_window_start) {
        this.delivery_window_start = delivery_window_start;
    }

    public String getDelivery_window_end() {
        return delivery_window_end;
    }

    public void setDelivery_window_end(String delivery_window_end) {
        this.delivery_window_end = delivery_window_end;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTime_slot() {
        return time_slot;
    }

    public void setTime_slot(String time_slot) {
        this.time_slot = time_slot;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getTime_of_assignment() {
        return time_of_assignment;
    }

    public void setTime_of_assignment(String time_of_assignment) {
        this.time_of_assignment = time_of_assignment;
    }

    public String getTime_of_booking() {
        return time_of_booking;
    }

    public void setTime_of_booking(String time_of_booking) {
        this.time_of_booking = time_of_booking;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getEta_end_time() {
        return eta_end_time;
    }

    public void setEta_end_time(String eta_end_time) {
        this.eta_end_time = eta_end_time;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getETA() {
        return ETA;
    }

    public void setETA(String ETA) {
        this.ETA = ETA;
    }

    public String getA_COMMENT_TECHNICIAN() {
        return A_COMMENT_TECHNICIAN;
    }

    public void setA_COMMENT_TECHNICIAN(String a_COMMENT_TECHNICIAN) {
        A_COMMENT_TECHNICIAN = a_COMMENT_TECHNICIAN;
    }

    public String getA_OBSERVATION() {
        return A_OBSERVATION;
    }

    public void setA_OBSERVATION(String a_OBSERVATION) {
        A_OBSERVATION = a_OBSERVATION;
    }

    public String getXA_SOURCE_SYSTEM() {
        return XA_SOURCE_SYSTEM;
    }

    public void setXA_SOURCE_SYSTEM(String XA_SOURCE_SYSTEM) {
        this.XA_SOURCE_SYSTEM = XA_SOURCE_SYSTEM;
    }

    public String getA_NETWORK_CHANGE_ELEMENT_LIST() {
        return A_NETWORK_CHANGE_ELEMENT_LIST;
    }

    public void setA_NETWORK_CHANGE_ELEMENT_LIST(String a_NETWORK_CHANGE_ELEMENT_LIST) {
        A_NETWORK_CHANGE_ELEMENT_LIST = a_NETWORK_CHANGE_ELEMENT_LIST;
    }

    public String getA_RECEIVE_PERSON_NAME() {
        return A_RECEIVE_PERSON_NAME;
    }

    public void setA_RECEIVE_PERSON_NAME(String a_RECEIVE_PERSON_NAME) {
        A_RECEIVE_PERSON_NAME = a_RECEIVE_PERSON_NAME;
    }

    public String getA_RECEIVE_PERSON_ID() {
        return A_RECEIVE_PERSON_ID;
    }

    public void setA_RECEIVE_PERSON_ID(String a_RECEIVE_PERSON_ID) {
        A_RECEIVE_PERSON_ID = a_RECEIVE_PERSON_ID;
    }

    public String getA_COMPLETE_REASON_INSTALL() {
        return A_COMPLETE_REASON_INSTALL;
    }

    public void setA_COMPLETE_REASON_INSTALL(String a_COMPLETE_REASON_INSTALL) {
        A_COMPLETE_REASON_INSTALL = a_COMPLETE_REASON_INSTALL;
    }

    public String getA_COMPLETE_CAUSA_REP_STB() {
        return A_COMPLETE_CAUSA_REP_STB;
    }

    public void setA_COMPLETE_CAUSA_REP_STB(String a_COMPLETE_CAUSA_REP_STB) {
        A_COMPLETE_CAUSA_REP_STB = a_COMPLETE_CAUSA_REP_STB;
    }

    public String getA_COMPLETE_REP_STB() {
        return A_COMPLETE_REP_STB;
    }

    public void setA_COMPLETE_REP_STB(String a_COMPLETE_REP_STB) {
        A_COMPLETE_REP_STB = a_COMPLETE_REP_STB;
    }

    public String getA_COMPLETE_CAUSA_REP_ADSL() {
        return A_COMPLETE_CAUSA_REP_ADSL;
    }

    public void setA_COMPLETE_CAUSA_REP_ADSL(String a_COMPLETE_CAUSA_REP_ADSL) {
        A_COMPLETE_CAUSA_REP_ADSL = a_COMPLETE_CAUSA_REP_ADSL;
    }

    public String getA_COMPLETE_REP_ADSL() {
        return A_COMPLETE_REP_ADSL;
    }

    public void setA_COMPLETE_REP_ADSL(String a_COMPLETE_REP_ADSL) {
        A_COMPLETE_REP_ADSL = a_COMPLETE_REP_ADSL;
    }

    public String getA_COMPLETE_CAUSA_REP_SAT() {
        return A_COMPLETE_CAUSA_REP_SAT;
    }

    public void setA_COMPLETE_CAUSA_REP_SAT(String a_COMPLETE_CAUSA_REP_SAT) {
        A_COMPLETE_CAUSA_REP_SAT = a_COMPLETE_CAUSA_REP_SAT;
    }

    public String getA_COMPLETE_REP_SAT() {
        return A_COMPLETE_REP_SAT;
    }

    public void setA_COMPLETE_REP_SAT(String a_COMPLETE_REP_SAT) {
        A_COMPLETE_REP_SAT = a_COMPLETE_REP_SAT;
    }

    public String getA_COMPLETE_CAUSA_REP_CBL() {
        return A_COMPLETE_CAUSA_REP_CBL;
    }

    public void setA_COMPLETE_CAUSA_REP_CBL(String a_COMPLETE_CAUSA_REP_CBL) {
        A_COMPLETE_CAUSA_REP_CBL = a_COMPLETE_CAUSA_REP_CBL;
    }

    public String getA_COMPLETE_REP_CBL() {
        return A_COMPLETE_REP_CBL;
    }

    public void setA_COMPLETE_REP_CBL(String a_COMPLETE_REP_CBL) {
        A_COMPLETE_REP_CBL = a_COMPLETE_REP_CBL;
    }

    public String getA_SUSPEND_REASON() {
        return A_SUSPEND_REASON;
    }

    public void setA_SUSPEND_REASON(String a_SUSPEND_REASON) {
        A_SUSPEND_REASON = a_SUSPEND_REASON;
    }

    public String getXA_CANCEL_REASON() {
        return XA_CANCEL_REASON;
    }

    public void setXA_CANCEL_REASON(String XA_CANCEL_REASON) {
        this.XA_CANCEL_REASON = XA_CANCEL_REASON;
    }

    public Timestamp getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(Timestamp insert_date) {
        this.insert_date = insert_date;
    }
}
