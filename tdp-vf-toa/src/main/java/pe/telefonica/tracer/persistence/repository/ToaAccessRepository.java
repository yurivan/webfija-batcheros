package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ToaAccessDB;

@Transactional()
public interface ToaAccessRepository extends JpaRepository<ToaAccessDB, Long> {

    @Query(value = "SELECT ta.id, ta.username, ta.password, ta.status FROM ibmx_a07e6d02edaf552.toa_access ta WHERE ta.username = :username AND ta.password = :password AND ta.status = 1", nativeQuery = true)
    public ToaAccessDB getToaAccess(@Param("username") final String username, @Param("password") final String password);

}

