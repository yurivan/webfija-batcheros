package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;


@Entity
@Table(name = "STATUS_TOA", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class StatusToaDB {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    @Column(updatable = false, name = "id")
    private Integer id;

    @Column(name = "id_visor")
    private String id_visor;

    @Column(name = "accion")
    private String accion;

    @Column(name = "requerimiento")
    private String requerimiento;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    @Column(name = "orden_trabajo")
    private String orden_trabajo;

    @Column(name = "fecha_envio")
    private Date fecha_envio;

    @Column(name = "hora_envio")
    private Time hora_envio;

    @Column(name = "fecha_envio_toa")
    private Date fecha_envio_toa;

    @Column(name = "hora_envio_toa")
    private Time hora_envio_toa;

    @Column(name = "fecha_agenda")
    private Date fecha_agenda;

    @Column(name = "franja_horaria")
    private String franja_horaria;

    @Column(name = "fecha_inicio_sla")
    private Date fecha_inicio_sla;

    @Column(name = "fecha_fin_sla")
    private Date fecha_fin_sla;

    @Column(name = "fecha_inicio")
    private Date fecha_inicio;

    @Column(name = "fecha_cierre")
    private Date fecha_cierre;

    @Column(name = "motivo_accion")
    private String motivo_accion;

    @Column(name = "celular_cliente")
    private String celular_cliente;

    @Column(name = "mensaje_cliente")
    private String mensaje_cliente;

    @Column(name = "insert_update")
    private Timestamp insert_update;

    public StatusToaDB(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getId_visor() {
        return id_visor;
    }

    public void setId_visor(String id_visor) {
        this.id_visor = id_visor;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getRequerimiento() {
        return requerimiento;
    }

    public void setRequerimiento(String requerimiento) {
        this.requerimiento = requerimiento;
    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public String getOrden_trabajo() {
        return orden_trabajo;
    }

    public void setOrden_trabajo(String orden_trabajo) {
        this.orden_trabajo = orden_trabajo;
    }

    public Date getFecha_envio() {
        return fecha_envio;
    }

    public void setFecha_envio(Date fecha_envio) {
        this.fecha_envio = fecha_envio;
    }

    public Time getHora_envio() {
        return hora_envio;
    }

    public void setHora_envio(Time hora_envio) {
        this.hora_envio = hora_envio;
    }

    public Date getFecha_envio_toa() {
        return fecha_envio_toa;
    }

    public void setFecha_envio_toa(Date fecha_envio_toa) {
        this.fecha_envio_toa = fecha_envio_toa;
    }

    public Time getHora_envio_toa() {
        return hora_envio_toa;
    }

    public void setHora_envio_toa(Time hora_envio_toa) {
        this.hora_envio_toa = hora_envio_toa;
    }

    public Date getFecha_agenda() {
        return fecha_agenda;
    }

    public void setFecha_agenda(Date fecha_agenda) {
        this.fecha_agenda = fecha_agenda;
    }

    public String getFranja_horaria() {
        return franja_horaria;
    }

    public void setFranja_horaria(String franja_horaria) {
        this.franja_horaria = franja_horaria;
    }

    public Date getFecha_inicio_sla() {
        return fecha_inicio_sla;
    }

    public void setFecha_inicio_sla(Date fecha_inicio_sla) {
        this.fecha_inicio_sla = fecha_inicio_sla;
    }

    public Date getFecha_fin_sla() {
        return fecha_fin_sla;
    }

    public void setFecha_fin_sla(Date fecha_fin_sla) {
        this.fecha_fin_sla = fecha_fin_sla;
    }

    public Date getFecha_inicio() {
        return fecha_inicio;
    }

    public void setFecha_inicio(Date fecha_inicio) {
        this.fecha_inicio = fecha_inicio;
    }

    public Date getFecha_cierre() {
        return fecha_cierre;
    }

    public void setFecha_cierre(Date fecha_cierre) {
        this.fecha_cierre = fecha_cierre;
    }

    public String getMotivo_accion() {
        return motivo_accion;
    }

    public void setMotivo_accion(String motivo_accion) {
        this.motivo_accion = motivo_accion;
    }

    public String getCelular_cliente() {
        return celular_cliente;
    }

    public void setCelular_cliente(String celular_cliente) {
        this.celular_cliente = celular_cliente;
    }

    public String getMensaje_cliente() {
        return mensaje_cliente;
    }

    public void setMensaje_cliente(String mensaje_cliente) {
        this.mensaje_cliente = mensaje_cliente;
    }

    public Timestamp getInsert_update() {
        return insert_update;
    }

    public void setInsert_update(Timestamp insert_update) {
        this.insert_update = insert_update;
    }
}
