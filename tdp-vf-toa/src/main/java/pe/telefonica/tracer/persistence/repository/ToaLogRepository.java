package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ToaLogDB;

@Transactional()
public interface ToaLogRepository extends JpaRepository<ToaLogDB, Long> {

}

