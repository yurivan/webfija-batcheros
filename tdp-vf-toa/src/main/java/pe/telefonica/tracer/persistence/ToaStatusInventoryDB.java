package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TOA_STATUS_INVENTORY", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ToaStatusInventoryDB {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    @Column(updatable = false, name = "id")
    private Integer id;

    @Column(name = "toa_status_id")
    private Integer toa_status_id;

    @Column(name = "type")
    private String type;

    @Column(name = "invtype")
    private String invtype;

    @Column(name = "invsn")
    private String invsn;

    @Column(name = "quantity")
    private String quantity;

    @Column(name = "xi_brand")
    private String xi_brand;

    @Column(name = "xi_model")
    private String xi_model;

    @Column(name = "xi_component")
    private String xi_component;

    @Column(name = "xi_mac_address")
    private String xi_mac_address;

    @Column(name = "i_num_activacion")
    private String i_num_activacion;

    public ToaStatusInventoryDB(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getToa_status_id() {
        return toa_status_id;
    }

    public void setToa_status_id(Integer toa_status_id) {
        this.toa_status_id = toa_status_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInvtype() {
        return invtype;
    }

    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }

    public String getInvsn() {
        return invsn;
    }

    public void setInvsn(String invsn) {
        this.invsn = invsn;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getXi_brand() {
        return xi_brand;
    }

    public void setXi_brand(String xi_brand) {
        this.xi_brand = xi_brand;
    }

    public String getXi_model() {
        return xi_model;
    }

    public void setXi_model(String xi_model) {
        this.xi_model = xi_model;
    }

    public String getXi_component() {
        return xi_component;
    }

    public void setXi_component(String xi_component) {
        this.xi_component = xi_component;
    }

    public String getXi_mac_address() {
        return xi_mac_address;
    }

    public void setXi_mac_address(String xi_mac_address) {
        this.xi_mac_address = xi_mac_address;
    }

    public String getI_num_activacion() {
        return i_num_activacion;
    }

    public void setI_num_activacion(String i_num_activacion) {
        this.i_num_activacion = i_num_activacion;
    }
}
