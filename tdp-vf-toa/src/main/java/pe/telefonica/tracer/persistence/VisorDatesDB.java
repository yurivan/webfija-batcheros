package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TDP_VISOR", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class VisorDatesDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id_visor")
    private String id;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    @Column(name = "fecha_solicitado", updatable = true)
    private Timestamp fecha_solicitado;

    @Column(name = "fecha_registrado", updatable = true)
    private Timestamp fecha_registrado;

    @Column(name = "fecha_equipo_tecnico", updatable = true)
    private Timestamp fecha_equipo_tecnico;

    @Column(name = "fecha_instalado", updatable = true)
    private Timestamp fecha_instalado;


    @Column(name = "flg_solicitado_email", updatable = true)
    private Integer flg_solicitado_email;

    @Column(name = "flg_registrado_email", updatable = true)
    private Integer flg_registrado_email;

    @Column(name = "flg_equipo_tecnico_email", updatable = true)
    private Integer flg_equipo_tecnico_email;

    @Column(name = "flg_instalado_email", updatable = true)
    private Integer flg_instalado_email;



    public VisorDatesDB(){

    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public Timestamp getFecha_solicitado() {
        return fecha_solicitado;
    }

    public void setFecha_solicitado(Timestamp fecha_solicitado) {
        this.fecha_solicitado = fecha_solicitado;
    }

    public Timestamp getFecha_registrado() {
        return fecha_registrado;
    }

    public void setFecha_registrado(Timestamp fecha_registrado) {
        this.fecha_registrado = fecha_registrado;
    }

    public Timestamp getFecha_equipo_tecnico() {
        return fecha_equipo_tecnico;
    }

    public void setFecha_equipo_tecnico(Timestamp fecha_equipo_tecnico) {
        this.fecha_equipo_tecnico = fecha_equipo_tecnico;
    }

    public Timestamp getFecha_instalado() {
        return fecha_instalado;
    }

    public void setFecha_instalado(Timestamp fecha_instalado) {
        this.fecha_instalado = fecha_instalado;
    }


    public Integer getFlg_solicitado_email() {
        return flg_solicitado_email;
    }

    public void setFlg_solicitado_email(Integer flg_solicitado_email) {
        this.flg_solicitado_email = flg_solicitado_email;
    }

    public Integer getFlg_registrado_email() {
        return flg_registrado_email;
    }

    public void setFlg_registrado_email(Integer flg_registrado_email) {
        this.flg_registrado_email = flg_registrado_email;
    }

    public Integer getFlg_equipo_tecnico_email() {
        return flg_equipo_tecnico_email;
    }

    public void setFlg_equipo_tecnico_email(Integer flg_equipo_tecnico_email) {
        this.flg_equipo_tecnico_email = flg_equipo_tecnico_email;
    }

    public Integer getFlg_instalado_email() {
        return flg_instalado_email;
    }

    public void setFlg_instalado_email(Integer flg_instalado_email) {
        this.flg_instalado_email = flg_instalado_email;
    }

}
