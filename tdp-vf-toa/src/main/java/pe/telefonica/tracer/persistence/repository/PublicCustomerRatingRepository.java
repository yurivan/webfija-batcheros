package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.PublicCustomerDataDB;
import pe.telefonica.tracer.persistence.PublicCustomerRatingDB;

@Transactional()
public interface PublicCustomerRatingRepository extends JpaRepository<PublicCustomerRatingDB, Long> {

    @Query(value = "SELECT pc.id, pc.documento, pc.respuesta1, pc.respuesta2, pc.respuesta3, pc.respuesta4, pc.valoracion, pc.insert_update" +
            " FROM ibmx_a07e6d02edaf552.public_customer pc WHERE pc.documento = :documento", nativeQuery = true)
    public PublicCustomerRatingDB getPublicCustomerRating(@Param("documento") final String documento);

}

