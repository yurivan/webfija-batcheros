package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ParametersDB;
import pe.telefonica.tracer.persistence.StatusToaDB;

import java.util.List;

@Transactional()
public interface StatusToaRepository extends JpaRepository<StatusToaDB, Long> {

    @Query(value = "SELECT id,id_visor,accion,requerimiento,codigo_pedido,orden_trabajo,fecha_envio,hora_envio,fecha_envio_toa,hora_envio_toa,fecha_agenda," +
            " franja_horaria,fecha_inicio_sla,fecha_fin_sla,fecha_inicio,fecha_cierre,motivo_accion,celular_cliente,mensaje_cliente,insert_update" +
            " FROM ibmx_a07e6d02edaf552.status_toa" +
            " WHERE codigo_pedido = :codigoPedido", nativeQuery = true)
    public StatusToaDB getStatusToa(@Param("codigoPedido") final String codigoPedido);

}
