package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.VisorByDocDB;

import java.util.List;

@Transactional()
public interface VisorByDocRepository extends JpaRepository<VisorByDocDB, Long> {
    @Query(value = "SELECT v.id_visor, v.codigo_pedido FROM ibmx_a07e6d02edaf552.tdp_visor v WHERE v.dni = :document" +
            " AND v.codigo_pedido is not null AND v.codigo_pedido <> ''", nativeQuery = true)
    public List<VisorByDocDB> getVisorByDoc(@Param("document") final String document);

    /*@Query(value = "SELECT id_visor, fecha_grabacion, cliente, telefono, direccion, departamento, dni, " +
            " CASE WHEN lower(operacion_comercial) = lower('SVAS') THEN " +
            " (" +
            "    SELECT " +
            "    (" +
            "    (CASE WHEN od.DECOSSD IS NOT NULL AND od.DECOSSD <> 0 THEN ('DECOS SMART: ' || od.DECOSSD || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.DECOSHD IS NOT NULL AND od.DECOSHD <> 0 THEN ('DECOS HD: ' || od.DECOSHD || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.DECOSDVR IS NOT NULL AND od.DECOSDVR <> 0 THEN ('DECOS DVR: ' || od.DECOSDVR || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.TVBLOCK IS NOT NULL AND od.TVBLOCK <> '' THEN ('BLOQUE TV: ' || od.TVBLOCK || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.SVAINTERNET IS NOT NULL AND od.SVAINTERNET <> '' THEN ('SVA INTERNET: ' || od.SVAINTERNET || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.SVALINE IS NOT NULL AND od.SVALINE <> '' THEN ('SVA LINEA: ' || od.SVALINE || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.BLOCKTV IS NOT NULL AND od.BLOCKTV <> '' THEN ('BLOQUE PRODUCTO: ' || od.BLOCKTV || '|') ELSE '' END)" +
            "    )" +
            "    as nombre_producto" +
            "    FROM ibmx_a07e6d02edaf552.order_detail od" +
            "    WHERE od.orderid = :idVisor" +
            " )" +
            " ELSE nombre_producto END nombre_producto, " +
            " CASE WHEN lower(operacion_comercial) = lower('SVAS') THEN nombre_producto ELSE '' END nombre_producto2, " +
            " estado_solicitud, motivo_estado," +
            " operacion_comercial, distrito, sub_producto, tipo_documento, provincia, fecha_de_llamada, nombre_producto_fuente, fecha_solicitado," +
            " fecha_registrado, estado_anterior, fecha_equipo_tecnico, fecha_instalado" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor" +
            " WHERE id_visor = :idVisor" +
            " AND lower(operacion_comercial) in" +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'),lower('SVAS'))", nativeQuery = true)
    public List<VisorByRequestCodeDB> getVisorById(@Param("idVisor") final String id);*/
}

