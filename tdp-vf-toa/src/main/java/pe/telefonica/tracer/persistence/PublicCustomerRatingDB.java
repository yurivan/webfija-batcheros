package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "PUBLIC_CUSTOMER", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PublicCustomerRatingDB {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    @Column(updatable = false, name = "id")
    private Integer id;

    @Column(name = "documento")
    private String documento;

    @Column(name = "respuesta1")
    private String respuesta1;

    @Column(name = "respuesta2")
    private String respuesta2;

    @Column(name = "respuesta3")
    private String respuesta3;

    @Column(name = "respuesta4")
    private String respuesta4;

    @Column(name = "valoracion")
    private Integer valoracion;

    @Column(name = "insert_update")
    private Timestamp insert_update;

    public PublicCustomerRatingDB(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getRespuesta1() {
        return respuesta1;
    }

    public void setRespuesta1(String respuesta1) {
        this.respuesta1 = respuesta1;
    }

    public String getRespuesta2() {
        return respuesta2;
    }

    public void setRespuesta2(String respuesta2) {
        this.respuesta2 = respuesta2;
    }

    public String getRespuesta3() {
        return respuesta3;
    }

    public void setRespuesta3(String respuesta3) {
        this.respuesta3 = respuesta3;
    }

    public String getRespuesta4() {
        return respuesta4;
    }

    public void setRespuesta4(String respuesta4) {
        this.respuesta4 = respuesta4;
    }

    public Integer getValoracion() {
        return valoracion;
    }

    public void setValoracion(Integer valoracion) {
        this.valoracion = valoracion;
    }

    public Timestamp getInsert_update() {
        return insert_update;
    }

    public void setInsert_update(Timestamp insert_update) {
        this.insert_update = insert_update;
    }
}
