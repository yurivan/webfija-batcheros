package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TRACEABILITY_ACCESS_LOG", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TraceabilityAccessLogDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private String id;

    @Column(name = "token_id")
    private Integer tokenId;

    @Column(name = "message")
    private String message;

    @Column(name = "date")
    private Timestamp date;

    public TraceabilityAccessLogDB(){

    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setTokenId(Integer tokenId){
        this.tokenId = tokenId;
    }

    public Integer getTokenId(){
        return tokenId;
    }

    public void setMessage(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }

    public void setDate(Timestamp date){
        this.date = date;
    }

    public Timestamp getDate(){
        return date;
    }

}
