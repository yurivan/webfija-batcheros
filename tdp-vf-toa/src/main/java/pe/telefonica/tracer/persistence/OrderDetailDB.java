package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "ORDER_DETAIL", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderDetailDB {

    @Id
    @Column(updatable = false, name = "id")
    private String id;

    /*@OneToOne()
    @JoinColumn(name="orderid", referencedColumnName = "id")
    private OrderDB order;*/

    @Column(name = "decossd")
    private Integer decosSD;

    @Column(name = "decoshd")
    private Integer decosHD;

    @Column(name = "decosdvr")
    private Integer decosDVR;

    @Column(name = "tvblock")
    private String tvBlock;

    @Column(name = "svainternet")
    private String svaInternet;

    @Column(name = "svaline")
    private String svaLine;

    @Column(name = "blocktv")
    private String blockTv;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /*public OrderDB getOrder() {
        return order;
    }

    public void setOrder(OrderDB order) {
        this.order = order;
    }*/

    public Integer getDecosSD() {
        return decosSD;
    }

    public void setDecosSD(Integer decosSD) {
        this.decosSD = decosSD;
    }

    public Integer getDecosHD() {
        return decosHD;
    }

    public void setDecosHD(Integer decosHD) {
        this.decosHD = decosHD;
    }

    public Integer getDecosDVR() {
        return decosDVR;
    }

    public void setDecosDVR(Integer decosDVR) {
        this.decosDVR = decosDVR;
    }

    public String getTvBlock() {
        return tvBlock;
    }

    public void setTvBlock(String tvBlock) {
        this.tvBlock = tvBlock;
    }

    public String getSvaInternet() {
        return svaInternet;
    }

    public void setSvaInternet(String svaInternet) {
        this.svaInternet = svaInternet;
    }

    public String getSvaLine() {
        return svaLine;
    }

    public void setSvaLine(String svaLine) {
        this.svaLine = svaLine;
    }

    public String getBlockTv() {
        return blockTv;
    }

    public void setBlockTv(String blockTv) {
        this.blockTv = blockTv;
    }
}
