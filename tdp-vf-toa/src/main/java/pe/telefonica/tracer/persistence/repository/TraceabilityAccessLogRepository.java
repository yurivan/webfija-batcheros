package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.TraceabilityAccessDB;
import pe.telefonica.tracer.persistence.TraceabilityAccessLogDB;

@Transactional()
public interface TraceabilityAccessLogRepository extends JpaRepository<TraceabilityAccessLogDB, Long> {



}

