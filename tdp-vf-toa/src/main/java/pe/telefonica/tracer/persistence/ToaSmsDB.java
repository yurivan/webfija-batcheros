package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TOA_SMS", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ToaSmsDB {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    @Column(updatable = false, name = "id")
    private Integer id;

    @Column(name = "message_id")
    private String message_id;

    @Column(name = "peticion")
    private String peticion;

    @Column(name = "status_toa")
    private String status_toa;

    @Column(name = "external_id")
    private String external_id;

    @Column(name = "xa_identificador_st")
    private String xa_identificador_st;

    @Column(name = "appt_number")
    private String appt_number;

    @Column(name = "aid")
    private String aid;

    @Column(name = "XA_NUMBER_WORK_ORDER")
    private String XA_NUMBER_WORK_ORDER;

    @Column(name = "XA_NUMBER_SERVICE_ORDER")
    private String XA_NUMBER_SERVICE_ORDER;

    @Column(name = "number")
    private String number;

    @Column(name = "number1")
    private String number1;

    @Column(name = "number2")
    private String number2;

    @Column(name = "number3")
    private String number3;

    @Column(name = "text")
    private String text;

    @Column(name = "insert_date")
    private Timestamp insert_date;

    public ToaSmsDB(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getPeticion() {
        return peticion;
    }

    public void setPeticion(String peticion) {
        this.peticion = peticion;
    }

    public String getStatus_toa() {
        return status_toa;
    }

    public void setStatus_toa(String status_toa) {
        this.status_toa = status_toa;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getXa_identificador_st() {
        return xa_identificador_st;
    }

    public void setXa_identificador_st(String xa_identificador_st) {
        this.xa_identificador_st = xa_identificador_st;
    }

    public String getAppt_number() {
        return appt_number;
    }

    public void setAppt_number(String appt_number) {
        this.appt_number = appt_number;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getXA_NUMBER_WORK_ORDER() {
        return XA_NUMBER_WORK_ORDER;
    }

    public void setXA_NUMBER_WORK_ORDER(String XA_NUMBER_WORK_ORDER) {
        this.XA_NUMBER_WORK_ORDER = XA_NUMBER_WORK_ORDER;
    }

    public String getXA_NUMBER_SERVICE_ORDER() {
        return XA_NUMBER_SERVICE_ORDER;
    }

    public void setXA_NUMBER_SERVICE_ORDER(String XA_NUMBER_SERVICE_ORDER) {
        this.XA_NUMBER_SERVICE_ORDER = XA_NUMBER_SERVICE_ORDER;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber1() {
        return number1;
    }

    public void setNumber1(String number1) {
        this.number1 = number1;
    }

    public String getNumber2() {
        return number2;
    }

    public void setNumber2(String number2) {
        this.number2 = number2;
    }

    public String getNumber3() {
        return number3;
    }

    public void setNumber3(String number3) {
        this.number3 = number3;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(Timestamp insert_date) {
        this.insert_date = insert_date;
    }
}
