package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ToaStatusDB;
import pe.telefonica.tracer.persistence.TraceabilityAccessLogDB;

@Transactional()
public interface ToaStatusRepository extends JpaRepository<ToaStatusDB, Long> {



}

