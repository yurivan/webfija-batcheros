package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ToaSmsDB;
import pe.telefonica.tracer.persistence.ToaStatusDB;

@Transactional()
public interface ToaSmsRepository extends JpaRepository<ToaSmsDB, Long> {



}

