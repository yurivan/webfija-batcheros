package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ParametersDB;
import pe.telefonica.tracer.persistence.VisorDB;

import java.util.List;

@Transactional()
public interface ParametersRepository extends JpaRepository<ParametersDB, Long> {

    @Query(value = "SELECT p.id, p.element, p.strvalue FROM ibmx_a07e6d02edaf552.parameters p" +
            " WHERE p.domain = 'CONFIG' AND p.category = 'PARAMETER' AND (p.element like '%emblue%' OR p.element like '%traceability%')", nativeQuery = true)
    public List<ParametersDB> getParameters();

}
