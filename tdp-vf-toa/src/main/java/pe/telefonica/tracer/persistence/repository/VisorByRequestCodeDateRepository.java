package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.VisorByRequestCodeDB;
import pe.telefonica.tracer.persistence.VisorByRequestCodeDateDB;

import java.util.List;

@Transactional()
public interface VisorByRequestCodeDateRepository extends JpaRepository<VisorByRequestCodeDateDB, Long> {

    @Query(value = "SELECT id_visor, codigo_pedido, fecha_solicitado, fecha_registrado, fecha_equipo_tecnico, fecha_instalado" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " WHERE codigo_pedido = :requestCode", nativeQuery = true)
    public List<VisorByRequestCodeDateDB> getVisorByRequestCodeDate(@Param("requestCode") final String requestCode);
}

