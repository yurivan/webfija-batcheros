package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.VisorDB;

import java.sql.Timestamp;
import java.util.List;

@Transactional(/*propagation = Propagation.MANDATORY*/)
public interface VisorRepository extends JpaRepository<VisorDB, Long> {
    /*@Query("select u from VisorDB u where u.callDate BETWEEN :start AND :end")
    public List<VisorDB> getRangeRows(@Param("start") final Timestamp start , @Param("end") final Timestamp end);*/

    /*@Query("SELECT new VisorDB((c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) as customer, c.email, v.idVisor, v.productName, v.requestStatus, v.commercialOperation," +
            " CASE WHEN v.phone is null OR v.phone = '' THEN c.phone1 ELSE v.phone END as phone, c.phone2)" +
            " FROM VisorDB v" +
            " INNER JOIN v.order o " +
            " INNER JOIN o.customer c " +
            " WHERE v.requestedDate is not null AND v.mailRequested is null AND v.requestStatus = 'PENDIENTE'" +
            " AND c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null AND c.email is not null" +
            " AND c.firstname <> '' AND c.lastname1 <> '' AND c.lastname2 <> '' AND c.email <> ''" +
            " AND lower(v.commercialOperation) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            " ORDER BY v.idVisor DESC")
    public List<VisorDB> getSolicitadoAM(Pageable pageable);*/

    /*@Query("SELECT new VisorDB((c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) as customer, v.idVisor, v.productName, v.requestStatus, v.commercialOperation" +
            " ), new CustomerDB(c.email, CASE WHEN v.phone is null OR v.phone = '' THEN c.phone1 ELSE v.phone END as phone, c.phone2)" +
            " FROM VisorDB v" +
            " INNER JOIN v.order o " +
            " INNER JOIN o.customer c " +
            " WHERE v.requestedDate is not null AND v.mailRequested is null AND v.requestStatus = 'PENDIENTE'" +
            " AND c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null AND c.email is not null" +
            " AND c.firstname <> '' AND c.lastname1 <> '' AND c.lastname2 <> '' AND c.email <> ''" +
            " AND lower(v.commercialOperation) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            " ORDER BY v.idVisor DESC")
    public List<Object[]> getSolicitadoAM(Pageable pageable);*/

    @Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END cliente, c.email, v.nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_solicitado is not null AND v.flg_solicitado_email is null AND v.estado_solicitud = 'PENDIENTE'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR" +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null" +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC", nativeQuery = true)
    public List<Object[]> getSolicitadoAM();

    @Query(value = "SELECT  v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email," +
            " (" +
            " (CASE WHEN od.DECOSSD IS NOT NULL AND od.DECOSSD <> 0 THEN ('DECOS SMART: ' || od.DECOSSD || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.DECOSHD IS NOT NULL AND od.DECOSHD <> 0 THEN ('DECOS HD: ' || od.DECOSHD || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.DECOSDVR IS NOT NULL AND od.DECOSDVR <> 0 THEN ('DECOS DVR: ' || od.DECOSDVR || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.TVBLOCK IS NOT NULL AND od.TVBLOCK <> '' THEN ('BLOQUE TV: ' || od.TVBLOCK || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.SVAINTERNET IS NOT NULL AND od.SVAINTERNET <> '' THEN ('SVA INTERNET: ' || od.SVAINTERNET || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.SVALINE IS NOT NULL AND od.SVALINE <> '' THEN ('SVA LINEA: ' || od.SVALINE || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.BLOCKTV IS NOT NULL AND od.BLOCKTV <> '' THEN ('BLOQUE PRODUCTO: ' || od.BLOCKTV || '|') ELSE '' END)" +
            " ) as nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2,"+
            " v.estado_solicitud, v.operacion_comercial" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.order_detail od ON od.orderid = o.id" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_solicitado is not null AND v.flg_solicitado_email is null AND v.estado_solicitud = 'PENDIENTE'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR" +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null" +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND (od.DECOSSD IS NOT NULL OR od.DECOSHD IS NOT NULL OR od.DECOSDVR IS NOT NULL OR od.TVBLOCK IS NOT NULL" +
            " OR od.SVAINTERNET IS NOT NULL OR od.SVALINE IS NOT NULL OR od.BLOCKTV IS NOT NULL)" +
            " AND lower(v.operacion_comercial) in (lower('SVAS')) " +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC", nativeQuery = true)
    public List<Object[]> getSolicitadoS();

    @Query(value = "(" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, v.nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC" +
            " )" +
            " UNION ALL " +
            " (" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, v.nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND v.id_visor NOT IN (SELECT id FROM ibmx_a07e6d02edaf552.order)" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            //" AND v.dni = '70540625' " +
            " ORDER BY v.id_visor DESC" +
            " )", nativeQuery = true)
    public List<Object[]> getRegistradoAM();

    @Query(value = "(" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email,  " +
            " (" +
            " (CASE WHEN DECOSSD IS NOT NULL AND DECOSSD <> 0 THEN 'DECOS SMART: ' || DECOSSD || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN DECOSHD IS NOT NULL AND DECOSHD <> 0 THEN 'DECOS HD: ' || DECOSHD || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN DECOSDVR IS NOT NULL AND DECOSDVR <> 0 THEN 'DECOS DVR: ' || DECOSDVR || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN TVBLOCK IS NOT NULL AND TVBLOCK <> '' THEN 'BLOQUE TV: ' || TVBLOCK || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN SVAINTERNET IS NOT NULL AND SVAINTERNET <> '' THEN 'SVA INTERNET: ' || SVAINTERNET || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN SVALINE IS NOT NULL AND SVALINE <> '' THEN 'SVA LINEA: ' || SVALINE || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN BLOCKTV IS NOT NULL AND BLOCKTV <> '' THEN 'BLOQUE PRODUCTO: ' || BLOCKTV || '|' ELSE '' END)" +
            " ) as nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            //" v.fecha_solicitado, v.fecha_registrado, v.fecha_equipo_tecnico, v.fecha_instalado, " +
            //" v.flg_solicitado_email, v.flg_registrado_email, v.flg_equipo_tecnico_email, v.flg_instalado_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.order_detail od ON od.orderid = o.id" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND (od.DECOSSD IS NOT NULL OR od.DECOSHD IS NOT NULL OR od.DECOSDVR IS NOT NULL OR od.TVBLOCK IS NOT NULL" +
            " OR od.SVAINTERNET IS NOT NULL OR od.SVALINE IS NOT NULL OR od.BLOCKTV IS NOT NULL)" +
            " AND lower(v.operacion_comercial) in (lower('SVAS')) " +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC" +
            " )" +
            " UNION ALL" +
            " (" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, V.sub_producto nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            //" v.fecha_solicitado, v.fecha_registrado, v.fecha_equipo_tecnico, v.fecha_instalado, " +
            //" v.flg_solicitado_email, v.flg_registrado_email, v.flg_equipo_tecnico_email, v.flg_instalado_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND v.id_visor NOT IN (SELECT id FROM ibmx_a07e6d02edaf552.order)" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in (lower('SVAS')) " +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC" +
            " )", nativeQuery = true)
    public List<Object[]> getRegistradoS();

    /*@Modifying(clearAutomatically = true)
    @Transactional()
    @Query(value = "UPDATE ibmx_a07e6d02edaf552.tdp_visor SET flg_solicitado_email = 1 WHERE id_visor IN (:ids)", nativeQuery = true)
    public Integer  setMailRequestedAsDone(@Param("ids") final String ids);*/

}
