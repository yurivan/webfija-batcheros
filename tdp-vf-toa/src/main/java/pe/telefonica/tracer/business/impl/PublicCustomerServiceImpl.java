package pe.telefonica.tracer.business.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.PublicCustomerService;
import pe.telefonica.tracer.business.TraceabilityAccessService;
import pe.telefonica.tracer.model.PublicCustomerRequest;
import pe.telefonica.tracer.persistence.PublicCustomerDataDB;
import pe.telefonica.tracer.persistence.PublicCustomerRatingDB;
import pe.telefonica.tracer.persistence.TraceabilityAccessDB;
import pe.telefonica.tracer.persistence.TraceabilityAccessLogDB;
import pe.telefonica.tracer.persistence.repository.PublicCustomerDataRepository;
import pe.telefonica.tracer.persistence.repository.PublicCustomerRatingRepository;
import pe.telefonica.tracer.persistence.repository.TraceabilityAccessLogRepository;
import pe.telefonica.tracer.persistence.repository.TraceabilityAccessRepository;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Created by Felix on 19/06/2017.
 */
@Service
public class PublicCustomerServiceImpl implements PublicCustomerService {

    private static final Logger _log = LoggerFactory.getLogger(PublicCustomerServiceImpl.class);

    @Autowired
    private PublicCustomerDataRepository publicCustomerDataRepository;

    @Autowired
    private PublicCustomerRatingRepository publicCustomerRatingRepository;

    @Override
    public PublicCustomerDataDB getPublicCustomerData(String document) throws Exception{
        try{
            return publicCustomerDataRepository.getPublicCustomerData(document);
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public PublicCustomerDataDB savePublicCustomerData(PublicCustomerDataDB publicCustomerData, PublicCustomerRequest publicCustomerRequest) throws Exception{
        try{
            if(publicCustomerData == null) publicCustomerData = new PublicCustomerDataDB();

            publicCustomerData.setDocumento(publicCustomerRequest.getDocumento());
            publicCustomerData.setTelefono3(publicCustomerRequest.getTelefono3());
            publicCustomerData.setTelefono4(publicCustomerRequest.getTelefono4());
            publicCustomerData.setDireccion2(publicCustomerRequest.getDireccion2());
            publicCustomerData.setCorreo2(publicCustomerRequest.getCorreo2());
            publicCustomerData.setRango_fecha_inicio(publicCustomerRequest.getRangoFechaInicio());
            publicCustomerData.setRango_fecha_fin(publicCustomerRequest.getRangoFechaFin());

            Timestamp timestamp = new Timestamp(new Date().getTime());
            publicCustomerData.setInsert_update(timestamp);
            publicCustomerDataRepository.save(publicCustomerData);
            return publicCustomerData;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public String validateDataFieldsRequest(PublicCustomerRequest request) throws Exception{
        try{
            String field = "";
            if( request.getDocumento() == null || request.getDocumento().trim().equals("") ) return "El campo DOCUMENTO es obligatorio.";
            else if( request.getTelefono3() == null/* || request.getTelefono3().trim().equals("")*/ ) field = "TELEFONO3";
            else if( request.getTelefono4() == null /*|| request.getTelefono4().trim().equals("")*/ ) field = "TELEFONO4";
            else if( request.getDireccion2() == null /*|| request.getDireccion2().trim().equals("")*/ ) field = "DIRECCION2";
            else if( request.getCorreo2() == null /*|| request.getCorreo2().trim().equals("")*/ ) field = "CORREO2";
            else if( request.getRangoFechaInicio() == null /*|| request.getRangoFechaInicio().trim().equals("")*/ ) field = "RANGOFECHAINICIO";
            else if( request.getRangoFechaFin() == null /*|| request.getRangoFechaFin().trim().equals("")*/ ) field = "RANGOFECHAFIN";

            if(!field.equals("")) return String.format("Se esperaba el campo %s. (Cadena vacía permitida)", field);

            if( request.getDocumento().trim().length() > 20 ) field = "DOCUMENTO";
            else if( request.getTelefono3().trim().length() > 20 ) field = "TELEFONO3";
            else if( request.getTelefono4().trim().length() > 20 ) field = "TELEFONO4";
            else if( request.getDireccion2().trim().length() > 250 ) field = "DIRECCION2";
            else if( request.getCorreo2().trim().length() > 50 ) field = "CORREO2";
            else if( request.getRangoFechaInicio().trim().length() > 15 ) field = "RANGOFECHAINICIO";
            else if( request.getRangoFechaFin().trim().length() > 15 ) field = "RANGOFECHAFIN";

            if(!field.equals("")) return String.format("El campo %s ha excedido el límite de caracteres.", field);

            return "";
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    /************************************************************/

    @Override
    public PublicCustomerRatingDB getPublicCustomerRating(String document) throws Exception{
        try{
            return publicCustomerRatingRepository.getPublicCustomerRating(document);
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public PublicCustomerRatingDB savePublicCustomerRating(PublicCustomerRatingDB publicCustomerRating, PublicCustomerRequest publicCustomerRequest) throws Exception{
        try{
            if(publicCustomerRating == null) publicCustomerRating = new PublicCustomerRatingDB();

            publicCustomerRating.setDocumento(publicCustomerRequest.getDocumento());
            publicCustomerRating.setRespuesta1(publicCustomerRequest.getRespuesta1());
            publicCustomerRating.setRespuesta2(publicCustomerRequest.getRespuesta2());
            publicCustomerRating.setRespuesta3(publicCustomerRequest.getRespuesta3());
            publicCustomerRating.setRespuesta4(publicCustomerRequest.getRespuesta4());
            if(publicCustomerRequest.getValoracion() != null && !publicCustomerRequest.getValoracion().equals(""))
                publicCustomerRating.setValoracion(Integer.parseInt(publicCustomerRequest.getValoracion()));

            Timestamp timestamp = new Timestamp(new Date().getTime());
            publicCustomerRating.setInsert_update(timestamp);
            publicCustomerRatingRepository.save(publicCustomerRating);
            return publicCustomerRating;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public String validateRatingFieldsRequest(PublicCustomerRequest request) throws Exception{
        try{
            String field = "";
            if( request.getDocumento() == null || request.getDocumento().trim().equals("") ) return "El campo DOCUMENTO es obligatorio.";
            else if( request.getRespuesta1() == null/* || request.getRespuesta1().trim().equals("")*/ ) field = "RESPUESTA1";
            else if( request.getRespuesta2() == null /*|| request.getRespuesta2().trim().equals("")*/ ) field = "RESPUESTA2";
            else if( request.getRespuesta3() == null /*|| request.getRespuesta3().trim().equals("")*/ ) field = "RESPUESTA3";
            else if( request.getRespuesta4() == null /*|| request.getRespuesta4().trim().equals("")*/ ) field = "RESPUESTA4";
            else if( request.getValoracion() == null /*|| request.getValoracion().trim().equals("")*/ ) field = "VALORACION";

            if(!field.equals("")) return String.format("Se esperaba el campo %s. (Cadena vacía permitida)", field);

            if( request.getDocumento().trim().length() > 20 ) field = "DOCUMENTO";
            else if( request.getRespuesta1().trim().length() > 250 ) field = "RESPUESTA1";
            else if( request.getRespuesta2().trim().length() > 250 ) field = "RESPUESTA2";
            else if( request.getRespuesta3().trim().length() > 250 ) field = "RESPUESTA3";
            else if( request.getRespuesta4().trim().length() > 250 ) field = "RESPUESTA4";

            if(!field.equals("")) return String.format("El campo %s ha excedido el límite de caracteres.", field);

            if(request.getValoracion() != null && !request.getValoracion().equals("") && !isInteger(request.getValoracion())) return "El campo VALORACION debe ser un entero.";

            return "";
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    private boolean isInteger(String value){
        try{
            Integer valueInt = Integer.parseInt(value);
            return true;
        }catch(Exception ex){
            return false;
        }
    }

}
