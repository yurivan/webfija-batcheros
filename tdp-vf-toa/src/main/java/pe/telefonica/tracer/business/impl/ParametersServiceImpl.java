package pe.telefonica.tracer.business.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.ParametersService;
import pe.telefonica.tracer.business.UserService;
import pe.telefonica.tracer.configuration.Constants;
import pe.telefonica.tracer.configuration.MenuValidator;
import pe.telefonica.tracer.model.AppUser;
import pe.telefonica.tracer.persistence.ParametersDB;
import pe.telefonica.tracer.persistence.repository.ParametersRepository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.*;

/**
 * Created by Felix on 19/06/2017.
 */
@Component
@Service
public class ParametersServiceImpl implements ParametersService {

    private static final Logger logger = LoggerFactory.getLogger(ParametersServiceImpl.class);

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ParametersRepository repository;

    @PersistenceContext
    private EntityManager em;

    @Value("${tdp.mail.emblue.authenticate.uri}")
    private String emailAutenticateUri;

    @Override
    public List<ParametersDB> getParameters() {
        try{
            return repository.getParameters();
        }catch(Exception ex){
            return new ArrayList<ParametersDB>();
        }
    }


    public Map<String, Object> loadProperties() {
        Map<String, Object> props = new HashMap<>();
        try{
            List<ParametersDB> items = getParameters();
            for(ParametersDB item : items){
                String key = item.getElement();
                String value = item.getStrvalue();
                props.put(key, value);
                logger.info(String.format("Parametro %s: %s", key, value));
            }
        } catch (Exception e) {
            logger.error("waa :/", e);
        }
        return props;
    }

    public List<ParametersDB> getParameters2() {
        try{

            Query q = em.createNativeQuery("SELECT p.id, p.element, p.strvalue FROM ibmx_a07e6d02edaf552.parameters p" +
                    " WHERE p.domain = 'CONFIG' AND p.category = 'PARAMETER' AND (p.element like '%emblue%' OR p.element like '%traceability%')");
            List<Object[]> objectList = q.getResultList();

            List<ParametersDB> items = new ArrayList<ParametersDB>();

            for (Object[] obj : objectList) {
                ParametersDB parameters = new ParametersDB();
                parameters.setId(obj[0].toString());
                parameters.setElement(obj[1].toString());
                parameters.setStrvalue(obj[2].toString());
                items.add(parameters);
            }

            return items;
        }catch(Exception ex){
            return new ArrayList<ParametersDB>();
        }
    }

    /*@PostConstruct
    public void fex(){
        String fex = "ola k ase";

        Map<String, Object> props = loadProperties();
        System.out.print(props);

        Properties propps = new Properties();
        propps.putAll(props);

        PropertySourcesPlaceholderConfigurer sc = (PropertySourcesPlaceholderConfigurer)context.getBean(PropertySourcesPlaceholderConfigurer.class);

        sc.setProperties(propps);

        //PropertySourcesPlaceholderConfigurer fex2 = MenuValidator.properties();

        //fex2.setProperties(propps);

        logger.info(fex);

        Constants cons = new Constants();

        String miRuta = emailAutenticateUri;
        logger.info(miRuta);

    }*/
}
