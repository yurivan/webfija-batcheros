package pe.telefonica.tracer.business;

import pe.telefonica.tracer.model.Menu;


/**
 * Created by Javier on 19/06/2017.
 */
public interface EmailService {

    public boolean sendMailExpress();
}
