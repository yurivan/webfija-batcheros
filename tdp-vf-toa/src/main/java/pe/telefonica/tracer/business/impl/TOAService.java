package pe.telefonica.tracer.business.impl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.MarshalException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.MimeHeaders;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import pe.telefonica.tracer.model.toa.*;
import org.springframework.stereotype.Component;
import pe.telefonica.tracer.persistence.*;
import pe.telefonica.tracer.persistence.repository.*;
//import pe.telefonica.tracer.persistence.repository.VisorByRequestCodeDateRepository;
//import toatech.agent.Message;
//import toatech.agent.SendMessage;
import pe.telefonica.tracer.model.toa.generated.Message;
import pe.telefonica.tracer.model.toa.generated.SendMessage;


@Component
public class TOAService {

    @Autowired
    private TOAServiceAsync toaServiceAsync;

    @Autowired
    ToaAccessRepository toaAccessRepository;

    @Value("${traceability.toa.authenabled}")
    Boolean authRequired;

    /*@Autowired
    private ToaStatusRepository toaStatusRepository;

    @Autowired
    private ToaSmsRepository toaSmsRepository;

    @Autowired
    private ToaStatusInventoryRepository toaStatusInventoryRepository;

    @Autowired

    private VisorByRequestCodeRepository visorRepository;

    @Autowired
    private VisorDatesRepository visorDatesRepository;*/

    //@Autowired
    //private VisorByRequestCodeDateRepository visorByRequestCodeDateRepository;

    @Async
    public CompletableFuture<String> saveLogRequest(SendMessage request){
        String XML = "";
        try{
            XML = marshallToString(request,true);
        }catch(Exception ex){
            ex.printStackTrace();
        }

        XML = XML.replace("&lt;","<");
        XML = XML.replace("&gt;",">");

        toaServiceAsync.saveLog("REQUEST", XML);

        return CompletableFuture.completedFuture("");
    }

    private static String marshallToString(Object object,
                                           boolean includeXMLHeader) throws MarshalException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(
                100);
        try {
            JAXBContext context = JAXBContext.newInstance(object.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FRAGMENT, !includeXMLHeader);
            marshaller.marshal(object, byteArrayOutputStream);
            return new String(byteArrayOutputStream.toByteArray());
        } catch (Throwable e) {
            return "Unable to marshal object to stream: " + e;
        } finally {
            IOUtils.closeQuietly(byteArrayOutputStream);
        }
    }

    //@Async
    public CompletableFuture<SendMessageResponse> processMessages(List<Message> messageList, MessageContext messageContext){
        SendMessageResponse response = new SendMessageResponse();
        List<MessageResponse> messageResponseList = new ArrayList<>();
        try{
            String messageText = "";

            String auth = authRequired ? getAuthenticated(messageContext) : "";

            if(!"".equals(auth)){
                toaServiceAsync.saveLog("NO AUTH", auth);
                return CompletableFuture.completedFuture(response);
            }

            for(Message message : messageList){
                messageText = processMessage(message);

                MessageResponse messageResponse = new MessageResponse();
                messageResponse.setMessageId(message.getMessageId());
                messageResponse.setStatus("sent");
                messageResponse.setDescription(messageText);

                messageResponseList.add(messageResponse);
            }
            response.setMessageResponse(messageResponseList);
        }catch(Exception ex){
            ex.printStackTrace();
            response.setMessageResponse(messageResponseList);
        }
        return CompletableFuture.completedFuture(response);
    }

    public String getAuthenticated(MessageContext messageContext){
        String response = "AUTH_ERROR";
        try{
            if(messageContext != null && messageContext.getRequest() != null){
                SaajSoapMessage soapRequest = (SaajSoapMessage) messageContext.getRequest();
                if(soapRequest != null && soapRequest.getSaajMessage() != null && soapRequest.getSaajMessage().getMimeHeaders() != null){
                    MimeHeaders headers = soapRequest.getSaajMessage().getMimeHeaders();

                    String user="", password="";
                    String[] userArray = headers.getHeader("Auth-User");
                    String[] passwordArray = headers.getHeader("Auth-Password");
                    if(userArray != null && passwordArray != null){
                        if(userArray.length > 0) user = userArray[0];
                        if(passwordArray.length > 0) password = passwordArray[0];

                        if(!"".equals(user) && !"".equals(password)){
                            ToaAccessDB toaAccess = toaAccessRepository.getToaAccess(user, password);
                            if (toaAccess != null) response = "";
                            else response = user + "-" + password;
                        }else response = user + "-" + password;
                    }else response = "NOTHING SEND";

                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return "AUTH_EXCEPTON";
        }
        return response;
    }

    /*public SendMessageResponse processMessages(List<Message> messageList){
        SendMessageResponse response = new SendMessageResponse();
        List<MessageResponse> messageResponseList = new ArrayList<>();
        try{
            String messageText = "";
            for(Message message : messageList){
                messageText = processMessage(message);

                MessageResponse messageResponse = new MessageResponse();
                messageResponse.setMessageId(message.getMessageId());
                messageResponse.setStatus("sent");
                messageResponse.setDescription(messageText);

                messageResponseList.add(messageResponse);
            }
            response.setMessageResponse(messageResponseList);
        }catch(Exception ex){
            ex.printStackTrace();
            response.setMessageResponse(messageResponseList);
        }
        return response;
    }*/

    /*public boolean existsRequest(String requestCode){
        boolean result = false;
        try{
            if(requestCode == null || "".equals(requestCode.trim())) return result;
            Object[] request = visorRepository.getVisorByRequestCodeSimple(requestCode);
            if(request != null && request.length > 0){
                result = true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }*/

    /*public boolean setTechnicAssignDate(String codigo_pedido){
        try{

            List<VisorByRequestCodeDateDB> visorList = visorByRequestCodeDateRepository.getVisorByRequestCodeDate(codigo_pedido);
            if(visorList == null || visorList.size() == 0) return true;

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateStr = sdf2.format(new Date());
            Date date = sdf2.parse(dateStr);
            Timestamp timestamp =  new Timestamp(date.getTime());

            for(VisorByRequestCodeDateDB obj : visorList){
                obj.setFecha_equipo_tecnico(timestamp);
                visorByRequestCodeDateRepository.save(obj);
            }

            return true;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    public boolean setInstaledDate(String codigo_pedido){
        try{

            List<VisorByRequestCodeDateDB> visorList = visorByRequestCodeDateRepository.getVisorByRequestCodeDate(codigo_pedido);
            if(visorList == null || visorList.size() == 0) return true;

            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateStr = sdf2.format(new Date());
            Date date = sdf2.parse(dateStr);
            Timestamp timestamp =  new Timestamp(date.getTime());

            for(VisorByRequestCodeDateDB obj : visorList){
                obj.setFecha_instalado(timestamp);
                visorByRequestCodeDateRepository.save(obj);
            }

            return true;
        }catch(Exception ex){
            ex.printStackTrace();
            return false;
        }
    }*/

    public String processMessage(Message message){
        String response = "";
        String messageId = (message.getMessageId() != null) ? message.getMessageId() : "XXXXXX";
        try{
            JAXBContext jc;
            Unmarshaller u;
            StringReader reader;
            String processMessage;
            boolean casted;

            String subject = message.getSubject() != null ? message.getSubject().trim() : "NULL";

            String bodyMessage = message.getBody() != null ? message.getBody().trim() : "NULL";

            if(bodyMessage.startsWith("<![CDATA[")){
                bodyMessage = bodyMessage.replace("<![CDATA[","");
                bodyMessage = bodyMessage.replace("]]>","");
            }

            switch(subject){
                case "IN_TOA":
                case "<IN_TOA>":

                    jc = JAXBContext.newInstance(IN_TOA.class);
                    u = jc.createUnmarshaller();

                    IN_TOA in_toa = new IN_TOA();

                    try{
                        reader = new StringReader(bodyMessage);
                        in_toa = (IN_TOA) u.unmarshal(reader);
                        casted = true;
                    }catch(Exception ex){
                        casted = false;
                    }

                    if(casted){
                        toaServiceAsync.processIN_TOA(in_toa, messageId, message.getBody());
                    }else{
                        String body = message.getBody() != null ? message.getBody() : "BODY NULL";
                        response = "Error al castear el estado: " + subject + " \ncon Body: " + body;
                    }

                    break;
                case "WO_INIT":
                case "<WO_INIT>":

                    jc = JAXBContext.newInstance(WO_INIT.class);
                    u = jc.createUnmarshaller();

                    WO_INIT wo_init = new WO_INIT();

                    try{
                        reader = new StringReader(bodyMessage);
                        wo_init = (WO_INIT) u.unmarshal(reader);
                        casted = true;
                    }catch(Exception ex){
                        casted = false;
                    }

                    if(casted){
                        toaServiceAsync.processWO_INIT(wo_init, messageId, message.getBody());
                    }else{
                        String body = message.getBody() != null ? message.getBody() : "BODY NULL";
                        response = "Error al castear el estado: " + subject + " \ncon Body: " + body;
                    }

                    break;
                case "NOT_LEG":
                case "<NOT_LEG>":

                    jc = JAXBContext.newInstance(NOT_LEG.class);
                    u = jc.createUnmarshaller();

                    NOT_LEG not_leg = new NOT_LEG();

                    try{
                        reader = new StringReader(bodyMessage);
                        not_leg = (NOT_LEG) u.unmarshal(reader);
                        casted = true;
                    }catch(Exception ex){
                        casted = false;
                    }

                    if(casted){
                        toaServiceAsync.processNOT_LEG(not_leg, messageId, message.getBody());
                    }else{
                        String body = message.getBody() != null ? message.getBody() : "BODY NULL";
                        response = "Error al castear el estado: " + subject + " \ncon Body: " + body;
                    }

                    break;
                case "WO_PRE_COMPLETED":
                case "<WO_PRE_COMPLETED>":

                    jc = JAXBContext.newInstance(WO_PRE_COMPLETED.class);
                    u = jc.createUnmarshaller();

                    WO_PRE_COMPLETED wo_pre_completed = new WO_PRE_COMPLETED();

                    try{
                        reader = new StringReader(bodyMessage);
                        wo_pre_completed = (WO_PRE_COMPLETED) u.unmarshal(reader);
                        casted = true;
                    }catch(Exception ex){
                        casted = false;
                    }

                    if(casted){
                        toaServiceAsync.processWO_PRE_COMPLETED(wo_pre_completed, messageId, message.getBody());
                    }else{
                        String body = message.getBody() != null ? message.getBody() : "BODY NULL";
                        response = "Error al castear el estado: " + subject + " \ncon Body: " + body;
                    }

                    if(!"".equals(response)){
                        jc = JAXBContext.newInstance(WO_COMPLETED.class);
                        u = jc.createUnmarshaller();

                        WO_COMPLETED wo_completed = new WO_COMPLETED();

                        try{
                            reader = new StringReader(bodyMessage);
                            wo_completed = (WO_COMPLETED) u.unmarshal(reader);
                            casted = true;
                        }catch(Exception ex){
                            casted = false;
                        }

                        if(casted){
                            toaServiceAsync.processWO_PRE_COMPLETED_L(wo_completed, messageId, message.getBody());
                            response = "";
                        }else{
                            String body = message.getBody() != null ? message.getBody() : "BODY NULL";
                            response = "Error al castear el estado: " + subject + " \ncon Body: " + body;
                        }
                    }

                    break;
                case "TOA_OUTBOUND_COMPLETED":
                case "<TOA_OUTBOUND_COMPLETED>":

                    jc = JAXBContext.newInstance(WO_COMPLETED.class);
                    u = jc.createUnmarshaller();

                    WO_COMPLETED wo_completed = new WO_COMPLETED();

                    try{
                        reader = new StringReader(bodyMessage);
                        wo_completed = (WO_COMPLETED) u.unmarshal(reader);
                        casted = true;
                    }catch(Exception ex){
                        casted = false;
                    }

                    if(casted){
                        toaServiceAsync.processWO_COMPLETED(wo_completed, messageId, message.getBody());
                    }else{
                        String body = message.getBody() != null ? message.getBody() : "BODY NULL";
                        response = "Error al castear el estado: " + subject + " \ncon Body: " + body;
                    }

                    break;
                case "TOA_OUTBOUND_SUSPEND":
                case "<TOA_OUTBOUND_SUSPEND>":

                    jc = JAXBContext.newInstance(WO_SUSPEND.class);
                    u = jc.createUnmarshaller();

                    WO_SUSPEND wo_suspend = new WO_SUSPEND();

                    try{
                        reader = new StringReader(bodyMessage);
                        wo_suspend = (WO_SUSPEND) u.unmarshal(reader);
                        casted = true;
                    }catch(Exception ex){
                        casted = false;
                    }

                    if(casted){
                        toaServiceAsync.processWO_SUSPEND(wo_suspend, messageId, message.getBody());
                    }else{
                        String body = message.getBody() != null ? message.getBody() : "BODY NULL";
                        response = "Error al castear el estado: " + subject + " \ncon Body: " + body;
                    }

                    break;
                case "WO_CANCEL":
                case "<WO_CANCEL>":

                    jc = JAXBContext.newInstance(WO_CANCEL.class);
                    u = jc.createUnmarshaller();

                    WO_CANCEL wo_cancel = new WO_CANCEL();

                    try{
                        reader = new StringReader(bodyMessage);
                        wo_cancel = (WO_CANCEL) u.unmarshal(reader);
                        casted = true;
                    }catch(Exception ex){
                        casted = false;
                    }

                    if(casted){
                        toaServiceAsync.processWO_CANCEL(wo_cancel, messageId, message.getBody());
                    }else{
                        String body = message.getBody() != null ? message.getBody() : "BODY NULL";
                        response = "Error al castear el estado: " + subject + " \ncon Body: " + body;
                    }

                    break;
                case "INT_SMS":
                case "<INT_SMS>":

                    jc = JAXBContext.newInstance(INT_SMS.class);
                    u = jc.createUnmarshaller();

                    INT_SMS int_sms = new INT_SMS();

                    try{
                        reader = new StringReader(bodyMessage);
                        int_sms = (INT_SMS) u.unmarshal(reader);
                        casted = true;
                    }catch(Exception ex){
                        casted = false;
                    }

                    if(casted){
                        toaServiceAsync.processINT_SMS(int_sms, messageId, message.getBody());
                    }else{
                        String body = message.getBody() != null ? message.getBody() : "BODY NULL";
                        response = "Error al castear el estado: " + subject + " \ncon Body: " + body;
                    }

                    break;
                default:
                    response = "El estado enviado no está siendo considerado: " + subject;
            }

            if(!"".equals(response)){
                toaServiceAsync.saveLog(messageId,response);
                return "Message_Id "+ messageId + " = " + response;
            }

            //return "Message_Id "+ messageId + " = " + response;
            return "Message_Id "+ messageId + " => Procesado.";
        }catch(Exception ex){
            ex.printStackTrace();
            return "Message_Id "+ messageId + " = " + ex.getMessage();
        }
    }

    /*public String sendSMS(){
        String result = "";
        try{

        }catch(Exception ex){
            ex.printStackTrace();
        }
        return result;
    }*/

    /*
    //@Async
    public CompletableFuture<String> processWO_INIT(WO_INIT wo_init, String messageId){
        String response = "";
        try{
            if(wo_init.getXa_peticion() == null || wo_init.getXa_peticion().trim().equals("")){
                if(wo_init.getAppt_number() != null && !wo_init.getAppt_number().trim().equals("")){
                    wo_init.setXa_peticion(wo_init.getAppt_number());
                }
            }

            boolean existsRequest = existsRequest(wo_init.getXa_peticion());
            if(!existsRequest){
                return CompletableFuture.completedFuture("Message_Id "+ messageId + " = " + " El código de pedido no existe.");
            }

            String processMessage = saveWO_INIT(wo_init);
            if("".equals(processMessage)) response = "Estado registrado correctamente.";
            else response = "Error al registrar el estado: " + processMessage;
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveWO_INIT(WO_INIT wo_init){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("WO_INIT");
            toaStatus.setPeticion(wo_init.getXa_peticion());

            toaStatus.setExternal_id(wo_init.getExternal_id());
            toaStatus.setXa_identificador_st(wo_init.getXa_identificador_st());
            toaStatus.setEta_start_time(wo_init.getEta_start_time());
            toaStatus.setAstatus(wo_init.getAstatus());
            toaStatus.setAppt_number(wo_init.getAppt_number());
            toaStatus.setAid(wo_init.getAid());
            toaStatus.setXA_NUMBER_WORK_ORDER(wo_init.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(wo_init.getXA_NUMBER_SERVICE_ORDER());

            String A_COMMENT_TECHNICIAN = "";

            if(wo_init.getActivity_Properties() != null && wo_init.getActivity_Properties().getProperty() != null){
                for(property property : wo_init.getActivity_Properties().getProperty()){
                    if("A_COMMENT_TECHNICIAN".equals(property.getLabel())){ A_COMMENT_TECHNICIAN = property.getValue(); continue; }
                }
            }

            toaStatus.setA_COMMENT_TECHNICIAN(A_COMMENT_TECHNICIAN);

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);
            result = "";

        }catch(Exception ex){
            ex.printStackTrace();
            result = ex.getMessage();
        }

        return result;
    }

    public String processIN_TOA(IN_TOA in_toa){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("IN_TOA");
            toaStatus.setPeticion(in_toa.getXa_peticion());
            toaStatus.setExternal_id(in_toa.getExternal_id());
            toaStatus.setXa_creation_date(in_toa.getXa_creation_date());
            toaStatus.setXa_identificador_st(in_toa.getXa_identificador_st());
            toaStatus.setXa_requirement_number(in_toa.getXa_requirement_number());
            toaStatus.setAppt_number(in_toa.getAppt_number());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(in_toa.getXA_NUMBER_SERVICE_ORDER());

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);
            result = "";

        }catch(Exception ex){
            ex.printStackTrace();
            result = ex.getMessage();
        }

        return result;
    }

    public String processNOT_LEG(NOT_LEG not_leg){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("NOT_LEG");
            toaStatus.setPeticion(not_leg.getXa_peticion());

            toaStatus.setXa_identificador_st(not_leg.getXa_identificador_st());
            toaStatus.setDelivery_window_start(not_leg.getDelivery_window_start());
            toaStatus.setDelivery_window_end(not_leg.getDelivery_window_end());
            toaStatus.setDate(not_leg.getDate());
            toaStatus.setUsuario(not_leg.getUsuario());
            toaStatus.setTime_slot(not_leg.getTime_slot());
            toaStatus.setStart_time(not_leg.getStart_time());
            toaStatus.setEnd_time(not_leg.getEnd_time());
            toaStatus.setAstatus(not_leg.getAstatus());
            toaStatus.setTime_of_assignment(not_leg.getTime_of_assignment());
            toaStatus.setTime_of_booking(not_leg.getTime_of_booking());
            toaStatus.setDuration(not_leg.getDuration());
            toaStatus.setExternal_id(not_leg.getExternal_id());
            toaStatus.setAppt_number(not_leg.getAppt_number());
            toaStatus.setAid(not_leg.getAid());
            toaStatus.setXA_NUMBER_WORK_ORDER(not_leg.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(not_leg.getXA_NUMBER_SERVICE_ORDER());

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);
            result = "";

        }catch(Exception ex){
            ex.printStackTrace();
            result = ex.getMessage();
        }

        return result;
    }

    public String processWO_PRE_COMPLETED(WO_PRE_COMPLETED wo_pre_completed){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("WO_PRE_COMPLETED");
            toaStatus.setPeticion(wo_pre_completed.getXa_peticion());

            toaStatus.setExternal_id(wo_pre_completed.getExternal_id());
            toaStatus.setXa_identificador_st(wo_pre_completed.getXa_identificador_st());
            toaStatus.setEta_start_time(wo_pre_completed.getEta_start_time());
            toaStatus.setEta_end_time(wo_pre_completed.getEta_end_time());
            toaStatus.setAstatus(wo_pre_completed.getAstatus());
            toaStatus.setAppt_number(wo_pre_completed.getAppt_number());
            toaStatus.setXA_NUMBER_WORK_ORDER(wo_pre_completed.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(wo_pre_completed.getXA_NUMBER_SERVICE_ORDER());

            String A_OBSERVATION = "";
            String XA_SOURCE_SYSTEM = "";
            String A_NETWORK_CHANGE_ELEMENT_LIST = "";
            String A_RECEIVE_PERSON_NAME = "";
            String A_RECEIVE_PERSON_ID = "";

            String A_COMPLETE_REASON_INSTALL = "";
            String A_COMPLETE_CAUSA_REP_STB = "";
            String A_COMPLETE_REP_STB = "";
            String A_COMPLETE_CAUSA_REP_ADSL = "";
            String A_COMPLETE_REP_ADSL = "";
            String A_COMPLETE_CAUSA_REP_SAT = "";
            String A_COMPLETE_REP_SAT = "";
            String A_COMPLETE_CAUSA_REP_CBL = "";
            String A_COMPLETE_REP_CBL = "";

            if(wo_pre_completed.getActivity_properties() != null && wo_pre_completed.getActivity_properties().getProperty() != null){
                for(property property : wo_pre_completed.getActivity_properties().getProperty()){
                    if("A_OBSERVATION".equals(property.getLabel())){ A_OBSERVATION = property.getValue(); continue; }
                    if("XA_SOURCE_SYSTEM".equals(property.getLabel())){ XA_SOURCE_SYSTEM = property.getValue(); continue; }
                    if("A_NETWORK_CHANGE_ELEMENT_LIST".equals(property.getLabel())){ A_NETWORK_CHANGE_ELEMENT_LIST = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_NAME".equals(property.getLabel())){ A_RECEIVE_PERSON_NAME = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_ID".equals(property.getLabel())){ A_RECEIVE_PERSON_ID = property.getValue(); continue; }

                    if("A_COMPLETE_REASON_INSTALL".equals(property.getLabel())){ A_COMPLETE_REASON_INSTALL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_STB".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_REP_STB".equals(property.getLabel())){ A_COMPLETE_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_SAT".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_REP_SAT".equals(property.getLabel())){ A_COMPLETE_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_CBL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_CBL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_CBL".equals(property.getLabel())){ A_COMPLETE_REP_CBL = property.getValue(); continue; }
                }
            }

            toaStatus.setA_OBSERVATION(A_OBSERVATION);
            toaStatus.setXA_SOURCE_SYSTEM(XA_SOURCE_SYSTEM);
            toaStatus.setA_NETWORK_CHANGE_ELEMENT_LIST(A_NETWORK_CHANGE_ELEMENT_LIST);
            toaStatus.setA_RECEIVE_PERSON_NAME(A_RECEIVE_PERSON_NAME);
            toaStatus.setA_RECEIVE_PERSON_ID(A_RECEIVE_PERSON_ID);

            toaStatus.setA_COMPLETE_REASON_INSTALL(A_COMPLETE_REASON_INSTALL);
            toaStatus.setA_COMPLETE_CAUSA_REP_STB(A_COMPLETE_CAUSA_REP_STB);
            toaStatus.setA_COMPLETE_REP_STB(A_COMPLETE_REP_STB);
            toaStatus.setA_COMPLETE_CAUSA_REP_ADSL(A_COMPLETE_CAUSA_REP_ADSL);
            toaStatus.setA_COMPLETE_REP_ADSL(A_COMPLETE_REP_ADSL);
            toaStatus.setA_COMPLETE_CAUSA_REP_SAT(A_COMPLETE_CAUSA_REP_SAT);
            toaStatus.setA_COMPLETE_REP_SAT(A_COMPLETE_REP_SAT);
            toaStatus.setA_COMPLETE_CAUSA_REP_CBL(A_COMPLETE_CAUSA_REP_CBL);
            toaStatus.setA_COMPLETE_REP_CBL(A_COMPLETE_REP_CBL);


            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);
            result = "";

        }catch(Exception ex){
            ex.printStackTrace();
            result = ex.getMessage();
        }

        return result;
    }

    public String processWO_COMPLETED(WO_COMPLETED wo_completed){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("WO_COMPLETED");
            toaStatus.setPeticion(wo_completed.getXa_peticion());

            toaStatus.setExternal_id(wo_completed.getExternal_id());
            toaStatus.setXa_identificador_st(wo_completed.getXa_identificador_st());
            toaStatus.setEta_start_time(wo_completed.getEta_start_time());
            toaStatus.setEta_end_time(wo_completed.getEta_end_time());
            toaStatus.setAstatus(wo_completed.getAstatus());
            toaStatus.setAppt_number(wo_completed.getAppt_number());
            toaStatus.setXA_NUMBER_WORK_ORDER(wo_completed.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(wo_completed.getXA_NUMBER_SERVICE_ORDER());

            String A_OBSERVATION = "";
            String XA_SOURCE_SYSTEM = "";
            String A_NETWORK_CHANGE_ELEMENT_LIST = "";
            String A_RECEIVE_PERSON_NAME = "";
            String A_RECEIVE_PERSON_ID = "";

            String A_COMPLETE_REASON_INSTALL = "";
            String A_COMPLETE_CAUSA_REP_STB = "";
            String A_COMPLETE_REP_STB = "";
            String A_COMPLETE_CAUSA_REP_ADSL = "";
            String A_COMPLETE_REP_ADSL = "";
            String A_COMPLETE_CAUSA_REP_SAT = "";
            String A_COMPLETE_REP_SAT = "";
            String A_COMPLETE_CAUSA_REP_CBL = "";
            String A_COMPLETE_REP_CBL = "";

            if(wo_completed.getActivity_properties() != null && wo_completed.getActivity_properties().getProperty() != null){
                for(property property : wo_completed.getActivity_properties().getProperty()){
                    if("A_OBSERVATION".equals(property.getLabel())){ A_OBSERVATION = property.getValue(); continue; }
                    if("XA_SOURCE_SYSTEM".equals(property.getLabel())){ XA_SOURCE_SYSTEM = property.getValue(); continue; }
                    if("A_NETWORK_CHANGE_ELEMENT_LIST".equals(property.getLabel())){ A_NETWORK_CHANGE_ELEMENT_LIST = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_NAME".equals(property.getLabel())){ A_RECEIVE_PERSON_NAME = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_ID".equals(property.getLabel())){ A_RECEIVE_PERSON_ID = property.getValue(); continue; }

                    if("A_COMPLETE_REASON_INSTALL".equals(property.getLabel())){ A_COMPLETE_REASON_INSTALL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_STB".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_REP_STB".equals(property.getLabel())){ A_COMPLETE_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_SAT".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_REP_SAT".equals(property.getLabel())){ A_COMPLETE_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_CBL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_CBL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_CBL".equals(property.getLabel())){ A_COMPLETE_REP_CBL = property.getValue(); continue; }
                }
            }

            toaStatus.setA_OBSERVATION(A_OBSERVATION);
            toaStatus.setXA_SOURCE_SYSTEM(XA_SOURCE_SYSTEM);
            toaStatus.setA_NETWORK_CHANGE_ELEMENT_LIST(A_NETWORK_CHANGE_ELEMENT_LIST);
            toaStatus.setA_RECEIVE_PERSON_NAME(A_RECEIVE_PERSON_NAME);
            toaStatus.setA_RECEIVE_PERSON_ID(A_RECEIVE_PERSON_ID);

            toaStatus.setA_COMPLETE_REASON_INSTALL(A_COMPLETE_REASON_INSTALL);
            toaStatus.setA_COMPLETE_CAUSA_REP_STB(A_COMPLETE_CAUSA_REP_STB);
            toaStatus.setA_COMPLETE_REP_STB(A_COMPLETE_REP_STB);
            toaStatus.setA_COMPLETE_CAUSA_REP_ADSL(A_COMPLETE_CAUSA_REP_ADSL);
            toaStatus.setA_COMPLETE_REP_ADSL(A_COMPLETE_REP_ADSL);
            toaStatus.setA_COMPLETE_CAUSA_REP_SAT(A_COMPLETE_CAUSA_REP_SAT);
            toaStatus.setA_COMPLETE_REP_SAT(A_COMPLETE_REP_SAT);
            toaStatus.setA_COMPLETE_CAUSA_REP_CBL(A_COMPLETE_CAUSA_REP_CBL);
            toaStatus.setA_COMPLETE_REP_CBL(A_COMPLETE_REP_CBL);


            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatus = toaStatusRepository.save(toaStatus);
            result = "";

            saveInventory(wo_completed.getInstalled_inventory(), wo_completed.getDeinstalled_inventory(),
                    wo_completed.getCustomer_inventory(), wo_completed.getResource_inventory(), toaStatus.getId());

        }catch(Exception ex){
            ex.printStackTrace();
            result = ex.getMessage();
        }

        return result;
    }

    public String processWO_SUSPEND(WO_SUSPEND wo_suspend){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            activity_properties activity_properties = wo_suspend.getActivity_Properties();

            if(activity_properties != null){
                toaStatus.setStatus_toa("WO_SUSPEND");
                toaStatus.setPeticion(activity_properties.getXa_peticion());

                toaStatus.setExternal_id(activity_properties.getExternal_id());
                toaStatus.setXa_identificador_st(activity_properties.getXa_identificador_st());
                toaStatus.setETA(activity_properties.getETA());
                toaStatus.setEta_end_time(activity_properties.getEta_end_time());
                toaStatus.setAstatus(activity_properties.getAstatus());
                toaStatus.setAppt_number(activity_properties.getAppt_number());
                toaStatus.setXA_NUMBER_WORK_ORDER(activity_properties.getXA_NUMBER_WORK_ORDER());
                toaStatus.setXA_NUMBER_SERVICE_ORDER(activity_properties.getXA_NUMBER_SERVICE_ORDER());

                String A_OBSERVATION = "";
                String XA_SOURCE_SYSTEM = "";
                String A_NETWORK_CHANGE_ELEMENT_LIST = "";
                String A_SUSPEND_REASON = "";

                if(activity_properties != null && activity_properties.getProperty() != null){
                    for(property property : activity_properties.getProperty()){
                        if("A_OBSERVATION".equals(property.getLabel())){ A_OBSERVATION = property.getValue(); continue; }
                        if("XA_SOURCE_SYSTEM".equals(property.getLabel())){ XA_SOURCE_SYSTEM = property.getValue(); continue; }
                        if("A_NETWORK_CHANGE_ELEMENT_LIST".equals(property.getLabel())){ A_NETWORK_CHANGE_ELEMENT_LIST = property.getValue(); continue; }
                        if("A_SUSPEND_REASON".equals(property.getLabel())){ A_SUSPEND_REASON = property.getValue(); continue; }
                    }
                }

                toaStatus.setA_OBSERVATION(A_OBSERVATION);
                toaStatus.setXA_SOURCE_SYSTEM(XA_SOURCE_SYSTEM);
                toaStatus.setA_NETWORK_CHANGE_ELEMENT_LIST(A_NETWORK_CHANGE_ELEMENT_LIST);
                toaStatus.setA_SUSPEND_REASON(A_SUSPEND_REASON);


                Timestamp timestamp = new Timestamp(new Date().getTime());
                toaStatus.setInsert_date(timestamp);

                toaStatusRepository.save(toaStatus);
                result = "";

                saveInventory(wo_suspend.getInstalled_inventory(), wo_suspend.getDeinstalled_inventory(),
                        wo_suspend.getCustomer_inventory(), wo_suspend.getResource_inventory(), toaStatus.getId());

            }else{
                result = "activity_properties NULL";
            }

        }catch(Exception ex){
            ex.printStackTrace();
            result = ex.getMessage();
        }

        return result;
    }

    public String saveInventory(inventory_installed inventory_installed, inventory_deinstalled inventory_deinstalled,
                                inventory_customer inventory_customer, inventory_resource inventory_resource, String toa_status_id){
        String result = "";
        try{
            if(inventory_installed != null && inventory_installed.getBLOCK() != null && inventory_installed.getBLOCK().getInventory() != null){
                for(inventory inventory : inventory_installed.getBLOCK().getInventory()){
                    if(inventory != null){
                        ToaStatusInventoryDB inventoryDB = new ToaStatusInventoryDB();
                        inventoryDB.setToa_status_id(Integer.parseInt(toa_status_id));
                        inventoryDB.setType("installed_inventory");
                        inventoryDB.setInvtype(inventory.getInvtype());
                        inventoryDB.setInvsn(inventory.getInvsn());
                        inventoryDB.setQuantity(inventory.getQuantity());
                        inventoryDB.setXi_brand(inventory.getXi_brand());
                        inventoryDB.setXi_model(inventory.getXi_model());
                        inventoryDB.setXi_component(inventory.getXi_component());
                        inventoryDB.setXi_mac_address(inventory.getXi_mac_address());
                        inventoryDB.setI_num_activacion(inventory.getI_num_activacion());
                        toaStatusInventoryRepository.save(inventoryDB);
                    }
                }
            }
            if(inventory_deinstalled != null && inventory_deinstalled.getBLOCK() != null && inventory_deinstalled.getBLOCK().getInventory() != null){
                for(inventory inventory : inventory_deinstalled.getBLOCK().getInventory()){
                    if(inventory != null){
                        ToaStatusInventoryDB inventoryDB = new ToaStatusInventoryDB();
                        inventoryDB.setToa_status_id(Integer.parseInt(toa_status_id));
                        inventoryDB.setType("deinstalled_inventory");
                        inventoryDB.setInvtype(inventory.getInvtype());
                        inventoryDB.setInvsn(inventory.getInvsn());
                        inventoryDB.setQuantity(inventory.getQuantity());
                        inventoryDB.setXi_brand(inventory.getXi_brand());
                        inventoryDB.setXi_model(inventory.getXi_model());
                        inventoryDB.setXi_component(inventory.getXi_component());
                        inventoryDB.setXi_mac_address(inventory.getXi_mac_address());
                        inventoryDB.setI_num_activacion(inventory.getI_num_activacion());
                        toaStatusInventoryRepository.save(inventoryDB);
                    }
                }
            }
            if(inventory_customer != null && inventory_customer.getBLOCK() != null && inventory_customer.getBLOCK().getInventory() != null){
                for(inventory inventory : inventory_customer.getBLOCK().getInventory()){
                    if(inventory != null){
                        ToaStatusInventoryDB inventoryDB = new ToaStatusInventoryDB();
                        inventoryDB.setToa_status_id(Integer.parseInt(toa_status_id));
                        inventoryDB.setType("customer_inventory");
                        inventoryDB.setInvtype(inventory.getInvtype());
                        inventoryDB.setInvsn(inventory.getInvsn());
                        inventoryDB.setQuantity(inventory.getQuantity());
                        inventoryDB.setXi_brand(inventory.getXi_brand());
                        inventoryDB.setXi_model(inventory.getXi_model());
                        inventoryDB.setXi_component(inventory.getXi_component());
                        inventoryDB.setXi_mac_address(inventory.getXi_mac_address());
                        inventoryDB.setI_num_activacion(inventory.getI_num_activacion());
                        toaStatusInventoryRepository.save(inventoryDB);
                    }
                }
            }
            if(inventory_resource != null && inventory_resource.getBLOCK() != null && inventory_resource.getBLOCK().getInventory() != null){
                for(inventory inventory : inventory_resource.getBLOCK().getInventory()){
                    if(inventory != null){
                        ToaStatusInventoryDB inventoryDB = new ToaStatusInventoryDB();
                        inventoryDB.setToa_status_id(Integer.parseInt(toa_status_id));
                        inventoryDB.setType("resource_inventory");
                        inventoryDB.setInvtype(inventory.getInvtype());
                        inventoryDB.setInvsn(inventory.getInvsn());
                        inventoryDB.setQuantity(inventory.getQuantity());
                        inventoryDB.setXi_brand(inventory.getXi_brand());
                        inventoryDB.setXi_model(inventory.getXi_model());
                        inventoryDB.setXi_component(inventory.getXi_component());
                        inventoryDB.setXi_mac_address(inventory.getXi_mac_address());
                        inventoryDB.setI_num_activacion(inventory.getI_num_activacion());
                        toaStatusInventoryRepository.save(inventoryDB);
                    }
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;
    }

    public String processWO_CANCEL(WO_CANCEL wo_cancel){
        String result = "";
        try{

            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("WO_CANCEL");
            toaStatus.setPeticion(wo_cancel.getXa_peticion());

            toaStatus.setExternal_id(wo_cancel.getExternal_id());
            toaStatus.setXa_identificador_st(wo_cancel.getXa_identificador_st());
            toaStatus.setEta_start_time(wo_cancel.getEta_start_time());
            toaStatus.setEta_end_time(wo_cancel.getEta_end_time());
            toaStatus.setAstatus(wo_cancel.getAstatus());
            toaStatus.setAppt_number(wo_cancel.getAppt_number());
            toaStatus.setAid(wo_cancel.getAid());
            toaStatus.setXA_NUMBER_WORK_ORDER(wo_cancel.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(wo_cancel.getXA_NUMBER_SERVICE_ORDER());

            String A_COMMENT_TECHNICIAN = "";
            String XA_CANCEL_REASON = "";
            String A_OBSERVATION = "";

            if(wo_cancel.getActivity_Properties() != null && wo_cancel.getActivity_Properties().getProperty() != null){
                for(property property : wo_cancel.getActivity_Properties().getProperty()){
                    if("A_COMMENT_TECHNICIAN".equals(property.getLabel())){ A_COMMENT_TECHNICIAN = property.getValue(); continue; }
                    if("XA_CANCEL_REASON".equals(property.getLabel())){ XA_CANCEL_REASON = property.getValue(); continue; }
                    if("A_OBSERVATION".equals(property.getLabel())){ A_OBSERVATION = property.getValue(); continue; }
                }
            }

            toaStatus.setA_COMMENT_TECHNICIAN(A_COMMENT_TECHNICIAN);
            toaStatus.setXA_CANCEL_REASON(XA_CANCEL_REASON);
            toaStatus.setA_OBSERVATION(A_OBSERVATION);

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);
            result = "";

        }catch(Exception ex){
            ex.printStackTrace();
            result = ex.getMessage();
        }

        return result;
    }

    public String processINT_SMS(INT_SMS int_sms){
        String result = "";
        try{
            ToaSmsDB toaSms = new ToaSmsDB();

            toaSms.setStatus_toa("INT_SMS");
            toaSms.setPeticion(int_sms.getXa_peticion());

            toaSms.setExternal_id(int_sms.getExternal_id());
            toaSms.setXa_identificador_st(int_sms.getXa_identificador_st());
            toaSms.setAppt_number(int_sms.getAppt_number());
            toaSms.setAid(int_sms.getAid());
            toaSms.setNumber(int_sms.getNumber());
            toaSms.setNumber1(int_sms.getNumber1());
            toaSms.setNumber2(int_sms.getNumber2());
            toaSms.setNumber3(int_sms.getNumber3());
            toaSms.setText(int_sms.getText());
            toaSms.setXA_NUMBER_WORK_ORDER(int_sms.getXA_NUMBER_WORK_ORDER());
            toaSms.setXA_NUMBER_SERVICE_ORDER(int_sms.getXA_NUMBER_SERVICE_ORDER());

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaSms.setInsert_date(timestamp);

            toaSmsRepository.save(toaSms);
            result = "";

        }catch(Exception ex){
            ex.printStackTrace();
            result = ex.getMessage();
        }

        return result;
    }*/
}