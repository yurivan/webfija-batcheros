package pe.telefonica.tracer.business;

import pe.telefonica.tracer.persistence.TraceabilityAccessDB;
import pe.telefonica.tracer.persistence.TraceabilityAccessLogDB;

/**
 * Created by Felix on 19/06/2017.
 */
public interface TraceabilityAccessService {

    public TraceabilityAccessDB getTraceabilityAccess(String token, Integer type) throws Exception;

    public TraceabilityAccessLogDB saveLog(String message, Integer tokenId) throws Exception;
}
