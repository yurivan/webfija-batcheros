package pe.telefonica.tracer.business;

import pe.telefonica.tracer.persistence.VisorByDocDB;
import pe.telefonica.tracer.persistence.VisorByRequestCodeDB;

import java.util.List;

/**
 * Created by Javier on 19/06/2017.
 */
public interface VisorService {

    public List<VisorByDocDB> getVisorByDoc(String document) throws Exception;

    public VisorByRequestCodeDB getVisorByRequestCode(String requestCode) throws Exception;
}
