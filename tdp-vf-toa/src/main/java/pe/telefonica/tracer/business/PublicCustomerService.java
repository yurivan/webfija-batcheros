package pe.telefonica.tracer.business;

import pe.telefonica.tracer.model.PublicCustomerRequest;
import pe.telefonica.tracer.persistence.PublicCustomerDataDB;
import pe.telefonica.tracer.persistence.PublicCustomerRatingDB;
import pe.telefonica.tracer.persistence.TraceabilityAccessDB;
import pe.telefonica.tracer.persistence.TraceabilityAccessLogDB;

/**
 * Created by Felix on 19/06/2017.
 */
public interface PublicCustomerService {

    public PublicCustomerDataDB getPublicCustomerData(String document) throws Exception;

    public PublicCustomerDataDB savePublicCustomerData(PublicCustomerDataDB publicCustomerData, PublicCustomerRequest publicCustomerRequest) throws Exception;

    public String validateDataFieldsRequest(PublicCustomerRequest request) throws Exception;

    public PublicCustomerRatingDB getPublicCustomerRating(String document) throws Exception;

    public PublicCustomerRatingDB savePublicCustomerRating(PublicCustomerRatingDB publicCustomerData, PublicCustomerRequest publicCustomerRequest) throws Exception;

    public String validateRatingFieldsRequest(PublicCustomerRequest request) throws Exception;
}
