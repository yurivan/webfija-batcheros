package pe.telefonica.tracer.business.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.VisorService;
import pe.telefonica.tracer.model.VisorByDocResponse;
import pe.telefonica.tracer.persistence.VisorByDocDB;
import pe.telefonica.tracer.persistence.VisorByRequestCodeDB;
import pe.telefonica.tracer.persistence.StatusToaDB;
import pe.telefonica.tracer.persistence.repository.VisorByDocRepository;
import pe.telefonica.tracer.persistence.repository.VisorByRequestCodeRepository;
import pe.telefonica.tracer.persistence.repository.VisorUpdateToaRepository;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Felix on 19/06/2017.
 */
@Service
public class VisorServiceImpl implements VisorService {

    private static final Logger _log = LoggerFactory.getLogger(VisorServiceImpl.class);

    @Autowired
    private VisorByDocRepository repositoryByDoc;

    @Autowired
    private VisorByRequestCodeRepository repositoryByRequestCode;

    @Autowired
    private VisorUpdateToaRepository repositotyUpdateToa;

    @Override
    public List<VisorByDocDB> getVisorByDoc(String document) throws Exception{
        try{
            List<VisorByDocDB> items = repositoryByDoc.getVisorByDoc(document);
            return items;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public VisorByRequestCodeDB getVisorByRequestCode(String requestCode) throws Exception{
        try{
            VisorByRequestCodeDB item = null;
            Object[] obj = repositoryByRequestCode.getVisorByRequestCode(requestCode);
            if(obj != null && obj.length > 0 && obj[0] != null){
                item = transform((Object[])obj[0]);
            }
            return item;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    private VisorByRequestCodeDB transform(Object[] obj) throws Exception{
        try{

            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            VisorByRequestCodeDB item = new VisorByRequestCodeDB();
            item.setId(obj[0] != null ? obj[0].toString() : "");
            item.setCodigo_pedido(obj[1] != null ? obj[1].toString() : "");
            if(obj[2] != null && !"".equals(obj[2])){
                if(obj[2].equals("1975-01-01 00:00:00") || obj[2].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_grabacion(null);
                }else{
                    Date date = sdf1.parse(obj[2].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_grabacion(dataStr);
                }
            }else{
                item.setFecha_grabacion(null);
            }
            item.setCliente(obj[3] != null ? obj[3].toString() : "");
            item.setTelefono(obj[4] != null ? obj[4].toString() : "");
            item.setDireccion(obj[5] != null ? obj[5].toString() : "");
            item.setDepartamento(obj[6] != null ? obj[6].toString() : "");
            item.setDni(obj[7] != null ? obj[7].toString() : "");

            if(obj[8] != null && !"".equals(obj[8].toString().trim())){
                item.setNombre_producto(obj[8].toString());
            }else{
                String nombreProducto2 = obj[9] != null ? obj[9].toString() : "";
                String subProduct = obj[14] != null ? obj[14].toString() : "";
                item.setNombre_producto(nombreProducto2 + ": " + subProduct);
            }
            //item.setNombre_producto(obj[7] != null ? obj[7].toString() : "");

            item.setEstado_solicitud(obj[10] != null ? obj[10].toString() : "");
            item.setMotivo_estado(obj[11] != null ? obj[11].toString() : "");
            item.setOperacion_comercial(obj[12] != null ? obj[12].toString() : "");
            item.setDistrito(obj[13] != null ? obj[13].toString() : "");
            //item.setSub_producto(obj[13] != null ? obj[13].toString() : "");
            item.setTipo_documento(obj[15] != null ? obj[15].toString() : "");
            item.setProvincia(obj[16] != null ? obj[16].toString() : "");
            if(obj[17] != null && !"".equals(obj[17])){
                if(obj[17].equals("1975-01-01 00:00:00") || obj[17].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_de_llamada(null);
                }else{
                    Date date = sdf1.parse(obj[17].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_de_llamada(dataStr);
                }
            }else{
                item.setFecha_de_llamada(null);
            }
            item.setNombre_producto_fuente(obj[18] != null ? obj[18].toString() : "");
            if(obj[19] != null && !"".equals(obj[19])){
                if(obj[19].equals("1975-01-01 00:00:00")  || obj[19].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_solicitado(null);
                }else{
                    Date date = sdf2.parse(obj[19].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_solicitado(dataStr);
                }
            }else{
                item.setFecha_solicitado(null);
            }
            if(obj[20] != null && !"".equals(obj[20])){
                if(obj[20].equals("1975-01-01 00:00:00")  || obj[20].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_registrado(null);
                }else{
                    Date date = sdf2.parse(obj[20].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_registrado(dataStr);
                }
            }else{
                item.setFecha_registrado(null);
            }
            if(obj[21] != null && !"".equals(obj[21])){
                if(obj[21].equals("1975-01-01 00:00:00")  || obj[21].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_equipo_tecnico(null);
                }else{
                    Date date = sdf2.parse(obj[21].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_equipo_tecnico(dataStr);
                }
            }else{
                item.setFecha_equipo_tecnico(null);
            }
            if(obj[22] != null && !"".equals(obj[22])){
                if(obj[22].equals("1975-01-01 00:00:00")  || obj[22].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_instalado(null);
                }else{
                    Date date = sdf2.parse(obj[22].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_instalado(dataStr);
                }
            }else{
                item.setFecha_instalado(null);
            }
            item.setEstado_anterior(obj[23] != null ? obj[23].toString() : "");

            //Nuevos campos
            item.setTelefono3(obj[24] != null ? obj[24].toString() : "");
            item.setTelefono4(obj[25] != null ? obj[25].toString() : "");
            item.setDireccion2(obj[26] != null ? obj[26].toString() : "");
            item.setCorreo2(obj[27] != null ? obj[27].toString() : "");
            item.setValoracion(obj[28] != null ? obj[28].toString() : "");
            item.setCoordenada_x(obj[29] != null ? obj[29].toString() : "");
            item.setCoordenada_y(obj[30] != null ? obj[30].toString() : "");

            return item;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    public List<VisorByDocResponse> toVisorByDocResponseList(List<VisorByDocDB> items) throws Exception{
        try{
            List<VisorByDocResponse> result = new ArrayList<>();
            for(VisorByDocDB item : items){
                VisorByDocResponse obj =  new VisorByDocResponse();
                obj.setCodigo_pedido(item.getCodigo_pedido());
                result.add(obj);
            }
            return result;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }
}
