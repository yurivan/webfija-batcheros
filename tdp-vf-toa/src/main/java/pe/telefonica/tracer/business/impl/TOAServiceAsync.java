package pe.telefonica.tracer.business.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import pe.telefonica.tracer.model.toa.*;
import pe.telefonica.tracer.persistence.ToaLogDB;
import pe.telefonica.tracer.persistence.ToaSmsDB;
import pe.telefonica.tracer.persistence.ToaStatusDB;
import pe.telefonica.tracer.persistence.ToaStatusInventoryDB;
import pe.telefonica.tracer.persistence.repository.*;
//import toatech.agent.Message;
import pe.telefonica.tracer.model.toa.generated.Message;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@Component
public class TOAServiceAsync {

    private static final Logger _log = LoggerFactory.getLogger(TraceabilityAccessServiceImpl.class);

    @Autowired
    private ToaStatusRepository toaStatusRepository;

    @Autowired
    private ToaSmsRepository toaSmsRepository;

    @Autowired
    private ToaStatusInventoryRepository toaStatusInventoryRepository;

    @Autowired
    private ToaLogRepository toaLogRepository;

    @Autowired
    private VisorByRequestCodeRepository visorRepository;

    @Autowired
    private VisorDatesRepository visorDatesRepository;

    //@Autowired
    //private VisorByRequestCodeDateRepository visorByRequestCodeDateRepository;

    public boolean existsRequest(String requestCode){
        /*boolean result = false;
        try{
            if(requestCode == null || "".equals(requestCode.trim())) return result;
            Object[] request = visorRepository.getVisorByRequestCodeSimple(requestCode);
            if(request != null && request.length > 0){
                result = true;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return result;*/
        return true;
    }

    //@Async
    public CompletableFuture<String> processIN_TOA(IN_TOA in_toa, String messageId, String xml){
        String response = "";
        try{
            if(in_toa.getXa_peticion() == null || in_toa.getXa_peticion().trim().equals("")){
                if(in_toa.getAppt_number() != null && !in_toa.getAppt_number().trim().equals("")){
                    in_toa.setXa_peticion(in_toa.getAppt_number());
                }
            }

            /*boolean existsRequest = existsRequest(in_toa.getXa_peticion());
            if(!existsRequest){
                return CompletableFuture.completedFuture("Message_Id "+ messageId + " = " + " El código de pedido no existe.");
            }*/

            String processMessage = saveIN_TOA(in_toa, messageId, xml);
            if("".equals(processMessage)){
                response = "Estado registrado correctamente.";

                if(in_toa.getXa_peticion() != null && !in_toa.getXa_peticion().trim().equals("") && !in_toa.getXa_peticion().trim().equals("0")){
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateStr = df.format(new Date());
                    Date date = df.parse(dateStr);
                    Timestamp techAssignDate = new Timestamp(date.getTime());
                    visorDatesRepository.updateTechAssignDate(in_toa.getXa_peticion(), techAssignDate);
                }

            }else{
                response = "Error al registrar el estado: " + processMessage;
                saveLog(messageId, response);
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveIN_TOA(IN_TOA in_toa, String messageId, String xml){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("IN_TOA");
            toaStatus.setMessage_id(messageId);
            toaStatus.setPeticion(in_toa.getXa_peticion());
            toaStatus.setExternal_id(in_toa.getExternal_id());
            toaStatus.setXa_creation_date(in_toa.getXa_creation_date());
            toaStatus.setXa_identificador_st(in_toa.getXa_identificador_st());
            toaStatus.setXa_requirement_number(in_toa.getXa_requirement_number());
            toaStatus.setAppt_number(in_toa.getAppt_number());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(in_toa.getXA_NUMBER_SERVICE_ORDER());

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);

        }catch(Exception ex){
            String messageError = "MessageId " + messageId + " -> " + ex.toString() + " : " + ex.getMessage() + " \n " + xml;
            _log.error(messageError);
            result = messageError;
        }

        return result;
    }

    //@Async
    public CompletableFuture<String> processWO_INIT(WO_INIT wo_init, String messageId, String xml){
        String response = "";
        try{
            if(wo_init.getXa_peticion() == null || wo_init.getXa_peticion().trim().equals("")){
                if(wo_init.getAppt_number() != null && !wo_init.getAppt_number().trim().equals("")){
                    wo_init.setXa_peticion(wo_init.getAppt_number());
                }
            }

            /*boolean existsRequest = existsRequest(wo_init.getXa_peticion());
            if(!existsRequest){
                return CompletableFuture.completedFuture("Message_Id "+ messageId + " = " + " El código de pedido no existe.");
            }*/

            String processMessage = saveWO_INIT(wo_init,messageId,xml);
            if("".equals(processMessage)) response = "Estado registrado correctamente.";
            else{
                response = "Error al registrar el estado: " + processMessage;
                saveLog(messageId, response);
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveWO_INIT(WO_INIT wo_init, String messageId, String xml){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("WO_INIT");
            toaStatus.setMessage_id(messageId);
            toaStatus.setPeticion(wo_init.getXa_peticion());

            toaStatus.setExternal_id(wo_init.getExternal_id());
            toaStatus.setXa_identificador_st(wo_init.getXa_identificador_st());
            toaStatus.setEta_start_time(wo_init.getEta_start_time());
            toaStatus.setAstatus(wo_init.getAstatus());
            toaStatus.setAppt_number(wo_init.getAppt_number());
            toaStatus.setAid(wo_init.getAid());
            toaStatus.setXA_NUMBER_WORK_ORDER(wo_init.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(wo_init.getXA_NUMBER_SERVICE_ORDER());

            String A_COMMENT_TECHNICIAN = "";

            if(wo_init.getActivity_Properties() != null && wo_init.getActivity_Properties().getProperty() != null){
                for(property property : wo_init.getActivity_Properties().getProperty()){
                    if("A_COMMENT_TECHNICIAN".equals(property.getLabel())){ A_COMMENT_TECHNICIAN = property.getValue(); continue; }
                }
            }

            toaStatus.setA_COMMENT_TECHNICIAN(A_COMMENT_TECHNICIAN);

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);

        }catch(Exception ex){
            String messageError = "MessageId " + messageId + " -> " + ex.toString() + " : " + ex.getMessage() + " \n " + xml;
            _log.error(messageError);
            result = messageError;
        }

        return result;
    }

    //@Async
    public CompletableFuture<String> processNOT_LEG(NOT_LEG not_leg, String messageId, String xml){
        String response = "";
        try{
            if(not_leg.getXa_peticion() == null || not_leg.getXa_peticion().trim().equals("")){
                if(not_leg.getAppt_number() != null && !not_leg.getAppt_number().trim().equals("")){
                    not_leg.setXa_peticion(not_leg.getAppt_number());
                }
            }

            /*boolean existsRequest = existsRequest(not_leg.getXa_peticion());
            if(!existsRequest){
                return CompletableFuture.completedFuture("Message_Id "+ messageId + " = " + " El código de pedido no existe.");
            }*/

            String processMessage = saveNOT_LEG(not_leg,messageId,xml);

            if("".equals(processMessage)) response = "Estado registrado correctamente.";
            else{
                response = "Error al registrar el estado: " + processMessage;
                saveLog(messageId, response);
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveNOT_LEG(NOT_LEG not_leg, String messageId, String xml){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("NOT_LEG");
            toaStatus.setMessage_id(messageId);
            toaStatus.setPeticion(not_leg.getXa_peticion());

            toaStatus.setXa_identificador_st(not_leg.getXa_identificador_st());
            toaStatus.setDelivery_window_start(not_leg.getDelivery_window_start());
            toaStatus.setDelivery_window_end(not_leg.getDelivery_window_end());
            toaStatus.setDate(not_leg.getDate());
            toaStatus.setUsuario(not_leg.getUsuario());
            toaStatus.setTime_slot(not_leg.getTime_slot());
            toaStatus.setStart_time(not_leg.getStart_time());
            toaStatus.setEnd_time(not_leg.getEnd_time());
            toaStatus.setAstatus(not_leg.getAstatus());
            toaStatus.setTime_of_assignment(not_leg.getTime_of_assignment());
            toaStatus.setTime_of_booking(not_leg.getTime_of_booking());
            toaStatus.setDuration(not_leg.getDuration());
            toaStatus.setExternal_id(not_leg.getExternal_id());
            toaStatus.setAppt_number(not_leg.getAppt_number());
            toaStatus.setAid(not_leg.getAid());
            toaStatus.setPname(not_leg.getPname());
            toaStatus.setXA_NUMBER_WORK_ORDER(not_leg.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(not_leg.getXA_NUMBER_SERVICE_ORDER());

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);

        }catch(Exception ex){
            String messageError = "MessageId " + messageId + " -> " + ex.toString() + " : " + ex.getMessage() + " \n " + xml;
            _log.error(messageError);
            result = messageError;
        }

        return result;
    }

    //@Async
    public CompletableFuture<String> processWO_PRE_COMPLETED(WO_PRE_COMPLETED wo_pre_completed, String messageId, String xml){
        String response = "";
        try{
            if(wo_pre_completed.getXa_peticion() == null || wo_pre_completed.getXa_peticion().trim().equals("")){
                if(wo_pre_completed.getAppt_number() != null && !wo_pre_completed.getAppt_number().trim().equals("")){
                    wo_pre_completed.setXa_peticion(wo_pre_completed.getAppt_number());
                }
            }

            /*boolean existsRequest = existsRequest(wo_pre_completed.getXa_peticion());
            if(!existsRequest){
                return CompletableFuture.completedFuture("Message_Id "+ messageId + " = " + " El código de pedido no existe.");
            }*/

            String processMessage = saveWO_PRE_COMPLETED(wo_pre_completed,messageId,xml);
            if("".equals(processMessage)) response = "Estado registrado correctamente.";
            else{
                response = "Error al registrar el estado: " + processMessage;
                saveLog(messageId, response);
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveWO_PRE_COMPLETED(WO_PRE_COMPLETED wo_pre_completed, String messageId, String xml){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("WO_PRE_COMPLETED");
            toaStatus.setMessage_id(messageId);
            toaStatus.setPeticion(wo_pre_completed.getXa_peticion());

            toaStatus.setExternal_id(wo_pre_completed.getExternal_id());
            toaStatus.setXa_identificador_st(wo_pre_completed.getXa_identificador_st());
            toaStatus.setEta_start_time(wo_pre_completed.getEta_start_time());
            toaStatus.setEta_end_time(wo_pre_completed.getEta_end_time());
            toaStatus.setAstatus(wo_pre_completed.getAstatus());
            toaStatus.setAppt_number(wo_pre_completed.getAppt_number());
            toaStatus.setXA_NUMBER_WORK_ORDER(wo_pre_completed.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(wo_pre_completed.getXA_NUMBER_SERVICE_ORDER());

            String A_OBSERVATION = "";
            String XA_SOURCE_SYSTEM = "";
            String A_NETWORK_CHANGE_ELEMENT_LIST = "";
            String A_RECEIVE_PERSON_NAME = "";
            String A_RECEIVE_PERSON_ID = "";

            String A_COMPLETE_REASON_INSTALL = "";
            String A_COMPLETE_CAUSA_REP_STB = "";
            String A_COMPLETE_REP_STB = "";
            String A_COMPLETE_CAUSA_REP_ADSL = "";
            String A_COMPLETE_REP_ADSL = "";
            String A_COMPLETE_CAUSA_REP_SAT = "";
            String A_COMPLETE_REP_SAT = "";
            String A_COMPLETE_CAUSA_REP_CBL = "";
            String A_COMPLETE_REP_CBL = "";

            if(wo_pre_completed.getActivity_properties() != null && wo_pre_completed.getActivity_properties().getProperty() != null){
                for(property property : wo_pre_completed.getActivity_properties().getProperty()){
                    if("A_OBSERVATION".equals(property.getLabel())){ A_OBSERVATION = property.getValue(); continue; }
                    if("XA_SOURCE_SYSTEM".equals(property.getLabel())){ XA_SOURCE_SYSTEM = property.getValue(); continue; }
                    if("A_NETWORK_CHANGE_ELEMENT_LIST".equals(property.getLabel())){ A_NETWORK_CHANGE_ELEMENT_LIST = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_NAME".equals(property.getLabel())){ A_RECEIVE_PERSON_NAME = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_ID".equals(property.getLabel())){ A_RECEIVE_PERSON_ID = property.getValue(); continue; }

                    if("A_COMPLETE_REASON_INSTALL".equals(property.getLabel())){ A_COMPLETE_REASON_INSTALL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_STB".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_REP_STB".equals(property.getLabel())){ A_COMPLETE_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_SAT".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_REP_SAT".equals(property.getLabel())){ A_COMPLETE_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_CBL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_CBL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_CBL".equals(property.getLabel())){ A_COMPLETE_REP_CBL = property.getValue(); continue; }
                }
            }

            toaStatus.setA_OBSERVATION(A_OBSERVATION);
            toaStatus.setXA_SOURCE_SYSTEM(XA_SOURCE_SYSTEM);
            toaStatus.setA_NETWORK_CHANGE_ELEMENT_LIST(A_NETWORK_CHANGE_ELEMENT_LIST);
            toaStatus.setA_RECEIVE_PERSON_NAME(A_RECEIVE_PERSON_NAME);
            toaStatus.setA_RECEIVE_PERSON_ID(A_RECEIVE_PERSON_ID);

            toaStatus.setA_COMPLETE_REASON_INSTALL(A_COMPLETE_REASON_INSTALL);
            toaStatus.setA_COMPLETE_CAUSA_REP_STB(A_COMPLETE_CAUSA_REP_STB);
            toaStatus.setA_COMPLETE_REP_STB(A_COMPLETE_REP_STB);
            toaStatus.setA_COMPLETE_CAUSA_REP_ADSL(A_COMPLETE_CAUSA_REP_ADSL);
            toaStatus.setA_COMPLETE_REP_ADSL(A_COMPLETE_REP_ADSL);
            toaStatus.setA_COMPLETE_CAUSA_REP_SAT(A_COMPLETE_CAUSA_REP_SAT);
            toaStatus.setA_COMPLETE_REP_SAT(A_COMPLETE_REP_SAT);
            toaStatus.setA_COMPLETE_CAUSA_REP_CBL(A_COMPLETE_CAUSA_REP_CBL);
            toaStatus.setA_COMPLETE_REP_CBL(A_COMPLETE_REP_CBL);


            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);

        }catch(Exception ex){
            String messageError = "MessageId " + messageId + " -> " + ex.toString() + " : " + ex.getMessage() + " \n " + xml;
            _log.error(messageError);
            result = messageError;
        }

        return result;
    }

    //@Async
    public CompletableFuture<String> processWO_COMPLETED(WO_COMPLETED wo_completed, String messageId, String xml){
        String response = "";
        try{
            if(wo_completed.getXa_peticion() == null || wo_completed.getXa_peticion().trim().equals("")){
                if(wo_completed.getAppt_number() != null && !wo_completed.getAppt_number().trim().equals("")){
                    wo_completed.setXa_peticion(wo_completed.getAppt_number());
                }
            }

            /*boolean existsRequest = existsRequest(wo_completed.getXa_peticion());
            if(!existsRequest){
                return CompletableFuture.completedFuture("Message_Id "+ messageId + " = " + " El código de pedido no existe.");
            }*/

            String processMessage = saveWO_COMPLETED(wo_completed, messageId, xml);

            if("".equals(processMessage)){
                response = "Estado registrado correctamente.";

                if(wo_completed.getXa_peticion() != null && !wo_completed.getXa_peticion().equals("") && !wo_completed.getXa_peticion().equals("0")){
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateStr = df.format(new Date());
                    Date date = df.parse(dateStr);
                    Timestamp installedDate = new Timestamp(date.getTime());
                    visorDatesRepository.updateInstalledDate(wo_completed.getXa_peticion(), installedDate);
                }


            }else{
                response = "Error al registrar el estado: " + processMessage;
                saveLog(messageId, response);
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveWO_COMPLETED(WO_COMPLETED wo_completed, String messageId, String xml){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("WO_COMPLETED");
            toaStatus.setMessage_id(messageId);
            toaStatus.setPeticion(wo_completed.getXa_peticion());

            toaStatus.setExternal_id(wo_completed.getExternal_id());
            toaStatus.setXa_identificador_st(wo_completed.getXa_identificador_st());
            toaStatus.setEta_start_time(wo_completed.getEta_start_time());
            toaStatus.setEta_end_time(wo_completed.getEta_end_time());
            toaStatus.setAstatus(wo_completed.getAstatus());
            toaStatus.setDate(wo_completed.getDate());
            toaStatus.setAppt_number(wo_completed.getAppt_number());
            toaStatus.setXA_NUMBER_WORK_ORDER(wo_completed.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(wo_completed.getXA_NUMBER_SERVICE_ORDER());

            String A_OBSERVATION = "";
            String XA_SOURCE_SYSTEM = "";
            String A_NETWORK_CHANGE_ELEMENT_LIST = "";
            String A_RECEIVE_PERSON_NAME = "";
            String A_RECEIVE_PERSON_ID = "";

            String A_COMPLETE_REASON_INSTALL = "";
            String A_COMPLETE_CAUSA_REP_STB = "";
            String A_COMPLETE_REP_STB = "";
            String A_COMPLETE_CAUSA_REP_ADSL = "";
            String A_COMPLETE_REP_ADSL = "";
            String A_COMPLETE_CAUSA_REP_SAT = "";
            String A_COMPLETE_REP_SAT = "";
            String A_COMPLETE_CAUSA_REP_CBL = "";
            String A_COMPLETE_REP_CBL = "";

            if(wo_completed.getActivity_properties() != null && wo_completed.getActivity_properties().getProperty() != null){
                for(property property : wo_completed.getActivity_properties().getProperty()){
                    if("A_OBSERVATION".equals(property.getLabel())){ A_OBSERVATION = property.getValue(); continue; }
                    if("XA_SOURCE_SYSTEM".equals(property.getLabel())){ XA_SOURCE_SYSTEM = property.getValue(); continue; }
                    if("A_NETWORK_CHANGE_ELEMENT_LIST".equals(property.getLabel())){ A_NETWORK_CHANGE_ELEMENT_LIST = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_NAME".equals(property.getLabel())){ A_RECEIVE_PERSON_NAME = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_ID".equals(property.getLabel())){ A_RECEIVE_PERSON_ID = property.getValue(); continue; }

                    if("A_COMPLETE_REASON_INSTALL".equals(property.getLabel())){ A_COMPLETE_REASON_INSTALL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_STB".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_REP_STB".equals(property.getLabel())){ A_COMPLETE_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_SAT".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_REP_SAT".equals(property.getLabel())){ A_COMPLETE_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_CBL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_CBL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_CBL".equals(property.getLabel())){ A_COMPLETE_REP_CBL = property.getValue(); continue; }
                }
            }

            toaStatus.setA_OBSERVATION(A_OBSERVATION);
            toaStatus.setXA_SOURCE_SYSTEM(XA_SOURCE_SYSTEM);
            toaStatus.setA_NETWORK_CHANGE_ELEMENT_LIST(A_NETWORK_CHANGE_ELEMENT_LIST);
            toaStatus.setA_RECEIVE_PERSON_NAME(A_RECEIVE_PERSON_NAME);
            toaStatus.setA_RECEIVE_PERSON_ID(A_RECEIVE_PERSON_ID);

            toaStatus.setA_COMPLETE_REASON_INSTALL(A_COMPLETE_REASON_INSTALL);
            toaStatus.setA_COMPLETE_CAUSA_REP_STB(A_COMPLETE_CAUSA_REP_STB);
            toaStatus.setA_COMPLETE_REP_STB(A_COMPLETE_REP_STB);
            toaStatus.setA_COMPLETE_CAUSA_REP_ADSL(A_COMPLETE_CAUSA_REP_ADSL);
            toaStatus.setA_COMPLETE_REP_ADSL(A_COMPLETE_REP_ADSL);
            toaStatus.setA_COMPLETE_CAUSA_REP_SAT(A_COMPLETE_CAUSA_REP_SAT);
            toaStatus.setA_COMPLETE_REP_SAT(A_COMPLETE_REP_SAT);
            toaStatus.setA_COMPLETE_CAUSA_REP_CBL(A_COMPLETE_CAUSA_REP_CBL);
            toaStatus.setA_COMPLETE_REP_CBL(A_COMPLETE_REP_CBL);


            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatus = toaStatusRepository.save(toaStatus);

            saveInventory(wo_completed.getInstalled_inventory(), wo_completed.getDeinstalled_inventory(),
                    wo_completed.getCustomer_inventory(), wo_completed.getResource_inventory(), toaStatus.getId(), messageId);

        }catch(Exception ex){
            String messageError = "MessageId " + messageId + " -> " + ex.toString() + " : " + ex.getMessage() + " \n " + xml;
            _log.error(messageError);
            result = messageError;
        }

        return result;
    }

    //@Async
    public CompletableFuture<String> processWO_PRE_COMPLETED_L(WO_COMPLETED wo_completed, String messageId, String xml){
        String response = "";
        try{
            if(wo_completed.getXa_peticion() == null || wo_completed.getXa_peticion().trim().equals("")){
                if(wo_completed.getAppt_number() != null && !wo_completed.getAppt_number().trim().equals("")){
                    wo_completed.setXa_peticion(wo_completed.getAppt_number());
                }
            }

            /*boolean existsRequest = existsRequest(wo_completed.getXa_peticion());
            if(!existsRequest){
                return CompletableFuture.completedFuture("Message_Id "+ messageId + " = " + " El código de pedido no existe.");
            }*/

            String processMessage = saveWO_PRE_COMPLETED_L(wo_completed, messageId, xml);

            if("".equals(processMessage)) response = "Estado registrado correctamente.";
            else{
                response = "Error al registrar el estado: " + processMessage;
                saveLog(messageId, response);
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveWO_PRE_COMPLETED_L(WO_COMPLETED wo_completed, String messageId, String xml){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("WO_PRE_COMPLETED_L");
            toaStatus.setMessage_id(messageId);
            toaStatus.setPeticion(wo_completed.getXa_peticion());

            toaStatus.setExternal_id(wo_completed.getExternal_id());
            toaStatus.setXa_identificador_st(wo_completed.getXa_identificador_st());
            toaStatus.setEta_start_time(wo_completed.getEta_start_time());
            toaStatus.setEta_end_time(wo_completed.getEta_end_time());
            toaStatus.setAstatus(wo_completed.getAstatus());
            toaStatus.setAppt_number(wo_completed.getAppt_number());
            toaStatus.setXA_NUMBER_WORK_ORDER(wo_completed.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(wo_completed.getXA_NUMBER_SERVICE_ORDER());

            String A_OBSERVATION = "";
            String XA_SOURCE_SYSTEM = "";
            String A_NETWORK_CHANGE_ELEMENT_LIST = "";
            String A_RECEIVE_PERSON_NAME = "";
            String A_RECEIVE_PERSON_ID = "";

            String A_COMPLETE_REASON_INSTALL = "";
            String A_COMPLETE_CAUSA_REP_STB = "";
            String A_COMPLETE_REP_STB = "";
            String A_COMPLETE_CAUSA_REP_ADSL = "";
            String A_COMPLETE_REP_ADSL = "";
            String A_COMPLETE_CAUSA_REP_SAT = "";
            String A_COMPLETE_REP_SAT = "";
            String A_COMPLETE_CAUSA_REP_CBL = "";
            String A_COMPLETE_REP_CBL = "";

            if(wo_completed.getActivity_properties() != null && wo_completed.getActivity_properties().getProperty() != null){
                for(property property : wo_completed.getActivity_properties().getProperty()){
                    if("A_OBSERVATION".equals(property.getLabel())){ A_OBSERVATION = property.getValue(); continue; }
                    if("XA_SOURCE_SYSTEM".equals(property.getLabel())){ XA_SOURCE_SYSTEM = property.getValue(); continue; }
                    if("A_NETWORK_CHANGE_ELEMENT_LIST".equals(property.getLabel())){ A_NETWORK_CHANGE_ELEMENT_LIST = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_NAME".equals(property.getLabel())){ A_RECEIVE_PERSON_NAME = property.getValue(); continue; }
                    if("A_RECEIVE_PERSON_ID".equals(property.getLabel())){ A_RECEIVE_PERSON_ID = property.getValue(); continue; }

                    if("A_COMPLETE_REASON_INSTALL".equals(property.getLabel())){ A_COMPLETE_REASON_INSTALL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_STB".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_REP_STB".equals(property.getLabel())){ A_COMPLETE_REP_STB = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_ADSL".equals(property.getLabel())){ A_COMPLETE_REP_ADSL = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_SAT".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_REP_SAT".equals(property.getLabel())){ A_COMPLETE_REP_SAT = property.getValue(); continue; }
                    if("A_COMPLETE_CAUSA_REP_CBL".equals(property.getLabel())){ A_COMPLETE_CAUSA_REP_CBL = property.getValue(); continue; }
                    if("A_COMPLETE_REP_CBL".equals(property.getLabel())){ A_COMPLETE_REP_CBL = property.getValue(); continue; }
                }
            }

            toaStatus.setA_OBSERVATION(A_OBSERVATION);
            toaStatus.setXA_SOURCE_SYSTEM(XA_SOURCE_SYSTEM);
            toaStatus.setA_NETWORK_CHANGE_ELEMENT_LIST(A_NETWORK_CHANGE_ELEMENT_LIST);
            toaStatus.setA_RECEIVE_PERSON_NAME(A_RECEIVE_PERSON_NAME);
            toaStatus.setA_RECEIVE_PERSON_ID(A_RECEIVE_PERSON_ID);

            toaStatus.setA_COMPLETE_REASON_INSTALL(A_COMPLETE_REASON_INSTALL);
            toaStatus.setA_COMPLETE_CAUSA_REP_STB(A_COMPLETE_CAUSA_REP_STB);
            toaStatus.setA_COMPLETE_REP_STB(A_COMPLETE_REP_STB);
            toaStatus.setA_COMPLETE_CAUSA_REP_ADSL(A_COMPLETE_CAUSA_REP_ADSL);
            toaStatus.setA_COMPLETE_REP_ADSL(A_COMPLETE_REP_ADSL);
            toaStatus.setA_COMPLETE_CAUSA_REP_SAT(A_COMPLETE_CAUSA_REP_SAT);
            toaStatus.setA_COMPLETE_REP_SAT(A_COMPLETE_REP_SAT);
            toaStatus.setA_COMPLETE_CAUSA_REP_CBL(A_COMPLETE_CAUSA_REP_CBL);
            toaStatus.setA_COMPLETE_REP_CBL(A_COMPLETE_REP_CBL);


            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatus = toaStatusRepository.save(toaStatus);

            saveInventory(wo_completed.getInstalled_inventory(), wo_completed.getDeinstalled_inventory(),
                    wo_completed.getCustomer_inventory(), wo_completed.getResource_inventory(), toaStatus.getId(), messageId);

        }catch(Exception ex){
            String messageError = "MessageId " + messageId + " -> " + ex.toString() + " : " + ex.getMessage() + " \n " + xml;
            _log.error(messageError);
            result = messageError;
        }

        return result;
    }

    //@Async
    public CompletableFuture<String> processWO_SUSPEND(WO_SUSPEND wo_suspend, String messageId, String xml){
        String response = "";
        try{
            if(wo_suspend.getActivity_Properties().getXa_peticion() == null || wo_suspend.getActivity_Properties().getXa_peticion().trim().equals("")){
                if(wo_suspend.getActivity_Properties().getAppt_number() != null && !wo_suspend.getActivity_Properties().getAppt_number().trim().equals("")){
                    wo_suspend.getActivity_Properties().setXa_peticion(wo_suspend.getActivity_Properties().getAppt_number());
                }
            }

            /*boolean existsRequest = existsRequest(wo_suspend.getActivity_Properties().getXa_peticion());
            if(!existsRequest){
                return CompletableFuture.completedFuture("Message_Id "+ messageId + " = " + " El código de pedido no existe.");
            }*/

            String processMessage = saveWO_SUSPEND(wo_suspend,messageId,xml);
            if("".equals(processMessage)) response = "Estado registrado correctamente.";
            else{
                response = "Error al registrar el estado: " + processMessage;
                saveLog(messageId, response);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveWO_SUSPEND(WO_SUSPEND wo_suspend, String messageId, String xml){
        String result = "";
        try{
            ToaStatusDB toaStatus = new ToaStatusDB();

            activity_properties activity_properties = wo_suspend.getActivity_Properties();

            if(activity_properties != null){
                toaStatus.setStatus_toa("WO_SUSPEND");
                toaStatus.setMessage_id(messageId);
                toaStatus.setPeticion(activity_properties.getXa_peticion());

                toaStatus.setExternal_id(activity_properties.getExternal_id());
                toaStatus.setXa_identificador_st(activity_properties.getXa_identificador_st());
                toaStatus.setETA(activity_properties.getETA());
                toaStatus.setEta_end_time(activity_properties.getEta_end_time());
                toaStatus.setAstatus(activity_properties.getAstatus());
                toaStatus.setAppt_number(activity_properties.getAppt_number());
                toaStatus.setXA_NUMBER_WORK_ORDER(activity_properties.getXA_NUMBER_WORK_ORDER());
                toaStatus.setXA_NUMBER_SERVICE_ORDER(activity_properties.getXA_NUMBER_SERVICE_ORDER());

                String A_OBSERVATION = "";
                String XA_SOURCE_SYSTEM = "";
                String A_NETWORK_CHANGE_ELEMENT_LIST = "";
                String A_SUSPEND_REASON = "";

                if(activity_properties != null && activity_properties.getProperty() != null){
                    for(property property : activity_properties.getProperty()){
                        if("A_OBSERVATION".equals(property.getLabel())){ A_OBSERVATION = property.getValue(); continue; }
                        if("XA_SOURCE_SYSTEM".equals(property.getLabel())){ XA_SOURCE_SYSTEM = property.getValue(); continue; }
                        if("A_NETWORK_CHANGE_ELEMENT_LIST".equals(property.getLabel())){ A_NETWORK_CHANGE_ELEMENT_LIST = property.getValue(); continue; }
                        if("A_SUSPEND_REASON".equals(property.getLabel())){ A_SUSPEND_REASON = property.getValue(); continue; }
                    }
                }

                toaStatus.setA_OBSERVATION(A_OBSERVATION);
                toaStatus.setXA_SOURCE_SYSTEM(XA_SOURCE_SYSTEM);
                toaStatus.setA_NETWORK_CHANGE_ELEMENT_LIST(A_NETWORK_CHANGE_ELEMENT_LIST);
                toaStatus.setA_SUSPEND_REASON(A_SUSPEND_REASON);


                Timestamp timestamp = new Timestamp(new Date().getTime());
                toaStatus.setInsert_date(timestamp);

                toaStatusRepository.save(toaStatus);

                saveInventory(wo_suspend.getInstalled_inventory(), wo_suspend.getDeinstalled_inventory(),
                        wo_suspend.getCustomer_inventory(), wo_suspend.getResource_inventory(), toaStatus.getId(), messageId);

            }else{
                result = "activity_properties NULL";
            }

        }catch(Exception ex){
            String messageError = "MessageId " + messageId + " -> " + ex.toString() + " : " + ex.getMessage() + " \n " + xml;
            _log.error(messageError);
            result = messageError;
        }

        return result;
    }

    //@Async
    public CompletableFuture<String> processWO_CANCEL(WO_CANCEL wo_cancel, String messageId, String xml){
        String response = "";
        try{
            if(wo_cancel.getXa_peticion() == null || wo_cancel.getXa_peticion().trim().equals("")){
                if(wo_cancel.getAppt_number() != null && !wo_cancel.getAppt_number().trim().equals("")){
                    wo_cancel.setXa_peticion(wo_cancel.getAppt_number());
                }
            }

            /*boolean existsRequest = existsRequest(wo_cancel.getXa_peticion());
            if(!existsRequest){
                return CompletableFuture.completedFuture("Message_Id "+ messageId + " = " + " El código de pedido no existe.");
            }*/

            String processMessage = saveWO_CANCEL(wo_cancel,messageId,xml);
            if("".equals(processMessage)) response = "Estado registrado correctamente.";
            else{
                response = "Error al registrar el estado: " + processMessage;
                saveLog(messageId, response);
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveWO_CANCEL(WO_CANCEL wo_cancel, String messageId, String xml){
        String result = "";
        try{

            ToaStatusDB toaStatus = new ToaStatusDB();

            toaStatus.setStatus_toa("WO_CANCEL");
            toaStatus.setMessage_id(messageId);
            toaStatus.setPeticion(wo_cancel.getXa_peticion());

            toaStatus.setExternal_id(wo_cancel.getExternal_id());
            toaStatus.setXa_identificador_st(wo_cancel.getXa_identificador_st());
            toaStatus.setEta_start_time(wo_cancel.getEta_start_time());
            toaStatus.setEta_end_time(wo_cancel.getEta_end_time());
            toaStatus.setAstatus(wo_cancel.getAstatus());
            toaStatus.setAppt_number(wo_cancel.getAppt_number());
            toaStatus.setAid(wo_cancel.getAid());
            toaStatus.setXA_NUMBER_WORK_ORDER(wo_cancel.getXA_NUMBER_WORK_ORDER());
            toaStatus.setXA_NUMBER_SERVICE_ORDER(wo_cancel.getXA_NUMBER_SERVICE_ORDER());

            String A_COMMENT_TECHNICIAN = "";
            String XA_CANCEL_REASON = "";
            String A_OBSERVATION = "";

            if(wo_cancel.getActivity_Properties() != null && wo_cancel.getActivity_Properties().getProperty() != null){
                for(property property : wo_cancel.getActivity_Properties().getProperty()){
                    if("A_COMMENT_TECHNICIAN".equals(property.getLabel())){ A_COMMENT_TECHNICIAN = property.getValue(); continue; }
                    if("XA_CANCEL_REASON".equals(property.getLabel())){ XA_CANCEL_REASON = property.getValue(); continue; }
                    if("A_OBSERVATION".equals(property.getLabel())){ A_OBSERVATION = property.getValue(); continue; }
                }
            }

            toaStatus.setA_COMMENT_TECHNICIAN(A_COMMENT_TECHNICIAN);
            toaStatus.setXA_CANCEL_REASON(XA_CANCEL_REASON);
            toaStatus.setA_OBSERVATION(A_OBSERVATION);

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaStatus.setInsert_date(timestamp);

            toaStatusRepository.save(toaStatus);

        }catch(Exception ex){
            String messageError = "MessageId " + messageId + " -> " + ex.toString() + " : " + ex.getMessage() + " \n " + xml;
            _log.error(messageError);
            result = messageError;
        }

        return result;
    }

    //@Async
    public CompletableFuture<String> processINT_SMS(INT_SMS int_sms, String messageId, String xml){
        String response = "";
        try{
            //ENVÍO DE SMS!!!

            if(int_sms.getXa_peticion() == null || int_sms.getXa_peticion().trim().equals("")){
                if(int_sms.getAppt_number() != null && !int_sms.getAppt_number().trim().equals("")){
                    int_sms.setXa_peticion(int_sms.getAppt_number());
                }
            }

            String processMessage = saveINT_SMS(int_sms,messageId,xml);
            if("".equals(processMessage)) response = "Estado registrado correctamente.";
            else{
                response = "Error al registrar el estado: " + processMessage;
                saveLog(messageId, response);
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return CompletableFuture.completedFuture(response);
    }

    public String saveINT_SMS(INT_SMS int_sms, String messageId, String xml){
        String result = "";
        try{
            ToaSmsDB toaSms = new ToaSmsDB();

            toaSms.setStatus_toa("INT_SMS");
            toaSms.setMessage_id(messageId);
            toaSms.setPeticion(int_sms.getXa_peticion());

            toaSms.setExternal_id(int_sms.getExternal_id());
            toaSms.setXa_identificador_st(int_sms.getXa_identificador_st());
            toaSms.setAppt_number(int_sms.getAppt_number());
            toaSms.setAid(int_sms.getAid());
            toaSms.setNumber(int_sms.getNumber());
            toaSms.setNumber1(int_sms.getNumber1());
            toaSms.setNumber2(int_sms.getNumber2());
            toaSms.setNumber3(int_sms.getNumber3());
            toaSms.setText(int_sms.getText());
            toaSms.setXA_NUMBER_WORK_ORDER(int_sms.getXA_NUMBER_WORK_ORDER());
            toaSms.setXA_NUMBER_SERVICE_ORDER(int_sms.getXA_NUMBER_SERVICE_ORDER());

            Timestamp timestamp = new Timestamp(new Date().getTime());
            toaSms.setInsert_date(timestamp);

            toaSmsRepository.save(toaSms);

        }catch(Exception ex){
            String messageError = "MessageId " + messageId + " -> " + ex.toString() + " : " + ex.getMessage() + " \n " + xml;
            _log.error(messageError);
            result = messageError;
        }

        return result;
    }

    public String saveInventory(inventory_installed inventory_installed, inventory_deinstalled inventory_deinstalled,
                                inventory_customer inventory_customer, inventory_resource inventory_resource,
                                String toa_status_id, String messageId){
        String result = "";
        try{
            if(inventory_installed != null && inventory_installed.getBLOCK() != null && inventory_installed.getBLOCK().getInventory() != null){
                for(inventory inventory : inventory_installed.getBLOCK().getInventory()){
                    if(inventory != null){
                        ToaStatusInventoryDB inventoryDB = new ToaStatusInventoryDB();
                        inventoryDB.setToa_status_id(Integer.parseInt(toa_status_id));
                        inventoryDB.setType("installed_inventory");
                        inventoryDB.setInvtype(inventory.getInvtype());
                        inventoryDB.setInvsn(inventory.getInvsn());
                        inventoryDB.setQuantity(inventory.getQuantity());
                        inventoryDB.setXi_brand(inventory.getXi_brand());
                        inventoryDB.setXi_model(inventory.getXi_model());
                        inventoryDB.setXi_component(inventory.getXi_component());
                        inventoryDB.setXi_mac_address(inventory.getXi_mac_address());
                        inventoryDB.setI_num_activacion(inventory.getI_num_activacion());
                        toaStatusInventoryRepository.save(inventoryDB);
                    }
                }
            }
            if(inventory_deinstalled != null && inventory_deinstalled.getBLOCK() != null && inventory_deinstalled.getBLOCK().getInventory() != null){
                for(inventory inventory : inventory_deinstalled.getBLOCK().getInventory()){
                    if(inventory != null){
                        ToaStatusInventoryDB inventoryDB = new ToaStatusInventoryDB();
                        inventoryDB.setToa_status_id(Integer.parseInt(toa_status_id));
                        inventoryDB.setType("deinstalled_inventory");
                        inventoryDB.setInvtype(inventory.getInvtype());
                        inventoryDB.setInvsn(inventory.getInvsn());
                        inventoryDB.setQuantity(inventory.getQuantity());
                        inventoryDB.setXi_brand(inventory.getXi_brand());
                        inventoryDB.setXi_model(inventory.getXi_model());
                        inventoryDB.setXi_component(inventory.getXi_component());
                        inventoryDB.setXi_mac_address(inventory.getXi_mac_address());
                        inventoryDB.setI_num_activacion(inventory.getI_num_activacion());
                        toaStatusInventoryRepository.save(inventoryDB);
                    }
                }
            }
            if(inventory_customer != null && inventory_customer.getBLOCK() != null && inventory_customer.getBLOCK().getInventory() != null){
                for(inventory inventory : inventory_customer.getBLOCK().getInventory()){
                    if(inventory != null){
                        ToaStatusInventoryDB inventoryDB = new ToaStatusInventoryDB();
                        inventoryDB.setToa_status_id(Integer.parseInt(toa_status_id));
                        inventoryDB.setType("customer_inventory");
                        inventoryDB.setInvtype(inventory.getInvtype());
                        inventoryDB.setInvsn(inventory.getInvsn());
                        inventoryDB.setQuantity(inventory.getQuantity());
                        inventoryDB.setXi_brand(inventory.getXi_brand());
                        inventoryDB.setXi_model(inventory.getXi_model());
                        inventoryDB.setXi_component(inventory.getXi_component());
                        inventoryDB.setXi_mac_address(inventory.getXi_mac_address());
                        inventoryDB.setI_num_activacion(inventory.getI_num_activacion());
                        toaStatusInventoryRepository.save(inventoryDB);
                    }
                }
            }
            if(inventory_resource != null && inventory_resource.getBLOCK() != null && inventory_resource.getBLOCK().getInventory() != null){
                for(inventory inventory : inventory_resource.getBLOCK().getInventory()){
                    if(inventory != null){
                        ToaStatusInventoryDB inventoryDB = new ToaStatusInventoryDB();
                        inventoryDB.setToa_status_id(Integer.parseInt(toa_status_id));
                        inventoryDB.setType("resource_inventory");
                        inventoryDB.setInvtype(inventory.getInvtype());
                        inventoryDB.setInvsn(inventory.getInvsn());
                        inventoryDB.setQuantity(inventory.getQuantity());
                        inventoryDB.setXi_brand(inventory.getXi_brand());
                        inventoryDB.setXi_model(inventory.getXi_model());
                        inventoryDB.setXi_component(inventory.getXi_component());
                        inventoryDB.setXi_mac_address(inventory.getXi_mac_address());
                        inventoryDB.setI_num_activacion(inventory.getI_num_activacion());
                        toaStatusInventoryRepository.save(inventoryDB);
                    }
                }
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
            saveLog(messageId, "Inventory Error: " + ex.getMessage());
        }
        return result;
    }

    //@Async
    public ToaLogDB saveLog(String messageId, String messageText){
        try{
            ToaLogDB obj = new ToaLogDB();
            obj.setMessage_id(messageId);
            obj.setMessage_text(messageText);
            Timestamp timestamp = new Timestamp(new Date().getTime());
            obj.setDate(timestamp);
            toaLogRepository.save(obj);
            return obj;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            return new ToaLogDB();
        }
    }

    public String sendSMS(){
        String result = "";
        try{

        }catch(Exception ex){
            _log.error(ex.getMessage());
        }
        return result;
    }


}