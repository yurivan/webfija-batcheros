package pe.telefonica.tracer.business;

import pe.telefonica.tracer.model.AppUser;
import pe.telefonica.tracer.persistence.ParametersDB;

import java.util.List;

/**
 * Created by Felix on 19/06/2017.
 */
public interface ParametersService {

    public List<ParametersDB> getParameters();


}
