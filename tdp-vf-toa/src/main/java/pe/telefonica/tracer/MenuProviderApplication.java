package pe.telefonica.tracer;

import org.aspectj.lang.annotation.After;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import pe.telefonica.tracer.business.impl.ParametersServiceImpl;
import pe.telefonica.tracer.configuration.Database;
import pe.telefonica.tracer.configuration.DatabasePropertiesSource;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;


@EnableAutoConfiguration
@SpringBootApplication
//@EnableScheduling
@EnableAsync
public class MenuProviderApplication {
	private static final Logger _log = LoggerFactory.getLogger(MenuProviderApplication.class);

	public static void main(String[] args) {
		_log.info("Starting...");
		SpringApplication.run(MenuProviderApplication.class, args);
		_log.info("...Started");
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer properties() {
		final PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
		DatabasePropertiesSource propertiesSource = new DatabasePropertiesSource(Database.datasource());
		Map<String, Object> props = propertiesSource.loadProperties();
		System.out.print(props);
		pspc.setIgnoreResourceNotFound(true);
		pspc.setIgnoreUnresolvablePlaceholders(true);
		Properties propps = new Properties();
		propps.putAll(props);
		pspc.setProperties(propps);
		return pspc;
	}

	@Bean
	public Executor asyncExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(6);
		executor.setMaxPoolSize(15);
		executor.setQueueCapacity(500);
		executor.setThreadNamePrefix("TOA-Lookup");
		executor.initialize();
		return executor;
	}

	@Bean
	public CommonsRequestLoggingFilter requestLoggingFilter() {
		CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
		loggingFilter.setIncludeClientInfo(false);
		loggingFilter.setIncludeQueryString(false);
		loggingFilter.setIncludePayload(true);
		loggingFilter.setIncludeHeaders(false);
		loggingFilter.setMaxPayloadLength(500);
		return loggingFilter;
	}

}
