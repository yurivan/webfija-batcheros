package pe.telefonica.tracer.model.toa;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="INT_SMS")
public class INT_SMS {

    private String xa_peticion;
    private String external_id;
    private String xa_identificador_st;
    private String appt_number;
    private String aid;
    private String number;
    private String text;
    private String XA_NUMBER_WORK_ORDER;
    private String XA_NUMBER_SERVICE_ORDER;
    private String number1;
    private String number2;
    private String number3;

    public String getXa_peticion() {
        return xa_peticion;
    }

    public void setXa_peticion(String xa_peticion) {
        this.xa_peticion = xa_peticion;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getXa_identificador_st() {
        return xa_identificador_st;
    }

    public void setXa_identificador_st(String xa_identificador_st) {
        this.xa_identificador_st = xa_identificador_st;
    }

    public String getAppt_number() {
        return appt_number;
    }

    public void setAppt_number(String appt_number) {
        this.appt_number = appt_number;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getXA_NUMBER_WORK_ORDER() {
        return XA_NUMBER_WORK_ORDER;
    }

    public void setXA_NUMBER_WORK_ORDER(String XA_NUMBER_WORK_ORDER) {
        this.XA_NUMBER_WORK_ORDER = XA_NUMBER_WORK_ORDER;
    }

    public String getXA_NUMBER_SERVICE_ORDER() {
        return XA_NUMBER_SERVICE_ORDER;
    }

    public void setXA_NUMBER_SERVICE_ORDER(String XA_NUMBER_SERVICE_ORDER) {
        this.XA_NUMBER_SERVICE_ORDER = XA_NUMBER_SERVICE_ORDER;
    }

    public String getNumber1() {
        return number1;
    }

    public void setNumber1(String number1) {
        this.number1 = number1;
    }

    public String getNumber2() {
        return number2;
    }

    public void setNumber2(String number2) {
        this.number2 = number2;
    }

    public String getNumber3() {
        return number3;
    }

    public void setNumber3(String number3) {
        this.number3 = number3;
    }
}
