package pe.telefonica.tracer.model.toa;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="IN_TOA")
public class IN_TOA {

    private String external_id;
    private String xa_peticion;
    private String xa_creation_date;
    private String xa_identificador_st;
    private String xa_requirement_number;
    private String appt_number;
    private String XA_NUMBER_SERVICE_ORDER;

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getXa_peticion() {
        return xa_peticion;
    }

    public void setXa_peticion(String xa_peticion) {
        this.xa_peticion = xa_peticion;
    }

    public String getXa_creation_date() {
        return xa_creation_date;
    }

    public void setXa_creation_date(String xa_creation_date) {
        this.xa_creation_date = xa_creation_date;
    }

    public String getXa_identificador_st() {
        return xa_identificador_st;
    }

    public void setXa_identificador_st(String xa_identificador_st) {
        this.xa_identificador_st = xa_identificador_st;
    }

    public String getXa_requirement_number() {
        return xa_requirement_number;
    }

    public void setXa_requirement_number(String xa_requirement_number) {
        this.xa_requirement_number = xa_requirement_number;
    }

    public String getAppt_number() {
        return appt_number;
    }

    public void setAppt_number(String appt_number) {
        this.appt_number = appt_number;
    }

    public String getXA_NUMBER_SERVICE_ORDER() {
        return XA_NUMBER_SERVICE_ORDER;
    }

    public void setXA_NUMBER_SERVICE_ORDER(String XA_NUMBER_SERVICE_ORDER) {
        this.XA_NUMBER_SERVICE_ORDER = XA_NUMBER_SERVICE_ORDER;
    }
}
