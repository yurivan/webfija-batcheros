package pe.telefonica.tracer.model;

public class StatusToaRequest {

	private String accion;
	private String requerimiento;
	private String codigo_pedido;
	private String orden_trabajo;
	private String fecha_envio;
	private String hora_envio;
	private String fecha_envio_toa;
	private String hora_envio_toa;
	private String fecha_agenda;
	private String franja_horaria;
	private String fecha_inicio_sla;
	private String fecha_fin_sla;
	private String fecha_inicio;
	private String fecha_cierre;
	private String motivo_accion;
	private String celular_cliente;
	private String mensaje_cliente;

	//private String token;

	public String getAccion() {
		return accion;
	}

	public void setAccion(String accion) {
		this.accion = accion;
	}

	public String getRequerimiento() {
		return requerimiento;
	}

	public void setRequerimiento(String requerimiento) {
		this.requerimiento = requerimiento;
	}

	public String getCodigo_pedido() {
		return codigo_pedido;
	}

	public void setCodigo_pedido(String codigo_pedido) {
		this.codigo_pedido = codigo_pedido;
	}

	public String getOrden_trabajo() {
		return orden_trabajo;
	}

	public void setOrden_trabajo(String orden_trabajo) {
		this.orden_trabajo = orden_trabajo;
	}

	public String getFecha_envio() {
		return fecha_envio;
	}

	public void setFecha_envio(String fecha_envio) {
		this.fecha_envio = fecha_envio;
	}

	public String getHora_envio() {
		return hora_envio;
	}

	public void setHora_envio(String hora_envio) {
		this.hora_envio = hora_envio;
	}

	public String getFecha_envio_toa() {
		return fecha_envio_toa;
	}

	public void setFecha_envio_toa(String fecha_envio_toa) {
		this.fecha_envio_toa = fecha_envio_toa;
	}

	public String getHora_envio_toa() {
		return hora_envio_toa;
	}

	public void setHora_envio_toa(String hora_envio_toa) {
		this.hora_envio_toa = hora_envio_toa;
	}

	public String getFecha_agenda() {
		return fecha_agenda;
	}

	public void setFecha_agenda(String fecha_agenda) {
		this.fecha_agenda = fecha_agenda;
	}

	public String getFranja_horaria() {
		return franja_horaria;
	}

	public void setFranja_horaria(String franja_horaria) {
		this.franja_horaria = franja_horaria;
	}

	public String getFecha_inicio_sla() {
		return fecha_inicio_sla;
	}

	public void setFecha_inicio_sla(String fecha_inicio_sla) {
		this.fecha_inicio_sla = fecha_inicio_sla;
	}

	public String getFecha_fin_sla() {
		return fecha_fin_sla;
	}

	public void setFecha_fin_sla(String fecha_fin_sla) {
		this.fecha_fin_sla = fecha_fin_sla;
	}

	public String getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public String getFecha_cierre() {
		return fecha_cierre;
	}

	public void setFecha_cierre(String fecha_cierre) {
		this.fecha_cierre = fecha_cierre;
	}

	public String getMotivo_accion() {
		return motivo_accion;
	}

	public void setMotivo_accion(String motivo_accion) {
		this.motivo_accion = motivo_accion;
	}

	public String getCelular_cliente() {
		return celular_cliente;
	}

	public void setCelular_cliente(String celular_cliente) {
		this.celular_cliente = celular_cliente;
	}

	public String getMensaje_cliente() {
		return mensaje_cliente;
	}

	public void setMensaje_cliente(String mensaje_cliente) {
		this.mensaje_cliente = mensaje_cliente;
	}

	/*public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}*/
}
