package pe.telefonica.tracer.model.toa;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="WO_PRE_COMPLETED")
public class WO_PRE_COMPLETED {

    private String xa_peticion;
    private String external_id;
    private String xa_identificador_st;
    private String eta_start_time;
    private String eta_end_time;
    private String astatus;
    private String appt_number;
    private String XA_NUMBER_WORK_ORDER;
    private String XA_NUMBER_SERVICE_ORDER;

    //@XmlElement(name = "activity_properties", required = true)
    private activity_properties activity_properties;

    public String getXa_peticion() {
        return xa_peticion;
    }

    public void setXa_peticion(String xa_peticion) {
        this.xa_peticion = xa_peticion;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getXa_identificador_st() {
        return xa_identificador_st;
    }

    public void setXa_identificador_st(String xa_identificador_st) {
        this.xa_identificador_st = xa_identificador_st;
    }

    public String getEta_start_time() {
        return eta_start_time;
    }

    public void setEta_start_time(String eta_start_time) {
        this.eta_start_time = eta_start_time;
    }

    public String getEta_end_time() {
        return eta_end_time;
    }

    public void setEta_end_time(String eta_end_time) {
        this.eta_end_time = eta_end_time;
    }

    public String getAstatus() {
        return astatus;
    }

    public void setAstatus(String astatus) {
        this.astatus = astatus;
    }

    public String getAppt_number() {
        return appt_number;
    }

    public void setAppt_number(String appt_number) {
        this.appt_number = appt_number;
    }

    public String getXA_NUMBER_WORK_ORDER() {
        return XA_NUMBER_WORK_ORDER;
    }

    public void setXA_NUMBER_WORK_ORDER(String XA_NUMBER_WORK_ORDER) {
        this.XA_NUMBER_WORK_ORDER = XA_NUMBER_WORK_ORDER;
    }

    public String getXA_NUMBER_SERVICE_ORDER() {
        return XA_NUMBER_SERVICE_ORDER;
    }

    public void setXA_NUMBER_SERVICE_ORDER(String XA_NUMBER_SERVICE_ORDER) {
        this.XA_NUMBER_SERVICE_ORDER = XA_NUMBER_SERVICE_ORDER;
    }

    public activity_properties getActivity_properties() {
        return activity_properties;
    }

    public void setActivity_properties(activity_properties activity_properties) {
        this.activity_properties = activity_properties;
    }
}
