package pe.telefonica.tracer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Felix on 19/06/2017.
 */
public class CustomRequest<T> {

    private T request;

    public T getRequest() {
        return request;
    }

    public void setRequest(T request) {
        request = request;
    }
}