package pe.telefonica.tracer.model.toa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "activity_properties", propOrder = {
        "property",
        "xa_peticion",
        "external_id",
        "xa_identificador_st",
        "ETA",
        "eta_end_time",
        "astatus",
        "appt_number",
        "XA_NUMBER_WORK_ORDER",
        "XA_NUMBER_SERVICE_ORDER"
})
public class activity_properties {

    @XmlElement(name = "property", required = true)
    private List<property> property;

    private String xa_peticion;
    private String external_id;
    private String xa_identificador_st;
    private String ETA;
    private String eta_end_time;
    private String astatus;
    private String appt_number;
    private String XA_NUMBER_WORK_ORDER;
    private String XA_NUMBER_SERVICE_ORDER;

    public List<pe.telefonica.tracer.model.toa.property> getProperty() {
        return property;
    }

    public void setProperty(List<pe.telefonica.tracer.model.toa.property> property) {
        this.property = property;
    }

    public String getXa_peticion() {
        return xa_peticion;
    }

    public void setXa_peticion(String xa_peticion) {
        this.xa_peticion = xa_peticion;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getXa_identificador_st() {
        return xa_identificador_st;
    }

    public void setXa_identificador_st(String xa_identificador_st) {
        this.xa_identificador_st = xa_identificador_st;
    }

    public String getETA() {
        return ETA;
    }

    public void setETA(String ETA) {
        this.ETA = ETA;
    }

    public String getEta_end_time() {
        return eta_end_time;
    }

    public void setEta_end_time(String eta_end_time) {
        this.eta_end_time = eta_end_time;
    }

    public String getAstatus() {
        return astatus;
    }

    public void setAstatus(String astatus) {
        this.astatus = astatus;
    }

    public String getAppt_number() {
        return appt_number;
    }

    public void setAppt_number(String appt_number) {
        this.appt_number = appt_number;
    }

    public String getXA_NUMBER_WORK_ORDER() {
        return XA_NUMBER_WORK_ORDER;
    }

    public void setXA_NUMBER_WORK_ORDER(String XA_NUMBER_WORK_ORDER) {
        this.XA_NUMBER_WORK_ORDER = XA_NUMBER_WORK_ORDER;
    }

    public String getXA_NUMBER_SERVICE_ORDER() {
        return XA_NUMBER_SERVICE_ORDER;
    }

    public void setXA_NUMBER_SERVICE_ORDER(String XA_NUMBER_SERVICE_ORDER) {
        this.XA_NUMBER_SERVICE_ORDER = XA_NUMBER_SERVICE_ORDER;
    }
}
