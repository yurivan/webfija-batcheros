package pe.telefonica.tracer.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class VisorRequest {

	private String document;
	private String requestCode;
	private String token;

	public void setRequestCode(String requestCode){
		this.requestCode = requestCode;
	}

	public String getRequestCode(){
		return requestCode;
	}

	public void setDocument(String document){
		this.document = document;
	}

	public String getDocument(){
		return document;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

}
