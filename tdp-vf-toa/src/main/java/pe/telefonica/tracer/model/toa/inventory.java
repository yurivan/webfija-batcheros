package pe.telefonica.tracer.model.toa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "inventory", propOrder = {
        "invtype",
        "invsn",
        "quantity",
        "xi_brand",
        "xi_model",
        "xi_component",
        "xi_mac_address",
        "i_num_activacion"
})
public class inventory {

    @XmlElement(name = "invtype", required = true)
    private String invtype;

    @XmlElement(name = "invsn", required = true)
    private String invsn;

    @XmlElement(name = "quantity", required = true)
    private String quantity;

    @XmlElement(name = "xi_brand", required = true)
    private String xi_brand;

    @XmlElement(name = "xi_model", required = true)
    private String xi_model;

    @XmlElement(name = "xi_component", required = true)
    private String xi_component;

    @XmlElement(name = "xi_mac_address", required = true)
    private String xi_mac_address;

    @XmlElement(name = "i_num_activacion", required = true)
    private String i_num_activacion;

    public String getInvtype() {
        return invtype;
    }

    public void setInvtype(String invtype) {
        this.invtype = invtype;
    }

    public String getInvsn() {
        return invsn;
    }

    public void setInvsn(String invsn) {
        this.invsn = invsn;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getXi_brand() {
        return xi_brand;
    }

    public void setXi_brand(String xi_brand) {
        this.xi_brand = xi_brand;
    }

    public String getXi_model() {
        return xi_model;
    }

    public void setXi_model(String xi_model) {
        this.xi_model = xi_model;
    }

    public String getXi_component() {
        return xi_component;
    }

    public void setXi_component(String xi_component) {
        this.xi_component = xi_component;
    }

    public String getXi_mac_address() {
        return xi_mac_address;
    }

    public void setXi_mac_address(String xi_mac_address) {
        this.xi_mac_address = xi_mac_address;
    }

    public String getI_num_activacion() {
        return i_num_activacion;
    }

    public void setI_num_activacion(String i_num_activacion) {
        this.i_num_activacion = i_num_activacion;
    }
}
