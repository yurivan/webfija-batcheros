package pe.telefonica.tracer.model;

public class VisorByDocResponse {

    private String codigo_pedido;

    public VisorByDocResponse(){

    }

    public void setCodigo_pedido(String codigo_pedido){
        this.codigo_pedido = codigo_pedido;
    }

    public String getCodigo_pedido(){
        return codigo_pedido;
    }

}
