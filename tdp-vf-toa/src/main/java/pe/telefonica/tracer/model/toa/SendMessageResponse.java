package pe.telefonica.tracer.model.toa;

import pe.telefonica.tracer.model.toa.MessageResponse;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "messageResponse"
})
@XmlRootElement(name = "send_message_response")
public class SendMessageResponse {

    @XmlElement(name = "message_response")
    protected List<MessageResponse> messageResponse;

    /**
     * Gets the value of the messageResponse property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messageResponse property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessageResponse().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MessageResponse }
     *
     *
     */
    public List<MessageResponse> getMessageResponse() {
        if (messageResponse == null) {
            messageResponse = new ArrayList<MessageResponse>();
        }
        return this.messageResponse;
    }

    public void setMessageResponse(List<MessageResponse> messageResponseList) {
        if (messageResponseList == null) {
            messageResponseList = new ArrayList<MessageResponse>();
        }
        this.messageResponse = messageResponseList;
    }

}
