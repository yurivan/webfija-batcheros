package pe.telefonica.tracer.model.toa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customer_inventory", propOrder = {
        "BLOCK"
})
public class inventory_customer {

    @XmlElement(name = "BLOCK", required = true)
    private BLOCK BLOCK;

    public BLOCK getBLOCK() {
        return BLOCK;
    }

    public void setBLOCK(BLOCK BLOCK) {
        this.BLOCK = BLOCK;
    }
}
