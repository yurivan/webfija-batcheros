package pe.telefonica.tracer.model;

public class PublicCustomerRequest {

	private String documento;
	private String telefono3;
	private String telefono4;
	private String direccion2;
	private String correo2;
	private String rangoFechaInicio;
	private String rangoFechaFin;
	private String respuesta1;
	private String respuesta2;
	private String respuesta3;
	private String respuesta4;
	private String valoracion;

	private String token;


	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getTelefono3() {
		return telefono3;
	}

	public void setTelefono3(String telefono3) {
		this.telefono3 = telefono3;
	}

	public String getTelefono4() {
		return telefono4;
	}

	public void setTelefono4(String telefono4) {
		this.telefono4 = telefono4;
	}

	public String getDireccion2() {
		return direccion2;
	}

	public void setDireccion2(String direccion2) {
		this.direccion2 = direccion2;
	}

	public String getCorreo2() {
		return correo2;
	}

	public void setCorreo2(String correo2) {
		this.correo2 = correo2;
	}

	public String getRangoFechaInicio() {
		return rangoFechaInicio;
	}

	public void setRangoFechaInicio(String rangoFechaInicio) {
		this.rangoFechaInicio = rangoFechaInicio;
	}

	public String getRangoFechaFin() {
		return rangoFechaFin;
	}

	public void setRangoFechaFin(String rangoFechaFin) {
		this.rangoFechaFin = rangoFechaFin;
	}

	public String getRespuesta1() {
		return respuesta1;
	}

	public void setRespuesta1(String respuesta1) {
		this.respuesta1 = respuesta1;
	}

	public String getRespuesta2() {
		return respuesta2;
	}

	public void setRespuesta2(String respuesta2) {
		this.respuesta2 = respuesta2;
	}

	public String getRespuesta3() {
		return respuesta3;
	}

	public void setRespuesta3(String respuesta3) {
		this.respuesta3 = respuesta3;
	}

	public String getRespuesta4() {
		return respuesta4;
	}

	public void setRespuesta4(String respuesta4) {
		this.respuesta4 = respuesta4;
	}

	public String getValoracion() {
		return valoracion;
	}

	public void setValoracion(String valoracion) {
		this.valoracion = valoracion;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
