package pe.telefonica.tracer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Javier on 19/06/2017.
 */
@JsonPropertyOrder({ "BodyIn" })
public class ApiRequest<E> {

    private E BodyIn;

    @JsonProperty("BodyIn")
    public E getBodyIn() {
        return BodyIn;
    }

    public void setBodyIn(E bodyIn) {
        BodyIn = bodyIn;
    }
}