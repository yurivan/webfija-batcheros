package pe.telefonica.tracer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Felix on 19/06/2017.
 */
@JsonPropertyOrder({ "User", "Pass", "Token" })
public class AuthenticateRequestBody {
    private String User;
    private String Pass;
    private String Token;

    @JsonProperty("User")
    public String getUser() {
        return this.User;
    }

    public void setUser(String User) {
        this.User = User;
    }

    @JsonProperty("Pass")
    public String getPass() {
        return this.Pass;
    }

    public void setPass(String Pass) {
        this.Pass = Pass;
    }

    @JsonProperty("Token")
    public String getToken() {
        return this.Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }


}
