package pe.telefonica.tracer.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Felix on 19/06/2017.
 */
@JsonPropertyOrder({"BodyOut"})
public class ApiResponse<E> {
    @JsonProperty("BodyOut")
    private E BodyOut;

    public E getBodyOut() {
        return BodyOut;
    }

    public void setBodyOut(E bodyOut) {
        BodyOut = bodyOut;
    }
}
