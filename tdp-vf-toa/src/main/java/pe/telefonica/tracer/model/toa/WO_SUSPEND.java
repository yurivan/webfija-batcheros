package pe.telefonica.tracer.model.toa;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="WO_SUSPEND")
public class WO_SUSPEND {

    @XmlElement(name = "activity_properties", required = true)
    private activity_properties activity_properties;

    private inventory_installed installed_inventory;
    private inventory_deinstalled deinstalled_inventory;
    private inventory_customer customer_inventory;
    private inventory_resource resource_inventory;

    public activity_properties getActivity_Properties() {
        return activity_properties;
    }

    public void setActivity_Properties(activity_properties activity_properties) {
        this.activity_properties = activity_properties;
    }

    public inventory_installed getInstalled_inventory() {
        return installed_inventory;
    }

    public void setInstalled_inventory(inventory_installed installed_inventory) {
        this.installed_inventory = installed_inventory;
    }

    public inventory_deinstalled getDeinstalled_inventory() {
        return deinstalled_inventory;
    }

    public void setDeinstalled_inventory(inventory_deinstalled deinstalled_inventory) {
        this.deinstalled_inventory = deinstalled_inventory;
    }

    public inventory_customer getCustomer_inventory() {
        return customer_inventory;
    }

    public void setCustomer_inventory(inventory_customer customer_inventory) {
        this.customer_inventory = customer_inventory;
    }

    public inventory_resource getResource_inventory() {
        return resource_inventory;
    }

    public void setResource_inventory(inventory_resource resource_inventory) {
        this.resource_inventory = resource_inventory;
    }

}
