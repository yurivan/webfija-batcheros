package pe.telefonica.tracer.model.toa;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BLOCK", propOrder = {
        "inventory"
})
public class BLOCK {

    @XmlElement(name = "inventory", required = true)
    private List<inventory> inventory;

    public List<inventory> getInventory() {
        return inventory;
    }

    public void setInventory(List<inventory> inventory) {
        this.inventory = inventory;
    }
}
