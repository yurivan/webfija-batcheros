package pe.telefonica.tracer.model.toa;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="NOT_LEG")
public class NOT_LEG {

    private String xa_peticion;
    private String xa_identificador_st;
    private String delivery_window_start;
    private String delivery_window_end;
    private String date;
    private String usuario;
    private String time_slot;
    private String end_time;
    private String start_time;
    private String astatus;
    private String time_of_assignment;
    private String time_of_booking;
    private String duration;
    private String external_id;
    private String appt_number;
    private String aid;
    private String pname;
    private String XA_NUMBER_WORK_ORDER;
    private String XA_NUMBER_SERVICE_ORDER;

    public String getXa_peticion() {
        return xa_peticion;
    }

    public void setXa_peticion(String xa_peticion) {
        this.xa_peticion = xa_peticion;
    }

    public String getXa_identificador_st() {
        return xa_identificador_st;
    }

    public void setXa_identificador_st(String xa_identificador_st) {
        this.xa_identificador_st = xa_identificador_st;
    }

    public String getDelivery_window_start() {
        return delivery_window_start;
    }

    public void setDelivery_window_start(String delivery_window_start) {
        this.delivery_window_start = delivery_window_start;
    }

    public String getDelivery_window_end() {
        return delivery_window_end;
    }

    public void setDelivery_window_end(String delivery_window_end) {
        this.delivery_window_end = delivery_window_end;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTime_slot() {
        return time_slot;
    }

    public void setTime_slot(String time_slot) {
        this.time_slot = time_slot;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getAstatus() {
        return astatus;
    }

    public void setAstatus(String astatus) {
        this.astatus = astatus;
    }

    public String getTime_of_assignment() {
        return time_of_assignment;
    }

    public void setTime_of_assignment(String time_of_assignment) {
        this.time_of_assignment = time_of_assignment;
    }

    public String getTime_of_booking() {
        return time_of_booking;
    }

    public void setTime_of_booking(String time_of_booking) {
        this.time_of_booking = time_of_booking;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public String getAppt_number() {
        return appt_number;
    }

    public void setAppt_number(String appt_number) {
        this.appt_number = appt_number;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getXA_NUMBER_WORK_ORDER() {
        return XA_NUMBER_WORK_ORDER;
    }

    public void setXA_NUMBER_WORK_ORDER(String XA_NUMBER_WORK_ORDER) {
        this.XA_NUMBER_WORK_ORDER = XA_NUMBER_WORK_ORDER;
    }

    public String getXA_NUMBER_SERVICE_ORDER() {
        return XA_NUMBER_SERVICE_ORDER;
    }

    public void setXA_NUMBER_SERVICE_ORDER(String XA_NUMBER_SERVICE_ORDER) {
        this.XA_NUMBER_SERVICE_ORDER = XA_NUMBER_SERVICE_ORDER;
    }
}
