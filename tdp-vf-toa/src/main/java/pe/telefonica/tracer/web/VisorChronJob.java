package pe.telefonica.tracer.web;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;

import org.springframework.stereotype.Component;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.business.impl.EmailServiceImpl;
import pe.telefonica.tracer.configuration.Constants;
import pe.telefonica.tracer.model.*;
import pe.telefonica.tracer.persistence.VisorDB;
import pe.telefonica.tracer.persistence.repository.VisorRepository;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class VisorChronJob {

    private static final Logger _log = LoggerFactory.getLogger(VisorChronJob.class);

    @Autowired
    VisorRepository repository;

    @Autowired
    Constants constants;

    String emailToken = "";

    List<String> emailsToSend;

    //@Autowired
    //Constants constants;

    //@Scheduled(cron = "*/30 * * * * *")
    public void executeCheck()  throws Exception{
       _log.info("Executing...");
       //Date now = new Date();
        /*Calendar old = Calendar.getInstance();
        old.add(Calendar.DAY_OF_MONTH, -3);


       Timestamp start = new Timestamp(old.getTime().getTime());
        Timestamp end = new Timestamp(new Date().getTime());
       long count = repository.getRangeRows(start, end).size();
        _log.info("start: " + start);
        _log.info("end: " + end);
       _log.info("count: " + count);*/

        //constants = new Constants();

        emailsToSend = getReceivers();

        getSolicitado();
        getRegistrado();


    }

    private void getSolicitado() throws Exception{
        String emailHTML = "";
        try {
            String path = "/static/template/template_solicitado.html";
            //File file = new File(getClass().getResource(path).getFile());

            File file = new File(new URI(getClass().getResource(path).toString()));

            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (IOException e) {
            //e.printStackTrace();
            _log.error(e.getMessage());
            throw new Exception(e.getMessage());
        }

        //example();

        List<Object[]> objList = repository.getSolicitadoAM();
        List<Object[]> objList2 = repository.getSolicitadoS();

        List<VisorDB> items = toEntityList(objList);
        List<VisorDB> items2 = toEntityList(objList2);

        //List<VisorDB> items = repository.getSolicitadoAM(new PageRequest(0, 3));

        //List<Object[]> items = repository.getSolicitadoAM(new PageRequest(0, 3));

        //List<VisorDB> items2 = repository.getSolicitadoS();

        items.addAll(items2);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String phone1 = (obj.getPhone1() != null && !obj.getPhone1().equals("")) ? obj.getPhone1() : "No ingresado";
            String phone2 = (obj.getPhone2() != null && !obj.getPhone2().equals("")) ? obj.getPhone2() : "No ingresado";

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%productName%:", nombreProducto);
            customEmailHTML = customEmailHTML.replace(":%clientPhone1%:", phone1);
            customEmailHTML = customEmailHTML.replace(":%clientPhone2%:", phone2);

            //String email = obj.getEmail();

            for(String email : emailsToSend){

                EmailResponseBody sendExpressResult = sendMailExpress(email, customEmailHTML, constants.getSubjectSolicitado(),constants.getEmailSendExpressUri());

                _log.info("Result ->");
                _log.info("Email: " + email);
                _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;

            }

            String id = String.format("'%s'", obj.getIdVisor());
            if (counter == 0) {
                ids = id;
            } else {
                ids += "," + id;
            }

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Solicitado): " + counterMail);


        /*Integer rowAffected = repository.setMailRequestedAsDone(ids);
        _log.info("rowAffected: " + rowAffected);*/

        Integer rowAffected = setMailRequestedAsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private void getRegistrado(){
        String emailHTML = "";
        try {
            String path = "/static/template/template_registrado.html";
            File file = new File(getClass().getResource(path).getFile());
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getRegistradoAM();
        List<Object[]> objList2 = repository.getRegistradoS();

        List<VisorDB> items = toEntityList(objList);
        List<VisorDB> items2 = toEntityList(objList2);

        items.addAll(items2);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String phone1 = (obj.getPhone1() != null && !obj.getPhone1().equals("")) ? obj.getPhone1() : "No ingresado";
            String phone2 = (obj.getPhone2() != null && !obj.getPhone2().equals("")) ? obj.getPhone2() : "No ingresado";

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%productName%:", nombreProducto);
            customEmailHTML = customEmailHTML.replace(":%clientPhone1%:", phone1);
            customEmailHTML = customEmailHTML.replace(":%clientPhone2%:", phone2);

            //String email = obj.getEmail();

            for(String email : emailsToSend){

                EmailResponseBody sendExpressResult = sendMailExpress(email, customEmailHTML, constants.getSubjectRegistrado(),constants.getEmailSendExpressUri());

                _log.info("Result ->");
                _log.info("Email: " + email);
                _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;

            }

            String id = String.format("'%s'", obj.getIdVisor());
            if (counter == 0) {
                ids = id;
            } else {
                ids += "," + id;
            }

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Registrado): " + counterMail);

        Integer rowAffected = setMailRegisterAsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private String generateSVAtext(VisorDB obj){
        String nombreProducto = "<br> Servicio(s) adicional(es) <br>";
        String[] splitted = obj.getNombre_producto().split("\\|");
        for(String element : splitted){
            if(element != null && !element.trim().equals("")) nombreProducto = nombreProducto + element.trim() + "<br>";
        }
        return nombreProducto;
    }

    private List<VisorDB> toEntityList(List<Object[]> objList){
        List<VisorDB> list = new ArrayList<>();
        for(Object[] obj : objList){
            String idVisor = (obj[0] != null) ? obj[0].toString() : "";
            String cliente = (obj[1] != null) ? obj[1].toString() : "";
            String email = (obj[2] != null) ? obj[2].toString() : "";
            String nombreProducto = (obj[3] != null) ? obj[3].toString() : "";
            String phone1 = (obj[4] != null) ? obj[4].toString() : "";
            String phone2 = (obj[5] != null) ? obj[5].toString() : "";
            String estadoSolicitud = (obj[6] != null) ? obj[6].toString() : "";
            String operacionComercial = (obj[7] != null) ? obj[7].toString() : "";
            VisorDB visorItem = new VisorDB(idVisor,cliente,email,nombreProducto,phone1,phone2,estadoSolicitud,operacionComercial);
            list.add(visorItem);
        }
        return list;
    }

    private Integer setMailRequestedAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_solicitado_email(1);
                item.setFlg_registrado_email(0);
                item.setFlg_equipo_tecnico_email(0);
                item.setFlg_instalado_email(0);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailRegisterAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_solicitado_email(1);
                item.setFlg_registrado_email(1);
                item.setFlg_equipo_tecnico_email(0);
                item.setFlg_instalado_email(0);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }


    private List<String> getReceivers(){
        String[] splitted = constants.getReceivers().split("\\|");
        //String[] splitted = "alvin.puma@smart-home.com.pe|diana.ch.n.26@gmail.com".split("\\|");
        List<String> emailsToSend = new ArrayList<>();
        for(int i = 0; i < splitted.length; i++){
            emailsToSend.add(splitted[i]);
        }
        return emailsToSend;
    }

    private String getToken(){
        if(emailToken.equals("")) emailToken = authenticate();

        Boolean result = checkConection(emailToken);
        if(!result) emailToken = authenticate();
        return emailToken;
    }

    private String authenticate(){
        AuthenticateRequestBody authenticateRequest = new AuthenticateRequestBody();
        authenticateRequest.setUser(constants.getEmailUser());
        authenticateRequest.setPass(constants.getEmailPassword());
        authenticateRequest.setToken(constants.getEmailToken());

        EmailServiceImpl<AuthenticateRequestBody, EmailResponseBody> emailAuthenticate = new EmailServiceImpl<>(constants.getEmailAutenticateUri());
        ClientResult<EmailResponseBody> authenticateResponse = emailAuthenticate.sendRequest(authenticateRequest);

        String tokenToSend = authenticateResponse.getResult().getToken();
        return tokenToSend;
    }

    private Boolean checkConection(String tokenToSend){
        CheckConexionRequestBody checkConexionRequest = new CheckConexionRequestBody();
        checkConexionRequest.setToken(tokenToSend);

        EmailServiceImpl<CheckConexionRequestBody, EmailResponseBody> emailCheckConexion = new EmailServiceImpl<>(constants.getEmailCheckConexionUri());
        ClientResult<EmailResponseBody> checkConexionResponse = emailCheckConexion.sendRequest(checkConexionRequest);

        EmailResponseBody checkConexionResult = checkConexionResponse.getResult();
        return (checkConexionResult != null) ? checkConexionResult.getResult() : false;
    }

    private EmailResponseBody sendMailExpress(String email, String message, String subject, String uri){
        emailToken = getToken();

        EmailRequestBody request = new EmailRequestBody();
        request.setEmail(email);
        request.setActionId(constants.getEmailSendExpressActionId());
        request.setMessage(message);
        request.setToken(emailToken);
        request.setSubject(subject);

        EmailServiceImpl<EmailRequestBody, EmailResponseBody> emailSendExpress = new EmailServiceImpl<>(uri);
        ClientResult<EmailResponseBody> sendExpressResponse = emailSendExpress.sendRequest(request);
        return sendExpressResponse.getResult();
    }

}
