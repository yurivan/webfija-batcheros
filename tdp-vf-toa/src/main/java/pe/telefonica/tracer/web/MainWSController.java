package pe.telefonica.tracer.web;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.telefonica.tracer.model.VisorRequest;


@RestController
@RequestMapping("/")
public class MainWSController {

    private static final Logger _log = LoggerFactory.getLogger(MainWSController.class);

    @RequestMapping(value="/",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity index(@RequestBody VisorRequest request) {
        _log.info("start index");
        return ResponseEntity.ok("");
    }

}
