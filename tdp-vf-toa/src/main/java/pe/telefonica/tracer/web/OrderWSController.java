package pe.telefonica.tracer.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.*;
import pe.telefonica.tracer.business.impl.PublicCustomerServiceImpl;
import pe.telefonica.tracer.business.impl.StatusToaServiceImpl;
import pe.telefonica.tracer.business.impl.TraceabilityAccessServiceImpl;
import pe.telefonica.tracer.business.impl.VisorServiceImpl;
import pe.telefonica.tracer.model.*;
import pe.telefonica.tracer.persistence.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/api/v1/visor")
public class OrderWSController {

    private static final Logger _log = LoggerFactory.getLogger(OrderWSController.class);

    @Autowired
    private VisorServiceImpl visorService;

    @Autowired
    private StatusToaServiceImpl statusToaService;

    @Autowired
    private PublicCustomerServiceImpl publicCustomerService;

    @Autowired
    private TraceabilityAccessServiceImpl traceabilityAccessService;

    @RequestMapping(value="/orderbydocument",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity getOrderByDocument(@RequestBody VisorRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start getOrderByDocument");
        Response<List<VisorByDocResponse>> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){
                    if(request.getDocument() == null || "".equals(request.getDocument().trim())){
                        response = setMessageError("El DNI ingresado es incorrecto.", access.getId());
                    }else{
                        List<VisorByDocDB> items = visorService.getVisorByDoc(request.getDocument());

                        if(items != null && items.size() > 0){

                            List<VisorByDocResponse> result = visorService.toVisorByDocResponseList(items);

                            response.setResponseData(result);
                            response.setResponseMessage("Success");
                            response.setResponseCode("1");
                            traceabilityAccessService.saveLog("Success!", access.getId());
                        }else{
                            response = setMessageError("El DNI ingresado no cuenta con pedidos.", access.getId());
                        }
                    }
                }else{
                    //response = setMessageError("Token de acceso inválido: " + token, null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(), null);
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value="/orderbyrequestcode",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity getOrderById(@RequestBody VisorRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start getOrderById");
        Response<VisorByRequestCodeDB> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){
                    if(request.getRequestCode() == null || "".equals(request.getRequestCode().trim())){
                        response = setMessageError("El Código de Pedido ingresado es incorrecto.",access.getId());
                    }else{
                        VisorByRequestCodeDB item = visorService.getVisorByRequestCode(request.getRequestCode());

                        if(item != null){
                            response.setResponseData(item);
                            response.setResponseMessage("Success");
                            response.setResponseCode("1");
                            traceabilityAccessService.saveLog("Success!", access.getId());
                        }else{
                            response = setMessageError("El Código de Pedido no existe.",access.getId());
                        }
                    }
                }else{
                    //response = setMessageError("Token de acceso inválido: " + token, null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(), null);
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value="/updatestatustoa",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity updateStatusTOA(@RequestBody StatusToaRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start updateStatusTOA");
        Response<String> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_TOA);
                if(access != null){
                    if( !statusToaService.validateFilledFieldsRequest(request) ){
                        response = setMessageError("Datos incompletos.", access.getId());
                    }else{

                        String resultValidation = statusToaService.validateContentFieldsRequest(request);
                        if(resultValidation.equals("")){

                            VisorByRequestCodeDB item = visorService.getVisorByRequestCode(request.getCodigo_pedido());

                            if(item != null){

                                StatusToaDB statusToaActual = statusToaService.getStatusToa(request.getCodigo_pedido());
                                statusToaService.saveStatusTOA(statusToaActual,  request, item.getId(), request.getCodigo_pedido());

                                response.setResponseData("OK");
                                response.setResponseMessage("Success");
                                response.setResponseCode("1");
                                traceabilityAccessService.saveLog("Save successfully: " + request.getCodigo_pedido(), access.getId());

                            }else{
                                response = setMessageError("El Código de Pedido no existe.", access.getId());
                            }

                        }else{
                            response = setMessageError(resultValidation, access.getId());
                        }

                    }
                }else{
                    //response = setMessageError("Token de acceso inválido: " + token,null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(),null);
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value="/savecustomerpublicdata",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity saveCustomerPublicData(@RequestBody PublicCustomerRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start saveCustomerPublicData");
        Response<String> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){

                    String resultValidation = publicCustomerService.validateDataFieldsRequest(request);
                    if(resultValidation.equals("")){

                        PublicCustomerDataDB publicCustomerDataActual = publicCustomerService.getPublicCustomerData(request.getDocumento());
                        publicCustomerService.savePublicCustomerData(publicCustomerDataActual,  request);

                        response.setResponseData("OK");
                        response.setResponseMessage("Success");
                        response.setResponseCode("1");
                        traceabilityAccessService.saveLog("Save successfully: " + request.getDocumento(), access.getId());

                    }else{
                        response = setMessageError(resultValidation, access.getId());
                    }

                }else{
                    //response = setMessageError("Token inválido: " + token,null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(),null);
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value="/savecustomerpublicrating",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity saveCustomerPublicRating(@RequestBody PublicCustomerRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start saveCustomerPublicData");
        Response<String> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){

                    String resultValidation = publicCustomerService.validateRatingFieldsRequest(request);
                    if(resultValidation.equals("")){

                        PublicCustomerRatingDB publicCustomerRatingActual = publicCustomerService.getPublicCustomerRating(request.getDocumento());
                        publicCustomerService.savePublicCustomerRating(publicCustomerRatingActual,  request);

                        response.setResponseData("OK");
                        response.setResponseMessage("Success");
                        response.setResponseCode("1");
                        traceabilityAccessService.saveLog("Save successfully: " + request.getDocumento(), access.getId());

                    }else{
                        response = setMessageError(resultValidation, access.getId());
                    }

                }else{
                    //response = setMessageError("Token inválido: " + token,null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(),null);
        }
        return ResponseEntity.ok(response);
    }

    private Response setMessageError(String message, Integer tokenId) {
        Response<Object> response = new Response<>();
        response.setResponseData(null);
        response.setResponseMessage(message);
        response.setResponseCode("0");
        try{
            traceabilityAccessService.saveLog(message, tokenId);
        }catch(Exception ex){
            _log.error(ex.getMessage());
        }
        return response;
    }

    private void saveUnauthorizedLog(String message) {
        try{
            traceabilityAccessService.saveLog(message, null);
        }catch(Exception ex){
            _log.error(ex.getMessage());
        }
    }

    /*private Response setMessageWarning(String message){
        Response<Object> response = new Response<>();
        response.setResponseData(null);
        response.setResponseMessage(message);
        response.setResponseCode("0");
        return response;
    }*/

}
