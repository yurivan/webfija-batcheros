package pe.telefonica.tracer.web.soap;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.filter.HiddenHttpMethodFilter;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import org.springframework.ws.soap.saaj.SaajSoapMessage;
import pe.telefonica.tracer.model.toa.WO_INIT;
import pe.telefonica.tracer.business.impl.TOAService;
import pe.telefonica.tracer.model.toa.MessageResponse;
//import toatech.agent.Message;
//import toatech.agent.SendMessage;
import pe.telefonica.tracer.model.toa.generated.Message;
import pe.telefonica.tracer.model.toa.generated.SendMessage;
import pe.telefonica.tracer.model.toa.SendMessageResponse;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.*;
import javax.xml.soap.MimeHeaders;
import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

@Endpoint
public class TOAEndpoint {
    private static final String NAMESPACE_URI = "urn:toatech:agent";

    private TOAService TOAservice;

    @Autowired
    public TOAEndpoint(TOAService TOAservice) {
        this.TOAservice = TOAservice;
    }

    /*@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getCountryRequest")
    @ResponsePayload
    public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) {
        GetCountryResponse response = new GetCountryResponse();
        response.setCountry(TOAservice.findCountry(request.getName()));

        return response;
    }*/

    /*@Bean
    public FilterRegistrationBean registration(HiddenHttpMethodFilter filter) {
        FilterRegistrationBean registration = new FilterRegistrationBean(filter);
        registration.setEnabled(false);
        return registration;
    }*/

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "send_message")
    @ResponsePayload
    public SendMessageResponse sendMessage(@RequestPayload SendMessage request, MessageContext messageContext /*, HttpServletRequest httpRequest*/) {

        SendMessageResponse response = new SendMessageResponse();
        try{

            //XmlMapper xmlMapper = new XmlMapper();
            //String xml = xmlMapper.writeValueAsString(new SimpleBean());

            /*SaajSoapMessage soapRequest = (SaajSoapMessage) messageContext.getRequest();
            MimeHeaders headers = soapRequest.getSaajMessage().getMimeHeaders();
            //String user = headers.getHeader("Auth-User");

            headers.getHeader("Auth-User");
            headers.getHeader("Auth-Password");*/

            TOAservice.saveLogRequest(request);


            //SendMessageResponse response = new SendMessageResponse();
            List<MessageResponse> messageResponseList = new ArrayList<>();
            try{

                TOAservice.processMessages(request.getMessages().getMessage(), messageContext);

                String messageText = "";
                for(Message message : request.getMessages().getMessage()){
                    messageText = "Estado recibido";

                    MessageResponse messageResponse = new MessageResponse();
                    messageResponse.setMessageId(message.getMessageId());
                    messageResponse.setStatus("sent");
                    messageResponse.setDescription(messageText);

                    messageResponseList.add(messageResponse);
                }
                response.setMessageResponse(messageResponseList);
            }catch(Exception ex){
                ex.printStackTrace();
                response.setMessageResponse(messageResponseList);
            }
            return response;

        }catch(Exception ex){
            ex.printStackTrace();
            response.setMessageResponse(getError(ex.getMessage()));
        }

        return response;
    }

    private List<MessageResponse> getError(String message){
        List<MessageResponse> messageResponseList = new ArrayList<>();
        MessageResponse messageResponse = new MessageResponse();
        messageResponse.setDescription(message);
        messageResponseList.add(messageResponse);
        return messageResponseList;
    }
}
