package pe.telefonica.tracer.configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Properties;

@Configuration
public class Database {
  private static final Logger logger = LoggerFactory.getLogger(Database.class);
  private static HikariDataSource datasource;
  static Properties prop = new Properties();
  static Connection con;

  @Bean
  public static DataSource datasource() {
    
    if (datasource == null) {
      final HikariConfig config = new HikariConfig();

      config.setDriverClassName(System.getenv("TDP_FIJA_DB_DRIVER"));
            config.setJdbcUrl(System.getenv("TDP_FIJA_DB_URL"));
      config.setUsername(System.getenv("TDP_FIJA_DB_USR"));
      config.setPassword(System.getenv("TDP_FIJA_DB_PW"));

            config.addDataSourceProperty("ApplicationName", "ToaQA-Soap");
            config.setMinimumIdle(Integer.parseInt(System.getenv("TDP_FIJA_DB_MINIMUM_IDLE")));
      config.setMaximumPoolSize(Integer.parseInt(System.getenv("TDP_FIJA_DB_POOLING")));
            config.setIdleTimeout(Integer.parseInt(System.getenv("TDP_FIJA_DB_TIMEOUT_IDLE")));

      datasource = new HikariDataSource(config);
    }
    return datasource;
  }

}
