package pe.telefonica.tracer.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class DatabasePropertiesSource {
	private static final Logger logger = LoggerFactory.getLogger(DatabasePropertiesSource.class);
	private DataSource datasource;
	
	public DatabasePropertiesSource(DataSource datasource) {
		this.datasource = datasource;
	}
	
	public Map<String, Object> loadProperties () {
		Map<String, Object> props = new HashMap<>();
		try (Connection con = datasource.getConnection()) {
			String query = "SELECT p.element, p.strvalue FROM ibmx_a07e6d02edaf552.parameters p " +
					" WHERE p.domain = 'CONFIG' AND p.category = 'PARAMETER' AND (p.element like '%emblue%' OR p.element like '%traceability%')";
			logger.info("query: "+query);
			try (PreparedStatement stmt = con.prepareStatement(query)) {
				try (ResultSet rs = stmt.executeQuery()) {
					while (rs.next()) {
						String key = rs.getString(1);
						String value = rs.getString(2);
						props.put(key, value);
						logger.info(String.format("Parametro %s: %s", key, value));
					}
				}
			}
		} catch (Exception e) {
			logger.error("waa :/", e);
		}
		return props;
	}

}
