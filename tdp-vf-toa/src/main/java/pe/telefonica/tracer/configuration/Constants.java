package pe.telefonica.tracer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Constants {

	@Value("${tdp.mail.emblue.authenticate.uri}")
	private String emailAutenticateUri;

	@Value("${tdp.mail.emblue.checkconexion.uri}")
	private String emailCheckConexionUri;
	
	@Value("${tdp.mail.emblue.sendexpress.uri}")
	private String emailSendExpressUri;

	@Value("${tdp.mail.emblue.user}")
	private String emailUser;

	@Value("${tdp.mail.emblue.password}")
	private String emailPassword;

	@Value("${tdp.mail.emblue.token}")
	private String emailToken;

	@Value("${tdp.mail.emblue.sendmailexpresss.actionId}")
	private String emailSendExpressActionId;

	@Value("alvin.puma@smart-home.com.pe|diana.ch.n.26@gmail.com|felix.vargas.vasslatam@gmail.com")
	private String receivers;

	@Value("${tdp.mail.traceability.subject.requested}")
	private String subjectSolicitado;

	@Value("${tdp.mail.traceability.subject.registered}")
	private String subjectRegistrado;

	public Constants(){
		/*emailAutenticateUri = "http://api.embluemail.com/Services/Emblue3Service.svc/json/Authenticate";
		emailCheckConexionUri = "http://api.embluemail.com/Services/Emblue3Service.svc/json/CheckConnection";
		emailSendExpressUri = "http://api.embluemail.com/Services/Emblue3Service.svc/json/sendMailExpress";
		emailUser = "developer@movistarplayperu.com";
		emailPassword = "Developer_movistarplayperu1";
		emailToken = "sUZJ0eb5-yQpgE-wRMcz-mzZFpYjg1v";
		emailSendExpressActionId = "537839";
		receivers = "felix.vargas@vasslatam.com";
		subjectSolicitado = "HEMOS RECIBIDO TU PEDIDO";
		subjectRegistrado = "TU PEDIDO HA SIDO REGISTRADO";*/
	}

	public String getEmailAutenticateUri() {
		return emailAutenticateUri;
	}

	public void setEmailAutenticateUri(String emailAutenticateUri) {
		this.emailAutenticateUri = emailAutenticateUri;
	}

	public String getEmailCheckConexionUri() {
		return emailCheckConexionUri;
	}

	public void setEmailCheckConexionUri(String emailCheckConexionUri) {
		this.emailCheckConexionUri = emailCheckConexionUri;
	}

	public String getEmailSendExpressUri() {
		return emailSendExpressUri;
	}

	public void setEmailSendExpressUri(String emailSendExpressUri) {
		this.emailSendExpressUri = emailSendExpressUri;
	}

	public String getEmailUser() {
		return emailUser;
	}

	public void setEmailUser(String emailUser) {
		this.emailUser = emailUser;
	}

	public String getEmailPassword() {
		return emailPassword;
	}

	public void setEmailPassword(String emailPassword) {
		this.emailPassword = emailPassword;
	}

	public String getEmailToken() {
		return emailToken;
	}

	public void setEmailToken(String emailToken) {
		this.emailToken = emailToken;
	}

	public String getEmailSendExpressActionId() {
		return emailSendExpressActionId;
	}

	public void setEmailSendExpressActionId(String emailSendExpressActionId) {
		this.emailSendExpressActionId = emailSendExpressActionId;
	}

	public String getReceivers() {
		return receivers;
	}

	public void setReceivers(String receivers) {
		this.receivers = receivers;
	}

	public String getSubjectSolicitado() {
		return subjectSolicitado;
	}

	public void setSubjectSolicitado(String subjectSolicitado) {
		this.subjectSolicitado = subjectSolicitado;
	}

	public String getSubjectRegistrado() {
		return subjectRegistrado;
	}

	public void setSubjectRegistrado(String subjectRegistrado) {
		this.subjectRegistrado = subjectRegistrado;
	}
	
}