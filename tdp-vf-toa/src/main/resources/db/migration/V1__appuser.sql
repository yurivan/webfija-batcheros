-- SCRIPTS AQUI
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD fecha_equipo_tecnico timestamp;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD fecha_instalado timestamp;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD flg_solicitado_email integer;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD flg_registrado_email integer;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD flg_equipo_tecnico_email integer;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD flg_instalado_email integer;

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.mail.emblue.authenticate.uri','http://api.embluemail.com/Services/Emblue3Service.svc/json/Authenticate'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.checkconexion.uri','http://api.embluemail.com/Services/Emblue3Service.svc/json/CheckConnection'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.sendexpress.uri','http://api.embluemail.com/Services/Emblue3Service.svc/json/sendMailExpress'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.sendmailexpresss.actionId','537839'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.user','developer@movistarplayperu.com'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.password','Developer_movistarplayperu1'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.token','sUZJ0eb5-yQpgE-wRMcz-mzZFpYjg1v'),
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.requested','HEMOS RECIBIDO TU PEDIDO'),
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.registered','TU PEDIDO HA SIDO REGISTRADO');




CREATE TABLE ibmx_a07e6d02edaf552.traceability_access(
	id SERIAL primary key,
    token varchar(250) not null,
    type integer not null
);

INSERT INTO ibmx_a07e6d02edaf552.traceability_access(token, type) VALUES('iAJiahANAjahIOaoPAIUnIAUZPzOPIW', 1),('MkJUHugYfDFrASxYhuHnkjnsSaSs', 2);

CREATE TABLE ibmx_a07e6d02edaf552.traceability_access_log(
	id SERIAL primary key,
    token_id integer,
    message text,
    date timestamp not null
);

--ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD estado_toa varchar(50);




CREATE TABLE ibmx_a07e6d02edaf552.status_toa(
	id SERIAL primary key,
	id_visor varchar(50),
    accion varchar(20),
    requerimiento varchar(15),
    codigo_pedido varchar(15),
    orden_trabajo varchar(15),
    fecha_envio date,
    hora_envio time,
    fecha_envio_toa date,
    hora_envio_toa time,
    fecha_agenda date,
    franja_horaria varchar(6),
    fecha_inicio_sla date,
    fecha_fin_sla date,
    fecha_inicio date,
    fecha_cierre date,
    motivo_accion  varchar(100),
    celular_cliente varchar(10),
    mensaje_cliente varchar(140),
    insert_update timestamp not null
);

CREATE TABLE ibmx_a07e6d02edaf552.public_customer(
	id SERIAL primary key,
	documento varchar(20),
    telefono3 varchar(20),
    telefono4 varchar(20),
    direccion2 varchar(250),
    correo2 varchar(50),
    rango_fecha_inicio varchar(15),
    rango_fecha_fin varchar(15),
    respuesta1 varchar(250),
    respuesta2 varchar(250),
    respuesta3 varchar(250),
    respuesta4 varchar(250),
    valoracion integer,
    insert_update timestamp not null
);




CREATE TABLE ibmx_a07e6d02edaf552.toa_status(
	id SERIAL primary key,
	message_id varchar(20),
	peticion varchar(20),
	status_toa varchar(20),
    external_id varchar(20),
    xa_identificador_st varchar(20),
    eta_start_time varchar(20),
    astatus varchar(20),
    appt_number varchar(20),
    aid varchar(20),
    XA_NUMBER_WORK_ORDER varchar(20),
    XA_NUMBER_SERVICE_ORDER varchar(20),

    xa_creation_date varchar(20),
    xa_requirement_number varchar(50),

    delivery_window_start varchar(20),
    delivery_window_end varchar(20),
    date varchar(20),
    usuario varchar(50),
    time_slot varchar(20),
    end_time varchar(20),
    start_time varchar(20),
    time_of_assignment varchar(20),
    time_of_booking varchar(20),
    duration varchar(20),

    eta_end_time varchar(20),

    ETA varchar(20),

    A_COMMENT_TECHNICIAN text,

    A_OBSERVATION text,
    XA_SOURCE_SYSTEM text,
    A_NETWORK_CHANGE_ELEMENT_LIST text,
    A_RECEIVE_PERSON_NAME text,
    A_RECEIVE_PERSON_ID text,
    A_COMPLETE_REASON_INSTALL text,
    A_COMPLETE_CAUSA_REP_STB text,
    A_COMPLETE_REP_STB text,
    A_COMPLETE_CAUSA_REP_ADSL text,
    A_COMPLETE_REP_ADSL text,
    A_COMPLETE_CAUSA_REP_SAT text,
    A_COMPLETE_REP_SAT text,
    A_COMPLETE_CAUSA_REP_CBL text,
    A_COMPLETE_REP_CBL text,

    A_SUSPEND_REASON text,

    XA_CANCEL_REASON text,

    insert_date timestamp not null,
    pname varchar(20)
);

CREATE TABLE ibmx_a07e6d02edaf552.toa_sms(
	id SERIAL primary key,
	message_id varchar(50),
	peticion varchar(50),
	status_toa varchar(20),
    external_id varchar(50),
    xa_identificador_st varchar(50),
    appt_number varchar(50),
    aid varchar(50),
    XA_NUMBER_WORK_ORDER varchar(50),
    XA_NUMBER_SERVICE_ORDER varchar(50),
    number varchar(50),
    number1 varchar(50),
    number2 varchar(50),
    number3 varchar(50),
    text text,
    insert_date timestamp not null
);

CREATE TABLE ibmx_a07e6d02edaf552.toa_status_inventory(
	id SERIAL primary key,
	toa_status_id integer,

    type varchar(50),
	invtype varchar(50),
	invsn varchar(50),
    quantity varchar(50),
    xi_brand varchar(50),
    xi_model varchar(50),
    xi_component varchar(50),
    xi_mac_address varchar(50),
    i_num_activacion varchar(50)
);

CREATE TABLE ibmx_a07e6d02edaf552.toa_log(
	id SERIAL primary key,
    message_id varchar(50),
    message_text text,
    date timestamp not null
);

CREATE TABLE ibmx_a07e6d02edaf552.toa_access(
	id SERIAL primary key,
    username varchar(250) not null,
    password varchar(250) not null,
    status integer not null
);

INSERT INTO ibmx_a07e6d02edaf552.toa_access(id,username,password,status)
VALUES (1,'tdptoa','uhGf9JhtXgRdFiMioWaxGbJn',1);

ALTER TABLE ibmx_a07e6d02edaf552.toa_sms
ALTER COLUMN number type varchar(50);
ALTER TABLE ibmx_a07e6d02edaf552.toa_sms
ALTER COLUMN number1 type varchar(50);
ALTER TABLE ibmx_a07e6d02edaf552.toa_sms
ALTER COLUMN number2 type varchar(50);
ALTER TABLE ibmx_a07e6d02edaf552.toa_sms
ALTER COLUMN number3 type varchar(50);