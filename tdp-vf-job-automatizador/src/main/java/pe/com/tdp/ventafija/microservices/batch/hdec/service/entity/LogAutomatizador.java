package pe.com.tdp.ventafija.microservices.batch.hdec.service.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_automatizador", schema = "ibmx_a07e6d02edaf552")
public class LogAutomatizador {

    @Id
    @Column(name = "codvtappcd") //=> ID
    private String codvtappcd;

    @Column(name = "coderrcd")
    private String coderrcd;

    @Column(name = "deserrds")  //=> Mensaje ejemplo OK  o OK-CM<S
    private String deserrds;

    @Column(name = "codestvtaapp")  //=>Estado R - C - S - N
    private String codestvtaapp;

    //CÓDIGO DE PEDIDO ATIS - CMS
    @Column(name = "codpedaticd")  //=> Código pedido atis
    private String codpedaticd;

    @Column(name = "codreqeedcd")  //=> Código de pedido cms
    private String codreqeedcd;

    public String getCodvtappcd() {
        return codvtappcd;
    }

    public void setCodvtappcd(String codvtappcd) {
        this.codvtappcd = codvtappcd;
    }

    public String getCoderrcd() {
        return coderrcd;
    }

    public void setCoderrcd(String coderrcd) {
        this.coderrcd = coderrcd;
    }

    public String getDeserrds() {
        return deserrds;
    }

    public void setDeserrds(String deserrds) {
        this.deserrds = deserrds;
    }

    public String getCodestvtaapp() {
        return codestvtaapp;
    }

    public void setCodestvtaapp(String codestvtaapp) {
        this.codestvtaapp = codestvtaapp;
    }

    public String getCodpedaticd() {
        return codpedaticd;
    }

    public void setCodpedaticd(String codpedaticd) {
        this.codpedaticd = codpedaticd;
    }

    public String getCodreqeedcd() {
        return codreqeedcd;
    }

    public void setCodreqeedcd(String codreqeedcd) {
        this.codreqeedcd = codreqeedcd;
    }
}
