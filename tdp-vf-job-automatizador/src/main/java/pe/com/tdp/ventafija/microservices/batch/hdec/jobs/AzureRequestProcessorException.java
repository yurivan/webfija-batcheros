package pe.com.tdp.ventafija.microservices.batch.hdec.jobs;

public class AzureRequestProcessorException extends Exception {
	private static final long serialVersionUID = 1L;

	public AzureRequestProcessorException(String message) {
		super(message);
	}

	public AzureRequestProcessorException(String message, Throwable cause) {
		super(message, cause);
	}

}
