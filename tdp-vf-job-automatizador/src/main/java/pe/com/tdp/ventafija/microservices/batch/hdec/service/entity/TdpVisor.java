package pe.com.tdp.ventafija.microservices.batch.hdec.service.entity;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tdp_visor", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema
public class TdpVisor {

    @Id
    @Column(name = "id_visor")
    private String idVisor;

    @Column(name = "estado_solicitud")
    private String estadoSolicitud;

    @Column(name = "motivo_estado")
    private String motivoEstado;

    public String getIdVisor() {
        return idVisor;
    }

    public void setIdVisor(String idVisor) {
        this.idVisor = idVisor;
    }

    public String getEstadoSolicitud() {
        return estadoSolicitud;
    }

    public void setEstadoSolicitud(String estadoSolicitud) {
        this.estadoSolicitud = estadoSolicitud;
    }

    public String getMotivoEstado() {
        return motivoEstado;
    }

    public void setMotivoEstado(String motivoEstado) {
        this.motivoEstado = motivoEstado;
    }
}