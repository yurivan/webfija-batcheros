package pe.com.tdp.ventafija.microservices.batch.hdec.bean;

import java.util.Date;

/**
 * Bean que representa una Venta y su estado de envio al BACK (tambien conocido como HDEC)
 * @author      Valdemar
 * @since       1.5
 */
public class VentaAutomatizador {
    private String id;
    private String estado;
    private String codigoPedido;
    private Integer auto_retry;
    private Integer ok_change_auto;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoPedido() {
        return codigoPedido;
    }

    public void setCodigoPedido(String codigoPedido) {
        this.codigoPedido = codigoPedido;
    }

    public Integer getAuto_retry() {
        return auto_retry;
    }

    public void setAuto_retry(Integer auto_retry) {
        this.auto_retry = auto_retry;
    }

    public Integer getOk_change_auto() {
        return ok_change_auto;
    }

    public void setOk_change_auto(Integer ok_change_auto) {
        this.ok_change_auto = ok_change_auto;
    }
}
