package pe.com.tdp.ventafija.microservices.batch.hdec.service;

import org.springframework.stereotype.Service;

@Service
public class TokenGeneratorService {

    public String generateUniqueToken(){
        String token = "";
        int random = (int)(Math.random() * 999999 + 1);
        return String.format("%06d" , random);
    }
}
