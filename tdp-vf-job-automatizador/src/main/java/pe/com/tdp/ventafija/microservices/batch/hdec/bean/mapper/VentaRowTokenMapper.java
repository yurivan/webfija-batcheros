package pe.com.tdp.ventafija.microservices.batch.hdec.bean.mapper;

import org.springframework.jdbc.core.RowMapper;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.TokenCanal;
import pe.com.tdp.ventafija.microservices.batch.hdec.bean.VentaBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VentaRowTokenMapper implements RowMapper<TokenCanal> {

    @Override
    public TokenCanal mapRow(ResultSet rs, int rowNum) throws SQLException {
        TokenCanal ventaBean = new TokenCanal();
        ventaBean.setEntidad(rs.getString("ENTIDAD"));
        return ventaBean;
    }
}
