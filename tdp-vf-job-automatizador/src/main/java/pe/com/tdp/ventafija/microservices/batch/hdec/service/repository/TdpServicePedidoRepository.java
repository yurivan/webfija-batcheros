package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.TdpServicePedido;

@Repository
@Transactional
public interface TdpServicePedidoRepository extends JpaRepository<TdpServicePedido, String> {

    TdpServicePedido findOneByPedidoCodigo(String pedidoCodigo);

}
