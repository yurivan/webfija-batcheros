package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Transactional
public class OrderRepositoryImpl implements OrderRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Object[]> spRetriveHdecData(String orderId) {
        return em.createNamedQuery("spRetriveHdecData")
                .setParameter(1, orderId).getResultList();
    }

}
