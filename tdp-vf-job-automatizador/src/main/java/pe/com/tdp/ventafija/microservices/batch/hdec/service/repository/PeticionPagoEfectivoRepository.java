package pe.com.tdp.ventafija.microservices.batch.hdec.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.batch.hdec.service.entity.PeticionPagoEfectivo;

@Repository
public interface PeticionPagoEfectivoRepository extends JpaRepository<PeticionPagoEfectivo, Integer> {

	public PeticionPagoEfectivo findByOrderId(String orderId);


}