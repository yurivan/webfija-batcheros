var cfenv = require('cfenv');
var appEnv = cfenv.getAppEnv();

module.exports = {
  port: appEnv.port,
  logoutSessionsOnSensitiveChanges: true
}
