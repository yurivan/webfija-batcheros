module.exports = function (app) {
  var Role = app.models.Role;

  Role.registerResolver('authorized', function (role, ctx, cb) {
    var userId = ctx.accessToken.userId;
    if (!userId) {
      return process.nextTick(() => cb(null, false));
    }

    return cb(null, true);
  });
}
