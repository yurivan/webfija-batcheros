var _ = require('underscore'),
        formidable = require('formidable'),
        xlsx = require('node-xlsx');

var TdpCatalogsxMotorizadoConverter = function () {}
var x=1;

TdpCatalogsxMotorizadoConverter.prototype.objToRow = function (obj) {
  var arr = [obj['codigopedido'],   obj['codmotorizado'],
             obj['fecharegistro'],  obj['tipdoccliente'],
             obj['numdoccliente'],  obj['nombrecliente'],
             obj['movilcliente'],   obj['biometria'],
             obj['estado'],         obj['motivorechazo'],
             obj['registeredtime'], obj['updatetime']];
  return arr;
};

TdpCatalogsxMotorizadoConverter.prototype.rowToObj = function (row) {
  var obj = { id: x };
      obj['codigopedido']   = row[0];
      obj['codmotorizado']  = row[1];
      obj['fecharegistro']  = row[2];
      obj['tipdoccliente']  = row[3];
      obj['numdoccliente']  = row[4];
      obj['nombrecliente']  = row[5];
      obj['movilcliente']   = row[6];
      obj['biometria']      = row[7];
      obj['estado']         = row[8];
      obj['motivorechazo']  = row[9];
      obj['registeredtime'] = row[10];
      obj['updatetime']     = row[11];
  x=x+1;
  return obj;
};

TdpCatalogsxMotorizadoConverter.prototype.validate = function (obj) {
  var errors = [];
  var count = 0;
  var isValid = true;
  _.each(obj, function (v, k) {
    isValid = isValid && (_.isNumber(v) || !_.isEmpty(v));
    if (!(_.isNumber(v) || !_.isEmpty(v))) {
      errors.push('Valor no valido para '+k+': '+v);
    }
    count++;
  });
  isValid = isValid && count > 1;
  if (!isValid) {
    errors.push('Fila incompleta');
  }
  return errors;
};

TdpCatalogsxMotorizadoConverter.prototype.sanitize = function (dataArr) {
  var result = [];
  _.each(dataArr.slice(1), function (row) {
    if (_.size(row) > 0) {
      result.push(row);
    }
  });
  return result;
};

TdpCatalogsxMotorizadoConverter.prototype.rowToList = function (row, delimitador) {

  var listaResultado = [];
    
  if ( row != undefined ) {

    if ( row != null ) {
     
        var i=0;
        var listaaux = row.trim().split(delimitador);
        _.each(listaaux, function (rowaux) {
              listaResultado[i] = rowaux.trim().replace(/ñ/gi, "n").toUpperCase();
              i++;
        });

    }
  }
  
  return listaResultado;

};

TdpCatalogsxMotorizadoConverter.prototype.setVacio = function (a) {

  var b;
  
  
  if (a == '' || a == '-') { 
    
      b = 'Todos';

  } else { 

      b = a;

  }
  
  return b;

};

TdpCatalogsxMotorizadoConverter.prototype.generate = function (dataArr) {

  var _this = this;
  var result = [];
  var i = 0;

  _.each(dataArr, function (row) {

    if (_.size(row) > 0) {

      if (row[0] != undefined ) {

        if ( row[0] != null ) {

          if ( row[0] != '' ) {
                             
            var fila = [10];
            fila[0]   = row[0];
            fila[1]   = row[1];
            fila[2]   = row[2];
            fila[3]   = row[3];
            fila[4]   = row[4];
            fila[5]   = row[5];
            fila[6]   = row[6];
            fila[7] = 0;
            fila[8] = 'PENDIENTE';
            fila[9] = 'No se valido';
            fila[10] = '25/06/2018  08:50';
            fila[11] = '25/06/2018  08:50';
            result[i] = fila;
            i++;
                
            
          } 
        }
      }
    }
  });
  return result;
};

TdpCatalogsxMotorizadoConverter.prototype.processData = function (fileData) {
  var _this = this;
    var data = {
      processed: [],
      error: [],
      totalCount: 0,
      errorCount: 0
    };
    var cleanData = this.sanitize(fileData);
    var errorCount = 0;
    var listarows = this.generate(cleanData);
    var size = listarows.length;
    _.each(listarows, function (row) {
      var p = _this.rowToObj(row);
      var errors = _this.validate(p);
      if (_.isEmpty(errors)) {
        data.processed.push(p);
      } else {
        data.error.push(_.extend({errors: errors}, p));
        errorCount++;
      }
    });
    data.totalCount = size;
    data.errorCount = errorCount;
    return data;
};

TdpCatalogsxMotorizadoConverter.prototype.processFile = function (req, cb) {
  var _this = this;
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    var responseData = { processError: 'no se pudo procesar el archivo' };
    var f = files['data'];
    if (f) {
      var contents;
      try { contents = xlsx.parse(f.path); } catch (e) { contents = false; console.error(e); }
      if (contents) {
        var cartilla = contents[0];
        var xlsxData = cartilla.data;
        responseData = _this.processData(xlsxData);
      }
      cb(responseData);
    }
  });
  return cb.promise;
}

TdpCatalogsxMotorizadoConverter.prototype.insertData = function (req, ds, m, cb) {
  x=1;
  var ServiceCallEvents = m.app.models.ServiceCallEvents;
  this.processFile(req, function (d) {
    ds.connector.execute('truncate ibmx_a07e6d02edaf552.tdp_motorizado_base;', function () {
      var countList = d.processed.length;
      var data = d.processed;
      for (var i = 0; i < countList; i++) {
        var sql="INSERT INTO ibmx_a07e6d02edaf552.tdp_motorizado_base (id, codigopedido, codmotorizado, fecharegistro, tipdoccliente, numdoccliente, nombrecliente, movilcliente, biometria, estado,motivorechazo, updatetime) "
        +"VALUES ("+data[i].id+", '"+data[i].codigopedido+"', '"+data[i].codmotorizado+"', '"+data[i].fecharegistro+"','"+data[i].tipdoccliente+"', '"+data[i].numdoccliente+"', '"+data[i].nombrecliente+"', '"+data[i].movilcliente+"', null,'PENDIENTE', null, null);"
        ds.connector.execute(sql, function (err, result) {
          if (err) throw err;
          console.log("Entro")
        });
      }

      m.create(d.processed, function (err, createdModel, created) {
        var response = { total: 1 };
        if (_.isArray(createdModel)) {
          response['registered'] = createdModel.length;
        }
        ServiceCallEvents.log(req.accessToken, 'Archivo de carga para vendedor procesado, cargados ' + createdModel.length + '@'+new Date());
        cb(response);
      });
    });
  });
  return cb.promise;
}

module.exports = TdpCatalogsxMotorizadoConverter;
