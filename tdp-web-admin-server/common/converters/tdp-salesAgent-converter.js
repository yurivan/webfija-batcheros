var _ = require('underscore'),
	formidable = require('formidable'),
	xlsx = require('node-xlsx'),
	//postgresql = require('postgresql');
	postgresql = require('pg');

var TdpSalesAgentsConverter = function (app) {
	this.app = app;
	this.model = this.app.models.TdpSalesAgent;
	this.datasource = this.model.getDataSource();
}

TdpSalesAgentsConverter.prototype.objToRow = function (obj) {
	//Sprint 5
	var arr = [obj['dni'],
	obj['apepaterno'],
	obj['apematerno'],
	obj['nombre'],
	obj['codatis'],
	obj['codcms'],
	obj['email'],
	obj['niveles'],
	obj['zona'],
	obj['zonal'],
	obj['canalatis'],
	obj['canal'],
	obj['segmento'],
	obj['entidad'],
	obj['nompuntoventa'],
	obj['codpuntoventa'],
	obj['canalEquivalenciaCampania'],
	obj['entidadEquivalenciaCampania'],
	obj['userCanalCodigo'],
	// obj['userFuerzaVenta'],
	obj['lima'],
	obj['ica'],
	obj['arequipa'],
	obj['amazonas'],
	obj['apurimac'],
	obj['ayacucho'],
	obj['cajamarca'],
	obj['chiclayo'],
	obj['chimbote'],
	obj['cusco'],
	obj['huancavelica'],
	obj['huancayo'],
	obj['huanuco'],
	obj['loreto'],
	obj['madreDeDios'],
	obj['moquegua'],
	obj['pasco'],
	obj['piura'],
	obj['puno'],
	obj['sanMartin'],
	obj['tacna'],
	obj['trujillo'],
	obj['tumbes'],
	obj['ucayali'],
	obj['ctipodoc']


	];
	return arr;
};

TdpSalesAgentsConverter.prototype.rowToObj = function (row, niveldata) {
	//Sprint 5
	var obj = { id: 0 };
	obj['ctipodoc'] = row[0];
	obj['dni'] = row[1];
	obj['apepaterno'] = row[2];
	obj['apematerno'] = row[3];
	obj['nombre'] = row[4];
	obj['codatis'] = row[5];
	obj['codcms'] = row[6];
	obj['email'] = row[7];
	obj['niveles'] = niveldata;
	obj['zona'] = row[9];
	obj['zonal'] = row[10];
	obj['canalatis'] = row[11];
	obj['canal'] = row[12];
	obj['segmento'] = row[13];
	obj['entidad'] = row[14];
	obj['nompuntoventa'] = row[15];
	obj['codpuntoventa'] = row[16];
	obj['canalEquivalenciaCampania'] = '';
	obj['entidadEquivalenciaCampania'] = '';
	obj['type_flujo_tipificacion'] = '';
	//obj['canalEquivalenciaCampania'] = row[17];
	//obj['entidadEquivalenciaCampania'] = row[18];
	obj['userCanalCodigo'] = row[17];
	// obj['userFuerzaVenta'] = row[17];
	obj['lima'] = row[18];
	obj['ica'] = row[19];
	obj['arequipa'] = row[20];
	obj['amazonas'] = row[21];
	obj['apurimac'] = row[22];
	obj['ayacucho'] = row[23];
	obj['cajamarca'] = row[24];
	obj['chiclayo'] = row[25];
	obj['chimbote'] = row[26];
	obj['cusco'] = row[27];
	obj['huancavelica'] = row[28];
	obj['huancayo'] = row[29];
	obj['huanuco'] = row[30];
	obj['loreto'] = row[31];
	obj['madreDeDios'] = row[32];
	obj['moquegua'] = row[33];
	obj['pasco'] = row[34];
	obj['piura'] = row[35];
	obj['puno'] = row[36];
	obj['sanMartin'] = row[37];
	obj['tacna'] = row[38];
	obj['trujillo'] = row[39];
	obj['tumbes'] = row[40];
	obj['ucayali'] = row[41];
	return obj;
};

TdpSalesAgentsConverter.prototype.validate = function (obj) {
	var errors = [];
	var count = 0;
	var isValid = true;
	_.each(obj, function (v, k) {

		if (k !== "apepaterno" && k !== "apematerno" && k !== "nombre"
			&& k !== "email" && k !== "niveles" && k !== "zona"
			&& k !== "zonal" && k !== "canalatis" && k !== "canal"
			&& k !== "segmento" && k !== "entidad" && k != "nompuntoventa"
			&& k !== "codpuntoventa" && k !== "canalEquivalenciaCampania" &&
			k !== "entidadEquivalenciaCampania"
			&& k !== "password" && k !== "userCanalCodigo" && k !== "userFuerzaVenta"
			&& k !== "type_flujo_tipificacion"

			&& k !== "lima" && k !== "ica" && k !== "arequipa" &&
			k !== "amazonas" && k !== "apurimac" && k !== "ayacucho"
			&& k !== "cajamarca" && k !== "chiclayo" && k !== "chimbote"
			&& k !== "cusco" && k !== "huancavelica" && k !== "huancayo"
			&& k !== "huanuco" && k !== "loreto" && k !== "madreDeDios"
			&& k !== "moquegua" && k !== "pasco" && k !== "piura" && k !== "puno"
			&& k !== "sanMartin" && k !== "tacna" && k !== "trujillo"
			&& k !== "tumbes" && k !== "ucayali" && k !== "ctipodoc"

		) {



			isValid = isValid && (_.isNumber(v) || !_.isEmpty(v));
			if (!(_.isNumber(v) || !_.isEmpty(v))) {
				//errors.push('Valor no valido para '+k+': '+v);
				errors.push('Valor no valido para ' + k + ': No tiene número');
			}
		}
		count++;
	});
	isValid = isValid && count > 15;
	if (!isValid) {
		errors.push('Campo vacio');
	}
	return errors;
};

TdpSalesAgentsConverter.prototype.sanitize = function (dataArr) {
	var result = [];
	_.each(dataArr.slice(1), function (row) {
		if (_.size(row) > 0) {
			result.push(row);
		}
	});
	return result;
};

TdpSalesAgentsConverter.prototype.processData = function (fileData, niveldata) {
	var _this = this;
	var dni = {};
	var cms = {};
	var atis = {};
	var data = {
		processed: [],
		error: [],
		totalCount: 0,
		errorCount: 0
	};
	var cleanData = this.sanitize(fileData);
	var size = cleanData.length;
	var errorCount = 0;
	_.each(cleanData, function (row) {
		var p = _this.rowToObj(row, niveldata);
		var errors = _this.validate(p);
		if (!_.isUndefined(p.dni)) {
			if (_.isUndefined(dni[p.dni])) {
				dni[p.dni] = p;
			} else {
				errors.push("DNI duplicado");
			}
		}
		if (!_.isUndefined(p.codcms)) {
			if (_.isUndefined(cms[p.codcms])) {
				cms[p.codcms] = p;
			} else {
				errors.push("Código CMS duplicado");
			}
		}
		if (!_.isUndefined(p.codatis)) {
			if (_.isUndefined(atis[p.codatis])) {
				atis[p.codatis] = p;
			} else {
				errors.push("Código ATIS duplicado");
			}
		}
		if (_.isEmpty(errors)) {
			data.processed.push(p);
		} else {
			data.error.push(_.extend({ errors: errors }, p));
			errorCount++;
		}
	});
	data.totalCount = size;
	data.errorCount = errorCount;
	return data;
};

/**
 * Carga la informacion de los usuarios para la aplicacion movil
 * @param data que sera registrada en el sistema, type: array
 */
TdpSalesAgentsConverter.prototype.saveAppUsers = function (data, accessToken, callback) {
	var that = this;
	var ServiceCallEvents = this.app.models.ServiceCallEvents;
	var toCreate = [];

	_.each(data, function (value) {
		var v = value;
		v['appcode'] = 'Z';
		toCreate.push(v);
	});

	this.model.destroyAll({
		or: [
			{ appcode: 'A' },
			{ appcode: 'W' }
		]
	}, function (err, info) {
		console.log('eliminados ' + info.count + ' registros');
		that.model.create(toCreate, function (err, models) {
			var response = {};
			response['errors'] = 0;
			if (err) {
				var count = 0;
				response['errorMessages'] = [];
				_.each(err, function (e) {
					if (e) {
						count++;
						response['errorMessages'].push(e.message);
					}
				});
				response['errors'] = count;
			}
			if (_.isArray(models)) {
				response['registered'] = models.length - response.errors;
			}
			console.log('registrados: ' + response.registered + ' errores: ' + response.errors);
			that.datasource.connector.execute("UPDATE ibmx_a07e6d02edaf552.user as use " +
				"SET status = CASE WHEN tdp_sales_agent.codATIS is null THEN  '0' ELSE '1' END " +
				"FROM ibmx_a07e6d02edaf552.user as us left join ibmx_a07e6d02edaf552.tdp_sales_agent " +
				"on us.ID = tdp_sales_agent.codATIS where use.ID = us.ID;", function () {
					console.log('finished :)')
					callback(response);
				});
			/**      that.datasource.connector.execute("update user left join tdp_sales_agent on "+
						"tdp_sales_agent.codATIS=user.ID set user.status='0' "+
						"where tdp_sales_agent.codATIS is null;",function(){
							console.log('finished :)')
							callback(response);
					});
			**/
			ServiceCallEvents.log(accessToken, 'Archivo de carga para vendedores procesado para la app, cargados ' + models.length + '@' + new Date());
		});
	});
};

TdpSalesAgentsConverter.prototype.saveWebUsers = function (data, userId, reset, niveles, callback) {
	console.log('iniciando guardar datos');
	var that = this;
	//cascad
	let parametersModel = this.app.models.Parameters;
	this.processWebUsers(data.processed, userId, function (toUpdate) {
		console.log('procesando info');
		var doReset = false;
		if (typeof reset === 'string' && reset === 'true') {
			doReset = true;
		}
		if (toUpdate.dataToProcess.length < 1) {
			callback({});
			return;
		}

		var where = {
			niveles: '1'
		};
		console.log("saveAppUsers  where ::::" + JSON.stringify(where))

		// Sprint 12 - aplicamos equivalencias de campañas
		//let parametersModel = this.app.models.Parameters;
		parametersModel.find({
			where: {
				domain: 'EQUIVALENCIA',
				category: 'FICHAPRODUCTO.CANAL'
			}
		}, function (err, parametersCanal) {
			if (!err) {
				parametersModel.find({
					where: {
						domain: 'EQUIVALENCIA',
						category: 'FICHAPRODUCTO.ENTIDAD'
					}
				}, function (err, parametersEntidad) {
					if (!err) {
						_.each(data.processed, function (value) {
							var v = value;
							// Sprint 12 - aplicamos equivalencias de campañas
							v['canalEquivalenciaCampania'] = '-';
							v['entidadEquivalenciaCampania'] = '-';
							for (var i = 0; i < parametersCanal.length; i++) {
								let parameterItem = parametersCanal[i];
								let equivalenciasOrigen = parameterItem.strvalue + ',';
								let canalAtisVendedor = v['canal'];
								let canalAtisAndEntidadVendedor = v['canal'] + '-' + v['entidad'];
								//Corregir esta validacion
								var arrayEquivalenciasOrigen = equivalenciasOrigen.split(',')
								for (var k = 0; k < arrayEquivalenciasOrigen.length; k++) {
									let equiv = arrayEquivalenciasOrigen[k]
									if (equiv === (canalAtisVendedor) || equiv === (canalAtisAndEntidadVendedor)) {
										v['canalEquivalenciaCampania'] = parameterItem.element;

										console.log("Se asigna " + parameterItem.strfilter)

										v['type_flujo_tipificacion'] = parameterItem.strfilter;
										k = arrayEquivalenciasOrigen.length + 1;
										i = parametersCanal.length + 1;
									}
								}
								/*if (equivalenciasOrigen.indexOf(canalAtisVendedor) >= 0 || 
									equivalenciasOrigen.indexOf(canalAtisAndEntidadVendedor) >= 0) {
										v['canalEquivalenciaCampania'] = parameterItem.element;
										break;
								}*/
							}

							for (var i = 0; i < parametersEntidad.length; i++) {
								let parameterItem = parametersEntidad[i];
								let equivalenciasOrigen = parameterItem.strvalue;
								let entidadVendedor = v['entidad'];

								if (entidadVendedor.indexOf(equivalenciasOrigen) >= 0) {
									v['entidadEquivalenciaCampania'] = parameterItem.element;
									break;
								}
							}
						});

						that.model.destroyAll(where, function (err, info) {
							console.log('eliminados ' + info.count + ' registros');
							that.model.create(toUpdate.dataToProcess, function (err, models) {
								var response = {};
								response['errors'] = 0;
								if (err) {
									console.error(err)
									var count = 0;
									response['errorMessages'] = [];
									_.each(err, function (e) {
										if (e) {
											count++;
											response['errorMessages'].push(e.message + " " + e.detail);
										}
									});
									response['errors'] = count;
								}
								if (_.isArray(models)) {
									response['registered'] = models.length - response.errors;
								}
								console.log('registrados: ' + response.registered + ' errores: ' + response.errors);
								//inicio update sprint 5
								var sqlUpdate = "UPDATE ibmx_a07e6d02edaf552.user as use " +
									"SET status = CASE WHEN tdp_sales_agent.codATIS is null THEN  '0' ELSE '1' END " +
									"FROM ibmx_a07e6d02edaf552.user as us left join ibmx_a07e6d02edaf552.tdp_sales_agent " +
									"on us.ID = tdp_sales_agent.codATIS where use.ID = us.ID;";
								that.datasource.connector.execute(sqlUpdate, function (err, result) {
									if (err) throw err;
									console.log("Se actualizo: [" + result.affectedRows + "] usuarios de su estado");
									callback(response);
								});
								//fin update sprint 5
							});
						});
					}

				});
			}
		})

	});
};

/**
 * Carga la informacion de los usuarios para la aplicacion web
 * @param data que sera registrada en el sistema, type: array
 * @param callback function to be called
 * @param userId id of the user
 */
TdpSalesAgentsConverter.prototype.processWebUsers = function (toProcess, userId, callback) {
	this.app.models.WebUser.loadAllowedCallCenters(userId, function (data) {
		var callsMap = {};
		var dataToProcess = [];
		var dataSkiped = [];
		_.each(data, function (value) {
			callsMap[value.name] = value.name;
		});
		// recopilar datos
		_.each(toProcess, function (value) {
			var v = value;
			v['appcode'] = 'W';
			dataToProcess.push(v);
		});
		callback({
			dataToProcess: dataToProcess,
			dataSkiped: dataSkiped,
			callsMap: callsMap
		});
	});
};

TdpSalesAgentsConverter.prototype.processFile = function (req, ds, cb) {
	var _this = this;
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		var responseData = { processError: 'no se pudo procesar el archivo' };
		var f = files['data'];
		var resetData = fields['resetdata'];
		var niveldata = fields['niveldata'];
		console.log("niveldata: " + niveldata);
		if (f) {
			var contents;
			try { contents = xlsx.parse(f.path); } catch (e) { contents = false; console.error(e); }
			if (contents) {
				var cartilla = contents[0];
				var xlsxData = cartilla.data;
				responseData = _this.processData(xlsxData, niveldata);
			}
			responseData['resetData'] = resetData;
			responseData['niveldata'] = niveldata;
			cb(responseData);
		}
	});
	return cb.promise;
}

TdpSalesAgentsConverter.prototype.processFileWeb = function (req, callback) {
	var that = this;
	this.processFile(req, this.datasource, function (responseData) {
		console.log('file prcessed');
		that.processWebUsers(responseData.processed, req.accessToken.userId, function (webprocessed) {
			var data = {
				processed: [],
				error: [],
				totalCount: 0,
				errorCount: 0
			};
			var error = responseData.error;

			_.each(webprocessed.dataSkiped, function (value) {
				error.push(_.extend({ errors: ['No tiene permisos para actualizar el registro para ' + value.entidad + '.'] }, value));
			});

			data.processed = webprocessed.dataToProcess;
			data.error = error;
			data.totalCount = webprocessed.dataToProcess.length;
			data.errorCount = responseData.errorCount + webprocessed.dataSkiped.length;
			data.callsMap = webprocessed.callsMap;
			data.resetData = responseData.resetData
			data.niveldata = responseData.niveldata
			callback(data);
		});
	});
	return callback.promise;
}

TdpSalesAgentsConverter.prototype.insertData = function (req, ds, m, cb) {
	var transactionId = new Date().getTime();
	var ServiceCallEvents = m.app.models.ServiceCallEvents;
	var _this = this;
	this.app.io.emit('upload.status', {
		transactionId: transactionId,
		progress: 0,
		response: {}
	});

	this.processFile(req, this.datasource, function (d) {
		cb({
			transactionId: transactionId
		});
		_this.saveAppUsers(d.processed, req.accessToken, function (response) {
			_this.app.io.emit('upload.status', {
				transactionId: transactionId,
				progress: 100,
				response: response
			});
		});
	});

	return cb.promise;
}

TdpSalesAgentsConverter.prototype.insertWebData = function (req, cb) {
	var transactionId = new Date().getTime();
	var ServiceCallEvents = this.app.models.ServiceCallEvents;
	var _this = this;
	this.app.io.emit('upload.status', {
		transactionId: transactionId,
		progress: 0,
		response: {}
	});

	// var form = new formidable.IncomingForm();
	// form.parse(req, function (err, fields, files) {
	//   var resetData = fields['resetdata'];
	console.log('before processed')
	_this.processFileWeb(req, function (d) {
		console.log('processed', d.resetData)
		cb({
			transactionId: transactionId
		});

		console.log("saveAppUsers  d.processed ::::" + JSON.stringify(d))
		console.log("saveAppUsers  req.accessToken ::::" + req.accessToken.userId)

		_this.saveWebUsers(d, req.accessToken.userId, d.resetData, d.niveldata, function (response) {
			_this.app.io.emit('upload.status', {
				transactionId: transactionId,
				progress: 100,
				response: response
			});
		});
	});
	// });
	return cb.promise;
}
TdpSalesAgentsConverter.prototype.saveSaleAgent = (data, m, callback) => {
	var that = this
	let parametersModel = m.app.models.Parameters;

	parametersModel.find({
		where: {
			domain: 'EQUIVALENCIA',
			category: 'FICHAPRODUCTO.CANAL'
		}
	}, function (err, parametersCanal) {
		//console.log(parametersCanal)
		if (!err) {
			parametersModel.find({
				where: {
					domain: 'EQUIVALENCIA',
					category: 'FICHAPRODUCTO.ENTIDAD'
				}
			}, function (err, parametersEntidad) {
				//console.log(parametersEntidad)
				if (!err) {
					data.canalEquivalenciaCampania = '-'
					data.entidadEquivalenciaCampania = '-'
					for (var i = 0; i < parametersCanal.length; i++) {
						let parameterItem = parametersCanal[i]
						let equivalenciasOrigen = parameterItem.strvalue + ','
						let canalAtisVendedor = data.canal
						let canalAtisAndEntidadVendedor = data.canal + '-' + data.entidad
						//Corregir esta validacion
						var arrayEquivalenciasOrigen = equivalenciasOrigen.split(',')
						for (var k = 0; k < arrayEquivalenciasOrigen.length; k++) {
							let equiv = arrayEquivalenciasOrigen[k]
							if (equiv === (canalAtisVendedor) || equiv === (canalAtisAndEntidadVendedor)) {
								data.canalEquivalenciaCampania = parameterItem.element
								data.type_flujo_tipificacion = parameterItem.strfilter
								k = arrayEquivalenciasOrigen.length + 1
								i = parametersCanal.length + 1
							}
						}
					}

					for (var i = 0; i < parametersEntidad.length; i++) {
						let parameterItem = parametersEntidad[i];
						let equivalenciasOrigen = parameterItem.strvalue
						let entidadVendedor = data.entidad

						if (entidadVendedor.indexOf(equivalenciasOrigen) >= 0) {
							data.entidadEquivalenciaCampania = parameterItem.element
							break
						}
					}

					console.log(data)
					m.connector.execute(`INSERT INTO ibmx_a07e6d02edaf552.tdp_sales_agent
					(dni, apepaterno,apematerno,nombre,codcms,email,niveles,zona,zonal,canalatis,segmento,entidad,nompuntoventa,codpuntoventa,codatis,canal, user_canal_codigo, user_fuerza_venta,
					appcode, canalequivcampania, entidadequivcampania)
     VALUES('${data.dni}','${data.apepaterno}','${data.apematerno}','${data.nombre}','${data.codcms}','${data.email}','${data.niveles}','${data.zona}',
	 '${data.zonal}','${data.canalatis}','${data.segmento}','${data.entidad}','${data.nompuntoventa}','${data.codpuntoventa}','${data.codatis}','${data.canal}',
					'${data.userCanalCodigo}','${data.userFuerzaVenta}',
					'W','${data.canalEquivalenciaCampania}','${data.entidadEquivalenciaCampania}')`,
						(error, info) => {
							//console.log("data in savesaleAgent " + util.inspect(data))
							var response = {}
							response['status'] = ""
							response['errors'] = 0;
							if (!error) {
								console.log(`info :::::::: ${info}`)
								response['status'] = "OK"
								m.connector.execute(`UPDATE ibmx_a07e6d02edaf552.user SET status = '1' WHERE ID = '${data.codatis}'`,
									function (err, result) {
										if (err) throw err;
										console.log("Se actualizo: el estado del usuario");
										callback(response);
									});
							} else {
								console.log(`error ${error}`)
								response['status'] = "FAILED"
								var count = 0;
								response['errorMessages'] = [];
								response['errorMessages'].push(error)
								_.each(error, function (e) {
									if (e) {
										count++;
										response['errorMessages'].push(e.message);
									}
								});
								response['errors'] = count;
								//console.log(`response error::::: ${util.inspect(response)}`)
								callback(response)
							}
						}
					)
				}

			});
		}
	})

}
TdpSalesAgentsConverter.prototype.updateSaleAgent = (data, m, callback) => {
	var that = this
	//console.log("data" + util.inspect(data))
	m.connector.execute(`UPDATE ibmx_a07e6d02edaf552.tdp_sales_agent SET 
		dni = '${data.dni}', 
		apepaterno = '${data.apepaterno}',
		apematerno = '${data.apematerno}',
		nombre = '${data.nombre}',
		codcms = '${data.codcms}',
		email = '${data.email}',
		niveles = '${data.niveles}',
		zona = '${data.zona}',
		zonal = '${data.zonal}',
		canalatis = '${data.canalatis}',
		canal = '${data.canal}',
		segmento = '${data.segmento}',
		entidad = '${data.entidad}',
		nompuntoventa = '${data.nompuntoventa}',
		codpuntoventa = '${data.codpuntoventa}',
		codatis = '${data.codatis}',
		user_fuerza_venta = '${data.userFuerzaVenta}',
		user_canal_codigo = '${data.userCanalCodigo}'
    WHERE  dni = '${data.dni}'`, (error, info) => {
			//console.log("data in savesaleAgent " + util.inspect(data))
			var response = {}
			response['status'] = ""
			response['errors'] = 0;
			if (!error) {
				var sqlUpdate = "UPDATE ibmx_a07e6d02edaf552.user as use " +
					"SET status = CASE WHEN tdp_sales_agent.codATIS is null THEN  '0' ELSE '1' END " +
					"FROM ibmx_a07e6d02edaf552.user as us left join ibmx_a07e6d02edaf552.tdp_sales_agent " +
					"on us.ID = tdp_sales_agent.codATIS where use.ID = us.ID;";
				m.connector.execute(sqlUpdate, function (err, result) {
					if (!err) {
						console.log("Se actualizo: [" + result.affectedRows + "] usuarios de su estado");
						console.log(`info :::::::: ${info}`)
						response['status'] = "OK"
						callback(response)
					} else {
						console.log(`error ${error}`)
						response['status'] = "FAILED"
						var count = 0;
						response['errorMessages'] = [];
						response['errorMessages'].push(error)
						_.each(error, function (e) {
							if (e) {
								count++;
								response['errorMessages'].push(e.message);
							}
						});
						response['errors'] = count;
						//console.log(`response error::::: ${util.inspect(response)}`)
						callback(response)
					}
				});
			} else {
				console.log(`error ${error}`)
				response['status'] = "FAILED"
				var count = 0;
				response['errorMessages'] = [];
				response['errorMessages'].push(error)
				_.each(error, function (e) {
					if (e) {
						count++;
						response['errorMessages'].push(e.message);
					}
				});
				response['errors'] = count;
				//console.log(`response error::::: ${util.inspect(response)}`)
				callback(response)
			}
		}
	)
}

TdpSalesAgentsConverter.prototype.deleteSaleAgent = (dni, codatis, m, callback) => {
	var that = this
	//console.log("data" + util.inspect(data))
	m.connector.execute(`DELETE FROM ibmx_a07e6d02edaf552.tdp_sales_agent WHERE dni = '${dni}'`, (error, info) => {
		//console.log("data in savesaleAgent " + util.inspect(data))
		var response = {}
		response['status'] = ""
		response['errors'] = 0;
		if (error == null || error == false) {
			m.connector.execute(`UPDATE ibmx_a07e6d02edaf552.user SET status='0' where id='${codatis}'`, (error, info) => {
				if (error == null || error == false) {
					response['status'] = "OK";
					//console.log(`response error::::: ${util.inspect(response)}`)
					callback(response);
				}
			})
		} else {
			console.log(`error ${error}`)
			response['status'] = "FAILED"
			var count = 0;
			response['errorMessages'] = [];
			response['errorMessages'].push(error)
			_.each(error, function (e) {
				if (e) {
					count++;
					response['errorMessages'].push(e.message);
				}
			});
			response['errors'] = count;
			//console.log(`response error::::: ${util.inspect(response)}`)
			callback(response)
		}
	}
	)
}

TdpSalesAgentsConverter.prototype.deleteSaleAgentUser = (dni, codatis, m, callback) => {
	var that = this
	//console.log("data" + util.inspect(data))
	m.connector.execute(`DELETE FROM ibmx_a07e6d02edaf552.tdp_sales_agent WHERE dni = '${dni}'`, (error, info) => {
		//console.log("data in savesaleAgent " + util.inspect(data))
		var response = {}
		response['status'] = ""
		response['errors'] = 0;
		if (error == null || error == false) {
			m.connector.execute(`UPDATE ibmx_a07e6d02edaf552.user SET status='0' where id='${codatis}'`, (error, info) => {
				if (error == null || error == false) {
					response['status'] = "OK";
					//console.log(`response error::::: ${util.inspect(response)}`)
					callback(response);
				}
			})
		} else {
			console.log(`error ${error}`)
			response['status'] = "FAILED"
			var count = 0;
			response['errorMessages'] = [];
			response['errorMessages'].push(error)
			_.each(error, function (e) {
				if (e) {
					count++;
					response['errorMessages'].push(e.message);
				}
			});
			response['errors'] = count;
			//console.log(`response error::::: ${util.inspect(response)}`)
			callback(response)
		}
	}
	)
}

// Registrar en tdp_sales_agent y user
TdpSalesAgentsConverter.prototype.saveSaleAgentUser = (data, m, callback) => {
	var that = this

	//console.log("data" + util.inspect(data))
	m.connector.execute(`INSERT INTO ibmx_a07e6d02edaf552.tdp_sales_agent
    (dni, apepaterno, apematerno, nombre, codcms, email, niveles, zona,zonal, canalatis, segmento, entidad, nompuntoventa, codpuntoventa, codatis, canal)
    VALUES (
	'${data.dni}',UPPER('${data.apepaterno}'),UPPER('${data.apematerno}'),UPPER('${data.nombre}'),'${data.codcms}','${data.email}','${data.niveles}','${data.zona}',
	'${data.zonal}',UPPER('${data.canalatis}'),'${data.segmento}',UPPER('${data.entidad}'),UPPER('${data.nompuntoventa}'),'${data.codpuntoventa}','${data.codatis}','${data.canal}'
	)`,
		(error, info) => {
			//console.log("data in savesaleAgent " + util.inspect(data))
			var response = {}
			response['status'] = ""
			response['errors'] = 0;
			if (!error) {

				console.log(`tdp_sales_agent:: info :: OK :: ${info}`)
				m.connector.execute(`UPDATE ibmx_a07e6d02edaf552.user SET status = '1' WHERE ID = '${data.codatis}'`,
					(errorUser, infoUser) => {
						//console.log("data in savesaleAgent " + util.inspect(data))
						var response = {}
						response['status'] = ""
						response['errors'] = 0;
						if (!errorUser) {
							console.log(`user:: infoUser :: OK :: ${infoUser}`)
							response['status'] = "OK"
							callback(response)
						} else {

							console.log(`error ${errorUser}`)
							response['status'] = "FAILED"
							var count = 0;
							response['errorMessages'] = [];
							response['errorMessages'].push(errorUser)
							_.each(errorUser, function (e) {
								if (e) {
									count++;
									response['errorMessages'].push(e.message);
								}
							});
							response['errors'] = count;
							callback(response)
						}
					}
				)
			} else {
				console.log(`error ${error}`)
				response['status'] = "FAILED"
				var count = 0;
				response['errorMessages'] = [];
				response['errorMessages'].push(error)
				_.each(error, function (e) {
					if (e) {
						count++;
						response['errorMessages'].push(e.message);
					}
				});
				response['errors'] = count;
				//console.log(`response error::::: ${util.inspect(response)}`)
				callback(response)
			}
		}
	)
}

// Actualizar en tdp_sales_agent y user
TdpSalesAgentsConverter.prototype.updateSaleAgentUser = (data, m, callback) => {
	var that = this
	//console.log("data" + util.inspect(data))
	m.connector.execute(`UPDATE ibmx_a07e6d02edaf552.tdp_sales_agent 
						SET 
							dni = '${data.dni}', 
							apepaterno = UPPER('${data.apepaterno}'),
							apematerno = UPPER('${data.apematerno}'),
							nombre = UPPER('${data.nombre}'),
							niveles = '${data.niveles}',
							canal = UPPER('${data.canal}'),
							entidad = UPPER('${data.entidad}'),
							nompuntoventa = UPPER('${data.nompuntoventa}'),
							codatis = '${data.codatis}',
							zonal = '${data.zonal}',
							zona = '${data.zona}'
						WHERE  dni = '${data.dni}'`,
		(error, info) => {
			//console.log("data in savesaleAgent " + util.inspect(data))
			var response = {}
			response['status'] = ""
			response['errors'] = 0;
			if (!error) {
				console.log(`info :::::::: ${info}`)
				m.connector.execute(`UPDATE ibmx_a07e6d02edaf552.user
									SET 
										status = '1',
										ID = '${data.codatis}',
										pwd = case when '${data.password}' = 'undefined' then pwd else md5('${data.password}') end
									WHERE ID = '${data.codatis}'`,
					(err, result) => {
						if (!err) {
							console.log("Se actualizo: [" + result.affectedRows + "] usuarios de su estado");
							console.log(`info :::::::: ${result}`)
							response['status'] = "OK"
							callback(response)
						} else {
							console.log(`error :::::::: ${err}`)
							response['status'] = "FAILED"
							var count = 0;
							response['errorMessages'] = [];
							response['errorMessages'].push(err)
							_.each(err, function (e) {
								if (e) {
									count++;
									response['errorMessages'].push(e.message);
								}
							});
							response['errors'] = count;
							//console.log(`response error::::: ${util.inspect(response)}`)
							callback(response)
						}
					});
			} else {
				console.log(`error ${error}`)
				response['status'] = "FAILED"
				var count = 0;
				response['errorMessages'] = [];
				response['errorMessages'].push(error)
				_.each(error, function (e) {
					if (e) {
						count++;
						response['errorMessages'].push(e.message);
					}
				});
				response['errors'] = count;
				//console.log(`response error::::: ${util.inspect(response)}`)
				callback(response)
			}
		}
	)
}

TdpSalesAgentsConverter.prototype.preloadSalesAgentUser = function (req, callback) {
	var that = this;
	this.processFileSalesAgentUser(req, this.datasource, function (responseData) {
		console.log('file prcessed');
		that.processWebUsers(responseData.processed, req.accessToken.userId, function (webprocessed) {
			var data = {
				processed: [],
				error: [],
				totalCount: 0,
				errorCount: 0
			};
			var error = responseData.error;

			_.each(webprocessed.dataSkiped, function (value) {
				error.push(_.extend({ errors: ['No tiene permisos para actualizar el registro para ' + value.entidad + '.'] }, value));
			});

			data.processed = webprocessed.dataToProcess;
			data.error = error;
			data.totalCount = webprocessed.dataToProcess.length;
			data.errorCount = responseData.errorCount + webprocessed.dataSkiped.length;
			data.callsMap = webprocessed.callsMap;
			data.resetData = responseData.resetData
			callback(data);
		});
	});
	return callback.promise;
}

TdpSalesAgentsConverter.prototype.preloadSalesSuperUser = function (req, callback) {
	var that = this;
	this.processFileSalesSuperUser(req, this.datasource, function (responseData) {
		console.log('file prcessed');
		that.processWebUsers(responseData.processed, req.accessToken.userId, function (webprocessed) {
			var data = {
				processed: [],
				error: [],
				totalCount: 0,
				errorCount: 0
			};
			var error = responseData.error;

			_.each(webprocessed.dataSkiped, function (value) {
				error.push(_.extend({ errors: ['No tiene permisos para actualizar el registro para ' + value.entidad + '.'] }, value));
			});

			data.processed = webprocessed.dataToProcess;
			data.error = error;
			data.totalCount = webprocessed.dataToProcess.length;
			data.errorCount = responseData.errorCount + webprocessed.dataSkiped.length;
			data.callsMap = webprocessed.callsMap;
			data.resetData = responseData.resetData
			callback(data);
		});
	});
	return callback.promise;
}



TdpSalesAgentsConverter.prototype.processFileSalesSuperUser = function (req, ds, cb) {
	var _this = this;
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		var responseData = { processError: 'no se pudo procesar el archivo' };
		var f = files['data'];
		var resetData = fields['resetdata'];
		if (f) {
			var contents;
			try { contents = xlsx.parse(f.path); } catch (e) { contents = false; console.error(e); }
			if (contents) {
				var cartilla = contents[0];
				var xlsxData = cartilla.data;
				responseData = _this.processDataSalesSuperUser(xlsxData);
			}
			responseData['resetData'] = resetData;
			cb(responseData);
		}
	});
	return cb.promise;
}


TdpSalesAgentsConverter.prototype.processFileSalesAgentUser = function (req, ds, cb) {
	var _this = this;
	var form = new formidable.IncomingForm();
	form.parse(req, function (err, fields, files) {
		var responseData = { processError: 'no se pudo procesar el archivo' };
		var f = files['data'];
		var resetData = fields['resetdata'];
		if (f) {
			var contents;
			try { contents = xlsx.parse(f.path); } catch (e) { contents = false; console.error(e); }
			if (contents) {
				var cartilla = contents[0];
				var xlsxData = cartilla.data;
				responseData = _this.processDataSalesAgentUser(xlsxData);
			}
			responseData['resetData'] = resetData;
			cb(responseData);
		}
	});
	return cb.promise;
}

TdpSalesAgentsConverter.prototype.processDataSalesAgentUser = function (fileData) {
	var _this = this;
	var dni = {};
	var cms = {};
	var atis = {};
	var data = {
		processed: [],
		error: [],
		totalCount: 0,
		errorCount: 0
	};
	var cleanData = this.sanitize(fileData);
	var size = cleanData.length;
	var errorCount = 0;
	_.each(cleanData, function (row) {
		var p = _this.rowToObjSalesAgentUser(row);
		var errors = _this.validate(p);
		if (!_.isUndefined(p.dni)) {
			if (_.isUndefined(dni[p.dni])) {
				dni[p.dni] = p;
			} else {
				errors.push("DNI duplicado");
			}
		}
		if (_.isUndefined(p.password)) {
			errors.push("Password vacio");
		}
		if (!_.isUndefined(p.codatis)) {
			if (_.isUndefined(atis[p.codatis])) {
				atis[p.codatis] = p;
			} else {
				errors.push("Código ATIS duplicado");
			}
		}
		if (_.isEmpty(errors)) {
			data.processed.push(p);
		} else {
			data.error.push(_.extend({ errors: errors }, p));
			errorCount++;
		}
	});
	data.totalCount = size;
	data.errorCount = errorCount;
	return data;
};


TdpSalesAgentsConverter.prototype.processDataSalesSuperUser = function (fileData) {
	var _this = this;
	var dni = {};
	var cms = {};
	var atis = {};
	var data = {
		processed: [],
		error: [],
		totalCount: 0,
		errorCount: 0
	};
	var cleanData = this.sanitize(fileData);
	var size = cleanData.length;
	var errorCount = 0;
	_.each(cleanData, function (row) {
		var p = _this.rowToObjSalesAgentUser(row);
		var errors = _this.validate(p);
		if (!_.isUndefined(p.dni)) {
			if (_.isUndefined(dni[p.dni])) {
				dni[p.dni] = p;
			} else {
				errors.push("DNI duplicado");
			}
		}
		if (_.isUndefined(p.password)) {
			errors.push("Password vacio");
		}
		if (_.isUndefined(p.niveles)) {
			errors.push("Nivel vacio");
		} else {
			if (p.niveles != 4 && p.niveles != 5)
				errors.push("Nivel debe ser 4 o 5");
		}
		if (!_.isUndefined(p.codatis)) {
			if (_.isUndefined(atis[p.codatis])) {
				atis[p.codatis] = p;
			} else {
				errors.push("Código ATIS duplicado");
			}
		}
		if (_.isEmpty(errors)) {
			data.processed.push(p);
		} else {
			data.error.push(_.extend({ errors: errors }, p));
			errorCount++;
		}
	});
	data.totalCount = size;
	data.errorCount = errorCount;
	return data;
};


TdpSalesAgentsConverter.prototype.rowToObjSalesAgentUser = function (row) {
	//Sprint 5
	var obj = { id: 0 };
	obj['dni'] = row[0];
	obj['apepaterno'] = row[1];
	obj['apematerno'] = row[2];
	obj['nombre'] = row[3];
	obj['codatis'] = row[4];
	obj['password'] = row[5];
	obj['codcms'] = '0';
	obj['email'] = '';
	obj['niveles'] = row[6];
	obj['zona'] = row[11];
	obj['zonal'] = row[12];
	obj['canalatis'] = row[7];
	obj['canal'] = row[7];
	obj['userCanalCodigo'] = '';
	obj['segmento'] = '';
	obj['entidad'] = row[8];
	obj['nompuntoventa'] = row[9];
	obj['codpuntoventa'] = row[10];
	obj['canalEquivalenciaCampania'] = '';
	obj['entidadEquivalenciaCampania'] = '';
	obj['userFuerzaVenta'] = '';
	return obj;
};

TdpSalesAgentsConverter.prototype.uploadSalesAgentUser = function (req, m, cb) {
	var transactionId = new Date().getTime();
	//var ServiceCallEvents = this.app.models.ServiceCallEvents;
	var _this = this;
	this.app.io.emit('upload.status', {
		transactionId: transactionId,
		progress: 0,
		response: {}
	});

	console.log('before processed')
	_this.processFileWebSalesAgentUser(req, function (d) {
		console.log('processed', d.resetData)
		cb({
			transactionId: transactionId
		});

		console.log("saveAppUsers  d.processed ::::" + JSON.stringify(d))
		console.log("saveAppUsers  req.accessToken ::::" + req.accessToken.userId)

		_this.saveSalesAgentUser(d, m, function (response) {
			_this.app.io.emit('upload.status', {
				transactionId: transactionId,
				progress: 100,
				response: response
			});
		});
	});
	// });
	return cb.promise;
}


TdpSalesAgentsConverter.prototype.uploadSalesSuperUser = function (req, m, cb) {
	var transactionId = new Date().getTime();
	//var ServiceCallEvents = this.app.models.ServiceCallEvents;
	var _this = this;
	this.app.io.emit('upload.status', {
		transactionId: transactionId,
		progress: 0,
		response: {}
	});

	console.log('before processed')
	_this.processFileWebSalesAgentUser(req, function (d) {
		console.log('processed', d.resetData)
		cb({
			transactionId: transactionId
		});

		console.log("saveAppUsers  d.processed ::::" + JSON.stringify(d))
		console.log("saveAppUsers  req.accessToken ::::" + req.accessToken.userId)

		_this.saveSalesSuperUser(d, m, function (response) {
			_this.app.io.emit('upload.status', {
				transactionId: transactionId,
				progress: 100,
				response: response
			});
		});
	});
	// });
	return cb.promise;
}

TdpSalesAgentsConverter.prototype.processFileWebSalesAgentUser = function (req, callback) {
	var that = this;
	this.processFileSalesAgentUser(req, this.datasource, function (responseData) {
		console.log('file prcessed');
		that.processWebUsers(responseData.processed, req.accessToken.userId, function (webprocessed) {
			var data = {
				processed: [],
				error: [],
				totalCount: 0,
				errorCount: 0
			};
			var error = responseData.error;

			_.each(webprocessed.dataSkiped, function (value) {
				error.push(_.extend({ errors: ['No tiene permisos para actualizar el registro para ' + value.entidad + '.'] }, value));
			});

			data.processed = webprocessed.dataToProcess;
			data.error = error;
			data.totalCount = webprocessed.dataToProcess.length;
			data.errorCount = responseData.errorCount + webprocessed.dataSkiped.length;
			data.callsMap = webprocessed.callsMap;
			data.resetData = responseData.resetData
			callback(data);
		});
	});
	return callback.promise;
}

TdpSalesAgentsConverter.prototype.saveSalesAgentUser = function (data, m, callback) {
	console.log('iniciando guardar datos');
	var that = this;
	//cascad
	let parametersModel = this.app.models.Parameters;
	var toUpdate = data.processed;

	console.log('procesando info');
	if (toUpdate.length < 1) {
		callback({});
		return;
	}

	var where = {
		or: [
			{ niveles: '3' }
		]
	};

	that.model.destroyAll(where, function (err, info) {
		console.log('eliminados ' + info.count + ' registros');

		that.model.create(toUpdate, function (err, models) {
			var response = {};
			response['errors'] = 0;
			if (err) {
				console.error(err)
				var count = 0;
				response['errorMessages'] = [];
				_.each(err, function (e) {
					if (e) {
						count++;
						response['errorMessages'].push(e.message + " " + e.detail);
					}
				});
				response['errors'] = count;
			}
			if (_.isArray(models)) {
				response['registered'] = models.length - response.errors;
			}
			console.log('registrados: ' + response.registered + ' errores: ' + response.errors);

			var countUserError = 0;
			_.each(toUpdate, function (userItem) {
				m.connector.execute(`INSERT INTO ibmx_a07e6d02edaf552."user"
				(id, pwd, phone, imei, resetpwd, previouspwd, registeredtime, lastupdatetime, status, nrointentos)
				VALUES ('${userItem.codatis}', md5('${userItem.password}'), '', '', '0', '', now(), now(), '1', 0)
				ON CONFLICT (id) DO UPDATE 
				SET pwd = excluded.pwd, status = '1', lastupdatetime = now(), nrointentos = 0`,
					(errorUser, infoUser) => {
						if (!errorUser) {
							console.log(`user:: infoUser :: OK :: ${infoUser}`)
						} else {
							console.log(`error ${errorUser}`)
						}
					}
				)

			});

			callback(response);
		});
	});

};

/* PARA NIVELES 4 Y 5 */

TdpSalesAgentsConverter.prototype.saveSalesSuperUser = function (data, m, callback) {
	console.log('iniciando guardar datos');
	var that = this;
	//cascad
	let parametersModel = this.app.models.Parameters;
	var toUpdate = data.processed;

	console.log('procesando info');
	if (toUpdate.length < 1) {
		callback({});
		return;
	}

	var where = {
		or: [
			{ niveles: '2' },
			{ niveles: '4' },
			{ niveles: '5' }
		]
	};

	that.model.destroyAll(where, function (err, info) {
		console.log('eliminados ' + info.count + ' registros');

		that.model.create(toUpdate, function (err, models) {
			var response = {};
			response['errors'] = 0;
			if (err) {
				console.error(err)
				var count = 0;
				response['errorMessages'] = [];
				_.each(err, function (e) {
					if (e) {
						count++;
						response['errorMessages'].push(e.message + " " + e.detail);
					}
				});
				response['errors'] = count;
			}
			if (_.isArray(models)) {
				response['registered'] = models.length - response.errors;
			}
			console.log('registrados: ' + response.registered + ' errores: ' + response.errors);

			var countUserError = 0;
			_.each(toUpdate, function (userItem) {
				m.connector.execute(`INSERT INTO ibmx_a07e6d02edaf552."user"
				(id, pwd, phone, imei, resetpwd, previouspwd, registeredtime, lastupdatetime, status, nrointentos)
				VALUES ('${userItem.codatis}', md5('${userItem.password}'), '', '', '0', '', now(), now(), '1', 0)
				ON CONFLICT (id) DO UPDATE 
				SET pwd = excluded.pwd, status = '1', lastupdatetime = now(), nrointentos = 0`,
					(errorUser, infoUser) => {
						if (!errorUser) {
							console.log(`user:: infoUser :: OK :: ${infoUser}`)
						} else {
							console.log(`error ${errorUser}`)
						}
					}
				)

			});

			callback(response);
		});
	});
};

module.exports = TdpSalesAgentsConverter;
