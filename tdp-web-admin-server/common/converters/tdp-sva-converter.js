var _ = require('underscore'),
  formidable = require('formidable'),
  xlsx = require('node-xlsx');
TdpSvaAdditional = require('../models/tdp-sva-additional')

var TdpSvasConverter = function () { }

var x = 1;

TdpSvasConverter.prototype.objToRow = function (obj) {
  var arr = [obj['prodtypecode'], obj['code'],
  obj['description'], obj['unit'], obj['cost'],
  obj['paymentdescription'], obj['type'], obj['svacode'], obj['financingmonth'],
  obj['tecnologiasva'], obj['sistemas'], obj['comercializable'], obj['tecnologiatvorigen'] ];
  return arr;
};

TdpSvasConverter.prototype.rowToObj = function (row) {
  var obj = { id: x };
  obj['prodtypecode'] = row[0];
  obj['code'] = row[1];
  obj['description'] = row[2];
  obj['unit'] = row[3];
  obj['cost'] = Number(row[4]).toFixed(2);
  obj['paymentdescription'] = row[5];
  obj['type'] = row[6];
  obj['svaCode'] = row[7];
  obj['financingmonth'] = row[8];
  // se adiciona lectura de canal y id
  obj['canal'] = row[9];

  let svaid = row[10];
  if (svaid == null || svaid == '') {
    obj['svaid'] = ' ';
  } else {
    obj['svaid'] = svaid;
  }

      obj['tecnologiasva'] = row[11];
      obj['sistemas'] = row[12];
      obj['comercializable'] = row[13];
      obj['tecnologiatvorigen'] = row[14];

  x = x + 1;
  return obj;
};

TdpSvasConverter.prototype.validate = function (obj) {
  var errors = [];
  var count = 0;
  var isValid = true;
  _.each(obj, function (v, k) {

    if (k !== "financingmonth") {
      isValid = isValid && (_.isNumber(v) || !_.isEmpty(v));
      if (!(_.isNumber(v) || !_.isEmpty(v))) {
        errors.push('Valor no valido para ' + k + ': ' + v);
      }
    }
    count++;
  });
  isValid = isValid && count > 7;
  if (!isValid) {
    errors.push('Fila incompleta');
  }
  return errors;
};

TdpSvasConverter.prototype.sanitize = function (dataArr) {
  var result = [];
  _.each(dataArr.slice(1), function (row) {
    if (_.size(row) > 0) {
      result.push(row);
    }
  });
  return result;
};

TdpSvasConverter.prototype.processData = function (fileData) {
  var _this = this;
  var data = {
    processed: [],
    error: [],
    totalCount: 0,
    errorCount: 0
  };
  var cleanData = this.sanitize(fileData);
  var size = cleanData.length;
  var errorCount = 0;
  var correcto = true;
  //var equipamiento = false;
  _.each(cleanData, function (row) {
    //console.log("row info ",row);
    var p = _this.rowToObj(row);
    var errors = _this.validate(p);
    if (_.isEmpty(errors)) {
      //console.log('error info ' + p.code.toString())
      if (p.type != 'STR' && p.type != 'INT') {
        p['error'] = 'El campo type no es STR o INT';
        correcto = false;
      } else if (isNaN(p.cost)) {
        p['error'] = 'El campo Cost no es un numero';
        correcto = false;
      } else if ((p.prodtypecode.toString()).length > 2) {
        p['error'] = 'El campo prodtypeCode excede a 2 caracteres';
        correcto = false;
      } else if ((p.code.toString()).length > 12) {
        p['error'] = 'El campo code excede a 12 caracteres';
        correcto = false;
      } else if ((p.description.toString()).length > 45) {
        p['error'] = 'El campo description excede a 45 caracteres';
        correcto = false;
      } else if ((p.unit.toString()).length > 45) {
        //console.log('error info 2 ' + p.code.toString())
        p['error'] = 'El campo unit excede a 45 caracteres';
        correcto = false;
      } else if ((p.paymentdescription.toString()).length > 100) {
        p['error'] = 'El campo payment excede a 100 caracteres';
        correcto = false;
      } else if ((p.type.toString()).length > 3) {
        p['error'] = 'El campo type excede a 3 caracteres';
        correcto = false;
      } else if ((p.svaCode.toString()).length > 50) {
        p['error'] = 'El campo svaCode excede a 3 caracteres';
        correcto = false;
      }

      if (correcto) {
        data.processed.push(p);
      } else {
        data.error.push(_.extend({ errors: errors }, p));
        errorCount++;
      }
      correcto = true;
    } else {
      data.error.push(_.extend({ errors: errors }, p));
      errorCount++;
    }
  });
  data.totalCount = size;
  data.errorCount = errorCount
  return data;
};


TdpSvasConverter.prototype.processFile = function (req, cb) {
  var _this = this;
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    var responseData = { processError: 'no se pudo procesar el archivo' };
    var f = files['data'];
    if (f) {
      var contents;
      try { contents = xlsx.parse(f.path); } catch (e) { contents = false; console.error("error processFile" + e); }
      if (contents) {
        var cartilla = contents[0];
        var xlsxData = cartilla.data;
        responseData = _this.processData(xlsxData);
      }
      cb(responseData);
    }
  });
  return cb.promise;
}

TdpSvasConverter.prototype.insertData = function (req, ds, m, cb) {
  X = 1;
  let tdpSvaAdditional = m.app.models.TdpSvaAdditional
  var _this = this;
  this.processFile(req, function (d) {
    console.log(JSON.stringify(d))
    ds.connector.execute('truncate ibmx_a07e6d02edaf552.tdp_sva;', function () {
      // Se adiciona boorrado de nueva tabla tdp_sva_additional
      ds.connector.execute('truncate ibmx_a07e6d02edaf552.tdp_sva_additional', () => {
        m.create(d.processed, function (err, createdModel, created) {
          console.log("Error InsertData: " + err);
          var response = { total: 1 };
          if (_.isArray(createdModel)) {
            response['registered'] = createdModel.length;
          }
          // Se adiciona procesado de datos adicionales de sva
          _this.processAditionalData(d.processed, ds, tdpSvaAdditional);

          console.log("log insertData" + req.accessToken);
          m.app.models.ServiceCallEvents.create({
            id: 0,
            eventDatetime: new Date(),
            tag: 'ADMINIST',
            msg: 'Archivo de carga para producto procesado, cargados ' + createdModel.length + '@' + new Date(),
            username: 'tester'
          }, function () { });
          cb(response);
        });
      });
    });
  });
  return cb.promise;
}

//funciones para procesamiento de datos adicionales de sva
TdpSvasConverter.prototype.processAditionalData = function (fileData, dataSource, m) {
  var _this = this;
  let dataFinal = []
  _.each(fileData, function (row) {
    console.log("row " + JSON.stringify(row))
    var canales = row['canal'].replace(/ñ/gi, "n").split('-');
    _this.addAditionalData(row, 6527, canales, dataFinal);
  });
  _this.processAditional(dataFinal, dataSource, m);
}

TdpSvasConverter.prototype.addAditionalData = function (row, aditionalCode, aditionalListPerRow, dataFinal) {
  for (let i = 0; i < aditionalListPerRow.length; i++) {
    obj = {
      "sva_id": row.id,
      "parameter_id": aditionalCode,
      "value": aditionalListPerRow[i].trim().toUpperCase()
    }
    dataFinal.push(obj);
  }
}

TdpSvasConverter.prototype.processAditional = function (dataList, dataSource, model) {
  model.create(dataList, (error, info) => {
    var response = {}
    if (!error) {
      response['status'] = info
      response['errors'] = ""
    } else {
      response['status'] = info
      response['errors'] = error
    }
    return response
  })

}

module.exports = TdpSvasConverter;
