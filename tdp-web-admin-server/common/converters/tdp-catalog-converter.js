var _ = require('underscore'),
    formidable = require('formidable'),
    xlsx = require('node-xlsx'),
    TdpCatalogAdditional = require('../models/tdp-catalog-additional')

var TdpCatalogsConverter = function() {}
var x = 1;

TdpCatalogsConverter.prototype.objToRow = function(obj) {
    var arr = [obj['idProduct'], obj['commercialoperation'],
        obj['segmento'], obj['canal'], obj['entidad'],
        obj['provincia'], obj['distrito'],
        obj['campaign'], obj['campaingcode'], obj['priority'], obj['prodtypecode'], obj['producttype'],
        obj['prodcategorycode'], obj['productcategory'], obj['productcode'], obj['productname'],
        obj['svatv'], obj['svainternet'], obj['svapaquetelinea'],
        obj['tiporegistro'], obj['discount'], obj['price'], obj['promprice'], obj['monthperiod'], obj['installcost'], obj['linetype'], obj['paymentmethod'], obj['cashprice'], obj['financingcost'], obj['financingmonth'], obj['equipmenttype'], obj['returnmonth'], obj['returnperiod'], obj['internetspeed'], obj['promspeed'], obj['periodpromspeed'], obj['internettech'], obj['tvsignal'], obj['tvtech'], obj['equiplinea'], obj['equipinternet'], obj['equiptv']
    ];
    return arr;
};

TdpCatalogsConverter.prototype.rowToObj = function(row) {
    var obj = { id: x };
    obj['idProduct'] = row[0];
    obj['commercialoperation'] = row[1];
    obj['segmento'] = row[2];
    obj['canal'] = row[3];
    obj['entidad'] = row[4];
    obj['provincia'] = row[5];
    obj['distrito'] = row[6];
    obj['campaign'] = row[7];
    obj['campaingcode'] = row[8];
    obj['priority'] = row[9];
    obj['prodtypecode'] = row[10];
    obj['producttype'] = row[11];
    obj['prodcategorycode'] = row[12];
    obj['productcategory'] = row[13];
    obj['productcode'] = row[14];
    obj['productname'] = row[15];
    obj['svatv'] = row[16]; //[15] servicios adicionales en paquete tv
    obj['svainternet'] = row[17]; //[16] servicios adicionales en paquete internet
    obj['svapaquetelinea'] = row[18]; //[17] servicios adicionales en paquete linea
    obj['tiporegistro'] = row[19] //[18] tipo registro
    obj['discount'] = row[20];
    obj['price'] = row[21];
    obj['promprice'] = row[22];
    obj['monthperiod'] = row[23];
    obj['installcost'] = row[24];
    obj['linetype'] = row[25];
    obj['paymentmethod'] = row[26];
    obj['cashprice'] = row[27];
    obj['financingcost'] = row[28];
    obj['financingmonth'] = row[29];
    obj['equipmenttype'] = row[30];
    obj['returnmonth'] = row[31];
    obj['returnperiod'] = row[32];
    obj['internetspeed'] = row[33];
    obj['promspeed'] = row[34] //[33] velocidad promocional
    obj['periodpromspeed'] = row[35] //[34] periodo velocidad promocional
    obj['internettech'] = row[36];
    obj['tvsignal'] = row[37];
    obj['tvtech'] = row[38];
    obj['equiplinea'] = row[39] //[38] equipamiento linea
    obj['equipinternet'] = row[40] //[39] equipamiento internet
    obj['equiptv'] = row[41] //[40] equipamiento tv

    x = x + 1;
    return obj;
};

TdpCatalogsConverter.prototype.rowToList = function(row, delimitador) {
    var listaResultado = [];
    if (row != undefined) {
        if (row != null) {
            var i = 0;
            var listaaux = row.trim().split(delimitador);
            _.each(listaaux, function(rowaux) {
                listaResultado[i] = rowaux.trim();
                i++;
            });
        }
    }
    return listaResultado;
};

TdpCatalogsConverter.prototype.setVacio = function(a) {
    var b;
    if (a == '' || a == '-') {
        b = 'Vacio';
    } else {
        b = a;
    }
    return b;
};

TdpCatalogsConverter.prototype.validate = function(obj) {
    var errors = [];
    var count = 0;
    var isValid = true;
    _.each(obj, function(v, k) {
        isValid = isValid && (_.isNumber(v) || !_.isEmpty(v));
        if (!(_.isNumber(v) || !_.isEmpty(v))) {
            errors.push('Valor no valido para ' + k + ': ' + v);
        }
        count++;
    });
    isValid = isValid && count > 26;
    if (!isValid) {
        errors.push('Fila incompleta');
    }
    return errors;
};

TdpCatalogsConverter.prototype.sanitize = function(dataArr) {
    var result = [];
    _.each(dataArr.slice(1), function(row) {
        if (_.size(row) > 0) {
            result.push(row);
        }
    });
    return result;
};
/*
TdpCatalogsConverter.prototype.processData = function (fileData) {
	var _this = this;
	var data = {
		processed: [],
		error: [],
		totalCount: 0,
		errorCount: 0
	};
	var cleanData = this.sanitize(fileData);
	var size = cleanData.length;
	var errorCount = 0;
	_.each(cleanData, function (row) {
		var p = _this.rowToObj(row);
		var errors = _this.validate(p);
		if (_.isEmpty(errors)) {
			data.processed.push(p);
		} else {
			data.error.push(_.extend({ errors: errors }, p));
			errorCount++;
		}
	});
	data.totalCount = size;
	data.errorCount = errorCount;
	return data;
};
*/
TdpCatalogsConverter.prototype.processFile = function(req, cb) {
    var _this = this;
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var responseData = { processError: 'no se pudo procesar el archivo' };
        var f = files['data'];
        if (f) {
            var contents;
            try { contents = xlsx.parse(f.path); } catch (e) { contents = false;
                console.error(e); }
            if (contents) {
                var cartilla = contents[0];
                var xlsxData = cartilla.data;
                responseData = _this.processData(xlsxData);
            }
            cb(responseData);
        }
    });
    return cb.promise;
}

TdpCatalogsConverter.prototype.insertData = function(req, ds, m, cb) {
    x = 1;
    var ServiceCallEvents = m.app.models.ServiceCallEvents;
    let tdpCatalogAdditional = m.app.models.TdpCatalogAdditional
    var _this = this;
    this.processFile(req, function(d) {
        ds.connector.execute('truncate ibmx_a07e6d02edaf552.tdp_catalog;', function() {
            ds.connector.execute('truncate ibmx_a07e6d02edaf552.tdp_catalog_aditional', () => {
                m.create(d.processed, function(err, createdModel, created) {
                    var response = { total: 1 };
                    if (_.isArray(createdModel)) {
                        response['registered'] = createdModel.length;
                    }

                    ///////registrar log Carga de Cartilla de productos - tdp_logcartilla
                    registrarLogcartilla(ds, d.processed);
                    /////////////////////////////////////////////////////////////////////

                    _this.processAditionalData(d.processed, ds, tdpCatalogAdditional);


                    ServiceCallEvents.log(req.accessToken, 'Archivo de carga para producto procesado, cargados ' + createdModel.length + '@' + new Date());

                    cb(response);
                });
            })
        });
    });
    return cb.promise;
}



registrarLogcartilla = function(ds, processed) {
    if (processed != null && processed != undefined) {
        //var detalle = JSON.stringify(processed);//objeto json de los productos
        var detalle = "Cantidad de productos cargados";
        let tamanio = processed.length;
        //const fecharegistro = `to_timestamp(${Date.now()} / 1000.0)`;
        //let query = `INSERT INTO ibmx_a07e6d02edaf552.tdp_logcartilla(fecharegistro, detalle, tamanio) values (${fecharegistro}, '${detalle}', ${tamanio})`;
        let query = `INSERT INTO ibmx_a07e6d02edaf552.tdp_logcartilla(detalle, tamanio) values ('${detalle}', ${tamanio})`;

        ds.connector.execute(query, (error, info) => {
            if (!error) {
                console.log('se registro log de cartilla');
            } else {
                console.log('error al registrar el log de cartilla');
                console.log(error);
            }
        });

    }

}



TdpCatalogsConverter.prototype.generate = function(dataArr) {

    var _this = this;
    var result = [];
    var i = 0;

    _.each(dataArr, function(row) {

        if (_.size(row) > 0) {
            /*
            var listacanal = _this.rowToList(row[2], '-');
            _.each(listacanal, function (canal) {
            	//insert into tdp-catalog-aditional(productid, parameterid, value)
            	//values(x, 6527, canal);
            });
            */
            var fila = [35];

            fila[0] = row[0] // Id
            fila[1] = row[1] // operacion comercial
            fila[2] = row[2] // segmento
            fila[3] = row[3] // canal
            fila[4] = row[4] // entidad
            fila[5] = row[5] // provincia
            fila[6] = row[6] // distrito
            fila[7] = row[7]
            fila[8] = row[8]
            fila[9] = row[9]
            fila[10] = row[10];
            fila[11] = row[11];
            fila[12] = row[12];
            fila[13] = row[13];
            fila[14] = row[14];
            fila[15] = row[15];
            fila[16] = row[16]; // servicios adicionales en paquete tv
            fila[17] = row[17]; // servicios adicionales en paquete internet
            fila[18] = row[18]; // servicios adicionales en paquete linea
            fila[19] = row[19];
            fila[20] = row[20];
            fila[21] = row[21];
            fila[22] = row[22];
            fila[23] = row[23];
            fila[24] = row[24];
            fila[25] = row[25];
            fila[26] = row[26];
            fila[27] = row[27];
            fila[28] = row[28];
            fila[29] = row[29];
            fila[30] = row[30];
            fila[31] = row[31];
            fila[32] = row[32];
            fila[33] = row[33];
            fila[34] = row[34];
            fila[35] = row[35];
            fila[36] = row[36];
            fila[37] = row[37];
            fila[38] = row[38]
            fila[39] = row[39]
            fila[40] = row[40]
            fila[41] = row[41]

            result[i] = fila;
            i++;

        }
    });
    return result;
};

TdpCatalogsConverter.prototype.processAditionalData = function(fileData, dataSource, m) {
    var _this = this;
    let dataFinal = []
    _.each(fileData, function(row) {
        console.log("row " + JSON.stringify(row))
        var canales = row['canal'].replace(/ñ/gi, "n").split('-');
        var entidades = row['entidad'].replace(/ñ/gi, "n").split(',');
        var campanas = row['campaign'].replace(/ñ/gi, "n").split('-');
        //var svastv = row['svatv'].split(',');
        //var svasinternet = row['svainternet'].split(',');
        //var svaspaquetelinea = row['svapaquetelinea'].split(',');
        var provincias = row['provincia'].replace(/ñ/gi, "n").split('-');
        var distrito = row['distrito'].replace(/ñ/gi, "n").split('-');
        /*console.log("canales ",canales)
        console.log("entidades ",entidades)
        console.log("campanas ",campanas)
        console.log("provincias ",provincias)
        console.log("distrito ",distrito)*/
        var ubicaciones = [];

        //cargamos el array de ubicaciones
        var indiceUbicacion = 0;
        _.each(provincias, function(provincia) {
            if ('-' == row['distrito']) {
                //si es que no hay distritos para esta provincia entonces guardamos "provinciax-todo"
                ubicaciones.push(provincia.trim() + "-todo");
            } else {
                if (distrito[indiceUbicacion]) {
                    var distritosPorProvincia = distrito[indiceUbicacion].split(',');
                    indiceUbicacion++;
                    _.each(distritosPorProvincia, function(distritoPorProvincia) {
                        ubicaciones.push(provincia.trim() + "-" + distritoPorProvincia.trim());
                    });
                }
            }
        });
        _this.addAditionalData(row, 6527, canales, dataFinal);
        _this.addAditionalData(row, 6528, entidades, dataFinal);
        _this.addAditionalData(row, 6529, ubicaciones, dataFinal);
        _this.addAditionalData(row, 6530, campanas, dataFinal);
    });
    _this.processAditional(dataFinal, dataSource, m);
}

TdpCatalogsConverter.prototype.addAditionalData = function(row, aditionalCode, aditionalListPerRow, dataFinal) {
    for (let i = 0; i < aditionalListPerRow.length; i++) {
        obj = {
            "product_id": row.id,
            "parameter_id": aditionalCode,
            "value": aditionalListPerRow[i].trim().toUpperCase()
        }
        dataFinal.push(obj);
    }

}

crearLista = (row, aditionalCode, dataList) => {
    let dataInList = []
    for (let i = 0; i < aditionalCode.length; i++) {
        let obj = {}
        for (let j = 0; j < dataList.length; j++) {

            for (let x = 0; x < dataList[j].length; x++) {
                obj = {
                    "prod_id": row.id,
                    "parameter_id": aditionalCode[i],
                    "value": dataList[j][x]
                }
            }

        }
        dataInList.push(obj)
    }
    return dataInList
}

TdpCatalogsConverter.prototype.processAditional = function(dataList, dataSource, model) {

    model.create(dataList, (error, info) => {
        var response = {}
        if (!error) {
            response['status'] = info
            response['errors'] = ""
        } else {
            response['status'] = info
            response['errors'] = error
        }
        return response
    })

}

TdpCatalogsConverter.prototype.processData = function(fileData) {
    var _this = this;
    var data = {
        processed: [],
        error: [],
        totalCount: 0,
        errorCount: 0
    };
    var cleanData = this.sanitize(fileData);
    var errorCount = 0;
    var listarows = this.generate(cleanData);
    var size = listarows.length;
    _.each(listarows, function(row) {
        var p = _this.rowToObj(row);
        var errors = _this.validate(p);
        if (_.isEmpty(errors)) {
            data.processed.push(p);
        } else {
            data.error.push(_.extend({ errors: errors }, p));
            errorCount++;
        }
    });
    data.totalCount = size;
    data.errorCount = errorCount;
    return data;
};

module.exports = TdpCatalogsConverter;