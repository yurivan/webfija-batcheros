var _ = require('underscore'),
        formidable = require('formidable'),
        xlsx = require('node-xlsx');

var TdpCatalogsxSalesAgentConverter = function () {}
var x=1;

TdpCatalogsxSalesAgentConverter.prototype.objToRow = function (obj) {
  var arr = [obj['codatis'], obj['campaign']];
  return arr;
};

TdpCatalogsxSalesAgentConverter.prototype.rowToObj = function (row) {
  var obj = { id: x };
    obj['codatis'] = row[0];
    obj['campaign'] = row[1];
  x=x+1;
  return obj;
};

TdpCatalogsxSalesAgentConverter.prototype.validate = function (obj) {
  var errors = [];
  var count = 0;
  var isValid = true;
  _.each(obj, function (v, k) {
    isValid = isValid && (_.isNumber(v) || !_.isEmpty(v));
    if (!(_.isNumber(v) || !_.isEmpty(v))) {
      errors.push('Valor no valido para '+k+': '+v);
    }
    count++;
  });
  isValid = isValid && count > 1;
  if (!isValid) {
    errors.push('Fila incompleta');
  }
  return errors;
};

TdpCatalogsxSalesAgentConverter.prototype.sanitize = function (dataArr) {
  var result = [];
  _.each(dataArr.slice(1), function (row) {
    if (_.size(row) > 0) {
      result.push(row);
    }
  });
  return result;
};

TdpCatalogsxSalesAgentConverter.prototype.rowToList = function (row, delimitador) {

  var listaResultado = [];
    
  if ( row != undefined ) {

    if ( row != null ) {
     
        var i=0;
        var listaaux = row.trim().split(delimitador);
        _.each(listaaux, function (rowaux) {
              listaResultado[i] = rowaux.trim().replace(/ñ/gi, "n").toUpperCase();
              i++;
        });

    }
  }
  
  return listaResultado;

};

TdpCatalogsxSalesAgentConverter.prototype.setVacio = function (a) {

  var b;
  
  
  if (a == '' || a == '-') { 
    
      b = 'Todos';

  } else { 

      b = a;

  }
  
  return b;

};

TdpCatalogsxSalesAgentConverter.prototype.generate = function (dataArr) {

  var _this = this;
  var result = [];
  var i = 0;

  _.each(dataArr, function (row) {

    if (_.size(row) > 0) {

      if (row[0] != undefined ) {

        if ( row[0] != null ) {

          if ( row[0] != '' ) {
                  
            var campanias = _this.setVacio(row[1]);

            var listacampanias = _this.rowToList(campanias,'-');

            _.each(listacampanias, function (campania) {
            
              if ( campania != '' && _.size(campania) > 0) {
                  
                  var fila = [2];
                  fila[0]  = row[0];
                  fila[1]  = campania;
                  result[i] = fila;
                  i++;
                  
              }});
          } 
        }
      }
    }});
  return result;
};

TdpCatalogsxSalesAgentConverter.prototype.processData = function (fileData) {
  var _this = this;
    var data = {
      processed: [],
      error: [],
      totalCount: 0,
      errorCount: 0
    };
    var cleanData = this.sanitize(fileData);
    var errorCount = 0;
    var listarows = this.generate(cleanData);
    var size = listarows.length;
    _.each(listarows, function (row) {
      var p = _this.rowToObj(row);
      var errors = _this.validate(p);
      if (_.isEmpty(errors)) {
        data.processed.push(p);
      } else {
        data.error.push(_.extend({errors: errors}, p));
        errorCount++;
      }
    });
    data.totalCount = size;
    data.errorCount = errorCount;
    return data;
};

TdpCatalogsxSalesAgentConverter.prototype.processFile = function (req, cb) {
  var _this = this;
  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    var responseData = { processError: 'no se pudo procesar el archivo' };
    var f = files['data'];
    if (f) {
      var contents;
      try { contents = xlsx.parse(f.path); } catch (e) { contents = false; console.error(e); }
      if (contents) {
        var cartilla = contents[0];
        var xlsxData = cartilla.data;
        responseData = _this.processData(xlsxData);
      }
      cb(responseData);
    }
  });
  return cb.promise;
}

TdpCatalogsxSalesAgentConverter.prototype.insertData = function (req, ds, m, cb) {
  x=1;
  var ServiceCallEvents = m.app.models.ServiceCallEvents;
  this.processFile(req, function (d) {
    ds.connector.execute('truncate ibmx_a07e6d02edaf552.tdp_catalogxsalesagent;', function () {
      m.create(d.processed, function (err, createdModel, created) {
        var response = { total: 1 };
        if (_.isArray(createdModel)) {
          response['registered'] = createdModel.length;
        }
        ServiceCallEvents.log(req.accessToken, 'Archivo de carga para vendedor procesado, cargados ' + createdModel.length + '@'+new Date());
        cb(response);
      });
    });
  });
  return cb.promise;
}

module.exports = TdpCatalogsxSalesAgentConverter;
