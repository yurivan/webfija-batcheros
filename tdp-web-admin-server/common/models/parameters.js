var _ = require('underscore'),
    postgresql = require('pg')
module.exports = function (Parameters) {
    Parameters.modifyResource = (req, res, cb) => {
        
        const dataSource = Parameters.getDataSource()
            const status = req.body
            dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.parameters SET strvalue = '${status}' where element = 'tdp.bypass.migraciones.avve'`, (error, info) => {
                let data = []
                if (!error) {
                    data = info;
                    cb(null, data);
                } else {
                    cb(error, data);
                }
                console.log('log data', data);
            })
        return cb.promise
    }

    Parameters.getResource = (req,res,cb) => {
        const dataSource = Parameters.getDataSource()

        dataSource.connector.execute("select strvalue from ibmx_a07e6d02edaf552.parameters where element = 'tdp.bypass.migraciones.avve'", (error, info) => {
            let data = []
            if (!error) {
                data = info 
                cb(null, data)
            }else {
                cb(error, data)
            }
            console.log('log data ', data)
        })
        return cb.promise
    }

    Parameters.modifyCatalogAllProv = (req, res, cb) => {
        
        const dataSource = Parameters.getDataSource()
            const status = req.body
            dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.parameters SET strvalue = '${status}' where element = 'catalog.campanias.AllProv'`, (error, info) => {
                let data = []
                if (!error) {
                    data = info;
                    cb(null, data);
                } else {
                    cb(error, data);
                }
                console.log('log data', data);
            })
        return cb.promise
    }

    Parameters.getCatalogAllProv = (req,res,cb) => {
        const dataSource = Parameters.getDataSource()

        dataSource.connector.execute("select strvalue from ibmx_a07e6d02edaf552.parameters where element = 'catalog.campanias.AllProv'", (error, info) => {
            let data = []
            if (!error) {
                data = info 
                cb(null, data)
            }else {
                cb(error, data)
            }
            console.log('log data ', data)
        })
        return cb.promise
    }

    Parameters.modifySWAutTgest = (req, res, cb) => {
        
        const dataSource = Parameters.getDataSource()
            const status = req.body
            dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.parameters SET strvalue = '${status}' where element = 'tdp.sw.automatizador.tgestiona'`, (error, info) => {
                let data = []
                if (!error) {
                    data = info;
                    cb(null, data);
                } else {
                    cb(error, data);
                }
                console.log('log data', data);
            })
        return cb.promise
    }

    Parameters.getSWAutTgest = (req,res,cb) => {
        const dataSource = Parameters.getDataSource()

        dataSource.connector.execute("select strvalue from ibmx_a07e6d02edaf552.parameters where element = 'tdp.sw.automatizador.tgestiona'", (error, info) => {
            let data = []
            if (!error) {
                data = info 
                cb(null, data)
            }else {
                cb(error, data)
            }
            console.log('log data ', data)
        })
        return cb.promise
    }


    Parameters.modifyParameterValue = (req, res, cb) => {

        const dataSource = Parameters.getDataSource()
        const request = req.body
        console.log('request', request);
        const domain = request.domain
        const category = request.category
        const element = request.element
        const strvalue = request.strvalue
        dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.parameters SET strvalue = '${strvalue}' 
        where domain = '${domain}' and category = '${category}' and element = '${element}'`,
            (error, info) => {
                let data = []
                if (!error) {
                    data = info;
                    cb(null, data);
                } else {
                    cb(error, data);
                }
                console.log('log data', data);
            })
        return cb.promise
    }

    Parameters.updateStatusWhatsapp = (req, res, cb) => {

        const dataSource = Parameters.getDataSource()
        const status = req.body
        dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.parameters SET strvalue = '${status}' where element = 'whatsapp.contract'`, (error, info) => {
            let data = []
            if (!error) {
                data = info;
                cb(null, data);
            } else {
                cb(error, data);
            }
            console.log('log data', data);
        })
        return cb.promise
    }

    Parameters.getStatusWhatsapp = (req, res, cb) => {
        const dataSource = Parameters.getDataSource()

        dataSource.connector.execute("select strvalue from ibmx_a07e6d02edaf552.parameters where element = 'whatsapp.contract'", (error, info) => {
            let data = []
            if (!error) {
                data = info
                cb(null, data)
            } else {
                cb(error, data)
            }
            console.log('log data ', data)
        })
        return cb.promise
    }

    Parameters.getSalesCondition = (req, res, cb) => {
        const dataSource = Parameters.getDataSource()

        dataSource.connector.execute("SELECT id,auxiliar,domain,category,element,strvalue,strfilter FROM ibmx_a07e6d02edaf552.parameters WHERE domain ='CONTRATOCONDICION' ORDER BY 2", (error, info) => {
            let data = []
            if (!error) {
                data = info
                cb(null, data)
            } else {
                cb(error, data)
            }
            console.log('log data ', data)
        })
        return cb.promise
    }

    Parameters.modifySalesCondition = (req, res, cb) => {

        const dataSource = Parameters.getDataSource()
        const request = req.body
        console.log('request', request);
        const auxiliar = request.auxiliar
        const strvalue = request.strvalue
        const strfilter = request.strfilter
        const category = request.category
        dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.parameters SET auxiliar = '${auxiliar}', strvalue = '${strvalue}', strfilter = '${strfilter}'
        where category = '${category}'`,
            (error, info) => {
                let data = []
                if (!error) {
                    data = info;
                    cb(null, data);
                } else {
                    cb(error, data);
                }
                console.log('log data', data);
            })
        return cb.promise
    }


    Parameters.remoteMethod('modifyResource', {
        http: {
            path: '/modifyingavve',
            verb: 'put'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    Parameters.remoteMethod('getResource', {
        http: {
            path: '/gettingavve',
            verb: 'get'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    Parameters.remoteMethod('modifyCatalogAllProv', {
        http: {
            path: '/modifyAllProv',
            verb: 'put'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    Parameters.remoteMethod('getCatalogAllProv', {
        http: {
            path: '/getAllProv',
            verb: 'get'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    Parameters.remoteMethod('modifySWAutTgest', {
        http: {
            path: '/modifySWAutTgest',
            verb: 'put'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    Parameters.remoteMethod('getSWAutTgest', {
        http: {
            path: '/getSWAutTgest',
            verb: 'get'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    Parameters.remoteMethod('modifyParameterValue', {
        http: {
            path: '/modifyParameterValue',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    });

    Parameters.remoteMethod('updateStatusWhatsapp', {
        http: {
            path: '/updateStatusWhatsapp',
            verb: 'put'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    Parameters.remoteMethod('getStatusWhatsapp', {
        http: {
            path: '/getStatusWhatsapp',
            verb: 'get'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    Parameters.remoteMethod('getSalesCondition', {
        http: {
            path: '/getSalesCondition',
            verb: 'get'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    Parameters.remoteMethod('modifySalesCondition', {
        http: {
            path: '/modifySalesCondition',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    });
};
