'use strict';

module.exports = function(Ftth) {

    Ftth.masive = (req, res, cb) => {

        let duplicates = 0;
        const dataSource = Ftth.getDataSource()
        const data = req.body
        const headers = data[0]
        
        var init = `WITH cte AS (INSERT INTO  ibmx_a07e6d02edaf552.tdp_ftth (`;
        for (let header of headers){
            init = init+header+`,`
        } 
        init=init.substring(0,init.length-1)+`) VALUES (`;

        var i = 0;
        var final = data.length;

        for(let row of data){
            if(i==0){
                i=1;
                continue;
            }
            var subquery = init
            for (let values of row){
                subquery = subquery+`'`+values+`',`
            }  
            subquery=subquery.substring(0,subquery.length-1)+`) ON CONFLICT (ftth_nro) DO NOTHING
                                                                    RETURNING ftth_nro
                                                                )
                                                                SELECT NULL AS result
                                                                WHERE EXISTS (SELECT 1 FROM cte)          
                                                                UNION ALL
                                                                SELECT ftth_nro AS result
                                                                FROM ibmx_a07e6d02edaf552.tdp_ftth
                                                                WHERE ftth_nro = '`+row[0]+`'
                                                                AND NOT EXISTS (SELECT 1 FROM cte); `
            dataSource.connector.execute(subquery, (error, info) => {
                let data = []
                i++;
                if (!error) {
                    data = JSON.parse(JSON.stringify(info));
                    if(data[0].result!=null){
                        duplicates=duplicates+1;
                    }
                    if(i==final){
                        cb(null,{'duplicados':duplicates ,'insertados': ((final-1)-duplicates)});
                    }
                } else {
                    cb(error, data);
                }
            })
        }
    }
    
    Ftth.remoteMethod('masive', {
        http: {
            path: '/masive',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })
};
