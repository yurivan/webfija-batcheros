'use strict';

var _ = require('underscore');
var excel = require('node-excel-export');

module.exports = function (Servicecallevents) {

	Servicecallevents.log = function (accessToken, msg) {
		var _this = this,
			dt = new Date(),
			log = {
				id: 0,
				eventDatetime: dt,
				tag: 'ADMIN',
				msg: msg,
				username: 'SYSTEM'
			};

		if (accessToken) {
			this.app.models.WebUser.findById(accessToken.userId, function (err, user) {
				log = _.extend(log, { username: user ? user.username : 'SYSTEM' });
				_this.create(log, function () { });
			});
		} else {
			this.create(log, function () { });
		}
	};

	//glazaror se implementa opcion para la descarga de log de errores             
	Servicecallevents.download = (req, res, cb) => {

		let ds = Servicecallevents.getDataSource();

		var styles = {
			headerDark: {
				fill: {
					fgColor: {
						rgb: '629FCD'
					}
				},
				font: {
					color: {
						rgb: 'FFFFFFFF'
					},
					sz: 14,
					bold: true,
					underline: false
				}
			},
			cellPink: {
				fill: {
					fgColor: {
						rgb: 'FFFFCCFF'
					}
				}
			},
			cellGreen: {
				fill: {
					fgColor: {
						rgb: 'FF00FF00'
					}
				}
			}
		};

		var specification = {
			'id': { displayName: 'ID', headerStyle: styles.headerDark, width: 100 },
			'eventDatetime': { displayName: 'FECHA', headerStyle: styles.headerDark, width: 120 },
			'sourceapp': { displayName: 'APLICACION', headerStyle: styles.headerDark, width: 120 },
			'servicecode': { displayName: 'SERVICIO', headerStyle: styles.headerDark, width: 120 },
			'username': { displayName: 'USUARIO', headerStyle: styles.headerDark, width: 120 },
			'docnumber': { displayName: 'DNI', headerStyle: styles.headerDark, width: 120 },
			'orderid': { displayName: 'VENTA', headerStyle: styles.headerDark, width: 150 },
			'msg': { displayName: 'MENSAJE', headerStyle: styles.headerDark, width: 150 }/*,
			'servicerequest': { displayName: 'DATOS CONSULTA SERVICIO', headerStyle: styles.headerDark, width: 300 }*/
		}

		var filter = JSON.parse(req.query.filter);
		console.log("filter " + JSON.stringify(filter))

		var request = filter["where"]

		//Obtenemos las fechas para el rango de búsqueda
		if (request["and"]) {
			const sizeOfParameters = filter["where"]["and"].length - 1
			console.log("length  " + sizeOfParameters)
			var firstDate = filter["where"]["and"][sizeOfParameters]["eventDatetime"]["between"][0]
			var lastDate = filter["where"]["and"][sizeOfParameters]["eventDatetime"]["between"][1]
		} else {
			var firstDate = filter["where"]["eventDatetime"]["between"][0]
			var lastDate = filter["where"]["eventDatetime"]["between"][1]
		}
		//dateArray 
		var dateArray = new Array()
		var firstDateAsDate = new Date(firstDate)
		var lastDateAsDate = new Date(lastDate)
		while (firstDateAsDate <= lastDateAsDate) {
			dateArray.push(new Date(firstDateAsDate))
			firstDateAsDate.setDate(firstDateAsDate.getDate() + 1)
		}
		var dataFile = []
		var sizeOfDateArray = dateArray.length
		var dataIsReady = false
		var dataArrived = 0
		dateArray.forEach(function (fechaInicial, index) {
			var endDate = fechaInicial.getTime() + 86399000
			var edt = new Date(endDate)
			//console.log(`startDate ${fechaInicial} ::::: lastDate ${edt}`)
			if (request["and"]) {
				filter["where"]["and"][sizeOfParameters]["eventDatetime"]["between"][0] = fechaInicial.getTime() - 18000000
				filter["where"]["and"][sizeOfParameters]["eventDatetime"]["between"][1] = edt.getTime() - 18000000
			} else {
				filter["where"]["eventDatetime"]["between"][0] = fechaInicial.getTime() - 18000000
				filter["where"]["eventDatetime"]["between"][1] = edt.getTime() - 18000000
			}
			Servicecallevents.find(filter, null, function (err, info) {
				var data = [];
				var dataset = [];

				if (!err && info) {
					dataArrived++
					if (sizeOfDateArray == dataArrived) {
						dataIsReady = true
					}
					data = info;
					//console.log(info[0][0]);
					var cantRegistro = data.length;
					//Este log es muy importante para las pruebas
					console.log('cantidad reg ' + cantRegistro);
					var dataset = [];
					var indiceRegistro = 0;
					for (var i = 0; i < cantRegistro; i++) {
						dataset.push({
							'id': data[i].id,
							'eventDatetime': data[i].eventDatetime,
							'sourceapp': data[i].sourceapp,
							'servicecode': data[i].servicecode,
							'username': data[i].username,
							'docnumber': data[i].docnumber,
							'orderid': data[i].orderid,
							'msg': data[i].msg/*,
							'servicerequest': data[i].servicerequest*/
						});
					}
					dataset.forEach(v => {
						dataFile.push(v)
					})
					if (dataIsReady) {
						var report = excel.buildExport(
							[ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
								{
									name: 'LogErrores', // <- Specify sheet name (optional)
									heading: [],  //<- Raw heading array (optional)
									specification: specification, // <- Report specification
									data: dataFile // <-- Report data
								}
							]
						);
						var datetime = new Date();
						var day = datetime.getDate();
						var month = datetime.getMonth() + 1;
						var year = datetime.getFullYear();
						var hour = datetime.getHours();
						var minute = datetime.getMinutes();
						var second = datetime.getSeconds();
						var dateFormat = day + '-' + month + '-' + year + '_' + hour + minute + second;
						res.attachment('reporteLogErrores' + dateFormat + '.xlsx'); // This is sails.js specific (in general you need to set headers)
						res.send(report);
					}
				}
			});
		})
	}

	//glazaror se define uri para la descarga de log de errores
	Servicecallevents.remoteMethod('download', {
		http: {
			path: '/download',
			verb: 'get'
		},
		accepts: [
			{ arg: 'req', type: 'object', 'http': { source: 'req' } },
			{ arg: 'res', type: 'object', 'http': { source: 'res' } }
		],
		returns: {
			arg: 'data',
			type: 'object'
		}
	});

};
