var _ = require('underscore'),
    postgresql = require('pg')
module.exports = function (Contract) {

    Contract.updateContract = (req, res, cb) => {
        const dataSource = Contract.getDataSource()

        let contractList = req.body.contractList
        let speechList = req.body.speechList
        let data = []
        let err = []
        let contractCounter = 0
        contractList.map((element, index) => {
            dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.tdp_contract SET webvalue='${element.webvalue}',appvalue='${element.appvalue}' where id=${element.id}`,
                (error, info) => {
                    if (!error) {
                        data.push(info)
                        //cb(null, data)
                    } else {
                        //cb(error, data)
                        err.push(error)
                    }
                    console.log('log data', data)
                    if (index == contractList.length - 1) {

                        speechList.map((element, index) => {
                            dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.tdp_contract SET webvalue='${element.appvalue}', appvalue='${element.appvalue}' where id=${element.id}`,
                                (error, info) => {
                                    if (!error) {
                                        data.push(info)
                                        //cb(null, data)
                                    } else {
                                        //cb(error, data)
                                        err.push(error)
                                    }
                                    if (index == contractList.length - 1) {
                                        if (err.length > 0) {
                                            cb(err, data)
                                        } else {
                                            cb(null, data)
                                        }
                                        return cb.promise
                                    }
                                })
                        })

                    }
                })
        })
    }

    Contract.remoteMethod('updateContract', {
        http: {
            path: '/updateContract',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })
}


