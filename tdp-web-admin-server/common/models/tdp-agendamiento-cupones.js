var _ = require('underscore'),
    postgresql = require('pg')
module.exports = function (TdpAgendamientoCupones) {

    TdpAgendamientoCupones.insertUpdateCupon = (req, res, cb) => {

        const dataSource = TdpAgendamientoCupones.getDataSource()
        const body = req.body
        console.log('body', body)
        const cuponesFranja = body.franja
        const cuponesDay = body.day
        const cuponesCupon = body.cupon

        console.log('req.franja', body.franja)
        console.log('req.day', body.day)
        console.log('req.cupon', body.cupon)

        dataSource.connector.execute(`select cupones_id from ibmx_a07e6d02edaf552.tdp_agendamiento_cupones 
        where cupones_franja = '${cuponesFranja}' and cupones_fecha = '${cuponesDay}' `,
            (error, info) => {
                let data = []
                if (!error) {
                    data = info
                    console.log('data info', info)
                    var stringInfo = JSON.stringify(info)
                    var jsonInfo = JSON.parse(stringInfo)

                    var cupones_id = 0
                    for (let i = 0; i < jsonInfo.length; i++) {
                        cupones_id = jsonInfo[i].cupones_id
                        console.log(cupones_id)
                    }

                    if (cupones_id == 0) {
                        dataSource.connector.execute(`INSERT INTO ibmx_a07e6d02edaf552.tdp_agendamiento_cupones 
                        (cupones_franja, cupones_fecha, cupones, cupones_utilizados, auditoria_create)
                        VALUES ('${cuponesFranja}', '${cuponesDay}', '${cuponesCupon}', 0, (now() AT TIME ZONE 'America/Lima'))`,
                            (error, info) => {
                                let data = []
                                if (!error) {
                                    data = info;
                                    cb(null, data);
                                } else {
                                    cb(error, data);
                                }
                                console.log('log data', data);
                            })
                    }else{
                        dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.tdp_agendamiento_cupones 
                        SET cupones =  '${cuponesCupon}', auditoria_modify = (now() AT TIME ZONE 'America/Lima') 
                        WHERE cupones_franja = '${cuponesFranja}' and cupones_fecha = '${cuponesDay}'`,
                            (error, info) => {
                                let data = []
                                if (!error) {
                                    data = info;
                                    cb(null, data);
                                } else {
                                    cb(error, data);
                                }
                                console.log('log data', data);
                            })

                    }
                } else {
                    cb(error, data)
                }
                console.log('log data ', data)
            })

        return cb.promise
    }

    TdpAgendamientoCupones.eliminarCupon = (req, res, cb) => {

        const dataSource = TdpAgendamientoCupones.getDataSource()
        const cuponesId = req.body.cuponesId

        console.log('req.body', req.body)

        dataSource.connector.execute(`DELETE FROM ibmx_a07e6d02edaf552.tdp_agendamiento_cupones WHERE cupones_id = ${cuponesId}`,
            (error, info) => {
                let data = []
                if (!error) {
                    data = info;
                    cb(null, data);
                } else {
                    cb(error, data);
                }
                console.log('log data', data);
            })
        return cb.promise
    }

    TdpAgendamientoCupones.remoteMethod('insertUpdateCupon', {
        http: {
            path: '/insertUpdateCupon',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    });

    TdpAgendamientoCupones.remoteMethod('eliminarCupon', {
        http: {
            path: '/eliminarCupon',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    });
};
