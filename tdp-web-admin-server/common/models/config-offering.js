


module.exports = function(ConfigOffering) {

  ConfigOffering.getDataConfigOffering = function (req, res, cb) {
  let ds = ConfigOffering.getDataSource();
  console.log('ds', ds);

  ds.connector.execute("SELECT ID,sourceApp,domain,category,sourceType,sourceData,targetType,targetData FROM ibmx_a07e6d02edaf552.config_offering;", function ( err, info) {
    let data = [];
    if( !err ) {
      data = info ;
      cb(null, data);
    } else {
      cb(err, data);
    }

    console.log('log data', data);
  });

  }


  ConfigOffering.remoteMethod('getDataConfigOffering', {
   http: {
     path: '/getDataConfigOffering',
     verb: 'get'
   },
   accepts: [
     {arg: 'req', type: 'object', 'http': {source: 'req'}},
     {arg: 'res', type: 'object', 'http': {source: 'res'}}
   ],
   returns: {
     arg: 'data',
     type: 'object'
   }
  });

};
