var _ = require('underscore'),
    TdpCatalogsxSalesAgentConverter = require('../converters/tdp-catalogxsalesagent-converter');

module.exports = function(TdpCatalogxSalesAgent) {

    TdpCatalogxSalesAgent.reset = function (req, res, cb) {
        var _this = this;
        try {
            var converter = new TdpCatalogsxSalesAgentConverter;
            converter.insertData(req, TdpCatalogxSalesAgent.getDataSource(), this, function (d) {
            cb(null, d);
            });
        } catch (e) {
            console.error(e);
        }
        return cb.promise;
    };

  TdpCatalogxSalesAgent.upload = function (req, res, cb) {
    var converter = new TdpCatalogsxSalesAgentConverter;
    try {
      converter.processFile(req, function (d) {
        cb(null, d);
      });
    } catch(e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpCatalogxSalesAgent.remoteMethod('upload', {
    http: {
      path: '/upload',
      verb: 'post'
    },
    accepts: [
      {arg: 'req', type: 'object', 'http': {source: 'req'}},
      {arg: 'res', type: 'object', 'http': {source: 'res'}}
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpCatalogxSalesAgent.remoteMethod('reset', {
    http: {
      path: '/reset',
      verb: 'post'
    },
    accepts: [
      {arg: 'req', type: 'object', 'http': {source: 'req'}},
      {arg: 'res', type: 'object', 'http': {source: 'res'}}
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

};
