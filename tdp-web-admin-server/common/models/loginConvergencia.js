'use strict';

module.exports = function(LoginConvergencia) {
    
    LoginConvergencia.addLoginConvergencia = (req, res, cb) => {
        let query = ''
        console.log(req.body);
        
        if(req.body.canal==''){
            query = "INSERT INTO ibmx_a07e6d02edaf552.tdp_login_convergencia(f_enable,pto_venta,canal) "+
            "VALUES ("+req.body.f_enable+",'"+req.body.pto_venta+"',(SELECT "+
                                                       "canal "+
                                                "FROM ibmx_a07e6d02edaf552.tdp_sales_agent "+
                                                "WHERE nompuntoventa = '"+req.body.pto_venta+"' "+
                                                "LIMIT 1 "+
                                                "))";
        }else{
            query="INSERT INTO ibmx_a07e6d02edaf552.tdp_login_convergencia(pto_venta,f_enable,canal) "+
                   "SELECT "+ 
                            "DISTINCT nompuntoventa,"+req.body.f_enable+" as fl,'"+req.body.canal+"' as can "+
                    "FROM ibmx_a07e6d02edaf552.tdp_sales_agent "+
                    "WHERE canal = '"+req.body.canal+"'  "+
                    "AND nompuntoventa	NOT IN (  "+
                        "SELECT  "+
                            "DISTINCT pto_venta  "+
                    "FROM ibmx_a07e6d02edaf552.tdp_login_convergencia )";
        }
        console.log(query)
        const dataSource = LoginConvergencia.getDataSource()
        const data = req.body
        dataSource.connector.execute(query, (error, info) => {
            if (!error) {
                cb(null,info);
            }else{
                cb(error,null);
            }
        })
    }

    LoginConvergencia.getflag = (req, res, cb) => {

        const dataSource = LoginConvergencia.getDataSource()
        const data = req.body
        let query = 'SELECT strvalue FROM  ibmx_a07e6d02edaf552.parameters WHERE parameters.element '+"= 'flag_convergencia'";
        dataSource.connector.execute(query, (error, info) => {
            if (!error) {
                cb(null,info);
            }else{
                cb(error,null);
            }
        })
    }

    LoginConvergencia.updatearflag = (req, res, cb) => {

        const dataSource = LoginConvergencia.getDataSource()
        const data = req.body
        let query = 'UPDATE ibmx_a07e6d02edaf552.parameters SET strvalue = NOT  strvalue::boolean WHERE parameters.element '+"= 'flag_convergencia'";
        dataSource.connector.execute(query, (error, info) => {
            if (!error) {
                cb(null,{'success': 'true'});
            }else{
                cb(error,{'error' : 'true'});
            }
        })
    }

    LoginConvergencia.remoteMethod('updatearflag', {
        http: {
            path: '/updatearflag',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    LoginConvergencia.remoteMethod('getflag', {
        http: {
            path: '/getflag',
            verb: 'get'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })

    LoginConvergencia.remoteMethod('addLoginConvergencia', {
        http: {
            path: '/addLoginConvergencia',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })
    
};
