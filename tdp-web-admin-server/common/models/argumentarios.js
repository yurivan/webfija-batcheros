var _ = require('underscore'),
    postgresql = require('pg')
module.exports = function (Argumentarios) {

    Argumentarios.updateArgumentario = (req, res, cb) => {

        const dataSource = Argumentarios.getDataSource()
        const body = req.body
        console.log('body', body)
        const argument_id = body.id
        const canal = body.canal
        const mensaje = body.mensaje

        console.log('req.canal', body.canal)
        console.log('req.pantalla', body.pantalla)
        console.log('req.mensaje', body.mensaje)


        dataSource.connector.execute(`select id, entidad, canal, mensaje, pantalla  from ibmx_a07e6d02edaf552.argumentarios 
        where canal = '${canal}'`,
            (error, info) => {
                let data = []
                if (!error) {
                    data = info
                    console.log('data info', info)
                    var stringInfo = JSON.stringify(info)
                    var jsonInfo = JSON.parse(stringInfo)

                    var arg_id = 0
                    for (let i = 0; i < jsonInfo.length; i++) {
                        arg_id = jsonInfo[i].id
                        console.log(arg_id)
                    }

                    /*if (cupones_id == 0) {
                        dataSource.connector.execute(`INSERT INTO ibmx_a07e6d02edaf552.tdp_agendamiento_cupones 
                        (cupones_franja, cupones_fecha, cupones, cupones_utilizados, auditoria_create)
                        VALUES ('${cuponesFranja}', '${cuponesDay}', '${cuponesCupon}', 0, (now() AT TIME ZONE 'America/Lima'))`,
                            (error, info) => {
                                let data = []
                                if (!error) {
                                    data = info;
                                    cb(null, data);
                                } else {
                                    cb(error, data);
                                }
                                console.log('log data', data);
                            })
                    }else{*/
                    dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.argumentarios 
                    SET mensaje  =  '${mensaje}' 
                    WHERE id = '${argument_id}'`,
                        (error, info) => {
                            let data = []
                            if (!error) {
                                data = info;
                                cb(null, data);
                            } else {
                                cb(error, data);
                            }
                            console.log('log data', data);
                        })

                   // }
                } else {
                    cb(error, data)
                }
                console.log('log data ', data)
            })

        return cb.promise
    }

    Argumentarios.eliminarCupon = (req, res, cb) => {

        const dataSource = Argumentarios.getDataSource()
        const cuponesId = req.body.cuponesId

        console.log('req.body', req.body)

        dataSource.connector.execute(`DELETE FROM ibmx_a07e6d02edaf552.tdp_agendamiento_cupones WHERE cupones_id = ${cuponesId}`,
            (error, info) => {
                let data = []
                if (!error) {
                    data = info;
                    cb(null, data);
                } else {
                    cb(error, data);
                }
                console.log('log data', data);
            })
        return cb.promise
    }

    Argumentarios.remoteMethod('updateArgumentario', {
        http: {
            path: '/updateArgumentario',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    });

};
