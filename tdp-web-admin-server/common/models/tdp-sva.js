var _ = require('underscore'), TdpSvasConverter = require('../converters/tdp-sva-converter');

module.exports = function(TdpSva) {

	TdpSva.reset = function(req, res, cb) {
		var _this = this;
		try {
			var converter = new TdpSvasConverter;
			converter.insertData(req, TdpSva.getDataSource(), this,
					function(d) {
						cb(null, d);
					});
		} catch (e) {
			console.error(e);
		}
		return cb.promise;
	};

	TdpSva.upload = function(req, res, cb) {
		var converter = new TdpSvasConverter;
		try {
			converter.processFile(req, function(d) {
				cb(null, d);
			});
		} catch (e) {
			console.error(e);
		}
		return cb.promise;
	};



TdpSva.getDataSvaOffering = function (req, res, cb) {
var ds = TdpSva.getDataSource();

		ds.connector.execute("select distinct prodTypeCode, description from ibmx_a07e6d02edaf552.tdp_sva;", function (err, info) {
			var data = [];
			if(!err){
				data = info;
				cb(null, data);
			}else{
				cb(err, data);
			}
	console.log(data);
		});
		};


	TdpSva.remoteMethod('upload', {
		http : {
			path : '/upload',
			verb : 'post'
		},
		accepts : [ {
			arg : 'req',
			type : 'object',
			'http' : {
				source : 'req'
			}
		}, {
			arg : 'res',
			type : 'object',
			'http' : {
				source : 'res'
			}
		} ],
		returns : {
			arg : 'data',
			type : 'object'
		}
	});

	TdpSva.remoteMethod('reset', {
		http : {
			path : '/reset',
			verb : 'post'
		},
		accepts : [ {
			arg : 'req',
			type : 'object',
			'http' : {
				source : 'req'
			}
		}, {
			arg : 'res',
			type : 'object',
			'http' : {
				source : 'res'
			}
		} ],
		returns : {
			arg : 'data',
			type : 'object'
		}

	});

	TdpSva.remoteMethod('getDataSvaOffering', {
      http: {
        path: '/getDataSvaOffering',
        verb: 'get'
      },
      accepts: [
        {arg: 'req', type: 'object', 'http': {source: 'req'}},
        {arg: 'res', type: 'object', 'http': {source: 'res'}}
      ],
      returns: {
        arg: 'data',
        type: 'object'
      }
    });
};
