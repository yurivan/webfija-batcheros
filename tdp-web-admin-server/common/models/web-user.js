module.exports = function(WebUser) {
  var excludedProperties = [
    'realm',
    'emailVerified',
    'verificationToken',
    'credentials',
    'challenges',
    'lastUpdated',
    'created',
    'status'
  ];

  excludedProperties.forEach(function (p) {
    delete WebUser.definition.rawProperties[p];
    delete WebUser.definition.properties[p];
    delete WebUser.prototype[p];
  });

  WebUser.loadAllowedCallCenters = function (id, cb) {
    var ds = WebUser.getDataSource();
    ds.connector.execute('select wu.id, ss.name from webuser wu '+
    'left join tdp_sales_source_webuser ssw on ssw.id_webuser = wu.id '+
    'left join tdp_sales_source ss on ss.id = ssw.id_tdp_sales_source '+
    'where ss.name is not null and wu.id = '+id, function (err, info) {
      var data = [];
      if (!err && info) {
        data = info;
      }
      cb(data);
    });
  };

  WebUser.load = function (id, cb) {
    this.findById(id, function (err, user) {
      if (err) cb(err);
      else if (user) {
        user.webprofile(function (err, webprofile) {
          if (err) cb(err);
          else {
            if(webprofile){
            webprofile.webscreens(function (err, webscreens) {
              cb(null, { user: user, webscreens: webscreens});
            });
          }
            
          }
        });
      } else {
        err = new Error(g.f('Invalid token: %s', token));
        err.statusCode = 401;
        err.code = 'INVALID_TOKEN';
        cb(err);
      }
    });
  };

  WebUser.remoteMethod('load', {
    http: {
      path: '/:id/load',
      verb: 'get'
    },
    accepts: [
      {arg: 'id', type: 'number', required: true}
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });
};
