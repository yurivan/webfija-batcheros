    var _ = require('underscore');

    var excel = require('node-excel-export');

    TdpCatalogsxMotorizadoConverter = require('../converters/tdp-catalogxmotorizado-converter');

    module.exports = function(TdpCatalogxMotorizado) {

    TdpCatalogxMotorizado.reset = function (req, res, cb) {
        var _this = this;
        try {
            var converter = new TdpCatalogsxMotorizadoConverter;
                converter.insertData(req, TdpCatalogxMotorizado.getDataSource(), _this, function (d) {
                cb(null, d);
            });
        } catch (e) {
            console.error(e);
        }
        return cb.promise;
    };

    TdpCatalogxMotorizado.upload = function (req, res, cb) {
      var converter = new TdpCatalogsxMotorizadoConverter;
      try {
          converter.processFile(req, function (d) {
          cb(null, d);
          });
      } catch(e) {
        console.error(e);
      }
      return cb.promise;
    };

    TdpCatalogxMotorizado.getMotorizado = function (req, res, cb) {

      var ds = TdpCatalogxMotorizado.getDataSource();
  
    ds.connector.execute("select distinct codmotorizado as motorizado from ibmx_a07e6d02edaf552.TDP_MOTORIZADO_BASE union select 'TODOS';", function (err, info) {
        var data = [];
        if (!err) {
          data = info;
          cb(null, data);
        } else {
          cb(err, data);
        }
  
  
      });
    };

    TdpCatalogxMotorizado.webReportMotorizado = function (startDate, finDate, codmotorizado, req, res, cb) {

    var ds = TdpCatalogxMotorizado.getDataSource();
    
		if(startDate != 'null' && startDate != null) {
			
			 startDate = startDate.replace(/-/g,"/");
    }
    
		if(finDate != 'null' && finDate != null) {
			
		   finDate = finDate.replace(/-/g,"/");
		}

		if(codmotorizado == 'null' || codmotorizado == null ) {
		   codmotorizado = "";
		}

		console.log("start date "+ startDate);
		console.log("fin date "+ finDate);
    console.log("codmotorizado "+ codmotorizado);
    
		var styles = {
				  headerDark: {
				    fill: {
				      fgColor: {
				        rgb: '629FCD'
				      }
				    },
				    font: {
				      color: {
				        rgb: 'FFFFFFFF'
				      },
				      sz: 14,
				      bold: true,
				      underline: false
				    }
				  },
				  cellPink: {
				    fill: {
				      fgColor: {
				        rgb: 'FFFFCCFF'
				      }
				    }
				  },
				  cellGreen: {
				    fill: {
				      fgColor: {
				        rgb: 'FF00FF00'
				      }
				    }
				  }
				};
		 
		var specification = {
    'codigopedido':            {displayName: 'Nro Pedido',                   headerStyle: styles.headerDark,width: 150},
		'codmotorizado':           {displayName: 'Código Motorizado',            headerStyle: styles.headerDark,width: 150},
    'fecharegistro':           {displayName: 'Fecha Registro',               headerStyle: styles.headerDark,width: 150},
    'tipdoccliente':           {displayName: 'Tipo de Documento',            headerStyle: styles.headerDark,width: 150},
		'numdoccliente':           {displayName: 'Número Documento',             headerStyle: styles.headerDark,width: 150},
    'nombrecliente':           {displayName: 'Cliente',                      headerStyle: styles.headerDark,width: 150},
		'movilcliente':            {displayName: 'Teléfono',                     headerStyle: styles.headerDark,width: 150},
    'biometria':               {displayName: 'Flag Biometria',               headerStyle: styles.headerDark,width: 150},
		'estado':                  {displayName: 'Estado',                       headerStyle: styles.headerDark,width: 150},
    'motivorechazo':           {displayName: 'Motivo',                       headerStyle: styles.headerDark,width: 150},
    'updatetime':              {displayName: 'Fecha Cambio Estado',          headerStyle: styles.headerDark,width: 150}
		}
		
    var query = ''

    if (codmotorizado == 'TODOS') {
      query = "select	m.codigopedido, m.codmotorizado, m.fecharegistro, m.tipdoccliente, m.numdoccliente, " +
        "m.nombrecliente, m.movilcliente,	(CASE WHEN m.biometria = 0 THEN 'NO' WHEN m.biometria = 1 THEN 'SI' ELSE '' END) as biometria, m.estado, m.motivorechazo, m.updatetime " +
        "from ibmx_a07e6d02edaf552.tdp_motorizado_base as m " +
        "where ('" + startDate + "' is null or m.fecharegistro >= to_timestamp('" + startDate + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')) " +
        "and ('" + finDate + "' is null or m.fecharegistro <= to_timestamp('" + finDate + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')) order by m.codmotorizado asc";
    } else {
      query = "select	m.codigopedido, m.codmotorizado, m.fecharegistro, m.tipdoccliente, m.numdoccliente, " +
        "m.nombrecliente, m.movilcliente,	(CASE WHEN m.biometria = 0 THEN 'NO' WHEN m.biometria = 1 THEN 'SI' ELSE '' END) as biometria, m.estado, m.motivorechazo, m.updatetime " +
                "from ibmx_a07e6d02edaf552.tdp_motorizado_base as m " +
        "where ('" + startDate + "' is null or m.fecharegistro >= to_timestamp('" + startDate + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')) " +
        "and ('" + finDate + "' is null or m.fecharegistro <= to_timestamp('" + finDate + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')) " +
                  "and (m.codmotorizado is null or m.codmotorizado like '%'||'"            + codmotorizado + "'||'%') " +
                "order by m.codigopedido desc";
    }

			ds.connector.execute(query, function (err, info) {
      try {
			var data = [];
      var dataset = [];
      
	    if (!err && info) {
	         data = info;

	         console.log(info[0][0]);
	         var cantRegistro = data.length;
	         console.log('cantidad reg '+ cantRegistro);
	         var dataset = [];
           var indiceRegistro = 0;
           
	         for(var i = 0; i < cantRegistro; i++) {
				         
            dataset.push({
              'codigopedido': data[i]['codigopedido'],
                            'codmotorizado':  data[i]['codmotorizado'],
                            'fecharegistro':  data[i]['fecharegistro'],
	        		              'tipdoccliente':  data[i]['tipdoccliente'],
	        		              'numdoccliente':  data[i]['numdoccliente'],
                            'nombrecliente':  data[i]['nombrecliente'],
	        		              'movilcliente':   data[i]['movilcliente'],
	        		              'biometria':      data[i]['biometria'],
					                  'estado':         data[i]['estado'],
                            'motivorechazo':  data[i]['motivorechazo'],
					                  'updatetime':     data[i]['updatetime']
	            });
	        	 
           }
           
	         var report = excel.buildExport(
					  [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
					    {
					      name: 'Motorizados', // <- Specify sheet name (optional)
					      heading: [],  //<- Raw heading array (optional)
					      specification: specification, // <- Report specification
					      data: dataset // <-- Report data
					    }
					  ]
					 );
	         var datetime = new Date();
	         var day = datetime.getDate();
	         var month = datetime.getMonth() +1;
	         var year = datetime.getFullYear();
	         var hour = datetime.getHours();
	         var minute = datetime.getMinutes();
	         var second = datetime.getSeconds();
	         var dateFormat = day+'-'+month+'-'+year+'_'+hour+minute+second;
	         res.attachment('reporteMotorizados'+dateFormat+'.xlsx'); // This is sails.js specific (in general you need to set headers)
				   res.send(report);
        }
      } catch (error) {
        err=error;
	         }
	       }
	    );

	  };

  TdpCatalogxMotorizado.remoteMethod('upload', {
    http: {
      path: '/upload',
      verb: 'post'
    },
    accepts: [
      {arg: 'req', type: 'object', 'http': {source: 'req'}},
      {arg: 'res', type: 'object', 'http': {source: 'res'}}
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpCatalogxMotorizado.remoteMethod('reset', {
    http: {
      path: '/reset',
      verb: 'post'
    },
    accepts: [
      {arg: 'req', type: 'object', 'http': {source: 'req'}},
      {arg: 'res', type: 'object', 'http': {source: 'res'}}
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpCatalogxMotorizado.remoteMethod('getMotorizado', {
    http: {
      path: '/getMotorizado',
      verb: 'get'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpCatalogxMotorizado.remoteMethod('webReportMotorizado', {
    http: {
      path: '/web/motorizado/:startDate/:finDate/:motorizado',
      verb: 'get'
    },
    accepts: [
      {arg: 'startDate',  type: 'string', required: true},
      {arg: 'finDate',    type: 'string', required: true},
      {arg: 'motorizado', type: 'string', required: true},
      {arg: 'req',        type: 'object', 'http': {source: 'req'}},
      {arg: 'res',        type: 'object', 'http': {source: 'res'}}
  
    ],
    returns: {}
  });

};
