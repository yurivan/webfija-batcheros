var _ = require('underscore'),
  TdpCatalogsConverter = require('../converters/tdp-catalog-converter');

module.exports = function (TdpCatalog) {
  TdpCatalog.reset = function (req, res, cb) {
    var _this = this;
    //console.log("req " + JSON.stringify(req))
    try {
      var converter = new TdpCatalogsConverter;
      converter.insertData(req, TdpCatalog.getDataSource(), this, function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpCatalog.upload = function (req, res, cb) {
    var converter = new TdpCatalogsConverter;
    try {
      converter.processFile(req, function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpCatalog.getCampaign = function (req, res, cb) {
    var ds = TdpCatalog.getDataSource();

    // Sprint 12 - reemplazamos la consulta de nombres de campanias
    ds.connector.execute("select distinct value as campaign from ibmx_a07e6d02edaf552.TDP_CATALOG_ADITIONAL where PARAMETER_ID = 6530;", function (err, info) {
      var data = [];
      if (!err) {
        data = info;
        cb(null, data);
      } else {
        cb(err, data);
      }


    });
  };

  TdpCatalog.getDataCatalogOffering = function (req, res, cb) {
    let ds = TdpCatalog.getDataSource();
    //console.log('ds', ds);

    ds.connector.execute("SELECT distinct prodTypeCode, productType FROM ibmx_a07e6d02edaf552.tdp_catalog;", function (err, info) {
      let data = [];
      if (!err) {
        data = info;
        cb(null, data);
      } else {
        cb(err, data);
      }

      //console.log('log data', data);
    });

  }

  TdpCatalog.remoteMethod('getDataCatalogOffering', {
    http: {
      path: '/getDataCatalogOffering',
      verb: 'get'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  // curl -X POST -H "Content-Type: multipart/form-data" --header "Accept: application/json" -F "data=@/tmp/CARTILLA PRODUCTO DICIEMBRE_V9.xlsx" "http://localhost:3000/api/TdpCatalogs/upload"
  TdpCatalog.remoteMethod('upload', {
    http: {
      path: '/upload',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpCatalog.remoteMethod('reset', {
    http: {
      path: '/reset',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpCatalog.remoteMethod('getCampaign', {
    http: {
      path: '/getCampaign',
      verb: 'get'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });


  TdpCatalog.remoteMethod('getDataCatalogOffering', {
    http: {
      path: '/getDataCatalogOffering',
      verb: 'get'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });


};
