var _ = require('underscore'),
    postgresql = require('pg')
module.exports = function (Message) {

    Message.updateMessage = (req, res, cb) => {
        const dataSource = Message.getDataSource()

        let messageList = req.body.messageList
        let data = []
        let err = []
        let messageCounter = 0
        messageList.map((element, index) => {
            dataSource.connector.execute(`UPDATE ibmx_a07e6d02edaf552.tdp_error_message_service SET responsemessage='${element.responsemessage}' where id=${element.id}`,
                (error, info) => {
                    if (!error) {
                        data.push(info)
                        cb(null, data)
                    } else {
                        cb(error, data)
                        err.push(error)
                    }
                    console.log('log data', data)
                    return cb.promise;
                })
        })
    }

    Message.remoteMethod('updateMessage', {
        http: {
            path: '/updateMessage',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    })
}


