// Sprint 12 - utilitario para hacer llamadas http
// Aqui definiremos constantes
const request = require('request');

function define(name, value) {
    Object.defineProperty(exports, name, {
        value:      value,
        enumerable: true
    });
}

request('https://restcountries.eu/rest/v2/all?fields=name;alpha2Code', { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    let paisesMap = {};
    for (i = 0; i < body.length; i ++) {
        paisesMap[body[i].alpha2Code] = body[i].name;
    }
    console.log(body.url);
    console.log(body.explanation);
    define("PAISES", paisesMap);
});