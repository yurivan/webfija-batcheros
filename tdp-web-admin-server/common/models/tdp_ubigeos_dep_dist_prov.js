var _ = require('underscore'),
    postgresql = require('pg')
module.exports = function (TdpUbigeosDepDistProv) {

    TdpUbigeosDepDistProv.selectUbigeos = (req, res, cb) => {

        const dataSource = TdpUbigeosDepDistProv.getDataSource()
        const request = req.body
        let select = "select * from ibmx_a07e6d02edaf552.tdp_ubigeos_dep_dist_prov where"


        if (request.coddpto == '%') {
            select = select + " coddpto<>'00'"
        } else {
            select = select + " coddpto='" + request.coddpto + "'"
        }

        if (request.codprov == '%') {
            select = select + " and codprov<>'00'"
        } else {
            select = select + " and codprov='" + request.codprov + "'"
        }

        if (request.coddist == '%') {
            select = select + " and coddist<>'00'"
        } else {
            select = select + " and coddist='" + request.coddist + "'"
        }
        console.log(select);
        
        dataSource.connector.execute(select,
            (err, info) => {
                var data = [];
                if (!err) {
                    data = info;
                    cb(null, data);
                } else {
                    cb(err, data);
                }
            })

        return cb.promise
    }

    TdpUbigeosDepDistProv.remoteMethod('selectUbigeos', {
        http: {
            path: '/selectUbigeos',
            verb: 'post'
        },
        accepts: [
            { arg: 'req', type: 'object', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } }
        ],
        returns: {
            arg: 'data',
            type: 'object'
        }
    });

};
