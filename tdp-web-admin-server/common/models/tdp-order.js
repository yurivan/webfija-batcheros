var _ = require('underscore');
var fs = require('fs');
//var nodeExcel = require('excel-export');
var excel = require('node-excel-export');

module.exports = function (TdpOrder) {

	// Sprint 12 - utilitario de constantes
	var constants = require("./constants");

	TdpOrder.webReportVenta = function (startDate, finDate, campaign, req, res, cb) {
		var ds = TdpOrder.getDataSource();
		if (startDate != 'null' && startDate != null) {
			//startDate = "'"+startDate.replace(/-/g,"/")+"'";
			startDate = startDate.replace(/-/g, "/");
		}
		if (finDate != 'null' && finDate != null) {
			//finDate = "'"+finDate.replace(/-/g,"/")+"'";
			finDate = finDate.replace(/-/g, "/");
		}

		if (campaign == 'null' || campaign == null) {
			campaign = "";
		}



		console.log("start date " + startDate);
		console.log("fin date " + finDate);
		console.log("campaign " + campaign);
		var styles = {
			headerDark: {
				fill: {
					fgColor: {
						rgb: '629FCD'
					}
				},
				font: {
					color: {
						rgb: 'FFFFFFFF'
					},
					sz: 14,
					bold: true,
					underline: false
				}
			},
			cellPink: {
				fill: {
					fgColor: {
						rgb: 'FFFFCCFF'
					}
				}
			},
			cellGreen: {
				fill: {
					fgColor: {
						rgb: 'FF00FF00'
					}
				}
			}
		};

		var specification = {
			'back': { displayName: 'BACK', headerStyle: styles.headerDark, width: 150 },
			'campaign': { displayName: 'CAMPAÑA', headerStyle: styles.headerDark, width: 150 },
			'firstName': { displayName: 'NOMBRE CLIENTE', headerStyle: styles.headerDark, width: 150 },
			'lastName1': { displayName: 'APELLIDO PATERNO CLIENTE', headerStyle: styles.headerDark, width: 150 },
			'lastName2': { displayName: 'APELLIDO MATERNO CLIENTE', headerStyle: styles.headerDark, width: 150 },
			'docType': { displayName: 'TIPO DOCUMENTO CLIENTE', headerStyle: styles.headerDark, width: 150 },
			'docNumber': { displayName: 'NUMERO DOCUMENTO CLIENTE', headerStyle: styles.headerDark, width: 150 },
			//Spring 12 - Se adiciona campo pais y sistema de venta
			'country': { displayName: 'PAIS', headerStyle: styles.headerDark, width: 150 },
			'salesSystem': { displayName: 'SISTEMA VENTA', headerStyle: styles.headerDark, width: 150 },
			'customerPhone': { displayName: 'TELEFONO CONTACTO 1', headerStyle: styles.headerDark, width: 150 },
			'customerPhone2': { displayName: 'TELEFONO CONTACTO 2', headerStyle: styles.headerDark, width: 150 },
			'email': { displayName: 'EMAIL', headerStyle: styles.headerDark, width: 150 },
			'sendContracts': { displayName: 'ENVIAR CONTRATOS', headerStyle: styles.headerDark, width: 150 },
			'dataProtection': { displayName: 'PROTECCION DE DATO', headerStyle: styles.headerDark, width: 150 },
			'publicationWhitePages': { displayName: 'PUBLICAR PAG. BLANCAS', headerStyle: styles.headerDark, width: 150 },
			'canal': { displayName: 'CANAL', headerStyle: styles.headerDark, width: 150 },
			'nombre': { displayName: 'NOMBRE VENDEDOR', headerStyle: styles.headerDark, width: 150 },
			'userID': { displayName: 'CODIGO ATIS', headerStyle: styles.headerDark, width: 150 },
			'codCMS': { displayName: 'CODIGO CMS', headerStyle: styles.headerDark, width: 150 },
			'zonal': { displayName: 'ZONAL', headerStyle: styles.headerDark, width: 150 },
			'region': { displayName: 'REGION', headerStyle: styles.headerDark, width: 150 },
			'TIPO_PRODUCTO': { displayName: 'TIPO PRODUCTO', headerStyle: styles.headerDark, width: 150 },
			'SUB_PRODUCTO': { displayName: 'SUB PRODUCTO', headerStyle: styles.headerDark, width: 150 },
			'DEPARTAMENTO': { displayName: 'DEPARTAMENTO', headerStyle: styles.headerDark, width: 150 },
			'PROVINCIA': { displayName: 'PROVINCIA', headerStyle: styles.headerDark, width: 150 },
			'DISTRITO': { displayName: 'DISTRITO', headerStyle: styles.headerDark, width: 150 },
			'DIRECCION': { displayName: 'DIRECCION', headerStyle: styles.headerDark, width: 150 },
			'OPERACION_COMERCIAL': { displayName: 'OPERACION COMERCIAL', headerStyle: styles.headerDark, width: 150 },
			'CODIGO_PEDIDO': { displayName: 'CODIGO PEDIDO', headerStyle: styles.headerDark, width: 150 },
			'migrationPhone': { displayName: 'TELEFONO A MIGRAR', headerStyle: styles.headerDark, width: 150 },
			'CODIGO_GESTION': { displayName: 'CODIGO GESTION', headerStyle: styles.headerDark, width: 150 },
			'CODIGO_SUB_GESTION': { displayName: 'CODIGO SUB GESTION', headerStyle: styles.headerDark, width: 150 },
			'internetTech': { displayName: 'TECNOLOGIA INTERNET', headerStyle: styles.headerDark, width: 150 },
			'expertoCode': { displayName: 'CODIGO EXPERTO', headerStyle: styles.headerDark, width: 150 },
			'disableWhitePage': { displayName: 'DESAFILIACION PAGINAS BLANCAS', headerStyle: styles.headerDark, width: 150 },
			'enableDigitalInvoice': { displayName: 'AFILICACION FACTURA DIGITAL', headerStyle: styles.headerDark, width: 150 },
			'parentalProtection': { displayName: 'CONTROL PARENTAL', headerStyle: styles.headerDark, width: 150 },
			'hasRecording': { displayName: 'TIENE GRABACION', headerStyle: styles.headerDark, width: 150 },
			'fecha_registro': { displayName: 'FECHA REGISTRO', headerStyle: styles.headerDark, width: 150 },
			'hora_registro': { displayName: 'HORA REGISTRO', headerStyle: styles.headerDark, width: 150 },
			'ID_GRABACION': { displayName: 'ID GRABACION', headerStyle: styles.headerDark, width: 150 },
			'tvTech': { displayName: 'TECNOLOGIA TV', headerStyle: styles.headerDark, width: 150 },
			'ID_VISOR': { displayName: 'CODIGO UNICO DE SOLICITUD', headerStyle: styles.headerDark, width: 150 },
			'dni': { displayName: 'DNI', headerStyle: styles.headerDark, width: 150 },
			'distrito': { displayName: 'DISTRITO VENDEDOR', headerStyle: styles.headerDark, width: 150 },
			'decosSD': { displayName: 'DECOS SD', headerStyle: styles.headerDark, width: 150 },
			'decosHD': { displayName: 'DECOS HD', headerStyle: styles.headerDark, width: 150 },
			'decosDVR': { displayName: 'DECOS DVR', headerStyle: styles.headerDark, width: 150 },
			'codpago': { displayName: 'CODIGO CIP', headerStyle: styles.headerDark, width: 150 },
			'FECHA_GRABACION': { displayName: 'FECHA GRABACION', headerStyle: styles.headerDark, width: 150 },
			'cliente': { displayName: 'CLIENTE', headerStyle: styles.headerDark, width: 150 },
			'telefono': { displayName: 'TELEFONO', headerStyle: styles.headerDark, width: 150 },
			'dni': { displayName: 'DNI', headerStyle: styles.headerDark, width: 150 },
			'NOMBRE_PRODUCTO': { displayName: 'NOMBRE PRODUCTO', headerStyle: styles.headerDark, width: 150 },
			'ESTADO_SOLICITUD': { displayName: 'ESTADO SOLICITUD', headerStyle: styles.headerDark, width: 150 },
			'MOTIVO_ESTADO': { displayName: 'MOTIVO ESTADO', headerStyle: styles.headerDark, width: 150 },
			'SPEECH': { displayName: 'SPEECH', headerStyle: styles.headerDark, width: 150 },
			'ACCION': { displayName: 'ACCION', headerStyle: styles.headerDark, width: 150 },
			'CODIGO_LLAMADA': { displayName: 'CODIGO LLAMADA', headerStyle: styles.headerDark, width: 150 },
			'FLAG_DEPENDENCIA': { displayName: 'FLAG DEPENDENCIA', headerStyle: styles.headerDark, width: 150 },
			'VERSION_FILA': { displayName: 'VERSION FILA', headerStyle: styles.headerDark, width: 150 },
			'tipo_documento': { displayName: 'TIPO DOCUMENTO', headerStyle: styles.headerDark, width: 150 },
			'ACCION_APLICATIVOS_BACK': { displayName: 'ACCION APLICATIVOS BACK', headerStyle: styles.headerDark, width: 150 },
			'FECHA_DE_LLAMADA': { displayName: 'FECHA DE LLAMADA', headerStyle: styles.headerDark, width: 150 },
			'VERSION_FILA_T': { displayName: 'VERSION FILA T', headerStyle: styles.headerDark, width: 150 },
			'SUB_PRODUCTO_EQUIV': { displayName: 'SUB PRODUCTO EQUIVALENTE', headerStyle: styles.headerDark, width: 150 },
			'PRODUCTO': { displayName: 'PRODUCTO', headerStyle: styles.headerDark, width: 150 },
			'commercialOperation': { displayName: 'OPERACION COMERCIAL', headerStyle: styles.headerDark, width: 150 },
			'productType': { displayName: 'TIPO PRODUCTO', headerStyle: styles.headerDark, width: 150 },
			'productCategory': { displayName: 'CATEGORIA PRODUCTO', headerStyle: styles.headerDark, width: 150 },
			'productCode': { displayName: 'CODIGO PRODUCTO', headerStyle: styles.headerDark, width: 150 },
			'paymentMode': { displayName: 'METODO DE PAGO', headerStyle: styles.headerDark, width: 150 },
			'customerID': { displayName: 'CUSTOMER ID', headerStyle: styles.headerDark, width: 150 },
			'price': { displayName: 'PRECIO', headerStyle: styles.headerDark, width: 150 },
			'status': { displayName: 'STATUS', headerStyle: styles.headerDark, width: 150 },
			'statusLegacy': { displayName: 'STATUS LEGACY', headerStyle: styles.headerDark, width: 150 },
			'cancelDescription': { displayName: 'DESCRIPCION CANCELA', headerStyle: styles.headerDark, width: 150 },
			'watsonRequest': { displayName: 'DESCRIPCION WATSON', headerStyle: styles.headerDark, width: 150 },
			'coordinateX': { displayName: 'X INSTALACION', headerStyle: styles.headerDark, width: 150 },
			'coordinateY': { displayName: 'Y INSTALACION', headerStyle: styles.headerDark, width: 150 },
			'equipmentDecoType': { displayName: 'equipmentDecoType', headerStyle: styles.headerDark, width: 150 },
			'sourcePhone': { displayName: 'sourcePhone', headerStyle: styles.headerDark, width: 150 },
			'serviceType': { displayName: 'TIPO SERVICIO', headerStyle: styles.headerDark, width: 150 },
			'cmsCustomer': { displayName: 'CMS CUSTOMER', headerStyle: styles.headerDark, width: 150 },
			'cmsServiceCode': { displayName: 'CMS SERVICE CODE', headerStyle: styles.headerDark, width: 150 },
			'registeredTime': { displayName: 'registeredTime', headerStyle: styles.headerDark, width: 150 },
			'lastUpdateTime': { displayName: 'lastUpdateTime', headerStyle: styles.headerDark, width: 150 },
			'productName': { displayName: 'NOMBRE PRODUCTO', headerStyle: styles.headerDark, width: 150 },
			'promPrice': { displayName: 'PRECIO PROMOCION', headerStyle: styles.headerDark, width: 150 },
			'cashPrice': { displayName: 'PRECIO CONTADO', headerStyle: styles.headerDark, width: 150 },
			'lineType': { displayName: 'TIPO LINEA', headerStyle: styles.headerDark, width: 150 },
			'monthPeriod': { displayName: 'PERIODO PROMOCION', headerStyle: styles.headerDark, width: 150 },
			'financingMonth': { displayName: 'PERIODO FINANCIAMIENTO', headerStyle: styles.headerDark, width: 150 },
			'financingCost': { displayName: 'COSTO FINANCIAMIENTO', headerStyle: styles.headerDark, width: 150 },
			'orderID': { displayName: 'CODIGO ORDEN', headerStyle: styles.headerDark, width: 150 },
			'initCoordinateX': { displayName: 'X VENTA', headerStyle: styles.headerDark, width: 150 },
			'initCoordinateY': { displayName: 'Y VENTA', headerStyle: styles.headerDark, width: 150 },
			'decosSD': { displayName: 'DECOS SD', headerStyle: styles.headerDark, width: 150 },
			'decosHD': { displayName: 'DECOS HD', headerStyle: styles.headerDark, width: 150 },
			'decosDVR': { displayName: 'DECOS DVR', headerStyle: styles.headerDark, width: 150 },
			'tvBlock': { displayName: 'TV BLOCK', headerStyle: styles.headerDark, width: 150 },
			'svaInternet': { displayName: 'SVA INTERNET', headerStyle: styles.headerDark, width: 150 },
			'svaLine': { displayName: 'SVA LINEA', headerStyle: styles.headerDark, width: 150 },
			'packingType': { displayName: 'TIPO PAQUETE', headerStyle: styles.headerDark, width: 150 },
			'winBackDiscount': { displayName: 'WIN BACK DISCOUNT', headerStyle: styles.headerDark, width: 150 },
			'blockTV': { displayName: 'BLOCK TV', headerStyle: styles.headerDark, width: 150 },
			'altasTV': { displayName: 'ALTAS TV', headerStyle: styles.headerDark, width: 150 },
			'recordingID': { displayName: 'ID GRABACION', headerStyle: styles.headerDark, width: 150 },
			'department': { displayName: 'DEPARTAMENTO', headerStyle: styles.headerDark, width: 150 },
			'province': { displayName: 'PROVINCIA', headerStyle: styles.headerDark, width: 150 },
			'district': { displayName: 'DISTRITO', headerStyle: styles.headerDark, width: 150 },
			'address': { displayName: 'DIRECCION', headerStyle: styles.headerDark, width: 150 },
			'addressComplement': { displayName: 'ADICIONAL DIRECCION', headerStyle: styles.headerDark, width: 150 },
			'registeredTime': { displayName: 'HORA REGISTRO', headerStyle: styles.headerDark, width: 150 },
			'inputDocMode': { displayName: 'INPUT DOC MODE', headerStyle: styles.headerDark, width: 150 },
			'speechtxt': { displayName: 'SPEECH', headerStyle: styles.headerDark, width: 150 },
			'codATIS': { displayName: 'CODIGO ATIS', headerStyle: styles.headerDark, width: 150 },
			'cobre_blq_vta': { displayName: 'ADSL BLOQUEADO', headerStyle: styles.headerDark, width: 150 },
			'cobre_blq_trm': { displayName: 'ADSL CAJAS BLOQUEADAS ', headerStyle: styles.headerDark, width: 150 },
			'agen_fecha': { displayName: 'FECHA AGENDAMIENTO', headerStyle: styles.headerDark, width: 150 },
			'agen_franja': { displayName: 'FRANJA AGENDAMIENTO', headerStyle: styles.headerDark, width: 150 }
		}

		var query = "select	v.back, o.campaign,c.firstname, c.lastname1, c.lastname2, c.doctype, c.docNumber, c.customerPhone," +
			"c.customerPhone2, c.email,	od.sendContracts, od.dataProtection, od.publicationWhitePages, sa.canal, sa.nombre, o.userID," +
			"sa.codCMS, sa.departamento as zonal, sa.provincia || sa.distrito as region, v.TIPO_PRODUCTO, v.SUB_PRODUCTO, v.DEPARTAMENTO " +
			"as departamento, v.PROVINCIA as provincia2, v.DISTRITO, v.DIRECCION,v.OPERACION_COMERCIAL, v.CODIGO_PEDIDO, o.migrationPhone," +
			"v.CODIGO_GESTION, v.CODIGO_SUB_GESTION,od.internetTech, od.expertoCode,od.disableWhitePage, od.enableDigitalInvoice," +
			"od.parentalProtection, od.hasRecording,to_char(o.registrationDate, 'DD/MM/YYYY') as fecha_registro," +
			"to_char(o.registrationDate, 'HH24:MI:SS') as hora_registro, v.ID_GRABACION,od.tvTech,v.ID_VISOR, sa.dni, sa.distrito," +
			"od.decosSD, od.decosHD, od.decosDVR,pe.codpago,to_char(v.FECHA_GRABACION, 'DD/MM/YYYY HH24:MI:SS') as fecha_grabacion1," +
			"v.cliente, v.telefono, v.dni as dni2, v.NOMBRE_PRODUCTO,case when v.ESTADO_SOLICITUD is null then 'CAIDA APP' else " +
			"v.ESTADO_SOLICITUD end as ESTADO_SOLICITUD, v.MOTIVO_ESTADO, v.SPEECH, v.ACCION, v.CODIGO_LLAMADA,v.FLAG_DEPENDENCIA," +
			"v.VERSION_FILA, v.tipo_documento, v.ACCION_APLICATIVOS_BACK, v.FECHA_DE_LLAMADA, v.VERSION_FILA_T, v.SUB_PRODUCTO_EQUIV," +
			"v.PRODUCTO,o.commercialOperation, o.productType, o.productCategory, o.productCode, o.paymentMode, o.customerID," +
			"o.price, o.status, o.statusLegacy, o.cancelDescription, o.coordinateX, o.coordinateY,o.equipmentDecoType, o.sourcePhone," +
			"o.serviceType, o.cmsCustomer, o.cmsServiceCode, to_char(o.registeredTime, 'DD/MM/YYYY HH24:MI:SS') as registeredTime," +
			"to_char(o.lastUpdateTime, 'DD/MM/YYYY HH24:MI:SS') as lastUpdateTime, o.productName,  o.promPrice, o.cashPrice, o.lineType," +
			"o.monthPeriod, o.financingMonth, o.financingCost, od.orderID, od.initCoordinateX, od.initCoordinateY, od.decosSD as decosSD2," +
			"od.decosHD as decosHD2, od.decosDVR as decosDVR2, od.tvBlock, od.svaInternet, od.svaLine,od.packingType, od.winBackDiscount," +
			"od.blockTV, od.altasTV, od.recordingID, od.department, od.province, od.district, od.address, od.addressComplement," +
			"to_char(od.registeredTime, 'HH24:MI:SS') as registeredTime2, od.inputDocMode, od.speechtxt, sa.codATIS, c.nationality, o.appcode," +
			"od.cobre_blq_vta, od.cobre_blq_trm, " +
			"v.agent_client_fecha_seleccionada as agen_fecha, v.agent_client_franga_horaria as agen_franja " +
			"from ibmx_a07e6d02edaf552.order as o " +
			"left join ibmx_a07e6d02edaf552.order_detail as od on o.ID = od.orderID " +
			"left join ibmx_a07e6d02edaf552.customer as c on o.customerID = c.ID " +
			"left join ibmx_a07e6d02edaf552.tdp_visor as v on v.dni = c.docNumber and v.ID_VISOR= o.ID " +
			"left join ibmx_a07e6d02edaf552.tdp_sales_agent as sa on o.userID = sa.codATIS " +
			"left join ibmx_a07e6d02edaf552.peticion_pago_efectivo pe on pe.order_id = o.id " +
			"where ('" + startDate + "' is null or o.registrationDate >= to_timestamp('" + startDate + " 00:00:00', 'DD/MM/YYYY HH24:MI:SS')) " +
			"and ('" + finDate + "' is null or o.registrationDate <= to_timestamp('" + finDate + " 23:59:59', 'DD/MM/YYYY HH24:MI:SS')) " +
			"and (o.campaign is null or o.campaign like '%'||'" + campaign + "'||'%') " +
			"order by o.registrationDate desc";

		ds.connector.execute(query, function (err, info) {
			//ds.connector.execute("call sp_tdp_admin_sales_report("+startDate+", "+finDate+", '"+campaign+"');", function (err, info) {//"+startDate+"
			//select productType, customerID from `order`;
			var data = [];
			var dataset = [];
			if (!err && info) {
				data = info;

				console.log(info[0][0]);
				var cantRegistro = data.length;
				console.log('cantidad reg ' + cantRegistro);
				var dataset = [];
				var indiceRegistro = 0;
				for (var i = 0; i < cantRegistro; i++) {
					//dataset.push({Producto: data[0][i].productName,Precio: data[0][i].promPrice });
					let pais = null;
					if (data[i]['doctype'] == 'DNI') {
						pais = "Per\u00FA";
					} else {
						pais = constants.PAISES[data[i]['nationality']];
					}
					let aplicacionOrigen = "";
					if (data[i]['appcode'] == 'APPVF') {
						aplicacionOrigen = "APP";
					} else if (data[i]['appcode'] == 'WEBVF') {
						aplicacionOrigen = "WEB";
					}
					dataset.push({
						'back': data[i]['back'],
						'campaign': data[i]['campaign'],
						'firstName': data[i]['firstname'],
						'lastName1': data[i]['lastname1'],
						'lastName2': data[i]['lastname2'],
						'docType': data[i]['doctype'],
						'docNumber': data[i]['docnumber'],
						'country': pais,
						'salesSystem': aplicacionOrigen,
						'customerPhone': data[i]['customerphone'],
						'customerPhone2': data[i]['customerphone2'],
						'email': data[i]['email'],
						'sendContracts': data[i]['sendcontracts'],
						'dataProtection': data[i]['dataprotection'],
						'publicationWhitePages': data[i]['publicationwhitepages'],
						'canal': data[i]['canal'],
						'nombre': data[i]['nombre'],
						'userID': data[i]['userid'],
						'codCMS': data[i]['codcms'],
						'zonal': data[i]['zonal'],
						'region': data[i]['region'],
						'TIPO_PRODUCTO': data[i]['tipo_producto'],
						'SUB_PRODUCTO': data[i]['sub_producto'],
						'DEPARTAMENTO': data[i]['departamento'],
						'PROVINCIA': data[i]['provincia'],
						'DISTRITO': data[i]['distrito'],
						'DIRECCION': data[i]['direccion'],
						'OPERACION_COMERCIAL': data[i]['operacion_comercial'],
						'CODIGO_PEDIDO': data[i]['codigo_pedido'],
						'migrationPhone': data[i]['migrationphone'],
						'CODIGO_GESTION': data[i]['codigo_gestion'],
						'CODIGO_SUB_GESTION': data[i]['codigo_sub_gestion'],
						'internetTech': data[i]['internettech'],
						'expertoCode': data[i]['expertocode'],
						'disableWhitePage': data[i]['disablewhitepage'],
						'enableDigitalInvoice': data[i]['enabledigitalinvoice'],
						'parentalProtection': data[i]['parentalprotection'],
						'hasRecording': data[i]['hasrecording'],
						'fecha_registro': data[i]['fecha_registro'],
						'hora_registro': data[i]['hora_registro'],
						'ID_GRABACION': data[i]['id_grabacion'],
						'tvTech': data[i]['tvtech'],
						'ID_VISOR': data[i]['id_visor'],
						'dni': data[i]['dni'],
						'distrito': data[i]['distrito'],
						'decosSD': data[i]['decossd'],
						'decosHD': data[i]['decoshd'],
						'decosDVR': data[i]['decosdvr'],
						'codpago': data[i]['codpago'],
						'FECHA_GRABACION': data[i]['fecha_grabacion'],
						'cliente': data[i]['cliente'],
						'telefono': data[i]['telefono'],
						'dni': data[i]['dni'],
						'NOMBRE_PRODUCTO': data[i]['nombre_producto'],
						'ESTADO_SOLICITUD': data[i]['estado_solicitud'],
						'MOTIVO_ESTADO': data[i]['motivo_estado'],
						'SPEECH': data[i]['speech'],
						'ACCION': data[i]['accion'],
						'CODIGO_LLAMADA': data[i]['codigo_llamada'],
						'FLAG_DEPENDENCIA': data[i]['flag_dependencia'],
						'VERSION_FILA': data[i]['version_fila'],
						'tipo_documento': data[i]['tipo_documento'],
						'ACCION_APLICATIVOS_BACK': data[i]['accion_aplicativos_back'],
						'FECHA_DE_LLAMADA': data[i]['fecha_de_llamada'],
						'VERSION_FILA_T': data[i]['version_fila_t'],
						'SUB_PRODUCTO_EQUIV': data[i]['sub_producto_equiv'],
						'PRODUCTO': data[i]['producto'],
						'commercialOperation': data[i]['commercialoperation'],
						'productType': data[i]['producttype'],
						'productCategory': data[i]['productcategory'],
						'productCode': data[i]['productcode'],
						'paymentMode': data[i]['paymentmode'],
						'customerID': data[i]['customerid'],
						'price': data[i]['price'],
						'status': data[i]['status'],
						'statusLegacy': data[i]['statuslegacy'],
						'cancelDescription': data[i]['canceldescription'],
						'watsonRequest': data[i]['watsonrequest'],
						'coordinateX': data[i]['coordinatex'],
						'coordinateY': data[i]['coordinatey'],
						'equipmentDecoType': data[i]['equipmentdecotype'],
						'sourcePhone': data[i]['sourcephone'],
						'serviceType': data[i]['servicetype'],
						'cmsCustomer': data[i]['cmscustomer'],

						'cmsServiceCode': data[i]['cmsservicecode'],
						'registeredTime': data[i]['registeredtime'],
						'lastUpdateTime': data[i]['lastupdatetime'],
						'productName': data[i]['productname'],
						'promPrice': data[i]['promprice'],
						'cashPrice': data[i]['cashprice'],
						'lineType': data[i]['linetype'],
						'monthPeriod': data[i]['monthperiod'],
						'financingMonth': data[i]['financingmonth'],
						'financingCost': data[i]['financingcost'],
						'orderID': data[i]['orderid'],
						'initCoordinateX': data[i]['initcoordinatex'],
						'initCoordinateY': data[i]['initcoordinatey'],
						'decosSD': data[i]['decossd'],
						'decosHD': data[i]['decoshd'],
						'decosDVR': data[i]['decosdvr'],
						'tvBlock': data[i]['tvblock'],
						'svaInternet': data[i]['svainternet'],
						'svaLine': data[i]['svaline'],
						'packingType': data[i]['packingtype'],
						'winBackDiscount': data[i]['winbackdiscount'],
						'blockTV': data[i]['blocktv'],
						'altasTV': data[i]['altastv'],
						'recordingID': data[i]['recordingid'],
						'department': data[i]['department'],
						'province': data[i]['province'],
						'district': data[i]['district'],
						'address': data[i]['address'],
						'addressComplement': data[i]['addresscomplement'],
						'registeredTime': data[i]['registeredtime'],
						'inputDocMode': data[i]['inputdocmode'],
						'speechtxt': data[i]['speechtxt'],
						'codATIS': data[i]['codatis'],
						'cobre_blq_vta': data[i]['cobre_blq_vta'],
						'cobre_blq_trm': data[i]['cobre_blq_trm'],

						'agen_fecha': data[i]['agen_fecha'],
						'agen_franja': data[i]['agen_franja']
					});
					//dataset.push(reg);
				}
				var report = excel.buildExport(
					[ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
						{
							name: 'Ventas', // <- Specify sheet name (optional)
							heading: [],  //<- Raw heading array (optional)
							specification: specification, // <- Report specification
							data: dataset // <-- Report data
						}
					]
				);
				var datetime = new Date();
				var day = datetime.getDate();
				var month = datetime.getMonth() + 1;
				var year = datetime.getFullYear();
				var hour = datetime.getHours();
				var minute = datetime.getMinutes();
				var second = datetime.getSeconds();
				var dateFormat = day + '-' + month + '-' + year + '_' + hour + minute + second;
				res.attachment('reporteVentas' + dateFormat + '.xlsx'); // This is sails.js specific (in general you need to set headers)
				res.send(report);

			}
		}
		);



	};



	TdpOrder.remoteMethod('webReportVenta', {
		http: {
			path: '/web/saleReport/:startDate/:finDate/:campaign',
			verb: 'get'
		},
		accepts: [
			{ arg: 'startDate', type: 'string', required: true },
			{ arg: 'finDate', type: 'string', required: true },
			{ arg: 'campaign', type: 'string', required: true },
			{ arg: 'req', type: 'object', 'http': { source: 'req' } },
			{ arg: 'res', type: 'object', 'http': { source: 'res' } }


		],
		returns: {}
	});

	/*TdpOrder.remoteMethod('removeFileReport', {
		http: {
		path: '/web/removeFileReport',
		verb: 'delete'
	  },
	  accepts: [
		{arg: 'req', type: 'object', 'http': {source: 'req'}},
		{arg: 'res', type: 'object', 'http': {source: 'res'}}
	  ],
	  returns: {
		  arg: 'data',
			type: 'string'
	  }
	});*/
};
