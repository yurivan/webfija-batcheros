var _ = require('underscore'),
  TdpSalesAgentsConverter = require('../converters/tdp-salesAgent-converter');

module.exports = function (TdpSalesAgent) {

  TdpSalesAgent.reset = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.insertData(req, TdpSalesAgent.getDataSource(), this, function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.upload = function (req, res, cb) {
    var converter = new TdpSalesAgentsConverter(this.app);
    try {
      converter.processFile(req, TdpSalesAgent.getDataSource(), function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.webReset = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.insertWebData(req, function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.webUpload = function (req, res, cb) {
    var converter = new TdpSalesAgentsConverter(this.app);
    try {
      converter.processFileWeb(req, function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.addSaleAgent = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.saveSaleAgent(req.body, TdpSalesAgent.getDataSource(), function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.updateSaleAgent = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.updateSaleAgent(req.body, TdpSalesAgent.getDataSource(), function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.deleteSaleAgent = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.deleteSaleAgent(req.query.dni, req.query.codatis, TdpSalesAgent.getDataSource(), function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.deleteSaleAgentUser = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.deleteSaleAgentUser(req.query.dni, req.query.codatis, TdpSalesAgent.getDataSource(), function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };
  

  // Sprint 12 - actualiza equivalencias campania
  TdpSalesAgent.updateCampaignEquivalentByChannel = function (req, res, cb) {
    var ds = TdpSalesAgent.getDataSource();

    var equivalencia = req.body.equivalencia;
    var canalEquivalencia = req.body.canalEquivalencia;
    var canalOrigenNuevo = req.body.canalOrigenNuevo;
    var tipificacion = req.body.tipificacion;

    var actualizarPorEntidad = false;
    if (canalOrigenNuevo.indexOf('-') > 0) {
      actualizarPorEntidad = true;
    }

    canalOrigenNuevo = "'" + canalOrigenNuevo.replace(/,/gi, "','") + "'";

    console.log("equivalencia: " + equivalencia + " - canalEquivalencia: " + canalEquivalencia + " - canalOrigenNuevo: " + canalOrigenNuevo);

    // Sprint 12 - reemplazamos la consulta de nombres de campanias
    var updateSentence = '';

    var updateSentenceFlujo = '';

    if ("FICHAPRODUCTO.CANAL" == equivalencia) {
      //if (actualizarPorEntidad) {
        //updateSentence = `UPDATE ibmx_a07e6d02edaf552.tdp_sales_agent SET canalequivcampania='${canalEquivalencia}' where concat(canal,'-',entidad) in (${canalOrigenNuevo})`;
      //} else {
        updateSentence = `UPDATE ibmx_a07e6d02edaf552.tdp_sales_agent SET canalequivcampania='${canalEquivalencia}' where canal in (${canalOrigenNuevo})`;
        updateSentenceFlujo = `UPDATE ibmx_a07e6d02edaf552.tdp_sales_agent SET type_flujo_tipificacion='${tipificacion}' where canalequivcampania='${canalEquivalencia}'`
      //}
    } else if ("FICHAPRODUCTO.ENTIDAD" == equivalencia) {
      updateSentence = `UPDATE ibmx_a07e6d02edaf552.tdp_sales_agent SET entidadequivcampania='${canalEquivalencia}' where entidad in (${canalOrigenNuevo})`
    }

    console.log("updateSentence: " + updateSentence);
    ds.connector.execute(updateSentence, function (err, info) {
      var data = [];
      if (!err) {
        data = info;
        if (data.count > 0) {
          ds.connector.execute(updateSentenceFlujo, function (err, info) {
            if (!err) {
              data = info;
              //cb(null, data);
            } else {
              //cb(err, data);
            }
          });
        }
        cb(null, data);
      } else {
        cb(err, data);
      }
    });
  };

  TdpSalesAgent.addSaleAgentUser = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.saveSaleAgentUser(req.body, TdpSalesAgent.getDataSource(), function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.updateSaleAgentUser = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.updateSaleAgentUser(req.body, TdpSalesAgent.getDataSource(), function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.preloadSalesAgentUser = function (req, res, cb) {
    var converter = new TdpSalesAgentsConverter(this.app);
    try {
      converter.preloadSalesAgentUser(req, function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.uploadSalesAgentUser = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.uploadSalesAgentUser(req, TdpSalesAgent.getDataSource(), function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };


  TdpSalesAgent.preloadSalesSuperUser = function (req, res, cb) {
    var converter = new TdpSalesAgentsConverter(this.app);
    try {
      converter.preloadSalesSuperUser(req, function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };

  TdpSalesAgent.uploadSalesSuperUser = function (req, res, cb) {
    var _this = this;
    try {
      var converter = new TdpSalesAgentsConverter(this.app);
      converter.uploadSalesSuperUser(req, TdpSalesAgent.getDataSource(), function (d) {
        cb(null, d);
      });
    } catch (e) {
      console.error(e);
    }
    return cb.promise;
  };


  // curl -X POST -H "Content-Type: multipart/form-data" --header "Accept: application/json" -F "data=@/tmp/CARTILLA PRODUCTO DICIEMBRE_V9.xlsx" "http://localhost:3000/api/TdpSalesAgents/upload"
  TdpSalesAgent.remoteMethod('upload', {
    http: {
      path: '/upload',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('reset', {
    http: {
      path: '/reset',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('webUpload', {
    http: {
      path: '/web/upload',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('webReset', {
    http: {
      path: '/web/reset',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('updateCampaignEquivalentByChannel', {
    http: {
      path: '/updateCampaignEquivalentByChannel',
      verb: 'put'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('addSaleAgent', {
    http: {
      path: '/salesagents',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('updateSaleAgent', {
    http: {
      path: '/salesagents',
      verb: 'put'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('addSaleAgentUser', {
    http: {
      path: '/salesagentsNiveles',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });


  TdpSalesAgent.remoteMethod('deleteSaleAgentUser', {
    http: {
      path: '/salesagentsNiveles',
      verb: 'delete'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('updateSaleAgentUser', {
    http: {
      path: '/salesagentsNiveles',
      verb: 'put'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('preloadSalesAgentUser', {
    http: {
      path: '/salesagentNiveles/preload',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('uploadSalesAgentUser', {
    http: {
      path: '/salesagentNiveles/upload',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('preloadSalesSuperUser', {
    http: {
      path: '/salesagentSupervisores/preload',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });


  TdpSalesAgent.remoteMethod('uploadSalesSuperUser', {
    http: {
      path: '/salesagentSupervisores/upload',
      verb: 'post'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

  TdpSalesAgent.remoteMethod('deleteSaleAgent', {
    http: {
      path: '/salesagents',
      verb: 'delete'
    },
    accepts: [
      { arg: 'req', type: 'object', 'http': { source: 'req' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } }
    ],
    returns: {
      arg: 'data',
      type: 'object'
    }
  });

};
