package pe.com.tdp.ventafija.microservices.common.util;

public class Constants {
	public static final String MY_SQL_SP_NAME = "[SP_NAME]";
	public static final String MY_SQL_PARAMETER = "[PARAMETER]";
	public static final String MY_SQL_COL = "[COL]";
	public static final String MY_SQL_TABLE = "[TABLE]";
	public static final String MY_SQL_WHERE = "[WHERE]";
}
