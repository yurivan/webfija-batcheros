package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class PagoEfectivoConsultaResponse {

    private String numeroCIP;
    private String idEstadoCIP;
    private String estadoCIP;

    public PagoEfectivoConsultaResponse(){
        super();
    }

    public String getNumeroCIP() {
        return numeroCIP;
    }

    public void setNumeroCIP(String numeroCIP) {
        this.numeroCIP = numeroCIP;
    }

    public String getIdEstadoCIP() {
        return idEstadoCIP;
    }

    public void setIdEstadoCIP(String idEstadoCIP) {
        this.idEstadoCIP = idEstadoCIP;
    }

    public String getEstadoCIP() {
        return estadoCIP;
    }

    public void setEstadoCIP(String estadoCIP) {
        this.estadoCIP = estadoCIP;
    }
}
