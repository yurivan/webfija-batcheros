package pe.com.tdp.ventafija.microservices;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

import javax.servlet.Filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableScheduling;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.util.DatabasePropertiesSource;
import pe.com.tdp.ventafija.microservices.common.web.context.VentaFijaContextPersistenceFilter;

/**
 * 
 * @author hruizroj
 *
 */
@SpringBootApplication
@EnableScheduling
public class EverisThirdpartyTdpApplication {
	private static final Logger logger = LogManager.getLogger();
	private static final String THIRDPARTY_SERVICE_CODE = "THIRDPARTY";

	@Bean
	public Filter log4jMvcFilter() {
		return new Log4JMvcFilter();
	}
	
	@Bean
	public Filter ventaFijaContextPersistenceFilter() {
		return new VentaFijaContextPersistenceFilter(THIRDPARTY_SERVICE_CODE);
	}

	@Bean
	public static PropertySourcesPlaceholderConfigurer properties() {
		final PropertySourcesPlaceholderConfigurer pspc = new PropertySourcesPlaceholderConfigurer();
		DatabasePropertiesSource propertiesSource = new DatabasePropertiesSource(Database.datasource());
		Map<String, Object> props = propertiesSource.loadProperties();
		System.out.print(props);
		pspc.setIgnoreResourceNotFound(true);
		pspc.setIgnoreUnresolvablePlaceholders(true);
		Properties propps = new Properties();
		propps.putAll(props);
		pspc.setProperties(propps);
		return pspc;
	}

	@Bean
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(5);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(25);
		return executor;
	}

	@Bean
	public FilterRegistrationBean someFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(log4jMvcFilter());
		registration.addUrlPatterns("*");
		registration.setName("log4jMvcFilter");
		registration.setOrder(1);
		return registration;
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
//		printProperties();
		SpringApplication.run(EverisThirdpartyTdpApplication.class, args);

		logger.info("everis-thirdparty beta-2.0.0 is running :)");

	}

	private static void printProperties() {
		try {
			printPropertyHeaders();

			Properties applicationProperties = new Properties();
			Properties servicesProperties = new Properties();
			Properties tdpWsProperties = new Properties();
			Properties databaseProperties = new Properties();

			applicationProperties
					.load(EverisThirdpartyTdpApplication.class.getResourceAsStream("/application.properties"));
			databaseProperties.load(new FileInputStream("/ms/config/app.properties"));
			servicesProperties.load(new FileInputStream("/ms/config/services.properties"));
			tdpWsProperties.load(new FileInputStream("/ms/config/tdp-ws-order.properties"));

			printProperties(applicationProperties);
			printProperties(databaseProperties);
			printProperties(servicesProperties);
			printProperties(tdpWsProperties);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void printPropertyHeaders() {
		logger.info("***********************************************************************************");
		logger.info(String.format("* %1$-80s*", "Application configuration"));
		logger.info("***********************************************************************************");
	}

	public static void printProperties(Properties properties) {
		Enumeration keys = properties.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			logger.info(String.format("* %s: %s", key, properties.getProperty(key)));
		}
		logger.info("---");
	}
}