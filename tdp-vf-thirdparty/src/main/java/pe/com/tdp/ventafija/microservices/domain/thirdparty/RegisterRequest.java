package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class RegisterRequest {

  private String saleId;

  public String getSaleId() {
    return saleId;
  }

  public void setSaleId(String saleId) {
    this.saleId = saleId;
  }
}
