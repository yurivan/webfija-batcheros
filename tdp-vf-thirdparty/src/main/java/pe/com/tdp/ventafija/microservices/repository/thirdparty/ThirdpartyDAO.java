package pe.com.tdp.ventafija.microservices.repository.thirdparty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.clients.dto.UpFrontRequestBody;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.dao.ServiceCallEventsDao;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.util.Constants;
import pe.com.tdp.ventafija.microservices.common.util.DateUtils;
import pe.com.tdp.ventafija.microservices.common.util.StringUtils;
import pe.com.tdp.ventafija.microservices.common.util.enums.OrderStatusEnum;
import pe.com.tdp.ventafija.microservices.common.util.exception.SyncException;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.*;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncDevComResult;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncDevTecResult;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncHdecResult;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncHdecSyntaxError;
import pe.com.tdp.ventafija.microservices.util.LogSystem;
import pe.pagoefectivo.service.ws.BEGenRequest;

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

//import pe.pagoefectivo.ws.pre.notificacion.PagoEfectivoNotificacionRequest;

@Repository
public class ThirdpartyDAO {
    private static final Logger logger = LogManager.getLogger();
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;
    @Value("${config.batch.cip.cantidad}")
    private String limitCip;
    @Value("${config.batch.cip.horas}")
    private String horasCip;

    @Autowired
    private ServiceCallEventsDao serviceCallEventsDao;

    public List<UpFrontRequestBody> retrieveUpFrontCandidates(List<String> ids) {
        List<UpFrontRequestBody> result = new ArrayList<>();
        if (ids == null || ids.isEmpty()) {
            return result;
        }
        StringBuilder place_holder = new StringBuilder();
        for (int i = 0; i < ids.size(); i++) {
            if (i == 0) {
                place_holder = new StringBuilder("?");
            } else {
                place_holder.append(",?");
            }
        }
        String sql = String.format("select c.lastname1, c.lastname2, c.email, c.firstname, "
                + "c.customerPhone, u.id, a.nombre, a.apePaterno, a.apeMaterno, c.docnumber, "
                + "o.productCode "
                + "from `order` o "
                + "left join customer c on c.id = o.customerid "
                + "left join `user` u on u.id = o.userid "
                + "left join tdp_sales_agent a on a.codATIS = u.id "
                + "where o.id in (%s) and `status` = ?", place_holder.toString());
        try (Connection con = Database.datasource().getConnection()) {
            PreparedStatement query = con.prepareStatement(sql);
            int i = 1;
            for (String id : ids) {
                query.setString(i, id);
                i++;
            }
            query.setString(i, OrderStatusEnum.EFECTIVO.getCode());
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                String lastName1 = rs.getString(1);
                String lastName2 = rs.getString(2);
                String email = rs.getString(3);
                String firstName = rs.getString(4);
                String customerPhone = rs.getString(5);
                String codAtis = rs.getString(6);
                String nombreVendedor = rs.getString(7);
                String apePaterno = rs.getString(8);
                String apeMaterno = rs.getString(9);
                String docNumber = rs.getString(10);
                String productCode = rs.getString(11);

                UpFrontRequestBody rq = new UpFrontRequestBody();
                rq.setApellidos(String.format("%s %s", lastName1, lastName2));
                rq.setCorreoElectronico(email);
                rq.setNombres(firstName);
                rq.setNumeroContacto(customerPhone);
                rq.setIdUsuarioATIS(codAtis);
                rq.setIdVendedor(codAtis); // TODO verificar
                rq.setNombreVendedor(String.format("%s %s %s", nombreVendedor, apePaterno, apeMaterno));
                rq.setNumeroDocumento(docNumber);
                rq.setProductoId(productCode);

                result.add(rq);
            }
        } catch (SQLException e) {
            logger.error("error en conexion", e);
        }
        return result;
    }

    /**
     * Updates tdp_visor with new data
     *
     * @param hdecs data to process
     * @return list of obj that had been updated
     * @throws SyncException when sync fails :/
     */
    public SyncHdecResult syncHDEC(List<HdecResponseData> hdecs) throws SyncException {
        logger.info("iniciando registro en visor");

        List<HdecResponseData> response = new ArrayList<>();
        List<HdecResponseData> errors = new ArrayList<>();
        int countInsert = 0;
        int countUpdate = 0;
        int countUpdateOrder = 0;
        int totalInsert = 0;
        int totalUpdate = 0;
        int totalError = 0;
        int totalOrderUpdate = 0;

        if (hdecs == null || hdecs.isEmpty()) {
            return new SyncHdecResult();
        }

        int i = 0;
        String existingIds = "";
        List<String> ids = new ArrayList<String>();
        for (HdecResponseData hdec : hdecs) {
            String id = String.format("'%s'", hdec.getId_visor());
            if (i == 0) {
                existingIds = id;
            } else {
                existingIds += "," + id;
            }
            ids.add(hdec.getId_visor());
            i++;
        }
        String selectQuery = "select id_visor, version_fila, estado_solicitud, codigo_pedido from ibmx_a07e6d02edaf552.tdp_visor where id_visor in (" + existingIds + ")";
        try (Connection con = Database.datasource().getConnection()) {

            Map<String, Map<String, String>> result = new HashMap<>();
            PreparedStatement stmt = con.prepareStatement(selectQuery);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String idVisor = rs.getString(1);
                String versionFilaT = rs.getString(2);
                String estadoSolicitud = rs.getString(3);
                String codigoPedido = rs.getString(4);
                Map<String, String> resultDetail = new HashMap<>();
                resultDetail.put("versionFila", versionFilaT);
                resultDetail.put("estado", estadoSolicitud);
                resultDetail.put("codigoPedido",codigoPedido);
                result.put(idVisor, resultDetail);
            }

            PreparedStatement psInsert;
            PreparedStatement psUpdate;
            PreparedStatement psUpdateOrder;

            final int batchSize = 3000;

            String insert = " insert into ibmx_a07e6d02edaf552.tdp_visor (ID_VISOR,BACK,FECHA_GRABACION,ID_GRABACION,CLIENTE,TELEFONO,"
                    + "DIRECCION,DEPARTAMENTO,DNI,NOMBRE_PRODUCTO,ESTADO_SOLICITUD,MOTIVO_ESTADO,SPEECH,"
                    + "ACCION,OPERACION_COMERCIAL,DISTRITO,CODIGO_LLAMADA,SUB_PRODUCTO,TIPO_PRODUCTO,CODIGO_GESTION,"
                    + "CODIGO_SUB_GESTION,FLAG_DEPENDENCIA,VERSION_FILA,TIPO_DOCUMENTO,ACCION_APLICATIVOS_BACK,PROVINCIA,FECHA_DE_LLAMADA,"
                    + "CODIGO_PEDIDO,VERSION_FILA_T,SUB_PRODUCTO_EQUIV,PRODUCTO,estado_anterior,fecha_registrado)"
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            /*String update = " update ibmx_a07e6d02edaf552.tdp_visor set BACK = ?,FECHA_GRABACION = ?,ID_GRABACION = ?,CLIENTE = ?,TELEFONO = ?," //5
                    + " DIRECCION = ?,DEPARTAMENTO = ?,DNI = ?,NOMBRE_PRODUCTO = ?,ESTADO_SOLICITUD = ?,MOTIVO_ESTADO = ?,SPEECH = ?," //12
                    + " ACCION = ?,OPERACION_COMERCIAL = ?,DISTRITO = ?,CODIGO_LLAMADA = ?,SUB_PRODUCTO = ?,TIPO_PRODUCTO = ?,CODIGO_GESTION = ?," //19
                    + " CODIGO_SUB_GESTION = ?,FLAG_DEPENDENCIA = ?,VERSION_FILA = ?,TIPO_DOCUMENTO = ?,ACCION_APLICATIVOS_BACK = ?,PROVINCIA = ?,FECHA_DE_LLAMADA = ?," //26
                    + " CODIGO_PEDIDO = ?,VERSION_FILA_T = ?,SUB_PRODUCTO_EQUIV = ?,PRODUCTO = ?, estado_anterior = ?, " //31
                    + " fecha_registrado = cast(? as timestamp) Where ID_VISOR = ?"; //33
            */
            String update = " update ibmx_a07e6d02edaf552.tdp_visor set BACK = ?,FECHA_GRABACION = ?,ID_GRABACION = ?,CLIENTE = ?,TELEFONO = ?," //5
                    + " DIRECCION = ?,          DNI = ?,NOMBRE_PRODUCTO = ?,ESTADO_SOLICITUD = ?,MOTIVO_ESTADO = ?,SPEECH = ?," //11
                    + " ACCION = ?,OPERACION_COMERCIAL = ?,        CODIGO_LLAMADA = ?,SUB_PRODUCTO = ?,TIPO_PRODUCTO = ?,CODIGO_GESTION = ?," //17
                    + " CODIGO_SUB_GESTION = ?,FLAG_DEPENDENCIA = ?,VERSION_FILA = ?,TIPO_DOCUMENTO = ?,ACCION_APLICATIVOS_BACK = ?,        FECHA_DE_LLAMADA = ?," //23
                    + " CODIGO_PEDIDO = ?,VERSION_FILA_T = ?,SUB_PRODUCTO_EQUIV = ?,PRODUCTO = ?, estado_anterior = ?, " //28
                    + " fecha_registrado = cast(? as timestamp) Where ID_VISOR = ?"; //30

            String updateOrder = "update ibmx_a07e6d02edaf552.order set cmsservicecode = ?, statuslegacy = ? where id = ?";

            psInsert = con.prepareStatement(insert);
            psUpdate = con.prepareStatement(update);
            psUpdateOrder = con.prepareStatement(updateOrder);

//			psInsert.execute("SET FOREIGN_KEY_CHECKS=0");
//			psInsert.execute("SET UNIQUE_CHECKS=0");

            logger.info("numero de registros a procesar: " + hdecs.size());
            for (HdecResponseData hdec : hdecs) {
                String codigoPedido = null;
                Date fechaGrabacion = null;
                Date fechaLlamada = null;
                java.sql.Timestamp fechaGrabacionTimestamp = null;
                java.sql.Timestamp fechaLlamadaTimestamp = null;
                try {
                    fechaGrabacion = DateUtils.parse(hdec.getFecha_grabacion());
                    if (fechaGrabacion != null) {
                        fechaGrabacionTimestamp = new java.sql.Timestamp(fechaGrabacion.getTime());
                    }
                } catch (ParseException e) {
                }
                try {
//					fechaLlamada = DateUtils.parse("yyyy-MM-dd HH:mm:ss.S", hdec.getFecha_de_llamada());
                    fechaLlamada = DateUtils.parse(hdec.getFecha_de_llamada());
                    if (fechaLlamada != null) {
                        fechaLlamadaTimestamp = new java.sql.Timestamp(fechaLlamada.getTime());
                    }
                } catch (ParseException e) {
                }

                Map<String, String> resultDetail = result.get(hdec.getId_visor());

                String versionFilaT = (resultDetail != null) ? resultDetail.get("versionFila") : null; // verifica si ya se registro anteriormente
                if (versionFilaT == null) {
                    countInsert++;
                    psInsert.setString(1, hdec.getId_visor());
                    psInsert.setString(2, hdec.getBack());
                    psInsert.setTimestamp(3, fechaGrabacionTimestamp);
                    psInsert.setString(4, hdec.getId_grabacion());
                    psInsert.setString(5, hdec.getCliente());
                    psInsert.setString(6, hdec.getTelefono());
                    psInsert.setString(7, hdec.getDireccion());
                    psInsert.setString(8, hdec.getDepartamento());
                    psInsert.setString(9, hdec.getDni());
                    psInsert.setString(10, hdec.getNombre_producto());
                    psInsert.setString(11, hdec.getEstado_solicitud());
                    psInsert.setString(12, hdec.getMotivo_estado());
                    psInsert.setString(13, hdec.getSpeech());
                    psInsert.setString(14, hdec.getAccion());
                    psInsert.setString(15, hdec.getOperacion_comercial());
                    psInsert.setString(16, hdec.getDistrito());
                    psInsert.setString(17, hdec.getCodigo_llamada());
                    psInsert.setString(18, hdec.getSub_producto());
                    psInsert.setString(19, hdec.getTipo_producto());
//					psInsert.setString(20, hdec.getCodigo_getion());
                    psInsert.setInt(20, Integer.parseInt(hdec.getCodigo_getion()));
//					psInsert.setString(21, hdec.getCodigo_sub_gestion());
                    psInsert.setInt(21, Integer.parseInt(hdec.getCodigo_sub_gestion()));
                    psInsert.setString(22, hdec.getFlag_dependencia());
                    psInsert.setString(23, hdec.getVersion_fila());
                    psInsert.setString(24, hdec.getTipo_documento());
                    psInsert.setString(25, hdec.getAccion_aplicativo_back());
                    psInsert.setString(26, hdec.getProvincia());
                    psInsert.setTimestamp(27, fechaLlamadaTimestamp);
                    psInsert.setString(28, hdec.getCodigo_pedido());
                    psInsert.setString(29, hdec.getVersion_fila_t());
                    psInsert.setString(30, hdec.getSub_producto_equiv());
                    psInsert.setString(31, hdec.getProducto());
                    psInsert.setString(32, null);

                    if ("INGRESADO".equalsIgnoreCase(hdec.getEstado_solicitud())) {

                        java.sql.Timestamp fechaIngresadoTimestamp = null;
                        Date fechaIngresado = null;

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                        String strDate = df.format(new Date());

                        try {
                            fechaIngresado = DateUtils.parse(strDate);
                            if (fechaGrabacion != null) {
                                fechaIngresadoTimestamp = new java.sql.Timestamp(fechaIngresado.getTime());
                            }
                        } catch (ParseException e) {
                        }

                        psInsert.setTimestamp(33, fechaIngresadoTimestamp);

                    } else {
                        psInsert.setTimestamp(33, null);
                    }

                    // ps.executeUpdate();
                    psInsert.addBatch();

                    resultDetail = new HashMap<String, String>();
                    resultDetail.put("versionFila", hdec.getVersion_fila_t());
                    resultDetail.put("estado", "");

                    result.put(hdec.getId_visor(), resultDetail);
                    response.add(hdec);

                    codigoPedido = hdec.getCodigo_pedido();
                } else {
                    long versionFilaTL = 0L;
                    if (!StringUtils.isEmptyString(versionFilaT)) {
                        try {
                            versionFilaTL = Long.valueOf(versionFilaT);
                        } catch (NumberFormatException ex) {
                            logger.error("Error format", ex);
                            logger.info("Error version fila " + hdec.getId_visor());
                            errors.add(hdec);
                            continue;
                        }
                    }
                    long versionFilaTUpdate = 0L;
                    try {
                        versionFilaTUpdate = Long.valueOf(hdec.getVersion_fila().trim());
                    } catch (NumberFormatException ex) {
                        logger.error("Error format", ex);
                        logger.info("Error version fila " + hdec.getBack());
                        errors.add(hdec);
                        continue;
                    }
                    if (versionFilaTUpdate >= versionFilaTL) {
                        countUpdate++;
                        /*psUpdate.setString(1, hdec.getBack());
                        psUpdate.setTimestamp(2, fechaGrabacionTimestamp);
                        psUpdate.setString(3, hdec.getId_grabacion());
                        psUpdate.setString(4, hdec.getCliente());
                        psUpdate.setString(5, hdec.getTelefono());
                        psUpdate.setString(6, hdec.getDireccion());
                        psUpdate.setString(7, hdec.getDepartamento());
                        psUpdate.setString(8, hdec.getDni());
                        psUpdate.setString(9, hdec.getNombre_producto());
                        psUpdate.setString(10, hdec.getEstado_solicitud());
                        psUpdate.setString(11, hdec.getMotivo_estado());
                        psUpdate.setString(12, hdec.getSpeech());
                        psUpdate.setString(13, hdec.getAccion());
                        psUpdate.setString(14, hdec.getOperacion_comercial());
                        psUpdate.setString(15, hdec.getDistrito());
                        psUpdate.setString(16, hdec.getCodigo_llamada());
                        psUpdate.setString(17, hdec.getSub_producto());
                        psUpdate.setString(18, hdec.getTipo_producto());
//						psUpdate.setString(19, hdec.getCodigo_getion());
                        psUpdate.setInt(19, Integer.parseInt(hdec.getCodigo_getion()));
//						psUpdate.setString(20, hdec.getCodigo_sub_gestion());
                        psUpdate.setInt(20, Integer.parseInt(hdec.getCodigo_sub_gestion()));
                        psUpdate.setString(21, hdec.getFlag_dependencia());
                        psUpdate.setString(22, hdec.getVersion_fila());
                        psUpdate.setString(23, hdec.getTipo_documento());
                        psUpdate.setString(24, hdec.getAccion_aplicativo_back());
                        psUpdate.setString(25, hdec.getProvincia());
                        psUpdate.setTimestamp(26, fechaLlamadaTimestamp);
                        psUpdate.setString(27, hdec.getCodigo_pedido());
                        psUpdate.setString(28, hdec.getVersion_fila_t());
                        psUpdate.setString(29, hdec.getSub_producto_equiv());
                        psUpdate.setString(30, hdec.getProducto());
                        psUpdate.setString(31, resultDetail.get("estado") );

                        if( "INGRESADO".equalsIgnoreCase(hdec.getEstado_solicitud()) ){
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String strDate = df.format(new Date());
                            psUpdate.setString(32, strDate );
                        }else{
                            psUpdate.setString(32, null );
                        }

                        psUpdate.setString(33, hdec.getId_visor());

                        */

                        psUpdate.setString(1, hdec.getBack());
                        psUpdate.setTimestamp(2, fechaGrabacionTimestamp);
                        psUpdate.setString(3, hdec.getId_grabacion());
                        psUpdate.setString(4, hdec.getCliente());
                        psUpdate.setString(5, hdec.getTelefono());
                        psUpdate.setString(6, hdec.getDireccion());
                        //psUpdate.setString(7, hdec.getDepartamento());
                        psUpdate.setString(7, hdec.getDni());
                        psUpdate.setString(8, hdec.getNombre_producto());
                        psUpdate.setString(9, hdec.getEstado_solicitud());
                        psUpdate.setString(10, hdec.getMotivo_estado());
                        psUpdate.setString(11, hdec.getSpeech());
                        psUpdate.setString(12, hdec.getAccion());
                        psUpdate.setString(13, hdec.getOperacion_comercial());
                        //psUpdate.setString(15, hdec.getDistrito());
                        psUpdate.setString(14, hdec.getCodigo_llamada());
                        psUpdate.setString(15, hdec.getSub_producto());
                        psUpdate.setString(16, hdec.getTipo_producto());
//						psUpdate.setString(19, hdec.getCodigo_getion());
                        psUpdate.setInt(17, Integer.parseInt(hdec.getCodigo_getion()));
//						psUpdate.setString(20, hdec.getCodigo_sub_gestion());
                        psUpdate.setInt(18, Integer.parseInt(hdec.getCodigo_sub_gestion()));
                        psUpdate.setString(19, hdec.getFlag_dependencia());
                        psUpdate.setString(20, hdec.getVersion_fila());
                        psUpdate.setString(21, hdec.getTipo_documento());
                        psUpdate.setString(22, hdec.getAccion_aplicativo_back());
                        //psUpdate.setString(25, hdec.getProvincia());
                        psUpdate.setTimestamp(23, fechaLlamadaTimestamp);
                        psUpdate.setString(24, hdec.getCodigo_pedido());
                        if(resultDetail.get("codigoPedido") != null && !resultDetail.get("codigoPedido").equals("")){
                            psUpdate.setString(24, resultDetail.get("codigoPedido"));
                        }
                        psUpdate.setString(25, hdec.getVersion_fila_t());
                        psUpdate.setString(26, hdec.getSub_producto_equiv());
                        psUpdate.setString(27, hdec.getProducto());
                        psUpdate.setString(28, resultDetail.get("estado"));

                        if ("INGRESADO".equalsIgnoreCase(hdec.getEstado_solicitud())) {
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String strDate = df.format(new Date());
                            psUpdate.setString(29, strDate);
                        } else {
                            psUpdate.setString(29, null);
                        }

                        psUpdate.setString(30, hdec.getId_visor());

                        psUpdate.addBatch();
                        response.add(hdec);
                        codigoPedido = hdec.getCodigo_pedido();
                    }
                }

                if (countInsert % batchSize == 0) {
                    int[] rowsInserted = psInsert.executeBatch();
                    totalInsert = processInsertCount(totalInsert, rowsInserted);
                }

                if (countUpdate % batchSize == 0) {
                    int[] rowsUpdated = psUpdate.executeBatch();
                    totalUpdate = processUpdateCount(totalUpdate, rowsUpdated);
                }

                if (codigoPedido != null && hdec.getId_visor() != null && hdec.getId_visor().startsWith("-")) {
                    countUpdateOrder++;
                    psUpdateOrder.setString(1, codigoPedido);
                    psUpdateOrder.setString(2, hdec.getEstado_solicitud());
                    psUpdateOrder.setString(3, hdec.getId_visor());
                    psUpdateOrder.addBatch();
                    if (countUpdateOrder % batchSize == 0) {
                        logger.info("update order");
                        int[] rowsUpdated = psUpdateOrder.executeBatch();
                        totalOrderUpdate = processUpdateCount(totalOrderUpdate, rowsUpdated);
                    }
                }
            }
            int[] rowsInserted = psInsert.executeBatch();
            int[] rowsUpdated = psUpdate.executeBatch();
            int[] rowsUpdateds = psUpdateOrder.executeBatch();
            totalOrderUpdate = processUpdateCount(totalOrderUpdate, rowsUpdateds);
            totalInsert = processInsertCount(totalInsert, rowsInserted);
            totalUpdate = processUpdateCount(totalUpdate, rowsUpdated);

//			psInsert.execute("SET FOREIGN_KEY_CHECKS=1");
//			psInsert.execute("SET UNIQUE_CHECKS=1");
        } catch (SQLException e) {
            logger.error("error en conexion", e);
            throw new SyncException(e);
        }
        logger.info("finalizados registro en visor");

        return new SyncHdecResult(totalInsert, totalUpdate, totalError, totalOrderUpdate, response, errors);
    }

    public SyncHdecResult syncHDEC2(List<HdecResponseData> hdecs, String fileNameTxt, List<SyncHdecSyntaxError> syntaxErrorList) throws SyncException {
        logger.info("iniciando registro en visor");

        List<HdecResponseData> response = new ArrayList<>();
        List<HdecResponseData> errors = new ArrayList<>();
        int countInsert = 0;
        int countInsertTdpOrder = 0;
        int countUpdate = 0;
        int countUpdateOrder = 0;
        int totalInsert = 0;
        int totalInsertTdpOrder = 0;
        int totalUpdate = 0;
        int totalError = 0;
        int totalOrderUpdate = 0;

        if (hdecs == null || hdecs.isEmpty()) {
            return new SyncHdecResult();
        }

        int i = 0;
        String existingIds = "";
        List<String> ids = new ArrayList<String>();
        for (HdecResponseData hdec : hdecs) {
            String id = String.format("'%s'", hdec.getId_visor());
            if (i == 0) {
                existingIds = id;
            } else {
                existingIds += "," + id;
            }
            ids.add(hdec.getId_visor());
            i++;
        }
        String selectQuery = "select id_visor, version_fila, estado_solicitud, codigo_pedido from ibmx_a07e6d02edaf552.tdp_visor where id_visor in (" + existingIds + ")";
        String selectTdpOrderQuery = "SELECT order_id FROM ibmx_a07e6d02edaf552.tdp_order where order_id in (" + existingIds + ")";
        try (Connection con = Database.datasource().getConnection()) {

            Map<String, Map<String, String>> result = new HashMap<>();
            PreparedStatement stmt = con.prepareStatement(selectQuery);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String idVisor = rs.getString(1);
                String versionFilaT = rs.getString(2);
                String estadoSolicitud = rs.getString(3);
                String codigoPedido = rs.getString(4);
                Map<String, String> resultDetail = new HashMap<>();
                resultDetail.put("versionFila", versionFilaT);
                resultDetail.put("estado", estadoSolicitud);
                resultDetail.put("codigoPedido", codigoPedido);
                result.put(idVisor, resultDetail);
            }

            /*Diferenciación en TDP_Order*/
            Map<String, String> resultTdpOrder = new HashMap<>();
            PreparedStatement stmtTdpOrder = con.prepareStatement(selectTdpOrderQuery);

            ResultSet rsTdpOrder = stmtTdpOrder.executeQuery();
            while (rsTdpOrder.next()) {
                String idVisor = rsTdpOrder.getString(1);
                resultTdpOrder.put(idVisor, idVisor);
            }
            /*Diferenciación en TDP_Order*/


            PreparedStatement psInsert;
            PreparedStatement psUpdate;
            PreparedStatement psUpdateOrder;
            PreparedStatement psInsertTdpOrder;
            PreparedStatement psUpdateTdpOrder;

            final int batchSize = 3000;

            String insert = " insert into ibmx_a07e6d02edaf552.tdp_visor (ID_VISOR,BACK,FECHA_GRABACION,ID_GRABACION,CLIENTE,TELEFONO,"
                    + "DIRECCION,DEPARTAMENTO,DNI,NOMBRE_PRODUCTO,ESTADO_SOLICITUD,MOTIVO_ESTADO,SPEECH,"
                    + "ACCION,OPERACION_COMERCIAL,DISTRITO,CODIGO_LLAMADA,SUB_PRODUCTO,TIPO_PRODUCTO,CODIGO_GESTION,"
                    + "CODIGO_SUB_GESTION,FLAG_DEPENDENCIA,VERSION_FILA,TIPO_DOCUMENTO,ACCION_APLICATIVOS_BACK,PROVINCIA,FECHA_DE_LLAMADA,"
                    + "CODIGO_PEDIDO,VERSION_FILA_T,SUB_PRODUCTO_EQUIV,PRODUCTO,estado_anterior,fecha_registrado," +
                    " id_transaccion, visor_create, visor_name_txt)"
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            /*String update = " update ibmx_a07e6d02edaf552.tdp_visor set BACK = ?,FECHA_GRABACION = ?,ID_GRABACION = ?,CLIENTE = ?,TELEFONO = ?," //5
                    + " DIRECCION = ?,DEPARTAMENTO = ?,DNI = ?,NOMBRE_PRODUCTO = ?,ESTADO_SOLICITUD = ?,MOTIVO_ESTADO = ?,SPEECH = ?," //12
                    + " ACCION = ?,OPERACION_COMERCIAL = ?,DISTRITO = ?,CODIGO_LLAMADA = ?,SUB_PRODUCTO = ?,TIPO_PRODUCTO = ?,CODIGO_GESTION = ?," //19
                    + " CODIGO_SUB_GESTION = ?,FLAG_DEPENDENCIA = ?,VERSION_FILA = ?,TIPO_DOCUMENTO = ?,ACCION_APLICATIVOS_BACK = ?,PROVINCIA = ?,FECHA_DE_LLAMADA = ?," //26
                    + " CODIGO_PEDIDO = ?,VERSION_FILA_T = ?,SUB_PRODUCTO_EQUIV = ?,PRODUCTO = ?, estado_anterior = ?, " //31
                    + " fecha_registrado = cast(? as timestamp) Where ID_VISOR = ?"; //33
            */
            String update = " update ibmx_a07e6d02edaf552.tdp_visor set BACK = ?,FECHA_GRABACION = ?,ID_GRABACION = ?,CLIENTE = ?,TELEFONO = ?," //5
                    + " DIRECCION = ?,          DNI = ?,NOMBRE_PRODUCTO = ?,ESTADO_SOLICITUD = ?,MOTIVO_ESTADO = ?,SPEECH = ?," //11
                    + " ACCION = ?,OPERACION_COMERCIAL = ?,        CODIGO_LLAMADA = ?,SUB_PRODUCTO = ?,TIPO_PRODUCTO = ?,CODIGO_GESTION = ?," //17
                    + " CODIGO_SUB_GESTION = ?,FLAG_DEPENDENCIA = ?,VERSION_FILA = ?,TIPO_DOCUMENTO = ?,ACCION_APLICATIVOS_BACK = ?,        FECHA_DE_LLAMADA = ?," //23
                    + " CODIGO_PEDIDO = ?,VERSION_FILA_T = ?,SUB_PRODUCTO_EQUIV = ?,PRODUCTO = ?, estado_anterior = ?, " //28
                    + " fecha_registrado = cast(? as timestamp), visor_update = ?, visor_name_txt = ?, id_transaccion = ?  Where ID_VISOR = ?"; //30

            String updateOrder = "update ibmx_a07e6d02edaf552.order set cmsservicecode = ?, statuslegacy = ? where id = ?";


            String insertTdpOrder = " insert into ibmx_a07e6d02edaf552.tdp_order(product_ps, product_precio_normal, " +
                    "   product_precio_promo, product_precio_promo_mes, product_internet_promo, product_internet_promo_tiempo," +
                    "   sva_nombre_1, sva_precio_1, sva_cantidad_1, sva_code_1, sva_nombre_2, sva_precio_2, sva_cantidad_2, sva_code_2," +
                    "   sva_nombre_3, sva_precio_3, sva_cantidad_3, sva_code_3, sva_nombre_4, sva_precio_4, sva_cantidad_4, sva_code_4," +
                    "   sva_nombre_5, sva_precio_5, sva_cantidad_5, sva_code_5, sva_nombre_6, sva_precio_6, sva_cantidad_6, sva_code_6," +
                    "   sva_nombre_7, sva_precio_7, sva_cantidad_7, sva_code_7, sva_nombre_8, sva_precio_8, sva_cantidad_8, sva_code_8," +
                    "   sva_nombre_9, sva_precio_9, sva_cantidad_9, sva_code_9, sva_nombre_10, sva_precio_10, sva_cantidad_10, sva_code_10," +
                    "   affiliation_electronic_invoice, affiliation_data_protection, affiliation_parental_protection, " +
                    "   product_pay_method, product_pay_price, product_pay_return_month, client_email, product_cost_install," +
                    "   product_equip_tv, product_equip_inter, product_equip_line, product_tec_inter, product_tec_tv," +
                    "   product_pay_return_period, product_internet_vel, order_contrato, disaffiliation_white_pages_guide, " +
                    "   product_tipo_reg, product_cost_install_month, " +
                    "   order_id, order_fecha_hora, order_operation_commercial, order_operation_number, order_origen, client_tipo_doc, " +
                    "   client_numero_doc, client_nombre, client_telefono_1, product_type, address_departamento, address_provincia, " +
                    "   address_distrito, address_principal, auditoria_create, auditoria_modify)" +
                    " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," +
                    " ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            String updateTdpOrder = " update ibmx_a07e6d02edaf552.tdp_order set " +
                    " product_ps = ?,product_precio_normal = ?,product_precio_promo = ?,product_precio_promo_mes = ?,product_internet_promo = ?,"
                    + " product_internet_promo_tiempo = ?,          " +
                    " sva_nombre_1 = ?,sva_precio_1 = ?,sva_cantidad_1 = ?,sva_code_1 = ?," +
                    " sva_nombre_2 = ?,sva_precio_2 = ?,sva_cantidad_2 = ?,sva_code_2 = ?," +
                    " sva_nombre_3 = ?,sva_precio_3 = ?,sva_cantidad_3 = ?,sva_code_3 = ?," +
                    " sva_nombre_4 = ?,sva_precio_4 = ?,sva_cantidad_4 = ?,sva_code_4 = ?," +
                    " sva_nombre_5 = ?,sva_precio_5 = ?,sva_cantidad_5 = ?,sva_code_5 = ?," +
                    " sva_nombre_6 = ?,sva_precio_6 = ?,sva_cantidad_6 = ?,sva_code_6 = ?," +
                    " sva_nombre_7 = ?,sva_precio_7 = ?,sva_cantidad_7 = ?,sva_code_7 = ?," +
                    " sva_nombre_8 = ?,sva_precio_8 = ?,sva_cantidad_8 = ?,sva_code_8 = ?," +
                    " sva_nombre_9 = ?,sva_precio_9 = ?,sva_cantidad_9 = ?,sva_code_9 = ?," +
                    " sva_nombre_10 = ?,sva_precio_10 = ?,sva_cantidad_10 = ?,sva_code_10 = ?," +
                    " affiliation_electronic_invoice = ?,affiliation_data_protection = ?,affiliation_parental_protection = ?," +
                    " product_pay_method = ?,product_pay_price = ?,product_pay_return_month = ?,client_email=?,product_cost_install=?," +
                    " product_equip_tv = ?,product_equip_inter = ?,product_equip_line = ?,product_tec_inter = ?,product_tec_tv = ?, " +
                    " product_pay_return_period = ?, product_internet_vel = ?,order_contrato = ?,disaffiliation_white_pages_guide = ?," +
                    " product_tipo_reg = ?, product_cost_install_month = ?, " +
                    " order_fecha_hora=?, order_operation_commercial=?, order_operation_number=?, order_origen=?, client_tipo_doc=?," +
                    " client_numero_doc=?, client_nombre=?, client_telefono_1=?, product_type=?, address_departamento=?, address_provincia=?," +
                    " address_distrito=?, address_principal=?, auditoria_modify=?" +
                    " Where order_id = ?";

            psInsert = con.prepareStatement(insert);
            psUpdate = con.prepareStatement(update);
            psUpdateOrder = con.prepareStatement(updateOrder);
            psInsertTdpOrder = con.prepareStatement(insertTdpOrder);
            psUpdateTdpOrder = con.prepareStatement(updateTdpOrder);

//			psInsert.execute("SET FOREIGN_KEY_CHECKS=0");
//			psInsert.execute("SET UNIQUE_CHECKS=0");

            logger.info("numero de registros a procesar: " + hdecs.size());
            int counter = 0;
            for (HdecResponseData hdec : hdecs) {
                counter++;
                //logger.info(counter);

                String codigoPedido = null;
                Date fechaGrabacion = null;
                Date fechaLlamada = null;
                Date fechaActual = null;
                java.sql.Timestamp fechaGrabacionTimestamp = null;
                java.sql.Timestamp fechaLlamadaTimestamp = null;
                java.sql.Timestamp fechaActualTimestamp = null;
                try {
                    fechaGrabacion = DateUtils.parse(hdec.getFecha_grabacion());
                    if (fechaGrabacion != null) {
                        fechaGrabacionTimestamp = new java.sql.Timestamp(fechaGrabacion.getTime());
                    }
                } catch (ParseException e) {
                }
                try {
//					fechaLlamada = DateUtils.parse("yyyy-MM-dd HH:mm:ss.S", hdec.getFecha_de_llamada());
                    fechaLlamada = DateUtils.parse(hdec.getFecha_de_llamada());
                    if (fechaLlamada != null) {
                        fechaLlamadaTimestamp = new java.sql.Timestamp(fechaLlamada.getTime());
                    }
                } catch (ParseException e) {
                }
                try {
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                    String strDate = df.format(new Date());
                    fechaActual = DateUtils.parse(strDate);
                    if (fechaGrabacion != null) {
                        fechaActualTimestamp = new java.sql.Timestamp(fechaActual.getTime());
                    }
                } catch (ParseException e) {
                }

                Map<String, String> resultDetail = result.get(hdec.getId_visor());

                String versionFilaT = (resultDetail != null) ? resultDetail.get("versionFila") : null; // verifica si ya se registro anteriormente
                if (versionFilaT == null) {
                    countInsert++;
                    psInsert.setString(1, hdec.getId_visor());
                    psInsert.setString(2, hdec.getBack());
                    psInsert.setTimestamp(3, fechaGrabacionTimestamp);
                    psInsert.setString(4, hdec.getId_grabacion());
                    psInsert.setString(5, hdec.getCliente());
                    psInsert.setString(6, hdec.getTelefono());
                    psInsert.setString(7, hdec.getDireccion());
                    psInsert.setString(8, hdec.getDepartamento());
                    psInsert.setString(9, hdec.getDni());
                    psInsert.setString(10, hdec.getNombre_producto());
                    psInsert.setString(11, hdec.getEstado_solicitud());
                    psInsert.setString(12, hdec.getMotivo_estado());
                    psInsert.setString(13, hdec.getSpeech());
                    psInsert.setString(14, hdec.getAccion());
                    psInsert.setString(15, hdec.getOperacion_comercial());
                    psInsert.setString(16, hdec.getDistrito());
                    psInsert.setString(17, hdec.getCodigo_llamada());
                    psInsert.setString(18, hdec.getSub_producto());
                    psInsert.setString(19, hdec.getTipo_producto());
//					psInsert.setString(20, hdec.getCodigo_getion());
                    Integer codigoGestion = -1;
                    try {
                        codigoGestion = Integer.parseInt(hdec.getCodigo_getion());
                    } catch (Exception ex) {
                    }
                    psInsert.setInt(20, codigoGestion);
//					psInsert.setString(21, hdec.getCodigo_sub_gestion());
                    Integer codigoSubGestion = -1;
                    try {
                        codigoSubGestion = Integer.parseInt(hdec.getCodigo_sub_gestion());
                    } catch (Exception ex) {
                    }
                    psInsert.setInt(21, codigoSubGestion);
                    psInsert.setString(22, hdec.getFlag_dependencia());
                    psInsert.setString(23, hdec.getVersion_fila());
                    psInsert.setString(24, hdec.getTipo_documento());
                    psInsert.setString(25, hdec.getAccion_aplicativo_back());
                    psInsert.setString(26, hdec.getProvincia());
                    psInsert.setTimestamp(27, fechaLlamadaTimestamp);
                    psInsert.setString(28, hdec.getCodigo_pedido());
                    psInsert.setString(29, hdec.getVersion_fila_t());
                    psInsert.setString(30, hdec.getSub_producto_equiv());
                    psInsert.setString(31, hdec.getProducto());
                    psInsert.setString(32, null);

                    if ("INGRESADO".equalsIgnoreCase(hdec.getEstado_solicitud())) {

                        /*java.sql.Timestamp fechaIngresadoTimestamp = null;
                        Date fechaIngresado = null;

                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                        String strDate = df.format(new Date());

                        try {
                            fechaIngresado = DateUtils.parse(strDate);
                            if (fechaGrabacion != null) {
                                fechaIngresadoTimestamp = new java.sql.Timestamp(fechaIngresado.getTime());
                            }
                        } catch (ParseException e) {
                        }

                        psInsert.setTimestamp(33, fechaIngresadoTimestamp );*/

                        psInsert.setTimestamp(33, fechaActualTimestamp);

                    } else {
                        psInsert.setTimestamp(33, null);
                    }

                    psInsert.setString(34, hdec.getId_transaccion());
                    psInsert.setTimestamp(35, fechaActualTimestamp);
                    psInsert.setString(36, fileNameTxt);

                    // ps.executeUpdate();
                    psInsert.addBatch();

                    resultDetail = new HashMap<String, String>();
                    resultDetail.put("versionFila", hdec.getVersion_fila_t());
                    resultDetail.put("estado", "");

                    result.put(hdec.getId_visor(), resultDetail);
                    response.add(hdec);

                    codigoPedido = hdec.getCodigo_pedido();

                    /* ----------------------INSERT INTO TDP_ORDER---------------------- */

                    if (resultTdpOrder.get(hdec.getId_visor()) == null) {

                        psInsertTdpOrder.setString(1, hdec.getProducto_ps());
                        Double precioNormalProducto = null;
                        try {
                            String str = hdec.getPrecio_normal_producto();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioNormalProducto = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioNormalProducto != null) psInsertTdpOrder.setDouble(2, precioNormalProducto);
                        else psInsertTdpOrder.setNull(2, Types.DOUBLE);
                        Double precioPromoProducto = null;
                        try {
                            String str = hdec.getPrecio_promo_producto();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioPromoProducto = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioPromoProducto != null) psInsertTdpOrder.setDouble(3, precioPromoProducto);
                        else psInsertTdpOrder.setNull(3, Types.DOUBLE);
                        Integer mesesPromoProducto = null;
                        try {
                            mesesPromoProducto = Integer.parseInt(hdec.getMeses_promo_producto());
                        } catch (Exception ex) {
                        }
                        if (mesesPromoProducto != null) psInsertTdpOrder.setInt(4, mesesPromoProducto);
                        else psInsertTdpOrder.setNull(4, Types.INTEGER);
                        Integer velocidadPromoInter = null;
                        try {
                            velocidadPromoInter = Integer.parseInt(hdec.getVelocidad_promo_inter());
                        } catch (Exception ex) {
                        }
                        if (velocidadPromoInter != null) psInsertTdpOrder.setInt(5, velocidadPromoInter);
                        else psInsertTdpOrder.setNull(5, Types.INTEGER);
                        Integer tiempoPromoInter = null;
                        try {
                            tiempoPromoInter = Integer.parseInt(hdec.getTiempo_promo_inter());
                        } catch (Exception ex) {
                        }
                        if (tiempoPromoInter != null) psInsertTdpOrder.setInt(6, tiempoPromoInter);
                        else psInsertTdpOrder.setNull(6, Types.INTEGER);

                        /////////////////

                        psInsertTdpOrder.setString(7, hdec.getNombre_sva1());

                        Double precioSva1 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva1();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva1 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva1 != null) psInsertTdpOrder.setDouble(8, precioSva1);
                        else psInsertTdpOrder.setNull(8, Types.DOUBLE);
                        Integer cantidadSva1 = null;
                        try {
                            cantidadSva1 = Integer.parseInt(hdec.getCantidad_sva1());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva1 != null) psInsertTdpOrder.setInt(9, cantidadSva1);
                        else psInsertTdpOrder.setNull(9, Types.INTEGER);

                        psInsertTdpOrder.setString(10, hdec.getCode_sva1());
                        psInsertTdpOrder.setString(11, hdec.getNombre_sva2());

                        Double precioSva2 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva2();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva2 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva2 != null) psInsertTdpOrder.setDouble(12, precioSva2);
                        else psInsertTdpOrder.setNull(12, Types.DOUBLE);
                        Integer cantidadSva2 = null;
                        try {
                            cantidadSva2 = Integer.parseInt(hdec.getCantidad_sva2());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva2 != null) psInsertTdpOrder.setInt(13, cantidadSva2);
                        else psInsertTdpOrder.setNull(13, Types.INTEGER);

                        psInsertTdpOrder.setString(14, hdec.getCode_sva2());
                        psInsertTdpOrder.setString(15, hdec.getNombre_sva3());

                        Double precioSva3 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva3();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva3 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva3 != null) psInsertTdpOrder.setDouble(16, precioSva3);
                        else psInsertTdpOrder.setNull(16, Types.DOUBLE);
                        Integer cantidadSva3 = null;
                        try {
                            cantidadSva3 = Integer.parseInt(hdec.getCantidad_sva3());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva3 != null) psInsertTdpOrder.setInt(17, cantidadSva3);
                        else psInsertTdpOrder.setNull(17, Types.INTEGER);

                        psInsertTdpOrder.setString(18, hdec.getCode_sva3());
                        psInsertTdpOrder.setString(19, hdec.getNombre_sva4());

                        Double precioSva4 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva4();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva4 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva4 != null) psInsertTdpOrder.setDouble(20, precioSva4);
                        else psInsertTdpOrder.setNull(20, Types.DOUBLE);
                        Integer cantidadSva4 = null;
                        try {
                            cantidadSva4 = Integer.parseInt(hdec.getCantidad_sva4());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva4 != null) psInsertTdpOrder.setInt(21, cantidadSva4);
                        else psInsertTdpOrder.setNull(21, Types.INTEGER);

                        psInsertTdpOrder.setString(22, hdec.getCode_sva4());
                        psInsertTdpOrder.setString(23, hdec.getNombre_sva5());

                        Double precioSva5 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva5();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva5 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva5 != null) psInsertTdpOrder.setDouble(24, precioSva5);
                        else psInsertTdpOrder.setNull(24, Types.DOUBLE);
                        Integer cantidadSva5 = null;
                        try {
                            cantidadSva5 = Integer.parseInt(hdec.getCantidad_sva5());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva5 != null) psInsertTdpOrder.setInt(25, cantidadSva5);
                        else psInsertTdpOrder.setNull(25, Types.INTEGER);

                        psInsertTdpOrder.setString(26, hdec.getCode_sva5());
                        psInsertTdpOrder.setString(27, hdec.getNombre_sva6());

                        Double precioSva6 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva6();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva6 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva6 != null) psInsertTdpOrder.setDouble(28, precioSva6);
                        else psInsertTdpOrder.setNull(28, Types.DOUBLE);
                        Integer cantidadSva6 = null;
                        try {
                            cantidadSva6 = Integer.parseInt(hdec.getCantidad_sva6());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva6 != null) psInsertTdpOrder.setInt(29, cantidadSva6);
                        else psInsertTdpOrder.setNull(29, Types.INTEGER);

                        psInsertTdpOrder.setString(30, hdec.getCode_sva6());
                        psInsertTdpOrder.setString(31, hdec.getNombre_sva7());

                        Double precioSva7 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva7();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva7 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva7 != null) psInsertTdpOrder.setDouble(32, precioSva7);
                        else psInsertTdpOrder.setNull(32, Types.DOUBLE);
                        Integer cantidadSva7 = null;
                        try {
                            cantidadSva7 = Integer.parseInt(hdec.getCantidad_sva7());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva7 != null) psInsertTdpOrder.setInt(33, cantidadSva7);
                        else psInsertTdpOrder.setNull(33, Types.INTEGER);

                        psInsertTdpOrder.setString(34, hdec.getCode_sva7());
                        psInsertTdpOrder.setString(35, hdec.getNombre_sva8());

                        Double precioSva8 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva8();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva8 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva8 != null) psInsertTdpOrder.setDouble(36, precioSva8);
                        else psInsertTdpOrder.setNull(36, Types.DOUBLE);
                        Integer cantidadSva8 = null;
                        try {
                            cantidadSva8 = Integer.parseInt(hdec.getCantidad_sva8());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva8 != null) psInsertTdpOrder.setInt(37, cantidadSva8);
                        else psInsertTdpOrder.setNull(37, Types.INTEGER);

                        psInsertTdpOrder.setString(38, hdec.getCode_sva8());
                        psInsertTdpOrder.setString(39, hdec.getNombre_sva9());

                        Double precioSva9 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva9();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva9 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva9 != null) psInsertTdpOrder.setDouble(40, precioSva9);
                        else psInsertTdpOrder.setNull(40, Types.DOUBLE);
                        Integer cantidadSva9 = null;
                        try {
                            cantidadSva9 = Integer.parseInt(hdec.getCantidad_sva9());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva9 != null) psInsertTdpOrder.setInt(41, cantidadSva9);
                        else psInsertTdpOrder.setNull(41, Types.INTEGER);

                        psInsertTdpOrder.setString(42, hdec.getCode_sva9());
                        psInsertTdpOrder.setString(43, hdec.getNombre_sva10());

                        Double precioSva10 = null;
                        try {
                            String str = hdec.getPrecio_uni_sva10();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            precioSva10 = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (precioSva10 != null) psInsertTdpOrder.setDouble(44, precioSva10);
                        else psInsertTdpOrder.setNull(44, Types.DOUBLE);
                        Integer cantidadSva10 = null;
                        try {
                            cantidadSva10 = Integer.parseInt(hdec.getCantidad_sva10());
                        } catch (Exception ex) {
                        }
                        if (cantidadSva10 != null) psInsertTdpOrder.setInt(45, cantidadSva10);
                        else psInsertTdpOrder.setNull(45, Types.INTEGER);

                        psInsertTdpOrder.setString(46, hdec.getCode_sva10());

                        ////////////////

                        psInsertTdpOrder.setString(47, hdec.getAceptacion_recibo_digital());
                        psInsertTdpOrder.setString(48, hdec.getAceptacion_proteccion_datos());
                        psInsertTdpOrder.setString(49, hdec.getFiltro_web_parental());
                        psInsertTdpOrder.setString(50, hdec.getModalidad_pago());
                        Double montoContado = null;
                        try {
                            String str = hdec.getMonto_contado();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            montoContado = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (montoContado != null) psInsertTdpOrder.setDouble(51, montoContado);
                        else psInsertTdpOrder.setNull(51, Types.DOUBLE);
                        Integer mesesDevolucion = null;
                        try {
                            mesesDevolucion = Integer.parseInt(hdec.getMeses_devolucion());
                        } catch (Exception ex) {
                        }
                        if (mesesDevolucion != null) psInsertTdpOrder.setInt(52, mesesDevolucion);
                        else psInsertTdpOrder.setNull(52, Types.INTEGER);

                        psInsertTdpOrder.setString(53, hdec.getEmail_cliente());

                        Double costoInstalacion = null;
                        try {
                            String str = hdec.getCosto_instalacion();
                            str = str.replaceAll("S/.", "").replaceAll(",", ".");
                            costoInstalacion = Double.parseDouble(str);
                        } catch (Exception ex) {
                        }
                        if (costoInstalacion != null) psInsertTdpOrder.setDouble(54, costoInstalacion);
                        else psInsertTdpOrder.setNull(54, Types.DOUBLE);

                        psInsertTdpOrder.setString(55, hdec.getEquipamiento_tv());
                        psInsertTdpOrder.setString(56, hdec.getEquipamiento_internet());
                        psInsertTdpOrder.setString(57, hdec.getEquipamiento_linea());
                        psInsertTdpOrder.setString(58, hdec.getTecnologia_internet());
                        psInsertTdpOrder.setString(59, hdec.getTecnologia_tv());

                        psInsertTdpOrder.setString(60, hdec.getPeriodo_retorno());
                        Integer velocidadInternet = null;
                        try {
                            velocidadInternet = Integer.parseInt(hdec.getVelocidad_internet());
                        } catch (Exception ex) {
                        }
                        if (velocidadInternet != null) psInsertTdpOrder.setInt(61, velocidadInternet);
                        else psInsertTdpOrder.setNull(61, Types.INTEGER);

                        psInsertTdpOrder.setString(62, hdec.getEnvio_contrato());
                        psInsertTdpOrder.setString(63, hdec.getDesafiliacion_pb());
                        psInsertTdpOrder.setString(64, hdec.getTipo_registro());

                        Integer mesesFinanciamiento = null;
                        try {
                            mesesFinanciamiento = Integer.parseInt(hdec.getMeses_financiamiento());
                        } catch (Exception ex) {
                        }
                        if (mesesFinanciamiento != null) psInsertTdpOrder.setInt(65, mesesFinanciamiento);
                        else psInsertTdpOrder.setNull(65, Types.INTEGER);


                        psInsertTdpOrder.setString(66, hdec.getId_visor());
                        psInsertTdpOrder.setTimestamp(67, fechaGrabacionTimestamp);
                        psInsertTdpOrder.setString(68, hdec.getOperacion_comercial());
                        psInsertTdpOrder.setInt(69, hdec.getNumero_operacion());
                        psInsertTdpOrder.setString(70, "TXT");
                        psInsertTdpOrder.setString(71, hdec.getTipo_documento());

                        psInsertTdpOrder.setString(72, hdec.getDni());
                        psInsertTdpOrder.setString(73, hdec.getCliente());
                        psInsertTdpOrder.setString(74, hdec.getTelefono());
                        psInsertTdpOrder.setString(75, hdec.getTipo_producto());
                        psInsertTdpOrder.setString(76, hdec.getDepartamento());
                        psInsertTdpOrder.setString(77, hdec.getProvincia());
                        psInsertTdpOrder.setString(78, hdec.getDistrito());

                        psInsertTdpOrder.setString(79, hdec.getDireccion());
                        psInsertTdpOrder.setTimestamp(80, fechaActualTimestamp);
                        psInsertTdpOrder.setTimestamp(81, null);

                        psInsertTdpOrder.addBatch();

                    }
                    /* ----------------------INSERT INTO TDP_ORDER---------------------- */

                } else {
/*                    long versionFilaTL = 0L;
                    if (!StringUtils.isEmptyString(versionFilaT)) {
                        try {
                            versionFilaTL = Long.valueOf(versionFilaT);
                        } catch (NumberFormatException ex) {
                            logger.error("Error format", ex);
                            logger.info("Error version fila " + hdec.getId_visor());
                            errors.add(hdec);
                            continue;
                        }
                    }
                    long versionFilaTUpdate = 0L;
                    try {
                        versionFilaTUpdate = Long.valueOf(hdec.getVersion_fila().trim());
                    } catch (NumberFormatException ex) {
                        logger.error("Error format", ex);
                        logger.info("Error version fila " + hdec.getBack());
                        errors.add(hdec);
                        continue;
                    }
                    if (versionFilaTUpdate >= versionFilaTL) {*/
                    countUpdate++;
                        /*psUpdate.setString(1, hdec.getBack());
                        psUpdate.setTimestamp(2, fechaGrabacionTimestamp);
                        psUpdate.setString(3, hdec.getId_grabacion());
                        psUpdate.setString(4, hdec.getCliente());
                        psUpdate.setString(5, hdec.getTelefono());
                        psUpdate.setString(6, hdec.getDireccion());
                        psUpdate.setString(7, hdec.getDepartamento());
                        psUpdate.setString(8, hdec.getDni());
                        psUpdate.setString(9, hdec.getNombre_producto());
                        psUpdate.setString(10, hdec.getEstado_solicitud());
                        psUpdate.setString(11, hdec.getMotivo_estado());
                        psUpdate.setString(12, hdec.getSpeech());
                        psUpdate.setString(13, hdec.getAccion());
                        psUpdate.setString(14, hdec.getOperacion_comercial());
                        psUpdate.setString(15, hdec.getDistrito());
                        psUpdate.setString(16, hdec.getCodigo_llamada());
                        psUpdate.setString(17, hdec.getSub_producto());
                        psUpdate.setString(18, hdec.getTipo_producto());
//						psUpdate.setString(19, hdec.getCodigo_getion());
                        psUpdate.setInt(19, Integer.parseInt(hdec.getCodigo_getion()));
//						psUpdate.setString(20, hdec.getCodigo_sub_gestion());
                        psUpdate.setInt(20, Integer.parseInt(hdec.getCodigo_sub_gestion()));
                        psUpdate.setString(21, hdec.getFlag_dependencia());
                        psUpdate.setString(22, hdec.getVersion_fila());
                        psUpdate.setString(23, hdec.getTipo_documento());
                        psUpdate.setString(24, hdec.getAccion_aplicativo_back());
                        psUpdate.setString(25, hdec.getProvincia());
                        psUpdate.setTimestamp(26, fechaLlamadaTimestamp);
                        psUpdate.setString(27, hdec.getCodigo_pedido());
                        psUpdate.setString(28, hdec.getVersion_fila_t());
                        psUpdate.setString(29, hdec.getSub_producto_equiv());
                        psUpdate.setString(30, hdec.getProducto());
                        psUpdate.setString(31, resultDetail.get("estado") );

                        if( "INGRESADO".equalsIgnoreCase(hdec.getEstado_solicitud()) ){
                            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String strDate = df.format(new Date());
                            psUpdate.setString(32, strDate );
                        }else{
                            psUpdate.setString(32, null );
                        }

                        psUpdate.setString(33, hdec.getId_visor());

                        */

                    psUpdate.setString(1, hdec.getBack());
                    psUpdate.setTimestamp(2, fechaGrabacionTimestamp);
                    psUpdate.setString(3, hdec.getId_grabacion());
                    psUpdate.setString(4, hdec.getCliente());
                    psUpdate.setString(5, hdec.getTelefono());
                    psUpdate.setString(6, hdec.getDireccion());
                    //psUpdate.setString(7, hdec.getDepartamento());
                    psUpdate.setString(7, hdec.getDni());
                    psUpdate.setString(8, hdec.getNombre_producto());
                    psUpdate.setString(9, hdec.getEstado_solicitud());
                    psUpdate.setString(10, hdec.getMotivo_estado());
                    psUpdate.setString(11, hdec.getSpeech());
                    psUpdate.setString(12, hdec.getAccion());
                    psUpdate.setString(13, hdec.getOperacion_comercial());
                    //psUpdate.setString(15, hdec.getDistrito());
                    psUpdate.setString(14, hdec.getCodigo_llamada());
                    psUpdate.setString(15, hdec.getSub_producto());
                    psUpdate.setString(16, hdec.getTipo_producto());
//						psUpdate.setString(19, hdec.getCodigo_getion());
                    Integer codigoGestion = -1;
                    try {
                        codigoGestion = Integer.parseInt(hdec.getCodigo_getion());
                    } catch (Exception ex) {
                    }
                    psUpdate.setInt(17, codigoGestion);
//						psUpdate.setString(20, hdec.getCodigo_sub_gestion());
                    Integer codigoSubGestion = -1;
                    try {
                        codigoSubGestion = Integer.parseInt(hdec.getCodigo_sub_gestion());
                    } catch (Exception ex) {
                    }
                    psUpdate.setInt(18, codigoSubGestion);
                    psUpdate.setString(19, hdec.getFlag_dependencia());
                    psUpdate.setString(20, hdec.getVersion_fila());
                    psUpdate.setString(21, hdec.getTipo_documento());
                    psUpdate.setString(22, hdec.getAccion_aplicativo_back());
                    //psUpdate.setString(25, hdec.getProvincia());
                    psUpdate.setTimestamp(23, fechaLlamadaTimestamp);
                    psUpdate.setString(24, hdec.getCodigo_pedido());
                    if(resultDetail.get("codigoPedido") != null && !resultDetail.get("codigoPedido").equals("")){
                        psUpdate.setString(24, resultDetail.get("codigoPedido"));
                    }
                    psUpdate.setString(25, hdec.getVersion_fila_t());
                    psUpdate.setString(26, hdec.getSub_producto_equiv());
                    psUpdate.setString(27, hdec.getProducto());
                    psUpdate.setString(28, resultDetail.get("estado"));

                    if ("INGRESADO".equalsIgnoreCase(hdec.getEstado_solicitud())) {
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String strDate = df.format(new Date());
                        psUpdate.setString(29, strDate);
                    } else {
                        psUpdate.setString(29, null);
                    }


                    try {
                        ServiceCallEvent datos = new ServiceCallEvent();
                        if (hdec.getBack().equalsIgnoreCase("TGESTIONA_APP")) {
                            if (!"ENVIANDO".equalsIgnoreCase(hdec.getEstado_solicitud()))
                                datos = cargarDatosServiceCallEvent(hdec, fileNameTxt);
                            serviceCallEventsDao.registerEvent(datos);
                        }
                    } catch (Exception e) {
                        logger.info("Problema al registrar en el ACTUALIZA_BATCHER : " + e);
                    }


                    psUpdate.setTimestamp(30, fechaActualTimestamp);
                    psUpdate.setString(31, fileNameTxt);
                    psUpdate.setString(32, hdec.getId_transaccion());
                    psUpdate.setString(33, hdec.getId_visor());

                    psUpdate.addBatch();
                    response.add(hdec);
                    codigoPedido = hdec.getCodigo_pedido();

                    /* -----------------UPDATE EN TDP_ORDER-----------------*/

                    psUpdateTdpOrder.setString(1, hdec.getProducto_ps());
                    Double precioNormalProducto = null;
                    try {
                        String str = hdec.getPrecio_normal_producto();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioNormalProducto = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioNormalProducto != null) psUpdateTdpOrder.setDouble(2, precioNormalProducto);
                    else psUpdateTdpOrder.setNull(2, Types.DOUBLE);
                    Double precioPromoProducto = null;
                    try {
                        String str = hdec.getPrecio_promo_producto();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioPromoProducto = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioPromoProducto != null) psUpdateTdpOrder.setDouble(3, precioPromoProducto);
                    else psUpdateTdpOrder.setNull(3, Types.DOUBLE);
                    Integer mesesPromoProducto = null;
                    try {
                        mesesPromoProducto = Integer.parseInt(hdec.getMeses_promo_producto());
                    } catch (Exception ex) {
                    }
                    if (mesesPromoProducto != null) psUpdateTdpOrder.setInt(4, mesesPromoProducto);
                    else psUpdateTdpOrder.setNull(4, Types.INTEGER);
                    Integer velocidadPromoInter = null;
                    try {
                        velocidadPromoInter = Integer.parseInt(hdec.getVelocidad_promo_inter());
                    } catch (Exception ex) {
                    }
                    if (velocidadPromoInter != null) psUpdateTdpOrder.setInt(5, velocidadPromoInter);
                    else psUpdateTdpOrder.setNull(5, Types.INTEGER);
                    Integer tiempoPromoInter = null;
                    try {
                        tiempoPromoInter = Integer.parseInt(hdec.getTiempo_promo_inter());
                    } catch (Exception ex) {
                    }
                    if (tiempoPromoInter != null) psUpdateTdpOrder.setInt(6, tiempoPromoInter);
                    else psUpdateTdpOrder.setNull(6, Types.INTEGER);

                    psUpdateTdpOrder.setString(7, hdec.getNombre_sva1());

                    Double precioSva1 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva1();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva1 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva1 != null) psUpdateTdpOrder.setDouble(8, precioSva1);
                    else psUpdateTdpOrder.setNull(8, Types.DOUBLE);
                    Integer cantidadSva1 = null;
                    try {
                        cantidadSva1 = Integer.parseInt(hdec.getCantidad_sva1());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva1 != null) psUpdateTdpOrder.setInt(9, cantidadSva1);
                    else psUpdateTdpOrder.setNull(9, Types.INTEGER);

                    psUpdateTdpOrder.setString(10, hdec.getCode_sva1());
                    psUpdateTdpOrder.setString(11, hdec.getNombre_sva2());

                    Double precioSva2 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva2();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva2 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva2 != null) psUpdateTdpOrder.setDouble(12, precioSva2);
                    else psUpdateTdpOrder.setNull(12, Types.DOUBLE);
                    Integer cantidadSva2 = null;
                    try {
                        cantidadSva2 = Integer.parseInt(hdec.getCantidad_sva2());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva2 != null) psUpdateTdpOrder.setInt(13, cantidadSva2);
                    else psUpdateTdpOrder.setNull(13, Types.INTEGER);

                    psUpdateTdpOrder.setString(14, hdec.getCode_sva2());
                    psUpdateTdpOrder.setString(15, hdec.getNombre_sva3());

                    Double precioSva3 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva3();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva3 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva3 != null) psUpdateTdpOrder.setDouble(16, precioSva3);
                    else psUpdateTdpOrder.setNull(16, Types.DOUBLE);
                    Integer cantidadSva3 = null;
                    try {
                        cantidadSva3 = Integer.parseInt(hdec.getCantidad_sva3());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva3 != null) psUpdateTdpOrder.setInt(17, cantidadSva3);
                    else psUpdateTdpOrder.setNull(17, Types.INTEGER);

                    psUpdateTdpOrder.setString(18, hdec.getCode_sva3());
                    psUpdateTdpOrder.setString(19, hdec.getNombre_sva4());

                    Double precioSva4 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva4();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva4 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva4 != null) psUpdateTdpOrder.setDouble(20, precioSva4);
                    else psUpdateTdpOrder.setNull(20, Types.DOUBLE);
                    Integer cantidadSva4 = null;
                    try {
                        cantidadSva4 = Integer.parseInt(hdec.getCantidad_sva4());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva4 != null) psUpdateTdpOrder.setInt(21, cantidadSva4);
                    else psUpdateTdpOrder.setNull(21, Types.INTEGER);

                    psUpdateTdpOrder.setString(22, hdec.getCode_sva4());
                    psUpdateTdpOrder.setString(23, hdec.getNombre_sva5());

                    Double precioSva5 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva5();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva5 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva5 != null) psUpdateTdpOrder.setDouble(24, precioSva5);
                    else psUpdateTdpOrder.setNull(24, Types.DOUBLE);
                    Integer cantidadSva5 = null;
                    try {
                        cantidadSva5 = Integer.parseInt(hdec.getCantidad_sva5());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva5 != null) psUpdateTdpOrder.setInt(25, cantidadSva5);
                    else psUpdateTdpOrder.setNull(25, Types.INTEGER);

                    psUpdateTdpOrder.setString(26, hdec.getCode_sva5());
                    psUpdateTdpOrder.setString(27, hdec.getNombre_sva6());

                    Double precioSva6 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva6();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva6 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva6 != null) psUpdateTdpOrder.setDouble(28, precioSva6);
                    else psUpdateTdpOrder.setNull(28, Types.DOUBLE);
                    Integer cantidadSva6 = null;
                    try {
                        cantidadSva6 = Integer.parseInt(hdec.getCantidad_sva6());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva6 != null) psUpdateTdpOrder.setInt(29, cantidadSva6);
                    else psUpdateTdpOrder.setNull(29, Types.INTEGER);

                    psUpdateTdpOrder.setString(30, hdec.getCode_sva6());
                    psUpdateTdpOrder.setString(31, hdec.getNombre_sva7());

                    Double precioSva7 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva7();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva7 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva7 != null) psUpdateTdpOrder.setDouble(32, precioSva7);
                    else psUpdateTdpOrder.setNull(32, Types.DOUBLE);
                    Integer cantidadSva7 = null;
                    try {
                        cantidadSva7 = Integer.parseInt(hdec.getCantidad_sva7());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva7 != null) psUpdateTdpOrder.setInt(33, cantidadSva7);
                    else psUpdateTdpOrder.setNull(33, Types.INTEGER);

                    psUpdateTdpOrder.setString(34, hdec.getCode_sva7());
                    psUpdateTdpOrder.setString(35, hdec.getNombre_sva8());

                    Double precioSva8 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva8();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva8 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva8 != null) psUpdateTdpOrder.setDouble(36, precioSva8);
                    else psUpdateTdpOrder.setNull(36, Types.DOUBLE);
                    Integer cantidadSva8 = null;
                    try {
                        cantidadSva8 = Integer.parseInt(hdec.getCantidad_sva8());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva8 != null) psUpdateTdpOrder.setInt(37, cantidadSva8);
                    else psUpdateTdpOrder.setNull(37, Types.INTEGER);

                    psUpdateTdpOrder.setString(38, hdec.getCode_sva8());
                    psUpdateTdpOrder.setString(39, hdec.getNombre_sva9());

                    Double precioSva9 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva9();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva9 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva9 != null) psUpdateTdpOrder.setDouble(40, precioSva9);
                    else psUpdateTdpOrder.setNull(40, Types.DOUBLE);
                    Integer cantidadSva9 = null;
                    try {
                        cantidadSva9 = Integer.parseInt(hdec.getCantidad_sva9());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva9 != null) psUpdateTdpOrder.setInt(41, cantidadSva9);
                    else psUpdateTdpOrder.setNull(41, Types.INTEGER);

                    psUpdateTdpOrder.setString(42, hdec.getCode_sva9());
                    psUpdateTdpOrder.setString(43, hdec.getNombre_sva10());

                    Double precioSva10 = null;
                    try {
                        String str = hdec.getPrecio_uni_sva10();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        precioSva10 = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (precioSva10 != null) psUpdateTdpOrder.setDouble(44, precioSva10);
                    else psUpdateTdpOrder.setNull(44, Types.DOUBLE);
                    Integer cantidadSva10 = null;
                    try {
                        cantidadSva10 = Integer.parseInt(hdec.getCantidad_sva10());
                    } catch (Exception ex) {
                    }
                    if (cantidadSva10 != null) psUpdateTdpOrder.setInt(45, cantidadSva10);
                    else psUpdateTdpOrder.setNull(45, Types.INTEGER);

                    psUpdateTdpOrder.setString(46, hdec.getCode_sva10());

                    psUpdateTdpOrder.setString(47, hdec.getAceptacion_recibo_digital());
                    psUpdateTdpOrder.setString(48, hdec.getAceptacion_proteccion_datos());
                    psUpdateTdpOrder.setString(49, hdec.getFiltro_web_parental());
                    psUpdateTdpOrder.setString(50, hdec.getModalidad_pago());
                    Double montoContado = null;
                    try {
                        String str = hdec.getMonto_contado();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        montoContado = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (montoContado != null) psUpdateTdpOrder.setDouble(51, montoContado);
                    else psUpdateTdpOrder.setNull(51, Types.DOUBLE);
                    Integer mesesDevolucion = null;
                    try {
                        mesesDevolucion = Integer.parseInt(hdec.getMeses_devolucion());
                    } catch (Exception ex) {
                    }
                    if (mesesDevolucion != null) psUpdateTdpOrder.setInt(52, mesesDevolucion);
                    else psUpdateTdpOrder.setNull(52, Types.INTEGER);

                    psUpdateTdpOrder.setString(53, hdec.getEmail_cliente());

                    Double costoInstalacion = null;
                    try {
                        String str = hdec.getCosto_instalacion();
                        str = str.replaceAll("S/.", "").replaceAll(",", ".");
                        costoInstalacion = Double.parseDouble(str);
                    } catch (Exception ex) {
                    }
                    if (costoInstalacion != null) psUpdateTdpOrder.setDouble(54, costoInstalacion);
                    else psUpdateTdpOrder.setNull(54, Types.DOUBLE);

                    psUpdateTdpOrder.setString(55, hdec.getEquipamiento_tv());
                    psUpdateTdpOrder.setString(56, hdec.getEquipamiento_internet());
                    psUpdateTdpOrder.setString(57, hdec.getEquipamiento_linea());
                    psUpdateTdpOrder.setString(58, hdec.getTecnologia_internet());
                    psUpdateTdpOrder.setString(59, hdec.getTecnologia_tv());

                    psUpdateTdpOrder.setString(60, hdec.getPeriodo_retorno());

                    Integer velocidadInternet = null;
                    try {
                        velocidadInternet = Integer.parseInt(hdec.getVelocidad_internet());
                    } catch (Exception ex) {
                    }
                    if (velocidadInternet != null) psUpdateTdpOrder.setInt(61, velocidadInternet);
                    else psUpdateTdpOrder.setNull(61, Types.INTEGER);

                    psUpdateTdpOrder.setString(62, hdec.getEnvio_contrato());
                    psUpdateTdpOrder.setString(63, hdec.getDesafiliacion_pb());
                    psUpdateTdpOrder.setString(64, hdec.getTipo_registro());

                    Integer mesesFinanciamiento = null;
                    try {
                        mesesFinanciamiento = Integer.parseInt(hdec.getMeses_financiamiento());
                    } catch (Exception ex) {
                    }
                    if (mesesFinanciamiento != null) psUpdateTdpOrder.setInt(65, mesesFinanciamiento);
                    else psUpdateTdpOrder.setNull(65, Types.INTEGER);


                    psUpdateTdpOrder.setTimestamp(66, fechaGrabacionTimestamp);
                    psUpdateTdpOrder.setString(67, hdec.getOperacion_comercial());
                    psUpdateTdpOrder.setInt(68, hdec.getNumero_operacion());
                    psUpdateTdpOrder.setString(69, "TXT");
                    psUpdateTdpOrder.setString(70, hdec.getTipo_documento());

                    psUpdateTdpOrder.setString(71, hdec.getDni());
                    psUpdateTdpOrder.setString(72, hdec.getCliente());
                    psUpdateTdpOrder.setString(73, hdec.getTelefono());
                    psUpdateTdpOrder.setString(74, hdec.getTipo_producto());
                    psUpdateTdpOrder.setString(75, hdec.getDepartamento());
                    psUpdateTdpOrder.setString(76, hdec.getProvincia());
                    psUpdateTdpOrder.setString(77, hdec.getDistrito());

                    psUpdateTdpOrder.setString(78, hdec.getDireccion());
                    psUpdateTdpOrder.setTimestamp(79, fechaActualTimestamp);
                    psUpdateTdpOrder.setString(80, hdec.getId_visor());

                    psUpdateTdpOrder.addBatch();

                    /* -----------------UPDATE EN TDP_ORDER-----------------*/
                    /*}*/
                }

                if (countInsert % batchSize == 0) {
                    int[] rowsInserted = psInsert.executeBatch();
                    totalInsert = processInsertCount(totalInsert, rowsInserted);
                }

                if (countUpdate % batchSize == 0) {
                    int[] rowsUpdated = psUpdate.executeBatch();
                    totalUpdate = processUpdateCount(totalUpdate, rowsUpdated);
                }

                if (codigoPedido != null && hdec.getId_visor() != null && hdec.getId_visor().startsWith("-")) {
                    countUpdateOrder++;
                    psUpdateOrder.setString(1, codigoPedido);
                    psUpdateOrder.setString(2, hdec.getEstado_solicitud());
                    psUpdateOrder.setString(3, hdec.getId_visor());
                    psUpdateOrder.addBatch();
                    if (countUpdateOrder % batchSize == 0) {
                        logger.info("update order");
                        int[] rowsUpdated = psUpdateOrder.executeBatch();
                        totalOrderUpdate = processUpdateCount(totalOrderUpdate, rowsUpdated);
                    }
                }
            }
            int[] rowsInserted = psInsert.executeBatch();
            int[] rowsUpdated = psUpdate.executeBatch();
            int[] rowsUpdateds = psUpdateOrder.executeBatch();
            totalOrderUpdate = processUpdateCount(totalOrderUpdate, rowsUpdateds);
            totalInsert = processInsertCount(totalInsert, rowsInserted);
            totalUpdate = processUpdateCount(totalUpdate, rowsUpdated);

            int[] rowsInsertedTdpOrder = psInsertTdpOrder.executeBatch();
            int totalInsertLast = processInsertCount(0, rowsInsertedTdpOrder);
            logger.info("Inserts in Tdp_Order: " + totalInsertLast);

            int[] rowsUpdatedTdpOrder = psUpdateTdpOrder.executeBatch();
            int totalUpdateLast = processInsertCount(0, rowsUpdatedTdpOrder);
            logger.info("Updates in Tdp_Order: " + totalUpdateLast);

            String syntaxErrorText = "";
            for (SyncHdecSyntaxError obj : syntaxErrorList) {
                syntaxErrorText = syntaxErrorText + "\n --> " + obj.getLine();
            }

            processVisorLog(con, fileNameTxt, hdecs.size(), syntaxErrorList.size(), errors.size(), syntaxErrorText);

//			psInsert.execute("SET FOREIGN_KEY_CHECKS=1");
//			psInsert.execute("SET UNIQUE_CHECKS=1");
        } catch (SQLException e) {
            logger.error("error en conexion: " + e.getMessage());

            try (Connection con = Database.datasource().getConnection()) {
                saveVisorLog(con, fileNameTxt, "ERROR - TXT", e.getMessage(), 0, 0);
            } catch (Exception ex) {
                logger.error("error al guardar Log en Exception: " + ex.getMessage());
            }

            throw new SyncException(e);
        }
        logger.info("finalizados registro en visor");

        return new SyncHdecResult(totalInsert, totalUpdate, totalError, totalOrderUpdate, response, errors);
    }

    private ServiceCallEvent cargarDatosServiceCallEvent(HdecResponseData hdec, String file) {
        ServiceCallEvent data = new ServiceCallEvent();
        //JSONObject obj = new JSONObject(hdec);

        data.setUsername("default");
        data.setMsg("OK");
        data.setOrderId(hdec.getId_visor());
        data.setDocNumber(hdec.getDni());
        data.setServiceCode("ACTUALIZA_BATCHER");
        data.setServiceUrl("TXT - TGESTIONA");
        data.setServiceRequest(" " + hdec + "/" + hdec.getEstado_solicitud());
        data.setServiceResponse("Actualizacion Exitosa :" + file);
        data.setSourceApp(hdec.getBack());
        data.setSourceAppVersion("1.0");
        data.setResult("OK");
        return data;
    }

    public void processVisorLog(Connection con, String txtName, Integer rowsDone, Integer rowsSyntaxError, Integer rowsVersionFilaError, String syntaxErrorText) {
        try {
            Integer totalErrors = rowsSyntaxError + rowsVersionFilaError;
            String status = totalErrors == 0 ? "OK" : "ERROR - FILAS";
            String detail = "";

            if (rowsSyntaxError > 0) detail = detail + "- Errores de sintaxis: \n" + syntaxErrorText + "\n \n";
            if (rowsVersionFilaError > 0) detail = detail + "- Errores en Versión de Fila";

            saveVisorLog(con, txtName, status, detail, rowsDone, totalErrors);
        } catch (Exception ex) {
            logger.error("error al procesar Log: " + ex.getMessage());
        }
    }

    public void saveVisorLog(Connection con, String txtName, String status, String detail, Integer rowsDone, Integer rowsError) {
        try {
            String insert = " INSERT INTO ibmx_a07e6d02edaf552.visor_load_log (TXT_NAME,LOAD_DATE,STATUS,DETAIL,ROWS_DONE,ROWS_ERROR)"
                    + " VALUES (?,?,?,?,?,?)";
            PreparedStatement psInsert = con.prepareStatement(insert);

            Timestamp timestamp = new Timestamp(new Date().getTime());

            psInsert.setString(1, txtName);
            psInsert.setTimestamp(2, timestamp);
            psInsert.setString(3, status);
            psInsert.setString(4, detail);
            psInsert.setInt(5, rowsDone);
            psInsert.setInt(6, rowsError);

            psInsert.executeUpdate();

        } catch (Exception ex) {
            logger.error("error al guardar Log:" + ex.getMessage());
        }
    }

    public void saveVisorLogTxtNotFound() {
        try (Connection con = Database.datasource().getConnection()) {
            saveVisorLog(con, "", "NOT FOUND", "txt not found", 0, 0);
        } catch (Exception ex) {
            logger.error("error al guardar Log Txt Found:" + ex.getMessage());
        }
    }

    public SyncDevComResult syncDevComercial(List<DevComercialResponseData> devcomList) throws SyncException {
        logger.info("iniciando registro en dev com");

        List<DevComercialResponseData> response = new ArrayList<>();
        List<DevComercialResponseData> errors = new ArrayList<>();
        int countInsert = 0;
        //int countUpdate = 0;
        //int countUpdateOrder = 0;
        int totalInsert = 0;
        //int totalUpdate = 0;
        int totalError = 0;
        int totalOrderUpdate = 0;

        if (devcomList == null || devcomList.isEmpty()) {
            return new SyncDevComResult();
        }

        devcomList = filterRequiredDevCom(devcomList);

        devcomList = filterDuplicatesDevCom(devcomList);

        int i = 0;
        String existingIds = "";
        List<String> ids = new ArrayList<String>();
        for (DevComercialResponseData devcom : devcomList) {
            String id = String.format("'%s'", devcom.getRequerimiento());
            if (i == 0) {
                existingIds = id;
            } else {
                existingIds += "," + id;
            }
            ids.add(devcom.getRequerimiento());
            i++;
        }
        String selectQuery = "select requerimiento, fecha_gestion, fecha_carga from ibmx_a07e6d02edaf552.devuelta_comercial where requerimiento in (" + existingIds + ")";
        try (Connection con = Database.datasource().getConnection()) {

            Map<String, Map<String, String>> result = new HashMap<>();
            PreparedStatement stmt = con.prepareStatement(selectQuery);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String requerimiento = rs.getString(1);
                String fechaGestion = rs.getString(2);
                String fechaCarga = rs.getString(3);
                Map<String, String> resultDetail = new HashMap<>();
                resultDetail.put("requerimiento", requerimiento);
                resultDetail.put("fechaGestion", fechaGestion);
                resultDetail.put("fechaCarga", fechaCarga);
                result.put(requerimiento, resultDetail);
            }

            PreparedStatement psInsert;
            //PreparedStatement psUpdate;
            //PreparedStatement psUpdateOrder;
            PreparedStatement psCaidas;

            final int batchSize = 3000;

            String insert = " insert into ibmx_a07e6d02edaf552.devuelta_comercial (REQUERIMIENTO,GESTION,DESMOTV,DES_MOT,DET_MOT,NOMB_TCO,"
                    + "TEMATICO_1,TEMATICO_2,TEMATICO_3,TEMATICO_4,FECAGEN,HORAAGEN,OBSGEST,FECHA_CARGA,FECHA_GESTION,SITUA,USU_GES,CREADO)"
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            String caidas = " insert into ibmx_a07e6d02edaf552.pedidos_en_caida (CODIGO_PEDIDO, TIPO_CAIDA, INTENTOS, ESTADO,INSERT_UPDATE)"
                    + " values (?,?,?,?,?)";

            //String update = " update ibmx_a07e6d02edaf552.tdp_visor set DEV_FECHA_GESTION = ?,DEV_GESTION = ?,DEV_TEMATICO1 = ?,DEV_TEMATICO2 = ?,DEV_TEMATICO3 = ?,"
            //+ " DEV_TEMATICO4 = ?,DEV_FECHA_CARGA = ?,DEV_MOTIVO = ?,DEV_NOMBRE_TECNICO = ?, DEV_TIPO = ?, ESTADO_SOLICITUD = ? Where CODIGO_PEDIDO = ?";

            //String updateOrder = "update ibmx_a07e6d02edaf552.order set cmsservicecode = ?, statuslegacy = ? where id = ?";

            psInsert = con.prepareStatement(insert);
            //psUpdate = con.prepareStatement(update);
            //psUpdateOrder = con.prepareStatement(updateOrder);

            psCaidas = con.prepareStatement(caidas);

//			psInsert.execute("SET FOREIGN_KEY_CHECKS=0");
//			psInsert.execute("SET UNIQUE_CHECKS=0");

            logger.info("numero de registros a procesar: " + devcomList.size());
            for (DevComercialResponseData devcom : devcomList) {
                //String codigoPedido = null;

                Map<String, String> resultDetail = result.get(devcom.getRequerimiento());

                boolean toSave = true;
                if (resultDetail != null) {

                    String fechaGestionR = resultDetail.get("fechaGestion");
                    String fechaCargaR = resultDetail.get("fechaCarga");

                    if (devcom.getFecha_gestion() != null && !devcom.getFecha_gestion().trim().equals("")
                            && !devcom.getFecha_gestion().trim().equals("NULL")) {

                        if (devcom.getFecha_gestion().equals(fechaGestionR)) {
                            toSave = false;
                        }

                    } else {

                        if (devcom.getFecha_carga() != null && !devcom.getFecha_carga().trim().equals("")
                                && !devcom.getFecha_carga().trim().equals("NULL")) {

                            if (devcom.getFecha_carga().equals(fechaCargaR)) {
                                toSave = false;
                            }

                        }

                    }

                }

                if (!toSave) continue;

                Date fechaGestion = null;
                Date fechaCarga = null;
                java.sql.Timestamp fechaGestionTimestamp = null;
                java.sql.Timestamp fechaCargaTimestamp = null;
                java.sql.Timestamp fechaCreacionTimestamp = null;
                try {
                    fechaGestion = DateUtils.parse(devcom.getFecha_gestion(), "yyyy-MM-dd HH:mm:ss");
                    if (fechaGestion != null) {
                        fechaGestionTimestamp = new java.sql.Timestamp(fechaGestion.getTime());
                    }
                } catch (ParseException e) {
                }
                try {
                    fechaCarga = DateUtils.parse(devcom.getFecha_carga(), "yyyy-MM-dd HH:mm:ss");
                    if (fechaCarga != null) {
                        fechaCargaTimestamp = new java.sql.Timestamp(fechaCarga.getTime());
                    }
                } catch (ParseException e) {
                }
                try {
                    fechaCreacionTimestamp = new java.sql.Timestamp(new Date().getTime());
                } catch (Exception e) {
                }


                countInsert++;

                psInsert.setString(1, devcom.getRequerimiento());
                psInsert.setString(2, devcom.getGestion());
                psInsert.setString(3, devcom.getDesmotv());
                psInsert.setString(4, devcom.getDes_mot());
                psInsert.setString(5, devcom.getDet_mot());
                psInsert.setString(6, devcom.getNomb_tco());
                psInsert.setString(7, devcom.getTematico1());
                psInsert.setString(8, devcom.getTematico2());
                psInsert.setString(9, devcom.getTematico3());
                psInsert.setString(10, devcom.getTematico4());
                psInsert.setString(11, devcom.getFecagen());
                psInsert.setString(12, devcom.getHoraagen());
                psInsert.setString(13, devcom.getObsgest());
                psInsert.setTimestamp(14, fechaCargaTimestamp);
                psInsert.setTimestamp(15, fechaGestionTimestamp);
                psInsert.setString(16, devcom.getSitua());
                psInsert.setString(17, devcom.getUsu_ges());
                psInsert.setTimestamp(18, fechaCreacionTimestamp);

                // ps.executeUpdate();

                psInsert.addBatch();
                response.add(devcom);

                /*if (countInsert % batchSize == 0) {
                    int[] rowsInserted = psInsert.executeBatch();
                    totalInsert = processInsertCount(totalInsert, rowsInserted);
                }*/

                //NO comunicacion
                if (devcom.getTematico1().equalsIgnoreCase("sin contacto") &&
                        (devcom.getTematico2().equalsIgnoreCase("cancelado") || devcom.getTematico2().equalsIgnoreCase("cancelar")) &&
                        (devcom.getTematico3().equalsIgnoreCase("ultimo tratamiento") || devcom.getTematico3().equalsIgnoreCase("ultimo trat"))) {
                    psCaidas.setString(1, devcom.getRequerimiento());
                    psCaidas.setString(2, "DEV_COM_NO");
                    psCaidas.setInt(3, 0);
                    psCaidas.setBoolean(4, false);
                    psCaidas.setTimestamp(5, fechaCreacionTimestamp);

                    psCaidas.addBatch();
                }

                //A pedido del cliente
                if ((devcom.getGestion().equalsIgnoreCase("retencion") &&
                        (devcom.getTematico1().equalsIgnoreCase("cancelar") || devcom.getTematico1().equalsIgnoreCase("cancelado")) &&
                        devcom.getTematico2().equalsIgnoreCase("cliente no desea"))
                        ||
                        (!devcom.getGestion().equalsIgnoreCase("retencion") &&
                                devcom.getTematico1().equalsIgnoreCase("con contacto") &&
                                (devcom.getTematico2().equalsIgnoreCase("cancelado") || devcom.getTematico2().equalsIgnoreCase("cancelar"))
                        )) {
                    psCaidas.setString(1, devcom.getRequerimiento());
                    psCaidas.setString(2, "DEV_COM_CL");
                    psCaidas.setInt(3, 0);
                    psCaidas.setBoolean(4, false);
                    psCaidas.setTimestamp(5, fechaCreacionTimestamp);

                    psCaidas.addBatch();
                }

            }
            int[] rowsInserted = psInsert.executeBatch();
            //int[] rowsUpdated = psUpdate.executeBatch();
            //int[] rowsUpdateds = psUpdateOrder.executeBatch();
            //totalOrderUpdate = processUpdateCount(totalOrderUpdate, rowsUpdateds);
            totalInsert = processInsertCount(totalInsert, rowsInserted);
            //totalUpdate = processUpdateCount(totalUpdate, rowsUpdated);

            int[] rowsInsertedCaidas = psCaidas.executeBatch();
            int totalInsertCaidas = processInsertCount(0, rowsInsertedCaidas);
            logger.info("Caidas en devueltas comerciales: " + totalInsertCaidas);

//			psInsert.execute("SET FOREIGN_KEY_CHECKS=1");
//			psInsert.execute("SET UNIQUE_CHECKS=1");
        } catch (SQLException e) {
            logger.error("error en conexion", e);
            throw new SyncException(e);
        }
        logger.info("finalizados registro en visor");

        return new SyncDevComResult(totalInsert, 0, totalError, totalOrderUpdate, response, errors);
    }

    public SyncDevTecResult syncDevTecnica(List<DevTecnicaResponseData> devtecList) throws SyncException {
        logger.info("iniciando registro en dev tec");

        List<DevTecnicaResponseData> response = new ArrayList<>();
        List<DevTecnicaResponseData> errors = new ArrayList<>();
        int countInsert = 0;
        //int countUpdate = 0;
        //int countUpdateOrder = 0;
        int totalInsert = 0;
        //int totalUpdate = 0;
        int totalError = 0;
        int totalOrderUpdate = 0;

        if (devtecList == null || devtecList.isEmpty()) {
            return new SyncDevTecResult();
        }

        devtecList = filterRequiredDevTec(devtecList);

        devtecList = filterDuplicatesDevTec(devtecList);

        int i = 0;
        String existingIds = "";
        List<String> ids = new ArrayList<String>();
        for (DevTecnicaResponseData devtec : devtecList) {
            String id = String.format("'%s'", devtec.getNumero_peticion());
            if (i == 0) {
                existingIds = id;
            } else {
                existingIds += "," + id;
            }
            ids.add(devtec.getNumero_peticion());
            i++;
        }
        String selectQuery = "select numero_peticion, fecha_liquidacion, fecha_ingreso from ibmx_a07e6d02edaf552.devuelta_tecnica where numero_peticion in (" + existingIds + ")";
        try (Connection con = Database.datasource().getConnection()) {

            Map<String, Map<String, String>> result = new HashMap<>();
            PreparedStatement stmt = con.prepareStatement(selectQuery);

            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String numeroPeticion = rs.getString(1);
                String fechaLiquidacion = rs.getString(2);
                String fechaIngreso = rs.getString(3);
                Map<String, String> resultDetail = new HashMap<>();
                resultDetail.put("numeroPeticion", numeroPeticion);
                resultDetail.put("fechaLiquidacion", fechaLiquidacion);
                resultDetail.put("fechaIngreso", fechaIngreso);
                result.put(numeroPeticion, resultDetail);
            }

            PreparedStatement psInsert;
            //PreparedStatement psUpdate;
            //PreparedStatement psUpdateOrder;
            PreparedStatement psCaidas;

            final int batchSize = 3000;

            String insert = " insert into ibmx_a07e6d02edaf552.devuelta_tecnica (NUMERO_PETICION, CODIGO_MOTIVO, MOTIVO, DESCRIPCION, TEMATICO_1," +
                    " TEMATICO_2,TEMATICO_3,FECHA_INGRESO,FECHA_LIQUIDACION,ESTADO,USUARIO,CREADO)"
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?)";

            String caidas = " insert into ibmx_a07e6d02edaf552.pedidos_en_caida (CODIGO_PEDIDO, TIPO_CAIDA, INTENTOS, ESTADO,INSERT_UPDATE)"
                    + " values (?,?,?,?,?)";

            /*String update = " update ibmx_a07e6d02edaf552.tdp_visor set DEV_FECHA_GESTION = ?,DEV_GESTION = ?,DEV_TEMATICO1 = ?,DEV_TEMATICO2 = ?,DEV_TEMATICO3 = ?,"
                    + " DEV_TEMATICO4 = ?,DEV_FECHA_CARGA = ?,DEV_MOTIVO = ?,DEV_NOMBRE_TECNICO = ?, DEV_TIPO = ?, ESTADO_SOLICITUD = ? Where CODIGO_PEDIDO = ?";

            String updateOrder = "update ibmx_a07e6d02edaf552.order set cmsservicecode = ?, statuslegacy = ? where id = ?";*/

            psInsert = con.prepareStatement(insert);
            //psUpdate = con.prepareStatement(update);
            //psUpdateOrder = con.prepareStatement(updateOrder);
            psCaidas = con.prepareStatement(caidas);

//			psInsert.execute("SET FOREIGN_KEY_CHECKS=0");
//			psInsert.execute("SET UNIQUE_CHECKS=0");

            logger.info("numero de registros a procesar: " + devtecList.size());
            for (DevTecnicaResponseData devtec : devtecList) {
                //String codigoPedido = null;

                Map<String, String> resultDetail = result.get(devtec.getNumero_peticion());

                boolean toSave = true;
                if (resultDetail != null) {
                    String fechaLiquidacionR = resultDetail.get("fechaLiquidacion");
                    String fechaIngresoR = resultDetail.get("fechaIngreso");

                    if (devtec.getFecha_liquidacion() != null && !devtec.getFecha_liquidacion().trim().equals("")
                            && !devtec.getFecha_liquidacion().trim().equals("NULL")) {

                        if (devtec.getFecha_liquidacion().equals(fechaLiquidacionR)) {
                            toSave = false;
                        }
                    } else {
                        if (devtec.getFecha_ingreso() != null && !devtec.getFecha_ingreso().trim().equals("")
                                && !devtec.getFecha_ingreso().trim().equals("NULL")) {

                            if (devtec.getFecha_ingreso().equals(fechaIngresoR)) {
                                toSave = false;
                            }
                        }
                    }
                }

                if (!toSave) continue;

                Date fechaIngreso = null;
                Date fechaLiquidacion = null;
                java.sql.Timestamp fechaIngresoTimestamp = null;
                java.sql.Timestamp fechaLiquidacionTimestamp = null;
                java.sql.Timestamp fechaCreacionTimestamp = null;
                try {
                    fechaIngreso = DateUtils.parse(devtec.getFecha_ingreso(), "yyyy-MM-dd HH:mm:ss");
                    if (fechaIngreso != null) {
                        fechaIngresoTimestamp = new java.sql.Timestamp(fechaIngreso.getTime());
                    }
                } catch (ParseException e) {
                }
                try {
                    fechaLiquidacion = DateUtils.parse(devtec.getFecha_liquidacion(), "yyyy-MM-dd HH:mm:ss");
                    if (fechaLiquidacion != null) {
                        fechaLiquidacionTimestamp = new java.sql.Timestamp(fechaLiquidacion.getTime());
                    }
                } catch (ParseException e) {
                }
                try {
                    fechaCreacionTimestamp = new java.sql.Timestamp(new Date().getTime());
                } catch (Exception e) {
                }


                countInsert++;

                psInsert.setString(1, devtec.getNumero_peticion());
                psInsert.setInt(2, (devtec.getCodigo_motivo() != null && !devtec.getCodigo_motivo().trim().equals("")) ? Integer.parseInt(devtec.getCodigo_motivo()) : null);
                psInsert.setString(3, devtec.getMotivo());
                psInsert.setString(4, devtec.getDescripcion().replaceAll("\u0000", ""));
                psInsert.setString(5, devtec.getTematico_1());
                psInsert.setString(6, devtec.getTematico_2());
                psInsert.setString(7, devtec.getTematico_3());
                psInsert.setTimestamp(8, fechaIngresoTimestamp);
                psInsert.setTimestamp(9, fechaLiquidacionTimestamp);
                psInsert.setString(10, devtec.getEstado());
                psInsert.setString(11, devtec.getUsuario());
                psInsert.setTimestamp(12, fechaCreacionTimestamp);

                // ps.executeUpdate();

                psInsert.addBatch();
                response.add(devtec);

                /*if (countInsert % batchSize == 0) {
                    int[] rowsInserted = psInsert.executeBatch();
                    totalInsert = processInsertCount(totalInsert, rowsInserted);
                }*/

                if (devtec.getTematico_1() != null && devtec.getTematico_1().equalsIgnoreCase("conforme") &&
                        devtec.getTematico_2() != null && devtec.getTematico_2().equalsIgnoreCase("en proceso de cancelacion")) {
                    psCaidas.setString(1, devtec.getNumero_peticion());
                    psCaidas.setString(2, "DEV_TEC");
                    psCaidas.setInt(3, 0);
                    psCaidas.setBoolean(4, false);
                    psCaidas.setTimestamp(5, fechaCreacionTimestamp);

                    psCaidas.addBatch();
                }
            }
            int[] rowsInserted = psInsert.executeBatch();
            //int[] rowsUpdated = psUpdate.executeBatch();
            //int[] rowsUpdateds = psUpdateOrder.executeBatch();
            //totalOrderUpdate = processUpdateCount(totalOrderUpdate, rowsUpdateds);
            totalInsert = processInsertCount(totalInsert, rowsInserted);
            //totalUpdate = processUpdateCount(totalUpdate, rowsUpdated);

            int[] rowsInsertedCaidas = psCaidas.executeBatch();
            int totalInsertCaidas = processInsertCount(0, rowsInsertedCaidas);
            logger.info("Caidas en devueltas tecnicas: " + totalInsertCaidas);

//			psInsert.execute("SET FOREIGN_KEY_CHECKS=1");
//			psInsert.execute("SET UNIQUE_CHECKS=1");
        } catch (SQLException e) {
            logger.error("error en conexion", e);
            throw new SyncException(e);
        }
        logger.info("finalizados registro en devueltas tecnicas");

        return new SyncDevTecResult(totalInsert, 0, totalError, totalOrderUpdate, response, errors);
    }

    /**
     * process insert result from insertBatch Response
     * <p>
     * //@param currentCount
     * //@param insertResults
     *
     * @return new count
     */

    private List<DevComercialResponseData> filterDuplicatesDevCom(List<DevComercialResponseData> devcomList) {
        try {
            List<DevComercialResponseData> devcomListNew = new ArrayList<>();
            List<String> ids = new ArrayList<>();

            for (int i = 0; i < devcomList.size(); i++) {
                boolean toAdd = true;
                for (int j = 0; j < devcomListNew.size(); j++) {

                    if (devcomList.get(i).getRequerimiento().equals(devcomListNew.get(j).getRequerimiento())) {

                        toAdd = false;

                        try {
                            Date oldDate = null;
                            Date newDate = null;
                            if (!devcomList.get(i).getFecha_gestion().equals("") && !devcomListNew.get(j).getFecha_gestion().equals("")) {
                                //Por gestion
                                oldDate = DateUtils.parse(devcomList.get(i).getFecha_gestion(), "yyyy-MM-dd HH:mm:ss");
                                newDate = DateUtils.parse(devcomListNew.get(j).getFecha_gestion(), "yyyy-MM-dd HH:mm:ss");
                            } else {
                                //Por carga
                                oldDate = DateUtils.parse(devcomList.get(i).getFecha_carga(), "yyyy-MM-dd HH:mm:ss");
                                newDate = DateUtils.parse(devcomListNew.get(j).getFecha_carga(), "yyyy-MM-dd HH:mm:ss");
                            }

                            if (oldDate.compareTo(newDate) == 1) {
                                devcomListNew.set(j, devcomList.get(i));
                            }
                        } catch (Exception fex) {
                            logger.error(fex.getMessage());
                        }

                    }

                }

                if (toAdd) devcomListNew.add(devcomList.get(i));

            }
            return devcomListNew;
        } catch (Exception ex) {
            return devcomList;
        }
    }

    private List<DevComercialResponseData> filterRequiredDevCom(List<DevComercialResponseData> devcomList) {
        try {
            List<DevComercialResponseData> devcomListNew = new ArrayList<>();

            /*Calendar calNow = Calendar.getInstance();
            calNow.setTime(new Date());*/

            Date now = DateUtils.parse("2018-07-22 10:00:00", "yyyy-MM-dd HH:mm:ss");

            for (DevComercialResponseData obj : devcomList) {

                Date fechaCarga = null;
                Date fechaGestion = null;
                try {
                    fechaCarga = DateUtils.parse(obj.getFecha_carga(), "yyyy-MM-dd HH:mm:ss");
                } catch (ParseException e) {
                }
                try {
                    fechaGestion = DateUtils.parse(obj.getFecha_gestion(), "yyyy-MM-dd HH:mm:ss");
                } catch (ParseException e) {
                }


                if ((fechaCarga != null && org.apache.commons.lang3.time.DateUtils.isSameDay(now, fechaCarga))
                        || (fechaGestion != null && org.apache.commons.lang3.time.DateUtils.isSameDay(now, fechaGestion))) {

                    boolean validated = false;
                    try {
                        String usuario = obj.getUsu_ges() != null ? obj.getUsu_ges() : "";
                        if (!usuario.equalsIgnoreCase("auto")) validated = true;
                    } catch (Exception ex) {
                    }

                    if (validated) devcomListNew.add(obj);

                }

            }
            return devcomListNew;
        } catch (Exception ex) {
            return devcomList;
        }
    }

    private List<DevTecnicaResponseData> filterDuplicatesDevTec(List<DevTecnicaResponseData> devtecList) {
        try {
            List<DevTecnicaResponseData> devcomListNew = new ArrayList<>();
            List<String> ids = new ArrayList<>();

            for (int i = 0; i < devtecList.size(); i++) {
                boolean toAdd = true;
                for (int j = 0; j < devcomListNew.size(); j++) {

                    if (devtecList.get(i).getNumero_peticion().equals(devcomListNew.get(j).getNumero_peticion())) {

                        toAdd = false;

                        try {
                            Date oldDate = null;
                            Date newDate = null;
                            if (!devtecList.get(i).getFecha_liquidacion().equals("") && !devcomListNew.get(j).getFecha_liquidacion().equals("")) {
                                //Por gestion
                                oldDate = DateUtils.parse(devtecList.get(i).getFecha_liquidacion(), "yyyy-MM-dd HH:mm:ss");
                                newDate = DateUtils.parse(devcomListNew.get(j).getFecha_liquidacion(), "yyyy-MM-dd HH:mm:ss");
                            } else {
                                //Por carga
                                oldDate = DateUtils.parse(devtecList.get(i).getFecha_ingreso(), "yyyy-MM-dd HH:mm:ss");
                                newDate = DateUtils.parse(devcomListNew.get(j).getFecha_ingreso(), "yyyy-MM-dd HH:mm:ss");
                            }

                            if (oldDate.compareTo(newDate) == 1) {
                                devcomListNew.set(j, devtecList.get(i));
                            }
                        } catch (Exception fex) {
                            logger.error(fex.getMessage());
                        }

                    }

                }

                if (toAdd) devcomListNew.add(devtecList.get(i));

            }
            return devcomListNew;
        } catch (Exception ex) {
            return devtecList;
        }
    }

    private List<DevTecnicaResponseData> filterRequiredDevTec(List<DevTecnicaResponseData> devtecList) {
        try {
            List<DevTecnicaResponseData> devtecListNew = new ArrayList<>();

            /*Calendar calNow = Calendar.getInstance();
            calNow.setTime(new Date());*/

            Date now = DateUtils.parse("2018-07-22 10:00:00", "yyyy-MM-dd HH:mm:ss");

            for (DevTecnicaResponseData obj : devtecList) {

                Date fechaIngreso = null;
                Date fechaLiquidacion = null;
                try {
                    fechaIngreso = DateUtils.parse(obj.getFecha_ingreso(), "yyyy-MM-dd HH:mm:ss");
                } catch (ParseException e) {
                }
                try {
                    fechaLiquidacion = DateUtils.parse(obj.getFecha_liquidacion(), "yyyy-MM-dd HH:mm:ss");
                } catch (ParseException e) {
                }


                if ((fechaIngreso != null && org.apache.commons.lang3.time.DateUtils.isSameDay(now, fechaIngreso))
                        || (fechaLiquidacion != null && org.apache.commons.lang3.time.DateUtils.isSameDay(now, fechaLiquidacion))) {

                    boolean validated = false;
                    try {
                        String codigo_motivo = obj.getCodigo_motivo() != null ? obj.getCodigo_motivo() : "-1";
                        Integer codigo_motivo_int = Integer.parseInt(codigo_motivo);
                        if (codigo_motivo_int > 0 && codigo_motivo_int < 15) validated = true;
                    } catch (Exception ex) {
                    }

                    if (validated) devtecListNew.add(obj);

                }

            }
            return devtecListNew;
        } catch (Exception ex) {
            return devtecList;
        }
    }

    private int processInsertCount(int currentCount, int[] insertResults) {
        int count = currentCount;
        for (int insertResult : insertResults) {
            switch (insertResult) {
                case Statement.SUCCESS_NO_INFO:
                    count += 1;
                    break;
                case Statement.EXECUTE_FAILED:
                    break;
                default:
                    count += insertResult;
                    break;
            }
        }
        return count;
    }

    /**
     * process update result from updateBatch Response
     *
     * @param currentCount
     * @param updateResults
     * @return new count
     */
    private int processUpdateCount(int currentCount, int[] updateResults) {
        int count = currentCount;
        for (int updateResult : updateResults) {
            switch (updateResult) {
                case Statement.SUCCESS_NO_INFO:
                case Statement.EXECUTE_FAILED:
                    break;
                default:
                    count += updateResult;
                    break;
            }
        }
        return count;
    }

    public String syncHDEC(HdecResponseData hdec) {

        String flagQuery = "OK";

        try (Connection con = Database.datasource().getConnection()) {
            String query = "SELECT * FROM tdp_visor WHERE ID_VISOR = " + hdec.getId_visor();
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                ResultSet rs = stmt.executeQuery();
                if (!rs.next()) {
                    //insert
                    String insert = " insert into tdp_visor (ID_VISOR,BACK,FECHA_GRABACION,ID_GRABACION,CLIENTE,TELEFONO,"
                            + "DIRECCION,DEPARTAMENTO,DNI,NOMBRE_PRODUCTO,ESTADO_SOLICITUD,MOTIVO_ESTADO,SPEECH,"
                            + "ACCION,OPERACION_COMERCIAL,DISTRITO,CODIGO_LLAMADA,SUB_PRODUCTO,TIPO_PRODUCTO,CODIGO_GESTION,"
                            + "CODIGO_SUB_GESTION,FLAG_DEPENDENCIA,VERSION_FILA,TIPO_DOCUMENTO,ACCION_APLICATIVOS_BACK,PROVINCIA,FECHA_DE_LLAMADA,"
                            + "CODIGO_PEDIDO,VERSION_FILA_T,SUB_PRODUCTO_EQUIV,PRODUCTO)"
                            + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    try (PreparedStatement ps = con.prepareStatement(insert)) {
                        ps.setString(1, hdec.getId_visor());
                        ps.setString(2, hdec.getBack());
                        ps.setDate(3, null);
                        ps.setString(4, hdec.getId_grabacion());
                        ps.setString(5, hdec.getCliente());
                        ps.setString(6, hdec.getTelefono());
                        ps.setString(7, hdec.getDireccion());
                        ps.setString(8, hdec.getDepartamento());
                        ps.setString(9, hdec.getDni());
                        ps.setString(10, hdec.getNombre_producto());
                        ps.setString(11, hdec.getEstado_solicitud());
                        ps.setString(12, hdec.getMotivo_estado());
                        ps.setString(13, hdec.getSpeech());
                        ps.setString(14, hdec.getAccion());
                        ps.setString(15, hdec.getOperacion_comercial());
                        ps.setString(16, hdec.getDistrito());
                        ps.setString(17, hdec.getCodigo_llamada());
                        ps.setString(18, hdec.getSub_producto());
                        ps.setString(19, hdec.getTipo_producto());
                        ps.setString(20, hdec.getCodigo_getion());
                        ps.setString(21, hdec.getCodigo_sub_gestion());
                        ps.setString(22, hdec.getFlag_dependencia());
                        ps.setString(23, hdec.getVersion_fila());
                        ps.setString(24, hdec.getTipo_documento());
                        ps.setString(25, hdec.getAccion_aplicativo_back());
                        ps.setString(26, hdec.getProvincia());
                        ps.setDate(27, null);
                        ps.setString(28, hdec.getCodigo_pedido());
                        ps.setString(29, hdec.getVersion_fila_t());
                        ps.setString(30, hdec.getSub_producto_equiv());
                        ps.setString(31, hdec.getProducto());
                        ps.executeUpdate();
                        flagQuery = "INSERT";
                    }

                } else {
                    //update
                    logger.info("ID_VISOR: " + rs.getString("ID_VISOR") + "|VERSION: " + rs.getString("VERSION_FILA_T"));

                    long versionFilaT = Long.valueOf(rs.getString("VERSION_FILA_T").trim());
                    long versionFilaTUpdate = Long.valueOf(hdec.getVersion_fila_t().trim());
                    if (versionFilaTUpdate > versionFilaT) {
                        String update = " update tdp_visor set BACK = ?,FECHA_GRABACION = ?,ID_GRABACION = ?,CLIENTE = ?,TELEFONO = ?,"
                                + " DIRECCION = ?,DEPARTAMENTO = ?,DNI = ?,NOMBRE_PRODUCTO = ?,ESTADO_SOLICITUD = ?,MOTIVO_ESTADO = ?,SPEECH = ?,"
                                + " ACCION = ?,OPERACION_COMERCIAL = ?,DISTRITO = ?,CODIGO_LLAMADA = ?,SUB_PRODUCTO = ?,TIPO_PRODUCTO = ?,CODIGO_GESTION = ?,"
                                + " CODIGO_SUB_GESTION = ?,FLAG_DEPENDENCIA = ?,VERSION_FILA = ?,TIPO_DOCUMENTO = ?,ACCION_APLICATIVOS_BACK = ?,PROVINCIA = ?,FECHA_DE_LLAMADA = ?,"
                                + " CODIGO_PEDIDO = ?,VERSION_FILA_T = ?,SUB_PRODUCTO_EQUIV = ?,PRODUCTO = ? Where ID_VISOR = ?";
                        try (PreparedStatement ps = con.prepareStatement(update)) {
                            ps.setString(1, hdec.getBack());
                            ps.setDate(2, null);
                            ps.setString(3, hdec.getId_grabacion());
                            ps.setString(4, hdec.getCliente());
                            ps.setString(5, hdec.getTelefono());
                            ps.setString(6, hdec.getDireccion());
                            ps.setString(7, hdec.getDepartamento());
                            ps.setString(8, hdec.getDni());
                            ps.setString(9, hdec.getNombre_producto());
                            ps.setString(10, hdec.getEstado_solicitud());
                            ps.setString(11, hdec.getMotivo_estado());
                            ps.setString(12, hdec.getSpeech());
                            ps.setString(13, hdec.getAccion());
                            ps.setString(14, hdec.getOperacion_comercial());
                            ps.setString(15, hdec.getDistrito());
                            ps.setString(16, hdec.getCodigo_llamada());
                            ps.setString(17, hdec.getSub_producto());
                            ps.setString(18, hdec.getTipo_producto());
                            ps.setString(19, hdec.getCodigo_getion());
                            ps.setString(20, hdec.getCodigo_sub_gestion());
                            ps.setString(21, hdec.getFlag_dependencia());
                            ps.setString(22, hdec.getVersion_fila());
                            ps.setString(23, hdec.getTipo_documento());
                            ps.setString(24, hdec.getAccion_aplicativo_back());
                            ps.setString(25, hdec.getProvincia());
                            ps.setDate(26, null);
                            ps.setString(27, hdec.getCodigo_pedido());
                            ps.setString(28, hdec.getVersion_fila_t());
                            ps.setString(29, hdec.getSub_producto_equiv());
                            ps.setString(30, hdec.getProducto());
                            ps.setString(31, hdec.getId_visor());
                            ps.executeUpdate();
                            flagQuery = "UPDATE";
                        }

                    }

                }
            }


        } catch (SQLException e) {
            logger.error("SQLException: " + e);
        } catch (Exception e) {
            logger.error("Exception: " + e);
        }
        return flagQuery;
    }

    public AzureResponseData getMetadata(AzureRequest request) throws Exception {

        AzureResponseData metadata = null;

        try (Connection con = Database.datasource().getConnection()) {
            String query = MY_SQL_SP.replace(Constants.MY_SQL_SP_NAME, "retrieveCustodyMetadata")
                    .replace(Constants.MY_SQL_PARAMETER, "?");
            try (PreparedStatement stmt = con.prepareStatement(query)) {
                stmt.setString(1, request.getKey());

                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    metadata = new AzureResponseData();
                    metadata.setBusiness(rs.getString("NEGOCIO"));
                    metadata.setCampaignName(rs.getString("NOMBRE_CAMPANIA"));
                    metadata.setChannel(rs.getString("CANAL_VENTA"));
                    metadata.setCustomerName(rs.getString("NOMBRE_CLIENTE"));
                    metadata.setDataProtection(rs.getString("PROTECCION_DE_DATOS"));
                    metadata.setDisableWhitePage(rs.getString("DESAFILIACION_DE_PAGINAS_BLANCAS"));
                    metadata.setDocumentDate(rs.getTimestamp("FECHA_DOCUMENTO"));
                    metadata.setDocumentNumber(rs.getString("NUMERO_DOCUMENTO"));
                    metadata.setEnableDigitalInvoice(rs.getString("AFILIACION_FACTURA_DIGITAL"));
                    metadata.setDni(rs.getString("DNI"));
                    metadata.setId(rs.getString("IDENTIFICADOR_UNICO"));
                    metadata.setPhoneNumber(rs.getString("NUMERO_CELULAR"));
                    metadata.setProviderDetail(rs.getString("DETALLE_PROVEEDOR"));
                    metadata.setProviderName(rs.getString("NOMBRE_PROVEEDOR"));
                    metadata.setTelephoneNumber(rs.getString("NUMERO_LINEA"));
                }
            }

        } catch (Exception e) {
            logger.error("Error getMetadata DAO.", e);
            throw e;
        }

        return metadata;
    }

    public void actualizarPagoEfectivoData(Integer id, String status, String CIP) {
        LogSystem.info(logger, id + ":: ActualizarPagoEfectivoData", "Inicio");
        try (Connection con = Database.datasource().getConnection()) {
            PreparedStatement stm = con.prepareStatement("update ibmx_a07e6d02edaf552.peticion_pago_efectivo set status = ?, codpago = ? where id = ?");
            stm.setString(1, status);
            stm.setString(2, CIP);
            stm.setInt(3, id);

            LogSystem.infoQuery(logger, id + ":: ActualizarPagoEfectivoData", "stm", stm.toString());
            stm.executeUpdate();
        } catch (SQLException e) {
            LogSystem.error(logger, id + ":: ActualizarPagoEfectivoData", "SQLException", e);
        }
        LogSystem.info(logger, id + ":: ActualizarPagoEfectivoData", "Fin");
    }

    public boolean actualizarPagoEfectivoStatus(String status, String CIP, String fechaPagoCIP) {
        Date date = new Date();
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaActual = formatter.format(date);

        try (Connection con = Database.datasource().getConnection()) {
            PreparedStatement stm = con.prepareStatement("update ibmx_a07e6d02edaf552.peticion_pago_efectivo set status = ?, fechastatus = ?, fechapago = ? where codpago = ?");
            stm.setString(1, status);
            stm.setString(2, fechaActual);
            stm.setString(3, fechaPagoCIP);
            stm.setString(4, CIP);

            int n = stm.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error("Error actualizarPagoEfectivoStatus DAO.", e);
            return false;
        }
    }

    public boolean actualizarPagoEfectivoProceso(String CIP,Date fecha) {
        Date date = new Date();
        Timestamp date2 = new Timestamp(fecha.getTime());
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String fechaActual = formatter.format(date);

        try (Connection con = Database.datasource().getConnection()) {
            PreparedStatement stm = con.prepareStatement("update ibmx_a07e6d02edaf552.peticion_pago_efectivo set fechaultconsulta = ? where codpago = ?");
            stm.setTimestamp(1, date2);
            stm.setString(2, CIP);

            int n = stm.executeUpdate();
            return true;
        } catch (SQLException e) {
            logger.error("Error actualizarPagoEfectivoStatus DAO.", e);
            return false;
        }
    }

    public boolean updateEstadoMotivo(String estado, String motivo, String cip) {
        try (Connection con = Database.datasource().getConnection()) {
            String update = "UPDATE ibmx_a07e6d02edaf552.tdp_visor " +
                    "SET estado_solicitud=?, motivo_estado=? " +
                    "WHERE id_visor in (SELECT distinct pp.order_id " +
                    "FROM ibmx_a07e6d02edaf552.peticion_pago_efectivo pp " +
                    "WHERE pp.codpago=?)";
            try (PreparedStatement ps = con.prepareStatement(update)) {
                ps.setString(1, estado);
                ps.setString(2, motivo);
                ps.setString(3, cip);
                int n = ps.executeUpdate();
                if (n > 0) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Loads the data to be send to pago efectivo
     *
     * @param id to load from db
     * @return BEGenRequest
     */
    public BEGenRequest loadPagoEfectivoData(Integer id) {
        LogSystem.info(logger, id + ":: LoadPagoEfectivoData", "Inicio");
        BEGenRequest data = null;
        try (Connection con = Database.datasource().getConnection()) {
            PreparedStatement query = con.prepareStatement("select 'status', order_id, idmoneda, total, metodospago, codservicio,"
                    + "codtransaccion, emailcomercio, fechaaexpirar, usuarioid, dataadicional, usuarionombre, usuarioapellidos,"
                    + "usuariolocalidad,usuarioprovincia,usuariopais,usuarioalias,usuariotipodoc,usuarionumerodoc,usuarioemail,"
                    + "conceptopago,detalles,paramsurl,paramsemail from ibmx_a07e6d02edaf552.peticion_pago_efectivo where id = ?");

            query.setInt(1, id);

            LogSystem.infoQuery(logger, id + ":: LoadPagoEfectivoData", "query", query.toString());
            ResultSet rs = query.executeQuery();
            while (rs.next()) {
                String status = rs.getString(1);
                String orderId = rs.getString(2);
                String idmoneda = rs.getString(3);
                String total = rs.getString(4);
                String metodospago = rs.getString(5);
                String codservicio = rs.getString(6);
                String codtransaccion = rs.getString(7);
                String emailcomercio = rs.getString(8);
                String fechaaexpirar = rs.getString(9);
                String usuarioid = rs.getString(10);
                String dataadicional = rs.getString(11);
                String usuarionombre = rs.getString(12);
                String usuarioapellidos = rs.getString(13);
                String usuariolocalidad = rs.getString(14);
                String usuarioprovincia = rs.getString(15);
                String usuariopais = rs.getString(16);
                String usuarioalias = rs.getString(17);
                String usuariotipodoc = rs.getString(18);
                String usuarionumerodoc = rs.getString(19);
                String usuarioemail = rs.getString(20);
                String conceptopago = rs.getString(21);
                String detalles = rs.getString(22);
                String paramsurl = rs.getString(23);
                String paramsemail = rs.getString(24);

                data = new BEGenRequest.Builder().setCod_servicio(codservicio)
                        .setConcepto_pago(conceptopago)
                        .setData_adicional(dataadicional)
                        .setEmail_comercio(emailcomercio)
                        .setFecha_expirar(fechaaexpirar)
                        .setMedio_pago(metodospago)
                        .setMoneda(idmoneda)
                        .setMonto(total)
                        .setNumero_orden(codtransaccion)
//						.setNumero_orden(Math.random()+"")
                        .setUsuario_alias(usuarioalias)
                        .setUsuario_apellidos(usuarioapellidos)
                        .setUsuario_localidad(usuariolocalidad)
                        .setUsuario_provincia(usuarioprovincia)
                        .setUsuario_email(usuarioemail)
                        .setUsuario_id(usuarioid)
                        .setUsuario_nombre(usuarionombre)
                        .setUsuario_numerodocumento(usuarionumerodoc)
                        .setUsuario_pais(usuariopais)
                        .setUsuario_tipodocumento(usuariotipodoc)
                        .build();
            }
        } catch (SQLException e) {
            LogSystem.error(logger, id + ":: LoadPagoEfectivoData", "SQLException", e);
        }

        LogSystem.info(logger, id + ":: LoadPagoEfectivoData", "Inicio");
        return data;
    }

    // INSERTS IN TABLE application_sync_log CALCULATOR-OFFERS BATCH
    public void syncCalc_Offers_Error(List<CalculatorResponseData> errorList) throws Exception {

        logger.info("Registrando errores encontrados.");
        PreparedStatement psInsert;
        int countError = 0;
        final int batchSize = 3000;

        String insertQuery = "insert into application_sync_log (service, level, filename, error, msg, timestamp) "
                + "values ('CALCULADORA', 'LINE', ?, ?, ?, now())";

        try (Connection con = Database.datasource().getConnection()) {
            psInsert = con.prepareStatement(insertQuery);

            try {
                for (CalculatorResponseData errorCalc : errorList) {
                    countError++;
                    psInsert.setString(1, errorCalc.getFileName());
                    psInsert.setString(2, errorCalc.getMsgError());
                    psInsert.setString(3, errorCalc.getLineString());
                    psInsert.addBatch();
                    if (countError % batchSize == 0) {
                        psInsert.executeBatch();
                    }
                }
                psInsert.executeBatch();

            } catch (BatchUpdateException bue) {
                logger.error("executeBatch threw BatchUpdateException: " + bue.getMessage());
            }

        } catch (Exception e) {
            logger.error("Error syncCalc_Offers_Error: " + e.getMessage());
            e.printStackTrace();
        }

    }

    // INSERTS IN TABLE application_sync_log by Calculator FILE
    public void syncCalc_Offers_Error(String msg, String fileName) throws Exception {

        logger.info("Registrando errores encontrados.");
        PreparedStatement psInsert;

        String insertQuery = "insert into application_sync_log (service, level, filename, error, msg, timestamp) "
                + "values ('CALCULADORA', 'FILE', ?, ?, ?, now())";

        try (Connection con = Database.datasource().getConnection()) {
            psInsert = con.prepareStatement(insertQuery);

            psInsert.setString(1, fileName);
            psInsert.setString(2, "ERROR");
            psInsert.setString(3, msg);

            psInsert.executeUpdate();
        } catch (Exception e) {
            logger.error("Error syncCalc_Offers_Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void deleteCalculator(List<CalculatorResponseData> calculatorList) throws Exception {

        PreparedStatement psDelete;

        int countDelete = 0;
        final int batchSize = 3000;
        // tdp_calculator x tdp_calculator_old
        String deleteQuery = "delete from tdp_calculator where telefono = ? or servicio = ?";

        try (Connection con = Database.datasource().getConnection()) {

            if (calculatorList.size() > 0) {

                int[] rowsDeleted = null;
                psDelete = con.prepareStatement(deleteQuery);
                try {
                    //psDelete.execute("SET FOREIGN_KEY_CHECKS=0");
                    //psDelete.execute("SET UNIQUE_CHECKS=0");

                    for (CalculatorResponseData calculator : calculatorList) {

                        countDelete++;
                        psDelete.setString(1, calculator.getTelefono());
                        psDelete.setString(2, calculator.getServicio());

                        psDelete.addBatch();
                        if (countDelete % batchSize == 0) {
                            psDelete.executeBatch();
                        }
                    }

                    rowsDeleted = psDelete.executeBatch();

                    //psDelete.execute("SET FOREIGN_KEY_CHECKS=1");
                    //psDelete.execute("SET UNIQUE_CHECKS=1");
                } catch (BatchUpdateException bue) {
                    logger.error("executeBatch threw BatchUpdateException: " + bue.getMessage());
                }
//				logger.info("Successfully deleted rows of calculator");
            }

        } catch (Exception e) {
            logger.error("Error deleteCalculator: " + e.getMessage());

        }
    }

    public boolean syncCalculator(List<CalculatorResponseData> calculatorList) throws Exception {

        PreparedStatement psInsert = null;

        int countInsert = 0;
        final int batchSize = 3000;
        // tdp_calculator x tdp_calculator_old
        String insertQuery = "insert into tdp_calculator(id, telefono, servicio, paquetepsorigen, paqueteorigen, cantidaddecos, rentatotal, internet, registeredTime, lastUpdateTime) "
                + "values (?, ?, ?, ?, ?, ?, ?, ?, now(), now())";

        try (Connection con = Database.datasource().getConnection()) {

            if (calculatorList.size() > 0) {
//				logger.info("calculator ready to Add...");
                psInsert = con.prepareStatement(insertQuery);

                int[] rowsInserted = null;

                try {
                    //psInsert.execute("SET FOREIGN_KEY_CHECKS=0");
                    //psInsert.execute("SET UNIQUE_CHECKS=0");

                    for (CalculatorResponseData model : calculatorList) {

                        psInsert.setString(1, model.getCalculatorID());
                        if (model.getTelefono().trim().equals("0") || model.getTelefono().trim().equals("")) {
                            psInsert.setString(2, null);
                        } else {
                            psInsert.setString(2, model.getTelefono());
                        }

                        if (model.getServicio().trim().equals("0") || model.getServicio().trim().equals("")) {
                            psInsert.setString(3, null);
                        } else {
                            psInsert.setString(3, model.getServicio());
                        }

                        psInsert.setString(4, model.getPaquetepsorigen());
                        psInsert.setString(5, model.getPaqueteorigen());
                        psInsert.setString(6, model.getCantidaddecos());
                        psInsert.setString(7, model.getRentatotal());
                        psInsert.setString(8, model.getInternet());

                        psInsert.addBatch();
                        if (countInsert % batchSize == 0) {
                            rowsInserted = psInsert.executeBatch();

                        }
                        countInsert++;
                    }
                    rowsInserted = psInsert.executeBatch();

                    //psInsert.execute("SET FOREIGN_KEY_CHECKS=1");
                    //psInsert.execute("SET UNIQUE_CHECKS=1");
                } catch (BatchUpdateException bue) {
                    System.out.println("executeBatch threw BatchUpdateException: " + bue.getMessage());
                }
            }

        } catch (Exception e) {
            logger.error("Error syncCalculator: " + e.getMessage());
            e.printStackTrace();
        }
        return true;
    }

    public boolean syncCalc_Offers(List<CalculatorResponseData> listCalculatorOffers) throws Exception {

        PreparedStatement psInsert = null;
        int countInsert = 0;
        boolean finish = false;
        final int batchSize = 3000;
        // tdp_calculator_offers x tdp_calculator_offers_old

        String insertQuery = "insert into tdp_calculator_offers(calculatorId, prioridad, productodestinops, svas, productodestinonombre, montofinanciado, cuotasfinanciado, registeredTime, lastUpdateTime, codigooferta) "
                + "values (?, ?, ?, ?, ?, ?, ?, now(), now(), ?)";

        try (Connection con = Database.datasource().getConnection()) {

            if (listCalculatorOffers.size() > 0) {
//				logger.info("Offers ready to Add...");
                psInsert = con.prepareStatement(insertQuery);

                int[] rowsInserted = null;

                try {
                    //psInsert.execute("SET FOREIGN_KEY_CHECKS=0");
                    //psInsert.execute("SET UNIQUE_CHECKS=0");

                    for (CalculatorResponseData model : listCalculatorOffers) {

                        psInsert.setString(1, model.getCalculatorID());
                        psInsert.setString(2, model.getPrioridad());
                        psInsert.setString(3, model.getProductodestinops().trim().equals("") ? null : model.getProductodestinops());
                        psInsert.setString(4, model.getSvas().trim().equals("") ? null : model.getSvas());
                        psInsert.setString(5, model.getProductodestinonombre());
                        psInsert.setString(6, model.getMontofinanciado().equals("") ? null : model.getMontofinanciado());
                        psInsert.setString(7, model.getCuotasfinanciado().equals("") ? null : model.getCuotasfinanciado());
                        psInsert.setString(8, model.getCodigooferta().equals("") ? null : model.getCodigooferta());

                        psInsert.addBatch();
                        if (countInsert % batchSize == 0) {
                            rowsInserted = psInsert.executeBatch();
                        }
                        countInsert++;
                    }

                    rowsInserted = psInsert.executeBatch();

                    //psInsert.execute("SET FOREIGN_KEY_CHECKS=1");
                    //psInsert.execute("SET UNIQUE_CHECKS=1");
                } catch (BatchUpdateException bue) {
                    logger.error("executeBatch threw BatchUpdateException: " + bue.getMessage());
                }
//				logger.info("Successfully inserted offers");
                finish = true;
            }

        } catch (Exception e) {
            logger.error("Error syncCalc_Offers: " + e.getMessage());
            e.printStackTrace();
        }
        return finish;
    }

    public int getIDCalculator() throws Exception {
        int id = 0;
        PreparedStatement psSelect;
        // tdp_calculator x tdp_calculator_old
        String selectQuery = "SELECT MAX(ID) as id FROM tdp_calculator";

        try (Connection con = Database.datasource().getConnection()) {

            psSelect = con.prepareStatement(selectQuery);

            ResultSet rs = psSelect.executeQuery();
            while (rs.next()) {
                id = rs.getInt("id");
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        logger.info("Obteniendo ultimo ID: " + id);
        return id;
    }

    public void flagOrderForAzure(List<AzureRequest> requests) throws SyncException {
        logger.info("Inicio FlagOrderForAzure DAO");
        if (requests.isEmpty()) {
            logger.info("sin ventas");
            logger.info("Fin FlagOrderForAzure DAO");
            return;
        }
        Date now = new Date();
        String update = "update ibmx_a07e6d02edaf552.azure_request set order_status = ?, last_update = ?, status = 'PE' where order_id = ? and status = 'XX'";

        String selectExisting = "select order_id from ibmx_a07e6d02edaf552.azure_request where order_id in (%s)";

        String insertAzureRequest = "insert into ibmx_a07e6d02edaf552.azure_request (order_id, order_status, registration_date, last_update, status) values (?, ?, ?, ?, ?)";

        String whereOrderIdPlaceHolder = getPlaceHolder(requests.size());
        logger.info("FlagOrderForAzure => whereOrderIdPlaceHolder: " + whereOrderIdPlaceHolder);

        update = String.format(update, whereOrderIdPlaceHolder);
        logger.info("FlagOrderForAzure => update: " + update);

        selectExisting = String.format(selectExisting, whereOrderIdPlaceHolder);
        logger.info("FlagOrderForAzure => selectExisting: " + selectExisting);

        try (Connection con = Database.datasource().getConnection()) {

            PreparedStatement updateRequests = con.prepareStatement(update);
            //updateRequests.setTimestamp(1, new java.sql.Timestamp(now.getTime()));
            PreparedStatement selectExistingQuery = con.prepareStatement(selectExisting);
            int i = 2;

            for (AzureRequest request : requests) {
                updateRequests.setString(1, request.getStatus());
                updateRequests.setTimestamp(2, new java.sql.Timestamp(now.getTime()));
                updateRequests.setString(3, request.getKey());
                selectExistingQuery.setString(i - 1, request.getKey());
                updateRequests.addBatch();
                i++;
            }
            logger.info("FlagOrderForAzure => updateRequests: " + updateRequests.toString());
            logger.info("FlagOrderForAzure => selectExistingQuery: " + selectExistingQuery.toString());

            updateRequests.executeUpdate();
            ResultSet selectRS = selectExistingQuery.executeQuery();
            Map<String, String> existing = new HashMap<>();
            while (selectRS.next()) {
                existing.put(selectRS.getString(1), selectRS.getString(1));
            }

            PreparedStatement insertRequests = con.prepareStatement(insertAzureRequest);
            for (AzureRequest request : requests) {
                if (existing.get(request.getKey()) == null) {
                    insertRequests.setString(1, request.getKey());
                    insertRequests.setString(2, request.getStatus());
                    insertRequests.setTimestamp(3, new java.sql.Timestamp(now.getTime()));
                    insertRequests.setTimestamp(4, new java.sql.Timestamp(now.getTime()));
                    insertRequests.setString(5, "PE"); // pendiente
                    insertRequests.addBatch();
                }
            }
            logger.info("FlagOrderForAzure => insertRequests: " + insertRequests.toString());
            int[] rowsInserted = insertRequests.executeBatch();
            logger.info("FlagOrderForAzure => Ventas exito");

        } catch (Exception e) {
            logger.error("Error FlagOrderForAzure DAO. ", e);
            logger.info("Fin FlagOrderForAzure DAO");
            throw new SyncException(e);
        }
        logger.info("Fin FlagOrderForAzure DAO");
    }

    private String getPlaceHolder(int num) {
        StringBuilder place_holder = new StringBuilder();
        for (int i = 0; i < num; i++) {
            if (i == 0) {
                place_holder = new StringBuilder("?");
            } else {
                place_holder.append(",?");
            }
        }
        return place_holder.toString();
    }

    /**
     * Loads the data to be send to pago efectivo
     *
     * @return BEGenRequest
     */
    public List<PagoEfectivoConsultaRequest2> loadPagoEfectivoDataPendiente(int desdeDiasAtras, String fechaInicioBatch) {
        List<PagoEfectivoConsultaRequest2> data = new ArrayList<>();
        PagoEfectivoConsultaRequest2 obj = null;
        try (Connection con = Database.datasource().getConnection()) {

            Date date = DateUtils.removeDays(new Date(), desdeDiasAtras);
            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            String fechaActual = formatter.format(date);
            //fechaActual = "05/11/2018 17:23:43";
            String fechaActual2 = formatter2.format(date);

            PreparedStatement query = con.prepareStatement("select distinct pp.fechaaexpirar,pp.codpago, pp.order_id, pp.usuarionumerodoc ,o.statusaudio, o.statuslegacy, ch.groupChannel " +
                    "from ibmx_a07e6d02edaf552.peticion_pago_efectivo pp " +
                    "inner join ibmx_a07e6d02edaf552.order o  on o.id = pp.order_id " +
                    "left join ibmx_a07e6d02edaf552.tdp_sales_agent s on s.codATIS = pp.usuarioid " +
                    "left join ibmx_a07e6d02edaf552.tdp_channels ch on " +
                    "(ch.channelAtis = s.canal and (ch.groupChannel = 'TIENDA' or ch.groupChannel = s.nomPuntoVenta)) " +
                    "where  codpago is not null and codpago <> '' and pp.status not in ('PA','EX','EL')  and " +
                    "TO_TIMESTAMP('" + fechaActual + "','DD/MM/YYYY HH24:MI:SS')" + " <= TO_TIMESTAMP(pp.fechaaexpirar,'DD/MM/YYYY HH24:MI:SS') " +
                    "and o.registrationdate >= TO_TIMESTAMP('" + fechaInicioBatch + "','DD-MM-YYYY HH24:MI:SS') " +
                    "and (pp.fechaultconsulta is null or (extract(epoch from (cast('"+ fechaActual2 +"' as timestamp) - cast(pp.fechaultconsulta as timestamp)) / 3600)) >= " + horasCip + ") " +
                    "order by pp.fechaaexpirar asc " +
                    "limit " + limitCip);
            //"where  codpago = '00000014794178' ");

            ResultSet rs = query.executeQuery();

            while (rs.next()) {
                String groupChannel = rs.getString("groupChannel");
                String statusAudio = rs.getString("statusaudio");
                groupChannel = groupChannel == null ? null : groupChannel.trim();

                // Si el perfil de la venta es distinto a tienda y no se ha subido audio no se agrega
                if (groupChannel != null && !groupChannel.equalsIgnoreCase("TIENDA") && !statusAudio.equalsIgnoreCase("2"))
                    continue;

                obj = new PagoEfectivoConsultaRequest2();
                obj.setNumeroCIP(rs.getString("codpago"));
                obj.setOrderId(rs.getString("order_id"));
                obj.setDocNumber(rs.getString("usuarionumerodoc"));
                obj.setStatusLegacy(rs.getString("statuslegacy"));
                data.add(obj);
            }
        } catch (SQLException e) {
            logger.error("Error query.", e);
        }

        return data;
    }


}