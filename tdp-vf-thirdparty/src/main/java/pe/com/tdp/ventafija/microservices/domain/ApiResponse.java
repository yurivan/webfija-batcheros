package pe.com.tdp.ventafija.microservices.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"HeaderOut","BodyOut"})
public class ApiResponse<E> {
  @JsonProperty("HeaderOut")
  private ApiResponseHeader HeaderOut;
  @JsonProperty("BodyOut")
  private E BodyOut;

  public ApiResponseHeader getHeaderOut() {
    return HeaderOut;
  }

  public void setHeaderOut(ApiResponseHeader headerOut) {
    HeaderOut = headerOut;
  }

  public E getBodyOut() {
    return BodyOut;
  }

  public void setBodyOut(E bodyOut) {
    BodyOut = bodyOut;
  }
}
