package pe.com.tdp.ventafija.microservices.domain.thirdparty.dto;

public class SynOutSyntaxError {
	public static final String CAUSE_INCOMPLETE_LINE = "INCOMPLETE_LINE";
	public static final String CAUSE_DNI_ERROR = "DNI_ERROR";
	public static final String CAUSE_TELEFONO_ERROR = "TELEFONO_ERROR";
	public static final String CAUSE_DISTRITO_ERROR = "DISTRITO_ERROR";
	
	private String cause;
	private String line;

	public SynOutSyntaxError(String cause, String line) {
		super();
		this.cause = cause;
		this.line = line;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

}
