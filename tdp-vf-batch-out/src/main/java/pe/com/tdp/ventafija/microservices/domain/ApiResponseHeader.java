package pe.com.tdp.ventafija.microservices.domain;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"originator","destination","execId","timestamp","msgType"})
public class ApiResponseHeader {

  private String originator;
  private String destination;
  private String execId;
  private String timestamp;
  private String msgType;

  public String getOriginator() {
    return originator;
  }

  public void setOriginator(String originator) {
    this.originator = originator;
  }

  public String getDestination() {
    return destination;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public String getExecId() {
    return execId;
  }

  public void setExecId(String execId) {
    this.execId = execId;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getMsgType() {
    return msgType;
  }

  public void setMsgType(String msgType) {
    this.msgType = msgType;
  }

}
