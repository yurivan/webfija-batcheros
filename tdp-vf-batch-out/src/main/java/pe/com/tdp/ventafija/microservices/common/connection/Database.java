/*package pe.com.tdp.ventafija.microservices.common.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pe.com.tdp.ventafija.microservices.common.util.Constants;

public class Database {
  private static final Logger logger = LogManager.getLogger();
  public static Connection getConnection() throws Exception {
    Connection connection = null;
    try {
      String url = "jdbc:mysql://us-cdbr-iron-east-04.cleardb.net:3306/"+Constants.MY_SQL_SCHEMA;
      Class.forName("com.mysql.jdbc.Driver");
      connection = DriverManager.getConnection(url, Constants.MY_SQL_USER, Constants.MY_SQL_PASSWORD);

    } catch (SQLException ex) {
      logger.info("An error occurred. Maybe user/password is invalid");
      ex.printStackTrace();
    }
    return connection;
  }

  public static void main(String[] args) {
    try {
      Connection con = Database.getConnection();

      Statement stmt = con.createStatement();
      String query = "SELECT * FROM ad_87a82782372a998.vendors;";
      ResultSet rs = stmt.executeQuery(query);

      while (rs.next()) {

        String firstName = rs.getString("nombre");
        String lastName = rs.getString("apePaterno");

        logger.info(firstName + "" + lastName);
      }
      logger.info("Conexion Exitosa");
      stmt.close();
      con.close();
    } catch (Exception e) {

      e.printStackTrace();
    }
  }
}
*/


package pe.com.tdp.ventafija.microservices.common.connection;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
public class Database {
  private static final Logger logger = LogManager.getLogger();
  private static HikariDataSource datasource;
  static Properties prop = new Properties();
  static Connection con;

    static boolean production = false;
  
  @Bean
  public static DataSource datasource() {
    
    if (datasource == null) {
     /* try {
    	  prop.load(new FileInputStream(new File("/ms/config/hikari.properties")));
      } catch (IOException e) {
        logger.info("Error DataSource"+e);
        HikariConfig config = new HikariConfig(prop);
      } */

            /*
      HikariConfig config = new HikariConfig();
		config.setDriverClassName("com.mysql.jdbc.Driver");
		config.setUsername("bcca6f7dd48c10");
		config.setPassword("5efd096d");
		config.setJdbcUrl("jdbc:mysql://us-cdbr-iron-east-04.cleardb.net:3306/ad_e79736d95caa4c3?useSSL=false");
		return new HikariDataSource(config);
       */
      /*HikariConfig config = new HikariConfig();
      config.setDriverClassName("com.mysql.jdbc.Driver");
      config.setUsername("root");
      config.setPassword("$Vass$Vass");
      config.setJdbcUrl("jdbc:mysql://181.65.165.130:8573/ad_10__dev?useSSL=false");*/
      HikariConfig config = new HikariConfig();

      config.setDriverClassName(System.getenv("TDP_FIJA_DB_DRIVER"));
            config.setJdbcUrl(System.getenv("TDP_FIJA_DB_URL"));
      config.setUsername(System.getenv("TDP_FIJA_DB_USR"));
      config.setPassword(System.getenv("TDP_FIJA_DB_PW"));

        if (production) {
            config.setJdbcUrl("jdbc:postgresql://sl-us-south-1-portal.13.dblayer.com:28973/compose?useSSL=false");
            config.setPassword("IXECEUBFNJLAZZEV");
        }

            config.addDataSourceProperty("ApplicationName", "Out-Batch");
            config.setMinimumIdle(Integer.parseInt(System.getenv("TDP_FIJA_DB_MINIMUM_IDLE")));
            config.setMaximumPoolSize(Integer.parseInt(System.getenv("TDP_FIJA_DB_POOLING")));
            config.setIdleTimeout(Integer.parseInt(System.getenv("TDP_FIJA_DB_TIMEOUT_IDLE")));

      datasource = new HikariDataSource(config);
    }
    return datasource;
  }

}
