package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class CatalogPlantaOutResponseData {
    private String id;
    private String cod_accion;
    private String campania;
    private String renta_origen;
    private String tx_nombre;
    private String numero_telefono;
    private String celular;
    private String tipo_documento;
    private String numero_documento;
    private String departamento;
    private String provincia;
    private String distrito;
    private String tx_direccion;

    private String ps_of1;
    private String ps_svas_of1;
    private String salto_of1;
    private String renta_destino_of1;
    private String renta_promocional_of1;
    private String renta_prom_periodo_of1;
    private String velocidad_promocional_of1;
    private String velocidad_prom_periodo_of1;
    private String ps_of2;
    private String ps_svas_of2;
    private String salto_of2;
    private String renta_destino_of2;
    private String renta_promocional_of2;
    private String renta_prom_periodo_of2;
    private String velocidad_promocional_of2;
    private String velocidad_prom_periodo_of2;

    private String tipo_archivo;
    private String fecha;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCod_accion() {
        return cod_accion;
    }

    public void setCod_accion(String cod_accion) {
        this.cod_accion = cod_accion;
    }

    public String getCampania() {
        return campania;
    }

    public void setCampania(String campania) {
        this.campania = campania;
    }

    public String getRenta_origen() {
        return renta_origen;
    }

    public void setRenta_origen(String renta_origen) {
        this.renta_origen = renta_origen;
    }

    public String getTx_nombre() {
        return tx_nombre;
    }

    public void setTx_nombre(String tx_nombre) {
        this.tx_nombre = tx_nombre;
    }

    public String getNumero_telefono() {
        return numero_telefono;
    }

    public void setNumero_telefono(String numero_telefono) {
        this.numero_telefono = numero_telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getNumero_documento() {
        return numero_documento;
    }

    public void setNumero_documento(String numero_documento) {
        this.numero_documento = numero_documento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getTx_direccion() {
        return tx_direccion;
    }

    public void setTx_direccion(String tx_direccion) {
        this.tx_direccion = tx_direccion;
    }

    public String getPs_of1() {
        return ps_of1;
    }

    public void setPs_of1(String ps_of1) {
        this.ps_of1 = ps_of1;
    }

    public String getPs_svas_of1() {
        return ps_svas_of1;
    }

    public void setPs_svas_of1(String ps_svas_of1) {
        this.ps_svas_of1 = ps_svas_of1;
    }

    public String getSalto_of1() {
        return salto_of1;
    }

    public void setSalto_of1(String salto_of1) {
        this.salto_of1 = salto_of1;
    }

    public String getRenta_destino_of1() {
        return renta_destino_of1;
    }

    public void setRenta_destino_of1(String renta_destino_of1) {
        this.renta_destino_of1 = renta_destino_of1;
    }

    public String getRenta_promocional_of1() {
        return renta_promocional_of1;
    }

    public void setRenta_promocional_of1(String renta_promocional_of1) {
        this.renta_promocional_of1 = renta_promocional_of1;
    }

    public String getRenta_prom_periodo_of1() {
        return renta_prom_periodo_of1;
    }

    public void setRenta_prom_periodo_of1(String renta_prom_periodo_of1) {
        this.renta_prom_periodo_of1 = renta_prom_periodo_of1;
    }

    public String getVelocidad_promocional_of1() {
        return velocidad_promocional_of1;
    }

    public void setVelocidad_promocional_of1(String velocidad_promocional_of1) {
        this.velocidad_promocional_of1 = velocidad_promocional_of1;
    }

    public String getVelocidad_prom_periodo_of1() {
        return velocidad_prom_periodo_of1;
    }

    public void setVelocidad_prom_periodo_of1(String velocidad_prom_periodo_of1) {
        this.velocidad_prom_periodo_of1 = velocidad_prom_periodo_of1;
    }

    public String getPs_of2() {
        return ps_of2;
    }

    public void setPs_of2(String ps_of2) {
        this.ps_of2 = ps_of2;
    }

    public String getPs_svas_of2() {
        return ps_svas_of2;
    }

    public void setPs_svas_of2(String ps_svas_of2) {
        this.ps_svas_of2 = ps_svas_of2;
    }

    public String getSalto_of2() {
        return salto_of2;
    }

    public void setSalto_of2(String salto_of2) {
        this.salto_of2 = salto_of2;
    }

    public String getRenta_destino_of2() {
        return renta_destino_of2;
    }

    public void setRenta_destino_of2(String renta_destino_of2) {
        this.renta_destino_of2 = renta_destino_of2;
    }

    public String getRenta_promocional_of2() {
        return renta_promocional_of2;
    }

    public void setRenta_promocional_of2(String renta_promocional_of2) {
        this.renta_promocional_of2 = renta_promocional_of2;
    }

    public String getRenta_prom_periodo_of2() {
        return renta_prom_periodo_of2;
    }

    public void setRenta_prom_periodo_of2(String renta_prom_periodo_of2) {
        this.renta_prom_periodo_of2 = renta_prom_periodo_of2;
    }

    public String getVelocidad_promocional_of2() {
        return velocidad_promocional_of2;
    }

    public void setVelocidad_promocional_of2(String velocidad_promocional_of2) {
        this.velocidad_promocional_of2 = velocidad_promocional_of2;
    }

    public String getVelocidad_prom_periodo_of2() {
        return velocidad_prom_periodo_of2;
    }

    public void setVelocidad_prom_periodo_of2(String velocidad_prom_periodo_of2) {
        this.velocidad_prom_periodo_of2 = velocidad_prom_periodo_of2;
    }

    public String getTipo_archivo() {
        return tipo_archivo;
    }

    public void setTipo_archivo(String tipo_archivo) {
        this.tipo_archivo = tipo_archivo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
