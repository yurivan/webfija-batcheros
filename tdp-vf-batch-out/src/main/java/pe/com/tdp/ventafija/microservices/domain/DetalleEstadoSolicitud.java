package pe.com.tdp.ventafija.microservices.domain;

import org.json.JSONObject;

public class DetalleEstadoSolicitud {

    public String COD_VTA_PPCD;
    public String COD_ERR_CD;
    public String DES_ERR_DS;
    public String FEC_HOR_REC_WA;
    public String COD_EST_VTA_APP;
    public String DES_EST_VTA_APP;
    public String COD_PED_ATI_CD;
    public String COD_REQ_PED_CD;
    public String COD_EST_PED_CD;
    public String DES_EST_PED_CD;
    public String FEC_HOR_REG;
    public String FEC_HOR_EMI_PED;
    public String COD_CLI_CD;
    public String COD_CTA_CD;
    public String COD_INS_LEG_CD;
    public String NUM_IDE_TLF;
    public String COD_CLI_CMS;
    public String COD_CTA_CMS;
    public String DES_OBS_VTA_APP;
    public String COD_ID_WEB_EX1;
    public String DES_ID_WEB_EX1;
    public String COD_EST_WEB_EX1;
    public String DES_EST_WEB_EX1;
    public String COD_ID_WEB_EX2;
    public String DES_ID_WEB_EX2;
    public String COD_EST_WEB_EX2;
    public String DES_EST_WEB_EX2;
    public String COD_ID_WEB_EX3;
    public String DES_ID_WEB_EX3;
    public String COD_EST_WEB_EX3;
    public String DES_EST_WEB_EX3;
    public String COD_ID_WEB_EX4;
    public String DES_ID_WEB_EX4;
    public String COD_EST_WEB_EX4;
    public String DES_EST_WEB_EX4;
    public String COD_ID_WEB_EX5;
    public String DES_ID_WEB_EX5;
    public String COD_EST_WEB_EX5;
    public String DES_EST_WEB_EX5;

    public String getCOD_VTA_PPCD() {
        return COD_VTA_PPCD;
    }

    public void setCOD_VTA_PPCD(String COD_VTA_PPCD) {
        this.COD_VTA_PPCD = COD_VTA_PPCD;
    }

    public String getCOD_ERR_CD() {
        return COD_ERR_CD;
    }

    public void setCOD_ERR_CD(String COD_ERR_CD) {
        this.COD_ERR_CD = COD_ERR_CD;
    }

    public String getDES_ERR_DS() {
        return DES_ERR_DS;
    }

    public void setDES_ERR_DS(String DES_ERR_DS) {
        this.DES_ERR_DS = DES_ERR_DS;
    }

    public String getFEC_HOR_REC_WA() {
        return FEC_HOR_REC_WA;
    }

    public void setFEC_HOR_REC_WA(String FEC_HOR_REC_WA) {
        this.FEC_HOR_REC_WA = FEC_HOR_REC_WA;
    }

    public String getCOD_EST_VTA_APP() {
        return COD_EST_VTA_APP;
    }

    public void setCOD_EST_VTA_APP(String COD_EST_VTA_APP) {
        this.COD_EST_VTA_APP = COD_EST_VTA_APP;
    }

    public String getDES_EST_VTA_APP() {
        return DES_EST_VTA_APP;
    }

    public void setDES_EST_VTA_APP(String DES_EST_VTA_APP) {
        this.DES_EST_VTA_APP = DES_EST_VTA_APP;
    }

    public String getCOD_PED_ATI_CD() {
        return COD_PED_ATI_CD;
    }

    public void setCOD_PED_ATI_CD(String COD_PED_ATI_CD) {
        this.COD_PED_ATI_CD = COD_PED_ATI_CD;
    }

    public String getCOD_REQ_PED_CD() {
        return COD_REQ_PED_CD;
    }

    public void setCOD_REQ_PED_CD(String COD_REQ_PED_CD) {
        this.COD_REQ_PED_CD = COD_REQ_PED_CD;
    }

    public String getCOD_EST_PED_CD() {
        return COD_EST_PED_CD;
    }

    public void setCOD_EST_PED_CD(String COD_EST_PED_CD) {
        this.COD_EST_PED_CD = COD_EST_PED_CD;
    }

    public String getDES_EST_PED_CD() {
        return DES_EST_PED_CD;
    }

    public void setDES_EST_PED_CD(String DES_EST_PED_CD) {
        this.DES_EST_PED_CD = DES_EST_PED_CD;
    }

    public String getFEC_HOR_REG() {
        return FEC_HOR_REG;
    }

    public void setFEC_HOR_REG(String FEC_HOR_REG) {
        this.FEC_HOR_REG = FEC_HOR_REG;
    }

    public String getFEC_HOR_EMI_PED() {
        return FEC_HOR_EMI_PED;
    }

    public void setFEC_HOR_EMI_PED(String FEC_HOR_EMI_PED) {
        this.FEC_HOR_EMI_PED = FEC_HOR_EMI_PED;
    }

    public String getCOD_CLI_CD() {
        return COD_CLI_CD;
    }

    public void setCOD_CLI_CD(String COD_CLI_CD) {
        this.COD_CLI_CD = COD_CLI_CD;
    }

    public String getCOD_CTA_CD() {
        return COD_CTA_CD;
    }

    public void setCOD_CTA_CD(String COD_CTA_CD) {
        this.COD_CTA_CD = COD_CTA_CD;
    }

    public String getCOD_INS_LEG_CD() {
        return COD_INS_LEG_CD;
    }

    public void setCOD_INS_LEG_CD(String COD_INS_LEG_CD) {
        this.COD_INS_LEG_CD = COD_INS_LEG_CD;
    }

    public String getNUM_IDE_TLF() {
        return NUM_IDE_TLF;
    }

    public void setNUM_IDE_TLF(String NUM_IDE_TLF) {
        this.NUM_IDE_TLF = NUM_IDE_TLF;
    }

    public String getCOD_CLI_CMS() {
        return COD_CLI_CMS;
    }

    public void setCOD_CLI_CMS(String COD_CLI_CMS) {
        this.COD_CLI_CMS = COD_CLI_CMS;
    }

    public String getCOD_CTA_CMS() {
        return COD_CTA_CMS;
    }

    public void setCOD_CTA_CMS(String COD_CTA_CMS) {
        this.COD_CTA_CMS = COD_CTA_CMS;
    }

    public String getDES_OBS_VTA_APP() {
        return DES_OBS_VTA_APP;
    }

    public void setDES_OBS_VTA_APP(String DES_OBS_VTA_APP) {
        this.DES_OBS_VTA_APP = DES_OBS_VTA_APP;
    }

    public String getCOD_ID_WEB_EX1() {
        return COD_ID_WEB_EX1;
    }

    public void setCOD_ID_WEB_EX1(String COD_ID_WEB_EX1) {
        this.COD_ID_WEB_EX1 = COD_ID_WEB_EX1;
    }

    public String getDES_ID_WEB_EX1() {
        return DES_ID_WEB_EX1;
    }

    public void setDES_ID_WEB_EX1(String DES_ID_WEB_EX1) {
        this.DES_ID_WEB_EX1 = DES_ID_WEB_EX1;
    }

    public String getCOD_EST_WEB_EX1() {
        return COD_EST_WEB_EX1;
    }

    public void setCOD_EST_WEB_EX1(String COD_EST_WEB_EX1) {
        this.COD_EST_WEB_EX1 = COD_EST_WEB_EX1;
    }

    public String getDES_EST_WEB_EX1() {
        return DES_EST_WEB_EX1;
    }

    public void setDES_EST_WEB_EX1(String DES_EST_WEB_EX1) {
        this.DES_EST_WEB_EX1 = DES_EST_WEB_EX1;
    }

    public String getCOD_ID_WEB_EX2() {
        return COD_ID_WEB_EX2;
    }

    public void setCOD_ID_WEB_EX2(String COD_ID_WEB_EX2) {
        this.COD_ID_WEB_EX2 = COD_ID_WEB_EX2;
    }

    public String getDES_ID_WEB_EX2() {
        return DES_ID_WEB_EX2;
    }

    public void setDES_ID_WEB_EX2(String DES_ID_WEB_EX2) {
        this.DES_ID_WEB_EX2 = DES_ID_WEB_EX2;
    }

    public String getCOD_EST_WEB_EX2() {
        return COD_EST_WEB_EX2;
    }

    public void setCOD_EST_WEB_EX2(String COD_EST_WEB_EX2) {
        this.COD_EST_WEB_EX2 = COD_EST_WEB_EX2;
    }

    public String getDES_EST_WEB_EX2() {
        return DES_EST_WEB_EX2;
    }

    public void setDES_EST_WEB_EX2(String DES_EST_WEB_EX2) {
        this.DES_EST_WEB_EX2 = DES_EST_WEB_EX2;
    }

    public String getCOD_ID_WEB_EX3() {
        return COD_ID_WEB_EX3;
    }

    public void setCOD_ID_WEB_EX3(String COD_ID_WEB_EX3) {
        this.COD_ID_WEB_EX3 = COD_ID_WEB_EX3;
    }

    public String getDES_ID_WEB_EX3() {
        return DES_ID_WEB_EX3;
    }

    public void setDES_ID_WEB_EX3(String DES_ID_WEB_EX3) {
        this.DES_ID_WEB_EX3 = DES_ID_WEB_EX3;
    }

    public String getCOD_EST_WEB_EX3() {
        return COD_EST_WEB_EX3;
    }

    public void setCOD_EST_WEB_EX3(String COD_EST_WEB_EX3) {
        this.COD_EST_WEB_EX3 = COD_EST_WEB_EX3;
    }

    public String getDES_EST_WEB_EX3() {
        return DES_EST_WEB_EX3;
    }

    public void setDES_EST_WEB_EX3(String DES_EST_WEB_EX3) {
        this.DES_EST_WEB_EX3 = DES_EST_WEB_EX3;
    }

    public String getCOD_ID_WEB_EX4() {
        return COD_ID_WEB_EX4;
    }

    public void setCOD_ID_WEB_EX4(String COD_ID_WEB_EX4) {
        this.COD_ID_WEB_EX4 = COD_ID_WEB_EX4;
    }

    public String getDES_ID_WEB_EX4() {
        return DES_ID_WEB_EX4;
    }

    public void setDES_ID_WEB_EX4(String DES_ID_WEB_EX4) {
        this.DES_ID_WEB_EX4 = DES_ID_WEB_EX4;
    }

    public String getCOD_EST_WEB_EX4() {
        return COD_EST_WEB_EX4;
    }

    public void setCOD_EST_WEB_EX4(String COD_EST_WEB_EX4) {
        this.COD_EST_WEB_EX4 = COD_EST_WEB_EX4;
    }

    public String getDES_EST_WEB_EX4() {
        return DES_EST_WEB_EX4;
    }

    public void setDES_EST_WEB_EX4(String DES_EST_WEB_EX4) {
        this.DES_EST_WEB_EX4 = DES_EST_WEB_EX4;
    }

    public String getCOD_ID_WEB_EX5() {
        return COD_ID_WEB_EX5;
    }

    public void setCOD_ID_WEB_EX5(String COD_ID_WEB_EX5) {
        this.COD_ID_WEB_EX5 = COD_ID_WEB_EX5;
    }

    public String getDES_ID_WEB_EX5() {
        return DES_ID_WEB_EX5;
    }

    public void setDES_ID_WEB_EX5(String DES_ID_WEB_EX5) {
        this.DES_ID_WEB_EX5 = DES_ID_WEB_EX5;
    }

    public String getCOD_EST_WEB_EX5() {
        return COD_EST_WEB_EX5;
    }

    public void setCOD_EST_WEB_EX5(String COD_EST_WEB_EX5) {
        this.COD_EST_WEB_EX5 = COD_EST_WEB_EX5;
    }

    public String getDES_EST_WEB_EX5() {
        return DES_EST_WEB_EX5;
    }

    public void setDES_EST_WEB_EX5(String DES_EST_WEB_EX5) {
        this.DES_EST_WEB_EX5 = DES_EST_WEB_EX5;
    }

    public DetalleEstadoSolicitud cargarDetalle(String autoConsultaObj) {

        JSONObject jsonObj = new JSONObject(autoConsultaObj);
        System.out.println(jsonObj);
        JSONObject jsonObjHeader = jsonObj.getJSONObject("HeaderOut");
        if(!"RESPONSE".equalsIgnoreCase(jsonObjHeader.getString("msgType"))) return null;

        JSONObject jsonObjBody = jsonObj.getJSONObject("BodyOut");

        this.setCOD_ERR_CD(!jsonObjBody.isNull("COD_ERR_CD") ? jsonObjBody.getString("COD_ERR_CD") : null);
        this.setDES_ERR_DS(!jsonObjBody.isNull("DES_ERR_DS") ? jsonObjBody.getString("DES_ERR_DS").toString() : null);
        this.setFEC_HOR_REC_WA(!jsonObjBody.isNull("FEC_HOR_REC_WA") ? jsonObjBody.getString("FEC_HOR_REC_WA") : null);
        this.setCOD_EST_VTA_APP(!jsonObjBody.isNull("COD_EST_VTA_APP") ? jsonObjBody.getString("COD_EST_VTA_APP") : null);
        this.setDES_EST_VTA_APP(!jsonObjBody.isNull("DES_EST_VTA_APP") ? jsonObjBody.getString("DES_EST_VTA_APP") : null);
        this.setCOD_PED_ATI_CD(!jsonObjBody.isNull("COD_PED_ATI_CD") ? jsonObjBody.getString("COD_PED_ATI_CD") : null);
        this.setCOD_REQ_PED_CD(!jsonObjBody.isNull("COD_REQ_PED_CD") ? jsonObjBody.getString("COD_REQ_PED_CD") : null);
        this.setCOD_EST_PED_CD(!jsonObjBody.isNull("COD_EST_PED_CD") ? jsonObjBody.getString("COD_EST_PED_CD") : null);
        this.setDES_EST_PED_CD(!jsonObjBody.isNull("DES_EST_PED_CD") ? jsonObjBody.getString("DES_EST_PED_CD") : null);
        this.setFEC_HOR_REG(!jsonObjBody.isNull("FEC_HOR_REG") ? jsonObjBody.getString("FEC_HOR_REG") : null);
        this.setFEC_HOR_EMI_PED(!jsonObjBody.isNull("FEC_HOR_EMI_PED") ? jsonObjBody.getString("FEC_HOR_EMI_PED") : null);
        this.setCOD_CLI_CD(!jsonObjBody.isNull("COD_CLI_CD") ? jsonObjBody.getString("COD_CLI_CD") : null);
        this.setCOD_CTA_CD(!jsonObjBody.isNull("COD_CTA_CD") ? jsonObjBody.getString("COD_CTA_CD") : null);
        this.setCOD_INS_LEG_CD(!jsonObjBody.isNull("COD_INS_LEG_CD") ? jsonObjBody.getString("COD_INS_LEG_CD") : null);
        this.setNUM_IDE_TLF(!jsonObjBody.isNull("NUM_IDE_TLF") ? jsonObjBody.getString("NUM_IDE_TLF") : null);
        this.setCOD_CLI_CMS(!jsonObjBody.isNull("COD_CLI_CMS") ? jsonObjBody.getString("COD_CLI_CMS") : null);
        this.setCOD_CTA_CMS(!jsonObjBody.isNull("COD_CTA_CMS") ? jsonObjBody.getString("COD_CTA_CMS") : null);
        this.setDES_OBS_VTA_APP(!jsonObjBody.isNull("DES_OBS_VTA_APP") ? jsonObjBody.getString("DES_OBS_VTA_APP") : null);
        this.setCOD_ID_WEB_EX1(!jsonObjBody.isNull("COD_ID_WEB_EX1") ? jsonObjBody.getString("COD_ID_WEB_EX1") : null);
        this.setDES_ID_WEB_EX1(!jsonObjBody.isNull("DES_ID_WEB_EX1") ? jsonObjBody.getString("DES_ID_WEB_EX1") : null);
        this.setCOD_EST_WEB_EX1(!jsonObjBody.isNull("COD_EST_WEB_EX1") ? jsonObjBody.getString("COD_EST_WEB_EX1") : null);
        this.setDES_EST_WEB_EX1(!jsonObjBody.isNull("DES_EST_WEB_EX1") ? jsonObjBody.getString("DES_EST_WEB_EX1") : null);
        this.setCOD_ID_WEB_EX2(!jsonObjBody.isNull("COD_ID_WEB_EX2") ? jsonObjBody.getString("COD_ID_WEB_EX2") : null);
        this.setDES_ID_WEB_EX2(!jsonObjBody.isNull("DES_ID_WEB_EX2") ? jsonObjBody.getString("DES_ID_WEB_EX2") : null);
        this.setCOD_EST_WEB_EX2(!jsonObjBody.isNull("COD_EST_WEB_EX2") ? jsonObjBody.getString("COD_EST_WEB_EX2") : null);
        this.setDES_EST_WEB_EX2(!jsonObjBody.isNull("DES_EST_WEB_EX2") ? jsonObjBody.getString("DES_EST_WEB_EX2") : null);
        this.setCOD_ID_WEB_EX3(!jsonObjBody.isNull("COD_ID_WEB_EX3") ? jsonObjBody.getString("COD_ID_WEB_EX3") : null);
        this.setDES_ID_WEB_EX3(!jsonObjBody.isNull("DES_ID_WEB_EX3") ? jsonObjBody.getString("DES_ID_WEB_EX3") : null);
        this.setCOD_EST_WEB_EX3(!jsonObjBody.isNull("COD_EST_WEB_EX3") ? jsonObjBody.getString("COD_EST_WEB_EX3") : null);
        this.setDES_EST_WEB_EX3(!jsonObjBody.isNull("DES_EST_WEB_EX3") ? jsonObjBody.getString("DES_EST_WEB_EX3") : null);
        this.setCOD_ID_WEB_EX4(!jsonObjBody.isNull("COD_ID_WEB_EX4") ? jsonObjBody.getString("COD_ID_WEB_EX4") : null);
        this.setDES_ID_WEB_EX4(!jsonObjBody.isNull("DES_ID_WEB_EX4") ? jsonObjBody.getString("DES_ID_WEB_EX4") : null);
        this.setCOD_EST_WEB_EX4(!jsonObjBody.isNull("COD_EST_WEB_EX4") ? jsonObjBody.getString("COD_EST_WEB_EX4") : null);
        this.setDES_EST_WEB_EX4(!jsonObjBody.isNull("DES_EST_WEB_EX4") ? jsonObjBody.getString("DES_EST_WEB_EX4") : null);
        this.setCOD_ID_WEB_EX5(!jsonObjBody.isNull("COD_ID_WEB_EX5") ? jsonObjBody.getString("COD_ID_WEB_EX5") : null);
        this.setDES_ID_WEB_EX5(!jsonObjBody.isNull("DES_ID_WEB_EX5") ? jsonObjBody.getString("DES_ID_WEB_EX5") : null);
        this.setCOD_EST_WEB_EX5(!jsonObjBody.isNull("COD_EST_WEB_EX5") ? jsonObjBody.getString("COD_EST_WEB_EX5") : null);
        this.setDES_EST_WEB_EX5(!jsonObjBody.isNull("DES_EST_WEB_EX5") ? jsonObjBody.getString("DES_EST_WEB_EX5") : null);
        return this;
    }
}
