package pe.com.tdp.ventafija.microservices.domain.thirdparty.dto;

import pe.com.tdp.ventafija.microservices.domain.thirdparty.CatalogOutResponseData;

import java.util.ArrayList;
import java.util.List;

public class SyncOutResult {
    private Integer insertCount;
    private Integer updateCount;
    private Integer errorCount;
    private Integer orderCount;
    //private List<CatalogOutResponseData> data;
    //private List<CatalogOutResponseData> errors;
    private List<SynOutSyntaxError> syntaxErrors;

    public SyncOutResult() {
        super();
        this.insertCount = 0;
        this.updateCount = 0;
        this.errorCount = 0;
        this.orderCount = 0;
        //this.data = new ArrayList<>();
        //this.errors = new ArrayList<>();
        this.syntaxErrors = new ArrayList<>();
    }

    public SyncOutResult(Integer insertCount, Integer updateCount, Integer errorCount, Integer orderCount/*,
                         List<CatalogOutResponseData> data, List<CatalogOutResponseData> errors*/) {
        this.insertCount = insertCount;
        this.updateCount = updateCount;
        this.errorCount = errorCount;
        this.orderCount = orderCount;
        //this.data = data;
        //this.errors = errors;
    }

    public Integer getInsertCount() {
        return insertCount;
    }

    public void setInsertCount(Integer insertCount) {
        this.insertCount = insertCount;
    }

    public Integer getUpdateCount() {
        return updateCount;
    }

    public void setUpdateCount(Integer updateCount) {
        this.updateCount = updateCount;
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public Integer getOrderCount() {
        return orderCount;
    }

    public void setOrderCount(Integer orderCount) {
        this.orderCount = orderCount;
    }

    /*public List<CatalogOutResponseData> getData() {
        return data;
    }

    public void setData(List<CatalogOutResponseData> data) {
        this.data = data;
    }

    public List<CatalogOutResponseData> getErrors() {
        return errors;
    }

    public void setErrors(List<CatalogOutResponseData> errors) {
        this.errors = errors;
    }*/

    public List<SynOutSyntaxError> getSyntaxErrors() {
        return syntaxErrors;
    }

    public void setSyntaxErrors(List<SynOutSyntaxError> syntaxErrors) {
        this.syntaxErrors = syntaxErrors;
    }
}
