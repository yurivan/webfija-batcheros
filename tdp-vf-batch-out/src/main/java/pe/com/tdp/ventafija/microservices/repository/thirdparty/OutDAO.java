package pe.com.tdp.ventafija.microservices.repository.thirdparty;

import java.sql.*;

import java.util.*;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.util.exception.SyncException;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.*;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncOutResult;


@Repository
public class OutDAO {
    private static final Logger logger = LogManager.getLogger();
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;

    public SyncOutResult syncCaptaOut(List<CatalogCaptaOutResponseData> outs, String fileNameTxt) throws SyncException {
        logger.info("iniciando registro en catalog_capta_out");

        //List<CatalogCaptaOutResponseData> response = new ArrayList<>();
        //List<CatalogCaptaOutResponseData> errors = new ArrayList<>();
        int totalInsert = 0;
        int totalUpdate = 0;
        int totalError = 0;
        int totalOrderUpdate = 0;

        try (Connection con = Database.datasource().getConnection()){

            String delete = " DELETE FROM ibmx_a07e6d02edaf552.tdp_catalog_capta_out";
            PreparedStatement psDelete = con.prepareStatement(delete);
            //psDelete.setString(1, fileNameTxt.substring(7));
            int deletedRows = psDelete.executeUpdate();
            logger.info("Cantidad de rows eliminados del archivo " + fileNameTxt + ": " + deletedRows);
            //con.commit();

            String insert = " insert into ibmx_a07e6d02edaf552.tdp_catalog_capta_out " +
                    " (cod_accion, cod_prod_oferta, tx_nombre, numero_telefono, tipo_documento, numero_documento, " +
                    " departamento, provincia, distrito, tx_direccion, ps_oferta2, tipo_archivo, fecha) " +
                    " values (?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement psInsert = con.prepareStatement(insert);
            logger.info("numero de registros a procesar: "+ outs.size());

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            Date fechaActual = cal.getTime();
            Timestamp fechaActualTimestamp = new Timestamp(fechaActual.getTime());

            int countBatch=0;

            for (CatalogCaptaOutResponseData out : outs) {

                psInsert.setString(1, out.getCod_accion());
                psInsert.setString(2, out.getCod_prod_oferta());
                psInsert.setString(3, out.getTx_nombre());
                psInsert.setString(4, out.getNumero_telefono());
                psInsert.setString(5, out.getTipo_documento());
                psInsert.setString(6, out.getNumero_documento());
                psInsert.setString(7, out.getDepartamento());
                psInsert.setString(8, out.getProvincia());
                psInsert.setString(9, out.getDistrito());
                psInsert.setString(10, out.getTx_direccion());
                psInsert.setString(11, out.getPs_oferta2());

                psInsert.setString(12, fileNameTxt.substring(7));
                psInsert.setTimestamp(13, fechaActualTimestamp);
                psInsert.addBatch();
                //response.add(out);

                countBatch++;

                if (countBatch == 5000) {
                    int[] result = psInsert.executeBatch();
                    totalInsert = processInsertCount(totalInsert, result);
                    psInsert.clearBatch();
                    countBatch = 0;
                }
            }

            if (countBatch > 0) {
                int[] result = psInsert.executeBatch();
                totalInsert = processInsertCount(totalInsert, result);
            }

            //int[] rowsInserted = psInsert.executeBatch();
            //totalInsert = processInsertCount(totalInsert, rowsInserted);

        } catch (SQLException e) {
            logger.error("error en conexion: " + e.getMessage());
            throw new SyncException(e);
        }

        logger.info("finalizados registro en catalog_capta_out");

        return new SyncOutResult(totalInsert, totalUpdate, totalError, totalOrderUpdate/*, response, errors*/);

    }

    public SyncOutResult syncPlantaOut(List<CatalogPlantaOutResponseData> outs, String fileNameTxt) throws SyncException {
        logger.info("iniciando registro en catalog_planta_out");

        //List<CatalogCaptaOutResponseData> response = new ArrayList<>();
        //List<CatalogCaptaOutResponseData> errors = new ArrayList<>();
        int totalInsert = 0;
        int totalUpdate = 0;
        int totalError = 0;
        int totalOrderUpdate = 0;

        try (Connection con = Database.datasource().getConnection()){

            String delete = " DELETE FROM ibmx_a07e6d02edaf552.tdp_catalog_planta_out";
            PreparedStatement psDelete = con.prepareStatement(delete);
            //psDelete.setString(1, fileNameTxt.substring(7));
            int deletedRows = psDelete.executeUpdate();
            logger.info("Cantidad de rows eliminados del archivo " + fileNameTxt + ": " + deletedRows);
            //con.commit();

            String insert = " insert into ibmx_a07e6d02edaf552.tdp_catalog_planta_out " +
                    " (cod_accion, campania, renta_origen, tx_nombre, numero_telefono, celular, tipo_documento, numero_documento, " +
                    " departamento, provincia, distrito, tx_direccion, ps_of1, ps_svas_of1, salto_of1, renta_destino_of1," +
                    " renta_promocional_of1, renta_prom_periodo_of1, velocidad_promocional_of1, velocidad_prom_periodo_of1," +
                    " ps_of2, ps_svas_of2, salto_of2, renta_destino_of2, renta_promocional_of2, renta_prom_periodo_of2," +
                    " velocidad_promocional_of2, velocidad_prom_periodo_of2, tipo_archivo, fecha) " +
                    " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            PreparedStatement psInsert = con.prepareStatement(insert);
            logger.info("numero de registros a procesar: "+ outs.size());

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            Date fechaActual = cal.getTime();
            Timestamp fechaActualTimestamp = new Timestamp(fechaActual.getTime());

            int countBatch=0;

            for (CatalogPlantaOutResponseData out : outs) {

                psInsert.setString(1, out.getCod_accion());
                psInsert.setString(2, out.getCampania());
                psInsert.setString(3, out.getRenta_origen());
                psInsert.setString(4, out.getTx_nombre());
                psInsert.setString(5, out.getNumero_telefono());
                psInsert.setString(6, out.getCelular());
                psInsert.setString(7, out.getTipo_documento());
                psInsert.setString(8, out.getNumero_documento());
                psInsert.setString(9, out.getDepartamento());
                psInsert.setString(10, out.getProvincia());
                psInsert.setString(11, out.getDistrito());
                psInsert.setString(12, out.getTx_direccion());
                psInsert.setString(13, out.getPs_of1());
                psInsert.setString(14, out.getPs_svas_of1());
                psInsert.setString(15, out.getSalto_of1());
                psInsert.setString(16, out.getRenta_destino_of1());
                psInsert.setString(17, out.getRenta_promocional_of1());
                psInsert.setString(18, out.getRenta_prom_periodo_of1());
                psInsert.setString(19, out.getVelocidad_promocional_of1());
                psInsert.setString(20, out.getVelocidad_prom_periodo_of1());
                psInsert.setString(21, out.getPs_of2());
                psInsert.setString(22, out.getPs_svas_of2());
                psInsert.setString(23, out.getSalto_of2());
                psInsert.setString(24, out.getRenta_destino_of2());
                psInsert.setString(25, out.getRenta_promocional_of2());
                psInsert.setString(26, out.getRenta_prom_periodo_of2());
                psInsert.setString(27, out.getVelocidad_promocional_of2());
                psInsert.setString(28, out.getVelocidad_prom_periodo_of2());

                psInsert.setString(29, fileNameTxt.substring(7));
                psInsert.setTimestamp(30, fechaActualTimestamp);
                psInsert.addBatch();

                countBatch++;

                if (countBatch == 5000) {
                    int[] result = psInsert.executeBatch();
                    totalInsert = processInsertCount(totalInsert, result);
                    psInsert.clearBatch();
                    countBatch = 0;
                }

            }

            if (countBatch > 0) {
                int[] result = psInsert.executeBatch();
                totalInsert = processInsertCount(totalInsert, result);
            }

            //int[] rowsInserted = psInsert.executeBatch();
            //totalInsert = processInsertCount(totalInsert, rowsInserted);

        } catch (SQLException e) {
            logger.error("error en conexion: " + e.getMessage());
            throw new SyncException(e);
        }

        logger.info("finalizados registro en catalog_planta_out");

        return new SyncOutResult(totalInsert, totalUpdate, totalError, totalOrderUpdate/*, response, errors*/);

    }

    private int processInsertCount(int currentCount, int[] insertResults) {
        int count = currentCount;
        for (int insertResult : insertResults) {
            switch (insertResult) {
                case Statement.SUCCESS_NO_INFO:
                    count += 1;
                    break;
                case Statement.EXECUTE_FAILED:
                    break;
                default:
                    count += insertResult;
                    break;
            }
        }
        return count;
    }

}