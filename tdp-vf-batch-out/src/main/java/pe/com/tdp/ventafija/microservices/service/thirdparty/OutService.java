package pe.com.tdp.ventafija.microservices.service.thirdparty;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;


import pe.com.tdp.ventafija.microservices.common.exception.ApplicationException;
import pe.com.tdp.ventafija.microservices.common.util.StringUtils;
import pe.com.tdp.ventafija.microservices.common.util.exception.SyncException;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.*;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.*;
import pe.com.tdp.ventafija.microservices.repository.thirdparty.OutDAO;


@Service
public class OutService {

	private static final Logger logger = LogManager.getLogger();

	@Value("${out.api.sync.enabled}")
	private Boolean syncEnabled;

	@Autowired
	private FtpOutService ftpService;

	@Value("${ftp.api.connection.dirout}")
	private String remoteDirectoryOut;

	@Value("${ftp.api.connection.takendirout}")
	private String takenDirectoryOut;

	@Value("${ftp.api.connection.errordirout}")
	private String errorDirectoryOut;

	public static String remoteDirectoryOutStatic;

	@Autowired
	private OutDAO dao;


	//@Scheduled(cron = "${out.api.sync.cron}")
	//@Scheduled(cron = "0/30 * * * * *")
	public void loadFileOut() {
		if (!syncEnabled) {
			logger.info("Carga del archivo para OUT no habilitada");
			return;
		}

		remoteDirectoryOutStatic = remoteDirectoryOut;

		Date ahora = new Date();
		long ahoramilisecs = ahora.getTime();
		logger.info("Iniciando carga del archivo para OUT " + ahora + " .....");
		//String remoteDirectory = "/sftp/Out";
		List<String> filesToProcess = ftpService.listFiles(remoteDirectoryOut);
		logger.info(String.format("listado de archivos del ftp en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));

		ahoramilisecs = new Date().getTime();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM_");
		sdf.setTimeZone(TimeZone.getTimeZone("America/Lima"));
		String actualMonth = sdf.format(new Date());


		for (String nameFile : filesToProcess) {
			if(nameFile != null && nameFile.startsWith(actualMonth)){
				if(nameFile.toLowerCase().contains("capta")){
					processFile(nameFile, ahoramilisecs);
				}else if(nameFile.toLowerCase().contains("planta")){
					processFile(nameFile, ahoramilisecs);
				}else{
					logger.info("Este archivo no esta siendo considerado.");
				}
			}
		}
	}

	public void processFile(String nameFile, Long ahoramilisecs) {
		logger.info("archivo: " + nameFile);
		// nuevo nombre identificativo del archivo ??
		String newFileName = String.format("COF_%s_%s", new Date().getTime(), nameFile);
		logger.info(String.format("Archivo cambiado de nombre en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
		ahoramilisecs = new Date().getTime();

		String filePath = "." + remoteDirectoryOut + nameFile;

		try {
			syncOut(filePath, nameFile);
			ftpService.moveFile(remoteDirectoryOut, nameFile, takenDirectoryOut, newFileName);
		} catch (SyncException e) {
			logger.error("Error: " + e.getMessage());
			int position = nameFile.lastIndexOf(".txt");
			String errorFileName = nameFile;
			if (position != -1) {
				errorFileName = nameFile.substring(0, position) + "0.txt";
			}

			ftpService.moveFile(remoteDirectoryOut, nameFile, errorDirectoryOut, errorFileName);
		}
		logger.info(String.format("Archivo procesado en %s minutos", ((double) ((new Date()).getTime() - ahoramilisecs)) / 60000));
	}

	public Response<String> syncOut(String remoteFilePath, String fileNameTxt) throws SyncException {
		logger.info("starting syncOut of file: " + remoteFilePath);
		Response<String> response = new Response<>();
		File file = null;
		try {
			file = ftpService.retrieveFile(remoteFilePath);
			if (file != null) {

				if(fileNameTxt.toLowerCase().contains("capta")){
					processCaptaOutFile(file, fileNameTxt);
				}else if(fileNameTxt.toLowerCase().contains("planta")){
					processPlantaOutFile(file, fileNameTxt);
				}

			} else {
				throw new ApplicationException("file.not.found");
			}

			response.setResponseCode("0");
			response.setResponseMessage("OUT se sincronizo correctamente.");
		} catch (IOException | ApplicationException e) {
			logger.info("Error al buscar el archivo");
			response.setResponseCode("1");
			response.setResponseMessage("Error al sincronizar OUT.");
		} finally {
			if (file != null) {
				file.delete();
			}
		}
		logger.info("sync1 finished");
		return response;
	}

	public void processCaptaOutFile(File file, String fileNameTxt) throws IOException, SyncException {
		logger.info("OutService.processOutCaptaFile .........");
		List<CatalogCaptaOutResponseData> outs = new ArrayList<>();
		List<SynOutSyntaxError> syntaxErrors = new ArrayList<>();

		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.ISO_8859_1));

		String line;
		String txtSplitBy = "\";\"";
		int count = 0;

		while ((line = br.readLine()) != null) {

			if (line.startsWith("\"COD_ACCION\"")) continue;

			count++;

			line = "\";" + line + ";\"";
			line = line.replaceAll("\";\""," \";\" ");

			String[] out = line.split(txtSplitBy);

			String[] outNew = new String[out.length - 2];
			for(int i=0; i<out.length;i++){
				if(i!=0 && i!=out.length-1) outNew[i-1] = out[i];
			}
			out = outNew;

			if (out.length < 11) {
				logger.info("error al procesar linea " + line);
				syntaxErrors.add(new SynOutSyntaxError(SynOutSyntaxError.CAUSE_INCOMPLETE_LINE, line));
				continue;
			}

			//logger.info(count);

			CatalogCaptaOutResponseData model = new CatalogCaptaOutResponseData();
			model.setCod_accion(StringUtils.trim(out[0]));
			model.setCod_prod_oferta(StringUtils.trim(out[1]));
			model.setTx_nombre(StringUtils.trim(out[2]));
			model.setNumero_telefono(StringUtils.trim(out[3]));
			model.setTipo_documento(StringUtils.trim(out[4]));
			model.setNumero_documento(StringUtils.trim(out[5]));
			model.setDepartamento(StringUtils.trim(out[6]));
			model.setProvincia(StringUtils.trim(out[7]));
			model.setDistrito(StringUtils.trim(out[8]));
			model.setTx_direccion(StringUtils.trim(out[9]));
			model.setPs_oferta2(StringUtils.trim(out[10]));

			outs.add(model);
		}

		in.close();
		fstream.close();

		SyncOutResult result = dao.syncCaptaOut(outs, fileNameTxt);

		result.setSyntaxErrors(syntaxErrors);
		result.setErrorCount(result.getErrorCount() + syntaxErrors.size());

		logger.info(String.format("Total registros leidos: %s, insertados: %s, actualizados: %s,actualizados order: %s, errores: %s",
				outs.size(), result.getInsertCount(), result.getUpdateCount(), result.getOrderCount(), result.getErrorCount()));

		List<String> linesWithError = new ArrayList<>();
		for (SynOutSyntaxError error : syntaxErrors) {
			linesWithError.add(error.getLine());
		}

		logger.info("fin OutService.processOutCaptaFile .....................");

	}

	public void processPlantaOutFile(File file, String fileNameTxt) throws IOException, SyncException {
		logger.info("OutService.processPlantaOutFile .........");
		List<CatalogPlantaOutResponseData> outs = new ArrayList<>();
		List<SynOutSyntaxError> syntaxErrors = new ArrayList<>();

		FileInputStream fstream = new FileInputStream(file);
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in, StandardCharsets.ISO_8859_1));

		String line;
		String txtSplitBy = "\";\"";
		int count = 0;

		while ((line = br.readLine()) != null) {

			if (line.startsWith("\"COD_ACCION\"")) continue;

			count++;

			line = "\";" + line + ";\"";
			line = line.replaceAll("\";\""," \";\" ");

			String[] out = line.split(txtSplitBy);

			String[] outNew = new String[out.length - 2];
			for(int i=0; i<out.length;i++){
				if(i!=0 && i!=out.length-1) outNew[i-1] = out[i];
			}
			out = outNew;

			if (out.length < 28) {
				logger.info("error al procesar linea " + line);
				syntaxErrors.add(new SynOutSyntaxError(SynOutSyntaxError.CAUSE_INCOMPLETE_LINE, line));
				continue;
			}

			//logger.info(count);

			CatalogPlantaOutResponseData model = new CatalogPlantaOutResponseData();
			model.setCod_accion(StringUtils.trim(out[0]));
			model.setCampania(StringUtils.trim(out[1]));
			model.setRenta_origen(StringUtils.trim(out[2]));
			model.setTx_nombre(StringUtils.trim(out[3]));
			model.setNumero_telefono(StringUtils.trim(out[4]));
			model.setCelular(StringUtils.trim(out[5]));
			model.setTipo_documento(StringUtils.trim(out[6]));
			model.setNumero_documento(StringUtils.trim(out[7]));
			model.setDepartamento(StringUtils.trim(out[8]));
			model.setProvincia(StringUtils.trim(out[9]));
			model.setDistrito(StringUtils.trim(out[10]));
			model.setTx_direccion(StringUtils.trim(out[11]));
			model.setPs_of1(StringUtils.trim(out[12]));
			model.setPs_svas_of1(StringUtils.trim(out[13]));
			model.setSalto_of1(StringUtils.trim(out[14]));
			model.setRenta_destino_of1(StringUtils.trim(out[15]));
			model.setRenta_promocional_of1(StringUtils.trim(out[16]));
			model.setRenta_prom_periodo_of1(StringUtils.trim(out[17]));
			model.setVelocidad_promocional_of1(StringUtils.trim(out[18]));
			model.setVelocidad_prom_periodo_of1(StringUtils.trim(out[19]));
			model.setPs_of2(StringUtils.trim(out[20]));
			model.setPs_svas_of2(StringUtils.trim(out[21]));
			model.setSalto_of2(StringUtils.trim(out[22]));
			model.setRenta_destino_of2(StringUtils.trim(out[23]));
			model.setRenta_promocional_of2(StringUtils.trim(out[24]));
			model.setRenta_prom_periodo_of2(StringUtils.trim(out[25]));
			model.setVelocidad_promocional_of2(StringUtils.trim(out[26]));
			model.setVelocidad_prom_periodo_of2(StringUtils.trim(out[27]));

			outs.add(model);
		}

		in.close();
		fstream.close();

		SyncOutResult result = dao.syncPlantaOut(outs, fileNameTxt);

		result.setSyntaxErrors(syntaxErrors);
		result.setErrorCount(result.getErrorCount() + syntaxErrors.size());

		logger.info(String.format("Total registros leidos: %s, insertados: %s, actualizados: %s,actualizados order: %s, errores: %s",
				outs.size(), result.getInsertCount(), result.getUpdateCount(), result.getOrderCount(), result.getErrorCount()));

		List<String> linesWithError = new ArrayList<>();
		for (SynOutSyntaxError error : syntaxErrors) {
			linesWithError.add(error.getLine());
		}

		logger.info("fin OutService.processPlantaOutFile .....................");

	}
}





