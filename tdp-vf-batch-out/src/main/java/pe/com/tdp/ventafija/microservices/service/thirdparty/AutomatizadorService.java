package pe.com.tdp.ventafija.microservices.service.thirdparty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.connection.Api;
import pe.com.tdp.ventafija.microservices.common.exception.ApplicationException;
import pe.com.tdp.ventafija.microservices.common.util.StringUtils;
import pe.com.tdp.ventafija.microservices.common.util.exception.SyncException;
import pe.com.tdp.ventafija.microservices.domain.DetalleEstadoSolicitud;
import pe.com.tdp.ventafija.microservices.domain.Response;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.CatalogCaptaOutResponseData;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.CatalogPlantaOutResponseData;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SynOutSyntaxError;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncOutResult;
import pe.com.tdp.ventafija.microservices.repository.thirdparty.AutomatizadorDAO;
import pe.com.tdp.ventafija.microservices.repository.thirdparty.OutDAO;

import javax.ws.rs.NotAuthorizedException;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;


@Service
public class AutomatizadorService {

	private static final Logger logger = LogManager.getLogger();

	@Value("${automatizador.api.sync.enabled}")
	private Boolean syncEnabled;

	@Value("${tdp.api.automatizador.consulting.uri}")
	private String url;

	@Value("${tdp.api.automatizador.consulting.apiid}")
	private String apiid;

	@Value("${tdp.api.automatizador.consulting.apisecret}")
	private String apisecret;

	@Value("${tdp.api.automatizador.consulting.pid}")
	private String pid;

	@Value("${tdp.api.automatizador.consulting.execId}")
	private String execid;

	@Value("${tdp.api.automatizador.consulting.msgId}")
	private String msgid;

	@Value("${tdp.api.automatizador.consulting.delay.minutes}")
	private String minutesDelay;

	@Autowired
	private AutomatizadorDAO dao;

	@Scheduled(cron = "0/30 * * * * *")
	public void detectAutomatizadorNoResponse() {
		if (!syncEnabled) {
			logger.info("Detección de NO respuesta de automatizador no habilitada");
			return;
		}
		try{
			List<String> orderIdList = dao.getAutomatizadorWithoutResponse(minutesDelay);
			Map<String,DetalleEstadoSolicitud> detalleMap = getAutomatizadorStatus(orderIdList);
			if(detalleMap != null && detalleMap.size() > 0) dao.saveConsulting(detalleMap);
		}catch (Exception ex){
			logger.info(ex.getMessage());
		}
	}

	public Map<String,DetalleEstadoSolicitud> getAutomatizadorStatus(List<String> orderIdList){
		Map<String,DetalleEstadoSolicitud> map = new HashMap<String,DetalleEstadoSolicitud>();
		try{
			for(String orderId : orderIdList){
				DetalleEstadoSolicitud detalle = loadAutomatizador(orderId);
				if(detalle != null){
					detalle.setCOD_VTA_PPCD(orderId);
					map.put(orderId,detalle);
				}
			}
			return map;
		}catch (Exception ex){
			return null;
		}
	}

	public DetalleEstadoSolicitud loadAutomatizador(String codVTA) {
		logger.info("load object "+codVTA);
		if (codVTA == null) {
			return null;
		}

		URI uri;
		DetalleEstadoSolicitud estadoSolicitud = new DetalleEstadoSolicitud();
		String autoConsultaObj = null;
		Map<String, String> headers;
		headers=cargarHeaders();
		try {
			uri = new URI(url + codVTA);
			logger.info(uri);
			autoConsultaObj = Api.jerseyGET(uri,headers);
			System.out.println(autoConsultaObj);
			if (autoConsultaObj != null) {
				estadoSolicitud = estadoSolicitud.cargarDetalle(autoConsultaObj);
			}
		} catch (URISyntaxException e) {
			logger.error("Error URISyntaxException: " + e.getMessage());
			return null;
		}
		catch (NotAuthorizedException e) {
			logger.error("Error NotAuthorizedException: " + e.getMessage());
			String responde = e.getResponse().readEntity(String.class);
			logger.error(responde);
			return null;
		}catch (Exception e) {
			logger.error("Error Exception: " + e.getMessage());
			return null;
		}
		logger.info("loaded object: "+autoConsultaObj);
		return estadoSolicitud;
	}

	public Map<String, String> cargarHeaders(){
		Map<String, String> headers = new HashMap<>();
		headers.put("X-IBM-Client-Id", apiid);
		headers.put("X-IBM-Client-Secret", apisecret);
		headers.put("country", "PE");
		headers.put("lang", "es");
		headers.put("entity", "TDP");
		headers.put("system", "STC");
		headers.put("subsystem", "CRM");
		headers.put("originator", "PE:TDP:STC:CRM");
		headers.put("sender", "OracleServiceBus");
		headers.put("userId", "USERSTC");
		headers.put("wsId", "SistemSTC");
		headers.put("wsIp", "10.10.10.10");
		headers.put("operation", "consultation");
		headers.put("destination", "PE:ZYTRUST:PORTALZT:GATEWAY");
		headers.put("pid", pid);
		headers.put("execId", execid);
		headers.put("msgId", msgid);
		headers.put("timestamp", "2018-08-13T15:28:21.988-05:00");
		headers.put("msgType", "REQUEST");
		return headers;
	}

}





