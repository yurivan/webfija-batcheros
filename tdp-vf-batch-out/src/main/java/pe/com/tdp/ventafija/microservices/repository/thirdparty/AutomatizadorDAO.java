package pe.com.tdp.ventafija.microservices.repository.thirdparty;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.util.exception.SyncException;
import pe.com.tdp.ventafija.microservices.domain.DetalleEstadoSolicitud;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.CatalogCaptaOutResponseData;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.CatalogPlantaOutResponseData;
import pe.com.tdp.ventafija.microservices.domain.thirdparty.dto.SyncOutResult;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;


@Repository
public class AutomatizadorDAO {
    private static final Logger logger = LogManager.getLogger();
    @Value("${datasource.query.sp.call}")
    private String MY_SQL_SP;

    public List<String> getAutomatizadorWithoutResponse(String minutesDelay) throws SyncException {
        logger.info("iniciando deteccion de automatizador sin respuesta");

        List<String> orderIdList = new ArrayList<>();
        try (Connection con = Database.datasource().getConnection()){
            String query = "SELECT tdpo.order_id FROM ibmx_a07e6d02edaf552.tdp_order tdpo" +
                    " LEFT JOIN ibmx_a07e6d02edaf552.tdp_automatizador tdpa ON tdpa.codvtappcd = tdpo.order_id" +
                    " WHERE automatizador_ok = 1 AND tdpa.codvtappcd is null" +
                    " AND tdpo.order_fecha_hora < (now() AT TIME ZONE 'America/Lima' - INTERVAL '"+ minutesDelay +" minutes')";

            PreparedStatement stmt = con.prepareStatement(query);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                String orderId = rs.getString(1);
                if(orderId != null && !orderId.equals("")) orderIdList.add(orderId);
            }

        } catch (SQLException e) {
            logger.error("error en conexion: " + e.getMessage());
            throw new SyncException(e);
        }
        logger.info("finalizados deteccion de automatizador sin respuesta");
        return orderIdList;
    }

    public int saveConsulting(Map<String,DetalleEstadoSolicitud> map) throws SyncException {
        logger.info("iniciando registro en tdp_automatizador");

        int totalInsert = 0;

        try (Connection con = Database.datasource().getConnection()){

            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            Date fechaActual = cal.getTime();
            Timestamp fechaActualTimestamp = new Timestamp(fechaActual.getTime());

            String insert = " insert into ibmx_a07e6d02edaf552.tdp_automatizador " +
                    " (coderrcd,deserrds,fechorrecwa,codestvtaapp,desestvtaapp,codpedaticd,codreqeedcd,codestpedcd,desestpedcd," +
                    " fechorreg,fechoremiped,codclicd,codctacd,codinslegcd,numidtlf,codclicms,codctacms,desobsvtaapp," +
                    " codidwebex1,desidwebex1,codestwebex1,desestwebex1,codidwebex2,desidwebex2,codestwebex2,desestwebex2," +
                    " codidwebex3,desidwebex3,codestwebex3,desestwebex3,codidwebex4,desidwebex4,codestwebex4,desestwebex4," +
                    " codidwebex5,desidwebex5,codestwebex5,desestwebex5, codvtappcd, creation_date) " +
                    " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?," +
                    " ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?,?)";

            PreparedStatement psInsert = con.prepareStatement(insert);
            logger.info("numero de registros a procesar: "+ map.size());

            for (Map.Entry<String, DetalleEstadoSolicitud> entry : map.entrySet()){
                DetalleEstadoSolicitud item = entry.getValue();

                Timestamp fecHorRecWaTimestamp = null;
                try{
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
                    Date fecHorRecWa = format.parse(item.getFEC_HOR_REC_WA());
                    fecHorRecWaTimestamp = new Timestamp(fecHorRecWa.getTime());
                }catch (Exception ex){}


                psInsert.setString(1, item.getCOD_ERR_CD());
                psInsert.setString(2, item.getDES_ERR_DS());
                psInsert.setTimestamp(3, fecHorRecWaTimestamp);
                psInsert.setString(4, item.getCOD_EST_VTA_APP());
                psInsert.setString(5, item.getDES_EST_VTA_APP());
                psInsert.setString(6, item.getCOD_PED_ATI_CD());
                psInsert.setString(7, item.getCOD_REQ_PED_CD());
                psInsert.setString(8, item.getCOD_EST_PED_CD());
                psInsert.setString(9, item.getDES_EST_PED_CD());
                psInsert.setString(10, item.getFEC_HOR_REG());
                psInsert.setString(11, item.getFEC_HOR_EMI_PED());
                psInsert.setString(12, item.getCOD_CLI_CD());
                psInsert.setString(13, item.getCOD_CTA_CD());
                psInsert.setString(14, item.getCOD_INS_LEG_CD());
                psInsert.setString(15, item.getNUM_IDE_TLF());
                psInsert.setString(16, item.getCOD_CLI_CMS());
                psInsert.setString(17, item.getCOD_CTA_CMS());
                psInsert.setString(18, item.getDES_OBS_VTA_APP());

                psInsert.setString(19, item.getCOD_ID_WEB_EX1());
                psInsert.setString(20, item.getDES_ID_WEB_EX1());
                psInsert.setString(21, item.getCOD_EST_WEB_EX1());
                psInsert.setString(22, item.getDES_EST_WEB_EX1());
                psInsert.setString(23, item.getCOD_ID_WEB_EX2());
                psInsert.setString(24, item.getDES_ID_WEB_EX2());
                psInsert.setString(25, item.getCOD_EST_WEB_EX2());
                psInsert.setString(26, item.getDES_EST_WEB_EX2());
                psInsert.setString(27, item.getCOD_ID_WEB_EX3());
                psInsert.setString(28, item.getDES_ID_WEB_EX3());
                psInsert.setString(29, item.getCOD_EST_WEB_EX3());
                psInsert.setString(30, item.getDES_EST_WEB_EX3());
                psInsert.setString(31, item.getCOD_ID_WEB_EX4());
                psInsert.setString(32, item.getDES_ID_WEB_EX4());
                psInsert.setString(33, item.getCOD_EST_WEB_EX4());
                psInsert.setString(34, item.getDES_EST_WEB_EX4());
                psInsert.setString(35, item.getCOD_ID_WEB_EX5());
                psInsert.setString(36, item.getDES_ID_WEB_EX5());
                psInsert.setString(37, item.getCOD_EST_WEB_EX5());
                psInsert.setString(38, item.getDES_EST_WEB_EX5());
                psInsert.setString(39, item.getCOD_VTA_PPCD());
                psInsert.setTimestamp(40, fechaActualTimestamp);
                psInsert.addBatch();

            }

            int[] rowsInserted = psInsert.executeBatch();
            totalInsert = processInsertCount(totalInsert, rowsInserted);
            logger.info("numero de registros insertados: "+ totalInsert);

        } catch (SQLException e) {
            logger.error("error en conexion: " + e.getMessage());
            throw new SyncException(e);
        }

        logger.info("finalizados registro en tdp_automatizador");

        return totalInsert;

    }

    private int processInsertCount(int currentCount, int[] insertResults) {
        int count = currentCount;
        for (int insertResult : insertResults) {
            switch (insertResult) {
                case Statement.SUCCESS_NO_INFO:
                    count += 1;
                    break;
                case Statement.EXECUTE_FAILED:
                    break;
                default:
                    count += insertResult;
                    break;
            }
        }
        return count;
    }

}