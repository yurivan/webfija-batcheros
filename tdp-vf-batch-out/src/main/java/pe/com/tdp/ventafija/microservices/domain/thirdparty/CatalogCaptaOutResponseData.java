package pe.com.tdp.ventafija.microservices.domain.thirdparty;

public class CatalogCaptaOutResponseData {
    private String id;
    private String cod_accion;
    private String cod_prod_oferta;
    private String tx_nombre;
    private String numero_telefono;
    private String tipo_documento;
    private String numero_documento;
    private String departamento;
    private String provincia;
    private String distrito;
    private String tx_direccion;
    private String ps_oferta2;
    private String tipo_archivo;
    private String fecha;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCod_accion() {
        return cod_accion;
    }

    public void setCod_accion(String cod_accion) {
        this.cod_accion = cod_accion;
    }

    public String getCod_prod_oferta() {
        return cod_prod_oferta;
    }

    public void setCod_prod_oferta(String cod_prod_oferta) {
        this.cod_prod_oferta = cod_prod_oferta;
    }

    public String getTx_nombre() {
        return tx_nombre;
    }

    public void setTx_nombre(String tx_nombre) {
        this.tx_nombre = tx_nombre;
    }

    public String getNumero_telefono() {
        return numero_telefono;
    }

    public void setNumero_telefono(String numero_telefono) {
        this.numero_telefono = numero_telefono;
    }

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getNumero_documento() {
        return numero_documento;
    }

    public void setNumero_documento(String numero_documento) {
        this.numero_documento = numero_documento;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getTx_direccion() {
        return tx_direccion;
    }

    public void setTx_direccion(String tx_direccion) {
        this.tx_direccion = tx_direccion;
    }

    public String getPs_oferta2() {
        return ps_oferta2;
    }

    public void setPs_oferta2(String ps_oferta2) {
        this.ps_oferta2 = ps_oferta2;
    }

    public String getTipo_archivo() {
        return tipo_archivo;
    }

    public void setTipo_archivo(String tipo_archivo) {
        this.tipo_archivo = tipo_archivo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
