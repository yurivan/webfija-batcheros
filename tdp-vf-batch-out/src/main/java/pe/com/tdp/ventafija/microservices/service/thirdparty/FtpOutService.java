package pe.com.tdp.ventafija.microservices.service.thirdparty;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.jcraft.jsch.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.OutputStream;

@Service
public class FtpOutService {
	private static final Logger logger = LogManager.getLogger(FtpOutService.class);

	@Value("${ftp.api.connection.hostname}")
	private String hostName;
	@Value("${ftp.api.connection.username}")
	private String username;
	@Value("${ftp.api.connection.password}")
	private String password;
	@Value("${ftp.api.connection.proxy}")
	private String proxy;

	private static String proxyStatic;

	private Session session;
	private Channel channel;
	private ChannelSftp c;



	public void moveFile (String srcDir, String fileName, String destDir, String newFileName) {
		logger.info("cambiando nombre de archivo");

		logger.info("ruta archivo procesado: "+srcDir+fileName);
		logger.info("nuevo nombre y ruta del archivo procesado: "+destDir+newFileName);
		try {

			if(c.isClosed()){
				listFiles(OutService.remoteDirectoryOutStatic);
			}

			c.rename("."+srcDir+fileName, "."+destDir+newFileName);
			logger.info("archivo movido y renombrado con exito ..........");
		} catch (Exception e) {
			logger.error("error: " + e.getMessage());
		}

		logger.info("finalizado cambiando nombre de archivo");
	}



	/**
	 * Lists the files with in a folder
	 * @param dir to list from
	 * @return a list of strings with the names of the files found
	 */
	public List<String> listFiles(String dir) {
		logger.info("Listando archivos ...");
		List<String> files = new ArrayList<>();
		java.util.Date ahora = new java.util.Date();
		try {
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			JSch jsch=new JSch();


			logger.info("obteniendo sesion sftp...");
			//int port = 8563;
			int port = 22;
			////////////////////////////////////////
			/*String username = "movistarsftp";
			String password = "movistarsftp";
			String hostName = "181.224.229.90";*/
			///////////////////////////////////////
			session=jsch.getSession(username, hostName, port);
			session.setPassword(password);
			session.setConfig(config);

			logger.info("username:" + username);
			logger.info("hostName:" + hostName);
			logger.info("password:" + password );
			logger.info("port:" + port);

			proxyStatic = proxy;

			Proxy proxy = getSockProxy();
			if (proxy ==null){
				logger.error("REPORT: Statica proxyError");
				return null;
			}
			// Comentar pues no entraaaaa con proxy
			session.setProxy(proxy);

			session.connect();

			channel=session.openChannel("sftp");
			channel.connect();
			logger.info("Se conecto al sftp");

			c=(ChannelSftp)channel;

			String initialPath = c.pwd();

			c.cd("."+dir);
			//c.cd(dir);

			logger.info("listando archivos de ."+dir);
			Vector<ChannelSftp.LsEntry> list = c.ls(".");

			for(ChannelSftp.LsEntry entry : list) {
				logger.info(entry.getFilename());
				files.add(entry.getFilename());
			}

			logger.info("se termino el listar archivos de ."+dir);

			c.cd(initialPath);

			logger.info("directorio actual: "+c.pwd());

		} catch (Exception e) {
			logger.error("Error de listado de archivos: " + e.getMessage());
		}
		logger.info("finalizado listando archivos desde "+ahora+" hasta "+new java.util.Date());

		return files;
	}


	private static Proxy getSockProxy() {
		logger.info("procesando SOCKS5 ..........");
		ProxySOCKS5 proxyTunnel = null;
		//String staticaUrl = "socks5://statica4359:eb95a2835f1e5907@sl-ams-01-guido.statica.io:1080";

		String staticaUrl = proxyStatic;

		if (staticaUrl!=null) {
			try {
				// OBTENER LOS DATOS DE HOST, PORT, USER Y PASSWORD
				//URL proxyUrl = new URL(staticaUrl);
				String authString = staticaUrl.substring(9, staticaUrl.indexOf("@"));
				String user = authString.split(":")[0];
				String password = authString.split(":")[1];
				String hostAndPort = staticaUrl.substring(staticaUrl.indexOf("@")+1);
				String host = hostAndPort.split(":")[0];
				int port = Integer.valueOf(hostAndPort.split(":")[1]);
				logger.info("authString: " + authString);
				logger.info("user: " + user);
				logger.info("password: " + password);
				logger.info("hostAndPort: " + hostAndPort );
				logger.info("host: " +host);

				//proxy = new ProxySOCKS5(host, port);
				//proxy.setUserPasswd(user, password);

				proxyTunnel = new ProxySOCKS5(host,port);
				proxyTunnel.setUserPasswd(user, password);
				//session.setProxy(proxyTunnel);

			} catch (Exception e) {
				logger.fatal("REPORT: " + e.getMessage());
				e.printStackTrace();
			}

		} else {
			logger.error("REPORT: Url Statica no Configurada");
		}
		return proxyTunnel;
	}

	public File retrieveFile (String remoteFilePath) throws IOException {
		logger.info("obtener archivo");
		File f = File.createTempFile("ftpFile"+new Date().getTime(), ".tmp");

		String fileURI=f.getAbsolutePath();
		logger.info("archivo temporal.......  "+fileURI);

		OutputStream output = new FileOutputStream(
				fileURI);
		try {

			c.get(remoteFilePath,output);

			logger.info("File download success!");
		} catch (Exception e) {
			logger.info("Error en SFTP: " + e.getMessage());

		} finally {
			output.close();

		}
		logger.info("finalizado obtener archivo");
		logger.info("tamaño del archivo: "+ f.length() + " bytes");
		return f;
	}


}
