package pe.com.tdp.ventafija.microservices.common.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "tdp_service_pedido", schema = "ibmx_a07e6d02edaf552")
public class TdpServicePedido {

    @Column(name = "pedido_type")
    private String pedidoType;
    @Id
    @Column(name = "pedido_codigo")
    private String pedidoCodigo;
    @Column(name = "pedido_estado_code")
    private String pedidoEstadoCode;
    @Column(name = "pedido_estado")
    private String pedidoEstado;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "auditoria_create")
    private Date auditoriaCreate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "GMT-5")
    @Column(name = "auditoria_modify")
    private Date auditoriaModify;

    public String getPedidoType() {
        return pedidoType;
    }

    public void setPedidoType(String pedidoType) {
        this.pedidoType = pedidoType;
    }

    public String getPedidoCodigo() {
        return pedidoCodigo;
    }

    public void setPedidoCodigo(String pedidoCodigo) {
        this.pedidoCodigo = pedidoCodigo;
    }

    public String getPedidoEstadoCode() {
        return pedidoEstadoCode;
    }

    public void setPedidoEstadoCode(String pedidoEstadoCode) {
        this.pedidoEstadoCode = pedidoEstadoCode;
    }

    public String getPedidoEstado() {
        return pedidoEstado;
    }

    public void setPedidoEstado(String pedidoEstado) {
        this.pedidoEstado = pedidoEstado;
    }

    public Date getAuditoriaCreate() {
        return auditoriaCreate;
    }

    public void setAuditoriaCreate(Date auditoriaCreate) {
        this.auditoriaCreate = auditoriaCreate;
    }

    public Date getAuditoriaModify() {
        return auditoriaModify;
    }

    public void setAuditoriaModify(Date auditoriaModify) {
        this.auditoriaModify = auditoriaModify;
    }
}
