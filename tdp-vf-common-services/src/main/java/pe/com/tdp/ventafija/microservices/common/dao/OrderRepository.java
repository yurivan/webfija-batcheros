package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Order;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, String>, OrderRepositoryCustom {
	
    //Order findOneById(String id);
    @Modifying
    @Query("update Order o set o.whatsapp = ?1 where o.id = ?2")
    void updateWhatsapp(Integer whatsapp, String id);

	@Query("select o.id from Order o where o.id = ?1 and o.status = ?2")
	String loadId(String id, String statusPendiente);
	
	@Query("select o.id from Order o where o.id = ?1")
	String loadId(String id);

	@Modifying
	@Query("update Order o set o.status = ?1, o.cancelDescription = ?2, o.lastUpdateTime = ?3 where o.id = ?4")
	void updateStatus(String status, String cancelDescription, Date lastUpdateTime, String id);

	@Modifying
	@Query("update Order o set o.status = ?1, o.lastUpdateTime = ?2 where o.id = ?3")
	void updateStatus(String status, Date lastUpdateTime, String id);

	@Modifying
	@Query("update Order o set o.status = ?1, o.lastUpdateTime = ?2 where o.id = ?3")
	void updateStatusAnterior(String status, Date lastUpdateTime, String id);


	@Modifying
	@Query("update Order o set o.lastUpdateTime = ?1 where o.id = ?2")
	void updateOrder(Date lastUpdateTime, String id);
	
	@Modifying
	@Query("update Order o set o.watsonRequest = ?1, o.lastUpdateTime = ?2 where o.id = ?3")
	void updateOrder(String watsonRequest, Date lastUpdateTime, String id);

/*		
	@Modifying
	@Query("update Order o set o.cancelDescription = ?1, o.lastUpdateTime = ?2 where o.id = ?3")
	void updateOrder(String cancelDescription, Date lastUpdateTime, String id);
*/		
	List<Order> findByRegistrationDateAfterAndStatus(Date from, String status);


    @Query("select o from Order o where o.id = ?1")
    Order findByFileOrder(String id);

	@Query("select o.status from Order o where o.id = ?1")
	String loadStatusById(String orderId);

    @Query("select o.statusAudio from Order o where o.id = ?1")
    String loadStatusAudioById(String orderId);

    @Modifying
    @Query("update Order o set o.statusAudio = ?1, o.lastUpdateTime = now() where o.id = ?2")
    void updateStatusAudioOrder(String statusAudio, String id);

	@Modifying
	@Query("update Order o set o.recuperoVenta = '1' where o.id = ?1")
	void updateFlagRecuperoOrder(String id);

    /*@Modifying
    @Query("update ibmx_a07e6d02edaf552.tdp_order o set o.address_principal = ?1, o.address_referencia = ?2 where o.order_id = ?3")
    void updateAddressTdpOrder(String address, String referencia, String id);*/


	@Query(value = "select distinct v.id_visor, v.cliente, v.dni, v.codigo_pedido, v.nombre_producto," +
			" CASE " +
            " WHEN v.estado_solicitud = 'PENDIENTE' THEN 'SOLICITADO' " +
            " WHEN v.estado_solicitud = 'PENDIENTE-PAGO' THEN 'SOLICITADO' " +
            " WHEN v.estado_solicitud = 'GENERANDO_CIP' THEN 'SOLICITADO' " +
            " WHEN (v.estado_solicitud = 'ENVIANDO' or v.estado_solicitud = 'ENVIANDO_AUTOMATIZADOR') THEN 'SOLICITADO'  " +
            " WHEN v.estado_solicitud = '' THEN 'SOLICITADO' " +
            " WHEN v.estado_solicitud = 'EN PROCESO' THEN 'SOLICITADO'	" +
            " WHEN v.estado_solicitud = 'INGRESADO' THEN 'INGRESADO'	" +
            " WHEN v.estado_solicitud = 'SEGUIMIENTO' THEN 'INGRESADO' " +
            " WHEN v.estado_solicitud = 'CAIDA' THEN 'CAIDA' " +
			" WHEN (v.estado_solicitud = 'OFFLINE' or v.estado_solicitud = 'OFFLINE_AUTOMATIZADOR') THEN 'OFFLINE' " +
			" END estado, " +
			" v.id_transaccion, o.user_atis," +
			" to_char(trunc(extract(year from v.fecha_grabacion)),'FM9999') as anio , " +
			" to_char(trunc(extract(month from v.fecha_grabacion)),'FM99') as mes, " +
			" to_char(trunc(extract(day from v.fecha_grabacion)),'FM99') as dia, " +
			" to_char(v.fecha_grabacion, 'HH12:MI AM') as hora, " +
			" v.motivo_estado, "+
			" cus.customerphone, "+
			" us.phone as telefonoAsignado, "+
			" v.direccion, " +
			" o.product_precio_normal, " +
			" o.product_precio_promo, " +
			" o.order_operation_commercial, " +
			" o.product_pay_method, " +
			" o.product_pay_price, " +
			" o.product_precio_promo_mes, " +
			" o.affiliation_electronic_invoice, " +
			" o.disaffiliation_white_pages_guide, " +
            " to_char(v.fecha_grabacion, 'DD/MM/YYYY HH12:MI:SS AM') as f_venta, " +
            " o.product_tec_inter," +
            " o.product_tec_tv," +
			" od.decossd as deco_smart, " +
			" od.decoshd as deco_hd, " +
			" od.decosdvr as deco_dvr, " +
			" ppe.codpago as codigocip, " +
			" ppe.status as estadocip, " +
			" ord.linetype as tipolinea, "+
			" od.automaticdebit as debitoAutomatico, "+
			" od.dataprotection as tratamientoDatos, "+
			" od.parentalprotection as webParental, "+
			" od.enabledigitalinvoice as reciboElect," +
			" ord.offline, "+
			" o.client_telefono_2, "+
//			" toa.xa_number_work_order, "+
			" (SELECT " +
			"xa_number_work_order " +
			"FROM toa_status toa " +
			"WHERE status_toa = 'WO_INIT' AND toa.peticion = v.codigo_pedido LIMIT 1) as xa_number_work_order, "+
			" o.sva_nombre_1, "+
			" o.sva_cantidad_1, "+
			" o.sva_nombre_2, "+
			" o.sva_cantidad_2, "+
			" o.sva_nombre_3, "+
			" o.sva_cantidad_3, "+
			" o.sva_nombre_4, "+
			" o.sva_cantidad_4, "+
			" o.sva_nombre_5, "+
			" o.sva_cantidad_5, "+
			" o.sva_nombre_6, "+
			" o.sva_cantidad_6, "+
			" o.sva_nombre_7, "+
			" o.sva_cantidad_7, "+
			" o.sva_nombre_8, "+
			" o.sva_cantidad_8, "+
			" o.sva_nombre_9, "+
			" o.sva_cantidad_9, "+
			" o.sva_nombre_10, "+
			" o.sva_cantidad_10," +
			" statusaudio "+
			" from ibmx_a07e6d02edaf552.tdp_visor v " +
			" join ibmx_a07e6d02edaf552.tdp_order o on v.id_visor = o.order_id " +
//			" left join ibmx_a07e6d02edaf552.toa_status toa on toa.peticion = v.codigo_pedido " +
			" left join ibmx_a07e6d02edaf552.order ord on ord.id = o.order_id " +
			" left join ibmx_a07e6d02edaf552.customer cus on cus.docnumber=o.client_numero_doc " +
			" left join ibmx_a07e6d02edaf552.order_detail od on o.order_id = od.orderid" +
			" left join ibmx_a07e6d02edaf552.peticion_pago_efectivo ppe on ppe.order_id=o.order_id " +
			" left join ibmx_a07e6d02edaf552.user us on us.id=ord.userid " +
			" where o.user_atis = :codigo AND cast(v.fecha_de_llamada as date) BETWEEN :fechaIni AND :fechaFin " +
            " and not v.estado_solicitud in('TÉCNICO ASIGNADO','SINFINALIZAR','SINFINALIZAR-OFFLINE') ", nativeQuery = true)
	List<Object[]> getReport(@Param("codigo") final String codigo,
							 @Param("fechaIni") final Timestamp fechaIni,
							 @Param("fechaFin") final Timestamp fechaFin);
//caida registro tipo caida


	@Query(value = "select element, strvalue, strfilter" +
			" from ibmx_a07e6d02edaf552.parameters " +
			" where  domain = 'REPORTCOLOR' AND category = 'ESTADO' order by auxiliar asc", nativeQuery = true)
	List<Object[]> getColoresEstado();

	@Query(value = "select element, strvalue" +
			" from ibmx_a07e6d02edaf552.parameters " +
			" where  domain = 'REPORTCOLOR' AND category = 'MES'", nativeQuery = true)
	List<Object[]> getColoresMes();

	@Query(value = "select peticion, status_toa" +
			" from ibmx_a07e6d02edaf552.toa_status " +
			" where  status_toa IN ('IN_TOA','WO_COMPLETED') AND peticion IN :codeList", nativeQuery = true)
	List<Object[]> getPedidosEstadosToa(@Param("codeList") List<String> codeList);

	@Query(value = "select codigo_pedido, estado, tipo_caida" +
			" from ibmx_a07e6d02edaf552.pedidos_en_caida " +
			" where   codigo_pedido IN :codeList AND (estado = true OR estado IS null)  order by id ", nativeQuery = true)
	List<Object[]> getPedidosEnCaida(@Param("codeList") List<String> codeList);

	@Query(value =
			"SELECT 'FILTRO' as filtro, 'F_CANAL' as code, '' as label WHERE 'C' = 'S' " +
			"UNION " +
			"SELECT 'FILTRO' as filtro, 'F_SOCIO' as code, '' as label WHERE 'C' = 'C' " +
			"UNION " +
			"SELECT 'FILTRO' as filtro,  'F_CAMPANIA' as code, '' as label WHERE 'C' = 'C' " +
			"UNION " +
			"SELECT 'FILTRO' as filtro,  'F_OPERACIONCOMERCIAL' as code, '' as label WHERE 'C' = 'C' " +
			"UNION " +
			"SELECT 'FILTRO' as filtro,  'F_ZONA' as code, '' as label " +
			"UNION " +
			"SELECT 'FILTRO' as filtro,  'F_ZONAL' as code, '' as label " +
			"UNION " +
			"SELECT 'FILTRO' as filtro,  'F_PUNTOVENTA' as code, '' as label " +
			"UNION " +
			"SELECT DISTINCT 'F_CANAL' as filtro, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.canal) as code, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.canal) as label " +
			"FROM ibmx_a07e6d02edaf552.tdp_sales_agent  " +
			"WHERE :reportTypeId = 'S' " +
			"UNION " +
			"SELECT DISTINCT 'F_SOCIO' as filtro, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.codatis) as code, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.nombre||' '|| ibmx_a07e6d02edaf552.tdp_sales_agent.apepaterno||' '|| ibmx_a07e6d02edaf552.tdp_sales_agent.apematerno) as label " +
			"FROM ibmx_a07e6d02edaf552.tdp_sales_agent  " +
			"WHERE :reportTypeId = 'C' " +
			"UNION " +
			"SELECT DISTINCT 'F_CAMPANIA' as filtro, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.product_campaing) as code, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.product_campaing) as label " +
			"FROM ibmx_a07e6d02edaf552.tdp_order " +
			"WHERE :reportTypeId = 'C' " +
			"UNION " +
			"SELECT DISTINCT 'F_OPERACIONCOMERCIAL' as filtro, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.order_operation_commercial) as code, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.order_operation_commercial) as label " +
			"FROM ibmx_a07e6d02edaf552.tdp_order " +
			"WHERE :reportTypeId = 'C' " +
			"UNION " +
			"SELECT DISTINCT 'F_ZONA' as filtro, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.zona) as code, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.zona) as label " +
			"FROM ibmx_a07e6d02edaf552.tdp_sales_agent  " +
			"UNION " +
			"SELECT DISTINCT 'F_ZONAL' as filtro, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.zonal) as code, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.zonal) as label " +
			"FROM ibmx_a07e6d02edaf552.tdp_sales_agent " +
			"UNION " +
			"SELECT DISTINCT 'F_PUNTOVENTA' as filtro, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.codpuntoventa) as code, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_sales_agent.nompuntoventa) as label " +
			"FROM ibmx_a07e6d02edaf552.tdp_sales_agent " +
			"ORDER BY 1", nativeQuery = true)
	List<Object[]> getFiltrosReporte(@Param("reportTypeId") final String reportTypeId);

	@Query(value = "SELECT " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.order_operation_commercial) as operation_commercial, " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) as estado, " +
			"count(ibmx_a07e6d02edaf552.tdp_visor.id_visor) as qty " +
			"FROM ibmx_a07e6d02edaf552.tdp_visor " +
			"JOIN ibmx_a07e6d02edaf552.tdp_order on ibmx_a07e6d02edaf552.tdp_visor.id_visor = ibmx_a07e6d02edaf552.tdp_order.order_id " +
			//"WHERE " +
			//"(ibmx_a07e6d02edaf552.tdp_visor.fecha_de_llamada > :fechaDesde or :fechaDesde is NULL) " +
			//"and (ibmx_a07e6d02edaf552.tdp_visor.fecha_de_llamada < :fechaHasta or :fechaHasta is NULL) " +
			//"and (UPPER(ibmx_a07e6d02edaf552.tdp_order.order_operation_commercial) IN :oc or :oc is null) " +
			"WHERE :tipoReporte='1' " +
			"group by " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.order_operation_commercial), " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) " +

			"UNION " +
			"SELECT  " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.order_operation_commercial) as operation_commercial, " +
			"CASE WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'PENDIENTE' THEN 'SOLICITADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'PENDIENTE-PAGO' THEN 'SOLICITADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'ENVIANDO' THEN 'SOLICITADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = '' THEN 'SOLICITADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'EN PROCESO' THEN 'SOLICITADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'INGRESADO' THEN 'INGRESADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'SEGUIMIENTO' THEN 'INGRESADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'CAIDA' THEN 'CAIDA'  " +
			"END estado, " +
			"count(ibmx_a07e6d02edaf552.tdp_visor.id_visor) as qty " +
			"FROM ibmx_a07e6d02edaf552.tdp_visor " +
			"JOIN ibmx_a07e6d02edaf552.tdp_order on ibmx_a07e6d02edaf552.tdp_visor.id_visor = ibmx_a07e6d02edaf552.tdp_order.order_id " +
			"WHERE :tipoReporte= '2' " +
			"group by  " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.order_operation_commercial),  " +
			"CASE WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'PENDIENTE' THEN 'SOLICITADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'PENDIENTE-PAGO' THEN 'SOLICITADO'  " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'ENVIANDO' THEN 'SOLICITADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = '' THEN 'SOLICITADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'EN PROCESO' THEN 'SOLICITADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'INGRESADO' THEN 'INGRESADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'SEGUIMIENTO' THEN 'INGRESADO' " +
			"WHEN UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) = 'CAIDA' THEN 'CAIDA' " +
			"END " +
			"UNION " +
			"SELECT " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.product_type) as product,  " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) as estado, " +
			"count(ibmx_a07e6d02edaf552.tdp_visor.id_visor) as qty " +
			"FROM ibmx_a07e6d02edaf552.tdp_visor " +
			"JOIN ibmx_a07e6d02edaf552.tdp_order on ibmx_a07e6d02edaf552.tdp_visor.id_visor = ibmx_a07e6d02edaf552.tdp_order.order_id  \n" +
			"WHERE :tipoReporte = '3' " +
			"group by " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_order.product_type), " +
			"UPPER(ibmx_a07e6d02edaf552.tdp_visor.estado_solicitud) ", nativeQuery = true)
	List<Object[]> getDataReporte( @Param("tipoReporte") String tipoReporte
								   //@Param("fechaDesde") Timestamp fechaDesde,
								   //@Param("fechaHasta") Timestamp fechaHasta
			//,@Param("oc") List<String> oc
	);

	//Sprint 27 Offline
	@Modifying
	@Query("update Order o set o.offline = ?2 where o.id = ?1")
	void updateFlagOffline(String id,String newFlagOffline);

	//Sprint 28 Filtro de Audios Por Hora
	@Query("select (CASE WHEN ?2 = 0 THEN 'SI' WHEN (extract(epoch from (cast(?3 as timestamp) - registrationDate) / 60 ) <= ?2)" +
			" THEN 'SI' ELSE 'NO' END) from" +
			" Order where id = ?1")
	String getOrderValidaAudio(String id, Integer tiempoTotal, String fechaServidor);
}
