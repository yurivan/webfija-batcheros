package pe.com.tdp.ventafija.microservices.common.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.domain.dto.HDECResponse;
import pe.com.tdp.ventafija.microservices.common.exception.ApiClientException;
import pe.com.tdp.ventafija.microservices.common.exception.ApplicationException;
import pe.com.tdp.ventafija.microservices.common.services.HDECRetryService;
import pe.com.tdp.ventafija.microservices.common.services.HDECService;
import pe.com.tdp.ventafija.microservices.common.util.HDECConstants;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Implementacion para hacer controlar las llamadas a HDEC.
 * Se maneja un limite de reintentos (configurado en base datos, no soporta cambios en caliente).
 * @author      glazaror
 * @since       1.5
 */
@Service
public class HDECRetryServiceImpl implements HDECRetryService {

    @Autowired
    private HDECService hdecService;

    @Value("${hdec.limitereintentos}")
    private Integer cantidadReintentos;

    @Value("${hdec.controlerror.codigos}")
    private String codigosErrorControlados;

    private List<String> codigosErrorControladosAsList;

    @PostConstruct
    public void postConstruct() {
        this.codigosErrorControladosAsList = new ArrayList<String>();
        String[] codigosError = codigosErrorControlados.split(",");
        for (String codigoError : codigosError) {
            codigosErrorControladosAsList.add("\"" + codigoError + "\"");
        }
    }

    @Override
    public HDECResponse sendToHDEC(String orderId) throws ApiClientException, ApplicationException, ClientException {
        HDECResponse response = null;
        try {
            hdecService.sendDirectToHDEC(orderId);
            response = HDECResponse.SATISFACTORIO;
        } catch (ClientException e) {
            // Sprint 11 - verificamos si es que el codigo de error es 500, 4002 o 4003... de ser el caso debemos actualizar el estado del visor a ENVIANDO
            String serviceResponse = e.getMessage();
            //if (serviceResponse != null && (serviceResponse.contains(HDECConstants.ERROR_4002) || serviceResponse.contains(HDECConstants.ERROR_4003) || serviceResponse.contains(HDECConstants.ERROR_500))) {
            //if (serviceResponse != null && serviceResponse.contains(HDECConstants.ERROR_4003)) {
            if (serviceResponse != null && containsErrorControlado(serviceResponse)) {
                response = HDECResponse.ERROR_CONTROLADO;
            } else {
                // en otros casos simplemente propagamos el error
                throw e;
            }
        }

        return response;
    }

    private boolean containsErrorControlado(String mensajeError) {
        for (String codigoErrorControlado : codigosErrorControladosAsList) {
            if (mensajeError.contains(codigoErrorControlado)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public HDECResponse sendToHDECWithRetry(String orderId) throws ApiClientException, ApplicationException, ClientException {
        HDECResponse response = null;
        for (int i = 0; i < cantidadReintentos; i++) {
            try {
                response = sendToHDEC(orderId);
                if (HDECResponse.SATISFACTORIO.equals(response)) {
                    // Si es que la respuesta fue satisfactoria entonces cortamos el bucle y retornamos la respuesta
                    return response;
                }
                // Si es que es un error controlado continuamos reintentando hasta el limite de reintentos

            } catch (ClientException e) {
                // Ocurrio un error no controlado... en este punto el servicio de hdec ya ha generado logs de la llamada
                // es por esto que ya no se vuelve a pintar el error
                // si es que ya se llego al limite de reintentos entonces propagamos el error
                if (i == (cantidadReintentos - 1)) {
                    throw e;
                }
            }
            // Si es que ocurre un exception general entonces eso no se controla... se rompe el bucle y propaga el error
        }
        return response;
    }
}
