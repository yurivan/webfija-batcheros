package pe.com.tdp.ventafija.microservices.common.services;

import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoResponseBody;

/**
 * Service to make api calls to tdp
 * @author jvilcayp
 *
 */
public interface ExpertoService {

	/**
	 * Makes request to Experto service on tdp
	 * @param apiExpertoRequestBody the body of the request
	 * @return the api response when obtained
	 * @throws ClientException when an error happens
	 */
	ApiResponse<OffersApiExpertoResponseBody> scoring(OffersApiExpertoRequestBody apiExpertoRequestBody, String orderId) throws ClientException;

	ApiResponse<OffersApiExpertoResponseBody> scoringRucData(String json, String orderId, String tipo);//Sprint 24 RUC
}
