package pe.com.tdp.ventafija.microservices.common.dao.impl;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.dao.CuponesRepositoryCustom;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Tdp_automatizador_cupones;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CuponesRepositoryImpl implements CuponesRepositoryCustom {

    private static final Logger logger = LogManager.getLogger();

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Tdp_automatizador_cupones> findByDomainAndCategoryOrderByAuxiliarAsc(String category) {
        List<Tdp_automatizador_cupones> data = new ArrayList<>();

        try {
            String sql = "select tap.id, tap.cupones_fecha, tap.cupones, tap.cupones_utilizados, tap.category "
                    + "from ibmx_a07e6d02edaf552.tdp_automatizador_cupones tap "
                    + "where tap.category = :category "
                    + "order by tap.id desc";

            LogVass.daoQuery(logger, sql
                    .replace(":category", "'" + category + "'"));

            Query q = em.createNativeQuery(sql);
            q.setParameter("category", category);

            List<Object[]> result = q.getResultList();
            for (Object[] row : result) {
                Tdp_automatizador_cupones tdp_automatizador_cupones = mapRowToCupones(row);

                data.add(tdp_automatizador_cupones);
            }
        } catch (Exception e) {
            logger.error("Error findOneByDomainAndCategoryAndElement DAO.", e);
        }
        return data;
    }


    @Override
    public Boolean updateCuponesUtilizados(Integer cupones_utilizados,Integer id) {
        Integer cupones_utilizar = cupones_utilizados + 1;

       /*Query dmlUpdate = em.createNativeQuery("UPDATE ibmx_a07e6d02edaf552.tdp_automatizador_cupones SET cupones_utilizados = ?1 WHERE id = ?2");

        dmlUpdate.setParameter(1, cupones_utilizar);
        dmlUpdate.setParameter(2, id);
        dmlUpdate.executeUpdate();
        */
        Boolean response = false;
        try (Connection con = Database.datasource().getConnection()) {
            String update = "UPDATE ibmx_a07e6d02edaf552.tdp_automatizador_cupones " +
                    "SET cupones_utilizados=? " +
                    "WHERE id=?";
            try (PreparedStatement ps = con.prepareStatement(update)) {
                ps.setInt(1, cupones_utilizar);
                ps.setInt(2, id);
                int n = ps.executeUpdate();
                if (n > 0) {
                    response = true;
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return response;
    }


    private Tdp_automatizador_cupones mapRowToCupones(Object[] row) {
        Integer id = (Integer) row[0];
        Timestamp cupones_fecha = (Timestamp) row[1];
        Integer cupones = (Integer) row[2];
        Integer cupones_utilizados = (Integer) row[3];
        String category = (String) row[4];


        Tdp_automatizador_cupones tdp_automatizador_cupones = new Tdp_automatizador_cupones();
        tdp_automatizador_cupones.setId(id);
        tdp_automatizador_cupones.setCupones_fecha(cupones_fecha);
        tdp_automatizador_cupones.setCupones(cupones);
        tdp_automatizador_cupones.setCupones_utilizados(cupones_utilizados);
        tdp_automatizador_cupones.setCategory(category);

        return tdp_automatizador_cupones;
    }





}
