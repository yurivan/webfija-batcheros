package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Tokens;

import java.util.List;

@Repository
public interface TokensRepository extends JpaRepository<Tokens, Integer> {

    //@Modifying
    @Query( value = "SELECT * FROM convtokens WHERE token = ?1",nativeQuery = true)
    Tokens findToken(String token);

}
