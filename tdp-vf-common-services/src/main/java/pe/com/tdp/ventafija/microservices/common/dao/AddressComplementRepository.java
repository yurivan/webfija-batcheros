package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.AddressComplement;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;

import java.util.List;

@Repository
public interface AddressComplementRepository extends JpaRepository<AddressComplement, Integer> {



}
