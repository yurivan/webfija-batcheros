package pe.com.tdp.ventafija.microservices.common.domain.entity;

public class AgenVal {

    private String tecnologiaVal;
    private String canalVal;
    private String departamentoVal;

    public String getTecnologiaVal() {
        return tecnologiaVal;
    }

    public void setTecnologiaVal(String tecnologiaVal) {
        this.tecnologiaVal = tecnologiaVal;
    }

    public String getCanalVal() {
        return canalVal;
    }

    public void setCanalVal(String canalVal) {
        this.canalVal = canalVal;
    }

    public String getDepartamentoVal() {
        return departamentoVal;
    }

    public void setDepartamentoVal(String departamentoVal) {
        this.departamentoVal = departamentoVal;
    }
}
