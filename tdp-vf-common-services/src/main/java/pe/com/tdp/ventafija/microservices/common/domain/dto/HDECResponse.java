package pe.com.tdp.ventafija.microservices.common.domain.dto;

public enum HDECResponse {
    SATISFACTORIO,
    ERROR_CONTROLADO,
    ERROR_GENERAL;
}
