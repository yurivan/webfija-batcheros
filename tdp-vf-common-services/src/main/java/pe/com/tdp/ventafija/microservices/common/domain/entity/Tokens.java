package pe.com.tdp.ventafija.microservices.common.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "convtokens", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema
public class Tokens {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "token")
    private String token;
    @Column(name = "created_at")
    private String created_at;
    @Column(name = "used_at")
    private String used_at;
    @Column(name = "expire_at")
    private String expire_at;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUsed_at() {
        return used_at;
    }

    public void setUsed_at(String used_at) {
        this.used_at = used_at;
    }

    public String getExpire_at() {
        return expire_at;
    }

    public void setExpire_at(String expire_at) {
        this.expire_at = expire_at;
    }
}
