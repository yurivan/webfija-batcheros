package pe.com.tdp.ventafija.microservices.common.dao;

import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;

public interface ParametersRepositoryCustom {

    Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element);

}
