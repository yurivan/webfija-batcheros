package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Argumentarios;

import java.util.List;

@Repository
public interface ArgumentariosRepository extends JpaRepository<Argumentarios, Integer>, ArgumentariosRepositoryCustom {

	//Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element);

	List<Argumentarios> findAll();

	List<Argumentarios> findByCanal(String canal);
	List<Argumentarios> findByNivel(String nivel);

}
