package pe.com.tdp.ventafija.microservices.common.web.context;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import pe.com.tdp.ventafija.microservices.common.util.LoginUtil;

public class VentaFijaTokenValidatorFilter implements Filter {
	private static final Logger logger = LogManager.getLogger();
	
	private List<String> securedUris = new ArrayList<String>(){{
		add("/login/token");
	    add("/user/web/reset");
	    add("/user/login");
		add("/user/web/register");
		add("/product/numdecos");
		add("/user/web/changePassword");
		add("/user/web/desencriptarToken");
		add("/user/web/saveLog");
	}};

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		logger.info("---->>> uri: " + httpRequest.getRequestURI() + " - method: " + httpRequest.getMethod());
		if (isSecuredUri(httpRequest.getRequestURI())) {
			//si es que la uri no esta securizada entonces invocamos directamente el metodo
			chain.doFilter(request, response);
		} else {
			if (!"OPTIONS".equals(httpRequest.getMethod())) {
				logger.info("---->>> header X_AUTH: " + httpRequest.getHeader("X_AUTH"));
				logger.info("---->>> header Authorization: " + httpRequest.getHeader("Authorization"));
				String byPassMovil = httpRequest.getHeader("X_AUTH");
				if ("xxx".equals(byPassMovil)) {
					//bypass
					chain.doFilter(request, response);
					
				} else {
				
					//si es que la uri esta securizada entonces validamos que el token sea valido
					Map<String, Object> datosUsuario = LoginUtil.obtenerUsuario(httpRequest.getHeader("Authorization"));
					if (datosUsuario != null) {
						//si es que se obtienen correctamente los datos de usuario entonces ejecutamos el metodo
						chain.doFilter(request, response);
					} else {
						//si es que no se obtienen datos de usuario entonces rechazamos
						HttpServletResponse httpResponse = (HttpServletResponse) response;
						httpResponse.sendError(403);
					}
				}
			} else {
				chain.doFilter(request, response);
			}
		}
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	private boolean isSecuredUri(String requestURI) {
		for (String uri : securedUris) {
			if (uri.startsWith(requestURI)) {
				return true;
			}
		}
		return false;
	}

}
