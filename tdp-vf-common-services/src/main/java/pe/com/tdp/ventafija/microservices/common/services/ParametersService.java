package pe.com.tdp.ventafija.microservices.common.services;

import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;

public interface ParametersService {

    Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element);

    String equivalenceDocument(String appiConnect, String documentType);

}