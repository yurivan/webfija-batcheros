package pe.com.tdp.ventafija.microservices.common.services;

import pe.com.tdp.ventafija.microservices.common.domain.entity.TdpServiceClient;
import pe.com.tdp.ventafija.microservices.common.domain.entity.TdpServicePedido;

import java.util.List;

public interface LegadoService {

    TdpServicePedido getEstadoCms(String codigoPedido);

    String getEstadoCmsCodeWithTry(String codigoPedido);

    TdpServicePedido getEstadoAtis(String codigoPedido);

    String getEstadoAtisCodeWithTry(String codigoPedido);

    List<TdpServiceClient> getParqueCms(String documentType, String documentNumber, String orderId);

    List<TdpServiceClient> getParqueAtis(String documentType, String documentNumber, String orderId);

}
