package pe.com.tdp.ventafija.microservices.common.dao.impl;

import ch.qos.logback.core.net.SyslogOutputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.connection.Database;
import pe.com.tdp.ventafija.microservices.common.dao.OrderRepositoryCustom;
import pe.com.tdp.ventafija.microservices.common.domain.dto.OrderUpfrontBean;
import pe.com.tdp.ventafija.microservices.common.domain.dto.SaleFileReport;
import pe.com.tdp.ventafija.microservices.common.domain.dto.SalesReport;
import pe.com.tdp.ventafija.microservices.common.domain.dto.UserOrderResponse;
import pe.com.tdp.ventafija.microservices.common.domain.entity.AgenVal;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.util.DateUtils;
import pe.com.tdp.ventafija.microservices.common.util.LogVass;
import pe.com.tdp.ventafija.microservices.common.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class OrderRepositoryImpl implements OrderRepositoryCustom {
    private static final Logger logger = LogManager.getLogger();
    private static final String ESTADO_CAIDA = "caida";
    private static final String ESTADO_SEGUIMIENTO = "seguimiento";
    private static final String ESTADO_CAIDA_APP = "caida app";
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Object[]> spRetriveHdecData(String orderId) {
        return em.createNamedQuery("spRetriveHdecData")
                .setParameter(1, orderId).getResultList();
    }
    
    // Sprint 4 - ahora el estado puede ser PENDIENTE o PENDIENTE-PAGO
    @Override
    public void spMigrateVisor(String orderId) {
    	//por defecto insertamos el registro con el estado PENDIENTE
    	this.spMigrateVisor(orderId, "PENDIENTE");
    }

    @Override
    public void spMigrateVisor(String orderId, String status) {
        Query dmlInsert = em.createNativeQuery(
                " INSERT INTO ibmx_a07e6d02edaf552.tdp_visor (id_visor, fecha_grabacion, cliente, telefono, direccion,"
                        + " departamento, dni, nombre_producto, estado_solicitud, operacion_comercial, "
                        + " distrito, sub_producto, tipo_producto, tipo_documento, provincia, fecha_de_llamada,"
                        + " version_fila_t, producto, version_fila, nombre_producto_fuente, fecha_solicitado, telefono2, correo)"
                        + " SELECT DISTINCT o.id, o.registrationDate, CONCAT(c.firstName,' ',c.lastName1,' ',c.lastName2),"
                        + " c.customerPhone, SUBSTRING(TRIM(CONCAT(od.address, ' - ', od.addressComplement)), 1, 200), od.department, c.docNumber,"
                        // Sprint 3 - Ahora utilizamos un estado previo a PENDIENTE
                        + " o.productName, '" + status + "',"
                        //+ " o.productName, 'PENDIENTE-AUDIO',"//Para no afectar otros procesos dejamos en estado 'PENDIENTE'
                        + " o.commercialOperation, od.district,"
                        + " o.productCategory, o.productType,"
                        + " c.docType, od.province, o.registrationDate,"
                        + " to_char(o.registrationDate, 'YYYYMMDDHH24MM'),"
                        + " o.productCode, 0,"
                        // Sprint 9 - Traslado del nombre del producto al cual pertenece el pedido SVA (Solo es NOT NULL en flujo SVA)
                        + " o.sourceproductname,"
                        // Sprint 9 - Fecha de SOLICITADO para trazabilidad visor
                        + " cast(:fechaSolicitado as timestamp), c.customerphone2, c.email"
                        + " FROM ibmx_a07e6d02edaf552.order o"
                        + " JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerID"
                        + " JOIN ibmx_a07e6d02edaf552.order_detail od ON od.orderID = o.id"
                        + " WHERE o.id = :orderId");

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = df.format(new Date());

        dmlInsert.setParameter("orderId", orderId);
        dmlInsert.setParameter("fechaSolicitado", strDate);
        dmlInsert.executeUpdate();


        Query dmlUpdate = em.createNativeQuery("UPDATE ibmx_a07e6d02edaf552.order SET statusLegacy = 'PENDIENTE' WHERE id = ?1");
        dmlUpdate.setParameter(1, orderId);
        dmlUpdate.executeUpdate();

		/*em.createNamedQuery("spMigrateVisor")
                .setParameter("inparam", orderId).executeUpdate();*/
    }


    @Override
    public List<SalesReport> loadReporteVentaData(Date startDate, String vendorId, String idTransaccion, String filterDni, String status) {
        List<SalesReport> data = new ArrayList<>();
        try {

                String sql = "select c.docNumber, o.productName, o.registrationDate, o.status, o.price, "
                        + "o.price - o.promPrice as discount, o.promPrice, o.monthPeriod, o.lineType, o.cashPrice, "
                        + "v.codigo_pedido, v.estado_solicitud, o.id, "
                        + "c.firstname, c.lastname1, c.lastname2, o.commercialoperation, o.cancelDescription, "
                        + "od.enabledigitalinvoice, od.disablewhitepage, v.motivo_estado, c.customerPhone, COALESCE(v.id_transaccion,'') as id_transaccion "
                        // para el reporte excel se requiere 1 campo mas: codigo vendedor, estado atis/cms y codigo cip
                        + ", o.userid, o.statusLegacy, pp.codpago "
                        + "from ibmx_a07e6d02edaf552.order o "
                        + "join ibmx_a07e6d02edaf552.customer c on c.id = o.customerID "
                        + "left join ibmx_a07e6d02edaf552.tdp_visor v on v.id_visor = o.id "
                        + "left join ibmx_a07e6d02edaf552.order_detail od on od.orderId = o.id "
                        // para el reporte excel se requiere codigo cip... tabla peticion_pago_efectivo
                        + "left join  ibmx_a07e6d02edaf552.peticion_pago_efectivo pp on pp.order_id = o.id "
                        + "where o.registrationDate > :dateFrom and o.userId = :vendorId AND v.ESTADO_SOLICITUD <> 'GENERANDO_CIP' "
                        + "";

                LogVass.daoQuery(logger, sql
                        .replace(":dateFrom", "'" + startDate + "'")
//                        .replace(":id_transaccion_", "'" + idTransaccion + "'")
                        .replace(":vendorId", "'" + vendorId + "'"));


            if (idTransaccion != null ) {
                if (!idTransaccion.equals("")) {
                    if (!idTransaccion.equals("undefined")){
                        sql += "and v.id_transaccion = :id_transaccion_ ";
                    }
                }
            }

            if (filterDni != null ) {
                if (!filterDni.equals("")) {
                    if (!filterDni.equals("undefined")){
                        sql += "and c.docNumber = :dniCliente ";
                      }
                }
            }

            if (status != null) {
                sql += " and v.estado_solicitud = :status ";
            }


                sql +=  "order by o.registrationDate desc";

                Query q = em.createNativeQuery(sql);
                q.setParameter("dateFrom", startDate);
                q.setParameter("vendorId", vendorId);

            if (idTransaccion != null ) {
                if (!idTransaccion.equals("")) {
                    if (!idTransaccion.equals("undefined")){
                        q.setParameter("id_transaccion_", idTransaccion);
                    }
                }
            }
            if (filterDni != null ) {
                if (!filterDni.equals("")) {
                    if (!filterDni.equals("undefined")){
                        q.setParameter("dniCliente", filterDni);
                    }
                }
            }

            if (status != null) {
                q.setParameter("status", status);
            }


                System.out.println(q);
                List<Object[]> result = q.getResultList();
                for (Object[] row : result) {
                    SalesReport salesReport = mapRowToSalesReport2(row);

                    data.add(salesReport);
                }

        } catch (Exception e) {
            logger.error("Error loadReporteVentaData DAO.", e);
        }
        return data;
    }

    /**
     * Sprint 3 - Consulta de ventas para la bandeja de pendientes de subir audios
     *
     * @param codigoEstado   codigo de estado de subida de audio
     * @param codigoVendedor codigo del vendedor responsable de la venta
     * @param dniCliente     dni del cliente
     * @param fechaInicio    fecha de inicio del rango de busqueda de la venta
     * @param fechaFin       fecha de fin del rango de busqueda de la venta
     * @return data found
     */
    @Override
    public List<SalesReport> loadBandejaVentasAudio(String nombrePuntoVenta, String entidad, String codigoEstado, String codigoVendedor, String buscarPorVendedor, String dniCliente, Date fechaInicio, Date fechaFin, String filtroRestringeHoras, String hour_minute) {
        List<SalesReport> data = new ArrayList<>();
        long tiempoTotal = 0;

        String array[];
        Date fechaServidor = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String fechaServConvertida = sdf.format(fechaServidor);
        Integer hora = 0,minuto = 0;

        if(filtroRestringeHoras != null && !filtroRestringeHoras.equalsIgnoreCase("")){
            try{
                array = hour_minute.split(":");
                hora = Integer.parseInt(array[0]);
                minuto = Integer.parseInt(array[1]);
                if(hora > 0 || minuto > 0){
                    tiempoTotal = TimeUnit.HOURS.toMinutes(hora) + minuto;
                }

            }catch (Exception e){
                tiempoTotal = 0;
            }
        }


        //xq no usan jpql en vez de los querys nativos???? pendiente la optimizaci�n de este c�digo
        String sql = "select c.docNumber, o.productName, o.registrationDate, o.status, o.price, "
                + "o.price - o.promPrice as discount, o.promPrice, o.monthPeriod, o.lineType, o.cashPrice, "
                + "v.codigo_pedido, v.estado_solicitud, o.id, "
                + "c.firstname, c.lastname1, c.lastname2, o.commercialoperation, o.cancelDescription, "
                + "od.enabledigitalinvoice, od.disablewhitepage, v.motivo_estado, o.statusaudio, o.userid,"
                + "case o.userid when :userid then 1 else 2 end orden "
                // Para el reporte excel se necesitan datos adicionales: celular cliente, codigo cip, id transaccion
                + ", c.customerPhone, o.statusLegacy, "
                +" pp.codpago ," +
                " v.id_transaccion, "
                + "(CASE WHEN " + tiempoTotal + "= 0 THEN 'Pendiente de Subida' WHEN (extract(epoch from (cast('"+ fechaServConvertida +"' as timestamp) - registrationDate) / 60 ) <= " + tiempoTotal + ")"
                + " THEN 'Pendiente de Subida' ELSE 'Tiempo de Subida de Audio Expirado' END) as restringehoras," +
                " o.paymentmode," +
                " pp.status as cip_status," +
                " pp.total "
                + "from ibmx_a07e6d02edaf552.order o "
                + "join ibmx_a07e6d02edaf552.customer c on c.id = o.customerID "
                + "left join ibmx_a07e6d02edaf552.tdp_visor v on v.id_visor = o.id "
                + "left join ibmx_a07e6d02edaf552.order_detail od on od.orderId = o.id "
                // Para el reporte excel se necesita codigo cip por lo tanto es necesario un left join con peticion_pago_efectivo
                + "left join  ibmx_a07e6d02edaf552.peticion_pago_efectivo pp on pp.order_id = o.id "
                + "where o.statusaudio = :statusaudio ";

        if(codigoEstado.equalsIgnoreCase("1")){
            sql += "and v.estado_solicitud IN ('PENDIENTE', 'PENDIENTE-PAGO') ";
        }else{
            sql += "and v.estado_solicitud NOT IN ('CAIDA') ";
        }

        sql += "and o.nompuntoventa = :nombrePuntoVenta and o.entidad = :entidad ";

        if ("SI".equals(buscarPorVendedor)) {
            sql += " and o.userid = :userid ";
        }

        if (dniCliente != null) {
            sql += " and c.docNumber = :dniCliente ";
        }
        if (fechaInicio != null && fechaFin != null) {
            sql += " and o.registrationDate between :dateFrom and :dateEnd ";
        }

        if(tiempoTotal > 0 && !filtroRestringeHoras.equalsIgnoreCase("EXP")){

            if(filtroRestringeHoras.equalsIgnoreCase("SI")){
                sql += " and  extract(epoch from (cast('" + fechaServConvertida + "' as TIMESTAMP) - o.registrationDate) / 60 )<= :maxTime ";
            }else{
                sql += " and extract(epoch from (cast('" + fechaServConvertida + "' as TIMESTAMP) - o.registrationDate) / 60 ) > :maxTime ";
            }

        }

        sql +=  " and o.registrationDate > (now() - INTERVAL '60 day') " +
                "order by 24, o.registrationDate desc ";

        Query q = em.createNativeQuery(sql);
        q.setParameter("statusaudio", codigoEstado);
        q.setParameter("nombrePuntoVenta", nombrePuntoVenta);
        q.setParameter("entidad", entidad);

        if (codigoVendedor != null) {
            q.setParameter("userid", codigoVendedor);
        }

        if (dniCliente != null) {
            q.setParameter("dniCliente", dniCliente);
        }
        if (fechaInicio != null && fechaFin != null) {
            q.setParameter("dateFrom", fechaInicio);
            q.setParameter("dateEnd", fechaFin);
        }
        if (tiempoTotal > 0 && !filtroRestringeHoras.equalsIgnoreCase("EXP")){
            q.setParameter("maxTime",tiempoTotal);
        }

        List<Object[]> result = q.getResultList();


        for (Object[] row : result) {
            SalesReport salesReport = mapRowToSalesBandejaVentaAudio(row);

            data.add(salesReport);
        }
        return data;
    }

    @Override
    public SalesReport loadSalesReportByOrderId(String orderId) {
        SalesReport salesReport = null;
        try {
            String sql = "select c.docNumber, o.productName, o.registrationDate, o.status, o.price, "
                    + "o.price - o.promPrice as discount, o.promPrice, o.monthPeriod, o.lineType, o.cashPrice, "
                    + "v.codigo_pedido, v.estado_solicitud, o.id, "
                    + "c.firstname, c.lastname1, c.lastname2, o.commercialoperation, o.cancelDescription, "
                    + "od.enabledigitalinvoice, od.disablewhitepage, v.motivo_estado, c.customerPhone "
                    + ", od.decossd, od.decoshd, od.decosdvr, od.tvblock, od.blocktv, od.svainternet, od.svaline "// Sprint 3 - campos de svas adicionales
                    + ", o.statusaudio, o.userid " // Sprint 3 - campo estado de audio y codigo vendedor
                    + ", o.statusLegacy,COALESCE(v.id_transaccion,'') as id_transaccion " // Sprint 4 - Estado legacy
                    + "from ibmx_a07e6d02edaf552.order o "
                    + "join ibmx_a07e6d02edaf552.customer c on c.id = o.customerID "
                    + "left join ibmx_a07e6d02edaf552.tdp_visor v on v.id_visor = o.id "
                    + "left join ibmx_a07e6d02edaf552.order_detail od on od.orderId = o.id "
                    + "where o.id = :orderId";

            LogVass.daoQuery(logger, sql.replace(":orderId", "'" + orderId + "'"));

            Query q = em.createNativeQuery(sql);
            q.setParameter("orderId", orderId);
            List<Object[]> result = q.getResultList();
            if (result != null && !result.isEmpty()) {
                salesReport = mapRowToSalesReport(result.get(0));
            }
        } catch (Exception e) {
            logger.error("Error loadSalesReportByOrderId DAO.", e);
        }
        return salesReport;
    }

    @Override
    public List<SaleFileReport> loadBandejaVentasArchivo(String codigoSupervisor,Date fechaInicio, Date fechaFin, String hour_minute) {
        List<SaleFileReport> data = new ArrayList<>();

        long tiempoTotal = 0;
        Date fechaServidor = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String fechaServConvertida = sdf.format(fechaServidor);

        try{
            String array[] = hour_minute.split(":");
            Integer hora = Integer.parseInt(array[0]);
            Integer minuto = Integer.parseInt(array[1]);
            tiempoTotal = TimeUnit.HOURS.toMinutes(hora) + minuto;

        }catch (Exception e){
            tiempoTotal = 0;
        }

        System.out.println("Hora Server --> " + fechaServConvertida + "|| Tiempo Total --> " + tiempoTotal);

        //xq no usan jpql en vez de los querys nativos???? pendiente la optimización de este código
        String sql = "select o.id, o.userid, o.statusaudio, "
                + "case o.statusaudio  when '1' then 'APROBADO' when '8' then 'PENDIENTE DE AUDITAR' end status_audio, "
                + "o.registrationDate, coalesce(o.whatsapp, 0) as whatsapp, "
                + "(CASE WHEN " + tiempoTotal + "= 0 THEN 'SI' WHEN (extract(epoch from (cast('"+ fechaServConvertida +"' as timestamp) - registrationDate) / 60 ) <= " + tiempoTotal + ")"
                + " THEN 'SI' ELSE 'NO' END) as restringeHoras "
                + "from ibmx_a07e6d02edaf552.order o "
                + "inner join ibmx_a07e6d02edaf552.tdp_sales_agent sa on (sa.entidad = o.entidad) "
                + "left join ibmx_a07e6d02edaf552.tdp_visor v on v.id_visor = o.id "
                + "where (o.statusaudio = '1' or o.statusaudio = '8') "
                + "and v.estado_solicitud <> 'CAIDA' "
                + "and sa.codatis = :codigoSupervisor "
                + "and ( (sa.nompuntoventa is not null and sa.nompuntoventa = o.nompuntoventa) or (sa.nompuntoventa is null) )";


        if (fechaInicio != null && fechaFin != null) {
            sql += " and date_trunc('day', o.registrationDate) between :dateFrom and :dateEnd ";
        }

        sql +=  " order by o.registrationDate asc";

        Query q = em.createNativeQuery(sql);
        q.setParameter("codigoSupervisor", codigoSupervisor);

        if (fechaInicio != null && fechaFin != null) {
            Date fi = DateUtils.removeTime(DateUtils.addDays(fechaInicio,1));
            Date ff = DateUtils.removeTime(DateUtils.addDays(fechaFin,1));
            q.setParameter("dateFrom", fi);
            q.setParameter("dateEnd", ff);
        }


        List<Object[]> result = q.getResultList();

        for (Object[] row : result) {
            SaleFileReport salesReport = mapRowToSalesBandejaVentasArchivo(row);

            data.add(salesReport);
        }
        return data;
    }

    private SalesReport mapRowToSalesReport(Object[] row) {
        String docNumber = (String) row[0];
        String productName = (String) row[1];
        Date registrationDate = (Date) row[2];
        String status = (String) row[3];
        BigDecimal price = (BigDecimal) row[4];
        BigDecimal discount = (BigDecimal) row[5];
        BigDecimal promPrice = (BigDecimal) row[6];
        Integer monthPeriod = (Integer) row[7];
        String lineType = (String) row[8];
        BigDecimal cashPrice = (BigDecimal) row[9];
        String codigoPedido = (String) row[10];
        String estadoSolicitud = (String) row[11];
        String orderId = (String) row[12];

        String clientName = (String) row[13];
        String clientLastName1 = (String) row[14];
        String clientLastName2 = (String) row[15];
        String commercialOperation = (String) row[16];
        String cancelDescription = (String) row[17];
        String enableDigitalInvoice = (String) row[18];
        String disableWhitePage = (String) row[19];
        String motivoEstado = (String) row[20];
        String customerPhone = (String) row[21];

        SalesReport salesReport = new SalesReport();
        salesReport.setDni(docNumber);
        salesReport.setProductName(productName);
        salesReport.setSaleDate(registrationDate);
        if (estadoSolicitud != null) {
            salesReport.setStatus(estadoSolicitud);
        } else {
            salesReport.setStatus(ESTADO_CAIDA_APP.toUpperCase());
        }
        salesReport.setPrice(price);
        salesReport.setDiscount(discount);
        salesReport.setPromPrice(promPrice);
        salesReport.setMonthPeriod(monthPeriod);
        salesReport.setLineType(lineType);
        salesReport.setCashPrice(cashPrice);
        salesReport.setOrderId(orderId);

        salesReport.setRequestCode(codigoPedido);
        salesReport.setStatusRequest(estadoSolicitud);

        salesReport.setClientName(String.format("%s %s %s", StringUtils.emptyWhenNull(clientName),
                StringUtils.emptyWhenNull(clientLastName1), StringUtils.emptyWhenNull(clientLastName2)));
        salesReport.setCommercialOperation(commercialOperation);
        salesReport.setCancelDescription(cancelDescription);
        salesReport.setEnableDigitalInvoice(enableDigitalInvoice);
        salesReport.setDisableWhitePage(disableWhitePage);

        if (ESTADO_CAIDA.equalsIgnoreCase(estadoSolicitud) || ESTADO_SEGUIMIENTO.equalsIgnoreCase(estadoSolicitud)) {
            salesReport.setCancelDescription(motivoEstado);
        }

        salesReport.setCustomerPhone(customerPhone);

        //sprint 3 - campos para svas adicionales y statusaudio - inicio modificaciones
        //+ ", od.decossd, od.decoshd, od.decosdvr, od.tvblock, od.blocktv, od.svainternet, od.svaline, o.statusaudio"//campos de svas adicionales
        if (row.length > 22) {
            Integer decosSD = (Integer) row[22];
            Integer decosHD = (Integer) row[23];
            Integer decosDVR = (Integer) row[24];
            String tvBlock = (String) row[25];
            String blockTV = (String) row[26];
            String svaInternet = (String) row[27];
            String svaLine = (String) row[28];
            String statusAudio = (String) row[29];
            String codigoVendedor = (String) row[30];

            String id_transaccion = (String) row[32];
            salesReport.setId_transaccion(id_transaccion);

            if (decosSD != null) {
                salesReport.setDecosSD(decosSD + " DECOS SD");
            }
            if (decosHD != null) {
                salesReport.setDecosHD(decosHD + " DECOS HD");
            }
            salesReport.setTvBlock(tvBlock);
            salesReport.setBlockTV(blockTV);
            salesReport.setSvaInternet(svaInternet);
            salesReport.setSvaLine(svaLine);
            salesReport.setStatusAudio(statusAudio);
            salesReport.setCodigoVendedor(codigoVendedor);
        }
        //sprint 3 - campos para svas adicionales - fin modificaciones

        if (row.length > 30) {
            // Sprint 4 - Estado legacy
            String statusLegacy = (String) row[31];
            salesReport.setCmsAtisStatus(statusLegacy);
        }
        return salesReport;
    }

    private SalesReport mapRowToSalesReport2(Object[] row) {
        String docNumber = (String) row[0];
        String productName = (String) row[1];
        Date registrationDate = (Date) row[2];
        String status = (String) row[3];
        BigDecimal price = (BigDecimal) row[4];
        BigDecimal discount = (BigDecimal) row[5];
        BigDecimal promPrice = (BigDecimal) row[6];
        Integer monthPeriod = (Integer) row[7];
        String lineType = (String) row[8];
        BigDecimal cashPrice = (BigDecimal) row[9];
        String codigoPedido = (String) row[10];
        String estadoSolicitud = (String) row[11];
        String orderId = (String) row[12];

        String clientName = (String) row[13];
        String clientLastName1 = (String) row[14];
        String clientLastName2 = (String) row[15];
        String commercialOperation = (String) row[16];
        String cancelDescription = (String) row[17];
        String enableDigitalInvoice = (String) row[18];
        String disableWhitePage = (String) row[19];
        String motivoEstado = (String) row[20];
        String customerPhone = (String) row[21];


        SalesReport salesReport = new SalesReport();
        String id_transaccion = (String) row[22];
        salesReport.setId_transaccion(id_transaccion);

        // para el reporte excel se requiere codigo cip... tabla peticion_pago_efectivo, statuslegacy y codigo vendedor
        String codigoVendedor = (String) row[23];
        String statusLegacy = (String) row[24];
        String codigoCIP = (String) row[25];
        salesReport.setCodigoVendedor(codigoVendedor);
        salesReport.setCmsAtisStatus(statusLegacy);
        salesReport.setNumeroCIP(codigoCIP);


        salesReport.setDni(docNumber);
        salesReport.setProductName(productName);
        salesReport.setSaleDate(registrationDate);
        if (estadoSolicitud != null) {
            salesReport.setStatus(estadoSolicitud);
        } else {
            salesReport.setStatus(ESTADO_CAIDA_APP.toUpperCase());
        }
        salesReport.setPrice(price);
        salesReport.setDiscount(discount);
        salesReport.setPromPrice(promPrice);
        salesReport.setMonthPeriod(monthPeriod);
        salesReport.setLineType(lineType);
        salesReport.setCashPrice(cashPrice);
        salesReport.setOrderId(orderId);

        salesReport.setRequestCode(codigoPedido);
        salesReport.setStatusRequest(estadoSolicitud);

        salesReport.setClientName(String.format("%s %s %s", StringUtils.emptyWhenNull(clientName),
                StringUtils.emptyWhenNull(clientLastName1), StringUtils.emptyWhenNull(clientLastName2)));
        salesReport.setCommercialOperation(commercialOperation);
        salesReport.setCancelDescription(cancelDescription);
        salesReport.setEnableDigitalInvoice(enableDigitalInvoice);
        salesReport.setDisableWhitePage(disableWhitePage);

        if (ESTADO_CAIDA.equalsIgnoreCase(estadoSolicitud) || ESTADO_SEGUIMIENTO.equalsIgnoreCase(estadoSolicitud)) {
            salesReport.setCancelDescription(motivoEstado);
        }

        salesReport.setCustomerPhone(customerPhone);

        //sprint 3 - campos para svas adicionales y statusaudio - inicio modificaciones
        //+ ", od.decossd, od.decoshd, od.decosdvr, od.tvblock, od.blocktv, od.svainternet, od.svaline, o.statusaudio"//campos de svas adicionales
//        if (row.length > 22) {
//            Integer decosSD = (Integer) row[22];
//            Integer decosHD = (Integer) row[23];
//            Integer decosDVR = (Integer) row[24];
//            String tvBlock = (String) row[25];
//            String blockTV = (String) row[26];
//            String svaInternet = (String) row[27];
//            String svaLine = (String) row[28];
//            String statusAudio = (String) row[29];
//            String codigoVendedor = (String) row[30];
//
//
//
//            if (decosSD != null) {
//                salesReport.setDecosSD(decosSD + " DECOS SD");
//            }
//            if (decosHD != null) {
//                salesReport.setDecosHD(decosHD + " DECOS HD");
//            }
//            salesReport.setTvBlock(tvBlock);
//            salesReport.setBlockTV(blockTV);
//            salesReport.setSvaInternet(svaInternet);
//            salesReport.setSvaLine(svaLine);
//            salesReport.setStatusAudio(statusAudio);
//            salesReport.setCodigoVendedor(codigoVendedor);
//        }
//        //sprint 3 - campos para svas adicionales - fin modificaciones
//
//        if (row.length > 30) {
//            // Sprint 4 - Estado legacy
//            String statusLegacy = (String) row[31];
//            salesReport.setCmsAtisStatus(statusLegacy);
//        }
        return salesReport;
    }


    @Override
    public List<UserOrderResponse> spGetUserOrders(String id, int numDays) {
        List<UserOrderResponse> listaUserOrders = new ArrayList<>();
        //List<Object[]> result = em.createNamedQuery("spGetUserOrders").setParameter("inParamUserId", id).setParameter("inParamNumDays", numDays).getResultList(); //sprint 3
        List<Object[]> result = em.createNamedQuery("spGetUserOrders").setParameter(1, id).getResultList();
        for (Object[] row : result) {
            UserOrderResponse userOrders = mapRowToUserOrders(row);

            listaUserOrders.add(userOrders);
        }
        return listaUserOrders;


    }


    private UserOrderResponse mapRowToUserOrders(Object[] row) {
        String status = (String) row[0];
        BigInteger quantity = BigInteger.valueOf(((Integer) row[1])); //sprint 3
        //BigInteger quantity = (BigInteger) row[1];	//sprint 3

        UserOrderResponse userOrderResponse = new UserOrderResponse();
        userOrderResponse.setStatus(status);
        userOrderResponse.setQuantity(quantity);


        return userOrderResponse;
    }

    //sprint 3 - no se deberia hacer esta conversi�n... pero estamos contra el tiempo...
    private SalesReport mapRowToSalesBandejaVentaAudio(Object[] row) {
        String docNumber = (String) row[0];
        String productName = (String) row[1];
        Date registrationDate = (Date) row[2];
        String status = (String) row[3];
        BigDecimal price = (BigDecimal) row[4];
        BigDecimal discount = (BigDecimal) row[5];
        BigDecimal promPrice = (BigDecimal) row[6];
        Integer monthPeriod = (Integer) row[7];
        String lineType = (String) row[8];
        BigDecimal cashPrice = (BigDecimal) row[9];
        String codigoPedido = (String) row[10];
        String estadoSolicitud = (String) row[11];
        String orderId = (String) row[12];

        String clientName = (String) row[13];
        String clientLastName1 = (String) row[14];
        String clientLastName2 = (String) row[15];
        String commercialOperation = (String) row[16];
        String cancelDescription = (String) row[17];
        String enableDigitalInvoice = (String) row[18];
        String disableWhitePage = (String) row[19];
        String motivoEstado = (String) row[20];

        String estadoAudio = (String) row[21];
        String codigoVendedor = (String) row[22];

        // obtenemos los 4 datos adicionales
        String celularCliente = (String) row[24];
        String cmsAtisStatus = (String) row[25];
        String codigoCIP = (String) row[26];
        String idTransaccion = (String) row[27];

        //Sprint 1
        String restringeHoras = (String) row[28];

        String metodoPago = (String) row[29];
        String statusCip = (String) row[30];
        String montoCip = (String) row[31];

        SalesReport salesReport = new SalesReport();
        salesReport.setMetpago(metodoPago);
        salesReport.setDni(docNumber);
        salesReport.setProductName(productName);
        salesReport.setSaleDate(registrationDate);
        if (estadoSolicitud != null) {
            salesReport.setStatus(estadoSolicitud);
        } else {
            salesReport.setStatus(ESTADO_CAIDA_APP.toUpperCase());
        }
        salesReport.setPrice(price);
        salesReport.setDiscount(discount);
        salesReport.setPromPrice(promPrice);
        salesReport.setMonthPeriod(monthPeriod);
        salesReport.setLineType(lineType);
        salesReport.setCashPrice(cashPrice);
        salesReport.setOrderId(orderId);

        salesReport.setRequestCode(codigoPedido);
        salesReport.setStatusRequest(estadoSolicitud);

        salesReport.setClientName(String.format("%s %s %s", StringUtils.emptyWhenNull(clientName),
                StringUtils.emptyWhenNull(clientLastName1), StringUtils.emptyWhenNull(clientLastName2)));
        salesReport.setCommercialOperation(commercialOperation);
        salesReport.setCancelDescription(cancelDescription);
        salesReport.setEnableDigitalInvoice(enableDigitalInvoice);
        salesReport.setDisableWhitePage(disableWhitePage);

        salesReport.setStatusAudio(estadoAudio);
        salesReport.setCodigoVendedor(codigoVendedor);

        // se solicita mostrar en el reporte excel el celular cliente, cmsatisstatus, codigo cip, idtransaccion
        salesReport.setCustomerPhone(celularCliente);
        salesReport.setCmsAtisStatus(cmsAtisStatus);
        salesReport.setNumeroCIP(codigoCIP);
        salesReport.setId_transaccion(idTransaccion);
        salesReport.setEstado_cip(statusCip);
        salesReport.setTotal_cip(montoCip);

        if(!estadoAudio.equals("2")){
            salesReport.setRestringehoras(restringeHoras);
        }

        if (ESTADO_CAIDA.equalsIgnoreCase(estadoSolicitud) || ESTADO_SEGUIMIENTO.equalsIgnoreCase(estadoSolicitud)) {
            salesReport.setCancelDescription(motivoEstado);
        }

        return salesReport;
    }

    private SaleFileReport mapRowToSalesBandejaVentasArchivo(Object[] row) {
        String codigoventa = (String) row[0];
        String codigoVendedor = (String) row[1];
        String codigoEstadoArchivo = (String) row[2];
        String estadoArchivo = (String) row[3];
        Date registrationDate = (Date) row[4];
        Integer whatsapp = (Integer) row[5];
        String audioActivo = (String) row[6];

        SaleFileReport saleFileReport = new SaleFileReport();

        saleFileReport.setOrderId(codigoventa);
        saleFileReport.setCodigoVendedor(codigoVendedor);
        saleFileReport.setStatusAudio(codigoEstadoArchivo);
        saleFileReport.setStatusAudioDescripcion(estadoArchivo);
        saleFileReport.setSaleDate(registrationDate);
        saleFileReport.setWhatsapp(whatsapp);
        saleFileReport.setAudioActivo(audioActivo);

        return saleFileReport;
    }

    public OrderUpfrontBean getOrderEstado(String codPago) {
        OrderUpfrontBean orderUpfrontBean = new OrderUpfrontBean();
        try (Connection con = Database.datasource().getConnection()) {
            String query = "SELECT pp.order_id,pp.codpago,v.estado_solicitud,v.motivo_estado " +
                    "FROM ibmx_a07e6d02edaf552.peticion_pago_efectivo pp " +
                    "INNER JOIN ibmx_a07e6d02edaf552.tdp_visor v " +
                    "ON v.id_visor=pp.order_id where pp.codpago=?";
            try (PreparedStatement ps = con.prepareStatement(query)) {
                ps.setString(1, "0000000" + codPago);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    orderUpfrontBean.setOrdenVenta(rs.getString(1));
                    orderUpfrontBean.setEstado(rs.getString(3));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderUpfrontBean;
    }

    public boolean updateEstadoMotivo(String estado, String motivo, String cip) {
        try (Connection con = Database.datasource().getConnection()) {
            String update = "UPDATE ibmx_a07e6d02edaf552.tdp_visor " +
                    "SET estado_solicitud=?, motivo_estado=? " +
                    "WHERE id_visor=(SELECT pp.order_id " +
                    "FROM ibmx_a07e6d02edaf552.peticion_pago_efectivo pp " +
                    "WHERE pp.codpago=?)";
            try (PreparedStatement ps = con.prepareStatement(update)) {
                ps.setString(1, estado);
                ps.setString(2, motivo);
                ps.setString(3, "0000000" + cip);
                int n = ps.executeUpdate();
                if (n > 0) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    
    // Sprint 7... pendiente pasar a spring data
    public boolean updateEstadoVisor(String estado, String orderId) {
        try (Connection con = Database.datasource().getConnection()) {
            String update = "UPDATE ibmx_a07e6d02edaf552.tdp_visor " +
                    "SET estado_solicitud=? " +
                    "WHERE id_visor=?";
            try (PreparedStatement ps = con.prepareStatement(update)) {
                ps.setString(1, estado);
                ps.setString(2, orderId);
                int n = ps.executeUpdate();
                if (n > 0) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }


    @Override
    public AgenVal updateAgentPendienteVisor(String orderId,String canal,String departamento,String tecnologia) {

        String ooo = orderId;
        AgenVal updateAgentPendienteVisor = new AgenVal();
        Boolean canalValidacion = false;
        Boolean departamentoValidacion = false;
        Boolean tecnologiaValidacion = false;

        String query = "SELECT v.departamento, coalesce(o.product_tec_inter, '') as product_tec_inter, s.canal " +
                "FROM ibmx_a07e6d02edaf552.tdp_order o " +
                "inner join ibmx_a07e6d02edaf552.tdp_visor v on o.order_id = v.id_visor " +
                "inner join ibmx_a07e6d02edaf552.tdp_sales_agent s on s.codatis = o.user_atis " +
                "where o.order_id = '" + orderId + "'";

        try (Connection con = Database.datasource().getConnection()) {

            try (PreparedStatement stmt = con.prepareStatement(query)) {
                LogVass.daoQueryDAO(logger, "updateAgentPendienteVisor", "query", stmt.toString());
                ResultSet rs = stmt.executeQuery();
                while (rs.next()) {
                    updateAgentPendienteVisor.setDepartamentoVal(rs.getString("departamento"));
                    updateAgentPendienteVisor.setTecnologiaVal(rs.getString("product_tec_inter"));
                    updateAgentPendienteVisor.setCanalVal(rs.getString("canal"));

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

            System.out.println(canal);
            System.out.println(tecnologia);
            System.out.println(departamento);

            // START CANAL
            String canalCon = canal;

            String canalArray[] = canalCon.split(",");
            if (canalArray.length == 1 && "" == canalArray[0]) {
                canalArray = null;
            }

            if(canal==null){
                canalCon="";
            }


            for (int item = 0; item < canalArray.length; item++) {
                //String canalValidar = "AGENCIAS";
                String canalValidar = updateAgentPendienteVisor.getCanalVal().toUpperCase();


                if (canalArray[item].equals(canalValidar)) {
                    canalValidacion = true;
                }
            }

          //FIN CANAL


            // START departamento
            String departamentoCon = departamento;

            String departamentoArray[] = departamentoCon.split(",");
            if (departamentoArray.length == 1 && "" == departamentoArray[0]) {
                departamentoArray = null;
            }

            if(departamento==null){
                departamentoCon="";
            }


            for (int item = 0; item < departamentoArray.length; item++) {
                //String departamentoValidar = "AGENCIAS";
                String departamentoValidar = updateAgentPendienteVisor.getDepartamentoVal().toUpperCase();


                if (departamentoArray[item].equals(departamentoValidar)) {
                    departamentoValidacion = true;
                }
            }
            //FIN CANAL


            // START tecnologia
            String tecnologiaCon = tecnologia;

            String tecnologiaArray[] = tecnologiaCon.split(",");
            if (tecnologiaArray.length == 1 && "" == tecnologiaArray[0]) {
                tecnologiaArray = null;
            }

            if(tecnologia==null){
                tecnologiaCon="";
            }


            for (int item = 0; item < tecnologiaArray.length; item++) {
                //String tecnologiaValidar = "AGENCIAS";
                String tecnologiaValidar = updateAgentPendienteVisor.getTecnologiaVal().toUpperCase();

                if (tecnologiaArray[item].equals(tecnologiaValidar)) {
                    tecnologiaValidacion = true;
                }
            }
        //FIN TECNOLOGIA

        String update = "UPDATE ibmx_a07e6d02edaf552.tdp_visor SET agent_pending = ? WHERE id_visor = ?";
        try (Connection con = Database.datasource().getConnection()) {
            try (PreparedStatement stmt = con.prepareStatement(update)) {
                //lógica para agendamiento pendiente cambiar estado AGENT_PENDIENT
                if(canalValidacion && departamentoValidacion && tecnologiaValidacion){
                    stmt.setString(1, "1");
                    stmt.setString(2, orderId);
                }else{
                    stmt.setString(1, "0");
                    stmt.setString(2, orderId);
                }

                LogVass.daoQueryDAO(logger, "updateAgentPendienteVisor", "update", stmt.toString());
                stmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return updateAgentPendienteVisor;
    }

    @Override
    public List<SalesReport> loadBandejaVentas(String codigoVendedor, String codigoEstado, Date fechaInicio, Date fechaFin) {
        List<SalesReport> data = new ArrayList<>();
        try {

            String sql = "select c.docNumber, o.productName, o.registrationDate, o.status, o.price, "
                    + "o.price - o.promPrice as discount, o.promPrice, o.monthPeriod, o.lineType, o.cashPrice, "
                    + "v.codigo_pedido, v.estado_solicitud, o.id, "
                    + "c.firstname, c.lastname1, c.lastname2, o.commercialoperation, o.cancelDescription, "
                    + "od.enabledigitalinvoice, od.disablewhitepage, v.motivo_estado, c.customerPhone, COALESCE(v.id_transaccion,'') as id_transaccion "
                    // para el reporte excel se requiere 1 campo mas: codigo vendedor, estado atis/cms y codigo cip
                    + ", o.userid, o.statusLegacy, pp.codpago,pp.status as estado_cip ,pp.total as total_cip ,o.paymentmode as metpago "
                    + "from ibmx_a07e6d02edaf552.order o "
                    + "join ibmx_a07e6d02edaf552.customer c on c.id = o.customerID "
                    + "left join ibmx_a07e6d02edaf552.tdp_visor v on v.id_visor = o.id "
                    + "left join ibmx_a07e6d02edaf552.order_detail od on od.orderId = o.id "
                    // para el reporte excel se requiere codigo cip... tabla peticion_pago_efectivo
                    + "left join  ibmx_a07e6d02edaf552.peticion_pago_efectivo pp on pp.order_id = o.id "
                    + "where o.registrationDate between :dateFrom and :fechaTo and o.userId = :vendorId AND v.ESTADO_SOLICITUD <> 'GENERANDO_CIP' ";

            if (codigoEstado != null) {
                if (!codigoEstado.equals("0")) {
                    if (!codigoEstado.equals("undefined")) {
                        sql += "and CASE " +
                                " WHEN v.estado_solicitud = 'PENDIENTE' THEN 1 " +
                                " WHEN v.estado_solicitud = 'PENDIENTE-PAGO' THEN 1 " +
                                " WHEN v.estado_solicitud = 'ENVIANDO' THEN 1  " +
                                " WHEN v.estado_solicitud = '' THEN 1 " +
                                " WHEN v.estado_solicitud = 'EN PROCESO' THEN 1 " +
                                " WHEN v.estado_solicitud = 'INGRESADO' THEN 2 " +
                                " WHEN v.estado_solicitud = 'SEGUIMIENTO' THEN 2 " +
                                " WHEN v.estado_solicitud = 'CAIDA' THEN 5 = :codigoEstado ";
                    }
                }
            }

            sql += "order by o.registrationDate desc";

            Query q = em.createNativeQuery(sql);
            q.setParameter("dateFrom", fechaInicio);
            q.setParameter("fechaTo", fechaFin);
            q.setParameter("vendorId", codigoVendedor);
            if (codigoEstado != null) {
                if (!codigoEstado.equals("0")) {
                    if (!codigoEstado.equals("undefined")) {
                        q.setParameter("codigoEstado", codigoEstado);
                    }
                }
            }

            System.out.println(q);
            List<Object[]> result = q.getResultList();
            for (Object[] row : result) {
                SalesReport salesReport = mapRowToSalesReport2(row);

                data.add(salesReport);
            }

        } catch (Exception e) {
            logger.error("Error loadReporteVentaData DAO.", e);
        }
        return data;
    }

}
