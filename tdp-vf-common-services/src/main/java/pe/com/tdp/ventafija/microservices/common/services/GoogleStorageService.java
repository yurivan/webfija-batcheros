package pe.com.tdp.ventafija.microservices.common.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.auth.Credentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.ReadChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.StorageOptions;

import pe.com.tdp.ventafija.microservices.common.services.config.ApplicationConfiguration;

@Service
public class GoogleStorageService {
	private static final Logger logger = LogManager.getLogger(GoogleStorageService.class);
	@Autowired
	public ApplicationConfiguration applicationConfiguration;


	/***
	 *
	 * @param firebaseObject
	 * @param audioName
	 * @return
	 */
	public File retriveFileFromZip(String firebaseObject,String audioName) {
		File audio = null;
		ZipFile zip = null;
		File f = null;

		FileOutputStream fos = null;
		InputStream in = null;
		try {
			f = retrieveFile(firebaseObject);

			zip = new ZipFile(f);

			ZipEntry entry = zip.getEntry(audioName);

			if (entry != null) {
				in = zip.getInputStream(entry);

				byte[] buffer = new byte[1024];

				audio = File.createTempFile("audio" + new Date().getTime(), ".tmp");

				int len;
				fos = new FileOutputStream(audio);

				while ((len = in.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
			}

		} catch (IOException e) {
			logger.error("io exp", e);
		} catch (Exception e) {
			logger.error("Exception.", e);
		} finally {
			try {
				if (zip != null)
					zip.close();
				if (fos != null)
					fos.close();
				if (in != null)
					in.close();
				if (f != null) {
					f.delete();
				}
			} catch (IOException e) {
				logger.info("IOException :" + e.getMessage());
			}
		}

		logger.info("audio obtenido: " + audio);
		return audio;
	}
	
	
	public File retrieveFile(String firebaseObject) throws FileNotFoundException, IOException{
		return downloadFile(applicationConfiguration.getGoogleStorageBucket(), firebaseObject);
		
		
	}

	public File retrieveFile(String firebaseObject, String id, String type) throws FileNotFoundException, IOException{
		return downloadFile(applicationConfiguration.getGoogleStorageBucket(), firebaseObject, id, type);


	}

	public File downloadFile (String bucket, String name, String id,String type) throws FileNotFoundException, IOException {
		logger.info(String.format("Descargando archivo %s de %s", name, bucket));
		File file = null;
		StorageOptions.Builder optionsBuilder = StorageOptions.newBuilder();
		//Credentials credentials = ServiceAccountCredentials.fromStream(new FileInputStream("/opt/misc/App-Venta-Fija-7f83e030b88c.json"));
		ClassLoader classLoader = getClass().getClassLoader();
		File creds = new File(classLoader.getResource("App-Venta-Fija-7f83e030b88c.json").getFile());
		Credentials credentials = ServiceAccountCredentials.fromStream(new FileInputStream(creds));
		optionsBuilder.setProjectId(applicationConfiguration.getGoogleApiProjectId());
		com.google.cloud.storage.Storage storage = optionsBuilder
				.setCredentials(credentials)
				.build()
				.getService();
		Blob blob = storage.get(BlobId.of(bucket, name));
		if (blob != null) {
			String nameFile = "download" + id + type;
			file = File.createTempFile(nameFile, ".tmp");
			try (ReadChannel reader = blob.reader()) {
				PrintStream writeTo = new PrintStream(new FileOutputStream(file));
				WritableByteChannel channel = Channels.newChannel(writeTo);
				ByteBuffer bytes = ByteBuffer.allocate(64 * 1024);
				while (reader.read(bytes) > 0) {
					bytes.flip();
					channel.write(bytes);
					bytes.clear();
				}
				writeTo.close();
			}catch (Exception e) {
				File dir = new File("/tmp");
				File[] files = dir.listFiles(
						(directory, name1) -> {
							return  name1.toLowerCase().endsWith(".tmp");
						});
				for (File f : files) {
					if (f.getName().startsWith(nameFile)) {
						f.delete();
					}
					break;
				}
			}
		}
		return file;
	}
	
	public File downloadFile (String bucket, String name) throws FileNotFoundException, IOException {
		logger.info(String.format("Descargando archivo %s de %s", name, bucket));
		File file = null;
		StorageOptions.Builder optionsBuilder = StorageOptions.newBuilder();

		//Credentials credentials = ServiceAccountCredentials.fromStream(new FileInputStream("/opt/misc/App-Venta-Fija-7f83e030b88c.json"));

		ClassLoader classLoader = getClass().getClassLoader();
		File creds = new File(classLoader.getResource("App-Venta-Fija-7f83e030b88c.json").getFile());
		Credentials credentials = ServiceAccountCredentials.fromStream(new FileInputStream(creds));

		optionsBuilder.setProjectId(applicationConfiguration.getGoogleApiProjectId());
		
		com.google.cloud.storage.Storage storage = optionsBuilder
				.setCredentials(credentials)
				.build()
				.getService();
		
		
		Blob blob = storage.get(BlobId.of(bucket, name));
		if (blob != null) {
			file = File.createTempFile("download", ".tmp");
			try (ReadChannel reader = blob.reader()) {
				PrintStream writeTo = new PrintStream(new FileOutputStream(file));
				WritableByteChannel channel = Channels.newChannel(writeTo);
				ByteBuffer bytes = ByteBuffer.allocate(64 * 1024);
				while (reader.read(bytes) > 0) {
					bytes.flip();
					channel.write(bytes);
					bytes.clear();
				}
				writeTo.close();
			}
		}
		
		return file;
	}
	
	public void sendFile (File f, String bucket, String fileName, String contentType) throws IOException, URISyntaxException {
		StorageOptions.Builder optionsBuilder = StorageOptions.newBuilder();



		//Credentials credentials = ServiceAccountCredentials.fromStream(new FileInputStream("/opt/misc/App-Venta-Fija-7f83e030b88c.json"));

		ClassLoader classLoader = getClass().getClassLoader();
		//File file = new File(classLoader.getResource("App-Venta-Fija-7f83e030b88c.json").getFile());
		File file = new File(new URI(classLoader.getResource("App-Venta-Fija-7f83e030b88c.json").toString()));

		Credentials credentials = ServiceAccountCredentials.fromStream(new FileInputStream(file));

		optionsBuilder.setProjectId(applicationConfiguration.getGoogleApiProjectId());
		
		com.google.cloud.storage.Storage storage = optionsBuilder
				.setCredentials(credentials)
				.build()
				.getService();
		
		BlobInfo bInfo = BlobInfo.newBuilder(applicationConfiguration.getGoogleStorageBucket(), fileName)
				.setContentType(contentType)
				.setContentDisposition("attachment")
				.build();
		
		storage.create(bInfo, new FileInputStream(f));
	}
	
	public void sendFile (File f, String fileName, String contentType) throws IOException {
		StorageOptions.Builder optionsBuilder = StorageOptions.newBuilder();

		//Credentials credentials = ServiceAccountCredentials.fromStream(new FileInputStream("/opt/misc/App-Venta-Fija-7f83e030b88c.json"));

		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("App-Venta-Fija-7f83e030b88c.json").getFile());

		Credentials credentials = ServiceAccountCredentials.fromStream(new FileInputStream(file));

		optionsBuilder.setProjectId(applicationConfiguration.getGoogleApiProjectId());
		
		com.google.cloud.storage.Storage storage = optionsBuilder
				.setCredentials(credentials)
				.build()
				.getService();
		
		BlobInfo bInfo = BlobInfo.newBuilder(applicationConfiguration.getGoogleStorageBucket(), fileName)
				.setContentType(contentType)
				.setContentDisposition("attachment")
				.build();
		
		storage.create(bInfo, new FileInputStream(f));
	}

	public void sendZipFile(File zipFile, String fileName) throws Exception {
		logger.info("enviando archivo "+fileName);
		// Cuando ocurre un error al subir archivo no se capturaba el error... se adiciona captura de excepcion general
		try {
			sendFile(zipFile, applicationConfiguration.getGoogleStorageBucket(), 
					String.format("%s/audios/%s_gsm.zip", fileName, fileName),
					"application/x-zip");
		/*} catch (IOException e) {
			logger.error("io exp", e);
		}*/
		} catch (Exception e) {
			logger.error("Error al ejecutar subida de audio con id venta: " + fileName , e);
			throw e;
		}
		logger.info("envio archivo culminado");
	}
}
