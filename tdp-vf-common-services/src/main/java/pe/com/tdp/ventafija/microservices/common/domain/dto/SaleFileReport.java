package pe.com.tdp.ventafija.microservices.common.domain.dto;

import java.util.Date;

public class SaleFileReport {

    private String orderId;
    private Date saleDate;
    private String statusAudio;
    private String statusAudioDescripcion;
    private String codigoVendedor;
    private Integer whatsapp;
    private String audioActivo;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Date getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(Date saleDate) {
        this.saleDate = saleDate;
    }

    public String getStatusAudio() {
        return statusAudio;
    }

    public void setStatusAudio(String statusAudio) {
        this.statusAudio = statusAudio;
    }

    public String getStatusAudioDescripcion() {
        return statusAudioDescripcion;
    }

    public void setStatusAudioDescripcion(String statusAudioDescripcion) {
        this.statusAudioDescripcion = statusAudioDescripcion;
    }

    public String getCodigoVendedor() {
        return codigoVendedor;
    }

    public void setCodigoVendedor(String codigoVendedor) {
        this.codigoVendedor = codigoVendedor;
    }

    public Integer getWhatsapp() {
        return whatsapp;
    }

    public void setWhatsapp(Integer whatsapp) {
        this.whatsapp = whatsapp;
    }

    public String getAudioActivo() {
        return audioActivo;
    }

    public void setAudioActivo(String audioActivo) {
        this.audioActivo = audioActivo;
    }
}
