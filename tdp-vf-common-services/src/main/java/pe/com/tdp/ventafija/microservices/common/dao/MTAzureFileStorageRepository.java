package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.common.domain.entity.MTAzureFileStorage;

import java.util.List;

@Repository
public interface MTAzureFileStorageRepository extends JpaRepository<MTAzureFileStorage, Long> {

    @Modifying
    @Query(value = "INSERT INTO ibmx_a07e6d02edaf552.mt_azure_file_storage(mtorder, " +
            "estado_tdpvisor, codigo_pedido_tdpvisor, date_register, file_date)" +
            "VALUES (:mtOrder, :estado_tdpvisor, :codigo_pedido_tdpvisor, now(), :file_date);", nativeQuery = true)
    @Transactional
    void insertMTAzureFileStorage(@Param("mtOrder") String mtOrder,
                                  @Param("estado_tdpvisor") String estado_tdpvisor,
                                  @Param("codigo_pedido_tdpvisor") String codigo_pedido_tdpvisor,
                                  @Param("file_date") String file_date);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update MTAzureFileStorage m set m.estadoTdpVisor = ?1, m.codigoPedidoTdpVisor = ?2 where m.mtOrder = ?3")
    void updateMTAzureFileStorage(String estadoTdpVisor, String codigoPedidoTdpVisor, String mtOrder);

    @Query("select m from MTAzureFileStorage m where m.mtOrder = ?1")
    MTAzureFileStorage getMtOrderFile(String mtOrder);

    @Query("select m from MTAzureFileStorage m where m.mtOrder like concat(?1,'_%')")
    List<MTAzureFileStorage> getMtOrderFileRetail(String mtOrder);
}
