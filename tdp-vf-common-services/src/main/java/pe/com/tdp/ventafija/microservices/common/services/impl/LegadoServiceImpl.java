package pe.com.tdp.ventafija.microservices.common.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadoatis.ConsultaEstadoAtisClient;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadoatis.DuplicateApiAtisRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadoatis.DuplicateApiAtisResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadocms.ConsultaEstadoCmsClient;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadocms.DuplicateApiCmsRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.consultaestadocms.DuplicateApiCmsResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.parqueatis.ParqueAtisClient;
import pe.com.tdp.ventafija.microservices.common.clients.parqueatis.ServicesApiAtisRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.parqueatis.ServicesApiAtisResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.parqueatis.ServicesApiAtisResponseBodyParkDetail;
import pe.com.tdp.ventafija.microservices.common.clients.parquecms.ParqueCmsClient;
import pe.com.tdp.ventafija.microservices.common.clients.parquecms.ServicesApiCmsRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.parquecms.ServicesApiCmsResponseBody;
import pe.com.tdp.ventafija.microservices.common.clients.parquecms.ServicesApiCmsResponseBodyDetail;
import pe.com.tdp.ventafija.microservices.common.constant.LegadosConstants;
import pe.com.tdp.ventafija.microservices.common.constant.ParametersConstants;
import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.dao.TdpServiceClientRepository;
import pe.com.tdp.ventafija.microservices.common.dao.TdpServicePedidoRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.domain.entity.TdpServiceClient;
import pe.com.tdp.ventafija.microservices.common.domain.entity.TdpServicePedido;
import pe.com.tdp.ventafija.microservices.common.dto.ServiceCallEvent;
import pe.com.tdp.ventafija.microservices.common.services.LegadoService;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.Constants;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class LegadoServiceImpl implements LegadoService {
    private static final Logger logger = LogManager.getLogger();

    private DateTimeZone dtZone = DateTimeZone.forID("America/Lima");

    @Autowired
    private ParametersRepository parametersRepository;
    @Autowired
    private TdpServiceClientRepository tdpServiceClientRepository;
    @Autowired
    private TdpServicePedidoRepository tdpServicePedidoRepository;



    private ApiHeaderConfig apiHeaderConfig;
    private ServiceCallEventsService serviceCallEventsService;

    // Detener contingencia stados de ATIS
    @Value("${contingencia.stop.estados.atis}")
    private String contingenciaStopEstadoAtis;
    // Detener contingencia stados de ATIS
    @Value("${contingencia.stop.estados.cms}")
    private String contingenciaStopEstadoCms;

    @Autowired
    public LegadoServiceImpl(ApiHeaderConfig apiHeaderConfig, ServiceCallEventsService serviceCallEventsService) {
        this.apiHeaderConfig = apiHeaderConfig;
        this.serviceCallEventsService = serviceCallEventsService;
    }

    private ApiResponse<DuplicateApiAtisResponseBody> getConsultaEstadoAtis(DuplicateApiAtisRequestBody apiRequestBody) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getAtisConsultaEstadoUri())
                .setApiId(apiHeaderConfig.getAtisConsultarEstadoApiId())
                .setApiSecret(apiHeaderConfig.getAtisConsultarEstadoApiSecret())
                .setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_STATUS_ATIS)
                .setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_ATIS)
                .build();

        ConsultaEstadoAtisClient consultaEstadoAtisClient = new ConsultaEstadoAtisClient(config);
        ClientResult<ApiResponse<DuplicateApiAtisResponseBody>> result = consultaEstadoAtisClient.post(apiRequestBody);
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

    @Override
    public TdpServicePedido getEstadoAtis(String codigoPedido) {
        TdpServicePedido tdpServicePedidos = new TdpServicePedido();

        boolean consultar = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        try {

            Parameters timeout = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "ESTADO", "timer.estado.atis");
            syncronizarDay = timeout.getStrValue() == null || timeout.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(timeout.getStrValue());
            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_ATIS);
            tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

            if (tdpServicePedidos != null && tdpServicePedidos.getAuditoriaModify() != null) {
                if (tdpServicePedidos.getPedidoEstadoCode() != null && contingenciaStopEstadoAtis.indexOf("|" + tdpServicePedidos.getPedidoEstadoCode() + "|") < 0) {
                    Date fechaVencimiento = tdpServicePedidos.getAuditoriaModify();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultar = true;
                    }
                }
            } else {
                consultar = true;
            }

            if (consultar) {
                try {
                    DuplicateApiAtisRequestBody apiRequestBody = new DuplicateApiAtisRequestBody();
                    apiRequestBody.setPeticion(codigoPedido);
                    ApiResponse<DuplicateApiAtisResponseBody> apiResponse = getConsultaEstadoAtis(apiRequestBody);
                    if ((Constants.RESPONSE).equals(apiResponse.getHeaderOut().getMsgType())) {
                        if (apiResponse.getBodyOut().getEstadoPeticion() != null) {
                            if (tdpServicePedidos == null) {
                                tdpServicePedidos = new TdpServicePedido();
                                tdpServicePedidos.setAuditoriaCreate(dateHoy);
                            }
                            tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_ATIS);
                            tdpServicePedidos.setPedidoCodigo(codigoPedido);
                            tdpServicePedidos.setPedidoEstadoCode(apiResponse.getBodyOut().getEstadoPeticion());
                            tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(apiResponse.getBodyOut().getEstadoPeticion(), translateList));

                            tdpServicePedidos.setAuditoriaModify(dateHoy);

                            tdpServicePedidoRepository.save(tdpServicePedidos);
                        }
                    }
                } catch (Exception apiconnect) {
                    logger.error("Error obtenerEstadoAtis Service. apiconnect.", apiconnect);
                }
            }
        } catch (Exception ex) {
            logger.error("Error obtenerEstadoAtis Service.", ex);
        }

        return tdpServicePedidos;
    }

    @Override
    public String getEstadoAtisCodeWithTry(String codigoPedido) {
        String estadoPedido = null;

        boolean consultar = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        Integer triesTotal = 1;
        try {

            Parameters timeout = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "ESTADO", "timer.estado.atis");
            syncronizarDay = timeout.getStrValue() == null || timeout.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(timeout.getStrValue());
            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_ATIS);
            TdpServicePedido tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

            if (tdpServicePedidos != null && tdpServicePedidos.getAuditoriaModify() != null) {
                if (contingenciaStopEstadoAtis.indexOf("|" + estadoPedido + "|") < 0) {
                    Date fechaVencimiento = tdpServicePedidos.getAuditoriaModify();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultar = true;
                    }else{
                        estadoPedido = tdpServicePedidos.getPedidoEstadoCode();
                    }
                }
            } else {
                consultar = true;
            }

            if (consultar) {
                Integer estadoAtisDowns = 0;
                Parameters tries = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TRY, "ESTADO", "try.estado.atis");
                triesTotal = tries.getStrValue() == null || tries.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(tries.getStrValue());

                while (estadoAtisDowns < triesTotal && estadoPedido == null) {

                    try {
                        DuplicateApiAtisRequestBody apiRequestBody = new DuplicateApiAtisRequestBody();
                        apiRequestBody.setPeticion(codigoPedido);
                        ApiResponse<DuplicateApiAtisResponseBody> apiResponse = getConsultaEstadoAtis(apiRequestBody);
                        if ((Constants.RESPONSE).equals(apiResponse.getHeaderOut().getMsgType())) {
                            if (apiResponse.getBodyOut().getEstadoPeticion() != null) {
                                if (tdpServicePedidos == null) {
                                    tdpServicePedidos = new TdpServicePedido();
                                    tdpServicePedidos.setAuditoriaCreate(dateHoy);
                                }
                                tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_ATIS);
                                tdpServicePedidos.setPedidoCodigo(codigoPedido);
                                tdpServicePedidos.setPedidoEstadoCode(apiResponse.getBodyOut().getEstadoPeticion());
                                tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(apiResponse.getBodyOut().getEstadoPeticion(), translateList));

                                tdpServicePedidos.setAuditoriaModify(dateHoy);

                                estadoPedido = tdpServicePedidos.getPedidoEstadoCode();

                                tdpServicePedidoRepository.save(tdpServicePedidos);
                            }
                        } else estadoAtisDowns++;

                        ServiceCallEvent datos= new ServiceCallEvent();
                        datos = cargarDatosServiceCallEvent(apiResponse.getBodyOut(), tdpServicePedidos, apiRequestBody);
                        serviceCallEventsService.registerEvent(datos);

                    } catch (Exception apiconnect) {
                        estadoAtisDowns++;
                        logger.error("Error obtenerEstadoAtis Service. apiconnect.", apiconnect);
                    }

                }

            }
        } catch (Exception ex) {
            logger.error("Error obtenerEstadoAtis Service.", ex);
        }

        return estadoPedido;
    }

    private ServiceCallEvent cargarDatosServiceCallEvent(DuplicateApiAtisResponseBody bodyOut, TdpServicePedido tdpServicePedidos, DuplicateApiAtisRequestBody apiRequestBody) {
        logger.info("ValidacionAtis generacion de Log para HDEC: ");
        ServiceCallEvent data = new ServiceCallEvent();
        JSONObject obj = new JSONObject(apiRequestBody);


        data.setMsg("OK");
        data.setUsername(tdpServicePedidos.getPedidoEstado());
        data.setOrderId(tdpServicePedidos.getPedidoCodigo());
        data.setDocNumber(tdpServicePedidos.getPedidoType());
        data.setServiceCode("ESTADO-ATIS");
        data.setServiceUrl(tdpServicePedidos.getPedidoEstado());
        data.setServiceRequest(""+obj);
        data.setServiceResponse(bodyOut.getEstadoPeticion() + " / " + tdpServicePedidos.getPedidoEstado());
        data.setSourceApp("WEBVF");
        data.setSourceAppVersion("1.0");
        data.setResult("OK");
        logger.info("ValidacionAtis Json apiRequestBody: " + obj);
        logger.info("ValidacionAtis data: " + data);
        return data;
    }


    private ServiceCallEvent cargarDatosServiceCallEventCms(DuplicateApiCmsResponseBody objCms, TdpServicePedido tdpServicePedidos, DuplicateApiCmsRequestBody apiRequestBody) {
        logger.info("ValidacionCms Azuki.");
        ServiceCallEvent data = new ServiceCallEvent();
        JSONObject obj = new JSONObject(apiRequestBody);

        data.setMsg("OK");
        data.setUsername(tdpServicePedidos.getPedidoEstado());
        data.setOrderId(tdpServicePedidos.getPedidoCodigo());
        data.setDocNumber(tdpServicePedidos.getPedidoType());
        data.setServiceCode("ESTADO-CMS");
        data.setServiceUrl(tdpServicePedidos.getPedidoEstado());
        data.setServiceRequest(""+obj);
        data.setServiceResponse(objCms.getEstadoRequerimiento() + " / " + tdpServicePedidos.getPedidoEstado());
        data.setSourceApp("WEBVF");
        data.setSourceAppVersion("1.0");
        data.setResult("OK");
        return data;
    }


    private ApiResponse<DuplicateApiCmsResponseBody> getConsultaEstadoCms(DuplicateApiCmsRequestBody apiRequestBody) throws
            ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getCmsConsultaEstadoUri())
                .setApiId(apiHeaderConfig.getCmsConsultarEstadoApiId())
                .setApiSecret(apiHeaderConfig.getCmsConsultarEstadoApiSecret())
                .setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_STATUS_CMS)
                .setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_CMS)
                .build();

        ConsultaEstadoCmsClient consultaEstadoCmsClient = new ConsultaEstadoCmsClient(config);
        ClientResult<ApiResponse<DuplicateApiCmsResponseBody>> result = consultaEstadoCmsClient.post(apiRequestBody);
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

    @Override
    public TdpServicePedido getEstadoCms(String codigoPedido) {
        TdpServicePedido tdpServicePedidos = new TdpServicePedido();

        boolean consultar = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        try {

            Parameters timeout = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "ESTADO", "timer.estado.cms");
            syncronizarDay = timeout.getStrValue() == null || timeout.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(timeout.getStrValue());
            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_CMSR);
            tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

            if (tdpServicePedidos != null && tdpServicePedidos.getAuditoriaModify() != null) {
                if (tdpServicePedidos.getPedidoEstadoCode() != null && contingenciaStopEstadoCms.indexOf("|" + tdpServicePedidos.getPedidoEstadoCode() + "|") < 0) {
                    Date fechaVencimiento = tdpServicePedidos.getAuditoriaModify();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultar = true;
                    }
                }
            } else {
                consultar = true;
            }

            if (consultar) {
                try {
                    DuplicateApiCmsRequestBody apiRequestBody = new DuplicateApiCmsRequestBody();
                    apiRequestBody.setCodigoRequerimiento(codigoPedido);
                    ApiResponse<DuplicateApiCmsResponseBody> objCms = getConsultaEstadoCms(apiRequestBody);
                    if ((Constants.RESPONSE).equals(objCms.getHeaderOut().getMsgType())) {
                        if (objCms.getBodyOut().getEstadoRequerimiento() != null) {
                            if (tdpServicePedidos == null) {
                                tdpServicePedidos = new TdpServicePedido();
                                tdpServicePedidos.setAuditoriaCreate(dateHoy);
                            }
                            tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_CMS);
                            tdpServicePedidos.setPedidoCodigo(codigoPedido);
                            tdpServicePedidos.setPedidoEstadoCode(objCms.getBodyOut().getEstadoRequerimiento());
                            tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(objCms.getBodyOut().getEstadoRequerimiento(), translateList));

                            tdpServicePedidos.setAuditoriaModify(dateHoy);

                            tdpServicePedidoRepository.save(tdpServicePedidos);
                        }
                    }
                } catch (Exception apiconnect) {
                    logger.error("Error obtenerEstadoCms Service. apiconnect.", apiconnect);
                }
            }
        } catch (Exception ex) {
            logger.error("Error obtenerEstadoCms Service.", ex);
        }

        return tdpServicePedidos;
    }

    @Override
    public String getEstadoCmsCodeWithTry(String codigoPedido) {
        String estadoPedido = null;

        boolean consultar = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        Integer triesTotal = 1;
        try {

            Parameters timeout = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "ESTADO", "timer.estado.cms");
            syncronizarDay = timeout.getStrValue() == null || timeout.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(timeout.getStrValue());
            List<Parameters> translateList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, ParametersConstants.PARAMETERS_CATEGORY_TRANSLATE_CMSR);
            TdpServicePedido tdpServicePedidos = tdpServicePedidoRepository.findOneByPedidoCodigo(codigoPedido);

            if (tdpServicePedidos != null && tdpServicePedidos.getAuditoriaModify() != null) {
                if (contingenciaStopEstadoCms.indexOf("|" + estadoPedido + "|") < 0) {
                    Date fechaVencimiento = tdpServicePedidos.getAuditoriaModify();
                    Calendar c = Calendar.getInstance();
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultar = true;
                    }else{
                        estadoPedido = tdpServicePedidos.getPedidoEstadoCode();
                    }
                }
            } else {
                consultar = true;
            }

            if (consultar) {
                Integer estadoCmsDowns = 0;
                Parameters tries = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TRY, "ESTADO", "try.estado.cms");
                triesTotal = tries.getStrValue() == null || tries.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(tries.getStrValue());

                while (estadoCmsDowns < triesTotal && estadoPedido == null) {

                    try {
                        DuplicateApiCmsRequestBody apiRequestBody = new DuplicateApiCmsRequestBody();
                        apiRequestBody.setCodigoRequerimiento(codigoPedido);
                        ApiResponse<DuplicateApiCmsResponseBody> objCms = getConsultaEstadoCms(apiRequestBody);
                        if ((Constants.RESPONSE).equals(objCms.getHeaderOut().getMsgType())) {
                            if (objCms.getBodyOut().getEstadoRequerimiento() != null) {
                                if (tdpServicePedidos == null) {
                                    tdpServicePedidos = new TdpServicePedido();
                                    tdpServicePedidos.setAuditoriaCreate(dateHoy);
                                }
                                tdpServicePedidos.setPedidoType(LegadosConstants.PARQUE_CMS);
                                tdpServicePedidos.setPedidoCodigo(codigoPedido);
                                tdpServicePedidos.setPedidoEstadoCode(objCms.getBodyOut().getEstadoRequerimiento());
                                tdpServicePedidos.setPedidoEstado(getDescriptionByElementScoring(objCms.getBodyOut().getEstadoRequerimiento(), translateList));

                                tdpServicePedidos.setAuditoriaModify(dateHoy);

                                estadoPedido = tdpServicePedidos.getPedidoEstadoCode();

                                tdpServicePedidoRepository.save(tdpServicePedidos);

                            }
                        } else estadoCmsDowns++;

                        ServiceCallEvent datos= new ServiceCallEvent();
                        datos = cargarDatosServiceCallEventCms(objCms.getBodyOut(), tdpServicePedidos, apiRequestBody);
                        serviceCallEventsService.registerEvent(datos);

                    } catch (Exception apiconnect) {
                        estadoCmsDowns++;
                        logger.error("Error obtenerEstadoCms Service. apiconnect.", apiconnect);
                    }

                }


            }
        } catch (Exception ex) {
            logger.error("Error obtenerEstadoCms Service.", ex);
        }

        return estadoPedido;
    }



    private ApiResponse<ServicesApiAtisResponseBody> getParqueAtis(ServicesApiAtisRequestBody apiAtisRequestBody, String orderId) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getAtisConsultaParqueUri())
                .setApiId(apiHeaderConfig.getAtisConsultarParqueApiId())
                .setApiSecret(apiHeaderConfig.getAtisConsultarParqueApiSecret())
                .setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_ATIS)
                .setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_ATIS)
                .build();

        ParqueAtisClient parqueAtisClient = new ParqueAtisClient(config);
        ClientResult<ApiResponse<ServicesApiAtisResponseBody>> result = parqueAtisClient.post(apiAtisRequestBody);
        ApiResponse<ServicesApiAtisResponseBody> objAtis = result.getResult();

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
            result.getEvent().setOrderId(orderId);
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return objAtis;
    }

    @Override
    public List<TdpServiceClient> getParqueAtis(String documentType, String documentNumber, String orderId) {
        documentType = documentType.toUpperCase();
        List<TdpServiceClient> tdpServiceClientList = new ArrayList<>();
        // Realizamos la equivalencia para ParqueATIS
        documentType = equivalenceDocument("PARQUEATIS", documentType);

        boolean consultaAtis = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        try {
            Parameters parametersAtis = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "PARQUE", "timer.parque.atis");
            syncronizarDay = parametersAtis.getStrValue() == null || parametersAtis.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(parametersAtis.getStrValue());
            List<Parameters> atisParameterList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, "ATIS");
            tdpServiceClientList = tdpServiceClientRepository.findAllByProductTypeAndClientTipoDocAndClientDocument(LegadosConstants.PARQUE_ATIS, documentType, documentNumber);

            if (tdpServiceClientList != null && tdpServiceClientList.size() > 0) {
                Date fechaVencimiento = tdpServiceClientList.get(0).getAuditoriaModify();
                Calendar c = Calendar.getInstance();
                if (fechaVencimiento != null) {
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultaAtis = true;
                    }
                }
            } else {
                consultaAtis = true;
            }
            if (consultaAtis) {
                try {
                    ServicesApiAtisRequestBody apiAtisRequestBody = new ServicesApiAtisRequestBody();
                    apiAtisRequestBody.setTipoDocumento(documentType);
                    apiAtisRequestBody.setNumeroDocumento(documentNumber);
                    ApiResponse<ServicesApiAtisResponseBody> objAtis = getParqueAtis(apiAtisRequestBody, orderId);
                    if ((Constants.RESPONSE).equals(objAtis.getHeaderOut().getMsgType())) {
                        for (TdpServiceClient serviceTypeAtis : tdpServiceClientList) {
                            if (serviceTypeAtis.getServiceEstadoCode().equalsIgnoreCase(LegadosConstants.PARQUE_1050)) {
                                serviceTypeAtis.setAuditoriaModify(dateHoy);
                                tdpServiceClientRepository.save(serviceTypeAtis);
                            }
                        }
                        for (ServicesApiAtisResponseBodyParkDetail obj : objAtis.getBodyOut().getDetalleParque()) {
                            if (obj.getTelefono() != null) {
                                TdpServiceClient tdpServiceClientATIS = new TdpServiceClient();
                                tdpServiceClientATIS.setAuditoriaCreate(dateHoy);
                                if (tdpServiceClientList.size() > 0) {
                                    for (TdpServiceClient serviceTypeAtis : tdpServiceClientList) {
                                        if (serviceTypeAtis.getProductService().equalsIgnoreCase(obj.getTelefono())) {
                                            tdpServiceClientATIS = serviceTypeAtis;
                                            break;
                                        }
                                    }
                                }

                                tdpServiceClientATIS.setProductType(LegadosConstants.PARQUE_ATIS);
                                tdpServiceClientATIS.setProductService(obj.getTelefono());
                                tdpServiceClientATIS.setProductPs(obj.getProductoPrincipal());
                                tdpServiceClientATIS.setProductoDescripcion(obj.getDescripcionComercial());
                                tdpServiceClientATIS.setProductOrigen(obj.getTipoOrigen());
                                tdpServiceClientATIS.setProductLinea(obj.getProductoLinea());
                                tdpServiceClientATIS.setProductInternet(obj.getProductoInternet());
                                tdpServiceClientATIS.setProductTv(obj.getProductoTV());

                                tdpServiceClientATIS.setClientTipoDoc(documentType);
                                tdpServiceClientATIS.setClientDocument(documentNumber);
                                tdpServiceClientATIS.setClientCms(null);

                                String ubigeoCode = obj.getCodigoUbigeo();
                                tdpServiceClientATIS.setDepartamentoCode(ubigeoCode.length() > 2 ? ubigeoCode.substring(0, 2) : "");
                                tdpServiceClientATIS.setDepartamento(obj.getDepartamento());
                                tdpServiceClientATIS.setProvinciaCode(ubigeoCode.length() > 4 ? ubigeoCode.substring(2, 4) : "");
                                tdpServiceClientATIS.setProvincia(obj.getProvincia());
                                tdpServiceClientATIS.setDistritoCode(ubigeoCode.length() >= 6 ? ubigeoCode.substring(4, 6) : "");
                                tdpServiceClientATIS.setDistrito(obj.getDistrito());
                                tdpServiceClientATIS.setCoordenadaX(null);
                                tdpServiceClientATIS.setCoordenadaY(null);
                                tdpServiceClientATIS.setDireccion(obj.getDireccion());

                                tdpServiceClientATIS.setServiceEstadoCode(obj.getEstadoPC());
                                tdpServiceClientATIS.setServiceEstado(getDescriptionByElementScoring(obj.getEstadoPC(), atisParameterList));

                                tdpServiceClientATIS.setAuditoriaModify(dateHoy);

                                tdpServiceClientRepository.save(tdpServiceClientATIS);
                                if (tdpServiceClientList.size() > 0) {
                                    Integer noService = 0;
                                    for (TdpServiceClient serviceTypeList : tdpServiceClientList) {
                                        if (serviceTypeList.getProductService().equalsIgnoreCase(tdpServiceClientATIS.getProductService())) {
                                            serviceTypeList = tdpServiceClientATIS;
                                            break;
                                        } else {
                                            noService++;
                                        }
                                    }
                                    if (noService == tdpServiceClientList.size())
                                        tdpServiceClientList.add(tdpServiceClientATIS);
                                } else {
                                    tdpServiceClientList.add(tdpServiceClientATIS);
                                }
                            }
                        }
                    } else {
                        try {
                            if (objAtis.getBodyOut().getClientException() != null)
                                if (objAtis.getBodyOut().getClientException().getExceptionCode() == 1050) {
                                    TdpServiceClient tdpServiceClientATIS = new TdpServiceClient();
                                    tdpServiceClientATIS.setAuditoriaCreate(dateHoy);
                                    if (tdpServiceClientList.size() > 0) {
                                        for (TdpServiceClient serviceTypeAtis : tdpServiceClientList) {
                                            if (serviceTypeAtis.getProductService().equalsIgnoreCase(LegadosConstants.PARQUE_ATIS + documentNumber)) {
                                                tdpServiceClientATIS = serviceTypeAtis;
                                                break;
                                            }
                                        }
                                    }

                                    tdpServiceClientATIS.setProductType(LegadosConstants.PARQUE_ATIS);
                                    tdpServiceClientATIS.setProductService(LegadosConstants.PARQUE_ATIS + documentNumber);
                                    tdpServiceClientATIS.setProductPs(null);
                                    tdpServiceClientATIS.setProductoDescripcion("Error de negocio producido en la lógica del servicio invocado.");
                                    tdpServiceClientATIS.setProductOrigen(null);
                                    tdpServiceClientATIS.setProductLinea(null);
                                    tdpServiceClientATIS.setProductInternet(null);
                                    tdpServiceClientATIS.setProductTv(null);

                                    tdpServiceClientATIS.setClientTipoDoc(documentType);
                                    tdpServiceClientATIS.setClientDocument(documentNumber);
                                    tdpServiceClientATIS.setClientCms(null);

                                    tdpServiceClientATIS.setDepartamentoCode(null);
                                    tdpServiceClientATIS.setDepartamento(null);
                                    tdpServiceClientATIS.setProvinciaCode(null);
                                    tdpServiceClientATIS.setProvincia(null);
                                    tdpServiceClientATIS.setDistritoCode(null);
                                    tdpServiceClientATIS.setDistrito(null);
                                    tdpServiceClientATIS.setCoordenadaX(null);
                                    tdpServiceClientATIS.setCoordenadaY(null);
                                    tdpServiceClientATIS.setDireccion(null);

                                    tdpServiceClientATIS.setServiceEstadoCode("1050NW");
                                    tdpServiceClientATIS.setServiceEstado("NO DATA");

                                    tdpServiceClientATIS.setAuditoriaModify(dateHoy);

                                    tdpServiceClientRepository.save(tdpServiceClientATIS);
                                    if (tdpServiceClientList.size() > 0) {
                                        Integer noService = 0;
                                        for (TdpServiceClient serviceTypeList : tdpServiceClientList) {
                                            if (serviceTypeList.getProductService().equalsIgnoreCase(tdpServiceClientATIS.getProductService())) {
                                                serviceTypeList = tdpServiceClientATIS;
                                                break;
                                            } else {
                                                noService++;
                                            }
                                        }
                                        if (noService == tdpServiceClientList.size())
                                            tdpServiceClientList.add(tdpServiceClientATIS);
                                    } else {
                                        tdpServiceClientList.add(tdpServiceClientATIS);
                                    }
                                }
                        } catch (Exception parques) {
                            logger.error("Error obtenerParqueAtis Service. parques.", parques);
                        }
                    }
                } catch (Exception atisService) {
                    logger.error("Error obtenerParqueAtis Service. AtisService.", atisService);
                }
            }
            List<TdpServiceClient> tdpServiceClientListTmp = new ArrayList<>();
            for (TdpServiceClient serviceType : tdpServiceClientList) {
                if (!LegadosConstants.PARQUE_1050.equalsIgnoreCase(serviceType.getServiceEstadoCode()))
                    tdpServiceClientListTmp.add(serviceType);
            }
            tdpServiceClientList = tdpServiceClientListTmp;
        } catch (Exception ex) {
            logger.error("Error obtenerParqueAtis Service.", ex);
        }

        return tdpServiceClientList;
    }

    private ApiResponse<ServicesApiCmsResponseBody> getParqueCms(ServicesApiCmsRequestBody apiCmsRequestBody, String orderId) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getCmsConsultaParqueUri())
                .setApiId(apiHeaderConfig.getCmsConsultarParqueApiId())
                .setApiSecret(apiHeaderConfig.getCmsConsultarParqueApiSecret())
                .setOperation(LegadosConstants.API_REQUEST_HEADER_OPERATION_CMS)
                .setDestination(LegadosConstants.API_REQUEST_HEADER_DESTINATION_CMS)
                .build();

        ParqueCmsClient parqueCmsClient = new ParqueCmsClient(config);
        ClientResult<ApiResponse<ServicesApiCmsResponseBody>> result = parqueCmsClient.post(apiCmsRequestBody);

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
            result.getEvent().setOrderId(orderId);
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }

        return result.getResult();
    }

    @Override
    public List<TdpServiceClient> getParqueCms(String documentType, String documentNumber, String orderId) {
        documentType = documentType.toUpperCase();
        List<TdpServiceClient> tdpServiceClientList = new ArrayList<>();
        // Realizamos la equivalencia para ParqueATIS
        documentType = equivalenceDocument("PARQUECMS", documentType);

        boolean consultaCms = false;
        DateTime hoy = new DateTime(new Date());
        DateTime hoyLima = hoy.withZone(dtZone);
        Date dateHoy = hoyLima.toDate();

        Integer syncronizarDay = 1;
        try {
            Parameters parametersAtis = parametersRepository.findOneByDomainAndCategoryAndElement(ParametersConstants.PARAMETERS_DOMAIN_TIMER, "PARQUE", "timer.parque.cms");
            syncronizarDay = parametersAtis.getStrValue() == null || parametersAtis.getStrValue().equalsIgnoreCase("") ? 1 : Integer.parseInt(parametersAtis.getStrValue());
            List<Parameters> cmssParameterList = parametersRepository.findByDomainAndCategoryOrderByAuxiliarAsc(ParametersConstants.PARAMETERS_DOMAIN_TRANSLATE, "CMSS");
            tdpServiceClientList = tdpServiceClientRepository.findAllByProductTypeAndClientTipoDocAndClientDocument(LegadosConstants.PARQUE_CMS, documentType, documentNumber);

            if (tdpServiceClientList != null && tdpServiceClientList.size() > 0) {
                Date fechaVencimiento = tdpServiceClientList.get(0).getAuditoriaModify();
                Calendar c = Calendar.getInstance();
                if (fechaVencimiento != null) {
                    c.setTime(fechaVencimiento);
                    c.add(Calendar.DATE, syncronizarDay);
                    fechaVencimiento = c.getTime();
                    if (dateHoy.after(fechaVencimiento)) {
                        consultaCms = true;
                    }
                }
            } else {
                consultaCms = true;
            }
            if (consultaCms) {
                try {
                    ServicesApiCmsRequestBody apiCmsRequestBody = new ServicesApiCmsRequestBody();
                    apiCmsRequestBody.setTipoDocumento(documentType);
                    apiCmsRequestBody.setNumeroDocumento(documentNumber);
                    ApiResponse<ServicesApiCmsResponseBody> objCms = getParqueCms(apiCmsRequestBody,orderId);
                    if ((Constants.RESPONSE).equals(objCms.getHeaderOut().getMsgType())) {
                        for (TdpServiceClient serviceTypeAtis : tdpServiceClientList) {
                            if (serviceTypeAtis.getServiceEstadoCode().equalsIgnoreCase(LegadosConstants.PARQUE_1050)) {
                                serviceTypeAtis.setAuditoriaModify(dateHoy);
                                tdpServiceClientRepository.save(serviceTypeAtis);
                            }
                        }
                        for (ServicesApiCmsResponseBodyDetail obj : objCms.getBodyOut().getDetalleParque()) {
                            TdpServiceClient tdpServiceClientCMS = new TdpServiceClient();
                            tdpServiceClientCMS.setAuditoriaCreate(dateHoy);
                            if (tdpServiceClientList.size() > 0) {
                                for (TdpServiceClient serviceTypeCms : tdpServiceClientList) {
                                    if (serviceTypeCms.getProductService().equalsIgnoreCase(obj.getCodigoServicio())) {
                                        tdpServiceClientCMS = serviceTypeCms;
                                        break;
                                    }
                                }
                            }

                            tdpServiceClientCMS.setProductType(LegadosConstants.PARQUE_CMS);
                            tdpServiceClientCMS.setProductService(obj.getCodigoServicio());
                            tdpServiceClientCMS.setProductPs(obj.getCodigoProducto());
                            tdpServiceClientCMS.setProductoDescripcion(obj.getDescripcionProducto());
                            tdpServiceClientCMS.setProductOrigen(obj.getTipoOrigen());
                            tdpServiceClientCMS.setProductLinea(null);
                            tdpServiceClientCMS.setProductInternet(null);
                            tdpServiceClientCMS.setProductTv(null);

                            tdpServiceClientCMS.setClientTipoDoc(documentType);
                            tdpServiceClientCMS.setClientDocument(documentNumber);
                            tdpServiceClientCMS.setClientCms(obj.getCodigoAbonado());

                            String departamento = obj.getDepartamento();
                            tdpServiceClientCMS.setDepartamentoCode(departamento.length() > 3 ? departamento.substring(0, 3) : "");
                            tdpServiceClientCMS.setDepartamento(departamento.length() > 3 ? departamento.substring(4, departamento.length()) : "");
                            String provincia = obj.getProvincia();
                            tdpServiceClientCMS.setProvinciaCode(provincia.length() > 3 ? provincia.substring(0, 3) : "");
                            tdpServiceClientCMS.setProvincia(provincia.length() > 3 ? provincia.substring(4, provincia.length()) : "");
                            String distrito = obj.getDistrito();
                            tdpServiceClientCMS.setDistritoCode(distrito.length() > 3 ? distrito.substring(0, 3) : "");
                            tdpServiceClientCMS.setDistrito(distrito.length() > 3 ? distrito.substring(4, distrito.length()) : "");
                            tdpServiceClientCMS.setCoordenadaX(new BigDecimal(obj.getCoordenadaX()));
                            tdpServiceClientCMS.setCoordenadaY(new BigDecimal(obj.getCoordenadaY()));
                            tdpServiceClientCMS.setDireccion(obj.getDireccion());

                            tdpServiceClientCMS.setServiceEstadoCode(obj.getEstado());
                            tdpServiceClientCMS.setServiceEstado(getDescriptionByElementScoring(obj.getEstado(), cmssParameterList));

                            tdpServiceClientCMS.setAuditoriaModify(dateHoy);

                            tdpServiceClientRepository.save(tdpServiceClientCMS);
                            if (tdpServiceClientList.size() > 0) {
                                for (TdpServiceClient serviceTypeList : tdpServiceClientList) {
                                    if (serviceTypeList.getProductService().equalsIgnoreCase(tdpServiceClientCMS.getProductService())) {
                                        serviceTypeList = tdpServiceClientCMS;
                                        break;
                                    }
                                }
                            } else {
                                tdpServiceClientList.add(tdpServiceClientCMS);
                            }
                        }
                    } else {
                        try {
                            if (objCms.getBodyOut().getClientException() != null)
                                if (objCms.getBodyOut().getClientException().getExceptionCode() == 1050) {
                                    TdpServiceClient tdpServiceClientCMS = new TdpServiceClient();
                                    tdpServiceClientCMS.setAuditoriaCreate(dateHoy);
                                    if (tdpServiceClientList.size() > 0) {
                                        for (TdpServiceClient serviceTypeCms : tdpServiceClientList) {
                                            if (serviceTypeCms.getProductService().equalsIgnoreCase(LegadosConstants.PARQUE_CMS + documentNumber)) {
                                                tdpServiceClientCMS = serviceTypeCms;
                                                break;
                                            }
                                        }
                                    }

                                    tdpServiceClientCMS.setProductType(LegadosConstants.PARQUE_CMS);
                                    tdpServiceClientCMS.setProductService(LegadosConstants.PARQUE_CMS + documentNumber);
                                    tdpServiceClientCMS.setProductPs(null);
                                    tdpServiceClientCMS.setProductoDescripcion("Error de negocio producido en la lógica del servicio invocado.");
                                    tdpServiceClientCMS.setProductOrigen(null);
                                    tdpServiceClientCMS.setProductLinea(null);
                                    tdpServiceClientCMS.setProductInternet(null);
                                    tdpServiceClientCMS.setProductTv(null);

                                    tdpServiceClientCMS.setClientTipoDoc(documentType);
                                    tdpServiceClientCMS.setClientDocument(documentNumber);
                                    tdpServiceClientCMS.setClientCms(null);

                                    tdpServiceClientCMS.setDepartamentoCode(null);
                                    tdpServiceClientCMS.setDepartamento(null);
                                    tdpServiceClientCMS.setProvinciaCode(null);
                                    tdpServiceClientCMS.setProvincia(null);
                                    tdpServiceClientCMS.setDistritoCode(null);
                                    tdpServiceClientCMS.setDistrito(null);
                                    tdpServiceClientCMS.setCoordenadaX(null);
                                    tdpServiceClientCMS.setCoordenadaY(null);
                                    tdpServiceClientCMS.setDireccion(null);

                                    tdpServiceClientCMS.setServiceEstadoCode("1050NW");
                                    tdpServiceClientCMS.setServiceEstado("NO DATA");

                                    tdpServiceClientCMS.setAuditoriaModify(dateHoy);

                                    tdpServiceClientRepository.save(tdpServiceClientCMS);
                                    if (tdpServiceClientList.size() > 0) {
                                        for (TdpServiceClient serviceTypeList : tdpServiceClientList) {
                                            if (serviceTypeList.getProductService().equalsIgnoreCase(tdpServiceClientCMS.getProductService())) {
                                                serviceTypeList = tdpServiceClientCMS;
                                                break;
                                            }
                                        }
                                    } else {
                                        tdpServiceClientList.add(tdpServiceClientCMS);
                                    }
                                }
                        } catch (Exception parques) {
                            logger.error("Error obtenerParqueCms Service. parques.", parques);
                        }
                    }
                } catch (Exception cmsService) {
                    logger.error("Error obtenerParqueCms Service. CmsService.", cmsService);
                }
            }
            List<TdpServiceClient> tdpServiceClientListTmp = new ArrayList<>();
            for (TdpServiceClient serviceType : tdpServiceClientList) {
                if (!LegadosConstants.PARQUE_1050.equalsIgnoreCase(serviceType.getServiceEstadoCode()))
                    tdpServiceClientListTmp.add(serviceType);
            }
            tdpServiceClientList = tdpServiceClientListTmp;
        } catch (Exception ex) {
            logger.error("Error obtenerParqueCms Service.", ex);
        }

        return tdpServiceClientList;
    }

    private String getDescriptionByElementScoring(String element, List<Parameters> cmssParameterList) {
        String description = "SIN VALOR";

        for (Parameters parameter : cmssParameterList) {
            if (element.equals(parameter.getElement())) {
                description = parameter.getStrValue();
            }
        }

        return description;
    }

    public String equivalenceDocument(String appiConnect, String documentType) {
        try {
            Parameters parametersDocument = parametersRepository.findOneByDomainAndCategoryAndElement("EQUIVALENCEDOCUMENT", appiConnect, documentType.toUpperCase());
            documentType = parametersDocument.getStrValue() == null ? documentType : parametersDocument.getStrValue();
        } catch (Exception equiv) {
            logger.error("Error EquivalenceDocument Service.", equiv);
        }
        return documentType;
    }
}
