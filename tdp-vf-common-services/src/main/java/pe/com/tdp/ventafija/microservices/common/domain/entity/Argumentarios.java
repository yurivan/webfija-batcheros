package pe.com.tdp.ventafija.microservices.common.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "argumentarios", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema
//@SequenceGenerator(name = "myseqcustomer", sequenceName = "ibmx_a07e6d02edaf552.customer_seq", catalog = "ibmx_a07e6d02edaf552", schema = "ibmx_a07e6d02edaf552", allocationSize = 1)

public class Argumentarios {

    @Id
    @Column
    private  Integer id;

    @Column(name = "canal")
    private String canal;

    @Column(name = "entidad")
    private String entidad;

    @Column(name = "nivel")
    private String nivel;

    @Column(name = "flujo")
    private String flujo;

    @Column(name = "mensaje")
    private String mensaje;

    @Column(name = "pantalla")
    private String pantalla;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getFlujo() {
        return flujo;
    }

    public void setFlujo(String flujo) {
        this.flujo = flujo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getPantalla() {
        return pantalla;
    }

    public void setPantalla(String pantalla) {
        this.pantalla = pantalla;
    }
}
