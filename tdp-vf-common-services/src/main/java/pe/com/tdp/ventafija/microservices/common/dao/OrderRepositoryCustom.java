package pe.com.tdp.ventafija.microservices.common.dao;

import pe.com.tdp.ventafija.microservices.common.domain.dto.SaleFileReport;
import pe.com.tdp.ventafija.microservices.common.domain.dto.SalesReport;
import pe.com.tdp.ventafija.microservices.common.domain.dto.UserOrderResponse;
import pe.com.tdp.ventafija.microservices.common.domain.entity.AgenVal;

import java.util.Date;
import java.util.List;


public interface OrderRepositoryCustom {

    /**
     * Retrives data that needs to be sent to HDEC
     *
     * @param orderId that will be queried
     * @return the data from query
     */
    List<Object[]> spRetriveHdecData(String orderId);

    /**
     * performs migration of data from relational schema to
     * a consolidated (visor)
     *
     * @param orderId the data that will be migrated
     */
    void spMigrateVisor(String orderId);

    /**
     * performs migration of data from relational schema to
     * a consolidated (visor)
     *
     * @param orderId the data that will be migrated
     * @status status to set in VISOR
     */
    void spMigrateVisor(String orderId, String status);

    /**
     * Retrives data for sales starting at startDate to presentDate
     *
     * @param startDate where the report starts
     * @return data found
     */

    //modificado para sprint 3
    List<SalesReport> loadReporteVentaData(Date startDate, String vendorId, String idTransaccion, String filterDni, String status);//Date

    /**
     * Retrives data for a sale
     *
     * @param orderId to search
     * @return sale report
     */
    SalesReport loadSalesReportByOrderId(String orderId);


    /*
     * Retrieves user sales information
     * @param id
     */
    List<UserOrderResponse> spGetUserOrders(String id, int numDays);

    /**
     * Sprint 3 - Consulta de ventas para la bandeja de pendientes de subir audios
     *
     * @param nombrePuntoVenta nombre del punto de venta
     * @param entidad          nombre de la entidad del punto de venta
     * @param codigoEstado     codigo de estado de subida de audio
     * @param codigoVendedor   codigo del vendedor responsable de la venta
     * @param dniCliente       dni del cliente
     * @param fechaInicio      fecha de inicio del rango de busqueda de la venta
     * @param fechaFin         fecha de fin del rango de busqueda de la venta
     * @return data found
     */
    List<SalesReport> loadBandejaVentasAudio(String nombrePuntoVenta, String entidad, String codigoEstado, String codigoVendedor, String buscarPorVendedor, String dniCliente, Date fechaInicio, Date fechaFin, String filtroRestringeHoras, String hour_minute);

	List<SaleFileReport> loadBandejaVentasArchivo(String codigoSupervisor, Date fechaInicio, Date fechaFin, String hour_minute);

    // Sprint 7... pendiente pasar spring data
    public boolean updateEstadoVisor(String estado, String orderId);

	public AgenVal updateAgentPendienteVisor(String orderId,String canal,String departamento,String tecnologia);

    List<SalesReport> loadBandejaVentas(String codigoVendedor, String codigoEstado, Date fechaInicio, Date fechaFin);//Date
}
