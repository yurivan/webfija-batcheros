package pe.com.tdp.ventafija.microservices.common.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.customer.*;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.constant.CustomerConstants;
import pe.com.tdp.ventafija.microservices.common.services.CustomerDataService;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;

import java.sql.Time;
import java.util.concurrent.TimeoutException;

@Service
public class CustomerServiceImpl implements CustomerDataService {

    private ApiHeaderConfig apiHeaderConfig;
    private ServiceCallEventsService serviceCallEventsService;

    // Detener contingencia stados de ATIS
    @Value("${contingencia.stop.estados.atis}")
    private String contingenciaStopEstadoAtis;
    // Detener contingencia stados de ATIS
    @Value("${contingencia.stop.estados.cms}")
    private String contingenciaStopEstadoCms;
    @Value("${tdp.api.cms.consultacliente.topealta}")
    private String topeAlta;
    @Value("${tdp.api.cms.consultacliente.topepark}")
    private String topePark;
    @Value("${tdp.api.cms.consultacliente.error}")
    private String errorMsg;
    @Autowired
    public CustomerServiceImpl(ApiHeaderConfig apiHeaderConfig, ServiceCallEventsService serviceCallEventsService) {
        this.apiHeaderConfig = apiHeaderConfig;
        this.serviceCallEventsService = serviceCallEventsService;
    }

    public String getMessageError(){
        return errorMsg == null ? "Error en el Servicio de Consulta de Deuda" : errorMsg;
    }

    public String getEstadoServicio(){
        return apiHeaderConfig.getCmsConsultaEstadoServicio().equals("") || apiHeaderConfig.getCmsConsultaEstadoServicio() == null ? "false" : apiHeaderConfig.getCmsConsultaEstadoServicio() ;
    }

    public String getTopeDeuda(){
        return (topeAlta == null || topeAlta.equals("") ? "0" : topeAlta) + "," +  (topePark == null || topePark.equals("") ? "0" : topePark);
    }

    public ApiResponse<CustomerDataResponseBody> getConsultaCustomer(CustomerDataRequestBody apiRequestBody) throws TimeoutException
    {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getCmsConsultaClienteUri())
                .setApiId(apiHeaderConfig.getCmsConsultaClienteApiId())
                .setApiSecret(apiHeaderConfig.getCmsConsultaClienteApiSecret())
                .setOperation(CustomerConstants.API_REQUEST_HEADER_OPERATION_STATUS_CMS)
                .setDestination(CustomerConstants.API_REQUEST_HEADER_DESTINATION_CMS)
                .build();

        ConsultaCodigoCustomer consultaCodigoCustomer = new ConsultaCodigoCustomer(config);
        ClientResult<ApiResponse<CustomerDataResponseBody>> result = consultaCodigoCustomer.post(apiRequestBody);
        if(!result.isSuccess()) {
            serviceCallEventsService.registerEvent(result.getEvent());
            throw new TimeoutException(result.getE().getMessage());
        }
        serviceCallEventsService.registerEvent(result.getEvent());

        return result.getResult();
    }

    public ApiResponse<CustomerLineResponseBody> getLineasCustomer(CustomerLineRequestBody apiRequestBody)  throws TimeoutException
    {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getCmsConsultaLineasUri())
                .setApiId(apiHeaderConfig.getCmsConsultaClienteApiId())
                .setApiSecret(apiHeaderConfig.getCmsConsultaClienteApiSecret())
                .setOperation(CustomerConstants.API_REQUEST_HEADER_OPERATION_STATUS_CMS)
                .setDestination(CustomerConstants.API_REQUEST_HEADER_DESTINATION_CMS)
                .build();

        ConsultaLineasCustomer consultaLineasCustomer = new ConsultaLineasCustomer(config);
        ClientResult<ApiResponse<CustomerLineResponseBody>> result = consultaLineasCustomer.post(apiRequestBody);
        if(!result.isSuccess()) {
            serviceCallEventsService.registerEvent(result.getEvent());
            throw new TimeoutException(result.getE().getMessage());
        }
        serviceCallEventsService.registerEvent(result.getEvent());

        return result.getResult();
    }

    public ApiResponse<CustomerReceiptsResponseBody> getDeudaCustomer(CustomerReceiptsRequestBody apiRequestBody)  throws TimeoutException
    {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getCmsConsultaRecibosUri())
                .setApiId(apiHeaderConfig.getCmsConsultaClienteApiId())
                .setApiSecret(apiHeaderConfig.getCmsConsultaClienteApiSecret())
                .setOperation(CustomerConstants.API_REQUEST_HEADER_OPERATION_STATUS_CMS)
                .setDestination(CustomerConstants.API_REQUEST_HEADER_DESTINATION_CMS)
                .build();

        ConsultaDeudaCustomer consultaLineasCustomer = new ConsultaDeudaCustomer(config);
        ClientResult<ApiResponse<CustomerReceiptsResponseBody>> result = consultaLineasCustomer.post(apiRequestBody);
        if(!result.isSuccess()) {
            serviceCallEventsService.registerEvent(result.getEvent());
            throw new TimeoutException(result.getE().getMessage());
        }
        serviceCallEventsService.registerEvent(result.getEvent());


        return result.getResult();
    }

    public ApiResponse<ConsultaRucDataResponseBody> getDataRuc(ConsultaRucDataRequestBody apiRequestBody)  throws TimeoutException
    {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getConsultaRucUri())
                .setApiId(apiHeaderConfig.getConsultaRucApiId())
                .setApiSecret(apiHeaderConfig.getConsultaRucApiSecret())
                .build();

        ConsultaRucCustomer consultaRucCustomer = new ConsultaRucCustomer(config);
        ApiResponse<ConsultaRucDataResponseBody> objRes = new ApiResponse<>();
        ConsultaRucDataResponseBody objResponse = new ConsultaRucDataResponseBody();
        String request = "";
        if(apiRequestBody.getNumDoc()!= null && !apiRequestBody.getNumDoc().equals("")) {
            request = "numDocumento=" + apiRequestBody.getNumDoc() + "&sisOrigen=" + (apiRequestBody.getSisOri().equals("") ? "1" : apiRequestBody.getSisOri());
        }
        ClientResult<ApiResponse<ConsultaRucDataResponseBody>> result = consultaRucCustomer.get(request);
        if(!result.isSuccess()) {
            serviceCallEventsService.registerEvent(result.getEvent());
            objRes.setBodyOut(objResponse);
            result.setResult(objRes);
        }
        serviceCallEventsService.registerEvent(result.getEvent());


        return result.getResult();
    }
}
