package pe.com.tdp.ventafija.microservices.common.services.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.dao.ParametersRepository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;
import pe.com.tdp.ventafija.microservices.common.services.ParametersService;

@Service
public class ParametersServiceImpl implements ParametersService {

    private static final Logger logger = LogManager.getLogger();

    @Autowired
    private ParametersRepository parametersRepository;

    @Override
    public Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element) {
        Parameters parameters = new Parameters();
        try {
            parameters = parametersRepository.findOneByDomainAndCategoryAndElement(domain, category, element);
        } catch (Exception ex) {
            logger.error("Error findOneByDomainAndCategoryAndElement Service.", ex);
        }
        return parameters;
    }

    @Override
    public String equivalenceDocument(String appiConnect, String documentType) {
        try {
            Parameters parametersDocument = parametersRepository.findOneByDomainAndCategoryAndElement("EQUIVALENCEDOCUMENT", appiConnect, documentType.toUpperCase());
            documentType = parametersDocument.getStrValue() == null ? documentType : parametersDocument.getStrValue();
        } catch (Exception ex) {
            logger.error("Error EquivalenceDocument Service.", ex);
        }
        return documentType;
    }

}