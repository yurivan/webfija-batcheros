package pe.com.tdp.ventafija.microservices.common.domain.dto;

public class OrderUpfrontBean {
    private String ordenVenta;
    private String estado;

    public OrderUpfrontBean() {
        super();
    }

    public String getOrdenVenta() {
        return ordenVenta;
    }

    public void setOrdenVenta(String ordenVenta) {
        this.ordenVenta = ordenVenta;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
