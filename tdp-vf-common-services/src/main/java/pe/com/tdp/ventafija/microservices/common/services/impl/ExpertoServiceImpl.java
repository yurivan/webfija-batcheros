package pe.com.tdp.ventafija.microservices.common.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.tdp.ventafija.microservices.common.clients.ClientConfig;
import pe.com.tdp.ventafija.microservices.common.clients.ClientException;
import pe.com.tdp.ventafija.microservices.common.clients.ClientResult;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiHeaderConfig;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;
import pe.com.tdp.ventafija.microservices.common.clients.experto.ExpertoClient;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoRequestBody;
import pe.com.tdp.ventafija.microservices.common.clients.experto.OffersApiExpertoResponseBody;
import pe.com.tdp.ventafija.microservices.common.services.ExpertoService;
import pe.com.tdp.ventafija.microservices.common.services.ServiceCallEventsService;
import pe.com.tdp.ventafija.microservices.common.util.Constants;

@Service
public class ExpertoServiceImpl implements ExpertoService {
    private ApiHeaderConfig apiHeaderConfig;
    private ServiceCallEventsService serviceCallEventsService;

    @Autowired
    public ExpertoServiceImpl(ApiHeaderConfig apiHeaderConfig, ServiceCallEventsService serviceCallEventsService) {
        this.apiHeaderConfig = apiHeaderConfig;
        this.serviceCallEventsService = serviceCallEventsService;
    }

    @Override
    public ApiResponse<OffersApiExpertoResponseBody> scoring(OffersApiExpertoRequestBody apiExpertoRequestBody, String orderId) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getExpertoObtenerExpertoUri())
                .setApiId(apiHeaderConfig.getExpertoObtenerExpertoApiId())
                .setApiSecret(apiHeaderConfig.getExpertoObtenerExpertoApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_EXPERTO)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_EXPERTO).build();

        ExpertoClient expertoClient = new ExpertoClient(config);
        ClientResult<ApiResponse<OffersApiExpertoResponseBody>> result = expertoClient.post(apiExpertoRequestBody);
        ApiResponse<OffersApiExpertoResponseBody> objExperto = result.getResult();

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
            result.getEvent().setOrderId(orderId);
        serviceCallEventsService.registerEvent(result.getEvent());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return objExperto;
    }

    //Sprint 24 RUC
    public ApiResponse<OffersApiExpertoResponseBody> scoringRucData(String json,String orderId, String tipo) {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl("")
                .setApiId("")
                .setApiSecret("")
                .setOperation("")
                .setDestination("").build();
        ExpertoClient expertoClient = new ExpertoClient(config);
        ClientResult<ApiResponse<OffersApiExpertoResponseBody>> result = expertoClient.getJson(json,tipo);
        ApiResponse<OffersApiExpertoResponseBody> objExperto = result.getResult();

        if (result.getEvent() != null && result.getEvent().getOrderId() == null)
            result.getEvent().setOrderId(orderId);
        serviceCallEventsService.registerEvent(result.getEvent());
        return objExperto;

    }

}
