package pe.com.tdp.ventafija.microservices.common.domain.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mt_azure_file_storage", schema = "ibmx_a07e6d02edaf552") //dochoaro se adiciona schema
public class MTAzureFileStorage {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "mtorder")
    private String mtOrder;

    @Column(name = "estado_tdpvisor")
    private String estadoTdpVisor;

    @Column(name = "codigo_pedido_tdpvisor")
    private String codigoPedidoTdpVisor;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_register")
    private Date dateRegister;

    @Column(name = "file_date")
    private String fileDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMtOrder() {
        return mtOrder;
    }

    public void setMtOrder(String mtOrder) {
        this.mtOrder = mtOrder;
    }

    public String getEstadoTdpVisor() {
        return estadoTdpVisor;
    }

    public void setEstadoTdpVisor(String estadoTdpVisor) {
        this.estadoTdpVisor = estadoTdpVisor;
    }

    public String getCodigoPedidoTdpVisor() {
        return codigoPedidoTdpVisor;
    }

    public void setCodigoPedidoTdpVisor(String codigoPedidoTdpVisor) {
        this.codigoPedidoTdpVisor = codigoPedidoTdpVisor;
    }

    public Date getDateRegister() {
        return dateRegister;
    }

    public void setDateRegister(Date dateRegister) {
        this.dateRegister = dateRegister;
    }

    public String getFileDate() {
        return fileDate;
    }

    public void setFileDate(String fileDate) {
        this.fileDate = fileDate;
    }
}
