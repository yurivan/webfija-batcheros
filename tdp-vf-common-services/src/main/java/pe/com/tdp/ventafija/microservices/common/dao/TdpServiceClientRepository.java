package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.common.domain.entity.TdpServiceClient;

import java.util.List;

@Repository
@Transactional
public interface TdpServiceClientRepository extends JpaRepository<TdpServiceClient, String> {

    List<TdpServiceClient> findAllByClientTipoDocAndClientDocument(String clientTipoDoc, String clientDocument);
    List<TdpServiceClient> findAllByProductTypeAndClientTipoDocAndClientDocument(String productType, String clientTipoDoc, String clientDocument);

}
