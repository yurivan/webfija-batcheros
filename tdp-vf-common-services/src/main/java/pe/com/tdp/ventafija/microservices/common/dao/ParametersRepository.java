package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Parameters;

import java.util.List;

@Repository
public interface ParametersRepository extends JpaRepository<Parameters, Integer>, ParametersRepositoryCustom {

	//Parameters findOneByDomainAndCategoryAndElement(String domain, String category, String element);

	List<Parameters> findByDomainAndCategoryAndElement(String domain, String category, String element);
	List<Parameters> findByDomainAndCategoryOrderByAuxiliarAsc(String domain, String category);

	List<Parameters> findByDomainAndCategoryAndStrfilter(String domain, String category, String strfilter);

	List<Parameters> findByDomainAndElementAndStrfilter(String domain, String element, String strfilter);


	List<Parameters> findByCategory(String category);



}
