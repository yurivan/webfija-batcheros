package pe.com.tdp.ventafija.microservices.common.services;

import pe.com.tdp.ventafija.microservices.common.clients.customer.*;
import pe.com.tdp.ventafija.microservices.common.clients.dto.ApiResponse;

import java.util.concurrent.TimeoutException;

public interface CustomerDataService {

    String getMessageError();

    String getEstadoServicio();

    String getTopeDeuda();

    ApiResponse<CustomerDataResponseBody> getConsultaCustomer(CustomerDataRequestBody apiRequestBody) throws TimeoutException;

    ApiResponse<CustomerLineResponseBody> getLineasCustomer(CustomerLineRequestBody apiRequestBody) throws TimeoutException;

    ApiResponse<CustomerReceiptsResponseBody> getDeudaCustomer(CustomerReceiptsRequestBody apiRequestBody) throws TimeoutException;

    ApiResponse<ConsultaRucDataResponseBody> getDataRuc(ConsultaRucDataRequestBody apiRequestBody)  throws TimeoutException;
}
