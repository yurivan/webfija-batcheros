package pe.com.tdp.ventafija.microservices.common.domain.entity;


import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "tdp_automatizador_cupones", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema
public class Tdp_automatizador_cupones {

    @Id
    @Column
    private Integer id;

    @Column(name = "cupones_fecha")
    private Timestamp cupones_fecha;

    @Column(name = "cupones")
    private Integer cupones;

    @Column(name = "cupones_utilizados")
    private Integer cupones_utilizados;

    @Column(name = "category")
    private String category;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getCupones_fecha() {
        return cupones_fecha;
    }

    public void setCupones_fecha(Timestamp cupones_fecha) {
        this.cupones_fecha = cupones_fecha;
    }

    public Integer getCupones() {
        return cupones;
    }

    public void setCupones(Integer cupones) {
        this.cupones = cupones;
    }

    public Integer getCupones_utilizados() {
        return cupones_utilizados;
    }

    public void setCupones_utilizados(Integer cupones_utilizados) {
        this.cupones_utilizados = cupones_utilizados;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
