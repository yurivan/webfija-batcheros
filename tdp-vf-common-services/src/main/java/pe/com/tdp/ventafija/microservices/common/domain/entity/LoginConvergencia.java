package pe.com.tdp.ventafija.microservices.common.domain.entity;

import javax.persistence.*;

@Entity
@Table(name = "tdp_login_convergencia", schema = "ibmx_a07e6d02edaf552") //glazaror se adiciona schema
public class LoginConvergencia {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    @Column(name = "pto_venta")
    private String pto_venta;
    @Column(name = "canal")
    private String canal;
    @Column(name = "f_enable")
    private boolean f_enable;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPto_venta() {
        return pto_venta;
    }

    public void setPto_venta(String pto_venta) {
        this.pto_venta = pto_venta;
    }

    public String getCanal() {
        return canal;
    }

    public void setCanal(String canal) {
        this.canal = canal;
    }

    public boolean isF_enable() {
        return f_enable;
    }

    public void setF_enable(boolean f_enable) {
        this.f_enable = f_enable;
    }
}
