package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.LoginConvergencia;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Tokens;

@Repository
public interface LoginConvergenciaRepository extends JpaRepository<LoginConvergencia, Integer> {

    //@Modifying
    @Query( value = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_login_convergencia WHERE pto_venta = ?1",nativeQuery = true)
    LoginConvergencia findAccessByPtoVenta(String pto_venta);

}
