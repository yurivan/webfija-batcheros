package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pe.com.tdp.ventafija.microservices.common.domain.entity.MTAzureBlobStorage;

@Repository
public interface MTAzureBlobStorageRepository extends JpaRepository<MTAzureBlobStorage, Long> {

    @Modifying
    @Query(value = "INSERT INTO ibmx_a07e6d02edaf552.mt_azure_blob_storage(mtorder, " +
            "estado_tdpvisor, codigo_pedido_tdpvisor, date_register, blob_date)" +
            "VALUES (:mtOrder, :estado_tdpvisor, :codigo_pedido_tdpvisor, now(), :blob_date);", nativeQuery = true)
    @Transactional
    void insertMTAzureBlobStorage(@Param("mtOrder") String mtOrder,
                                  @Param("estado_tdpvisor") String estado_tdpvisor,
                                  @Param("codigo_pedido_tdpvisor") String codigo_pedido_tdpvisor,
                                  @Param("blob_date") String blob_date);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update MTAzureBlobStorage m set m.estadoTdpVisor = ?1, m.codigoPedidoTdpVisor = ?2 where m.mtOrder = ?3")
    void updateMTAzureBlobStorage(String estadoTdpVisor, String codigoPedidoTdpVisor, String mtOrder);

    @Query("select m from MTAzureBlobStorage m where m.mtOrder = ?1")
    MTAzureBlobStorage getMtOrderBlob(String mtOrder);
}
