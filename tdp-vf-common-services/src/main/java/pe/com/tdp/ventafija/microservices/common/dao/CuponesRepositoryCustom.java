package pe.com.tdp.ventafija.microservices.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pe.com.tdp.ventafija.microservices.common.domain.entity.Tdp_automatizador_cupones;

import java.util.List;

@Repository
public interface CuponesRepositoryCustom{


	 List<Tdp_automatizador_cupones>  findByDomainAndCategoryOrderByAuxiliarAsc(String category);


	Boolean updateCuponesUtilizados(Integer cupones_utilizados,Integer id);

}
