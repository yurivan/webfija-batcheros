package pe.com.tdp.ventafija.microservices.common.domain.dto;

import java.math.BigDecimal;
import java.util.Date;

public class SalesReport {
	private String orderId;
	private String dni;
	private String productName;
	private Date saleDate;
	private BigDecimal price;
	private BigDecimal promPrice;
	private BigDecimal discount;
	private Integer monthPeriod;
	private String lineType;
	private BigDecimal cashPrice;
	private String status; // estado en el sistema
	private String requestCode; // codigoPedido
	private String statusRequest; // estado en visor old estadoSolicitud
	private String cmsAtisStatus; // estado de la peticion a cms/atis
	
	private String clientName;
	private String commercialOperation;
	private String cancelDescription;
	private String enableDigitalInvoice;
	private String disableWhitePage;

	private String numeroCIP;
	private String estadoCIP;
	private String customerPhone;
  
  private String statusAudio;// sprint 3 - estado de subida del audio
	private String codigoVendedor;// sprint 3 - codigo de vendedor
	
	// Sprint 3 - se adicionan 7 propiedades para mostrar "svas adicionales" en el detalle de venta
	private String decosSD;
	private String decosHD;
	private String decosDVR;
	private String tvBlock;
	private String blockTV;
	private String svaInternet;
	private String svaLine;

	//Sprint 15
	private String id_transaccion;

	private String metpago;
	private String estado_cip;
	private String total_cip;

	//Nueva Mesa Fija - Sprint 1
	private String restringehoras;

	public String getId_transaccion() {
		return id_transaccion;
	}

	public void setId_transaccion(String id_transaccion) {
		this.id_transaccion = id_transaccion;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Date getSaleDate() {
		return saleDate;
	}

	public void setSaleDate(Date saleDate) {
		this.saleDate = saleDate;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getPromPrice() {
		return promPrice;
	}

	public void setPromPrice(BigDecimal promPrice) {
		this.promPrice = promPrice;
	}

	public BigDecimal getDiscount() {
		return discount;
	}

	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}

	public Integer getMonthPeriod() {
		return monthPeriod;
	}

	public void setMonthPeriod(Integer monthPeriod) {
		this.monthPeriod = monthPeriod;
	}

	public String getLineType() {
		return lineType;
	}

	public void setLineType(String lineType) {
		this.lineType = lineType;
	}

	public BigDecimal getCashPrice() {
		return cashPrice;
	}

	public void setCashPrice(BigDecimal cashPrice) {
		this.cashPrice = cashPrice;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getCmsAtisStatus() {
		return cmsAtisStatus;
	}

	public void setCmsAtisStatus(String cmsAtisStatus) {
		this.cmsAtisStatus = cmsAtisStatus;
	}

	public String getRequestCode() {
		return requestCode;
	}

	public void setRequestCode(String requestCode) {
		this.requestCode = requestCode;
	}

	public String getStatusRequest() {
		return statusRequest;
	}

	public void setStatusRequest(String statusRequest) {
		this.statusRequest = statusRequest;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getCommercialOperation() {
		return commercialOperation;
	}

	public void setCommercialOperation(String commercialOperation) {
		this.commercialOperation = commercialOperation;
	}

	public String getCancelDescription() {
		return cancelDescription;
	}

	public void setCancelDescription(String cancelDescription) {
		this.cancelDescription = cancelDescription;
	}

	public String getEnableDigitalInvoice() {
		return enableDigitalInvoice;
	}

	public void setEnableDigitalInvoice(String enableDigitalInvoice) {
		this.enableDigitalInvoice = enableDigitalInvoice;
	}

	public String getDisableWhitePage() {
		return disableWhitePage;
	}

	public void setDisableWhitePage(String disableWhitePage) {
		this.disableWhitePage = disableWhitePage;
	}

	public String getNumeroCIP() { return numeroCIP; }

	public void setNumeroCIP(String numeroCIP) { this.numeroCIP = numeroCIP; }

	public String getEstadoCIP() { return estadoCIP; }

	public void setEstadoCIP(String estadoCIP) { this.estadoCIP = estadoCIP; }

	public String getCustomerPhone() {	return customerPhone;  }

	public void setCustomerPhone(String customerPhone) { this.customerPhone = customerPhone; }
  
  
  // Sprint 3 - se adicionan 7 propiedades para mostrar "svas adicionales" en el detalle de venta
  public String getStatusAudio() {
		return statusAudio;
	}

	public void setStatusAudio(String statusAudio) {
		this.statusAudio = statusAudio;
	}

	public String getCodigoVendedor() {
		return codigoVendedor;
	}

	public void setCodigoVendedor(String codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}

	public String getDecosSD() {
		return decosSD;
	}

	public void setDecosSD(String decosSD) {
		this.decosSD = decosSD;
	}

	public String getDecosHD() {
		return decosHD;
	}

	public void setDecosHD(String decosHD) {
		this.decosHD = decosHD;
	}

	public String getDecosDVR() {
		return decosDVR;
	}

	public void setDecosDVR(String decosDVR) {
		this.decosDVR = decosDVR;
	}

	public String getTvBlock() {
		return tvBlock;
	}

	public void setTvBlock(String tvBlock) {
		this.tvBlock = tvBlock;
	}

	public String getBlockTV() {
		return blockTV;
	}

	public void setBlockTV(String blockTV) {
		this.blockTV = blockTV;
	}

	public String getSvaInternet() {
		return svaInternet;
	}

	public void setSvaInternet(String svaInternet) {
		this.svaInternet = svaInternet;
	}

	public String getSvaLine() {
		return svaLine;
	}

	public void setSvaLine(String svaLine) {
		this.svaLine = svaLine;
	}

	public String getMetpago() {
		return metpago;
	}

	public void setMetpago(String metpago) {
		this.metpago = metpago;
	}

	public String getEstado_cip() {
		return estado_cip;
	}

	public void setEstado_cip(String estado_cip) {
		this.estado_cip = estado_cip;
	}

	public String getTotal_cip() {
		return total_cip;
	}

	public void setTotal_cip(String total_cip) {
		this.total_cip = total_cip;
	}

	public String getRestringehoras() {
		return restringehoras;
	}

	public void setRestringehoras(String restringehoras) {
		this.restringehoras = restringehoras;
	}
}
