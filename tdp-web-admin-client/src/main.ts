require('./style.less');
// import './app/js/ie10-viewport-bug-workaround'

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule }              from './app.module';

platformBrowserDynamic().bootstrapModule(AppModule);
