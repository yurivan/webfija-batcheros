import {Component} from '@angular/core';
import {Router,NavigationEnd} from '@angular/router';

import {AuthService} from './app/services/auth.service';

@Component({
  selector: 'my-app',
  templateUrl: './app.template.html'
})
export class AppComponent {
  currentUser:any;
  subscription:any;
  grantedAccess:any;
      constructor(private router:Router, private authService:AuthService) {
        var _this = this;
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.currentUser = this.currentUser == null ? {} : this.currentUser;
        this.subscription = authService.eventObservable.subscribe(function (x) {
          _this.onLogin();
        });
        router.events.filter(event => event instanceof NavigationEnd)
        .subscribe((event : NavigationEnd) => {
          window.scroll(0, 0)
        })
      }

      logout() {
        this.authService.logout();
        this.currentUser = {};
        this.router.navigate(["/login"]);
      }

      onLogin () {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser == null) {
          this.logout();
        }else{
          this.grantedAccess = this.authService.grantedAccess;
        }
      }
    }