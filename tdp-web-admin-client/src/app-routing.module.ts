import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';

import { AuthGuard } from './app/auth-guard.injectable';
import { WelcomeComponent } from './app/welcome/welcome.component';
import { ReportComponent } from './app/report/report.component';
import { ReportxMotorizadoComponent } from './app/reportxmotorizado/reportxmotorizado.component';
import { AdministrationComponent } from './app/administration/administration.component';
import { LoginComponent } from './app/auth/login.component';
import { UploadComponent } from './app/upload/upload.component';
import { CatalogComponent } from './app/catalog/catalog.component';
import { CatalogUploadComponent } from './app/catalog/catalogUpload.component';
import { CatalogDetailComponent } from './app/catalog/catalogDetail.component';
import { ServiceCallEventsComponent } from './app/administration/servicecallevents.component';
import { ServiceCallEventsDetailComponent } from './app/administration/servicecalleventsdetail.component';
import { ProfileComponent } from './app/profile/profile.component';
import { ProfileScreenComponent } from './app/profile/profilescreen.component';
import { ScreenComponent } from './app/profile/screen.component';
import { SvaComponent } from './app/sva/sva.component';
import { SvaUploadComponent } from './app/sva/svaUpload.component';
import { SvaDetailComponent } from './app/sva/svaDetail.component';

import { SalesAgentComponent } from './app/salesAgent/salesAgent.component';
import { SalesAgentCreateComponent } from './app/salesAgent/salesAgentCreate.component';
import { SalesAgentUpdateComponent } from './app/salesAgent/salesAgentUpdate.component';
import { SalesAgentUploadComponent } from './app/salesAgent/salesAgentUpload.component';
import { SalesAgentWebUploadComponent } from './app/salesAgent/salesAgentwebUpload.component';
import { SalesAgentDetailComponent } from './app/salesAgent/salesAgentDetail.component';

import { SalesAgentNivelesComponent } from './app/salesAgent/salesAgentNiveles.component';
import { SalesAgentNivelesDetailComponent } from './app/salesAgent/salesAgentNivelesDetail.component';
import { SalesAgentNivelesCreateComponent } from './app/salesAgent/salesAgentNivelesCreate.component';
import { SalesAgentNivelesUpdateComponent } from './app/salesAgent/salesAgentNivelesUpdate.component';
import { SalesAgentNivelesWebUploadComponent } from './app/salesAgent/salesAgentNivelesWebUpload.component';


import { SalesAgentSupervisoresComponent } from './app/salesAgent/salesAgentSupervisores.component';
import { SalesAgentSupervisoresDetailComponent } from './app/salesAgent/salesAgentSupervisoresDetail.component';
import { SalesAgentSupervisoresCreateComponent } from './app/salesAgent/salesAgentSupervisoresCreate.component';
import { SalesAgentSupervisoresUpdateComponent } from './app/salesAgent/salesAgentSupervisoresUpdate.component';
import { SalesAgentSupervisoresWebUploadComponent } from './app/salesAgent/salesAgentSupervisoresWebUpload.component';


import { UsersComponent } from './app/administration/users.component';
import { UserRegisterComponent } from './app/administration/userregister.component';
import { UserEditarComponent } from './app/administration/usereditar.component';
import { configofferingComponent } from './app/configoffering/configoffering.component';
import { AdminConfigComponent } from './app/administrationconfig/adminconfig.component';
import { ReviewContractComponent } from './app/reviewcontract/reviewcontract.component'
import { ListContractComponent } from './app/listcontract/listcontract.component';
import { EditContractComponent } from './app/reviewcontract/editcontract.component';
import { ContractPoliciesComponent } from './app/policies/contractpolicies.component';
import { ConfigDocumentsComponent } from './app/configdocuments/configdocuments.component';
import { ListMessageComponent } from './app/listmessage/listmessage.component';
import { EditMessageComponent } from './app/reviewmessage/editmessage.component';
import { ReviewMessageComponent } from './app/reviewmessage/reviewmessage.component';
import { CatalogxSalesAgentComponent } from './app/catalogxsalesagent/catalogxsalesagent.component';
import { CatalogxSalesAgentUploadComponent } from './app/catalogxsalesagent/catalogxsalesagentUpload.component';
import { CatalogxSalesAgentDetailComponent } from './app/catalogxsalesagent/catalogxsalesagentDetail.component';
import { CatalogxMotorizadoComponent } from './app/catalogxmotorizado/catalogxmotorizado.component';
import { CatalogxMotorizadoUploadComponent } from './app/catalogxmotorizado/catalogxmotorizadoUpload.component';
import { CatalogxMotorizadoDetailComponent } from './app/catalogxmotorizado/catalogxmotorizadoDetail.component';

// Sprint 12
import { EquivalentComponent } from './app/configequivalent/equivalent.component';
import { EquivalentDetailComponent } from './app/configequivalent/equivalentDetail.component';
import { EquivalentCreateComponent } from './app/configequivalent/equivalentCreate.component';
// Sprint 14
import { ParamTrazabilidadComponent } from './app/paramtrazabilidad/paramtrazabilidad.component';

import { VisorLoadLogsComponent } from './app/administration/visorloadlogs.component';

import { AgendamientoConfigComponent } from './app/agendamiento/agendamientoConfig.component';
import { AgendamientoCanalesComponent } from './app/agendamiento/agendamientoCanales.component';
import { AgendamientoTecnologiasComponent } from './app/agendamiento/agendamientoTecnologias.component';
import { AgendamientoDepartamentosComponent } from './app/agendamiento/agendamientoDepartamentos.component';
import { AgendamientoCuponesComponent } from './app/agendamiento/agendamientoCupones.component';

import { salesconditioncomponent } from './app/salescondition/salescondition.component';
import { EditSalesConditionComponent } from './app/editsalescondition/editsalescondition.component';
import { FtthComponent } from './app/ftth/ftth.component';
import { FtthAddComponent } from './app/ftth/ftthadd.component';
import { FtthDetailComponent } from './app/ftth/ftthdetail.component';

import { ArgumentariosConfigComponent } from './app/argumentarios/argumentariosConfig.component';
import { ArgumentariosTiendaComponent } from './app/argumentarios/argumentariosTienda.component';
import { ArgumentariosCallCenterComponent } from './app/argumentarios/argumentariosCallCenter.component';
import { ArgumentariosOutComponent } from './app/argumentarios/argumentariosOut.component.';
import { profilesComponent } from './app/profiles/profiles.component';
import { profilesAddComponent } from './app/profiles/profilesAdd.component';
import { profilesEditComponent } from './app/profiles/profilesEdit.component';
import { loginConvergenciaComponent } from './app/loginConvergencia/loginConvergencia.component';
import { loginConvergenciaAddComponent } from './app/loginConvergencia/loginConvergenciaAdd.component';
import { loginConvergenciaEditComponent } from './app/loginConvergencia/loginConvergenciaEdit.component';

import { AdminUserConfigComponent } from './app/configUserAdmin/adminUserConfig.component';
import { AdminUserPerfilComponent } from './app/configUserAdmin/adminUserPerfil.component';
import { AdminUserOpcionesComponent } from './app/configUserAdmin/adminUserOpciones.component';
import { AdminUserPerfilxOpcionComponent } from './app/configUserAdmin/adminUserPerfilxOpcion.component.';
import { NumerosAsociados } from './app/numerosasociados/numerosasociados.component';
import { NumerosAsociadosAdd } from './app/numerosasociados/numerosasociadosadd.component';
import { NumerosAsociadosUpdate } from './app/numerosasociados/numerosasociadosedit.component';

const APP_ROUTES: Routes = [
  { path: '', redirectTo: '/welcome', pathMatch: 'full' },
  { path: 'welcome', component: WelcomeComponent, canActivate: [AuthGuard] },
  { path: 'reports', component: ReportComponent, canActivate: [AuthGuard] },
  { path: 'reportxmotorizados', component: ReportxMotorizadoComponent, canActivate: [AuthGuard] },
  { path: 'administration', component: AdministrationComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'upload', component: UploadComponent, canActivate: [AuthGuard] },
  { path: 'catalog', component: CatalogComponent, canActivate: [AuthGuard] },
  { path: 'catalog/upload', component: CatalogUploadComponent, canActivate: [AuthGuard] },
  { path: 'catalog/:id', component: CatalogDetailComponent, canActivate: [AuthGuard] },
  { path: 'administration/servicecallevents', component: ServiceCallEventsComponent, canActivate: [AuthGuard] },
  { path: 'administration/servicecallevents/:id', component: ServiceCallEventsDetailComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'profilescreen', component: ProfileScreenComponent, canActivate: [AuthGuard] },
  { path: 'screen', component: ScreenComponent, canActivate: [AuthGuard] },
  { path: 'sva', component: SvaComponent, canActivate: [AuthGuard] },
  { path: 'sva/upload', component: SvaUploadComponent, canActivate: [AuthGuard] },
  { path: 'sva/:id', component: SvaDetailComponent, canActivate: [AuthGuard] },

  { path: 'salesAgent', component: SalesAgentComponent, canActivate: [AuthGuard] },
  { path: 'salesAgent/detail/:codatis', component: SalesAgentDetailComponent, canActivate: [AuthGuard] },
  { path: 'salesAgent/create', component: SalesAgentCreateComponent, canActivate: [AuthGuard] },
  { path: 'salesAgent/update/:codatis', component: SalesAgentUpdateComponent, canActivate: [AuthGuard] },
  { path: 'salesAgent/upload', component: SalesAgentUploadComponent, canActivate: [AuthGuard] },
  { path: 'salesAgent/web/upload', component: SalesAgentWebUploadComponent, canActivate: [AuthGuard] },

  { path: 'salesAgentNiveles', component: SalesAgentNivelesComponent, canActivate: [AuthGuard] },
  { path: 'salesAgentNiveles/detail/:codatis', component: SalesAgentNivelesDetailComponent, canActivate: [AuthGuard] },
  { path: 'salesAgentNiveles/create', component: SalesAgentNivelesCreateComponent, canActivate: [AuthGuard] },
  { path: 'salesAgentNiveles/update/:codatis', component: SalesAgentNivelesUpdateComponent, canActivate: [AuthGuard] },
  { path: 'salesAgentNiveles/web/upload', component: SalesAgentNivelesWebUploadComponent, canActivate: [AuthGuard] },

  { path: 'salesAgentSupervisores', component: SalesAgentSupervisoresComponent, canActivate: [AuthGuard] },
  { path: 'salesAgentSupervisores/detail/:codatis', component: SalesAgentSupervisoresDetailComponent, canActivate: [AuthGuard] },
  { path: 'salesAgentSupervisores/create', component: SalesAgentSupervisoresCreateComponent, canActivate: [AuthGuard] },
  { path: 'salesAgentSupervisores/update/:codatis', component: SalesAgentSupervisoresUpdateComponent, canActivate: [AuthGuard] },
  { path: 'salesAgentSupervisores/web/upload', component: SalesAgentSupervisoresWebUploadComponent, canActivate: [AuthGuard] },


  { path: 'administration/users', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'administration/users/new', component: UserRegisterComponent, canActivate: [AuthGuard] },
  { path: 'administration/users/:id', component: UserEditarComponent, canActivate: [AuthGuard] },
  { path: 'configoffering', component: configofferingComponent, canActivate: [AuthGuard] },
  { path: 'setsecurityvalues', component: AdminConfigComponent, canActivate: [AuthGuard] },
  { path: 'contractweb/:id', component: ReviewContractComponent, canActivate: [AuthGuard] },
  { path: 'listcontract', component: ListContractComponent, canActivate: [AuthGuard] },
  { path: 'editcontract/:id', component: EditContractComponent, canActivate: [AuthGuard] },
  { path: 'contractpolicies', component: ContractPoliciesComponent, canActivate: [AuthGuard] },
  { path: 'adminconfig', component: ConfigDocumentsComponent, canActivate: [AuthGuard] },
  { path: 'listmessage', component: ListMessageComponent, canActivate: [AuthGuard] },
  { path: 'editmessage/:id', component: EditMessageComponent, canActivate: [AuthGuard] },
  { path: 'messageweb/:id', component: ReviewMessageComponent, canActivate: [AuthGuard] },
  { path: 'catalogxsalesagent', component: CatalogxSalesAgentComponent, canActivate: [AuthGuard] },
  { path: 'catalogxsalesagent/upload', component: CatalogxSalesAgentUploadComponent, canActivate: [AuthGuard] },
  { path: 'catalogxsalesagent/:id', component: CatalogxSalesAgentDetailComponent, canActivate: [AuthGuard] },
  { path: 'catalogxmotorizado', component: CatalogxMotorizadoComponent, canActivate: [AuthGuard] },
  { path: 'catalogxmotorizado/upload', component: CatalogxMotorizadoUploadComponent, canActivate: [AuthGuard] },
  { path: 'catalogxmotorizado/:id', component: CatalogxMotorizadoDetailComponent, canActivate: [AuthGuard] },

  // Sprint 12
  { path: 'equivalent/:opcion', component: EquivalentComponent, canActivate: [AuthGuard] },
  { path: 'equivalent/detail/:id/:etiquetaCampoDestino/:etiquetaCampoOrigen/:flagTipif', component: EquivalentDetailComponent, canActivate: [AuthGuard] },
  { path: 'equivalent/create/:equivalencia/:etiquetaCampoDestino/:etiquetaCampoOrigen/:flagTipif', component: EquivalentCreateComponent, canActivate: [AuthGuard] },
  // Sprint 14
  { path: 'setparamtrazabilidad', component: ParamTrazabilidadComponent, canActivate: [AuthGuard] },
  { path: 'visorloadlogs', component: VisorLoadLogsComponent, canActivate: [AuthGuard] },

  // Agendamiento
  { path: 'agendamiento/config', component: AgendamientoConfigComponent, canActivate: [AuthGuard] },
  { path: 'agendamiento/canales', component: AgendamientoCanalesComponent, canActivate: [AuthGuard] },
  { path: 'agendamiento/tecnologias', component: AgendamientoTecnologiasComponent, canActivate: [AuthGuard] },
  { path: 'agendamiento/departamentos', component: AgendamientoDepartamentosComponent, canActivate: [AuthGuard] },
  { path: 'agendamiento/cupones', component: AgendamientoCuponesComponent, canActivate: [AuthGuard] },

  //Sales Condition
  { path: 'salescondition', component: salesconditioncomponent, canActivate: [AuthGuard]},
  { path: 'editsalescondition', component: EditSalesConditionComponent, canActivate: [AuthGuard]},
  { path: 'ftth', component: FtthComponent, canActivate: [AuthGuard]},
  { path: 'ftth/new', component: FtthAddComponent, canActivate: [AuthGuard]},
  { path: 'ftth/:id', component: FtthDetailComponent, canActivate: [AuthGuard]},

  //Argumentarios
  { path: 'argumentarios/config', component: ArgumentariosConfigComponent, canActivate: [AuthGuard] },
  { path: 'argumentarios/tienda', component: ArgumentariosTiendaComponent, canActivate: [AuthGuard] },
  { path: 'argumentarios/callcenter', component: ArgumentariosCallCenterComponent, canActivate: [AuthGuard] },
  { path: 'argumentarios/out', component: ArgumentariosOutComponent, canActivate: [AuthGuard] },
  
  { path: 'permisos-perfiles', component: profilesComponent, canActivate: [AuthGuard] },
  { path: 'permisos-perfiles/add', component: profilesAddComponent, canActivate: [AuthGuard] },
  { path: 'permisos-perfiles/edit/:id', component: profilesEditComponent, canActivate: [AuthGuard] },

  { path: 'login-convergencia', component: loginConvergenciaComponent, canActivate: [AuthGuard] },
  { path: 'login-convergencia/add/:type', component: loginConvergenciaAddComponent, canActivate: [AuthGuard] },
  { path: 'login-convergencia/edit/:id', component: loginConvergenciaEditComponent, canActivate: [AuthGuard] },
   //Config UserAdmin
   { path: 'adminUser/config', component: AdminUserConfigComponent, canActivate: [AuthGuard] },
   { path: 'adminUser/perfil', component: AdminUserPerfilComponent, canActivate: [AuthGuard] },
   { path: 'adminUser/opciones', component: AdminUserOpcionesComponent, canActivate: [AuthGuard] },
   { path: 'adminUser/opcionesxperfil', component: AdminUserPerfilxOpcionComponent, canActivate: [AuthGuard] },
  
  
   { path: 'seguridadsocio', component: NumerosAsociados, canActivate: [AuthGuard] },
   { path: 'seguridadsocio/create', component: NumerosAsociadosAdd, canActivate: [AuthGuard] },
   { path: 'seguridadsocio/edit/:id', component: NumerosAsociadosUpdate, canActivate: [AuthGuard] },

   
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }