import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './app/welcome/welcome.component';
import { ReportComponent } from './app/report/report.component';
import { ReportxMotorizadoComponent } from './app/reportxmotorizado/reportxmotorizado.component';
import { AdministrationComponent } from './app/administration/administration.component';
import { LoginComponent } from './app/auth/login.component';
import { UploadComponent } from './app/upload/upload.component';
import { CatalogComponent } from './app/catalog/catalog.component';
import { CatalogUploadComponent } from './app/catalog/catalogUpload.component';
import { CatalogDetailComponent } from './app/catalog/catalogDetail.component';
import { ServiceCallEventsComponent } from './app/administration/servicecallevents.component';
import { ServiceCallEventsDetailComponent } from './app/administration/servicecalleventsdetail.component';
import { ProfileComponent } from './app/profile/profile.component';
import { ProfileScreenComponent } from './app/profile/profilescreen.component';
import { ScreenComponent } from './app/profile/screen.component';
import { SvaComponent } from './app/sva/sva.component';
import { SvaUploadComponent } from './app/sva/svaUpload.component';
import { SvaDetailComponent } from './app/sva/svaDetail.component';

import { SalesAgentComponent } from './app/salesAgent/salesAgent.component';
import { SalesAgentCreateComponent } from './app/salesAgent/salesAgentCreate.component';
import { SalesAgentUpdateComponent } from './app/salesAgent/salesAgentUpdate.component';
import { SalesAgentUploadComponent } from './app/salesAgent/salesAgentUpload.component';
import { SalesAgentWebUploadComponent } from './app/salesAgent/salesAgentwebUpload.component';
import { SalesAgentDetailComponent } from './app/salesAgent/salesAgentDetail.component';

import { SalesAgentNivelesComponent } from './app/salesAgent/salesAgentNiveles.component';
import { SalesAgentNivelesDetailComponent } from './app/salesAgent/salesAgentNivelesDetail.component';
import { SalesAgentNivelesCreateComponent } from './app/salesAgent/salesAgentNivelesCreate.component';
import { SalesAgentNivelesUpdateComponent } from './app/salesAgent/salesAgentNivelesUpdate.component';
import { SalesAgentNivelesWebUploadComponent } from './app/salesAgent/salesAgentNivelesWebUpload.component';

import { SalesAgentSupervisoresComponent } from './app/salesAgent/salesAgentSupervisores.component';
import { SalesAgentSupervisoresDetailComponent } from './app/salesAgent/salesAgentSupervisoresDetail.component';
import { SalesAgentSupervisoresCreateComponent } from './app/salesAgent/salesAgentSupervisoresCreate.component';
import { SalesAgentSupervisoresUpdateComponent } from './app/salesAgent/salesAgentSupervisoresUpdate.component';
import { SalesAgentSupervisoresWebUploadComponent } from './app/salesAgent/salesAgentSupervisoresWebUpload.component';

import { UsersComponent } from './app/administration/users.component';
import { UserRegisterComponent } from './app/administration/userregister.component';
import { UserEditarComponent } from './app/administration/usereditar.component';

import { configofferingComponent } from "./app/configoffering/configoffering.component";
import { ConfigofferingService } from "./app/services/configoffering.service";

import { AppRoutingModule } from './app-routing.module';
import { HttpService } from './app/services/http.service';
import { UserService } from './app/services/user.service';
import { AuthService } from './app/services/auth.service';
import { CatalogService } from './app/services/catalog.service';
import { CatalogxSalesAgentService } from './app/services/catalogxsalesagent.service';
import { CatalogxMotorizadoService } from './app/services/catalogxmotorizado.service';
import { ServiceCallEventsService } from './app/services/servicecallevents.service';
import { ProfileService } from './app/services/profile.service';
import { ScreenService } from './app/services/screen.service';
import { ProfileScreenService } from './app/services/profilescreen.service';
import { profilesService } from './app/services/profiles.service';

import { UploadService } from './app/services/upload.service';
import { SvaService } from './app/services/sva.service';
import { SalesAgentService } from './app/services/salesAgent.service';
import { NumberAgentService } from './app/services/numberAgent.service';

import { ReportService } from './app/services/report.service';
import { ReportxMotorizadoService } from './app/services/reportxmotorizado.service';
import { AuthGuard } from './app/auth-guard.injectable';

import { PipeSearchProductType } from './app/catalog/filterProductType.pipe';
import { PipeSearchProductCategory } from './app/catalog/filterProductCategory.pipe';
import { PipeSearchProductName } from './app/catalog/filterProductName.pipe';
import { PipeSearchLastName } from './app/salesAgent/filterLastName.pipe';
import { PipeSearchMotherLastName } from './app/salesAgent/filterMotherLastName.pipe';
import { PipeSearchChannel } from './app/salesAgent/filterChannel.pipe';
import { PipeSearchProductTypeCode } from './app/sva/filterProductTypeCode.pipe';
import { PipeSearchUnit } from './app/sva/filterUnit.pipe';
import { PipeSearchDescription } from './app/sva/filterDescription.pipe';
import { AdminConfigComponent } from './app/administrationconfig/adminconfig.component'

import "materialize-css";
import { MaterializeDirective } from "angular2-materialize";
import { ContractService } from './app/services/contract.service';
import { ListContractComponent } from './app/listcontract/listcontract.component';
import { ReviewContractComponent } from './app/reviewcontract/reviewcontract.component';
import { EditContractComponent } from './app/reviewcontract/editcontract.component';
import { ContractPoliciesComponent } from './app/policies/contractpolicies.component';
import { PolicyService } from './app/services/policy.service';
import { ConfigDocumentsComponent } from './app/configdocuments/configdocuments.component';
import { DocumentService } from './app/services/configdocuments.service';
import { ListMessageComponent } from './app/listmessage/listmessage.component';
import { MessageService } from './app/services/message.service';
import { EditMessageComponent } from './app/reviewmessage/editmessage.component';
import { ReviewMessageComponent } from './app/reviewmessage/reviewmessage.component';
import { CatalogxSalesAgentComponent } from './app/catalogxsalesagent/catalogxsalesagent.component';
import { CatalogxSalesAgentUploadComponent } from './app/catalogxsalesagent/catalogxsalesagentUpload.component';
import { CatalogxSalesAgentDetailComponent } from './app/catalogxsalesagent/catalogxsalesagentDetail.component';
import { CatalogxMotorizadoComponent } from './app/catalogxmotorizado/catalogxmotorizado.component';
import { CatalogxMotorizadoUploadComponent } from './app/catalogxmotorizado/catalogxmotorizadoUpload.component';
import { CatalogxMotorizadoDetailComponent } from './app/catalogxmotorizado/catalogxmotorizadoDetail.component';

// Sprint 12
import { EquivalentComponent } from './app/configequivalent/equivalent.component';
import { EquivalentDetailComponent } from './app/configequivalent/equivalentDetail.component';
import { EquivalentCreateComponent } from './app/configequivalent/equivalentCreate.component';
import { EquivalentService } from './app/services/equivalent.service';
// Sprint 14
import { ParamTrazabilidadComponent } from './app/paramtrazabilidad/paramtrazabilidad.component';

import { VisorLoadLogsComponent } from './app/administration/visorloadlogs.component';
import { VisorLoadLogsService } from './app/services/visorloadlogs.service';
import { loginConvergenciaService } from './app/services/loginConvergencia.service';

import { AgendamientoService } from './app/services/agendamiento.service';
import { AgendamientoConfigComponent } from './app/agendamiento/agendamientoConfig.component';
import { AgendamientoCanalesComponent } from './app/agendamiento/agendamientoCanales.component';
import { AgendamientoTecnologiasComponent } from './app/agendamiento/agendamientoTecnologias.component';
import { AgendamientoDepartamentosComponent } from './app/agendamiento/agendamientoDepartamentos.component';
import { AgendamientoCuponesComponent } from './app/agendamiento/agendamientoCupones.component';

import { salesconditioncomponent } from './app/salescondition/salescondition.component';
import { EditSalesConditionComponent } from './app/editsalescondition/editsalescondition.component';

import { FtthComponent } from './app/ftth/ftth.component';
import { FtthAddComponent } from './app/ftth/ftthadd.component';
import { FtthDetailComponent } from './app/ftth/ftthdetail.component';
import { FtthService } from './app/services/ftth.service';

import { ArgumentariosCallCenterComponent } from './app/argumentarios/argumentariosCallCenter.component';
import { ArgumentariosOutComponent } from './app/argumentarios/argumentariosOut.component.';
import { ArgumentariosConfigComponent } from './app/argumentarios/argumentariosConfig.component';
import { ArgumentariosTiendaComponent } from './app/argumentarios/argumentariosTienda.component';
import { ArgumentariosService } from './app/services/argumentarios.service';

import { profilesComponent } from './app/profiles/profiles.component';
import { profilesAddComponent } from './app/profiles/profilesAdd.component';
import { profilesEditComponent } from './app/profiles/profilesEdit.component';

import { loginConvergenciaComponent } from './app/loginConvergencia/loginConvergencia.component';
import { loginConvergenciaAddComponent } from './app/loginConvergencia/loginConvergenciaAdd.component';
import { loginConvergenciaEditComponent } from './app/loginConvergencia/loginConvergenciaEdit.component';

import { UbigeoService } from './app/services/ubigeo.service';

import { AdminUserConfigComponent } from './app/configUserAdmin/adminUserConfig.component';
import { AdminUserPerfilComponent } from './app/configUserAdmin/adminUserPerfil.component';
import { AdminUserOpcionesComponent } from './app/configUserAdmin/adminUserOpciones.component';
import { AdminUserPerfilxOpcionComponent } from './app/configUserAdmin/adminUserPerfilxOpcion.component.';
import { NumerosAsociados } from './app/numerosasociados/numerosasociados.component';
import { NumerosAsociadosAdd } from './app/numerosasociados/numerosasociadosadd.component';
import { NumerosAsociadosUpdate } from './app/numerosasociados/numerosasociadosedit.component';
import { ExcelService } from './app/services/excel.service';

import { UiSwitchModule } from 'angular2-ui-switch'

@NgModule({
  imports: [BrowserModule, FormsModule, HttpModule, AppRoutingModule,UiSwitchModule],
  declarations: [
    MaterializeDirective,
    AppComponent,
    PipeSearchProductType,
    PipeSearchProductCategory,
    PipeSearchProductName,
    WelcomeComponent,
    ReportComponent,
    ReportxMotorizadoComponent,
    AdministrationComponent,
    LoginComponent,
    UploadComponent,
    CatalogComponent,
    CatalogUploadComponent,
    CatalogDetailComponent,
    ServiceCallEventsComponent,
    ServiceCallEventsDetailComponent,
    ProfileComponent,
    ProfileScreenComponent,
    ScreenComponent,
    SvaComponent,
    SvaUploadComponent,
    SvaDetailComponent,
    SalesAgentComponent,
    SalesAgentCreateComponent,
    SalesAgentUpdateComponent,
    SalesAgentUploadComponent,
    SalesAgentDetailComponent,
    SalesAgentNivelesComponent,
    SalesAgentNivelesDetailComponent,
    SalesAgentNivelesCreateComponent,
    SalesAgentNivelesUpdateComponent,
    SalesAgentNivelesWebUploadComponent,
    SalesAgentSupervisoresComponent,
    SalesAgentSupervisoresDetailComponent,
    SalesAgentSupervisoresCreateComponent,
    SalesAgentSupervisoresUpdateComponent,
    SalesAgentSupervisoresWebUploadComponent,
    UsersComponent,
    UserRegisterComponent,
    UserEditarComponent,
    PipeSearchLastName,
    PipeSearchMotherLastName,
    PipeSearchChannel,
    PipeSearchProductTypeCode,
    PipeSearchUnit,
    PipeSearchDescription,
    SalesAgentWebUploadComponent,
    configofferingComponent,
    AdminConfigComponent,
    ReviewContractComponent,
    ListContractComponent,
    EditContractComponent,
    ContractPoliciesComponent,
    ConfigDocumentsComponent,
    ListMessageComponent,
    EditMessageComponent,
    ReviewMessageComponent,
    CatalogxSalesAgentComponent,
    CatalogxSalesAgentUploadComponent,
    CatalogxSalesAgentDetailComponent,
    CatalogxMotorizadoComponent,
    CatalogxMotorizadoUploadComponent,
    CatalogxMotorizadoDetailComponent,
    EquivalentComponent,
    EquivalentDetailComponent,
    EquivalentCreateComponent,
    ParamTrazabilidadComponent,
    VisorLoadLogsComponent,
    AgendamientoConfigComponent,
    AgendamientoCanalesComponent,
    AgendamientoTecnologiasComponent,
    AgendamientoDepartamentosComponent,
    AgendamientoCuponesComponent,
    salesconditioncomponent,
    EditSalesConditionComponent,
    FtthComponent,
    FtthAddComponent,
    FtthDetailComponent,
    ArgumentariosConfigComponent,
    ArgumentariosCallCenterComponent,
    ArgumentariosOutComponent,
    ArgumentariosTiendaComponent,
    profilesComponent,
    profilesAddComponent,
    profilesEditComponent,
    loginConvergenciaComponent,
    loginConvergenciaAddComponent,
    loginConvergenciaEditComponent,
    AdminUserConfigComponent,
    AdminUserPerfilComponent,
    AdminUserOpcionesComponent,
    AdminUserPerfilxOpcionComponent,
    NumerosAsociados,
    NumerosAsociadosAdd,NumerosAsociadosUpdate
  ],
  providers: [AuthGuard, AuthService, CatalogService, CatalogxSalesAgentService, CatalogxMotorizadoService,
    ServiceCallEventsService, ProfileService, ScreenService, UserService, ReportxMotorizadoService,
    ProfileScreenService, UploadService, SvaService, SalesAgentService, ReportService, EquivalentService, VisorLoadLogsService,
    HttpService, ConfigofferingService, ContractService, PolicyService, DocumentService, MessageService,
    AgendamientoService, FtthService,ArgumentariosService ,profilesService,UbigeoService,loginConvergenciaService,NumberAgentService,ExcelService,{
      provide: APP_INITIALIZER,
      useFactory: function (a) { return function () { return a.checkUserAccess(); } },
      deps: [AuthService],
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() { }
}