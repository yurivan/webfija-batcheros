import { Component, OnInit } from '@angular/core';
import {ReportxMotorizadoService} from '../services/reportxmotorizado.service'

declare var $:any;

@Component({
    moduleId: 'reportxmotorizado',
    selector: 'my-reportxmotorizado',
    templateUrl: 'reportxmotorizado.template.html'
})
export class ReportxMotorizadoComponent implements OnInit {

    private initDate : string ="";
    private finDate : string = "";
    private dateInitFormat : string = "";
    private dateFinFormat : string = "";
    private motorizado : string = '';
    private motorizados = [];
    private readonlyFinDate : boolean = true;
    private showAlertDateInit : boolean = false;
    
    constructor(private reportxMotorizadoService: ReportxMotorizadoService) { }

    showModalFiltro(){
         $('.modal').modal();
         $('#modal1').modal('open');
    }
    
    selectInitDate(){
        
        if(this.initDate){
           this.showAlertDateInit = false;
           this.readonlyFinDate = false;
        }else{
            this.finDate = "";
            this.readonlyFinDate = true;
        }    
    }
    
    downloadMotorizados() {
        this.showAlertDateInit = false;
        
        if(!this.initDate ) {
            this.showAlertDateInit = true;
            this.dateInitFormat = null; 
        } else {
                  
            let yyyy = this.initDate.substring(0,4);
            let mm   = this.initDate.substring(5,7);
            let dd   = this.initDate.substring(8,10);
                       this.dateInitFormat = dd+"-"+mm+"-"+yyyy;
            console.log('fecha init '+ this.dateInitFormat);
        
        }
        
        if (!this.finDate) {
             this.dateFinFormat = null;
        } else {
                      
            let yyyy = this.finDate.substring(0,4);
            let mm   = this.finDate.substring(5,7);
            let dd   = this.finDate.substring(8,10);
                         
            this.dateFinFormat = dd+"-"+mm+"-"+yyyy;
            console.log('fecha fin '+ this.dateFinFormat);
        }
        
        if (!this.motorizado) {
            this.motorizado = null;
        }
        console.log("motorizado "+ this.motorizado);
       
        //var _this = this;
        if(!this.showAlertDateInit) {
            try {
            this.reportxMotorizadoService.load(this.dateInitFormat,this.dateFinFormat,this.motorizado);
            } catch (error) {
                alert('No se encontro fecha indicada');                
            }
        }
        //despues de llamar al servicio verifico si se selecciono una campa�a para asignarle un valor vacio
        if(!this.motorizado) {
            this.motorizado = '';
        }
    
    };
    
    ngOnInit() {
         let _this = this;
         this.reportxMotorizadoService.loadMotorizado().subscribe(function(datos) {
              _this.motorizados = datos.data;
              console.log(_this.motorizados);
         }, function(err){});
        
         console.log(this.motorizados, this);
    }
}
