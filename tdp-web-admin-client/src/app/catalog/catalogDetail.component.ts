import {Component,OnInit} from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import{CatalogService} from '../services/catalog.service'

declare var $:any;

@Component({
  moduleId: 'catalogDetail',
  selector: 'my-catalogDetail',
  templateUrl: './catalogDetail.template.html'
})

export class CatalogDetailComponent implements OnInit {
  product:any;
  showEditProduct:string;
  nameEdit:string;
  messageValidate:string;
  idCatalog:any;
  editProduct:any;
  showMessageValidator:any;
	loading: boolean

      constructor(private route:ActivatedRoute, private catalogService:CatalogService) {
        this.product = {};
        this.showEditProduct = 'hide';
        this.nameEdit = "Editar";
        this.messageValidate = "";
        this.idCatalog = "";
        this.showMessageValidator = 'hide';
        this.editProduct = {};
		this.loading = true
      }

      ngOnInit(){
        var _this = this;
        //console.log(this.route.params);
        this.route.params
          .subscribe(function(params){
            _this.idCatalog = params['id'];
            _this.catalogService.loadById(params['id'])
            .subscribe(function(data){
              _this.product = data;
              _this.loadFieldEdit(data);
              //console.log(_this.product);
            },function(err){
              console.log(err);
						this.loading = false
            });
          },function(err){
            console.log(err);
				this.loading = false
          });
      }

      modalFieldEdit () {

    	  if(this.showEditProduct == 'show'){
    		  if(this.nameEdit == "Guardar"){
        		  $('.modal').modal();
              	$('#modal1').modal('open');
        	  }
    	  }else{
    		  this.showEditProduct = 'show';
    		  this.nameEdit = "Guardar";
		}

    	  }

      fieldEdit () {
		this.loading = true
    	  this.messageValidate = "";
    	  this.showMessageValidator ='hide';
    	  if(this.nameEdit == "Guardar"){

    		  var _this = this;

    		  $( "input" ).each(function(){
    			  if($(this).val().length == 0){
					_this.messageValidate = "Verifique que todos los campos tenga los valores correctos";
					//_this.messageValidate + $(this).attr("name") + ", ";
    			  }
    		  });

    		  if(this.messageValidate.length == 0){
    			  this.catalogService.updateCatalog(this.idCatalog,this.editProduct).subscribe(function(dataCatalog){
                      if(dataCatalog){

                    	  _this.product = dataCatalog;
						_this.loading = false

                      }

                    },function(err){
                      console.log(err);
                      this.loadFieldEdit(_this.product);
					this.loading = false
                    });
    		  }else{
    			  //_this.messageValidate = "Los campos "+ this.messageValidate + " tienen valores no permitidos";
    			  this.showMessageValidator ='show';
				this.loading = false
    		  }


    	  }
    	  if(this.messageValidate.length == 0){

    	  if(this.showEditProduct == 'show'){
    		  this.showEditProduct = 'hide';
    		  this.nameEdit = "Editar";

    	  }else{

    		  this.showEditProduct = 'show';
    		  this.nameEdit = "Guardar";


    	  }
    	  }

        }

        cancelEdit(){

        	if(this.showEditProduct == 'show'){
      		  this.showEditProduct = 'hide';
      		this.nameEdit = "Editar";
      		this.showMessageValidator ='hide';
      		this.loadFieldEdit(this.product);
      	  }

	}

        loadFieldEdit(data){

		this.editProduct.idProduct = data.idProduct;
        	this.editProduct.commercialoperation = data.commercialoperation;
			  this.editProduct.campaign = data.campaign;
			  this.editProduct.campaingcode = data.campaingcode;
			  this.editProduct.priority = data.priority;
			  this.editProduct.prodtypecode = data.prodtypecode;
			  this.editProduct.producttype = data.producttype;
			  this.editProduct.prodcategorycode = data.prodcategorycode;
			  this.editProduct.productcategory = data.productcategory;
			  this.editProduct.productcode = data.productcode;
			  this.editProduct.productname = data.productname;
			  this.editProduct.discount = data.discount;
			  this.editProduct.price = data.price;
			  this.editProduct.promprice = data.promprice;
			  this.editProduct.monthperiod = data.monthperiod;
			  this.editProduct.installcost = data.installcost;
			  this.editProduct.linetype = data.linetype;
			  this.editProduct.paymentmethod = data.paymentmethod;
			  this.editProduct.cashprice = data.cashprice;
			  this.editProduct.financingcost = data.financingcost;
			  this.editProduct.financingmonth = data.financingmonth;
			  this.editProduct.equipmenttype = data.equipmenttype;
			  this.editProduct.returnmonth = data.returnmonth;
			  this.editProduct.returnperiod = data.returnperiod;
			  this.editProduct.internetspeed = data.internetspeed;
			  this.editProduct.internettech = data.internettech;
			  this.editProduct.tvsignal = data.tvsignal;
			  this.editProduct.tvtech = data.tvtech;

		this.loading = false
        }
    }
