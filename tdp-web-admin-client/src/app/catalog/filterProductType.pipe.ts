import {Pipe} from '@angular/core';

import {CatalogService} from '../services/catalog.service'

@Pipe({
  name: 'searchProductType',
  pure: false
})
export class PipeSearchProductType {
      constructor(private catalogService:CatalogService) {}

      transform(args, value){
        return  args
          .filter(function(item){
              return item.producttype.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
