import {Pipe} from '@angular/core';

import {CatalogService} from '../services/catalog.service';

@Pipe({
  name: 'searchProductName',
  pure: false
})
export class PipeSearchProductName {
      constructor(private catalogService:CatalogService) {}
      transform(args, value){
        return  args
          .filter(function(item){
              return item.productname.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
