import {Pipe} from '@angular/core';

import {CatalogService} from '../services/catalog.service';

@Pipe({
  name: 'searchProductCategory',
  pure: false
})
export class PipeSearchProductCategory {
      constructor(private catalogService:CatalogService) {}
      transform(args, value){
        return  args
          .filter(function(item){
              return item.productcategory.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
