import {Component, OnInit} from '@angular/core'
import {CatalogService} from '../services/catalog.service'

@Component({
  moduleId: 'catalogupload',
  selector: 'catalog-upload',
  templateUrl: './catalogUpload.template.html'
})

export class CatalogUploadComponent {
  model:any;
  loading:boolean = false;
  files:any[];

      constructor(private catalogService:CatalogService) {
        this.model = {};
      }

      onClickOtroArchivo () {
        this.model = {};
        this.files = [];
      }

      onClickPersist () {
        var _this = this;
        this.loading = true;
        this.catalogService.reset(this.files[0]).subscribe(function (responseData) {
          _this.model = responseData.data;
          _this.loading = false;
        }, function (err) {
          console.log(err);
          _this.loading = false;
        });
      }

      onFormSubmit () {
        var _this = this;
        this.loading = true;
        this.catalogService.upload(this.files[0]).subscribe(function (responseData) {
          _this.model = responseData.data;
          _this.loading = false;
        }, function (err) {
          console.log(err);
          _this.loading = false;
        });
        
      }


      onChangeFile (evt) {
        this.files = evt.srcElement.files;
      }



    }
