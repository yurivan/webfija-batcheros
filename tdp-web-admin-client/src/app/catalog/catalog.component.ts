import { Component, OnInit } from '@angular/core'
import { CatalogService } from '../services/catalog.service'

@Component({
  moduleId: 'catalog',
  selector: 'my-catalog',
  templateUrl: './catalog.template.html'
})
export class CatalogComponent implements OnInit {
  catalog = [];
  model: any;
  strValue: boolean;
  constructor(private catalogService: CatalogService) {
    this.model = {
      count: 0,
      currentPage: 0,
      perPage: 10,
      pages: [],
      loading: false
    };
  }

  ngOnInit() {
    this.getStatusProvCatalog();
    this.search(this.model.currentPage);
  }

  getStatusProvCatalog() {
    this.catalogService.getStatus().subscribe(data => {
      let datos = data["_body"]
      if (datos.substring(22, 26) === 'true') {
        this.strValue = true
      } else {
        this.strValue = false
      }
    }, error => {
      console.log(error)
    })
  }

  updateProvCatalog() {
    let shouldIUpdateAllProv = this.strValue
    console.log("updateAllProv " + shouldIUpdateAllProv)
    this.catalogService.updateStrValue(shouldIUpdateAllProv).subscribe(data => {
      console.log(data)
    }, error => {
      console.log(error)
    })
  }

  onSearch() {
    this.search(0);
  }

  gotoPage(i) {
    this.model.currentPage = i;
    this.search(i);
  }

  gotoNextPage() {
    var nextPage = this.model.currentPage + 1;
    if (this.model.numPages < nextPage) {
      this.model.currentPage = this.model.numPages;
    } else {
      this.model.currentPage = nextPage;
    }
    this.search(this.model.currentPage);
  }

  gotoPrevPage() {
    var nextPage = this.model.currentPage - 1;
    if (0 >= nextPage) {
      this.model.currentPage = 0;
    } else {
      this.model.currentPage = nextPage;
    }
    this.search(this.model.currentPage);
  }

  search(currentPage) {
    var _this = this;
    var producttype = '%' + (this.model.producttype == null ? '' : this.model.producttype) + '%';
    var productcategory = '%' + (this.model.productcategory == null ? '' : this.model.productcategory) + '%';
    var productname = '%' + (this.model.productname == null ? '' : this.model.productname) + '%';
    var filter = {
      limit: this.model.perPage,
      skip: currentPage * this.model.perPage,
      where: {
        and: [
          { producttype: { like: producttype } },
          { productcategory: { like: productcategory } },
          { productname: { like: productname } }
        ]
      }
    };
    this.model.loading = true;
    this.catalogService.search(filter).subscribe(function (data) {
      _this.catalog = data;
      _this.computePaginator(filter.where);
      _this.model.loading = false;
    }, function (err) { });;
  }

  computePaginator(where) {
    var _this = this;
    this.catalogService.count(where).subscribe(function (data) {
      _this.model.pages = []
      _this.model.count = data.count;
      _this.model.numPages = Math.ceil(_this.model.count / _this.model.perPage);
      var numPages = _this.model.numPages;
      var min = _this.model.currentPage - 5;
      var max = _this.model.currentPage + 5;
      if (min < 0) min = 0;
      if (max > numPages) max = numPages;
      for (var i = min; i < max; i++) {
        _this.model.pages.push(i);
      }
    });
  }
}
