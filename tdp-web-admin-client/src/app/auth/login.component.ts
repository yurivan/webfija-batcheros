import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { User } from '../models/user.model';

@Component({
  moduleId: 'login',
  selector: 'my-login',
  templateUrl: 'login.template.html'
})
export class LoginComponent implements OnInit {
  model:User = new User();
  returnUrl:string='';
      constructor(private route:ActivatedRoute,
        private router:Router,
        private authService:AuthService) {}


      ngOnInit() {
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
      }

      login() {
        var _this = this;
        this.authService.login(this.model.username, this.model.password)
          .subscribe(function (data) {
            _this.router.navigate([_this.returnUrl]);
          }, function (err) {
            alert('Error al iniciar sesion');
          });
      }
    }
