import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import 'rxjs/Rx';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor (private router :Router) {}

  canActivate (route, state) {
    let _this = this;
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      return true;
    }
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    return false;
  }
}
