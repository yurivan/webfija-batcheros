import {Component, OnInit} from '@angular/core'
import {CatalogxMotorizadoService} from '../services/catalogxmotorizado.service'

@Component({
  moduleId: 'catalogxmotorizadoupload',
  selector: 'catalogxmotorizado-upload',
  templateUrl: './upload.template.html'
})
export class CatalogxMotorizadoUploadComponent {
  model:any;
  loading:boolean = false;
  files:any[];
      constructor(private catalogxMotorizadoService:CatalogxMotorizadoService) {
        this.model = {};
      }

      onClickOtroArchivo () {
        this.model = {};
        this.files = [];
      }

      onClickPersist () {
        var _this = this;
        this.loading = true;
        this.catalogxMotorizadoService.reset(this.files[0]).subscribe(function (responseData) {
          _this.model = responseData.data;
          _this.loading = false;
        }, function (err) {
          console.log(err);
          _this.loading = false;
        });
      }

      onFormSubmit () {
        var _this = this;
        this.loading = true;
        this.catalogxMotorizadoService.upload(this.files[0]).subscribe(function (responseData) {
          _this.model = responseData.data;
          _this.loading = false;
        }, function (err) {
          console.log(err);
          _this.loading = false;
        });
      }

      onChangeFile (evt) {
        this.files = evt.srcElement.files;
      }
    }
