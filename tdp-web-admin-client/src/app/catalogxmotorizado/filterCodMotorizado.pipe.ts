import {Pipe} from '@angular/core';

import {CatalogxMotorizadoService} from '../services/catalogxmotorizado.service';

@Pipe({
  name: 'searchCodMotorizado',
  pure: false
})
export class PipeSearchCodMotorizado {
      constructor(private catalogxMotorizadoService:CatalogxMotorizadoService) {}
      transform(args, value){
        return  args
          .filter(function(item){
              return item.campaign.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
