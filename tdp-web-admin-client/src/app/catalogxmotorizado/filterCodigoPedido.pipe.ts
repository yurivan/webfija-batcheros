import {Pipe} from '@angular/core';

import {CatalogxMotorizadoService} from '../services/catalogxmotorizado.service';

@Pipe({
  name: 'searchCodigoPedido',
  pure: false
})
export class PipeSearchCodigoPedido {
      constructor(private catalogxMotorizadoService:CatalogxMotorizadoService) {
      	this.catalogxMotorizadoService = catalogxMotorizadoService;
      }
      transform(args, value){
        return  args
          .filter(function(item){
              return item.idventa.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
