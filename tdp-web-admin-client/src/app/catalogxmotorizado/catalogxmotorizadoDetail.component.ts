import {Component,OnInit} from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import {CatalogxMotorizadoService} from '../services/catalogxmotorizado.service'

declare var $:any;

@Component({
  moduleId: 'catalogxmotorizadoDetail',
  selector: 'my-catalogxmotorizadoDetail',
  templateUrl: './catalogxmotorizadoDetail.template.html'
})
export class CatalogxMotorizadoDetailComponent implements OnInit {
  motorizado:any;
  showEditMotorizado:string;
  nameEdit:string;
  messageValidate:string;
  idCatalogxMotorizado:any;
  editMotorizado:any;
  showMessageValidator:any;

      constructor(private route:ActivatedRoute, private catalogxMotorizadoService:CatalogxMotorizadoService) {
        this.motorizado = {};
        this.showEditMotorizado = 'hide';
        this.nameEdit = "Editar";
        this.messageValidate = "";
        this.idCatalogxMotorizado = "";
        this.showMessageValidator = 'hide';
        this.editMotorizado = {};
      }
      ngOnInit(){
        var _this = this;
        //console.log(this.route.params);
        this.route.params
          .subscribe(function(params){
            _this.idCatalogxMotorizado = params['id'];
            _this.catalogxMotorizadoService.loadById(params['id'])
            .subscribe(function(data){
              _this.motorizado = data;
              _this.loadFieldEdit(data);
              //console.log(_this.campaign);
            },function(err){
              console.log(err);
            });
          },function(err){
            console.log(err);
          });
      }

      modalFieldEdit () {

    	  if(this.showEditMotorizado == 'show'){

    		  if(this.nameEdit == "Guardar"){

        		  $('.modal').modal();
              	$('#modal1').modal('open');

        	  }
    	  }else{

    		  this.showEditMotorizado = 'show';
    		  this.nameEdit = "Guardar";


    	  }

        }
      fieldEdit () {
    	  this.messageValidate = "";
    	  this.showMessageValidator ='hide';
    	  if(this.nameEdit == "Guardar"){

    		  var _this = this;

    		  $( "input" ).each(function(){

    			  if($(this).val().length == 0){

    				  _this.messageValidate = "Verifique que todos los campos tenga los valores correctos";//_this.messageValidate + $(this).attr("name") + ", ";

    			  }
    		  });

    		  if(this.messageValidate.length == 0){
    			  this.catalogxMotorizadoService.updateCatalogxMotorizado(this.idCatalogxMotorizado,this.editMotorizado).subscribe(function(dataCatalogxMotorizado){
                      if(dataCatalogxMotorizado){

                    	  _this.motorizado = dataCatalogxMotorizado;

                      }

                    },function(err){
                      console.log(err);
                      this.loadFieldEdit(_this.motorizado);
                    });
    		  }else{
    			  //_this.messageValidate = "Los campos "+ this.messageValidate + " tienen valores no permitidos";
    			  this.showMessageValidator ='show';
    		  }


    	  }
    	  if(this.messageValidate.length == 0){

    	  if(this.showEditMotorizado == 'show'){
    		   this.showEditMotorizado = 'hide';
    		   this.nameEdit = "Editar";

    	  }else{

    		  this.showEditMotorizado = 'show';
    		  this.nameEdit = "Guardar";


    	  }
    	  }

        }
        cancelEdit(){

        	if(this.showEditMotorizado == 'show'){
      		   this.showEditMotorizado = 'hide';
      	     this.nameEdit = "Editar";
      		   this.showMessageValidator ='hide';
      		   this.loadFieldEdit(this.motorizado);
      	  }
       
        }
        loadFieldEdit(data){

        	this.editMotorizado.codigopedido   = data.codigopedido;
          this.editMotorizado.codmotorizado  = data.codmotorizado;
          this.editMotorizado.fecharegistro  = data.fecharegistro;
          this.editMotorizado.tipdoccliente  = data.tipdoccliente;
          this.editMotorizado.numdoccliente  = data.numdoccliente;
          this.editMotorizado.nombrecliente  = data.nombrecliente;
          this.editMotorizado.movilcliente   = data.movilcliente;
          this.editMotorizado.biometria      = data.biometria;
          this.editMotorizado.estado         = data.estado;
          this.editMotorizado.motivorechazo  = data.motivorechazo;
          this.editMotorizado.registeredtime = data.registeredtime;
          this.editMotorizado.updatetime     = data.updatetime;
        }
    }
