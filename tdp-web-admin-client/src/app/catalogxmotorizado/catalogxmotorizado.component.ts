import { Component, OnInit } from '@angular/core'
import { CatalogxMotorizadoService } from '../services/catalogxmotorizado.service'

@Component({
  moduleId: 'catalogxmotorizado',
  selector: 'my-catalogxmotorizado',
  templateUrl: './catalogxmotorizado.template.html'
})
export class CatalogxMotorizadoComponent implements OnInit {
  catalogxmotorizado = [];
  model: any;
  constructor(private catalogxmotorizadoservice: CatalogxMotorizadoService) {
    this.model = {
      count: 0,
      currentPage: 0,
      perPage: 10,
      pages: [],
      loading: false
    };
  }

  ngOnInit() {
    this.search(this.model.currentPage);
  }

  onSearch() {
    this.search(0);
  }

  gotoPage(i) {
    this.model.currentPage = i;
    this.search(i);
  }

  gotoNextPage() {
    var nextPage = this.model.currentPage + 1;
    if (this.model.numPages < nextPage) {
      this.model.currentPage = this.model.numPages;
    } else {
      this.model.currentPage = nextPage;
    }
    this.search(this.model.currentPage);
  }

  gotoPrevPage() {
    var nextPage = this.model.currentPage - 1;
    if (0 >= nextPage) {
      this.model.currentPage = 0;
    } else {
      this.model.currentPage = nextPage;
    }
    this.search(this.model.currentPage);
  }

  search(currentPage) {
    var _this = this;
    var codigopedido = '%' + (this.model.codigopedido == null ? '' : this.model.codigopedido) + '%';
    var codmotorizado = '%' + (this.model.codmotorizado == null ? '' : this.model.codmotorizado) + '%';
    var fecharegistro = '%' + (this.model.fecharegistro == null ? '' : this.model.fecharegistro) + '%';
    var tipdoccliente = '%' + (this.model.tipdoccliente == null ? '' : this.model.tipdoccliente) + '%';
    var numdoccliente = '%' + (this.model.numdoccliente == null ? '' : this.model.numdoccliente) + '%';
    var nombrecliente = '%' + (this.model.nombrecliente == null ? '' : this.model.nombrecliente) + '%';
    var movilcliente = '%' + (this.model.movilcliente == null ? '' : this.model.movilcliente) + '%';
    var biometria = '%' + (this.model.biometria == null ? '' : this.model.biometria) + '%';
    var estado = '%' + (this.model.estado == null ? '' : this.model.estado) + '%';
    var motivorechazo = '%' + (this.model.motivorechazo == null ? '' : this.model.motivorechazo) + '%';
    var registeredtime = '%' + (this.model.registeredtime == null ? '' : this.model.registeredtime) + '%';
    var updatetime = '%' + (this.model.updatetime == null ? '' : this.model.updatetime) + '%';
    var filter = {
      limit: this.model.perPage,
      skip: currentPage * this.model.perPage,
      where: {
        and: [
          { codmotorizado: { like: codmotorizado } },
          { codigopedido: { like: codigopedido } }
        ]
      }
    };
    this.model.loading = true;
    this.catalogxmotorizadoservice.search(filter).subscribe(function (data) {
      _this.catalogxmotorizado = data;
      _this.computePaginator(filter.where);
      _this.model.loading = false;
    }, function (err) { });;
  }

  computePaginator(where) {
    var _this = this;
    this.catalogxmotorizadoservice.count(where).subscribe(function (data) {
      _this.model.pages = []
      _this.model.count = data.count;
      _this.model.numPages = Math.ceil(_this.model.count / _this.model.perPage);
      var numPages = _this.model.numPages;
      var min = _this.model.currentPage - 5;
      var max = _this.model.currentPage + 5;
      if (min < 0) min = 0;
      if (max > numPages) max = numPages;
      for (var i = min; i < max; i++) {
        _this.model.pages.push(i);
      }
    });
  }

  download() {
    
  }
}
