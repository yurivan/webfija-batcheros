import {Component,OnInit} from '@angular/core';

import {ProfileService} from '../services/profile.service';

@Component({
  moduleId: 'profile',
  selector: 'my-profile',
  templateUrl: './profile.template.html'
})
export class ProfileComponent implements OnInit {
  new_profile:any;
  profile:any[];
      constructor(private profileService:ProfileService) {
      	this.new_profile = {};
      	this.profile = [];
      }

      ngOnInit(){
      	var _this = this;
      	console.log(this.profile);
      	this.profileService.load()
      		.subscribe(function(data){
      			_this.profile = data;
      			console.log(data);
      		}, function(err){
      			console.log(err);
      		});
      }
      activateUpdate(profile){
    	  this.new_profile.id=profile.id;
    	  this.new_profile.name=profile.name;
      }
      save(){
    	  console.log(this.new_profile.id);
    	  if(typeof this.new_profile.id !== 'undefined' && this.new_profile.id !== null){
    		  this.update();
    	  }else{
    		  this.add();
    	  }
      }
      update(){
          this.profileService.update(this.new_profile.id,{
        	  name: this.new_profile.name,
        	  lastupdatetime: new Date()
        	})
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           this.new_profile.id=null;
            	           this.new_profile.name=null;
            	           return true;
            	         },
            	         error => {
            	           console.error("Error saving profile!");
            	         }
            );
      }
      remove(id){
          this.profileService.remove(id)
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           return true;
            	         },
            	         error => {
            	           console.error("Error removing profile!");
            	         }
            );
      }
      add(){
         // this.profileService.add(0,this.new_profile.name, new Date(),new
			// Date())
           this.profileService.add({
						 id: null,
        	   name: this.new_profile.name,
        	   registeredtime: new Date(),
        	   lastupdatetime: new Date()
        	 })
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           this.new_profile.id=null;
            	           this.new_profile.name=null;
            	           return true;
            	         },
            	         error => {
            	           console.error("Error saving profile!");
            	         }
            );
      }
    }
