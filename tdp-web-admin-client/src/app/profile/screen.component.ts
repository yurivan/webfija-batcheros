import {Component} from '@angular/core';

import {ScreenService} from '../services/screen.service';

@Component({
  moduleId: 'screen',
  selector: 'my-screen',
  templateUrl: './screen.template.html'
})
export class ScreenComponent {
  new_screen:any;
  screen:any[];
      constructor(private screenService:ScreenService) {
      	this.new_screen = {};
      	this.screen = [];
      }
      ngOnInit(){
      	var _this = this;
      	console.log(this.screen);
      	this.screenService.load()
      		.subscribe(function(data){
      			_this.screen = data;
      			console.log(data);
      		}, function(err){
      			console.log(err);
      		});
      }
      activateUpdate(screen){
    	  this.new_screen.id=screen.id;
    	  this.new_screen.name=screen.name;
    	  this.new_screen.url=screen.url;
      }
      save(){
    	  console.log(this.new_screen.id);
    	  if(typeof this.new_screen.id !== 'undefined' && this.new_screen.id !== null){
    		  this.update();
    	  }else{
    		  this.add();
    	  }
      }
      update(){
          this.screenService.update(this.new_screen.id,{
        	  name: this.new_screen.name,
        	  url: this.new_screen.url,
        	  lastupdatetime: new Date()
        	})
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           this.new_screen.id=null;
            	           this.new_screen.name=null;
            	           this.new_screen.url=null;
            	           return true;
            	         },
            	         error => {
            	           console.error("Error saving screen!");
            	         }
            );
      }
      remove(id){
          this.screenService.remove(id)
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           return true;
            	         },
            	         error => {
            	           console.error("Error removing screen!");
            	         }
            );
      }
      add(){
         // this.screenService.add(0,this.new_screen.name, new Date(),new
			// Date())
           this.screenService.add({
        	   id: 0,
        	   name: this.new_screen.name,
        	   url: this.new_screen.url,
        	   registeredtime: new Date(),
        	   lastupdatetime: new Date()
        	 })
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           this.new_screen.id=null;
            	           this.new_screen.name=null;
            	           this.new_screen.url=null;
            	           return true;
            	         },
            	         error => {
            	           console.error("Error saving screen!");
            	         }
            );
      }
    }
