import {Component} from '@angular/core';

import {ProfileScreenService} from '../services/profilescreen.service';
import {ScreenService} from '../services/screen.service';
import {ProfileService} from '../services/profile.service';

@Component({
  moduleId: 'profilescreen',
  selector: 'my-profilescreen',
  templateUrl: './profilescreen.template.html'
})
export class ProfileScreenComponent {
  new_profilescreen:any;
  profilescreen:any[];
  profile:any[];
  screen:any[];
      constructor(private profilescreenService:ProfileScreenService,private profileService:ProfileService,private screenService:ScreenService) {
        this.new_profilescreen = {};
        this.profilescreen = [];
      	this.profile = [];
      	this.screen = [];
      }
      ngOnInit(){
      	var _this = this;
      	console.log(this.profilescreen);
      	this.profilescreenService.load()
      		.subscribe(function(data){
      			_this.profilescreen = data;
      			console.log(data);
      		}, function(err){
      			console.log(err);
      		});
      	this.profileService.load()
  		.subscribe(function(data){
  			_this.profile = data;
  			console.log(data);
  		}, function(err){
  			console.log(err);
  		});
      	this.screenService.load()
  		.subscribe(function(data){
  			_this.screen = data;
  			console.log(data);
  		}, function(err){
  			console.log(err);
  		});
      }
      activateUpdate(profilescreen){
    	  this.new_profilescreen.id=profilescreen.id;
    	  this.new_profilescreen.webprofileid=profilescreen.webprofileid;
    	  this.new_profilescreen.webscreenid=profilescreen.webscreenid;
      }
      save(){
    	  console.log("Cod de acceso: "+this.new_profilescreen.id);
    	  if(typeof this.new_profilescreen.id !== 'undefined' && this.new_profilescreen.id !== null){
    		  this.update();
    	  }else{
    		  this.add();
    	  }
      }
      update(){
          this.profilescreenService.update(this.new_profilescreen.id,{
        	  webprofileid: this.new_profilescreen.webprofileid,
        	  webscreenid: this.new_profilescreen.webscreenid,
        	  lastupdatetime: new Date()
        	})
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           this.new_profilescreen.id=null;
            	           this.new_profilescreen.webprofileid=null;
            	           this.new_profilescreen.webscreenid=null;
            	           return true;
            	         },
            	         error => {
            	           console.error("Error saving profilescreen!");
            	         }
            );
      }
      remove(id){
          this.profilescreenService.remove(id)
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           return true;
            	         },
            	         error => {
            	           console.error("Error removing profilescreen!");
            	         }
            );
      }
      add(){
         // this.profilescreenService.add(0,this.new_profile.name, new Date(),new
			// Date())
			//var UID = ( Math.random() * 16 ).toString(8)
			//console.log("Id autogenerado: "+UID)
           this.profilescreenService.add({
        	   id: null,
        	   webprofileid: this.new_profilescreen.webprofileid,
        	   webscreenid: this.new_profilescreen.webscreenid,
        	   registeredtime: new Date(),
        	   lastupdatetime: new Date()
        	 })
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           this.new_profilescreen.id=null;
            	           this.new_profilescreen.webprofileid=null;
            	           this.new_profilescreen.webscreenid=null;
            	           return true;
            	         },
            	         error => {
            	           console.error("Error saving profilescreen!");
            	         }
            );
      }
    }
