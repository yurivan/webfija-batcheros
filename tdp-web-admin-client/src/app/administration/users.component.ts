import {Component,OnInit} from '@angular/core';

import {UserService} from '../services/user.service';

@Component({
  moduleId: 'users',
  selector: 'users',
  templateUrl: './users.template.html'
})
export class UsersComponent implements OnInit {
  model:any;
  data:any[];

      constructor(private service:UserService) {
        this.data = [];
        this.model = {
          count: 0,
          currentPage: 0,
          perPage: 10,
          pages: []
        };
      }
      ngOnInit(){
        this.search(this.model.currentPage);
      }

      onSearch () {
        this.search(0);
      }

      gotoPage (i) {
        this.model.currentPage = i;
        this.search(i);
      }

      gotoNextPage () {
        var nextPage = this.model.currentPage + 1;
        if (this.model.numPages <= nextPage) {
          this.model.currentPage = this.model.numPages-1;
        } else {
          this.model.currentPage = nextPage;
        }
        this.search(this.model.currentPage);
      }

      gotoPrevPage () {
        var nextPage = this.model.currentPage - 1;
        if (0 >= nextPage) {
          this.model.currentPage = 0;
        } else {
          this.model.currentPage = nextPage;
        }
        this.search(this.model.currentPage);
      }

      search (currentPage) {
        var _this = this;
        var tag = '%' + (this.model.tag == null ? '' : this.model.tag) + '%';
        var username = '%' + (this.model.username == null ? '' : this.model.username) + '%';
        var msg = '%' + (this.model.msg == null ? '' : this.model.msg) + '%';
        var filter = {
          limit: this.model.perPage,
          skip: currentPage*this.model.perPage,
          where: {
            and: [
              {username: { like: username}}
            ]
          }
        };
        this.model.loading = true;
        this.service.search(filter).subscribe(function(data){
          _this.data = data;
          //////
          console.log(data);
          /////////
          _this.computePaginator(filter.where);
          _this.model.loading = false;
        }, function(err){});
      }

      computePaginator (where) {
        var _this = this;
        this.service.count(where).subscribe(function (data) {
          _this.model.pages = []
          _this.model.count = data.count;
          _this.model.numPages = Math.ceil(_this.model.count / _this.model.perPage);
          var numPages = _this.model.numPages;
          var min = _this.model.currentPage - 5;
          var max = _this.model.currentPage + 5;
          if (min < 0) min = 0;
          if (max > numPages) max = numPages;
          for (var i = min; i<max; i++) {
            _this.model.pages.push(i);
          }
        });
      }
    }
