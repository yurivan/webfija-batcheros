import {Component,OnInit,AfterViewInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router';

import {UserService} from '../services/user.service';
import {ProfileService} from '../services/profile.service';

declare var $:any;

@Component({
  moduleId: 'usereditar',
  selector: 'useredita',
  templateUrl: './usereditar.template.html'
})
export class UserEditarComponent implements OnInit,AfterViewInit {
  loading:boolean;
  profiles:any[];
  users:any;
  tipoDocumentos:any[];
  tipoDocumentosMap:any;
  doctypeid:number;
  model:any;

  constructor(private route:ActivatedRoute, private router:Router, private service:UserService, private profileService:ProfileService){
        this.loading = false;
        this.profiles = [];
        this.users = [];
        this.tipoDocumentos = [{id: 1, name: "DNI"}];
        this.tipoDocumentosMap = {
          1: this.tipoDocumentos[0]
        }
        this.doctypeid = 0;
      }
      ngOnInit(){
        var _this = this;
        this.model={}
        this.route.params
          .subscribe(function(params){
            _this.service.loadById(params['id'])
              .subscribe(function(data){
                _this.loadProfile();
                _this.users = data;
                _this.doctypeid = 1;
                _this.model.loading = false;
                console.log(_this.users);
              }, function(err){
                console.log(err);
              });
          },function(err){
            console.log(err);
          });
          console.log(this.route.params);
      }

      loadProfile(){
        var _this= this;
        this.profileService.load().subscribe(function (profiles) {
          _this.profiles = profiles;
          _this.loading = false;
        });
      }

      ngAfterViewInit(){
        $('select').material_select();
      }

      onSubmit () {
        let _this = this;
        this.loading = true;
        this.users.doctype = this.tipoDocumentosMap[this.doctypeid].name;
        this.service.update(this.users.id,this.users).subscribe(function () {
          _this.loading = false;
          alert("Usuario registrado!");
          _this.router.navigate(['administration/users']);
        }, function (err) {
          console.log(err);
          _this.loading = false;
          alert('No se pudo actualizar datos del usuario!');
        });
      }
    }
