import {Component,OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ServiceCallEventsService} from '../services/servicecallevents.service';

@Component({
  moduleId: 'report',
  selector: 'my-report',
  templateUrl: './servicecalleventsdetail.template.html'
})
export class ServiceCallEventsDetailComponent implements OnInit {
  model:any;
      constructor(private route:ActivatedRoute, private serviceCallEventsService:ServiceCallEventsService) {
        this.model = {};
      }

      ngOnInit(){
      	var _this = this;
        this.route.params.subscribe(function (params) {
          _this.serviceCallEventsService.loadById(params['id'])
          .subscribe(function(data){
      			_this.model = data;
      		}, function(err){});
        }, function (err) {});
      }
    }
