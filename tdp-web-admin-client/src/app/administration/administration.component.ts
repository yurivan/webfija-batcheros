import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  moduleId: 'administration',
  selector: 'my-administration',
  templateUrl: 'administration.template.html'
})
export class AdministrationComponent {
  grantedAccess: any;

  constructor(private authService: AuthService) {
    this.grantedAccess = this.authService.grantedAccess;
  }
}
