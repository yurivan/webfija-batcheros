import {Component,OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {UserService} from '../services/user.service';
import {ProfileService} from '../services/profile.service';

@Component({
  moduleId: 'userregister',
  selector: 'userregister',
  templateUrl: './userregister.template.html'
})
export class UserRegisterComponent implements OnInit {
   model:any;
   loading:boolean;
   profiles:any[];

      constructor(private router:Router, private service:UserService, private profileService:ProfileService) {
        this.model = {id: 0};
        this.loading = false;
        this.profiles = [];
      }

      ngOnInit(){
        let _this = this;
        this.loading = true;
        this.profileService.load().subscribe(function (profiles) {
          _this.profiles = profiles;
          _this.loading = false;
        });
      }

      onSubmit () {
        let _this = this;
        this.loading = true;
    this.model.id=this.model.docnumber;
        this.service.save(this.model).subscribe(function () {
          _this.loading = false;
          _this.router.navigate(['administration/users']);
        }, function (err) {
          console.log(err);
          _this.loading = false;
          alert('No se pudo registrar el usuario!');
        });
      }
    }
