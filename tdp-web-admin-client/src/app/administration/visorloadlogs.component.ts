import { Component, OnInit } from '@angular/core';
import { VisorLoadLogsService } from '../services/visorloadlogs.service'

//glazaror import globals
import * as globals from '../services/globals';

@Component({
  moduleId: 'report',
  selector: 'my-report',
  templateUrl: './visorloadlogs.template.html'
})
export class VisorLoadLogsComponent implements OnInit {

  private readonlyFinDate: boolean = true; //glazaror se adiciona propiedad readonly para datepicker fechaFin
  private maxDatePicker: string = "";
  visorLoadLogs = [];
  model: any;
  logsHasBeenDownloaded: boolean = false

  constructor(private visorLoadLogsService: VisorLoadLogsService) {
    this.model = {
      count: 0,
      currentPage: 0,
      perPage: 10,
      pages: []
    };
  }
  ngOnInit() {
    this.logsHasBeenDownloaded = true
	
	//default
	this.model.iniDate = this.getDefaultDate();//"2018-08-31";
	this.model.finDate = this.getDefaultDate();;//"2018-08-31";
	this.selectInitDate();
	
	//this.model.iniDate.setDate(this.model.iniDate.getDate() - 4);
		
    this.search(this.model.currentPage);
	
	
	//this.model.iniDate = "10/08/2018";
	//this.model.finDate = "10/08/2018";
  }
  
  getDefaultDate() {
		var defaultDate = new Date();
		var defaultDateAsString = defaultDate.getFullYear() + "-" + 
			("0"+(defaultDate.getMonth()+1)).slice(-2) + "-" + ("0" + defaultDate.getDate()).slice(-2);
		return defaultDateAsString;
  }

  //glazaror funcion para habilitar/deshabilitar el datepicker de fechaFin
  selectInitDate() {

    if (this.model.iniDate) {
      this.readonlyFinDate = false;

      //glazaror prueba inicio
      var maxDate = new Date(this.getDateAsMilliseconds(this.model.iniDate, false));
      maxDate.setDate(maxDate.getDate() + 30);

      var month = (maxDate.getUTCMonth() + 1) + ""; //months from 1-12
      var day = maxDate.getUTCDate() + "";
      var year = maxDate.getUTCFullYear() + "";

      this.maxDatePicker = year + "-" + this.getDateFilled(month) + "-" + this.getDateFilled(day);
      //glazaror prueba fin
    } else {
      this.model.finDate = "";
      this.readonlyFinDate = true;
    }
  }

  getDateFilled(value) {
    if (value.length == 1) {
      return "0" + value;
    }
    return value;
  }

  onSearch() {
	if (this.model.status) {
		this.model.status = this.model.status.toUpperCase();
	}
    if (this.model.iniDate != null && (this.model.finDate == "" || this.model.finDate == undefined)) {
      //seteamos por default la fecha fin
      this.readonlyFinDate = false;

      //glazaror prueba inicio
      var maxDate = new Date(this.getDateAsMilliseconds(this.model.iniDate, false));
      maxDate.setDate(maxDate.getDate() + 30);

      var month = (maxDate.getUTCMonth() + 1) + ""; //months from 1-12
      var day = maxDate.getUTCDate() + "";
      var year = maxDate.getUTCFullYear() + "";

      this.model.finDate = year + "-" + this.getDateFilled(month) + "-" + this.getDateFilled(day);
      //glazaror prueba fin
    }
    this.search(0);
  }

  gotoPage(i) {
    this.model.currentPage = i;
    this.search(i);
  }

  gotoNextPage() {
    var nextPage = this.model.currentPage + 1;
    if (this.model.numPages <= nextPage) {
      this.model.currentPage = this.model.numPages - 1;
    } else {
      this.model.currentPage = nextPage;
    }
    this.search(this.model.currentPage);
  }

  gotoPrevPage() {
    var nextPage = this.model.currentPage - 1;
    if (0 >= nextPage) {
      this.model.currentPage = 0;
    } else {
      this.model.currentPage = nextPage;
    }
    this.search(this.model.currentPage);
  }

  getFilter() {
    var whereClause = null;
    var andClause = [];
    var status = '%' + (this.model.status == null ? '' : this.model.status) + '%';
	if (this.model.status != null) {
      andClause.push({ status: { like: status } });
    }

    //glazaror filtro fecha
    if (this.model.iniDate != null && this.model.finDate) {
      var iniDateTimeMilliseconds = this.getDateAsMilliseconds(this.model.iniDate, true);
      var finDateTimeMilliseconds = this.getDateAsMilliseconds(this.model.finDate, false);
      andClause.push({ loadDate: { between: [iniDateTimeMilliseconds, finDateTimeMilliseconds] } });
    }

    if (andClause.length > 1) {
      whereClause = {
        and: andClause
      }
    } else if (andClause.length == 1) {
      whereClause = andClause[0];
    }

    var filter = {
      skip: this.model.currentPage * this.model.perPage,
      order: 'loadDate DESC'
    };
    if (whereClause != null) {
      filter['where'] = whereClause;
    }
    return filter;
  }

  search(currentPage) {
    var _this = this;
    var whereClause = null;
    var andClause = [];
	var status = '%' + (this.model.status == null ? '' : this.model.status) + '%';
    if (this.model.status != null) {
      andClause.push({ status: { like: status } });
    }

    //glazaror filtro fecha
    if (this.model.iniDate != null && this.model.finDate) {
      var iniDateTimeMilliseconds = this.getDateAsMilliseconds(this.model.iniDate, true);
      var finDateTimeMilliseconds = this.getDateAsMilliseconds(this.model.finDate, false);
      andClause.push({ loadDate: { between: [iniDateTimeMilliseconds, finDateTimeMilliseconds] } });
    }

    if (andClause.length > 1) {
      whereClause = {
        and: andClause
      }
    } else if (andClause.length == 1) {
      whereClause = andClause[0];
    }

    var filter = {
      limit: this.model.perPage,
      skip: currentPage * this.model.perPage,
      order: 'loadDate DESC'
    };
    if (whereClause != null) {
      filter['where'] = whereClause;
    }
    this.model.loading = true;
    this.visorLoadLogsService.search(filter).subscribe(function (data) {
      _this.visorLoadLogs = data;
      if (whereClause != null) {
        _this.computePaginator(whereClause);
      } else {
        _this.computePaginator({});
      }
      _this.model.loading = false;
    }, function (err) { });
  }

  computePaginator(where) {
    var _this = this;
    this.visorLoadLogsService.count(where).subscribe(function (data) {
      _this.model.pages = []
      _this.model.count = data.count;
      _this.model.numPages = Math.ceil(_this.model.count / _this.model.perPage);
      var numPages = _this.model.numPages;
      var min = _this.model.currentPage - 5;
      var max = _this.model.currentPage + 5;
      if (min < 0) min = 0;
      if (max > numPages) max = numPages;
      for (var i = min; i < max; i++) {
        _this.model.pages.push(i);
      }
    });
  }

  //glazaror funcion para obtener la fecha en formato yyyymmdd en milisegundos
  getDateAsMilliseconds(dateYYYYMMDD, fromStartDay) {

    var iniDatetimeYYYY = dateYYYYMMDD.substring(0, 4);
    var iniDatetimeMM = dateYYYYMMDD.substring(5, 7) - 1;
    var iniDatetimeDD = dateYYYYMMDD.substring(8, 10);

    var d = null;

    if (fromStartDay) {
      d = new Date(iniDatetimeYYYY, iniDatetimeMM, iniDatetimeDD, 0, 0, 0, 0);
    } else {
      d = new Date(iniDatetimeYYYY, iniDatetimeMM, iniDatetimeDD, 23, 59, 59, 0);
    }
    return d.getTime();
  }

  //glazaror funcion para consumir el servicio de descarga de datos
  download() {
    if (this.model.iniDate == null || this.model.finDate == null) {
      alert("Para la descarga de logs por favor ingrese el filtro de fechas");
      return;
    }
    this.logsHasBeenDownloaded = false
    var filterObject = this.getFilter();
    //filterObject['fields'] = { servicerequest: false, serviceresponse: false };
    var filter = encodeURIComponent(JSON.stringify(filterObject));
    var token = localStorage.getItem('token');

    var w = window.open(globals.BASE_URL + 'api/VisorLoadLogs/download?filter=' + filter + '&access_token=' + token);
    if (w) {
      w.onunload = () => {
        this.logsHasBeenDownloaded = true
      }
    } else {
      this.logsHasBeenDownloaded = true
    }
  }
}
