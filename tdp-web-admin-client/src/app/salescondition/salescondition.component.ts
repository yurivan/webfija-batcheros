import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { ConfigofferingService } from '../services/configoffering.service';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';

export class Parameters {
    id: number;
    auxiliar: number;
    domain: string;
    category: string;
    element: string;
    strvalue: string;
    strfilter: string;

    titulo: string;
    contenido: string;
}
@Component({
    moduleId: 'salescondition',
    selector: 'salescondition',
    templateUrl: 'salescondition.template.html'
})
export class salesconditioncomponent implements OnInit {

    salesCondition: Parameters[];
    unitCondition: Parameters;
    flagCondition: boolean = false;

 

    constructor(private router: Router,
        private configService: ConfigofferingService) {
        this.router = router;
        this.configService = configService;
    }

    ngOnInit() {
        this.flagCondition = false;
        this.cargarSalesCondition();
    }

    returnList() {
        this.router.navigate(['/administration']);
    }

    cargarSalesCondition() {
        this.salesCondition = [];
        let _this = this;
        this.configService.getSalesCondition().subscribe(
            data => {
                _this.salesCondition = data.data;
                _this.flagCondition = true;
                localStorage.setItem('salesCondition', JSON.stringify(_this.salesCondition))
            },
            err => {
                console.log(err)
            }
        );
    }

    acceder(category: string){
        for(let condition of this.salesCondition){
            if(condition.category==category){
                this.unitCondition=condition;
            }
        }
        localStorage.setItem('unitCondition',JSON.stringify(this.unitCondition))
        this.router.navigate(['editsalescondition']);
    }
}

