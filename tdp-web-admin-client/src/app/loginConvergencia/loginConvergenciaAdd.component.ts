import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd,ActivatedRoute } from '@angular/router';
import { loginConvergenciaService } from '../services/loginConvergencia.service';
import { LoginConvergencia } from './loginConvergencia.component';

@Component({
    moduleId: 'loginConvergenciaAdd',
    selector: 'loginConvergenciaAdd',
    templateUrl: 'loginConvergenciaAdd.template.html'
})
export class loginConvergenciaAddComponent implements OnInit {

    acceso: LoginConvergencia;
    loading: boolean;
    saved: boolean;
    type: string

    constructor(private router: Router,private loginConvergenciaService:loginConvergenciaService,private route:ActivatedRoute) {
        this.router = router;
    }

    ngOnInit() {
        this.initProfile();
    }

    returnList() {
        this.router.navigate(['/login-convergencia']);
    }

    initProfile(){
        this.type=this.route.snapshot.params['type']
        console.log(this.type);
        
        this.acceso = new LoginConvergencia;
        this.acceso.canal = ''
        this.acceso.pto_venta = ''
        this.acceso.f_enable = false
        this.saved=false;
        this.loading=false;
    }

    add(){
        if(this.acceso.canal=="" && this.acceso.pto_venta==""){
            return false
        }
        let _scope = this;
        this.loading=true;
        this.loginConvergenciaService.save(this.acceso).subscribe(
            data => { 
                console.log(data)
                _scope.loading=false;
                _scope.saved=true;
            },
            err => {
                console.log(err)
                _scope.loading=false;        
                alert("Ocurrio un error al insertar, punto de venta existente")  
            }
        );
    }
}

