import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';
import { loginConvergenciaService } from '../services/loginConvergencia.service';

export class LoginConvergencia{
    id: number;
    pto_venta: string;
    canal: string;
    f_enable: boolean;
}

@Component({
    moduleId: 'loginConvergencia',
    selector: 'loginConvergencia',
    templateUrl: 'loginConvergencia.template.html'
})
export class loginConvergenciaComponent implements OnInit {

    accesos: LoginConvergencia[];
    loading: boolean;
    flag: boolean;

    constructor(private router: Router,private loginConvergenciaService:loginConvergenciaService) {
        this.router = router;
    }

    ngOnInit() {
        this.cargarAccesos();
    }

    returnList() {
        this.router.navigate(['/administration']);
    }

    cargarAccesos() {
        let _scope = this;
        this.accesos = [];
        this.loading=true;
        this.flag = false;

        this.loginConvergenciaService.getLoginConvergencia().subscribe(
            data => {
                _scope.accesos = data;
                console.log(_scope.accesos);  
                _scope.loading=false;
            },
            err => {
                console.log(err)
                _scope.loading=false;        
            }
        );

        this.loginConvergenciaService.getParameter().subscribe(
            data => {
                _scope.flag = (data.data[0].strvalue =="true"); ;
                console.log(_scope.flag);
            },
            err => {
                console.log(err)
                _scope.loading=false;        
            }
        );
    }

    delete(id){
        let _scope = this;
        this.loading=true;   
        this.loginConvergenciaService.delete(id).subscribe(
            data => {
                _scope.loading=false;   
                _scope.cargarAccesos()
            },
            err => {
                console.log(err)
                _scope.loading=false;      
            }
        );
    }

    updateApagado(){
        let _scope = this;
        this.loading=true;   
        this.loginConvergenciaService.updateflag().subscribe(
            data => {
                _scope.loading=false;   
            },
            err => {
                console.log(err)
                _scope.loading=false;      
            }
        );
    }
}

