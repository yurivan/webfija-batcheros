import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';
import { loginConvergenciaService } from '../services/loginConvergencia.service';
import { LoginConvergencia } from './loginConvergencia.component';
import {ActivatedRoute} from '@angular/router';

@Component({
    moduleId: 'loginConvergenciaEdit',
    selector: 'loginConvergenciaEdit',
    templateUrl: 'loginConvergenciaEdit.template.html'
})
export class loginConvergenciaEditComponent implements OnInit {

    acceso: LoginConvergencia;
    loading: boolean;
    saved: boolean;

    constructor(private router: Router,private loginConvergenciaService:loginConvergenciaService,private route:ActivatedRoute) {
        this.router = router;
    }

    ngOnInit() {
        this.initProfile();
    }

    returnList() {
        this.router.navigate(['/login-convergencia']);
    }

    initProfile(){
        this.acceso = new LoginConvergencia;
        let _scope = this;
        this.loginConvergenciaService.get(this.route.snapshot.params['id']).subscribe(data => {
                console.log(data);
                this.acceso=data;
            },
            err => {
                console.log(err)
                _scope.loading=false;        
                alert("Ocurrio un error al obtener el acceso")  
            }
        );
    }

    save(){
        if(this.acceso.canal=="" || this.acceso.pto_venta==""){
            return false
        }
        let _scope = this;
        this.loading=true;
        this.loginConvergenciaService.update(this.acceso).subscribe(
            data => { 
                console.log(data)
                _scope.loading=false;
                _scope.saved=true;
            },
            err => {
                console.log(err)
                _scope.loading=false;        
                alert("Ocurrio un error al actualizar")  
            }
        );
    }
}

