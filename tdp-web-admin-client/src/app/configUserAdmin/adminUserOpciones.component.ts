import {Component} from '@angular/core';

import { ArgumentariosService } from '../services/argumentarios.service';
import { UserService } from '../services/user.service';

declare var $: any;
var $$ = require('jquery');
export class responseSecurity {
    id: number
    entidad: string
    canal: string
    nivel: string
    flujo: string
	mensaje: string
	pantalla: string
	edit: boolean
}

@Component({
  moduleId: 'adminUserOpciones',
  selector: 'adminUserOpciones',
  templateUrl: 'adminUserOpciones.template.html'
})
export class AdminUserOpcionesComponent {
	loading: boolean

	msgResult: string
	msgError: string
	
	argumentListCanal : responseSecurity[];
	argumentListNivel : responseSecurity[];
	mjsOld : string

	inputDay: Date
	inputCupones: number
	selectFranja: string

	cuponId: number

	parameterTmp: responseSecurity
	argumentSelect: boolean;

	model:any;
  	data:any[];
	constructor(private service: UserService) {
        this.data = [];
        this.model = {
          count: 0,
          currentPage: 0,
          perPage: 10,
          pages: []
        };
    }

	ngOnInit(): void {
			this.loading = false
			$$(".opcionesEdit").prop("style", "visibility: hidden !important;");
			//this.getList()
			this.argumentSelect = false
			$('.modal').modal();
	}

	removeCupon(cuponesId) {

			$('#modalEliminar').modal('open');
			this.cuponId = cuponesId

	}

	cancelArgument(argumentItem){
		argumentItem.edit= true
		this.argumentSelect=false
		argumentItem.mensaje = this.mjsOld
	}

	cancelDelete() {
			$('#modalEliminar').modal('close');
	}

	CleanMessage() {
			this.msgResult = ""
			this.msgError = ""
			this.inputDay = null
			this.inputCupones = null
			this.selectFranja = null
	}

	validate(event: any) {
			var code = event.keyCode;
			if ((code < 48 || code > 57) // numerical
					&& code !== 46 //delete
					&& code !== 8  //back space
					&& code !== 37 // <- arrow
					&& code !== 39) // -> arrow
			{
					event.preventDefault();
			}
			return
	}

/*
	search (currentPage) {
        var _this = this;
        var tag = '%' + (this.model.tag == null ? '' : this.model.tag) + '%';
        var username = '%' + (this.model.username == null ? '' : this.model.username) + '%';
        var msg = '%' + (this.model.msg == null ? '' : this.model.msg) + '%';
        var filter = {
          limit: this.model.perPage,
          skip: currentPage*this.model.perPage,
          where: {
            and: [
              {username: { like: username}}
            ]
          }
        };
        this.model.loading = true;
        this.service.search(filter).subscribe(function(data){
          _this.data = data;
          //////
          console.log(data);
          /////////
          _this.computePaginator(filter.where);
          _this.model.loading = false;
        }, function(err){});
	}
*/
}