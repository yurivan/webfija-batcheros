import { Component, OnInit } from '@angular/core';
import { ArgumentariosService } from '../services/argumentarios.service';

declare var $: any;
var $$ = require('jquery');
export class responseSecurity {
    id: number
    entidad: string
    canal: string
    nivel: string
    flujo: string
	mensaje: string
	pantalla: string
	edit: boolean
}

@Component({
  moduleId: 'adminUserPerfil',
  selector: 'adminUserPerfil',
  templateUrl: 'adminUserPerfil.template.html'
})
export class AdminUserPerfilComponent {
	loading: boolean

	msgResult: string
	msgError: string

	argumentSelect : boolean

	argumentListCanal : responseSecurity[];
	argumentListNivel : responseSecurity[];

	argumentDisabled : boolean[];
	mjsOld: string;
	data : Object;
	//edit : boolean;
	inputDay: Date
	inputCupones: number
	selectFranja: string

	cuponId: number

	parameterTmp: responseSecurity

	constructor(private argumentariosService: ArgumentariosService) {
	}

	ngOnInit(): void {
			this.loading = false
			$$(".opcionesEdit").prop("style", "visibility: hidden !important;");
			this.getList()
			this.argumentSelect = false
			$('.modal').modal();
	}

	CleanMessage() {
		this.msgResult = ""
		this.msgError = ""
		this.inputDay = null
		this.inputCupones = null
		this.selectFranja = null
	}

	getList() {
			this.CleanMessage()
			var _this = this
			_this.loading = true

			_this.argumentariosService.getListArgumentariosCanal('CALL CENTER').subscribe(function (data) {
					console.log(data)
					_this.argumentListCanal = data.json();
					for (let argumentItem of _this.argumentListCanal){
						argumentItem.edit=true
					}
					//console.log(parameterTmp)
					_this.loading = false
			}, function (error) {
					console.log(error)
					_this.loading = false
					this.msgError = "No se logró obtener Argumentarios"
			})

	}


	removeCupon(cuponesId) {
			$('#modalEliminar').modal('open');
			this.cuponId = cuponesId
	}

	editArgument(argumentItem){
		//habilitar la edicion
		if(this.argumentSelect){
			alert("Ya esta modificando un mensaje")
		}else{
			argumentItem.edit= false
			this.mjsOld = argumentItem.mensaje	
		}
			this.argumentSelect=true
		//this.argumentDisabled[argumentItem.edit]=false
	}
	cancelArgument(argumentItem){
		argumentItem.edit= true
		this.argumentSelect=false
		argumentItem.mensaje = this.mjsOld
	}

	cancelDelete() {
			$('#modalEliminar').modal('close');
	}



	validate(event: any) {
			var code = event.keyCode;
			if ((code < 48 || code > 57) // numerical
					&& code !== 46 //delete
					&& code !== 8  //back space
					&& code !== 37 // <- arrow
					&& code !== 39) // -> arrow
			{
					event.preventDefault();
			}
			return
	}

/*
	search (currentPage) {
        var _this = this;
        var tag = '%' + (this.model.tag == null ? '' : this.model.tag) + '%';
        var username = '%' + (this.model.username == null ? '' : this.model.username) + '%';
        var msg = '%' + (this.model.msg == null ? '' : this.model.msg) + '%';
        var filter = {
          limit: this.model.perPage,
          skip: currentPage*this.model.perPage,
          where: {
            and: [
              {username: { like: username}}
            ]
          }
        };
        this.model.loading = true;
        this.service.search(filter).subscribe(function(data){
          _this.data = data;
          //////
          console.log(data);
          /////////
          _this.computePaginator(filter.where);
          _this.model.loading = false;
        }, function(err){});
	  }
*/
}