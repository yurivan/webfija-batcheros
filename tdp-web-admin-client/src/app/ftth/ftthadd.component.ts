import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { FtthService } from '../services/ftth.service';
import { UbigeoService } from '../services/ubigeo.service';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';


@Component({
    moduleId: 'ftthadd',
    selector: 'ftthadd',
    templateUrl: 'ftthadd.template.html'
})

export class FtthAddComponent implements OnInit {

    model: any;
    loading: boolean;
    departamento: any[];
    provincia: any[];
    distrito: any[];

    constructor(private router: Router, private service: FtthService, private ubigeo: UbigeoService) { 
        this.model = {  };
        this.model.departamento="";
        this.model.provincia="";
        this.model.distrito="";
        this.loading = true;
        let _scope = this;
        this.ubigeo.list({'coddpto':'%','codprov':'00','coddist':'00'}).subscribe(data => {
            console.log(data['_body']);
            _scope.departamento=(JSON.parse(data['_body']).data);
            _scope.provincia=[];
            _scope.distrito=[];
        this.loading = false;
        },
        error => {

        });
    }
    returnList(){
        this.router.navigate(['/ftth'])        
    }

    ngOnInit(){
        
    }

    onSubmit(){
        let _scope = this;
    
        if(this.model.distrito=="" || this.model.provincia=="" || this.model.departamento==""){
            return;
        }

        console.log(this.model);

        this.model.distrito=this.distrito.find(x => x.coddist == this.model.distrito).nomubigeo.toUpperCase()
        this.model.provincia=this.provincia.find(x => x.codprov == this.model.provincia).nomubigeo.toUpperCase()
        this.model.departamento=this.departamento.find(x => x.coddpto == this.model.departamento).nomubigeo.toUpperCase()

        this.loading = true;

        console.log(this.model);
        this.service.save(this.model).subscribe(function () {
          _scope.loading = false;
          _scope.router.navigate(['ftth']);
        }, function (err) {
          console.log(err);
          _scope.loading = false;
          alert('No se pudo registrar el edificio!');
        });
    }

    updateUbigeo(type){
        var filters = {'coddpto': this.model.departamento ,'codprov':'%','coddist':'00'}
        
        this.distrito=[]
        this.model.distrito=""
        
        if(type == 1){
            this.model.provincia=""
            if(this.model.departamento==''){
                this.provincia=[]
                return
            }
        }else{
            if(this.model.provincia==''){
                return
            }
            filters.codprov=this.model.provincia
            filters.coddist='%'
        }
        
        var _scope = this
        this.ubigeo.list(filters).subscribe(data => {
            console.log(data['_body']);
            if(type==1){
                _scope.provincia=(JSON.parse(data['_body']).data);
            }else{
                _scope.distrito=(JSON.parse(data['_body']).data);
            }
            this.loading = false;
        },
        error => {

        });
    }
}

