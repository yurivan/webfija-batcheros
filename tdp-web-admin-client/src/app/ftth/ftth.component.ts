import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { FtthService } from '../services/ftth.service';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';
import * as XLSX from 'xlsx';
type AOA = any[][];

@Component({
    moduleId: 'ftth',
    selector: 'ftth',
    templateUrl: 'ftth.template.html'
})

export class FtthComponent implements OnInit {

    data: any[];
    model: any;
    loading: boolean;
    csvContent: string;
    readed: any;
    @ViewChild('myInput')
    myInputVariable: ElementRef;
    resp: any[];
    
    constructor(private router: Router, private ftthService: FtthService) { 
        this.model = {
            count: 0,
            currentPage: 0,
            perPage: 10,
            pages: []
          };

    }
    returnList(){
        this.router.navigate(['/administration'])        
    }

    ngOnInit(){
        this.search(this.model.currentPage);
    }

    onSearch () {
        this.search(0);
    }

    gotoPage (i) {
        this.model.currentPage = i;
        this.search(i);
    }

    gotoNextPage () {
        var nextPage = this.model.currentPage + 1;
        if (this.model.numPages <= nextPage) {
            this.model.currentPage = this.model.numPages-1;
        } else {
            this.model.currentPage = nextPage;
        }
        this.search(this.model.currentPage);
    }

    gotoPrevPage () {
        var nextPage = this.model.currentPage - 1;
        if (0 >= nextPage) {
            this.model.currentPage = 0;
        } else {
            this.model.currentPage = nextPage;
        }
        this.search(this.model.currentPage);
    }

    search (currentPage) {
        var _this = this;
        var username = '%' + (this.model.username == null ? '' : this.model.username) + '%';
        var filter = {
            limit: this.model.perPage,
            skip: currentPage*this.model.perPage,
            where: {
            and: [
                {username: { like: username}}
            ]
            }
        };
        this.model.loading = true;
        this.loading=true;
        this.ftthService.search(filter).subscribe(function(data){
            _this.loading=false;
            _this.data = data;
            console.log(data);
            _this.computePaginator(filter.where);
        }, function(err){
            _this.loading=false;
        });
    }

    computePaginator (where) {
        var _this = this;
        this.ftthService.count(where).subscribe(function (data) {
            _this.model.pages = []
            _this.model.count = data.count;
            _this.model.numPages = Math.ceil(_this.model.count / _this.model.perPage);
            var numPages = _this.model.numPages;
            var min = _this.model.currentPage - 5;
            var max = _this.model.currentPage + 5;
            if (min < 0) min = 0;
            if (max > numPages) max = numPages;
            for (var i = min; i<max; i++) {
            _this.model.pages.push(i);
            }
        });
    }

    onFileLoad(fileLoadedEvent) {
        const textFromFileLoaded = fileLoadedEvent.target.result;              
        this.csvContent = textFromFileLoaded;    
        console.log(this.csvContent);
    }

    fileChange(event) {

        var _scope = this;
        
        let fileList: FileList = event.target.files;
        const target: DataTransfer = <DataTransfer>(event.target);

        if(fileList.length > 0) {

            let file: File = fileList[0];

            if(file.name.split('.').pop().toUpperCase()=='XLSX'){

                this.loading=true;

                const reader: FileReader = new FileReader();
                reader.onload = (e: any) => {   
                    const bstr: string = e.target.result;
                    const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});
                    const wsname: string = wb.SheetNames[0];
                    const ws: XLSX.WorkSheet = wb.Sheets[wsname];
                    this.readed = XLSX.utils.sheet_to_json(ws, {header: 1});
                    
                    let datasend = [];
                    
                    for(let data of this.readed){
                        console.log(data);
                        
                        if(data.length!=0){
                            datasend.push(data);
                        }
                    }
                    
                    this.ftthService.masive(datasend).subscribe(data => {
                        _scope.reset()
                        _scope.loading = false;
                        let datos = JSON.parse(data["_body"])['data']
                        alert('Se insertaron '+datos['insertados']+' con exito, se encontraron '+
                            datos['duplicados']+' duplicados');
                        _scope.search(this.model.currentPage);
                    }, error => {
                        _scope.reset()
                        console.log(error);
                        _scope.loading = false;
                        alert('Error, problemas con el formato, revisar el archivo');
                    });
                };
                reader.readAsBinaryString(target.files[0]);
            }else{
                _scope.reset()
                alert("Formato no aceptado")
            }
        }
    }


    reset() {
        console.log(this.myInputVariable.nativeElement.files);
        this.myInputVariable.nativeElement.value = "";
        console.log(this.myInputVariable.nativeElement.files);
    }
}

