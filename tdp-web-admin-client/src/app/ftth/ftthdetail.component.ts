import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { FtthService } from '../services/ftth.service';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';
import {ActivatedRoute} from '@angular/router';

@Component({
    moduleId: 'ftthdetail',
    selector: 'ftthdetail',
    templateUrl: 'ftthdetail.template.html'
})

export class FtthDetailComponent implements OnInit {

    model: any;
    loading: boolean;

    constructor(private router: Router, private service: FtthService,private route:ActivatedRoute) { 
        this.model = {  };
        this.loading = false;
    }
    returnList(){
        this.router.navigate(['/ftth'])        
    }

    ngOnInit(){
        this.service.list(this.route.snapshot.params['id']).subscribe(data => {
           this.model = JSON.parse(data['_body'])
        })
    }

}

