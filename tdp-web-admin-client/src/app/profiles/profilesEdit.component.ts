import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';
import { profilesService } from '../services/profiles.service';
import { Profiles } from './profiles.component';
import {ActivatedRoute} from '@angular/router';

@Component({
    moduleId: 'profilesEdit',
    selector: 'profilesEdit',
    templateUrl: 'profilesEdit.template.html'
})
export class profilesEditComponent implements OnInit {

    perfil: Profiles;
    loading: boolean;
    saved: boolean;

    constructor(private router: Router,private profilesService:profilesService,private route:ActivatedRoute) {
        this.router = router;
    }

    ngOnInit() {
        this.initProfile();
    }

    returnList() {
        this.router.navigate(['/permisos-perfiles']);
    }

    initProfile(){
        this.perfil = new Profiles;
        let _scope = this;
        this.profilesService.get(this.route.snapshot.params['id']).subscribe(data => {
                console.log(data);
                this.perfil=data;
            },
            err => {
                console.log(err)
                _scope.loading=false;        
                alert("Ocurrio un error al insertar, nivel duplicado")  
            }
        );
    }

    save(){
        if(this.perfil.name=="" || this.perfil.nivel==0){
            return false
        }
        let _scope = this;
        this.loading=true;
        this.profilesService.update(this.perfil).subscribe(
            data => { 
                console.log(data)
                _scope.loading=false;
                _scope.saved=true;
            },
            err => {
                console.log(err)
                _scope.loading=false;        
                alert("Ocurrio un error al actualizar")  
            }
        );
    }
}

