import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';
import { profilesService } from '../services/profiles.service';

export class Profiles{
    id: number;
    name: string;
    canal: string;
    nivel: number;
    f_venta: boolean;
    f_bandeja: boolean;
    f_recupero: boolean;
    f_reptrack: boolean;
    f_repventa: boolean;
}

@Component({
    moduleId: 'profiles',
    selector: 'profiles',
    templateUrl: 'profiles.template.html'
})
export class profilesComponent implements OnInit {

    perfiles: Profiles[];
    loading: boolean;

    constructor(private router: Router,private profilesService:profilesService) {
        this.router = router;
    }

    ngOnInit() {
        this.cargarProfiles();
    }

    returnList() {
        this.router.navigate(['/administration']);
    }

    cargarProfiles() {
        let _scope = this;
        this.perfiles = [];
        this.loading=true;
        this.profilesService.getProfiles().subscribe(
            data => {
                _scope.perfiles = data;
                console.log(_scope.perfiles);  
                _scope.loading=false;
            },
            err => {
                console.log(err)
                _scope.loading=false;        
            }
        );
    }

    updatear(id){
        let data=this.perfiles.find(i => i.id === id)
        let _scope = this;
        this.loading=true;   
        this.profilesService.update(data).subscribe(
            data => {
                _scope.loading=false;   
            },
            err => {
                console.log(err)
                _scope.loading=false;        
            }
        );
    }

    delete(id){
        let _scope = this;
        this.loading=true;   
        this.profilesService.delete(id).subscribe(
            data => {
                _scope.loading=false;   
                _scope.cargarProfiles()
            },
            err => {
                console.log(err)
                _scope.loading=false;      
            }
        );
    }

}

