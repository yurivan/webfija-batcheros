import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';
import { profilesService } from '../services/profiles.service';
import { Profiles } from './profiles.component';

@Component({
    moduleId: 'profilesAdd',
    selector: 'profilesAdd',
    templateUrl: 'profilesAdd.template.html'
})
export class profilesAddComponent implements OnInit {

    perfil: Profiles;
    loading: boolean;
    saved: boolean;

    constructor(private router: Router,private profilesService:profilesService) {
        this.router = router;
    }

    ngOnInit() {
        this.initProfile();
    }

    returnList() {
        this.router.navigate(['/permisos-perfiles']);
    }

    initProfile(){
        this.perfil = new Profiles;
        this.perfil.name = ""
        this.perfil.nivel = 0;
        this.perfil.canal = "";
        this.perfil.f_venta = false;
        this.perfil.f_bandeja = false;
        this.perfil.f_recupero = false;
        this.perfil.f_reptrack = false;
        this.perfil.f_repventa = false;
        this.saved=false;
        this.loading=false;
    }

    add(){
        if(this.perfil.name=="" || this.perfil.nivel==0){
            return false
        }
        let _scope = this;
        this.loading=true;
        this.profilesService.save(this.perfil).subscribe(
                    data => { 
                        console.log(data)
                        _scope.loading=false;
                        _scope.saved=true;
                    },
                    err => {
                        console.log(err)
                        _scope.loading=false;        
                        alert("Ocurrio un error al insertar, nivel duplicado")  
                    }
                );
    }
}

