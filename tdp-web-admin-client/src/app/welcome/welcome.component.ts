import { Component } from '@angular/core';

@Component({
  moduleId: 'welcome',
  selector: 'my-welcome',
  templateUrl: 'welcome.template.html'
})
export class WelcomeComponent {
  currentUser: any;

  constructor() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.currentUser = this.currentUser == null ? {} : this.currentUser;
  }
}
