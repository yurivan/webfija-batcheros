import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { MessageService } from '../services/message.service';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';

export class MessageParameters {
    id: number;
    service: string;
    orden: number;
    descriptionkey: string;
    servicekey: string;
    responsemessage: string;
    responsecode: number;
    servicecode: string;
}
@Component({
    moduleId: 'listmessage',
    selector: 'listmessage',
    templateUrl: 'listmessage.template.html'
})
export class ListMessageComponent implements OnInit, AfterViewInit {
    ngAfterViewInit(): void {

    }
    returnList() {
        this.router.navigate(['/administration']);
    }

    ngOnInit() {
    }
    constructor(private router: Router) { }
}

