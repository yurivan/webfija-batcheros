import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { EquivalentService } from '../services/equivalent.service'

@Component({
  moduleId: 'equivalent',
  selector: 'my-equivalent',
  templateUrl: './equivalent.template.html'
})
export class EquivalentComponent implements OnInit {
  equivalentList = [];
  fichaList = [];
  showResults = false;
  etiquetaCampoDestino = "Campo";
  etiquetaCampoOrigen = "Valor en Origen";

  //Sprint 27 - dochoaro
  etiquetaTipificacion = "TIPO FLUJO VENTA";
  flagTipif = false;
  //Sprint 27 - dochoaro

  model: any;
  constructor(private route: ActivatedRoute, private equivalentService: EquivalentService) {
    this.model = {
      count: 0,
      currentPage: 0,
      perPage: 10,
      pages: [],
      loading: false
    };
  }

  ngOnInit() {
    var _this = this;
    this.model.categoryValue = '0';
    this.loadFichas();

    this.route.params
      .subscribe(function (params) {
        //_this.idEquivalent = params['id'];
        let opcion = params['opcion'];
        if ('0' != opcion) {
          _this.model.categoryValue = opcion;
          _this.onSearch();
        }
      }, function (err) {
        console.log(err);
      });
    //this.search(this.model.currentPage);
  }

  loadFichas() {
    var _this = this;
    var filterFicha = {
      where: {
        and: [
          { domain: 'EQUIVALENCIA' },
          { category: 'FICHAPRODUCTO' }
        ]
      }
    };
    this.equivalentService.search(filterFicha).subscribe(function (data) {
      _this.fichaList = data;
    }, function (err) { });;
  }

  loadCamposMapeados() {
    var _this = this;
    var filterCampoMapeo = {
      where: {
        and: [
          { domain: 'EQUIVALENCIA' },
          { category: this.model.categoryValue + '.CAMPOS' }
        ]
      }
    };
    this.equivalentService.search(filterCampoMapeo).subscribe(function (data) {
      for (let i = 0; i < data.length; i++) {
        let item = data[i];
        if ('CAMPOSORIGEN' == item.element) {
          _this.etiquetaCampoOrigen = item.strvalue;

        } else if ('CAMPODESTINO' == item.element) {
          _this.etiquetaCampoDestino = item.strvalue;
        }
      }

      //Sprint 27 - dochoaro
      if (_this.model.categoryValue == "FICHAPRODUCTO.CANAL") {
        _this.flagTipif = true;
      } else {
        _this.flagTipif = false;
      }
      //Sprint 27 - dochoaro

    }, function (err) { });;
  }

  onSearch() {
    if ('0' == this.model.categoryValue) {
      alert("Por favor seleccione una Equivalencia");
      return;
    }
    this.loadCamposMapeados();
    this.search(0);
  }

  gotoPage(i) {
    this.model.currentPage = i;
    this.search(i);
  }

  gotoNextPage() {
    var nextPage = this.model.currentPage + 1;
    if (this.model.numPages < nextPage) {
      this.model.currentPage = this.model.numPages;
    } else {
      this.model.currentPage = nextPage;
    }
    this.search(this.model.currentPage);
  }

  gotoPrevPage() {
    var nextPage = this.model.currentPage - 1;
    if (0 >= nextPage) {
      this.model.currentPage = 0;
    } else {
      this.model.currentPage = nextPage;
    }
    this.search(this.model.currentPage);
  }

  search(currentPage) {
    var _this = this;
    var domainValue = 'EQUIVALENCIA';
    var categoryValue = (this.model.categoryValue == null ? '' : this.model.categoryValue);
    var elementValue = '%' + (this.model.elementValue == null ? '' : this.model.elementValue) + '%';
    var filter = {
      limit: this.model.perPage,
      skip: currentPage * this.model.perPage,
      where: {
        and: [
          { domain: domainValue },
          { category: categoryValue },
          { element: { like: elementValue } }
        ]
      }
    };
    this.model.loading = true;
    this.equivalentService.search(filter).subscribe(function (data) {
      _this.equivalentList = data;
      _this.computePaginator(filter.where);
      _this.model.loading = false;
      _this.showResults = true;
    }, function (err) { });;
  }

  computePaginator(where) {
    var _this = this;
    this.equivalentService.count(where).subscribe(function (data) {
      _this.model.pages = []
      _this.model.count = data.count;
      _this.model.numPages = Math.ceil(_this.model.count / _this.model.perPage);
      var numPages = _this.model.numPages;
      var min = _this.model.currentPage - 5;
      var max = _this.model.currentPage + 5;
      if (min < 0) min = 0;
      if (max > numPages) max = numPages;
      for (var i = min; i < max; i++) {
        _this.model.pages.push(i);
      }
    });
  }
}
