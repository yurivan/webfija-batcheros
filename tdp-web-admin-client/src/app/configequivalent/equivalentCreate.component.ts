import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { EquivalentService } from '../services/equivalent.service';
import { SalesAgentService } from '../services/salesAgent.service'

export class Equivalent {
    //id : number
    domain: string
    category: string
    element: string
    strvalue: string
    strfilter: string
}
@Component({
    moduleId: 'equivalentCreate',
    selector: 'my-equivalentCreate',
    templateUrl: './equivalentCreate.template.html'
})

export class EquivalentCreateComponent implements OnInit {

    equivalent: Equivalent
    ocultar: boolean
    responseTitle: string
    showError: boolean
    etiquetaCampoDestino = "Valor en Destino";
    etiquetaCampoOrigen = "Valor en Origen";

    //Sprint 27 - dochoaro
    tipificacionList = [];
    etiquetaTipificacion = "TIPO FLUJO VENTA";
    flagTipif = false;
    //Sprint 27 - dochoaro

    constructor(private route: ActivatedRoute, private equivalentService: EquivalentService, private salesAgentService: SalesAgentService) {
        this.equivalent = new Equivalent()
        this.ocultar = false
        this.responseTitle = ""
        this.showError = false
    }

    ngOnInit() {
        var _this = this;
        this.equivalent.strfilter = '0';
        this.loadTipificacion();
        this.route.params
            .subscribe(function (params) {
                //_this.idEquivalent = params['id'];
                _this.etiquetaCampoDestino = params['etiquetaCampoDestino'];
                _this.etiquetaCampoOrigen = params['etiquetaCampoOrigen'];

                //_this.equivalent.id = 0;
                _this.equivalent.domain = 'EQUIVALENCIA';
                _this.equivalent.category = params['equivalencia'];

                //Sprint 27 - dochoaro
                _this.flagTipif = params['flagTipif']
                //Sprint 27 - dochoaro

                /*_this.equivalentService.loadById(params['id'])
                .subscribe(function(data){
                  _this.equivalent = data;
                  //_this.loadFieldEdit(data);
                  //console.log(_this.product);
                },function(err){
                  console.log(err);
                });*/
            }, function (err) {
                console.log(err);
            });
    }

    //Sprint 27 - dochoaro
    loadTipificacion() {
        var _this = this;
        var filterTipificacion = {
          where: {
            and: [
              { domain: 'EQUIVALENCIA' },
              { category: 'TIPIFICACION' }
            ]
          }
        };
        this.equivalentService.search(filterTipificacion).subscribe(function (data) {
          _this.tipificacionList = data;
        }, function (err) { });;
      }
      //Sprint 27 - dochoaro

    isValid() {
        //estas validaciones deberia realizarse en el form... pendiente...
        if (!this.equivalent.element) {
            this.showError = true;
            this.responseTitle = "Por favor ingresar correctamente el nombre de campo destino";
            return false;
        }
        if (!this.equivalent.strvalue) {
            this.showError = true;
            this.responseTitle = "Por favor ingresar correctamente los valores equivalentes";
            return false;
        }
        if ('0' == this.equivalent.strfilter) {
            alert("Por favor seleccione un flujo de venta");
            return;
          }
        return true;
    }

    create() {
        //validamos datos ingresados... a la mala
        if (!this.isValid()) {
            return;
        }
        var __this = this;
        this.equivalentService
            .add(this.equivalent)
            .subscribe((data) => {
                if (data["status"] == 200) {

                    let campaignEquivalent = {
                        equivalencia: __this.equivalent.category,
                        canalEquivalencia: __this.equivalent.element,
                        canalOrigenNuevo: __this.equivalent.strvalue,
                        tipificacion: __this.equivalent.strfilter
                    };

                    __this.salesAgentService.updateCampaignEquivalentByChannel(campaignEquivalent).subscribe(function (updated) {
                        __this.ocultar = true;
                        __this.responseTitle = "Se guardó correctamente"
                    }, function (err) {
                        console.log(err);
                        //this.loadFieldEdit(_this.equivalent);
                    });


                } else {
                    this.showError = true
                    this.responseTitle = "Ocurrió un problema en el servidor"
                    //this.responseTitle = "El CODIGO ATIS o DNI ya se encuentra registrado"
                }
            })
    }
    validate(event: any) {
        var code = event.keyCode;
        if ((code < 48 || code > 57) // numerical
            && code !== 46 //delete
            && code !== 8  //back space
            && code !== 37 // <- arrow
            && code !== 39) // -> arrow
        {
            event.preventDefault();
        }
        return
    }
}
