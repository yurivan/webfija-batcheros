import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { EquivalentService } from '../services/equivalent.service'
import { SalesAgentService } from '../services/salesAgent.service'

declare var $: any;

@Component({
	moduleId: 'equivalentDetail',
	selector: 'my-equivalentDetail',
	templateUrl: './equivalentDetail.template.html'
})
export class EquivalentDetailComponent implements OnInit {
	equivalent: any;
	showEditEquivalent: string;
	nameEdit: string;
	messageValidate: string;
	idEquivalent: any;
	editEquivalent: any;
	showMessageValidator: any;
	etiquetaCampoDestino = "Valor en Destino";
	etiquetaCampoOrigen = "Valor en Origen";

	//Sprint 27 - dochoaro
	tipificacionList = [];
	etiquetaTipificacion = "TIPO FLUJO VENTA";
	flagTipif: boolean = false;
	//Sprint 27 - dochoaro

	processingDelete = false;

	ocultar: boolean
	responseTitle: string

	constructor(private route: ActivatedRoute, private router: Router, private equivalentService: EquivalentService,
		private salesAgentService: SalesAgentService) {
		this.equivalent = {};
		this.showEditEquivalent = 'hide';
		this.nameEdit = "Editar";
		this.messageValidate = "";
		this.idEquivalent = "";
		this.showMessageValidator = 'hide';
		this.editEquivalent = {};
	}

	ngOnInit() {
		var _this = this;
		this.loadTipificacion();
		//console.log(this.route.params);
		this.route.params
			.subscribe(function (params) {
				_this.idEquivalent = params['id'];
				_this.etiquetaCampoDestino = params['etiquetaCampoDestino'];
				_this.etiquetaCampoOrigen = params['etiquetaCampoOrigen'];
				if (params['flagTipif'] == "true") {
					_this.flagTipif = true
				} else {
					_this.flagTipif = false
				}

				_this.equivalentService.loadById(params['id'])
					.subscribe(function (data) {
						_this.equivalent = data;
						_this.loadFieldEdit(data);
						//console.log(_this.product);
					}, function (err) {
						console.log(err);
					});
			}, function (err) {
				console.log(err);
			});
	}

	//Sprint 27 - dochoaro
	loadTipificacion() {
		var _this = this;
		var filterTipificacion = {
			where: {
				and: [
					{ domain: 'EQUIVALENCIA' },
					{ category: 'TIPIFICACION' }
				]
			}
		};
		this.equivalentService.search(filterTipificacion).subscribe(function (data) {
			_this.tipificacionList = data;
		}, function (err) { });;
	}
	//Sprint 27 - dochoaro

	modalFieldEdit() {

		if (this.showEditEquivalent == 'show') {

			if (this.nameEdit == "Guardar") {

				$('.modal').modal();
				$('#modal1').modal('open');

			}
		} else {

			this.showEditEquivalent = 'show';
			this.nameEdit = "Guardar";


		}

	}
	fieldEdit() {
		this.messageValidate = "";
		this.showMessageValidator = 'hide';
		if (this.nameEdit == "Guardar") {

			var _this = this;

			$("input").each(function () {

				if ($(this).val().length == 0) {

					_this.messageValidate = "Verifique que todos los campos tenga los valores correctos";//_this.messageValidate + $(this).attr("name") + ", ";

				}
			});

			if (this.messageValidate.length == 0) {
				this.equivalentService.updateEquivalent(this.idEquivalent, this.editEquivalent).subscribe(function (dataEquivalent) {
					if (dataEquivalent) {
						// actualizamos en la ficha de vendedores las equivalencias de canal y entidad
						_this.equivalent = dataEquivalent;
						let campaignEquivalent = {
							equivalencia: _this.equivalent.category,
							canalEquivalencia: _this.editEquivalent.element,
							canalOrigenNuevo: _this.editEquivalent.strvalue,
							tipificacion: _this.editEquivalent.strfilter
						};
						_this.salesAgentService.updateCampaignEquivalentByChannel(campaignEquivalent).subscribe(function (updated) {
						}, function (err) {
							console.log(err);
							//this.loadFieldEdit(_this.equivalent);
						});
					}

				}, function (err) {
					console.log(err);
					this.loadFieldEdit(_this.equivalent);
				});
			} else {
				//_this.messageValidate = "Los campos "+ this.messageValidate + " tienen valores no permitidos";
				this.showMessageValidator = 'show';
			}


		}
		if (this.messageValidate.length == 0) {

			if (this.showEditEquivalent == 'show') {
				this.showEditEquivalent = 'hide';
				this.nameEdit = "Editar";

			} else {

				this.showEditEquivalent = 'show';
				this.nameEdit = "Guardar";


			}
		}

	}
	cancelEdit() {

		if (this.showEditEquivalent == 'show') {
			this.showEditEquivalent = 'hide';
			this.nameEdit = "Editar";
			this.showMessageValidator = 'hide';
			this.loadFieldEdit(this.equivalent);
		}
		/*else{
			this.showEditProduct = 'show';
		this.nameEdit = "Guardar";

		}*/

	}
	loadFieldEdit(data) {
		this.editEquivalent.element = data.element;
		this.editEquivalent.strvalue = data.strvalue;
		this.editEquivalent.strfilter = data.strfilter;
	}


	deleteEquivalent() {
		var _this = this;
		var equivalentCategory = this.equivalent.category;
		this.processingDelete = true;
		this.equivalentService.remove(this.idEquivalent).subscribe(function (dataEquivalent) {
			//alert("Equivalencia eliminada");

			let campaignEquivalent = {
				equivalencia: _this.equivalent.category,
				canalEquivalencia: '-',
				canalOrigenNuevo: _this.equivalent.strvalue
			};
			_this.salesAgentService.updateCampaignEquivalentByChannel(campaignEquivalent).subscribe(function (updated) {
				_this.processingDelete = false;
				_this.ocultar = true;
				_this.responseTitle = "Equivalencia eliminada correctamente";
				//_this.router.navigate(['equivalent/' + equivalentCategory]);
			}, function (err) {
				console.log(err);
				//this.loadFieldEdit(_this.equivalent);
			});




		}, function (err) {
			console.log(err);
			//this.loadFieldEdit(_this.equivalent);
		});
	}
}
