import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Rx';
import { DocumentService } from '../services/configdocuments.service';
import { error } from 'util';
import { Router } from '@angular/router';

export class Document {
    idWeb: string
    domainWeb: string
    categoryWeb: string
    elementWeb: string
    strvalueWeb: string
    idApp: string
    domainApp: string
    categoryApp: string
    elementApp: string
    strvalueApp: string
}
@Component({
    moduleId: 'configdocuments',
    selector: 'configdocuments',
    templateUrl: 'configdocuments.template.html'
})
export class ConfigDocumentsComponent implements OnInit {
    documentsParameters = []
    dni: Document
    cex: Document
    pas: Document
    ruc: Document
    otrosCE: Document
    checkStatus: boolean
    dniWebStatus: boolean
    cexWebStatus: boolean
    pasWebStatus: boolean
    rucWebStatus: boolean
    dniAppStatus: boolean
    cexAppStatus: boolean
    pasAppStatus: boolean
    rucAppStatus: boolean

    otrosCEWebStatus: boolean
    otrosCEAppStatus: boolean

    isDisabledDniWebStatus: boolean
    isDisabledCexWebStatus: boolean
    isDisabledPasWebStatus: boolean
    isDisabledRucWebStatus: boolean
    isDisabledDniAppStatus: boolean
    isDisabledCexAppStatus: boolean
    isDisabledPasAppStatus: boolean
    isDisabledRucAppStatus: boolean

    isDisabledOtrosCEWebStatus: boolean
    isDisabledOtrosCEAppStatus: boolean

    originalCheckedWeb: number
    originalCheckedApp: number

    maxAmountServicesError: boolean
    maxAmountDecodersByFloorError: boolean
    maxAmountDecodersByInAltaError: boolean
    txtResponse: string
    ocultar: boolean

    ngOnInit() {
        /*this.maxAmountServices.strvalue = ""
        this.maxAmountDecodersByFloor.strvalue = ""
        this.maxAmountDecodersByInAlta.strvalue = ""*/
        this.dniWebStatus = false
        this.cexWebStatus = false
        this.pasWebStatus = false
        this.rucWebStatus = false
        this.dniAppStatus = false
        this.cexAppStatus = false
        this.pasAppStatus = false
        this.rucAppStatus = false
        this.otrosCEAppStatus = false
        this.otrosCEWebStatus = false

        this.isDisabledDniWebStatus = false
        this.isDisabledCexWebStatus = false
        this.isDisabledPasWebStatus = false
        this.isDisabledRucWebStatus = false
        this.isDisabledDniAppStatus = false
        this.isDisabledCexAppStatus = false
        this.isDisabledPasAppStatus = false
        this.isDisabledRucAppStatus = false
        this.isDisabledOtrosCEWebStatus = false
        this.isDisabledOtrosCEAppStatus = false

        this.originalCheckedWeb = 0
        this.originalCheckedApp = 0

        this.getStatusWebDocuments()
        this.getStatusAppDocuments()

    }
    constructor(private documentService: DocumentService, private router: Router) {
        this.dni = new Document()
        this.cex = new Document()
        this.pas = new Document()
        this.ruc = new Document()
        this.otrosCE=new Document()

        this.maxAmountDecodersByFloorError = false
        this.maxAmountDecodersByInAltaError = false
        this.txtResponse = ""
        this.ocultar = false
    }
    getStatusWebDocuments() {
        this.documentService.getStatusOfWebDocuments().subscribe(parameters => {
            parameters
                .map(response => {
                    switch (response.element) {
                        case "DNI":
                            this.dni.idWeb = response.id
                            this.dni.domainWeb = response.domain
                            this.dni.categoryWeb = response.category
                            this.dni.elementWeb = response.element
                            this.dni.strvalueWeb = response.strvalue
                            if (this.dni.strvalueWeb) {

                                switch (this.dni.strvalueWeb) {
                                    case "0":
                                        this.isDisabledDniWebStatus = true
                                        break
                                    case "1":
                                        this.dniWebStatus = false
                                        break
                                    case "2":
                                        this.dniWebStatus = true
                                        this.originalCheckedWeb++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        case "CEX":

                            this.cex.idWeb = response.id
                            this.cex.domainWeb = response.domain
                            this.cex.categoryWeb = response.category
                            this.cex.elementWeb = response.element
                            this.cex.strvalueWeb = response.strvalue
                            if (this.cex.strvalueWeb) {

                                switch (this.cex.strvalueWeb) {
                                    case "0":
                                        this.isDisabledCexWebStatus = true
                                        break
                                    case "1":
                                        this.cexWebStatus = false
                                        break
                                    case "2":
                                        this.cexWebStatus = true
                                        this.originalCheckedWeb++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        case "PAS":
                            this.pas.idWeb = response.id
                            this.pas.domainWeb = response.domain
                            this.pas.categoryWeb = response.category
                            this.pas.elementWeb = response.element
                            this.pas.strvalueWeb = response.strvalue
                            if (this.pas.strvalueWeb) {

                                switch (this.pas.strvalueWeb) {
                                    case "0":
                                        this.isDisabledPasWebStatus = true
                                        break
                                    case "1":
                                        this.pasWebStatus = false
                                        break
                                    case "2":
                                        this.pasWebStatus = true
                                        this.originalCheckedWeb++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        case "RUC":

                            this.ruc.idWeb = response.id
                            this.ruc.domainWeb = response.domain
                            this.ruc.categoryWeb = response.category
                            this.ruc.elementWeb = response.element
                            this.ruc.strvalueWeb = response.strvalue
                            if (this.ruc.strvalueWeb) {
                                switch (this.ruc.strvalueWeb) {
                                    case "0":
                                        this.isDisabledRucWebStatus = true
                                        break
                                    case "1":
                                        this.rucWebStatus = false
                                        break
                                    case "2":
                                        this.rucWebStatus = true
                                        this.originalCheckedWeb++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        case "Otros Extranjeros - Aut. SNM":

                            this.otrosCE.idWeb = response.id
                            this.otrosCE.domainWeb = response.domain
                            this.otrosCE.categoryWeb = response.category
                            this.otrosCE.elementWeb = response.element
                            this.otrosCE.strvalueWeb = response.strvalue
                            if (this.otrosCE.strvalueWeb) {
                                switch (this.otrosCE.strvalueWeb) {
                                    case "0":
                                        this.isDisabledOtrosCEWebStatus = true
                                        break
                                    case "1":
                                        this.otrosCEWebStatus = false
                                        break
                                    case "2":
                                        this.otrosCEWebStatus = true
                                        this.originalCheckedWeb++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        default:
                            console.log("error")
                    }
                })
        }, error => {
            /*this.ocultar = true
            this.txtResponse = "Hubo un error, intente de nuevo."
            setTimeout(() => {
                this.ocultar = false
            }, 3000)*/
        })
    }

    getStatusAppDocuments() {
        this.documentService.getStatusOfAppDocuments().subscribe(parameters => {
            parameters
                .map(response => {
                    switch (response.element) {
                        case "DNI":

                            this.dni.idApp = response.id
                            this.dni.domainApp = response.domain
                            this.dni.categoryApp = response.category
                            this.dni.elementApp = response.element
                            this.dni.strvalueApp = response.strvalue
                            if (this.dni.strvalueApp) {
                                switch (this.dni.strvalueApp) {
                                    case "0":
                                        this.isDisabledDniAppStatus = true
                                        break
                                    case "1":
                                        this.dniAppStatus = false
                                        break
                                    case "2":
                                        this.dniAppStatus = true
                                        this.originalCheckedApp++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        case "CEX":

                            this.cex.idApp = response.id
                            this.cex.domainApp = response.domain
                            this.cex.categoryApp = response.category
                            this.cex.elementApp = response.element
                            this.cex.strvalueApp = response.strvalue
                            if (this.cex.strvalueApp) {
                                switch (this.cex.strvalueApp) {
                                    case "0":
                                        this.isDisabledCexAppStatus = true
                                        break
                                    case "1":
                                        this.cexAppStatus = false
                                        break
                                    case "2":
                                        this.cexAppStatus = true
                                        this.originalCheckedApp++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        case "PAS":

                            this.pas.idApp = response.id
                            this.pas.domainApp = response.domain
                            this.pas.categoryApp = response.category
                            this.pas.elementApp = response.element
                            this.pas.strvalueApp = response.strvalue
                            if (this.pas.strvalueApp) {
                                switch (this.pas.strvalueApp) {
                                    case "0":
                                        this.isDisabledPasAppStatus = true
                                        break
                                    case "1":
                                        this.pasAppStatus = false
                                        break
                                    case "2":
                                        this.pasAppStatus = true
                                        this.originalCheckedApp++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        case "RUC":

                            this.ruc.idApp = response.id
                            this.ruc.domainApp = response.domain
                            this.ruc.categoryApp = response.category
                            this.ruc.elementApp = response.element
                            this.ruc.strvalueApp = response.strvalue
                            if (this.ruc.strvalueApp) {
                                switch (this.ruc.strvalueApp) {
                                    case "0":
                                        this.isDisabledRucAppStatus = true
                                        break
                                    case "1":
                                        this.rucAppStatus = false
                                        break
                                    case "2":
                                        this.rucAppStatus = true
                                        this.originalCheckedApp++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        case "Otros Extranjeros - Aut. SNM":

                            this.otrosCE.idApp = response.id
                            this.otrosCE.domainApp = response.domain
                            this.otrosCE.categoryApp = response.category
                            this.otrosCE.elementApp = response.element
                            this.otrosCE.strvalueApp = response.strvalue
                            if (this.otrosCE.strvalueApp) {
                                switch (this.otrosCE.strvalueApp) {
                                    case "0":
                                        this.isDisabledOtrosCEAppStatus = true
                                        break
                                    case "1":
                                        this.otrosCEAppStatus = false
                                        break
                                    case "2":
                                        this.otrosCEAppStatus = true
                                        this.originalCheckedApp++
                                        break
                                    default:
                                        console.log("error")
                                        break
                                }
                            }
                            break
                        default:
                            console.log("error")
                    }
                })
        }, error => {
            /*this.ocultar = true
            this.txtResponse = "Hubo un error, intente de nuevo."
            setTimeout(() => {
                this.ocultar = false
            }, 3000)*/
        })
    }

    setParams() {
        let totalOfCheckedInWeb = 0
        let totalOfCheckedInApp = 0
        let checkedInWeb = []
        let checkedInApp = []

        checkedInWeb.push(this.dniWebStatus, this.cexWebStatus, this.pasWebStatus, this.rucWebStatus, this.otrosCEWebStatus)
        checkedInApp.push(this.dniAppStatus, this.cexAppStatus, this.pasAppStatus, this.rucAppStatus, this.otrosCEAppStatus)
        for (let i = 0; i < checkedInWeb.length; i++) {
            if (checkedInWeb[i]) {
                totalOfCheckedInWeb++
            }
        }
        for (let i = 0; i < checkedInApp.length; i++) {
            if (checkedInApp[i]) {
                totalOfCheckedInApp++
            }
        }
        if (this.dniWebStatus) {
            this.dni.strvalueWeb = '2'
        } else {
            if (this.dni.strvalueWeb == '1' || this.dni.strvalueWeb == '2') {
                this.dni.strvalueWeb = '1'
            } else {
                this.dni.strvalueWeb = '0'
            }

        }
        if (this.dniAppStatus) {
            this.dni.strvalueApp = '2'
        } else {
            if (this.dni.strvalueApp == '1' || this.dni.strvalueApp == '2') {
                this.dni.strvalueApp = '1'
            } else {
                this.dni.strvalueApp = '0'
            }
        }
        if (this.cexWebStatus) {
            this.cex.strvalueWeb = '2'
        } else {
            if (this.cex.strvalueWeb == '1' || this.cex.strvalueWeb == '2') {
                this.cex.strvalueWeb = '1'
            } else {
                this.cex.strvalueWeb = '0'
            }
        }
        if (this.cexAppStatus) {
            this.cex.strvalueApp = '2'
        } else {
            if (this.cex.strvalueApp == '1' || this.cex.strvalueApp == '2') {
                this.cex.strvalueApp = '1'
            } else {
                this.cex.strvalueApp = '0'
            }
        }
        if (this.pasWebStatus) {
            this.pas.strvalueWeb = '2'
        } else {
            if (this.pas.strvalueWeb == '1' || this.pas.strvalueWeb == '2') {
                this.pas.strvalueWeb = '1'
            } else {
                this.pas.strvalueWeb = '0'
            }
        }
        if (this.pasAppStatus) {
            this.pas.strvalueApp = '2'
        } else {
            if (this.pas.strvalueApp == '1' || this.pas.strvalueApp == '2') {
                this.pas.strvalueApp = '1'
            } else {
                this.pas.strvalueApp = '0'
            }
        }
        if (this.rucWebStatus) {
            this.ruc.strvalueWeb = '2'
        } else {
            if (this.ruc.strvalueWeb == '1' || this.ruc.strvalueWeb == '2') {
                this.ruc.strvalueWeb = '1'
            } else {
                this.ruc.strvalueWeb = '0'
            }
        }
        if (this.rucAppStatus) {
            this.ruc.strvalueApp = '2'
        } else {
            if (this.ruc.strvalueApp == '1' || this.ruc.strvalueApp == '2') {
                this.ruc.strvalueApp = '1'
            } else {
                this.ruc.strvalueApp = '0'
            }
        }
        if (this.otrosCEWebStatus) {
            this.otrosCE.strvalueWeb = '2'
        } else {
            if (this.otrosCE.strvalueWeb == '1' || this.otrosCE.strvalueWeb == '2') {
                this.otrosCE.strvalueWeb = '1'
            } else {
                this.otrosCE.strvalueWeb = '0'
            }
        }
        if (this.otrosCEAppStatus) {
            this.otrosCE.strvalueApp = '2'
        } else {
            if (this.otrosCE.strvalueApp == '1' || this.otrosCE.strvalueApp == '2') {
                this.otrosCE.strvalueApp = '1'
            } else {
                this.otrosCE.strvalueApp = '0'
            }
        }
        if (totalOfCheckedInApp > 0 && totalOfCheckedInWeb > 0) {
            this.documentsParameters.push(this.dni, this.cex, this.pas, this.ruc, this.otrosCE)
            let counterWeb = 0
            let counterApp = 0
            let resultWeb = false
            this.documentsParameters.map(elementWeb => {
                let body = {
                    "id": elementWeb.idWeb,
                    "strvalue": elementWeb.strvalueWeb
                }
                this.documentService.updateDocumentsStrValue(body).subscribe(data => {
                    if (counterWeb == this.documentsParameters.length - 1) {
                        resultWeb = true
                    }
                })
            })
            this.documentsParameters.map(elementApp => {
                let body = {
                    "id": elementApp.idApp,
                    "strvalue": elementApp.strvalueApp
                }
                this.documentService.updateDocumentsStrValue(body).subscribe(data => {
                    if (counterApp == this.documentsParameters.length - 1) {
                        // if (resultWeb) {


                        // }
                    }
                }, error => { }
                    , () => {
                        this.ocultar = true
                        this.txtResponse = "Se guardaron los cambios."
                        setTimeout(() => {
                            this.ocultar = false
                        }, 3000)
                        this.originalCheckedApp = totalOfCheckedInApp
                        this.originalCheckedWeb = totalOfCheckedInWeb
                    }
                )
            })
        }else {
            alert("Error: Al menos un tipo de documento debe estar seleccionado en WEB y APP")
        }
    }
    return() {
        let totalOfCheckedInWeb = 0
        let totalOfCheckedInApp = 0
        let isThereAnyChangeInWeb = false
        let isThereAnyChangeInApp = false
        let checkedInWeb = []
        let checkedInApp = []

        checkedInWeb.push(this.dniWebStatus, this.cexWebStatus, this.pasWebStatus, this.rucWebStatus, this.otrosCEWebStatus)
        checkedInApp.push(this.dniAppStatus, this.cexAppStatus, this.pasAppStatus, this.rucAppStatus, this.otrosCEAppStatus)
        for (let i = 0; i < checkedInWeb.length; i++) {
            if (checkedInWeb[i]) {
                totalOfCheckedInWeb++
            }
        }
        for (let i = 0; i < checkedInApp.length; i++) {
            if (checkedInApp[i]) {
                totalOfCheckedInApp++
            }
        }
        if(totalOfCheckedInWeb != this.originalCheckedWeb) {
            isThereAnyChangeInWeb = true
        }
        if(totalOfCheckedInApp != this.originalCheckedApp) {
            isThereAnyChangeInApp = true
        }
        if(isThereAnyChangeInApp) {
            if(confirm("Han habido cambios seguro que desea salir ? ")){
                this.router.navigate(['/administration'])
            }                
        }else {
            if(isThereAnyChangeInApp) {
                if(confirm("Han habido cambios seguro que desea salir ? ")){
                    this.router.navigate(['/administration'])
                }  
            }else {
                this.router.navigate(['/administration'])
            }
        }  
    }
}