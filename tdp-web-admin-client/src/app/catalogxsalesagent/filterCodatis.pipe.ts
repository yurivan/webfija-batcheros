import {Pipe} from '@angular/core';

import {CatalogxSalesAgentService} from '../services/catalogxsalesagent.service';

@Pipe({
  name: 'searchCodatis',
  pure: false
})
export class PipeSearchCodatis {
      constructor(private catalogxSalesAgentService:CatalogxSalesAgentService) {
      	this.catalogxSalesAgentService = catalogxSalesAgentService;
      }
      transform(args, value){
        return  args
          .filter(function(item){
              return item.codatis.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
