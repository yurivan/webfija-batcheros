import {Pipe} from '@angular/core';

import {CatalogxSalesAgentService} from '../services/catalogxsalesagent.service';

@Pipe({
  name: 'searchCampaign',
  pure: false
})
export class PipeSearchCampaign {
      constructor(private catalogxSalesAgentService:CatalogxSalesAgentService) {}
      transform(args, value){
        return  args
          .filter(function(item){
              return item.campaign.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
