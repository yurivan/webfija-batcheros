import {Component,OnInit} from '@angular/core'
import {ActivatedRoute} from '@angular/router'
import {CatalogxSalesAgentService} from '../services/catalogxsalesagent.service'

declare var $:any;

@Component({
  moduleId: 'catalogxsalesagentDetail',
  selector: 'my-catalogxsalesagentDetail',
  templateUrl: './catalogxsalesagentDetail.template.html'
})
export class CatalogxSalesAgentDetailComponent implements OnInit {
  campaign:any;
  showEditCampaign:string;
  nameEdit:string;
  messageValidate:string;
  idCatalogxSalesAgent:any;
  editCampaign:any;
  showMessageValidator:any;

      constructor(private route:ActivatedRoute, private catalogxSalesAgentService:CatalogxSalesAgentService) {
        this.campaign = {};
        this.showEditCampaign = 'hide';
        this.nameEdit = "Editar";
        this.messageValidate = "";
        this.idCatalogxSalesAgent = "";
        this.showMessageValidator = 'hide';
        this.editCampaign = {};
      }
      ngOnInit(){
        var _this = this;
        //console.log(this.route.params);
        this.route.params
          .subscribe(function(params){
            _this.idCatalogxSalesAgent = params['id'];
            _this.catalogxSalesAgentService.loadById(params['id'])
            .subscribe(function(data){
              _this.campaign = data;
              _this.loadFieldEdit(data);
              //console.log(_this.campaign);
            },function(err){
              console.log(err);
            });
          },function(err){
            console.log(err);
          });
      }

      modalFieldEdit () {

    	  if(this.showEditCampaign == 'show'){

    		  if(this.nameEdit == "Guardar"){

        		  $('.modal').modal();
              	$('#modal1').modal('open');

        	  }
    	  }else{

    		  this.showEditCampaign = 'show';
    		  this.nameEdit = "Guardar";


    	  }

        }
      fieldEdit () {
    	  this.messageValidate = "";
    	  this.showMessageValidator ='hide';
    	  if(this.nameEdit == "Guardar"){

    		  var _this = this;

    		  $( "input" ).each(function(){

    			  if($(this).val().length == 0){

    				  _this.messageValidate = "Verifique que todos los campos tenga los valores correctos";//_this.messageValidate + $(this).attr("name") + ", ";

    			  }
    		  });

    		  if(this.messageValidate.length == 0){
    			  this.catalogxSalesAgentService.updateCatalogxSalesAgent(this.idCatalogxSalesAgent,this.editCampaign).subscribe(function(dataCatalogxSalesAgent){
                      if(dataCatalogxSalesAgent){

                    	  _this.campaign = dataCatalogxSalesAgent;

                      }

                    },function(err){
                      console.log(err);
                      this.loadFieldEdit(_this.campaign);
                    });
    		  }else{
    			  //_this.messageValidate = "Los campos "+ this.messageValidate + " tienen valores no permitidos";
    			  this.showMessageValidator ='show';
    		  }


    	  }
    	  if(this.messageValidate.length == 0){

    	  if(this.showEditCampaign == 'show'){
    		  this.showEditCampaign = 'hide';
    		  this.nameEdit = "Editar";

    	  }else{

    		  this.showEditCampaign = 'show';
    		  this.nameEdit = "Guardar";


    	  }
    	  }

        }
        cancelEdit(){

        	if(this.showEditCampaign == 'show'){
      		  this.showEditCampaign = 'hide';
      		this.nameEdit = "Editar";
      		this.showMessageValidator ='hide';
      		this.loadFieldEdit(this.campaign);
      	  }
       
        }
        loadFieldEdit(data){

        	this.editCampaign.codatis = data.codatis;
		    	this.editCampaign.campaign = data.campaign;
        }
    }
