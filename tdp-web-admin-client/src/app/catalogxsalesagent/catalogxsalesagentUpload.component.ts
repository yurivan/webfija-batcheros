import {Component, OnInit} from '@angular/core'
import {CatalogxSalesAgentService} from '../services/catalogxsalesagent.service'

@Component({
  moduleId: 'catalogxsalesagentupload',
  selector: 'catalogxsalesagent-upload',
  templateUrl: './upload.template.html'
})
export class CatalogxSalesAgentUploadComponent {
  model:any;
  loading:boolean = false;
  files:any[];
      constructor(private catalogxSalesAgentService:CatalogxSalesAgentService) {
        this.model = {};
      }

      onClickOtroArchivo () {
        this.model = {};
        this.files = [];
      }

      onClickPersist () {
        var _this = this;
        this.loading = true;
        this.catalogxSalesAgentService.reset(this.files[0]).subscribe(function (responseData) {
          _this.model = responseData.data;
          _this.loading = false;
        }, function (err) {
          console.log(err);
          _this.loading = false;
        });
      }

      onFormSubmit () {
        var _this = this;
        this.loading = true;
        this.catalogxSalesAgentService.upload(this.files[0]).subscribe(function (responseData) {
          _this.model = responseData.data;
          _this.loading = false;
        }, function (err) {
          console.log(err);
          _this.loading = false;
        });
      }

      onChangeFile (evt) {
        this.files = evt.srcElement.files;
      }
    }
