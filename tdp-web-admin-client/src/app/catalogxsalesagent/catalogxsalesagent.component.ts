import {Component, OnInit} from '@angular/core'
import {CatalogxSalesAgentService} from '../services/catalogxsalesagent.service'

@Component({
  moduleId: 'catalogxsalesagent',
  selector: 'my-catalogxsalesagent',
  templateUrl: './catalogxsalesagent.template.html'
})
export class CatalogxSalesAgentComponent implements OnInit {
  catalogxsalesagent = [];
  model:any;
      constructor(private catalogxsalesagentservice:CatalogxSalesAgentService) {
        this.model = {
          count: 0,
          currentPage: 0,
          perPage: 10,
          pages: [],
          loading: false
        };
      }

      ngOnInit(){
        this.search(this.model.currentPage);
      }

      onSearch () {
        this.search(0);
      }

      gotoPage (i) {
        this.model.currentPage = i;
        this.search(i);
      }

      gotoNextPage () {
        var nextPage = this.model.currentPage + 1;
        if (this.model.numPages < nextPage) {
          this.model.currentPage = this.model.numPages;
        } else {
          this.model.currentPage = nextPage;
        }
        this.search(this.model.currentPage);
      }

      gotoPrevPage () {
        var nextPage = this.model.currentPage - 1;
        if (0 >= nextPage) {
          this.model.currentPage = 0;
        } else {
          this.model.currentPage = nextPage;
        }
        this.search(this.model.currentPage);
      }

      search (currentPage) {
        var _this = this;
        var codatis = '%' + (this.model.codatis == null ? '' : this.model.codatis) + '%';
        var campaign = '%' + (this.model.campaign == null ? '' : this.model.campaign) + '%';
        var filter = {
          limit: this.model.perPage,
          skip: currentPage*this.model.perPage,
          where: {
            and: [
              {codatis: { like: codatis}},
              {campaign: { like: campaign}}
            ]
          }
        };
        this.model.loading = true;
        this.catalogxsalesagentservice.search(filter).subscribe(function(data){
          _this.catalogxsalesagent = data;
          _this.computePaginator(filter.where);
          _this.model.loading = false;
        }, function(err){});;
      }

      computePaginator (where) {
        var _this = this;
        this.catalogxsalesagentservice.count(where).subscribe(function (data) {
          _this.model.pages = []
          _this.model.count = data.count;
          _this.model.numPages = Math.ceil(_this.model.count / _this.model.perPage);
          var numPages = _this.model.numPages;
          var min = _this.model.currentPage - 5;
          var max = _this.model.currentPage + 5;
          if (min < 0) min = 0;
          if (max > numPages) max = numPages;
          for (var i = min; i<max; i++) {
            _this.model.pages.push(i);
          }
        });
      }
    }
