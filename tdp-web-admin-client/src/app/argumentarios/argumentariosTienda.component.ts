import {Component} from '@angular/core';

import { ArgumentariosService } from '../services/argumentarios.service';

declare var $: any;
var $$ = require('jquery');
export class responseSecurity {
    id: number
    entidad: string
    canal: string
    nivel: string
    flujo: string
	mensaje: string
	pantalla: string
	edit: boolean
}

@Component({
  moduleId: 'argumentariosTienda',
  selector: 'argumentariosTienda',
  templateUrl: 'argumentariosTienda.template.html'
})
export class ArgumentariosTiendaComponent {
	loading: boolean

	msgResult: string
	msgError: string

	argumentListCanal : responseSecurity[];
	argumentListNivel : responseSecurity[];
	mjsOld : string

	inputDay: Date
	inputCupones: number
	selectFranja: string

	cuponId: number

	parameterTmp: responseSecurity
	argumentSelect: boolean;

	constructor(private argumentariosService: ArgumentariosService) {
	}

	ngOnInit(): void {
			this.loading = false
			$$(".opcionesEdit").prop("style", "visibility: hidden !important;");
			this.getList()
			this.argumentSelect = false
			$('.modal').modal();
	}

	getList() {
			this.CleanMessage()
			var _this = this
			_this.loading = true

			_this.argumentariosService.getListArgumentariosCanal('GENERAL').subscribe(function (data) {
					console.log(data)
					_this.argumentListCanal = data.json();
					for (let argumentItem of _this.argumentListCanal){
						argumentItem.edit=true
					}
					//console.log(parameterTmp)
					_this.loading = false
			}, function (error) {
					console.log(error)
					_this.loading = false
					this.msgError = "No se logró obtener Argumentarios"
			})
			_this.argumentariosService.getListArgumentariosNivel().subscribe(function (data) {
				console.log(data)
				_this.argumentListNivel = data.json();
				for (let argumentItem of _this.argumentListNivel){
					argumentItem.edit=true
				}
				console.log("data Juntada:"+data)
				//console.log(parameterTmp)
				_this.loading = false
			}, function (error) {
					console.log(error)
					_this.loading = false
					this.msgError = "No se logró obtener Argumentarios"
			})
	}

	/*addCupon() {
			var cuponDay = this.inputDay
			var cuponCupones = this.inputCupones
			var cuponFranja = this.selectFranja
			this.CleanMessage()
			this.loading = true

			if (!cuponDay) {
					this.msgError = "Dato Fecha no puede ser en blanco."
					this.loading = false
					return
			}
			if (!cuponCupones) {
					this.msgError = "Dato Cupones no puede ser en blanco."
					this.loading = false
					return
			}
			if (!cuponFranja) {
					this.msgError = "Dato Franja no puede ser en blanco."
					this.loading = false
					return
			}
			var cuponItem = {
					"day": cuponDay,
					"franja": cuponFranja,
					"cupon": cuponCupones
			}
			console.log(cuponItem)

			var _this = this
			_this.msgResult = ""
			_this.msgError = ""
			_this.CleanMessage()
			_this.loading = true

			_this.argumentariosService.updateArgumentarios(cuponItem).subscribe(function (data) {
					console.log(data)
					if (data != null && data.data != null) {
							var response = data.data
							if (response.affectedRows != null) {
									if (response.affectedRows == 1) {
											_this.msgResult = "Cupón Añadido"
									} else {
											_this.msgError = "No se logró realizar eliminar el cupón."
									}
							} else {
									_this.msgError = "No se logró grabar el cupón..."
							}
					} else {
							_this.msgError = "No se logró grabar el cupón.."
					}
					_this.loading = false
					_this.getList()
			}, function (error) {
					console.log(error)
					_this.loading = false
					_this.msgError = "No se logró eliminar el cupón."
			})
	}*/

	removeCupon(cuponesId) {

			$('#modalEliminar').modal('open');
			this.cuponId = cuponesId

	}

	editArgument(argumentItem){
		//habilitar la edicion
		if(this.argumentSelect){
			alert("Ya esta modificando un mensaje")
		}else{
			argumentItem.edit= false
			this.mjsOld = argumentItem.mensaje	
		}
			this.argumentSelect=true
		//this.argumentDisabled[argumentItem.edit]=false
	}
	cancelArgument(argumentItem){
		argumentItem.edit= true
		this.argumentSelect=false
		argumentItem.mensaje = this.mjsOld
	}

	setArgument(argumentItem){
		
		alert("Datos Mensaje:"+argumentItem.mensaje)
		
		var _this = this
			_this.msgResult = ""
			_this.msgError = ""
			_this.CleanMessage()
			_this.loading = true
		//guarda los cambios realizados
		this.argumentariosService.updateArgumentarios(argumentItem).subscribe(function (data) {
			console.log(data)
			if (data != null && data.data != null) {
					var response = data.data
					if (response.affectedRows != null) {
							if (response.affectedRows == 1) {
									_this.msgResult = "Cupón Añadido"
							} else {
									_this.msgError = "No se logró realizar eliminar el cupón."
							}
					} else {
							_this.msgError = "No se logró grabar el cupón..."
					}
			} else {
					_this.msgError = "No se logró grabar el cupón.."
			}
			_this.loading = false
			_this.getList()
	}, function (error) {
			console.log(error)
			_this.loading = false
			_this.msgError = "No se logró eliminar el cupón."
	})

	}

	/*doDelete() {

			var _this = this
			var cuponesId = _this.cuponId
			_this.msgResult = ""
			_this.msgError = ""
			_this.CleanMessage()
			_this.loading = true

			_this.agendamientoService.deleteAgendamientoCupones(cuponesId).subscribe(function (data) {
					console.log(data)
					if (data != null && data.data != null) {
							var response = data.data
							if (response.affectedRows != null) {
									if (response.affectedRows == 1) {
											_this.msgResult = "Cupón eliminado"
									} else {
											_this.msgError = "No se logró realizar eliminar el cupón."
									}
							} else {
									_this.msgError = "No se logró grabar el cupón..."
							}
					} else {
							_this.msgError = "No se logró grabar el cupón.."
					}
					_this.loading = false
					_this.getCupones()
			}, function (error) {
					console.log(error)
					_this.loading = false
					_this.msgError = "No se logró eliminar el cupón."
			})
	}*/

	cancelDelete() {
			$('#modalEliminar').modal('close');
	}

	CleanMessage() {
			this.msgResult = ""
			this.msgError = ""
			this.inputDay = null
			this.inputCupones = null
			this.selectFranja = null
	}

	validate(event: any) {
			var code = event.keyCode;
			if ((code < 48 || code > 57) // numerical
					&& code !== 46 //delete
					&& code !== 8  //back space
					&& code !== 37 // <- arrow
					&& code !== 39) // -> arrow
			{
					event.preventDefault();
			}
			return
	}
}