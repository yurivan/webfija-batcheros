import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class AgendamientoService {
    constructor(private http: HttpService) { }


    insertUpdateAgendamientoCupones(cuponItem) {

        return this.http.post(`${globals.BASE_URL}api/TdpAgendamientoCupones/insertUpdateCupon`, cuponItem)
            .map(response => {
                return response.json()
            })
    }

    getAgendamientoCupones() {

        var filterObject = {
            order: 'fecha DESC, franja'
        };

        var filter = encodeURIComponent(JSON.stringify(filterObject));
        return this.http.get(`${globals.BASE_URL}api/TdpAgendamientoCupones?filter=${filter}`)
    }

    deleteAgendamientoCupones(cuponesId) {
        var body = {
            "cuponesId": cuponesId
        }

        return this.http.post(`${globals.BASE_URL}api/TdpAgendamientoCupones/eliminarCupon`, body)
            .map(response => {
                return response.json()
            })
    }

    getconfigParameters(categoryAgendamiento, elementAgendamiento) {
        var filterObject = {}

        if (categoryAgendamiento != "") {
            filterObject = {
                where: {
                    and: [
                        { domain: 'AGENDAMIENTO' },
                        { category: categoryAgendamiento }
                    ]
                }
            }
        }

        if (categoryAgendamiento != "" && elementAgendamiento != "") {
            filterObject = {
                where: {
                    and: [
                        { domain: 'AGENDAMIENTO' },
                        { category: categoryAgendamiento },
                        { element: elementAgendamiento }
                    ]
                }
            }
        }


        var filter = encodeURIComponent(JSON.stringify(filterObject));
        return this.http.get(`${globals.BASE_URL}api/Parameters?filter=${filter}`)
    }

    setconfigParameters(categoryAgendamiento, elementAgendamiento, strvalueAgendamiento) {
        var filterObject = {}
        var body = {
            "domain": 'AGENDAMIENTO',
            "category": categoryAgendamiento,
            "element": elementAgendamiento,
            "strvalue": strvalueAgendamiento
        }

        var filter = encodeURIComponent(JSON.stringify(filterObject));
        return this.http.post(`${globals.BASE_URL}api/Parameters/modifyParameterValue`, body)
            .map(response => {
                return response.json()
            })
    }
}