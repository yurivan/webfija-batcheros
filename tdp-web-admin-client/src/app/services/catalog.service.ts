import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import { UploadService } from './upload.service';
import * as globals from './globals';

@Injectable()
export class CatalogService {
  constructor(private http: HttpService, private uploadService: UploadService) { }

  updateStrValue(data) {
    let body = JSON.stringify(data)
    console.log("catalog service " + body)
    return this.http.put(`${globals.BASE_URL}api/Parameters/modifyAllProv`, body)
  }

  getStatus() {
    return this.http.get(`${globals.BASE_URL}api/Parameters/getAllProv`)
  }

  load() {
    console.log('load')
    return this.http.get(globals.BASE_URL + 'api/TdpCatalogs')
      .map(function (response) {
        return response.json();
      });
  }

  loadById(id) {
    console.log('logbyid')
    return this.http.get(globals.BASE_URL + 'api/TdpCatalogs/' + id)
      .map(function (response) {
        return response.json();
      });
  }

  search(filterObject) {
    console.log('search')
    var filter = encodeURIComponent(JSON.stringify(filterObject));
    return this.http.get(globals.BASE_URL + 'api/TdpCatalogs?filter=' + filter)
      .map(function (response) {
        return response.json();
      });
  }

  count(whereObject) {
    console.log('count')
    var where = encodeURIComponent(JSON.stringify(whereObject));
    return this.http.get(globals.BASE_URL + 'api/TdpCatalogs/count?where=' + where)
      .map(function (response) {
        return response.json();
      });
  }

  upload(f) {
    console.log('upload(f)')
    return this.uploadService.upload(globals.BASE_URL + 'api/TdpCatalogs/upload', f);
    // .subscribe(function () {console.log('asdfhhj');}, function () {console.log('asdfaaaaaa');});
  }

  reset(f) {
    console.log('reset')
    return this.uploadService.upload(globals.BASE_URL + 'api/TdpCatalogs/reset', f);
  }

  updateCatalog(idCatalog, data) {
    console.log('updatecatalog')
    return this.http.put(globals.BASE_URL + 'api/TdpCatalogs/' + idCatalog, data).map(function (response) {
      return response.json();
    });
  }

  cargacatalog() {

    let data = '{}';
    let token = localStorage.getItem('token');

    console.log('log token', token);
    console.log('cargacatalog')
    return this.http.get(globals.BASE_URL + 'api/TdpCatalogs/getDataCatalogOffering/?access_token=' + token)
      .map(function (response) {
        return response.json();
      });
  }

  update(id, data) {
    console.log('update');
    let body = JSON.stringify(data);
    return this.http.put(globals.BASE_URL + '/api/TdpCatalogs/' + id, body);
  }
  remove(id) {
    console.log('remove');
    return this.http.delete(globals.BASE_URL + 'api/TdpCatalogs/' + id);
  }

  add(data) {
    console.log('add(data)');
    let body = JSON.stringify(data);
    return this.http.post(globals.BASE_URL + '/api/TdpCatalogs', body);
  }
}
