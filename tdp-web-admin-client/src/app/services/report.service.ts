import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class ReportService {
    constructor(private http: HttpService) { }

    load(initDate,finDate,campaign){
        let data = '{}';
        var token = localStorage.getItem('token');
       // this.http.delete(globals.BASE_URL + 'api/TdpOrders/web/removeFileReport');
        window.open(globals.BASE_URL + 'api/TdpOrders/web/saleReport/'+initDate+'/'+finDate+'/'+campaign+'?access_token='+token);
        
        //this.http.delete(globals.BASE_URL + 'api/TdpOrders/web/removeFileReport');
        
      
    }
    
    loadCampaign(){
        return this.http.get(globals.BASE_URL + 'api/TdpCatalogs/getCampaign').map(function(response){
        return response.json();
      });
    }
    
    /*removeFileReport(){
        return this.http.delete(globals.BASE_URL + 'api/TdpOrders/web/removeFileReport');
    }*/


}
