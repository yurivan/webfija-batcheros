import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import { UploadService } from './upload.service';

import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import * as io from 'socket.io-client';
import * as globals from './globals';

@Injectable()
export class NumberAgentService {
	private socket;
	private transactions: any;
	// _eventBus = new Subject<{}>();
	private observer;
	_eventBus = Observable.create(observer => {
		this.observer = observer;
		return () => {
			this.socket.disconnect();
		};
	});

	constructor(private http: HttpService, private uploadService: UploadService) {
		var that = this;
		this.transactions = {};
		this.socket = io(globals.BASE_URL);
		this.socket.on('upload.status', (data) => {
			if (data.transactionId && that.transactions[data.transactionId]) {
				if (data.progress == 100) {
					delete this.transactions[data.transactionId];
				}
				this.observer.next(data);
			}
		});
	}

	token(entidad) {
		return this.http.get(globals.BASE_URL + 'api/TokenSegs/findOne?filter={"where":{"entidad": "'+entidad+'"}}')
			.map(function (response) {
				return response.json();
			});
	}

	load() {
		return this.http.get(globals.BASE_URL + 'api/NroAgents')
			.map(function (response) {
				return response.json();
			});
	}

	loadByPages(perPage, currentPos) {
		return this.http.get(globals.BASE_URL + 'api/NroAgents?filter[limit]=' + perPage + '&filter[skip]=' + currentPos)
			.map(function (response) {
				return response.json();
			});
	}

	loadByCodAtis(codatis) {
		return this.http.get(`${globals.BASE_URL}api/NroAgents/findOne?filter={"where":{"codatis": "${codatis}"}}`)
			.map(function (response) {
				return response.json();
			});
	}

	// loadByPages(perPage,currentPos){
	//   return this.http.get('api/NroAgents?filter[limit]='+perPage+'&filter[skip]='+currentPos)
	//     .map(function(response){
	//       return response.json();
	//     });
	// }

	countData() {
		return this.http.get(globals.BASE_URL + 'api/NroAgents/count')
			.map(function (response) {
				return response.json();
			});
	}

	upload(f) {
		return this.uploadService.upload(globals.BASE_URL + 'api/NroAgents/upload', f)
			.map(data => {
				return data;
			});
		// .subscribe(function () {console.log('asdfhhj');}, function () {console.log('asdfaaaaaa');});
	}

	uploadWeb(f) {
		return this.uploadService.upload(globals.BASE_URL + 'api/NroAgents/web/upload', f)
			.map(data => {
				return data;
			});
		// .subscribe(function () {console.log('asdfhhj');}, function () {console.log('asdfaaaaaa');});
	}

	reset(f) {
		var that = this;
		return this.uploadService.upload(globals.BASE_URL + 'api/NroAgents/reset', f)
			.map(data => {
				let transactionId = data.data.transactionId;
				that.transactions[transactionId] = transactionId;
				console.log(that.transactions);
				return data;
			});
	}

	resetWeb(f, resetdata, niveldata) {
		var that = this;
		return this.uploadService.uploadWeb(globals.BASE_URL + 'api/NroAgents/web/reset', f, resetdata, niveldata)
			.map(data => {
				let transactionId = data.data.transactionId;
				that.transactions[transactionId] = transactionId;
				console.log(that.transactions);
				return data;
			});
	}

	search(filterObject) {
		var filter = encodeURIComponent(JSON.stringify(filterObject));
		return this.http.get(globals.BASE_URL + 'api/NroAgents?filter=' + filter)
			.map(function (response) {
				return response.json();
			});
	}

	count(whereObject) {
		var where = encodeURIComponent(JSON.stringify(whereObject));
		return this.http.get(globals.BASE_URL + 'api/NroAgents/count?where=' + where)
			.map(function (response) {
				return response.json();
			});
	}
	addSalesAgent(data) {
		let body = JSON.stringify(data);
		return this.http.post(`${globals.BASE_URL}api/NroAgents`, body)
			.map(response => {
				return response.json()
			})
	}

	updataSalesAgentService(data) {
		let body = JSON.stringify(data);
		return this.http.put(`${globals.BASE_URL}api/NroAgents/salesagents`, body)
			.map(response => {
				return response.json()
			})
	}

	deleteSalesAgent(dni, codAtis) {
		return this.http.delete(`${globals.BASE_URL}api/NroAgents/${codAtis}`)
			.map(response => {
				return response.json()
			})
	}

	// Sprint 12
	updateCampaignEquivalentByChannel(data) {
		let body = JSON.stringify(data);
		return this.http.put(`${globals.BASE_URL}api/NroAgents/updateCampaignEquivalentByChannel`, body)
			.map(response => {
				return response.json()
			})
	}

	addSaleAgentUser(data) {
		let body = JSON.stringify(data);
		return this.http.post(`${globals.BASE_URL}api/NroAgents/salesagentsNiveles`, body)
			.map(response => {
				return response.json()
			})
	}

	updateSalesAgentUser(data) {
		let body = JSON.stringify(data);
		return this.http.put(`${globals.BASE_URL}api/NroAgents`, body)
			.map(response => {
				return response.json()
			})
	}

	preloadSalesAgentUser(f) {
		return this.uploadService.upload(globals.BASE_URL + 'api/NroAgents/salesagentNiveles/preload', f)
			.map(data => {
				return data;
			});
	}

	uploadSalesAgentUser(f, resetdata) {
		var that = this;
		return this.uploadService.uploadWeb(globals.BASE_URL + 'api/NroAgents/salesagentNiveles/upload', f, resetdata, '')
			.map(data => {
				let transactionId = data.data.transactionId;
				that.transactions[transactionId] = transactionId;
				console.log(that.transactions);
				return data;
			});
	}

	preloadSalesSuperUser(f) {
		return this.uploadService.upload(globals.BASE_URL + 'api/NroAgents/salesagentSupervisores/preload', f)
			.map(data => {
				return data;
			});
	}


	uploadSalesSuperUser(f, resetdata) {
		var that = this;
		return this.uploadService.uploadWeb(globals.BASE_URL + 'api/NroAgents/salesagentSupervisores/upload', f, resetdata, '')
			.map(data => {
				let transactionId = data.data.transactionId;
				that.transactions[transactionId] = transactionId;
				console.log(that.transactions);
				return data;
			});
	}

}
