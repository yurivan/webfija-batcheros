import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class ArgumentariosService {
    constructor(private http: HttpService) { }


    updateArgumentarios(argumentItem) {

        return this.http.post(`${globals.BASE_URL}api/Argumentarios/updateArgumentario`, argumentItem)
            .map(response => {
                return response.json()
            })
    }

    getAgendamientoCupones() {

        var filterObject = {
            order: 'fecha DESC, franja'
        };

        var filter = encodeURIComponent(JSON.stringify(filterObject));
        return this.http.get(`${globals.BASE_URL}api/Argumentarios?filter=${filter}`)
    }

    getListArgumentariosCanal(categoryArgument) {
        var filterObject = {}
        if (categoryArgument != "") {

            filterObject = {
                where: {
                    and: [
                        { canal: categoryArgument }
                    ]
                }
            }
        }
        var filter = encodeURIComponent(JSON.stringify(filterObject));
        return this.http.get(`${globals.BASE_URL}api/Argumentarios?filter=${filter}`)
    }

    getListArgumentariosNivel() {
        var filterObject = {}
            filterObject = {
                where: {
                    and: [
                        { nivel: 'COMPLETO' }
                    ]
                }
            }
        var filter = encodeURIComponent(JSON.stringify(filterObject));
        return this.http.get(`${globals.BASE_URL}api/Argumentarios?filter=${filter}`)
    }


    setconfigParameters(categoryAgendamiento, elementAgendamiento, strvalueAgendamiento) {
        var filterObject = {}
        var body = {
            "domain": 'AGENDAMIENTO',
            "category": categoryAgendamiento,
            "element": elementAgendamiento,
            "strvalue": strvalueAgendamiento
        }

        var filter = encodeURIComponent(JSON.stringify(filterObject));
        return this.http.post(`${globals.BASE_URL}api/Parameters/modifyParameterValue`, body)
            .map(response => {
                return response.json()
            })
    }
}