import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { HttpService } from './http.service';

import * as globals from './globals';

@Injectable()
export class loginConvergenciaService {
  
  baseUrl = globals.BASE_URL+'api/LoginConvergencia';

  constructor(private http:HttpService){}

  getLoginConvergencia(){
    return this.http.get(this.baseUrl)
      .map(function(response){
        return response.json();
      });
  }

  update(data){
      let body = JSON.stringify(data);
      return this.http.put(this.baseUrl+"/"+data.id,body)
        .map(response => {
          return response.json()
        })
  }

  save(data){
    let body = JSON.stringify(data);
      return this.http.post(this.baseUrl+"/addLoginConvergencia",body)
        .map(response => {
          return response.json()
        })
  }

  delete(id){
    return this.http.delete(this.baseUrl+"/"+id)
    .map(response => {
      return response.json()
    })
  }

  get(id){
    return this.http.get(this.baseUrl+"/"+id)
    .map(response => {
      return response.json()
    })
  }

  updateflag(){
    return this.http.post(this.baseUrl+"/updatearflag",{})
    .map(response => {
      return response.json()
    })
  }

  getParameter(){
    return this.http.get(this.baseUrl+"/getflag")
    .map(response => {
      return response.json()
    })
  }
}
