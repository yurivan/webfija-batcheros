import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import { UploadService } from './upload.service';

import * as globals from './globals';

@Injectable()
export class VisorLoadLogsService{
      constructor(private http:HttpService){
        this.http = http;
      }

      load(){
        return this.http.get(globals.BASE_URL+'api/VisorLoadLogs')
          .map(function(response){
            return response.json();
          });
      }

      search (filterObject) {
        var filter = encodeURIComponent(JSON.stringify(filterObject));
        return this.http.get(globals.BASE_URL+'api/VisorLoadLogs?filter='+filter)
          .map(function(response){
            return response.json();
          });
      }

      count (whereObject) {
        var where = encodeURIComponent(JSON.stringify(whereObject));
        return this.http.get(globals.BASE_URL+'api/VisorLoadLogs/count?where='+where)
          .map(function(response){
            return response.json();
          });
      }

      loadById(id){
        return this.http.get(globals.BASE_URL+'api/VisorLoadLogs/'+id)
          .map(function(response){
            return response.json();
          });
      }
    }
