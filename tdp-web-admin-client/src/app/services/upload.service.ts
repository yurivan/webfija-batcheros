import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { HttpService } from './http.service';

@Injectable()
export class UploadService {
  constructor(private http: Http) { }

  upload(url, file) {
    var _this = this;
    var token = localStorage.getItem('token');
    return Observable.create(function (observer) {
      var formData = new FormData(), xhr = new XMLHttpRequest();

      formData.append('data', file, file.name);

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            observer.next(JSON.parse(xhr.response));
            observer.complete();
          } else {
            observer.error(xhr.response);
          }
        }
      }

      xhr.open('POST', url, true);
      xhr.setRequestHeader('Authorization', token);
      xhr.send(formData);
    });
  }

  uploadWeb(url, file, resetData, niveldata) {
    var _this = this;
    var token = localStorage.getItem('token');
    return Observable.create(function (observer) {
      var formData = new FormData(), xhr = new XMLHttpRequest();

      formData.append('data', file, file.name);
      formData.append('resetdata', resetData);
      formData.append('niveldata', niveldata);

      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            observer.next(JSON.parse(xhr.response));
            observer.complete();
          } else {
            observer.error(xhr.response);
          }
        }
      }

      xhr.open('POST', url, true);
      xhr.setRequestHeader('Authorization', token);
      xhr.send(formData);
    });
  }
}
