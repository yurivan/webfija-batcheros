import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { HttpService } from './http.service';

import * as globals from './globals';

@Injectable()
export class profilesService {
  
  baseUrl = globals.BASE_URL+'api/Profiles';

  constructor(private http:HttpService){}

  getProfiles(){
    return this.http.get(this.baseUrl)
      .map(function(response){
        return response.json();
      });
  }

  update(data){
      let body = JSON.stringify(data);
      return this.http.put(this.baseUrl+"/"+data.id,body)
        .map(response => {
          return response.json()
        })
  }

  save(data){
    let body = JSON.stringify(data);
      return this.http.post(this.baseUrl,body)
        .map(response => {
          return response.json()
        })
  }

  delete(id){
    return this.http.delete(this.baseUrl+"/"+id)
    .map(response => {
      return response.json()
    })
  }

  get(id){
    return this.http.get(this.baseUrl+"/"+id)
    .map(response => {
      return response.json()
    })
  }
}
