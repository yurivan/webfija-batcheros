import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import { UploadService } from './upload.service';
import * as globals from './globals';

@Injectable()
export class CatalogxMotorizadoService {
  constructor(private http: HttpService, private uploadService:UploadService){}
  
  loadById(id){
    console.log('logbyid')
    return this.http.get(globals.BASE_URL+'api/TdpCatalogxMotorizados/'+id)
      .map(function(response){
        return response.json();
      });
  }

  search(filterObject) {
    console.log('search')
    var filter = encodeURIComponent(JSON.stringify(filterObject));
    console.log(globals.BASE_URL+'api/TdpCatalogxMotorizados?filter='+filter)
    return this.http.get(globals.BASE_URL+'api/TdpCatalogxMotorizados?filter='+filter)
      .map(function(response){
        return response.json();
      });
  }

  count(whereObject) {
    console.log('count')
    var where = encodeURIComponent(JSON.stringify(whereObject));
    return this.http.get(globals.BASE_URL+'api/TdpCatalogxMotorizados/count?where='+where)
      .map(function(response){
        return response.json();
      });
  }

  upload(f) {
    console.log('upload(f)')
    return this.uploadService.upload(globals.BASE_URL+'api/TdpCatalogxMotorizados/upload', f);
  }

  reset(f) {
    console.log('reset')
    return this.uploadService.upload(globals.BASE_URL+'api/TdpCatalogxMotorizados/reset', f);
  }

  updateCatalogxMotorizado(idCatalogxMotorizados, data) {
    console.log('updateCatalogxMotorizado')
          return this.http.put(globals.BASE_URL+'api/TdpCatalogxMotorizados/'+idCatalogxMotorizados, data).map(function(response){
            return response.json();
          });
        }
 
}
