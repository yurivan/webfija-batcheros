import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class ContractService {
    constructor(private http: HttpService) { }

    getContractList(type: string, option: boolean) {
        let domain: string = 'CONTRACT'
        return this.http.get(`${globals.BASE_URL}api/Contracts?filter[where][type]=${type}&filter[where][public]=${option}&filter[where][domain]=${domain}`)
            .map(response => {
                console.log("getContractList " + JSON.stringify(response))
                return response.json()
            })
    }

    updateReviewList(body) {
        return this.http.post(`${globals.BASE_URL}api/Contracts/updateContract`, body)
            .map(response => {
                return response.json()
            })
    }

    getSpeechList(type: string, option: boolean) {
        let domain: string = 'SPEECH'
        return this.http.get(`${globals.BASE_URL}api/Contracts?filter[where][type]=${type}&filter[where][public]=${option}&filter[where][domain]=${domain}`)
            .map(response => {
                console.log("getSpeechList " + JSON.stringify(response))
                return response.json()
            })
    }


    updateStatusWhatsapp(data) {
        let body = JSON.stringify(data)
        console.log("configoffering service " + body)
        return this.http.put(`${globals.BASE_URL}api/Parameters/updateStatusWhatsapp`, body)
    }

    getStatusWhatsapp() {
        return this.http.get(`${globals.BASE_URL}api/Parameters/getStatusWhatsapp`)
    }
}