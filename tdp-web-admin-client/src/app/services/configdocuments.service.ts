import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class DocumentService {
    constructor(private http: HttpService) { }

    getStatusOfWebDocuments() {
        return this.http.get(`${globals.BASE_URL}api/Parameters?filter[where][domain]=HERRAMIENTACONFIGURABLEWEB`)
        .map(response => {
            return response.json()
        })
    }

    getStatusOfAppDocuments() {
        return this.http.get(`${globals.BASE_URL}api/Parameters?filter[where][domain]=HERRAMIENTACONFIGURABLEAPP`)
        .map(response => {
            return response.json()
        })
    }

    updateDocumentsStrValue(body) {
        return this.http.put(`${globals.BASE_URL}api/Parameters`, body)
            .map(response => {
                return response.json()
            })
    }
}