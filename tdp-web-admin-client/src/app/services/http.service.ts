import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class HttpService {
  constructor(private http: Http){}

  createAuthHeader () {
    let headers = new Headers({ 'Content-Type': 'application/json','Accept':'application/json' });
    var token = localStorage.getItem('token');
    headers.append('Authorization', token);
    return headers;
  }

  get(url){
    let headers = this.createAuthHeader();
    let options = new RequestOptions({ headers: headers });
    return this.http.get(url, options)
  }

  post(url, data){
    let headers = this.createAuthHeader();
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url, data, options);
  }

  put(url, data) {
    let headers = this.createAuthHeader();
    let options = new RequestOptions({ headers: headers });
    return this.http.put(url, data, options);
  }

  delete(url) {
    let headers = this.createAuthHeader();
    let options = new RequestOptions({ headers: headers });
    return this.http.delete(url, options);
  }
}
