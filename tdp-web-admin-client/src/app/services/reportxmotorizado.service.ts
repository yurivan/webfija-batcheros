import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class ReportxMotorizadoService {
    constructor(private http: HttpService) { }

    load(initDate,finDate,motorizado) {

        let data = '{}';
        var token = localStorage.getItem('token');

        window.open(globals.BASE_URL + 'api/TdpCatalogxMotorizados/web/motorizado/'+initDate+'/'+finDate+'/'+motorizado+'?access_token='+token);
        
    }
    
    loadMotorizado() {
        return this.http.get(globals.BASE_URL + 'api/TdpCatalogxMotorizados/getMotorizado').map(function(response) {
        return response.json();
      });
    }
    

}
