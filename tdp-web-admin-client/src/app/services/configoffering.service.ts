import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import { UploadService } from './upload.service';

import * as globals from './globals';

@Injectable()
export class ConfigofferingService {
      constructor(private http:HttpService){}


load(){

   let data = '{}';
   let token = localStorage.getItem('token');

   console.log('log token', token);

   return this.http.get(globals.BASE_URL+'api/TdpCatalogs/getDataCatalogOffering/?access_token='+token)
     .map(function(response){
       return response.json();
     });
 }

      update(id,data){
    	    let body = JSON.stringify(data);
    	    return this.http.put(globals.BASE_URL+'api/ConfigOfferings/' +id, body);
      }

      remove(id){
          return this.http.delete(globals.BASE_URL+'api/ConfigOfferings/'+id);
      }

      updateStrValue(data){
        let body = JSON.stringify(data)
        console.log("configoffering service " + body)
        return this.http.put(`${globals.BASE_URL}api/Parameters/modifyingavve`,body)
    }

    getStatus(){
     return this.http.get(`${globals.BASE_URL}api/Parameters/gettingavve`) 
    }

  updateFlagAutTgest(data) {
    let body = JSON.stringify(data)
    console.log("configoffering service " + body)
    return this.http.put(`${globals.BASE_URL}api/Parameters/modifySWAutTgest`, body)
  }

  getStatusFlagAutTgest() {
    return this.http.get(`${globals.BASE_URL}api/Parameters/getSWAutTgest`)
  }

      add(data){
  	    let body = JSON.stringify(data);
  	    return this.http.post(globals.BASE_URL+'api/ConfigOfferings', body);
      }
  cargaconfigoffering(){

     let data = '{}';
     let token = localStorage.getItem('token');

     console.log('log token', token);

     return this.http.get(globals.BASE_URL+'api/ConfigOfferings/getDataConfigOffering/?access_token='+token)
       .map(function(response){
         return response.json();
       });
   }

  getSalesCondition() {
    return this.http.get(`${globals.BASE_URL}api/Parameters/getSalesCondition`)
      .map(response => {
        console.log("getSalesCondition " + JSON.stringify(response))
        return response.json()
      })
  }

  modifySalesCondition(body) {
    return this.http.post(`${globals.BASE_URL}api/Parameters/modifySalesCondition`, body)
      .map(response => {
        console.log("modifySalesCondition " + JSON.stringify(response))
        return response.json()
      })
  }
}
