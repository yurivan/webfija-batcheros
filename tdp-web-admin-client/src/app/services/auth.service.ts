import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class AuthService {
	eventSource = new BehaviorSubject(0);
	counter: number = 0;
	eventObservable = this.eventSource.asObservable();
	grantedAccess: any = {};
	webscreens: any[] = [];
	webprofilescreens: any[] = [];
	webprofile: any;
	constructor(private http: HttpService) { }

	checkUserAccess() {
		let token = localStorage.getItem('token');
		let currentUser = JSON.parse(localStorage.getItem('currentUser'));
		if (!currentUser) {
			currentUser = {};
		}
		let _this = this;
		let userId = currentUser.id;

		this.getListScreenProfile(currentUser.profileid).subscribe(data => {
			_this.webprofilescreens = JSON.parse(data["_body"]);
			console.log(_this.webprofilescreens)
			localStorage.setItem('webprofileScreens', JSON.stringify(_this.webprofilescreens));
		}, error => {
			console.log(error)
		});
/*
		this.getListProfile(currentUser.profileid).subscribe(data => {
			let datos = data["_body"]
			console.log(datos)
			localStorage.setItem('webprofile', JSON.stringify(datos));
			_this.webprofile = datos;
		}, error => {
			console.log(error)
		});
*/
		if (userId != null) {
			return this.http.get(globals.BASE_URL + 'api/WebUsers/' + userId + '/load').map(function (response) {
				return response.json().data;
			}).toPromise().then(function (data) {
				//console.log(data);
				localStorage.setItem('currentUser', JSON.stringify(data.user));
				localStorage.setItem('webscreens', JSON.stringify(data.webscreens));
				_this.webscreens = data.webscreens;
				_this.processAccess();
			}).catch(function (err) {
				console.log("auth " + err);
				_this.logout();
			});
		} else {
			_this.logout()
		}

	}

	processAccess() {
		//console.log(this.grantedAccess);
		//let webprofile = this.webprofile;
		let webscreens = this.webscreens;
		let webprofilescreens = this.webprofilescreens;

		let grantedAccess = {
			'AdminArgumentarios' : false,
			'AdminUserConfig' : false,
			'AdminCondicionesVenta' : false,
			'AdminFibra' : false,
			'AdminPerfiles' : false,
			'AdminConvergencia':false,
			'BackOfficeSupervisorSocio' : false,
			'AdminAgendamiento' :false,
			'AdminMenu': false,
			'Monitor' :false,
			'AdminMotorizado': false,
			'AdminMotorizadoCat' : false,
			'ConfEquivalencia' :false,
			'CatalogVendedores' :false,
			'ParametTrazabilidad' :false,
			'AdminMensajes' :false,
			'AdminDoc' :false,
			'AdminContract' :false,
			'AdminPolComerciales' :false,
			'ParametConfig' :false,
			'ConfigOffering' :false,
			'CatalogSVA' :false,
			'CatalogProduc' :false,
			'ConsultaLog' :false,
			'AdminCoordinador' :false,
			'AdminVendedores' :false,
			'AdminReporte' :false
		};

/*
		let urlMapping = {
			'administration':'AdminMenu',
			'reports': 'AdminReporte',
			'salesAgent': 'Admin',
			'salesAgentNiveles': 'Admin',
			'administration/servicecallevents': 'Admin',
			'catalog': 'Admin',
			'sva': 'Admin',
			'configoffering': 'Admin',
			'setsecurityvalues': 'Admin',
			'contractpolicies': 'Admin',
			'listcontract': 'Admin',
			'adminconfig': 'Admin',
			'listmessage': 'Admin',
			'setparamtrazabilidad': 'Admin',
			'catalogxsalesagent': 'Admin',
			'equivalent/0': 'Admin',
			'catalogxmotorizado': 'AdminMotorizado',
			'reportxmotorizados': 'AdminMotorizado',
			'profilescreen': 'Admin',
			'administration/users': 'Admin',
			'visorloadlogs': 'Admin',
			'agendamiento/config': 'AdminAgendamiento',
			'ftth': 'AdminFtth'
		};

		//console.log('cascader', webscreens);
		//console.log('cascader', webscreens.length);
		for (let i = 0; i < webscreens.length; i++) {
			let ws = webscreens[i];
			//console.log('ws', ws);
			let grantedAccessKey = ws.id;
			//console.log('grantedAccessKey', grantedAccessKey);
			if (grantedAccessKey) {
				grantedAccess[grantedAccessKey] = true;
			}
		}*/

		for (let i = 0; i < webscreens.length; i++) {
			let ws = webscreens[i];
			for (let j = 0; j < webprofilescreens.length; j++) {
				//console.log('ws', ws);
				let wf = webprofilescreens[j];
				//console.log('grantedAccessKey', grantedAccessKey);
				if (wf.webscreenid == ws.id) {
					grantedAccess[ws.namesegurity] = true;
					break;
				}
			}
		}
		this.grantedAccess = grantedAccess;
		this.eventSource.next(this.counter++);
		//console.log(this.grantedAccess);
	}

	login(username, password) {
		var _this = this;
		return this.http.post(globals.BASE_URL + 'api/WebUsers/login?include=user', JSON.stringify({ username: username, password: password }))
			.map(function (response) {
				var data = response.json();
				if (data && data.id) {
					localStorage.setItem('currentUser', JSON.stringify(data.user));
					localStorage.setItem('token', data.id);
				}

				_this.checkUserAccess().then(function () {
					_this.processAccess();
					_this.eventSource.next(_this.counter++);
				});
				return data.user;
			});
	}

	logout() {
		localStorage.removeItem('token');
		localStorage.removeItem('currentUser');
		localStorage.removeItem('webscreens');
	}
	getconfigParameters() {
		var filter = {
			category: 'AUTH_PARAMETER'
		}
		var filter1 = encodeURIComponent(JSON.stringify(filter));
		var fil = "AUTH_PARAMETER"
		return this.http.get(`${globals.BASE_URL}api/Parameters?filter[where][category]=${fil}`)

	}

	getdeudaParameters() {
		var filter = {
			auxiliar: '666'
		}
		var filter1 = encodeURIComponent(JSON.stringify(filter));
		var fil = "666"
		return this.http.get(`${globals.BASE_URL}api/Parameters?filter[where][auxiliar]=${fil}`)

	}

	setConfigParameters(attribute, strvalue) {
		var body = {
			"strvalue": strvalue
		}
		return this.http.post(`${globals.BASE_URL}api/Parameters/update?where[element]=${attribute}`, body)
			.map(response => {
				return response.json()
			})

	}

	getListScreenProfile(profileId) {
		var filter = {
			webprofileid: profileId
		}
		var filter1 = encodeURIComponent(JSON.stringify(filter));
		var fil = profileId
		return this.http.get(`${globals.BASE_URL}api/Webprofilescreens?filter[where][webprofileid]=${fil}`)
	}

	getListProfile(profileId) {
		var filter = {
			id: profileId
		}
		var filter1 = encodeURIComponent(JSON.stringify(filter));
		var fil = profileId
		return this.http.get(`${globals.BASE_URL}api/Webprofiles?filter[where][webprofileid]=${fil}`)
	}
	
}
