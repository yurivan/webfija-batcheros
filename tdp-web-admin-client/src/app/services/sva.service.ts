import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import { UploadService } from './upload.service';

import * as globals from './globals';

@Injectable()
export class SvaService {
      constructor(private http:HttpService, private uploadService:UploadService){}

      load(){
        return this.http.get(globals.BASE_URL+'api/TdpSvas')
          .map(function(response){
            return response.json();
          });
      }
      loadById(id){
        return this.http.get(globals.BASE_URL+'api/TdpSvas/'+id)
          .map(function(response){
            return response.json();
          });
      }
      /*loadByPages(perPage,currentPos){
        return this.http.get(globals.BASE_URL+'api/TdpSvas?filter[limit]='+perPage+'&filter[skip]='+currentPos)
          .map(function(response){
            return response.json();
          });
      }*/
      /*countData(){
        return this.http.get(globals.BASE_URL+'api/TdpSvas/count')
          .map(function(response){
            return response.json();
          });
      }*/
      upload (f) {
        return this.uploadService.upload(globals.BASE_URL+'api/TdpSvas/upload', f);
      }
      reset (f) {
        return this.uploadService.upload(globals.BASE_URL+'api/TdpSvas/reset', f);
      }

cargasva(){

   let data = '{}';
   let token = localStorage.getItem('token');

   console.log('log token', token);

   return this.http.get(globals.BASE_URL+'api/TdpSvas/getDataSvaOffering/?access_token='+token)
     .map(function(response){
       return response.json();
     });
 }


update(id,data){
    let body = JSON.stringify(data);
    return this.http.put(globals.BASE_URL+'/api/TdpSvas/' +id, body);
}

remove(id){
    return this.http.delete(globals.BASE_URL+'api/TdpSvas/'+id);
}

add(data){
  let body = JSON.stringify(data);
  return this.http.post(globals.BASE_URL+'/api/TdpSvas', body);
}




search(filterObject) {
  var filter = encodeURIComponent(JSON.stringify(filterObject));
  return this.http.get(globals.BASE_URL + 'api/TdpSvas?filter=' + filter)
    .map(function (response) {
      return response.json();
    });
}

count(whereObject) {
  var where = encodeURIComponent(JSON.stringify(whereObject));
  return this.http.get(globals.BASE_URL + 'api/TdpSvas/count?where=' + where)
    .map(function (response) {
      return response.json();
    });
}




    }
