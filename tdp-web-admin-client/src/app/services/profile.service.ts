import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import { UploadService } from './upload.service';

import * as globals from './globals';

@Injectable()
export class ProfileService {
  constructor(private http:HttpService) {}

  load(){
    return this.http.get(globals.BASE_URL+'api/Webprofiles')
      .map(function(response){
        return response.json();
      });
  }

  update(id,data){
      let body = JSON.stringify(data);
      return this.http.put(globals.BASE_URL+'api/Webprofiles/' +id, body);
  }

  remove(id){
      return this.http.delete(globals.BASE_URL+'api/Webprofiles/'+id);
  }

  add(data){
    let body = JSON.stringify(data);
    return this.http.post(globals.BASE_URL+'api/Webprofiles', body);
  }
}
