import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class PolicyService {
    constructor(private http: HttpService) { }

    getPolicyParameters() {
        return this.http.get(`${globals.BASE_URL}api/Parameters?filter[where][category]=POLICY_PARAMETER`)
            .map(response => {
                console.log("getPolicytList " + JSON.stringify(response))
                return response.json()
            })
    }

    updatePolicyParameters(body) {
        return this.http.put(`${globals.BASE_URL}api/Parameters`, body)
            .map(response => {
                return response.json()
            })
    }
}