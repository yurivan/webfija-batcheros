import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class MessageService {
    constructor(private http: HttpService) { }

    getMessageList(servicecode: string) {
        return this.http.get(`${globals.BASE_URL}api/ErrorMessageServices?filter[where][servicecode]=${servicecode}`)
            .map(response => {
                console.log("getMessageList " + JSON.stringify(response))
                return response.json()
            })
    }

    updateReviewList(body) {
        return this.http.post(`${globals.BASE_URL}api/ErrorMessageServices/updateMessage`, body)
            .map(response => {
                return response.json()
            })
    }

    setConfigParametersTrz(id, responsecode) {
		var body = {
			"responsecode": responsecode
		}
		return this.http.post(`${globals.BASE_URL}api/ErrorMessageServices/update?where[id]=${id}`, body)
			.map(response => {
				return response.json()
			})

    }
    
    updateReviewUnit(id, responsemessage) {
		var body = {
			"responsemessage": responsemessage
		}
		return this.http.post(`${globals.BASE_URL}api/ErrorMessageServices/update?where[id]=${id}`, body)
			.map(response => {
				return response.json()
			})

	}
}