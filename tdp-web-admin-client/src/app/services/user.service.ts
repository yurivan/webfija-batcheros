import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { HttpService } from './http.service';

import * as globals from './globals';

@Injectable()
export class UserService {
  baseUrl = globals.BASE_URL+'api/WebUsers';

      constructor(private http:HttpService){}

      load(){
        return this.http.get(this.baseUrl)
          .map(function(response){
            return response.json();
          });
      }

      search (filterObject) {
        var filter = encodeURIComponent(JSON.stringify(filterObject));
        return this.http.get(this.baseUrl+'?filter='+filter)
          .map(function(response){
            return response.json();
          });
      }

      count (whereObject) {
        var where = encodeURIComponent(JSON.stringify(whereObject));
        return this.http.get(this.baseUrl+'/count?where='+where)
          .map(function(response){
            return response.json();
          });
      }

      loadById(id){
        return this.http.get(this.baseUrl+'/'+id)
          .map(function(response){
            return response.json();
          });
      }

      save (user) {
        let body = JSON.stringify(user);
  	    return this.http.post(this.baseUrl, body);
      }

      update(id,user){
        let body = JSON.stringify(user);
        return this.http.post(this.baseUrl+"/update?where={\"id\":"+id+"}",body);
      }
    }
