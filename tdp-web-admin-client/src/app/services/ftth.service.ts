import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';

import * as globals from './globals';

@Injectable()
export class FtthService {
  
  baseUrl: string;
  constructor(private http:HttpService) {
    this.baseUrl=globals.BASE_URL+'api/Ftths'
  }
  
  load(){
    return this.http.get(this.baseUrl)
      .map(function(response){
        return response.json();
      });
  }

  search (filterObject) {
    var filter = encodeURIComponent(JSON.stringify(filterObject));
    return this.http.get(this.baseUrl+'?filter='+filter)
      .map(function(response){
        return response.json();
      });
  }

  count (whereObject) {
    var where = encodeURIComponent(JSON.stringify(whereObject));
    return this.http.get(this.baseUrl+'/count?where='+where)
      .map(function(response){
        return response.json();
      });
  }

  save(data){
    let body = JSON.stringify(data);
    return this.http.post(this.baseUrl, body);
  }

  masive(data){
    let body = JSON.stringify(data);
    return this.http.post(this.baseUrl+"/masive", body);
  }

  list(data){
    return this.http.get(this.baseUrl+"/"+data);
  }
  
}
