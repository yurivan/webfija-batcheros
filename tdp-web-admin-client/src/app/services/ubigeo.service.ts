import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';

import * as globals from './globals';

@Injectable()
export class UbigeoService {
  
  baseUrl: string;
  constructor(private http:HttpService) {
    this.baseUrl=globals.BASE_URL+'api/TdpUbigeosDepDistProvs/'
  }

  list(data){
    return this.http.post(this.baseUrl+"selectUbigeos",data);
  }
  
}
