import { Component, OnInit } from '@angular/core';

import { SalesAgentService } from '../services/salesAgent.service';

@Component({
  moduleId: 'salesAgentSupervisores',
  selector: 'salesAgentSupervisores',
  templateUrl: './salesAgentSupervisores.template.html'
})

export class SalesAgentSupervisoresComponent implements OnInit {
  private socket;
  agents: any[];
  currentPage: number;
  itemsPerPage: number;
  item: number;
  totalData: number;
  totPages: number;
  pages: any[];
  model: any;
  loading: boolean;
  showMessage: boolean;
  responseTitle: string;
  constructor(private salesAgentService: SalesAgentService) {
    this.agents = [];
    this.currentPage = 0;
    this.itemsPerPage = 10;
    this.item = 0;
    this.totalData = 0;
    this.totPages = 0;
    this.pages = [];
    this.model = { searchLastName: '', searchMotherLastName: '', searchChannel: '', searchCodigoAtis: '', searchDNI: '' };
    this.loading = false;
    this.showMessage = false;
    this.responseTitle = "";
  }
  ngOnInit() {
    this.loading = true;
    this.totalPages();
    this.setPage(this.currentPage);
    this.getData(this.itemsPerPage, this.item);
    //console.log(this);
  }
  onSearch() {
    this.showMessage = false;
    this.item = 0;
    this.getData(this.itemsPerPage, this.item);
    this.totalPages();
  }
  range() {
    this.pages = [];
    var start, rangeSize;
    if (this.totPages === 0) {
      rangeSize = 1;
    } else if (this.totPages > 0 && this.totPages < 6) {
      rangeSize = 2;
    } else if (this.totPages >= 6 && this.totPages < 11) {
      rangeSize = 3;
    } else {
      rangeSize = 5;
    }
    //console.log("Nº Paginas a mostrar: "+this.totPages);
    //this.totalPages();
    start = this.currentPage;
    //console.log("paginas: "+this.totPages);

    if (start > this.totPages - rangeSize) {
      start = this.totPages - rangeSize + 1;
      //console.log("if: "+ start);
    }
    if (start < 0) {
      start = 0;
    }
    let end = start + rangeSize;
    if (end > this.totPages) {
      end = this.totPages + 1;
    }
    for (var i = start; i < end; i++) {
      this.pages.push(i);
    }
    //console.log(this.pages);
    return this.pages;
  }
  getData(perPage, pages) {
    this.loading = true;
    //{searchLastName:'',searchMotherLastName:'',searchChannel:''};
    var _this = this;
    var apematerno = '%' + (this.model.searchMotherLastName == null ? '' : this.model.searchMotherLastName) + '%';
    var apepaterno = '%' + (this.model.searchLastName == null ? '' : this.model.searchLastName) + '%';
    var canal = '%' + (this.model.searchChannel == null ? '' : this.model.searchChannel) + '%';

    var codigoAtis = '%' + (this.model.searchCodigoAtis == null ? '' : this.model.searchCodigoAtis) + '%';
    var dni = '%' + (this.model.searchDNI == null ? '' : this.model.searchDNI) + '%';

    var filter = {}
    console.log(canal);
    
    if (canal == "%%") {
      filter = {
        limit: perPage,
        skip: pages,
        where: {
          and: [
            { apepaterno: { like: apepaterno } },
            { apematerno: { like: apematerno } },
            { codatis: { like: codigoAtis } },
            { dni: { like: dni } },
            {
              or: [
                { niveles: '2' },
                { niveles: '4' },
                { niveles: '5' }
              ]
            }
          ]
        }
      };
    } else {
      filter = {
        limit: perPage,
        skip: pages,
        where: {
          and: [
            { canal: { like: canal } },
            { apepaterno: { like: apepaterno } },
            { apematerno: { like: apematerno } },
            { codatis: { like: codigoAtis } },
            { dni: { like: dni } },
            {
              or: [
                { niveles: '2' },
                { niveles: '4' },
                { niveles: '5' }
              ]
            }
          ]
        }
      };
    }
    console.log(filter);
    this.salesAgentService.search(filter).subscribe(function (data) {
      _this.agents = data;
      _this.loading = false
    }, function (err) { _this.loading = false });

  }
  totalPages() {
    var _this = this;
    var apematerno = '%' + (this.model.searchMotherLastName == null ? '' : this.model.searchMotherLastName) + '%';
    var apepaterno = '%' + (this.model.searchLastName == null ? '' : this.model.searchLastName) + '%';
    var canal = '%' + (this.model.searchChannel == null ? '' : this.model.searchChannel) + '%';

    var codigoAtis = '%' + (this.model.searchCodigoAtis == null ? '' : this.model.searchCodigoAtis) + '%';
    var dni = '%' + (this.model.searchDNI == null ? '' : this.model.searchDNI) + '%';
    var where = {}
    if (canal == "%%") {
      where = {
        and: [
          { apepaterno: { like: apepaterno } },
          { apematerno: { like: apematerno } },
          { codatis: { like: codigoAtis } },
          { dni: { like: dni } },
          {
            or: [
              { niveles: '2' },
              { niveles: '4' },
              { niveles: '5' }
            ]
          }
        ]
      };
    } else {
      where = {
        and: [
          { canal: { like: canal } },
          { apepaterno: { like: apepaterno } },
          { apematerno: { like: apematerno } },
          { codatis: { like: codigoAtis } },
          { dni: { like: dni } },
          {
            or: [
              { niveles: '2' },
              { niveles: '4' },
              { niveles: '5' }
            ]
          }
        ]
      };
    }
    this.salesAgentService.count(where)
      .subscribe(function (data) {
        _this.totalData = data.count;
        _this.totPages = Math.ceil(_this.totalData / _this.itemsPerPage) - 1;
        _this.range();
      }, function (err) {
        console.log(err);
      });
  }
  prevPage() {
    if (this.currentPage > 0) {
      this.currentPage = this.currentPage - 1;
      this.item = this.currentPage * this.itemsPerPage;
      this.loading = true;
      console.log(this.currentPage);
      //console.log(this.item);
    }
    this.getData(this.itemsPerPage, this.item);
    this.range();
  }
  disablePrevPage() {
    //console.log(this.currentPage);
    return this.currentPage === 0 ? "disabled" : "";
  }
  nextPage() {
    if (this.currentPage < this.totPages) {
      this.currentPage = this.currentPage + 1;
      this.item = this.currentPage * this.itemsPerPage;
      this.loading = true;
      console.log(this.currentPage);
      //console.log(this.item);
    }
    this.getData(this.itemsPerPage, this.item);
    this.range();
  }
  disableNextPage() {
    return this.currentPage === this.totPages ? "disabled" : "";
  }
  setPage(n) {
    this.currentPage = n;
    this.item = this.currentPage * this.itemsPerPage;
    this.loading = true;
    //console.log(this.currentPage);
    //console.log(this.item);
    this.getData(this.itemsPerPage, this.item);
    this.range();
  }

  deleteSalesAgent(dni, codAtis) {
    this.salesAgentService
      .deleteSalesAgentUser(dni, codAtis)
      .subscribe((data) => {
        if (data["data"]["status"] === 'OK') {
          this.onSearch();
          this.showMessage = true;
          this.responseTitle = "Vendedor eliminado correctamente";
        } else {
          this.showMessage = true;
          this.responseTitle = "Ocurrio un error al eliminar el vendedor. Por favor intente nuevamente";
        }

      })
  }
}
