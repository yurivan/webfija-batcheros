import {Pipe} from '@angular/core';

import {SalesAgentService} from '../services/salesAgent.service';

@Pipe({
  name: 'searchLastName',
  pure: false
})
export class PipeSearchLastName {
      constructor(private salesAgentService:SalesAgentService) {}
      transform(args, value){
        return  args
          .filter(function(item){
            if(item.apepaterno === null || item.apepaterno === ""){
              return null;
            }else{
              return item.apepaterno.toUpperCase().indexOf(value.toUpperCase()) !== -1;
            }
          });
      }
    }
