import { Component, OnInit } from '@angular/core';
import { SalesAgentService } from '../services/salesAgent.service';

export class SalesAgent {
	dni: string
	apepaterno: string
	apematerno: string
	nombre: string
	codatis: string
	codcms: string
	email: string
	niveles: string
	zona: string
	zonal: string
	atis: string
	canalatis: string
	segmento: string
	entidad: string
	nompuntoventa: string
	codpuntoventa: string
	canal: string
}
@Component({
	moduleId: 'salesAgentSupervisoresCreate',
	selector: 'salesAgentSupervisoresCreate',
	templateUrl: './salesAgentSupervisoresCreate.template.html'
})

export class SalesAgentSupervisoresCreateComponent implements OnInit {

	salesAgent: SalesAgent
	ocultar: boolean
	responseTitle: string
	showError: boolean
	loading: boolean

	constructor(private saleAgentService: SalesAgentService) {
		this.salesAgent = new SalesAgent()

		this.salesAgent.codcms = "0"
		this.salesAgent.email = ""
		this.salesAgent.zona = ""
		this.salesAgent.zonal = ""
		this.salesAgent.canal = ""
		this.salesAgent.canalatis = ""
		this.salesAgent.segmento = ""
		this.salesAgent.entidad = ""
		this.salesAgent.nompuntoventa = ""
		this.salesAgent.codpuntoventa = ""

		this.ocultar = false
		this.responseTitle = ""
		this.showError = false
		this.loading = false
	}

	ngOnInit() {
	}

	isValid() {
		//estas validaciones deberia realizarse en el form... pendiente...
		if (!this.salesAgent.dni || this.salesAgent.dni.length < 8) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el DNI";
			return false;
		}
		if (!this.salesAgent.codatis) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el CODIGO ATIS";
			return false;
		}
		if (!this.salesAgent.apepaterno) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Apellido Paterno";
			return false;
		}
		if (!this.salesAgent.apematerno) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Apellido Materno";
			return false;
		}
		if (!this.salesAgent.nombre) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Nombre";
			return false;
		}
		if (this.salesAgent.niveles) {
			if (this.salesAgent.niveles!="2" && this.salesAgent.niveles!="4" && this.salesAgent.niveles!="5" ) {
				this.showError = true;
				this.responseTitle = "Por favor ingresar correctamente el NIVEL";
				return false;
			}
		}else{
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el NIVEL";
			return false;
		}

		return true;
	}

	create() {
		//validamos datos ingresados... a la mala
		if (!this.isValid()) {
			return;
		}
		this.loading = true
		this.saleAgentService
			.addSaleAgentUser(this.salesAgent)
			.subscribe((data) => {
				this.loading = false
				if (data["data"]["status"] === 'OK') {
					this.showError = false;
					this.ocultar = true
					this.responseTitle = "Se guardó correctamente"
				} else if (data["data"]["status"] === 'FAILED') {
					this.showError = true
					//this.responseTitle = "Ocurrió un problema en el servidor"
					this.responseTitle = "El CODIGO ATIS o DNI ya se encuentra registrado"
				}
			})
	}

	validate(event: any) {
		var code = event.keyCode;
		if ((code < 48 || code > 57) // numerical
			&& code !== 46 //delete
			&& code !== 8  //back space
			&& code !== 37 // <- arrow
			&& code !== 39) // -> arrow
		{
			event.preventDefault();
		}
		return
	}
}
