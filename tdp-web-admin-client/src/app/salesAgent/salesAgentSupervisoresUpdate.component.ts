import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SalesAgentService } from '../services/salesAgent.service';
import { SalesAgent } from './salesAgentSupervisoresCreate.component'

@Component({
	moduleId: 'salesAgentSupervisoresUpdate',
	selector: 'salesAgentSupervisoresUpdate',
	templateUrl: './salesAgentSupervisoresUpdate.template.html'
})
export class SalesAgentSupervisoresUpdateComponent implements OnInit {
	loading: boolean;
	salesAgent: SalesAgent
	ocultar: boolean
	responseTitle: string
	showError: boolean

	constructor(private route: ActivatedRoute, private salesAgentService: SalesAgentService) {
		this.salesAgent = new SalesAgent()

		this.salesAgent.codcms = "0"
		this.salesAgent.email = ""
		this.salesAgent.zona = ""
		this.salesAgent.zonal = ""
		this.salesAgent.canal = ""
		this.salesAgent.canalatis = ""
		this.salesAgent.segmento = ""
		this.salesAgent.entidad = ""
		this.salesAgent.nompuntoventa = ""
		this.salesAgent.codpuntoventa = ""

		this.ocultar = false
		this.responseTitle = ""
		this.showError = false
		this.loading = false
	}
	ngOnInit() {
		this.loading = true
		this.route.params
			.subscribe(params => {
				console.log("codatis " + params["codatis"])
				this.salesAgentService.loadByCodAtis([params["codatis"]])
					.subscribe(data => {
						this.salesAgent = data
						this.loading = false
					}, err => {
						console.log(err)
					})
			}, err => {
				console.log(err)
			})

	}

	isValid() {
		//estas validaciones deberia realizarse en el form... pendiente...
		if (!this.salesAgent.dni || this.salesAgent.dni.length < 8) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el DNI";
			return false;
		}
		if (!this.salesAgent.codatis) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Usuario";
			return false;
		}
		if (!this.salesAgent.apepaterno) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Apellido Paterno";
			return false;
		}
		if (!this.salesAgent.apematerno) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Apellido Materno";
			return false;
		}
		if (!this.salesAgent.nombre) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Nombre";
			return false;
		}
		if (this.salesAgent.niveles) {
			if (this.salesAgent.niveles!="2" && this.salesAgent.niveles!="4" && this.salesAgent.niveles!="5" ) {
				this.showError = true;
				this.responseTitle = "Por favor ingresar correctamente el NIVEL (2, 4 o 5)";
				return false;
			}
		}else{
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el NIVEL";
			return false;
		}
		/*
		if (!this.salesAgent.canalatis) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el dato canal";
			return false;
		}
		if (!this.salesAgent.entidad) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el dato entidad";
			return false;
		}
		if (!this.salesAgent.nompuntoventa) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el nombre del punto de venta";
			return false;
		}
		*/
		return true;
	}

	update() {
		//validamos datos ingresados... a la mala
		if (!this.isValid()) {
			return;
		}
		this.loading = true
		console.log(this.salesAgent);
		
		this.salesAgentService
			.updateSalesAgentUser(this.salesAgent)
			.subscribe((data) => {
				this.loading = false
				if (data["data"]["status"] === 'OK') {
					this.ocultar = true
					this.responseTitle = "Se actualizó correctamente"
				} else if (data["data"]["status"] === 'FAILED') {
					this.showError = true
					this.responseTitle = "Ocurrió un problema en el servidor"
				}

			})
	}
	validate(event: any) {
		var code = event.keyCode;
		if ((code < 48 || code > 57) // numerical
			&& code !== 46 //delete
			&& code !== 8  //back space
			&& code !== 37 // <- arrow
			&& code !== 39) // -> arrow
		{
			event.preventDefault();
		}
		return
	}
}
