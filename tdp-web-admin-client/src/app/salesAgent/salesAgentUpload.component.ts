import {Component,OnInit,OnDestroy} from '@angular/core';

import {SalesAgentService} from '../services/salesAgent.service';

@Component({
  moduleId: 'salesAgentUpload',
  selector: 'my-salesAgentUpload',
  templateUrl: './salesAgentUpload.template.html'
})
export class SalesAgentUploadComponent implements OnInit,OnDestroy {
  model:any;
  loading:boolean;
  processing:boolean;
  files:any[];
  subscription;
      constructor(private salesAgentService:SalesAgentService) {
        this.model = {};
        this.loading = false;
        this.processing = false;

        this.subscription=this.salesAgentService._eventBus.subscribe(data => {
          console.log(data.response);
          this.model = data.response;
          this.processing = false;
        });
      }

      ngOnInit() {}

      ngOnDestroy() {
        // this.subscription.dispose();
      }

      onClickOtroArchivo() {
        this.model = {};
        this.files = [];
      }

      onClickPersist() {
        var t = this;
        this.loading = true;
        this.salesAgentService.reset(this.files[0]).subscribe(function (responseData) {
          // _this.model = responseData.data;
          t.processing = true;
          t.loading = false;
        }, function (err) {
          console.log(err);
          this.loading = false;
        });
      }

      onFormSubmit() {
        var t = this;
        this.loading = true;
        this.salesAgentService.upload(this.files[0]).subscribe(function (responseData) {
          t.model = responseData.data;
          console.log(t.model);
          t.loading = false;
        }, function (err) {
          console.log(err);
          t.loading = false;
        });
      }

      onChangeFile(evt) {
        this.files = evt.srcElement.files;
      }
    }
