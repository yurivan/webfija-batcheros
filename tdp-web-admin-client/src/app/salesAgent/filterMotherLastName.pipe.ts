import {Pipe} from '@angular/core';

import {SalesAgentService} from '../services/salesAgent.service';

@Pipe({
  name: 'searchMotherLastName',
  pure: false
})
export class PipeSearchMotherLastName {
      constructor(private salesAgentService:SalesAgentService) {
      	this.salesAgentService = salesAgentService;
      }
      transform(args, value){
        return  args
          .filter(function(item){
            if(item.apematerno === null || item.apematerno === ""){
              return null;
            }else{
              return item.apematerno.toUpperCase().indexOf(value.toUpperCase()) !== -1;
            }
          });
      }
    }
