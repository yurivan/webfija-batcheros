import { Component, OnInit, OnDestroy } from '@angular/core';

import { SalesAgentService } from '../services/salesAgent.service';

@Component({
  moduleId: 'salesAgentWebUpload',
  selector: 'my-salesAgentWebUpload',
  templateUrl: './salesAgentwebUpload.template.html'
})
export class SalesAgentWebUploadComponent implements OnInit, OnDestroy {
  model: any;
  loading: boolean;
  processing: boolean;
  files: any[];
  subscription;
  resetdata;
  niveldata: string;
  constructor(private salesAgentService: SalesAgentService) {
    this.model = {};
    this.loading = false;
    this.processing = false;
    this.resetdata = false;
    this.niveldata = '1';

    this.subscription = this.salesAgentService._eventBus.subscribe(data => {
      console.log(data.response);
      this.model = data.response;
      this.processing = false;
    });
  }

  ngOnInit() { }

  ngOnDestroy() {
    // this.subscription.dispose();
  }

  onClickOtroArchivo() {
    this.model = {};
    this.files = [];
  }

  onClickPersist() {
    var t = this;
    this.loading = true;
    this.salesAgentService.resetWeb(this.files[0], this.resetdata, this.niveldata).subscribe(function (responseData) {
      // _this.model = responseData.data;
      t.processing = true;
      t.loading = false;
    }, function (err) {
      console.log(err);
      this.loading = false;
    });
  }

  onFormSubmit() {
    var t = this;
    this.loading = true;
    this.salesAgentService.uploadWeb(this.files[0]).subscribe(function (responseData) {
      t.model = responseData.data;
      t.loading = false;
    }, function (err) {
      console.log(err);
      t.loading = false;
    });
  }

  onChangeFile(evt) {
    this.files = evt.srcElement.files;
  }
}
