import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SalesAgentService } from '../services/salesAgent.service';

@Component({
  moduleId: 'salesAgentDetail',
  selector: 'my-salesAgentDetail',
  templateUrl: './salesAgentDetail.template.html'
})
export class SalesAgentDetailComponent {
  loading: boolean;
  salesAgent: any;

  constructor(private route: ActivatedRoute, private salesAgentService: SalesAgentService) {
    this.salesAgent = {};
    //console.log(this.salesAgentService);
  }
  ngOnInit() {
    var _this = this;
    _this.loading = true;
    //console.log(this.route.params);
    this.route.params
      .subscribe(function (params) {
        _this.salesAgentService.loadByCodAtis(params['codatis'])
          .subscribe(function (data) {
            _this.salesAgent = data;
            _this.loading = false;
            //console.log(_this.salesAgent);
          }, function (err) {
            console.log(err);
            _this.loading = false;
          });
      }, function (err) {
        console.log(err);
        _this.loading = false;
      });
  }
}
