import {Pipe} from '@angular/core';

import {SalesAgentService} from '../services/salesAgent.service';

@Pipe({
  name: 'searchChannel',
  pure: false
})
export class PipeSearchChannel {
      constructor(private salesAgentService:SalesAgentService) {}
      transform(args, value){
        return  args
          .filter(function(item){
              return item.canal.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
