import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SalesAgentService } from '../services/salesAgent.service';

@Component({
  moduleId: 'salesAgentNivelesDetail',
  selector: 'salesAgentNivelesDetail',
  templateUrl: './salesAgentNivelesDetail.template.html'
})
export class SalesAgentNivelesDetailComponent {
  loading: boolean;
  salesAgent: any;

  constructor(private route: ActivatedRoute, private salesAgentService: SalesAgentService) {
    this.salesAgent = {};
    //console.log(this.salesAgentService);
  }
  ngOnInit() {
    this.loading = true;
    var _this = this;
    //console.log(this.route.params);
    this.route.params
      .subscribe(function (params) {
        _this.salesAgentService.loadByCodAtis(params['codatis'])
          .subscribe(function (data) {
            _this.salesAgent = data;
            _this.loading = false;
            //console.log(_this.salesAgent);
          }, function (err) {
            _this.loading = false;
            console.log(err);
          });
      }, function (err) {
        _this.loading = false;
        console.log(err);
      });
  }
}
