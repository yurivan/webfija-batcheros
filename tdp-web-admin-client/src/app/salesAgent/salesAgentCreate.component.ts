import { Component, OnInit } from '@angular/core';
import { SalesAgentService } from '../services/salesAgent.service';

export class SalesAgent {
	dni: string
	apepaterno: string
	apematerno: string
	nombre: string
	codatis: string
	codcms: string
	email: string
	niveles: string
	zona: string
	zonal: string
	atis: string
	canalatis: string
	segmento: string
	entidad: string
	nompuntoventa: string
	codpuntoventa: string
	canal: string
	userCanalCodigo: string
	userFuerzaVenta: string
}
@Component({
	moduleId: 'salesAgentCreate',
	selector: 'my-salesAgentCreate',
	templateUrl: './salesAgentCreate.template.html'
})

export class SalesAgentCreateComponent implements OnInit {

	salesAgent: SalesAgent
	ocultar: boolean
	responseTitle: string
	showError: boolean
	loading: boolean

	constructor(private saleAgentService: SalesAgentService) {
		this.salesAgent = new SalesAgent()
		this.ocultar = false
		this.responseTitle = ""
		this.showError = false
		this.loading = false
	}

	ngOnInit() {
		this.salesAgent.niveles = "1"
	}

	isValid() {
		//estas validaciones deberia realizarse en el form... pendiente...
		if (!this.salesAgent.dni || this.salesAgent.dni.length < 8) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el DNI";
			return false;
		}
		if (!this.salesAgent.codatis) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el CODIGO ATIS";
			return false;
		}
		if (!this.salesAgent.apepaterno) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Apellido Paterno";
			return false;
		}
		if (!this.salesAgent.apematerno) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Apellido Materno";
			return false;
		}
		if (!this.salesAgent.nombre) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Nombre";
			return false;
		}
		if (!this.salesAgent.codcms) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el CODIGO CMS";
			return false;
		}
		if (!this.salesAgent.email) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el email";
			return false;
		}
		if (!this.salesAgent.niveles) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el NIVEL";
			return false;
		}
		if (!this.salesAgent.zona) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente la zona";
			return false;
		}
		if (!this.salesAgent.zonal) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el dato zonal";
			return false;
		}
		if (!this.salesAgent.userCanalCodigo) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Código de canal de Venta";
			return false;
		}
		if (!this.salesAgent.canal) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el dato canal atis";
			return false;
		}
		if (!this.salesAgent.canalatis) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el dato canal";
			return false;
		}
		if (!this.salesAgent.segmento) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el dato segmento";
			return false;
		}
		if (!this.salesAgent.entidad) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el dato entidad";
			return false;
		}
		if (!this.salesAgent.nompuntoventa) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el nombre del punto de venta";
			return false;
		}
		if (!this.salesAgent.codpuntoventa) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el codigo del punto de venta";
			return false;
		}
		if (!this.salesAgent.userFuerzaVenta) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Código de Fuerza de Venta Generico";
			return false;
		}
		return true;
	}

	create() {
		this.loading = true
		//validamos datos ingresados... a la mala
		if (!this.isValid()) {
			return;
		}
		this.saleAgentService
			.addSalesAgent(this.salesAgent)
			.subscribe((data) => {
				if (data["data"]["status"] === 'OK') {
					this.ocultar = true
					this.responseTitle = "Se guardó correctamente"
					this.loading = false
				} else if (data["data"]["status"] === 'FAILED') {
					this.showError = true
					//this.responseTitle = "Ocurrió un problema en el servidor"
					this.responseTitle = "El CODIGO ATIS o DNI ya se encuentra registrado"
					this.loading = false
				}
			})
	}
	validate(event: any) {
		var code = event.keyCode;
		if ((code < 48 || code > 57) // numerical
			&& code !== 46 //delete
			&& code !== 8  //back space
			&& code !== 37 // <- arrow
			&& code !== 39) // -> arrow
		{
			event.preventDefault();
		}
		return
	}
}
