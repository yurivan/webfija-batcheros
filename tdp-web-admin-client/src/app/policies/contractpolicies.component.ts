import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Rx';
import { PolicyService } from '../services/policy.service';
import { error } from 'util';

export class ResponsePolicy {
    id: string
    auxiliar: string
    domain: string
    category: string
    element: string
    strvalue: string
}
@Component({
    moduleId: 'contractpolicies',
    selector: 'contractpolicies',
    templateUrl: 'contractpolicies.template.html'
})
export class ContractPoliciesComponent implements OnInit {
    policyParameters = []
    maxAmountServices: ResponsePolicy
    maxAmountDecodersByFloor: ResponsePolicy
    maxAmountDecodersByInAlta: ResponsePolicy
    maxAmountServicesError: boolean
    maxAmountDecodersByFloorError: boolean
    maxAmountDecodersByInAltaError: boolean
    txtResponse: string
    ocultar: boolean

    ngOnInit() {
        this.maxAmountServices.strvalue = ""
        this.maxAmountDecodersByFloor.strvalue = ""
        this.maxAmountDecodersByInAlta.strvalue = ""
        this.getContractPolicies()

    }
    constructor(private policyservice: PolicyService) {
        this.maxAmountServices = new ResponsePolicy()
        this.maxAmountDecodersByFloor = new ResponsePolicy()
        this.maxAmountDecodersByInAlta = new ResponsePolicy()
        this.maxAmountServicesError = false
        this.maxAmountDecodersByFloorError = false
        this.maxAmountDecodersByInAltaError = false
        this.txtResponse = ""
        this.ocultar = false
    }
    getContractPolicies() {
        this.policyservice.getPolicyParameters().subscribe(parameters => {
            parameters
                .map(response => {
                    switch (response.element) {
                        case "max_amount_services":
                            this.maxAmountServices.id = response.id
                            this.maxAmountServices.auxiliar = response.auxiliar
                            this.maxAmountServices.domain = response.domain
                            this.maxAmountServices.category = response.category
                            this.maxAmountServices.element = response.element
                            this.maxAmountServices.strvalue = response.strvalue
                            break
                        case "max_amount_decoders_by_floor":
                            this.maxAmountDecodersByFloor.id = response.id
                            this.maxAmountDecodersByFloor.auxiliar = response.auxiliar
                            this.maxAmountDecodersByFloor.domain = response.domain
                            this.maxAmountDecodersByFloor.category = response.category
                            this.maxAmountDecodersByFloor.element = response.element
                            this.maxAmountDecodersByFloor.strvalue = response.strvalue
                            break
                        case "max_amount_aditional_decoders_in_vap":
                            this.maxAmountDecodersByInAlta.id = response.id
                            this.maxAmountDecodersByInAlta.auxiliar = response.auxiliar
                            this.maxAmountDecodersByInAlta.domain = response.domain
                            this.maxAmountDecodersByInAlta.category = response.category
                            this.maxAmountDecodersByInAlta.element = response.element
                            this.maxAmountDecodersByInAlta.strvalue = response.strvalue
                            break
                        default:
                            console.log("error")
                    }
                })
        }, error => {
            this.ocultar = true
            this.txtResponse = "Hubo un error, intente de nuevo."
            setTimeout(() => {
                this.ocultar = false
            }, 3000)
        })
    }

    setParams() {
        let isOkmaxAmountServices = this.validatemaxAmountServices()
        let isOkAdditionalDecoders = this.validateAdditionalDecoders()
        let isOkAdditionalDecodersAlta = this.validateAdditionalDecodersAlta()
        if (isOkmaxAmountServices
            && isOkAdditionalDecoders
            && isOkAdditionalDecodersAlta
        ) {
            let counter = 0
            this.policyParameters.push(this.maxAmountServices, this.maxAmountDecodersByFloor, this.maxAmountDecodersByInAlta)
            this.policyParameters.map(element => {
                let body = {
                    "id": element.id,
                    "strvalue": element.strvalue
                }
                console.log("body " + JSON.stringify(body))
                this.policyservice.updatePolicyParameters(body)
                    .subscribe(data => {
                        if (counter == this.policyParameters.length - 1) {
                            console.log("terminó")
                            this.ocultar = true
                            this.txtResponse = "Se guardaron los cambios."
                            setTimeout(() => {
                                this.ocultar = false
                            }, 3000)
                        }
                        counter++
                    }, error => {
                        this.ocultar = true
                        this.txtResponse = "Hubo un error, intente de nuevo."
                        setTimeout(() => {
                            this.ocultar = false
                        }, 3000)
                    })
            })
        }
    }

    validate(event: any) {
        var code = event.keyCode;
        if ((code < 48 || code > 57) // numerical
            && code !== 46 //delete
            && code !== 8  //back space
            && code !== 37 // <- arrow
            && code !== 39) // -> arrow
        {
            event.preventDefault();
        }
        return
    }

    validatemaxAmountServices() {
        if (this.maxAmountServices.strvalue != null && parseInt(this.maxAmountServices.strvalue) > 0) {
            this.maxAmountServicesError = false
            return true
        } else {
            this.maxAmountServicesError = true
            return false
        }
    }

    validateAdditionalDecoders() {

        if (this.maxAmountDecodersByFloor.strvalue != null && parseInt(this.maxAmountDecodersByFloor.strvalue) > 0) {
            this.maxAmountDecodersByFloorError = false
            return true
        } else {
            this.maxAmountDecodersByFloorError = true
            return false
        }
    }

    validateAdditionalDecodersAlta() {

        if (this.maxAmountDecodersByInAlta.strvalue != null && parseInt(this.maxAmountDecodersByInAlta.strvalue) > 0) {
            this.maxAmountDecodersByInAltaError = false
            return true
        } else {
            this.maxAmountDecodersByInAltaError = true
            return false
        }
    }
}