import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs/Rx';

export class responseSecurity {
    id: string
    auxiliar: string
    domain: string
    category: string
    element: string
    strvalue: string
    strfilter: string
}

@Component({
    moduleId: 'adminconfig',
    selector: 'adminconfig',
    templateUrl: 'adminconfig.template.html'
})

export class AdminConfigComponent implements OnInit {
    securityParameters = []
    deudaParameters = []
    days_reset_pwd_error: boolean
    min_expired_session_error: boolean
    min_account_lock_error: boolean
    amount_ret_error: boolean
    responseCompleted: boolean
    ocultar: boolean
    txtResponse: string

    days_reset_pwd: responseSecurity
    min_expired_session: responseSecurity
    min_account_lock: responseSecurity
    amount_ret: responseSecurity
    sale_hour_start: responseSecurity
    sale_hour_end: responseSecurity
    deudaMaxAlta: responseSecurity
    deudaMaxParque: responseSecurity
    onOffdeuda: responseSecurity

    //Sprint 28 Parametrizar Hora Tope de Cargas
    horasTope : responseSecurity;
    horaTope_h : string;
    horaTope_m : string;

    hour_error: string

    ngOnInit() {
        this.getsomething()
    }

    constructor(private authService: AuthService) {

        this.days_reset_pwd = new responseSecurity()
        this.min_expired_session = new responseSecurity()
        this.min_account_lock = new responseSecurity()
        this.amount_ret = new responseSecurity()
        this.sale_hour_start = new responseSecurity()
        this.sale_hour_end = new responseSecurity()
        this.deudaMaxAlta = new responseSecurity()
        this.deudaMaxParque = new responseSecurity()
        this.onOffdeuda = new responseSecurity()
        this.horasTope = new responseSecurity()

        this.days_reset_pwd_error = false
        this.min_expired_session_error = false
        this.min_account_lock_error = false
        this.amount_ret_error = false
        this.responseCompleted = false
        this.ocultar = false
        this.txtResponse = ""
    }

    getsomething() {
        let indice = 0
        let deuda = 0
        let _this=this
        _this.authService.getconfigParameters().subscribe(parametro => {
            parametro.json().map(response => {
                _this.securityParameters.push(response)
                indice++
            })
            console.log("security " + JSON.stringify(_this.securityParameters))
            for (let i = 0; i < _this.securityParameters.length; i++) {
                switch (_this.securityParameters[i].element) {
                    case "days_reset_pwd":
                        _this.days_reset_pwd = _this.securityParameters[i]
                        break
                    case "min_expired_session":
                         _this.min_expired_session = _this.securityParameters[i]
                        break
                    case "min_account_lock":
                        _this.min_account_lock = _this.securityParameters[i]
                        break
                    case "amount_ret":
                        _this.amount_ret = _this.securityParameters[i]
                        break
                    case "sale_hour_start":
                        _this.sale_hour_start = _this.securityParameters[i]
                        break
                    case "sale_hour_end":
                        _this.sale_hour_end = _this.securityParameters[i]
                        break
                    case "carga_audio_hora_restriccion":
                        _this.horasTope = _this.securityParameters[i]
                        _this.horaTope_h = _this.horasTope.strvalue.split(":")[0]
                        _this.horaTope_m = _this.horasTope.strvalue.split(":")[1]
                        console.log("Horas" + _this.horasTope.strvalue.split(":")[0] + _this.horasTope.strvalue.split(":")[1])
                        break
                }
            }

        }, error => {
            _this.ocultar = true
            _this.txtResponse = "Hubo un error, intente de nuevo."
        })


        _this.authService.getdeudaParameters().subscribe(parametro => {
            parametro.json().map(response => {
                _this.deudaParameters.push(response)
                deuda++
            })
            console.log("Parametros Deuda " + JSON.stringify(_this.deudaParameters))
            for (let j = 0; j < _this.deudaParameters.length; j++) {
                switch (_this.deudaParameters[j].element) {
                    case 'tdp.api.cms.consultacliente.topealta':
                        _this.deudaMaxAlta = _this.deudaParameters[j]
                        break
                    case 'tdp.api.cms.consultacliente.topepark':
                        _this.deudaMaxParque = _this.deudaParameters[j]
                        break
                    case 'tdp.api.cms.consultacliente.activo':
                        _this.onOffdeuda = _this.deudaParameters[j]
                        break
                    
                }
            }

        }, error => {
            _this.ocultar = true
            _this.txtResponse = "Hubo un error, intente de nuevo."
        })
        

    }

    setParams() {
        let isOkDaysResetPwd = this.validateDaysResetPwd()
        let isOkAmountDay = this.validateAmountRet()
        let isOkMinAccountLock = this.validateMinAccountLock()
        let isMinExpiredSession = this.validateMinExpiredSession()

        this.horasTope.strvalue = this.horaTope_h + ":" + this.horaTope_m

        let isOkHourStart = this.validateSaleHour(this.sale_hour_start.strvalue, "Hora de Inicio de venta")
        let isOkHourEnd = false
        if (isOkHourStart)
            isOkHourEnd = this.validateSaleHour(this.sale_hour_end.strvalue, "Hora de Fin de venta")
        else
            return

        if (isOkDaysResetPwd
            && isOkAmountDay
            && isOkMinAccountLock
            && isMinExpiredSession
            && isOkHourStart
            && isOkHourEnd) {

            const arrayfinal = [this.days_reset_pwd, this.amount_ret, this.min_account_lock, this.min_expired_session,
                 this.sale_hour_start, this.sale_hour_end,this.deudaMaxParque, this.deudaMaxAlta, this.onOffdeuda, this.horasTope]
            let responseAmount = 0
            for (let i = 0; i < arrayfinal.length; i++) {
                this.authService.setConfigParameters(arrayfinal[i]["element"], arrayfinal[i]["strvalue"]).subscribe(respuesta => {
                    responseAmount++
                    if (responseAmount == arrayfinal.length) {
                        this.ocultar = true
                        this.txtResponse = "Se guardaron los cambios."
                        setTimeout(() => {
                            this.ocultar = false
                        }, 5000)

                    }
                }, error => {
                    this.ocultar = true
                    this.txtResponse = "Hubo un error, intente de nuevo."
                    setTimeout(() => {
                        this.ocultar = false
                    }, 5000)
                })
            }
        }
    }

    validate(event: any) {
        var code = event.keyCode;
        if ((code < 48 || code > 57) // numerical
            && code !== 46 //delete
            && code !== 8  //back space
            && code !== 37 // <- arrow
            && code !== 39) // -> arrow
        {
            event.preventDefault();
        }
        return
    }

    validateHour(event: any) {
        var code = event.keyCode;
        if ((code < 48 || code > 57) // numerical
            && code !== 46  //delete
            && code !== 8   //back space
            && code !== 37  // <- arrow
            && code !== 39  // -> arrow
            && code !== 190) // -> dos puntos
        {
            event.preventDefault();
        }
        return
    }

    validateDaysResetPwd() {
        if (this.days_reset_pwd.strvalue != null && parseInt(this.days_reset_pwd.strvalue) > 0) {
            this.days_reset_pwd_error = false
            return true
        } else {
            this.days_reset_pwd_error = true
            return false
        }
    }

    validateMinExpiredSession() {
        if (this.min_expired_session.strvalue != null && parseInt(this.min_expired_session.strvalue) > 0) {
            this.min_expired_session_error = false
            return true
        } else {
            this.min_expired_session_error = true
            return false
        }
    }

    validateMinAccountLock() {
        if (this.min_account_lock.strvalue != null && parseInt(this.min_account_lock.strvalue) > 0) {
            this.min_account_lock_error = false
            return true
        } else {
            this.min_account_lock_error = true
            return false
        }
    }

    validateAmountRet() {
        if (this.amount_ret.strvalue != null && parseInt(this.amount_ret.strvalue) > 0) {
            this.amount_ret_error = false
            return true
        } else {
            this.amount_ret_error = true
            return false
        }
    }

    validateSaleHour(sale_hour, hora_title) {
        console.log(sale_hour)
        if (sale_hour != null && sale_hour.length > 0) {
            if (sale_hour.indexOf(":") == 2) {
                var hours = sale_hour.split(":")
                var hour = hours[0]
                var minute = hours[1]
                if (parseInt(hour) < 24 && parseInt(hour) >= 0) {
                    if (parseInt(minute) < 60 && parseInt(minute) >= 0) {
                        this.hour_error = ""
                        return true
                    } else {
                        this.hour_error = hora_title + ". Los minutos no deben ser superior a 59. (0-59)"
                        return false
                    }
                } else {
                    this.hour_error = hora_title + ". La hora no debe ser superior a 23. El formato de hora es de 24 horas (0-23)"
                    return false
                }
            } else {
                this.hour_error = hora_title + ". El formato de hora es en 24 horas, HH:mm"
                return false
            }
        } else {
            if (sale_hour.length < 5) {
                this.hour_error = hora_title + ", validar el formato de hora HH:mm"
                return false
            } else {
                this.hour_error = hora_title + " no debe ser vacío."
                return false
            }
        }
    }
}