import {Component} from '@angular/core';
import {ConfigofferingService} from '../services/configoffering.service';
import {SvaService} from '../services/sva.service';
import {CatalogService} from '../services/catalog.service';
@Component({
  moduleId: 'configoffering',
  selector: 'my-configoffering',
  templateUrl: './configoffering.template.html'
 
})
export class configofferingComponent {
  new_configoffering:any;
  configoffering:any[];
  sva:any[];
  catalog:any[];
  strValue:boolean
  flagAutTgest: boolean
      constructor(private configofferingService:ConfigofferingService,private svaService:SvaService,private catalogService:CatalogService) {
        this.new_configoffering = {
          domain: 'OFFERS'
        };
        this.configoffering = [];
      	this.sva = [];
      	this.catalog = [];
      }
      ngOnInit(){
        this.getStatusOfAvve()
    this.getStatusAutTgest()
        let _this = this;
        _this.new_configoffering.loading = true;
        this.configofferingService.cargaconfigoffering().subscribe(function(data){

            _this.configoffering = data.data;
            console.log("this.configoffreing " + _this.configoffering)
            console.log('data', data.data);
            console.log("cargaconfigoffering() - ngoninit"+ this.configoffering);
          }, function(err){
            console.log(err);
          });
      	this.catalogService.cargacatalog().subscribe(function(data){
          
      			_this.catalog = data.data;
            console.log('data', data.data);
            console.log("cargacatalog() - ngoninit" + this.catalog);
      		}, function(err){
      			console.log(err);
      		});
          this.svaService.cargasva().subscribe(function(data){
              _this.sva = data.data;
              console.log('data', data.data);
              console.log("cargasva" + this.sva);
            }, function(err){
              console.log(err);
            });
      }
      activateUpdate(configoffering){
        console.log("activateUpdate()" + configoffering);
    	  this.new_configoffering.id=configoffering.ID;
    	  this.new_configoffering.sourceApp=configoffering.sourceApp;
        this.new_configoffering.category=configoffering.category;
        this.new_configoffering.sourceType=configoffering.sourceType;
        this.new_configoffering.sourceData=configoffering.sourceData;
        this.new_configoffering.targetType=configoffering.targetType;
        this.new_configoffering.targetData=configoffering.targetData;
        window.scrollTo(0, 0);
      }
      save(){
    	//  console.log('ASDF', this.new_configoffering.id, typeof this.new_configoffering.id !== 'undefined', this.new_configoffering.id !== null);
if(typeof this.new_configoffering.targetData !='undefined'  && this.new_configoffering.targetData !=null && this.new_configoffering.sourceApp !=null && this.new_configoffering.sourceApp !='undefined' && this.new_configoffering.category !=null && this.new_configoffering.category !='undefined' ){

  if(typeof this.new_configoffering.id != 'undefined' && this.new_configoffering.id !== null){
    this.update();
  }else{
    this.add();
  }

}else{
  alert("Los campos APP/WEB	, Operación Comercial y Catalogo o SVA destino no pueden ser vacíos");



}
    }
      update(){
        console.log("ENTRA UPDATE");
          this.configofferingService.update(this.new_configoffering.id,{

            sourceapp: this.new_configoffering.sourceApp,
          //  domain: this.new_configoffering.domain,
            category: this.new_configoffering.category,
            sourcetype: this.new_configoffering.sourceType,
            sourcedata: this.new_configoffering.sourceData,
            targettype: this.new_configoffering.targetType,
            targetdata: this.new_configoffering.targetData,

        	})
            .subscribe(
            	       data => {
            	           this.ngOnInit();
                         this.new_configoffering.id=null;
                         this.new_configoffering.sourceApp=null;
                      //   this.new_configoffering.domain=null;
                         this.new_configoffering.category=null;
                         this.new_configoffering.sourceType=null;
                         this.new_configoffering.sourceData=null;
                         this.new_configoffering.targetType=null;
                         this.new_configoffering.targetData=null;
            	           return true;
            	         },
            	         error => {
            	           console.error("Error saving new_configoffering!");
            	         }
            );
      }
      remove(id){
          this.configofferingService.remove(id)
            .subscribe(
            	       data => {
            	           this.ngOnInit();
            	           return true;
            	         },
            	         error => {
            	           console.error("Error removing new_configoffering!");
            	         }
            );
      }
      add(){
        console.log("add()");
           this.configofferingService.add({
        	   id: 0,
             sourceapp: this.new_configoffering.sourceApp,
             domain: this.new_configoffering.domain,
             category: this.new_configoffering.category,
             sourcetype: this.new_configoffering.sourceType,
             sourcedata: this.new_configoffering.sourceData,
             targettype: this.new_configoffering.targetType,
             targetdata: this.new_configoffering.targetData,
        	 })
            .subscribe(
            	       data => {
            	           this.ngOnInit();
                         this.new_configoffering.id=null;
                         this.new_configoffering.sourceApp=null;
                         this.new_configoffering.domain=null;
                         this.new_configoffering.category=null;
                         this.new_configoffering.sourceType=null;
                         this.new_configoffering.sourceData=null;
                         this.new_configoffering.targetType=null;
                         this.new_configoffering.targetData=null;
            	           return true;
            	         },
            	         error => {
            	           console.error("Error saving new_configoffering!");
            	         }
            );
      }
      updateAvve(){
        let shouldIUpdateAvve = this.strValue 
        console.log("updateAvve " + shouldIUpdateAvve)
        this.configofferingService.updateStrValue(shouldIUpdateAvve).subscribe(data => {
          console.log(data)
        }, error => {
          console.log(error)
        })
      }

      getStatusOfAvve(){
        this.configofferingService.getStatus().subscribe(data => {
          let datos = data["_body"]
          if(datos.substring(22,26) === 'true') {
            this.strValue = true
          }else{
            this.strValue = false
          }
        }, error => {
          console.log(error)
        })
      }

  getStatusAutTgest() {
    this.configofferingService.getStatusFlagAutTgest().subscribe(data => {
      let datos = data["_body"]
      if (datos.substring(22, 26) === 'true') {
        this.flagAutTgest = true
      } else {
        this.flagAutTgest = false
      }
    }, error => {
      console.log(error)
    })
  }

  updateAutTgest() {
    let shouldIAutTgest = this.flagAutTgest
    console.log("updateAutTgest " + shouldIAutTgest)
    this.configofferingService.updateFlagAutTgest(shouldIAutTgest).subscribe(data => {
      console.log(data)
    }, error => {
      console.log(error)
    })
  }

    }
