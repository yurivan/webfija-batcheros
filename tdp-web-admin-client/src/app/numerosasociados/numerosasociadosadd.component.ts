import { Component, OnInit } from '@angular/core';

import { SalesAgentService } from '../services/salesAgent.service';
import { NumberAgentService } from '../services/numberAgent.service';
import { ActivatedRoute } from '@angular/router';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  moduleId: 'numerosasociadosadd',
  selector: 'numerosasociadosadd',
  templateUrl: './numerosasociadosadd.template.html'
})

export class NumerosAsociadosAdd {
  private socket;
  agents: any[];
  loading: boolean;
  showMessage: boolean;
  responseTitle: string;
  vendedor: any;
  nro: string;
  correo: string;

  constructor(private salesAgentService: SalesAgentService,private numberAgentService: NumberAgentService, private route: ActivatedRoute,private router: Router) {
    this.agents = [];
    this.loading = false;
    this.showMessage = false;
    this.responseTitle = "";
  }

  ngOnInit() {
    this.getData();
  }
  
  returnList(){
    this.router.navigate(['/seguridadsocio'])        
  }

  getData() {
    this.loading = true;
    //{searchLastName:'',searchMotherLastName:'',searchChannel:''};
    var _this = this;
    
    var data = '%' + JSON.parse(localStorage.getItem("currentUser"))['entidad'] + '%'
    var  filter = {
      where: {
        and: [
          { entidad: { like: data }},
          { niveles: '1' }
        ]
      }
    };

    this.salesAgentService.search(filter).subscribe(function (data) {
      _this.agents = data;
      _this.loading = false
    }, function (err) { _this.loading = false });

  }

  onSubmit(){
    var model = {}
    var _scope= this;
    
    if((_scope.vendedor=='' || !_scope.vendedor) || (!_scope.nro || _scope.nro.length<9)  ){
      return
    }

    this.loading = true;

    var selected = this.agents.filter(p=> p.codatis==_scope.vendedor)[0];

    model["dni"]=selected["dni"];
    model["codatis"]=selected["codatis"];
    model["nrocelular"]=_scope.nro;
    model["email"]=_scope.correo;
    model["canal"]=selected["canal"];
    model["entidad"]=selected["entidad"];
    model["apepaterno"]=selected["apepaterno"];
    model["apematerno"]=selected["apematerno"];
    model["ingreso"]='0';

    this.numberAgentService.addSalesAgent(model).subscribe(function () {
      _scope.loading = false;
      _scope.showMessage = true;
      _scope.responseTitle="Se agrego con exito"
    }, function (err) {
      console.log(err);
      _scope.loading = false;
      alert('No se pudo registrar los datos!');
    });
  }
}
