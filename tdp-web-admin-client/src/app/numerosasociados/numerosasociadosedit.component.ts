import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NumberAgentService } from '../services/numberAgent.service';


@Component({
	moduleId: 'numerosasociadosedit',
	selector: 'numerosasociadosedit',
	templateUrl: './numerosasociadosedit.template.html'
})
export class NumerosAsociadosUpdate implements OnInit {

	loading: boolean;
	salesAgent: any
	ocultar: boolean
	responseTitle: string
	showError: boolean
	type:string;
	imeireset: boolean;

	constructor(private route: ActivatedRoute, private salesAgentService: NumberAgentService) {
		this.salesAgent = {}
		this.ocultar = false
		this.responseTitle = ""
		this.showError = false
		this.loading = false
		this.imeireset = false
	}

	ngOnInit() {
		this.loading = true
		this.type = this.route.snapshot.params['type']
		this.route.params
			.subscribe(params => {
				console.log("codatis " + params["id"])
				this.salesAgentService.loadByCodAtis([params["id"]])
					.subscribe(data => {
						this.salesAgent = data
						this.loading = false
					}, err => {
						console.log(err)
					})
			}, err => {
				console.log(err)
			})

	}

	isValid() {
		//estas validaciones deberia realizarse en el form... pendiente...
		if (!this.salesAgent.nrocelular || this.salesAgent.nrocelular.length < 9) {
			this.showError = true;
			this.responseTitle = "Por favor ingresar correctamente el Numero";
			return false;
		}
		return true;
	}

	update() {
		//validamos datos ingresados... a la mala
		if (!this.isValid()) {
			return;
		}
		this.loading = true
		var _scope= this;
		var date = new Date();
		if(!this.salesAgent.last_change){
			this.salesAgent.last_change=date;
			this.salesAgent.ingreso='0';
		}else{
			var date2 = new Date(this.salesAgent.last_change);
			var timeDiff = Math.abs(date.getTime() - date2.getTime());
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
			console.log(diffDays)
			if(diffDays<30){
				this.ocultar = true
				this.responseTitle = "Usted ya actualizo el numero hace menos de un mes";
				this.loading = false
				return;
			}else{
				this.salesAgent.last_change=date;
				this.salesAgent.ingreso='0';
			}
		}

		if(this.imeireset){
			this.salesAgent.IMEI=null;
		}

		this.salesAgentService
			.updateSalesAgentUser(this.salesAgent)
			.subscribe((data) => {
				this.loading = false
				if (data["codatis"] == _scope.salesAgent.codatis) {
					this.ocultar = true
					this.responseTitle = "Se actualizó correctamente"
				} else {
					this.showError = true
					this.responseTitle = "Ocurrió un problema en el servidor"
				}

			})
	}
	validate(event: any) {
		var code = event.keyCode;
		if ((code < 48 || code > 57) // numerical
			&& code !== 46 //delete
			&& code !== 8  //back space
			&& code !== 37 // <- arrow
			&& code !== 39) // -> arrow
		{
			event.preventDefault();
		}
		return
	}
}
