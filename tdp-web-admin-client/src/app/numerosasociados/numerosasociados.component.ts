import { Component, OnInit } from '@angular/core';

import { NumberAgentService } from '../services/numberAgent.service';
import { ActivatedRoute } from '@angular/router';
import {ExcelService} from '../services/excel.service';

@Component({
  moduleId: 'numerosasociados',
  selector: 'numerosasociados',
  templateUrl: './numerosasociados.template.html'
})

export class NumerosAsociados {
  private socket;
  agents: any[];
  currentPage: number;
  itemsPerPage: number;
  item: number;
  totalData: number;
  totPages: number;
  pages: any[];
  model: any;
  loading: boolean;
  showMessage: boolean;
  responseTitle: string;
  token: string;

  constructor(private salesAgentService: NumberAgentService, private route: ActivatedRoute,private excelService:ExcelService) {
    this.agents = [];
    this.currentPage = 0;
    this.itemsPerPage = 10;
    this.item = 0;
    this.totalData = 0;
    this.totPages = 0;
    this.pages = [];
    this.model = { searchLastName: '', searchMotherLastName: '', searchChannel: '', searchCodigoAtis: '', searchDNI: '' };
    this.loading = false;
    this.showMessage = false;
    this.responseTitle = "";
    this.token = "";
  }

  ngOnInit() {
    this.loading = true;
    this.totalPages();
    this.setPage(this.currentPage);
    this.getToken();
    this.getData(this.itemsPerPage, this.item);
  }

  getToken(){

    var _this = this;
    this.salesAgentService.token(JSON.parse(localStorage.getItem("currentUser"))['entidad']).
    subscribe(function (data) {
      _this.token = data.token;
    }, function (err) { 
      
    });
  }

  exportAsXLSX():void {

    var _this = this;

    var apematerno = '%' + (this.model.searchMotherLastName == null ? '' : this.model.searchMotherLastName) + '%';
    var apepaterno = '%' + (this.model.searchLastName == null ? '' : this.model.searchLastName) + '%';

    var codigoAtis = '%' + (this.model.searchCodigoAtis == null ? '' : this.model.searchCodigoAtis) + '%';
    var dni = '%' + (this.model.searchDNI == null ? '' : this.model.searchDNI) + '%';
   
    var where = {
      and: [
        { entidad: { like: '%%' }},
        { apepaterno: { like: apepaterno } },
        { apematerno: { like: apematerno } },
        { codatis: { like: codigoAtis } },
        { dni: { like: dni } },
        { niveles: { like: '1' } }
      ]
    };



    var data = '%' + JSON.parse(localStorage.getItem("currentUser"))['entidad'] + '%'
    console.log(JSON.parse(localStorage.getItem("currentUser")));
    
    where = {
      and: [
        { entidad: { like: data }},
        { apepaterno: { like: apepaterno } },
        { apematerno: { like: apematerno } },
        { codatis: { like: codigoAtis } },
        { dni: { like: dni } },
        { niveles: { like: '1' } }
      ]
    };

    this.salesAgentService.search(where).subscribe(function (data) {
      var rep = data;
      _this.loading = false
      _this.excelService.exportAsExcelFile(rep, 'export');

    }, function (err) { _this.loading = false });
 }

  onSearch() {
    this.showMessage = false;
    this.item = 0;
    this.getData(this.itemsPerPage, this.item);
    this.totalPages();
  }
  range() {
    this.pages = [];
    var start, rangeSize;
    if (this.totPages === 0) {
      rangeSize = 1;
    } else if (this.totPages > 0 && this.totPages < 6) {
      rangeSize = 2;
    } else if (this.totPages >= 6 && this.totPages < 11) {
      rangeSize = 3;
    } else {
      rangeSize = 5;
    }
    //console.log("Nº Paginas a mostrar: "+this.totPages);
    //this.totalPages();
    start = this.currentPage;
    //console.log("paginas: "+this.totPages);

    if (start > this.totPages - rangeSize) {
      start = this.totPages - rangeSize + 1;
      //console.log("if: "+ start);
    }
    if (start < 0) {
      start = 0;
    }
    let end = start + rangeSize;
    if (end > this.totPages) {
      end = this.totPages + 1;
    }
    for (var i = start; i < end; i++) {
      this.pages.push(i);
    }
    //console.log(this.pages);
    return this.pages;
  }
  getData(perPage, pages) {
    this.loading = true;
    //{searchLastName:'',searchMotherLastName:'',searchChannel:''};
    var _this = this;
    var apematerno = '%' + (this.model.searchMotherLastName == null ? '' : this.model.searchMotherLastName) + '%';
    var apepaterno = '%' + (this.model.searchLastName == null ? '' : this.model.searchLastName) + '%';

    var codigoAtis = '%' + (this.model.searchCodigoAtis == null ? '' : this.model.searchCodigoAtis) + '%';
    var dni = '%' + (this.model.searchDNI == null ? '' : this.model.searchDNI) + '%';

    var filter = {
      limit: perPage,
      skip: pages,
      where: {
        and: [
          { entidad: { like: '%%' } },
          { apepaterno: { like: apepaterno } },
          { apematerno: { like: apematerno } },
          { codatis: { like: codigoAtis } },
          { dni: { like: dni } },
          { niveles: { like: '1' } }
          
        ]
      }
    };

    var data = '%' + JSON.parse(localStorage.getItem("currentUser"))['entidad'] + '%'
    filter = {
      limit: perPage,
      skip: pages,
      where: {
        and: [
          { entidad: { like: data }},
          { apepaterno: { like: apepaterno } },
          { apematerno: { like: apematerno } },
          { codatis: { like: codigoAtis } },
          { dni: { like: dni } },
          { niveles: { like: '1' } }
        ]
      }
    };

    this.salesAgentService.search(filter).subscribe(function (data) {
      _this.agents = data;
      _this.loading = false
    }, function (err) { _this.loading = false });

  }

  totalPages() {
    var _this = this;
    var apematerno = '%' + (this.model.searchMotherLastName == null ? '' : this.model.searchMotherLastName) + '%';
    var apepaterno = '%' + (this.model.searchLastName == null ? '' : this.model.searchLastName) + '%';

    var codigoAtis = '%' + (this.model.searchCodigoAtis == null ? '' : this.model.searchCodigoAtis) + '%';
    var dni = '%' + (this.model.searchDNI == null ? '' : this.model.searchDNI) + '%';
   
    var where = {
      and: [
        { entidad: { like: '%%' }},
        { apepaterno: { like: apepaterno } },
        { apematerno: { like: apematerno } },
        { codatis: { like: codigoAtis } },
        { dni: { like: dni } },
        { niveles: { like: '1' } }
      ]
    };

    var data = '%' + JSON.parse(localStorage.getItem("currentUser"))['entidad'] + '%'
    console.log(JSON.parse(localStorage.getItem("currentUser")));
    
    where = {
      and: [
        { entidad: { like: data }},
        { apepaterno: { like: apepaterno } },
        { apematerno: { like: apematerno } },
        { codatis: { like: codigoAtis } },
        { dni: { like: dni } },
        { niveles: { like: '1' } }
      ]
    };

    this.salesAgentService.count(where)
      .subscribe(function (data) {
        _this.totalData = data.count;
        _this.totPages = Math.ceil(_this.totalData / _this.itemsPerPage) - 1;
        _this.range();
      }, function (err) {
        console.log(err);
      });
  }
  prevPage() {
    if (this.currentPage > 0) {
      this.currentPage = this.currentPage - 1;
      this.item = this.currentPage * this.itemsPerPage;
      this.loading = true;
      console.log(this.currentPage);
      //console.log(this.item);
    }
    this.getData(this.itemsPerPage, this.item);
    this.range();
  }
  disablePrevPage() {
    //console.log(this.currentPage);
    return this.currentPage === 0 ? "disabled" : "";
  }
  nextPage() {
    if (this.currentPage < this.totPages) {
      this.currentPage = this.currentPage + 1;
      this.item = this.currentPage * this.itemsPerPage;
      this.loading = true;
      console.log(this.currentPage);
      //console.log(this.item);
    }
    this.getData(this.itemsPerPage, this.item);
    this.range();
  }
  disableNextPage() {
    return this.currentPage === this.totPages ? "disabled" : "";
  }
  setPage(n) {
    this.currentPage = n;
    this.item = this.currentPage * this.itemsPerPage;
    this.loading = true;
    //console.log(this.currentPage);
    //console.log(this.item);
    this.getData(this.itemsPerPage, this.item);
    this.range();
  }

  deleteSalesAgent(dni, codAtis) {
    this.salesAgentService
      .deleteSalesAgent(dni, codAtis)
      .subscribe((data) => {
        if (data["count"] == '1') {
          this.onSearch();
          this.showMessage = true;
          this.responseTitle = "Numero eliminado correctamente";
        } else {
          this.showMessage = true;
          this.responseTitle = "Ocurrio un error al eliminar el vendedor. Por favor intente nuevamente";
        }

      })
  }
}
