import {Pipe} from '@angular/core';

import {SvaService} from '../services/sva.service';

@Pipe({
  name: 'searchProductTypeCode',
  pure: false
})
export class PipeSearchProductTypeCode {
      constructor(private svaService:SvaService) {
      	this.svaService = svaService;
      }
      transform(args, value){
        return  args
          .filter(function(item){
              return item.prodtypecode.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
