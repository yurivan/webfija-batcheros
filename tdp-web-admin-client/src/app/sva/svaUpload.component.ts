import {Component} from '@angular/core';

import {SvaService} from '../services/sva.service';

@Component({
  moduleId: 'svaUpload',
  selector: 'my-svaUpload',
  templateUrl: './svaUpload.template.html'
})
export class SvaUploadComponent {
  model:any;
  loading:boolean;
  files:any[];

      constructor(private svaService:SvaService) {
        this.model = {};
        this.loading = false;
      }

      onClickOtroArchivo() {
        this.model = {};
        this.files = [];
      }

      onClickPersist() {
        var _this = this;
        this.loading = true;
        this.svaService.reset(this.files[0]).subscribe(function (responseData) {
          _this.model = responseData.data;
          _this.loading = false;
        }, function (err) {
          console.log("Error Persist:"+err);
          _this.loading = false;
        });
      }

      onFormSubmit() {
        var _this = this;
        this.loading = true;
        this.svaService.upload(this.files[0]).subscribe(function (responseData) {
          _this.model = responseData.data;
          _this.loading = false;
        }, function (err) {
          console.log("Error Submit"+err);
          _this.loading = false;
        });
      }

      onChangeFile(evt) {
        this.files = evt.srcElement.files;
      }
    }
