import {Component, AfterViewInit} from '@angular/core';

import {SvaService} from '../services/sva.service';
var $ = require('jquery');
@Component({
  moduleId: 'sva',
  selector: 'my-sva',
  templateUrl: './sva.template.html'
})
export class SvaComponent implements AfterViewInit{
  sva:any[];
  model:any;
  /*currentPage:number;
  itemsPerPage:number;
  item:number;
  totalData:number;
  totPages:number;
  pages:any[];*/
      constructor(private svaService:SvaService) {
      	this.sva = [];
        /*this.model = {searchProductTypeCode:'',searchUnit:'',searchDescription:''};
        this.currentPage = 0;
        this.itemsPerPage = 10;
        this.item = 0;
        this.totalData = 0;
        this.totPages = 0;
        this.pages = [];*/

        this.model = {
          count: 0,
          currentPage: 0,
          perPage: 10,
          pages: [],
          loading: false
        };

      }
      ngOnInit(){
          /*this.totalPages();
          this.getData(this.itemsPerPage, this.item);*/
      }
      /*range(){
          this.pages = [];
          var start;
          var start, rangeSize;
          if(this.totPages === 0 ){
            rangeSize = 1;
          }else if(this.totPages > 0 && this.totPages < 6){
            rangeSize = 2;
          }else if(this.totPages >= 6 && this.totPages < 11){
            rangeSize = 3;
          }else{
            rangeSize = 5;
          }
          start = this.currentPage;
          if ( start > this.totPages - rangeSize) {
            start = this.totPages- rangeSize+1;
          }
          for (var i=start; i<start+rangeSize; i++) {
            this.pages.push(i);
          }
          return this.pages;
      }
        getData(perPage,pages){
          var _this = this;
              this.svaService.loadByPages(perPage,pages)
              .subscribe(function(data){
                _this.sva = data;
              }, function(err){
                console.log(err);
              });
        }
        totalPages(){
          var _this = this;
          this.svaService.countData()
            .subscribe(function(data){
              _this.totalData = data.count;
              _this.totPages = Math.ceil(_this.totalData/_this.itemsPerPage)-1;
              _this.range();
            }, function(err){
              console.log(err);
            });
        }
        prevPage(){
          if(this.currentPage > 0){
            this.currentPage=this.currentPage-1;
            this.item = this.currentPage*this.itemsPerPage;
          }
          this.getData(this.itemsPerPage, this.item);
          this.range();
        }
        disablePrevPage(){
          return this.currentPage === 0 ? "disabled" : "";
        }
        nextPage(){
          if(this.currentPage<this.totPages){
            this.currentPage=this.currentPage+1;
            this.item = this.currentPage*this.itemsPerPage;
          }
          this.getData(this.itemsPerPage, this.item);
          this.range();
        }
        disableNextPage(){
          return this.currentPage === this.totPages ? "disabled" : "";
        }
        setPage(n){
          this.currentPage = n;
          this.item = this.currentPage*this.itemsPerPage;
          this.getData(this.itemsPerPage, this.item);
          this.range();
        }*/



        onSearch() {
          this.search(0);
        }
        
        gotoPage(i) {
          this.model.currentPage = i;
          this.search(i);
        }
        
        gotoNextPage() {
          if((this.model.currentPage = this.model.numPages -1) ||  this.model.numPages == 1){
            return;
          }
        
          var nextPage = this.model.currentPage + 1;
          if (this.model.numPages < nextPage) {
            this.model.currentPage = this.model.numPages;
          } else {
            this.model.currentPage = nextPage;
          }
          this.search(this.model.currentPage);
        }
        
        gotoPrevPage() {
          var nextPage = this.model.currentPage - 1;
          if (0 >= nextPage) {
            this.model.currentPage = 0;
          } else {
            this.model.currentPage = nextPage;
          }
          this.search(this.model.currentPage);
        }
        
        search(currentPage) {
          var _this = this;
          var producttypecode = '%' + (this.model.producttypecode == null ? '' : this.model.producttypecode) + '%';
          var description = '%' + (this.model.description == null ? '' : this.model.description) + '%';
          var unit = '%' + (this.model.unit == null ? '' : this.model.unit) + '%';
          var sistemas = '%' + (this.model.sistemas == null ? '' : this.model.sistemas) + '%';
          var comercializable = '%' + (this.model.comercializable == null ? '' : this.model.comercializable) + '%';
          
          var filter = {
            limit: this.model.perPage,
            skip: currentPage * this.model.perPage,
            where: {
              and: [
                { producttypecode: { like: producttypecode } },
                { description: { like: description } },
                { unit: { like: unit } },
                { sistemas: { like: sistemas } },
                { comercializable: { like: comercializable } }
              ]
            }
          };
          this.model.loading = true;
          this.svaService.search(filter).subscribe(function (data) {
            _this.sva = data;
            _this.computePaginator(filter.where);
            _this.model.loading = false;
          }, function (err) { });;
        }
        
        computePaginator(where) {
          var _this = this;
          this.svaService.count(where).subscribe(function (data) {
            _this.model.pages = []
            _this.model.count = data.count;
            _this.model.numPages = Math.ceil(_this.model.count / _this.model.perPage);
            var numPages = _this.model.numPages;
            var min = _this.model.currentPage - 5;
            var max = _this.model.currentPage + 5;
            if (min < 0) min = 0;
            if (max > numPages) max = numPages;
            for (var i = min; i < max; i++) {
              _this.model.pages.push(i);
            }
          });
        }
        
        ngAfterViewInit(){
          this.setWidthPanel();
          this.search(this.model.currentPage);
        }
        
        setWidthPanel(){
          //  Ajustar el ancho del panel al 100%
          var browserZoomLevel = Math.round(window.devicePixelRatio * 100);
          if(browserZoomLevel > 95){
            $("#div-pan-sva").css({width:  '1050px'});
          }
        
        }






    }
