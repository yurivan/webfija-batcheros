import {Pipe} from '@angular/core';

import {SvaService} from '../services/sva.service';

@Pipe({
  name: 'searchDescription',
  pure: false
})
export class PipeSearchDescription {
      constructor(private svaService:SvaService) {
      	this.svaService = svaService;
      }
      transform(args, value){
        return  args
          .filter(function(item){
              return item.description.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
