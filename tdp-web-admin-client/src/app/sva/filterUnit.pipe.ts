import {Pipe} from '@angular/core'

import {SvaService} from '../services/sva.service';

@Pipe({
  name: 'searchUnit',
  pure: false
})
export class PipeSearchUnit {
      constructor(private svaService:SvaService) {
      	this.svaService = svaService;
      }
      transform(args, value){
        return  args
          .filter(function(item){
              return item.unit.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
      }
    }
