import {Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {SvaService} from '../services/sva.service';

@Component({
  moduleId: 'svaDetail',
  selector: 'my-svaDetail',
  templateUrl: './svaDetail.template.html'
})
export class SvaDetailComponent {
  item:any;
      constructor(private route:ActivatedRoute, private svaService:SvaService) {
        this.item = {};
      }
      ngOnInit(){
        var _this = this;
        this.route.params
          .subscribe(function(params){
            _this.svaService.loadById(params['id'])
            .subscribe(function(data){
              _this.item = data;
            },function(err){
              console.log(err);
            });
          },function(err){
            console.log(err);
          });
      }
    }
