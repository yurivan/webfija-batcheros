import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { Observable } from 'rxjs/Rx';

export class responseParamTrazabilidad {
    id: number
    service: string
    orden: number
    descriptionkey: string
    servicekey: string
    responsemessage: string
    responsecode: number
    servicecode: string
}
@Component({
    moduleId: 'paramtrazabilidad',
    selector: 'paramtrazabilidad',
    templateUrl: 'paramtrazabilidad.template.html'
})
export class ParamTrazabilidadComponent implements OnInit {
    trazabilidadParameters = []
    trz_param_sol_b: boolean
    trz_param_reg_b: boolean
    trz_param_tecasig_b: boolean
    trz_param_inst_b: boolean
    trz_param_sol: responseParamTrazabilidad
    trz_param_reg: responseParamTrazabilidad
    trz_param_tecasig : responseParamTrazabilidad
    trz_param_inst: responseParamTrazabilidad

    responseCompleted: boolean
    ocultar: boolean
    txtResponse: string

    ocultar2: boolean
    txtResponse2: string

    ngOnInit() {
        this.getsomething()
    }

    constructor(private messageService: MessageService) {
        this.trz_param_sol = new responseParamTrazabilidad()
        this.trz_param_reg = new responseParamTrazabilidad()
        this.trz_param_tecasig = new responseParamTrazabilidad()
        this.trz_param_inst = new responseParamTrazabilidad()

        this.trz_param_sol_b = false
        this.trz_param_reg_b = false
        this.trz_param_tecasig_b = false
        this.trz_param_inst_b = false


        this.responseCompleted = false
        this.ocultar = false
        this.txtResponse = ""
    }
    getsomething() {
        this.messageService.getMessageList('PTRZ').subscribe(parametro => {
            for (let i = 0; i < parametro.length; i++) {
                this.trazabilidadParameters.push(parametro[i])
            }
            console.log("security " + JSON.stringify(this.trazabilidadParameters))
            for (let i = 0; i < this.trazabilidadParameters.length; i++) {
                switch (this.trazabilidadParameters[i].servicekey) {
                    case "trz_param_sol":
                        this.trz_param_sol = this.trazabilidadParameters[i]
                        break
                    case "trz_param_reg":
                        this.trz_param_reg = this.trazabilidadParameters[i]
                        break
                    case "trz_param_tecasig":
                        this.trz_param_tecasig = this.trazabilidadParameters[i]
                        break
                    case "trz_param_inst":
                        this.trz_param_inst = this.trazabilidadParameters[i]
                        break
                }
            }

        }, error => {
            this.ocultar2 = true
            this.txtResponse2 = "Hubo un error, intente de nuevo."
        })
    }
    setParams() {
        let isOkTrzParamSol = this.validateTrzParamSol()
        let isOkTrzParamReg = this.validateTrzParamReg()
        let isOkTrzParamTecasig = this.validateTrzParamTecasig()
        let isOkTrzParamInst = this.validateTrzParamInst()
        if (isOkTrzParamSol
            && isOkTrzParamReg
            && isOkTrzParamTecasig
            && isOkTrzParamInst
        ) {
            const arrayfinal = [this.trz_param_sol, this.trz_param_reg, this.trz_param_tecasig, this.trz_param_inst]
            let responseAmount = 0
            for (let i = 0; i < arrayfinal.length; i++) {
                this.messageService.setConfigParametersTrz(arrayfinal[i]["id"], arrayfinal[i]["responsecode"]).subscribe(respuesta => {
                    responseAmount++
                    if (responseAmount == arrayfinal.length) {
                        this.ocultar = true
                        this.txtResponse = "Se guardaron los cambios."
                        setTimeout(() => {
                            this.ocultar = false
                        }, 3000)

                    }
                }, error => {
                    this.ocultar2 = true
                    this.txtResponse2 = "Hubo un error, intente de nuevo."
                    setTimeout(() => {
                        this.ocultar2 = false
                    }, 3000)
                })
            }
        }else{
            this.ocultar2 = true
            this.txtResponse2 = "Hubo un error, solo acepta valores 0 y 1, intente de nuevo."
            setTimeout(() => {
                this.ocultar2 = false
            }, 3000)
        }
    }

    validate(event: any) {
        var code = event.keyCode;
        if ((code < 48 || code > 57) // numerical
            && code !== 46 //delete
            && code !== 8  //back space
            && code !== 37 // <- arrow
            && code !== 39) // -> arrow
        {
            event.preventDefault();
        }
        return
    }

    validateTrzParamSol() {
        if (this.trz_param_sol.responsecode != null && (this.trz_param_sol.responsecode == 1 || this.trz_param_sol.responsecode == 0)) {
            this.trz_param_sol_b = false
            return true
        } else {
            this.trz_param_sol_b = true
            return false
        }
    }

    validateTrzParamTecasig() {
        if (this.trz_param_tecasig.responsecode != null && (this.trz_param_tecasig.responsecode == 1 || this.trz_param_tecasig.responsecode == 0)) {
            this.trz_param_tecasig_b = false
            return true
        } else {
            this.trz_param_tecasig_b = true
            return false
        }
    }

    validateTrzParamReg() {
        if (this.trz_param_reg.responsecode != null && (this.trz_param_reg.responsecode == 1 || this.trz_param_reg.responsecode == 0)) {
            this.trz_param_reg_b = false
            return true
        } else {
            this.trz_param_reg_b = true
            return false
        }
    }

    validateTrzParamInst() {
        if (this.trz_param_inst.responsecode != null && (this.trz_param_inst.responsecode == 1 || this.trz_param_inst.responsecode == 0)) {
            this.trz_param_inst_b = false
            return true
        } else {
            this.trz_param_inst_b = true
            return false
        }
    }

}