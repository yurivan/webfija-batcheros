import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { Observable } from 'rxjs/Rx';
import { MessageParameters } from '../listmessage/listmessage.component';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router/src/router';

@Component({
    moduleId: 'reviewmessage',
    selector: 'reviewmessage',
    templateUrl: 'reviewmessage.template.html'
})
export class ReviewMessageComponent implements OnInit {
    dataResponse
    dataSpeechResponse
    elementResponse: MessageParameters
    dataHasArrived2: boolean
    title: string

    constructor(private messageService: MessageService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        //this.dataResponse = []
        this.elementResponse = new MessageParameters()
        this.dataResponse = new Array()
        this.dataSpeechResponse = new Array()
        this.dataHasArrived2 = false
    }
    ngOnInit() {
        this.route.params
            .subscribe(params => {
                //consolelog("type " + params["id"])
                this.getMessage(params["id"])
                switch (params["id"]) {
                    case 'ARPU':
                        this.title = "ARPU"
                        break;
                    case 'AU':
                        this.title = "AUDIO"
                        break;
                    case 'B':
                        this.title = "BIOMETRIA"
                        break;
                    case 'C':
                        this.title = "CAMBIAR CONTRASEÑA"
                        break;
                    case 'SC':
                        this.title = "CONDICIONES VENTA"
                        break;
                    case 'CO':
                        this.title = "CONTRATO"
                        break;
                    case 'VD':
                        this.title = "DUPLICADOS"
                        break;
                    case 'EXP':
                        this.title = "EXPERTO"
                        break;
                    case 'IC':
                        this.title = "INFORMACION CLIENTE"
                        break;
                    case 'L':
                        this.title = "LOGIN"
                        break;
                    case 'N':
                        this.title = "NORMALIZADOR"
                        break;
                    case 'PA':
                        this.title = "PARQUE"
                        break;
                    case 'CP':
                        this.title = "POLITICA COMERCIAL"
                        break;
                    case 'PRO':
                        this.title = "PRODUCTO"
                        break;
                    case 'R':
                        this.title = "REGISTRAR"
                        break;
                    case 'E':
                        this.title = "RENIEC"
                        break;
                    case 'P':
                        this.title = "RESTABLECER CONTRASEÑA"
                        break;
                    case 'SVA':
                        this.title = "SVA"
                        break;
                    case 'MTRZ':
                        this.title = "TRAZABILIDAD"
                        break;
                    case 'V':
                        this.title = "VENTA"
                        break;
                    case 'OO':
                        this.title = "PRENDIDO Y APAGADO DE VENTAS"
                        break;
                }
            }
            )
    }

    getMessage(servicecode: string) {
        this.messageService.getMessageList(servicecode).subscribe(data => {
            data.map(element => {
                this.elementResponse = element
                this.dataResponse.push(this.elementResponse)
            })
            this.dataResponse.push(data)
        }, error => {

        }, () => {
            this.dataHasArrived2 = true
        })
    }

    returnList() {
        this.router.navigate(['/listmessage']);
    }
}