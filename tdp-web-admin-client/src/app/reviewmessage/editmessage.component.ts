import { Component, OnInit } from '@angular/core';
import { MessageService } from '../services/message.service';
import { Observable } from 'rxjs/Rx';
import { MessageParameters } from '../listmessage/listmessage.component';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router/src/router';
import { AuthService } from '../services/auth.service';
import { error } from 'util';
import { forEach } from '@angular/router/src/utils/collection';
import { setTimeout } from 'timers';

@Component({
    moduleId: 'editmessage',
    selector: 'editmessage',
    templateUrl: 'editmessage.template.html'
})
export class EditMessageComponent implements OnInit {
    dataResponse
    elementResponse: MessageParameters
    title: string
    clientValues
    isCheckedAppBox: boolean
    checkStatus: boolean
    listTemp
    errorResponse
    typeOfContract: string
    alwaysShowApp: boolean
    originalResponse
    dataSpeechResponse
    originalSpeechResponse
    dataHasArrived2: boolean
    dataWasSaved: boolean
    messageSuccessful: boolean
    messageResponse: string
    dataSaved: boolean
    dataPublished: boolean
    listMessageError = []

    constructor(private messageService: MessageService,
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService
    ) {
        this.elementResponse = new MessageParameters()
        this.dataResponse = new Array()
        this.dataSpeechResponse = new Array()
        this.clientValues = new Array()
        this.isCheckedAppBox = false
        this.listTemp = new Array()
        this.errorResponse = new Array()
        this.originalResponse = new Array()
        this.originalSpeechResponse = new Array()
        this.dataHasArrived2 = false
        this.dataWasSaved = false
        this.messageSuccessful = false
        this.messageResponse = ""
        this.dataSaved = false
        this.dataPublished = false
    }

    ngOnInit() {
        this.checkStatus = true
        this.alwaysShowApp = true

        this.route.params
            .subscribe(params => {
                this.typeOfContract = params["id"]
                this.getMessage(params["id"])
                switch (params["id"]) {
                    case 'ARPU':
                        this.title = "ARPU"
                        break;
                    case 'AU':
                        this.title = "AUDIO"
                        break;
                    case 'B':
                        this.title = "BIOMETRIA"
                        break;
                    case 'C':
                        this.title = "CAMBIAR CONTRASEÑA"
                        break;
                    case 'SC':
                        this.title = "CONDICIONES VENTA"
                        break;
                    case 'CO':
                        this.title = "CONTRATO"
                        break;
                    case 'VD':
                        this.title = "DUPLICADOS"
                        break;
                    case 'EXP':
                        this.title = "EXPERTO"
                        break;
                    case 'IC':
                        this.title = "INFORMACION CLIENTE"
                        break;
                    case 'L':
                        this.title = "LOGIN"
                        break;
                    case 'N':
                        this.title = "NORMALIZADOR"
                        break;
                    case 'PA':
                        this.title = "PARQUE"
                        break;
                    case 'PRO':
                        this.title = "PRODUCTO"
                        break;
                    case 'CP':
                        this.title = "POLITICA COMERCIAL"
                        break;
                    case 'R':
                        this.title = "REGISTRAR"
                        break;
                    case 'E':
                        this.title = "RENIEC"
                        break;
                    case 'P':
                        this.title = "RESTABLECER CONTRASEÑA"
                        break;
                    case 'SVA':
                        this.title = "SVA"
                        break;
                    case 'MTRZ':
                        this.title = "TRAZABILIDAD"
                        break;
                    case 'V':
                        this.title = "VENTA"
                        break;
                    case 'OO':
                        this.title = "PRENDIDO Y APAGADO DE VENTAS"
                        break;
                }
            })
    }

    getMessage(type: string) {
        this.listMessageError = []
        this.messageService.getMessageList(type).subscribe(parametro => {
            for (let i = 0; i < parametro.length; i++) {
                this.listMessageError.push(parametro[i])
            }
            console.log("security " + JSON.stringify(this.listMessageError))
        },
            error => { },
            () => {
                let posicion = 1
                this.listMessageError.map(element => {
                    posicion++
                })
                this.listMessageError.map(element => {
                    let newResponse = {
                        "id": element.id,
                        "service": element.service,
                        "orden": element.orden,
                        "descriptionkey": element.descriptionkey,
                        "servicekey": element.servicekey,
                        "responsemessage": element.responsemessage,
                        "responsecode": element.responsecode,
                        "servicecode": element.servicecode
                    }
                    this.originalResponse.push(newResponse)
                }
                )
            })
        /*this.messageService.getMessageList(type)
            .subscribe(
            data => {
                data.map(element => {
                    this.elementResponse = element
                    this.dataResponse.push(this.elementResponse)
                })
            },
            error => { },
            () => {
                let posicion = 1
                this.dataResponse.map(element => {
                    posicion++
                })
                this.dataResponse.map(element => {
                    let newResponse = {
                        "id": element.id,
                        "service": element.service,
                        "orden": element.orden,
                        "descriptionkey": element.descriptionkey,
                        "servicekey": element.servicekey,
                        "responsemessage": element.responsemessage,
                        "responsecode": element.responsecode,
                        "servicecode": element.servicecode
                    }
                    this.originalResponse.push(newResponse)
                }
                )
            })*/
    }

    returnList() {
        if (confirm("¿Está seguro que desea salir?")) {
            this.router.navigate(['/listmessage'])
        }
    }

    save() {
        let send = "SAVE"
        this.dataWasSaved = true
        this.validateForm(send)
    }

    validateForm(send: string) {
        this.errorResponse = new Array(0)
        let arrayInicial = new Array()
        this.dataResponse.map(element => {
            arrayInicial.push(element)
        })
        this.saveData(false)
    }

    saveMessageError() {
        this.dataWasSaved = true
        const arrayfinal = [this.listMessageError]
        let responseAmount = 0
        for (let i = 0; i < this.listMessageError.length; i++) {
            this.messageService.updateReviewUnit(this.listMessageError[i].id, this.listMessageError[i].responsemessage).subscribe(respuesta => {
                responseAmount++
                if (responseAmount == arrayfinal.length) {
                    this.dataWasSaved = false
                    this.messageSuccessful = true
                    this.dataSaved = true
                    this.messageResponse = "Se guardó correctamente"
                    setTimeout(() => {
                        this.messageSuccessful = false
                        //this.router.navigate(['/listcontract'])
                    }, 5000)
                }
            })
        }
    }

    saveData(send) {
        let amountRequest = 0
        let dataResult = false
        if (send == false) {
            let listBody = []
            this.dataResponse.map((element, index) => {
                if (element.id != undefined) {
                    let body = {}
                    body = {
                        "id": element.id,
                        "responsemessage": element.responsemessage
                    }
                    listBody.push(body)
                }
            })
            let bodyRequest = {
                "messageList": listBody
            }
            this.messageService.updateReviewList(bodyRequest).subscribe(response => {
                this.dataWasSaved = false
                this.messageSuccessful = true
                this.dataSaved = true
                this.messageResponse = "Se guardó correctamente"
                setTimeout(() => {
                    this.messageSuccessful = false
                    //this.router.navigate(['/listcontract'])
                }, 3000)
                this.originalResponse = []
                this.dataResponse.map(element => {
                    if (element != undefined) {
                        let newResponse = {
                            "id": element.id,
                            "service": element.service,
                            "orden": element.orden,
                            "descriptionkey": element.descriptionkey,
                            "servicekey": element.servicekey,
                            "responsemessage": element.responsemessage,
                            "responsecode": element.responsecode,
                            "servicecode": element.servicecode
                        }
                        this.originalResponse.push(newResponse)
                    }

                })
            }, err => { }
                , () => {
                    this.dataWasSaved = false
                    this.messageSuccessful = true
                    this.dataSaved = true
                    this.messageResponse = "Se guardó correctamente"
                })
        }
    }
}

