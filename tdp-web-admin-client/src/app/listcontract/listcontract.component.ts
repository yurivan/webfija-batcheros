import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { ContractService } from '../services/contract.service';
import { Observable } from 'rxjs/Rx';
import { Router, NavigationEnd } from '@angular/router';

export class ContractParameters {
    id: number;
    campo: string;
    category: string;
    appValue: string;
    webValue: string;
    public: boolean;
    type: string;
}

@Component({
    moduleId: 'listcontract',
    selector: 'listcontract',
    templateUrl: 'listcontract.template.html'
})

export class ListContractComponent implements OnInit, AfterViewInit {

    flagWhatsapp: boolean

    ngAfterViewInit(): void {

    }
    returnList() {
        this.router.navigate(['/administration']);
    }

    ngOnInit() {
        this.getStatusWhatsApp()
    }
    constructor(private router: Router, private contractService: ContractService) { }

    getStatusWhatsApp() {
        this.contractService.getStatusWhatsapp().subscribe(data => {
            let datos = data["_body"]
            if (datos.substring(22, 26) === 'true') {
                this.flagWhatsapp = true
            } else {
                this.flagWhatsapp = false
            }
        }, error => {
            console.log(error)
        })
    }

    UpdateStatusWhatsApp() {
        let shouldWhatsapp = this.flagWhatsapp
        this.contractService.updateStatusWhatsapp(shouldWhatsapp).subscribe(data => {
            console.log(data)
        }, error => {
            console.log(error)
        })
    }
}

