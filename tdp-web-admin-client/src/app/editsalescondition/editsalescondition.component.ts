import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { MessageParameters } from '../listmessage/listmessage.component';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router/src/router';
import { AuthService } from '../services/auth.service';
import { ConfigofferingService } from '../services/configoffering.service';
import { error } from 'util';
import { forEach } from '@angular/router/src/utils/collection';
import { setTimeout } from 'timers';

export class Parameters {
    id: number;
    auxiliar: number;
    domain: string;
    category: string;
    element: string;
    strvalue: string;
    strfilter: string;

    titulo: string;
    contenido: string;
}
@Component({
    moduleId: 'editsalescondition',
    selector: 'editsalescondition',
    templateUrl: 'editsalescondition.template.html'
})
export class EditSalesConditionComponent implements OnInit {
    unitCondition: Parameters;
    dataWasSaved: boolean = false;
    messageSuccessful: boolean = false;
    messageResponse: string;

    marcador: { id: string, str: string; }[];
    posicion: { id: number, str: number; }[];
    codeMarcador: string;
    codePosicion: number;

    salesCondition: Parameters[];
    unit2Condition: Parameters;

    tmp_codeCondition: number;
    tmp_category: string;

    constructor(private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService,
        private configofferingService: ConfigofferingService) {
        this.configofferingService = configofferingService;
        this.messageSuccessful = false;
    }

    ngOnInit() {
        this.unitCondition = JSON.parse(localStorage.getItem('unitCondition'));
        this.salesCondition = JSON.parse(localStorage.getItem('salesCondition'))
        this.llenarCombos();
        this.codeMarcador = this.unitCondition.strfilter;
        this.codePosicion = this.unitCondition.auxiliar;
        let array = this.unitCondition.strvalue.split('|');
        this.unitCondition.titulo = array[0];
        this.unitCondition.contenido = array[1];
        this.tmp_codeCondition = this.unitCondition.auxiliar;
        this.tmp_category = this.unitCondition.category;
    }

    llenarCombos() {
        this.marcador = [
            { id: '0', str: 'Si' },
            { id: '1', str: 'No' },
            { id: '2', str: 'En Blanco' }
        ]

        this.posicion = [
            { id: 1, str: 1 },
            { id: 2, str: 2 },
            { id: 3, str: 3 },
            { id: 4, str: 4 }
        ]
    }

    returnList() {
        //if (confirm("¿Está seguro que desea salir?")) {
        this.router.navigate(['/salescondition'])
        //}
    }

    save() {
        this.dataWasSaved = true;
        if (this.tmp_codeCondition == this.codePosicion) {
            this.unitCondition.strvalue = this.unitCondition.titulo + '|' + this.unitCondition.contenido;
            this.unitCondition.auxiliar = this.codePosicion;
            this.unitCondition.strfilter = this.codeMarcador;
            let body = {
                "auxiliar": this.unitCondition.auxiliar,
                "strvalue": this.unitCondition.strvalue,
                "strfilter": this.unitCondition.strfilter,
                "category": this.unitCondition.category
            }

            let _this = this;
            this.configofferingService.modifySalesCondition(body).subscribe(
                response => {
                    if (response.data.affectedRows == 1) {
                        _this.messageSuccessful = true;
                        _this.messageResponse = "Se guardo correctamente";
                        _this.dataWasSaved = false;
                    } else {
                        _this.messageResponse = "Error al guardar, reintentar de nuevo";
                    }
                },
                err => {
                    _this.messageResponse = "Servicio caido, reintentar de nuevo" + JSON.stringify(err);
                }
            );
        } else {
            this.unitCondition.strvalue = this.unitCondition.titulo + '|' + this.unitCondition.contenido;
            this.unitCondition.auxiliar = this.codePosicion;
            this.unitCondition.strfilter = this.codeMarcador;
            let body = {
                "auxiliar": this.unitCondition.auxiliar,
                "strvalue": this.unitCondition.strvalue,
                "strfilter": this.unitCondition.strfilter,
                "category": this.unitCondition.category
            }

            for (let condicion of this.salesCondition) {
                if (condicion.auxiliar == this.codePosicion && this.tmp_category != condicion.category) {
                    this.unit2Condition = new Parameters();
                    this.unit2Condition = condicion;
                    this.unit2Condition.auxiliar = this.tmp_codeCondition;
                }
            }

            let body2 = {
                "auxiliar": this.unit2Condition.auxiliar,
                "strvalue": this.unit2Condition.strvalue,
                "strfilter": this.unit2Condition.strfilter,
                "category": this.unit2Condition.category
            }

            let _this = this;
            this.configofferingService.modifySalesCondition(body).subscribe(
                response => {
                    if (response.data.affectedRows == 1) {
                        _this.cambiarPosicion(body2);
                    } else {
                        _this.messageResponse = "Error al guardar, reintentar de nuevo";
                    }
                },
                err => {
                    _this.messageResponse = "Servicio caido, reintentar de nuevo" + JSON.stringify(err);
                }
            );
        }
    }

    cambiarPosicion(body2) {
        let _this = this;
        this.configofferingService.modifySalesCondition(body2).subscribe(
            response => {
                if (response.data.affectedRows == 1) {
                    _this.messageSuccessful = true;
                    _this.messageResponse = "Se guardo correctamente";
                    _this.dataWasSaved = false;
                } else {
                    _this.messageResponse = "Error al guardar, reintentar de nuevo";
                }
            },
            err => {
                _this.messageResponse = "Servicio caido, reintentar de nuevo" + JSON.stringify(err);
            }
        );

    }
}

