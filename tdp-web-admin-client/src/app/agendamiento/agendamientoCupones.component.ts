import { Component, OnInit } from '@angular/core';
import { AgendamientoService } from '../services/agendamiento.service'

declare var $: any;

export class responseSecurity {
    id: string
    auxiliar: string
    domain: string
    category: string
    element: string
    strvalue: string
}

@Component({
    moduleId: 'agendamientoCupones',
    selector: 'agendamientoCupones',
    templateUrl: 'agendamientoCupones.template.html'
})

export class AgendamientoCuponesComponent implements OnInit {

    loading: boolean

    msgResult: string
    msgError: string

    cuponesList = []

    inputDay: Date
    inputCupones: number
    selectFranja: string

    cuponId: number

    parameterTmp: responseSecurity

    constructor(private agendamientoService: AgendamientoService) {
    }

    ngOnInit(): void {
        this.loading = false
        this.getCupones()

        $('.modal').modal();
    }

    getCupones() {
        this.CleanMessage()
        var _this = this
        _this.loading = true

        _this.agendamientoService.getAgendamientoCupones().subscribe(function (data) {
            console.log(data)
            _this.cuponesList = data.json();
            //console.log(parameterTmp)
            _this.loading = false
        }, function (error) {
            console.log(error)
            _this.loading = false
            this.msgError = "No se logró obtener Agendamientos"
        })
    }

    addCupon() {
        var cuponDay = this.inputDay
        var cuponCupones = this.inputCupones
        var cuponFranja = this.selectFranja
        this.CleanMessage()
        this.loading = true

        if (!cuponDay) {
            this.msgError = "Dato Fecha no puede ser en blanco."
            this.loading = false
            return
        }
        if (!cuponCupones) {
            this.msgError = "Dato Cupones no puede ser en blanco."
            this.loading = false
            return
        }
        if (!cuponFranja) {
            this.msgError = "Dato Franja no puede ser en blanco."
            this.loading = false
            return
        }
        var cuponItem = {
            "day": cuponDay,
            "franja": cuponFranja,
            "cupon": cuponCupones
        }
        console.log(cuponItem)

        var _this = this
        _this.msgResult = ""
        _this.msgError = ""
        _this.CleanMessage()
        _this.loading = true

        _this.agendamientoService.insertUpdateAgendamientoCupones(cuponItem).subscribe(function (data) {
            console.log(data)
            if (data != null && data.data != null) {
                var response = data.data
                if (response.affectedRows != null) {
                    if (response.affectedRows == 1) {
                        _this.msgResult = "Cupón Añadido"
                    } else {
                        _this.msgError = "No se logró realizar eliminar el cupón."
                    }
                } else {
                    _this.msgError = "No se logró grabar el cupón..."
                }
            } else {
                _this.msgError = "No se logró grabar el cupón.."
            }
            _this.loading = false
            _this.getCupones()
        }, function (error) {
            console.log(error)
            _this.loading = false
            _this.msgError = "No se logró eliminar el cupón."
        })
    }

    removeCupon(cuponesId) {

        $('#modalEliminar').modal('open');
        this.cuponId = cuponesId

    }

    doDelete() {

        var _this = this
        var cuponesId = _this.cuponId
        _this.msgResult = ""
        _this.msgError = ""
        _this.CleanMessage()
        _this.loading = true

        _this.agendamientoService.deleteAgendamientoCupones(cuponesId).subscribe(function (data) {
            console.log(data)
            if (data != null && data.data != null) {
                var response = data.data
                if (response.affectedRows != null) {
                    if (response.affectedRows == 1) {
                        _this.msgResult = "Cupón eliminado"
                    } else {
                        _this.msgError = "No se logró realizar eliminar el cupón."
                    }
                } else {
                    _this.msgError = "No se logró grabar el cupón..."
                }
            } else {
                _this.msgError = "No se logró grabar el cupón.."
            }
            _this.loading = false
            _this.getCupones()
        }, function (error) {
            console.log(error)
            _this.loading = false
            _this.msgError = "No se logró eliminar el cupón."
        })
    }

    cancelDelete() {
        $('#modalEliminar').modal('close');
    }

    CleanMessage() {
        this.msgResult = ""
        this.msgError = ""
        this.inputDay = null
        this.inputCupones = null
        this.selectFranja = null
    }

    validate(event: any) {
        var code = event.keyCode;
        if ((code < 48 || code > 57) // numerical
            && code !== 46 //delete
            && code !== 8  //back space
            && code !== 37 // <- arrow
            && code !== 39) // -> arrow
        {
            event.preventDefault();
        }
        return
    }
}