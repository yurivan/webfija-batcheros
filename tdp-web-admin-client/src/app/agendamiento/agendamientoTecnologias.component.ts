import { Component, OnInit } from '@angular/core';
import { AgendamientoService } from '../services/agendamiento.service'

export class responseSecurity {
    id: string
    auxiliar: string
    domain: string
    category: string
    element: string
    strvalue: string
}

@Component({
    moduleId: 'agendamientoTecnologias',
    selector: 'agendamientoTecnologias',
    templateUrl: 'agendamientoTecnologias.template.html'
})

export class AgendamientoTecnologiasComponent implements OnInit {

    loading: boolean

    msgResult: string
    msgError: string

    canalesList = []

    inputTecnologia: string

    parameterTmp: responseSecurity

    constructor(private agendamientoService: AgendamientoService) {
    }

    ngOnInit(): void {
        this.loading = false
        this.getCanales()
    }

    getCanales() {
        this.CleanMessage()
        var _this = this
        _this.loading = true

        this.agendamientoService.getconfigParameters("VAL_AREA", "TECNOLOGIA").subscribe(function (data) {
            //console.log(data)
            var parameterTmp = data.json();
            //console.log(parameterTmp)
            parameterTmp.forEach(parameter => {
                //console.log(parameter)
                _this.canalesList = []
                var canales = parameter.strvalue.split(",")
                console.log(canales)
                canales.forEach(canales => {
                    var canalItem = { canal: canales }
                    _this.canalesList.push(canalItem)
                });
            });
            _this.loading = false
        }, function (error) {
            console.log(error)
            _this.loading = false
            this.msgError = "No se logró obtener CANALES"
        })
    }

    setCanales() {
        this.CleanMessage()
        var _this = this
        _this.loading = true

        var canales = ""
        var canalesObject = [];
        _this.canalesList.forEach(obj => {
            canalesObject.push(obj.canal)
        });
        canales = canalesObject.join(",")
        console.log(canales)

        _this.agendamientoService.setconfigParameters("VAL_AREA", "TECNOLOGIA", canales).subscribe(function (data) {
            console.log(data)
            if (data != null && data.data != null) {
                var response = data.data
                if (response.affectedRows != null) {
                    if (response.affectedRows == 1) {
                        _this.msgResult = "TECNOLOGÍAS actualizadas"
                    } else {
                        _this.msgError = "No se logró realizar cambios en las TECNOLOGÍAS."
                    }
                } else {
                    _this.msgError = "No se logró grabar las TECNOLOGÍAS..."
                }
            } else {
                _this.msgError = "No se logró grabar las TECNOLOGÍAS.."
            }
            _this.loading = false
        }, function (error) {
            console.log(error)
            _this.loading = false
            _this.msgError = "No se logró grabar las TECNOLOGÍAS."
        })
    }

    addCanal() {
        var canalValue = this.inputTecnologia
        this.CleanMessage()
        this.loading = true

        if (canalValue) {
            var canalItem = { canal: canalValue.toUpperCase() }

            var yala = 0
            this.canalesList.forEach(obj => {
                if (obj.canal == canalItem.canal)
                    yala++;
            });

            if (yala == 0) {
                this.canalesList.push(canalItem);
                this.inputTecnologia = ""
                this.loading = false
            } else {
                this.msgError = "Tecnología ya registrada"
                this.loading = false
            }

        } else {
            this.msgError = "Dato Tecnología no puede ser en blanco."
            this.loading = false
        }
    }

    removeCanal(canalValue) {
        this.msgResult = ""
        this.msgError = ""
        var canalesListtmp = []

        this.canalesList.forEach(obj => {
            if (obj.canal != canalValue)
                canalesListtmp.push(obj)
        });

        this.canalesList = canalesListtmp
        this.loading = false

    }

    CleanMessage() {
        this.msgResult = ""
        this.msgError = ""
        this.inputTecnologia = ""
    }
}