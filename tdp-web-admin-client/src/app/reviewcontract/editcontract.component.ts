import { Component, OnInit } from '@angular/core';
import { ContractService } from '../services/contract.service';
import { Observable } from 'rxjs/Rx';
import { ContractParameters } from '../listcontract/listcontract.component';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router/src/router';
import { AuthService } from '../services/auth.service';
import { error } from 'util';
import { forEach } from '@angular/router/src/utils/collection';
import { setTimeout } from 'timers';

@Component({
    moduleId: 'editcontract',
    selector: 'editcontract',
    templateUrl: 'editcontract.template.html'
})
export class EditContractComponent implements OnInit {
    dataResponse
    elementResponse: ContractParameters
    title: string
    clientValues
    isCheckedAppBox: boolean
    checkStatus: boolean
    listTemp
    errorResponse
    typeOfContract: string
    alwaysShowApp: boolean
    originalResponse
    dataSpeechResponse
    originalSpeechResponse
    dataHasArrived: boolean
    dataWasSaved: boolean
    messageSuccessful: boolean
    messageResponse: string
    dataSaved: boolean
    dataPublished: boolean

    constructor(private contractService: ContractService,
        private route: ActivatedRoute,
        private router: Router,
        private authService: AuthService
    ) {
        this.elementResponse = new ContractParameters()
        this.dataResponse = new Array()
        this.dataSpeechResponse = new Array()
        this.clientValues = new Array()
        this.isCheckedAppBox = false
        this.listTemp = new Array()
        this.errorResponse = new Array()
        this.originalResponse = new Array()
        this.originalSpeechResponse = new Array()
        this.dataHasArrived = false
        this.dataWasSaved = false
        this.messageSuccessful = false
        this.messageResponse = ""
        this.dataSaved = false
        this.dataPublished = false
    }
    ngOnInit() {
        this.checkStatus = true
        this.alwaysShowApp = true

        this.route.params
            .subscribe(params => {
                this.typeOfContract = params["id"]
                this.getContract(params["id"], false)
                this.getSpeech(params["id"])
                switch (params["id"]) {
                    case 'A':
                        this.title = "ALTA PURA"
                        break
                    case 'M':
                        this.title = "MIGRACIÓN"
                        break
                    case 'S':
                        this.title = "SVA"
                        break
                }
            }
            )
    }

    getSpeech(type: string) {
        this.contractService.getSpeechList(type, false)
            .subscribe(data => {
                data.map(element => {
                    this.elementResponse = element
                    this.dataSpeechResponse.push(this.elementResponse)
                    this.orderArray(this.dataSpeechResponse)
                })
            }, error => { },
                () => {
                    this.dataHasArrived = true
                    this.dataSpeechResponse.map(element => {
                        let newResponse = {
                            "id": element.id,
                            "auxiliar": element.auxiliar,
                            "campo": element.campo,
                            "category": element.category,
                            "avalue": element.appvalue,
                            "wvalue": element.webvalue,
                            "public": element.public,
                            "type": element.type
                        }
                        this.originalSpeechResponse.push(newResponse)
                        this.orderArray(this.originalSpeechResponse)
                    }
                    )
                })
    }

    orderArray = list => {
        return list.sort((a, b) => {
            let valueA = a.auxiliar,
                valueB = b.auxiliar
            if (valueA < valueB) return -1;
            if (valueA > valueB) return 1;
            return 0;
        })
    }

    getContract(type: string, option: boolean) {
        this.contractService.getContractList(type, option)
            .subscribe(
                data => {

                    data.map(element => {
                        this.elementResponse = element
                        this.dataResponse.push(this.elementResponse)
                        this.orderArray(this.dataResponse)
                    })
                },
                error => { },
                () => {
                    let posicion = 1
                    this.dataResponse.map(element => {
                        posicion++
                    })

                    this.dataResponse.map(element => {
                        let newResponse = {
                            "id": element.id,
                            "auxiliar": element.auxiliar,
                            "campo": element.campo,
                            "category": element.category,
                            "avalue": element.appvalue,
                            "wvalue": element.webvalue,
                            "public": element.public,
                            "type": element.type
                        }
                        this.originalResponse.push(newResponse)
                        this.orderArray(this.originalResponse)
                    }
                    )
                })
    }
    returnList() {
        let noHasBeenModified = this.checkForm()
        if (noHasBeenModified) {
            this.router.navigate(['/listcontract'])
        } else {

            if (this.dataPublished || this.dataSaved) {
                let noHasBeenModified = this.checkForm()
                if (noHasBeenModified) {
                    this.router.navigate(['/listcontract'])
                } else {
                    if (confirm("¿Está seguro que desea salir?")) {
                        this.router.navigate(['/listcontract'])
                    }
                }
            } else {
                if (confirm("¿Está seguro que desea salir?")) {
                    this.router.navigate(['/listcontract'])
                }
            }
        }
    }
    save() {
        let send = "SAVE"
        this.dataWasSaved = true
        this.validateForm(send)
    }
    publish() {
        let send = "PUBLISH"
        this.dataWasSaved = true
        this.validateForm(send)
    }

    checkForm() {
        this.errorResponse = new Array(0)
        let arrayInicial = new Array()
        let arraySpeechInicial = new Array()
        this.dataResponse.map(element => {
            arrayInicial.push(element)
        })
        this.dataSpeechResponse.map(element => {
            arraySpeechInicial.push(element)
        })
        if (this.typeOfContract == "A") {
            let statusWeb = this.checkStatusForm(this.originalResponse, arrayInicial, "W")
            let statusApp = this.checkStatusForm(this.originalResponse, arrayInicial, "A")
            let statusSpeech = this.checkStatusForm(this.originalSpeechResponse, arraySpeechInicial, "A")
            if (statusWeb && statusApp && statusSpeech) {
                return true
            } else {
                return false
            }
        }
        if (this.typeOfContract == "M") {
            let statusWeb = this.checkStatusForm(this.originalResponse, arrayInicial, "W")
            let statusApp = this.checkStatusForm(this.originalResponse, arrayInicial, "A")
            let statusSpeech = this.checkStatusForm(this.originalSpeechResponse, arraySpeechInicial, "A")

            if (statusWeb && statusApp && statusSpeech) {
                return true
            } else {
                return false
            }
        }
        if (this.typeOfContract == "S") {
            let statusWeb = this.checkStatusForm(this.originalResponse, arrayInicial, "W")
            let statusApp = this.checkStatusForm(this.originalResponse, arrayInicial, "A")
            let statusSpeech = this.checkStatusForm(this.originalSpeechResponse, arraySpeechInicial, "A")

            if (statusWeb && statusApp && statusSpeech) {
                return true
            } else {
                return false
            }
        }
    }



    validateForm(send: string) {
        this.errorResponse = new Array(0)
        let arrayInicial = new Array()
        let arraySpeechInicial = new Array()
        this.dataResponse.map(element => {
            arrayInicial.push(element)
        })
        this.dataSpeechResponse.map(element => {
            arraySpeechInicial.push(element)
        })

        if (this.typeOfContract == "A") {
            let allFormsAreOK = this.validateForms(
                this.originalResponse,
                arrayInicial,
                "W",
                this.originalSpeechResponse,
                arraySpeechInicial,
                this.originalResponse,
                arrayInicial,
                'A')
            if (allFormsAreOK) {
                if (send == "SAVE") {
                    this.saveData(false)
                } else if (send == "PUBLISH") {
                    this.saveData(true)
                }
            }

        }
        if (this.typeOfContract == "M") {
            let allFormsAreOK = this.validateForms(
                this.originalResponse,
                arrayInicial,
                "W",
                this.originalSpeechResponse,
                arraySpeechInicial,
                this.originalResponse,
                arrayInicial,
                'A')
            if (allFormsAreOK) {
                if (send == "SAVE") {
                    this.saveData(false)
                } else if (send == "PUBLISH") {
                    this.saveData(true)
                }
            }
        }
        if (this.typeOfContract == "S") {
            let allFormsAreOK = this.validateForms(
                this.originalResponse,
                arrayInicial,
                "W",
                this.originalSpeechResponse,
                arraySpeechInicial,
                this.originalResponse,
                arrayInicial,
                'A')
            if (allFormsAreOK) {
                if (send == "SAVE") {
                    this.saveData(false)
                } else if (send == "PUBLISH") {
                    this.saveData(true)
                }
            }
        }
    }


    getMainWordsInContract(list, type: string) {
        let arrayFromForm = new Array()
        for (let i = 0; i < list.length; i++) {
            let element: string
            if (type == "A") {
                if (list[i]["appvalue"] != undefined) {
                    element = list[i]["appvalue"]
                    if (element.match(/\[\w.\s.*\]/g)) {
                        let wordsSelected = element.match(/\[\w.\s.*\]/g)
                        arrayFromForm.push(wordsSelected)
                    }
                    if (element.match(/\[\w*\]/g)) {
                        let wordsSelected = element.match(/\[\w*\]/g)
                        arrayFromForm.push(wordsSelected)

                    } else {
                        arrayFromForm.push(['null'])
                    }
                } else if (list[i]["avalue"] != undefined) {
                    element = list[i]["avalue"]
                    if (element.match(/\[\w.\s.*\]/g)) {
                        let wordsSelected = element.match(/\[\w.\s.*\]/g)
                        arrayFromForm.push(wordsSelected)
                    }
                    if (element.match(/\[\w*\]/g)) {
                        let wordsSelected = element.match(/\[\w*\]/g)
                        arrayFromForm.push(wordsSelected)

                    } else {
                        arrayFromForm.push(['null'])
                    }
                }
            } else if (type == "W") {
                if (list[i]["webvalue"] != undefined) {
                    element = list[i]["webvalue"]
                    if (element.match(/\[\w.\s.*\]/g)) {
                        let wordsSelected = element.match(/\[\w.\s.*\]/g)
                        arrayFromForm.push(wordsSelected)
                    }
                    if (element.match(/\[\w*\]/g)) {
                        let wordsSelected = element.match(/\[\w*\]/g)
                        arrayFromForm.push(wordsSelected)

                    } else {
                        arrayFromForm.push(['null'])
                    }
                } else if (list[i]["wvalue"] != undefined) {
                    element = list[i]["wvalue"]
                    if (element.match(/\[\w.\s.*\]/g)) {
                        let wordsSelected = element.match(/\[\w.\s.*\]/g)
                        arrayFromForm.push(wordsSelected)
                    }
                    if (element.match(/\[\w*\]/g)) {
                        let wordsSelected = element.match(/\[\w*\]/g)
                        arrayFromForm.push(wordsSelected)

                    } else {
                        arrayFromForm.push(['null'])
                    }
                }
            }
        }
        return arrayFromForm
    }

    isTheFormOk = (originalArray, modifiedArray) => {
        let errorMessage = ""
        for (let i = 0; i < originalArray.length; i++) {
            for (let j = 0; j < originalArray[i].length; j++) {
                if (modifiedArray[i].length == 0) {
                    console.log(" modifiedArray[i].length == 0 ")
                    if (originalArray[i][j].length > modifiedArray[i].length) {
                        this.dataWasSaved = false
                        errorMessage = `${originalArray[i][j]} en la línea ${i + 1}`
                        this.errorResponse.push(errorMessage)
                        //modifiedArray[i].splice(k, 1)
                        break
                    }
                }
                if (modifiedArray[i].length > 0) {
                    let varWasFound = false
                    let countError = 0
                    for (let k = 0; k < modifiedArray[i].length; k++) {
                        //console.log("comparando ::: " + originalArray[i][j] + " -------modified " + modifiedArray[i][k])
                        if (originalArray[i][j] != undefined) {
                            if (originalArray[i][j].includes(modifiedArray[i][k])) {
                                originalArray[i] = originalArray[i].filter(element => {
                                    return element !== originalArray[i][j]
                                })
                                modifiedArray[i] = modifiedArray[i].filter(element => {
                                    return element !== modifiedArray[i][k]
                                })
                                k--
                                if (modifiedArray[i].length == 0 && originalArray[i].length > 0) {
                                    this.dataWasSaved = false
                                    errorMessage = `${originalArray[i][j]} en la línea ${i + 1}`
                                    if (originalArray[i][j] != undefined) {
                                        this.errorResponse.push(errorMessage)
                                    }

                                    break
                                } else {
                                    continue
                                }
                            } else {
                                // console.log(`k : ${k} ==  ----- length ${modifiedArray[i].length}
                                // ------------ original array length ${originalArray[i].length}`)
                                // console.log(`${originalArray[i][j]} 1 -------------- ${modifiedArray[i][k]} `)
                                if (!originalArray[i][j]) {
                                    //console.log(`${originalArray[i][j]} 2 -------------- ${modifiedArray[i][k]} `)
                                    if (k == (modifiedArray[i].length)) {
                                        this.dataWasSaved = false
                                        errorMessage = `${originalArray[i][j]} en la línea ${i + 1}`
                                        this.errorResponse.push(errorMessage)
                                        //modifiedArray[i].splice(k, 1)
                                        break
                                    } else {
                                        this.dataWasSaved = false
                                        errorMessage = `${originalArray[i][j]} en la línea ${i + 1}`
                                        this.errorResponse.push(errorMessage)
                                        //modifiedArray[i].splice(k, 1)
                                        break
                                    }
                                }
                            }


                        }

                    }
                }
            }
        }
        if (this.errorResponse.length == 0) {
            //this.dataWasSaved = false
            return true
        } else {
            this.dataWasSaved = false
            return false
        }
    }

    validateChangesForm = (originalArray, modifiedArray) => {
        let originalArrayMapped = []
        let modifiedArrayMapped = []
        originalArray.map(element => {
            originalArrayMapped.push(element)
        })
        modifiedArray.map(element => {
            modifiedArrayMapped.push(element)
        })
        let isThereAnyChange = false
        for (let i = 0; i < originalArrayMapped.length; i++) {
            if (modifiedArrayMapped[i].webvalue !== undefined) {
                if (originalArrayMapped[i]["wvalue"].includes(modifiedArrayMapped[i]["webvalue"])
                    && originalArrayMapped[i]["avalue"].includes(modifiedArrayMapped[i]["appvalue"])) {
                    modifiedArrayMapped.splice(i, 1)
                    isThereAnyChange = true
                    break
                } else {
                    modifiedArrayMapped.splice(i, 1)
                    isThereAnyChange = false
                    return false
                }
            }
        }
        return isThereAnyChange
    }

    saveData(send) {
        let amountRequest = 0
        let amountSpeechRequest = 0
        let speechResult = false
        let dataResult = false
        if (send == false) {
            let contractBody = []
            let speechBody = []
            this.dataResponse.map((element, index) => {
                if (element.id != undefined) {
                    let body = {}
                    body = {
                        "id": element.id,
                        "appvalue": element.appvalue,
                        "webvalue": element.webvalue
                    }
                    contractBody.push(body)
                }
            })
            this.dataSpeechResponse.map((element, index) => {
                let body = {}
                body = {
                    "id": element.id,
                    "appvalue": element.appvalue
                }
                speechBody.push(body)
            })
            let bodyRequest = {
                "contractList": contractBody,
                "speechList": speechBody
            }
            this.contractService.updateReviewList(bodyRequest).subscribe(response => {
                this.dataWasSaved = false
                this.messageSuccessful = true
                this.dataSaved = true
                this.messageResponse = "Se guardó correctamente"
                setTimeout(() => {
                    this.messageSuccessful = false
                    //this.router.navigate(['/listcontract'])
                }, 3000)
                this.originalResponse = []
                this.originalSpeechResponse = []
                this.dataResponse.map(element => {
                    if (element != undefined) {

                        let newResponse = {
                            "id": element.id,
                            "auxiliar": element.auxiliar,
                            "campo": element.campo,
                            "category": element.category,
                            "avalue": element.appvalue,
                            "wvalue": element.webvalue,
                            "public": element.public,
                            "type": element.type
                        }
                        this.originalResponse.push(newResponse)
                    }

                })

                this.dataSpeechResponse.map(element => {
                    if (element != undefined) {
                        console.log("element Spec" + JSON.stringify(element))
                        let newResponse = {
                            "id": element.id,
                            "auxiliar": element.auxiliar,
                            "campo": element.campo,
                            "category": element.category,
                            "avalue": element.appvalue,
                            "wvalue": element.webvalue,
                            "public": element.public,
                            "type": element.type
                        }
                        this.originalSpeechResponse.push(newResponse)
                    }
                }
                )
                /*amountRequest++
                if (amountRequest == this.dataResponse.length) {
                    dataResult = true
                } else {
                    dataResult = false
                }*/
            })

        } else if (send == true) {
            let contractBodySave = []
            let speechBodySave = []
            this.dataResponse.map((element, index) => {
                if (element.id != undefined) {
                    let body = {}
                    body = {
                        "id": element.id,
                        "appvalue": element.appvalue,
                        "webvalue": element.webvalue
                    }
                    contractBodySave.push(body)
                }
            })
            this.dataSpeechResponse.map((element, index) => {
                let body = {}
                body = {
                    "id": element.id,
                    "appvalue": element.appvalue
                }
                speechBodySave.push(body)
            })
            let bodyRequestSave = {
                "contractList": contractBodySave,
                "speechList": speechBodySave
            }
            this.contractService.updateReviewList(bodyRequestSave).subscribe(response => {
                /*amountRequest++
                if (amountRequest == this.dataResponse.length) {
                    dataResult = true
                } else {
                    dataResult = false
                }*/
            })
            let contractBody = []
            let speechBody = []
            this.dataResponse.map((element, index) => {
                if (element.id != undefined) {
                    let body = {}
                    body = {
                        "id": element.id + 1,
                        "appvalue": element.appvalue,
                        "webvalue": element.webvalue
                    }
                    contractBody.push(body)
                }
            })
            this.dataSpeechResponse.map((element, index) => {
                let body = {}
                body = {
                    "id": element.id + 1,
                    "appvalue": element.appvalue
                }
                speechBody.push(body)
            })
            let bodyRequest = {
                "contractList": contractBody,
                "speechList": speechBody
            }
            this.contractService.updateReviewList(bodyRequest).subscribe(response => {
                speechResult = true
                this.dataWasSaved = false
                this.messageSuccessful = true
                this.dataPublished = true
                this.messageResponse = "Se publicó correctamente"
                setTimeout(() => {
                    this.messageSuccessful = false
                }, 3000)
                this.originalResponse = []
                this.originalSpeechResponse = []
                this.dataResponse.map(element => {
                    let newResponse = {
                        "id": element.id,
                        "auxiliar": element.auxiliar,
                        "campo": element.campo,
                        "category": element.category,
                        "avalue": element.appvalue,
                        "wvalue": element.webvalue,
                        "public": element.public,
                        "type": element.type
                    }
                    this.originalResponse.push(newResponse)
                })

                this.dataSpeechResponse.map(element => {
                    let newResponse = {
                        "id": element.id,
                        "auxiliar": element.auxiliar,
                        "campo": element.campo,
                        "category": element.category,
                        "avalue": element.appvalue,
                        "wvalue": element.webvalue,
                        "public": element.public,
                        "type": element.type
                    }
                    this.originalSpeechResponse.push(newResponse)
                }
                )
            })
        }
    }

    checkStatusForm = (originalArray, modifiedArray, type) => {
        let isthereanychange = false
        let attributeO = ""
        let attributeM = ""
        if (type == "W") {
            attributeO = 'wvalue'
            attributeM = 'webvalue'
        } else {
            attributeO = 'avalue'
            attributeM = 'appvalue'
        }
        for (let i = 0; i < originalArray.length; i++) {
            if (originalArray[i][attributeO].includes(modifiedArray[i][attributeM])) {
                isthereanychange = true
            } else {
                return false
            }
        }
        return isthereanychange
    }

    validateForms(web, mWeb, typeWeb, speech, mSpeech, app, mApp, typeApp) {

        let validateFormWeb = this.isTheFormOk(
            this.getMainWordsInContract(web, typeWeb),
            this.getMainWordsInContract(mWeb, typeWeb)
        )

        let validateSpeechForm = this.isTheFormOk(
            this.getMainWordsInContract(speech, typeApp),
            this.getMainWordsInContract(mSpeech, typeApp)
        )

        let validateForm = this.isTheFormOk(
            this.getMainWordsInContract(app, typeApp),
            this.getMainWordsInContract(mApp, typeApp)
        )
        if (validateFormWeb && validateSpeechForm && validateForm) {
            return true
        } else {
            return false
        }
    }
}

