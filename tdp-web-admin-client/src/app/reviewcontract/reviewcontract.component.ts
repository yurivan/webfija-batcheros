import { Component, OnInit } from '@angular/core';
import { ContractService } from '../services/contract.service';
import { Observable } from 'rxjs/Rx';
import { ContractParameters } from '../listcontract/listcontract.component';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router/src/router';

@Component({
    moduleId: 'reviewcontract',
    selector: 'reviewcontract',
    templateUrl: 'reviewcontract.template.html'
})
export class ReviewContractComponent implements OnInit {
    dataResponse
    dataSpeechResponse
    elementResponse: ContractParameters
    dataHasArrived: boolean
    title: string

    constructor(private contractService: ContractService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        //this.dataResponse = []
        this.elementResponse = new ContractParameters()
        this.dataResponse = new Array()
        this.dataSpeechResponse = new Array()
        this.dataHasArrived = false
    }
    ngOnInit() {
        this.route.params
            .subscribe(params => {
                //consolelog("type " + params["id"])
                this.getContract(params["id"], true)
                this.getContractSpeech(params["id"], true)
                switch (params["id"]) {
                    case 'A':
                        this.title = "ALTA PURA"
                        break
                    case 'M':
                        this.title = "MIGRACIÓN"
                        break
                    case 'S':
                        this.title = "SVA"
                        break
                }
            }
            )
    }

    getContract(type: string, option: boolean) {
        this.contractService.getContractList(type, option).subscribe(data => {
            data.map(element => {
                this.elementResponse = element
                this.dataResponse.push(this.elementResponse)
                this.orderArray(this.dataResponse)
            })
        }, error => {

        }, () => {
            this.dataHasArrived = true
        })
    }
    getContractSpeech(type: string, option: boolean) {
        this.contractService.getSpeechList(type, option).subscribe(data => {
            data.map(element => {
                this.elementResponse = element
                this.dataSpeechResponse.push(this.elementResponse)
                this.orderArray(this.dataSpeechResponse)
            })
        }, error => {

        }, () => {
            //consolelog("count " + this.dataResponse.length)
            //consolelog("length " + this.dataResponse[0]['id'])
        })
    }

    orderArray = list => (
        list.sort((a, b) => {
            let valueA = a.auxiliar,
                valueB = b.auxiliar
            if (valueA < valueB) return -1;
            if (valueA > valueB) return 1;
            return 0;
        })
    )

    returnList() {
        this.router.navigate(['/listcontract']);
    }
}