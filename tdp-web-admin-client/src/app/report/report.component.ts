import { Component, OnInit } from '@angular/core';
import { ReportService } from '../services/report.service'

declare var $: any;

//declare var jQuery:any;
//var $ = require("../../../node_modules/materialize-css/node_modules/jquery/dist/jquery.js");

@Component({
    moduleId: 'report',
    selector: 'my-report',
    templateUrl: 'report.template.html'
})
export class ReportComponent implements OnInit {

    private initDate: string = "";
    private finDate: string = "";
    private dateInitFormat: string = "";
    private dateFinFormat: string = "";
    private campaign: string = '';
    private campaigns = [];
    private readonlyFinDate: boolean = true;
    private showAlertDateInit: boolean = false;

    constructor(private reportService: ReportService) { }

    showModalFiltro() {
        $('.modal').modal();
        $('#modal1').modal('open');
    }

    selectInitDate() {

        if (this.initDate) {
            this.showAlertDateInit = false;
            this.readonlyFinDate = false;
        } else {
            this.finDate = "";
            //this.showAlertDateInit = true;
            this.readonlyFinDate = true;
        }
    }

    downloadVentas() {
        this.showAlertDateInit = false;

        if (!this.initDate) {
            this.showAlertDateInit = true;
            this.dateInitFormat = null;
        } else {
            /*var date1 = new Date(this.initDate+'');
            var d = date1.getDate();  
            var m = date1.getMonth() + 1;
                var yyyy =   date1.getFullYear();*/

            let yyyy = this.initDate.substring(0, 4);
            let mm = this.initDate.substring(5, 7);
            let dd = this.initDate.substring(8, 10);
            this.dateInitFormat = dd + "-" + mm + "-" + yyyy;
            console.log('fecha init ' + this.dateInitFormat);

        }

        if (!this.finDate) {
            this.dateFinFormat = null;
        } else {
            /* var date2 = new Date(this.initDate);  
         var m = date2.getMonth() + 1;
         var d = date2.getDate() ; 
             var yyyy =   date2.getFullYear();
             */

            let yyyy = this.finDate.substring(0, 4);
            let mm = this.finDate.substring(5, 7);
            let dd = this.finDate.substring(8, 10);


            this.dateFinFormat = dd + "-" + mm + "-" + yyyy;
            console.log('fecha fin ' + this.dateFinFormat);
        }

        if (!this.campaign) {
            this.campaign = null;
        }
        console.log("campaign " + this.campaign);

        //var _this = this;
        if (!this.showAlertDateInit) {
            this.reportService.load(this.dateInitFormat, this.dateFinFormat, this.campaign);

        }
        //despues de llamar al servicio verifico si se selecciono una campaña para asignarle un valor vacio
        if (!this.campaign) {
            this.campaign = '';
        }
        //this.campaign = '';    
    };



    ngOnInit() {
        let _this = this;
        this.reportService.loadCampaign().subscribe(function (datos) {
            _this.campaigns = datos.data;

            console.log(_this.campaigns);
        }, function (err) { });

        console.log(this.campaigns, this);
    }
}
