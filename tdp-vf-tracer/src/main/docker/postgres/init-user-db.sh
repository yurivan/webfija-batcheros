#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER menuadmin PASSWORD 'admin';
    CREATE DATABASE menu_data OWNER menuadmin;
    GRANT ALL PRIVILEGES ON DATABASE menu_data TO menuadmin;
EOSQL