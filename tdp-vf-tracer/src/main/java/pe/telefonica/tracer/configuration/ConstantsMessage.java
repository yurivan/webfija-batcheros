package pe.telefonica.tracer.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ConstantsMessage {

	public static String SOLICITADO_WEB_EXITO = "trz_msg_web_sol_exi";
	public static String SOLICITADO_SMS_EXITO = "trz_msg_sms_sol_exi";

	public static String REGISTRADO_WEB_EXITO = "trz_msg_web_reg_exi";
	public static String REGISTRADO_SMS_EXITO = "trz_msg_sms_reg_exi";
	public static String REGISTRADO_WEB_RETRASO = "trz_msg_web_reg_ret";
	public static String REGISTRADO_SMS_RETRASO = "trz_msg_sms_reg_ret";
	public static String REGISTRADO_WEB_CAIDA = "trz_msg_web_reg_cai";
	public static String REGISTRADO_SMS_CAIDA = "trz_msg_sms_reg_cai";

	public static String TECNICO_WEB_EXITO = "trz_msg_web_tec_exi";
	public static String TECNICO_SMS_EXITO = "trz_msg_sms_tec_exi";
	public static String TECNICO_WEB_NOM = "trz_msg_web_tec_nom";
	public static String TECNICO_SMS_NOM = "trz_msg_sms_tec_nom";
	public static String TECNICO_WEB_OTH = "trz_msg_web_tec_oth";
	public static String TECNICO_SMS_OTH = "trz_msg_sms_tec_oth";
	public static String TECNICO_WEB_HOU = "trz_msg_web_tec_hou";
	public static String TECNICO_SMS_HOU = "trz_msg_sms_tec_hou";
	public static String TECNICO_WEB_HOU_BK = "trz_msg_web_tec_hou_bk";
	public static String TECNICO_SMS_HOU_BK = "trz_msg_sms_tec_hou_bk";
	public static String TECNICO_WEB_RETRASO = "trz_msg_web_tec_ret";
	public static String TECNICO_SMS_RETRASO = "trz_msg_sms_tec_ret";
	public static String TECNICO_WEB_CAIDA = "trz_msg_web_tec_cai";
	public static String TECNICO_SMS_CAIDA = "trz_msg_sms_tec_cai";

	public static String INSTALADO_WEB_EXITO = "trz_msg_web_ins_exi";
	public static String INSTALADO_SMS_EXITO = "trz_msg_sms_ins_exi";
	public static String INSTALADO_WEB_RETRASO = "trz_msg_web_ins_ret";
	public static String INSTALADO_SMS_RETRASO = "trz_msg_sms_ins_ret";
	public static String INSTALADO_WEB_RETRASO_DEV_COM = "trz_msg_web_ins_ret_dc";
	public static String INSTALADO_WEB_RETRASO_REAGENDADO = "trz_msg_web_ins_ret_reagend";
	public static String INSTALADO_SMS_RETRASO_DEV_COM = "trz_msg_sms_ins_ret_dc";
	public static String INSTALADO_WEB_CAIDA_COMERCIAL_CLIENTE = "trz_msg_web_ins_cai_com_cli";
	public static String INSTALADO_SMS_CAIDA_COMERCIAL_CLIENTE = "trz_msg_sms_ins_cai_com_cli";
	public static String INSTALADO_WEB_CAIDA_COMERCIAL_NO_CONTACTO = "trz_msg_web_ins_cai_com_nocon";
	public static String INSTALADO_SMS_CAIDA_COMERCIAL_NO_CONTACTO = "trz_msg_sms_ins_cai_com_nocon";
	public static String INSTALADO_WEB_CAIDA_TECNICA = "trz_msg_web_ins_cai_tec";
	public static String INSTALADO_SMS_CAIDA_TECNICA = "trz_msg_sms_ins_cai_tec";

}