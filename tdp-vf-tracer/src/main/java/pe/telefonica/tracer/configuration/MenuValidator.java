package pe.telefonica.tracer.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import pe.telefonica.tracer.business.impl.ParametersServiceImpl;

import javax.inject.Inject;
import javax.validation.Validator;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Javier on 19/06/2017.
 */
@Configuration
public class MenuValidator {

    @Bean
    public Validator localSpringProductValidator(){
        return new LocalValidatorFactoryBean();
    }

}
