package pe.telefonica.tracer.common.mailing;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Felix on 19/06/2017.
 */
public class EmailResponseBody {
    @JsonProperty("Code")
    private String Code;
    @JsonProperty("Message")
    private String Message;

    @JsonProperty("AggregatesSendEmail")
    private Integer AggregatesSendEmail;

    @JsonProperty("TotalSendEmail")
    private Integer TotalSendEmail;

    @JsonProperty("GroupId")
    private Integer GroupId;

    @JsonProperty("Timestamp")
    private String Timestamp;

    @JsonProperty("Token")
    private String Token;

    @JsonProperty("Result")
    private Boolean Result;


    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String Message) {
        this.Message = Message;
    }

    public Integer getAggregatesSendEmail() {
        return AggregatesSendEmail;
    }

    public void setAggregatesSendEmail(Integer AggregatesSendEmail) {
        this.AggregatesSendEmail = AggregatesSendEmail;
    }

    public Integer getTotalSendEmail() {
        return TotalSendEmail;
    }

    public void setTotalSendEmail(Integer TotalSendEmail) {
        this.TotalSendEmail = TotalSendEmail;
    }

    public Integer getGroupId() {
        return GroupId;
    }

    public void setGroupId(Integer GroupId) {
        this.GroupId = GroupId;
    }

    public String getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(String Timestamp) {
        this.Timestamp = Timestamp;
    }

    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

    public Boolean getResult() {
        return Result;
    }

    public void setResult(Boolean Result) {
        this.Result = Result;
    }

}
