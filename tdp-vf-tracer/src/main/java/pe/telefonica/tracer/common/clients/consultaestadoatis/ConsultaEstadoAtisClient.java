package pe.telefonica.tracer.common.clients.consultaestadoatis;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.telefonica.tracer.common.clients.AbstractClient;
import pe.telefonica.tracer.common.clients.ClientConfig;
import pe.telefonica.tracer.common.clients.dto.ApiHeaderConfig;
import pe.telefonica.tracer.common.clients.dto.ApiResponse;
import pe.telefonica.tracer.common.util.Constants;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class ConsultaEstadoAtisClient extends AbstractClient<DuplicateApiAtisRequestBody, DuplicateApiAtisResponseBody> {

	@Autowired
	private ApiHeaderConfig apiHeaderConfig;

	public ConsultaEstadoAtisClient(ClientConfig config) {
		super(config);
	}

	@PostConstruct
	public void ConsultaEstadoAtisClientSetConfig() {
		ClientConfig config = new ClientConfig.ClientConfigBuilder()
				.setUrl(apiHeaderConfig.getAtisConsultaEstadoUri())
				.setApiId(apiHeaderConfig.getAtisConsultarEstadoApiId())
				.setApiSecret(apiHeaderConfig.getAtisConsultarEstadoApiSecret())
				.setOperation(Constants.API_REQUEST_HEADER_OPERATION_STATUS_ATIS)
				.setDestination(Constants.API_REQUEST_HEADER_DESTINATION_ATIS)
				.build();

		this.config = config;
	}

	@Override
	protected String getServiceCode() {
		return SERVICE_CODE_CONSULTA_ESTADO_ATIS;
	}

	@Override
	protected ApiResponse<DuplicateApiAtisResponseBody> getResponse(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(json,
				new TypeReference<ApiResponse<DuplicateApiAtisResponseBody>>() {
				});
	}

}
