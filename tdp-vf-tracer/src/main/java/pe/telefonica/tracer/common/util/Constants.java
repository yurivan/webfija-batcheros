package pe.telefonica.tracer.common.util;

public class Constants {

    private Constants() {
    }




    public static final String PARAMETERS_DOMAIN_TRANSLATE = "TRANSLATE";
    public static final String PARAMETERS_CATEGORY_CMSR = "CMSR";
    public static final String PARAMETERS_CATEGORY_ATIS = "ATIS";
    public static final String COMMERCIAL_OPE_SVA = "SVAS";
    public static final String TYPE_SVA = "S";


    //xxx


    /*
        public static final String API_REQUEST_HEADER_OPERATION_RENIEC = "ConsultaRENIEC";
        public static final String API_REQUEST_HEADER_OPERATION_AVVE = "Consulta FFTT a AAVE";
        public static final String API_REQUEST_HEADER_OPERATION_CMS = "Consulta Parque en CMS";
        public static final String API_REQUEST_HEADER_OPERATION_STATUS_CMS = "Consulta Estado en CMS";
        public static final String API_REQUEST_HEADER_OPERATION_ATIS = "Consulta Parque en ATIS";
        public static final String API_REQUEST_HEADER_OPERATION_STATUS_ATIS = "Consulta Estado en ATIS";
        public static final String API_REQUEST_HEADER_OPERATION_EXPERTO = "Consulta FFTT a EXPERTO";



        public static final String API_REQUEST_HEADER_DESTINATION_CMS = "ES:BUS:CMS:CMS";
        public static final String API_REQUEST_HEADER_DESTINATION_RENIEC = "ES:BUS:RENIEC:RENIEC";
        public static final String API_REQUEST_HEADER_DESTINATION_AVVE = "ES:BUS:AVVE:AVVE";
        public static final String API_REQUEST_HEADER_DESTINATION_ATIS = "ES:BUS:ATIS:ATIS";
        public static final String API_REQUEST_HEADER_DESTINATION_EXPERTO = "ES:BUS:EXPERTO:EXPERTO"; */
    public static final String API_REQUEST_HEADER_DESTINATION_UPFRONT = "ES:BUS:UPFRONT:UPFRONT";
    public static final String API_REQUEST_HEADER_DESTINATION_HDEC = "ES:BUS:HDEC:HDEC";
    public static final String API_REQUEST_HEADER_OPERATION_HDEC = "Registro en HDEC";


    //>>>>>>> 3117f16ae5e8b0e53f542c74d43d1531b8ec03d2
    public static final String API_REQUEST_HEADER_OPERATION_UPFRONT = "Consulta UPFRONT";
    public static final String API_REQUEST_HEADER_COUNTRY = "PE";
    public static final String API_REQUEST_HEADER_LANG = "es";
    public static final String API_REQUEST_HEADER_ENTITY = "TDP";
    public static final String API_REQUEST_HEADER_SYSTEM = "APPVENTASFIJA";
    public static final String API_REQUEST_HEADER_SUBSYSTEM = "APPVENTASFIJA";
    public static final String API_REQUEST_HEADER_ORIGINATOR = "PE:TDP:APPVENTASFIJA:APPVENTASFIJA";
    public static final String API_REQUEST_HEADER_SENDER = "OracleServiceBus";
    public static final String API_REQUEST_HEADER_USER_ID = "AppUser";
    public static final String API_REQUEST_HEADER_WS_ID = "BluemixEveris";
    public static final String API_REQUEST_HEADER_WS_IP = "10.11.11.11";
//<<<<<<< HEAD


    //=======
//>>>>>>> 3117f16ae5e8b0e53f542c74d43d1531b8ec03d2
    public static final String API_REQUEST_HEADER_OPERATION = "Envia mensaje de texto";
    public static final String API_REQUEST_HEADER_DESTINATION = "ES:BUS:SMS:SMS";
    public static final String API_REQUEST_HEADER_EXEC_ID = "550e8400-e29b-41d4-a716-446655440003";
    //public static final String API_REQUEST_HEADER_TIMESTAMP = "2016-09-08T06:28:00.233+01:00";
    public static final String API_REQUEST_HEADER_MSG_TYPE = "REQUEST";
//<<<<<<< HEAD

    public static final String DELIMITER_VERTICAL_BAR = "||";
    public static final String EMPTY = "";
    public static final int MAXIMUM_PASSWORD_STORAGE = 5;
    //public static final String RESPONSE ="RESPONSE";

    public static final String SALE_STATUS_DOWN = "CAIDA";
    public static final String SALE_STATUS_FOLLOW_PENDING = "SEGUIMIENTO/PENDIENTE";
    public static final String SALE_STATUS_PENDING = "PENDIENTE";
    public static final String SALE_STATUS_FOLLOW = "SEGUIMIENTO";
    public static final String SALE_STATUS_REGISTER = "REGISTRADO";


//product

    public static final String RESPONSE_ACTION = "CLIENTE NUEVO";
    //public static final String DOES_NOT_APPLY = "NO APLICA";
    public static final String API_REQUEST_HEADER_OPERATION_AVVE = "Consulta FFTT a AAVE";
    public static final String API_REQUEST_HEADER_OPERATION_EXPERTO = "Consulta FFTT a EXPERTO";
    public static final String API_REQUEST_HEADER_DESTINATION_AVVE = "ES:BUS:AVVE:AVVE";
    public static final String API_REQUEST_HEADER_DESTINATION_EXPERTO = "ES:BUS:EXPERTO:EXPERTO";


    //customer



    public static final String API_REQUEST_HEADER_OPERATION_STATUS_ATIS = "Consulta Estado en ATIS";
    public static final String API_REQUEST_HEADER_OPERATION_ATIS = "Consulta Parque en ATIS";
    public static final String API_REQUEST_HEADER_DESTINATION_ATIS = "ES:BUS:ATIS:ATIS";
    public static final String API_REQUEST_HEADER_DESTINATION_CMS = "ES:BUS:CMS:CMS";
    public static final String API_REQUEST_HEADER_OPERATION_STATUS_CMS = "Consulta Estado en CMS";
    public static final String API_REQUEST_HEADER_OPERATION_CMS = "Consulta Parque en CMS";
    public static final String API_REQUEST_HEADER_OPERATION_RENIEC = "ConsultaRENIEC";
    public static final String API_REQUEST_HEADER_DESTINATION_RENIEC = "ES:BUS:RENIEC:RENIEC";


    public static final String DOES_NOT_APPLY = "NO APLICA";

    public static final String OFFERS_API_EXPERTO_REQUEST_DOCUMENT_TYPE_DNI = "DNI";


    //xxx

    public static final String API_ID_DUMMY = "a2384c02-7629-4dd3-a56e-4688d6c9e0d1";
    public static final String API_SECRET_DUMMY = "R0qY3dY1uJ7xL7tO2wO8hF5pO7pV7pA2nK2oP0jD8iM6jK8uX3";

    public static final String RESPONSE = "RESPONSE";


    public static final String DUPLICATE_SVA_DECO = "PUNTO ADICIONAL";
    public static final String DUPLICATE_STATUS_DOWN = "CAIDA";
    public static final String DUPLICATE_STATUS_PENDING = "PENDIENTE";
    public static final String DUPLICATE_STATUS_FOLLOW = "SEGUIMIENTO";
    public static final String DUPLICATE_STATUS_IN_PROCESS = "EN PROCESO";
    public static final String DUPLICATE_STATUS_EMPTY = "";
    public static final String DUPLICATE_STATUS_JOINED = "INGRESADO";
    public static final String DUPLICATE_COMMERCIAL_OPERATION_SVA = "SVAS";
    public static final String DUPLICATE_COMMERCIAL_OPERATION_AP = "ALTA PURA";
    public static final String DUPLICATE_COMMERCIAL_OPERATION_AC_BA = "ALTA COMPONENTE BA";
    public static final String DUPLICATE_COMMERCIAL_OPERATION_AC_TV = "ALTA COMPONENTE TV";
    public static final String DUPLICATE_COMMERCIAL_OPERATION_MIGRACION = "MIGRACION";

    public static final int DUPLICATE_ORDER_CODE_CMS_1 = 7;
    public static final int DUPLICATE_ORDER_CODE_CMS_2 = 8;
    public static final int DUPLICATE_ORDER_CODE_ATIS = 9;

    /*public static final String DUPLICATE_CMS_NEW = "01";
    public static final String DUPLICATE_CMS_REJECTED = "02";
    public static final String DUPLICATE_CMS_CANCELED = "03";
    public static final String DUPLICATE_CMS_SUSPENDED = "04";
    public static final String DUPLICATE_CMS_ACTIVE = "05";*/

    public static final String DUPLICATE_CMS_NEW = "01";
    public static final String DUPLICATE_CMS_REJECTED = "02";
    public static final String DUPLICATE_CMS_PENDING = "03";
    public static final String DUPLICATE_CMS_COMPLETED = "04";
    public static final String DUPLICATE_CMS_NO_PROCEDE = "05";

    public static final String DUPLICATE_ATIS_TERMINATE = "TE";
    public static final String DUPLICATE_ATIS_FINALIZED = "FI";
    public static final String DUPLICATE_ATIS_PENDING = "PE";
    public static final String DUPLICATE_ATIS_PENDING_APROVE = "PD";
    public static final String DUPLICATE_ATIS_CANCELED = "CG";
    public static final String DUPLICATE_ATIS_PENDING_VALIDATE = "PV";
    public static final String DUPLICATE_ATIS_CONFIGURED = "CE";
    public static final String DUPLICATE_ATIS_ERROR = "XX";

    public static final String DUPLICATE_ATIS_ACTIVE = "IC";
    public static final String DUPLICATE_ATIS_SUS_DUE = "SD";
    public static final String DUPLICATE_ATIS_SUS_APC = "SA";
    public static final String DUPLICATE_ATIS_SUS = "SU";


    public static final String MY_SQL_SP_NAME = "[SP_NAME]";
    public static final String MY_SQL_PARAMETER = "[PARAMETER]";
    public static final String MY_SQL_COL = "[COL]";
    public static final String MY_SQL_TABLE = "[TABLE]";
    public static final String MY_SQL_WHERE = "[WHERE]";

    public static final String GENDER_MAS = "M";
    public static final String GENDER_FEM = "F";

    public static final String API_ERROR_RESPONSE_CODE = "-1";

    public static final String API_REQUEST_HEADER_OPERATION_GIS = "ObtenerScore";
    public static final String API_REQUEST_HEADER_DESTINATION_GIS = "PE:TDP:WebPorta:WebPorta";

    public static final String API_REQUEST_HEADER_OPERATION_GD = "GeocodificarDireccion";
    public static final String API_REQUEST_HEADER_DESTINATION_GD = "PE:TDP:CMS:IVR";
}
