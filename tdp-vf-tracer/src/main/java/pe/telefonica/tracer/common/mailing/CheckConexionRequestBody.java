package pe.telefonica.tracer.common.mailing;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by Felix on 19/06/2017.
 */
@JsonPropertyOrder({ "Token" })
public class CheckConexionRequestBody {
    private String Token;

    @JsonProperty("Token")
    public String getToken() {
        return this.Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

}
