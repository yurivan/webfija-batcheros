package pe.telefonica.tracer.common.clients;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.telefonica.tracer.common.clients.dto.ApiRequest;
import pe.telefonica.tracer.common.clients.dto.ApiRequestHeader;
import pe.telefonica.tracer.common.clients.dto.ApiResponse;
import pe.telefonica.tracer.common.connection.Api;
import pe.telefonica.tracer.common.util.Constants;
import pe.telefonica.tracer.persistence.TraceabilityExternalLogDB;
import pe.telefonica.tracer.persistence.repository.TraceabilityExternalLogRepository;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.Locale;


public abstract class AbstractClient<T, R> {
    public static final String SERVICE_CODE_CONSULTA_ESTADO_CMS = "ESTADOCMS";
    public static final String SERVICE_CODE_CONSULTA_ESTADO_ATIS = "ESTADOATIS";

    @Autowired
    private TraceabilityExternalLogRepository traceabilityExternalLogRepository;

    protected ClientConfig config;

    protected AbstractClient(ClientConfig config) {
        super();
        this.config = config;
    }

    public ClientResult<ApiResponse<R>> post(T body, String codigo_pedido) {
        ClientResult<ApiResponse<R>> result = new ClientResult<>();

        try {

            String json = doRequest(body,codigo_pedido);

            ApiResponse<R> apiResponse = getResponse(json);

            result.setResult(apiResponse);
            result.setSuccess(true);
        } catch (Exception e) {

            //log ERROR
            String messageError = "Exception -> " + e.toString() + " : " + e.getMessage();
            TraceabilityExternalLogDB externalLog = new TraceabilityExternalLogDB(codigo_pedido, "", messageError);
            traceabilityExternalLogRepository.save(externalLog);

            result.setSuccess(false);
        }
        return result;
    }


    protected String doRequest(T body, String codigo_pedido) throws URISyntaxException {
        URI uri = new URI(config.getUrl());
        String jsonSMS = Api.jerseyPOST(uri, getApiRequest(body), config.getApiId(), config.getApiSecret(), codigo_pedido);
        ////////log OK
        return jsonSMS;
    }

    protected ApiRequest<T> getApiRequest(T body) {
        ApiRequest<T> apiRequest = new ApiRequest<>();
        ApiRequestHeader apiRequestHeader = getHeader(config.getOperation(), config.getDestination());
        apiRequest.setHeaderIn(apiRequestHeader);
        apiRequest.setBodyIn(body);

        String jsonRequest;
        try {
            jsonRequest = new ObjectMapper().writeValueAsString(apiRequest);
        } catch (JsonProcessingException e1) {
            jsonRequest = "N/A";
        }

        //logggggggg

        return apiRequest;
    }

    protected ApiRequestHeader getHeader(String API_REQUEST_HEADER_OPERATION, String API_REQUEST_HEADER_DESTINATION) {
        ApiRequestHeader apiRequestHeader = new ApiRequestHeader();
        apiRequestHeader.setCountry(Constants.API_REQUEST_HEADER_COUNTRY);
        apiRequestHeader.setLang(Constants.API_REQUEST_HEADER_LANG);
        apiRequestHeader.setEntity(Constants.API_REQUEST_HEADER_ENTITY);
        apiRequestHeader.setSystem(Constants.API_REQUEST_HEADER_SYSTEM);
        apiRequestHeader.setSubsystem(Constants.API_REQUEST_HEADER_SUBSYSTEM);
        apiRequestHeader.setOriginator(Constants.API_REQUEST_HEADER_ORIGINATOR);
        apiRequestHeader.setSender(Constants.API_REQUEST_HEADER_SENDER);
        apiRequestHeader.setUserId(Constants.API_REQUEST_HEADER_USER_ID);
        apiRequestHeader.setWsId(Constants.API_REQUEST_HEADER_WS_ID);
        apiRequestHeader.setWsIp(Constants.API_REQUEST_HEADER_WS_IP);
        apiRequestHeader.setOperation(API_REQUEST_HEADER_OPERATION);
        apiRequestHeader.setDestination(API_REQUEST_HEADER_DESTINATION);
        apiRequestHeader.setExecId(Constants.API_REQUEST_HEADER_EXEC_ID);
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", new Locale("es", "PE"));
        apiRequestHeader.setTimestamp(sdf.format(new Date()) + "-05:00");
        apiRequestHeader.setMsgType(Constants.API_REQUEST_HEADER_MSG_TYPE);

        return apiRequestHeader;
    }

    protected abstract String getServiceCode();

    protected abstract ApiResponse<R> getResponse(String json) throws Exception;
}
