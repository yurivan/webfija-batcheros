package pe.telefonica.tracer.common.clients.consultaestadocms;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.telefonica.tracer.common.clients.AbstractClient;
import pe.telefonica.tracer.common.clients.ClientConfig;
import pe.telefonica.tracer.common.clients.dto.ApiHeaderConfig;
import pe.telefonica.tracer.common.clients.dto.ApiResponse;
import pe.telefonica.tracer.common.util.Constants;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class ConsultaEstadoCmsClient extends AbstractClient<DuplicateApiCmsRequestBody, DuplicateApiCmsResponseBody> {

	@Autowired
	private ApiHeaderConfig apiHeaderConfig;

	@Override
	protected String getServiceCode() {
		return SERVICE_CODE_CONSULTA_ESTADO_CMS;
	}

	@Override
	protected ApiResponse<DuplicateApiCmsResponseBody> getResponse(String json)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		
		ApiResponse<DuplicateApiCmsResponseBody> objCms = mapper.readValue(json, new TypeReference<ApiResponse<DuplicateApiCmsResponseBody>>() {});
		return objCms;
	}

	public ConsultaEstadoCmsClient(ClientConfig config) {
		super(config);
	}

	@PostConstruct
	public void ConsultaEstadoCmsClientSetConfig() {
		ClientConfig config = new ClientConfig.ClientConfigBuilder()
				.setUrl(apiHeaderConfig.getCmsConsultaEstadoUri())
				.setApiId(apiHeaderConfig.getCmsConsultarEstadoApiId())
				.setApiSecret(apiHeaderConfig.getCmsConsultarEstadoApiSecret())
				.setOperation(Constants.API_REQUEST_HEADER_OPERATION_STATUS_CMS)
				.setDestination(Constants.API_REQUEST_HEADER_DESTINATION_CMS)
				.build();

		this.config = config;
	}

}
