package pe.telefonica.tracer.common.clients.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "exceptionCategory", "exceptionCode","exceptionMsg" ,"exceptionDetail","exceptionSeverity","appDetail"})
public class ApiResponseBodyFault {
  @JsonInclude(Include.NON_NULL)
  private String exceptionCategory;
  @JsonInclude(Include.NON_NULL)
  private int exceptionCode;
  @JsonInclude(Include.NON_NULL)
  private String exceptionMsg;
  @JsonInclude(Include.NON_NULL)
  private String exceptionDetail;
  @JsonInclude(Include.NON_NULL)
  private String exceptionSeverity;
  @JsonInclude(Include.NON_NULL)
  private ApiResponseBodyFaultDetail appDetail;

  public String getExceptionCategory() {
    return exceptionCategory;
  }

  public void setExceptionCategory(String exceptionCategory) {
    this.exceptionCategory = exceptionCategory;
  }

  public int getExceptionCode() {
    return exceptionCode;
  }

  public void setExceptionCode(int exceptionCode) {
    this.exceptionCode = exceptionCode;
  }

  public String getExceptionMsg() {
    return exceptionMsg;
  }

  public void setExceptionMsg(String exceptionMsg) {
    this.exceptionMsg = exceptionMsg;
  }

  public String getExceptionDetail() {
    return exceptionDetail;
  }

  public void setExceptionDetail(String exceptionDetail) {
    this.exceptionDetail = exceptionDetail;
  }

  public String getExceptionSeverity() {
    return exceptionSeverity;
  }

  public void setExceptionSeverity(String exceptionSeverity) {
    this.exceptionSeverity = exceptionSeverity;
  }

  public ApiResponseBodyFaultDetail getAppDetail() {
    return appDetail;
  }

  public void setAppDetail(ApiResponseBodyFaultDetail appDetail) {
    this.appDetail = appDetail;
  }


}
