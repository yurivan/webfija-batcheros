package pe.telefonica.tracer.common.connection;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;
import pe.telefonica.tracer.persistence.repository.TraceabilityExternalLogRepository;
import pe.telefonica.tracer.persistence.TraceabilityExternalLogDB;

import javax.annotation.PostConstruct;
import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Date;
import java.util.Map;

/**
 * 
 * @author felix
 *
 */
@Component
public class Api {
	private static final Logger _log = LoggerFactory.getLogger(Api.class);

	private static TraceabilityExternalLogRepository traceabilityExternalLogRepository;

	@Autowired
	private TraceabilityExternalLogRepository traceabilityExternalLogRepositoryAutowired;

	@PostConstruct
	private void init() {
		traceabilityExternalLogRepository = this.traceabilityExternalLogRepositoryAutowired;
	}


	private Api() {

	}

	/**
	 * 
	 * @param uri
	 * @return
	 */
	public static String jerseyGET(URI uri) {
		Date timeStamp = new Date();
		ClientConfig config = new ClientConfig();

		Client client = ClientBuilder.newClient(config);
		WebTarget target = client.target(UriBuilder.fromUri(uri).build());

		String response = target.request().accept(MediaType.TEXT_PLAIN).get(String.class).toString();
		_log.info(response);
		return response;
	}
	
	public static String jerseyPOST (URI uri, Object apiRequest, Map<String, String> headers) {
		Date timeStamp = new Date();
		ObjectMapper mapper = new ObjectMapper();
		String requestJson;
		try {
			requestJson = mapper.writeValueAsString(apiRequest);
		} catch (JsonProcessingException e) {}
		Client client = ClientBuilder.newClient();
		client.register(new LoggingFeature());
		WebTarget target = client.target(UriBuilder.fromUri(uri).build());

		String json = "";

		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		for (Map.Entry<String, String> entry : headers.entrySet()) {
			invocationBuilder.header(entry.getKey(), entry.getValue());
		}
		invocationBuilder.header("content-type", "application/json");
		Response response = invocationBuilder.post(Entity.entity(apiRequest, MediaType.APPLICATION_JSON), Response.class);
		json = response.readEntity(String.class);

		return json;
	}

	/**
	 * 
	 * @param uri
	 * @param apiRequest
	 * @return
	 */
	public static String jerseyPOST(URI uri, Object apiRequest, String apiId, String apiSecret, String codigo_pedido) {
		Date timeStamp = new Date();
		ObjectMapper mapper = new ObjectMapper();
		String requestJson = "";
		try {
			requestJson = mapper.writeValueAsString(apiRequest);
		} catch (JsonProcessingException e) {}
		Client client = ClientBuilder.newClient();
		client.register(new LoggingFeature());
		WebTarget target = client.target(UriBuilder.fromUri(uri).build());

		String json = "";

		Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
		invocationBuilder.header("x-ibm-client-id", apiId);
		invocationBuilder.header("x-ibm-client-secret", apiSecret);
		invocationBuilder.header("content-type", "application/json");
		Response response = invocationBuilder.post(Entity.entity(apiRequest, MediaType.APPLICATION_JSON), Response.class);
		json = response.readEntity(String.class);

		_log.info("codigo_pedido -> " + codigo_pedido);
		_log.info(requestJson);
		_log.info(json);

		TraceabilityExternalLogDB externalLog = new TraceabilityExternalLogDB(codigo_pedido, requestJson, json);
		traceabilityExternalLogRepository.save(externalLog);

		return json;
	}

}
