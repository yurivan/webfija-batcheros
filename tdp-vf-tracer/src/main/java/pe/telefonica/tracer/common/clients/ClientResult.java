package pe.telefonica.tracer.common.clients;


public class ClientResult<T> {
	private T result;
	private boolean success;
	private ClientException e;

	public ClientResult() {
		super();
		this.success = false;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public ClientException getE() {
		return e;
	}

	public void setE(ClientException e) {
		this.e = e;
	}
}
