package pe.telefonica.tracer.common.clients.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ApiHeaderConfig {
	public static final String MSG_TYPE_REQUEST = "REQUEST";
	public static final String MSG_TYPE_RESPONSE = "RESPONSE";
	public static final String MSG_TYPE_ERROR = "ERROR";

	@Value("${tdp.api.cms.consultaestado.uri}")
	private String cmsConsultaEstadoUri;

	@Value("${tdp.api.atis.consultaestado.uri}")
	private String atisConsultaEstadoUri;
	
	@Value("${tdp.api.cms.consultaestado.apiid}")
	private String cmsConsultarEstadoApiId;
	@Value("${tdp.api.cms.consultaestado.apisecret}")
	private String cmsConsultarEstadoApiSecret;
	
	@Value("${tdp.api.atis.consultaestado.apiid}")
	private String atisConsultarEstadoApiId;
	@Value("${tdp.api.atis.consultaestado.apisecret}")
	private String atisConsultarEstadoApiSecret;



	public String getCmsConsultaEstadoUri() {
		return cmsConsultaEstadoUri;
	}

	public void setCmsConsultaEstadoUri(String cmsConsultaEstadoUri) {
		this.cmsConsultaEstadoUri = cmsConsultaEstadoUri;
	}

	public String getAtisConsultaEstadoUri() {
		return atisConsultaEstadoUri;
	}

	public void setAtisConsultaEstadoUri(String atisConsultaEstadoUri) {
		this.atisConsultaEstadoUri = atisConsultaEstadoUri;
	}

	public String getCmsConsultarEstadoApiId() {
		return cmsConsultarEstadoApiId;
	}

	public void setCmsConsultarEstadoApiId(String cmsConsultarEstadoApiId) {
		this.cmsConsultarEstadoApiId = cmsConsultarEstadoApiId;
	}

	public String getCmsConsultarEstadoApiSecret() {
		return cmsConsultarEstadoApiSecret;
	}

	public void setCmsConsultarEstadoApiSecret(String cmsConsultarEstadoApiSecret) {
		this.cmsConsultarEstadoApiSecret = cmsConsultarEstadoApiSecret;
	}

	public String getAtisConsultarEstadoApiId() {
		return atisConsultarEstadoApiId;
	}

	public void setAtisConsultarEstadoApiId(String atisConsultarEstadoApiId) {
		this.atisConsultarEstadoApiId = atisConsultarEstadoApiId;
	}

	public String getAtisConsultarEstadoApiSecret() {
		return atisConsultarEstadoApiSecret;
	}

	public void setAtisConsultarEstadoApiSecret(String atisConsultarEstadoApiSecret) {
		this.atisConsultarEstadoApiSecret = atisConsultarEstadoApiSecret;
	}
	
}