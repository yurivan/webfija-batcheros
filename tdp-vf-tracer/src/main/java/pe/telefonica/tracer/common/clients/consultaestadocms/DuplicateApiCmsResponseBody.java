package pe.telefonica.tracer.common.clients.consultaestadocms;

import com.fasterxml.jackson.annotation.JsonProperty;
import pe.telefonica.tracer.common.clients.dto.ApiResponseBodyFault;

public class DuplicateApiCmsResponseBody {

	@JsonProperty("EstadoRequerimiento")
	private String EstadoRequerimiento;
	@JsonProperty("ClientException")
	private ApiResponseBodyFault ClientException;
	@JsonProperty("ServerException")
	private ApiResponseBodyFault ServerException;

	public String getEstadoRequerimiento() {
		return EstadoRequerimiento;
	}

	public void setEstadoRequerimiento(String estadoRequerimiento) {
		EstadoRequerimiento = estadoRequerimiento;
	}

	public ApiResponseBodyFault getClientException() {
		return ClientException;
	}

	public void setClientException(ApiResponseBodyFault clientException) {
		ClientException = clientException;
	}

	public ApiResponseBodyFault getServerException() {
		return ServerException;
	}

	public void setServerException(ApiResponseBodyFault serverException) {
		ServerException = serverException;
	}

}
