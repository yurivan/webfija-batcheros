package pe.telefonica.tracer.common.mailing;

/**
 * Created by Felix on 19/06/2017.
 */
public class ClientResult<T> {
    //private ServicioCallEvent event;
    private T result;
    private boolean success;
    //private ClientException e;

    public ClientResult() {
        super();
        this.success = false;
    }

        /*public ServicioCallEvent getEvent() {
            return event;
        }

        public void setEvent(ServicioCallEvent event) {
            this.event = event;
        }*/

    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

        /*public ClientException getE() {
            return e;
        }

        public void setE(ClientException e) {
            this.e = e;
        }*/
}
