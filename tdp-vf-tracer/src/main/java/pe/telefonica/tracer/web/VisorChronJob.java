package pe.telefonica.tracer.web;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.scheduling.annotation.Scheduled;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.impl.ValidateDropServiceImpl;
import pe.telefonica.tracer.common.mailing.EmailResponseBody;
import pe.telefonica.tracer.common.mailing.Mailing;
import pe.telefonica.tracer.configuration.Constants;
import pe.telefonica.tracer.persistence.*;
import pe.telefonica.tracer.persistence.repository.*;
import pe.telefonica.tracer.persistence.repository.impl.VisorRepositoryImpl;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class VisorChronJob {

    private static final Logger _log = LoggerFactory.getLogger(VisorChronJob.class);

    @Autowired
    VisorRepository repository;

    @Autowired
    VisorRepositoryImpl visorRepositoryImpl;

    //@Autowired
    //ComercialDevRepository comercialDevRepository;

    //@Autowired
    //TechnicalDevRepository technicalDevRepository;

    @Autowired
    ValidateDropServiceImpl validateDropService;

    @Autowired
    PedidoCaidaRepository pedidoCaidaRepository;

    @Autowired
    PedidoEstadoRepository pedidoEstadoRepository;

    @Autowired
    MessageServiceRepository messageServiceRepository;

    @Autowired
    Constants constants;

    @Autowired
    Mailing mailing;

    String emailToken = "";

    List<String> emailsToSend;

    Integer solicitadoEnable = 0;
    Integer registradoEnable = 0;
    Integer asignacionTecnicaEnable = 0;
    Integer instaladoEnable = 0;
    Boolean statusSetted = false;

    private void detectStatusEnables(){
        List<MessageServiceDB> enables = messageServiceRepository.getStatusEnables();
        for(MessageServiceDB obj : enables){
            if(obj != null && obj.getServicekey() != null){
                if(obj.getServicekey().equalsIgnoreCase("trz_param_sol")){ solicitadoEnable = obj.getResponsecode();continue;}
                if(obj.getServicekey().equalsIgnoreCase("trz_param_reg")){ registradoEnable = obj.getResponsecode();continue;}
                if(obj.getServicekey().equalsIgnoreCase("trz_param_tecasig")){ asignacionTecnicaEnable = obj.getResponsecode();continue;}
                if(obj.getServicekey().equalsIgnoreCase("trz_param_inst")){ instaladoEnable = obj.getResponsecode();continue;}
            }
        }
        statusSetted = true;
    }

    @PostConstruct
    private void setReceivers(){
        emailsToSend = getReceivers();
    }

    //@Scheduled(cron = "*/15 * * * * *")
    //@Scheduled(cron = "${tdp.traceability.timer.mailing}")
    public void checkEmailSending()  throws Exception{
        if(!constants.getEnableMailing()){
            _log.info("MAILING se encuentra DESACTIVADO...");
            return;
        }
        _log.info("Executing MAILING...");

        if(!statusSetted) detectStatusEnables();

        if(solicitadoEnable == 1){
            //getSolicitadoExito();
        }else _log.info("Estado SOLICITADO se encuentra DESACTIVADO...");

        if(registradoEnable == 1){
            //getRegistradoExito();
            //getRegistradoRetraso();
            getRegistradoCaida();
        }else _log.info("Estado REGISTRADO se encuentra DESACTIVADO...");

        if(asignacionTecnicaEnable == 1){
            //getAsignacionTecnicaExito();
            //getAsignacionTecnicaRetraso();
            //getAsignacionTecnicaCaida();
        }else _log.info("Estado ASIGNACIÓN TÉCNICA se encuentra DESACTIVADO...");

        if(instaladoEnable == 1){
            //getInstaladoExito();
            //getInstaladoRetraso();
            //getCaidaDevueltaComercialNoContacto();
            //getCaidaDevueltaComercialAPedido();
            //getCaidaDevueltaTecnica();
        }else _log.info("Estado INSTALADO se encuentra DESACTIVADO...");

    }

    //@Scheduled(cron = "*/15 * * * * *")
    @Scheduled(cron = "${tdp.traceability.timer.delayasign}")
    public void validateDropsByDelayInAsign()  throws Exception{
        if(!constants.getEnableDelayAsign()){
            _log.info("CAIDAS POR RETRASO EN ASIGNACIÓN se encuentra DESACTIVADO...");
            return;
        }
        _log.info("Executing CAIDAS POR RETRASO EN ASIGNACIÓN...");

        validateDropService.validateDropsByDelayInAsign();
        _log.info("End CAIDAS POR RETRASO EN ASIGNACIÓN...");
    }


    //@Scheduled(cron = "*/30 * * * * *")
    @Scheduled(cron = "${tdp.traceability.timer.dropdtdc}")
    public void validateDropsDTC()  throws Exception{
        if(!constants.getEnableDropDTDC()){
            _log.info("CAIDAS POR DTDC se encuentra DESACTIVADO...");
            return;
        }
        _log.info("Executing CAIDAS POR DTDC...");

        validateDropService.validateDropsDTDC();
        _log.info("End CAIDAS POR DTDC...");
    }

    //@Scheduled(cron = "*/30 * * * * *")
    @Scheduled(cron = "${tdp.traceability.timer.dc48}")
    public void validateDC48()  throws Exception{
        if(!constants.getEnableDC48()){
            _log.info("RETRASO POR DC48 se encuentra DESACTIVADO...");
            return;
        }
        _log.info("Executing RETRASO POR DC48...");

        getDC48();
        _log.info("End RETRASO POR DC48...");
    }

    private void getSolicitadoExito() throws Exception{
        String emailHTML = "";
        try {
            String path = "/static/template/template_solicitado.html";
            //File file = new File(getClass().getResource(path).getFile());

            File file = new File(new URI(getClass().getResource(path).toString()));

            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (IOException e) {
            //e.printStackTrace();
            _log.error(e.getMessage());
            throw new Exception(e.getMessage());
        }

        List<Object[]> objList = repository.getSolicitadoAlta();
        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);

        /*if("".equals(filter)){
            objList = repository.getSolicitadoAlta();
        }else if("X".equals(filter)){
            _log.error("Segmentación mal configurada.");
            return;
        }else{
            objList = visorRepositoryImpl.getSolicitadoAltaFiltrado();
        }*/

        /*List<Object[]> objListX = visorRepositoryImpl.getSolicitadoAltaFiltrado();
        List<VisorDB> itemsX = toEntityList(objListX);

        List<Object[]> objList = repository.getSolicitadoAlta();*/
        //////////////////List<Object[]> objList = repository.getSolicitadoAM();
        //////////////////List<Object[]> objList2 = repository.getSolicitadoS();


        //////////////////List<VisorDB> items2 = toEntityList(objList2);

        //List<VisorDB> items = repository.getSolicitadoAM(new PageRequest(0, 3));

        //List<Object[]> items = repository.getSolicitadoAM(new PageRequest(0, 3));

        //List<VisorDB> items2 = repository.getSolicitadoS();

        ///////////////////items.addAll(items2);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String phone1 = (obj.getPhone1() != null && !obj.getPhone1().equals("")) ? obj.getPhone1() : "No ingresado";
            String phone2 = (obj.getPhone2() != null && !obj.getPhone2().equals("")) ? obj.getPhone2() : "No ingresado";

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%productName%:", nombreProducto);
            customEmailHTML = customEmailHTML.replace(":%clientPhone1%:", phone1);
            customEmailHTML = customEmailHTML.replace(":%clientPhone2%:", phone2);

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectSolicitado());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Solicitado): " + counterMail);


        /*Integer rowAffected = repository.setMailRequestedAsDone(ids);
        _log.info("rowAffected: " + rowAffected);*/

        Integer rowAffected = setMailRequestedAsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private void getRegistradoExito(){
        String emailHTML = "";
        try {
            String path = "/static/template/template_registrado.html";
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getRegistradoAlta();
        //////////List<Object[]> objList = repository.getRegistradoAM();
        //////////List<Object[]> objList2 = repository.getRegistradoS();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);
        /////////List<VisorDB> items2 = toEntityList(objList2);

        ////////items.addAll(items2);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String phone1 = (obj.getPhone1() != null && !obj.getPhone1().equals("")) ? obj.getPhone1() : "No ingresado";
            String phone2 = (obj.getPhone2() != null && !obj.getPhone2().equals("")) ? obj.getPhone2() : "No ingresado";

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%productName%:", nombreProducto);
            customEmailHTML = customEmailHTML.replace(":%requestCode%:", obj.getCodigo_pedido());
            customEmailHTML = customEmailHTML.replace(":%clientPhone1%:", phone1);
            customEmailHTML = customEmailHTML.replace(":%clientPhone2%:", phone2);

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectRegistrado());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Registrado): " + counterMail);

        Integer rowAffected = setMailRegisteredAsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private void getAsignacionTecnicaExito(){
        String emailHTML = "";
        try {
            String path = "/static/template/template_asignacion.html";
            //File file = new File(getClass().getResource(path).getFile());
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getAsignacionTecnicaAlta();
        ////////List<Object[]> objList = repository.getAsignacionTecnica();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String phone1 = (obj.getPhone1() != null && !obj.getPhone1().equals("")) ? obj.getPhone1() : "No ingresado";
            String phone2 = (obj.getPhone2() != null && !obj.getPhone2().equals("")) ? obj.getPhone2() : "No ingresado";

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%requestCode%:", obj.getCodigo_pedido());
            customEmailHTML = customEmailHTML.replace(":%clientPhone1%:", phone1);
            customEmailHTML = customEmailHTML.replace(":%clientPhone2%:", phone2);

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectAsignacionTecnica());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Asignación técnica): " + counterMail);

        Integer rowAffected = setMailTechAssignedAsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private void getInstaladoExito(){
        String emailHTML = "";
        try {
            String path = "/static/template/template_instalado.html";
            //File file = new File(getClass().getResource(path).getFile());
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getInstaladoAlta();
        /////////List<Object[]> objList = repository.getInstalado();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%productName%:", nombreProducto);

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectInstalado());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Instalado): " + counterMail);

        Integer rowAffected = setMailInstalledAsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private void getCaidaDevueltaComercialNoContacto() throws Exception{
        String emailHTML = "";
        try {
            String path = "/static/template/template_devuelta_comercial_no_contacto.html";
            //File file = new File(getClass().getResource(path).getFile());
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getDevueltaComercialNoContactoAlta();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%requestCode%:", obj.getCodigo_pedido());
            customEmailHTML = customEmailHTML.replace(":%productName%:", obj.getNombre_producto());

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectDevueltaComercial());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Devuelta comercial no contacto): " + counterMail);

        Integer rowAffected = setMailComercialDevNoContactoAsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private void getCaidaDevueltaComercialAPedido() throws Exception{
        String emailHTML = "";
        try {
            String path = "/static/template/template_devuelta_comercial_a_pedido.html";
            //File file = new File(getClass().getResource(path).getFile());
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getDevueltaComercialAPedidoAlta();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%requestCode%:", obj.getCodigo_pedido());
            customEmailHTML = customEmailHTML.replace(":%productName%:", obj.getNombre_producto());

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectDevueltaComercial());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Devuelta comercial a pedido): " + counterMail);

        Integer rowAffected = setMailComercialDevAPedidoAsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private void getCaidaDevueltaTecnica() throws Exception{
        String emailHTML = "";
        try {
            String path = "/static/template/template_devuelta_tecnica.html";
            //File file = new File(getClass().getResource(path).getFile());
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getDevueltaTecnicaAlta();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%requestCode%:", obj.getCodigo_pedido());
            customEmailHTML = customEmailHTML.replace(":%productName%:", obj.getNombre_producto());

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectDevueltaTecnica());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Devuelta tecnica): " + counterMail);

        Integer rowAffected = setMailTechnicalDevAsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private void getDC48() throws Exception{
        String emailHTML = "";
        try {
            String path = "/static/template/template_dc_48.html";
            //File file = new File(getClass().getResource(path).getFile());
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Object[]> objList = pedidoCaidaRepository.getDC48();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);

        String ids = "";
        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%requestCode%:", obj.getCodigo_pedido());

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectDC48());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (DC48): " + counterMail);

        Integer rowAffected = setMailDC48AsDone(items);
        _log.info("Registros actualizados: " + rowAffected);
    }

    private void getRegistradoRetraso(){
        String emailHTML = "";
        try {
            String path = "/static/template/template_registrado_retraso.html";
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getRegistradoRetrasoAlta();
        //////////List<Object[]> objList = repository.getRegistradoAM();
        //////////List<Object[]> objList2 = repository.getRegistradoS();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);
        /////////List<VisorDB> items2 = toEntityList(objList2);

        ////////items.addAll(items2);

        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String phone1 = (obj.getPhone1() != null && !obj.getPhone1().equals("")) ? obj.getPhone1() : "No ingresado";
            String phone2 = (obj.getPhone2() != null && !obj.getPhone2().equals("")) ? obj.getPhone2() : "No ingresado";

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%productName%:", nombreProducto);
            customEmailHTML = customEmailHTML.replace(":%clientPhone1%:", phone1);
            customEmailHTML = customEmailHTML.replace(":%clientPhone2%:", phone2);

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectRegistradoRetraso());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Registrado Retraso): " + counterMail);

        Integer rowAffected = setMailRegisteredRetAsDone(items);
        _log.info("Registros actualizados (Registrado Retraso): " + rowAffected);
    }

    private void getAsignacionTecnicaRetraso(){
        String emailHTML = "";
        try {
            String path = "/static/template/template_asignacion_retraso.html";
            //File file = new File(getClass().getResource(path).getFile());
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getAsignacionTecnicaRetrasoAlta();
        ////////List<Object[]> objList = repository.getAsignacionTecnica();

        List<VisorDB> items = toEntityList(objList);
        items = validarRetraso(items);
        items = mailingFilterBy(items);

        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%productName%:", nombreProducto);
            customEmailHTML = customEmailHTML.replace(":%requestCode%:", obj.getCodigo_pedido());

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectAsignacionTecnicaRetraso());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Asignación técnica Retraso): " + counterMail);

        Integer rowAffected = setMailTechAssignedRetAsDone(items);
        _log.info("Registros actualizados (Asignación técnica Retraso): " + rowAffected);
    }

    private void getInstaladoRetraso(){
        String emailHTML = "";
        try {
            String path = "/static/template/template_instalado_retraso.html";
            //File file = new File(getClass().getResource(path).getFile());
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getInstaladoRetrasoAlta();
        /////////List<Object[]> objList = repository.getInstalado();

        List<VisorDB> items = toEntityList(objList);
        items = validarRetraso(items);
        items = mailingFilterBy(items);

        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%productName%:", nombreProducto);
            customEmailHTML = customEmailHTML.replace(":%requestCode%:", obj.getCodigo_pedido());

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectInstaladoRetraso());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Instalado Retraso): " + counterMail);

        Integer rowAffected = setMailInstalledRetAsDone(items);
        _log.info("Registros actualizados (Instalado Retraso): " + rowAffected);
    }

    private void getRegistradoCaida(){
        String emailHTML = "";
        try {
            String path = "/static/template/template_registrado_caida.html";
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getRegistradoCaidaAlta();
        //////////List<Object[]> objList = repository.getRegistradoAM();
        //////////List<Object[]> objList2 = repository.getRegistradoS();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);
        /////////List<VisorDB> items2 = toEntityList(objList2);

        ////////items.addAll(items2);

        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%clientName%:", obj.getCliente());
            customEmailHTML = customEmailHTML.replace(":%productName%:", nombreProducto);

            String DDMM = "";
            String fechaGrabacion = obj.getFecha_grabacion();
            if(fechaGrabacion != null && !"".equalsIgnoreCase(fechaGrabacion)){
                try{
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date = sdf.parse(fechaGrabacion);
                    SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM");
                    DDMM = sdf2.format(date);
                }catch (Exception ex){}
            }

            customEmailHTML = customEmailHTML.replace(":%ddmm%:", DDMM);

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectRegistradoCaida());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Registrado Caida): " + counterMail);

        Integer rowAffected = setMailRegisteredDropAsDone(items);
        _log.info("Registros actualizados (Registrado Caida): " + rowAffected);
    }

    private void getAsignacionTecnicaCaida(){
        String emailHTML = "";
        try {
            String path = "/static/template/template_asignacion_caida.html";
            //File file = new File(getClass().getResource(path).getFile());
            File file = new File(new URI(getClass().getResource(path).toString()));
            Document htmlFile = Jsoup.parse(file, "UTF-8");
            emailHTML = htmlFile.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Object[]> objList = repository.getEquipoTecnicoCaidaAlta();
        ////////List<Object[]> objList = repository.getAsignacionTecnica();

        List<VisorDB> items = toEntityList(objList);
        items = mailingFilterBy(items);

        int counterMail = 0;
        int counter = 0;
        for(VisorDB obj : items){

            //if(obj.getEmail() == null || obj.getEmail().equals("")) continue;

            String nombreProducto = "SVAS".equals(obj.getOperacion_comercial()) ? generateSVAtext(obj) : obj.getNombre_producto();

            String customEmailHTML = emailHTML.replace(":%productName%:", nombreProducto);
            customEmailHTML = customEmailHTML.replace(":%requestCode%:", obj.getCodigo_pedido());

            String email = obj.getEmail();

            //for(String email : emailsToSend){

                if(email != null && !email.equals("")){
                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectAsignacionTecnicaCaida());

                    _log.info("Result ->");
                    _log.info("Email: " + email);
                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;
                }else{
                    _log.info("La venta no cuenta con CORREO");
                }

            //}

            counter++;
        }

        _log.info("TOTAL DE CORREOS ENVIADOS (Asignación técnica Caida): " + counterMail);

        Integer rowAffected = setMailTechAssignedDropAsDone(items);
        _log.info("Registros actualizados (Asignación técnica Caida): " + rowAffected);
    }

    private String generateSVAtext(VisorDB obj){
        String nombreProducto = "<br> Servicio(s) adicional(es) <br>";
        String[] splitted = obj.getNombre_producto().split("\\|");
        for(String element : splitted){
            if(element != null && !element.trim().equals("")) nombreProducto = nombreProducto + element.trim() + "<br>";
        }
        return nombreProducto;
    }

    private List<VisorDB> toEntityList(List<Object[]> objList){
        List<VisorDB> list = new ArrayList<>();
        for(Object[] obj : objList){
            String idVisor = (obj[0] != null) ? obj[0].toString() : "";
            String cliente = (obj[1] != null) ? obj[1].toString() : "";
            String email = (obj[2] != null) ? obj[2].toString() : "";
            String nombreProducto = (obj[3] != null) ? obj[3].toString() : "";
            String phone1 = (obj[4] != null) ? obj[4].toString() : "";
            String phone2 = (obj[5] != null) ? obj[5].toString() : "";
            String estadoSolicitud = (obj[6] != null) ? obj[6].toString() : "";
            String operacionComercial = (obj[7] != null) ? obj[7].toString() : "";
            String departamento = (obj[8] != null) ? obj[8].toString() : "";
            String provincia = (obj[9] != null) ? obj[9].toString() : "";
            String distrito = (obj[10] != null) ? obj[10].toString() : "";
            String codigoPedido = (obj[11] != null) ? obj[11].toString() : "";

            VisorDB visorItem = null;
            if(obj.length > 12){
                Integer flgSolicitadoEmail = 0;
                try{flgSolicitadoEmail = (obj[12] != null) ? Integer.parseInt(obj[12].toString()) : 0;}catch (Exception ex){}
                Integer flgRegistradoEmail = 0;
                try{flgRegistradoEmail = (obj[13] != null) ? Integer.parseInt(obj[13].toString()) : 0;}catch (Exception ex){}
                Integer flgEquipoTecnicoEmail = 0;
                try{flgEquipoTecnicoEmail = (obj[14] != null) ? Integer.parseInt(obj[14].toString()) : 0;}catch (Exception ex){}
                Integer flgInstaladoEmail = 0;
                try{flgInstaladoEmail = (obj[15] != null) ? Integer.parseInt(obj[15].toString()) : 0;}catch (Exception ex){}
                Integer flgRegistradoRetEmail = 0;
                try{flgRegistradoRetEmail = (obj[16] != null) ? Integer.parseInt(obj[16].toString()) : 0;}catch (Exception ex){}
                Integer flgEquipoTecnicoRetEmail = 0;
                try{flgEquipoTecnicoRetEmail = (obj[17] != null) ? Integer.parseInt(obj[17].toString()) : 0;}catch (Exception ex){}
                Integer flgInstaladoRetEmail = 0;
                try{flgInstaladoRetEmail = (obj[18] != null) ? Integer.parseInt(obj[18].toString()) : 0;}catch (Exception ex){}
                Integer flgRegistradoCaidaEmail = 0;
                try{flgRegistradoCaidaEmail = (obj[19] != null) ? Integer.parseInt(obj[19].toString()) : 0;}catch (Exception ex){}
                visorItem = new VisorDB(idVisor,cliente,email,nombreProducto,phone1,phone2,estadoSolicitud,operacionComercial,
                        departamento,provincia,distrito,codigoPedido,flgSolicitadoEmail,flgRegistradoEmail,
                        flgEquipoTecnicoEmail,flgInstaladoEmail,flgRegistradoRetEmail,flgEquipoTecnicoRetEmail,
                        flgInstaladoRetEmail,flgRegistradoCaidaEmail);
            }else{
                visorItem = new VisorDB(idVisor,cliente,email,nombreProducto,phone1,phone2,estadoSolicitud,operacionComercial,
                        departamento,provincia,distrito,codigoPedido);
            }

            if(obj.length > 20){
                String fechaGrabacion = (obj[20] != null) ? obj[20].toString() : "";
                visorItem.setFecha_grabacion(fechaGrabacion);
            }

            list.add(visorItem);
        }
        return list;
    }

    private Integer setMailRequestedAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_solicitado_email(1);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailRegisteredAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_registrado_email(1);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailTechAssignedAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_equipo_tecnico_email(1);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailInstalledAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_instalado_email(1);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    /*private Integer setMailComercialDevAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            List<String> codigoPedidoList = new ArrayList<>();
            for(VisorDB obj : items){
                codigoPedidoList.add(obj.getCodigo_pedido());
            }

            if(codigoPedidoList.size() == 0) return 0;

            List<ComercialDevDB> comercialList = comercialDevRepository.getComercialDev(codigoPedidoList);
            for(ComercialDevDB comercialDevDB : comercialList){
                comercialDevDB.setFlg_email(1);
                comercialDevRepository.save(comercialDevDB);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }*/

    private Integer setMailComercialDevNoContactoAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            List<String> codigoPedidoList = new ArrayList<>();
            for(VisorDB obj : items){
                codigoPedidoList.add(obj.getCodigo_pedido());
            }

            if(codigoPedidoList.size() == 0) return 0;

            List<PedidoCaidaDB> comercialList = pedidoCaidaRepository.getCaidasComercialNoContacto(codigoPedidoList);
            for(PedidoCaidaDB comercialDevDB : comercialList){
                comercialDevDB.setFlg_email(1);
                pedidoCaidaRepository.save(comercialDevDB);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailComercialDevAPedidoAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            List<String> codigoPedidoList = new ArrayList<>();
            for(VisorDB obj : items){
                codigoPedidoList.add(obj.getCodigo_pedido());
            }

            if(codigoPedidoList.size() == 0) return 0;

            List<PedidoCaidaDB> comercialList = pedidoCaidaRepository.getCaidasComercialAPedido(codigoPedidoList);
            for(PedidoCaidaDB comercialDevDB : comercialList){
                comercialDevDB.setFlg_email(1);
                pedidoCaidaRepository.save(comercialDevDB);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    /*private Integer setMailTechnicalDevAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            List<String> codigoPedidoList = new ArrayList<>();
            for(VisorDB obj : items){
                codigoPedidoList.add(obj.getCodigo_pedido());
            }

            if(codigoPedidoList.size() == 0) return 0;

            List<TechnicalDevDB> technicalList = technicalDevRepository.getTechnicalDev(codigoPedidoList);
            for(TechnicalDevDB technicalDevDB : technicalList){
                technicalDevDB.setFlg_email(1);
                technicalDevRepository.save(technicalDevDB);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }*/

    private Integer setMailTechnicalDevAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            List<String> codigoPedidoList = new ArrayList<>();
            for(VisorDB obj : items){
                codigoPedidoList.add(obj.getCodigo_pedido());
            }

            if(codigoPedidoList.size() == 0) return 0;

            List<PedidoCaidaDB> technicalList = pedidoCaidaRepository.getCaidasTecnica(codigoPedidoList);
            for(PedidoCaidaDB technicalDevDB : technicalList){
                technicalDevDB.setFlg_email(1);
                pedidoCaidaRepository.save(technicalDevDB);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailDC48AsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            if(items.size() == 0) return 0;

            for(VisorDB visorDB : items){
                PedidoCaidaDB pedidoCaidaDB = new PedidoCaidaDB();
                pedidoCaidaDB.setCodigo_pedido(visorDB.getCodigo_pedido());
                pedidoCaidaDB.setTipo_caida("RET_DC48");
                pedidoCaidaDB.setInsert_update(new Timestamp(new Date().getTime()));
                pedidoCaidaDB.setFlg_email(1);
                pedidoCaidaRepository.save(pedidoCaidaDB);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailRegisteredRetAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_registrado_ret_email(1);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailTechAssignedRetAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_equipo_tecnico_ret_email(1);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailInstalledRetAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_instalado_ret_email(1);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailRegisteredDropAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            for(VisorDB item : items){
                item.setFlg_registrado_caida_email(1);
                repository.save(item);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }

    private Integer setMailTechAssignedDropAsDone(List<VisorDB> items){
        Integer counter = 0;
        try{
            List<String> codigoPedidoList = new ArrayList<>();
            for(VisorDB obj : items){
                codigoPedidoList.add(obj.getCodigo_pedido());
            }

            if(codigoPedidoList.size() == 0) return 0;

            List<PedidoCaidaDB> comercialList = pedidoCaidaRepository.getCaidasRetrasoAsignacion(codigoPedidoList);
            for(PedidoCaidaDB asignacionRetrasoDB : comercialList){
                asignacionRetrasoDB.setFlg_email(1);
                pedidoCaidaRepository.save(asignacionRetrasoDB);
                counter++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return counter;
    }


    private List<String> getReceivers(){
        String[] splitted = constants.getReceivers().split("\\|");
        //String[] splitted = "alvin.puma@smart-home.com.pe|diana.ch.n.26@gmail.com".split("\\|");
        List<String> emailsToSend = new ArrayList<>();
        for(int i = 0; i < splitted.length; i++){
            emailsToSend.add(splitted[i]);
        }
        return emailsToSend;
    }

    /*private String mailingFilterBy(){
        String filter = "";
        if(constants.getEnableFilterByDistrict()){

            String districts = constants.getDistrictsInFilter();
            if(districts != null){
                String[] places = districts.split("\\|");
                boolean first = true;
                for(String place : places){
                    String[] items = place.split("\\-");
                    if(items.length == 3){

                        String depart = items[0];
                        String provin = items[1];
                        String distri = items[2];

                        filter = filter + "("+
                                " v.departamento = '" + depart + "' and " +
                                " v.provincia = '" + provin + "' and " +
                                " v.distrito = '" + distri + "'" +
                                " )";

                        if(first){
                            filter = filter + " OR ";
                            first = false;
                        }

                    }else{
                        filter="X";
                        break;
                    }
                }
            }

        }
        return filter;
    }*/

    private List<VisorDB> validarRetraso(List<VisorDB> items){
        List<String> codigoPedidoList = new ArrayList<>();
        for (VisorDB obj : items){
            if(obj.getCodigo_pedido() != null) codigoPedidoList.add(obj.getCodigo_pedido());
        }

        List<PedidoEstadoDB> pedidoEstadoDB = pedidoEstadoRepository.getPedidoEstadoList(codigoPedidoList);
        Map<String,Timestamp> codigoPedidoMap = new HashMap<>();
        for (PedidoEstadoDB obj : pedidoEstadoDB){
            codigoPedidoMap.put(obj.getPedido_codigo(),obj.getAuditoria_create());
        }

        List<VisorDB> filteredList = new ArrayList<>();

        for (VisorDB val : items) {
            _log.info("Codigo pedido encontrado: " + val.getCodigo_pedido());
            boolean inDelay = false;
            if (val.getCodigo_pedido().length() == 9) {
                inDelay = validateDropService.validateDelayAtis(val.getCodigo_pedido(),codigoPedidoMap);
            } else if (val.getCodigo_pedido().length() == 8 || val.getCodigo_pedido().length() == 7) {
                inDelay = validateDropService.validateDelayCms(val.getCodigo_pedido(),codigoPedidoMap);
            }

            if(inDelay){
                filteredList.add(val);
            }

        }
        return filteredList;
    }

    private List<VisorDB> mailingFilterBy(List<VisorDB> items){
        List<VisorDB> itemsResponse = new ArrayList<>();
        if(constants.getEnableFilterByDistrict()){

            String districts = constants.getDistrictsInFilter();
            if(districts != null){
                String[] places = districts.split("\\|");

                for(VisorDB item : items){

                    String department = replaceCharsLower(item.getDepartamento());
                    String province = replaceCharsLower(item.getProvincia());
                    String district = replaceCharsLower(item.getDistrito());

                    for(String place : places){
                        String[] obj = place.split("\\-");
                        if(obj.length == 3){
                            String departmentFilter = replaceCharsLower(obj[0]);
                            String provinceFilter = replaceCharsLower(obj[1]);
                            String districtFilter = replaceCharsLower(obj[2]);

                            if(department.equals(departmentFilter) && province.equals(provinceFilter)
                                    && district.equals(districtFilter)){
                                itemsResponse.add(item);
                            }
                        }else{
                            itemsResponse = null;
                            break;
                        }
                    }
                }

            }
        }else itemsResponse = items;
        return itemsResponse;
    }

    private String replaceCharsLower(String txt){
        if(txt == null) return "XYZ";
        txt = txt.toLowerCase().replace("á","a").replace("é","e")
                .replace("í","i").replace("ó","o").replace("ú","u");
        return txt;
    }

   // @Scheduled(cron = "*0 * * * * *")
    public void validateFechaAsig()  throws Exception{

        _log.info("Executing validateFechaAsig...");


         validateDropService.validateDateAsig();

        _log.info("End validateFechaAsig...");
    }


}
