package pe.telefonica.tracer.web;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.web.bind.annotation.*;
import pe.telefonica.tracer.business.impl.PublicCustomerServiceImpl;
import pe.telefonica.tracer.business.impl.StatusToaServiceImpl;
import pe.telefonica.tracer.business.impl.TraceabilityAccessServiceImpl;
import pe.telefonica.tracer.business.impl.VisorServiceImpl;
import pe.telefonica.tracer.common.mailing.EmailResponseBody;
import pe.telefonica.tracer.common.mailing.Mailing;
import pe.telefonica.tracer.configuration.Constants;
import pe.telefonica.tracer.model.*;
import pe.telefonica.tracer.persistence.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/api/v1/visor")
public class OrderWSController {

    private static final Logger _log = LoggerFactory.getLogger(OrderWSController.class);

    @Autowired
    private VisorServiceImpl visorService;

    @Autowired
    private StatusToaServiceImpl statusToaService;

    @Autowired
    private PublicCustomerServiceImpl publicCustomerService;

    @Autowired
    private TraceabilityAccessServiceImpl traceabilityAccessService;

    @Autowired
    Constants constants;

    @Autowired
    Mailing mailing;

    List<String> emailsToSend;

    @RequestMapping(value="/orderbydocument",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity getOrderByDocument(@RequestBody VisorRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start getOrderByDocument");
        Response<List<VisorByDocResponse>> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){
                    if(request.getDocument() == null || "".equals(request.getDocument().trim())){
                        response = setMessageError("El número de documento ingresado es inválido.", access.getId());
                    }else{

                        if(request.getDocument().trim().length() < 8 || request.getDocument().trim().length() > 12){
                            response = setMessageError("El número de documento ingresado es inválido.", access.getId());
                        }else{
                            List<VisorByDocDB> items = visorService.getVisorByDoc(request.getDocument().trim());

                            if(items != null && items.size() > 0){

                                List<VisorByDocResponse> result = visorService.toVisorByDocResponseList(items);

                                response.setResponseData(result);
                                response.setResponseMessage("Success");
                                response.setResponseCode("1");
                                traceabilityAccessService.saveLog("Success!", access.getId());
                            }else{
                                response = setMessageError("El número de documento ingresado no cuenta con pedidos.", access.getId());
                            }
                        }
                    }
                }else{
                    //response = setMessageError("Token de acceso inválido: " + token, null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(), null);
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value="/orderbyrequestcode",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity getOrderById(@RequestBody VisorRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start getOrderById");
        Response<VisorByRequestCodeDB> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){
                    if(request.getRequestCode() == null || "".equals(request.getRequestCode().trim())){
                        response = setMessageError("El Código de Pedido ingresado es incorrecto.",access.getId());
                    }else{

                        if(request.getRequestCode().trim().length() < 6 || request.getRequestCode().trim().length() > 10){
                            response = setMessageError("El Código de Pedido ingresado es incorrecto.", access.getId());
                        }else{
                            VisorByRequestCodeDB item = visorService.getVisorByRequestCode(request.getRequestCode());

                            if(item != null){

                                /*StatusDetail obj = new StatusDetail(1,"Hemos recibido su pedido.","2018-01-01 10:00:00");
                                List<StatusDetail> objList = new ArrayList<>();
                                objList.add(obj);
                                item.setDetalle(objList);*/

                                //item.setEstado_solicitud(1);
                                //item.setSub_estado_solicitud(1);

                                //item.setNombre_producto("Duo plano local 20 Mbps");
                                //item.setOperacion_comercial("ALTA PURA");
                                //item.setFecha_solicitado("2018-01-01 10:00:00");

                                //item.setPrimera(true);

                                response.setResponseData(item);
                                response.setResponseMessage("Success");
                                response.setResponseCode("1");
                                traceabilityAccessService.saveLog("Success!", access.getId());
                            }else{
                                response = setMessageError("El Código de Pedido no existe.",access.getId());
                            }
                        }
                    }
                }else{
                    //response = setMessageError("Token de acceso inválido: " + token, null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(), null);
        }
        return ResponseEntity.ok(response);
    }

    /*@RequestMapping(value="/updatestatustoa",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity updateStatusTOA(@RequestBody StatusToaRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start updateStatusTOA");
        Response<String> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_TOA);
                if(access != null){
                    if( !statusToaService.validateFilledFieldsRequest(request) ){
                        response = setMessageError("Datos incompletos.", access.getId());
                    }else{

                        String resultValidation = statusToaService.validateContentFieldsRequest(request);
                        if(resultValidation.equals("")){

                            VisorByRequestCodeDB item = visorService.getVisorByRequestCode(request.getCodigo_pedido());

                            if(item != null){

                                StatusToaDB statusToaActual = statusToaService.getStatusToa(request.getCodigo_pedido());
                                statusToaService.saveStatusTOA(statusToaActual,  request, item.getId(), request.getCodigo_pedido());

                                response.setResponseData("OK");
                                response.setResponseMessage("Success");
                                response.setResponseCode("1");
                                traceabilityAccessService.saveLog("Save successfully: " + request.getCodigo_pedido(), access.getId());

                            }else{
                                response = setMessageError("El Código de Pedido no existe.", access.getId());
                            }

                        }else{
                            response = setMessageError(resultValidation, access.getId());
                        }

                    }
                }else{
                    //response = setMessageError("Token de acceso inválido: " + token,null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(),null);
        }
        return ResponseEntity.ok(response);
    }*/

    @RequestMapping(value="/savecustomerpublicdata",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity saveCustomerPublicData(@RequestBody PublicCustomerRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start saveCustomerPublicData");
        Response<String> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){

                    String resultValidation = publicCustomerService.validateDataFieldsRequest(request);
                    if(resultValidation.equals("")){

                        PublicCustomerDataDB publicCustomerDataActual = publicCustomerService.getPublicCustomerData(request.getDocumento(),request.getCodigoPedido());

                        // Anthony
                        PublicCustomerRequest requestOut = publicCustomerService.validCustomerData(request);

                        publicCustomerService.savePublicCustomerData(publicCustomerDataActual,  requestOut);

                        response.setResponseData("OK");
                        response.setResponseMessage("Success");
                        response.setResponseCode("1");
                        traceabilityAccessService.saveLog("Save successfully: " + request.getDocumento(), access.getId());

                    }else{
                        response = setMessageError(resultValidation, access.getId());
                    }

                }else{
                    //response = setMessageError("Token inválido: " + token,null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(),null);
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value="/savecustomerpublicrating",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity saveCustomerPublicRating(@RequestBody PublicCustomerRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start saveCustomerPublicData");
        Response<String> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){

                    String resultValidation = publicCustomerService.validateRatingFieldsRequest(request);
                    if(resultValidation.equals("")){

                        PublicCustomerRatingDB publicCustomerRatingActual = publicCustomerService.getPublicCustomerRating(request.getDocumento(), request.getCodigoPedido());



                        publicCustomerService.savePublicCustomerRating(publicCustomerRatingActual,  request);

                        response.setResponseData("OK");
                        response.setResponseMessage("Success");
                        response.setResponseCode("1");
                        traceabilityAccessService.saveLog("Save successfully: " + request.getDocumento(), access.getId());

                    }else{
                        response = setMessageError(resultValidation, access.getId());
                    }

                }else{
                    //response = setMessageError("Token inválido: " + token,null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(),null);
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value="/savecustomerpublicrescheduled",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity saveCustomerPublicRescheduled(@RequestBody PublicCustomerRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start saveCustomerPublicData");
        Response<String> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){

                    String resultValidation = publicCustomerService.validateRescheduledFieldsRequest(request);
                    if(resultValidation.equals("")){

                        //PublicCustomerRescheduledDB publicCustomerRescheduledActual = publicCustomerService.getPublicCustomerRescheduled(request.getDocumento(), request.getCodigoPedido());
                        //publicCustomerService.savePublicCustomerRescheduled(publicCustomerRescheduledActual,  request);

                        if(publicCustomerService.canReschedule(request.getDocumento(), request.getCodigoPedido())){
                            publicCustomerService.savePublicCustomerRescheduled(request);

                            response.setResponseData("OK");
                            response.setResponseMessage("Success");
                            response.setResponseCode("1");
                            traceabilityAccessService.saveLog("Save successfully: " + request.getDocumento(), access.getId());
                        }else{
                            response.setResponseData(null);
                            response.setResponseMessage("Aún no puedes volver a agendar la llamada.");
                            response.setResponseCode("0");
                        }

                    }else{
                        response = setMessageError(resultValidation, access.getId());
                    }

                }else{
                    //response = setMessageError("Token inválido: " + token,null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(),null);
        }
        return ResponseEntity.ok(response);
    }

    @RequestMapping(value="/recoveryordercode",method=RequestMethod.POST, produces = "application/json; charset=UTF-8")
    public @ResponseBody ResponseEntity recoveryOrderCode(@RequestBody VisorRequest request, HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        _log.info("start recoveryOrderCode");
        //Response<String> response = new Response<>();
        Response<EmailPhone> response = new Response<>();
        //String token = "";
        try{
            //token = request.getToken();

            String token = httpRequest.getHeader("Access-Token");

            if(token != null && !token.equals("")){
                TraceabilityAccessDB access = traceabilityAccessService.getTraceabilityAccess(token, TraceabilityAccessDB.TYPE_WEB_PUBLIC);
                if(access != null){
                    if(request.getDocument() == null || "".equals(request.getDocument().trim())){
                        response = setMessageError("El número de documento ingresado es inválido.", access.getId());
                    }else{

                        if(request.getDocument().trim().length() < 8 || request.getDocument().trim().length() > 12){
                            response = setMessageError("El número de documento ingresado es inválido.", access.getId());
                        }else{
                            List<VisorByDocDB> items = visorService.getVisorByDocToRecovery(request.getDocument().trim());

                            if(items != null && items.size() > 0){

                                List<String> orderCodeList = new ArrayList<>();
                                for(VisorByDocDB obj : items){
                                    orderCodeList.add(obj.getCodigo_pedido());
                                }

                                List<String> clientDataList = visorService.getClientData(request.getDocument().trim(),orderCodeList);

                                String emailHTML = "";
                                try {
                                    String path = "/static/template/template_recuperar_pedido.html";
                                    //File file = new File(getClass().getResource(path).getFile());

                                    File file = new File(new URI(getClass().getResource(path).toString()));

                                    Document htmlFile = Jsoup.parse(file, "UTF-8");
                                    emailHTML = htmlFile.toString();
                                } catch (IOException e) {
                                    //e.printStackTrace();
                                    _log.error(e.getMessage());
                                    throw new Exception(e.getMessage());
                                }

                                String orderCodeResponse = "";
                                for(int i = 0; i < items.size(); i++){
                                    String orderCode = items.get(i).getCodigo_pedido() != null ? items.get(i).getCodigo_pedido() : "";
                                    if(i == 0) orderCodeResponse = orderCode;
                                    else orderCodeResponse = orderCodeResponse + ", " + orderCode;
                                }

                                String productNameResponse = "";
                                for(int i = 0; i < items.size(); i++){
                                    String productName = items.get(i).getNombre_producto() != null ? items.get(i).getNombre_producto() : "";
                                    if(i == 0) productNameResponse = productName;
                                    else productNameResponse = productNameResponse + ", " + productName;
                                }

                                String cliente = (items.get(0).getCliente() != null) ? items.get(0).getCliente().trim() : "Estimado(a)";

                                String customEmailHTML = emailHTML.replace(":%requestCode%:", orderCodeResponse);
                                customEmailHTML = customEmailHTML.replace(":%productName%:", productNameResponse);
                                customEmailHTML = customEmailHTML.replace(":%documentNumber%:", request.getDocument().trim());
                                customEmailHTML = customEmailHTML.replace(":%clientName%:", cliente);

                                /*PRUEBASSSSS*/
                                emailsToSend = getReceivers();
                                /*PRUEBASSSSS*/

                                List<String> toEmail = new ArrayList<>();
                                String[] splitted = clientDataList.get(0).split(",");
                                for(String objx : splitted){
                                    toEmail.add(objx);
                                }




                                int counterMail = 0;
                                for(String email : toEmail){

                                    EmailResponseBody sendExpressResult = mailing.sendMailExpress(email, customEmailHTML, constants.getSubjectRecuperarCodigo());

                                    _log.info("Result ->");
                                    _log.info("Email: " + email);
                                    _log.info("Enviado: " + sendExpressResult.getAggregatesSendEmail());
                                    _log.info("Fecha y Hora: " + sendExpressResult.getTimestamp());
                                    if(sendExpressResult.getAggregatesSendEmail().equals(1)) counterMail++;

                                }

                                _log.info("TOTAL DE CORREOS ENVIADOS (Recuperar código de pedido): " + counterMail);

                                //response.setResponseData("Su código de pedido fue enviado a: " + clientDataList.get(0) + " - " + clientDataList.get(1));

                                EmailPhone emailPhone = new EmailPhone();
                                emailPhone.setCorreos(clientDataList.get(0));
                                emailPhone.setNumeros(clientDataList.get(1));
                                response.setResponseData(emailPhone);
                                //response.setResponseData("Correos: " + clientDataList.get(0) + "," + clientDataList.get(1));

                                response.setResponseMessage("Success");
                                response.setResponseCode("1");
                                traceabilityAccessService.saveLog("Success!", access.getId());
                            }else{
                                response = setMessageError("El número de documento ingresado no cuenta con pedidos.", access.getId());
                            }
                        }
                    }
                }else{
                    //response = setMessageError("Token de acceso inválido: " + token, null);
                    saveUnauthorizedLog("Token de acceso inválido: " + token);
                    httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Token de acceso inválido: " + token);
                }
            }else{
                //response = setMessageError("Debe enviar el token de acceso.", null);
                saveUnauthorizedLog("Debe enviar el token de acceso.");
                httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Debe enviar el token de acceso.");
            }

        }catch(Exception ex){
            response = setMessageError(ex.getMessage(), null);
        }
        return ResponseEntity.ok(response);
    }

    private Response setMessageError(String message, Integer tokenId) {
        Response<Object> response = new Response<>();
        response.setResponseData(null);
        response.setResponseMessage(message);
        response.setResponseCode("0");
        try{
            traceabilityAccessService.saveLog(message, tokenId);
        }catch(Exception ex){
            _log.error(ex.getMessage());
        }
        return response;
    }

    private void saveUnauthorizedLog(String message) {
        try{
            traceabilityAccessService.saveLog(message, null);
        }catch(Exception ex){
            _log.error(ex.getMessage());
        }
    }

    private List<String> getReceivers(){
        String[] splitted = constants.getReceivers().split(",");
        //String[] splitted = "alvin.puma@smart-home.com.pe|diana.ch.n.26@gmail.com".split("\\|");
        List<String> emailsToSend = new ArrayList<>();
        for(int i = 0; i < splitted.length; i++){
            emailsToSend.add(splitted[i]);
        }
        return emailsToSend;
    }

    /*private Response setMessageWarning(String message){
        Response<Object> response = new Response<>();
        response.setResponseData(null);
        response.setResponseMessage(message);
        response.setResponseCode("0");
        return response;
    }*/

}
