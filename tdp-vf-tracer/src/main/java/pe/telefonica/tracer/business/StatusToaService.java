package pe.telefonica.tracer.business;

import pe.telefonica.tracer.model.StatusToaRequest;
import pe.telefonica.tracer.persistence.StatusToaDB;

import java.util.List;

/**
 * Created by Javier on 19/06/2017.
 */
public interface StatusToaService {

    public boolean saveStatusTOA(StatusToaDB statusToaActual, StatusToaRequest request, String id_visor, String codigo_pedido) throws Exception;

    public StatusToaDB getStatusToa(String codigoPedido) throws Exception;

    public boolean validateFilledFieldsRequest(StatusToaRequest request) throws Exception;

    public String validateContentFieldsRequest(StatusToaRequest request) throws Exception;
}
