package pe.telefonica.tracer.business;

import pe.telefonica.tracer.model.PublicCustomerRequest;
import pe.telefonica.tracer.persistence.*;

import java.util.List;

/**
 * Created by Felix on 19/06/2017.
 */
public interface PublicCustomerService {

    public PublicCustomerDataDB getPublicCustomerData(String document, String codigo) throws Exception;

    public PublicCustomerDataDB savePublicCustomerData(PublicCustomerDataDB publicCustomerData, PublicCustomerRequest publicCustomerRequest) throws Exception;

    public String validateDataFieldsRequest(PublicCustomerRequest request) throws Exception;

    public PublicCustomerRatingDB getPublicCustomerRating(String document, String codigoPedido) throws Exception;

    public PublicCustomerRatingDB savePublicCustomerRating(PublicCustomerRatingDB publicCustomerData, PublicCustomerRequest publicCustomerRequest) throws Exception;

    public String validateRatingFieldsRequest(PublicCustomerRequest request) throws Exception;

    public List<PublicCustomerRescheduledDB> getPublicCustomerRescheduled(String document, String codigoPedido) throws Exception;

    public PublicCustomerRescheduledDB savePublicCustomerRescheduled(/*PublicCustomerRescheduledDB publicCustomerRating,*/ PublicCustomerRequest publicCustomerRequest) throws Exception;

    public String validateRescheduledFieldsRequest(PublicCustomerRequest request) throws Exception;

    // Anthony
    public PublicCustomerRequest validCustomerData(PublicCustomerRequest request) throws Exception;
}
