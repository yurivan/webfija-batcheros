package pe.telefonica.tracer.business.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.PublicCustomerService;
import pe.telefonica.tracer.business.TraceabilityAccessService;
import pe.telefonica.tracer.model.PublicCustomerRequest;
import pe.telefonica.tracer.persistence.*;
import pe.telefonica.tracer.persistence.repository.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Pattern;

/**
 * Created by Felix on 19/06/2017.
 */
@Service
public class PublicCustomerServiceImpl implements PublicCustomerService {

    private static final Logger _log = LoggerFactory.getLogger(PublicCustomerServiceImpl.class);

    @Autowired
    private PublicCustomerDataRepository publicCustomerDataRepository;

    @Autowired
    private PublicCustomerRatingRepository publicCustomerRatingRepository;

    @Autowired
    private PublicCustomerRescheduledRepository publicCustomerRescheduledRepository;

    @Override
    public PublicCustomerDataDB getPublicCustomerData(String document, String codigo) throws Exception{
        try{
            return publicCustomerDataRepository.getPublicCustomerData(document,codigo);
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public PublicCustomerDataDB savePublicCustomerData(PublicCustomerDataDB publicCustomerData, PublicCustomerRequest publicCustomerRequest) throws Exception{
        try{
            if(publicCustomerData == null) publicCustomerData = new PublicCustomerDataDB();

            publicCustomerData.setDocumento(publicCustomerRequest.getDocumento());
            publicCustomerData.setTelefono3(publicCustomerRequest.getTelefono3());
            publicCustomerData.setTelefono4(publicCustomerRequest.getTelefono4());
            publicCustomerData.setDireccion2(publicCustomerRequest.getDireccion2());
            publicCustomerData.setCorreo2(publicCustomerRequest.getCorreo2());
            publicCustomerData.setCodigoPedido(publicCustomerRequest.getCodigoPedido());

            Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            Date dateNow = calNow.getTime();

            Timestamp timestamp = new Timestamp(dateNow.getTime());
            publicCustomerData.setInsert_update(timestamp);
            publicCustomerDataRepository.save(publicCustomerData);
            return publicCustomerData;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public String validateDataFieldsRequest(PublicCustomerRequest request) throws Exception{
        try{
            String field = "";
            if( request.getDocumento() == null || request.getDocumento().trim().equals("") ) return "El campo DOCUMENTO es obligatorio.";
            else if( request.getCodigoPedido() == null || request.getCodigoPedido().trim().equals("") ) return "El campo CODIGOPEDIDO es obligatorio.";
            else if( request.getTelefono3() == null/* || request.getTelefono3().trim().equals("")*/ ) field = "TELEFONO3";
            else if( request.getTelefono4() == null /*|| request.getTelefono4().trim().equals("")*/ ) field = "TELEFONO4";
            else if( request.getDireccion2() == null /*|| request.getDireccion2().trim().equals("")*/ ) field = "DIRECCION2";
            else if( request.getCorreo2() == null /*|| request.getCorreo2().trim().equals("")*/ ) field = "CORREO2";
            //else if( request.getRangoFechaInicio() == null /*|| request.getRangoFechaInicio().trim().equals("")*/ ) field = "RANGOFECHAINICIO";
            //else if( request.getRangoFechaFin() == null /*|| request.getRangoFechaFin().trim().equals("")*/ ) field = "RANGOFECHAFIN";

            if(!field.equals("")) return String.format("Se esperaba el campo %s. (Cadena vacía permitida)", field);

            /*DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
            if(!"".equals(request.getRangoFechaInicio()) && !isCorrectDate(request.getRangoFechaInicio(), dateFormat)) field = "RANGOFECHAINICIO";
            else if(!"".equals(request.getRangoFechaFin()) && !isCorrectDate(request.getRangoFechaFin(), dateFormat)) field = "RANGOFECHAFIN";

            if(!field.equals("")) return String.format(" El campo %s tiene un formato incorrecto", field);*/


            if(!isTelephoneValid(request.getTelefono3())) field = "TELEFONO3";
            else if(!isTelephoneValid(request.getTelefono4())) field = "TELEFONO4";
            else if(!isEmailValid(request.getCorreo2())) field = "CORREO2";

            if(!field.equals("")) return String.format("El campo %s tiene un valor no permitido.", field);


            if( request.getDocumento().trim().length() > 12 ) field = "DOCUMENTO";
            else if( request.getCodigoPedido().trim().length() > 20 ) field = "CODIGOPEDIDO";
            else if( request.getTelefono3().trim().length() > 20 ) field = "TELEFONO3";
            else if( request.getTelefono4().trim().length() > 20 ) field = "TELEFONO4";
            else if( request.getDireccion2().trim().length() > 250 ) field = "DIRECCION2";
            else if( request.getCorreo2().trim().length() > 50 ) field = "CORREO2";
            //else if( request.getRangoFechaInicio().trim().length() > 19 ) field = "RANGOFECHAINICIO";
            //else if( request.getRangoFechaFin().trim().length() > 19 ) field = "RANGOFECHAFIN";

            if(!field.equals("")) return String.format("El campo %s ha excedido el límite de caracteres.", field);

            return "";
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    public boolean isTelephoneValid(String number){
        if(Pattern.matches("[0-9\\.\\-]*", number)) return true;
        else return false;
    }

    public boolean isEmailValid(String email){
        if(Pattern.matches("[A-Za-z0-9-_.]+@[A-Za-z0-9-_]+(?:\\.[A-Za-z0-9]+)+", email)) return true;
        else return false;
    }

    /************************************************************/

    @Override
    public PublicCustomerRatingDB getPublicCustomerRating(String document, String codigoPedido) throws Exception{
        try{
            return publicCustomerRatingRepository.getPublicCustomerRating(document,codigoPedido);
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public PublicCustomerRatingDB savePublicCustomerRating(PublicCustomerRatingDB publicCustomerRating, PublicCustomerRequest publicCustomerRequest) throws Exception{
        try{
            if(publicCustomerRating == null) publicCustomerRating = new PublicCustomerRatingDB();

            publicCustomerRating.setDocumento(publicCustomerRequest.getDocumento());
            publicCustomerRating.setCodigo_pedido(publicCustomerRequest.getCodigoPedido());
            publicCustomerRating.setRespuesta1(publicCustomerRequest.getRespuesta1());
            publicCustomerRating.setRespuesta2(publicCustomerRequest.getRespuesta2());
            publicCustomerRating.setRespuesta3(publicCustomerRequest.getRespuesta3());
            /*if(publicCustomerRequest.getValoracion() != null && !publicCustomerRequest.getValoracion().equals(""))
                publicCustomerRating.setValoracion(Integer.parseInt(publicCustomerRequest.getValoracion()));*/
            publicCustomerRating.setComentario(publicCustomerRequest.getComentario());

            Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            Date dateNow = calNow.getTime();

            Timestamp timestamp = new Timestamp(dateNow.getTime());
            publicCustomerRating.setInsert_update(timestamp);
            publicCustomerRatingRepository.save(publicCustomerRating);
            return publicCustomerRating;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public String validateRatingFieldsRequest(PublicCustomerRequest request) throws Exception{
        try{
            String field = "";
            if( request.getDocumento() == null || request.getDocumento().trim().equals("") ) return "El campo DOCUMENTO es obligatorio.";
            else if( request.getCodigoPedido() == null || request.getCodigoPedido().trim().equals("") ) return "El campo CODIGOPEDIDO es obligatorio.";
            else if( request.getRespuesta1() == null/* || request.getRespuesta1().trim().equals("")*/ ) field = "RESPUESTA1";
            else if( request.getRespuesta2() == null /*|| request.getRespuesta2().trim().equals("")*/ ) field = "RESPUESTA2";
            else if( request.getRespuesta3() == null /*|| request.getRespuesta3().trim().equals("")*/ ) field = "RESPUESTA3";
            else if( request.getComentario() == null /*|| request.getValoracion().trim().equals("")*/ ) field = "COMENTARIO";

            if(!field.equals("")) return String.format("Se esperaba el campo %s. (Cadena vacía permitida)", field);

            if( request.getDocumento().trim().length() > 12 ) field = "DOCUMENTO";
            else if( request.getCodigoPedido().trim().length() > 20 ) field = "CODIGOPEDIDO";
            else if( request.getComentario().trim().length() > 500 ) field = "COMENTARIO";

            if(!field.equals("")) return String.format("El campo %s ha excedido el límite de caracteres.", field);

            if( request.getRespuesta1() > 100 ) field = "RESPUESTA1";
            else if( request.getRespuesta2() > 100 ) field = "RESPUESTA2";
            else if( request.getRespuesta3() > 100 ) field = "RESPUESTA3";

            if(!field.equals("")) return String.format("El campo %s ha sobrepasado el valor permitido.", field);

            return "";
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public List<PublicCustomerRescheduledDB> getPublicCustomerRescheduled(String document, String codigoPedido) throws Exception{
        try{
            return publicCustomerRescheduledRepository.getPublicCustomerRescheduled(document,codigoPedido);
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public PublicCustomerRescheduledDB savePublicCustomerRescheduled(/*PublicCustomerRescheduledDB publicCustomerRescheduled,*/ PublicCustomerRequest publicCustomerRequest) throws Exception{
        try{
            //if(publicCustomerRescheduled == null) publicCustomerRescheduled = new PublicCustomerRescheduledDB();
            PublicCustomerRescheduledDB publicCustomerRescheduled = new PublicCustomerRescheduledDB();

            publicCustomerRescheduled.setDocumento(publicCustomerRequest.getDocumento());
            publicCustomerRescheduled.setCodigo_pedido(publicCustomerRequest.getCodigoPedido());
            publicCustomerRescheduled.setCodigo_pedido(publicCustomerRequest.getCodigoPedido());
            publicCustomerRescheduled.setRango_fecha_inicio(publicCustomerRequest.getRangoFechaInicio());
            publicCustomerRescheduled.setRango_fecha_fin(publicCustomerRequest.getRangoFechaFin());

            Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            Date dateNow = calNow.getTime();

            Timestamp timestamp = new Timestamp(dateNow.getTime());
            publicCustomerRescheduled.setInsert_update(timestamp);
            publicCustomerRescheduledRepository.save(publicCustomerRescheduled);
            return publicCustomerRescheduled;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public String validateRescheduledFieldsRequest(PublicCustomerRequest request) throws Exception{
        try{
            String field = "";
            if( request.getDocumento() == null || request.getDocumento().trim().equals("") ) return "El campo DOCUMENTO es obligatorio.";
            else if( request.getCodigoPedido() == null || request.getCodigoPedido().trim().equals("") ) return "El campo CODIGOPEDIDO es obligatorio.";
            else if( request.getRangoFechaInicio() == null || request.getRangoFechaInicio().trim().equals("") ) return "El campo RANGOFECHAINICIO es obligatorio";
            else if( request.getRangoFechaFin() == null || request.getRangoFechaFin().trim().equals("") ) return "El campo RANGOFECHAFIN es obligatorio";

            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            if(!"".equals(request.getRangoFechaInicio()) && !isCorrectDate(request.getRangoFechaInicio(), dateFormat)) field = "RANGOFECHAINICIO";
            else if(!"".equals(request.getRangoFechaFin()) && !isCorrectDate(request.getRangoFechaFin(), dateFormat)) field = "RANGOFECHAFIN";

            if(!field.equals("")) return String.format("El campo %s tiene un formato incorrecto", field);


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            if(!isAllowedDate(request.getRangoFechaInicio(), sdf) || !isAllowedDate(request.getRangoFechaFin(), sdf)) field = "INVALID";

            if(!field.equals("")) return String.format("No se permite agendar fechas pasadas.", field);


            if(!isAfterTo(request.getRangoFechaInicio(), request.getRangoFechaFin(), sdf)) field = "START_END_ERROR";

            if(!field.equals("")) return String.format("El campo RANGOFECHAFIN debe ser posterior al campo RANGOFECHAINICIO.", field);


            if( request.getDocumento().trim().length() > 12 ) field = "DOCUMENTO";
            else if( request.getCodigoPedido().trim().length() > 20 ) field = "CODIGOPEDIDO";
            else if( request.getRangoFechaInicio().trim().length() > 19 ) field = "RANGOFECHAINICIO";
            else if( request.getRangoFechaFin().trim().length() > 19 ) field = "RANGOFECHAFIN";

            if(!field.equals("")) return String.format("El campo %s ha excedido el límite de caracteres.", field);

            return "";
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    public boolean canReschedule(String documento, String codigoPedido){
        boolean result = true;
        try{
            List<PublicCustomerRescheduledDB> rescheduledList = getPublicCustomerRescheduled(documento, codigoPedido);
            if(rescheduledList != null && rescheduledList.size() > 0){
                PublicCustomerRescheduledDB last = rescheduledList.get(rescheduledList.size()-1);

                SimpleDateFormat dateSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date lastReagendado = dateSDF.parse(last.getRango_fecha_fin());
                lastReagendado = dateHoursAfter(lastReagendado, 72);

                Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
                Date dateNow = calNow.getTime();

                if(dateNow.compareTo(lastReagendado) == -1) return false;
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return result;
    }

    private Date dateHoursAfter(Date date, Integer hoursToAdd) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
        cal.setTime(date);
        cal.add(Calendar.HOUR, hoursToAdd);
        return cal.getTime();
    }

    private boolean isInteger(String value){
        try{
            Integer valueInt = Integer.parseInt(value);
            return true;
        }catch(Exception ex){
            return false;
        }
    }

    public boolean isCorrectDate(String date, DateTimeFormatter dtf){
        try{
            LocalDate.parse(date, dtf);
            return true;
        }catch(Exception ex){
            return false;
        }
    }

    public boolean isAllowedDate(String dateStr, SimpleDateFormat sdf){
        boolean result = false;
        try{
            Date date = sdf.parse(dateStr);

            Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            Date dateNow = calNow.getTime();

            if(dateNow.compareTo(date) == -1) result = true;
        }catch(Exception ex){
            return result;
        }
        return result;
    }

    public boolean isAfterTo(String dateStrStart, String dateStrEnd, SimpleDateFormat sdf){
        boolean result = false;
        try{
            Date dateStart = sdf.parse(dateStrStart);
            Date dateEnd = sdf.parse(dateStrEnd);
            if(dateEnd.compareTo(dateStart) == 1) result = true;
        }catch(Exception ex){
            return result;
        }
        return result;
    }

    // Anthony
    public PublicCustomerRequest validCustomerData(PublicCustomerRequest request) throws Exception {
        try {
            PublicCustomerDataDB customerUpdate = new PublicCustomerDataDB();

            List<Object[]> items = publicCustomerDataRepository.validCustomerData(request.getDocumento(),request.getCodigoPedido());
            VisorByRequestCodeDB customer = new VisorByRequestCodeDB();

            if (items.size() > 0) {
                customer = transformToCustomer(items.get(0));

                if (!request.getTelefono3().equals("")) {
                    if (request.getTelefono3().equals(customer.getTelefono()) || request.getTelefono3().equals(customer.getTelefono2())) {
                        request.setTelefono3("");
                    }
                }

                if (!request.getTelefono4().equals("")) {
                    if (request.getTelefono4().equals(customer.getTelefono2()) || request.getTelefono4().equals(customer.getTelefono2())) {
                        request.setTelefono4("");
                        ;
                    }
                }

                if (!request.getDireccion2().equals("")) {
                    if (request.getDireccion2().equalsIgnoreCase(customer.getDireccion())) {
                        request.setDireccion2("");
                    }
                }

                if (!request.getCorreo2().equals("")) {
                    if (request.getCorreo2().equals(customer.getCorreo())) {
                        request.setCorreo2("");
                    }
                }

            }

        } catch (Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
        return request;
    }

    private VisorByRequestCodeDB transformToCustomer(Object[] obj) throws Exception{
        try{
            VisorByRequestCodeDB item = new VisorByRequestCodeDB();

            item.setTelefono(obj[0] != null ? obj[0].toString() : "");
            item.setTelefono2(obj[1] != null ? obj[1].toString() : "");
            item.setDireccion(obj[2] != null ? obj[2].toString() : "");
            item.setCorreo(obj[3] != null ? obj[3].toString() : "");

            return item;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }




}
