package pe.telefonica.tracer.business;

import pe.telefonica.tracer.model.AppUser;

/**
 * Created by Javier on 19/06/2017.
 */
public interface UserService {

    public AppUser login(String phone, String email);


}
