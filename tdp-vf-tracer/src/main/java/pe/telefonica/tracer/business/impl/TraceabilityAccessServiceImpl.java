package pe.telefonica.tracer.business.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.TraceabilityAccessService;
import pe.telefonica.tracer.persistence.TraceabilityAccessDB;
import pe.telefonica.tracer.persistence.TraceabilityAccessLogDB;
import pe.telefonica.tracer.persistence.repository.TraceabilityAccessLogRepository;
import pe.telefonica.tracer.persistence.repository.TraceabilityAccessRepository;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Felix on 19/06/2017.
 */
@Service
public class TraceabilityAccessServiceImpl implements TraceabilityAccessService {

    private static final Logger _log = LoggerFactory.getLogger(TraceabilityAccessServiceImpl.class);

    @Autowired
    private TraceabilityAccessRepository traceabilityAccessRepository;

    @Autowired
    private TraceabilityAccessLogRepository traceabilityAccessLogRepository;

    @Override
    public TraceabilityAccessDB getTraceabilityAccess(String token, Integer type) throws Exception{
        try{
            return traceabilityAccessRepository.getTraceabilityAccess(token, type);
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public TraceabilityAccessLogDB saveLog(String message, Integer tokenId) throws Exception{
        try{
            TraceabilityAccessLogDB obj = new TraceabilityAccessLogDB();
            obj.setTokenId(tokenId);
            obj.setMessage(message);
            Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            Date dateNow = calNow.getTime();
            Timestamp timestamp = new Timestamp(dateNow.getTime());
            obj.setDate(timestamp);
            traceabilityAccessLogRepository.save(obj);
            return obj;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            return new TraceabilityAccessLogDB();
        }
    }

}
