package pe.telefonica.tracer.business.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.StatusToaService;
import pe.telefonica.tracer.common.clients.ClientConfig;
import pe.telefonica.tracer.common.clients.ClientException;
import pe.telefonica.tracer.common.clients.ClientResult;
import pe.telefonica.tracer.common.clients.consultaestadoatis.ConsultaEstadoAtisClient;
import pe.telefonica.tracer.common.clients.consultaestadoatis.DuplicateApiAtisRequestBody;
import pe.telefonica.tracer.common.clients.consultaestadoatis.DuplicateApiAtisResponseBody;
import pe.telefonica.tracer.common.clients.consultaestadocms.ConsultaEstadoCmsClient;
import pe.telefonica.tracer.common.clients.consultaestadocms.DuplicateApiCmsRequestBody;
import pe.telefonica.tracer.common.clients.consultaestadocms.DuplicateApiCmsResponseBody;
import pe.telefonica.tracer.common.clients.dto.ApiHeaderConfig;
import pe.telefonica.tracer.common.clients.dto.ApiResponse;
import pe.telefonica.tracer.common.util.Constants;
import pe.telefonica.tracer.model.StatusToaRequest;
import pe.telefonica.tracer.model.ToaFechasDB;
import pe.telefonica.tracer.persistence.*;
import pe.telefonica.tracer.persistence.repository.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 * Created by Felix on 19/06/2017.
 */
@Service
public class ValidateDropServiceImpl {

    private static final Logger _log = LoggerFactory.getLogger(ValidateDropServiceImpl.class);

    @Autowired
    PedidoCaidaRepository repositoryCaida;

    @Autowired
    PedidoEstadoRepository pedidoEstadoRepository;

    @Autowired
    ParametersRepository parametersRepository;

    @Autowired
    private ApiHeaderConfig apiHeaderConfig;

    @Autowired
    ConsultaEstadoAtisClient consultaEstadoAtisClient;

    @Autowired
    ConsultaEstadoCmsClient consultaEstadoCmsClient;

    List<ParametersDB> parametersDBList;


    @Autowired
    FechaAsigTec fechaAsigTec;


    @Async
    public CompletableFuture<Object> validateDropsByDelayInAsign() throws Exception {
        try {
            List<Object[]> objList = repositoryCaida.getValidateCaidaAsignacion();
            List<VisorDB> items = toEntityList(objList);

            List<String> codigoPedidoList = new ArrayList<>();
            for (VisorDB obj : items){
                if(obj.getCodigo_pedido() != null) codigoPedidoList.add(obj.getCodigo_pedido());
            }

            List<PedidoEstadoDB> pedidoEstadoDB = pedidoEstadoRepository.getPedidoEstadoList(codigoPedidoList);
            Map<String,Timestamp> codigoPedidoMap = new HashMap<>();
            for (PedidoEstadoDB obj : pedidoEstadoDB){
                codigoPedidoMap.put(obj.getPedido_codigo(),obj.getAuditoria_create());
            }

            if(parametersDBList == null) parametersDBList = parametersRepository.getParametersEstados();

            for (VisorDB val : items) {
                _log.info("Codigo pedido encontrado: " + val.getCodigo_pedido());
                boolean saveDrop = false;
                if (val.getCodigo_pedido().length() == 9) {
                    saveDrop = validateAtis(val.getCodigo_pedido(),codigoPedidoMap);
                } else if (val.getCodigo_pedido().length() == 8 || val.getCodigo_pedido().length() == 7) {
                    saveDrop = validateCms(val.getCodigo_pedido(),codigoPedidoMap);
                }

                if(saveDrop){
                    PedidoCaidaDB itemsCaida = new PedidoCaidaDB();
                    itemsCaida.setCodigo_pedido(val.getCodigo_pedido());
                    itemsCaida.setTipo_caida("RET_ASIGN");
                    Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
                    Date dateNow = calNow.getTime();
                    itemsCaida.setInsert_update(new Timestamp(dateNow.getTime()));
                    repositoryCaida.save(itemsCaida);
                }

            }
            return CompletableFuture.completedFuture(new Object());
        } catch (Exception ex) {
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Async
    public CompletableFuture<Object> validateDropsDTDC() throws Exception{
        try {

            List<PedidoCaidaDB> lista = repositoryCaida.getCaidas();
            PedidoCaidaDB itemsCaida=null;

            for (PedidoCaidaDB item: lista) {
                _log.info("Codigo pedido encontrado: " + item.getCodigo_pedido());

                if (item.getCodigo_pedido().length() == 9) {
                    boolean validateATIS = validateAtis(item.getCodigo_pedido(), null);
                    if (validateATIS) {
                        //insert a la tabla pedidos_en_caida
                        item.setId(item.getId());
                        item.setEstado(true);
                        repositoryCaida.save(item);
                    }else{
                        item.setId(item.getId());
                        item.setIntentos(item.getIntentos()+1);
                        repositoryCaida.save(item);
                    }
                } else if (item.getCodigo_pedido().length() == 8 || item.getCodigo_pedido().length() == 7) {
                    boolean validateCMS = validateCms(item.getCodigo_pedido(), null);
                    if (validateCMS) {
                        item.setId(item.getId());
                        item.setEstado(true);
                        repositoryCaida.save(item);
                    }else{
                        item.setId(item.getId());
                        item.setIntentos(item.getIntentos() + 1);
                        repositoryCaida.save(item);
                    }
                }
            }
            return CompletableFuture.completedFuture(new Object());
        } catch (Exception ex) {
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }




    public boolean validateAtis(String codigoPedido, Map<String,Timestamp> codigoPedidoMap) {
        DuplicateApiAtisRequestBody begin = new DuplicateApiAtisRequestBody();
        begin.setPeticion(codigoPedido);
        try {
            ApiResponse<DuplicateApiAtisResponseBody> response = getConsultaEstadoAtis(begin);
            if(response == null || response.getBodyOut() == null || response.getBodyOut().getEstadoPeticion() == null) return false;

            if(codigoPedidoMap != null){
                savePedidoEstado(codigoPedido, response.getBodyOut().getEstadoPeticion(), "ATIS", codigoPedidoMap);
            }

            if (response.getBodyOut().getEstadoPeticion().equals("CG")) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean validateDelayAtis(String codigoPedido, Map<String,Timestamp> codigoPedidoMap) {
        DuplicateApiAtisRequestBody begin = new DuplicateApiAtisRequestBody();
        begin.setPeticion(codigoPedido);
        try {
            ApiResponse<DuplicateApiAtisResponseBody> response = getConsultaEstadoAtis(begin);
            if(response == null || response.getBodyOut() == null || response.getBodyOut().getEstadoPeticion() == null) return false;

            if(codigoPedidoMap != null){
                savePedidoEstado(codigoPedido, response.getBodyOut().getEstadoPeticion(), "ATIS", codigoPedidoMap);
            }

            if (!response.getBodyOut().getEstadoPeticion().equals("CG") && !response.getBodyOut().getEstadoPeticion().equals("FI")
                    && !response.getBodyOut().getEstadoPeticion().equals("TE")) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean validateAtisTraza(String codigoPedido) {
        DuplicateApiAtisRequestBody begin = new DuplicateApiAtisRequestBody();
        begin.setPeticion(codigoPedido);
        try {
            ApiResponse<DuplicateApiAtisResponseBody> response = getConsultaEstadoAtis(begin);
            if(response == null || response.getBodyOut() == null || response.getBodyOut().getEstadoPeticion() == null) return false;


            if (response.getBodyOut().getEstadoPeticion().equals("CG")) {
                return true;
            }

           /* if (response.getBodyOut().getEstadoPeticion().equals("TE")) {
                return true;
            }

            if (!response.getBodyOut().getEstadoPeticion().equals("CG") && !response.getBodyOut().getEstadoPeticion().equals("FI")
                    && !response.getBodyOut().getEstadoPeticion().equals("TE")) {
                return true;
            }*/

        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }


    public boolean validateCms(String codigoPedido, Map<String,Timestamp> codigoPedidoMap) {
        DuplicateApiCmsRequestBody begin = new DuplicateApiCmsRequestBody();
        begin.setCodigoRequerimiento(codigoPedido);
        try {
            ApiResponse<DuplicateApiCmsResponseBody> response = getConsultaEstadoCms(begin);
            if(response == null || response.getBodyOut() == null || response.getBodyOut().getEstadoRequerimiento() == null) return false;

            if(codigoPedidoMap != null){
                savePedidoEstado(codigoPedido, response.getBodyOut().getEstadoRequerimiento(), "CMS", codigoPedidoMap);
            }

            if (response.getBodyOut().getEstadoRequerimiento().equals("05")) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean validateDelayCms(String codigoPedido, Map<String,Timestamp> codigoPedidoMap) {
        DuplicateApiCmsRequestBody begin = new DuplicateApiCmsRequestBody();
        begin.setCodigoRequerimiento(codigoPedido);
        try {
            ApiResponse<DuplicateApiCmsResponseBody> response = getConsultaEstadoCms(begin);
            if(response == null || response.getBodyOut() == null || response.getBodyOut().getEstadoRequerimiento() == null) return false;

            if(codigoPedidoMap != null){
                savePedidoEstado(codigoPedido, response.getBodyOut().getEstadoRequerimiento(), "CMS", codigoPedidoMap);
            }

            if (!response.getBodyOut().getEstadoRequerimiento().equals("05") && !response.getBodyOut().getEstadoRequerimiento().equals("04")
                    && !response.getBodyOut().getEstadoRequerimiento().equals("02")) {
                return true;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    private PedidoEstadoDB savePedidoEstado(String codigo, String codeStatus, String type, Map<String,Timestamp> codigoPedidoMap){
        if(codigoPedidoMap != null){
            PedidoEstadoDB pedidoEstado = new PedidoEstadoDB();
            pedidoEstado.setPedido_type(type);
            pedidoEstado.setPedido_codigo(codigo);
            pedidoEstado.setPedido_estado_code(codeStatus);
            pedidoEstado.setPedido_estado(getStatusName(codeStatus));
            Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
            Date dateNow = calNow.getTime();
            Timestamp timestamp = new Timestamp(dateNow.getTime());
            pedidoEstado.setAuditoria_modify(timestamp);
            if(codigoPedidoMap.get(codigo) == null){
                pedidoEstado.setAuditoria_create(timestamp);
            }else{
                pedidoEstado.setAuditoria_create(codigoPedidoMap.get(codigo));
            }
            pedidoEstadoRepository.save(pedidoEstado);
            return pedidoEstado;
        }else return null;
    }

    private String getStatusName(String statusCode){
        String result = "";

        if(parametersDBList == null) parametersDBList = parametersRepository.getParametersEstados();

        for(ParametersDB obj : parametersDBList){
            if(statusCode != null && statusCode.equalsIgnoreCase(obj.getElement())){
                result = obj.getStrvalue();
                break;
            }
        }
        return result;
    }

    private List<VisorDB> toEntityList(List<Object[]> objList) {
        List<VisorDB> list = new ArrayList<>();
        for (Object[] obj : objList) {
            String idVisor = (obj[0] != null) ? obj[0].toString() : "";
            String estadoSolicitud = (obj[1] != null) ? obj[1].toString() : "";
            Timestamp fecharegistrado = (obj[2] != null) ? Timestamp.valueOf(obj[2].toString()) : Timestamp.valueOf("");
            String codigopedido = (obj[3] != null) ? obj[3].toString() : "";

            VisorDB visorItem = new VisorDB(idVisor, estadoSolicitud, fecharegistrado, codigopedido);

            list.add(visorItem);
        }
        return list;
    }

    private ApiResponse<DuplicateApiCmsResponseBody> getConsultaEstadoCms(
            DuplicateApiCmsRequestBody apiRequestBody) throws
            ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getCmsConsultaEstadoUri())
                .setApiId(apiHeaderConfig.getCmsConsultarEstadoApiId())
                .setApiSecret(apiHeaderConfig.getCmsConsultarEstadoApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_STATUS_CMS)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_CMS)
                .build();

        //ConsultaEstadoCmsClient consultaEstadoCmsClient = new ConsultaEstadoCmsClient(config);
        ClientResult<ApiResponse<DuplicateApiCmsResponseBody>> result = consultaEstadoCmsClient.post(apiRequestBody, apiRequestBody.getCodigoRequerimiento());

        if (!result.isSuccess()) {
            throw result.getE();
        }
        return result.getResult();
    }

    private ApiResponse<DuplicateApiAtisResponseBody> getConsultaEstadoAtis(DuplicateApiAtisRequestBody apiRequestBody) throws ClientException {
        ClientConfig config = new ClientConfig.ClientConfigBuilder()
                .setUrl(apiHeaderConfig.getAtisConsultaEstadoUri())
                .setApiId(apiHeaderConfig.getAtisConsultarEstadoApiId())
                .setApiSecret(apiHeaderConfig.getAtisConsultarEstadoApiSecret())
                .setOperation(Constants.API_REQUEST_HEADER_OPERATION_STATUS_ATIS)
                .setDestination(Constants.API_REQUEST_HEADER_DESTINATION_ATIS)
                .build();

        //ConsultaEstadoAtisClient consultaEstadoAtisClient = new ConsultaEstadoAtisClient(config);
        ClientResult<ApiResponse<DuplicateApiAtisResponseBody>> result = consultaEstadoAtisClient.post(apiRequestBody, apiRequestBody.getPeticion());

        if (!result.isSuccess()) {
            //throw result.getE();
        }
        return result.getResult();
    }

    @Async
    public CompletableFuture<Object> validateDateAsig() throws Exception{
        try {

            // *****************fecha_instalado is not null and fecha_equipo_tecnico is null********
            List<pe.telefonica.tracer.model.VisorDB> items = fechaAsigTec.allFechaSinAsinar();
            System.out.println(items);

            //StatusToaDB item = repositotyUpdateToa.getStatusToa(items.get(i).getCodigo_pedido());
            List<ToaFechasDB> listFecha  = fechaAsigTec.allFechaSinAsinar(items);
            System.out.println(listFecha);

            //aquí insertamos las fechas de asignación técnica a su código de pedido.
            for (int i = 0; i < listFecha.size(); i++){
                String peticion = listFecha.get(i).getPeticion();
                Timestamp insertDate = listFecha.get(i).getInsert_date();
                if(listFecha.get(i).getStatus_toa().equalsIgnoreCase("WO_COMPLETED")){
                     fechaAsigTec.updateAsigTecFecha(peticion,insertDate);
                }
            }


/*
            // *****************fecha_instalado is null and fecha_equipo_tecnico null********
            List<pe.telefonica.tracer.model.VisorDB> itemSinFechasEnVisor = fechaAsigTec.allFechaVaciaEnVisor();
            System.out.println(itemSinFechasEnVisor);

            List<ToaFechasDB> listFechasToaComplete  = fechaAsigTec.allFechaSinAsinar(itemSinFechasEnVisor);
            System.out.println(listFechasToaComplete);

            for (int i = 0; i < listFechasToaComplete.size(); i++){
                String peticion = listFechasToaComplete.get(i).getPeticion();
                Timestamp insertDate = listFechasToaComplete.get(i).getInsert_date();
                if(listFechasToaComplete.get(i).getStatus_toa().equalsIgnoreCase("WO_INIT")){
                    fechaAsigTec.updateAsigTecFecha(peticion,insertDate);
                }

            }

            for (int i = 0; i < listFechasToaComplete.size(); i++){
                String peticion = listFechasToaComplete.get(i).getPeticion();
                Timestamp insertDate = listFechasToaComplete.get(i).getInsert_date();
                if(listFechasToaComplete.get(i).getStatus_toa().equalsIgnoreCase("WO_COMPLETED")){
                  fechaAsigTec.updateInstaladoFecha(peticion,insertDate);
                }

            }



            //*****************fecha instalada es nula********
            List<pe.telefonica.tracer.model.VisorDB> itemsSinFechaInstaladoAsignado = fechaAsigTec.allFechaInstaladoNula();
            System.out.println(itemsSinFechaInstaladoAsignado);

            List<ToaFechasDB> listFechasToaAsigNoComplete  = fechaAsigTec.allFechaSinAsinar2(itemsSinFechaInstaladoAsignado);
            System.out.println(listFechasToaAsigNoComplete);

            for (int i = 0; i < listFechasToaAsigNoComplete.size(); i++){
                String peticion = listFechasToaAsigNoComplete.get(i).getPeticion();
                Timestamp insertDate = listFechasToaAsigNoComplete.get(i).getInsert_date();
                if(listFechasToaAsigNoComplete.get(i).getStatus_toa().equalsIgnoreCase("WO_COMPLETED")){
                    fechaAsigTec.updateInstaladoFecha(peticion,insertDate);
                }

            }*/



            return CompletableFuture.completedFuture(new Object());
        } catch (Exception ex) {
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

}
