package pe.telefonica.tracer.business.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.StatusToaService;
import pe.telefonica.tracer.model.StatusToaRequest;
import pe.telefonica.tracer.persistence.StatusToaDB;
import pe.telefonica.tracer.persistence.repository.StatusToaRepository;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by Felix on 19/06/2017.
 */
@Service
public class StatusToaServiceImpl implements StatusToaService {

    private static final Logger _log = LoggerFactory.getLogger(StatusToaServiceImpl.class);

    @Autowired
    private StatusToaRepository repositotyUpdateToa;

    @Override
    public StatusToaDB getStatusToa(String codigoPedido) throws Exception{
        try{
            StatusToaDB item = repositotyUpdateToa.getStatusToa(codigoPedido);
            return item;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public boolean saveStatusTOA(StatusToaDB statusToaActual, StatusToaRequest request, String id_visor, String codigo_pedido) throws Exception{
        try{

            if( statusToaActual == null )
                statusToaActual = new StatusToaDB();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm:ss");

            statusToaActual.setId_visor(id_visor);
            statusToaActual.setAccion(request.getAccion());
            statusToaActual.setRequerimiento(request.getCodigo_pedido());
            statusToaActual.setCodigo_pedido(codigo_pedido);
            statusToaActual.setOrden_trabajo(request.getOrden_trabajo());

            Date fechaEnvioDate = dateFormat.parse(request.getFecha_envio());
            java.sql.Date fechaEnvio = new java.sql.Date(fechaEnvioDate.getTime());
            statusToaActual.setFecha_envio(fechaEnvio);

            Date horaEnvioDate = timeFormat.parse(request.getHora_envio());
            java.sql.Time horaEnvio = new java.sql.Time(horaEnvioDate.getTime());
            statusToaActual.setHora_envio(horaEnvio);

            Date fechaEnvioToaDate = dateFormat.parse(request.getFecha_envio_toa());
            java.sql.Date fechaEnvioToa = new java.sql.Date(fechaEnvioToaDate.getTime());
            statusToaActual.setFecha_envio_toa(fechaEnvioToa);

            Date horaEnvioToaDate = timeFormat.parse(request.getHora_envio_toa());
            java.sql.Time horaEnvioToa = new java.sql.Time(horaEnvioToaDate.getTime());
            statusToaActual.setHora_envio_toa(horaEnvioToa);

            if(request.getFecha_agenda() != null && !request.getFecha_agenda().equals("")){
                Date fechaAgendaDate = dateFormat.parse(request.getFecha_agenda());
                java.sql.Date fechaAgenda = new java.sql.Date(fechaAgendaDate.getTime());
                statusToaActual.setFecha_agenda(fechaAgenda);
            }

            statusToaActual.setFranja_horaria(request.getFranja_horaria());

            Date fechaInicioSlaDate = dateFormat.parse(request.getFecha_inicio_sla());
            java.sql.Date fechaInicioSla = new java.sql.Date(fechaInicioSlaDate.getTime());
            statusToaActual.setFecha_inicio_sla(fechaInicioSla);

            Date fechaFinSlaDate = dateFormat.parse(request.getFecha_fin_sla());
            java.sql.Date fechaFinSla = new java.sql.Date(fechaFinSlaDate.getTime());
            statusToaActual.setFecha_fin_sla(fechaFinSla);

            Date fechaInicioDate = dateFormat.parse(request.getFecha_inicio());
            java.sql.Date fechaInicio = new java.sql.Date(fechaInicioDate.getTime());
            statusToaActual.setFecha_inicio(fechaInicio);

            Date fechaCierreDate = dateFormat.parse(request.getFecha_cierre());
            java.sql.Date fechaCierre = new java.sql.Date(fechaCierreDate.getTime());
            statusToaActual.setFecha_cierre(fechaCierre);

            statusToaActual.setMotivo_accion(request.getMotivo_accion());
            statusToaActual.setCelular_cliente(request.getCelular_cliente());
            statusToaActual.setMensaje_cliente(request.getMensaje_cliente());

            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            statusToaActual.setInsert_update(timestamp);



            repositotyUpdateToa.save(statusToaActual);
            return true;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public boolean validateFilledFieldsRequest(StatusToaRequest request) throws Exception{
        try{
            if( request.getAccion() != null && !request.getAccion().trim().equals("") &&
                request.getRequerimiento() != null && !request.getRequerimiento().trim().equals("") &&
                request.getCodigo_pedido() != null && !request.getCodigo_pedido().trim().equals("") &&
                request.getOrden_trabajo() != null && !request.getOrden_trabajo().trim().equals("") &&
                request.getFecha_envio() != null && !request.getFecha_envio().trim().equals("") &&
                request.getHora_envio() != null && !request.getHora_envio().trim().equals("") &&
                request.getFecha_envio_toa() != null && !request.getFecha_envio_toa().trim().equals("") &&
                request.getHora_envio_toa() != null && !request.getHora_envio_toa().trim().equals("") &&
                request.getFranja_horaria() != null && !request.getFranja_horaria().trim().equals("") &&
                request.getFecha_inicio_sla() != null && !request.getFecha_inicio_sla().trim().equals("") &&
                request.getFecha_fin_sla() != null && !request.getFecha_fin_sla().trim().equals("") &&
                request.getFecha_inicio() != null && !request.getFecha_inicio().trim().equals("") &&
                request.getFecha_cierre() != null && !request.getFecha_cierre().trim().equals("") &&
                request.getMotivo_accion() != null && !request.getMotivo_accion().trim().equals("") &&
                request.getCelular_cliente() != null && !request.getCelular_cliente().trim().equals("") &&
                request.getMensaje_cliente() != null && !request.getMensaje_cliente().trim().equals("") ){

                return true;
            }else{
                return false;
            }
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public String validateContentFieldsRequest(StatusToaRequest request) throws Exception{
        try{
            String field = "";
            if(request.getAccion().trim().length() > 20) field = "ACCION";
            else if(request.getRequerimiento().trim().length() > 15) field = "REQUERIMIENTO";
            else if(request.getCodigo_pedido().trim().length() > 15) field = "CODIGO PEDIDO";
            else if(request.getOrden_trabajo().trim().length() > 15) field = "ORDEN TRABAJO";
            else if(request.getFranja_horaria().trim().length() > 6) field = "FRANJA HORARIA";
            else if(request.getMotivo_accion().trim().length() > 100) field = "MOTIVO ACCION";
            else if(request.getCelular_cliente().trim().length() > 10) field = "CELULAR CLIENTE";
            else if(request.getMensaje_cliente().trim().length() > 140) field = "MENSAJE CLIENTE";

            if(!field.equals("")) return String.format("El campo %s excede el límite de caracteres.", field);

            //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            //SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

            DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("hh:mm:ss a");

            if(!isCorrectDate(request.getFecha_envio(), dateFormat)) field = "FECHA ENVIO";
            else if(!isCorrectTime(request.getHora_envio(), timeFormat)) field = "HORA ENVIO";
            else if(!isCorrectDate(request.getFecha_envio_toa(), dateFormat)) field = "FECHA ENVIO TOA";
            else if(!isCorrectTime(request.getHora_envio_toa(), timeFormat)) field = "HORA ENVIO TOA";
            else if(request.getFecha_agenda() != null && !request.getFecha_agenda().equals("") && !isCorrectDate(request.getFecha_agenda(), dateFormat)) field = "FECHA AGENDA";
            else if(!isCorrectDate(request.getFecha_inicio_sla(), dateFormat)) field = "FECHA INICIO SLA";
            else if(!isCorrectDate(request.getFecha_fin_sla(), dateFormat)) field = "FECHA FIN SLA";
            else if(!isCorrectDate(request.getFecha_inicio(), dateFormat)) field = "FECHA INICIO";
            else if(!isCorrectDate(request.getFecha_cierre(), dateFormat)) field = "FECHA CIERRE";

            if(!field.equals("")) return String.format("El campo %s no tiene el formato correcto.", field);

            return "";
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    /*public boolean isCorrectFormat(String date, SimpleDateFormat sdf) throws Exception{
        try{
            sdf.parse(date);
            return true;
        }catch(Exception ex){
            return false;
        }
    }*/

    public boolean isCorrectDate(String date, DateTimeFormatter dtf) throws Exception{
        try{
            LocalDate.parse(date, dtf);
            return true;
        }catch(Exception ex){
            return false;
        }
    }
    public boolean isCorrectTime(String date, DateTimeFormatter dtf) throws Exception{
        try{
            LocalTime.parse(date + " AM", dtf);
            return true;
        }catch(Exception ex){
            return false;
        }
    }

}
