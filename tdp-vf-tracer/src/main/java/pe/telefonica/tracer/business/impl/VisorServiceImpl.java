package pe.telefonica.tracer.business.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.VisorService;
import pe.telefonica.tracer.configuration.Constants;
import pe.telefonica.tracer.configuration.ConstantsMessage;
import pe.telefonica.tracer.model.ClientData;
import pe.telefonica.tracer.model.StatusDetail;
import pe.telefonica.tracer.model.VisorByDocResponse;
import pe.telefonica.tracer.persistence.*;
import pe.telefonica.tracer.persistence.repository.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Felix y editado by Valdemar on 19/06/2017.
 */
@Service
public class VisorServiceImpl implements VisorService {

    private static final Logger _log = LoggerFactory.getLogger(VisorServiceImpl.class);

    @Autowired
    private VisorByDocRepository repositoryByDoc;

    @Autowired
    PedidoCaidaRepository repositoryCaida;

    @Autowired
    private VisorByRequestCodeRepository repositoryByRequestCode;

    @Autowired
    private VisorUpdateToaRepository repositotyUpdateToa;

    @Autowired
    private ToaStatusRepository toaStatusRepository;

    @Autowired
    private PedidoCaidaRepository pedidoCaidaRepository;

    @Autowired
    private MessageParamRepository messageParamRepository;

    @Autowired
    ComercialDevRepository comercialDevRepository;

    @Autowired
    PublicCustomerDataRepository publicCustomerDataRepository;

    @Autowired
    PublicCustomerServiceImpl publicCustomerService;

    @Autowired
    ValidateDropServiceImpl validateDropService;

    @Autowired
    Constants constants;

    List<ToaStatusDB> estadosTOAList = new ArrayList<>();

    List<PedidoCaidaDB> devueltasCaidaList = new ArrayList<>();

    String fechaRetraso = "";
    String fechaHayTecnico = "";
    String nombreTecnico = "";
    String fechaInTwoHours = "";
    String fechaCaidaRetraso = "";
    String fechaCaidaDevuelta = "";
    String fechaRetrasoDevComercial = "";
    List<PublicCustomerRescheduledDB> rescheduledList = null;

    @Override
    public List<VisorByDocDB> getVisorByDoc(String document) throws Exception{
        try{
            List<VisorByDocDB> items = repositoryByDoc.getVisorByDoc(document);
            return items;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    public List<VisorByDocDB> getVisorByDocToRecovery(String document) throws Exception{
        try{
            List<VisorByDocDB> items = repositoryByDoc.getVisorByDocToRecovery(document);
            return items;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    @Override
    public VisorByRequestCodeDB getVisorByRequestCode(String requestCode) throws Exception{
        try{
            VisorByRequestCodeDB item = null;
            Object[] obj = repositoryByRequestCode.getVisorByRequestCode(requestCode);
            if(obj != null && obj.length > 0 && obj[0] != null){
                item = transform((Object[])obj[0]);
            }else return null;

            Integer estadoSolicitudInt = 0;
            Integer subEstadoSolicitudInt = 0;

            List<String> detalleKeys = new ArrayList<>();

            if("PENDIENTE".equals(item.getEstado_solicitud_str()) || "EN PROCESO".equals(item.getEstado_solicitud_str()) ||
                    "EN SEGUIMIENTO".equals(item.getEstado_solicitud_str()) || "".equals(item.getEstado_solicitud_str())){

                Date fechaSolicitado = null;
                try{
                    if(item.getFecha_solicitado() != null){
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        fechaSolicitado = sdf.parse(item.getFecha_solicitado());

                        Date twoDaysAgo = dateHoursAgo(48);
                        if(fechaSolicitado.compareTo(twoDaysAgo) == -1 && item.getFecha_registrado() == null){
                            //Retraso en Registrado
                            estadoSolicitudInt = 2;
                            subEstadoSolicitudInt = 2;

                            detalleKeys.add(ConstantsMessage.SOLICITADO_WEB_EXITO);

                            Date retraso = dateHoursAfter(fechaSolicitado,48);
                            fechaRetraso = sdf.format(retraso);

                        }else{
                            //Sigue en Solicitado y a tiempo
                            estadoSolicitudInt = 1;
                            subEstadoSolicitudInt = 1;

                            detalleKeys.add(ConstantsMessage.SOLICITADO_WEB_EXITO);

                        }

                    }
                }catch (Exception ex){
                    _log.error(ex.getMessage());
                    estadoSolicitudInt = 1;
                    subEstadoSolicitudInt = 1;
                }

            }else if("CAIDA".equals(item.getEstado_solicitud_str())){
                //caida en Registrado
                estadoSolicitudInt = 2;
                subEstadoSolicitudInt = 0;
                String motivoCaida = getMotivoCaidaProcesado(item.getMotivo_caida());
                item.setMotivo_caida(motivoCaida);

                detalleKeys.add(ConstantsMessage.SOLICITADO_WEB_EXITO);
                detalleKeys.add(ConstantsMessage.REGISTRADO_WEB_CAIDA);

            }else if("INGRESADO".equals(item.getEstado_solicitud_str())){
                //está al menos en Registrado

                detalleKeys.add(ConstantsMessage.SOLICITADO_WEB_EXITO);
                detalleKeys.add(ConstantsMessage.REGISTRADO_WEB_EXITO);

                estadoSolicitudInt = 2;
                subEstadoSolicitudInt = 1;


                try{
                    //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date fechaRegistrado = stringToDate(item.getFecha_registrado());
                    Date threeDaysAgo = dateHoursAgo(72);
                    if(fechaRegistrado.compareTo(threeDaysAgo) == -1){
                        //Retraso en Tecnico Asignado
                        estadoSolicitudInt = 3;
                        subEstadoSolicitudInt = 2;

                        Date retraso = dateHoursAfter(fechaRegistrado,72);
                        fechaRetraso = dateToString(retraso);

                        //detalleKeys.add(ConstantsMessage.TECNICO_WEB_RETRASO);

                    }
                }catch (Exception ex){
                    _log.error(ex.getMessage());
                }


                estadosTOAList = toaStatusRepository.getEstadosTOAtoShow(item.getCodigo_pedido());
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                Boolean validarEstados = validateDropService.validateAtisTraza(item.getCodigo_pedido());
                //PedidoCaidaDB
                PedidoCaidaDB pedidoCaidaDB = new PedidoCaidaDB();
                if(validarEstados){

                    pedidoCaidaDB.setCodigo_pedido(item.getCodigo_pedido());
                    pedidoCaidaDB.setTipo_caida("RET_ASIGN");
                    pedidoCaidaDB.setIntentos(1);
                    pedidoCaidaDB.setEstado(true);
                    pedidoCaidaDB.setInsert_update(timestamp);
                    pedidoCaidaDB.setFlg_email(0);
                    //daoCaidas.saveCaidaAtis(pedidoCaidaDB);

                    repositoryCaida.save(pedidoCaidaDB);
                }



                devueltasCaidaList = pedidoCaidaRepository.getCaidas(item.getCodigo_pedido());

                Integer[] estadosTOA = getEstadosTOAParam(detalleKeys);
                if(estadosTOA[0] > 0){
                    estadoSolicitudInt = estadosTOA[0];
                    subEstadoSolicitudInt = estadosTOA[1];
                }

                Integer caidasEstado = getCaidasParam(detalleKeys);
                if(caidasEstado > 0){
                    estadoSolicitudInt = caidasEstado;
                    subEstadoSolicitudInt = 0;
                }

                if(subEstadoSolicitudInt != 0){
                    String retrasoDev48 = getRetrasoEnDevueltaComercial(item.getCodigo_pedido());
                    if(!"".equalsIgnoreCase(retrasoDev48)){
                        detalleKeys.add(ConstantsMessage.INSTALADO_WEB_RETRASO_DEV_COM);
                        fechaRetrasoDevComercial = retrasoDev48;
                        estadoSolicitudInt = 4;
                        subEstadoSolicitudInt = 2;
                    }
                }


            }

            if(subEstadoSolicitudInt.equals(2)){
                if(estadoSolicitudInt.equals(2)) detalleKeys.add(ConstantsMessage.REGISTRADO_WEB_RETRASO);
                if(estadoSolicitudInt.equals(3)) detalleKeys.add(ConstantsMessage.TECNICO_WEB_RETRASO);
                if(estadoSolicitudInt.equals(4)) detalleKeys.add(ConstantsMessage.INSTALADO_WEB_RETRASO);
            }

            item.setEstado_solicitud(estadoSolicitudInt);
            item.setSub_estado_solicitud(subEstadoSolicitudInt);

            if(estadoSolicitudInt.equals(4) && detalleKeys.contains(ConstantsMessage.INSTALADO_WEB_RETRASO_DEV_COM) ){
                item.setReagenda_permitida(true);
                rescheduledList = publicCustomerService.getPublicCustomerRescheduled(item.getDocumento(), item.getCodigo_pedido());
                if(rescheduledList != null && rescheduledList.size() > 0){
                    detalleKeys.add(ConstantsMessage.INSTALADO_WEB_RETRASO_REAGENDADO);
                    PublicCustomerRescheduledDB last = rescheduledList.get(rescheduledList.size()-1);
                    item.setFecha_reagendado_inicio(last.getRango_fecha_inicio());
                    item.setFecha_reagendado_fin(last.getRango_fecha_fin());

                    SimpleDateFormat dateSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date lastReagendado = dateSDF.parse(last.getRango_fecha_fin());

                    Integer rescheduledEnabled = 3;
                    try{ rescheduledEnabled = Integer.parseInt(constants.getTimeRescheduledEnable()); }catch (Exception ex){}

                    lastReagendado = dateHoursAfter(lastReagendado, rescheduledEnabled);

                    Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
                    Date dateNow = calNow.getTime();

                    if(dateNow.compareTo(lastReagendado) == -1){
                        item.setReagenda_permitida(false);
                    }
                }
            }

            List<StatusDetail> statusDetail = processMessages(detalleKeys, item);
            item.setDetalle(statusDetail);

            PublicCustomerDataDB publicCustomerDataDB = publicCustomerDataRepository.getPublicCustomerData(item.getDocumento(),item.getCodigo_pedido());
            if(publicCustomerDataDB == null) {
                item.setPrimera(true);
            } else {
                item.setPrimera(false);
            }

            return item;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    private List<StatusDetail> processMessages(List<String> detalleKeys, VisorByRequestCodeDB item){
        List<StatusDetail> result = new ArrayList<>();
        try{
            String CODIGO_PEDIDO = "[CODIGO_PEDIDO]";
            String NOMB_TECNICO = "[NOMB_TECNICO]";
            String NOMB_PRODUCTO = "[NOMB_PRODUCTO]";
            String DIA_REAGENDA = "[DIA_REAGENDA]";
            String DESDE_REAGENDA = "[DESDE_REAGENDA]";
            String HASTA_PRODUCTO = "[HASTA_PRODUCTO]";
            String DIA_MES = "[DDMM]";

            List<MessageParamDB> messageParamDBList =  messageParamRepository.getMessageParam(detalleKeys);

            for(MessageParamDB obj : messageParamDBList){

                String message = obj.getResponsemessage();
                message = message.replace(CODIGO_PEDIDO, item.getCodigo_pedido());
                message = message.replace(NOMB_PRODUCTO, item.getNombre_producto());

                StatusDetail statusDetail = new StatusDetail();
                statusDetail.setDescripcion(message);

                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.SOLICITADO_WEB_EXITO)){
                    statusDetail.setFecha(item.getFecha_solicitado() != null ? item.getFecha_solicitado() : item.getFecha_grabacion());
                    statusDetail.setEstado(1);
                    result.add(statusDetail);
                    continue;
                }

                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.REGISTRADO_WEB_EXITO)){
                    statusDetail.setFecha(item.getFecha_registrado());
                    statusDetail.setEstado(2);
                    result.add(statusDetail);
                    continue;
                }
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.REGISTRADO_WEB_RETRASO)){
                    statusDetail.setFecha(fechaRetraso);
                    statusDetail.setEstado(2);
                    result.add(statusDetail);
                    continue;
                }
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.REGISTRADO_WEB_CAIDA)){
                    statusDetail.setDescripcion(item.getMotivo_caida());
                    statusDetail.setFecha(item.getFecha_grabacion());
                    statusDetail.setEstado(2);
                    result.add(statusDetail);
                    continue;
                }

                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.TECNICO_WEB_RETRASO)){
                    statusDetail.setFecha(fechaRetraso);
                    statusDetail.setEstado(3);
                    result.add(statusDetail);
                    continue;
                }
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.TECNICO_WEB_CAIDA)){
                    statusDetail.setFecha(fechaCaidaRetraso);
                    statusDetail.setEstado(3);
                    result.add(statusDetail);
                    continue;
                }
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.TECNICO_WEB_EXITO)){
                    statusDetail.setFecha(item.getFecha_equipo_tecnico());
                    statusDetail.setEstado(3);
                    result.add(statusDetail);
                    continue;
                }
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.TECNICO_WEB_NOM)){
                    statusDetail.setFecha(fechaHayTecnico);
                    statusDetail.setEstado(3);
                    result.add(statusDetail);
                    continue;
                }
                /*if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.TECNICO_WEB_OTH)){

                }*/
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.TECNICO_WEB_HOU)){
                    String nombTecnico = "TECNICO";
                    if(nombreTecnico != null){
                        String[] nombreTecnicoSplitted = nombreTecnico.split("-");
                        if(nombreTecnicoSplitted.length == 2) nombTecnico = nombreTecnicoSplitted[1];
                    }

                    message = message.replace(NOMB_TECNICO, nombTecnico);
                    statusDetail.setDescripcion(message);

                    statusDetail.setFecha(fechaInTwoHours);
                    statusDetail.setEstado(3);
                    result.add(statusDetail);
                    continue;
                }

                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.TECNICO_WEB_HOU_BK)){
                    statusDetail.setFecha(fechaInTwoHours);
                    statusDetail.setEstado(3);
                    result.add(statusDetail);
                    continue;
                }

                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.INSTALADO_WEB_RETRASO)){
                    statusDetail.setFecha(fechaRetraso);
                    statusDetail.setEstado(4);
                    result.add(statusDetail);
                    continue;
                }
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.INSTALADO_WEB_RETRASO_DEV_COM)){
                    statusDetail.setFecha(fechaRetrasoDevComercial);
                    statusDetail.setEstado(4);
                    result.add(statusDetail);
                    item.setDevuelta(3);
                    continue;
                }

                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.INSTALADO_WEB_RETRASO_REAGENDADO)){

                    if(rescheduledList != null && rescheduledList.size() > 0){

                        SimpleDateFormat daySDF = new SimpleDateFormat("dd/MM/yyyy");
                        SimpleDateFormat timeSDF = new SimpleDateFormat("HH:mm");
                        SimpleDateFormat dateSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        String originalMessage = statusDetail.getDescripcion();

                        for(PublicCustomerRescheduledDB rescheduled : rescheduledList){
                            StatusDetail statusDetailRescheduled = new StatusDetail();
                            String dateStr = "";
                            try{
                                dateStr = dateSDF.format(rescheduled.getInsert_update());
                            }catch (Exception ex){}
                            String dayStr = "";
                            String startStr = "";
                            String endStr = "";
                            try{
                                Date startDate = dateSDF.parse(rescheduled.getRango_fecha_inicio());
                                Date endDate = dateSDF.parse(rescheduled.getRango_fecha_fin());
                                dayStr = daySDF.format(startDate);
                                startStr = timeSDF.format(startDate);
                                endStr = timeSDF.format(endDate);
                            }catch (Exception ex){}

                            statusDetailRescheduled.setFecha(dateStr);
                            statusDetailRescheduled.setEstado(4);
                            String messageRescheduled = originalMessage;
                            messageRescheduled = messageRescheduled.replace(DIA_REAGENDA, dayStr);
                            messageRescheduled = messageRescheduled.replace(DESDE_REAGENDA, startStr);
                            messageRescheduled = messageRescheduled.replace(HASTA_PRODUCTO, endStr);
                            statusDetailRescheduled.setDescripcion(messageRescheduled);

                            result.add(statusDetailRescheduled);
                        }

                    }

                    continue;
                }

                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.INSTALADO_WEB_CAIDA_COMERCIAL_CLIENTE)){
                    statusDetail.setFecha(fechaCaidaDevuelta);
                    statusDetail.setEstado(4);
                    result.add(statusDetail);
                    item.setDevuelta(1);
                    continue;
                }
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.INSTALADO_WEB_CAIDA_COMERCIAL_NO_CONTACTO)){
                    statusDetail.setFecha(fechaCaidaDevuelta);
                    statusDetail.setEstado(4);
                    result.add(statusDetail);
                    item.setDevuelta(1);
                    continue;
                }
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.INSTALADO_WEB_CAIDA_TECNICA)){
                    statusDetail.setFecha(fechaCaidaDevuelta);
                    statusDetail.setEstado(4);
                    result.add(statusDetail);
                    item.setDevuelta(2);
                    continue;
                }
                if(obj.getServicekey().equalsIgnoreCase(ConstantsMessage.INSTALADO_WEB_EXITO)){
                    statusDetail.setFecha(item.getFecha_instalado());
                    statusDetail.setEstado(4);
                    result.add(statusDetail);
                    item.setDevuelta(null);
                    continue;
                }
            }

        }catch (Exception ex){

        }
        return result;
    }

    private Integer[] getEstadosTOAParam(List<String> detalleKeys){
        Integer estado = 0;
        Integer sub_estado = 0;
        Integer[] result = new Integer[]{0, 0};
        try{

            //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");

            List<ToaStatusDB> toaStatusListIN_TOA = new ArrayList<>();
            List<ToaStatusDB> toaStatusListNOT_LEG = new ArrayList<>();
            List<ToaStatusDB> toaStatusListWO_COMPLETED = new ArrayList<>();

            for(ToaStatusDB obj :estadosTOAList ){
                if("IN_TOA".equalsIgnoreCase(obj.getStatus_toa())){
                    if(toaStatusListIN_TOA.size() == 0) toaStatusListIN_TOA.add(obj);
                    continue;
                }
                if("NOT_LEG".equalsIgnoreCase(obj.getStatus_toa())){
                    toaStatusListNOT_LEG.add(obj);
                    continue;
                }
                if("WO_COMPLETED".equalsIgnoreCase(obj.getStatus_toa())){
                    if(toaStatusListWO_COMPLETED.size() == 0) toaStatusListWO_COMPLETED.add(obj);
                    continue;
                }
            }

            if(toaStatusListIN_TOA.size() == 1){
                detalleKeys.add(ConstantsMessage.TECNICO_WEB_EXITO);
                estado = 3;
                sub_estado = 1;

                if(toaStatusListWO_COMPLETED.size() == 0){
                    try{
                        Date IN_TOA_date = new Date(toaStatusListIN_TOA.get(0).getInsert_date().getTime());

                        Date threeDaysAgo = dateHoursAgo(72);
                        if(IN_TOA_date.compareTo(threeDaysAgo) == -1){
                            //Retraso en Tecnico Asignado
                            sub_estado = 2;
                            estado = 4;
                            //detalleKeys.add(ConstantsMessage.INSTALADO_WEB_RETRASO);

                            Date retraso = dateHoursAfter(IN_TOA_date,72);
                            fechaRetraso = dateToString(retraso);
                        }
                    }catch (Exception ex){
                        _log.error(ex.getMessage());
                    }
                }
            }

            if(toaStatusListNOT_LEG.size() > 0){
                /*for(int i = toaStatusListNOT_LEG.size() - 1; i >= 0; i--){
                    if(i == toaStatusListNOT_LEG.size() - 1) detalleKeys.add(ConstantsMessage.TECNICO_WEB_NOM);
                    else detalleKeys.add(ConstantsMessage.TECNICO_WEB_OTH);
                }*/
                detalleKeys.add(ConstantsMessage.TECNICO_WEB_NOM);

                Date tecnico = toaStatusListNOT_LEG.get(0).getInsert_date();
                fechaHayTecnico = dateToString(tecnico);
                nombreTecnico = toaStatusListNOT_LEG.get(0).getPname();

            }

            if(toaStatusListWO_COMPLETED.size() == 1){
                detalleKeys.add(ConstantsMessage.INSTALADO_WEB_EXITO);
                estado = 4;
                sub_estado = 1;
            }

            if(toaStatusListNOT_LEG.size() > 0){
                /*for(int i = toaStatusListNOT_LEG.size() - 1; i >= 0; i--){
                    if(i == toaStatusListNOT_LEG.size() - 1) {
                        detalleKeys.add(ConstantsMessage.TECNICO_WEB_NOM);
                    }else detalleKeys.add(ConstantsMessage.TECNICO_WEB_OTH);
                }*/

                Date fechaAgendado = null;
                try{
                    //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //fechaAgendado = sdf.parse( toaStatusListNOT_LEG.get(toaStatusListNOT_LEG.size() - 1).getStart_time() );

                    fechaAgendado = stringToDate(toaStatusListNOT_LEG.get(0).getStart_time());
                }catch (Exception ex){
                    _log.error(ex.getMessage());
                }

                /*if(fechaAgendado == null){
                    try{
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
                        fechaAgendado = sdf.parse( toaStatusListNOT_LEG.get(toaStatusListNOT_LEG.size() - 1).getStart_time() );
                    }catch (Exception ex){
                        _log.error(ex.getMessage());
                    }
                }*/

                if(fechaAgendado != null && toaStatusListNOT_LEG.get(0).getPname() != null && !toaStatusListNOT_LEG.get(0).getPname().equals("")){

                    Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
                    cal.setTime(fechaAgendado);

                    boolean isBK = false;

                    if(toaStatusListNOT_LEG.get(0).getPname().startsWith("BK_")){
                        cal.add(cal.MINUTE, -30);
                        isBK = true;
                    }else {
                        cal.add(cal.HOUR, -2);
                    }

                    Date dateHoursAgo = cal.getTime();

                    Calendar calNow = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
                    Date dateNow = calNow.getTime();

                    if(dateNow.compareTo(dateHoursAgo) == 1 /*&& dateNow.compareTo(fechaAgendado) == -1*/){
                        //El tecnico llegará en 2 horas
                        if(isBK) detalleKeys.add(ConstantsMessage.TECNICO_WEB_HOU_BK);
                        else detalleKeys.add(ConstantsMessage.TECNICO_WEB_HOU);

                        fechaInTwoHours = dateToString(dateHoursAgo);
                    }

                }

            }

            result = new Integer[]{estado, sub_estado};

        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return result;
    }

    private String getRetrasoEnDevueltaComercial(String codigo_pedido){
        String retraso = "";
        try{
            ComercialDevDB comercialDev = comercialDevRepository.getComercialDevRetraso48(codigo_pedido);
            if(comercialDev != null){
                retraso = dateToString(comercialDev.getCreado());
            }
        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return retraso;
    }

    private Integer getCaidasParam(List<String> detalleKeys){
        Integer estadoSolicitudInt = 0;
        try{
            for(PedidoCaidaDB pedidoCaidaDB : devueltasCaidaList){

                if("RET_ASIGN".equalsIgnoreCase(pedidoCaidaDB.getTipo_caida())){
                    detalleKeys.add(ConstantsMessage.TECNICO_WEB_CAIDA);
                    estadoSolicitudInt = 3;
                    fechaCaidaRetraso = dateToString(pedidoCaidaDB.getInsert_update());
                }
                if("DEV_TEC".equalsIgnoreCase(pedidoCaidaDB.getTipo_caida())){
                    detalleKeys.add(ConstantsMessage.INSTALADO_WEB_CAIDA_TECNICA);
                    estadoSolicitudInt = 4;
                    fechaCaidaDevuelta = dateToString(pedidoCaidaDB.getInsert_update());
                }
                if("DEV_COM_NO".equalsIgnoreCase(pedidoCaidaDB.getTipo_caida())){
                    detalleKeys.add(ConstantsMessage.INSTALADO_WEB_CAIDA_COMERCIAL_NO_CONTACTO);
                    estadoSolicitudInt = 4;
                    fechaCaidaDevuelta = dateToString(pedidoCaidaDB.getInsert_update());
                }
                if("DEV_COM_CL".equalsIgnoreCase(pedidoCaidaDB.getTipo_caida())){
                    detalleKeys.add(ConstantsMessage.INSTALADO_WEB_CAIDA_COMERCIAL_CLIENTE);
                    estadoSolicitudInt = 4;
                    fechaCaidaDevuelta = dateToString(pedidoCaidaDB.getInsert_update());
                }

            }

        }catch (Exception ex){
            _log.error(ex.getMessage());
        }
        return estadoSolicitudInt;
    }

    private String dateToString(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        //SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        String dateStr = "";
        try{
            dateStr = sdf.format(date);
        }catch (Exception ex){}
        /*try{
            dateStr = sdf2.format(date);
        }catch (Exception ex){}*/
        return dateStr;
    }

    private Date stringToDate(String dateStr){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
        Date date = null;
        try{
            date = sdf.parse(dateStr);
        }catch (Exception ex){}
        try{
            date = sdf2.parse(dateStr);
        }catch (Exception ex){}
        return date;
    }

    private Date dateHoursAgo(Integer hoursToLess) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
        cal.add(Calendar.HOUR, hoursToLess * -1);
        return cal.getTime();
    }

    private Date dateHoursAfter(Date date, Integer hoursToAdd) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("America/Lima"));
        cal.setTime(date);
        cal.add(Calendar.HOUR, hoursToAdd);
        return cal.getTime();
    }

    private String getMotivoCaidaProcesado(String motivoCaida){
        try{
            return "MOTIVO CAIDA 1";
        }catch (Exception ex){
            _log.error(ex.getMessage());
            return "MOTIVO CAIDA POR DEFECTO";
        }
    }

    private VisorByRequestCodeDB transform(Object[] obj) throws Exception{
        try{

            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            VisorByRequestCodeDB item = new VisorByRequestCodeDB();
            item.setId(obj[0] != null ? obj[0].toString() : "");
            item.setCodigo_pedido(obj[1] != null ? obj[1].toString() : "");
            if(obj[2] != null && !"".equals(obj[2])){
                if(obj[2].equals("1975-01-01 00:00:00") || obj[2].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_grabacion(null);
                }else{
                    Date date = sdf1.parse(obj[2].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_grabacion(dataStr);
                }
            }else{
                item.setFecha_grabacion(null);
            }
            item.setCliente(obj[3] != null ? obj[3].toString() : "");
            item.setTelefono(obj[4] != null ? obj[4].toString() : "");
            item.setTelefono2(obj[5] != null ? obj[5].toString() : "");
            item.setDireccion(obj[6] != null ? obj[6].toString().replace("\ufffd","Ñ") : "");
            item.setDepartamento(obj[7] != null ? obj[7].toString() : "");
            item.setDocumento(obj[8] != null ? obj[8].toString() : "");

            if(obj[9] != null && !"".equals(obj[9].toString().trim())){
                item.setNombre_producto(obj[9].toString());
            }else{
                String nombreProducto2 = obj[10] != null ? obj[10].toString() : "";
                String subProduct = obj[15] != null ? obj[15].toString() : "";
                item.setNombre_producto(nombreProducto2 + ": " + subProduct);
            }
            //item.setNombre_producto(obj[7] != null ? obj[7].toString() : "");

            item.setEstado_solicitud_str(obj[11] != null ? obj[11].toString() : "");
            item.setMotivo_caida(obj[12] != null ? obj[12].toString() : "");

            //item.setEstado_solicitud(0);
            //item.setSub_estado_solicitud(0);

            item.setOperacion_comercial(obj[13] != null ? obj[13].toString() : "");
            item.setDistrito(obj[14] != null ? obj[14].toString() : "");
            //item.setSub_producto(obj[13] != null ? obj[13].toString() : "");
            item.setTipo_documento(obj[16] != null ? obj[16].toString() : "");
            item.setProvincia(obj[17] != null ? obj[17].toString() : "");
            /*if(obj[18] != null && !"".equals(obj[18])){
                if(obj[18].equals("1975-01-01 00:00:00") || obj[18].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_de_llamada(null);
                }else{
                    Date date = sdf1.parse(obj[18].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_de_llamada(dataStr);
                }
            }else{
                item.setFecha_de_llamada(null);
            }*/
            item.setNombre_producto_fuente(obj[18] != null ? obj[18].toString() : "");
            if(obj[19] != null && !"".equals(obj[19])){
                if(obj[19].equals("1975-01-01 00:00:00")  || obj[19].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_solicitado(null);
                }else{
                    Date date = sdf2.parse(obj[19].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_solicitado(dataStr);
                }
            }else{
                item.setFecha_solicitado(null);
            }
            if(obj[20] != null && !"".equals(obj[20])){
                if(obj[20].equals("1975-01-01 00:00:00")  || obj[20].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_registrado(null);
                }else{
                    Date date = sdf2.parse(obj[20].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_registrado(dataStr);
                }
            }else{
                item.setFecha_registrado(null);
            }
            if(obj[21] != null && !"".equals(obj[21])){
                if(obj[21].equals("1975-01-01 00:00:00")  || obj[21].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_equipo_tecnico(null);
                }else{
                    Date date = sdf2.parse(obj[21].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_equipo_tecnico(dataStr);
                }
            }else{
                item.setFecha_equipo_tecnico(null);
            }
            if(obj[2] != null && !"".equals(obj[22])){
                if(obj[22].equals("1975-01-01 00:00:00")  || obj[22].toString().equals("1975-01-01 00:00:00.0")){
                    item.setFecha_instalado(null);
                }else{
                    Date date = sdf2.parse(obj[22].toString());
                    String dataStr = sdf2.format(date);
                    item.setFecha_instalado(dataStr);
                }
            }else{
                item.setFecha_instalado(null);
            }
            //item.setEstado_anterior(obj[24] != null ? obj[24].toString() : "");

            //Nuevos campos
            item.setTelefono3(obj[23] != null ? obj[23].toString() : "");
            item.setTelefono4(obj[24] != null ? obj[24].toString() : "");
            item.setDireccion2(obj[25] != null ? obj[25].toString().replace("\ufffd","Ñ") : "");
            item.setCorreo2(obj[26] != null ? obj[26].toString() : "");

            String valoracion = obj[27] != null ? obj[27].toString() : "";
            Boolean valoracionBool = "true".equalsIgnoreCase(valoracion) ? true : false;

            item.setValoracion(valoracionBool);
            item.setCoordenada_x(obj[28] != null ? obj[28].toString() : "");
            item.setCoordenada_y(obj[29] != null ? obj[29].toString() : "");
            item.setCorreo(obj[30] != null ? obj[30].toString() : "");

            item.setEquipamientoTv(obj[31] != null ? obj[31].toString() : "");
            item.setEquipamientoInternet(obj[32] != null ? obj[32].toString() : "");
            item.setVelocidad(obj[33] != null && !obj[33].toString().trim().equals("") ? obj[33].toString().trim() + " Mbps" : "");

            return item;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    public List<VisorByDocResponse> toVisorByDocResponseList(List<VisorByDocDB> items) throws Exception{
        try{
            List<VisorByDocResponse> result = new ArrayList<>();
            for(VisorByDocDB item : items){
                VisorByDocResponse obj =  new VisorByDocResponse();
                obj.setCodigo_pedido(item.getCodigo_pedido());
                result.add(obj);
            }
            return result;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    public List<String> getClientData(String document, List<String> requestCodeList) throws Exception{
        try{
            List<Object[]> items = repositoryByDoc.getClientData(document, requestCodeList);
            List<String> emailList = new ArrayList<>();
            List<String> phoneList = new ArrayList<>();
            for(Object[] obj : items){
                ClientData clientData = transformToClient(obj);
                if(!clientData.getEmail().equals("") && !emailList.contains(clientData.getEmail())) emailList.add(clientData.getEmail());
                if(!clientData.getCorreo2().equals("") && !emailList.contains(clientData.getCorreo2())) emailList.add(clientData.getCorreo2());

                if(!clientData.getCustomerphone().equals("") && !phoneList.contains(clientData.getCustomerphone())) phoneList.add(clientData.getCustomerphone());
                if(!clientData.getCustomerphone2().equals("") && !phoneList.contains(clientData.getCustomerphone2())) phoneList.add(clientData.getCustomerphone2());
                if(!clientData.getTelefono3().equals("") && !phoneList.contains(clientData.getTelefono3())) phoneList.add(clientData.getTelefono3());
                if(!clientData.getTelefono4().equals("") && !phoneList.contains(clientData.getTelefono4())) phoneList.add(clientData.getTelefono4());
            }

            String emailResponse = "";
            for(int i = 0; i < emailList.size(); i++){
                if(i == 0) emailResponse = emailList.get(i);
                else emailResponse = emailResponse + "," + emailList.get(i);
            }
            String phoneResponse = "";
            for(int i = 0; i < phoneList.size(); i++){
                if(i == 0) phoneResponse = phoneList.get(i);
                else phoneResponse = phoneResponse + "," + phoneList.get(i);
            }

            List<String> response = new ArrayList<>();
            response.add(emailResponse);
            response.add(phoneResponse);

            return response;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }

    private ClientData transformToClient(Object[] obj) throws Exception{
        try{
            ClientData item = new ClientData();
            item.setEmail(obj[0] != null ? obj[0].toString().trim() : "");
            item.setCustomerphone(obj[1] != null ? obj[1].toString().trim() : "");
            item.setCustomerphone2(obj[2] != null ? obj[2].toString().trim() : "");
            item.setCorreo2(obj[3] != null ? obj[3].toString().trim() : "");
            item.setTelefono3(obj[4] != null ? obj[4].toString().trim() : "");
            item.setTelefono4(obj[5] != null ? obj[5].toString().trim() : "");
            return item;
        }catch(Exception ex){
            _log.error(ex.getMessage());
            throw new Exception(ex.getMessage());
        }
    }
}
