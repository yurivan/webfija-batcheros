package pe.telefonica.tracer.model;

public class StatusDetail {

    private Integer estado;
    private String descripcion;
    private String fecha;

    public StatusDetail(){

    }

    public StatusDetail(Integer estado, String descripcion, String fecha){
        this.estado = estado;
        this.descripcion = descripcion;
        this.fecha = fecha;
    }

    public void setEstado(Integer estado){
        this.estado = estado;
    }

    public Integer getEstado(){
        return estado;
    }

    public void setDescripcion(String descripcion){
        this.descripcion = descripcion;
    }

    public String getDescripcion(){
        return descripcion;
    }

    public void setFecha(String fecha){
        this.fecha = fecha;
    }

    public String getFecha(){
        return fecha;
    }

}
