package pe.telefonica.tracer.model;

import java.sql.Timestamp;

public class VisorDB {
    private String id;
    private String codigo_pedido;
    private Timestamp fecha_equipo_tecnico;
    private Timestamp fecha_instalado;


    public VisorDB() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public Timestamp getFecha_equipo_tecnico() {
        return fecha_equipo_tecnico;
    }

    public void setFecha_equipo_tecnico(Timestamp fecha_equipo_tecnico) {
        this.fecha_equipo_tecnico = fecha_equipo_tecnico;
    }

    public Timestamp getFecha_instalado() {
        return fecha_instalado;
    }

    public void setFecha_instalado(Timestamp fecha_instalado) {
        this.fecha_instalado = fecha_instalado;
    }
}
