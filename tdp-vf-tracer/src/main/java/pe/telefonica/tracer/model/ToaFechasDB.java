package pe.telefonica.tracer.model;

import java.sql.Timestamp;

public class ToaFechasDB {
    private String id;
    private String peticion;
    private String status_toa;
    private Timestamp insert_date;

    public ToaFechasDB() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPeticion() {
        return peticion;
    }

    public void setPeticion(String peticion) {
        this.peticion = peticion;
    }

    public String getStatus_toa() {
        return status_toa;
    }

    public void setStatus_toa(String status_toa) {
        this.status_toa = status_toa;
    }

    public Timestamp getInsert_date() {
        return insert_date;
    }

    public void setInsert_date(Timestamp insert_date) {
        this.insert_date = insert_date;
    }
}
