package pe.telefonica.tracer.model;

public class PublicCustomerRequest {

	private String documento;
	private String codigoPedido;
	private String telefono3;
	private String telefono4;
	private String direccion2;
	private String correo2;
	private String rangoFechaInicio;
	private String rangoFechaFin;
	private Integer respuesta1;
	private Integer respuesta2;
	private Integer respuesta3;
	private String comentario;

	private String token;


	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getCodigoPedido() {
		return codigoPedido;
	}

	public void setCodigoPedido(String codigoPedido) {
		this.codigoPedido = codigoPedido;
	}

	public String getTelefono3() {
		return telefono3;
	}

	public void setTelefono3(String telefono3) {
		this.telefono3 = telefono3;
	}

	public String getTelefono4() {
		return telefono4;
	}

	public void setTelefono4(String telefono4) {
		this.telefono4 = telefono4;
	}

	public String getDireccion2() {
		return direccion2;
	}

	public void setDireccion2(String direccion2) {
		this.direccion2 = direccion2;
	}

	public String getCorreo2() {
		return correo2;
	}

	public void setCorreo2(String correo2) {
		this.correo2 = correo2;
	}

	public String getRangoFechaInicio() {
		return rangoFechaInicio;
	}

	public void setRangoFechaInicio(String rangoFechaInicio) {
		this.rangoFechaInicio = rangoFechaInicio;
	}

	public String getRangoFechaFin() {
		return rangoFechaFin;
	}

	public void setRangoFechaFin(String rangoFechaFin) {
		this.rangoFechaFin = rangoFechaFin;
	}

	public Integer getRespuesta1() {
		return respuesta1;
	}

	public void setRespuesta1(Integer respuesta1) {
		this.respuesta1 = respuesta1;
	}

	public Integer getRespuesta2() {
		return respuesta2;
	}

	public void setRespuesta2(Integer respuesta2) {
		this.respuesta2 = respuesta2;
	}

	public Integer getRespuesta3() {
		return respuesta3;
	}

	public void setRespuesta3(Integer respuesta3) {
		this.respuesta3 = respuesta3;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
}
