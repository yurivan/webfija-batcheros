package pe.telefonica.tracer.model;

import java.util.List;

public class ClientData {

    private String email;
    private String customerphone;
    private String customerphone2;
    private String correo2;
    private String telefono3;
    private String telefono4;

    private List<String> emailList;
    private List<String> phoneList;

    public ClientData(){
    }

    public ClientData(String email, String customerphone, String customerphone2, String correo2, String telefono3, String telefono4){
        this.email = email;
        this.customerphone = customerphone;
        this.customerphone2 = customerphone2;
        this.correo2 = correo2;
        this.telefono3 = telefono3;
        this.telefono4 = telefono4;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerphone() {
        return customerphone;
    }

    public void setCustomerphone(String customerphone) {
        this.customerphone = customerphone;
    }

    public String getCustomerphone2() {
        return customerphone2;
    }

    public void setCustomerphone2(String customerphone2) {
        this.customerphone2 = customerphone2;
    }

    public String getCorreo2() {
        return correo2;
    }

    public void setCorreo2(String correo2) {
        this.correo2 = correo2;
    }

    public String getTelefono3() {
        return telefono3;
    }

    public void setTelefono3(String telefono3) {
        this.telefono3 = telefono3;
    }

    public String getTelefono4() {
        return telefono4;
    }

    public void setTelefono4(String telefono4) {
        this.telefono4 = telefono4;
    }

    public List<String> getEmailList() {
        return emailList;
    }

    public void setEmailList(List<String> emailList) {
        this.emailList = emailList;
    }

    public List<String> getPhoneList() {
        return phoneList;
    }

    public void setPhoneList(List<String> phoneList) {
        this.phoneList = phoneList;
    }
}
