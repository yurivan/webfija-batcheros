package pe.telefonica.tracer.persistence;

import lombok.Setter;
import lombok.Getter;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TDP_VISOR", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class VisorDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id_visor")
    private String idVisor;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    /*@Column(name = "back")
    private String back;

    @Column(name = "fecha_grabacion")
    private Timestamp recordedDate;

    @Column(name = "id_grabacion")
    private String recordId;*/

    @Transient
    @Column(name = "cliente")
    private String cliente;

    @Transient
    private String email;
    @Transient
    private String phone1;
    @Transient
    private String phone2;

    /*@Transient
    @Column(name = "telefono")
    private String phone;

    @Column(name = "direccion")
    private String address;

    @Column(name = "departamento")
    private String department;

    @Column(name = "dni")
    private String idcard;*/

    @Transient
    @Column(name = "nombre_producto")
    private String nombre_producto;

    @Transient
    @Column(name = "estado_solicitud")
    private String estado_solicitud;

    /*@Column(name = "motivo_estado")
    private String requestStatusReason;

    @Column(name = "speech")
    private String speech;

    @Column(name = "accion")
    private String action;*/

    @Transient
    @Column(name = "operacion_comercial")
    private String operacion_comercial;

    /*@Column(name = "distrito")
    private String district;

    @Column(name = "codigo_llamada")
    private String callCode;

    @Column(name = "sub_producto")
    private String subProduct;

    @Column(name = "tipo_producto")
    private String productType;

    @Column(name = "codigo_gestion")
    private Integer flowCode;

    @Column(name = "codigo_sub_gestion")
    private Integer subFlowCode;

    @Column(name = "flag_dependencia")
    private String dependencyFlag;

    @Column(name = "version_fila")
    private String rowVersion;

    @Column(name = "tipo_documento")
    private String documentType;

    @Column(name = "provincia")
    private String province;

    @Column(name = "fecha_de_llamada")
    private Timestamp callDate;

    @Column(name = "codigo_pedido")
    private String requestCode;

    @Column(name = "version_fila_t")
    private String rowVersionT;

    @Column(name = "sub_producto_equiv")
    private String subProductEquivalece;

    @Column(name = "producto")
    private String product;*/


    @Transient
    @Column(name = "fecha_solicitado")
    private Timestamp fecha_solicitado;

    @Transient
    @Column(name = "fecha_registrado")
    private Timestamp fecha_registrado;

    @Transient
    @Column(name = "fecha_equipo_tecnico")
    private Timestamp fecha_equipo_tecnico;

    @Transient
    @Column(name = "fecha_instalado")
    private Timestamp fecha_instalado;


    @Column(name = "flg_solicitado_email", updatable = true)
    private Integer flg_solicitado_email;

    @Column(name = "flg_registrado_email", updatable = true)
    private Integer flg_registrado_email;

    @Column(name = "flg_equipo_tecnico_email", updatable = true)
    private Integer flg_equipo_tecnico_email;

    @Column(name = "flg_instalado_email", updatable = true)
    private Integer flg_instalado_email;

    @Column(name = "flg_registrado_ret_email", updatable = true)
    private Integer flg_registrado_ret_email;

    @Column(name = "flg_equipo_tecnico_ret_email", updatable = true)
    private Integer flg_equipo_tecnico_ret_email;

    @Column(name = "flg_instalado_ret_email", updatable = true)
    private Integer flg_instalado_ret_email;

    @Column(name = "flg_registrado_caida_email", updatable = true)
    private Integer flg_registrado_caida_email;



    @Column(name = "departamento")
    private String departamento;

    @Column(name = "provincia")
    private String provincia;

    @Column(name = "distrito")
    private String distrito;


    @Transient
    @Column(name = "fecha_grabacion")
    private String fecha_grabacion;

    /*@OneToOne()
    @JoinColumn(name="id_visor", referencedColumnName = "id")
    private OrderDB order;*/

    public VisorDB(){

    }

    public VisorDB(String idVisor, String cliente, String email, String nombre_producto, String phone1, String phone2,
                   String estado_solicitud, String operacion_comercial, String departamento, String provincia, String distrito,
                   String codigo_pedido, Integer flg_solicitado_email, Integer flg_registrado_email, Integer flg_equipo_tecnico_email,
                   Integer flg_instalado_email, Integer flg_registrado_ret_email, Integer flg_equipo_tecnico_ret_email,
                   Integer flg_instalado_ret_email, Integer flg_registrado_caida_email){
        this.cliente = cliente;
        this.email = email;
        this.idVisor = idVisor;
        this.nombre_producto = nombre_producto;
        this.estado_solicitud = estado_solicitud;
        this.operacion_comercial = operacion_comercial;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.departamento = departamento;
        this.provincia = provincia;
        this.distrito = distrito;
        this.codigo_pedido = codigo_pedido;
        this.flg_solicitado_email = flg_solicitado_email;
        this.flg_registrado_email = flg_registrado_email;
        this.flg_equipo_tecnico_email = flg_equipo_tecnico_email;
        this.flg_instalado_email = flg_instalado_email;
        this.flg_registrado_ret_email = flg_registrado_ret_email;
        this.flg_equipo_tecnico_ret_email = flg_equipo_tecnico_ret_email;
        this.flg_instalado_ret_email = flg_instalado_ret_email;
        this.flg_registrado_caida_email = flg_registrado_caida_email;
    }

    public VisorDB(String idVisor, String cliente, String email, String nombre_producto, String phone1, String phone2,
                   String estado_solicitud, String operacion_comercial, String departamento, String provincia, String distrito,
                   String codigo_pedido){
        this.cliente = cliente;
        this.email = email;
        this.idVisor = idVisor;
        this.nombre_producto = nombre_producto;
        this.estado_solicitud = estado_solicitud;
        this.operacion_comercial = operacion_comercial;
        this.phone1 = phone1;
        this.phone2 = phone2;
        this.departamento = departamento;
        this.provincia = provincia;
        this.distrito = distrito;
        this.codigo_pedido = codigo_pedido;
    }

    public VisorDB(String idVisor, String estado_solicitud, Timestamp fecha_registrado, String codigo_pedido) {
        this.idVisor = idVisor;
        this.estado_solicitud = estado_solicitud;
        this.fecha_registrado = fecha_registrado;
        this.codigo_pedido = codigo_pedido;
    }

    public VisorDB(String cliente, String idVisor, String nombre_producto, String estado_solicitud, String operacion_comercial){
        this.cliente = cliente;
        this.idVisor = idVisor;
        this.nombre_producto = nombre_producto;
        this.estado_solicitud = estado_solicitud;
        this.operacion_comercial = operacion_comercial;
    }

    public String getIdVisor() {
        return idVisor;
    }

    public void setIdVisor(String idVisor) {
        this.idVisor = idVisor;
    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    /*public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    public Timestamp getRecordedDate() {
        return recordedDate;
    }

    public void setRecordedDate(Timestamp recordedDate) {
        this.recordedDate = recordedDate;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }*/

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    /*public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }*/

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    public String getEstado_solicitud() {
        return estado_solicitud;
    }

    public void setEstado_solicitud(String estado_solicitud) {
        this.estado_solicitud = estado_solicitud;
    }

    /*public String getRequestStatusReason() {
        return requestStatusReason;
    }

    public void setRequestStatusReason(String requestStatusReason) {
        this.requestStatusReason = requestStatusReason;
    }

    public String getSpeech() {
        return speech;
    }

    public void setSpeech(String speech) {
        this.speech = speech;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }*/

    public String getOperacion_comercial() {
        return operacion_comercial;
    }

    public void setOperacion_comercial(String operacion_comercial) {
        this.operacion_comercial = operacion_comercial;
    }

    /*public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCallCode() {
        return callCode;
    }

    public void setCallCode(String callCode) {
        this.callCode = callCode;
    }

    public String getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(String subProduct) {
        this.subProduct = subProduct;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Integer getFlowCode() {
        return flowCode;
    }

    public void setFlowCode(Integer flowCode) {
        this.flowCode = flowCode;
    }

    public Integer getSubFlowCode() {
        return subFlowCode;
    }

    public void setSubFlowCode(Integer subFlowCode) {
        this.subFlowCode = subFlowCode;
    }

    public String getDependencyFlag() {
        return dependencyFlag;
    }

    public void setDependencyFlag(String dependencyFlag) {
        this.dependencyFlag = dependencyFlag;
    }

    public String getRowVersion() {
        return rowVersion;
    }

    public void setRowVersion(String rowVersion) {
        this.rowVersion = rowVersion;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Timestamp getCallDate() {
        return callDate;
    }

    public void setCallDate(Timestamp callDate) {
        this.callDate = callDate;
    }

    public String getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(String requestCode) {
        this.requestCode = requestCode;
    }

    public String getSubProductEquivalece() {
        return subProductEquivalece;
    }

    public void setSubProductEquivalece(String subProductEquivalece) {
        this.subProductEquivalece = subProductEquivalece;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }*/


    public Timestamp getFecha_solicitado() {
        return fecha_solicitado;
    }

    public void setFecha_solicitado(Timestamp fecha_solicitado) {
        this.fecha_solicitado = fecha_solicitado;
    }

    public Timestamp getFecha_registrado() {
        return fecha_registrado;
    }

    public void setFecha_registrado(Timestamp fecha_registrado) {
        this.fecha_registrado = fecha_registrado;
    }

    public Timestamp getFecha_equipo_tecnico() {
        return fecha_equipo_tecnico;
    }

    public void setFecha_equipo_tecnico(Timestamp fecha_equipo_tecnico) {
        this.fecha_equipo_tecnico = fecha_equipo_tecnico;
    }

    public Timestamp getFecha_instalado() {
        return fecha_instalado;
    }

    public void setFecha_instalado(Timestamp fecha_instalado) {
        this.fecha_instalado = fecha_instalado;
    }


    public Integer getFlg_solicitado_email() {
        return flg_solicitado_email;
    }

    public void setFlg_solicitado_email(Integer flg_solicitado_email) {
        this.flg_solicitado_email = flg_solicitado_email;
    }

    public Integer getFlg_registrado_email() {
        return flg_registrado_email;
    }

    public void setFlg_registrado_email(Integer flg_registrado_email) {
        this.flg_registrado_email = flg_registrado_email;
    }

    public Integer getFlg_equipo_tecnico_email() {
        return flg_equipo_tecnico_email;
    }

    public void setFlg_equipo_tecnico_email(Integer flg_equipo_tecnico_email) {
        this.flg_equipo_tecnico_email = flg_equipo_tecnico_email;
    }

    public Integer getFlg_instalado_email() {
        return flg_instalado_email;
    }

    public void setFlg_instalado_email(Integer flg_instalado_email) {
        this.flg_instalado_email = flg_instalado_email;
    }

    /*public OrderDB getOrder() {
        return order;
    }

    public void setOrder(OrderDB order) {
        this.order = order;
    }*/

    public Integer getFlg_registrado_ret_email() {
        return flg_registrado_ret_email;
    }

    public void setFlg_registrado_ret_email(Integer flg_registrado_ret_email) {
        this.flg_registrado_ret_email = flg_registrado_ret_email;
    }

    public Integer getFlg_equipo_tecnico_ret_email() {
        return flg_equipo_tecnico_ret_email;
    }

    public void setFlg_equipo_tecnico_ret_email(Integer flg_equipo_tecnico_ret_email) {
        this.flg_equipo_tecnico_ret_email = flg_equipo_tecnico_ret_email;
    }

    public Integer getFlg_instalado_ret_email() {
        return flg_instalado_ret_email;
    }

    public void setFlg_instalado_ret_email(Integer flg_instalado_ret_email) {
        this.flg_instalado_ret_email = flg_instalado_ret_email;
    }

    public Integer getFlg_registrado_caida_email() {
        return flg_registrado_caida_email;
    }

    public void setFlg_registrado_caida_email(Integer flg_registrado_caida_email) {
        this.flg_registrado_caida_email = flg_registrado_caida_email;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }
}
