package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.PublicCustomerDataDB;
import pe.telefonica.tracer.persistence.PublicCustomerRatingDB;

@Transactional()
public interface PublicCustomerRatingRepository extends JpaRepository<PublicCustomerRatingDB, Long> {

    @Query(value = "SELECT pc.id, pc.documento, pc.codigo_pedido, pc.respuesta1, pc.respuesta2, pc.respuesta3, pc.comentario, pc.insert_update" +
            " FROM ibmx_a07e6d02edaf552.public_customer_rating pc WHERE pc.documento = :documento AND pc.codigo_pedido = :codigoPedido", nativeQuery = true)
    public PublicCustomerRatingDB getPublicCustomerRating(@Param("documento") final String documento, @Param("codigoPedido") final String codigoPedido);

}

