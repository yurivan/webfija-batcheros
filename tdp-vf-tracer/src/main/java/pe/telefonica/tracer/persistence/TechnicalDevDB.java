package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "DEVUELTA_TECNICA", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TechnicalDevDB {

    @Id
    @Column(updatable = false, name = "id")
    private Integer id;

    @Column(name = "numero_peticion")
    private String numero_peticion;

    @Column(name = "flg_email")
    private Integer flg_email;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero_peticion() {
        return numero_peticion;
    }

    public void setNumero_peticion(String numero_peticion) {
        this.numero_peticion = numero_peticion;
    }

    public Integer getFlg_email() {
        return flg_email;
    }

    public void setFlg_email(Integer flg_email) {
        this.flg_email = flg_email;
    }
}
