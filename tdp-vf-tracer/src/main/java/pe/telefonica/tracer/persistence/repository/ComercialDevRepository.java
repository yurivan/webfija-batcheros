package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ComercialDevDB;
import pe.telefonica.tracer.persistence.TraceabilityAccessLogDB;

import java.util.List;

@Transactional()
public interface ComercialDevRepository extends JpaRepository<ComercialDevDB, Long> {

    @Query(value = "SELECT dc.id, dc.requerimiento, dc.flg_email FROM ibmx_a07e6d02edaf552.devuelta_comercial dc WHERE dc.requerimiento in :requerimentList", nativeQuery = true)
    public List<ComercialDevDB> getComercialDev(@Param("requerimentList") List<String> requerimentList);

    @Query(value = "SELECT dc.id, dc.requerimiento, (dc.creado - INTERVAL '2 days') creado FROM ibmx_a07e6d02edaf552.devuelta_comercial dc " +
            " WHERE dc.requerimiento = :codigoPedido AND creado < (now() - INTERVAL '2 days')" +
            " AND dc.requerimiento not in " +
            " (select codigo_pedido from ibmx_a07e6d02edaf552.pedidos_en_caida where codigo_pedido = :codigoPedido" +
            " and tipo_caida in ('DEV_COM_CL','DEV_COM_NO') and estado = true)" +
            " AND dc.requerimiento not in " +
            " (select codigo_pedido from ibmx_a07e6d02edaf552.tdp_visor where codigo_pedido = :codigoPedido" +
            " and fecha_instalado is not null)" +
            " order by creado asc limit 1", nativeQuery = true)
    public ComercialDevDB getComercialDevRetraso48(@Param("codigoPedido") String codigoPedido);

}

