package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.PedidoCaidaDB;
import pe.telefonica.tracer.persistence.PedidoEstadoDB;
import pe.telefonica.tracer.persistence.ToaStatusDB;

import java.util.List;

@Transactional()
public interface ConsultarFechasToaRepository extends JpaRepository<ToaStatusDB,Long> {

    @Query(value = "SELECT * FROM ibmx_a07e6d02edaf552.toa_status ts " +
                "WHERE ts.peticion in :codigoPedidoList and ts.status_toa = 'WO_COMPLETED'", nativeQuery = true)
    public List<ToaStatusDB> getPedidoEstadoList(@Param("codigoPedidoList") List<String> codigoPedidoList);




}
