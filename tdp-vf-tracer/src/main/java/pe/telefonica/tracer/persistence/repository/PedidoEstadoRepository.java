package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ComercialDevDB;
import pe.telefonica.tracer.persistence.PedidoEstadoDB;

import java.util.List;

@Transactional()
public interface PedidoEstadoRepository extends JpaRepository<PedidoEstadoDB, Long> {

    @Query(value = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_service_pedido sp" +
            " WHERE sp.pedido_codigo in :codigoPedidoList", nativeQuery = true)
    public List<PedidoEstadoDB> getPedidoEstadoList(@Param("codigoPedidoList") List<String> codigoPedidoList);



}

