package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.PedidoCaidaDB;

import java.util.List;

@Transactional()
public interface PedidoCaidaRepository extends JpaRepository<PedidoCaidaDB, Long> {

    @Query(value = "SELECT p.id, p.codigo_pedido, p.tipo_caida, p.intentos, p.estado, p.insert_update, p.flg_email " +
            " FROM ibmx_a07e6d02edaf552.pedidos_en_caida p WHERE p.codigo_pedido = :codigoPedido" +
            " AND p.tipo_caida in ('RET_ASIGN','DEV_COM_CL','DEV_COM_NO','DEV_TEC') AND (p.estado = true OR p.estado is NULL)",
            nativeQuery = true)
    public List<PedidoCaidaDB> getCaidas(@Param("codigoPedido") final String codigoPedido);

    @Query(value = "SELECT p.id, p.codigo_pedido, p.tipo_caida, p.intentos, p.estado, p.insert_update, p.flg_email " +
            " FROM ibmx_a07e6d02edaf552.pedidos_en_caida p WHERE " +
            " p.tipo_caida = 'DEV_COM_NO' AND (p.estado = true OR p.estado is NULL) AND codigo_pedido in :requerimentList",
            nativeQuery = true)
    public List<PedidoCaidaDB> getCaidasComercialNoContacto(@Param("requerimentList") List<String> requerimentList);

    @Query(value = "SELECT p.id, p.codigo_pedido, p.tipo_caida, p.intentos, p.estado, p.insert_update, p.flg_email " +
            " FROM ibmx_a07e6d02edaf552.pedidos_en_caida p WHERE " +
            " p.tipo_caida = 'DEV_COM_CL' AND (p.estado = true OR p.estado is NULL) AND codigo_pedido in :requerimentList",
            nativeQuery = true)
    public List<PedidoCaidaDB> getCaidasComercialAPedido(@Param("requerimentList") List<String> requerimentList);

    @Query(value = "SELECT p.id, p.codigo_pedido, p.tipo_caida, p.intentos, p.estado, p.insert_update, p.flg_email " +
            " FROM ibmx_a07e6d02edaf552.pedidos_en_caida p WHERE " +
            " p.tipo_caida = 'DEV_TEC' AND (p.estado = true OR p.estado is NULL) AND codigo_pedido in :requerimentList",
            nativeQuery = true)
    public List<PedidoCaidaDB> getCaidasTecnica(@Param("requerimentList") List<String> requerimentList);

    @Query(value = "SELECT p.id, p.codigo_pedido, p.tipo_caida, p.intentos, p.estado, p.insert_update, p.flg_email " +
            " FROM ibmx_a07e6d02edaf552.pedidos_en_caida p WHERE " +
            " p.tipo_caida = 'RET_ASIGN' AND codigo_pedido in :requerimentList",
            nativeQuery = true)
    public List<PedidoCaidaDB> getCaidasRetrasoAsignacion(@Param("requerimentList") List<String> requerimentList);

    /*@Query(value = "(" +
            "select id_visor,estado_solicitud,fecha_registrado,codigo_pedido " +
            " from ibmx_a07e6d02edaf552.tdp_visor where codigo_pedido " +
            " not in (select codigo_pedido from ibmx_a07e6d02edaf552.pedidos_en_caida where tipo_caida='RET_ASIGN') " +
            " and codigo_pedido is not null and codigo_pedido <> ''" +
            " and fecha_registrado < (now() - INTERVAL '3 days')" +
            " and fecha_equipo_tecnico is null and fecha_instalado is null LIMIT 2" +
            ")", nativeQuery = true)
    public List<Object[]> getValidateCaidaAsignacion();*/

    @Query(value = "select id_visor,estado_solicitud,fecha_registrado,codigo_pedido " +
            " from ibmx_a07e6d02edaf552.tdp_visor where codigo_pedido " +
            " not in (SELECT pedido_codigo FROM ibmx_a07e6d02edaf552.tdp_service_pedido where pedido_estado_code in ('CG','FI','TE','02','04','05')) " +
            " and codigo_pedido is not null and codigo_pedido <> ''" +
            " and fecha_registrado < (now() AT TIME ZONE 'America/Lima' - INTERVAL '3 days')" +
            " and fecha_registrado > (now() AT TIME ZONE 'America/Lima' - INTERVAL '2 months')" +
            " and fecha_equipo_tecnico is null and fecha_instalado is null " +
            " order by fecha_registrado desc LIMIT 100", nativeQuery = true)
    public List<Object[]> getValidateCaidaAsignacion();


    @Query(value = "SELECT id, codigo_pedido, tipo_caida, intentos, estado, insert_update, flg_email FROM ibmx_a07e6d02edaf552.pedidos_en_caida " +
            " WHERE tipo_caida IN ('DEV_COM_CL','DEV_COM_NO','DEV_TEC') AND  intentos <= 6  AND estado = false Order by 1 LIMIT 100 ", nativeQuery = true)
    public List<PedidoCaidaDB> getCaidas();

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial, " +
            " v.departamento, v.provincia, v.distrito, v.codigo_pedido FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.estado_solicitud = 'INGRESADO' AND v.codigo_pedido IS NOT null AND v.codigo_pedido <> ''" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            " AND lower(v.operacion_comercial) in (lower('ALTA PURA'),lower('ALT'))" +
            " AND v.codigo_pedido IN" +
            " (select distinct(requerimiento) from ibmx_a07e6d02edaf552.devuelta_comercial" +
            " where creado < (now() AT TIME ZONE 'America/Lima') - interval '48 hours'" +
            " and creado > date(now() AT TIME ZONE 'America/Lima') - interval '72 hours')" +
            " AND v.codigo_pedido NOT IN" +
            " (select codigo_pedido from ibmx_a07e6d02edaf552.pedidos_en_caida " +
            " where tipo_caida in ('DEV_TEC','DEV_COM_CL','DEV_COM_NO','RET_DC48')) LIMIT 100", nativeQuery = true)
    public List<Object[]> getDC48();
}

