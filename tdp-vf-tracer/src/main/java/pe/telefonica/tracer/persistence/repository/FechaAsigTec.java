package pe.telefonica.tracer.persistence.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.business.impl.ValidateDropServiceImpl;
import pe.telefonica.tracer.model.ToaFechasDB;
import pe.telefonica.tracer.model.VisorDB;


import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class FechaAsigTec {

    @Autowired
    private DataSource datasource;

    private static final Logger _log = LoggerFactory.getLogger(FechaAsigTec.class);

    public List<VisorDB> allFechaSinAsinar(){
        List<VisorDB> response = new ArrayList<>();

        try (Connection con = datasource.getConnection()) {


            String sql = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_visor WHERE codigo_pedido != '' " +
                    "and fecha_instalado is not null and fecha_equipo_tecnico is null ORDER BY id_visor asc LIMIT 10";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    VisorDB obj = new VisorDB();
                    obj.setId(rs.getString("id_visor"));
                    obj.setCodigo_pedido(rs.getString("codigo_pedido"));
                    obj.setFecha_equipo_tecnico(rs.getTimestamp("fecha_equipo_tecnico"));
                    obj.setFecha_instalado(rs.getTimestamp("fecha_instalado"));

                    response.add(obj);
                }
            }

            //ps.executeUpdate();
        } catch (Exception e) {
            _log.info("Error insert", e);
        }

        return response;

    }

    public List<VisorDB> allFechaVaciaEnVisor(){
        List<VisorDB> response = new ArrayList<>();

        try (Connection con = datasource.getConnection()) {


            String sql = "SELECT id_visor,codigo_pedido,fecha_equipo_tecnico,fecha_instalado FROM ibmx_a07e6d02edaf552.tdp_visor WHERE codigo_pedido != '' " +
                    "and fecha_instalado is null and fecha_equipo_tecnico is null ORDER BY id_visor asc LIMIT 100";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    VisorDB obj = new VisorDB();
                    obj.setId(rs.getString("id_visor"));
                    obj.setCodigo_pedido(rs.getString("codigo_pedido"));
                    obj.setFecha_equipo_tecnico(rs.getTimestamp("fecha_equipo_tecnico"));
                    obj.setFecha_instalado(rs.getTimestamp("fecha_instalado"));

                    response.add(obj);
                }
            }

            //ps.executeUpdate();
        } catch (Exception e) {
            _log.info("Error insert", e);
        }

        return response;

    }

    public List<ToaFechasDB> allFechaSinAsinar(List<VisorDB> items){
        List<ToaFechasDB> response = new ArrayList<>();


        try (Connection con = datasource.getConnection()) {



            for (int i = 0; i<items.size();i++){
                String sql = "SELECT id,peticion,status_toa,insert_date FROM ibmx_a07e6d02edaf552.toa_status " +
                        "WHERE peticion = '" + items.get(i).getCodigo_pedido() + "'";


               try (PreparedStatement ps = con.prepareStatement(sql)) {

                   ResultSet rs = ps.executeQuery();
                   while (rs.next()) {
                       ToaFechasDB obj = new ToaFechasDB();
                       obj.setId(rs.getString("id"));
                       obj.setPeticion(rs.getString("peticion"));
                       obj.setStatus_toa(rs.getString("status_toa"));
                       obj.setInsert_date(rs.getTimestamp("insert_date"));
                       response.add(obj);
                   }
               }
           }

        } catch (Exception e) {
            _log.info("Error insert", e);
        }

        return response;

    }

    public List<ToaFechasDB> allFechaSinAsinar2(List<VisorDB> items){
        List<ToaFechasDB> response = new ArrayList<>();

        try (Connection con = datasource.getConnection()) {


            for (int i = 0; i<items.size();i++){
                String sql = "SELECT * FROM ibmx_a07e6d02edaf552.toa_status " +
                        "WHERE peticion = '" + items.get(i).getCodigo_pedido() + "' and status_toa = 'WO_COMPLETED'";


                try (PreparedStatement ps = con.prepareStatement(sql)) {

                    ResultSet rs = ps.executeQuery();
                    while (rs.next()) {
                        ToaFechasDB obj = new ToaFechasDB();
                        obj.setId(rs.getString("id"));
                        obj.setPeticion(rs.getString("peticion"));
                        obj.setStatus_toa(rs.getString("status_toa"));
                        obj.setInsert_date(rs.getTimestamp("insert_date"));
                        response.add(obj);
                        break;
                    }
                }
            }

        } catch (Exception e) {
            _log.info("Error insert", e);
        }

        return response;

    }


    public List<VisorDB> allFechaInstaladoNula(){
        List<VisorDB> response = new ArrayList<>();

        try (Connection con = datasource.getConnection()) {


            String sql = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_visor WHERE codigo_pedido != '' " +
              "and fecha_instalado is null and fecha_equipo_tecnico is not null ORDER BY id_visor asc LIMIT 30";

            try (PreparedStatement ps = con.prepareStatement(sql)) {

                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    VisorDB obj = new VisorDB();
                    obj.setId(rs.getString("id_visor"));
                    obj.setCodigo_pedido(rs.getString("codigo_pedido"));
                    obj.setFecha_equipo_tecnico(rs.getTimestamp("fecha_equipo_tecnico"));
                    obj.setFecha_instalado(rs.getTimestamp("fecha_instalado"));

                    response.add(obj);
                }
            }

            //ps.executeUpdate();
        } catch (Exception e) {
            _log.info("Error insert", e);
        }

        return response;

    }



    public void updateAsigTecFecha( String peticion, Timestamp FechaAsigTec){
        try (Connection con = datasource.getConnection()) {
            String sql = "update ibmx_a07e6d02edaf552.tdp_visor " +
                    "set fecha_equipo_tecnico = ?, fech_act_tracer = '1' where codigo_pedido = ?";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setTimestamp(1, FechaAsigTec);
            ps.setString(2, peticion);


            ps.executeUpdate();
        } catch (Exception e) {
            _log.error("Error insert updateAsigTecFecha status", e);
        }
    }

    public void updateInstaladoFecha( String peticion, Timestamp FechaInstalado){
        try (Connection con = datasource.getConnection()) {
            String sql = "update ibmx_a07e6d02edaf552.tdp_visor " +
                    "set fecha_instalado = ?, fech_act_tracer = '1' where codigo_pedido = ?";

            PreparedStatement ps = con.prepareStatement(sql);

            ps.setTimestamp(1, FechaInstalado);
            ps.setString(2, peticion);


            ps.executeUpdate();
        } catch (Exception e) {
            _log.error("Error insert updateInstaladoFecha status", e);
        }
    }




}
