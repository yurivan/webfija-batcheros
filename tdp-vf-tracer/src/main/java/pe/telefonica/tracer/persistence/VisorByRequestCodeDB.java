package pe.telefonica.tracer.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import pe.telefonica.tracer.model.StatusDetail;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.util.List;


@Entity
@Table(name = "TDP_VISOR", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class VisorByRequestCodeDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id_visor")
    private String id;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    @Column(name = "fecha_grabacion")
    private String fecha_grabacion;

    @Column(name = "cliente")
    private String cliente;

    @Column(name = "telefono")
    private String telefono;

    @Transient
    private String telefono2;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "departamento")
    private String departamento;

    @Transient
    private String documento;

    @Column(name = "nombre_producto")
    private String nombre_producto;

    //@Transient
    //private String nombre_producto2;

    @Transient
    private String velocidad;

    @Transient
    private String equipamientoInternet;

    @Transient
    private String equipamientoTv;

    @Transient
    private Integer estado_solicitud;

    @JsonIgnore
    private String estado_solicitud_str;

    @Transient
    private String motivo_caida;

    @Transient
    private Integer sub_estado_solicitud;

    @Column(name = "operacion_comercial")
    private String operacion_comercial;

    @Column(name = "distrito")
    private String distrito;

    /*@Column(name = "sub_producto")
    private String sub_producto;*/

    @Column(name = "tipo_documento")
    private String tipo_documento;

    @Column(name = "provincia")
    private String provincia;

    //@Column(name = "fecha_de_llamada")
    //private String fecha_de_llamada;

    @Column(name = "nombre_producto_fuente")
    private String nombre_producto_fuente;

    @Column(name = "fecha_solicitado")
    private String fecha_solicitado;

    @Column(name = "fecha_registrado")
    private String fecha_registrado;

    @Column(name = "fecha_equipo_tecnico")
    private String fecha_equipo_tecnico;

    @Column(name = "fecha_instalado")
    private String fecha_instalado;

    //Nuevos campos

    @Transient
    private String correo;
    @Transient
    private Boolean primera;

    @Transient
    private String telefono3;
    @Transient
    private String telefono4;
    @Transient
    private String direccion2;
    @Transient
    private String correo2;
    @Transient
    private Boolean valoracion;
    @Transient
    private String coordenada_x;
    @Transient
    private String coordenada_y;

    @Transient
    private Integer devuelta;

    @Transient
    private boolean reagenda_permitida;
    @Transient
    private String fecha_reagendado_inicio;
    @Transient
    private String fecha_reagendado_fin;

    @Transient
    private List<StatusDetail> detalle;


    public VisorByRequestCodeDB(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono2() {
        return telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    /*public String getNombre_producto2() {
        return nombre_producto2;
    }

    public void setNombre_producto2(String nombre_producto2) {
        this.nombre_producto2 = nombre_producto2;
    }*/

    public String getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(String velocidad) {
        this.velocidad = velocidad;
    }

    public String getEquipamientoInternet() {
        return equipamientoInternet;
    }

    public void setEquipamientoInternet(String equipamientoInternet) {
        this.equipamientoInternet = equipamientoInternet;
    }

    public String getEquipamientoTv() {
        return equipamientoTv;
    }

    public void setEquipamientoTv(String equipamientoTv) {
        this.equipamientoTv = equipamientoTv;
    }

    public Integer getEstado_solicitud() {
        return estado_solicitud;
    }

    public void setEstado_solicitud(Integer estado_solicitud) {
        this.estado_solicitud = estado_solicitud;
    }

    public String getEstado_solicitud_str() {
        return estado_solicitud_str;
    }

    public void setEstado_solicitud_str(String estado_solicitud_str) {
        this.estado_solicitud_str = estado_solicitud_str;
    }

    public String getMotivo_caida() {
        return motivo_caida;
    }

    public void setMotivo_caida(String motivo_caida) {
        this.motivo_caida = motivo_caida;
    }

    public Integer getSub_estado_solicitud() {
        return sub_estado_solicitud;
    }

    public void setSub_estado_solicitud(Integer sub_estado_solicitud) {
        this.sub_estado_solicitud = sub_estado_solicitud;
    }

    public String getOperacion_comercial() {
        return operacion_comercial;
    }

    public void setOperacion_comercial(String operacion_comercial) {
        this.operacion_comercial = operacion_comercial;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    /*public String getSub_producto() {
        return sub_producto;
    }

    public void setSub_producto(String sub_producto) {
        this.sub_producto = sub_producto;
    }*/

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    /*public String getFecha_de_llamada() {
        return fecha_de_llamada;
    }

    public void setFecha_de_llamada(String fecha_de_llamada) {
        this.fecha_de_llamada = fecha_de_llamada;
    }*/

    public String getNombre_producto_fuente() {
        return nombre_producto_fuente;
    }

    public void setNombre_producto_fuente(String nombre_producto_fuente) {
        this.nombre_producto_fuente = nombre_producto_fuente;
    }

    public String getFecha_solicitado() {
        return fecha_solicitado;
    }

    public void setFecha_solicitado(String fecha_solicitado) {
        this.fecha_solicitado = fecha_solicitado;
    }

    public String getFecha_registrado() {
        return fecha_registrado;
    }

    public void setFecha_registrado(String fecha_registrado) {
        this.fecha_registrado = fecha_registrado;
    }

    public String getFecha_equipo_tecnico() {
        return fecha_equipo_tecnico;
    }

    public void setFecha_equipo_tecnico(String fecha_equipo_tecnico) {
        this.fecha_equipo_tecnico = fecha_equipo_tecnico;
    }

    public String getFecha_instalado() {
        return fecha_instalado;
    }

    public void setFecha_instalado(String fecha_instalado) {
        this.fecha_instalado = fecha_instalado;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Boolean getPrimera() {
        return primera;
    }

    public void setPrimera(Boolean primera) {
        this.primera = primera;
    }

    public String getTelefono3() {
        return telefono3;
    }

    public void setTelefono3(String telefono3) {
        this.telefono3 = telefono3;
    }

    public String getTelefono4() {
        return telefono4;
    }

    public void setTelefono4(String telefono4) {
        this.telefono4 = telefono4;
    }

    public String getDireccion2() {
        return direccion2;
    }

    public void setDireccion2(String direccion2) {
        this.direccion2 = direccion2;
    }

    public String getCorreo2() {
        return correo2;
    }

    public void setCorreo2(String correo2) {
        this.correo2 = correo2;
    }

    public Boolean getValoracion() {
        return valoracion;
    }

    public void setValoracion(Boolean valoracion) {
        this.valoracion = valoracion;
    }

    public String getCoordenada_x() {
        return coordenada_x;
    }

    public void setCoordenada_x(String coordenada_x) {
        this.coordenada_x = coordenada_x;
    }

    public String getCoordenada_y() {
        return coordenada_y;
    }

    public void setCoordenada_y(String coordenada_y) {
        this.coordenada_y = coordenada_y;
    }

    public Integer getDevuelta() {
        return devuelta;
    }

    public void setDevuelta(Integer devuelta) {
        this.devuelta = devuelta;
    }

    public boolean getReagenda_permitida() {
        return reagenda_permitida;
    }

    public void setReagenda_permitida(boolean reagenda_permitida) {
        this.reagenda_permitida = reagenda_permitida;
    }

    public String getFecha_reagendado_inicio() {
        return fecha_reagendado_inicio;
    }

    public void setFecha_reagendado_inicio(String fecha_reagendado_inicio) {
        this.fecha_reagendado_inicio = fecha_reagendado_inicio;
    }

    public String getFecha_reagendado_fin() {
        return fecha_reagendado_fin;
    }

    public void setFecha_reagendado_fin(String fecha_reagendado_fin) {
        this.fecha_reagendado_fin = fecha_reagendado_fin;
    }

    public List<StatusDetail> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<StatusDetail> detalle) {
        this.detalle = detalle;
    }
}
