package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "PUBLIC_CUSTOMER_RESCHEDULED", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PublicCustomerRescheduledDB {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    @Column(updatable = false, name = "id")
    private Integer id;

    @Column(name = "documento")
    private String documento;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    @Column(name = "rango_fecha_inicio")
    private String rango_fecha_inicio;

    @Column(name = "rango_fecha_fin")
    private String rango_fecha_fin;

    @Column(name = "insert_update")
    private Timestamp insert_update;

    public PublicCustomerRescheduledDB(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public String getRango_fecha_inicio() {
        return rango_fecha_inicio;
    }

    public void setRango_fecha_inicio(String rango_fecha_inicio) {
        this.rango_fecha_inicio = rango_fecha_inicio;
    }

    public String getRango_fecha_fin() {
        return rango_fecha_fin;
    }

    public void setRango_fecha_fin(String rango_fecha_fin) {
        this.rango_fecha_fin = rango_fecha_fin;
    }

    public Timestamp getInsert_update() {
        return insert_update;
    }

    public void setInsert_update(Timestamp insert_update) {
        this.insert_update = insert_update;
    }
}
