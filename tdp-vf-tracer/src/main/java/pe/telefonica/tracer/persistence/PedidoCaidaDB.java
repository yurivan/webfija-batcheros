package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "PEDIDOS_EN_CAIDA", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PedidoCaidaDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
   //private String id;
    private Integer id;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    @Column(name = "tipo_caida")
    private String tipo_caida;

    @Column(name = "intentos")
    private Integer intentos;

    @Column(name = "estado")
    private Boolean estado;

    @Column(name = "insert_update")
    private Timestamp insert_update;

    @Column(name = "flg_email")
    private Integer flg_email;

    public PedidoCaidaDB(){

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public String getTipo_caida() {
        return tipo_caida;
    }

    public void setTipo_caida(String tipo_caida) {
        this.tipo_caida = tipo_caida;
    }

    public Integer getIntentos() {
        return intentos;
    }

    public void setIntentos(Integer intentos) {
        this.intentos = intentos;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Timestamp getInsert_update() {
        return insert_update;
    }

    public void setInsert_update(Timestamp insert_update) {
        this.insert_update = insert_update;
    }

    public Integer getFlg_email() {
        return flg_email;
    }

    public void setFlg_email(Integer flg_email) {
        this.flg_email = flg_email;
    }
}
