package pe.telefonica.tracer.persistence.repository.impl;


import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import pe.telefonica.tracer.persistence.repository.VisorRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class VisorRepositoryImpl{

    @PersistenceContext
    private EntityManager em;

    private VisorRepositoryImpl() {
    }

    public List<Object[]> getSolicitadoAltaFiltrado(){

        List<Object[]> list = em.createNativeQuery("SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END cliente, c.email, v.nombre_producto," +
                " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
                " v.estado_solicitud, v.operacion_comercial" +
                " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
                " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
                " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
                " WHERE v.fecha_solicitado is not null AND v.flg_solicitado_email is null AND v.estado_solicitud = 'PENDIENTE'" +
                " AND (" +
                " (v.cliente is not null AND trim(v.cliente) <> '') OR" +
                " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null" +
                " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
                " ) AND c.email is not null AND trim(c.email) <> ''" +
                " AND lower(v.operacion_comercial) in " +
                " (lower('ALTA PURA'),lower('ALT'))" +
                " AND v.fecha_grabacion > (now() - INTERVAL '3 months')" +
                //" AND v.dni = '70540625'" +
                " ORDER BY v.id_visor DESC").getResultList();
        return list;

    }
}
