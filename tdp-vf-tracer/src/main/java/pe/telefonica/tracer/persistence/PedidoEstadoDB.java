package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TDP_SERVICE_PEDIDO", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PedidoEstadoDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "pedido_codigo")
    private String pedido_codigo;

    @Column(name = "pedido_type")
    private String pedido_type;

    @Column(name = "pedido_estado_code")
    private String pedido_estado_code;

    @Column(name = "pedido_estado")
    private String pedido_estado;

    @Column(name = "auditoria_create")
    private Timestamp auditoria_create;

    @Column(name = "auditoria_modify")
    private Timestamp auditoria_modify;

    public PedidoEstadoDB(){

    }

    public String getPedido_codigo() {
        return pedido_codigo;
    }

    public void setPedido_codigo(String pedido_codigo) {
        this.pedido_codigo = pedido_codigo;
    }

    public String getPedido_type() {
        return pedido_type;
    }

    public void setPedido_type(String pedido_type) {
        this.pedido_type = pedido_type;
    }

    public String getPedido_estado_code() {
        return pedido_estado_code;
    }

    public void setPedido_estado_code(String pedido_estado_code) {
        this.pedido_estado_code = pedido_estado_code;
    }

    public String getPedido_estado() {
        return pedido_estado;
    }

    public void setPedido_estado(String pedido_estado) {
        this.pedido_estado = pedido_estado;
    }

    public Timestamp getAuditoria_create() {
        return auditoria_create;
    }

    public void setAuditoria_create(Timestamp auditoria_create) {
        this.auditoria_create = auditoria_create;
    }

    public Timestamp getAuditoria_modify() {
        return auditoria_modify;
    }

    public void setAuditoria_modify(Timestamp auditoria_modify) {
        this.auditoria_modify = auditoria_modify;
    }
}
