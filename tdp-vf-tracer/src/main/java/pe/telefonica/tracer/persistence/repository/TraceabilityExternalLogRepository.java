package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.TraceabilityExternalLogDB;

@Transactional()
public interface TraceabilityExternalLogRepository extends JpaRepository<TraceabilityExternalLogDB, Long> {



}

