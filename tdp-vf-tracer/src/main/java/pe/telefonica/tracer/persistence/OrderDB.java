package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "ORDER", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderDB {

    @Id
    @Column(updatable = false, name = "id")
    private String idOrder;

    @OneToOne()
    @JoinColumn(name="customerid", referencedColumnName = "id")
    private CustomerDB customer;

    @OneToOne()
    @JoinColumn(name="id", referencedColumnName = "orderid")
    private OrderDetailDB orderDetail;


    public String getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(String idOrder) {
        this.idOrder = idOrder;
    }

    public CustomerDB getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDB customer) {
        this.customer = customer;
    }

    public OrderDetailDB getOrderDetail() {
        return orderDetail;
    }

    public void setOrderDetail(OrderDetailDB orderDetail) {
        this.orderDetail = orderDetail;
    }
}
