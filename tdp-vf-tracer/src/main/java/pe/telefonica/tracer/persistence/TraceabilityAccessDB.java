package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "TRACEABILITY_ACCESS", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TraceabilityAccessDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    public static Integer TYPE_WEB_PUBLIC = 1;
    public static Integer TYPE_TOA = 2;

    @Id
    @Column(updatable = false, name = "id")
    private Integer id;

    @Column(name = "token")
    private String token;

    public TraceabilityAccessDB(){

    }

    public void setId(Integer id){
        this.id = id;
    }

    public Integer getId(){
        return id;
    }

    public void setToken(String token){
        this.token = token;
    }

    public String getToken(){
        return token;
    }

}
