package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TDP_VISOR", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class VisorByIdDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id_visor")
    private String id;

    @Column(name = "fecha_grabacion")
    private String fecha_grabacion;

    @Column(name = "cliente")
    private String cliente;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "departamento")
    private String departamento;

    @Column(name = "dni")
    private String dni;

    @Column(name = "nombre_producto")
    private String nombre_producto;

    //@Transient
    //private String nombre_producto2;

    @Column(name = "estado_solicitud")
    private String estado_solicitud;

    @Column(name = "motivo_estado")
    private String motivo_estado;

    @Column(name = "operacion_comercial")
    private String operacion_comercial;

    @Column(name = "distrito")
    private String distrito;

    /*@Column(name = "sub_producto")
    private String sub_producto;*/

    @Column(name = "tipo_documento")
    private String tipo_documento;

    @Column(name = "provincia")
    private String provincia;

    @Column(name = "fecha_de_llamada")
    private String fecha_de_llamada;

    @Column(name = "nombre_producto_fuente")
    private String nombre_producto_fuente;

    @Column(name = "fecha_solicitado")
    private String fecha_solicitado;

    @Column(name = "fecha_registrado")
    private String fecha_registrado;

    @Column(name = "fecha_equipo_tecnico")
    private String fecha_equipo_tecnico;

    @Column(name = "fecha_instalado")
    private String fecha_instalado;

    @Column(name = "estado_anterior")
    private String estado_anterior;

    public VisorByIdDB(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFecha_grabacion() {
        return fecha_grabacion;
    }

    public void setFecha_grabacion(String fecha_grabacion) {
        this.fecha_grabacion = fecha_grabacion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }

    /*public String getNombre_producto2() {
        return nombre_producto2;
    }

    public void setNombre_producto2(String nombre_producto2) {
        this.nombre_producto2 = nombre_producto2;
    }*/

    public String getEstado_solicitud() {
        return estado_solicitud;
    }

    public void setEstado_solicitud(String estado_solicitud) {
        this.estado_solicitud = estado_solicitud;
    }

    public String getMotivo_estado() {
        return motivo_estado;
    }

    public void setMotivo_estado(String motivo_estado) {
        this.motivo_estado = motivo_estado;
    }

    public String getOperacion_comercial() {
        return operacion_comercial;
    }

    public void setOperacion_comercial(String operacion_comercial) {
        this.operacion_comercial = operacion_comercial;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    /*public String getSub_producto() {
        return sub_producto;
    }

    public void setSub_producto(String sub_producto) {
        this.sub_producto = sub_producto;
    }*/

    public String getTipo_documento() {
        return tipo_documento;
    }

    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getFecha_de_llamada() {
        return fecha_de_llamada;
    }

    public void setFecha_de_llamada(String fecha_de_llamada) {
        this.fecha_de_llamada = fecha_de_llamada;
    }

    public String getNombre_producto_fuente() {
        return nombre_producto_fuente;
    }

    public void setNombre_producto_fuente(String nombre_producto_fuente) {
        this.nombre_producto_fuente = nombre_producto_fuente;
    }

    public String getFecha_solicitado() {
        return fecha_solicitado;
    }

    public void setFecha_solicitado(String fecha_solicitado) {
        this.fecha_solicitado = fecha_solicitado;
    }

    public String getFecha_registrado() {
        return fecha_registrado;
    }

    public void setFecha_registrado(String fecha_registrado) {
        this.fecha_registrado = fecha_registrado;
    }

    public String getFecha_equipo_tecnico() {
        return fecha_equipo_tecnico;
    }

    public void setFecha_equipo_tecnico(String fecha_equipo_tecnico) {
        this.fecha_equipo_tecnico = fecha_equipo_tecnico;
    }

    public String getFecha_instalado() {
        return fecha_instalado;
    }

    public void setFecha_instalado(String fecha_instalado) {
        this.fecha_instalado = fecha_instalado;
    }

    public String getEstado_anterior() {
        return estado_anterior;
    }

    public void setEstado_anterior(String estado_anterior) {
        this.estado_anterior = estado_anterior;
    }
}
