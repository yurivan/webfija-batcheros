package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.VisorDB;

import java.util.List;

@Transactional(/*propagation = Propagation.MANDATORY*/)
public interface VisorRepository extends JpaRepository<VisorDB, Long> {
    /*@Query("select u from VisorDB u where u.callDate BETWEEN :start AND :end")
    public List<VisorDB> getRangeRows(@Param("start") final Timestamp start , @Param("end") final Timestamp end);*/

    /*@Query("SELECT new VisorDB((c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) as customer, c.email, v.idVisor, v.productName, v.requestStatus, v.commercialOperation," +
            " CASE WHEN v.phone is null OR v.phone = '' THEN c.phone1 ELSE v.phone END as phone, c.phone2)" +
            " FROM VisorDB v" +
            " INNER JOIN v.order o " +
            " INNER JOIN o.customer c " +
            " WHERE v.requestedDate is not null AND v.mailRequested is null AND v.requestStatus = 'PENDIENTE'" +
            " AND c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null AND c.email is not null" +
            " AND c.firstname <> '' AND c.lastname1 <> '' AND c.lastname2 <> '' AND c.email <> ''" +
            " AND lower(v.commercialOperation) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            " ORDER BY v.idVisor DESC")
    public List<VisorDB> getSolicitadoAM(Pageable pageable);*/

    /*@Query("SELECT new VisorDB((c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) as customer, v.idVisor, v.productName, v.requestStatus, v.commercialOperation" +
            " ), new CustomerDB(c.email, CASE WHEN v.phone is null OR v.phone = '' THEN c.phone1 ELSE v.phone END as phone, c.phone2)" +
            " FROM VisorDB v" +
            " INNER JOIN v.order o " +
            " INNER JOIN o.customer c " +
            " WHERE v.requestedDate is not null AND v.mailRequested is null AND v.requestStatus = 'PENDIENTE'" +
            " AND c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null AND c.email is not null" +
            " AND c.firstname <> '' AND c.lastname1 <> '' AND c.lastname2 <> '' AND c.email <> ''" +
            " AND lower(v.commercialOperation) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            " ORDER BY v.idVisor DESC")
    public List<Object[]> getSolicitadoAM(Pageable pageable);*/

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END cliente, c.email, v.nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_solicitado is not null AND v.flg_solicitado_email is null AND v.estado_solicitud = 'PENDIENTE'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR" +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null" +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC", nativeQuery = true)
    public List<Object[]> getSolicitadoAM();*/

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END cliente, c.email, v.nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_solicitado is not null AND v.flg_solicitado_email is null AND v.estado_solicitud = 'PENDIENTE'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR" +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null" +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in " +
            " (lower('ALTA PURA'),lower('ALT'))" +
            " AND v.fecha_grabacion > (now() - INTERVAL '3 months')" +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC", nativeQuery = true)
    public List<Object[]> getSolicitadoAlta();*/

    @Query(value = "SELECT v.id_visor, o.client_nombre, v.correo, v.nombre_producto," +
            " CASE WHEN v.telefono is null THEN '' ELSE v.telefono END as phone1, " +
            " CASE WHEN v.telefono2 is null THEN '' ELSE v.telefono2 END as phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, COALESCE(v.codigo_pedido,'') codigo_pedido," +
            " COALESCE(v.flg_solicitado_email,0) flg_solicitado_email," +
            " COALESCE(v.flg_registrado_email,0) flg_registrado_email," +
            " COALESCE(v.flg_equipo_tecnico_email,0) flg_equipo_tecnico_email," +
            " COALESCE(v.flg_instalado_email,0) flg_instalado_email," +
            " COALESCE(v.flg_registrado_ret_email,0) flg_registrado_ret_email," +
            " COALESCE(v.flg_equipo_tecnico_ret_email,0) flg_equipo_tecnico_ret_email," +
            " COALESCE(v.flg_instalado_ret_email,0) flg_instalado_ret_email," +
            " COALESCE(v.flg_registrado_caida_email,0) flg_registrado_caida_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " WHERE v.fecha_grabacion is not null AND (v.flg_solicitado_email is null or v.flg_solicitado_email = 0) " +
            " AND v.fecha_registrado is null AND v.fecha_equipo_tecnico is null AND v.fecha_instalado is null"+
            " AND v.estado_solicitud = 'PENDIENTE'" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            //" AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_grabacion > (now() - INTERVAL '3 months')" +
            " AND v.fecha_grabacion > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " ORDER BY v.id_visor DESC LIMIT 100", nativeQuery = true)
    public List<Object[]> getSolicitadoAlta();

    /*@Query(value = "SELECT  v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email," +
            " (" +
            " (CASE WHEN od.DECOSSD IS NOT NULL AND od.DECOSSD <> 0 THEN ('DECOS SMART: ' || od.DECOSSD || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.DECOSHD IS NOT NULL AND od.DECOSHD <> 0 THEN ('DECOS HD: ' || od.DECOSHD || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.DECOSDVR IS NOT NULL AND od.DECOSDVR <> 0 THEN ('DECOS DVR: ' || od.DECOSDVR || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.TVBLOCK IS NOT NULL AND od.TVBLOCK <> '' THEN ('BLOQUE TV: ' || od.TVBLOCK || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.SVAINTERNET IS NOT NULL AND od.SVAINTERNET <> '' THEN ('SVA INTERNET: ' || od.SVAINTERNET || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.SVALINE IS NOT NULL AND od.SVALINE <> '' THEN ('SVA LINEA: ' || od.SVALINE || '|') ELSE '' END) || ' ' ||" +
            " (CASE WHEN od.BLOCKTV IS NOT NULL AND od.BLOCKTV <> '' THEN ('BLOQUE PRODUCTO: ' || od.BLOCKTV || '|') ELSE '' END)" +
            " ) as nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2,"+
            " v.estado_solicitud, v.operacion_comercial" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.order_detail od ON od.orderid = o.id" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_solicitado is not null AND v.flg_solicitado_email is null AND v.estado_solicitud = 'PENDIENTE'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR" +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null" +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND (od.DECOSSD IS NOT NULL OR od.DECOSHD IS NOT NULL OR od.DECOSDVR IS NOT NULL OR od.TVBLOCK IS NOT NULL" +
            " OR od.SVAINTERNET IS NOT NULL OR od.SVALINE IS NOT NULL OR od.BLOCKTV IS NOT NULL)" +
            " AND lower(v.operacion_comercial) in (lower('SVAS')) " +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC", nativeQuery = true)
    public List<Object[]> getSolicitadoS();*/

    /*@Query(value = "(" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, v.nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC" +
            " )" +
            " UNION ALL " +
            " (" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, v.nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND v.id_visor NOT IN (SELECT id FROM ibmx_a07e6d02edaf552.order)" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in " +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))" +
            //" AND v.dni = '70540625' " +
            " ORDER BY v.id_visor DESC" +
            " )", nativeQuery = true)
    public List<Object[]> getRegistradoAM();*/

    /*@Query(value = "(" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, v.nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in " +
            " (lower('ALTA PURA'),lower('ALT'))" +
            " AND v.fecha_grabacion > (now() - INTERVAL '3 months')" +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC" +
            " )" +
            " UNION ALL " +
            " (" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, v.nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND v.id_visor NOT IN (SELECT id FROM ibmx_a07e6d02edaf552.order)" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in " +
            " (lower('ALTA PURA'),lower('ALT'))" +
            " AND v.fecha_grabacion > (now() - INTERVAL '3 months')" +
            //" AND v.dni = '70540625' " +
            " ORDER BY v.id_visor DESC" +
            " )", nativeQuery = true)
    public List<Object[]> getRegistradoAlta();*/

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, COALESCE(v.codigo_pedido,'') codigo_pedido," +
            " COALESCE(v.flg_solicitado_email,0) flg_solicitado_email," +
            " COALESCE(v.flg_registrado_email,0) flg_registrado_email," +
            " COALESCE(v.flg_equipo_tecnico_email,0) flg_equipo_tecnico_email," +
            " COALESCE(v.flg_instalado_email,0) flg_instalado_email," +
            " COALESCE(v.flg_registrado_ret_email,0) flg_registrado_ret_email," +
            " COALESCE(v.flg_equipo_tecnico_ret_email,0) flg_equipo_tecnico_ret_email," +
            " COALESCE(v.flg_instalado_ret_email,0) flg_instalado_ret_email," +
            " COALESCE(v.flg_registrado_caida_email,0) flg_registrado_caida_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) " +
            " AND v.fecha_equipo_tecnico is null AND v.fecha_instalado is null"+
            " AND v.estado_solicitud = 'INGRESADO'" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            //" AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_registrado > (now() - INTERVAL '3 months')" +
            " AND v.fecha_registrado > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " ORDER BY v.fecha_registrado ASC LIMIT 100", nativeQuery = true)
    public List<Object[]> getRegistradoAlta();

    /*@Query(value = "(" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email,  " +
            " (" +
            " (CASE WHEN DECOSSD IS NOT NULL AND DECOSSD <> 0 THEN 'DECOS SMART: ' || DECOSSD || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN DECOSHD IS NOT NULL AND DECOSHD <> 0 THEN 'DECOS HD: ' || DECOSHD || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN DECOSDVR IS NOT NULL AND DECOSDVR <> 0 THEN 'DECOS DVR: ' || DECOSDVR || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN TVBLOCK IS NOT NULL AND TVBLOCK <> '' THEN 'BLOQUE TV: ' || TVBLOCK || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN SVAINTERNET IS NOT NULL AND SVAINTERNET <> '' THEN 'SVA INTERNET: ' || SVAINTERNET || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN SVALINE IS NOT NULL AND SVALINE <> '' THEN 'SVA LINEA: ' || SVALINE || '|' ELSE '' END) || ' ' ||" +
            " (CASE WHEN BLOCKTV IS NOT NULL AND BLOCKTV <> '' THEN 'BLOQUE PRODUCTO: ' || BLOCKTV || '|' ELSE '' END)" +
            " ) as nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            //" v.fecha_solicitado, v.fecha_registrado, v.fecha_equipo_tecnico, v.fecha_instalado, " +
            //" v.flg_solicitado_email, v.flg_registrado_email, v.flg_equipo_tecnico_email, v.flg_instalado_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.order o ON o.id = v.id_visor" +
            " INNER JOIN ibmx_a07e6d02edaf552.order_detail od ON od.orderid = o.id" +
            " INNER JOIN ibmx_a07e6d02edaf552.customer c ON c.id = o.customerid" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND (od.DECOSSD IS NOT NULL OR od.DECOSHD IS NOT NULL OR od.DECOSDVR IS NOT NULL OR od.TVBLOCK IS NOT NULL" +
            " OR od.SVAINTERNET IS NOT NULL OR od.SVALINE IS NOT NULL OR od.BLOCKTV IS NOT NULL)" +
            " AND lower(v.operacion_comercial) in (lower('SVAS')) " +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC" +
            " )" +
            " UNION ALL" +
            " (" +
            " SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, V.sub_producto nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial" +
            //" v.fecha_solicitado, v.fecha_registrado, v.fecha_equipo_tecnico, v.fecha_instalado, " +
            //" v.flg_solicitado_email, v.flg_registrado_email, v.flg_equipo_tecnico_email, v.flg_instalado_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            " WHERE v.fecha_registrado is not null AND (v.flg_registrado_email is null OR v.flg_registrado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND v.id_visor NOT IN (SELECT id FROM ibmx_a07e6d02edaf552.order)" +
            " AND (" +
            " (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            " (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            " AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) in (lower('SVAS')) " +
            //" AND v.dni = '70540625'" +
            " ORDER BY v.id_visor DESC" +
            " )", nativeQuery = true)
    public List<Object[]> getRegistradoS();*/

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, CAST('' as text) nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial, v.codigo_pedido" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            " WHERE v.fecha_equipo_tecnico is not null AND (v.flg_equipo_tecnico_email is null OR v.flg_equipo_tecnico_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND (" +
            "     (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            "     (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            "     AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) " +
            " in (lower('SVAS'),lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION')) " +
            " ORDER BY v.id_visor DESC" , nativeQuery = true)
    public List<Object[]> getAsignacionTecnica();*/

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, COALESCE(v.nombre_producto,'') nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial, " +
            " v.departamento, v.provincia, v.distrito," +
            " v.codigo_pedido" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            " WHERE v.fecha_equipo_tecnico is not null AND (v.flg_equipo_tecnico_email is null OR v.flg_equipo_tecnico_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND (" +
            "     (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            "     (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            "     AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) " +
            " in (lower('ALTA PURA'),lower('ALT')) " +
            " AND v.fecha_grabacion > (date(now() AT TIME ZONE 'America/Lima') + interval '0h')" +
            " ORDER BY v.id_visor DESC" , nativeQuery = true)
    public List<Object[]> getAsignacionTecnicaAlta();*/

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, COALESCE(v.codigo_pedido,'') codigo_pedido," +
            " COALESCE(v.flg_solicitado_email,0) flg_solicitado_email," +
            " COALESCE(v.flg_registrado_email,0) flg_registrado_email," +
            " COALESCE(v.flg_equipo_tecnico_email,0) flg_equipo_tecnico_email," +
            " COALESCE(v.flg_instalado_email,0) flg_instalado_email," +
            " COALESCE(v.flg_registrado_ret_email,0) flg_registrado_ret_email," +
            " COALESCE(v.flg_equipo_tecnico_ret_email,0) flg_equipo_tecnico_ret_email," +
            " COALESCE(v.flg_instalado_ret_email,0) flg_instalado_ret_email," +
            " COALESCE(v.flg_registrado_caida_email,0) flg_registrado_caida_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.fecha_equipo_tecnico is not null AND (v.flg_equipo_tecnico_email is null OR v.flg_equipo_tecnico_email = 0) " +
            " AND v.fecha_instalado is null"+
            " AND v.estado_solicitud = 'INGRESADO'" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            //" AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_equipo_tecnico > (now() - INTERVAL '3 months')\n" +
            " AND v.fecha_equipo_tecnico > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " ORDER BY v.fecha_equipo_tecnico ASC LIMIT 100" , nativeQuery = true)
    public List<Object[]> getAsignacionTecnicaAlta();

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, CAST('' as text) nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial, v.codigo_pedido" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            " WHERE v.fecha_instalado is not null AND (v.flg_instalado_email is null OR v.flg_instalado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND (" +
            "     (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            "     (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            "     AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) " +
            " in (lower('SVAS'),lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION')) " +
            " ORDER BY v.id_visor DESC" , nativeQuery = true)
    public List<Object[]> getInstalado();*/

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            " c.email, COALESCE(v.nombre_producto,'') nombre_producto," +
            " CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            " v.estado_solicitud, v.operacion_comercial, " +
            " v.departamento, v.provincia, v.distrito," +
            " v.codigo_pedido" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            " WHERE v.fecha_instalado is not null AND (v.flg_instalado_email is null OR v.flg_instalado_email = 0) AND v.estado_solicitud = 'INGRESADO'" +
            " AND (" +
            "     (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            "     (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            "     AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            " ) AND c.email is not null AND trim(c.email) <> ''" +
            " AND lower(v.operacion_comercial) " +
            " in (lower('ALTA PURA'),lower('ALT')) " +
            " AND v.fecha_grabacion > (now() - INTERVAL '3 months')" +
            " ORDER BY v.id_visor DESC" , nativeQuery = true)
    public List<Object[]> getInstaladoAlta();*/

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, COALESCE(v.codigo_pedido,'') codigo_pedido," +
            " COALESCE(v.flg_solicitado_email,0) flg_solicitado_email," +
            " COALESCE(v.flg_registrado_email,0) flg_registrado_email," +
            " COALESCE(v.flg_equipo_tecnico_email,0) flg_equipo_tecnico_email," +
            " COALESCE(v.flg_instalado_email,0) flg_instalado_email," +
            " COALESCE(v.flg_registrado_ret_email,0) flg_registrado_ret_email," +
            " COALESCE(v.flg_equipo_tecnico_ret_email,0) flg_equipo_tecnico_ret_email," +
            " COALESCE(v.flg_instalado_ret_email,0) flg_instalado_ret_email," +
            " COALESCE(v.flg_registrado_caida_email,0) flg_registrado_caida_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.fecha_instalado is not null AND (v.flg_instalado_email is null OR v.flg_instalado_email = 0) " +
            " AND v.estado_solicitud = 'INGRESADO'" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            " AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_instalado > (now() - INTERVAL '3 months')" +
            " AND v.fecha_instalado > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " ORDER BY v.fecha_instalado ASC LIMIT 100" , nativeQuery = true)
    public List<Object[]> getInstaladoAlta();

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            "            c.email, CAST('' as text) nombre_producto," +
            "            CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            "            v.estado_solicitud, v.operacion_comercial, v.codigo_pedido" +
            "            FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            "            LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            "            WHERE v.estado_solicitud = 'INGRESADO'" +
            "            AND codigo_pedido in (" +
            "             select requerimiento from ibmx_a07e6d02edaf552.devuelta_comercial where tematico_3 = 'ULTIMO TRATAMIENTO' " +
            "             and (flg_email is null or flg_email = 0)" +
            "            )" +
            "            AND (" +
            "                 (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            "                 (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            "                 AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            "            ) AND c.email is not null AND trim(c.email) <> ''" +
            "            AND lower(v.operacion_comercial) " +
            "            in (lower('SVAS'),lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))\n" +
            "            ORDER BY v.id_visor DESC" , nativeQuery = true)
    public List<Object[]> getDevueltaComercial();*/

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            "            c.email, COALESCE(v.nombre_producto,'') nombre_producto," +
            "            CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            "            v.estado_solicitud, v.operacion_comercial, " +
            "            v.departamento, v.provincia, v.distrito," +
            "            v.codigo_pedido" +
            "            FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            "            LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            "            WHERE v.estado_solicitud = 'INGRESADO'" +
            "            AND codigo_pedido in (" +
            "               SELECT codigo_pedido FROM ibmx_a07e6d02edaf552.pedidos_en_caida" +
            "               WHERE tipo_caida = 'DEV_COM_NO' AND estado = true AND (flg_email is null OR flg_email = 0)" +
            "            )" +
            "            AND (" +
            "                 (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            "                 (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            "                 AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            "            ) AND c.email is not null AND trim(c.email) <> ''" +
            "            AND lower(v.operacion_comercial) " +
            "            in (lower('ALTA PURA'),lower('ALT'))" +
            "            AND v.fecha_grabacion > (now() - INTERVAL '3 months')" +
            "            ORDER BY v.id_visor DESC" , nativeQuery = true)
    public List<Object[]> getDevueltaComercialNoContactoAlta();*/

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, v.codigo_pedido" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.estado_solicitud = 'INGRESADO'" +
            " AND v.codigo_pedido in (" +
            " SELECT codigo_pedido FROM ibmx_a07e6d02edaf552.pedidos_en_caida" +
            " WHERE tipo_caida = 'DEV_COM_NO' AND estado = true AND (flg_email is null OR flg_email = 0)" +
            " AND insert_update > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " )" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            //" AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_registrado > (now() - INTERVAL '3 months')" +
            " ORDER BY v.fecha_registrado DESC LIMIT 100" , nativeQuery = true)
    public List<Object[]> getDevueltaComercialNoContactoAlta();

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            "            c.email, COALESCE(v.nombre_producto,'') nombre_producto," +
            "            CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            "            v.estado_solicitud, v.operacion_comercial, " +
            "            v.departamento, v.provincia, v.distrito," +
            "            v.codigo_pedido" +
            "            FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            "            LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            "            WHERE v.estado_solicitud = 'INGRESADO'" +
            "            AND codigo_pedido in (" +
            "               SELECT codigo_pedido FROM ibmx_a07e6d02edaf552.pedidos_en_caida" +
            "               WHERE tipo_caida = 'DEV_COM_CL' AND estado = true AND (flg_email is null OR flg_email = 0)" +
            "            )" +
            "            AND (" +
            "                 (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            "                 (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            "                 AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            "            ) AND c.email is not null AND trim(c.email) <> ''" +
            "            AND lower(v.operacion_comercial) " +
            "            in (lower('ALTA PURA'),lower('ALT'))" +
            "            AND v.fecha_grabacion > (now() - INTERVAL '3 months')" +
            "            ORDER BY v.id_visor DESC" , nativeQuery = true)
    public List<Object[]> getDevueltaComercialAPedidoAlta();*/

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, v.codigo_pedido" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.estado_solicitud = 'INGRESADO'" +
            " AND v.codigo_pedido in (" +
            " SELECT codigo_pedido FROM ibmx_a07e6d02edaf552.pedidos_en_caida" +
            " WHERE tipo_caida = 'DEV_COM_CL' AND estado = true AND (flg_email is null OR flg_email = 0)" +
            " AND insert_update > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " )" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            //" AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_registrado > (now() - INTERVAL '3 months')" +
            //" AND v.fecha_registrado > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " ORDER BY v.fecha_registrado DESC LIMIT 100" , nativeQuery = true)
    public List<Object[]> getDevueltaComercialAPedidoAlta();

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            "            c.email, CAST('' as text) nombre_producto," +
            "            CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            "            v.estado_solicitud, v.operacion_comercial, v.codigo_pedido" +
            "            FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            "            LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            "            WHERE v.estado_solicitud = 'INGRESADO'" +
            "            AND codigo_pedido in (" +
            "             select numero_peticion from ibmx_a07e6d02edaf552.devuelta_tecnica where tematico_2 = 'En proceso de cancelacion' " +
            "             and (flg_email is null or flg_email = 0)" +
            "            )" +
            "            AND (" +
            "                 (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            "                 (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            "                 AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            "            ) AND c.email is not null AND trim(c.email) <> ''" +
            "            AND lower(v.operacion_comercial) " +
            "            in (lower('SVAS'),lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'))\n" +
            "            ORDER BY v.id_visor DESC" , nativeQuery = true)
    public List<Object[]> getDevueltaTecnica();*/

    /*@Query(value = "SELECT v.id_visor, CASE WHEN v.cliente is not null AND v.cliente <> '' THEN v.cliente ELSE (c.firstname || ' ' || c.lastname1 || ' ' || c.lastname2) END as cliente," +
            "            c.email, COALESCE(v.nombre_producto,'') nombre_producto," +
            "            CASE WHEN v.telefono is null OR v.telefono = '' THEN c.customerphone ELSE v.telefono END as phone1, c.customerphone2 as phone2," +
            "            v.estado_solicitud, v.operacion_comercial, " +
            "            v.departamento, v.provincia, v.distrito," +
            "            v.codigo_pedido" +
            "            FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            "            LEFT JOIN ibmx_a07e6d02edaf552.customer c ON c.docnumber = v.dni" +
            "            WHERE v.estado_solicitud = 'INGRESADO'" +
            "            AND codigo_pedido in (" +
            "                 SELECT codigo_pedido FROM ibmx_a07e6d02edaf552.pedidos_en_caida" +
            "                 WHERE tipo_caida = 'DEV_TEC' AND estado = true AND (flg_email is null OR flg_email = 0)" +
            "            )" +
            "            AND (" +
            "                 (v.cliente is not null AND trim(v.cliente) <> '') OR " +
            "                 (c.firstname is not null AND c.lastname1 is not null AND c.lastname2 is not null " +
            "                 AND trim(c.firstname) <> '' AND trim(c.lastname1) <> '' AND trim(c.lastname2) <> '')" +
            "            ) AND c.email is not null AND trim(c.email) <> ''" +
            "            AND lower(v.operacion_comercial) " +
            "            in (lower('ALTA PURA'),lower('ALT'))" +
            "            AND v.fecha_grabacion > (now() - INTERVAL '3 months')" +
            "            ORDER BY v.id_visor DESC" , nativeQuery = true)
    public List<Object[]> getDevueltaTecnicaAlta();*/

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, v.codigo_pedido" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.estado_solicitud = 'INGRESADO'" +
            " AND v.codigo_pedido in (" +
            " SELECT codigo_pedido FROM ibmx_a07e6d02edaf552.pedidos_en_caida" +
            " WHERE tipo_caida = 'DEV_TEC' AND estado = true AND (flg_email is null OR flg_email = 0)" +
            " AND insert_update > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " )" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            //" AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_registrado > (now() - INTERVAL '3 months')" +
            //" AND v.fecha_registrado > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " ORDER BY v.fecha_registrado DESC LIMIT 100" , nativeQuery = true)
    public List<Object[]> getDevueltaTecnicaAlta();


    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, COALESCE(v.codigo_pedido,'') codigo_pedido," +
            " COALESCE(v.flg_solicitado_email,0) flg_solicitado_email," +
            " COALESCE(v.flg_registrado_email,0) flg_registrado_email," +
            " COALESCE(v.flg_equipo_tecnico_email,0) flg_equipo_tecnico_email," +
            " COALESCE(v.flg_instalado_email,0) flg_instalado_email," +
            " COALESCE(v.flg_registrado_ret_email,0) flg_registrado_ret_email," +
            " COALESCE(v.flg_equipo_tecnico_ret_email,0) flg_equipo_tecnico_ret_email," +
            " COALESCE(v.flg_instalado_ret_email,0) flg_instalado_ret_email," +
            " COALESCE(v.flg_registrado_caida_email,0) flg_registrado_caida_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.fecha_registrado is null AND (v.flg_registrado_ret_email is null OR v.flg_registrado_ret_email = 0) " +
            " AND v.estado_solicitud = 'PENDIENTE'" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_solicitado > (now() - INTERVAL '3 months')" +
            " AND v.fecha_solicitado < (now() AT TIME ZONE 'America/Lima') - interval '48 hours'" +
            " AND v.fecha_solicitado > (now() AT TIME ZONE 'America/Lima') - interval '72 hours'" +
            " ORDER BY v.fecha_solicitado ASC LIMIT 100", nativeQuery = true)
    public List<Object[]> getRegistradoRetrasoAlta();

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, COALESCE(v.codigo_pedido,'') codigo_pedido," +
            " COALESCE(v.flg_solicitado_email,0) flg_solicitado_email," +
            " COALESCE(v.flg_registrado_email,0) flg_registrado_email," +
            " COALESCE(v.flg_equipo_tecnico_email,0) flg_equipo_tecnico_email," +
            " COALESCE(v.flg_instalado_email,0) flg_instalado_email," +
            " COALESCE(v.flg_registrado_ret_email,0) flg_registrado_ret_email," +
            " COALESCE(v.flg_equipo_tecnico_ret_email,0) flg_equipo_tecnico_ret_email," +
            " COALESCE(v.flg_instalado_ret_email,0) flg_instalado_ret_email," +
            " COALESCE(v.flg_registrado_caida_email,0) flg_registrado_caida_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " LEFT JOIN ibmx_a07e6d02edaf552.tdp_service_pedido tsp ON tsp.pedido_codigo = v.codigo_pedido" +
            " WHERE v.fecha_equipo_tecnico is null AND (v.flg_equipo_tecnico_ret_email is null OR v.flg_equipo_tecnico_ret_email = 0) " +
            " AND v.estado_solicitud = 'INGRESADO'" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            " AND (tsp.pedido_estado_code is null OR tsp.pedido_estado_code not in ('CG','FI','TE','02','04','05')) " +
            //" AND (tsp.auditoria_modify is null OR tsp.auditoria_modify < (now() AT TIME ZONE 'America/Lima') - interval '24 hours')"+
            //" AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_registrado > (now() - INTERVAL '3 months')" +
            " AND v.fecha_registrado < (now() AT TIME ZONE 'America/Lima') - interval '72 hours'" +
            " AND v.fecha_registrado > (now() AT TIME ZONE 'America/Lima') - interval '96 hours'" +
            " ORDER BY v.fecha_equipo_tecnico ASC LIMIT 100" , nativeQuery = true)
    public List<Object[]> getAsignacionTecnicaRetrasoAlta();

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, COALESCE(v.codigo_pedido,'') codigo_pedido," +
            " COALESCE(v.flg_solicitado_email,0) flg_solicitado_email," +
            " COALESCE(v.flg_registrado_email,0) flg_registrado_email," +
            " COALESCE(v.flg_equipo_tecnico_email,0) flg_equipo_tecnico_email," +
            " COALESCE(v.flg_instalado_email,0) flg_instalado_email," +
            " COALESCE(v.flg_registrado_ret_email,0) flg_registrado_ret_email," +
            " COALESCE(v.flg_equipo_tecnico_ret_email,0) flg_equipo_tecnico_ret_email," +
            " COALESCE(v.flg_instalado_ret_email,0) flg_instalado_ret_email," +
            " COALESCE(v.flg_registrado_caida_email,0) flg_registrado_caida_email" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " LEFT JOIN ibmx_a07e6d02edaf552.tdp_service_pedido tsp ON tsp.pedido_codigo = v.codigo_pedido" +
            " WHERE v.fecha_instalado is null AND (v.flg_instalado_ret_email is null OR v.flg_instalado_ret_email = 0) " +
            " AND v.estado_solicitud = 'INGRESADO'" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            " AND (tsp.pedido_estado_code is null OR tsp.pedido_estado_code not in ('CG','FI','TE','02','04','05')) " +
            //" AND (tsp.auditoria_modify is null OR tsp.auditoria_modify < (now() AT TIME ZONE 'America/Lima') - interval '24 hours')"+
            //" AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_equipo_tecnico > (now() - INTERVAL '3 months')" +
            " AND v.fecha_equipo_tecnico < (now() AT TIME ZONE 'America/Lima') - interval '72 hours'" +
            " AND v.fecha_equipo_tecnico > (now() AT TIME ZONE 'America/Lima') - interval '96 hours'" +
            " ORDER BY v.fecha_equipo_tecnico ASC LIMIT 100" , nativeQuery = true)
    public List<Object[]> getInstaladoRetrasoAlta();

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, COALESCE(v.codigo_pedido,'') codigo_pedido," +
            " COALESCE(v.flg_solicitado_email,0) flg_solicitado_email," +
            " COALESCE(v.flg_registrado_email,0) flg_registrado_email," +
            " COALESCE(v.flg_equipo_tecnico_email,0) flg_equipo_tecnico_email," +
            " COALESCE(v.flg_instalado_email,0) flg_instalado_email," +
            " COALESCE(v.flg_registrado_ret_email,0) flg_registrado_ret_email," +
            " COALESCE(v.flg_equipo_tecnico_ret_email,0) flg_equipo_tecnico_ret_email," +
            " COALESCE(v.flg_instalado_ret_email,0) flg_instalado_ret_email," +
            " COALESCE(v.flg_registrado_caida_email,0) flg_registrado_caida_email," +
            " v.fecha_grabacion" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.fecha_registrado is null AND (v.flg_registrado_caida_email is null OR v.flg_registrado_caida_email = 0) " +
            " AND v.estado_solicitud = 'CAIDA'" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_solicitado > (now() - INTERVAL '3 months')" +
            " AND v.fecha_grabacion > date(now() AT TIME ZONE 'America/Lima') - interval '3 day'" +
            " ORDER BY v.fecha_grabacion ASC LIMIT 100", nativeQuery = true)
    public List<Object[]> getRegistradoCaidaAlta();

    @Query(value = "SELECT v.id_visor, o.client_nombre, " +
            " CASE WHEN pc.correo2 IS NOT NULL AND pc.correo2 <> '' THEN pc.correo2 WHEN v.correo IS NULL THEN '' ELSE v.correo END correo, " +
            " v.nombre_producto," +
            " CASE WHEN pc.telefono3 IS NOT NULL AND pc.telefono3 <> '' THEN pc.telefono3 WHEN v.telefono IS NULL THEN '' ELSE v.telefono END phone1," +
            " CASE WHEN pc.telefono4 IS NOT NULL AND pc.telefono4 <> '' THEN pc.telefono4 WHEN v.telefono2 IS NULL THEN '' ELSE v.telefono2 END phone2," +
            " v.estado_solicitud, v.operacion_comercial," +
            " v.departamento, v.provincia, v.distrito, v.codigo_pedido" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor v" +
            " INNER JOIN ibmx_a07e6d02edaf552.tdp_order o ON o.order_id = v.id_visor" +
            " LEFT JOIN ibmx_a07e6d02edaf552.public_customer pc ON pc.documento = v.dni AND pc.codigo_pedido = v.codigo_pedido" +
            " WHERE v.estado_solicitud = 'INGRESADO'" +
            " AND v.codigo_pedido in (" +
            " SELECT codigo_pedido FROM ibmx_a07e6d02edaf552.pedidos_en_caida" +
            " WHERE tipo_caida = 'RET_ASIGN' AND (flg_email is null OR flg_email = 0)" +
            " AND insert_update > date(now() AT TIME ZONE 'America/Lima') - interval '1 day'" +
            " )" +
            " AND o.client_nombre is not null AND trim(o.client_nombre) <> ''" +
            //" AND v.correo is not null AND trim(v.correo) <> ''" +
            " AND lower(v.operacion_comercial) in ('alta pura','alt')" +
            //" AND v.fecha_registrado > (now() - INTERVAL '3 months')" +
            //" AND v.fecha_registrado > date(now() AT TIME ZONE 'America/Lima') - interval '7 day'" +
            " ORDER BY v.fecha_registrado DESC LIMIT 100" , nativeQuery = true)
    public List<Object[]> getEquipoTecnicoCaidaAlta();

    /*@Modifying(clearAutomatically = true)
    @Transactional()
    @Query(value = "UPDATE ibmx_a07e6d02edaf552.tdp_visor SET flg_solicitado_email = 1 WHERE id_visor IN (:ids)", nativeQuery = true)
    public Integer  setMailRequestedAsDone(@Param("ids") final String ids);*/

}
