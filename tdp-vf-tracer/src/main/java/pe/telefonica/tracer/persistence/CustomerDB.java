package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "CUSTOMER", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id")
    private String idCustomer;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname1")
    private String lastname1;

    @Column(name = "lastname2")
    private String lastname2;

    @Column(name = "email")
    private String email;

    @Column(name = "customerphone")
    private String phone1;

    @Column(name = "customerphone2")
    private String phone2;

    public CustomerDB(String email, String phone1, String phone2){
        this.email = email;
        this.phone1 = phone1;
        this.phone2 = phone2;
    }

    public CustomerDB(){

    }


    public String getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(String idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname1() {
        return lastname1;
    }

    public void setLastname1(String lastname1) {
        this.lastname1 = lastname1;
    }

    public String getLastname2() {
        return lastname2;
    }

    public void setLastname2(String lastname2) {
        this.lastname2 = lastname2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

}
