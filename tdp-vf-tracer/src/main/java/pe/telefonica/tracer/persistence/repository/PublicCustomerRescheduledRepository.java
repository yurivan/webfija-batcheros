package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.PublicCustomerDataDB;
import pe.telefonica.tracer.persistence.PublicCustomerRescheduledDB;

import java.util.List;

@Transactional()
public interface PublicCustomerRescheduledRepository extends JpaRepository<PublicCustomerRescheduledDB, Long> {

    @Query(value = "SELECT pc.id, pc.documento, pc.codigo_pedido, pc.rango_fecha_inicio, pc.rango_fecha_fin ,pc.insert_update" +
            " FROM ibmx_a07e6d02edaf552.public_customer_rescheduled pc WHERE pc.documento = :documento AND pc.codigo_pedido = :codigoPedido" +
            " ORDER BY pc.id ASC", nativeQuery = true)
    public List<PublicCustomerRescheduledDB> getPublicCustomerRescheduled(@Param("documento") final String documento, @Param("codigoPedido") final String codigoPedido);

}

