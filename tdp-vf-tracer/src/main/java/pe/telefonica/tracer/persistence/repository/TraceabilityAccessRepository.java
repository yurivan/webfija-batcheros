package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.TraceabilityAccessDB;

import java.util.List;

@Transactional()
public interface TraceabilityAccessRepository extends JpaRepository<TraceabilityAccessDB, Long> {
    @Query(value = "SELECT ta.id, ta.token FROM ibmx_a07e6d02edaf552.traceability_access ta WHERE ta.token = :token AND ta.type = :type", nativeQuery = true)
    public TraceabilityAccessDB getTraceabilityAccess(@Param("token") final String token, @Param("type") final Integer type);


}

