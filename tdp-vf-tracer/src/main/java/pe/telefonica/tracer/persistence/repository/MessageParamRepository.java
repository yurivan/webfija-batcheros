package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.MessageParamDB;
import pe.telefonica.tracer.persistence.ParametersDB;

import java.util.List;

@Transactional()
public interface MessageParamRepository extends JpaRepository<MessageParamDB, Long> {

    @Query(value = "SELECT p.id, p.orden, p.servicekey, p.responsemessage FROM ibmx_a07e6d02edaf552.TDP_ERROR_MESSAGE_SERVICE p" +
            " WHERE p.servicekey in :servicekeyList order by p.orden ASC", nativeQuery = true)
    public List<MessageParamDB> getMessageParam(@Param("servicekeyList") List<String> servicekeyList);

}
