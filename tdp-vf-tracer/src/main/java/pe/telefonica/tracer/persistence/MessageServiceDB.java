package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@Table(name = "TDP_ERROR_MESSAGE_SERVICE", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageServiceDB {

    @Id
    @Column(updatable = false, name = "id")
    private String id;

    @Column(name = "service")
    private String service;

    @Column(name = "orden")
    private Integer orden;

    @Column(name = "descriptionkey")
    private String descriptionkey;

    @Column(name = "servicekey")
    private String servicekey;

    @Column(name = "responsemessage")
    private String responsemessage;

    @Column(name = "responsecode")
    private Integer responsecode;

    @Column(name = "servicecode")
    private String servicecode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public String getDescriptionkey() {
        return descriptionkey;
    }

    public void setDescriptionkey(String descriptionkey) {
        this.descriptionkey = descriptionkey;
    }

    public String getServicekey() {
        return servicekey;
    }

    public void setServicekey(String servicekey) {
        this.servicekey = servicekey;
    }

    public String getResponsemessage() {
        return responsemessage;
    }

    public void setResponsemessage(String responsemessage) {
        this.responsemessage = responsemessage;
    }

    public Integer getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(Integer responsecode) {
        this.responsecode = responsecode;
    }

    public String getServicecode() {
        return servicecode;
    }

    public void setServicecode(String servicecode) {
        this.servicecode = servicecode;
    }
}
