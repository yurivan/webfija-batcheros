package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;
import java.util.Date;


@Entity
@Table(name = "TRACEABILITY_EXTERNAL_LOG", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class TraceabilityExternalLogDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private String id;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    @Column(name = "request")
    private String request;

    @Column(name = "response")
    private String response;

    @Column(name = "date")
    private Timestamp date;

    public TraceabilityExternalLogDB(){

    }

    public TraceabilityExternalLogDB(String codigo_pedido, String request, String response){
        this.codigo_pedido = codigo_pedido;
        this.request = request;
        this.response = response;

        Timestamp timestamp = new Timestamp(new Date().getTime());
        this.date = timestamp;

    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setRequest(String request){
        this.request = request;
    }

    public String getRequest(){
        return request;
    }

    public void setResponse(String response){
        this.response = response;
    }

    public String getResponse(){
        return response;
    }

    public void setDate(Timestamp date){
        this.date = date;
    }

    public Timestamp getDate(){
        return date;
    }

}
