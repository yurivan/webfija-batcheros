package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "PUBLIC_CUSTOMER_RATING", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PublicCustomerRatingDB {

    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Id
    @Column(updatable = false, name = "id")
    private Integer id;

    @Column(name = "documento")
    private String documento;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    @Column(name = "respuesta1")
    private Integer respuesta1;

    @Column(name = "respuesta2")
    private Integer respuesta2;

    @Column(name = "respuesta3")
    private Integer respuesta3;

    @Column(name = "comentario")
    private String comentario;

    @Column(name = "insert_update")
    private Timestamp insert_update;

    public PublicCustomerRatingDB(){

    }

    public String getCodigo_pedido() {
        return codigo_pedido;
    }

    public void setCodigo_pedido(String codigo_pedido) {
        this.codigo_pedido = codigo_pedido;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public Integer getRespuesta1() {
        return respuesta1;
    }

    public void setRespuesta1(Integer respuesta1) {
        this.respuesta1 = respuesta1;
    }

    public Integer getRespuesta2() {
        return respuesta2;
    }

    public void setRespuesta2(Integer respuesta2) {
        this.respuesta2 = respuesta2;
    }

    public Integer getRespuesta3() {
        return respuesta3;
    }

    public void setRespuesta3(Integer respuesta3) {
        this.respuesta3 = respuesta3;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Timestamp getInsert_update() {
        return insert_update;
    }

    public void setInsert_update(Timestamp insert_update) {
        this.insert_update = insert_update;
    }
}
