package pe.telefonica.tracer.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Timestamp;


@Entity
@Table(name = "TDP_VISOR", schema = "ibmx_a07e6d02edaf552")
@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class VisorByDocDB {

    //@GeneratedValue(strategy=GenerationType.IDENTITY)

    @Id
    @Column(updatable = false, name = "id_visor")
    private String id;

    @Column(name = "codigo_pedido")
    private String codigo_pedido;

    @Column(name = "cliente")
    private String cliente;

    @Column(name = "nombre_producto")
    private String nombre_producto;

    public VisorByDocDB(){

    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setCodigo_pedido(String codigo_pedido){
        this.codigo_pedido = codigo_pedido;
    }

    public String getCodigo_pedido(){
        return codigo_pedido;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getNombre_producto() {
        return nombre_producto;
    }

    public void setNombre_producto(String nombre_producto) {
        this.nombre_producto = nombre_producto;
    }
}
