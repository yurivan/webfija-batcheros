package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ComercialDevDB;
import pe.telefonica.tracer.persistence.TechnicalDevDB;

import java.util.List;

@Transactional()
public interface TechnicalDevRepository extends JpaRepository<TechnicalDevDB, Long> {

    @Query(value = "SELECT dt.id, dt.numero_peticion, dt.flg_email FROM ibmx_a07e6d02edaf552.devuelta_tecnica dt WHERE dt.numero_peticion in :numPetList", nativeQuery = true)
    public List<TechnicalDevDB> getTechnicalDev(@Param("numPetList") List<String> numPetList);

}

