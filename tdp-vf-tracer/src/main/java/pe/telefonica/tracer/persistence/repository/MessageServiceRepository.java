package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.MessageServiceDB;
import pe.telefonica.tracer.persistence.ParametersDB;

import java.util.List;

@Transactional()
public interface MessageServiceRepository extends JpaRepository<MessageServiceDB, Long> {

    @Query(value = "SELECT * FROM ibmx_a07e6d02edaf552.tdp_error_message_service" +
            " WHERE service = 'PARAM_TRAZABILIDAD'", nativeQuery = true)
    public List<MessageServiceDB> getStatusEnables();

}
