package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.PublicCustomerDataDB;

import java.util.List;

@Transactional()
public interface PublicCustomerDataRepository extends JpaRepository<PublicCustomerDataDB, Long> {
    // Anthony
    @Query(value = "SELECT pc.codigo_pedido , pc.id, pc.documento, pc.telefono3, pc.telefono4, pc.direccion2, pc.correo2, pc.insert_update" +
            " FROM ibmx_a07e6d02edaf552.public_customer pc WHERE pc.documento = :documento AND codigo_pedido = :codPedido", nativeQuery = true)
    public PublicCustomerDataDB getPublicCustomerData(@Param("documento") final String documento, @Param("codPedido") final String codPedido);


    // Anthony
    @Query(value = "SELECT telefono, telefono2, direccion, correo" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor " +
            " WHERE  dni = :documento AND codigo_pedido = :codPedido", nativeQuery = true)
    public List<Object[]> validCustomerData(@Param("documento") final String documento, @Param("codPedido") final String codPedido);

}

