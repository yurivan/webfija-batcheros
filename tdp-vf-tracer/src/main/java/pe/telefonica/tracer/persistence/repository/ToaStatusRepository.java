package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.ToaStatusDB;

import java.util.List;

@Transactional()
public interface ToaStatusRepository extends JpaRepository<ToaStatusDB, Long> {

    @Query(value = "SELECT t.id, t.peticion, t.status_toa, t.astatus, t.usuario, t.time_slot, t.end_time, t.start_time, t.insert_date, t.pname " +
            " FROM ibmx_a07e6d02edaf552.TOA_STATUS t WHERE t.peticion = :codigoPedido" +
            " AND status_toa in ('IN_TOA','NOT_LEG','WO_COMPLETED')" +
            " ORDER BY t.insert_date DESC", nativeQuery = true)
    public List<ToaStatusDB> getEstadosTOAtoShow(@Param("codigoPedido") String codigoPedido);

}

