package pe.telefonica.tracer.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import pe.telefonica.tracer.persistence.VisorByIdDB;

import java.util.List;

@Transactional()
public interface VisorByIdRepository extends JpaRepository<VisorByIdDB, Long> {
    @Query(value = "SELECT id_visor, COALESCE(fecha_grabacion,'1975-01-01 00:00:00') fecha_grabacion, COALESCE(cliente,'') cliente, COALESCE(telefono,'') telefono, " +
            " COALESCE(direccion,'') direccion, COALESCE(departamento,'') departamento, COALESCE(dni,'') dni, " +
            " CASE WHEN lower(operacion_comercial) = lower('SVAS') THEN " +
            " (" +
            "    SELECT " +
            "    (" +
            "    (CASE WHEN od.DECOSSD IS NOT NULL AND od.DECOSSD <> 0 THEN ('DECOS SMART: ' || od.DECOSSD || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.DECOSHD IS NOT NULL AND od.DECOSHD <> 0 THEN ('DECOS HD: ' || od.DECOSHD || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.DECOSDVR IS NOT NULL AND od.DECOSDVR <> 0 THEN ('DECOS DVR: ' || od.DECOSDVR || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.TVBLOCK IS NOT NULL AND od.TVBLOCK <> '' THEN ('BLOQUE TV: ' || od.TVBLOCK || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.SVAINTERNET IS NOT NULL AND od.SVAINTERNET <> '' THEN ('SVA INTERNET: ' || od.SVAINTERNET || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.SVALINE IS NOT NULL AND od.SVALINE <> '' THEN ('SVA LINEA: ' || od.SVALINE || '|') ELSE '' END) || ' ' ||" +
            "    (CASE WHEN od.BLOCKTV IS NOT NULL AND od.BLOCKTV <> '' THEN ('BLOQUE PRODUCTO: ' || od.BLOCKTV || '|') ELSE '' END)" +
            "    )" +
            "    as nombre_producto" +
            "    FROM ibmx_a07e6d02edaf552.order_detail od" +
            "    WHERE od.orderid = :idVisor" +
            " )" +
            " ELSE COALESCE(nombre_producto,'') END nombre_producto, " +
            " CASE WHEN lower(operacion_comercial) = lower('SVAS') THEN COALESCE(nombre_producto,'') ELSE '' END nombre_producto2, " +
            " COALESCE(estado_solicitud,'') estado_solicitud, COALESCE(motivo_estado,'') motivo_estado," +
            " COALESCE(operacion_comercial,'') operacion_comercial, COALESCE(distrito,'') distrito, COALESCE(sub_producto,'') sub_producto, " +
            " COALESCE(tipo_documento,'') tipo_documento, COALESCE(provincia,'') provincia, COALESCE(fecha_de_llamada,'1975-01-01 00:00:00') fecha_de_llamada, " +
            " COALESCE(nombre_producto_fuente,'') nombre_producto_fuente, COALESCE(fecha_solicitado,'1975-01-01 00:00:00') fecha_solicitado," +
            " COALESCE(fecha_registrado,'1975-01-01 00:00:00') fecha_registrado, COALESCE(fecha_equipo_tecnico,'1975-01-01 00:00:00') fecha_equipo_tecnico, " +
            " COALESCE(fecha_instalado,'1975-01-01 00:00:00') fecha_instalado, COALESCE(estado_anterior,'') estado_anterior" +
            " FROM ibmx_a07e6d02edaf552.tdp_visor" +
            " WHERE id_visor = :idVisor" +
            " AND lower(operacion_comercial) in" +
            " (lower('ALTA PURA'),lower('ALTA COMPONENTE TV'),lower('ALTA COMPONENTE'),lower('ALTA COMPONENTE BA'),lower('MIGRACION'),lower('SVAS'))", nativeQuery = true)
    public Object[] getVisorById(@Param("idVisor") final String id);
}

