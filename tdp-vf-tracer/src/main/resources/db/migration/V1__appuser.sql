-- SCRIPTS AQUI
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD fecha_equipo_tecnico timestamp;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD fecha_instalado timestamp;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD flg_solicitado_email integer;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD flg_registrado_email integer;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD flg_equipo_tecnico_email integer;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD flg_instalado_email integer;

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.mail.emblue.authenticate.uri','http://api.embluemail.com/Services/Emblue3Service.svc/json/Authenticate'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.checkconexion.uri','http://api.embluemail.com/Services/Emblue3Service.svc/json/CheckConnection'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.sendexpress.uri','http://api.embluemail.com/Services/Emblue3Service.svc/json/sendMailExpress'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.sendmailexpresss.actionId','537839'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.user','developer@movistarplayperu.com'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.password','Developer_movistarplayperu1'),
(0,'CONFIG','PARAMETER','tdp.mail.emblue.token','sUZJ0eb5-yQpgE-wRMcz-mzZFpYjg1v'),
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.requested','HEMOS RECIBIDO TU PEDIDO'),
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.registered','TU PEDIDO HA SIDO REGISTRADO');

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.techassigned','UN EQUIPO TÉCNICO HA SIDO ASIGNADO A TU PEDIDO'),
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.installed','TU SERVICIO HA SIDO INSTALADO');

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.recoveryordercode','RECUPERANDO TU CÓDIGO DE PEDIDO');



CREATE TABLE ibmx_a07e6d02edaf552.traceability_access(
	id SERIAL primary key,
    token varchar(250) not null,
    type integer not null
);

INSERT INTO ibmx_a07e6d02edaf552.traceability_access(token, type) VALUES('iAJiahANAjahIOaoPAIUnIAUZPzOPIW', 1),('MkJUHugYfDFrASxYhuHnkjnsSaSs', 2);

CREATE TABLE ibmx_a07e6d02edaf552.traceability_access_log(
	id SERIAL primary key,
    token_id integer,
    message text,
    date timestamp not null
);

--ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor ADD estado_toa varchar(50);




CREATE TABLE ibmx_a07e6d02edaf552.status_toa(
	id SERIAL primary key,
	id_visor varchar(50),
    accion varchar(20),
    requerimiento varchar(15),
    codigo_pedido varchar(15),
    orden_trabajo varchar(15),
    fecha_envio date,
    hora_envio time,
    fecha_envio_toa date,
    hora_envio_toa time,
    fecha_agenda date,
    franja_horaria varchar(6),
    fecha_inicio_sla date,
    fecha_fin_sla date,
    fecha_inicio date,
    fecha_cierre date,
    motivo_accion  varchar(100),
    celular_cliente varchar(10),
    mensaje_cliente varchar(140),
    insert_update timestamp not null
);

CREATE TABLE ibmx_a07e6d02edaf552.public_customer(
	id SERIAL primary key,
	documento varchar(20),
	codigo_pedido varchar(20),
    telefono3 varchar(20),
    telefono4 varchar(20),
    direccion2 varchar(250),
    correo2 varchar(50),
    insert_update timestamp not null
);

CREATE TABLE ibmx_a07e6d02edaf552.public_customer_rating(
	id SERIAL primary key,
    documento varchar(20),
    codigo_pedido varchar(20),
    rango_fecha_inicio varchar(19),
    rango_fecha_fin varchar(19),
    respuesta1 integer,
    respuesta2 integer,
    respuesta3 integer,
    comentario text,
    insert_update timestamp not null
);

CREATE TABLE ibmx_a07e6d02edaf552.public_equivalence(
	id SERIAL primary key,
	source varchar(20),
    values_number integer,
    value1 varchar(100),
    value2 varchar(100),
    value3 varchar(100),
    traduction text,
    insert_update timestamp not null,
    status integer
);
INSERT INTO ibmx_a07e6d02edaf552.public_equivalence(id, source, values_number, value1, traduction, insert_update, status) VALUES
(1,'TOA',1,'WO_INIT','Se ha iniciado la instalación.', '2018-06-11 14:00:00'::date, 1),
(2,'TOA',1,'NOT LEG','Se le ha asignado un técnico a su pedido.', '2018-06-11 14:00:00'::date, 1),
(3,'TOA',1,'WO_PRE_COMPLETED','Se ha terminado la instalación. En espera de confirmación.', '2018-06-11 14:00:00'::date, 1),
(4,'TOA',1,'WO_COMPLETED','La instalación ha terminado satisfactoriamente.', '2018-06-11 14:00:00'::date, 1),
(5,'TOA',1,'WO_SUSPEND','Su pedido ha sido suspendida temporalmente.', '2018-06-11 14:00:00'::date, 1),
(6,'TOA',1,'WO_CANCEL','Su pedido ya no se está trabajando en TOA.', '2018-06-11 14:00:00'::date, 1);

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','devueltas.api.sync.cron1','30 12 * * * ?'),
(0,'CONFIG','PARAMETER','devueltas.api.sync.cron2','00 20 * * * ?'),
(0,'CONFIG','PARAMETER','devueltasc.api.sync.enabled','true'),
(0,'CONFIG','PARAMETER','devueltast.api.sync.enabled','true');

CREATE TABLE ibmx_a07e6d02edaf552.pedidos_en_caida(
	id SERIAL primary key,
	codigo_pedido varchar(20),
    tipo_caida varchar(10),
    intentos integer,
    estado boolean,
    insert_update timestamp not null,
    flg_email integer
);

CREATE TABLE ibmx_a07e6d02edaf552.traceability_external_log(
	id SERIAL primary key,
	codigo_pedido varchar(20),
    request text,
    response text,
    date timestamp
);

CREATE TABLE ibmx_a07e6d02edaf552.public_customer_rescheduled(
	id SERIAL primary key,
    documento varchar(20),
    codigo_pedido varchar(20),
    rango_fecha_inicio varchar(19),
    rango_fecha_fin varchar(19),
    insert_update timestamp not null
);
ALTER TABLE ibmx_a07e6d02edaf552.public_customer_rating DROP COLUMN rango_fecha_inicio;
ALTER TABLE ibmx_a07e6d02edaf552.public_customer_rating DROP COLUMN rango_fecha_fin;

INSERT INTO ibmx_a07e6d02edaf552.tdp_error_message_service(service,orden,descriptionkey,servicekey,responsemessage,
responsecode,servicecode) VALUES ('MSG_TRAZABILIDAD',9,'Instalado en retraso Reagendado','trz_msg_web_ins_ret_reagend',
'Reagendaste la llamada para el día [DIA_REAGENDA] desde las [DESDE_REAGENDA] hasta las [HASTA_PRODUCTO]',1,'MTRZ');

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.traceability.time.rescheduledenable','4');

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.traceability.enablefilterbydistrict','true');
INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.traceability.districtsinfilter','Lima-Lima-Lurín|Lima-Lima-Miraflores');

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.dc48','INTENTAMOS CONTACTARTE');

ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor
ADD COLUMN flg_registrado_ret_email integer;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor
ADD COLUMN flg_equipo_tecnico_ret_email integer;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor
ADD COLUMN flg_instalado_ret_email integer;
ALTER TABLE ibmx_a07e6d02edaf552.tdp_visor
ADD COLUMN flg_registrado_caida_email integer;


INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.traceability.timer.mailing','0 */1 * * * *'),
(0,'CONFIG','PARAMETER','tdp.traceability.timer.delayasign','0 0 0 1 1 *'),
(0,'CONFIG','PARAMETER','tdp.traceability.timer.dropdtdc','0 0 0 1 1 *'),
(0,'CONFIG','PARAMETER','tdp.traceability.timer.dc48','0 0 0 1 1 *');

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.traceability.enable.mailing','false'),
(0,'CONFIG','PARAMETER','tdp.traceability.enable.delayasign','false'),
(0,'CONFIG','PARAMETER','tdp.traceability.enable.dropdtdc','false'),
(0,'CONFIG','PARAMETER','tdp.traceability.enable.dc48','false');

INSERT INTO ibmx_a07e6d02edaf552.tdp_error_message_service(service,orden,descriptionkey,servicekey,responsemessage,
responsecode,servicecode) VALUES ('MSG_TRAZABILIDAD',7,'Tecnico Asignado desasignado WEB','trz_msg_web_tec_hou_bk',
'Tu técnico ha tenido un inconveniente. Nos estaremos comunicando en las próximas horas',1,'MTRZ'),
('MSG_TRAZABILIDAD',1,'Tecnico Asignado desasignado SMS','trz_msg_sms_tec_hou_bk',
'Tu técnico ha tenido un inconveniente. Nos estaremos comunicando en las próximas horas',1,'MTRZ');

//Hasta el pase del 2018-09-19

INSERT INTO ibmx_a07e6d02edaf552.parameters(auxiliar,domain,category,element,strvalue) VALUES
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.registered_down','RESULTADO DE TU PEDIDO'),
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.registered_delay','ESTAMOS TRABAJANDO PARA ATENDER TU PEDIDO'),
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.techassigned_down','RESULTADO DE TU PEDIDO'),
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.techassigned_delay','ESTAMOS TRABAJANDO PARA ATENDER TU PEDIDO'),
(0,'CONFIG','PARAMETER','tdp.mail.traceability.subject.installed_delay','ESTAMOS TRABAJANDO PARA ATENDER TU PEDIDO');