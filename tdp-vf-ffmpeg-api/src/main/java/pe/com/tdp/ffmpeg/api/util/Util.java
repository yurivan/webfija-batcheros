package pe.com.tdp.ffmpeg.api.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.io.File;
import java.io.IOException;
import java.util.Date;


/***
 * @author VASSLATAM
 * @implSpec this implementation provides different functions about handling stuff in Java
 * @implNote there are physical and absolute paths that must be satisfied by the host environment
 */
public class Util {

    private static final Logger logger = LogManager.getLogger(Util.class);

    private final static String mediaType = "audio/x-gsm";
    private final static String mediaTypeExtension = ".gsm";


    /***
     * This function provides the Headers to be used with the Spring RestController
     * @param fileName
     * @param length
     * @return headers describing a fileName and its contents length
     */
    public static HttpHeaders getDownloadHeaders(final String fileName, final long length) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType(mediaType));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
        headers.add("Access-Control-Allow-Headers", "Content-Type");
        headers.add("Content-Disposition", "filename=" + fileName);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.setContentLength(length);
        return headers;
    }


    /**
     * This function execute a host Process and when its completed returns the converted file
     * The returned file must be deleted manually afterwards when the "exists()" function returns true
     *
     * @param inputFile
     * @return converted gsm file
     */
    public static File convertToGsm(File inputFile) {
        String outputFile = buildString("/tmp/", "audio", String.valueOf(new Date().getTime()), mediaTypeExtension);
        try {
            String cmd = buildString("/opt/ffmpeg/ffmpeg -i ", inputFile.getAbsolutePath(), " -vn -ar 8000 -ac 1 -ab 13k -f gsm ", outputFile);
            logger.info(cmd);
            Process p = Runtime.getRuntime().exec(cmd);
            p.waitFor();
            File returningFile = new File(outputFile);
            if (returningFile.exists() && !returningFile.isDirectory()) {
                return returningFile;
            } else {
                logger.info("ffmpeg no ha generado el archivo para el comando: " + cmd);
            }
        } catch (IOException | InterruptedException e) {
            logger.error("Error de conversion", e);
        }
        return null;
    }


    /***
     * This is a simple StringBuilder
     * @param values
     * @return
     */
    public static String buildString(final String... values) {
        final StringBuilder sbr = new StringBuilder();
        for (String value : values) {
            sbr.append(value);
        }
        return sbr.toString();
    }
}
