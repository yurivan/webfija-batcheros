package pe.com.tdp.ffmpeg.api.web;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pe.com.tdp.ffmpeg.api.util.Util;

import java.io.*;
import java.util.Date;

/***
 * @author VASSLATAM
 */
@Controller
@RequestMapping("/api/v1")
public class FFmpegController {
    private static final Logger logger = LogManager.getLogger(FFmpegController.class);
    private static final String GSM_FORMAT = "gsm";


    /***
     * This function receives a binary file which is obtained vía HTTP request. The parameter must be a WAV file.
     * The WAV file obtained is processed as a GSM audio file vía FFmpeg's GSM encoder
     * @param file
     * @return
     */
    @RequestMapping(value = "/gsm", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    @ResponseBody
    public ResponseEntity<InputStreamResource> processWAVtoGSM(@RequestParam("file") MultipartFile file) {

        try {
            InputStream is = file.getInputStream();
            File outputFile = File.createTempFile("audio" + new Date().getTime(), "_tmp.wav");
            OutputStream os = new FileOutputStream(outputFile);
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = is.read(buffer)) != -1) {
                os.write(buffer, 0, bytesRead);
            }
            is.close();
            os.flush();
            os.close();
            File result = Util.convertToGsm(outputFile);

            final HttpHeaders download = Util.getDownloadHeaders(result.getName(), result.length());
            ResponseEntity<InputStreamResource> response = new ResponseEntity<InputStreamResource>(new InputStreamResource(new FileInputStream(result)), download, HttpStatus.OK);

            if (result.exists())
                result.delete();
            if (outputFile.exists())
                outputFile.delete();

            return response;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}