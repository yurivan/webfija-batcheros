package pe.com.tdp.ffmpeg.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/***
 * @author VASSLATAM
 */
@EnableAutoConfiguration
@SpringBootApplication
public class FFmpegAPI {


    public static void main(String[] args) {
        SpringApplication.run(FFmpegAPI.class, args);
    }
}
