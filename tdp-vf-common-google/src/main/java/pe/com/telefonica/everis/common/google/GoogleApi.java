package pe.com.telefonica.everis.common.google;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.auth.Credentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.ReadChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.StorageOptions;

public class GoogleApi {
	private static final Logger logger = LoggerFactory.getLogger(GoogleApi.class);
	private GoogleApiConfig config;

	public GoogleApi(GoogleApiConfig config) {
		super();
		this.config = config;
	}

	public File retriveFileFromZip(String orderId, String firebaseObject, String audioName) {
		logger.info("::" + orderId + ":: RetriveFileFromZip => Inicio");

		File audio = null;
		ZipFile zipFile = null;
		File fileZipFirebase;

		FileOutputStream fos = null;
		InputStream in = null;
		try {
			fileZipFirebase = retrieveFile(orderId, firebaseObject);

			zipFile = new ZipFile(fileZipFirebase);

			if (fileZipFirebase != null) {
				fileZipFirebase.delete();
			}

			ZipEntry entry = zipFile.getEntry(audioName);
			if (entry != null) {
				in = zipFile.getInputStream(entry);

				byte[] buffer = new byte[1024];

				audio = File.createTempFile("audio" + new Date().getTime(), ".tmp");

				int len;
				fos = new FileOutputStream(audio);

				while ((len = in.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
			}
		} catch (IOException e) {
			logger.error("::" + orderId + ":: RetriveFileFromZip => IOException: " + e.getMessage(), e);

		} catch (Exception e) {
			logger.error("::" + orderId + ":: RetriveFileFromZip => Exception: " + e.getMessage(), e);
		} finally {
			try {
				if (zipFile != null)
					zipFile.close();
				if (fos != null)
					fos.close();
				if (in != null)
					in.close();
			} catch (IOException e) {
				logger.error("::" + orderId + ":: RetriveFileFromZip => IOException: " + e.getMessage(), e);
			}
		}

		logger.info("::" + orderId + ":: RetriveFileFromZip => audio: " + audio);
		logger.info("::" + orderId + ":: RetriveFileFromZip => Fin");
		return audio;
	}

	public File retrieveFile(String orderId, String firebaseObject) throws FileNotFoundException, IOException {
		return downloadFile(config.getGoogleStorageBucket(), firebaseObject, orderId);
	}

	public File downloadFile(String bucket, String name, String orderId) throws FileNotFoundException, IOException {
		logger.info("::" + orderId + ":: DownloadFile => Inicio");

		File file = null;
		ClassLoader classLoader = getClass().getClassLoader();
		File creds = new File(classLoader.getResource("App-Venta-Fija-7f83e030b88c.json").getFile());
		Credentials credentials = ServiceAccountCredentials.fromStream(new FileInputStream(creds));

		StorageOptions.Builder optionsBuilder = StorageOptions.newBuilder();
		optionsBuilder.setProjectId(config.getGoogleApiProjectId());

		com.google.cloud.storage.Storage storage = optionsBuilder
				.setCredentials(credentials)
				.build()
				.getService();

		logger.info("::" + orderId + ":: DownloadFile => Firebase: " + String.format("Descargando archivo %s de %s", name, bucket));
		Blob blob = storage.get(BlobId.of(bucket, name));
		if (blob != null) {
			file = File.createTempFile("download", ".tmp");
			try (ReadChannel reader = blob.reader()) {
				PrintStream writeTo = new PrintStream(new FileOutputStream(file));
				WritableByteChannel channel = Channels.newChannel(writeTo);
				ByteBuffer bytes = ByteBuffer.allocate(64 * 1024);
				while (reader.read(bytes) > 0) {
					bytes.flip();
					channel.write(bytes);
					bytes.clear();
				}
				channel.close();
				writeTo.close();
			}
		}else{
			byte[] buffer = new byte[1024];
			file = File.createTempFile("audio" + new Date().getTime(), ".tmp");
			int len;
			FileOutputStream fos = new FileOutputStream(file);
			int cantBytes = 40;
			while ((cantBytes) > 0) {
				fos.write(buffer, 0,  (int) Math.floor(Math.random() * 2 + 1));
				cantBytes--;
			}
		}

		logger.info("::" + orderId + ":: DownloadFile => Fin");
		return file;
	}

	@Override
	public String toString() {
		return "GoogleApi [config=" + config + "]";
	}
}
