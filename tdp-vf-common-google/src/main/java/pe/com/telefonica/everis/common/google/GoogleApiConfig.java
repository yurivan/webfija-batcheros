package pe.com.telefonica.everis.common.google;

public class GoogleApiConfig {
	private String googleStorageBucket;
	private String googleApiJsonCredential;
	private String googleApiApplicationName;
	private String googleApiProjectId;

	public GoogleApiConfig(String googleStorageBucket, String googleApiJsonCredential, String googleApiApplicationName,
			String googleApiProjectId) {
		super();
		this.googleStorageBucket = googleStorageBucket;
		this.googleApiJsonCredential = googleApiJsonCredential;
		this.googleApiApplicationName = googleApiApplicationName;
		this.googleApiProjectId = googleApiProjectId;
	}

	public String getGoogleStorageBucket() {
		return googleStorageBucket;
	}

	public void setGoogleStorageBucket(String googleStorageBucket) {
		this.googleStorageBucket = googleStorageBucket;
	}

	public String getGoogleApiJsonCredential() {
		return googleApiJsonCredential;
	}

	public void setGoogleApiJsonCredential(String googleApiJsonCredential) {
		this.googleApiJsonCredential = googleApiJsonCredential;
	}

	public String getGoogleApiApplicationName() {
		return googleApiApplicationName;
	}

	public void setGoogleApiApplicationName(String googleApiApplicationName) {
		this.googleApiApplicationName = googleApiApplicationName;
	}

	public String getGoogleApiProjectId() {
		return googleApiProjectId;
	}

	public void setGoogleApiProjectId(String googleApiProjectId) {
		this.googleApiProjectId = googleApiProjectId;
	}

	@Override
	public String toString() {
		return "GoogleApiConfig [googleStorageBucket=" + googleStorageBucket + ", googleApiJsonCredential="
				+ googleApiJsonCredential + ", googleApiApplicationName=" + googleApiApplicationName
				+ ", googleApiProjectId=" + googleApiProjectId + "]";
	}

	public static class GoogleApiConfigBuilder {
		private String googleStorageBucket;
		private String googleApiJsonCredential;
		private String googleApiApplicationName;
		private String googleApiProjectId;

		public GoogleApiConfigBuilder setGoogleStorageBucket(String googleStorageBucket) {
			this.googleStorageBucket = googleStorageBucket;
			return this;
		}

		public GoogleApiConfigBuilder setGoogleApiJsonCredential(String googleApiJsonCredential) {
			this.googleApiJsonCredential = googleApiJsonCredential;
			return this;
		}

		public GoogleApiConfigBuilder setGoogleApiApplicationName(String googleApiApplicationName) {
			this.googleApiApplicationName = googleApiApplicationName;
			return this;
		}

		public GoogleApiConfigBuilder setGoogleApiProjectId(String googleApiProjectId) {
			this.googleApiProjectId = googleApiProjectId;
			return this;
		}
		
		public GoogleApiConfig build () {
			return new GoogleApiConfig(googleStorageBucket, googleApiJsonCredential, googleApiApplicationName, googleApiProjectId);
		}
	}

}
