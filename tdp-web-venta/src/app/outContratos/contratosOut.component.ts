import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';
import { ProgressBarService } from '../services/progressbar.service';
import { WhatsApp } from '../model/whatsapp';
var $ = require('jquery');

@Component({
	moduleId: 'contratosOut',
	selector: 'contratosOut',
	templateUrl: './contratosOut.template.html',
	providers: [CustomerService, OrderService]
})

export class ContratosOutComponent implements OnInit {
	@ViewChild('fileInput') inputEl: ElementRef;
	contract = [];
	order: Order;
	closeSaleError: boolean;
	closeSaleErrorMessage: string;
	private loading: boolean;
	disaffiliationWhitePagesGuide = true;
	affiliationElectronicInvoice = true;
	affiliationDataProtection = true;
	publicationWhitePagesGuide = true;
	shippingContractsForEmail = true;
	cipError: boolean = false;
	isWsp: boolean;

	//WHATSAPP
	whatsapp: WhatsApp;
	channels: string[];
	codewsp: number;


	constructor(private router: Router,
		private customerService: CustomerService,
		private orderService: OrderService,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService) {
		this.router = router;
		this.customerService = customerService;
		this.closeSaleError = false;
		this.closeSaleErrorMessage = '';
	}

	ngOnInit() {


		this.progresBar.getProgressStatus()
		this.loading = true;
		this.order = JSON.parse(localStorage.getItem('order'));

		this.whatsapp = JSON.parse(localStorage.getItem('whatsApp'));
		this.isWsp = this.whatsapp.onOff;
		this.channels = this.whatsapp.canal.split(',');

		this.customerService.getContract(this.order)
			.subscribe(
				data => {
					if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
						this.contract = data.responseData.contractWeb
						let count = 0;
						for (let i = 0; i < this.contract.length; i++) {
							if (this.contract[i].category == 'client_has_email') {
								count++;
							}
						}

						for (let i = 0; i < this.contract.length; i++) {
							if (this.contract[i].category == 'client_has_email') {
								count--;
								this.contract[i].category = this.contract[i].category + count;
							}
						}
					} else {
						alert(data.responseMessage);
					}
				},
				err => {
					//console.log(err)
				}
			);

		if (typeof this.order.disaffiliationWhitePagesGuide !== 'undefined') {
			this.disaffiliationWhitePagesGuide = this.order.disaffiliationWhitePagesGuide;
		}
		if (typeof this.order.affiliationElectronicInvoice !== 'undefined') {
			this.affiliationElectronicInvoice = this.order.affiliationElectronicInvoice;
		}
		if (typeof this.order.affiliationDataProtection !== 'undefined') {
			this.affiliationDataProtection = this.order.affiliationDataProtection;

		}
		if (typeof this.order.publicationWhitePagesGuide !== 'undefined') {
			this.publicationWhitePagesGuide = this.order.publicationWhitePagesGuide;
		}
		if (typeof this.order.shippingContractsForEmail !== 'undefined') {
			this.shippingContractsForEmail = this.order.shippingContractsForEmail;
		}
		this.loading = false;
		this.getCIP();
	}

	getCIP() {
		/*if (this.order.id != null && this.order.id != '') {
			this.order.status = "F";
			this.ls.setData(this.order);
			this.closeSaleError = false;
			this.loading = false;
			return;
		}*/

		let webOrder = this.createWebOrder()
		for (let i = 0; i < this.order.sva.length; i++) {
			let sva = this.order.sva[i];
			webOrder.sva.push(sva.id);
		}
		if (this.tieneNumeros(webOrder.department)) {
			let strDepartment = webOrder.department;
			webOrder.department = strDepartment.substring(4);
		}
		if (this.tieneNumeros(webOrder.province)) {
			let strProvince = webOrder.province;
			webOrder.province = strProvince.substring(4);
		}
		if (this.tieneNumeros(webOrder.district)) {
			let strDistrinct = webOrder.district;
			webOrder.district = strDistrinct.substring(4);
		}
		this.orderService.cerrarVenta(webOrder).subscribe(
			data => {
				if (data.responseCode == '00') {
					this.order.id = data.responseData.orderId;
					this.order.cip = data.responseData.cip;
					this.loading = true;
					this.order.status = "F"

					//verificamos si es que el pago es al contado y no se genero cip
					if (this.order.scoringDataModel && this.order.scoringDataModel.responseData.result) {
						var pagoAdelantado = this.order.scoringDataModel.responseData.result[0].detalle[0].pagoAdelantado;
						if (pagoAdelantado > 0 && this.order.cip == "") {
							this.cipError = true;
						} else {
							this.cipError = false;
						}
					}


					this.ls.setData(this.order);
					this.closeSaleError = false;
				} else {
					this.closeSaleError = true;
					this.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				this.loading = false;
				this.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				this.loading = false;
			}
		);
	}
	/*sprint 3 - esta funcion ya no sera utilizada... sera reemplazada por cerrarVenta*/
	uploadAudio() {
		let audioElement = this.inputEl.nativeElement;
		if (audioElement.files.length < 1) {
			alert('seleccione el audio a cargar...');
			return;
		}
		let file = audioElement.files[0];
		let fileName = audioElement.value;
		if (!fileName.toLowerCase().endsWith('.gsm')) {
			alert('el archivo seleccionado debe ser del formato GSM.');
			return;
		}
		let webOrder = this.createWebOrder()
		for (let i = 0; i < this.order.sva.length; i++) {
			let sva = this.order.sva[i];
			webOrder.sva.push(sva.id);
		}
		this.loading = true;

		if (this.tieneNumeros(webOrder.department)) {
			let strDepartment = webOrder.department;
			webOrder.department = strDepartment.substring(4);
		}

		if (this.tieneNumeros(webOrder.province)) {
			let strProvince = webOrder.province;
			webOrder.province = strProvince.substring(4);
		}

		if (this.tieneNumeros(webOrder.district)) {
			let strDistrinct = webOrder.district;
			webOrder.district = strDistrinct.substring(4);
		}

		this.orderService.cerrarVenta(webOrder).subscribe(
			data => {
				if (data.responseCode == '00') {
					this.order.id = data.responseData.orderId;
					this.order.cip = data.responseData.cip;
					this.order.status = "F"
					this.ls.setData(this.order);
					this.closeSaleError = false;
					this.orderService.uploadAudio(this.order.id, file);
					this.router.navigate(['contactoinstalacionOut']);
				} else {
					this.closeSaleError = true;
					this.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				this.loading = false;
				this.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				this.loading = false;
			}
		);
	}

	cerrarVenta() {
		for (let canal of this.channels) {
			if (canal == this.order.user.sellerChannelEquivalentCampaign) {
				if (this.isWsp) {
					this.codewsp = 1;
					break;
				} else {
					this.codewsp = 0;
					break;
				}
			} else {
				this.codewsp = 2;
			}
		}
		localStorage.setItem('codewsp', JSON.stringify(this.codewsp));
		this.router.navigate(['contactoinstalacionOut']);

		/*
		let _xthis = this;
		let webOrder = {
			orderId: _xthis.order.id,
			whatsapp: _xthis.codewsp
		};

		this.orderService.actualizarEstado(webOrder).subscribe(
			data => {
				console.log("Data in contract component" + data)
				if (data.responseCode == '00') {
					this.closeSaleError = false;

					//sprint 3 - ya no se subiran audios en el flujo de venta
					this.router.navigate(['contactoinstalacionOut']);
				} else {
					this.closeSaleError = true;
					this.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				this.loading = false;
				this.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				this.loading = false;
			}
		);*/
	}

	sendTiendaVenta() {
		let webOrder = this.createWebOrder()
		for (let i = 0; i < this.order.sva.length; i++) {
			let sva = this.order.sva[i];
			webOrder.sva.push(sva.id);
		}
		//this.loading = true;
		this.orderService.cerrarVenta(webOrder).subscribe(
			data => {
				if (data.responseCode == '00') {
					this.order.id = data.responseData.orderId;
					this.order.cip = data.responseData.cip;
					this.order.status = "F"
					this.ls.setData(this.order);
					this.closeSaleError = false;

					this.router.navigate(['contactoinstalacionOut']);

				} else {
					this.closeSaleError = true;
					this.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				this.loading = false;
				this.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				this.loading = false;
			}
		);
	}


	//cusi
	tieneNumeros(texto) {
		var numeros = "0123456789";
		for (let i = 0; i < texto.length; i++) {
			if (numeros.indexOf(texto.charAt(i), 0) != -1) {
				return true;
			}
		}
		return false;
	}

	createWebOrder() {
		this.order.cip = " "

		var svaList = [];
		$.each(this.order.sva, function (index, item) {
			svaList.push(item.id);
		});

		let webOrder = {
			orderId: this.order.id,
			userId: this.order.user.userId,
			phone: this.order.product.phone,
			department: this.order.product.department,
			province: this.order.product.province,
			district: this.order.product.district,
			address: this.order.product.address,
			latitudeInstallation: this.order.product.coordinateY ? this.order.product.coordinateY : 0,
			longitudeInstallation: this.order.product.coordinateX ? this.order.product.coordinateX : 0,
			addressAdditionalInfo: this.order.product.additionalAddress ? this.order.product.additionalAddress : '',
			sva: svaList,
			customer: this.order.customer,
			product: this.order.selectedOffering,
			type: this.order.type,
			disaffiliationWhitePagesGuide: this.order.disaffiliationWhitePagesGuide,
			affiliationElectronicInvoice: this.order.affiliationElectronicInvoice,
			affiliationDataProtection: this.order.affiliationDataProtection,
			publicationWhitePagesGuide: this.order.publicationWhitePagesGuide,
			shippingContractsForEmail: this.order.shippingContractsForEmail,
			originOrder: this.order.user.groupPermission,
			// Sprint 5 - campos adicionales al cerrar la venta: nombrePuntoVenta y entidad
			entidad: this.order.user.entity,
			nombrePuntoVenta: this.order.user.group,
			// Sprint 6 - condicion preventa filtro web parental
			parentalProtection: this.order.parentalProtection,
			// Sprint 21 - condicion preventa filtro debito automatico
			automaticDebit: this.order.automaticDebit,
			// Sprint 9 - Nombre del producto de origen (solo servirá para flujo SVA)
			sourceProductName: this.order.product.productDescription,
			// Sprint 10 - Campania seleccionada para altas y migras
			campaniaSeleccionada: this.order.campaniaSeleccionada,
			// Sprint 13 - Envio datos normalizador hacia automatizador parte 2
			address_referencia: this.order.address_referencia,
			address_ubigeoGeocodificado: this.order.address_ubigeoGeocodificado,
			address_descripcionUbigeo: this.order.address_descripcionUbigeo,
			address_direccionGeocodificada: this.order.address_direccionGeocodificada,
			address_tipoVia: this.order.address_tipoVia,
			address_nombreVia: this.order.address_nombreVia,
			address_numeroPuerta1: this.order.address_numeroPuerta1,
			address_numeroPuerta2: this.order.address_numeroPuerta2,
			address_cuadra: this.order.address_cuadra,
			address_tipoInterior: this.order.address_tipoInterior,
			address_numeroInterior: this.order.address_numeroInterior,
			address_piso: this.order.address_piso,
			address_tipoVivienda: this.order.address_tipoVivienda,
			address_nombreVivienda: this.order.address_nombreVivienda,
			address_tipoUrbanizacion: this.order.address_tipoUrbanizacion,
			address_nombreUrbanizacion: this.order.address_nombreUrbanizacion,
			address_manzana: this.order.address_manzana,
			address_lote: this.order.address_lote,
			address_kilometro: this.order.address_kilometro,
			address_nivelConfianza: this.order.address_nivelConfianza,
			//Anthony - Sprint 15
			serviceType: this.order.product.parkType,
			cmsServiceCode: this.order.product.serviceCode,
			cmsCustomer: this.order.product.subscriber,
			//Sprint 16
			datetimeSalesCondition: this.order.datetimeSalesCondition,
			offline : this.order.offline,
			//Sprint 27 - dochoaro - tipificacion
            tipificacion: this.order.user.typeFlujoTipificacion   
		}

		return webOrder
	}
}
