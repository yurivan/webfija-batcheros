import { Component, OnInit, OnDestroy, Input, NgModule } from '@angular/core';

@Component({
    selector: 'cool-loading-indicator',
    template: `
        <div *ngIf="showIndicator" class="cool-loading-indicator">
            <ng-content></ng-content>
        </div>
    `
})
export class LoadingComponent {
  showIndicatorCounter: number = 0;
  showIndicator: boolean = false;

  //
}
