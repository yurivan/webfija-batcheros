import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DireccionService } from '../services/direccion.service';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order';
import { Customer } from '../model/customer';
import { LocalStorageService } from '../services/ls.service';
import * as globals from '../services/globals';
var $ = require('jquery');
import { ProgressBarService } from '../services/progressbar.service';
(<any>window).jQuery = $

//import $ = require('jquery');

import 'date-input-polyfill';
import { Argumentarios } from '../model/argumentarios';
import { Parameters } from '../model/parameters';

@Component({
    moduleId: 'datoscliente',
    selector: 'datoscliente',
    templateUrl: 'datoscliente.template.html',
    providers: [DireccionService, CustomerService, OrderService]
})
export class DatosClienteComponent implements OnInit {

    direccionService: DireccionService;
    customerService: CustomerService;

    objDepart: any;
    private keyDeparts: string[];
    private departSeleccionado: any;
    selectDepart: any;

    mjsDatosCliente : string;
    argumentarios : Argumentarios[];
    objProvs: any;
    showProvs = new Array();
    private keyProvs: string[];
    selectProv: any;
    private proviSeleccionado: any;

    objDists: any;
    showDists = new Array();
    selectDistric: any;
    private keyDistrs: string[];
    private districSeleccionado: any;
    months = new Array()
    listErrorMessage = new Array()

    objReniec: any;
    distModel: any;
    provModel: any;
    order: Order;
    canalEntidad : Parameters;
    customer: Customer;
    customerReniec: Customer;
    mensajeValidacion: String;
    channelCall: boolean;

    private loadError: boolean;
    private loading: boolean;
    private mensaje_error: String;
    cargaDept: boolean;

    private validAttempts: number;
    private reniecValidAttempts: number;
    private reniecMaxErrors: number;
    webOrder: any;

    //glazaror inicio variables para parentesco
    objParentesco: any;
    private keyParentescoList: string[];
    private parentescoSeleccionado: any;
    selectParentesco: any;

    parentescoModel: any;
    //glazaror fin variables para parentesco

    mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios
    isCheckedNameUser: boolean
    isCheckedPlaceOfBirth: boolean
    isCheckedDayOfBirth: boolean
    isItOkMothersName: boolean
    mothersName: boolean
    isValidationCompleted: boolean

    objfecnac: any;
    contValidation: number
    modalHome: boolean
	
	paises : any;
	
	nombres : string;
	apellidoPaterno : string;
	apellidoMaterno : string;
	nacionalidad : string;
	fechaNacimiento : string;
	
    constructor(private router: Router,
        direccionService: DireccionService,
        customerService: CustomerService,
        private ls: LocalStorageService,
        private orderService: OrderService,
        private progresBar: ProgressBarService) {

        this.contValidation = 0
        this.router = router;
        this.direccionService = direccionService;
        this.customerService = customerService;
        this.ls = ls;
        this.cargaDept = false;
        this.distModel = 0;
        this.provModel = 0;
        this.parentescoModel = 0; //glazaror
        this.loadError = false;
        this.mensaje_error = "";
        this.validAttempts = 0;
        this.reniecValidAttempts = globals.RENIEC_VALID_ATTEMPTS;
        this.reniecMaxErrors = globals.RENIEC_MAX_ERRORS;

        this.orderService = orderService;
        this.objReniec = {}
        this.mensajeValidacionDatosCliente = "";// Sprint 6
        this.isCheckedNameUser = false
        this.isCheckedPlaceOfBirth = false
        this.isCheckedDayOfBirth = false
        this.isValidationCompleted = false
        this.isItOkMothersName = false
        this.modalHome = false
        this.months = [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"]
    }

    ngOnInit() {
        $(window).ready(() => {
            $("#modalHome").modal('hide')
        })
        $(document).ready(() => {
            $(".paraselect select").val("");
            $(".paraselect select").focusout(function () {
                if ($(this).val() != "") {
                    $(this).addClass("has-content");
                } else {
                    $(this).removeClass("has-content");
                }
            })

        })
        this.progresBar.getProgressStatus()
        // add loading 
        this.loading = true;

        this.obtenerDepartamentos();
        //sprint2 no se debe mostrar provincia ni distrito
        //this.obtenerProvincias();
        //this.obtenerDistritos();

        this.order = JSON.parse(localStorage.getItem('order'))
        this.canalEntidad = JSON.parse(localStorage.getItem('canalEntidad'))
        this.customer = this.order.customer

        this.argumentarios = JSON.parse(localStorage.getItem('argumentarios'));
        for (let i in this.argumentarios) {
            let obj = new Argumentarios;
            obj = this.argumentarios[i];
            if(obj.pantalla.toUpperCase() == "datosCliente".toUpperCase()){
                this.mjsDatosCliente = obj.mensaje;
            }
        }

        //if( this.customer.documentType == "CE"){
        //  this.router.navigate(['/saleProcess/close']);
        //} else {
        // console.log("departamento seleccionado " + this.customer.birthDep);
        this.channelCall = true;
        // Sprint 6 - validacion RENIEC ahora tambien se muestra para los vendedores con perfil TIENDA
        //if(this.customer && this.customer.documentType == "DNI" && this.order.user.typeFlujoTipificacion != "PRESENCIAL" ){
        
        //}
		
		// Sprint 7
		this.customerService.getPaises()
			.subscribe(
				data => {
					this.paises = data;
					//alert(data.responseMessage);
					
				},
				err => {
					console.log(err)
				}
		);
		
		this.cargarDatosCliente();
		
		this.loading = false;

    }
	
	cargarDatosCliente() {
        if(this.order.customer.documentType !== 'RUC' && this.order.customer.documentNumber.substr(0,2) !== '20'){
		this.nombres = this.order.customer.firstName;
		this.apellidoPaterno = this.order.customer.lastName1;
		this.apellidoMaterno = this.order.customer.lastName2;
		this.nacionalidad = this.order.customer.nationality;
		this.fechaNacimiento = this.order.customer.birthDate;
	}
	}

    convertirInfoListaReniec() {

        let fecNac: String;
        let fechaExp: String;

        if (this.objReniec != null) {
            fecNac = this.objReniec.birthdate;
            this.objReniec.birthdate = fecNac.substring(6, 8) + "/" + fecNac.substring(4, 6) + "/" + fecNac.substring(0, 4);

            fechaExp = this.objReniec.expeditionDate;
            this.objReniec.expeditionDate = fechaExp.substring(6, 8) + "/" + fechaExp.substring(4, 6) + "/" + fechaExp.substring(0, 4);

            this.obtainNameDep(this.objReniec.departmentBirthLocation);

        }

    }

    obtenerDepartamentos() {
        this.objDepart = this.direccionService.getDataDepartamentoReniec();

        this.keyDeparts = Object.keys(this.objDepart);
    }

    //glazaror inicio nueva funcion para cargar parentesco
    obtenerListaParentesco(isMother) {
        let parentescoElement: any;

        if (isMother) {
            this.keyParentescoList = this.objReniec.motherNameAlternatives;
        } else {
            this.keyParentescoList = this.objReniec.fatherNameAlternatives;
        }
        this.keyParentescoList.splice(0, 1);
        //glazaror
        parentescoElement = document.getElementById("parentescoSelect");
        /*if (parentescoElement != null) {
            parentescoElement.options[0].selected = true;
        }*/

    }
    //glazaror fin nueva funcion para cargar parentesco

    obtainNameDep(sku) {
        //this.listarProvincias(sku);

        this.keyDeparts.forEach((item, index) => {
            if (sku != null) {
                if (sku !== 'undefined') {
                    if (this.objDepart[item].sku == parseInt(sku).toString()) {
                        this.objReniec.departmentName = (this.objDepart[item].name);
                    }
                }
            }
        });
    }

    selDepart() {
        //console.log(this.objReniec.departmentBirthLocation)
        if (this.customer.birthDep == null) {
            this.selectDepart = document.getElementById("department");
            if (this.selectDepart) {
                this.departSeleccionado = this.selectDepart.value;
                this.obtainNameDep(this.objReniec.departmentBirthLocation);
            } else {
                this.obtainNameDep(this.objReniec.departmentBirthLocation);
            }

            //this.distModel=0;
            //this.provModel=0;
        } else {
            this.departSeleccionado = this.customer.birthDep
            this.obtainNameDep(this.objReniec.departmentBirthLocation);
        }
        //this.listarProvincias(this.departSeleccionado);

    }

    //sprint2 no se debe mostrar city
    /*selCity(){

         if( this.customer.birthCity == null || this.customer.birthCity === ""){
             this.selectProv = document.getElementById("city");
             this.proviSeleccionado = this.selectProv.value;
             this.listarDistritos(this.departSeleccionado+this.proviSeleccionado);

         }else{
             this.proviSeleccionado =  this.customer.birthCity;
             this.provModel= this.customer.birthCity;
             this.listarDistritos(this.departSeleccionado+this.proviSeleccionado);
             //console.log("listar distritos: "+ this.departSeleccionado+this.proviSeleccionado);
         }

    }*/

    //sprint2 no se debe mostrar distrito
    /*selDistrict(){
        if( this.customer.birthDist == null){
            this.selectDistric = document.getElementById("district");
            this.districSeleccionado = this.selectDistric.value;
        }else{
            this.districSeleccionado = this.customer.birthDist
            this.distModel= this.customer.birthDist;
        }

   }*/

    obtainReniec() {

        this.loading = true;
        //console.log("obtain REniec " + JSON.stringify(this.customer))
        this.customerService.getIdentify(this.customer, this.order.id)
            .subscribe(
                data => {
                    if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
                        // add data perfil tienda
                        this.objReniec = data.responseData;

                      //  console.log("objeto Reniec " + JSON.stringify(this.objReniec))
                        //console.log("objReniec " + JSON.stringify(this.objReniec))

                        // console.log("mother " + JSON.stringify((this.objReniec)))
                        this.obtenerListaParentesco(true);//glazaror carga de parentesco
                        /*if (this.order.user.typeFlujoTipificacion == "PRESENCIAL") {
                            this.continueSale();
                        }*/
                        //console.log(this.objReniec);
                    } else {
                        this.mostrarError();
                    }
                },
                err => {
                   // console.log(err);
                    this.mostrarError();
                },
                () => {

                    if (this.objReniec != null) {
                        //console.log("objReniec " + JSON.stringify(this.objReniec))
                        this.customer.birthDate = this.objReniec.birthdate.substring(0, 4) + "-" + this.objReniec.birthdate.substring(4, 6) + "-" + this.objReniec.birthdate.substring(6, 8)
                        //console.log("this.customer.birthDate " + this.customer.birthDate)
                        if (this.customer.birthDate) {
                            let date1 = this.customer.birthDate
                            let p = date1.split("-")
                            let connector = "de"
                            if (parseInt(p[0]) >= 2000) {
                                connector = "del"
                            }
                            this.customer.birthDate = `${p[2]} de ${this.months[parseInt(p[1]) - 1]} ${connector} ${p[0]}`
                        } else {
                            this.customer.birthDate = "No se encontró la fecha de nacimiento."

                        }
                        this.loading = false;
                        this.loadError = false;
                        if (this.channelCall) {
                            if (this.customer.birthDep) {
                                this.selDepart();
                                //this.selCity();
                                //this.selDistrict();

                            } else {
                                this.selDepart();
                            }
                            //this.compararDatos();

                        } else {
                            this.convertirInfoListaReniec();
                            this.loading = true;
                        }
                    } else {
                        this.mostrarError();
                    }
                });
    }

    cancelSale() {
        this.webOrder = {};
        this.order = this.ls.getData();
        //console.log("inicio",this.order);
        this.webOrder.userId = this.order.user.userId;
        this.webOrder.customer = this.order.customer;
        if (this.order.product != null && this.order.type != globals.ORDER_ALTA_NUEVA) {
            this.webOrder.phone = this.order.product.phone;
            this.webOrder.department = this.order.product.department;
            this.webOrder.province = this.order.product.province;
            this.webOrder.district = this.order.product.district;
            this.webOrder.address = this.order.product.address;
            this.webOrder.latitudeInstallation = this.order.product.coordinateY ? this.order.product.coordinateY : 0;
            this.webOrder.longitudeInstallation = this.order.product.coordinateX ? this.order.product.coordinateX : 0;
            this.webOrder.addressAdditionalInfo = this.order.product.additionalAddress ? this.order.product.additionalAddress : null;
            //console.log("product != null", this.order);
        }
        if (this.order.selectedOffering != null) {
            this.webOrder.product = this.order.selectedOffering;
        }
        this.webOrder.cancelDescription = "Validacion Reniec";
        this.webOrder.sva = [];
        this.webOrder.type = this.order.type;
        this.webOrder.disaffiliationWhitePagesGuide = this.order.disaffiliationWhitePagesGuide;
        this.webOrder.affiliationElectronicInvoice = this.order.affiliationElectronicInvoice;
        this.webOrder.affiliationDataProtection = this.order.affiliationDataProtection;
        this.webOrder.publicationWhitePagesGuide = this.order.publicationWhitePagesGuide;
        this.webOrder.shippingContractsForEmail = this.order.shippingContractsForEmail;

        if (this.order.sva != null) {
            for (let i = 0; i < this.order.sva.length; i++) {
                let sva = this.order.sva[i];
                this.webOrder.sva.push(sva.id);
            }
        }
        $('#modalHome').modal('hide');
        this.modalHome = false
        this.loading = true;
        this.orderService.cancelSale(this.webOrder).subscribe(
            data => {
                //console.log(data);
                if (data.responseCode == "00") {
                    //this.errorSending = false;
                    this.router.navigate(['/acciones']);
                    //this.closeModal();
                } else {
                    //this.errorSending = true;
                }
                this.loading = false;
            },
            err => {
                this.loading = false;
              //  console.log(err);
            }
        );
    }

    mostrarError() {
        this.loading = false;
        this.loadError = true;
        this.mensaje_error = "El servicio Reniec dio un error, Por favor vuelva a intentarlo."
    }

    //validar() {
    // Sprint 4 - validamos que se haya ingresado celular, telefono, correo
    //var customer = this.ls.getData().customer;
    //if (customer.mobilePhone == null || customer.mobilePhone == "" || customer.email == null || customer.email == "") {
    //if (this.ls.getFlagDatosClienteValidos() == "1" || this.ls.getFlagDatosClienteValidos() == "2") {
    /*if (this.ls.getFlagDatosClienteValidos() == "3" || this.ls.getFlagDatosClienteValidos() == "4") {
        //alert("Por favor complete los datos del cliente");
        if (this.ls.getFlagDatosClienteValidos() == "3") {
            //alert("Falta correo y correo");
            this.mensajeValidacionDatosCliente = "Celular y Correo";
        } else if (this.ls.getFlagDatosClienteValidos() == "4") {
            this.mensajeValidacionDatosCliente = "Celular";
        }
        $('#modalCliente').modal('show');
    } else {*/
    //if (customer.mobilePhone == null || customer.mobilePhone == "" /*|| customer.email == null || customer.email == ""*/) {
    //this.mensajeValidacionDatosCliente = "Celular";
    //$('#modalCliente').modal('show');
    //} else {
    //this.order.customer.mobilePhone = customer.mobilePhone;
    //this.order.customer.email = customer.email;
    //this.order.customer.telephone = customer.telephone;
    //this.mensajeValidacion = "";
    //this.ls.setData(this.order);
    //if (this.validAttempts + 1 < this.reniecValidAttempts) {
    //    $('#myModalReniec').modal('show');
    //}

    //this.compararDatos();
    //}
    //}

    compararDatos() {

        if (this.channelCall) {

            let cont = 0;

            let cump = 0;
            let birthDate: any;
            //let fecExp : any;
            let completeName: any;

            let parentescoElement: any;//glazaror variable parentesco

            birthDate = document.getElementById("bday");
            //fecExp = document.getElementById("fecExpedicion");
            completeName = document.getElementById("fullname");

            //glazaror
            parentescoElement = document.getElementById("parentescoSelect");


            //console.log("nombre  completo " + completeName.value);

            /*if(birthDate.value.length !== 0){
             birthDate = birthDate.value;
             birthDate = birthDate.replace(/\-/g,'');
            }*/

            /*if(fecExp.value.length !== 0){
                fecExp = fecExp.value;
                fecExp = fecExp.replace(/\-/g,'');
            }*/
            if (this.objReniec) {

                /*if(this.departSeleccionado == this.objReniec.departmentBirthLocation){
                        cont ++;
                }*/

                //            let completeNameReniec =this.objReniec.names + " " + this.objReniec.lastName + " " + this.objReniec.motherLastName;
                //            if(completeName.value.toUpperCase() == completeNameReniec.toUpperCase()){
                //                cont ++;
                //            }
                /*if(birthDate == this.objReniec.birthdate){
                cont ++;
                }*/
                //            if(fecExp == this.objReniec.expeditionDate){
                //                cont ++;
                //                console.log("CUMPLE CON FEC EXP");
                //            }
                //            if(cont >= 5){
                //            console.log("CRYD: RENIEC_VALID " + this.reniecValidAttempts);
                //            console.log("CRYD: RENIEC_ERRORS " + this.reniecMaxErrors);

                //glazaror se adiciona validacion de parentesco
                //if (this.order.user.group == 'IN') {

                if (parentescoElement.value.length !== 0) {
                    parentescoElement = parentescoElement[parentescoElement.selectedIndex].text;
                    var divParentesco = document.getElementById("divParentesco");
                    //if (divParentesco.innerHTML == "MADRE") {
                    if (parentescoElement == this.objReniec.motherName) {
                        cont++;

                    }/* else {
							//glazaror... si es que se elige incorrectamente el nombre de la madre entonces se muestra una lista de alternativas de nombres de padre
							divParentesco.innerHTML = "PADRE";
							this.obtenerListaParentesco(false);//glazaror carga de parentesco
						}*/
                    //}
					/* else if (divParentesco.innerHTML == "PADRE" && parentescoElement == this.objReniec.fatherName) {
						cont ++;
					}*/
                }
                //} else {
                //cont ++;
                //}



                //alert("order.user.group: " + this.order.user.group);

                //glazaror se incrementa el numero de validaciones a 5
                if (cont >= 1 - this.reniecMaxErrors) {

                    $('#myModalReniec').modal('hide');
                    this.continueSale();

                } else {
                    this.mensajeValidacion = "Cliente no cumple con la validación. Reintentar";
                    this.validAttempts++;
                    if (this.validAttempts + 1 == this.reniecValidAttempts) {
                        this.mensajeValidacion = "No cumple con la validación, esta a punto de cerrar venta por validación. Reintentar";
                    }
                    if (this.validAttempts >= this.reniecValidAttempts) {
                        //this.mensajeValidacion = "Venta cancelada.";
                        //                    $('#myModalReniec').modal('hide');
                        //                    $('#modalHome').modal('show');
                        $('#modalHome').modal({ backdrop: 'static', keyboard: false, show: true });
                        //                    this.cancelSale();
                    }
                }
                // console.log("Cumple con "+cont + " de 5");

            }
        }
    }
    ok() {
        $('#myModalReniec').modal('hide');
    }

    // Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"... deberia ser una sola funcion pasandole como parametro el nombre del modal
    cerrarDialogo(nombreModal) {
        $("#" + nombreModal).modal('hide');
    }

    continueSale() {
        let fecNac: String;
        //let fecExp : String;

       // console.log("this.objReniec " + JSON.stringify(this.objReniec))

        if (this.customer.documentType == "DNI") {
            this.customerReniec = this.customer;
            this.customerReniec.firstName = this.objReniec.names;
            this.customerReniec.lastName1 = this.objReniec.lastName;
            this.customerReniec.lastName2 = this.objReniec.motherLastName;

            fecNac = this.objReniec.birthdate;
            this.objReniec.birthdate = fecNac.substring(0, 4) + "-" + fecNac.substring(4, 6) + "-" + fecNac.substring(6, 8);
            this.customerReniec.birthDate = this.objReniec.birthdate;

            //        fecExp=this.objReniec.expeditionDate;
            //        this.objReniec.expeditionDate = fecExp.substring(0, 4) + "-"+ fecExp.substring(4, 6) + "-"  +fecExp.substring(6, 8);
            //        this.customerReniec.expDate=this.objReniec.expeditionDate;

            this.customerReniec.birthDep = this.objReniec.departmentBirthLocation;
            this.customerReniec.birthCity = this.objReniec.provinceBirthLocation;
            this.customerReniec.birthDist = this.objReniec.districtBirthLocation;
            this.order.customer = this.customerReniec;

        }


        //verificamos si se trata de un nuevo cliente
        /*if (this.order.customer.mobilePhone == "") {
            this.order.showClientData = true;
        }*/
        // Sprint 3 - Solicitaron que siempre se pida registrar los datos del cliente
        this.order.showClientData = true;

        // Sprint 4 - obtenemos los datos del cliente...pueden estar actualizados los datos celular, telefono, correo
        //var customer = this.ls.getData().customer;
        // Sprint 4 - seteamos los datos del cliente
        //this.order.customer = customer;

        this.ls.setData(this.order);
        this.order.status = 'T';
        //this.router.navigate(['saleProcess/contract']); //glazaror prueba... primero registrar al cliente

        /*if (this.order.customer.mobilePhone == "") {
            this.router.navigate(['saleProcess']); //glazaror prueba... primero registrar al cliente
        } else {
            this.router.navigate(['saleProcess/contract']); //glazaror prueba... en caso de ser cliente mostrar contrato
        }*/
        // Sprint 3 - Solicitaron que siempre se pida registrar los datos del cliente
        //this.router.navigate(['saleProcess/contract']);

        // Sprint 6 - Ahora se debe cargar la pantalla de condiciones de venta
        //this.router.navigate(['saleProcess/salecondition']);
        if(this.canalEntidad.element == 'OUT'){
            this.router.navigate(['resumenOut']);
        }
        else{
        this.router.navigate(['/saleProcess/salesSummary']);
    }
    }



    validateForm(motherName: string) {
        if (motherName) {
            motherName = motherName.substring(3, motherName.length)
            let motherNameFromReniec = this.objReniec.motherName
            this.contValidation++
            $('#pop_reniec').modal('hide')
            this.modalHome = false
            //this.continueSale()
            if (this.isCheckedNameUser &&
                this.isCheckedPlaceOfBirth &&
                this.isCheckedDayOfBirth) {
                if (motherName == motherNameFromReniec) {
                    this.isValidationCompleted = true
                    this.isItOkMothersName = true
                    this.changeStyles(true)
					
					//
					
                } else {
                    this.changeStyles(false)
                    this.isValidationCompleted = false
                    this.isItOkMothersName = false
                    if (motherName != motherNameFromReniec) {
                        this.listErrorMessage.push("Aún no ha validado el nombre de la madre del cliente.")
                        this.isValidationCompleted = false
                        if (this.contValidation >= 3) {
                            this.modalHome = true
                            $('#pop_reniec').modal('show')
                        }
                    }
                }
            } else {
                if (this.isCheckedNameUser == false) {
                    this.listErrorMessage.push("Falta validar el nombre del cliente.")
                    this.isValidationCompleted= false
                }
                if (this.isCheckedPlaceOfBirth == false) {
                    this.listErrorMessage.push("Falta validar el lugar de nacimiento.")
                    this.isValidationCompleted= false
                }
                if (this.isCheckedDayOfBirth == false) {
                    this.listErrorMessage.push("Falta validar la fecha de nacimiento.")
                    this.isValidationCompleted= false
                }
                if (motherName == motherNameFromReniec) {
                    this.isItOkMothersName = true
                    this.changeStyles(true)
                } else {
                    this.changeStyles(false)
                    this.isValidationCompleted = false
                    this.isItOkMothersName = false
                    if (motherName != motherNameFromReniec) {
                        this.listErrorMessage.push("Aún no ha validado el nombre de la madre del cliente.")
                        this.isValidationCompleted= false
                        if (this.contValidation >= 3) {
                            this.modalHome = true
                            $('#pop_reniec').modal('show')
                        }
                    }
                }
                setTimeout(() => {
                    this.listErrorMessage = []
                }, 4000)
            }
        } else {
            if (this.isItOkMothersName) {
                this.contValidation++
                $('#pop_reniec').modal('hide')
                this.modalHome = false
                //this.continueSale()
                if (this.isCheckedNameUser &&
                    this.isCheckedPlaceOfBirth &&
                    this.isCheckedDayOfBirth) {
                    this.isValidationCompleted = true
                    this.isItOkMothersName = true
                    this.changeStyles(true)
                } else {
                    if (this.isCheckedNameUser == false) {
                        this.listErrorMessage.push("Falta validar el nombre del cliente.")
                        this.isValidationCompleted = false
                    }
                    if (this.isCheckedPlaceOfBirth == false) {
                        this.listErrorMessage.push("Falta validar el lugar de nacimiento.")
                        this.isValidationCompleted = false
                    }
                    if (this.isCheckedDayOfBirth == false) {
                        this.listErrorMessage.push("Falta validar la fecha de nacimiento.")
                        this.isValidationCompleted = false
                    }
                    setTimeout(() => {
                        this.listErrorMessage = []
                    }, 4000)
                }
            }
        }
    }

    nextPage() {
		this.continueSale();
        //this.router.navigate(['/saleProcess/salesSummary']);
    }


    changeStyles(valid: boolean) {
        if (valid) {
            $("#selecttr").css("background-color", "#dff0d8")
            $("#selecttr").find("option").css("color", "black")
            $("#selecttr").find("td").css("color", "black")
            $("#selecttr").find("select").css("color", "black")
            $("#selecttr").find("label").css("color", "#3399FF")
        } else {
            $("#selecttr").css("background-color", "#ff7e7e")
            $("#selecttr").find("td").css("color", "white")
            $("#selecttr").find("select").css("color", "white")
            $("#selecttr").find("option").css("color", "black")
            $("#selecttr").find("label").css("color", "white")


        }
    }
	
	cancelarConsulta() {
		this.loading = false;
        this.loadError = false;
        this.mensaje_error = "";
	}
	
	capturarDatosCliente() {
        if(this.order.customer.documentType == 'RUC' && this.order.customer.documentNumber.substr(0,2) == '20'){
            if(this.apellidoPaterno !== null && this.apellidoPaterno.trim() !== ''){
                this.order.customer.nombreCompletoRrll = this.apellidoPaterno + ' ';
            }
            if(this.apellidoMaterno !== null && this.apellidoMaterno.trim() !== ''){
                this.order.customer.nombreCompletoRrll += this.apellidoMaterno + ' ';
            }
            if(this.nombres !== null && this.nombres.trim() !== ''){
                this.order.customer.nombreCompletoRrll += this.nombres;
            }
            this.order.customer.nationality = this.nacionalidad;
            this.order.customer.birthDate = this.fechaNacimiento;
            //console.log('RRLL Capturado --> ' + this.order.customer.nombreCompletoRrll); 
            if(this.order.customer.nombreCompletoRrll.trim() == ''){
                alert('Representante Legal Sin Data Ingresada');
                return;
            }
        }
        else{
		this.order.customer.firstName = this.nombres;
		this.order.customer.lastName1 = this.apellidoPaterno;
		this.order.customer.lastName2 = this.apellidoMaterno;
		this.order.customer.nationality = this.nacionalidad;
		this.order.customer.birthDate = this.fechaNacimiento;
        }

        this.ls.setData(this.order);
       // console.log('Canal -' + this.canalEntidad.element);
        if((this.order.user.typeFlujoTipificacion=='REMOTO') && (this.order.customer.documentType== "DNI" || this.order.customer.documentType == "RUC") && this.order.type == globals.ORDER_ALTA_NUEVA ){
			  this.router.navigate(['direccion']);
        }else if(this.order.user.typeFlujoTipificacion=='REMOTO OUT'){
            this.router.navigate(['resumenOut']);
        }
        else{
	        this.router.navigate(['/saleProcess/salesSummary']);
        }
        
	}
}
