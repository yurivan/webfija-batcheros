import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DireccionService } from '../services/direccion.service';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order';
import { Parameters } from '../model/parameters';
import { Customer } from '../model/customer';
import { LocalStorageService } from '../services/ls.service';
import * as globals from '../services/globals';
var $ = require('jquery');
import { ProgressBarService } from '../services/progressbar.service';
(<any>window).jQuery = $

//import $ = require('jquery');

import 'date-input-polyfill';
import { Argumentarios } from '../model/argumentarios';

@Component({
    moduleId: 'ruc',
    selector: 'ruc',
    templateUrl: 'searchRuc.template.html',
    providers: [DireccionService, CustomerService, OrderService]
})
export class SearchRucComponent implements OnInit {

    direccionService: DireccionService;
    customerService: CustomerService;

    objDepart: any;
    private keyDeparts: string[];
    private departSeleccionado: any;
    selectDepart: any;

    mjsReniec: string;
    argumentarios: Argumentarios[];

    objProvs: any;
    showProvs = new Array();
    private keyProvs: string[];
    selectProv: any;
    private proviSeleccionado: any;

    objDists: any;
    showDists = new Array();
    selectDistric: any;
    private keyDistrs: string[];
    private districSeleccionado: any;
    months = new Array()
    listErrorMessage = new Array()

    objRuc: any;
    distModel: any;
    provModel: any;
    order: Order;
    canalEntidad: Parameters;
    customer: Customer;
    customerRuc: Customer;
    mensajeValidacion: String;
    channelCall: boolean;

    private loadError: boolean;
    private loading: boolean;
    private mensaje_error: String;
    cargaDept: boolean;

    private validAttempts: number;
    private reniecValidAttempts: number;
    private reniecMaxErrors: number;
    webOrder: any;

    //glazaror inicio variables para parentesco
    objParentesco: any;
    private keyParentescoList: string[];
    private parentescoSeleccionado: any;
    selectParentesco: any;

    parentescoModel: any;
    //glazaror fin variables para parentesco

    mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios
    isCheckedNameUser: boolean
    isCheckedPlaceOfBirth: boolean
    isCheckedDayOfBirth: boolean
    isItOkMothersName: boolean
    mothersName: boolean
    isValidationCompleted: boolean

    objfecnac: any;
    contValidation: number
    modalHome: boolean

    showDni: boolean = true;
    showCarneExtranjeria: boolean = false;
    showOtrosCE: boolean = false;
    showPasaporte : boolean = false;
	docNroDni: string;
    docNroCarnetExt: string;
    docNroOtrosCE: string;
    selectedType: any;
    documentNumber: any;
    mostrarRepresentanteLegal : boolean = true;
    tituloSeleccionDoc : string;
    
    //Sprint 25 Flujo Offline
    activaFlujoOff : boolean;
    nomRazSoc : string;
    
    constructor(private router: Router,
        direccionService: DireccionService,
        customerService: CustomerService,
        private ls: LocalStorageService,
        private orderService: OrderService,
        private progresBar: ProgressBarService) {

        this.contValidation = 0
        this.router = router;
        this.direccionService = direccionService;
        this.customerService = customerService;
        this.ls = ls;
        this.cargaDept = false;
        this.distModel = 0;
        this.provModel = 0;
        this.parentescoModel = 0; //glazaror
        this.loadError = false;
        this.mensaje_error = "";
        this.validAttempts = 0;
        this.reniecValidAttempts = globals.RENIEC_VALID_ATTEMPTS;
        this.reniecMaxErrors = globals.RENIEC_MAX_ERRORS;

        this.orderService = orderService;
        this.objRuc = {}
        this.mensajeValidacionDatosCliente = "";// Sprint 6
        this.isCheckedNameUser = true
        this.isCheckedPlaceOfBirth = true
        this.isCheckedDayOfBirth = true
        this.isValidationCompleted = false
        this.isItOkMothersName = false
        this.modalHome = false
        this.months = [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"]
    }

    ngOnInit() {
        // $(window).ready(() => {
        //     $("#modalHome").modal('hide')
        // })
        $(document).ready(() => {
            $(".paraselect select").val("");
            $(".paraselect select").focusout(function () {
                if ($(this).val() != "") {
                    $(this).addClass("has-content");
                } else {
                    $(this).removeClass("has-content");
                }
            })

        })
        this.progresBar.getProgressStatus()
        // add loading 
        //this.loading = true;

        //this.obtenerDepartamentos();
        
        this.docNroDni = '';
        this.docNroCarnetExt = '';
        this.docNroOtrosCE = '';

        this.order = JSON.parse(localStorage.getItem('order'))
        //console.log('Data RUC -->' + JSON.stringify(this.order.customer));
        this.canalEntidad = JSON.parse(localStorage.getItem('canalEntidad'))
        this.customer = this.order.customer

        let resultDoc = this.order.customer.documentNumber
        var valorCondicional = resultDoc.substring(0, 2)
        this.selectedType = 'DNI';

        this.activaFlujoOff = this.order.offline.flag.indexOf('1') !== -1 ? true : false;

        if(valorCondicional !== '20'){
            this.tituloSeleccionDoc = 'VALIDACIÓN DE IDENTIDAD';
        }
        else{
            this.tituloSeleccionDoc =  'VALIDACIÓN DE REPRESENTANTE LEGAL';
        }

        this.argumentarios = JSON.parse(localStorage.getItem('argumentarios'));

        /*for (let i in this.argumentarios) {
            let obj = new Argumentarios;
            obj = this.argumentarios[i];
            if (obj.pantalla.toUpperCase() == "reniec".toUpperCase()) {
                this.mjsReniec = obj.mensaje;
            }
        }*/

        this.channelCall = true;
        if (this.customer) {
            //this.obtainRuc();


        } else {
            this.continueSale();
        }

    }

    obtenerDepartamentos() {

        this.objDepart = this.direccionService.getDataDepartamentoReniec();

        this.keyDeparts = Object.keys(this.objDepart);
    }

    obtainRuc() {

        this.loading = true;
        //console.log("obtain RUC " + JSON.stringify(this.customer))
        this.customerService.getdataCustomerRuc(this.customer.documentNumber)
            .subscribe(
                data => {
                    if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
                        // add data perfil tienda
                        this.objRuc = data.responseData;
                        this.isValidationCompleted = true;
                        this.selectedType = 'DNI';
                        //console.log("objeto Ruc " + JSON.stringify(this.objRuc))
                    } else {
                        this.mostrarRepresentanteLegal = false;
                        this.mostrarError();
                    }
                },
                err => {
                    this.mostrarRepresentanteLegal = false;
                    console.log(err);
                    this.mostrarError();
                },
                () => {

                    if (this.objRuc != null) {
                        this.loading = false;
                        this.loadError = false;
                    } else {
                        this.mostrarError();
                    }
                });
    }

    onChangeSelectDocument(value) {

        if (value == "DNI") {
			this.docNroCarnetExt = '';
			this.docNroOtrosCE = '';
		} else if (value == "CEX") {
			this.docNroDni = '';
			this.docNroOtrosCE = ''
		} else {
			this.docNroCarnetExt = ''
			this.docNroDni = ''
		}
		switch (value) {

			case "DNI":

				this.showDni = true;
				this.showCarneExtranjeria = false;
				this.showPasaporte = false;
				this.showOtrosCE = false;
				break;

			case "CEX":

				this.showCarneExtranjeria = true;
				this.showDni = false;

				this.showPasaporte = false;
				this.showOtrosCE = false;

				break;

			case "Otros":

				this.showOtrosCE = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showPasaporte = false;
				break;

			case "PAS":

				this.showPasaporte = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showOtrosCE = false;

                break;

            default:
				this.showDni = true;
				this.showCarneExtranjeria = false;
				this.showPasaporte = false;
				this.showOtrosCE = false;
				this.selectedType = "DNI";
				break;
		}
		//this.aplicarEfectosChange();
	}

    cancelSale() {
        this.webOrder = {};
        this.order = this.ls.getData();
        //console.log("inicio",this.order);
        this.webOrder.userId = this.order.user.userId;
        this.webOrder.customer = this.order.customer;
        if (this.order.product != null && this.order.type != globals.ORDER_ALTA_NUEVA) {
            this.webOrder.phone = this.order.product.phone;
            this.webOrder.department = this.order.product.department;
            this.webOrder.province = this.order.product.province;
            this.webOrder.district = this.order.product.district;
            this.webOrder.address = this.order.product.address;
            this.webOrder.latitudeInstallation = this.order.product.coordinateY ? this.order.product.coordinateY : 0;
            this.webOrder.longitudeInstallation = this.order.product.coordinateX ? this.order.product.coordinateX : 0;
            this.webOrder.addressAdditionalInfo = this.order.product.additionalAddress ? this.order.product.additionalAddress : null;
            //console.log("product != null", this.order);
        }
        if (this.order.selectedOffering != null) {
            this.webOrder.product = this.order.selectedOffering;
        }
        this.webOrder.cancelDescription = "Validacion Reniec";
        this.webOrder.sva = [];
        this.webOrder.type = this.order.type;
        this.webOrder.disaffiliationWhitePagesGuide = this.order.disaffiliationWhitePagesGuide;
        this.webOrder.affiliationElectronicInvoice = this.order.affiliationElectronicInvoice;
        this.webOrder.affiliationDataProtection = this.order.affiliationDataProtection;
        this.webOrder.publicationWhitePagesGuide = this.order.publicationWhitePagesGuide;
        this.webOrder.shippingContractsForEmail = this.order.shippingContractsForEmail;

        if (this.order.sva != null) {
            for (let i = 0; i < this.order.sva.length; i++) {
                let sva = this.order.sva[i];
                this.webOrder.sva.push(sva.id);
            }
        }
        $('#modalHome').modal('hide');
        this.modalHome = false
        this.loading = true;
        this.orderService.cancelSale(this.webOrder).subscribe(
            data => {
                //console.log(data);
                if (data.responseCode == "00") {
                    //this.errorSending = false;
                    this.router.navigate(['/acciones']);
                    //this.closeModal();
                } else {
                    //this.errorSending = true;
                }
                this.loading = false;
            },
            err => {
                this.loading = false;
                console.log(err);
            }
        );
    }

    mostrarError() {
        this.loading = false;
        this.loadError = true;
        this.mensaje_error = "El servicio Ruc dio un error, Por favor vuelva a intentarlo."
    }
    ok() {
        $('#myModalReniec').modal('hide');
    }

    // Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"... deberia ser una sola funcion pasandole como parametro el nombre del modal
    cerrarDialogo(nombreModal) {
        $("#" + nombreModal).modal('hide');
    }

    continueSale() {
        let fecNac: String;

        let resultDoc = this.order.customer.documentNumber
        var valorCondicional = resultDoc.substring(0, 2)

        this.customerRuc = this.customer;
        //this.customerRuc.razonSocial = this.objRuc.razonSocial;
        //this.customerRuc.estContrib = this.objRuc.estContrib;
        //this.customerRuc.condDomicilio = this.objRuc.condDomicilio;
        //this.customerRuc.direccion = this.objRuc.direccion;

        this.documentNumber = this.docNroDni || this.docNroCarnetExt || this.docNroOtrosCE;
        this.order.showClientData = true;
        
        if(valorCondicional == '10' && this.canalEntidad.element !== 'OUT' ) {
            this.ls.setData(this.order);
            this.order.status = 'T';
            this.router.navigate(['/saleProcess/reniec']);
        }
        else if(valorCondicional == '10' && this.canalEntidad.element == 'OUT' ) {
            this.ls.setData(this.order);
            this.order.status = 'T';
            this.router.navigate(['validacionOut']);
        }
        else if(valorCondicional == '20' || valorCondicional == '15' || valorCondicional == '17'){
            if(this.order.offline.flag.indexOf('1') !== -1 && this.nomRazSoc == '' && valorCondicional == '20'){
                alert('Debe Ingresar una Razon Social valida.');
            }
            else if(this.order.offline.flag.indexOf('1') !== -1 && this.nomRazSoc !== '' && valorCondicional == '20'){
                this.customerRuc.firstName = this.nomRazSoc;
            }
            if (this.selectedType == "DNI" && this.documentNumber) {
                this.customerRuc.tipoDocumentoRrll = this.selectedType;
                this.customerRuc.numeroDocumentoRrll = this.documentNumber;
                this.order.customer = this.customerRuc;
    
                this.ls.setData(this.order);
                this.order.status = 'T';
                if(this.canalEntidad.element == 'OUT'){
                    //console.log('Manda a OUT');
                    this.router.navigate(['validacionOut']);
                }
                else{
                    this.router.navigate(['/saleProcess/reniec']);
                }
            }
            else if(this.selectedType !== "DNI" && this.documentNumber){
                this.customerRuc.tipoDocumentoRrll = this.selectedType;
                this.customerRuc.numeroDocumentoRrll = this.documentNumber;
    
                this.ls.setData(this.order);
                this.order.status = 'T';
                this.order.customer = this.customerRuc;
                this.router.navigate(['/saleProcess/datoscliente']);
            }
            else{
                    alert('Debe Ingresar un Numero de Documento Valido.');
                }
            
        }
       
    }

    validarRuc() {
        this.continueSale();
        //this.router.navigate(['/saleProcess/salesSummary']);
    }


    changeStyles(valid: boolean) {
        if (valid) {
            $("#selecttr").css("background-color", "#dff0d8")
            $("#selecttr").find("option").css("color", "black")
            $("#selecttr").find("td").css("color", "black")
            $("#selecttr").find("select").css("color", "black")
            $("#selecttr").find("label").css("color", "#3399FF")
        } else {
            $("#selecttr").css("background-color", "#ff7e7e")
            $("#selecttr").find("td").css("color", "white")
            $("#selecttr").find("select").css("color", "white")
            $("#selecttr").find("option").css("color", "black")
            $("#selecttr").find("label").css("color", "white")


        }
    }

    aplicarEfectosChange() {

		$("#tablaDat").addClass("none");

		$(".input-effect input").val("");

		$(".input-effect input").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
		$(".paraselect select").val("");
		$(".paraselect select").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
	}

	focusout(target) {
		this.focusoutById(target.id);
		/*var target = $("#" + target.id);
		if (target.val() != "") {
			target.addClass("has-content");
		} else {
			target.removeClass("has-content");
		}*/
	}

	focusoutById(id) {
		var target = $("#" + id);
		if (target.val() != "") {
			target.addClass("has-content");
		} else {
			target.removeClass("has-content");
		}
	}

    cancelarConsulta() {
        this.loading = false;
        this.loadError = false;
        this.mensaje_error = "";
    }
}
