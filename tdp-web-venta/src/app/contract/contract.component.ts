import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';
import { ProgressBarService } from '../services/progressbar.service';
var $ = require('jquery');
// AGENDAMIENTO
import { Agendamiento } from '../model/agendamientos';
import { WhatsApp } from '../model/whatsapp';
import { Argumentarios } from '../model/argumentarios';
// FIN 

@Component({
	moduleId: 'contract',
	selector: 'contract',
	templateUrl: './contract.template.html',
	providers: [CustomerService, OrderService]
})

export class ContractComponent implements OnInit {
	@ViewChild('fileInput') inputEl: ElementRef;
	contract = [];
	order: Order;
	closeSaleError: boolean;
	closeSaleErrorMessage: string;
	private loading: boolean;
	disaffiliationWhitePagesGuide = true;
	affiliationElectronicInvoice = true;
	affiliationDataProtection = true;
	publicationWhitePagesGuide = true;
	shippingContractsForEmail = true;
	cipError: boolean = false;
	isWsp: boolean;

	// AGENDAMIENTO
	dataAgendamiento: Agendamiento
	departamento: string
	canal: string
	tecnologia: string
	departamentoValidacion: boolean
	canalValidacion: boolean
	tecnologiaValidacion: boolean
	// FIN 

	argumentarios : Argumentarios[];
	mjsContract : string;
	//WHATSAPP
	whatsapp: WhatsApp;
	channels: string[];
	codewsp: number;

	//Sprint 26 Offline
	errorPagoEfectivo : boolean = false;
	emailSeller : string;
	emailPattern : string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"; 

	constructor(private router: Router,
		private customerService: CustomerService,
		private orderService: OrderService,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService) {
		this.router = router;
		this.customerService = customerService;
		this.closeSaleError = false;
		this.closeSaleErrorMessage = '';
		// AGENDAMIENTO
		this.departamentoValidacion = false
		this.canalValidacion = false
		this.tecnologiaValidacion = false
		// FIN AGENDAMIENTO
	}

	ngOnInit() {

		this.order = JSON.parse(localStorage.getItem('order'));
		//console.log(this.order);

		this.argumentarios = JSON.parse(localStorage.getItem('argumentarios'));		
		
		for (let i in this.argumentarios) {
			let obj = new Argumentarios;
			obj = this.argumentarios[i];
			if(obj.pantalla.toUpperCase() == "contract".toUpperCase()){
				//console.log("Entidad Argumntario : "+ obj.entidad);
				//console.log("Entidad Perfil : "+ this.order.user.entity);
				//console.log("Mensaje Argumntario : "+ obj.mensaje);
				if(obj.canal.toUpperCase() == "CALL CENTER"){
					this.mjsContract=obj.mensaje;
					break;
				}
			}
		}
		
		this.whatsapp = JSON.parse(localStorage.getItem('whatsApp'));
		this.isWsp = this.whatsapp.onOff;
		this.channels = this.whatsapp.canal.split(',');

		//console.log("Validacion Duplicado-Ingreso al Regresar al Contrato" + this.ls.getData().ventaEcha)
		// if (this.ls.getData().ventaEcha) {

		// 	alert("La venta esta Realizada no puede retroceder.")

		// 	// if (this.order.flujoAgendamiento == true) {
		// 	// 	this.router.navigate(['agendamiento']);
		// 	// } else {
		// 		// this.router.navigate(['contactoinstalacion']);
		// 	   this.router.navigate(['saleProcess/close']);
		// 	// }

		// }
		this.progresBar.getProgressStatus()
		this.loading = true;



		//if( this.order.user.typeFlujoTipificacion == "PRESENCIAL") {
		//	this.sendTiendaVenta();
		//} else {
		this.customerService.getContract(this.order)
			.subscribe(
				data => {
					if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
						this.contract = data.responseData.contractWeb
						let count = 0;
						for (let i = 0; i < this.contract.length; i++) {
							if (this.contract[i].category == 'client_has_email') {
								count++;
							}
						}

						for (let i = 0; i < this.contract.length; i++) {
							if (this.contract[i].category == 'client_has_email') {
								count--;
								this.contract[i].category = this.contract[i].category + count;
							}
						}


						//localStorage.setItem('contract', JSON.stringify(Object.assign(this.contract)))
						//console.log(data.responseData.sva);
						//this.loading = false;
					} else {
						alert(data.responseMessage);
					}
				},
				err => {
					console.log(err)
				}
			);
		//}
		//	let webOrder = {
		//        userId: this.order.user.userId,
		//        phone: this.order.product.phone,
		//		department: this.order.product.department,
		//        province: this.order.product.province,
		//        district: this.order.product.district,
		//        address: this.order.product.address,
		//        latitudeInstallation: this.order.product.coordinateX ? this.order.product.coordinateX : 0,
		//        longitudeInstallation: this.order.product.coordinateY ? this.order.product.coordinateY : 0,
		//        addressAdditionalInfo: this.order.product.additionalAddress ? '' : this.order.product.additionalAddress,
		//        sva: [],
		//        customer: this.order.customer,
		//        product: this.order.selectedOffering,
		//        type: this.order.type,
		//        disaffiliationWhitePagesGuide: this.order.disaffiliationWhitePagesGuide,
		//        affiliationElectronicInvoice: this.order.affiliationElectronicInvoice,
		//        affiliationDataProtection: this.order.affiliationDataProtection,
		//        publicationWhitePagesGuide: this.order.publicationWhitePagesGuide,
		//        shippingContractsForEmail: this.order.shippingContractsForEmail
		//      };
		//		for (let i = 0; i < this.order.sva.length; i++) {
		//			let sva = this.order.sva[i];
		//			webOrder.sva.push(sva.id);
		//		}
		if (typeof this.order.disaffiliationWhitePagesGuide !== 'undefined') {
			this.disaffiliationWhitePagesGuide = this.order.disaffiliationWhitePagesGuide;
		}
		if (typeof this.order.affiliationElectronicInvoice !== 'undefined') {
			this.affiliationElectronicInvoice = this.order.affiliationElectronicInvoice;
		}
		if (typeof this.order.affiliationDataProtection !== 'undefined') {
			this.affiliationDataProtection = this.order.affiliationDataProtection;

		}
		if (typeof this.order.publicationWhitePagesGuide !== 'undefined') {
			this.publicationWhitePagesGuide = this.order.publicationWhitePagesGuide;
		}
		if (typeof this.order.shippingContractsForEmail !== 'undefined') {
			this.shippingContractsForEmail = this.order.shippingContractsForEmail;
		}

		this.getCIP();
	}
	getCIP() {

		if(this.order.offline.flag.indexOf(',7') != -1){
			this.order.offline.flag = this.order.offline.flag.replace(',7','');
		}
		else if(this.order.offline.flag.indexOf('7') != -1){
			this.order.offline.flag = this.order.offline.flag.replace('7','')
		}
		// Sprint 7... si es que la venta ya ha sido registrada anteriormente entonces no volver a generar otro id
		/*if (this.order.id != null && this.order.id != '') {
			this.order.status = "F";
			this.ls.setData(this.order);
			this.closeSaleError = false;
			this.loading = false;
			return;
		}*/

		let webOrder = this.createWebOrder()
		//console.log(webOrder);
		
		for (let i = 0; i < this.order.sva.length; i++) {
			let sva = this.order.sva[i];
			webOrder.sva.push(sva.id);
		}
		if (this.tieneNumeros(webOrder.department)) {
			let strDepartment = webOrder.department;
			webOrder.department = strDepartment.substring(4);
		}
		if (this.tieneNumeros(webOrder.province)) {
			let strProvince = webOrder.province;
			webOrder.province = strProvince.substring(4);
		}
		if (this.tieneNumeros(webOrder.district)) {
			let strDistrinct = webOrder.district;
			webOrder.district = strDistrinct.substring(4);
		}
		this.orderService.cerrarVenta(webOrder).subscribe(
			data => {
				if (data.responseCode == '00') {
					this.order.id = data.responseData.orderId;
					this.order.cip = data.responseData.cip;
					this.loading = true;
					this.order.status = "F"

					//verificamos si es que el pago es al contado y no se genero cip
					if (this.order.scoringDataModel && this.order.scoringDataModel.responseData.result) {
						var pagoAdelantado;
						var arreglo = this.order.scoringDataModel.responseData.result[0].detalle;
						for (let x in arreglo){
							if(arreglo[x]["codProd"] == this.order.selectedOffering.productCategoryCode ){
								pagoAdelantado = arreglo[x]["pagoAdelantado"];
							}
						}
						if (pagoAdelantado > 0 && this.order.cip == "") {
							this.cipError = true;
						} else {
							this.cipError = false;
						}
					}

					if(data.responseData.cip == 'ERROR_CIP'){
						this.errorPagoEfectivo = true;
						this.order.offline.flag += this.order.offline.flag == '' ? '7' : ',7';
					}


					this.ls.setData(this.order);
					this.closeSaleError = false;
					//sprint 3 - ya no se subiran audios en el flujo de venta
					//this.orderService.uploadAudio(this.order.id, file);
					//this.router.navigate(['saleProcess/close']);
				} else {
					this.closeSaleError = true;
					this.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				this.loading = false;
				this.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				this.loading = false;
			}
		);
	}
	/*sprint 3 - esta funcion ya no sera utilizada... sera reemplazada por cerrarVenta*/
	uploadAudio() {
		let audioElement = this.inputEl.nativeElement;
		if (audioElement.files.length < 1) {
			alert('seleccione el audio a cargar...');
			return;
		}
		let file = audioElement.files[0];
		let fileName = audioElement.value;
		if (!fileName.toLowerCase().endsWith('.gsm')) {
			alert('el archivo seleccionado debe ser del formato GSM.');
			return;
		}
		let webOrder = this.createWebOrder()
		for (let i = 0; i < this.order.sva.length; i++) {
			let sva = this.order.sva[i];
			webOrder.sva.push(sva.id);
		}
		this.loading = true;

		if (this.tieneNumeros(webOrder.department)) {
			let strDepartment = webOrder.department;
			webOrder.department = strDepartment.substring(4);
		}

		if (this.tieneNumeros(webOrder.province)) {
			let strProvince = webOrder.province;
			webOrder.province = strProvince.substring(4);
		}

		if (this.tieneNumeros(webOrder.district)) {
			let strDistrinct = webOrder.district;
			webOrder.district = strDistrinct.substring(4);
		}

		this.orderService.cerrarVenta(webOrder).subscribe(
			data => {
				if (data.responseCode == '00') {
					this.order.id = data.responseData.orderId;
					this.order.cip = data.responseData.cip;
					this.order.status = "F"
					if(data.responseData.cip == 'ERROR_CIP'){
						this.errorPagoEfectivo = true;
						this.order.offline.flag += this.order.offline.flag == '' ? '7' : ',7';
					}
					this.ls.setData(this.order);
					this.closeSaleError = false;
					this.orderService.uploadAudio(this.order.id, file);
					// this.router.navigate(['saleProcess/close']);
					this.router.navigate(['contactoinstalacion']);
				} else {
					this.closeSaleError = true;
					this.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				this.loading = false;
				this.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				this.loading = false;
			}
		);
	}

	/*sprint 3 - esta funcion reemplaza a la funcion uploadAudio*/
	/*sprint 3 : Tenemos que validar la carga de los datos(al comienzo o al final())*/
	cerrarVenta() {
		/*let audioElement = this.inputEl.nativeElement;
		if (audioElement.files.length < 1) {
			alert('seleccione el audio a cargar...');
			return;
		}
		let file = audioElement.files[0];
		let fileName = audioElement.value;
		if (!fileName.toLowerCase().endsWith('.gsm')) {
			alert('el archivo seleccionado debe ser del formato GSM.');
			return;
		}*/
        /*this.order.disaffiliationWhitePagesGuide = this.disaffiliationWhitePagesGuide;
        this.order.affiliationElectronicInvoice = this.affiliationElectronicInvoice;
        this.order.affiliationDataProtection = this.affiliationDataProtection;
        this.order.publicationWhitePagesGuide = this.publicationWhitePagesGuide;
        this.order.shippingContractsForEmail = this.shippingContractsForEmail;

		let webOrder = {
        userId: this.order.user.userId,
        phone: this.order.product.phone,
		   department: this.order.product.department,
        province: this.order.product.province,
        district: this.order.product.district,
        address: this.order.product.address,
        latitudeInstallation: this.order.product.coordinateX ? this.order.product.coordinateX : 0,
        longitudeInstallation: this.order.product.coordinateY ? this.order.product.coordinateY : 0,
        addressAdditionalInfo: this.order.product.additionalAddress ? this.order.product.additionalAddress : '',
        sva: [],
        customer: this.order.customer,
        product: this.order.selectedOffering,
        type: this.order.type,
        disaffiliationWhitePagesGuide: this.order.disaffiliationWhitePagesGuide,
        affiliationElectronicInvoice: this.order.affiliationElectronicInvoice,
        affiliationDataProtection: this.order.affiliationDataProtection,
        publicationWhitePagesGuide: this.order.publicationWhitePagesGuide,
        shippingContractsForEmail: this.order.shippingContractsForEmail,
        originOrder : this.order.user.groupPermission
      };
 
		for (let i = 0; i < this.order.sva.length; i++) {
			let sva = this.order.sva[i];
			webOrder.sva.push(sva.id);
		}
		this.loading = true;
		console.log('this webOrder', webOrder);
		
		if (this.tieneNumeros(webOrder.department)){
			let strDepartment = webOrder.department;
			webOrder.department = strDepartment.substring(4);
		}

		if (this.tieneNumeros(webOrder.province)){
			let strProvince = webOrder.province;
			webOrder.province = strProvince.substring(4);
		}

		if (this.tieneNumeros(webOrder.district)){
			let strDistrinct = webOrder.district;
			webOrder.district = strDistrinct.substring(4);
		}
		
		this.orderService.cerrarVenta(webOrder).subscribe(
			data => {
			console.log("Data in contract component" + data)	
				if (data.responseCode == '00') {
					this.order.id = data.responseData.orderId;
					this.order.cip = data.responseData.cip;
					this.order.status = "F"
					this.ls.setData(this.order);
					this.closeSaleError = false;

					//sprint 3 - ya no se subiran audios en el flujo de venta
					//this.orderService.uploadAudio(this.order.id, file);*/
		//this.router.navigate(['saleProcess/close']);
		/*		} else {
					this.closeSaleError = true;
					this.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				this.loading = false;
				this.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				this.loading = false;
			}
		);*/
		//Sprint 26 Offline
		if(this.order.offline.flag.indexOf('7') !== -1 && this.emailSeller == ""){
			alert('Estimado Vendedor debe ingresar su correo electronico para poder indicarle cuando se genere el CIP.');
			return;
		}
		else if(this.order.offline.flag.indexOf('7') !== -1){
			this.order.offline.body['emailvendedor'] = this.emailSeller;
			this.ls.setData(this.order);
			console.log(this.order);
		}
		for (let canal of this.channels) {
			if (canal == this.order.user.sellerChannelEquivalentCampaign) {
				if (this.isWsp) {
					this.codewsp = 1;
					break;
				} else {
					this.codewsp = 0;
					break;
				}
			} else {
				this.codewsp = 2;
			}
		}

		let _xthis = this;
		let webOrder = {
			orderId: _xthis.order.id,
			whatsapp: _xthis.codewsp,
			offline: _xthis.order.offline
		};

		_xthis.orderService.actualizarEstado(webOrder).subscribe(
			data => {
		//		console.log("Data in contract component" + data)
				if (data.responseCode == '00') {
					_xthis.closeSaleError = false;
					//sprint 3 - ya no se subiran audios en el flujo de venta
					//this.orderService.uploadAudio(this.order.id, file);*/
					//this.router.navigate(['saleProcess/close']);
					_xthis.order.ventaEcha = true;
				//	console.log("Validacion Duplicado-Ingreso a la Funcion CONTRATAR" + _xthis.order.ventaEcha)
					_xthis.ls.setData(_xthis.order);
					this.router.navigate(['contactoinstalacion']);
					// _xthis.validacionAgendamiento()
					// this.router.navigate(['saleProcess/close']);
				} else {
					_xthis.closeSaleError = true;
					_xthis.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				_xthis.loading = false;
				_xthis.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				_xthis.loading = false;
			}
		);
	}

	validacionAgendamiento() {
	//	console.log("Agendamiento validación para contrato")
		this.dataAgendamiento = JSON.parse(localStorage.getItem("dataAgendamiento"))
		this.departamento = this.dataAgendamiento.departamento
		this.canal = this.dataAgendamiento.canal
		this.tecnologia = this.dataAgendamiento.tecnologia


		// START CANAL
		let canal = this.canal
		if (canal == null) {
			canal = ""
		}
		let canalArray = canal.split(",");

		if (canalArray.length == 1 && "" == canalArray[0]) {
			canalArray = [];
		}

		for (let indiceCampaign in canalArray) {
			let value = canalArray[indiceCampaign].trim().toUpperCase();
			canalArray[indiceCampaign] = value;
		}

		for (let item in canalArray) {
			let canalValidar = this.order.user.channel.trim().toUpperCase();
			if (canalArray[item] === canalValidar) {
			//	console.log("Con Agendamiento Canal")
				this.canalValidacion = true
			}
		}

		// FIN CANAL

		// START TECNOLOGÍA
		let tecnologia = this.tecnologia
		if (tecnologia == null) {
			tecnologia = ""
		}
		let tecnologiaArray = tecnologia.split(",");

		if (tecnologiaArray.length == 1 && "" == tecnologiaArray[0]) {
			tecnologiaArray = [];
		}

		for (let indiceCampaign in tecnologiaArray) {
			let value = tecnologiaArray[indiceCampaign].trim().toUpperCase();
			tecnologiaArray[indiceCampaign] = value;
		}

		if (typeof this.order.selectedOffering.internetTech !== 'undefined' && this.order.selectedOffering.internetTech !== null) {

			for (let item in tecnologiaArray) {
				let tecnologiaValidar = this.order.selectedOffering.internetTech.trim().toUpperCase();
				// let tecnologiaValidar = ""
				if (tecnologiaArray[item] === tecnologiaValidar) {
				//	console.log("Con Agendamiento Tecnología")
					this.tecnologiaValidacion = true
				}
			}
		}


		// FIN TECNOLOGÍA


		// START DEPARTAMENTO
		let departamento = this.departamento
		if (departamento == null) {
			departamento = ""
		}

		let departamentoArray = departamento.split(",");

		if (departamentoArray.length == 1 && "" == departamentoArray[0]) {
			departamentoArray = [];
		}

		for (let indiceCampaign in departamentoArray) {
			let value = departamentoArray[indiceCampaign].trim().toUpperCase();
			departamentoArray[indiceCampaign] = value;
		}

		for (let item in departamentoArray) {
			let departamentoValidar = this.order.product.department.trim().toUpperCase();
			// let departamentoValidar = ""
			if (departamentoArray[item] === departamentoValidar) {
				//console.log("Con Agendamiento Departamentos")
				this.departamentoValidacion = true

			}
		}

		// FIN DEPARTAMENTO

		let valCanal = this.canalValidacion
		let valTec = this.tecnologiaValidacion
		let depVal = this.departamentoValidacion


		if (valCanal && valTec && depVal) {
			this.router.navigate(['agendamiento']);
		} else {
			this.router.navigate(['contactoinstalacion']);
		}


	}


	sendTiendaVenta() {
		let webOrder = this.createWebOrder()
		for (let i = 0; i < this.order.sva.length; i++) {
			let sva = this.order.sva[i];
			webOrder.sva.push(sva.id);
		}
		//this.loading = true;
		this.orderService.cerrarVenta(webOrder).subscribe(
			data => {
				if (data.responseCode == '00') {
					this.order.id = data.responseData.orderId;
					this.order.cip = data.responseData.cip;
					this.order.status = "F"
					this.ls.setData(this.order);
					this.closeSaleError = false;

					//this.orderService.uploadAudio(this.order.id, file);
					this.router.navigate(['saleProcess/close']);
					// this.router.navigate(['contactoinstalacion']);



				} else {
					this.closeSaleError = true;
					this.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				this.loading = false;
				this.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				this.loading = false;
			}
		);
	}



	//cusi
	tieneNumeros(texto) {
		var numeros = "0123456789";
		for (let i = 0; i < texto.length; i++) {
			if (numeros.indexOf(texto.charAt(i), 0) != -1) {
				return true;
			}
		}
		return false;
	}

	createWebOrder() {
		this.order.cip = " "
		//this.order.disaffiliationWhitePagesGuide = this.disaffiliationWhitePagesGuide;
		//this.order.affiliationElectronicInvoice = this.affiliationElectronicInvoice;
		//this.order.affiliationDataProtection = this.affiliationDataProtection;
		//this.order.publicationWhitePagesGuide = this.publicationWhitePagesGuide;
		//this.order.shippingContractsForEmail = this.shippingContractsForEmail;
		//this.order.campaniaSeleccionada = 'PRUEBA';

		var svaList = [];
		$.each(this.order.sva, function (index, item) {
			svaList.push(item.id);
		});
		console.log("objeto offline");
		
		console.log(this.order.offline);
		
		let webOrder = {
			orderId: this.order.id,
			userId: this.order.user.userId,
			phone: this.order.product.phone,
			department: this.order.product.department,
			province: this.order.product.province,
			district: this.order.product.district,
			address: this.order.product.address,
			latitudeInstallation: this.order.product.coordinateY ? this.order.product.coordinateY : 0,
			longitudeInstallation: this.order.product.coordinateX ? this.order.product.coordinateX : 0,
			addressAdditionalInfo: this.order.product.additionalAddress ? this.order.product.additionalAddress : '',
			sva: svaList,
			customer: this.order.customer,
			product: this.order.selectedOffering,
			type: this.order.type,
			disaffiliationWhitePagesGuide: this.order.disaffiliationWhitePagesGuide,
			affiliationElectronicInvoice: this.order.affiliationElectronicInvoice,
			affiliationDataProtection: this.order.affiliationDataProtection,
			publicationWhitePagesGuide: this.order.publicationWhitePagesGuide,
			shippingContractsForEmail: this.order.shippingContractsForEmail,
			originOrder: this.order.user.groupPermission,
			// Sprint 5 - campos adicionales al cerrar la venta: nombrePuntoVenta y entidad
			entidad: this.order.user.entity,
			nombrePuntoVenta: this.order.user.group,
			// Sprint 6 - condicion preventa filtro web parental
			parentalProtection: this.order.parentalProtection,
			// Sprint 21 - condicion preventa filtro debito automatico
			automaticDebit: this.order.automaticDebit,
			// Sprint 9 - Nombre del producto de origen (solo servirá para flujo SVA)
			sourceProductName: this.order.product.productDescription,
			// Sprint 10 - Campania seleccionada para altas y migras
			campaniaSeleccionada: this.order.campaniaSeleccionada,
			// Sprint 13 - Envio datos normalizador hacia automatizador parte 2
			address_referencia: this.order.address_referencia,
			address_ubigeoGeocodificado: this.order.address_ubigeoGeocodificado,
			address_descripcionUbigeo: this.order.address_descripcionUbigeo,
			address_direccionGeocodificada: this.order.address_direccionGeocodificada,
			address_tipoVia: this.order.address_tipoVia,
			address_nombreVia: this.order.address_nombreVia,
			address_numeroPuerta1: this.order.address_numeroPuerta1,
			address_numeroPuerta2: this.order.address_numeroPuerta2,
			address_cuadra: this.order.address_cuadra,
			address_tipoInterior: this.order.address_tipoInterior,
			address_numeroInterior: this.order.address_numeroInterior,
			address_piso: this.order.address_piso,
			address_tipoVivienda: this.order.address_tipoVivienda,
			address_nombreVivienda: this.order.address_nombreVivienda,
			address_tipoUrbanizacion: this.order.address_tipoUrbanizacion,
			address_nombreUrbanizacion: this.order.address_nombreUrbanizacion,
			address_manzana: this.order.address_manzana,
			address_lote: this.order.address_lote,
			address_kilometro: this.order.address_kilometro,
			address_nivelConfianza: this.order.address_nivelConfianza,
			//Anthony - Sprint 15
			serviceType: this.order.product.parkType,
			cmsServiceCode: this.order.product.serviceCode,
			cmsCustomer: this.order.product.subscriber,
			//Sprint 16
			datetimeSalesCondition: this.order.datetimeSalesCondition,
			flagFtth: this.order.flagFtth,
			offline: this.order.offline,

			//Sprint 27 - dochoaro - tipificacion
            tipificacion: this.order.user.typeFlujoTipificacion    
		}
		return webOrder
	}

	//Sprint 26
	focusout(target) {
		this.focusoutById(target.id);
		/*var target = $("#" + target.id);
		if (target.val() != "") {
			target.addClass("has-content");
		} else {
			target.removeClass("has-content");
		}*/
	}

	focusoutById(id) {
		var target = $("#" + id);
		if (target.val() != "") {
			target.addClass("has-content");
		} else {
			target.removeClass("has-content");
		}
	}
}
