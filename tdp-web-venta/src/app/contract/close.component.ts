import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';
import { ProgressBarService } from '../services/progressbar.service';

import {Customer } from '../model/customer'; 
import { User } from '../model/user'; 
import { Agendamiento } from '../model/agendamientos'; 
import { AgendamientoService } from '../services/agendamiento.service';


var $ = require('jquery');
@Component({
	moduleId: 'close',
	selector: 'close',
	templateUrl: './close.template.html',
	providers: [OrderService]
})
export class CloseComponent implements OnInit{
	order: Order;
	agendamient: Agendamiento;
    orderService: OrderService;
    loading : boolean = false;
    mostrarIDventa: boolean = false;


    model: Customer = new Customer;
	newCustomer: Customer[];



	constructor(private router: Router, 
		orderService: OrderService, 
		private ls: LocalStorageService,
		private progresBar: ProgressBarService,
		private agendamientoService:AgendamientoService){
		this.router = router;
		this.orderService = orderService;
		this.order = this.ls.getData();
  		this.order.status="N";
  		this.ls.setData(this.order);
	}

	finalizarVenta(){
		// alert("venta finalizada");
		// localStorage.removeItem("ventaFinalizada")
	}

	ngOnDestroy(){
		localStorage.removeItem("ventaFinalizada")
	}
	ngOnInit(){
		localStorage.setItem("ventaFinalizada","ok")
		history.pushState(null, document.title, location.href);
		window.addEventListener('popstate', function (event)
		{
		  history.pushState(null, document.title, location.href);
		//   alert("La venta esta Realizada no puede retroceder.")
		  
		});

		if(localStorage.getItem('agendamientoGuardar')){
			this.agendamient = JSON.parse(localStorage.getItem('agendamientoGuardar'));
		
			// alert(JSON.stringify(this.agendamient));


			let id_visor = this.order.id
			let agentClientName = this.agendamient.agentClientName
			let agentClientTel = this.agendamient.agentClientTel
			let agentClientFrangaHoraria = this.agendamient.agentClientFrangaHoraria
			let agentClientFechaSeleccionada = this.agendamient.agentClientFechaSeleccionada
			let cuponesUtilizadoActualizado = this.agendamient.cuponesUtilizadoActualizado
			let id_cupon = this.agendamient.id_cupon
			
			
			this.agendamientoService.saveAgendamiento(id_visor,agentClientName,agentClientTel,agentClientFrangaHoraria,agentClientFechaSeleccionada,cuponesUtilizadoActualizado,id_cupon).subscribe(
				data =>{
					
						this.loading = false;

				
				},
				err=>{
					alert("error")
					this.loading = false;
				}
			)
		}
		

		
		// console.log(this.agendamient)
       
		this.progresBar.getProgressStatus()
		this.order = JSON.parse(localStorage.getItem('order'));
		//console.log("this.order " + JSON.stringify(this.order))
		var userPerfil = this.order.user.typeFlujoTipificacion;
		//console.log("userPerfil "+userPerfil)
		//console.log("this.order 2 " + JSON.stringify(this.order.condicionPackVerde))		 
		if (userPerfil.indexOf("PRESENCIAL") == 0){
			this.mostrarIDventa=false
		}else{
			this.mostrarIDventa=true
		}

		// this.order.customer = this.model;
		// let email = order.customer.email;
	 

	 // 	console.log("Correo "+email);


		//$("#text_email").addClass("none");


	 
	}
}
