import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { CustomerService } from '../services/customer.service';
import { Customer } from '../model/customer';
import { Order } from '../model/order';
import { User } from '../model/user';
import { LocalStorageService } from '../services/ls.service';
import { DireccionService } from '../services/direccion.service';

import { ScoringService } from '../services/scoring.service'; // Sprint 8

import { AppComponent } from '../../app.component';
// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';

import { ProgressBarService } from '../services/progressbar.service';
import { Argumentarios } from '../model/argumentarios';

var $ = require('jquery');

@Component({
	moduleId: 'contacto',
	selector: 'contacto',
	templateUrl: './contacto.template.html',
	providers: [CustomerService, DireccionService, ScoringService]
})

export class ContactoComponent implements OnInit {

	model: Customer = new Customer;
	mobilePhone: string = "";

	newCustomer: Customer[];
	argumentarios : Argumentarios[];
	mjsUser : string;

	order: Order;
	modelDirectio: string = "";
	selectedType: any;
	documentNumber: any;
	isTrue = true;
	loading = false;
	previousUrl;
	errorDocumentType: boolean;
	errorDocumentNumber: boolean;


	showDni: boolean = true;
	showCarneExtranjeria: boolean = false;
	docNroDni: string;
	docNroCarnetExt: string;
	errorConexion: string;
	msgError: boolean = false;

	// Sprint 7... tipos de documento a mostrar son configurables
	showTipoDocumentoDNI: boolean = true;
	showTipoDocumentoCEX: boolean = false;
	showTipoDocumentoPAS: boolean = false;
	showTipoDocumentoRUC: boolean = false;

	showPasaporte: boolean = false;
	showRUC: boolean = false;

	/* Sprint 8 - se adicionan listas de departamentos, provincias y distritos */
	objDepart: any;
	private keyDeparts: string[];
	private departSeleccionado = 0;
	selectDepart: any;

	objProvs: any;
	showProvs = new Array();
	private keyProvs: string[];
	selectProv: any;
	private proviSeleccionado = 0;

	objDists: any;
	showDists = new Array();
	selectDistric: any;
	private keyDistrs: string[];
	private districSeleccionado = 0;

	private loadError: boolean;
	scoringDataModel: any;
	respuestaScoring: boolean;
	private mensaje_error: String;
	customer: Customer;
	scoringService: ScoringService;// Sprint 8
	direccionService: DireccionService; // Sprint 8
	messageTelephone: string
	isTheNumberOk: boolean
	numberIsCorrect: boolean
	flagBlackListPhone: boolean;

	constructor(private router: Router,
		private customerService: CustomerService,
		private ls: LocalStorageService,
		direccionService: DireccionService, scoringService: ScoringService,
		private appComponent: AppComponent,
		private progresBar: ProgressBarService) {
		//this.model = {}
		this.router = router;
		this.customerService = customerService;
		this.ls = ls;
		this.scoringService = scoringService;// Sprint 8
		this.direccionService = direccionService; // Sprint 8
		this.messageTelephone = ""
		this.isTheNumberOk = false
		this.numberIsCorrect = false
	}


	showCancelModal() {
		//console.log(this.order.status);

		$('#cancel').modal('show');


	}


	//    ok(){
	// 	//console.log(this.selectedOption);
	// 	this.errorOption = false;
	// 	this.errorSending = false;
	// 	this.errorCustomReason = false;
	// 	if(this.selectedOption.id==0 || this.selectedOption == 0 || this.selectedOption.value == null){
	// 		this.errorOption = true;
	// 		$("#reason").focus();
	// 	}else{
	// 		this.errorOption = false;
	// 		if(this.selectedOption.id == this.cancelList.length){
	// 			if(this.customReason!=null && typeof(this.customReason) != 'undefined' && !this.isEmpty(this.customReason)){
	// 				if(this.customReason.trim().length==0){
	// 					this.errorCustomReason = true;
	// 				}else{
	// 					this.errorCustomReason = false;
	// 					this.orderData();
	// 				}
	// 			}else{
	// 				this.errorCustomReason = true;
	// 			}
	// 		}else{
	// 			this.orderData();
	// 		}
	// 	}

	// }


	ngOnInit() {

		this.progresBar.getProgressStatus()






		this.order = JSON.parse(localStorage.getItem('order'));

		this.argumentarios = JSON.parse(localStorage.getItem('argumentarios'));		
		
		for (let i in this.argumentarios) {
			let obj = new Argumentarios;
			obj = this.argumentarios[i];
			if(obj.pantalla.toUpperCase() == "contacto".toUpperCase()){
			//	console.log("Entidad Argumntario : "+ obj.entidad);
			//	console.log("Entidad Perfil : "+ this.order.user.entity);
			//	console.log("Mensaje Argumntario : "+ obj.mensaje);
				if(obj.canal.toUpperCase() != "CALL CENTER"){
					this.mjsUser=obj.mensaje.replace('[NOMBRE]',this.order.user.name);
					this.mjsUser=this.mjsUser.replace('[APELLIDO]',this.order.user.lastName);
					break;
				}else if(obj.canal.toUpperCase() == "CALL CENTER"){
					this.mjsUser = obj.mensaje;
					break;
				}
			}
		}


		// order.customer = this.model;
		// order.customer.telephone = this.telefono;
		// let celular = order.customer.telephone;

		// if(celular.length == 8){
		// 	document.getElementById("continuar_").innerHTML = 'CONTINUAR' 
		// }

	//	console.log(this.order);
		// console.log("***********************************************");

		$(".input-effect input").val("");

		$(".input-effect input").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})

		$(".paraselect select").val("");

		$(".paraselect select").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
	}

	correctNumber = (number: string) => {
		this.messageTelephone = `El teléfono se ha guardado correctamente: ${number}`
		document.getElementById("continuar_").innerHTML = 'CONTINUAR'
		this.isTheNumberOk = false
		this.numberIsCorrect = true
		setTimeout(() => {
			this.numberIsCorrect = false
		}, 3000)
	}

	errorInNumber = (message: string) => {
		this.messageTelephone = `${message}`
		this.isTheNumberOk = true
	}

	registrarTelefono() {
		let order = JSON.parse(localStorage.getItem('order'));
		order.customer = this.model;
		order.customer.mobilePhone = this.mobilePhone;
		let celular = order.customer.mobilePhone;

		let cantidad = celular.length
		let grupo = order.user.groupPermission

		/*if (cantidad == 7) {
			if (celular.substring(0, 1) != "0") {
				this.errorInNumber("El teléfono debe empezar con '0'")
			} else {
				this.correctNumber(celular)
			}
		} else */
		if (cantidad == 9) {
			if (celular.substring(0, 1) != "9") {
				this.errorInNumber("El teléfono debe empezar con '9'")
			} else {
				let strListBlackPhone = localStorage.getItem('dataBlackListPhone');

				if(strListBlackPhone==null){
					strListBlackPhone="['011111111','022222222','033333333','044444444','055555555','066666666','077777777','088888888','999999999','000000000']";
				}

				if (strListBlackPhone.includes(celular)) {
					this.errorInNumber("El teléfono es invalido")
				} else {
					this.correctNumber(celular)
				}
			}
		} else {
			if (celular.length < "9") /*{
				this.errorInNumber("El teléfono debe contar con : 0 + Ciudad + Fijo")
			} else */ {
				this.errorInNumber(`El teléfono debe tener 9 dígitos: ${celular}`)
			}
		}
		localStorage.setItem("order", JSON.stringify(order));
	}

	// Sprint 8
	continuarVenta() {

		// En la siguiente pantalla de searchUser/scoring debemos borrar el scoring que esta en el localstorage
		let order = JSON.parse(localStorage.getItem('order'));
		order.scoringDataModel = null;
		order.departmentScoring = null;
		order.provinceScoring = null;
		order.districtScoring = null;
		localStorage.setItem("order", JSON.stringify(order));

		this.router.navigate(['searchUser']);//sprint2 ahora se debe cargar el scoring como 3ra pantalla
	}
}
