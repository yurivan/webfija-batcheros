import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common'
import { Router } from '@angular/router';

import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';

import { ProgressBarService } from '../services/progressbar.service';
import { OutCallService } from '../services/outcall.service';
import { BandejaArchivo } from '../model/bandejaArchivo';
import { OrderService } from '../services/order.service';


var $ = require('jquery');
@Component({
	moduleId: 'audioout',
	selector: 'audioout',
	templateUrl: './audioout.template.html',
	providers: [OutCallService, DatePipe,OrderService]
})

export class audiooutcomponent implements OnInit {
	@ViewChild('fileInput') inputEl: ElementRef;

	order: Order;
	loading = false;

	isTrue = true;

	codatis: string;

	dateStart: string;
	dateEnd: string;
	codeState: string;
	typeState: { id: string, name: string }[];

	bandejaAudio: BandejaArchivo[];
	bandeja0: BandejaArchivo[];
	bandeja1: BandejaArchivo[];
	bandeja2: BandejaArchivo[];

	public data;
	private uploadMaximumSize = 94371840;

	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService,
		private outCall: OutCallService,
		private datepipe: DatePipe,
		private orderService: OrderService) {
		this.router = router;
		this.ls = ls;
	}

	ngOnInit() {
		this.loading = true;
		$('ul > #reportes').removeClass('none');
		$('ul > #Venderr').addClass('none');
		//})
		this.progresBar.getProgressStatus()
		$(".progres").prop("style", "visibility: hidden !important;");

		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}
		this.order = this.ls.getData();
		this.codatis = this.order.user.userId;

		this.getStateAudio();
		this.getDay();
		this.codeState = "0"
		this.getFiltrar();

		this.efectos();
	}

	getFiltrar() {
		this.loading = true;
		let _this = this;
		this.outCall.getListAudio(this.codatis, this.dateStart, this.dateEnd).subscribe(
			data => {
				if (data.responseData.length != 0) {
					console.log(JSON.stringify(data.responseData));
					_this.clasificarEstadoAudio(data.responseData);
					_this.bandejaAudio = _this.bandeja0;
					_this.convertDate();
					_this.codeState = '0';
					_this.loading = false;
				} else {
					_this.loading = false;
				}
			},
			err => {
			}
		);

	}

	removeListAudio(id) {
		this.bandejaAudio = [];
		switch (id) {
			case '0':
				this.bandejaAudio = this.bandeja0;
				break;
			case '1':
				this.bandejaAudio = this.bandeja1;
				break;
			case '2':
				this.bandejaAudio = this.bandeja2;
				break;
		}
		//this.convertDate();
	}

	clasificarEstadoAudio(responseData) {
		this.bandeja0 = [];
		this.bandeja1 = [];
		this.bandeja2 = [];

		this.bandeja0 = responseData;
		for (let audio of responseData) {
			if (audio.statusAudioDescripcion == 'PENDIENTE DE AUDITAR') {
				this.bandeja1.push(audio);
			} else {
				this.bandeja2.push(audio);
			}
		}
	}

	convertDate() {
		for (let i = 0; i < this.bandejaAudio.length; i++) {
			this.bandejaAudio[i].saleDate = this.datepipe.transform(this.bandejaAudio[i].saleDate, 'dd/MM/yyyy');
		}
	}

	getStateAudio() {
		this.typeState = [
			{ id: "0", name: "TODOS" },
			{ id: "1", name: "PENDIENTE DE AUDITAR" },
			{ id: "2", name: "APROBADO" }
		]
	}

	getDay() {
		let hoy = new Date();
		let dd = hoy.getDate();
		let mm = hoy.getMonth() + 1; //hoy es 0!
		let yyyy = hoy.getFullYear();

		let day = dd + "";
		let month = mm + "";
		let monthAnt = (mm - 1) + "";

		if (day.length == 1) {
			day = '0' + day;
		}

		if (month.length == 1) {
			month = '0' + month;
		}

		if (monthAnt.length == 1) {
			monthAnt = '0' + monthAnt;
		}

		this.dateStart = yyyy + '-' + monthAnt + '-' + day;
		this.dateEnd = yyyy + '-' + month + '-' + day;
	}

	efectos() {

		$(".input-effect input").val("");
		$(".paraselect select").val("");
		$(".paraselect select").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})

		$(".input-effect :not(#divTipoBusqueda) input").val("");
		$(".input-effect input").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})

		//subir archivos
		$('.inputfile').each(function () {
			var $input = $(this),
				$label = $input.next('label'),
				labelVal = $label.html();

			$input.on('change', function (e) {
				var fileName = '';

				if (this.files && this.files.length > 1)
					fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);

				else if (e.target.value)
					fileName = e.target.value.split('\\').pop();

				if (fileName)
					$label.find('span').html(fileName);
				else
					$label.html(labelVal);

				$("#subirAudioVisible").removeClass("none")
			});

			// Firefox bug fix
			$input
				.on('focus', function () { $input.addClass('has-focus'); })
				.on('blur', function () { $input.removeClass('has-focus'); });
		});
	}

	adicionarFile($event,id){
		let inAudio=$event.substring(12,$event.length-4)

		//console.log("Recibo: " +inAudio+" Tengo: "+id)
		if(inAudio){
			for(let audio of this.bandejaAudio){
				if(audio.orderId==id){
					this.subirAudio();
				}
			}
		}else{
			alert("Audio no coincide el nombre.")
		}		
	}

	subirAudio() {

		//console.log("this.inputEl: "+this.inputEl)
		

		let audioElement = this.inputEl.nativeElement;

		//console.log("audioElement: "+audioElement)



		let file = audioElement.files[0];
		//this.orderService.uploadAudioMasivo(audioElement.files);
		this.data = [];
		var total = 0;

		if (audioElement.files.length == 0) {
			var mensajeCantidadArchivos = audioElement.files.length + " archivos";
			this.data = [
				{
					"numero": "1",
					"nombreArchivo": "No audio seleccionado",
					"mensaje": "Por favor seleccione al menos un archivo de audio en formato GSM.",
					"responseCode": 1,
					"satisfactorio": false
				}
			];
			alert(this.data[0].mensaje);
			return
		}

		for (var i = 0; i < audioElement.files.length; i += 1) {
			total += audioElement.files[i].size;
		}

		if (total > this.uploadMaximumSize) {
			var mensajeCantidadArchivos = audioElement.files.length + " archivos";
			this.data = [
				{
					"numero": "1",
					"nombreArchivo": mensajeCantidadArchivos,
					"mensaje": "El tamaño de los archivos seleccionados en total superan los 90MB permitidos. Por favor seleccione menos archivos.",
					"responseCode": 1,
					"satisfactorio": false
				}
			];
			alert(this.data[0].mensaje);
			return;
		}

		this.loading = true;
		this.orderService.uploadAudioMasivo(audioElement.files)
			.then(
				response => {
					this.data = response["responseData"];
				//	console.log(this.data)
					alert(this.data[0].mensaje)
					this.loading = false;
					this.inputEl.nativeElement.value = "";
					this.getFiltrar();
				}
			);
	}

	validarDetalle(data:string){ 
		localStorage.setItem("orderIdOut",data)
		this.router.navigate(['audiooutdetalle']);
		
	}

}