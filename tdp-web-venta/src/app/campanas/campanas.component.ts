import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Router } from '@angular/router';
import { Customer } from '../model/customer';
import { ProductOffering } from '../model/productOffering';
import { OfferingService } from '../services/offering.service';
import { Order } from '../model/order';
import { Producto } from '../model/producto';
import { LocalStorageService } from '../services/ls.service';
import { CustomerService } from '../services/customer.service';
import * as globals from '../services/globals';
var $ = require('jquery');
(<any>window).jQuery = $
import { ProgressBarService } from '../services/progressbar.service';
import {Location} from '@angular/common';

@Component({
    moduleId: 'campanas',
    selector: 'campanas',
    templateUrl: './campanas.template.html',
    providers: [OfferingService, CustomerService]
})


export class CampanasComponent implements OnInit {
    currentProductIndex: -1;
    img_upgrades: String[];
    imagenes1: String[];
    productOffer: ProductOffering[];
    productOfferTrio: ProductOffering[]
    productOfferDuo: ProductOffering[]
    productOfferMono: ProductOffering[]
    productOfferCampaigns: ProductOffering[];
    campaigns: String[];
    typeProducts: String[];
    private loading: boolean;
    private visible_accordion;
    private is_product_empty: boolean;
    private is_month_zero: boolean;

    private order: Order;
    private product: Producto;
    private customer: Customer;
    selected: any;
    private showAlert: boolean;
    private showImgValidate: boolean;
    private showButtonSvas: boolean;
    private showButtonOnlySvas: boolean;
    private arpu: number;

    private nameDistrict: string;
    private nameProvince: string;
    private nameDepart: string;
    state: string = 'inactive';
    tab1: boolean
    tab2: boolean
    tab3: boolean
    tab4: boolean
    campana: boolean

    mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios

    model: any;
    type: string;

    filter = {
        productName: '',
        campaign: '',
        productType: ''
    }
    //answerValidationOffer : any;
    answerValidationOffer = {
        responseCode: '',
        responseMessage: '',
        responseData: ''

    }
    errorMessage: string;
    errorCode: string;
    /*model = {
        documentType: null,
        documentNumber: null
    }*/
    panelFlag: boolean[];
    panelFlagMsg: string;
    panelFlagCount: number;
    productsEmpty: boolean = false;


    campaniaName: string[]
    campaniasUnicas: string[]
    diferenciado: string[]

    constructor(private offeringService: OfferingService,
        private router: Router,
        private ls: LocalStorageService,
        private customerService: CustomerService,
        private progresBar: ProgressBarService,
        private _location: Location) {
        this.imagenes1 = [];
        this.offeringService = offeringService;
        this.img_upgrades = [];
        this.loading = true;
        this.visible_accordion = false;
        this.is_product_empty = false;
        this.ls = ls;
        this.customerService = customerService;
        this.showAlert = false;
        this.showImgValidate = false;
        this.showButtonSvas = true;
        this.showButtonOnlySvas = false;
        this.panelFlag = [];
        this.panelFlagMsg = "Ver Mas";
        this.arpu = 0;
        this.panelFlagCount = globals.PANEL_FLAG;
        this.productOfferTrio = []
        this.productOfferDuo = []
        this.productOfferMono = []
        this.productOfferCampaigns = []
        this.tab1 = true
        this.tab2 = false
        this.tab3 = false
        this.tab4 = false

        this.campana = true;

        this.mensajeValidacionDatosCliente = "";//Sprint 6
        this.campaniasUnicas = []
        this.diferenciado = []
    }

    ngOnInit() {

        if(localStorage.getItem("visita")){
            // localStorage.removeItem("dataRed")
            // this.router.navigate(['direccion']);
            this._location.back();
            return
        }

        this.order = this.ls.getData();
       // console.log(this.order);
        
        this.type = this.order.type;

        if (!this.order || !this.order.product || !this.order.customer) {
            this.loading = false;
            this.router.navigate(['home']);
        } else {
            this.product = this.order.product;
            this.customer = this.order.customer;
            this.arpu = Number(this.product.rentaTotal);
            this.model = (this.product);
            this.order.sva = [];
            this.ls.setData(this.order);
            this.getData();
        }

        if (this.type == globals.ORDER_ALTA_NUEVA) {
            var ubicacionPedido = this.ls.getUbicacionPedido();
            this.nameDistrict = ubicacionPedido.distrito;
            this.nameProvince = ubicacionPedido.provincia;
            this.nameDepart = ubicacionPedido.departamento;
        } else if (this.type == globals.ORDER_MEJORAR_EXPERIENCIA || this.type == globals.ORDER_SVA) {
            this.nameDistrict = this.order.product.district;
            this.nameProvince = this.order.product.province;
            this.nameDepart = this.order.product.department;
        }
        this.progresBar.getProgressStatus();

        // if(this.campaniasUnicas.length == 1){
        //     this.router.navigate(['offering']);  
        // }


    }

    toOffering(text, index) {
       // console.log("INDEX " + text);
        localStorage.setItem("select-campana", text);
        //  this.router.navigate(['offering']);
        var productosDeCampaniaSeleccionada = [];

        for (var indiceProducto in this.productOfferCampaigns) {
            var producto = this.productOfferCampaigns[indiceProducto];
            if (producto.campaign == text) {
                productosDeCampaniaSeleccionada.push(producto);
            }
        }
        localStorage.setItem("productsCampaniaSeleccionada", JSON.stringify(productosDeCampaniaSeleccionada));
        this.router.navigate(["offering"]);
    }

    getData() {
        this.loading = true;
        
            if(localStorage.getItem('dataRed')){
                var dataRed = localStorage.getItem('dataRed')
                this.cargarServicio(JSON.parse(dataRed))
                //alert("Entró al if")
            }else{
				//Sprint 12 - se adicionan parametros canal y entidad equivalentes en campanias
            this.offeringService.getData(this.customer, this.product, this.order.user.userId, this.order.user.entity, 
				this.order.user.sellerChannelAtis, this.order.user.sellerSegment, 
                this.order.user.sellerChannelEquivalentCampaign, this.order.user.sellerEntityEquivalentCampaign, this.order.id,
                this.order.flagFtth).subscribe(
                data => {
                    this.cargarServicio(data)
                    //console.log(data)
                },
                err => this.errorCargaOffering(err)
            );
        }

    }

    cargarServicio(data) {
        var campaigns = this.order.user.campaigns;
        //verificamos si es que el vendedor tiene campanias asignadas
        var campaignsArray = campaigns.split(",");
        //console.log("LENGHT"+campaignsArray.length);
        if(localStorage.getItem('dataRed') == null){
            localStorage.setItem('dataRed',JSON.stringify(data)); 
        }

        if (campaignsArray.length == 1 && "" == campaignsArray[0]) {
            campaignsArray = []; 
        }
        for (var indiceCampaign in campaignsArray) {
            var value = campaignsArray[indiceCampaign].trim().toUpperCase();
            campaignsArray[indiceCampaign] = value;
            //console.log("-----indiceCampaign----" + value);
        }

        this.productOfferCampaigns = data.responseData;

        this.campaniasUnicas = [];

        for (var indiceProducto in this.productOfferCampaigns) {
            // console.log(this.productOfferCampaigns[indiceProducto].campaign); 

            var encontrado = false;
            for (var indiceCampaniaExistente in this.campaniasUnicas) {
                if (this.campaniasUnicas[indiceCampaniaExistente] == this.productOfferCampaigns[indiceProducto].campaign) {
                    encontrado = true;
                    break;
                }
            }
            if (!encontrado) {
                this.campaniasUnicas.push(this.productOfferCampaigns[indiceProducto].campaign);
               // console.log("-----campaniasUnicas----" + this.productOfferCampaigns[indiceProducto].campaign);
            }
        }
        for (var i = 0; i < this.campaniasUnicas.length; i++) {
            // console.log("campaniasUnicas "+this.campaniasUnicas[i]);
        }
        this.diferenciado = [];
        var encon = false;
      //  console.log("Por defecto: "+this.campaniasUnicas)
      //  console.log("Asigado: "+campaigns)
        for (var j in this.campaniasUnicas) {
           // console.log("campaniasUnicas " + this.campaniasUnicas[j]);
           // console.log("campaignsArray " + campaignsArray[j]);
            
            for(var x in campaignsArray){
                if (this.campaniasUnicas[j] == campaignsArray[x].toUpperCase()) {
                    encon = true;
                    break;
                }    
            }

            if (encon) {
                if(campaignsArray[j] != null){
                    this.diferenciado.push(campaignsArray[j]) 
                }
            }

        }

      //  console.log("Asigados recientemente: "+this.diferenciado);
        // console.log("Cantidad de elementos1"+campaignsArray[j];);
        // console.log("Cantidad de elementos1"+this.campaniasUnicas);
        //

        if (this.campaniasUnicas.length == 1) {
            localStorage.setItem("visita","existe");
            this.router.navigate(['offering']);
            // alert("campañas "+this.campaniasUnicas[0])
            localStorage.setItem("select-campana", this.campaniasUnicas[0]);
        }

        if(campaignsArray.length == 1){  
            this.router.navigate(['offering']);
            localStorage.setItem("select-campana", campaigns);
        }

        if (this.campaniasUnicas.length == 0) {
            // this.router.navigate(['offering']);
            // alert("campañas "+this.campaniasUnicas[0]) 
            // localStorage.setItem("select-campana", this.campaniasUnicas[0]);
            this.campana = false;
        }

        //

        this.loading = false;
    }

    errorCargaOffering(err) {
        this.loading = false;
        //this.loadError  = true;
        this.errorMessage = "Por favor vuelva a intentarlo";
    }

}
