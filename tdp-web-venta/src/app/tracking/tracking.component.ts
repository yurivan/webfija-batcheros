import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HistoricoService } from '../services/historico.service';
import { LocalStorageService } from '../services/ls.service';
import { Tracking } from '../model/tracking';

import { Order } from '../model/order'

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { global } from '@angular/core/src/facade/lang';

import { ProgressBarService } from '../services/progressbar.service';
import { Lista } from '../model/lista';
import { Estados } from '../model/estadosTr';
import { Recupero } from '../model/recupero';
import { UserService } from '../services/user.service';
import { User } from '../model/user';


var $ = require('jquery');
@Component({
	moduleId: 'tracking',
	selector: 'tracking',
	templateUrl: './tracking.template.html',
	providers: [HistoricoService]
})

export class TrackingComponent implements OnInit {

	isTrue = true;
	loading: boolean;

	codatis: string;

	strSearchVenta: string = "";
	isFlagModalVenta: boolean = false;
	isFlagRecupero: boolean = false;
	isTypeRecupero: boolean = false;
	isMessage: boolean;
	strMessage: string;

	fechaRegistro: string;

	track: Tracking;
	recupero: Recupero;
	isTrack: boolean = false;
	listSearch: Lista[];

	strName1: string;
	strColor1: string;
	count1: number;
	lista1: Lista[];
	notFound1: boolean = false;

	strName2: string;
	strColor2: string;
	count2: number;
	lista2: Lista[];
	notFound2: boolean = false;

	strName3: string;
	strColor3: string;
	count3: number;
	lista3: Lista[];
	notFound3: boolean = false;

	strName4: string;
	strColor4: string;
	count4: number;
	lista4: Lista[];
	notFound4: boolean = false;

	strName5: string;
	strColor5: string;
	count5: number;
	lista5: Lista[];
	notFound5: boolean = false;

	strName6: string;
	strColor6: string;
	count6: number;
	lista6: Lista[];
	notFound6: boolean = false;

	listaVenta: Lista[];
	venta: Lista;
	svaVentas = [];
	order: Order;

	isMessageRecupero: string;
	user: any;
	showRecupero: boolean = false;

	constructor(private router: Router,
		private hs: HistoricoService,
		private progresBar: ProgressBarService,
		private ls: LocalStorageService,
		private userService: UserService) {
		this.router = router;
		this.hs = hs;
		this.ls = ls;
	}

	ngOnInit() {
		$(document).ready(function () {
			$('.input_tel').keypress(function (tecla) {
				if (tecla.charCode < 48 || tecla.charCode > 57) return false;
			});
		});

		$('ul > #reportes').removeClass('none');
		$('ul > #Venderr').addClass('none');

		this.progresBar.getProgressStatus()
		$(".progres").prop("style", "visibility: hidden !important;");
		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}

		this.order = this.ls.getData();
		this.codatis = this.order.user.userId;
		//Logica
		this.getTracking(this.codatis);

		let _scope = this 

		this.user = this.order.user;

		this.userService.niveles(this.user.niveles, this.user.channel)
		.subscribe(data => {
			_scope.showRecupero = data.responseData.f_recupero;
			//console.log(_scope.showRecupero);
			
		},error =>{
			$("#data").removeClass("temp")
		});

	}

	getTracking(codatis) {
		let _this = this;
		this.hs.getFilterTracking(codatis).subscribe(
			data => {
				_this.track = data.responseData;
				_this.getTypeStates(_this.track);
				_this.addListSearch();
				_this.isTrack = true;
			},
			err => { }
		);
	}

	addListSearch() {
		this.listSearch = [];
		for (let estados of this.track.estados) {
			for (let list of estados.lista) {
				list.color = estados.color;
				this.listSearch.push(list);
			}
		}
		//console.log(this.listSearch)
	}

	getTypeStates(track) {
		let estados: Estados[];
		estados = track.estados;
		for (let i = 0; i < estados.length; i++) {
			switch (i) {
				case 0:
					this.strName1 = estados[i].nombre;
					this.strColor1 = estados[i].color;
					this.count1 = estados[i].cantidad;
					this.lista1 = estados[i].lista;
					if (this.lista1.length == 0) {
						this.notFound1 = true;
					}
					break;
				case 1:
					this.strName2 = estados[i].nombre;
					this.strColor2 = estados[i].color;
					this.count2 = estados[i].cantidad;
					this.lista2 = estados[i].lista;
					if (this.lista2.length == 0) {
						this.notFound2 = true;
					}
					break;
				case 2:
					this.strName3 = estados[i].nombre;
					this.strColor3 = estados[i].color;
					this.count3 = estados[i].cantidad;
					this.lista3 = estados[i].lista;
					if (this.lista3.length == 0) {
						this.notFound3 = true;
					}
					break;
				case 3:
					this.strName4 = estados[i].nombre;
					this.strColor4 = estados[i].color;
					this.count4 = estados[i].cantidad;
					this.lista4 = estados[i].lista;
					if (this.lista4.length == 0) {
						this.notFound4 = true;
					}
					break;
				case 4:
					this.strName5 = estados[i].nombre;
					this.strColor5 = estados[i].color;
					this.count5 = estados[i].cantidad;
					this.lista5 = estados[i].lista;
					if (this.lista5.length == 0) {
						this.notFound5 = true;
					}
					break;
				case 5:
					this.strName6 = estados[i].nombre;
					this.strColor6 = estados[i].color;
					this.count6 = estados[i].cantidad;
					this.lista6 = estados[i].lista;
					if (this.lista6.length == 0) {
						this.notFound6 = true;
					}
					break;
			}
		}
	}

	getDiv(num) {
		console.log("holas "+num);
		
		switch (num) {
			case 1:
				$('#solicitado').addClass('active');
				$('#ingresado').removeClass('active');
				$('#proceso').removeClass('active');
				$('#instalado').removeClass('active');
				$('#caida').removeClass('active');
				$('#offline').removeClass('active');
				break;
			case 2:
				$('#solicitado').removeClass('active');
				$('#ingresado').addClass('active');
				$('#proceso').removeClass('active');
				$('#instalado').removeClass('active');
				$('#caida').removeClass('active');
				$('#offline').removeClass('active');
				break;
			case 3:
				$('#solicitado').removeClass('active');
				$('#ingresado').removeClass('active');
				$('#proceso').addClass('active');
				$('#instalado').removeClass('active');
				$('#caida').removeClass('active');
				$('#offline').removeClass('active');
				break;
			case 4:
				$('#solicitado').removeClass('active');
				$('#ingresado').removeClass('active');
				$('#proceso').removeClass('active');
				$('#instalado').addClass('active');
				$('#caida').removeClass('active');
				$('#offline').removeClass('active');
				break;
			case 5:
				$('#solicitado').removeClass('active');
				$('#ingresado').removeClass('active');
				$('#proceso').removeClass('active');
				$('#instalado').removeClass('active');
				$('#caida').addClass('active');
				$('#offline').removeClass('active');
				break;
			case 6:
				$('#solicitado').removeClass('active');
				$('#ingresado').removeClass('active');
				$('#proceso').removeClass('active');
				$('#instalado').removeClass('active');
				$('#caida').removeClass('active');
				$('#offline').addClass('active');
				break;
		}
	}

	searchUser() {
		let listaBusqueda: Lista[];
		listaBusqueda = [];
		if (this.strSearchVenta == "") {
			this.isMessage = true;
			this.strMessage = "El campo de busqueda se encuentra vacio."
		} else {
			let count = this.strSearchVenta.length;
			for (let list of this.listSearch) {
				if (this.strSearchVenta.toLowerCase() == list.document.substring(0, count).toLowerCase()
					|| this.strSearchVenta.toLowerCase() == list.name.substring(0, count).toLowerCase()) {
					listaBusqueda.push(list);
				}
			}
			if (listaBusqueda.length == 0) {
				this.isMessage = true;
				this.strMessage = "No se encontro ventas indicada."
			} else {
				//console.log(JSON.stringify(listaBusqueda));
				localStorage.setItem('listaVentas', JSON.stringify(listaBusqueda));
				this.router.navigate(['resultados']);
			}
		}
	}

	cerrarModal() {
		this.isFlagModalVenta = false;
	}

	cerrarModalRecupero() {
		this.isFlagRecupero = false;
	}

	cerrarMensaje() {
		this.isMessage = false;
	}

	getDetailData(idventa) {
		this.venta = null;
		this.svaVentas = []
		for (let estados of this.track.estados) {
			for (let lista of estados.lista) {
				if (idventa == lista.id) {
					this.venta = lista;
					for (let index = 0; index < 10; index++) {
						let svaCantidad = lista[`svaCantidad${index + 1}`]
						let svaNombre = lista[`svaNombre${index + 1}`]
						if (svaCantidad != null && svaCantidad != "" && svaCantidad != 0) {
							this.svaVentas.push({ svaNombre, svaCantidad })
						}
					}
					this.venta.color = estados.color;
					this.isFlagModalVenta = true;
					this.isTypeRecupero = true;
				}

				if (this.venta != null) {
					break;
				}
			}

			if (this.venta != null) {
				break;
			}
		}
	}

	getDetailRecupero(idventa) {
		this.venta = null;
		for (let estados of this.track.estados) {
			for (let lista of estados.lista) {
				if (idventa == lista.id) {
					this.venta = lista;
					this.venta.color = estados.color;
					this.isFlagRecupero = true;
					this.isTypeRecupero = true;
					this.isMessageRecupero = "Datos a completar para el recupero";
				}

				if (this.venta != null) {
					break;
				}
			}

			if (this.venta != null) {
				break;
			}
		}
	}

	recuperoDeuda() {
		this.loading = true;
		this.recupero = new Recupero();
		this.recupero.orderId = this.venta.id;
		this.recupero.motivoCaida = this.venta.motivoCaida;
		this.recupero.fechaPago = new Date(this.fechaRegistro);
		let _this = this;
		this.hs.getRecuperoVenta(this.recupero).subscribe(
			data => {
				_this.isMessageRecupero = data.responseData.respuesta;
				_this.isTypeRecupero = false;
				_this.loading = false;
				_this.getTracking(_this.codatis)
			},
			err => { }
		)
	}

	recuperoDireccion() {
		this.loading = true;
		this.recupero = new Recupero();
		this.recupero.orderId = this.venta.id;
		this.recupero.motivoCaida = this.venta.motivoCaida;
		this.recupero.direccion = this.venta.direccion;
		let _this = this;
		this.hs.getRecuperoVenta(this.recupero).subscribe(
			data => {
				_this.isMessageRecupero = data.responseData.respuesta;
				_this.isTypeRecupero = false;
				_this.loading = false;
				_this.getTracking(_this.codatis)
			},
			err => { }
		)
	}

	recuperoTelf() {
		this.loading = true;
		let ok= false
		if (this.venta.telefonoCliente.length==9 && Number.isInteger(Number(this.venta.telefonoCliente))) {
			this.recupero = new Recupero();
			this.recupero.orderId = this.venta.id;
			this.recupero.motivoCaida = this.venta.motivoCaida;

			this.recupero.telefono = this.venta.telefonoCliente;
			let _this = this;
			this.hs.getRecuperoVenta(this.recupero).subscribe(
				data => {
					_this.isMessageRecupero = data.responseData.respuesta;
					_this.isTypeRecupero = false;
					_this.loading = false;
					_this.getTracking(_this.codatis)
				},
				err => { }
			)
		} else {
			alert("Ingresa el telefono correctamente.")
			this.loading=false
		}

	}

	recuperoDecos() {
		this.loading = true;
		this.recupero = new Recupero();
		this.recupero.orderId = this.venta.id;
		this.recupero.motivoCaida = this.venta.motivoCaida;
		this.recupero.svaSmart = this.venta.decosSmart;
		this.recupero.svaHd = this.venta.decosHD;
		this.recupero.svaDvr = this.venta.decosDVR;
		let _this = this;
		this.hs.getRecuperoVenta(this.recupero).subscribe(
			data => {
				_this.isMessageRecupero = data.responseData.respuesta;
				_this.isTypeRecupero = false;
				_this.loading = false;
				_this.getTracking(_this.codatis)
			},
			err => { }
		)
	}

	interpretarServicios(offline){
		
		var data = []
		data["flag"]=""

		for(let parameters of offline.split(";")){
			if(parameters.split(":").length==2){
				data[parameters.split(":")[0].trim()] = parameters.split(":")[1].trim()
			}
		}

		var respuesta = ""
		if(data["flag"].indexOf("1") > -1){
			respuesta += "RUC ,"
		}if(data["flag"].indexOf("6") > -1){
			respuesta += "RENIEC ,"
		}if(data["flag"].indexOf("7") > -1){
			respuesta += "PAGOEFECTIVO ,"
		}
		return respuesta.substring(0, respuesta.length - 1);
	}
}
