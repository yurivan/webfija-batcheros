import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common'
import { Router } from '@angular/router';

import { CustomerService } from '../services/customer.service';
import { outdetalle } from '../model/outdetalle';
import { LocalStorageService } from '../services/ls.service';

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { SvaService } from '../services/sva.service';
import { Customer } from '../model/customer';
import { Producto } from '../model/producto';
import { OutCallService } from '../services/outcall.service';
import { OutDetalleService } from '../services/outDetalle.service';
import { BandejaArchivo } from '../model/bandejaArchivo';
import { locale } from 'moment';

var $ = require('jquery');
class RequestDepartment {
	id: string
	value: any
}

@Component({
	moduleId: 'audiooutdetalle',
	selector: 'audiooutdetalle',
	templateUrl: './audiooutdetalle.template.html',
	providers: [OutCallService, DatePipe, OutDetalleService]
})



export class audiooutdetallecomponent implements OnInit {
	loading = false;
	audioOutDetalle = [];

	id: string
	nombre: string
	fechaNacimiento: string
	numTelefono: string
	tipoDocumento: string
	numDocumento: string

	departamento: string
	provincia: string
	distrito: string

	direccion: string
	referencia: string

	producto: string
	tecnologiaInternet: string
	beneficio: string

	incluye: string
	equipamento: string
	modo: string
	precioRegular: string
	PrecioPromocional: string


	tratamientoDatos: string
	reciboDigital: string
	filtroParental: string

	orderIdOut: string

	modalCaida: boolean

	outDetalle: outdetalle

	clientSvaNombre1: string
	clientSvaNombre2: string
	clientSvaNombre3: string
	clientSvaNombre4: string
	clientSvaNombre5: string
	clientSvaNombre6: string
	clientSvaNombre7: string
	clientSvaNombre8: string
	clientSvaNombre9: string
	clientSvaNombre10: string

	clientSvaPrecio1: string
	clientSvaPrecio2: string
	clientSvaPrecio3: string
	clientSvaPrecio4: string
	clientSvaPrecio5: string
	clientSvaPrecio6: string
	clientSvaPrecio7: string
	clientSvaPrecio8: string
	clientSvaPrecio9: string
	clientSvaPrecio10: string


	clientProducPrecioPromocional: string
	clientProducPrecioPromocionalTotal: string

	other:string


	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService,
		private outCall: OutCallService,
		private outDetalleService: OutDetalleService,
		private datepipe: DatePipe) {
		this.router = router;
		this.ls = ls;
		this.outDetalleService = outDetalleService;
		this.modalCaida = false
		this.other = ""
	}

	ngOnInit() {
		this.orderIdOut = localStorage.getItem('orderIdOut');
		this.loading = true;
		this.initService(this.orderIdOut);
		this.progresBar.getProgressStatus();


		

	}
	initService(order_id: string) {
		this.outDetalleService.getData(order_id)//"-Ka2uHX_ZFuk62SqEAaa"
			.subscribe(
				data => {
					if (typeof data.responseData !== 'undefined' && data.responseData !== null) {

						this.outDetalle = data.responseData
						this.initValues(data.responseData);
					} else {
						alert(data.responseMessage);

					}
				},
				err => {
					console.log(err)
				}
			);
	}

	initValues(data: outdetalle) {
		this.id = this.orderIdOut
		this.nombre = data.clientNombre
		this.fechaNacimiento = data.clientFechaNacimiento
		this.numTelefono = data.clientNumeroTelefono
		this.tipoDocumento = data.clientTipoDocumento
		this.numDocumento = data.clientNumeroDoc		
								
		this.departamento = data.clientProducDepa
		this.provincia = data.clientProducProvincia
		this.distrito = data.clientProducDistrito


		this.direccion = data.clientProducDireccion
		this.referencia = data.clientProducReferencia

		// if(this.direccion == ''){
		// 	this.direccion = null
		//  }

		// if(this.referencia == ''){
		// 	this.referencia = null
		// }

		this.producto = data.clientProducNombre

		this.tecnologiaInternet = data.clientProducTecnologia
		if (this.tecnologiaInternet == '') {
			this.tecnologiaInternet = null
		}
		this.beneficio = data.clientProducBeneficios
		if (this.beneficio == '') {
			this.beneficio = null
		}
		this.incluye = data.clientProducIncluye
		if (this.incluye == '') {
			this.incluye = null
		}
		this.equipamento = data.clientProducEquipamento
		if (this.equipamento == '') {
			this.equipamento = null
		}
		this.modo = data.clientProducModo
		if (this.modo == '') {
			this.modo = null
		}
		this.precioRegular = data.clientProducPrecioRegular
		if (this.precioRegular == '') {
			this.precioRegular = null
		}
		this.PrecioPromocional = data.clientProducPrecioPromocional
		if (this.PrecioPromocional == '') {
			this.PrecioPromocional = null
		}
		this.PrecioPromocional = data.clientProducPrecioPromocional
		if (this.PrecioPromocional == '') {
			this.PrecioPromocional = null
		}

		// SVA's START
		this.clientSvaNombre1 = data.clientSvaNombre1
		this.clientSvaPrecio1 = data.clientSvaPrecio1
		this.clientSvaNombre2 = data.clientSvaNombre2
		this.clientSvaPrecio2 = data.clientSvaPrecio2
		this.clientSvaNombre3 = data.clientSvaNombre3
		this.clientSvaPrecio3 = data.clientSvaPrecio3
		this.clientSvaNombre4 = data.clientSvaNombre4
		this.clientSvaPrecio4 = data.clientSvaPrecio4

		this.clientSvaNombre5 = data.clientSvaNombre5
		this.clientSvaPrecio5 = data.clientSvaPrecio5
		this.clientSvaNombre6 = data.clientSvaNombre6
		this.clientSvaPrecio6 = data.clientSvaPrecio6
		this.clientSvaNombre7 = data.clientSvaNombre7
		this.clientSvaPrecio7 = data.clientSvaPrecio7
		this.clientSvaNombre8 = data.clientSvaNombre8
		this.clientSvaPrecio8 = data.clientSvaPrecio8

		this.clientSvaNombre9 = data.clientSvaNombre9
		this.clientSvaPrecio9 = data.clientSvaPrecio9
		this.clientSvaNombre10 = data.clientSvaNombre10
		this.clientSvaPrecio10 = data.clientSvaPrecio10

		if (this.clientSvaNombre1 == '') {
			this.clientSvaNombre1 = null
		}
		if (this.clientSvaPrecio1 == '') {
			this.clientSvaPrecio1 = null
		}
		if (this.clientSvaNombre2 == '') {
			this.clientSvaNombre2 = null
		}
		if (this.clientSvaPrecio2 == '') {
			this.clientSvaPrecio2 = null
		}
		if (this.clientSvaNombre3 == '') {
			this.clientSvaNombre3 = null
		}
		if (this.clientSvaPrecio3 == '') {
			this.clientSvaPrecio3 = null
		}
		if (this.clientSvaNombre4 == '') {
			this.clientSvaNombre4 = null
		}
		if (this.clientSvaPrecio4 == '') {
			this.clientSvaPrecio4 = null
		}

		if (this.clientSvaNombre5 == '') {
			this.clientSvaNombre5 = null
		}
		if (this.clientSvaPrecio5 == '') {
			this.clientSvaPrecio5 = null
		}
		if (this.clientSvaNombre6 == '') {
			this.clientSvaNombre6 = null
		}
		if (this.clientSvaPrecio6 == '') {
			this.clientSvaPrecio6 = null
		}
		if (this.clientSvaNombre7 == '') {
			this.clientSvaNombre7 = null
		}
		if (this.clientSvaPrecio7 == '') {
			this.clientSvaPrecio7 = null
		}
		if (this.clientSvaNombre8 == '') {
			this.clientSvaNombre8 = null
		}
		if (this.clientSvaPrecio8 == '') {
			this.clientSvaPrecio8 = null
		}

		if (this.clientSvaPrecio9 == '') {
			this.clientSvaPrecio9 = null
		}
		if (this.clientSvaNombre9 == '') {
			this.clientSvaNombre9 = null
		}
		if (this.clientSvaPrecio10 == '') {
			this.clientSvaPrecio10 = null
		}
		if (this.clientSvaNombre10 == '') {
			this.clientSvaNombre10 = null
		}
		//SVA's FIN


		this.clientProducPrecioPromocional = data.clientProducPrecioPromocional
		this.clientProducPrecioPromocionalTotal = data.clientProducPrecioPromocionalTotal

		if (this.clientProducPrecioPromocional == '') {
			this.clientProducPrecioPromocional = null
		}
		if (this.clientProducPrecioPromocionalTotal == '') {
			this.clientProducPrecioPromocionalTotal = null
		}

		this.tratamientoDatos = data.clientTratamientoDatos
		this.reciboDigital = data.clientReciboDigital
		this.filtroParental = data.clientFiltroParental


		this.loading = false;



	}

	aprobado() {


		this.loading = true;
		let order = JSON.parse(localStorage.getItem('order'));

		let order_id = this.orderIdOut
		let user_id = order.user.userId;
		let name = order.user.name + " " + order.user.lastName;
		let canal_atis = order.user.channel;
		let extension = "gsm"

		let direccion = this.direccion
		let referencia = this.referencia

		this.outDetalleService.setAprobado(order_id, user_id, name, canal_atis, extension, direccion, referencia)
			.subscribe(
				data => {
					if (typeof data.responseCode !== 'undefined' || data.responseCode !== null || data.responseCode == '0') {
						alert(data.responseMessage)
						this.router.navigate(['audioout']);
						this.loading = false;

					} else {

						this.loading = false; 
						alert("hubo un problema")
						
					}
				},
				err => {
					console.log(err)
				}
			);

	}
	closeOpen() {
		this.modalCaida = false
	}
	openModal() {
		this.modalCaida = true
	}
 

	caida() {
		let motivoCaida = null
		$("input:radio:checked").each(   
			function() {
				motivoCaida = $(this).val()
			}
		);
 
		 this.loading = true;
		 let order = JSON.parse(localStorage.getItem('order'));
 
		 let order_id = this.orderIdOut
		 let user_id = order.user.userId;
		 let name = order.user.name + " " + order.user.lastName;
		 let canal_atis = order.user.channel;
		 let extension = "gsm"
		 let motivo_caida = motivoCaida
 
		 this.outDetalleService.setCaida(order_id, user_id, name, canal_atis, extension,motivo_caida)
			 .subscribe(
				 data => {
					 if (typeof data.responseCode !== 'undefined' || data.responseCode !== null || data.responseCode == '0') {
						 alert(data.responseMessage)
						 this.loading = false;
						 this.router.navigate(['audioout'])
					 } else {
						 this.loading = false;
						 alert("hubo un problema")
					 }
				 },
				 err => {
					 console.log(err)
				 }
			 );
  
	}

 
}