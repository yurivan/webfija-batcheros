import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HistoricoService } from '../services/historico.service';
import { LocalStorageService } from '../services/ls.service';
import { Historico } from '../model/historico';
import { Meses } from '../model/meses';

import { Order } from '../model/order'

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { global } from '@angular/core/src/facade/lang';

import { ProgressBarService } from '../services/progressbar.service';
import { Dias } from '../model/dias';
import { Lista } from '../model/lista';
import { Estados } from '../model/estados';

import { VentaService } from '../services/venta.service';

var $ = require('jquery');
@Component({
	moduleId: 'historico',
	selector: 'historico',
	templateUrl: './historico.template.html',
	providers: [HistoricoService, VentaService]
})

export class HistoricoComponent implements OnInit {

	isTrue = true;
	loading: boolean;
	isSearchHistoric: boolean;
	isFlagModalVenta: boolean = false;
	isMessage: boolean = false;
	strMessage: string;

	codatis: string;
	codeState: string;
	dateStart: string;
	dateEnd: string;

	strSearchVenta: string = "";

	historicoModel: Historico;
	listaMes: Meses[];
	listaDia: Dias[];
	listaVenta: Lista[];
	venta: Lista;
	order: Order;
	listSearch: Lista[];
	estados: Estados[];
	svaVentas = [];
	typeState: { id: string, name: string }[];

	selectEstado: any;
	inputTextFechaFin: any;
	inputTextFechaInicio: any;

	constructor(private router: Router,
		private hs: HistoricoService,
		private progresBar: ProgressBarService,
		private ventaService: VentaService,
		private ls: LocalStorageService) {
		this.ventaService = ventaService;
		this.router = router;
		this.hs = hs;
		this.ls = ls;
	}

	ngOnInit() {
		$('ul > #reportes').removeClass('none');
		$('ul > #Venderr').addClass('none');
		this.progresBar.getProgressStatus()
		$(".progres").prop("style", "visibility: hidden !important;");
		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}

		this.order = this.ls.getData();
		this.codatis = this.order.user.userId;
		//Logica
		this.getStateTrazabilidad();

		this.getDay();
		this.codeState = "0"

		this.loading = false;
		this.isSearchHistoric = false;
		this.listaMes = null;
	}

	getDay() {
		let hoy = new Date();
		let dd = hoy.getDate();
		let mm = hoy.getMonth() + 1; //hoy es 0!
		let yyyy = hoy.getFullYear();

		let day = dd + "";
		let month = mm + "";
		let monthAnt = (mm-1) + "";

		if (day.length == 1) {
			day = '0' + day;
		}

		if (month.length == 1) {
			month = '0' + month;
		}

		if (monthAnt.length == 1) {
			monthAnt = '0' + monthAnt;
		}

		this.dateStart = yyyy + '-' + monthAnt + '-' + day + ' 00:00:00';
		this.dateEnd = yyyy + '-' + month + '-' + day + ' 23:59:59';
	}

	getStateTrazabilidad() {
		//Posiblemente a mejorar
		this.typeState = [
			{ id: "0", name: "Todos" },
			{ id: "1", name: "Solicitado" },
			{ id: "2", name: "Ingresado" },
			{ id: "3", name: "En Proceso" },
			{ id: "4", name: "Instalado" },
			{ id: "5", name: "Caida" }
		]
	}

	getFiltrar() {
		this.listaMes = null;
		this.loading = true;
		this.isSearchHistoric = false;
		$('#messageHistorico').hide();
		let _this = this;
		$('#historico').show()

		let comienza = ""
		let termina = ""
		// let comienza = this.dateStart + ' 00:00';
		// let termina = this.dateEnd + ' 23:59';
		if(this.dateStart.length > 11){
			 comienza = this.dateStart 	 	
		}else{
			 comienza = this.dateStart + ' 00:00:00';
		}

		if(this.dateEnd.length > 11){
			 termina = this.dateEnd 	 	
		}else{
			 termina = this.dateEnd + ' 23:59:59';
		}

		this.hs.getFilterHistoric(this.codatis, this.codeState, 
			comienza, termina).subscribe(
			data => {
				if (data.responseData.meses.length != 0) {
					_this.historicoModel = data.responseData;
					_this.listaMes = _this.historicoModel.meses;
					_this.loading = false;
					_this.isSearchHistoric = true;
					_this.addListSearch();
				} else {
					$('#historico').hide()
					$('#messageHistorico').html('No se encontro información en la búsqueda.');
					$('#messageHistorico').show();
					_this.loading = false;
				}
			},
			err => { }
		);
	}

	addListSearch() {
		this.listSearch = [];
		for (let mes of this.listaMes) {
			this.listaDia = mes.dias;
			this.estados = mes.estados;
			for (let dia of this.listaDia) {
				this.listaVenta = dia.lista;
				for (let ventas of this.listaVenta) {
					switch (ventas.estado) {
						case "SOLICITADO":
							ventas.color = this.estados[0].color
							break;
						case "INGRESADO":
							ventas.color = this.estados[1].color
							break;
						case "EN PROCESO":
							ventas.color = this.estados[2].color
							break;
						case "INSTALADO":
							ventas.color = this.estados[3].color
							break;
						case "CAIDA":
							ventas.color = this.estados[4].color
							break;
					}
					this.listSearch.push(ventas);
				}
			}
		}
	}

	searchUser() {
		let listaBusqueda: Lista[];
		listaBusqueda = [];
		if (this.strSearchVenta == "") {
			this.isMessage = true;
			this.strMessage = "El campo de busqueda se encuentra vacio."
		} else {
			let count = this.strSearchVenta.length;
			for (let list of this.listSearch) {
				if (this.strSearchVenta.toLowerCase() == list.document.substring(0, count).toLowerCase()
					|| this.strSearchVenta.toLowerCase() == list.name.substring(0, count).toLowerCase()) {
					listaBusqueda.push(list);
				}
			}
			if (listaBusqueda.length == 0) {
				this.isMessage = true;
				this.strMessage = "No se encontro ventas indicada."
			} else {
				//console.log(JSON.stringify(listaBusqueda));
				localStorage.setItem('listaVentas', JSON.stringify(listaBusqueda));
				this.router.navigate(['resultados']);
			}
		}
	}

	getDetailData(idventa) {
		this.venta = null;
		this.svaVentas = []
		for (let mes of this.listaMes) {
			this.listaDia = mes.dias;
			for (let dia of this.listaDia) {
				this.listaVenta = dia.lista;
				for (let ventas of this.listaVenta) {
					if (idventa == ventas.id) {
						this.venta = ventas;
						for (let index = 0; index < 10; index++) {
							let svaCantidad = ventas[`svaCantidad${index + 1}`]
							let svaNombre = ventas[`svaNombre${index + 1}`]
							if (svaCantidad != null && svaCantidad != "" && svaCantidad != 0) {
								this.svaVentas.push({ svaNombre, svaCantidad })
							}
						}
						this.venta.color = mes.color;
						this.isFlagModalVenta = true;
					}

					if (this.venta != null) {
						break;
					}
				}

				if (this.venta != null) {
					break;
				}
			}

			if (this.venta != null) {
				break;
			}
		}
	}

	cerrarModal() {
		this.isFlagModalVenta = false;
	}

	cerrarMensaje() {
		this.isMessage = false;
	}

	buscarReporteVenta() {
		this.loading = true;

		var status_seleccionado = this.codeState;
		var codigoVendedor = this.order.user.userId;

		this.inputTextFechaInicio = document.getElementById("fecha1");
		this.inputTextFechaFin = document.getElementById("fecha2");
		var fechaInicio = this.inputTextFechaInicio.value;
		var fechaFin = this.inputTextFechaFin.value;
		//console.log("fechaInicio");
		//console.log(fechaInicio);
		//console.log("fechaFin");
		//console.log(fechaFin);

		if (fechaInicio) {
			var dateFechaInicio = this.getDate(fechaInicio, true);
			fechaInicio = dateFechaInicio.toUTCString();

			// si es que no se selecciono fecha fin entonces seteamos la misma fecha inicio
			if (!fechaFin) {
				fechaFin = fechaInicio;
			}
			var dateFechaFin = this.getDate(fechaFin, false);
			fechaFin = dateFechaFin.toUTCString();
		}

		//alert("buscarVentasAudiosPendientes!!!!! -> estadoSeleccionado: " + status_seleccionado + " fechaInicio: " + fechaInicio + " fechaInicio: " + fechaFin);
		this.ventaService.getReporteVentaHistorico(codigoVendedor, status_seleccionado,	fechaInicio, fechaFin);
		this.loading = false;

	}

	getDate(dateYYYYMMDD, fromStartDay) {

		var iniDatetimeYYYY = dateYYYYMMDD.substring(0, 4);
		var iniDatetimeMM = dateYYYYMMDD.substring(5, 7) - 1;
		var iniDatetimeDD = dateYYYYMMDD.substring(8, 10);

		var d = null;

		if (fromStartDay) {
			d = new Date(iniDatetimeYYYY, iniDatetimeMM, iniDatetimeDD, 0, 0, 0, 0);
		} else {
			d = new Date(iniDatetimeYYYY, iniDatetimeMM, iniDatetimeDD, 23, 59, 59, 0);
		}
		return d;
	}
}