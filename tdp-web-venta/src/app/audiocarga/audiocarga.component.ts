import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Http } from "@angular/http";
import { VentaService } from '../services/venta.service';
import { LocalStorageService } from '../services/ls.service';
import { Venta } from '../model/venta';
import { Order } from '../model/order';

import { SalesReportService } from '../services/salesReport.service';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../services/order.service';

var $ = require('jquery');

import { ProgressBarService } from '../services/progressbar.service';

@Component({
	selector: 'audiocarga',
	templateUrl: './audiocarga.component.html',
	providers: [VentaService, SalesReportService, OrderService]
})


export class AudioCargaComponent implements OnInit {
	@ViewChild('fileInput') inputEl: ElementRef;
	public data;
	public filterQuery = "";
	public rowsOnPage = 10;
	public sortBy = "email";
	public sortOrder = "asc";
	ventas: Venta[];
	order: Order;
	selectEstado: any;
	inputTextCodigoVendedor: any;
	inputTextDniCliente: any;
	private loading: boolean;
	model: any;
	private readonlyFinDate: boolean = true; //glazaror se adiciona propiedad readonly para datepicker fechaFin
	private maxDatePicker: string = "";
	private estadoSeleccionado: any;
	private audioCargado: boolean;
	private uploadMaximumSize = 94371840;
	salesReportDetail = [];
	idOrder: string;

	filtroRestringeHoras : string = "SI";

	tablaVacia: boolean = true;

	constructor(private progresBar: ProgressBarService, private salesReportService: SalesReportService, private location: Location, private activeRoute: ActivatedRoute, private http: Http, private ventaService: VentaService, private ls: LocalStorageService, private router: Router, private orderService: OrderService) {
		this.ventaService = ventaService;
		this.ls = ls;
		this.model = {

		};
		this.salesReportService = salesReportService;
		this.idOrder = "";
		// this.cargarParametros(this.activeRoute);
	}
	cargarParametros(activeRoute) {
		activeRoute.queryParams.subscribe(params => {
			this.idOrder = params['id'];
			//console.log("orderrr " + this.idOrder);
		});
	}


	ngOnInit(): void {

		this.progresBar.getProgressStatus()

		// $("#Venderr").attr("href", "/acciones")
		// $("#reportess").attr("href", "/home")
		//$("#bandeja_audio").attr("href", "/audiocarga")
		this.order = this.ls.getData();
		this.loadMaxDate();
		
		this.efectos();

		$('#estadoAudio').val("1");
		$('#restringeHoras').val("SI");
		this.buscarVentasAudiosPendientes();
		this.initVentaAudio_();

		$(document).ready(function () {
			$('#codigoVendedor').keypress(function (tecla) {
				if (tecla.charCode < 48 || tecla.charCode > 57) return false;

			});

		});



	}


	initVentaAudio_() {
		this.audioCargado = false;
		if (this.idOrder != '0') {
			this.loading = true;
			this.salesReportService.getData(this.idOrder)//"-Ka2uHX_ZFuk62SqEAaa"
				.subscribe(
					data => {
						if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
							this.salesReportDetail = data.responseData
							localStorage.setItem('salesReportDetail', JSON.stringify(Object.assign(this.salesReportDetail)))
						//	console.log(data.responseData);
							this.loading = false;

						} else {
							//comentado por validación que será requerida después
							// alert(data.responseMessage);
							this.loading = false;
						}
					},
					err => {
					//	console.log(err)
						this.loading = false;
					}
				);
		}
	}

	efectos() {

		$(".input-effect input").val("");

		$(".paraselect select").val("");

		$(".paraselect select").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})

		$(".input-effect :not(#divTipoBusqueda) input").val("");

		$(".input-effect input").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})

		//subir archivos
		$('.inputfile').each(function () {
			var $input = $(this),
				$label = $input.next('label'),
				labelVal = $label.html();

			$input.on('change', function (e) {
				var fileName = '';

				if (this.files && this.files.length > 1)
					fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);

				else if (e.target.value)
					fileName = e.target.value.split('\\').pop();

				if (fileName)
					$label.find('span').html(fileName);
				else
					$label.html(labelVal);

				$("#subirAudioVisible").removeClass("none")
				$("#tablaLista").addClass("none")
				$("#tablaListaConData").addClass("none")
				$("#tablaListaAudio").removeClass("none")

			});

			// Firefox bug fix
			$input
				.on('focus', function () { $input.addClass('has-focus'); })
				.on('blur', function () { $input.removeClass('has-focus'); });
		});
	}
	buscarVentasAudiosPendientes() {

		$("#tablaLista").removeClass("none")
		$("#subirAudioVisible").addClass("none")
		$("#tablaListaAudio").addClass("none")
		var status_seleccionado = $('#estadoAudio').val();
		this.filtroRestringeHoras = $('#restringeHoras').val();
		
		if(status_seleccionado == "1" || status_seleccionado == "2"){
		  // console.log("Síííííí seleccionó un status");
		   var entidad = this.order.user.entity;
		   var group = this.order.user.group;
		   var codigoVendedor = this.order.user.userId;
		   var buscarPorVendedor = "NO";

		   this.selectEstado = document.getElementById("estadoAudio");
		   this.estadoSeleccionado = this.selectEstado.value;
		   
		   this.inputTextCodigoVendedor = document.getElementById("codigoVendedor");
		   this.inputTextDniCliente = document.getElementById("dniCliente");
		   var codigoVendedorBusqueda = this.inputTextCodigoVendedor.value;
		   if (codigoVendedorBusqueda != null && codigoVendedorBusqueda.length > 0) {
		   	buscarPorVendedor = "SI";
		   	codigoVendedor = codigoVendedorBusqueda;
		   }
		   var dniCliente = this.inputTextDniCliente.value;
		   
		   var fechaInicio = "";
		   var fechaFin = "";

		   if (this.model.iniDate) {
		   	var dateFechaInicio = this.getDate(this.model.iniDate, true);
		   	fechaInicio = dateFechaInicio.toUTCString();

		   	// si es que no se selecciono fecha fin entonces seteamos la misma fecha inicio
		   	if (!this.model.finDate) {
		   		this.model.finDate = this.model.iniDate;
		   	}
		   	var dateFechaFin = this.getDate(this.model.finDate, false);
		   	fechaFin = dateFechaFin.toUTCString();
		   }


		   //alert("buscarVentasAudiosPendientes!!!!! -> entidad: " + entidad + " - group: " + group + " - estadoSeleccionado: " + this.estadoSeleccionado);
		   this.loading = true;
		   this.ventaService.getVentasByPuntoVenta(group, entidad, this.estadoSeleccionado, codigoVendedor, buscarPorVendedor, dniCliente, fechaInicio, fechaFin, this.filtroRestringeHoras).subscribe(
		   	data => this.cargarDatos(data),
		   	err => this.cargarError(err),
		   	() => this.cargaCompletada());
		}else{
		//   console.log("Noooooo seleccionó un status"); 
		   alert("Debe seleccionar el estado para realizar la búsqueda")
		}



	}
	
	buscarReporteVenta() {

		$("#tablaLista").removeClass("none")
		$("#subirAudioVisible").addClass("none")
		$("#tablaListaAudio").addClass("none")
		var status_seleccionado = $('#estadoAudio').val();
  		 
		if(status_seleccionado == "1" || status_seleccionado == "2"){
		   //console.log("Síííííí seleccionó un status");
		   var entidad = this.order.user.entity;
		   var group = this.order.user.group;
		   var codigoVendedor = this.order.user.userId;
		   var buscarPorVendedor = "NO";

		   this.selectEstado = document.getElementById("estadoAudio");
		   this.estadoSeleccionado = this.selectEstado.value;
		   
		   this.inputTextCodigoVendedor = document.getElementById("codigoVendedor");
		   this.inputTextDniCliente = document.getElementById("dniCliente");
		   var codigoVendedorBusqueda = this.inputTextCodigoVendedor.value;
		   if (codigoVendedorBusqueda != null && codigoVendedorBusqueda.length > 0) {
		   	buscarPorVendedor = "SI";
		   	codigoVendedor = codigoVendedorBusqueda;
		   }
		   var dniCliente = this.inputTextDniCliente.value;
		   
		   var fechaInicio = "";
		   var fechaFin = "";

		   if (this.model.iniDate) {
		   	var dateFechaInicio = this.getDate(this.model.iniDate, true);
		   	fechaInicio = dateFechaInicio.toUTCString();

		   	// si es que no se selecciono fecha fin entonces seteamos la misma fecha inicio
		   	if (!this.model.finDate) {
		   		this.model.finDate = this.model.iniDate;
		   	}
		   	var dateFechaFin = this.getDate(this.model.finDate, false);
		   	fechaFin = dateFechaFin.toUTCString();
		   }


		   //alert("buscarVentasAudiosPendientes!!!!! -> entidad: " + entidad + " - group: " + group + " - estadoSeleccionado: " + this.estadoSeleccionado);
		   this.loading = true;
		   this.ventaService
				.getReporteVenta(group, entidad, this.estadoSeleccionado, codigoVendedor, buscarPorVendedor, 
					dniCliente, fechaInicio, fechaFin, "EXP");
		   this.loading = false;
		}else{
		   //console.log("Noooooo seleccionó un status"); 
		   alert("Debe seleccionar el estado para realizar la búsqueda")
		}



	}

	getDate(dateYYYYMMDD, fromStartDay) {

		var iniDatetimeYYYY = dateYYYYMMDD.substring(0, 4);
		var iniDatetimeMM = dateYYYYMMDD.substring(5, 7) - 1;
		var iniDatetimeDD = dateYYYYMMDD.substring(8, 10);

		var d = null;

		if (fromStartDay) {
			d = new Date(iniDatetimeYYYY, iniDatetimeMM, iniDatetimeDD, 0, 0, 0, 0);
		} else {
			d = new Date(iniDatetimeYYYY, iniDatetimeMM, iniDatetimeDD, 23, 59, 59, 0);
		}
		return d;
	}

	loadMaxDate() {
		var maxDate = new Date();

		var month = (maxDate.getUTCMonth() + 1) + ""; //months from 1-12
		var day = maxDate.getUTCDate() + "";
		var year = maxDate.getUTCFullYear() + "";

		this.maxDatePicker = year + "-" + this.getDateFilled(month) + "-" + this.getDateFilled(day);
	}

	getDateFilled(value) {
		if (value.length == 1) {
			return "0" + value;
		}
		return value;
	}

	//glazaror funcion para habilitar/deshabilitar el datepicker de fechaFin
	selectInitDate() {

		if (this.model.iniDate) {
			this.readonlyFinDate = false;

		} else {
			this.model.finDate = "";
			this.readonlyFinDate = true;
		}
	}

	verDetalleVentaAudio(ventaId) {
		alert("orderId::: " + ventaId);
		this.router.navigate(['audiocarga/ventaaudiodetail', { params: { id: ventaId } }]);
	}

	cargarDatos(data) {
		this.data = data.responseData;
		this.loading = false;

		if (data.responseData.length == 0) {
			this.tablaVacia = true;
			$("#tablaListaConData").addClass('none');
			$("#tablaListaVacia").removeClass('none');
		} else {
			this.tablaVacia = false;
			$("#tablaListaVacia").addClass('none');
			$("#tablaListaConData").removeClass('none');
		}
	}

	cargarError(err) {
		//console.log(err);
		this.loading = false;
	}

	cargaCompletada() {
		//this.loading = false;
		//this.visible_venta = true;
	}




	goBack() {
		this.location.back();
	}

	public toInt(num: string) {
		return +num;
	}

	public sortByWordLength = (a: any) => {
		return a.city.length;
	}



	subirAudio() {

		//console.log("this.inputEl: "+this.inputEl)
	

		let audioElement = this.inputEl.nativeElement;

		//console.log("audioElement: "+audioElement)



		let file = audioElement.files[0];
		//this.orderService.uploadAudioMasivo(audioElement.files);
		this.data = [];
		var total = 0;

		if (audioElement.files.length == 0) {
			var mensajeCantidadArchivos = audioElement.files.length + " archivos";
			this.data = [
				{
					"numero": "1",
					"nombreArchivo": "No audio seleccionado",
					"mensaje": "Por favor seleccione al menos un archivo de audio en formato GSM.",
					"responseCode": 1,
					"satisfactorio": false
				}
			];
			return
		}

		for (var i = 0; i < audioElement.files.length; i += 1) {
			total += audioElement.files[i].size;
		}

		if (total > this.uploadMaximumSize) {
			var mensajeCantidadArchivos = audioElement.files.length + " archivos";
			this.data = [
				{
					"numero": "1",
					"nombreArchivo": mensajeCantidadArchivos,
					"mensaje": "El tamaño de los archivos seleccionados en total superan los 90MB permitidos. Por favor seleccione menos archivos.",
					"responseCode": 1,
					"satisfactorio": false
				}
			];
			return;
		}

		this.loading = true;
		this.orderService.uploadAudioMasivo(audioElement.files)
			.then(
				response => {
					this.data = response["responseData"];
					this.loading = false;
					this.inputEl.nativeElement.value = "";
				}
			);
	}

}