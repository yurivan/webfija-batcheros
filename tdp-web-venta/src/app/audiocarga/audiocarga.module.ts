import { NgModule }      from '@angular/core';
import { CommonModule }      from '@angular/common';
import { FormsModule } from "@angular/forms";
import { DataTableModule } from "angular2-datatable";
import { HttpModule } from "@angular/http";
import { RouterModule, Routes } from '@angular/router';

import { AudioCargaComponent }   from './audiocarga.component';
import { DataFilterPipe }   from './data-filter.pipe';

const APP_ROUTES: Routes = [
	{path: 'audiocarga', component: AudioCargaComponent}//sprint3
];

@NgModule({
  imports:      [ 
    CommonModule, 
    DataTableModule, 
    FormsModule,
    HttpModule,
	RouterModule.forRoot(APP_ROUTES)
  ],
  declarations: [ AudioCargaComponent, DataFilterPipe ],
  exports: [RouterModule, AudioCargaComponent]
})

export class AudioCargaModule { }


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/