import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common'
import { Router } from '@angular/router';

import { CustomerService } from '../services/customer.service';

import { LocalStorageService } from '../services/ls.service';

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { AgendamientoService } from '../services/agendamiento.service';



import { Agendamiento } from '../model/agendamientos';
import { Order } from '../model/order';



import { locale } from 'moment';

var $ = require('jquery');
class RequestDepartment {
	id: string
	value: any
}

@Component({
	moduleId: 'migraoutsva',
	selector: 'migraoutsva',
	templateUrl: './migraoutsva.template.html',
	providers: [DatePipe]
})



export class migraoutsvacomponent implements OnInit {
	loading = false;

	dataAgendamiento:Agendamiento
	order:Order
	departamento:string
	canal:string
	tecnologia:string

	
	  
	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService,
		private datepipe: DatePipe,
		private agendamientoService:AgendamientoService) {
		this.router = router;
		this.ls = ls;
	}

	ngOnInit() {
	
		this.dataAgendamiento = JSON.parse(localStorage.getItem("dataAgendamiento"))
		this.order = JSON.parse(localStorage.getItem("order"))
 	 
		this.departamento = this.dataAgendamiento.departamento
		this.canal = this.dataAgendamiento.canal
		this.tecnologia = this.dataAgendamiento.tecnologia

		


	// START CANAL
		let canal = this.canal
		if(canal==null){
			canal=""
		}
		let canalArray = canal.split(",");

		if (canalArray.length == 1 && "" == canalArray[0]) {
			canalArray = []; 
		}
		
		for (let indiceCampaign in canalArray) {
			let value = canalArray[indiceCampaign].trim().toUpperCase();
			canalArray[indiceCampaign] = value;
		}

		for(let item in canalArray){
			let canalValidar = this.order.user.channel
			if(canalArray[item] === canalValidar){
				//console.log("Con Agendamiento Canal")
			}
		}
		
	// FIN CANAL

	// START TECNOLOGÍA
		let tecnologia = this.tecnologia
		if(tecnologia==null){
			tecnologia=""
		}
		let tecnologiaArray = tecnologia.split(",");

		if (tecnologiaArray.length == 1 && "" == tecnologiaArray[0]) {
			tecnologiaArray = []; 
		}
		
		for (let indiceCampaign in tecnologiaArray) {
			let value = tecnologiaArray[indiceCampaign].trim().toUpperCase();
			tecnologiaArray[indiceCampaign] = value;
		}

		for(let item in tecnologiaArray){
			// let tecnologiaValidar = this.order.selectedOffering.internetTech
			let tecnologiaValidar = ""
			if(tecnologiaArray[item] === tecnologiaValidar){
				//console.log("Con Agendamiento Tecnología")
			}
		}
		
	// FIN TECNOLOGÍA


	// START DEPARTAMENTO
		let departamento = this.departamento
		if(departamento==null){
			departamento=""
		}
		
		let departamentoArray = departamento.split(",");

		if (departamentoArray.length == 1 && "" == departamentoArray[0]) {
			departamentoArray = []; 
		}
		
		for (let indiceCampaign in departamentoArray) {
			let value = departamentoArray[indiceCampaign].trim().toUpperCase();
			departamentoArray[indiceCampaign] = value;
		}

		for(let item in departamentoArray){
			// let departamentoValidar = this.order.product.department
			let departamentoValidar = ""
			if(departamentoArray[item] === departamentoValidar){
				//console.log("Con Agendamiento Departamentos")
			}
		}
		
	// FIN DEPARTAMENTO

		


	}

	migrarOut(){
		this.router.navigate(['migraOutCondiciones']);
	}	 
	 
}