import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription }   from 'rxjs/Subscription';

import { SearchUserComponent } from '../searchUser/searchuser.component';
import { ParkComponent } from '../park/park.component';

// import { CustomerService } from '../services/customer.service';
import { OrderService } from './../services/order.service';
import { Customer } from '../model/customer';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';

import * as globals from './../services/globals';

var $ = require('jquery');

@Component({
	moduleId: 'cancelSale',
	selector: 'cancelSale',
	templateUrl: './cancelsale.template.html',
	providers: [OrderService]
})


export class CancelSaleComponent implements OnInit{

	product:any;
	offering:any;
	sva:any;
    order: Order;
    cancelList: any;
    cancelDescription: any;
    selectedOption: any;
    customReason: any;
    anotherReason: boolean;
    errorOption: boolean;
    errorCustomReason: boolean;
    errorSending: boolean;
    webOrder: any;
    loading: any;
	constructor(private router: Router, private ls: LocalStorageService, private orderService: OrderService){
		this.router = router;
		// this.customerService = customerService;  private customerService: CustomerService,
    	this.ls = ls;
    	this.orderService = orderService;
			this.loading = false;
	}
	ngOnInit(){
		var that = this;
		this.cancelList=[];
		this.webOrder = {};
		this.errorOption = false;
		this.anotherReason = false;
		this.errorCustomReason = false;
		this.errorSending = false;
		this.cancelList=this.orderService.responseCancelList();
		this.selectedOption = 0;
		$('#cancel').on('shown.bs.modal', function () {
			that.showCancelModal();
		});
	}
	isEmpty(o){
		for(var i in o){ return false;}
		return true;
	}
	showCancelModal(){
		this.order = this.ls.getData();
		//console.log(this.order);
		this.errorSending = false;
		this.errorCustomReason = false;
		this.errorOption = false;
		this.anotherReason = false;
		this.selectedOption = 0;
		(<HTMLSelectElement>document.getElementById('reason')).value="0";
		$('#reason').focus();
	}
	closeModal(){
		$('#cancel').modal('hide');
	}
	ok(){
		//console.log(this.selectedOption);
		this.errorOption = false;
		this.errorSending = false;
		this.errorCustomReason = false;
		if(this.selectedOption.id==0 || this.selectedOption == 0 || this.selectedOption.value == null){
			this.errorOption = true;
			$("#reason").focus();
		}else{
			this.errorOption = false;
			if(this.selectedOption.id == this.cancelList.length){
				if(this.customReason!=null && typeof(this.customReason) != 'undefined' && !this.isEmpty(this.customReason)){
					if(this.customReason.trim().length==0){
						this.errorCustomReason = true;
					}else{
						this.errorCustomReason = false;
						this.orderData();
					}
				}else{
					this.errorCustomReason = true;
				}
			}else{
				this.orderData();
			}
		}

	}
	sendReason(event){
		//
		this.selectedOption = event;
		if(this.selectedOption.id == this.cancelList.length){
			this.anotherReason = true;
		}else{
			this.customReason = null;
			this.anotherReason = false;
		}
		this.cancelDescription = this.selectedOption.value;
	}
	orderData(){
		//var valor: boolean;
		this.webOrder = {};
		this.order = this.ls.getData();
		//console.log("inicio",this.order);
		this.webOrder.userId = this.order.user.userId;
		this.webOrder.customer = this.order.customer;
		if(this.order.product != null && this.order.type != globals.ORDER_ALTA_NUEVA){
			this.webOrder.phone = this.order.product.phone;	
			this.webOrder.department = this.order.product.department;
			this.webOrder.province = this.order.product.province;
			this.webOrder.district = this.order.product.district;
			this.webOrder.address = this.order.product.address;
			this.webOrder.latitudeInstallation = this.order.product.coordinateY ? this.order.product.coordinateY : 0;
			this.webOrder.longitudeInstallation = this.order.product.coordinateX ? this.order.product.coordinateX : 0;
			this.webOrder.addressAdditionalInfo = this.order.product.additionalAddress ? this.order.product.additionalAddress : null;
			//console.log("product != null", this.order);
		}
		if(this.order.selectedOffering != null){
			this.webOrder.product = this.order.selectedOffering;
		}
		this.webOrder.cancelDescription = this.customReason ? this.customReason : this.cancelDescription;
		this.webOrder.sva = [];
		if(this.order.status != "C"){
			this.webOrder.type = this.order.type;
		}
		if(this.order.status == "S" || this.order.status == "R" || this.order.status == "T"){
			this.webOrder.disaffiliationWhitePagesGuide = this.order.disaffiliationWhitePagesGuide;
			this.webOrder.affiliationElectronicInvoice = this.order.affiliationElectronicInvoice;
			this.webOrder.affiliationDataProtection = this.order.affiliationDataProtection;
			this.webOrder.publicationWhitePagesGuide = this.order.publicationWhitePagesGuide;
			this.webOrder.shippingContractsForEmail = this.order.shippingContractsForEmail;
		}
		if(this.order.sva != null){
			for (let i = 0; i < this.order.sva.length; i++) {
				let sva = this.order.sva[i];
				this.webOrder.sva.push(sva.id);
			}
		}
		//localStorage.setItem('test',JSON.stringify(this.webOrder));
		
		//this.router.navigate(['/searchUser']);
		//this.closeModal();
		this.loading=true;
		
		// Sprint 7... si es que ya se tiene un id de venta generado entonces enviamos este id en la cancelacion de venta
		if (this.order.id != null && this.order.id != '') {
			this.webOrder.orderId = this.order.id;
		}
		
		this.orderService.cancelSale(this.webOrder).subscribe(
			data=> {
				//console.log(data);
				if(data.responseCode == "00"){
					this.errorSending = false;	
					this.router.navigate(['/home']);
					this.closeModal();
				}else{
					this.errorSending = true;
				}
				this.loading = false;
			},
			err => {
				this.loading = false;
					console.log(err);
			}
		);
	}
}
