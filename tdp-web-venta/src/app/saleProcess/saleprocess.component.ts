import {
	Component, OnInit, ViewChild, OnDestroy,
	Directive, forwardRef, Attribute,
	OnChanges, SimpleChanges, Input
} from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { Subscription } from 'rxjs/Subscription';
import {
	NG_VALIDATORS, Validator,
	Validators, AbstractControl, ValidatorFn
} from '@angular/forms';

import { SearchUserComponent } from '../searchUser/searchuser.component';
import { ParkComponent } from '../park/park.component';

import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { Customer } from '../model/customer';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';

import { CancelSaleComponent } from './cancelsale.component';
 

import * as globals from './../services/globals';

import { ScoringComponent } from '../scoring/scoring.component';//glazaror sprint2

// Sprint 6 - se adiciona messageservice
import { MessageService } from '../services/message.service'; // Sprint 6

import { ProgressBarService } from '../services/progressbar.service'; // Sprint 6

var $ = require('jquery');

@Component({
	moduleId: 'saleProcess',
	selector: 'saleProcess',
	templateUrl: './saleprocess.template.html',
	providers: [CustomerService, OrderService]
})


export class SaleProcessComponent implements OnInit {

	product: any;
	offering: any;
	sva: any;
	order: Order;
	prov: any;
	new: any;
	model;
	// add userData
	user;
	isTrue: boolean;
	isHere: any;
	showModal: boolean;
	showActualProduct: boolean;
	showOffering: boolean;
	showSVA: boolean;
	showContract: boolean;
	quantitySVA: number;
	subscription: Subscription;
	errorData: boolean;
	errorTelephone: boolean;
	errorMobilePhone: boolean;
	errorEmail: boolean;
	showTelephone: boolean;
	type: string;
	cancelDescription: any;
	selectedOption: any;
	customReason: any;
	anotherReason: boolean;
	errorOption: boolean;
	errorCustomReason: boolean;
	errorSending: boolean;
	loading: boolean;
	webOrder: any;
	ingresoCorreoObligatorio: boolean;


	showClientData: boolean;//glazaror sprint2 se adiciona para ocultar/mostrar cajas de texto telefono, celular y correo cuando se trata de nuevo cliente

	// Sprint 6 se adiciona message y subscription
	message: any;
	subscription2: Subscription;
	paises : any;

	constructor(private router: Router, 
		private customerService: CustomerService,
		 private location: Location, 
		 private ls: LocalStorageService,
		  private messageService: MessageService,
		  private progresBar: ProgressBarService) {
		this.router = router;
		this.customerService = customerService;
		this.location = location;
		this.ls = ls;
		this.ingresoCorreoObligatorio = false;
		// subscribe to home component messages
		this.subscription2 = this.messageService.getMessage().subscribe(message => {
			this.message = message.text;
			//alert("messageeeee: " + this.message);
			this.verificarDatosCliente(this.message);
		});
	}
	ngOnInit() {

		this.progresBar.getProgressStatus()

		

		this.webOrder = {};
		this.order = this.ls.getData();
		//console.log('order', this.order);
		// add field user
		this.user = this.order.user;
		this.showContract = false;
		this.showModal = false;
		this.errorTelephone = false;
		this.errorMobilePhone = false;
		this.errorEmail = false;
		this.errorOption = false;
		this.anotherReason = false;
		this.errorCustomReason = false;
		this.errorSending = false;
		this.loading = false;
		//JSON.parse(localStorage.getItem('order'));
		////console.log(this.order);
		this.selectedOption = 0;
		this.model = this.order.customer;

		this.verificarClientData();//glazaror sprint2
		
		//var paises = this.customerService.getPaises();// Sprint 7
		
		// Sprint 7
		this.customerService.getPaises()
			.subscribe(
				data => {
					this.paises = data;
					//alert(data.responseMessage);
					
				},
				err => {
					console.log(err)
				}
		);

		this.callData();
		this.subscription = this.ls.order$.subscribe(data => {
			if (!this.isEmpty(data)) {
				this.order = data;
				this.callData();
				////console.log(this.order);
			}
		});
		this.router.events.subscribe((event: Event) => {
			if (event instanceof NavigationEnd) {
				if (event.url == "/saleProcess" || event.url == "/saleProcess/park" ||
					event.url == "/saleProcess/offering" || event.url == "/saleProcess/sva" ||
					event.url == "/saleProcess/salesSummary" || event.url == "/saleProcess/direccion" ||
					event.url == "/saleProcess/reniec" || event.url == "/saleProcess/contract" ||
					event.url == "/saleProcess/scoring" ||//glazaror sprint2
					event.url == "/saleProcess/close" ||
					event.url == "/saleProcess/searchRuc" ) {//Sprint24 RUC
					this.isHere = event.url;
					this.verificarClientData();//glazaror sprint2
					////console.log(this.isHere);
					if (event.url == "/saleProcess") {
						if (!this.isEmpty(this.model.telephone)
							&& !this.isEmpty(this.model.mobilePhone)
							&& !this.isEmpty(this.model.email)) {
							// 	this.isTrue = true;
							// 	this.router.navigate(['saleProcess/park']);
							// Sprint 3 - se solicito siempre habilitar la edicion de datos del cliente
							this.isTrue = false;
						} else {
							this.isTrue = false;
						}
					}
				}

				if (event.url == "/saleProcess/park" || event.url == "/saleProcess/offering" ||
					event.url == "/saleProcess/sva" || event.url == "/saleProcess/salesSummary" ||
					event.url == "/saleProcess/reniec" || event.url == "/saleProcess/contract" ||
					event.url == "/saleProcess/scoring" || //glazaror sprint2
					event.url == "/saleProcess/close" || event.url == "/saleProcess/direccion" ||
					event.url == "/saleProcess/searchRuc" ) { //Sprint 24 RUC
					this.isTrue = true;
				}

				// Sprint 4 - Si es que algun dato del cliente (celular, telefono, correo) esta en blanco o nulo entonces se debe obligar al vendedor a llenar sus datos en la pantalla de contrato
				if (event.url == "/saleProcess/reniec" || event.url == "/saleProcess/salecondition") {
					// Sprint 4 - Si es que algun dato del cliente (celular, telefono, correo) esta en blanco o nulo entonces se debe obligar al vendedor a llenar sus datos en la pantalla de contrato
					if (this.isDatosClienteObligatoriosIncompleto()) {
						this.isTrue = false;
					}
				}

				// Sprint 4 - en las pantallas de contrato y cierre: los datos del cliente no deben ser editables
				if (event.url == "/saleProcess/contract" || event.url == "/saleProcess/close") {
					this.isTrue = true;
				}

				if (event.url == "/saleProcess/reniec" || event.url == "/saleProcess/contract" ||
					event.url == "/saleProcess/close") {
					this.showActualProduct = false;
					this.showOffering = false;
					this.showSVA = false;
					this.showContract = true;
				} else {
					this.showContract = false;
					if (event.url == "/saleProcess/salesSummary") {
						if (this.type == globals.ORDER_ALTA_NUEVA) {
							this.showActualProduct = false;
						} else {
							this.showActualProduct = true;
						}
						if (this.type == globals.ORDER_SVA) {
							this.showOffering = false;
						} else {
							this.showOffering = true;
						}
						if (!this.isEmpty(this.order.sva)) {
							this.showSVA = true;
						} else {
							this.showSVA = false;
						}

					}
				}
			}




 			
		});

	

	}

	//Sprint 6 - evaluar correo obligatorio
	verificarDatosCliente(condicion) {
		if (condicion == 'CORREO OBLIGATORIO') {
			this.isTrue = false;
			this.ingresoCorreoObligatorio = true;

		} else if (condicion == 'CORREO OPCIONAL') {
			this.ingresoCorreoObligatorio = false;

		} else if (condicion == 'CORREO OPCIONAL SIN EDITAR') {

			this.ingresoCorreoObligatorio = false;
			this.isTrue = true;

		} else if (condicion == 'NOTIFICACION CORREO OPCIONAL') {

			this.ingresoCorreoObligatorio = false;

		} else if (condicion == 'NOTIFICACION CORREO OBLIGATORIO') {

			this.ingresoCorreoObligatorio = true;
		}
	}

	//Sprint 4
	isDatosClienteObligatoriosIncompleto() {
		// Sprint 6 - obligamos a ingresar los datos del cliente si es que algun dato obligatorio no ha sido ingresado

		/*if ( (this.order.customer.mobilePhone == '' || this.order.customer.mobilePhone == null)
			|| (this.order.customer.email == '' || this.order.customer.email == null)
			|| (this.order.customer.telephone == '' || this.order.customer.telephone == null)
			) {*/
		//if (this.ls.getFlagDatosClienteValidos() == '1' || this.ls.getFlagDatosClienteValidos() == '2') {
		if (
			(this.order.customer.mobilePhone == '' || this.order.customer.mobilePhone == null)
		) {
			//entonces obligamos a ingresar los datos en la pantalla actual
			return true;
		}
		return false;
	}

	//glazaror sprint2
	verificarClientData() {
		//si es que la persona aun no esta registrada como cliente de telefonica entonces el dato celular esta en blanco
		/*if (this.order.customer.mobilePhone != "" || this.order.showClientData == true) {
			this.showClientData = true;
		} else {
			this.showClientData = false;
		}*/
		// Sprint 3 - se solicito pedir siempre los datos del cliente antes de mostrar la pantalla de contrato
		this.showClientData = true;

	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
	}
	callData() {
		if (this.order) {
			this.product = this.order.product;
			this.offering = this.order.selectedOffering;
			this.sva = this.order.sva;
			this.type = this.order.type;
			this.showActualProduct = false;
			this.showOffering = false;
			this.showSVA = false;
			if (!this.isEmpty(this.product) && this.type != globals.ORDER_ALTA_NUEVA && this.order.status != "R") {//&& this.type != globals.ORDER_SVA
				this.showActualProduct = true;
				// //console.log("mostrar producto",this.product,this.showActualProduct);
			} else {
				this.showActualProduct = false;
				// //console.log("no mostrar producto",this.product,this.showActualProduct);
			}
			if (!this.isEmpty(this.offering) && this.type != globals.ORDER_SVA && this.order.status != "R") {
				this.showOffering = true;
				// //console.log("mostrar oferta: ",this.order.selectedOffering,this.showOffering);
			} else {
				this.showOffering = false;
				// //console.log("no mostrar oferta: ",this.order.selectedOffering,this.showOffering);
			}
			if (!this.isEmpty(this.sva) && this.order.status != "R") {
				var count = 0;
				for (let data of this.sva) {
					if (data.selected == true) {
						count++;
					}
				}
				this.quantitySVA = count;
				////console.log(this.quantitySVA)
				this.showSVA = true;
				// //console.log("Mostrar SVA tamaño: "+this.quantitySVA,this.showSVA);
			} else {
				this.showSVA = false;
				////console.log("No mostrar SVA tamaño: "+this.quantitySVA,this.showSVA);
			}
		}
		////console.log(this.order);
	}
	validate() {
		////console.log("Inicio: "+this.model.telephone);
		if (this.isNumber(this.model.telephone) &&
			(typeof (this.model.mobilePhone) !== 'undefined' && this.model.mobilePhone !== null && !this.isEmpty(this.model.mobilePhone) && this.isNumber(this.model.mobilePhone)) &&
			(typeof (this.model.email) !== 'undefined' && this.model.email !== null && !this.isEmpty(this.model.email) && this.validateEmail(this.model.email))) {
			if (this.isEmpty(this.model.telephone) || this.model.telephone == null) {
				this.errorTelephone = false;
				if (this.model.mobilePhone.trim().length != 9) {
					////console.log("1.No tiene 9 digitos: "+this.model.mobilePhone.trim().length);
					this.errorMobilePhone = true;
					$("#mobilePhone").focus();
					this.errorEmail = this.getErrorEmail(this.model.email);
					////console.log("1.Email incorrecto: "+ this.errorEmail);
					this.showModal = false;
				} else {
					////console.log("1.Si tiene 9 digitos: "+this.model.mobilePhone.trim().length);
					this.showTelephone = false;
					this.errorMobilePhone = false;
					this.errorEmail = this.getErrorEmail(this.model.email);
					////console.log("1.Email incorrecto: "+ this.errorEmail);
					////console.log("1.Todo esta correcto");
					this.showModal = true;
				}
			} else {
				if (this.model.telephone.trim().length != 8 && this.model.telephone.trim().length != 9) {
					////console.log("1.1.No tiene 8 digitos: "+this.model.telephone.trim().length);
					this.errorTelephone = true;
					$("#telephone").focus();
					if (this.model.mobilePhone.trim().length != 9) {
						////console.log("1.1.No tiene 9 digitos: "+this.model.mobilePhone.trim().length);
						this.errorMobilePhone = true;
						$("#mobilePhone").focus();
						this.errorEmail = this.getErrorEmail(this.model.email);
						////console.log("1.1.Email incorrecto: "+ this.errorEmail);
					} else {
						////console.log("1.1.Si tiene 9 digitos: "+this.model.mobilePhone.trim().length);
						this.errorMobilePhone = false;
						this.errorEmail = this.getErrorEmail(this.model.email);
						////console.log("1.1.Email incorrecto: "+ this.errorEmail);
					}
					this.showModal = false;
				} else {
					////console.log("1.1,Tiene 8 digitos: "+this.model.telephone.trim().length);
					this.errorTelephone = false;
					if (this.model.mobilePhone.trim().length != 9) {
						////console.log("1.1.No tiene 9 digitos: "+this.model.mobilePhone.trim().length);
						this.errorMobilePhone = true;
						$("#mobilePhone").focus();
						this.errorEmail = this.getErrorEmail(this.model.email);
						////console.log("1.1.Email incorrecto: "+ this.errorEmail);
						this.showModal = false;
					} else {
						////console.log("1.1.Tiene 9 digitios: "+this.model.mobilePhone.trim().length);
						this.errorEmail = this.getErrorEmail(this.model.email);
						////console.log("1.1.Email incorrecto: "+ this.errorEmail);
						this.showTelephone = true;
						this.errorMobilePhone = false;
						this.errorTelephone = false;
						this.errorEmail = false;
						////console.log("1.1.Todo esta correcto");
						this.showModal = true;
					}
				}
			}
			//this.order.customer = this.model;
			////console.log(this.order);

			//this.ls.setData(this.order);
			//localStorage.setItem('order', JSON.stringify(this.order));

			//this.isTrue = true;

			//this.router.navigate(['saleProcess/park']);
		} else {
			if ((this.model.mobilePhone == null || this.isEmpty(this.model.mobilePhone)) &&
				!this.validateEmail(this.model.email)) {
				////console.log("Ambos campos vacios");
				this.errorMobilePhone = true;
				this.errorEmail = true;
			} else {
				if (this.model.mobilePhone == null || this.isEmpty(this.model.mobilePhone)) {
					////console.log("2.Campo celular vacio");
					this.errorMobilePhone = true;
					$("#mobilePhone").focus();
				} else {
					if (this.model.mobilePhone.trim().length != 9) {
						////console.log("2.Campo celular con menos de 9 digitos");
						this.errorMobilePhone = true;
						$("#mobilePhone").focus();
					} else {
						////console.log("2.Campo celular con 9 digitos");
						this.errorMobilePhone = false;
					}
				}
				if (!this.validateEmail(this.model.email)) {
					////console.log("2.Campo email con error");
					this.errorEmail = true;
					$("#email").focus();
				} else {
					////console.log("2.Campo email sin error")
					this.errorEmail = false;
				}
			}
			//this.showModal = false;
			if (this.errorMobilePhone || this.errorEmail) {
				this.showModal = false;
			} else {
				this.showModal = true;
			}
		}
		// Sprint 4 - si es que showModal = true entonces guardar datos del cliente
		if (this.showModal) {
			this.guardarDatosClienteFinal();
		}
	}
	update() {
		//this.order.customer = this.model;//glazaror se comenta
		this.order.status = "C";//se comenta
		this.order.customer.email = this.model.email;
		this.order.customer.mobilePhone = this.model.mobilePhone;
		this.order.customer.telephone = this.model.telephone;
		this.ls.setData(this.order);
		this.isTrue = true;
		//this.router.navigate(['saleProcess/park']); //glazaror prueba
		this.router.navigate(['saleProcess/contract']); //glazaror ahora debe cargar contract
	}
	isEmpty(o) {
		for (var i in o) { return false; }
		return true;
	}
	isNumber(o) {
		let result = false;
		if (!isNaN(o)) { result = true; }
		return result;
	}
	validateEmail(o) {
		// Sprint 6 - el correo es opcional... si es que esta vacio entonces return true
		/*if (o == null || o == "") {
			return true;
		}*/
		if (!this.ingresoCorreoObligatorio && (o == null || o == "")) {
			return true;
		}
		var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		if (!email_regex.test(o)) {
			return false;
		}
		else {
			return true;
		}
	}
	getErrorEmail(o) {
		if (!this.validateEmail(o)) {
			return true;
		} else {
			return false;
		}
	}
	exit() {
		// if(this.isHere == "/saleProcess/park" || this.isHere == "/saleProcess"){
		// 	this.router.navigate(['/searchUser']);
		// 	let order = new Order();
		// 	order.user= this.order.user;
		// 	//localStorage.setItem('order', JSON.stringify(order));
		// 	this.ls.setData(order);
		// }else
		////console.log(this.isHere, this.order.status)

		if (this.isHere == "/saleProcess/direccion") {//glazaror regresar desde la pantalla direccion a la pantalla scoring
			this.router.navigate(['/saleProcess/scoring']);
		} else {

			if (this.isHere == "/saleProcess/park") {
				this.showCancelModal();
			} else if (this.isHere == "/saleProcess/close") {
				this.router.navigate(['/home']);
				let order = new Order();
				order.user = this.order.user;
				this.ls.setData(order);
			} else {
				// this.location.back();
				switch (this.order.status) {
					case 'N':
						this.router.navigate(['/searchUser']);
						// this.order.status = "C";
						break;
					case 'C':
						this.router.navigate(['/searchUser']);
						this.order.status = "N";
						break;
					case 'D':
						if (this.order.type == globals.ORDER_ALTA_NUEVA && this.order.product) {
							this.router.navigate(['/saleProcess/direccion']);
							this.order.type = "A";
							this.order.status = "P";
						} else {
							this.router.navigate(['/saleProcess/park']);
							this.order.product = null;
							this.order.type = null;
							this.order.status = "C";
						}
						break;
					case 'P':
						/*if (this.order.type == globals.ORDER_ALTA_NUEVA) {
							this.router.navigate(['/saleProcess/direccion']);
							this.order.type = "A";
							this.order.status = "D";
						} else*/
						// {
						this.router.navigate(['/saleProcess/park']);
						this.order.product = null;
						this.order.type = null;
						this.order.status = "C";
						// }
						break;
					case 'O':
						if (this.order.type == globals.ORDER_ALTA_NUEVA) {
							this.router.navigate(['/saleProcess/offering']);
							this.order.type = "A";
							this.order.status = "D";
						} else {
							this.router.navigate(['/saleProcess/offering']);
							this.order.selectedOffering = null;
							this.order.equipment = null;
							this.order.equipmentName = null;
							this.order.status = "P";
						}
						break;
					case 'S':
						this.router.navigate(['/saleProcess/sva']);
						this.order.status = "O";
						break;
					case 'R':
						this.router.navigate(['/saleProcess/salesSummary']);
						this.order.status = "S";
						break;
					case 'T':
						this.router.navigate(['/saleProcess/reniec']);
						this.order.status = "R";
						break;
					case 'F':
						this.router.navigate(['/saleProcess/salecondition']);
						this.order.status = "C";
						break;
				}
				this.ls.setData(this.order);
			}
		}
	}
	deleteProduct() {
		this.router.navigate(['/saleProcess/park']);
		let order = new Order();
		order.status = "C";
		order.user = this.order.user;
		order.customer = this.order.customer;
		////console.log(this.order);
		//localStorage.setItem('order', JSON.stringify(order));
		this.ls.setData(order);
	}
	deleteOffering() {
		this.router.navigate(['/saleProcess/offering']);
		let order = new Order();
		order.status = "P";
		order.user = this.order.user;
		order.customer = this.order.customer;
		order.product = this.order.product;
		if (order.product.parkType == '') {
			order.type = globals.ORDER_ALTA_NUEVA;
		} else {
			order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
		}
		////console.log(this.order);
		//localStorage.setItem('order', JSON.stringify(order));
		this.ls.setData(order);

	}
	showCancelModal() {
		////console.log(this.order.status);
		if (this.order.status != 'N') {
			$('#cancel').modal('show');

		} else {
			$('#cancel').modal('hide');
		}
	}
	closeModal() {
		$('#cancel').modal('hide');
	}
	getCSSNav(order) {
		let css;
		if (order.status == 'N' || order.status == 'F') {
			css = { 'col-lg-4 col-md-5 col-sm-6': true };
		} else {
			css = { 'col-lg-3 col-md-4 col-sm-5': true };
		}
		return css;
	}
	getCSSX(order) {
		let css;
		if (order.status == 'N' || order.status == 'F') {
			css = { 'hidden': true };
		} else {
			css = { 'col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center head-bottom': true };
		}
		return css;
	}

	//sprint 4
	editarDatosCliente() {
		if (this.isHere != "/saleProcess/close" && this.isHere != "/saleProcess/contract") {
			this.isTrue = false;
			this.cambiarModoEdicionCliente(true);
		}
	}

	//sprint 4
	guardarDatosClienteFinal() {
		//validate
		this.isTrue = true;
		this.order.status = "C";//se comenta
		this.order.customer.email = this.model.email;
		this.order.customer.mobilePhone = this.model.mobilePhone;
		this.order.customer.telephone = this.model.telephone;
		
		// Sprint 7... si es que el tipo de documento es PAS, CEX entonces capturamos datos adicionales:
		// nombres, apellido paterno, apellido materno, fecha nacimiento, nacionalidad
		if (this.order.customer.documentType == 'PAS' || this.order.customer.documentType == 'CEX') {
			this.order.customer.firstName = this.model.firstName;
			this.order.customer.lastName1 = this.model.lastName1;
			this.order.customer.lastName2 = this.model.lastName2;
			this.order.customer.birthDate = this.model.birthDate;
			this.order.customer.nationality = this.model.nationality;
		}
		
		this.order.editandoCliente = false;
		this.ls.setData(this.order);
		this.isTrue = true;

	}

	cambiarModoEdicionCliente(modoEdicion) {
		this.order = this.ls.getData();
		this.order.editandoCliente = modoEdicion;
		this.ls.setData(this.order);
	}

	validarDatosClienteIngresados() {
		var mobilePhone = $('#mobilePhone')[0];
		var telephone = $('#telephone')[0];
		var email = $('#email')[0];


		if (!mobilePhone.validity.valid || !telephone.validity.valid) {

			if (mobilePhone.required && email.required) {


			} else if (mobilePhone.required && !email.required) {

			}

		} else {

		}
	}

	//sprint 4
	cancelarGrabadoDatosCliente() {
		if (this.ingresoCorreoObligatorio && !this.validateEmail(this.model.email)) {
			$("#email").focus();
			return;
		}
		this.cambiarModoEdicionCliente(false);

		// Al cancelar la edicion de los datos del cliente cambiamos el flag a true... solo en el caso de la pantalla de reniec se deben validar obligatoriamente
		if (this.isHere != "/saleProcess/reniec") {

			//seteamos como no requeridos los datos del cliente
			var mobilePhone = $('#mobilePhone')[0];
			var email = $('#email')[0];
			mobilePhone.required = false;
			email.required = false;
		}

		var customer = this.ls.getData().customer;
		this.order.customer.email = customer.email;
		this.order.customer.mobilePhone = customer.mobilePhone;
		this.order.customer.telephone = customer.telephone;

		this.model.email = customer.email;
		this.model.mobilePhone = customer.mobilePhone;
		this.model.telephone = customer.telephone;
		this.isTrue = true;
	}
}