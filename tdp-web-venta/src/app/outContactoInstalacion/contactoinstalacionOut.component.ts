import { Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, PatternValidator, Validator } from '@angular/forms';


import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { Customer } from '../model/customer';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';
import { DireccionService } from '../services/direccion.service';

import { ScoringService } from '../services/scoring.service'; // Sprint 8

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';

var $ = require('jquery');
import { ProgressBarService } from '../services/progressbar.service';

@Component({
	moduleId: 'contactoinstalacionOut',
	selector: 'contactoinstalacionOut',
	templateUrl: './contactoinstalacionOut.template.html',
	providers: [CustomerService, DireccionService, ScoringService, OrderService]
})

export class ContactoInstalacionOutComponent implements OnInit {

	model: Customer = new Customer;

	telefono: string = "";

	nombre: string = "";
	telefonoOpcional: string = ""


	newCustomer: Customer[];

	order: Order;
	modelDirectio: string = "";
	selectedType: any;
	documentNumber: any;
	isTrue = true;
	loading = false;
	previousUrl;
	errorDocumentType: boolean;
	errorDocumentNumber: boolean;


	showDni: boolean = true;
	showCarneExtranjeria: boolean = false;
	docNroDni: string;
	docNroCarnetExt: string;
	errorConexion: string;
	msgError: boolean = false;

	// Sprint 7... tipos de documento a mostrar son configurables
	showTipoDocumentoDNI: boolean = true;
	showTipoDocumentoCEX: boolean = false;
	showTipoDocumentoPAS: boolean = false;
	showTipoDocumentoRUC: boolean = false;

	showPasaporte: boolean = false;
	showRUC: boolean = false;

	/* Sprint 8 - se adicionan listas de departamentos, provincias y distritos */
	objDepart: any;
	private keyDeparts: string[];
	private departSeleccionado = 0;
	selectDepart: any;

	objProvs: any;
	showProvs = new Array();
	private keyProvs: string[];
	selectProv: any;
	private proviSeleccionado = 0;

	objDists: any;
	showDists = new Array();
	selectDistric: any;
	private keyDistrs: string[];
	private districSeleccionado = 0;

	private loadError: boolean;
	scoringDataModel: any;
	respuestaScoring: boolean;
	private mensaje_error: String;
	customer: Customer;
	scoringService: ScoringService;// Sprint 8
	direccionService: DireccionService; // Sprint 8
	//pattern: string

	closeSaleError: boolean;
	closeSaleErrorMessage: string;

	messageTelephone: string
	messageCellphone: string

	isTheTelephoneOk: boolean
	isTheCellphoneOk: boolean
	telephoneOptional: boolean
	isTheCellPhoneValidated: boolean

	constructor(private router: Router,
		private customerService: CustomerService,
		private ls: LocalStorageService,
		direccionService: DireccionService,
		scoringService: ScoringService,
		private progresBar: ProgressBarService,
		private orderService: OrderService) {

		this.router = router;
		this.customerService = customerService;
		this.ls = ls;
		this.scoringService = scoringService;// Sprint 8
		this.direccionService = direccionService; // Sprint 8
		this.nombre = ""
		this.telefono = ""
		this.telefonoOpcional = ""
		//this.pattern
		this.messageTelephone = ""
		this.messageCellphone = ""
		this.isTheTelephoneOk = false
		this.isTheCellphoneOk = false
		this.telephoneOptional = false
		this.isTheCellPhoneValidated = false
	}

	ngOnInit() {

		if(localStorage.getItem("ventaFinalizada")){
			let exitoso = localStorage.getItem("ventaFinalizada")
			if(exitoso === "ok"){
				this.router.navigate(['closeOut']);
			}
		}


		let order = JSON.parse(localStorage.getItem('order'))
		this.nombre = order.customer.firstName
		this.telefono = order.customer.mobilePhone
		this.telefonoOpcional = order.customer.telephone
		this.progresBar.getProgressStatus()
		this.selectedType = 'DNI';
		this.documentNumber = null;
		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}
		this.order = JSON.parse(localStorage.getItem('order'));
		this.docNroDni = '';

		// Sprint 7... tipos de documento a mostrar son configurables
		this.showTipoDocumentoCEX = globals.SHOW_CEX;
		this.showTipoDocumentoPAS = globals.SHOW_PAS;
		this.showTipoDocumentoRUC = globals.SHOW_RUC;

		// Sprint 8... carga de departamentos, provincias, distritos
		this.obtenerDepartamentos();
		this.obtenerProvincias();
		this.obtenerDistritos();

		// Sprint 8... verificamos valores por default ubicacion
		this.cargarValoresDefaultUbicacion();
	}

	ngAfterViewInit() {
		//Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
		//Add 'implements AfterViewInit' to the class.
		this.jqueryvalidation()

	}

	// Sprint 8... verificamos valores por default ubicacion
	cargarValoresDefaultUbicacion() {
		this.selectDepart = document.getElementById("department");
		this.selectProv = document.getElementById("city");
		this.selectDistric = document.getElementById("district");

		if (this.order.departmentScoring) {
			this.departSeleccionado = this.order.departmentScoring;
			this.proviSeleccionado = this.order.provinceScoring;
			this.listarProvincias(this.departSeleccionado);
			this.districSeleccionado = this.order.districtScoring;
			this.listarDistritos(this.departSeleccionado + this.proviSeleccionado);
			this.respuestaScoring = true;
			this.scoringDataModel = this.order.scoringDataModel;
		} else {
			//primero verificamos si es que la ubicación fue persistida en una venta anterior
			var ubicacionDefault = this.ls.getUbicacionDefault();
			if (ubicacionDefault != null) {
				this.departSeleccionado = ubicacionDefault.codigoDepartamento;
				this.proviSeleccionado = ubicacionDefault.codigoProvincia;
				this.listarProvincias(this.departSeleccionado);
				this.districSeleccionado = ubicacionDefault.codigoDistrito;
				this.listarDistritos(this.departSeleccionado + this.proviSeleccionado);

				if (this.selectDepart != null) {
					this.selectDepart.value = this.departSeleccionado;
					this.selectProv.value = this.proviSeleccionado;
					this.selectDistric.value = this.districSeleccionado;
				}
			}
		}
	}

	// Sprint 8... se copian funciones obtenerDepartamentos, obtenerProvincias, listarProvincias, obtenerDistritos, listarDistritos
	// selDepart, selCity, selDistrict, limpiarDireccion
	obtenerDepartamentos() {
		this.objDepart = this.direccionService.getDataDepartamento();
		this.keyDeparts = Object.keys(this.objDepart);
	}

	obtenerProvincias() {
		this.objProvs = this.direccionService.getDataProvincias();
	}

	listarProvincias(skuDep) {
		this.showProvs.length = 0;
		this.keyProvs = Object.keys(this.objProvs);
		this.keyProvs.forEach((item, index) => {

			if (this.objProvs[item].skuDep == skuDep) {
				this.showProvs.push(this.objProvs[item]);
			}

		});
	}

	obtenerDistritos() {
		this.objDists = this.direccionService.getDataDistritos();
	}

	listarDistritos(skuDepPro) {

		this.showDists.length = 0;
		this.keyDistrs = Object.keys(this.objDists);
		this.keyDistrs.forEach((item, index) => {

			if (this.objDists[item].skuDepPro == skuDepPro) {
				this.showDists.push(this.objDists[item]);
			}

		})

	}

	selDepart() {
		this.selectDepart = document.getElementById("department");
		this.departSeleccionado = this.selectDepart.value;
		this.listarProvincias(this.departSeleccionado);
		this.limpiarDireccion('department');
	}

	selCity() {
		this.selectProv = document.getElementById("city");
		this.proviSeleccionado = this.selectProv.value;
		this.listarDistritos(this.departSeleccionado + this.proviSeleccionado);
		this.limpiarDireccion('province');
	}

	selDistrict() {
		this.selectDistric = document.getElementById("district");
		this.districSeleccionado = this.selectDistric.value;
	}

	limpiarDireccion(tipo) {
		if ('department' == tipo) {
			this.selectProv = document.getElementById("city");
			this.selectDistric = document.getElementById("district");
			this.selectProv.value = "";
			this.selectDistric.value = "";

			this.proviSeleccionado = 0;
			this.districSeleccionado = 0;
			this.listarDistritos(0);
		} else if ('province' == tipo) {
			this.selectDistric = document.getElementById("district");
			this.selectDistric.value = "";
			this.districSeleccionado = 0;
		}
	}

	//function onChage
	onChangeSelectDocument(value) {
		if (value == "DNI") {
			this.docNroCarnetExt = '';
		}

		//this.docNroCarnetExt = '';
		this.docNroDni = '';

		switch (value) {

			case "DNI":

				this.showDni = true;
				this.showCarneExtranjeria = false;

				this.showPasaporte = false;
				this.showRUC = false;

				break;

			case "CEX":

				this.showCarneExtranjeria = true;
				this.showDni = false;

				this.showPasaporte = false;
				this.showRUC = false;
				break;

			case "PAS":

				this.showPasaporte = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showRUC = false;

				break;

			case "RUC":

				this.showRUC = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showPasaporte = false;
				break;

			default:
				this.showDni = true;
				this.showCarneExtranjeria = false;
				this.showPasaporte = false;
				this.showRUC = false;
				this.selectedType = null;
				break;
		}
	}

	onSubmit() {

		this.documentNumber = this.docNroDni || this.docNroCarnetExt;

		if (this.documentNumber) {
			this.msgError = false;
			this.loading = true;
			this.customerService.getData(this.selectedType, this.documentNumber).subscribe(data => {
				if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
					let order = new Order();
					order.user = this.order.user;
					this.customer = data.responseData
					this.customer.documentType = this.selectedType;
					this.customer.documentNumber = this.documentNumber;
					order.customer = this.customer;
					this.ls.setData(order);
					this.loading = false;
					this.evaluar();

				} else {
					let newCustomer;
					newCustomer = {
						documentType: this.selectedType,
						documentNumber: this.documentNumber,
						firstName: "",
						lastName1: "",
						lastName2: "",
						email: "",
						telephone: "",
						mobilePhone: "",

						nationality: ""
					}
					let order = new Order();
					order.status = "N";
					order.user = this.order.user;
					order.customer = newCustomer;
					this.ls.setData(order);
					this.loading = false;
					this.router.navigate(['/saleProcess/park']);//glazaror ahora se debe cargar los productos cliente directamente
				}
			},
				err => {
				//	console.log('error', err);
					this.loading = false;
					this.msgError = true;
					this.errorConexion = 'Vuelve a Intentar';
				})
		}
	}
	isEmpty(o) {
		for (var i in o) { return false; }
		return true;
	}
	isNumber(o) {
		if (!isNaN(o) && !this.isEmpty(o)) { return true; }
		return false;
	}

	// Sprint 8... copiado del componente scoring
	evaluar() {
		this.loading = true;
		this.selectDepart = document.getElementById("department");
		this.selectProv = document.getElementById("city");
		this.selectDistric = document.getElementById("district");

		var departamentoLabel = this.selectDepart[this.selectDepart.selectedIndex].innerHTML;
		var provinciaLabel = this.selectProv[this.selectProv.selectedIndex].innerHTML;
		var distritoLabel = this.selectDistric[this.selectDistric.selectedIndex].innerHTML;

		this.order = this.ls.getData();
		//guardamos el departamento, ciudad y distrito seleccionados para el scoring
		this.order.departmentScoring = this.selectDepart.value;
		this.order.provinceScoring = this.selectProv.value;
		this.order.districtScoring = this.selectDistric.value;

		this.ls.setData(this.order);

		var ubigeo = "" + this.selectDepart.value + this.selectProv.value + this.selectDistric.value;
		this.scoringService.getData(this.customer, this.order.user.userId, departamentoLabel, provinciaLabel, distritoLabel, ubigeo, this.order.user.channel)
			.subscribe(
			data => this.cargarScoringData(data),
			err => this.errorScoringData(err),
		);
	}


	// Sprint 8
	errorScoringData(err) {
		this.loading = false;
		this.loadError = true;
		this.mensaje_error = globals.ERROR_MESSAGE;

		//mensajeValidacion
		$('#myModalReniec').modal('show');
	}

	// Sprint 8
	cargarScoringData(data) {

		if (data.responseCode == -1) {
			this.mostrarError(data.responseMessage);
			return;
		}

		let selectDepartment: any;
		let selectCity: any;
		let selectDistrict: any;

		selectDepartment = document.getElementById("department");
		selectCity = document.getElementById("city");
		selectDistrict = document.getElementById("district");

		var departmentValue = selectDepartment.value;
		var cityValue = selectCity.value;
		var districtValue = selectDistrict.value;

		this.respuestaScoring = true;
		this.scoringDataModel = data;

		this.order.scoringDataModel = data;
		this.ls.setData(this.order);
		this.ls.setUbicacionDefault(departmentValue, cityValue, districtValue);

		this.loadError = false;
		this.loading = false;
	}

	// Sprint 8
	mostrarError(mensaje) {
		this.loading = false;
		this.loadError = true;
		this.mensaje_error = mensaje;
		this.respuestaScoring = false;
	}

	// Sprint 8
	cerrarVenta() {
		this.router.navigate(['closeOut']);//sprint2 ahora se debe cargar el scoring como 3ra pantalla
	}

	saveGroup() {
		this.loading = true;
		//if (this.order.user.typeFlujoTipificacion == "PRESENCIAL") {
			//this.sendTiendaVenta();
		//} else {
			let orderUpdated = JSON.parse(localStorage.getItem('order'))
			this.updateCustomer(orderUpdated);
		//}
	}

	updateCustomer = (order) => {
		this.customerService.actualizarContacto(order.customer)
			.subscribe(data => {
				this.loading = false;
				this.customerService.updateContactTdpOrder(order).subscribe(data => {
					if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
					} 
				},
				err => {
					//console.log('error', err);
					this.errorConexion = 'Vuelve a Intentar';
				})
				if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
					localStorage.setItem("order", JSON.stringify(order));
					this.router.navigate(['closeOut']);
				} else {
					let newCustomer;
					newCustomer = {
						documentType: this.selectedType,
						documentNumber: this.documentNumber,
						firstName: "",
						lastName1: "",
						lastName2: "",
						email: "",
						telephone: "",
						mobilePhone: "",

						nationality: ""
					}
				}
			},
			err => {
			//	console.log('error', err);
				this.loading = false;
				this.msgError = true;
				this.errorConexion = 'Vuelve a Intentar';
			})
	}

	validacionTel1() {
		let order = JSON.parse(localStorage.getItem('order'))
		order.customer.firstName = this.nombre
		order.customer.mobilePhone = this.telefono
		order.customer.telephone = this.telefonoOpcional

		let nombre = order.customer.firstName;
		let celular = order.customer.mobilePhone;
		let telephone = order.customer.telefonoOpcional
		let cantidad = celular.length;
		let grupo = order.user.groupPermission;

		celular = this.telefono
		telephone = this.telefonoOpcional

		if (nombre != "") {
			if (telephone != "" && telephone) {
				if (telephone.charAt(0) == '0' || telephone.charAt(0) == '9') {
					if (Number(telephone)) {
						if (telephone.length == 9) {
							if (celular != telephone) {
								//el número está bien
								let strListBlackPhone = localStorage.getItem('dataBlackListPhone');

								if (strListBlackPhone == null) {
									strListBlackPhone = "['011111111','022222222','033333333','044444444','055555555','066666666','077777777','088888888','999999999','000000000']";
								}

								if (strListBlackPhone.includes(telephone)) {
									this.messageTelephone = "Número no valido."
									this.telephoneOptional = false
									this.isTheTelephoneOk = true
								} else {
									this.telephoneOptional = true
									this.isTheTelephoneOk = false
								}
							} else {
								this.messageTelephone = "Ingrese un número diferente"
								this.isTheTelephoneOk = true
							}
						} else {
							this.messageTelephone = "Ingrese un número de 9 dígitos"
							this.isTheTelephoneOk = true
						}
					} else {
						this.messageTelephone = "Ingrese un número de teléfono válido"
						this.isTheTelephoneOk = true
					}
				} else {
					if (telephone.length > 0) {
						if (telephone.length < 9) {
							this.messageTelephone = "Debe ingresar un número de 9 dígitos"
							this.isTheTelephoneOk = true
							if (telephone.charAt(0) == '0') {
								this.messageTelephone = "El teléfono debe contar con : 0 + Ciudad + Fijo"
								this.isTheTelephoneOk = true
							} else if (telephone.charAt(0) == '9') {
								this.messageTelephone = "El número debe empezar con '9'"
								this.isTheTelephoneOk = true
							}
						} else {
							if (telephone.charAt(0) == '0') {
								this.messageTelephone = "El teléfono debe contar con : 0 + Ciudad + Fijo"
								this.isTheTelephoneOk = true
							} else if (telephone.charAt(0) == '9') {
								this.messageTelephone = "El número debe empezar con '9'"
								this.isTheTelephoneOk = true
							} else {
								this.messageTelephone = "El número debe empezar con '0' ó '9'"
								this.isTheTelephoneOk = true
							}
						}
					} else {
						this.messageTelephone = "Ingrese un número por favor"
						this.isTheTelephoneOk = true
					}
				}
			} else {
				this.isTheTelephoneOk = false
			}

			if (celular != "" && celular) {
				if (celular.charAt(0) == '9') {
					if (celular.length == 9) {
						if (telephone == "" || telephone == null) {
							let strListBlackPhone = localStorage.getItem('dataBlackListPhone');

							if (strListBlackPhone == null) {
								strListBlackPhone = "['011111111','022222222','033333333','044444444','055555555','066666666','077777777','088888888','999999999','000000000']";
							}

							if (strListBlackPhone.includes(celular)) {
								this.messageCellphone = "Número no valido."
								this.isTheCellPhoneValidated = false
								this.isTheCellphoneOk = true
							} else {
								this.isTheCellPhoneValidated = true
								this.isTheCellphoneOk = false
							}
						} else {
							if (this.telephoneOptional) {
								this.isTheCellPhoneValidated = true
								this.isTheCellphoneOk = false
							}

							let strListBlackPhone = localStorage.getItem('dataBlackListPhone');

							if (strListBlackPhone == null) {
								strListBlackPhone = "['011111111','022222222','033333333','044444444','055555555','066666666','077777777','088888888','999999999','000000000']";
							}

							if (strListBlackPhone.includes(celular)) {
								this.messageCellphone = "Número no valido."
								this.isTheCellPhoneValidated = false
								this.isTheCellphoneOk = true
							} else {
								this.isTheCellPhoneValidated = true
								this.isTheCellphoneOk = false
							}
						}
					} else {
						this.messageCellphone = "Ingrese un número de 9 dígitos"
						this.isTheCellPhoneValidated = false
						this.isTheCellphoneOk = true
					}
				} else {
					if (celular && celular.length > 0) {
						if (celular.length < 9) {
							this.messageCellphone = "El número debe empezar  con '9'"
							this.isTheCellphoneOk = true
							if (celular.charAt(0) == '9') {
								this.messageCellphone = "El número debe empezar con '9'"
								this.isTheCellphoneOk = true
							}
						} else {
							this.messageCellphone = "El número debe empezar  con '9'"
							this.isTheCellphoneOk = true
						}
					} else {
						this.messageCellphone = "Ingrese un número por favor..."
						this.isTheCellphoneOk = true
					}
				}
			} else {
				this.messageCellphone = "Debe registrar el número de celular"
				this.isTheCellphoneOk = true
			}
		} else {
			document.getElementById("alerta_registro_error").innerHTML = "Nombre inválido " + nombre;
			$("#alerta_registro_error").removeClass("none");
			$("#alerta_registro").addClass("none");
		}
		localStorage.setItem("order", JSON.stringify(order));
	}


	validacionTel() {
		this.validacionTel1()
		if (this.isTheCellPhoneValidated) {
			if (this.telefonoOpcional == "" || !this.isTheTelephoneOk) {
				let order = JSON.parse(localStorage.getItem('order'))
				this.saveGroup()
			}
		}
	}

	jqueryvalidation() {
		$(document).ready(() => {
			if ($("#name_").val() != "") {
				$("#name_").addClass("has-content")
			} else {
				$("#name_").removeClass("has-content")
			}
			if ($("#telefono").val() != "") {
				$("#telefono").addClass("has-content")
			} else {
				$("#telefono").removeClass("has-content")
			}
			if ($("#telefonoOpcional").val() != "") {
				$("#telefonoOpcional").addClass("has-content")
			} else {
				$("#telefonoOpcional").removeClass("has-content")
			}
		})
	}

	focusout(target) {
		this.focusoutById(target.id);
	}

	focusoutById(id) {
		var target = $("#" + id);
		if (target.val() != "") {
			target.addClass("has-content");
		} else {
			target.removeClass("has-content");
		}
	}



	sendTiendaVenta() {
		//parchar aquiiiiiiiii leer el webOrder del localstorage
		//let webOrder = JSON.parse(localStorage.getItem('order'))
		let webOrder = this.createWebOrder()
		var customer = webOrder.customer;
		customer.mobilePhone = this.telefono;
		customer.telephone = this.telefonoOpcional;

		this.orderService.cerrarVenta(webOrder).subscribe(
			data => {
				if (data.responseCode == '00') {
					this.order.id = data.responseData.orderId;
					this.order.cip = data.responseData.cip;
					this.order.status = "F"
					this.order.estadoEnvioHDEC = data.responseData.estadoHDEC;
					this.ls.setData(this.order);
					this.closeSaleError = false;

					// Sprint 11 - aqui se debe actualizar los datos del cliente
					let orderUpdated = JSON.parse(localStorage.getItem('order'))
					this.updateCustomer(orderUpdated);
					let codewsp = JSON.parse(localStorage.getItem('codewsp'))
					let webOrder = {
						orderId: orderUpdated.id,
						whatsapp: codewsp
					};
			
					this.orderService.actualizarEstado(webOrder).subscribe(
						data => {
						//	console.log("Data in contract component" + data)
							if (data.responseCode == '00') {
								this.closeSaleError = false;
			
								//sprint 3 - ya no se subiran audios en el flujo de venta
								this.router.navigate(['closeOut']);
							} else {
								this.closeSaleError = true;
								this.closeSaleErrorMessage = data.responseMessage;
							}
						},
						err => {
							this.loading = false;
							this.closeSaleError = true;
							alert('Error al consultar el servicio.');
						},
						() => {
							this.loading = false;
						}
					);
				} else {
					this.closeSaleError = true;
					this.closeSaleErrorMessage = data.responseMessage;
				}
			},
			err => {
				this.loading = false;
				this.closeSaleError = true;
				alert('Error al consultar el servicio.');
			},
			() => {
				this.loading = false;
			}
		);
	}

	createWebOrder() {
		this.order.cip = " "

		let webOrder = {
			userId: this.order.user.userId,
			phone: this.order.product.phone,
			department: this.order.product.department,
			province: this.order.product.province,
			district: this.order.product.district,
			address: this.order.product.address,
			latitudeInstallation: this.order.product.coordinateY ? this.order.product.coordinateY : 0,
			longitudeInstallation: this.order.product.coordinateX ? this.order.product.coordinateX : 0,
			addressAdditionalInfo: this.order.product.additionalAddress ? this.order.product.additionalAddress : '',
			sva: [],
			customer: this.order.customer,
			product: this.order.selectedOffering,
			type: this.order.type,
			disaffiliationWhitePagesGuide: this.order.disaffiliationWhitePagesGuide,
			affiliationElectronicInvoice: this.order.affiliationElectronicInvoice,
			affiliationDataProtection: this.order.affiliationDataProtection,
			publicationWhitePagesGuide: this.order.publicationWhitePagesGuide,
			shippingContractsForEmail: this.order.shippingContractsForEmail,
			originOrder: this.order.user.groupPermission,
			// Sprint 5 - campos adicionales al cerrar la venta: nombrePuntoVenta y entidad
			entidad: this.order.user.entity,
			nombrePuntoVenta: this.order.user.group,
			// Sprint 6 - condicion preventa filtro web parental
			parentalProtection: this.order.parentalProtection,
			// Sprint 9 - Nombre del producto de origen (solo servirá para flujo SVA)
			sourceProductName: this.order.product.productDescription,
			// Sprint 10 - Campania seleccionada para altas y migras
			campaniaSeleccionada: this.order.campaniaSeleccionada,
			// Sprint 13 - Envio datos normalizador hacia automatizador parte 2
			address_referencia: this.order.address_referencia,
			address_ubigeoGeocodificado: this.order.address_ubigeoGeocodificado,
			address_descripcionUbigeo: this.order.address_descripcionUbigeo,
			address_direccionGeocodificada: this.order.address_direccionGeocodificada,
			address_tipoVia: this.order.address_tipoVia,
			address_nombreVia: this.order.address_nombreVia,
			address_numeroPuerta1: this.order.address_numeroPuerta1,
			address_numeroPuerta2: this.order.address_numeroPuerta2,
			address_cuadra: this.order.address_cuadra,
			address_tipoInterior: this.order.address_tipoInterior,
			address_numeroInterior: this.order.address_numeroInterior,
			address_piso: this.order.address_piso,
			address_tipoVivienda: this.order.address_tipoVivienda,
			address_nombreVivienda: this.order.address_nombreVivienda,
			address_tipoUrbanizacion: this.order.address_tipoUrbanizacion,
			address_nombreUrbanizacion: this.order.address_nombreUrbanizacion,
			address_manzana: this.order.address_manzana,
			address_lote: this.order.address_lote,
			address_kilometro: this.order.address_kilometro,
			address_nivelConfianza: this.order.address_nivelConfianza,
			// Anthony - Sprint 15
			serviceType: this.order.product.parkType,
			cmsServiceCode: this.order.product.serviceCode,
			cmsCustomer: this.order.product.subscriber,
			//Sprint 16
			datetimeSalesCondition: this.order.datetimeSalesCondition,
			offline: this.order.offline,
			//Sprint 27 - dochoaro - tipificacion
            tipificacion: this.order.user.typeFlujoTipificacion   
		}

		return webOrder
	}

	cancelarReintentoVenta() {
		this.closeSaleError = false;
	}
}
