import { Component, OnInit } from '@angular/core';
import { VentaService } from '../services/venta.service';
import { Venta } from '../model/venta';

import { ActivatedRoute, Router } from '@angular/router';

@Component({
	moduleId: 'venta',
	selector: 'venta',
	templateUrl: './venta.template.html',
	providers: [VentaService]
})

export class VentaComponent implements OnInit{
   ventas : Venta[];
   userId : string;
   loading : boolean;
   no_data : boolean;
   
   visible_venta : boolean;

  fecha : Date;
  listaVentas : string;
  status : string;
  numDays : number;
  ventasFiltradas : Array<Venta>;
  flagBusquedaPorTransaccion : boolean = false;

  trans: string;
  dni: string;

	constructor(private ventaService : VentaService, private activeRoute : ActivatedRoute, private router: Router){

	this.activeRoute.queryParams.subscribe(params => {

      this.status = params['status'];
      this.numDays = params['numDays'];
    });

    this.ventaService = ventaService;

    this.loading = true;

    this.visible_venta = false;
    this.ventasFiltradas = [];

    let order = JSON.parse(localStorage.getItem('order'));
    this.ventaService.getData(this.numDays,order.user.userId, this.status).subscribe(
        data =>this.cargarDatos(data),
        err=> this.cargarError(err),
    () => this.cargaCompletada());
    this.userId = "";

	}



  cargaCompletada(){
  this.loading = false;
    this.visible_venta = true;
  }



  cargarDatos(data){
    this.ventas = data.responseData;
    this.loading = false;
    this.no_data = false;
    
    if (data.responseData.length == 0) {
      this.no_data = true;
    }  

     //console.log(this.ventas);
    /*if(this.ventas != null || this.ventas != undefined){
        for(let i = 0; i < this.ventas.length;i++){
      if(this.ventas[i].status == this.status){
       this.ventas[i].saleDate = new Date(this.ventas[i].saleDate);

        this.ventasFiltradas.push(this.ventas[i]);
      }
    }
    }*/
  }

  cargarError(err){
    console.log(err);
  }

	ngOnInit(){
    let ruta = this.router
    let url = ruta.url

    this.no_data = false
    
    //console.log(" ruta " + url)
  } 
  
  filterIdTransaccion(){
  //  = document.getElementById("id_transaccion").value;
  //      = document.getElementById("numeroDni").value;

       var id_transaccion_ =  this.trans
       var numeroDni_ = this.dni

      //  console.log("data"+id_transaccion_+" --- "+numeroDni_)

    if(id_transaccion_=='' && numeroDni_==''){
      alert("Debe ingresar un dato para realizar la búsqueda")
      return
    }
      
    this.loading = true;
    let order = JSON.parse(localStorage.getItem('order'));

	this.flagBusquedaPorTransaccion = true;
    this.ventaService.getDataFilter(this.numDays,order.user.userId,id_transaccion_,numeroDni_).subscribe(
      data =>this.cargarDatos(data),
      err=> this.cargarError(err),
    () => this.cargaCompletada());
 
                  
  }
  
  descargarReporte(){
    //var id_transaccion_ = document.getElementById("id_transaccion").value;
	/*var id_transaccion_ = '10';
    
    if(id_transaccion_==''){
      alert("Debe ingresar un dato para realizar la búsqueda")
      return
    }*/

    let order = JSON.parse(localStorage.getItem('order'));

	var id_transaccion_ =  this.trans;
    var numeroDni_ = this.dni;
	var statusVenta = "" + this.status;
	
	if (this.flagBusquedaPorTransaccion) {
		this.ventaService.getDataFilterReporte(this.numDays,order.user.userId,id_transaccion_,numeroDni_, statusVenta);
	} else {
		this.ventaService.getDataReporte(this.numDays,order.user.userId, this.status);
	}


    // alert("kakakaka"+id_transaccion_);
                  
  }
}
