import { Pipe, PipeTransform } from '@angular/core';
import { OfferingService } from '../services/offering.service';

@Pipe({
	name: "searchProductName",
	pure: false
})
export class SearchProduct implements PipeTransform{

	constructor(private offeringService: OfferingService){
		this.offeringService = offeringService;
	}

	transform(args, value){
		return args
				.filter(function(item){
					//console.log(item.productName);
					return item.productName.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
	}
}