import { Pipe, PipeTransform } from '@angular/core';
import { OfferingService } from '../services/offering.service';

@Pipe({
	name: "NA",
	pure: false
})
export class NA implements PipeTransform{

	constructor(private offeringService: OfferingService){
		this.offeringService = offeringService;
	}

	transform(args, value){
			return args
					.filter(function(item){
						return item.productName.toUpperCase().indexOf(value.toUpperCase()) === -1;
	          });
		}
	}
//}