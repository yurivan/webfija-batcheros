import { Pipe, PipeTransform } from '@angular/core';
import { OfferingService } from '../services/offering.service';

@Pipe({
	name: "searchCampaign",
	pure: false
})
export class SearchCampaign implements PipeTransform{

	constructor(private offeringService: OfferingService){
		this.offeringService = offeringService;
	}

	transform(args, value){
		return args
				.filter(function(item){
					return item.campaign.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
	}
}