import { Pipe, PipeTransform } from '@angular/core';
import { OfferingService } from '../services/offering.service';

@Pipe({
	name: "searchProductType",
	pure: false
})
export class SearchProductType implements PipeTransform{

	constructor(private offeringService: OfferingService){
		this.offeringService = offeringService;
	}

	transform(args, value){
		return args
				.filter(function(item){
					return item.productType.toUpperCase().indexOf(value.toUpperCase()) !== -1;
          });
	}
}