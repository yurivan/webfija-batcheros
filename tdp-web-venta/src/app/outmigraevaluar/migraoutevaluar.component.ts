import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { CustomerService } from '../services/customer.service';
import { LocalStorageService } from '../services/ls.service';

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { Order } from '../model/order';
import { OutMigraCustomer } from '../model/outMigraCustomer';

var $ = require('jquery');
@Component({
	moduleId: 'migraoutevaluar',
	selector: 'migraoutevaluar',
	templateUrl: './migraoutevaluar.template.html',
	providers: [CustomerService]
})

export class migraoutevaluarcomponent implements OnInit {
	loading = false;
	order: Order;
	inputnumberTlfCms: string;
	migraCustomer: OutMigraCustomer[];

	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService,
		private customerService: CustomerService) {
		this.router = router;
		this.ls = ls;
		this.customerService = customerService;
	}

	ngOnInit() {
		this.progresBar.getProgressStatus()
		$(".progres").prop("style", "visibility: hidden !important;");
		this.order = this.ls.getData();
	}


	migrarOut() {
		this.loading = true;
		this.migraCustomer = [];
		let _this = this;
		this.customerService.getCustomerOutWebService(this.inputnumberTlfCms).subscribe(
			data => {
				_this.migraCustomer = data.responseData;
				if (_this.migraCustomer.length > 0) {
					//_this.order.type=globals.ORDER_MEJORAR_EXPERIENCIA;
					_this.order.atis_cms=_this.inputnumberTlfCms;
					_this.ls.setData(_this.order);
					localStorage.setItem('migraCustomer', JSON.stringify(_this.migraCustomer));
					_this.router.navigate(['migraoutparque']);
				}else{
					alert("No se encotraron resultados.");
					_this.loading=false;
				}
			},
			err => {
			}
		);

	}

}