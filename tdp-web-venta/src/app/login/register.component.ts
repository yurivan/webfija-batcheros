import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';
import { User } from '../model/user';
import { Order } from '../model/order';
import { Router } from '@angular/router';
import { LocalStorageService } from '../services/ls.service';
import { AppComponent } from '../../app.component';
var $ = require('jquery');

@Component({
	moduleId: 'register',
	selector: 'register',
	templateUrl: './register.template.html'
})
export class RegisterComponent implements OnInit, AfterViewInit {
	model;
	pwdError: boolean;
	error: boolean;
	success: boolean;
	loading: boolean;
	atisValidacion: boolean;

	/**politica de contraseña */
	POLITICA_PASSWORD: boolean;
	contNumero: boolean;
	contMayusc: boolean;
	contMinusc: boolean;
	contCaractEsp: boolean;
	NocontAtis: boolean;
	mayorigualocho: boolean;
	vermodal: boolean;
	verTool: boolean;

	documents = ['DNI', 'CEX']
	documentSelected = ''

	constructor(private loginService: LoginService,
		private userService: UserService,
		private router: Router,
		private ls: LocalStorageService,
		private appComponent: AppComponent) {
		this.model = {}
		this.success = false;
		this.pwdError = false
		this.atisValidacion = false;

		this.POLITICA_PASSWORD = false;
		this.contNumero = false;
		this.contMayusc = false;
		this.contMinusc = false;
		this.contCaractEsp = false;
		this.NocontAtis = false;
		this.mayorigualocho = false;
		this.vermodal = false;
		this.verTool = false;
	}

	ngOnInit() {
		// $(document).ready(function () {

		// })


		if (localStorage.getItem('atis_login')) {
			let atisCodGet = localStorage.getItem('atis_login');
			this.model.codAtis = atisCodGet;
			this.atisValidacion = true;
		} else {
			this.atisValidacion = false;
		}


		$(document).ready(function () {

			$(".input-effect input").val("");

			$(".input-effect input").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})
			$(".paraselect select").val("");
			$(".paraselect select").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})


			//desactivar copy paste password
			const nppwd = document.getElementById('pwd');
			const nprepwd = document.getElementById('repwd');
			nppwd.onpaste = function (e) { e.preventDefault(); }
			nprepwd.onpaste = function (e) { e.preventDefault(); }


		});

	}

	onClickLogin() {
		this.router.navigate(['/login'], {});
	}

	isDatosValidos() {
		if (this.model.dni && this.model.codAtis && this.model.pwd && this.model.repwd) {
			return true;
		}
		return false;
	}


	politicaPassword() {
		this.iniciarPolitica();
		let ppatis = this.model.codAtis
		let ppwd = this.model.pwd;
		var ppwd_array = ppwd.split("");

		var patnumber = /^\d+$/;
		var patmayusc = /[A-Z]/;
		var patminusc = /[a-z]/;
		var patCaractEsp = /[@#%=&]/;
		var patAzNumCarEsp = /^[a-zA-Z0-9@#%=&]+$/;

		for (var i = 0; i < ppwd_array.length; i++) {
			
			if (!this.contNumero) {
				this.contNumero = patnumber.test(ppwd_array[i]);
			}
			if (!this.contMayusc) {
				this.contMayusc = patmayusc.test(ppwd_array[i]);
			}
			if (!this.contMinusc) {
				this.contMinusc = patminusc.test(ppwd_array[i]);
			}
			if (!this.contCaractEsp) {
				this.contCaractEsp = patCaractEsp.test(ppwd_array[i]);
			}

		}

		if (this.contCaractEsp) {
			this.contCaractEsp = patAzNumCarEsp.test(ppwd);
		}

		this.NocontAtis = !ppwd.includes(ppatis);
		this.mayorigualocho = ppwd_array.length > 7 ? true : false;


		if (this.contNumero && this.contMayusc && this.contMinusc && this.NocontAtis && this.mayorigualocho && this.contCaractEsp) {
			this.POLITICA_PASSWORD = true;
		} else {
			this.vermodal = true;
		}

		return this.vermodal;
	}


	iniciarPolitica() {
		this.POLITICA_PASSWORD = false;
		this.contNumero = false;
		this.contMayusc = false;
		this.contMinusc = false;
		this.contCaractEsp = false;
		this.NocontAtis = false;
		this.mayorigualocho = false;
		this.vermodal = false;
	}

	cerraModal() {
		this.vermodal = false;
	}




	doRegister() {

		//validamos que todos los datos se encuentren llenados
		if (!this.isDatosValidos()) {
			return;
		}
		this.error = false;
		if (this.model.pwd == this.model.repwd) {
			/**Validar Políticas de password */
			if (this.politicaPassword()) {
				return;
			}

			this.loading = true;
			this.userService.register(this.documentSelected, this.model.dni, this.model.codAtis, this.model.pwd)
				.subscribe(response => {
					if (response.responseCode == '0') {
						this.success = true;
						localStorage.setItem("dni", this.model.dni)
						localStorage.setItem("atis", this.model.codAtis);
						localStorage.setItem("pwd", this.model.pwd)
						this.model.codAtis = ""
						this.model.pwd = ""
						this.model.repwd = ""
						if (localStorage.getItem("atis_login")) {
							localStorage.removeItem("atis_login");
						}
					} else {
						this.error = true;
						setTimeout(() => { this.error = false; }, 2000)
					}
				}, err => {
					this.loading = false;
					this.error = true;
				}, () => {
					this.loading = false;
				});
		} else {
			//alert("No coinciden las contraseñas")
			this.pwdError = true
			setTimeout(() => { this.pwdError = false; }, 2000)
			this.loading = false
		}

	}



	mostrarTool() {
		this.verTool = true;
		this.setPosicionTool();
	}

	ocultarTool() {
		this.verTool = false;
	}


	setPosicionTool() {
		/** Método para posicionar el toolTip (!) según el zoom*/
		var browserZoomLevel = Math.round(window.devicePixelRatio * 100);

		if (browserZoomLevel > 120) {
			$("#apwd").css({ top: '50%', left: '60%', position: 'absolute' });
		} else if (browserZoomLevel > 115) {
			$("#apwd").css({ top: '50%', left: '60%', position: 'absolute' });
		} else if (browserZoomLevel > 110) {
			$("#apwd").css({ top: '50%', left: '60%', position: 'absolute' });
		} else if (browserZoomLevel > 105) {
			$("#apwd").css({ top: '50%', left: '59%', position: 'absolute' });
		} else if (browserZoomLevel > 100) {
			$("#apwd").css({ top: '50%', left: '58%', position: 'absolute' });
		} else if (browserZoomLevel > 95) {
			$("#apwd").css({ top: '50%', left: '58%', position: 'absolute' });
		} else if (browserZoomLevel > 90) {
			$("#apwd").css({ top: '50%', left: '59%', position: 'absolute' });
		} else if (browserZoomLevel > 85) {
			$("#apwd").css({ top: '50%', left: '57.2%', position: 'absolute' });
		} else if (browserZoomLevel > 80) {
			$("#apwd").css({ top: '50%', left: '56%', position: 'absolute' });
		} else if (browserZoomLevel > 75) {
			$("#apwd").css({ top: '50%', left: '56%', position: 'absolute' });
		} else if (browserZoomLevel > 70) {
			$("#apwd").css({ top: '50%', left: '56%', position: 'absolute' });
		}

	}



	ngAfterViewInit() {
		/** Posicionar el elemento toolTip (!) al cargar página */
		this.setPosicionTool();
	}









}
