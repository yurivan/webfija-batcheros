import { Component, OnInit, AfterViewInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';
import { User } from '../model/user';
import { Order } from '../model/order';
import { Router } from '@angular/router';
import { LocalStorageService } from '../services/ls.service';
import { AppComponent } from '../../app.component';
var $ = require('jquery');

@Component({
	moduleId: 'reset',
	selector: 'reset',
	templateUrl: './reset.template.html'
})
export class ResetComponent implements OnInit, AfterViewInit {
	model;
	error: boolean;
	success: boolean;
	loading: boolean;
	errorMessage: string = '';
	pwdError: boolean;
	errorPWD: boolean
	errorRPWD: boolean
	buttonClass: any

	/**politica de contraseña */
	POLITICA_PASSWORD: boolean;
	contNumero: boolean;
	contMayusc: boolean;
	contMinusc: boolean;
	contCaractEsp: boolean;
	NocontAtis: boolean;
	mayorigualocho:boolean;
	vermodal:boolean;
	verTool:boolean;

	constructor(private loginService: LoginService,
		private userService: UserService,
		private router: Router,
		private ls: LocalStorageService, private appComponent: AppComponent) {
		this.model = {}
		this.success = false;
		this.pwdError = false
		this.errorPWD = false
		this.errorRPWD = false
		this.buttonClass = { boton_graytdp: true, wd240: true }

		this.POLITICA_PASSWORD = false;
		this.contNumero = false;
	    this.contMayusc = false;
	    this.contMinusc = false;
	    this.contCaractEsp = false;
	    this.NocontAtis = false;
	    this.mayorigualocho = false;
		this.vermodal = false;
		this.verTool = false;

	}

	ngOnInit() {
		$(document).ready(function () {
			$(".input-effect input").val("");

			$(".input-effect input").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})
			$(".paraselect select").val("");
			$(".paraselect select").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})

			//desactivar copy paste password
			const nppwd = document.getElementById('pwd');
			const nprepwd = document.getElementById('repwd');
			nppwd.onpaste = function(e) {e.preventDefault();}
			nprepwd.onpaste = function(e) {e.preventDefault();}
						
			//$('#lblrepcon').css('margin','15px');

		})
	}




	isDatosValidos() {
		if (this.model.codAtis && this.model.pwd && this.model.repwd) {
			return true;
		}
		return false;
	}

	doReset() {
		//validamos que todos los datos se encuentren llenados
		if (!this.isDatosValidos()) {
			return;
		}

		/*
		if(this.model.repwd.length < 8 || this.model.pwd.length < 8 ){
			this.error = true;
			this.errorMessage = "Por favor, verifique que las contrase\u00F1as tengan al menos ocho (8) caracteres.";
			setTimeout(() => {
				this.error = false
			}, 2000);
            return;
		}
		*/

		if (this.model.codAtis.length > 0) {
			if (this.model.pwd == this.model.repwd) {
				/**Validar Políticas de password */
				if(this.politicaPassword()){
					return;
				}

				this.loading = true;
				this.userService.changePassword(this.model.codAtis, this.model.pwd, this.model.repwd).subscribe(
					response => {
						if (response.responseCode == '0') {
							this.success = true;
							this.model.pwd = ""
							this.model.repwd = ""
							this.model.codAtis = ""
							this.error = false
						} else {
							this.error = true;
							this.errorMessage = response.responseMessage;
							setTimeout(() => {
								this.error = false
							}, 2000);
						}
					}, err => {
						this.loading = false;
						this.error = true;
						this.errorMessage = "Ocurrió un error al restablecer la contraseña, por favor comuníquese con el administrador.";
						setTimeout(() => {
							this.error = false
						}, 2000);
					}, () => {
						this.loading = false
					}
				)

			} else {
				this.error = true;
				this.errorMessage = "Las contraseñas no coinciden.";
				setTimeout(() => {
					this.error = false
				}, 2000);
			}
		} else {
			this.error = true;
			this.errorMessage = "Por favor, verifique que el código ATIS sea el correcto.";
			setTimeout(() => {
				this.error = false
			}, 2000);
		}


	}
	/*validatePwd(pwd: HTMLInputElement) {
		console.log("pwd " + pwd)
        var tel = $("#tel").val() + "";
        var countTel = tel.length;
        if ((countTel == 7 || countTel == 9) && Number(tel)) {
            $("#telefono_tienda").removeClass("error_in");
            $("#idMsmTelefono").hide();
        } else {
            $("#telefono_tienda").addClass("error_in");
            $("#idMsmTelefono").show();
        }
	}*/

	/*onKey(event: any, field: string) {
		let value = event.target.value
		if (value.length < 8) {
			if (field == 'PWD') {
				this.errorPWD = true
			}
			if (field == 'RPWD') {
				this.errorRPWD = true
			}
			this.setClasses(false)
		} else {
			if (field == 'PWD') {
				this.errorPWD = false
			}
			if (field == 'RPWD') {
				this.errorRPWD = false
			}
			this.setClasses(true)
		}
	}

	setClasses(validation:boolean) {
		if(this.errorPWD == false && this.errorRPWD == false) {
			if(validation) {
				$('#boton').removeClass()
				$('#boton').addClass('boton_greentdp')
				$('#boton').addClass('wd240')
			}else {
				$('#boton').removeClass()
				$('#boton').addClass('boton_graytdp')
				$('#boton').addClass('wd240')
			}
		}		
	}
*/


	politicaPassword(){
		this.iniciarPolitica();
		let ppatis = this.model.codAtis
		let ppwd = this.model.pwd;
		var ppwd_array = ppwd.split("");

		var patnumber =/^\d+$/;
		var patmayusc =/[A-Z]/;
		var patminusc =/[a-z]/;
		var patCaractEsp = /[@#%=&]/;
		var patAzNumCarEsp =/^[a-zA-Z0-9@#%=&]+$/;
		
		for(var i=0; i < ppwd_array.length; i++){
			
			if(!this.contNumero){
				this.contNumero = patnumber.test(ppwd_array[i]);
			}
			if(!this.contMayusc){
				this.contMayusc = patmayusc.test(ppwd_array[i]);
			}
			if(!this.contMinusc){
				this.contMinusc = patminusc.test(ppwd_array[i]);
			}
			if(!this.contCaractEsp){
				this.contCaractEsp = patCaractEsp.test(ppwd_array[i]);
			}
			
		}

		if(this.contCaractEsp){
			this.contCaractEsp = patAzNumCarEsp.test(ppwd);
		}		

		this.NocontAtis = !ppwd.includes(ppatis);
		this.mayorigualocho = ppwd_array.length > 7 ? true : false;

		
		if(this.contNumero && this.contMayusc && this.contMinusc && this.NocontAtis && this.mayorigualocho && this.contCaractEsp){
			this.POLITICA_PASSWORD = true;
		}else{
      this.vermodal = true;
    }

		return this.vermodal;
	}


	iniciarPolitica(){
		this.POLITICA_PASSWORD = false;
		this.contNumero = false;
		this.contMayusc = false;
		this.contMinusc = false;
		this.contCaractEsp = false;
		this.NocontAtis = false;
		this.mayorigualocho = false;
		this.vermodal = false;
	}

	cerraModal(){
		this.vermodal = false;
	}



	
	mostrarTool(){
		this.verTool = true;
		this.setPosicionTool();
		}
	
		ocultarTool(){
			this.verTool = false;
		}
	
	
		setPosicionTool(){
			/** Método para posicionar el toolTip (!) según el zoom*/
			var browserZoomLevel = Math.round(window.devicePixelRatio * 100);
	
			if(browserZoomLevel > 120){
				$("#apwd").css({top: '45%', left: '60%', position:'absolute'});
			}else if(browserZoomLevel > 115){
				$("#apwd").css({top: '45%', left: '60%', position:'absolute'});
			}else if(browserZoomLevel > 110){
				$("#apwd").css({top: '45%', left: '60%', position:'absolute'});
			}else if(browserZoomLevel > 105){
				$("#apwd").css({top: '45%', left: '59%', position:'absolute'});
			}else if(browserZoomLevel > 100){
				$("#apwd").css({top: '45%', left: '58%', position:'absolute'});
			}else if(browserZoomLevel > 95){
				$("#apwd").css({top: '45%', left: '58%', position:'absolute'});
			}else if(browserZoomLevel > 90){
				$("#apwd").css({top: '45%', left: '59%', position:'absolute'});
			}else if(browserZoomLevel > 85){
				$("#apwd").css({top: '45%', left: '57.2%', position:'absolute'});
			}else if(browserZoomLevel > 80){
				$("#apwd").css({top: '46%', left: '56%', position:'absolute'});
			}else if(browserZoomLevel > 75){
				$("#apwd").css({top: '46%', left: '56%', position:'absolute'});
			}else if(browserZoomLevel > 70){
				$("#apwd").css({top: '46%', left: '56%', position:'absolute'});
			}
			
		}
		 
		 
	
	ngAfterViewInit(){
		/** Posicionar el elemento toolTip (!) al cargar página */
		this.setPosicionTool();
	}








}
