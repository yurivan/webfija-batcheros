import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';
import { BlackListService } from '../services/blacklist.service';
import { AgendamientoService } from '../services/agendamiento.service';
import { User } from '../model/user';
import { Order } from '../model/order';
import { Router } from '@angular/router';
import { LocalStorageService } from '../services/ls.service';
import { AppComponent } from '../../app.component';
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { Parameters } from '../model/parameters';

var $ = require('jquery');

@Component({
	moduleId: 'login',
	selector: 'login',
	templateUrl: './login.template.html'
})
export class LoginComponent implements OnInit {
	model: User = new User;
	error: boolean;
	loading: boolean;
	versionApp = globals.VERSION;
	// Mensaje para mostrar en el front-end
	errorMessage: string;
	showModalCancelSale: boolean = false;

	order: Order;
	canalEntidad: Parameters;
	equiv: Boolean;
	type: Boolean;
	tipificacion: String;

	captchaVal : Boolean = false;
	errorCaptcha : Boolean = false;

	constructor(private loginService: LoginService,
		private userService: UserService,
		private router: Router,
		private ls: LocalStorageService,
		private appComponent: AppComponent,
		private progresBar: ProgressBarService,
		private blacklist: BlackListService,
		private agendamiento: AgendamientoService) {
			window['verifyCallback'] = this.verifyCallback.bind(this);
		 }

	verifyCallback(response){
		console.log("Captcha" + response);
		if(response !== ""){
			this.captchaVal = true;
		}
	}

	ngOnInit() {
		this.displayRecaptcha();
		//mascarar password
		const nppwd = document.getElementById('password');
		nppwd.onpaste = function(e) {e.preventDefault();}

		this.progresBar.getProgressStatus()

		$(".noen").prop("style", "visibility: hidden !important;");



		if (localStorage != undefined) {

			if (localStorage.getItem("atis") === null) {
			} else {
				let atis = localStorage.getItem("atis")
				let password = localStorage.getItem("pwd")
				this.model.username = atis
				this.model.password = password
				if ($(".input-effect input").val() != "") {
					$(".input-effect input").addClass("has-content");
				} else {
					$(".input-effect input").removeClass("has-content");
				}


			}
		}
		$(".input-effect input").val("");

		$(".input-effect input").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})

		$(".paraselect select").val("");

		$(".paraselect select").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})



		let token = localStorage.getItem('token');
		let order = JSON.parse(localStorage.getItem('order'));
		if (token && order && order.user && order.user.name) {
			//this.router.navigate(['/home']);
			//sprint2 se debe cargar directamente el formulario de busqueda de cliente
			//this.router.navigate(['/searchUser']);

			this.blacklist.getInitial().subscribe(
				data => {
					//console.log(JSON.stringify(data.responseData)) 
					localStorage.setItem('dataBlackListPhone', JSON.stringify(data.responseData.blackList.phone));
					localStorage.setItem('paramNormalizador', JSON.stringify(data.responseData.normalizador))
					localStorage.setItem('dataOnOff', JSON.stringify(data.responseData.onOff));
					localStorage.setItem('whatsApp', JSON.stringify(data.responseData.whatsApp));
				},
				err => { },
			);

			let __this = this;

			__this.blacklist.getCanalEntidad(order.user.channel).subscribe(
				data => {
					//console.log(JSON.stringify(data.responseData))
					if (data.responseMessage == "DEFAULT") {
						data.responseData.element = order.user.entity;
					}
					localStorage.setItem('canalEntidad', JSON.stringify(data.responseData));

					__this.canalEntidad = JSON.parse(localStorage.getItem('canalEntidad'))
					//_this.equiv = _this.canalEntidad.strValue.includes("CALL")
					//_this.type = _this.canalEntidad.element.includes("OUT")
					__this.tipificacion = __this.canalEntidad.strfilter
					
					__this.userService.niveles(order.user.niveles, order.user.channel)
						.subscribe(data => {
							$(".noen").prop("style", "visibility: visible !important;");
							//console.log(data.responseData);
							//console.log("ACA");
							if (data.responseData.f_venta) {
								if (__this.tipificacion=='REMOTO OUT')
									__this.router.navigate(['principalout']);
								else
									__this.router.navigate(['acciones']);
							} else if (data.responseData.f_bandeja) {
								if (__this.tipificacion=='REMOTO OUT')
									__this.router.navigate(['audioout']);
								else
									__this.router.navigate(['audiocarga']);
							} else if (data.responseData.f_reptrack) {
								__this.router.navigate(['tracking']);
							} else if (data.responseData.f_repventa) {
								__this.router.navigate(['reportes','opCom']);
							} else {
								__this.router.navigate(['home']);
							}
						}, error => {

						});

				},
				err => { },
			);
		} /*
    		else {
	    		let order:Order = this.ls.getData();
				//console.log('log order', order);
				if (order && order.user) {
					order.user.name = null;
					order.user.lastName = null;
				}
		        this.ls.setData(order);
	    	|}
    	*/

	}

	displayRecaptcha(){
		var doc = <HTMLDivElement>document.getElementById('signup-form');
		var script = document.createElement('script');
		script.innerHTML = '';
		script.src = 'https://www.google.com/recaptcha/api.js';
		script.async = true;
		script.defer = true;
		doc.appendChild(script);
	}

	isDatosValidos() {
		if (this.model.username && this.model.password) {
			return true;
		}
		return false;
	}

	ok() {
		this.router.navigate(['register']);

	}

	registro() {
		this.router.navigate(['register']);
		if (localStorage.getItem("atis_login")) {
			localStorage.removeItem("atis_login");
		}
	}

	doLogin() {
		//validamos que todos los datos se encuentren llenados
		if(!this.captchaVal){
			this.errorCaptcha = true;
			setTimeout(() => {
				this.errorCaptcha = false;
			}, 4000)
			return;
		}
		if (!this.isDatosValidos()) {
			return;
		}

		//this.router.navigate(['/changePassword'], {});
		let _this2 = this;
		this.loading = true;
		////console.log("Eliminando localStorage");
		//localStorage.clear();
		this.loginService.login(this.model.username, this.model.password)
			.subscribe(response => {
				_this2.userService.checkUser().then(data => {
					_this2.error = false;
					_this2.order = JSON.parse(localStorage.getItem('order'));
					_this2.order.user.userId = _this2.model.username.trim();
					localStorage.setItem('order', JSON.stringify(_this2.order));
					_this2.loading = false;
					//_this2.router.navigate(['/home'], {});
					//sprint2 se debe cargar directamente el formulario de busqueda de cliente
					//_this2.router.navigate(['/searchUser']);
					//_this2.router.navigate(['/home']);
					_this2.blacklist.getInitial().subscribe(
						data => {
							//console.log(JSON.stringify(data.responseData))
							localStorage.setItem('dataBlackListPhone', JSON.stringify(data.responseData.blackList.phone));
							localStorage.setItem('paramNormalizador', JSON.stringify(data.responseData.normalizador))
							localStorage.setItem('dataOnOff', JSON.stringify(data.responseData.onOff));
							localStorage.setItem('whatsApp', JSON.stringify(data.responseData.whatsApp));
							localStorage.setItem('dataAgendamiento', JSON.stringify(data.responseData.agendamiento));

						},
						err => { },
					);
					_this2.blacklist.getCanalEntidad(response.responseData.responseData.channel).subscribe(
						data => {
							//console.log(JSON.stringify(data.responseData))
							if(data.responseMessage=="DEFAULT"){
								data.responseData.element=response.responseData.responseData.entity;
							}
							localStorage.setItem('canalEntidad', JSON.stringify(data.responseData));

							_this2.canalEntidad = JSON.parse(localStorage.getItem('canalEntidad'))
							//_this.equiv = _this.canalEntidad.strValue.includes("CALL")
							//_this.type = _this.canalEntidad.element.includes("OUT")
							_this2.tipificacion = _this2.canalEntidad.strfilter

							_this2.userService.niveles(_this2.order.user.niveles, _this2.order.user.channel)
								.subscribe(data => {
									$(".noen").prop("style", "visibility: visible !important;");
									console.log(data.responseData);
									console.log("ACA");
									if (data.responseData.f_venta) {
										if (_this2.tipificacion=='REMOTO OUT')
											_this2.router.navigate(['principalout']);
										else
											_this2.router.navigate(['acciones']);
									} else if (data.responseData.f_bandeja) {
										if (_this2.tipificacion=='REMOTO OUT')
											_this2.router.navigate(['audioout']);
										else
											_this2.router.navigate(['audiocarga']);
									} else if (data.responseData.f_reptrack) {
										_this2.router.navigate(['tracking']);
									} else if (data.responseData.f_repventa) {
										_this2.router.navigate(['reportes','opCom']);
									} else {
										_this2.router.navigate(['home']);
									}
								}, error => {

								});

						},
						err => { },
					);


					_this2.blacklist.getArgumentarios(response.responseData.responseData.channel).subscribe(
						data => {
							//console.log(JSON.stringify(data.responseData))
							localStorage.setItem('argumentarios', JSON.stringify(data.responseData));
						},
						err => { },
					);

					// _this2.agendamiento.getInitial().subscribe(
					// 	data => {
					// 		//console.log(JSON.stringify(data.responseData))
					// 		// alert("ACERCATE: "+JSON.stringify(data.responseData.agendamiento))
					// 		localStorage.setItem('dataAgendamiento', JSON.stringify(data.responseData.agendamiento));
					// 		localStorage.setItem('dataOnOff', JSON.stringify(data.responseData.onOff));
					// 	},
					// 	err => { },
					// );
					/*		
					if (response.responseData.responseData.sellerChannelEquivalentCampaign == 'OUT' && response.responseData.responseData.niveles == '1') {
						_this2.order.user.niveles=response.responseData.responseData.niveles
						localStorage.setItem('order', JSON.stringify(_this2.order));
						//Ingreso PERFIL OUT
						_this2.router.navigate(['principalout']);
						_this2.appComponent.session();
						$(".noen").prop("style", "visibility: visible !important;");
						if (localStorage.getItem("atis_login")) {
							localStorage.removeItem("atis_login");
						}
					} else if (response.responseData.responseData.sellerChannelEquivalentCampaign == 'OUT' && response.responseData.responseData.niveles == '2') {
						_this2.order.user.niveles=response.responseData.responseData.niveles
						localStorage.setItem('order', JSON.stringify(_this2.order));
						//Ingreso PERFIL OUT BANDEJA AUDIO
						_this2.router.navigate(['audioout']);
						_this2.appComponent.session();
						$(".noen").prop("style", "visibility: visible !important;");
						if (localStorage.getItem("atis_login")) {
							localStorage.removeItem("atis_login");
						}
					} else {
						_this2.router.navigate(['acciones']);
						_this2.appComponent.session();
						$(".noen").prop("style", "visibility: visible !important;");
						if (localStorage.getItem("atis_login")) {
							localStorage.removeItem("atis_login");
						}
					}*/



				}).catch(err => {
					_this2.errorMessage = "Servidor no disponible. Intente en unos minutos."
					_this2.error = true;
					_this2.loading = false;
				});
			}, err => {
				////console.log(err);
				// Obtener el json de la nueva variable global
				try {
					var errorResponse = _this2.loginService.returndataresponse();
					// this.router.navigate(['register'])
					//console.log("errorResponse " + errorResponse.responseMessage)
					//console.log("errorResponse " + errorResponse.responseCode)
					////console.log(errorResponse);
					if (errorResponse.responseCode == "4") {
						_this2.errorMessage = errorResponse.responseMessage;
						_this2.error = false;
						localStorage.setItem("atis_login", _this2.model.username);
						_this2.registro()
					} else {
						_this2.errorMessage = errorResponse.responseMessage;
						_this2.error = true;
						setTimeout(() => {
							_this2.errorMessage = "";
							_this2.error = false;
						}, 4000)
						
					}
				} catch (err) {
					_this2.errorMessage = "Servidor no disponible. Intente en unos minutos."
				}
				//_this2.error = true;
				_this2.loading = false;
			});
	}
}
