import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { CustomerService } from '../services/customer.service';
import { Customer } from '../model/customer';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';
import { DireccionService } from '../services/direccion.service';

import { ScoringService } from '../services/scoring.service'; // Sprint 8

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { CancelSaleService } from '../services/cancelsaleservice';
import { global } from '@angular/core/src/facade/lang';

var $ = require('jquery');
class RequestDepartment {
	id: string
	value: any
}
@Component({
	moduleId: 'documenttype',
	selector: 'documenttype',
	templateUrl: './documenttype.template.html',
	providers: [CustomerService, DireccionService, ScoringService]
})

export class documenttypecomponent implements OnInit {
	messageScoringError: String;

	messageScoringCod: boolean

	messageScoringErrorVisual: boolean
	
	// codinterconect: String;
	cod_int: string
	renta_Inter_Conect: string

	showCancelSaleLink: boolean = false
	newCustomer: Customer[];
	order: Order;
	modelDirectio: string = "";
	selectedType: any;
	documentNumber: any;
	isTrue = true;
	loading = false;
	previousUrl;
	errorDocumentType: boolean;
	errorDocumentNumber: boolean;

	docNroRuc:string

	showDni: boolean = true;
	showCarneExtranjeria: boolean = false;
	showOtrosCE: boolean = false;
	docNroDni: string;
	docNroCarnetExt: string;
	docNroOtrosCE: string;
	errorConexion: string;
	msgError: boolean = false;

	// Sprint 7... tipos de documento a mostrar son configurables
	showTipoDocumentoDNI: boolean = true;
	showTipoDocumentoCEX: boolean = false;
	showTipoDocumentoPAS: boolean = false;
	showTipoDocumentoRUC: boolean = false;
	showTipoDocumentoOtrosCE: boolean = false;

	showPasaporte: boolean = false;
	showRUC: boolean = false;

	/* Sprint 8 - se adicionan listas de departamentos, provincias y distritos */
	objDepart: any;
	private keyDeparts: string[];
	private departSeleccionado = 0;
	selectDepart: any;

	objProvs: any;
	showProvs = new Array();
	private keyProvs: string[];
	selectProv: any;
	private proviSeleccionado = 0;

	objDists: any;
	showDists = new Array();
	selectDistric: any;
	private keyDistrs: string[];
	private districSeleccionado = 0;

	private loadError: boolean;
	scoringDataModel: any;
	respuestaScoring: boolean;
	private mensaje_error: String;
	customer: Customer;
	scoringService: ScoringService;// Sprint 8
	direccionService: DireccionService; // Sprint 8
	code: string
	isItMigraProcess: boolean
	showUbicacionDefault: boolean;
	isAltaPura: boolean = false;

	deudaClienteMensaje: string;
	deudaClienteMensajeEstado: boolean = false;


 
	

	@ViewChild('buttonEvaluar') buttonEvaluarRef: ElementRef;

	constructor(private router: Router,
		private customerService: CustomerService,
		private ls: LocalStorageService,
		direccionService: DireccionService,
		scoringService: ScoringService,
		private progresBar: ProgressBarService,
		private cancelSaleService: CancelSaleService) {
		this.router = router;
		this.customerService = customerService;
		this.ls = ls;
		this.scoringService = scoringService;// Sprint 8
		this.direccionService = direccionService; // Sprint 8
		this.code = ''
		this.isItMigraProcess = false
		this.messageScoringCod = false
		this.deudaClienteMensaje = ""
		this.deudaClienteMensajeEstado = false
		this.messageScoringErrorVisual = false
		
	}

	ngOnInit() {
		//this.order = JSON.parse(localStorage.getItem('order'));
		this.order = this.ls.getData();
		this.docNroRuc = ""
		this.isAltaPura = false;
		switch (this.order.type) {
			case 'A':
				//todo
				this.isAltaPura = true;
				break
			case 'M'://migras
				this.isItMigraProcess = true
				break
			case 'S'://servicios adicionales (sva)
				this.isItMigraProcess = true
				break
			default:
				//todo
				break
		}

		this.progresBar.getProgressStatus()

		this.selectedType = 'DNI';
		this.documentNumber = null;
		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}

		this.docNroDni = '';
		//$('#documentType').focus();

		// Sprint 7... tipos de documento a mostrar son configurables
		this.showTipoDocumentoCEX = globals.SHOW_CEX;
		this.showTipoDocumentoPAS = globals.SHOW_PAS;
		this.showTipoDocumentoRUC = globals.SHOW_RUC;
		this.showTipoDocumentoOtrosCE = globals.SHOW_OTROSCE;
 
		this.aplicarEfectos();
	}

	aplicarEfectosChange() {

		$("#tablaDat").addClass("none");

		$(".input-effect input").val("");

		$(".input-effect input").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
		$(".paraselect select").val("");
		$(".paraselect select").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
	}

	focusout(target) {
		this.focusoutById(target.id);
	}

	focusoutById(id) {
		var target = $("#" + id);
		if (target.val() != "") {
			target.addClass("has-content");
		} else {
			target.removeClass("has-content");
		}
	}

	aplicarEfectos() {
		var _this = this;
		$(document).ready(function () {
			//$(".input-effect input").val("");
			var inputsIdSinEfecto = "";
			if (_this.order.departmentScoring) {
				inputsIdSinEfecto = "#divDocumentNumber";
				_this.focusoutById("docNroDni");
			}
			$(".input-effect:not(" + inputsIdSinEfecto + ") input").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})
			// obviamos el efecto para el tipo de documento
			// si es que tenemos una ubicacion por default entonces tambien debemos obviar el departamento, provincia y distrito
			var idsSinEfecto = "#divDocumentType";
			if (_this.showUbicacionDefault) {
				idsSinEfecto += ",#divDepartament,#divCity,#divDistrict";
			}
			$(".paraselect:not(" + idsSinEfecto + ") select").val("");
			$(".paraselect select").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})
		})
	}
  
	//function onChage
	onChangeSelectDocument(value) {
		this.messageScoringCod = false;

		if (value == "DNI") {
			this.docNroCarnetExt = '';
			this.docNroOtrosCE = '';
		} else if (value == "CEX") {
			this.docNroDni = '';
			this.docNroOtrosCE = ''
		} else {
			this.docNroCarnetExt = ''
			this.docNroDni = ''
		}


		//this.docNroCarnetExt = '';
		//this.docNroDni = '';

		switch (value) {

			case "DNI":

				this.showDni = true;
				this.showCarneExtranjeria = false;

				this.showPasaporte = false;
				this.showRUC = false;
				this.showOtrosCE = false;
				break;

			case "CEX":

				this.showCarneExtranjeria = true;
				this.showDni = false;

				this.showPasaporte = false;
				this.showRUC = false;
				this.showOtrosCE = false;

				break;

			case "Otros":

				this.showOtrosCE = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;

				this.showPasaporte = false;
				this.showRUC = false;
				break;

			case "PAS":

				this.showPasaporte = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;	
				this.showRUC = false;
				this.showOtrosCE = false;

				break;

			case "RUC":

				this.showRUC = true;
				// this.docNroRuc = "10101"
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showPasaporte = false;
				this.showOtrosCE = false;

				break;

			default:
				this.showDni = true;
				this.showCarneExtranjeria = false;
				this.showPasaporte = false;
				this.showRUC = false;
				this.showOtrosCE = false;
				this.selectedType = "DNI";
				break;
		}
	}
 
	getData() {

		this.selectedType = this.selectedType
		this.documentNumber = this.docNroDni || this.docNroCarnetExt || this.docNroOtrosCE || this.docNroRuc;

 
		let jsonDocumenType = {};
		if(this.selectedType){
			if(this.documentNumber){
				// jsonDocumenType = {
				// 	 tipo_documento: this.selectedType,
				// 	 numero_documento: this.documentNumber
				// }
				// localStorage.setItem("documenttype",JSON.stringify(jsonDocumenType))

			    this.order.customer = this.ls.getData();

				this.order.customer.documentType = this.selectedType;
				this.order.customer.documentNumber = this.documentNumber;
 
				//salvamos los datos
				this.ls.setData(this.order);

				// alert(this.order.customer.documentNumber)
			}
		}


	}
    
	guardarInfo(){
	    this.getData()
		if (this.order.customer && this.order.customer.documentType == "DNI") {
			this.router.navigate(['saleProcess/reniec']);
		} else {

			if(this.order.customer.nationality){
				this.router.navigate(['/saleProcess/salesSummary']);
				
			}else{
					this.router.navigate(['/saleProcess/datoscliente']);
				
					
			}
			// Para clientes con carne extrajeria/pasaporte/ruc redireccionamos a la pantalla de REGISTRO DE DATOS Cliente
		
		}

	} 

	

}