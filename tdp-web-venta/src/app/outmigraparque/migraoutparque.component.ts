import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { CustomerService } from '../services/customer.service';
import { LocalStorageService } from '../services/ls.service';

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { SvaService } from '../services/sva.service';
import { Order } from '../model/order';
import { OutMigraCustomer } from '../model/outMigraCustomer';
import { Jsonp } from '@angular/http';
import { Producto } from '../model/producto';
import { OutOfferingPlanta } from '../model/outOfferingPlanta';
import { Customer } from '../model/customer';

var $ = require('jquery');

@Component({
	moduleId: 'migraoutparque',
	selector: 'migraoutparque',
	templateUrl: './migraoutparque.template.html',
	providers: [SvaService]
})

export class migraoutparquecomponent implements OnInit {
	loading = false;
	migraCustomer: OutMigraCustomer[];
	migraAtis: OutMigraCustomer[];
	migraCms: OutMigraCustomer[];
	order: Order;
	offeringPlanta: OutOfferingPlanta[];

	flagAtisView: boolean;
	flagCmsView: boolean;
	showModalPark: boolean[] = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
	private prodSelected: number
	private migraParkType: string = "";
	mensajeValidacionDatosCliente: string;

	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService,
		private svaService: SvaService) {
		this.router = router;
		this.ls = ls;
		this.prodSelected = -1;
		this.mensajeValidacionDatosCliente = "";
		this.svaService=svaService;
	}

	ngOnInit() {
		this.progresBar.getProgressStatus()
		$(".progres").prop("style", "visibility: hidden !important;");

		$(document).ready(function () {

			$('.inputfile').each(function () {
				var $input = $(this),
					$label = $input.next('label'),
					labelVal = $label.html();

				$input.on('change', function (e) {
					var fileName = '';

					if (this.files && this.files.length > 1)
						fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
					else if (e.target.value)
						fileName = e.target.value.split('\\').pop();

					if (fileName)
						$label.find('span').html(fileName);
					else
						$label.html(labelVal);
				});

				// Firefox bug fix
				$input
					.on('focus', function () { $input.addClass('has-focus'); })
					.on('blur', function () { $input.removeClass('has-focus'); });
			});

			let visibility = (index) => {
				for (let i = 5; i <= 6; i++) {
					if (i == index) {
						$("#tab" + i + "").show()
					} else {
						$("#tab" + i + "").hide()
					}
				}
			}

			let elementChoosen = (id) => {
				for (let i = 5; i <= 6; i++) {
					if (i == id) {
						$("#a" + i + "").parent().addClass("active")
						if (i == 5) {
							$("#a" + i + "").parent().css({ "color": "#50535a" });
						}
						if (i == 6) {
							$("#a" + i + "").parent().css({ "color": "#5bc500" });
						}
					} else {
						$("#a" + i + "").parent().removeClass("active")
						$("#a" + i + "").parent().css({ "color": "#50535a" });
					}
				}
			}

			$("#a5").click(() => {
				visibility(5)
				elementChoosen(5)
			})
			$("#a6").click(() => {
				visibility(6)
				elementChoosen(6)
			})
			$("#tab6").hide()
		})

		this.order = this.ls.getData();
		this.order.selectedOffering = null;
		if (this.order.type == null) {
			//por default es migra
			this.order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
		}

		this.order.status = "P";
		this.ls.setData(this.order);

		this.migraCustomer = JSON.parse(localStorage.getItem("migraCustomer"))
		this.distinctParkType(this.migraCustomer);
	}

	distinctParkType(migraCustomer: OutMigraCustomer[]) {
		this.migraAtis = [];
		this.migraCms = [];

		for (let migra of migraCustomer) {
			if (migra.parkType == "ATIS") {
				this.migraAtis.push(migra)
			} else {
				this.migraCms.push(migra)
			}
		}

		if (this.migraAtis.length == 0) {
			this.flagAtisView = false;
		} else {
			this.flagAtisView = true;
		}

		if (this.migraCms.length == 0) {
			this.flagCmsView = false;
		} else {
			this.flagCmsView = true;
		}
	}

	getModal(ind) {
		this.showModalPark[ind] = true;
	}

	selectionChange(input: HTMLInputElement, ind, parkType) {
		if (this.isChecked) {
			this.prodSelected = ind;
			this.migraParkType = parkType;
		}
	}

	isChecked(input: HTMLInputElement) {
		if (input.checked === true) {
			return true
		} else {
			return false
		}
	}


	sendMigra() {
		this.loading=true;
		// Sprint 4 - Si es que los datos del cliente estan en modo edicion y estan incorrectos entonces no dejamos avanzar el flujo
		//var customer = this.ls.getData().customer;
		let hayMasDeDosPelotudo: number = 0;
		var atLeastOne = false;
		$('input[name=check]').each(function () {
			if (this.checked) {
				hayMasDeDosPelotudo++;
				atLeastOne = true;
			}
		})

		if (!atLeastOne) {
			alert("Debe elegir un producto");
			this.loading=false;
			return;
		}

		if (hayMasDeDosPelotudo > 1) {
			alert("Debe elegir sólo un producto");
			this.loading=false;
			return;
		}


		if (this.ls.getData().editandoCliente) {
			this.mensajeValidacionDatosCliente = "Celular";
			$('#modalCliente').modal('show');
			this.loading=false;
			return;
		}


		// //console.log(this.order);

		this.order.product = new Producto();
		if(this.migraParkType=="ATIS"){
			this.order.product.parkType=this.migraAtis[this.prodSelected].parkType;
			this.order.product.productDescription=this.migraAtis[this.prodSelected].productDescription;
			this.order.product.status=this.migraAtis[this.prodSelected].status;
			this.order.product.sourceType=this.migraAtis[this.prodSelected].sourceType;
			this.order.product.department=this.migraAtis[this.prodSelected].department;
			this.order.product.province=this.migraAtis[this.prodSelected].province;
			this.order.product.district=this.migraAtis[this.prodSelected].district;
			this.order.product.address=this.migraAtis[this.prodSelected].address;
			this.order.product.ubigeoCode=this.migraAtis[this.prodSelected].ubigeoCode;
			this.order.product.phone=this.migraAtis[this.prodSelected].phone;
			this.order.product.psprincipal=this.migraAtis[this.prodSelected].psprincipal;
			this.order.product.pslinea=this.migraAtis[this.prodSelected].pslinea;
			this.order.product.psinternet=this.migraAtis[this.prodSelected].psinternet;
			this.order.product.pstv=this.migraAtis[this.prodSelected].pstv;
			this.order.product.numDecos=+this.migraAtis[this.prodSelected].numDecos;
			this.order.product.rentaTotal=this.migraAtis[this.prodSelected].rentaTotal;
			this.order.product.velInter=this.migraAtis[this.prodSelected].velInter;
			//this.order.product.subscriber=this.migraCms[this.prodSelected].subscriber;
		}else{
			this.order.product.parkType=this.migraCms[this.prodSelected].parkType;
			this.order.product.productDescription=this.migraCms[this.prodSelected].productDescription;
			this.order.product.status=this.migraCms[this.prodSelected].status;
			this.order.product.sourceType=this.migraCms[this.prodSelected].sourceType;
			this.order.product.department=this.migraCms[this.prodSelected].department;
			this.order.product.province=this.migraCms[this.prodSelected].province;
			this.order.product.district=this.migraCms[this.prodSelected].district;
			this.order.product.address=this.migraCms[this.prodSelected].address;
			this.order.product.ubigeoCode=this.migraCms[this.prodSelected].ubigeoCode;
			//this.order.product.phone=this.migraCms[this.prodSelected].phone;
			this.order.product.psprincipal=this.migraCms[this.prodSelected].psprincipal;
			this.order.product.pslinea=this.migraCms[this.prodSelected].pslinea;
			this.order.product.psinternet=this.migraCms[this.prodSelected].psinternet;
			this.order.product.pstv=this.migraCms[this.prodSelected].pstv;
			this.order.product.numDecos=+this.migraCms[this.prodSelected].numDecos;
			this.order.product.rentaTotal=this.migraCms[this.prodSelected].rentaTotal;
			this.order.product.velInter=this.migraCms[this.prodSelected].velInter;
			this.order.product.subscriber=this.migraCms[this.prodSelected].subscriber;
		}
		/*if (this.order.product.parkType == "CMS") {
			this.order.product.department = this.order.product.department.substring(4, this.order.product.department.length)
			this.order.product.district = this.order.product.district.substring(4, this.order.product.district.length)
			this.order.product.province = this.order.product.province.substring(4, this.order.product.province.length)
		}*/
		this.order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
		this.order.status = "P";
		//localStorage.setItem('order', JSON.stringify(this.order));
		
		this.ls.setData(this.order);
		////console.log( this.order.status );
		//this.router.navigate( ['saleProcess/offering'] );

		localStorage.setItem('productoActual',JSON.stringify(this.order.product));
		let status = this.order.product.status;

		if (status == "SUSPENDIDO") {
			alert("El cliente no puede continuar por estar SUSPENDIDO");
		}
		else if (status == "ACTIVO") {
			let _this=this;
                //sprint 25 calculadora
			this.svaService.getOfferingPlanta(_this.order.atis_cms, _this.order.product.parkType, _this.order.type,_this.order.product).subscribe(
				data=>{
					_this.offeringPlanta=data.responseData;
					if(_this.offeringPlanta.length>0){
						localStorage.setItem('offeringPlanta',JSON.stringify(_this.offeringPlanta));
						_this.llenarCustomer(_this.offeringPlanta)
					}else{
						alert("No se encotraron resultados.");
						_this.loading=false;
					}
				},
				err=>{}
			);
			
		}
	}

	llenarCustomer(offeringPlanta: OutOfferingPlanta[]){

		this.order.customer=new Customer();
		this.order.customer.documentType=offeringPlanta[0].tipoDocumento
		this.order.customer.documentNumber=offeringPlanta[0].numeroDocumento
		this.order.customer.firstName=offeringPlanta[0].customerName;
		this.order.customer.mobilePhone=offeringPlanta[0].celular;
		this.order.customer.telephone=offeringPlanta[0].celular2;
		this.ls.setData(this.order);
		this.router.navigate(['migraoutoffering']);
	}

	llenarCustomer2(offeringPlanta: OutOfferingPlanta[]) {
		this.order.customer = new Customer();
		this.order.customer.documentType = offeringPlanta[0].tipoDocumento
		this.order.customer.documentNumber = offeringPlanta[0].numeroDocumento
		this.order.customer.firstName = offeringPlanta[0].customerName;
		this.order.customer.mobilePhone = offeringPlanta[0].celular;
		this.order.customer.telephone = offeringPlanta[0].celular2;
		this.ls.setData(this.order);
	}

	cancelCancelacionVenta(ind) {
        this.showModalPark[ind] = false;
    }

	migrarOut() {
		this.router.navigate(['migraOutOffering']);
	}

	sendSVA() {
		this.loading=true;
		let hayMasDeDosPelotudo: number = 0;
		var atLeastOne = false;
		$('input[name=check]').each(function () {
			if (this.checked) {
				hayMasDeDosPelotudo++;
				atLeastOne = true;
			}
		})

		if (!atLeastOne) {
			alert("Debe elegir un producto");
			return;
		}

		if (hayMasDeDosPelotudo > 1) {
			alert("Debe elegir sólo un producto");
			return;
		}

		if (this.prodSelected == -1) {

		}

		this.order.product = new Producto();
		if (this.migraParkType == "ATIS") {
			this.order.product.parkType = this.migraAtis[this.prodSelected].parkType;
			this.order.product.productDescription = this.migraAtis[this.prodSelected].productDescription;
			this.order.product.status = this.migraAtis[this.prodSelected].status;
			this.order.product.sourceType = this.migraAtis[this.prodSelected].sourceType;
			this.order.product.department = this.migraAtis[this.prodSelected].department;
			this.order.product.province = this.migraAtis[this.prodSelected].province;
			this.order.product.district = this.migraAtis[this.prodSelected].district;
			this.order.product.address = this.migraAtis[this.prodSelected].address;
			this.order.product.ubigeoCode = this.migraAtis[this.prodSelected].ubigeoCode;
			this.order.product.phone = this.migraAtis[this.prodSelected].phone;
			this.order.product.psprincipal = this.migraAtis[this.prodSelected].psprincipal;
			this.order.product.pslinea = this.migraAtis[this.prodSelected].pslinea;
			this.order.product.psinternet = this.migraAtis[this.prodSelected].psinternet;
			this.order.product.pstv = this.migraAtis[this.prodSelected].pstv;
			this.order.product.numDecos = +this.migraAtis[this.prodSelected].numDecos;
			this.order.product.rentaTotal = this.migraAtis[this.prodSelected].rentaTotal;
			this.order.product.velInter = this.migraAtis[this.prodSelected].velInter;
		} else {
			this.order.product.parkType = this.migraCms[this.prodSelected].parkType;
			this.order.product.productDescription = this.migraCms[this.prodSelected].productDescription;
			this.order.product.status = this.migraCms[this.prodSelected].status;
			this.order.product.sourceType = this.migraCms[this.prodSelected].sourceType;
			this.order.product.department = this.migraCms[this.prodSelected].department;
			this.order.product.province = this.migraCms[this.prodSelected].province;
			this.order.product.district = this.migraCms[this.prodSelected].district;
			this.order.product.address = this.migraCms[this.prodSelected].address;
			this.order.product.ubigeoCode = this.migraCms[this.prodSelected].ubigeoCode;
			this.order.product.psprincipal = this.migraCms[this.prodSelected].psprincipal;
			this.order.product.pslinea = this.migraCms[this.prodSelected].pslinea;
			this.order.product.psinternet = this.migraCms[this.prodSelected].psinternet;
			this.order.product.pstv = this.migraCms[this.prodSelected].pstv;
			this.order.product.numDecos = +this.migraCms[this.prodSelected].numDecos;
			this.order.product.rentaTotal = this.migraCms[this.prodSelected].rentaTotal;
			this.order.product.velInter = this.migraCms[this.prodSelected].velInter;
			this.order.product.subscriber = this.migraCms[this.prodSelected].subscriber;
		}
		//this.order.status = "P";
		//console.log(this.order);

		localStorage.setItem('productoActual', JSON.stringify(this.order.product));

		this.order.type = globals.ORDER_SVA;
		this.ls.setData(this.order);

		//console.log("t " + this.order.product.parkType)

		let status = this.order.product.status;

		if (status == "SUSPENDIDO") {
			alert("El cliente no puede continuar por estar SUSPENDIDO");
		}
        //Sprint 25
		else if (status == "ACTIVO") {
			let _this = this;
			this.svaService.getOfferingPlanta(_this.order.atis_cms, _this.order.product.parkType, _this.order.type, _this.order.product).subscribe(
				data => {
					_this.offeringPlanta = data.responseData;
					if (_this.offeringPlanta.length > 0) {
						localStorage.setItem('offeringPlanta', JSON.stringify(_this.offeringPlanta));
						_this.llenarCustomer2(_this.offeringPlanta)
						_this.router.navigate(['svaOut']);
						_this.loading = true;
					} else {
						alert("No se encotraron resultados.");
						_this.loading = false;
					}
				},
				err => { }
			);
		}
	}
}