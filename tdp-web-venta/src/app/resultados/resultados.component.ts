import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from '../services/ls.service';


// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { global } from '@angular/core/src/facade/lang';

import { ProgressBarService } from '../services/progressbar.service';
import { Lista } from '../model/lista';


var $ = require('jquery');
@Component({
	moduleId: 'resultados',
	selector: 'resultados',
	templateUrl: './resultados.template.html',
	providers: []
})

export class ResultadosComponent implements OnInit {

	isTrue = true;
	listVenta: Lista[];
	isFlagModalVenta: boolean = false;

	venta: Lista;
	svaVentas = [];

	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService) {
		this.router = router;
		this.ls = ls;
	}

	ngOnInit() {
		$('ul > #reportes').removeClass('none');
		$('ul > #Venderr').addClass('none');
		//})
		this.progresBar.getProgressStatus()
		$(".progres").prop("style", "visibility: hidden !important;");
		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}

		this.listVenta = JSON.parse(localStorage.getItem('listaVentas'));
		this.completeDay();
		//location.reload(true);	
	}

	completeDay() {
		let listaBuscada: Lista[];
		listaBuscada = [];
		for (let lista of this.listVenta) {
			if (lista.dia.length == 1) {
				lista.dia = "0" + lista.dia;
			}
			if (lista.mes.length == 1) {
				lista.mes = "0" + lista.mes;
			}
			lista.fechaCompleta = lista.dia + "/" + lista.mes + "/" + lista.anio;
			listaBuscada.push(lista);
		}
		this.listVenta = listaBuscada;
	}

	getDetailData(idventa) {
		this.venta = null;		
		for (let ventas of this.listVenta) {
			if (idventa == ventas.id) {
				this.venta = ventas;
				for (let index = 0; index < 10; index++) {
					let svaCantidad = ventas[`svaCantidad${index + 1}`]
					let svaNombre = ventas[`svaNombre${index + 1}`]
					if (svaCantidad != null && svaCantidad != "" && svaCantidad != 0) {
						this.svaVentas.push({ svaNombre, svaCantidad })
					}
				}
				this.isFlagModalVenta = true;
			}
			
			if (this.venta != null) {
				break;
			}
		}
	}

	cerrarModal() {
		this.isFlagModalVenta = false;
	}

}