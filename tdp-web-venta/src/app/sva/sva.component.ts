import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { SvaService } from '../services/sva.service';
import { Sva } from '../model/sva';
import { Order } from '../model/order';
import { Producto } from '../model/producto';
import { ProductOffering } from '../model/productOffering';
import { SearchUserComponent } from '../searchUser/searchuser.component';
import { LocalStorageService } from '../services/ls.service';
import { CustomerService } from '../services/customer.service';
import * as globals from '../services/globals';

import { MessageService } from '../services/message.service';// Sprint 6 - servicio de mensajeria
import { ProgressBarService } from '../services/progressbar.service';
import { isUndefined } from 'util';
import { Customer } from '../model/customer';
import { OfferingService } from '../services/offering.service';
var $ = require('jquery');

@Component({
    moduleId: 'sva',
    selector: 'sva',
    templateUrl: './sva.template.html',
    providers: [SvaService, CustomerService, OfferingService]
})

export class SvaComponent implements OnInit {

    private numeral: string;
    private condicional: string;

    //Begin New SVA
    sumaSvas = 0;
    sumaRegular = 0;
    sumaPromocional = 0;
    sumaContado = 0;
    productoPrecio = 0;
    productoPrecioPro = 0;
    //End New SVA

    flagSVA: boolean = false;

    dataCode: string;
    respuestaErrorSva: string;
    messageWarning: string;
    sva = [];
    svaMas = [];
    svaUni = [];
    equipamiento: any[];
    selectedSva = [];
    selectedSvaMAS = [];
    order: Order;
    productOffering: ProductOffering;
    type: string;
    orderSva: string;

    svaOffLine: boolean;
    mjsOffline: string;
    maxCountArpu: number;

    product: Producto;
    equipment: any;
    equipmentName: string;
    private loading: boolean;
    group: any;
    groupSVA: any;
    private showAlert: boolean;
    private showImgValidate: boolean;

    countDeco = 0;
    messageMaxDeco: any;
    maximunDecos: number;
    numDecosDefault: number;

    private showMessageDecos: boolean;
    private numMaxDecos: number;
    private maxAditionalDecos: number;
    hasEquipment: boolean;
    hasValue: boolean;

    costSVAL: number = 0;
    costSVAI: number = 0;
    costDSHD: number = 0;
    costDHD: number = 0;
    costBTV: number = 0;
    costBP: number = 0;
    costDVR: number = 0;
    preciopromocional: number = 0;
    precionormal: number = 0;
    countSVA: number = 0;
    private customer: Customer;
    private arpu: number
    model: any
    productOffer: ProductOffering[]
    errorCode: string
    errorMessage: string
    private showButtonSvas: boolean
    private showButtonOnlySvas: boolean
    panelFlag: boolean[];
    panelFlagMsg: string;
    panelFlagCount: number;
    campaigns: String[]
    typeProducts: String[]
    private is_product_empty: boolean
    private visible_accordion
    imagenes1: String[]
    img_upgrades: String[]
    private nameDistrict: string
    private nameProvince: string
    private nameDepart: string
    productActual: Producto;

    mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios

    resumen: any;

    correoElectronico: string;
    correoElectronicoObligatorio: boolean = false;
    defaultMapSva = new Map<string, string>();
    defaultMapSvaMAS = new Map<string, string>();
    mapSva = new Map<string, Sva>();
    mapSvaMAS = new Map<string, Sva>();
    svaU: Sva;
    answerValidationOffer = {
        responseCode: '',
        responseMessage: '',
        responseData: ''
    }

    flagMostrarActual: boolean = false;
    flagActualSVA: boolean = false;

    constructor(private router: Router,
        private svaService: SvaService,
        private ls: LocalStorageService,
        private customerService: CustomerService,
        private messageService: MessageService,
        private progresBar: ProgressBarService,
        private offeringService: OfferingService) {
        this.router = router;
        this.svaService = svaService;
        this.ls = ls;
        this.equipamiento = [];
        this.customerService = customerService;
        this.showAlert = false;
        this.showImgValidate = false;
        this.maximunDecos = -1;

        this.hasEquipment = false;
        this.showMessageDecos = false;
        this.numMaxDecos = 0;
        this.hasValue = false
        this.offeringService = offeringService
        this.is_product_empty = false
        this.visible_accordion = false
        this.imagenes1 = []
        this.img_upgrades = []
    }

    cargarServicio(data) {
        //console.log("6")
        this.productOffer = data.responseData;

        //console.log(data);
        //console.log(data.responseData);

        if (data.responseData == undefined) {
            alert(data.responseMessage);
            return false;
        }

        try {
            if (data.responseCode != 5)
                this.errorCode = data.responseCode;
        } catch (e) {
        }
        this.errorMessage = data.responseMessage;
        if (this.type == globals.ORDER_ALTA_NUEVA) {
            this.showButtonSvas = false;
        }
        if (data.responseCode && data.responseCode == "-1") {
            if (this.type !== globals.ORDER_ALTA_NUEVA) {
                this.showButtonSvas = true;
                this.showButtonOnlySvas = true;
            }
        }
        this.cargarCombos(this.productOffer);
        this.cargarImagenes(this.productOffer);
        this.loadPanelFlag();

    }

    loadPanelFlag() {
        // para que usan esto?
        this.panelFlag = []
        if (this.productOffer) {
            this.productOffer.forEach((item, index) => {
                if (index < globals.PANEL_FLAG) {
                    this.panelFlag.push(true);
                } else {
                    this.panelFlag.push(false);
                }

            });
        }

    }

    cargarCombos(productOffer: ProductOffering[]) {
        let tmpCamp: Array<string> = [];
        let tmpTP: Array<string> = [];

        if (productOffer) {
            productOffer.forEach((item, index) => {
                if (!tmpCamp.includes(item.campaign)) {
                    tmpCamp.push(item.campaign);
                }
                if (!tmpTP.includes(item.productType)) {
                    tmpTP.push(item.productType);
                }
            });
        }
        this.campaigns = tmpCamp;
        this.typeProducts = tmpTP;
    }

    cargarImagenes(pro: ProductOffering[]) {
        this.loading = false;
        if (pro) {
            if (pro.length <= 0) {
                this.is_product_empty = true;
                this.visible_accordion = false;
            } else {
                this.visible_accordion = true;
                for (let i = 0; i < pro.length; i++) {
                    this.imagenes1[i] = "abrir_accordion.png";
                    this.img_upgrades[i] = "upgrade2.png";
                }
            }
        } else {
            console.log("Error al consumir servicio");
        }
        return "";
    }

    errorCargaOffering(err) {
        console.log(err);
        this.loading = false;
        this.errorMessage = "Por favor vuelva a intentarlo";
    }

    getData() {

        //console.log("5")
        this.loading = true;
        //Sprint 12 - se adicionan parametros canal y entidad equivalentes en campanias
        this.offeringService.getData(this.customer, this.product, this.order.user.userId, this.order.user.entity,
            this.order.user.sellerChannelAtis, this.order.user.sellerSegment,
            this.order.user.sellerChannelEquivalentCampaign, this.order.user.sellerEntityEquivalentCampaign, this.order.id).subscribe(
                data => {
                    this.cargarServicio(data)
                },
                err => this.errorCargaOffering(err)
            );
    }

    ngOnInit() {

        this.progresBar.getProgressStatus()
        this.svaOffLine = false;
        this.mjsOffline = "<p>Servicios Adicionales </p>";
        var resumen = JSON.parse(localStorage.getItem('resumen'));

        if (resumen == null) {
            resumen = {
                producto: "producto",
                direccion: '',
                costo: "69",
                precio: "169"
            }
        }

        resumen.precio = 0.00;
        localStorage.setItem('resumen', JSON.stringify(resumen));

        this.progresBar.getProgressStatus()

        $("#sva_linea_").attr("style", "display:none");
        $("#sva_internet_").attr("style", "display:none");
        $("#sva_smart_").attr("style", "display:none");
        $("#sva_dvr_").attr("style", "display:none");
        $("#sva_hd_").attr("style", "display:none");
        $("#sva_tv_").attr("style", "display:none");
        $("#sva_bp_").attr("style", "display:none");

        $("#correo_card").attr("style", "display:none");
        $("#parental_card").attr("style", "display:none");
        $("#pack_verde_card").attr("style", "display:none");


        this.loading = true;
        this.orderSva = globals.ORDER_SVA;

        this.order = this.ls.getData();
        //console.log("tipo " + this.order.type)
        
        if (this.order.sva){
            this.order.sva.forEach(value => {
                if (value.svaMass) {
                    this.flagSVA = true
                }
            })
        }
      

        if (!this.order || !this.order.product || !this.order.customer) {
            this.loading = false;
            this.router.navigate(['home']);
        } else {
            this.product = this.order.product;
            this.customer = this.order.customer;
            this.arpu = Number(this.product.rentaTotal);
            this.model = (this.product);
            this.ls.setData(this.order);
            this.getData();
        }

        //console.log("offering");
        //console.log(this.order.selectedOffering);
        this.product = this.order.product;
        this.type = this.order.type;
        if (this.type != globals.ORDER_SVA) {
            this.productOffering = this.order.selectedOffering;
        } else {
            this.productOffering = null;
        }

        //condicional
        let price = 0.0;
        let promPrice = 0.0;

        let productTypeCode = "";

        let code = 0;
        let productName = "";
        let address = "";
        let installCost = 0;
        let paymentMethod = "";
        if (this.order.selectedOffering != null) {
            resumen.precio = this.order.selectedOffering.price;
            resumen.promPrecio = this.order.selectedOffering.price;

            if (this.order.type == globals.ORDER_SVA) {
                this.productoPrecio = this.order.selectedOffering.price;
                this.productoPrecioPro = this.order.selectedOffering.promPrice;
            } else {
                this.productoPrecio = +(this.order.selectedOffering.price).toFixed(2);
                this.productoPrecioPro = +(this.order.selectedOffering.promPrice).toFixed(2);
            }
            price = this.order.selectedOffering.price;
            promPrice = this.order.selectedOffering.promPrice;

            productTypeCode = this.order.selectedOffering.productTypeCode;

            code = parseInt(productTypeCode);
            productName = this.order.selectedOffering.productName;
            address = this.order.product.address;
            installCost = this.order.selectedOffering.installCost;
            paymentMethod = this.order.selectedOffering.paymentMethod;
        }

        this.numeral = productTypeCode;

        this.setInnerHTML("order_producto_priceProm", promPrice);
        if (this.order.type != 'S') {
            this.setInnerHTML("order_producto_price", price);
        } else {
            this.setInnerHTML("order_producto_price", '-');
        }

        if (this.productOffering != null) {

            this.setInnerHTML("order_producto", productName);
            this.setInnerHTML("order_producto_address", "En: " + address);
            if (installCost != null) {
                this.setInnerHTML("order_producto_installCost", "Costo de Instalación: " + installCost.toString());
            }
            this.setInnerHTML("order_producto_paymentMethod", paymentMethod);
            if (promPrice != null) {
                this.setInnerHTML("order_producto_total_prom", "S/ " + promPrice.toString());
            }
            this.setInnerHTML("order_producto_total", "S/ " + price.toString());
        }

        //console.log("SVA PRODUCTO ACTUAL: ");
        let productType: any;
        let productCode: any;
        let serviceCode: any;
        let legacyCode: any;
        let productId: any;
        let codeP: any;
        let unitP: any;
        if (this.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
            this.maximunDecos = this.productOffering.cantidaddecos;
            this.productActual = JSON.parse(localStorage.getItem('productActual'));
        } else if (this.type == globals.ORDER_ALTA_NUEVA) {
            this.maximunDecos = globals.DEFAULT_MAX_DECOS;
        }

        legacyCode = this.product.parkType;
        //Migra
        if (this.type == this.orderSva) {
            productType = String(Number(this.product.sourceType));
            serviceCode = this.product.parkType == "CMS" ? this.product.serviceCode : this.product.phone;
            productCode = this.product.productCode ? this.product.productCode : null
            productId = null;
            codeP = null;
            unitP = null;

            // Pasamos adicionalmente el canal de venta
            this.obtainSvaV2(productCode, serviceCode, productType, legacyCode, productId, codeP, unitP, this.order.user.sellerChannelEquivalentCampaign, "", legacyCode);
            //Migra
        } else if (this.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
            productType = this.productOffering.productTypeCode;
            serviceCode = this.product.parkType == "CMS" ? this.product.serviceCode : this.product.phone;
            productCode = this.productOffering.productCode;
            productId = this.productOffering.id;
            if (this.productOffering.sva != null) {
                codeP = this.productOffering.sva.code;
                unitP = this.productOffering.sva.unit;
            } else {
                codeP = "";
                unitP = "";
            }
            // Para operaciones comerciales distintas a SVAS no interesa el canal de venta... pasamos en blanco
            this.obtainSvaV2(productCode, serviceCode, productType, legacyCode, productId, codeP, unitP, this.order.user.sellerChannelEquivalentCampaign, this.order.selectedOffering.tvTech, this.order.selectedOffering['targetType']);
        } else {
            productType = this.productOffering.productTypeCode
            productId = this.productOffering.id;
            //this.obtainSva(productType, this.productOffering.altaPuraSvasBloqueTV, this.productOffering.altaPuraSvasLinea, this.productOffering.altaPuraSvasInternet, productId);
            this.obtainSva(productType, this.productOffering.altaPuraSvasBloqueTV, this.productOffering.altaPuraSvasLinea, this.productOffering.altaPuraSvasInternet, productId, this.order.selectedOffering.tvTech);
        }

        //correo electronico x default
        if (this.order.customer != null && this.order.customer.email != null && this.order.customer.email.length > 0) {
            this.correoElectronico = this.order.customer.email;
        }

        this.precionormal = (this.order.selectedOffering != null) ? this.order.selectedOffering.price : 0.00;
        this.preciopromocional = (this.order.selectedOffering != null) ? this.order.selectedOffering.promPrice : 0.00;

        this.aplicarEfectos();
        //console.log(this.order);
    }

    setInnerHTML(idElement, contenido) {
        var elemento = document.getElementById(idElement);
        if (elemento != null) {
            elemento.innerHTML = contenido.toString();
        }
    }

    aplicarEfectos() {
        $(".input-effect input").focusout(function () {
            if ($(this).val() != "") {
                $(this).addClass("has-content");
            } else {
                $(this).removeClass("has-content");
            }
        })
        $(".paraselect select").focusout(function () {
            if ($(this).val() != "") {
                $(this).addClass("has-content");
            } else {
                $(this).removeClass("has-content");
            }
        })
    }

    obtainSva(productType, svasBloqueTV, svasLinea, svasInternet, productId, tvTech) {
        this.svaService.getData_tvTech(productType, svasBloqueTV, svasLinea, svasInternet, productId, tvTech)
            .subscribe(
                data => {
                    if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
                        this.sva = data.responseData.sva
                        this.equipamiento = data.responseData.equipment;
                        this.order.equipment = data.responseData.equipment && data.responseData.equipment[0] ? data.responseData.equipment[0] : null;
                        this.order.equipmentName = this.order.equipment ? this.order.equipment.unit : '';
                        this.ls.setData(this.order);
                        this.completeData(this.sva);

                        this.obtainNumDecos("A");

                    } else {
                        alert(data.responseMessage);
                    }
                },
                err => {
                    console.log(err)
                }
            );
    }

    obtainNumDecos(type) {

        if (this.order.type == "A") {
            /*Sprint-11 */
            //Aquí debemos
            // agregar la asignación de la variable
            var arrayDecosList = localStorage.getItem('arrayDecosList');
            let productType = this.order.selectedOffering.productType;
            arrayDecosList = JSON.parse(arrayDecosList);
            if (productType.toUpperCase() === 'TRIO') {
                this.maxAditionalDecos = parseInt(arrayDecosList[0])
                //console.log("maxAditionalDecos----" + this.maxAditionalDecos)
            } else if (productType.toUpperCase() === "DUO BA") {
                this.maxAditionalDecos = parseInt(arrayDecosList[1])
                //console.log("maxAditionalDecos----" + this.maxAditionalDecos)
            } else if (productType.toUpperCase() === "DUO TV") {
                this.maxAditionalDecos = parseInt(arrayDecosList[2])
                //console.log("maxAditionalDecos----" + this.maxAditionalDecos)
            } else if (productType.toUpperCase() === "MONO BA") {
                this.maxAditionalDecos = parseInt(arrayDecosList[3])
                //console.log("maxAditionalDecos----" + this.maxAditionalDecos)
            } else if (productType.toUpperCase() === "MONO TV") {
                this.maxAditionalDecos = parseInt(arrayDecosList[4])
                //console.log("maxAditionalDecos----" + this.maxAditionalDecos)
            } else if (productType.toUpperCase() === "MONO LINEA") {
                this.maxAditionalDecos = parseInt(arrayDecosList[5])
                //console.log("maxAditionalDecos----" + this.maxAditionalDecos)
            }  //Aquí debemos agregar la asignación de la variable
            /*End Sprint-11 */

        } else {
            this.svaService.getNumDecos(type)
                .subscribe(
                    data => {
                        if (typeof data.responseData !== 'undefined' && data.responseData !== null) {

                            //console.log("numDecos");
                            //console.log(data);

                            data.responseData.forEach((item, index) => {
                                if (item.element == 'max_amount_aditional_decoders_in_vap') {
                                    this.maxAditionalDecos = item.strValue;
                                    //console.log(this.maxAditionalDecos);
                                }
                            });

                            this.loading = false;

                        } else {
                            alert(data.responseMessage);
                        }
                    },
                    err => {
                        console.log(err)
                    }
                );
        }

    }

    obtainSvaV2(productCode, serviceCode, productType, legacyCode, productId, codeP, unitP, canalVenta, tvTech, targetType) {

        //console.log(this.order);
        let xthis = this;
        this.maxAditionalDecos = 6 - this.order.product.numDecos;

        this.svaService.getDatav2(productCode, serviceCode, productType, legacyCode, productId, codeP, unitP, canalVenta, tvTech, targetType)
            .subscribe(
                data => {
                    if (typeof data.responseData !== 'undefined' && data.responseData !== null && data.responseCode == "0") {
                        xthis.sva = data.responseData.sva;
                        if (data.responseData.offLine) {
                            this.svaOffLine = data.responseData.offLine;
                            alert("Flujo Offline");
                            this.mjsOffline = '<p>Servicios Adicionales  <br> El servicio de calculadora no responde. Por favor, seleccione los SVA que desea el cliente </p>';
                            if (this.order.offline.flag.indexOf('5') == -1) {
                                this.order.offline.flag = this.order.offline.flag + (this.order.offline.flag ? ',5' : '5');
                            }
                        }
                        console.log(data.responseData.sva)
                        xthis.maxCountArpu = +data.responseData.sva[0].maxDecos;
                        xthis.equipamiento = data.responseData.equipment;
                        if (xthis.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                            xthis.order.equipment = data.responseData.equipment && data.responseData.equipment[0] ? data.responseData.equipment[0] : null;
                            xthis.order.equipmentName = xthis.order.equipment ? xthis.order.equipment.unit : '';
                            xthis.flagActualSVA = true;
                        } else if (xthis.type == globals.ORDER_SVA) {
                            xthis.equipment = data.responseData.equipment && data.responseData.equipment[0] ? data.responseData.equipment[0] : null;
                            xthis.equipmentName = xthis.order.equipment ? xthis.order.equipment.unit : '';
                        }

                        xthis.ls.setData(this.order);
                        xthis.completeData(this.sva);

                        //xthis.loading = false;
                    } else {
                        //xthis.loading = false;
                        xthis.respuestaErrorSva = data.responseMessage;
                    }
                    xthis.svaService.getAgregarSva(productCode, serviceCode, productType, legacyCode, productId, codeP, unitP, canalVenta,tvTech)
                        .subscribe(
                            data => {
                                if (typeof data.responseData !== 'undefined' && data.responseData !== null && data.responseCode == "0") {
                                    console.log(this.sva)
                                    data.responseData.sva.forEach(element => {
                                        let svaM = element;
                                        let encontro = false;
                                        this.sva.forEach(value => {
                                            if (svaM.id == value.id) {
                                                encontro = true;
                                            }
                                        })
                                        if (!encontro) {
                                            this.svaMas.push(svaM)
                                        }
                                    });
                                    //xthis.svaMas = data.responseData.sva;
                                    console.log(this.svaMas)
                                    this.svaMas = this.svaMas.filter(value => value.code !== 'DSHD' && value.code !== 'DHD' && value.code !== 'DVR')
                                    console.log(this.svaMas)
                                    /*if(data.responseData.offLine){
                                        this.svaOffLine = data.responseData.offLine;
                                        alert("Flujo Offline");
                                    }*/
                                    //xthis.maxCountArpu = +data.responseData.sva[0].maxDecos;
                                    //xthis.equipamiento = data.responseData.equipment;
                                    /*if (xthis.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                                        xthis.order.equipment = data.responseData.equipment && data.responseData.equipment[0] ? data.responseData.equipment[0] : null;
                                        xthis.order.equipmentName = xthis.order.equipment ? xthis.order.equipment.unit : '';
                                        xthis.flagActualSVA=true;
                                    } else if (xthis.type == globals.ORDER_SVA) {
                                        xthis.equipment = data.responseData.equipment && data.responseData.equipment[0] ? data.responseData.equipment[0] : null;
                                        xthis.equipmentName = xthis.order.equipment ? xthis.order.equipment.unit : '';
                                    }*/

                                    //xthis.ls.setData(this.order);
                                    xthis.completeDataSVA(this.svaMas);

                                    xthis.loading = false;
                                } else {
                                    xthis.loading = false;
                                    xthis.respuestaErrorSva = data.responseMessage;
                                }
                            },
                            err => {
                                console.log(err)
                            },
                            () => {
                                if (xthis.order.type == globals.ORDER_SVA) {

                                    xthis.hasEquipment = false;
                                    xthis.sva.forEach((item, index) => {
                                        if (item.selected == true) {
                                            if (!xthis.hasEquipment) {
                                                xthis.hasEquipment = item.hasEquipment == "1" ? true : false;
                                            }

                                        }
                                    });
                                }
                            }
                        );
                },
                err => {
                    console.log(err)
                },
                () => {
                    if (xthis.order.type == globals.ORDER_SVA) {

                        xthis.hasEquipment = false;
                        xthis.sva.forEach((item, index) => {
                            if (item.selected == true) {
                                if (!xthis.hasEquipment) {
                                    xthis.hasEquipment = item.hasEquipment == "1" ? true : false;
                                }

                            }
                        });
                    }
                }
            );
    }



    completeData(sva) {
        console.log('SVA List Basica' + sva);

        let temp = {};
        let group = [];
        if (this.order.sva != null) {
            for (let i = 0; i < this.order.sva.length; i++) {
                this.svaU = this.order.sva[i];
                this.mapSva.set(this.svaU.code + this.svaU.unit + this.svaU.id, this.svaU);
            }

            //no es optimo... pero no hay de otra x el momento...
            if (this.type != globals.ORDER_SVA) {
                let defaultSvas = JSON.parse(localStorage.getItem("defaultSvas"));
                for (let i = 0; i < defaultSvas.length; i++) {
                    this.defaultMapSva.set(defaultSvas[i].code, "SI");
                }
            }
        }
        let groupCount = -1;

        //console.log("--- MAXIMUM ---");
        //console.log(this.maximunDecos);

        for (let i = 0; i < this.sva.length; i++) {
            let svaObject = this.sva[i];

            /*if ((svaObject.code == 'DSHD' || svaObject.code == 'DHD' || svaObject.code == 'DVR')
                && (svaObject.unit == "3" || svaObject.unit == "4")) continue;*/

            let svaOb
            if (!temp[svaObject.code]) {


                groupCount++;
                this.selectedSva[groupCount] = 0;
                let obj = { cod: svaObject.code, label: svaObject.description, arr: [], type: svaObject.type, selected: false };
                temp[svaObject.code] = obj;
                group.push(obj);
                //determinamos si es que es un sva obligatorio (en caso de migras)
                if (this.defaultMapSva.get(svaObject.code) != null) {
                    svaObject.obligatorio = true;
                    obj.selected = true;
                } else {
                    svaObject.obligatorio = false;
                    obj.selected = false;
                }
                if (this.mapSva.get(svaObject.code + svaObject.unit + svaObject.id) != null) {
                    svaObject.selected = true;
                    obj.selected = true;
                    this.selectedSva[groupCount] = svaObject;

                    //Sprint-11 Validacion y comparación de sva 0 en vista y backend.
                    if (this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA || this.order.type == globals.ORDER_SVA) {
                        var validateZero = this.mapSva.get(svaObject.code + svaObject.unit + svaObject.id);
                        if (validateZero.cost == "0") {
                            this.selectedSva[groupCount].cost = "0";
                        }
                    }


                }
                //para el caso de alta pura:
                //para alta pura verificamos si es que el sva es un sva por default
                if (svaObject.altaPuraDefaultSVA) {
                    svaObject.obligatorio = true;
                    obj.selected = true;
                    this.selectedSva[groupCount] = svaObject;
                }

                obj.arr.push(svaObject);

            } else {
                let obj = temp[svaObject.code];
                //determinamos si es que es un sva obligatorio (en caso de migras)
                if (this.defaultMapSva.get(svaObject.code) != null) {
                    svaObject.obligatorio = true;
                    obj.selected = true;
                } else {
                    svaObject.obligatorio = false;
                    obj.selected = false;
                }
                if (this.mapSva.get(svaObject.code + svaObject.unit + svaObject.id) != null) {
                    svaObject.selected = true;
                    obj.selected = true;
                    this.selectedSva[groupCount] = svaObject;
                }
                //para el caso de alta pura:
                //para alta pura verificamos si es que el sva es un sva por default
                if (svaObject.altaPuraDefaultSVA) {
                    svaObject.obligatorio = true;
                    obj.selected = true;
                    this.selectedSva[groupCount] = svaObject;
                }
                obj.arr.push(svaObject);
            }
        }
        this.group = group;
        console.log('group', this.group)

        //20180323 fix para mostrar el precio total incluyendo el precio de los sva por default... y para mostrar los divs de svas default
        //$('#emailDiv').hide();
        for (let i = 0; i < this.selectedSva.length; i++) {
            var groupItemSeleccionado = null;
            if (this.selectedSva[i].code) {
                groupItemSeleccionado = group[i];
            }
            this.dropdown(this.selectedSva[i], 1, groupItemSeleccionado);

            if (this.selectedSva[i].code == "BTV" && this.selectedSva[i].cost == "0" && this.order.type == globals.ORDER_ALTA_NUEVA) {
                this.defaultMapSva.set(this.selectedSva[i].code, "SI");
            }
            if (this.selectedSva[i].code == "SVAI" && this.selectedSva[i].cost == "0" && this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                this.defaultMapSva.set(this.selectedSva[i].code, "SI");
            }
            if (this.selectedSva[i].code == "SVAL" && this.selectedSva[i].cost == "0" && this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                this.defaultMapSva.set(this.selectedSva[i].code, "SI");
            }

        }
        //console.log("hola sva "+ (<HTMLSelectElement>document.getElementById('DSHD')).value);



    }

    validateSva(selectedSvas) {

        let codtypeProducto = selectedSvas[0].code;
        let documentType = this.order.customer.documentType;
        let documentNumber = this.order.customer.documentNumber;

        var sourceProductName = this.order.product.productDescription;
        var tvBlock = " ";
        var SVA_internet = " ";
        var SVA_line = " ";
        var decoHD = " ";
        var decoSHD = " ";
        var decoDVR = " ";
        var repetidorRSW = " ";
        var blockProduct = " ";

        var isTvBlock = false;
        var isSVA_internet = false;
        var isSVA_line = false;
        var isDecoHD = false;
        var isDecoSHD = false;
        var isDecoDVR = false;
        var isRepetidorRSW = false;
        var isBlockProduct = false;

        var isSVA_internetVM = false;
        var isSVA_lineVM = false;
        // var isDecoHDVM = false;
        // var isDecoSHDVM = false;
        // var isDecoDVRVM = false;
        var isDecoRSWVM = false;
        var isBPVM = false;
        var isdpeVM = false;
        var ispdVM = false;
        var iscintVM = false;



        var selectedSvasFinal = [];

        $('.tablaDat select').each(function (index, item) {
            if (this.value != "") {
                if (this.id == "SVAI") isSVA_internet = true;
                if (this.id == "SVAL") isSVA_line = true;
                if (this.id == "DHD") isDecoHD = true;
                if (this.id == "DSHD") isDecoSHD = true;
                if (this.id == "DVR") isDecoDVR = true;
                if (this.id == "RSW") isRepetidorRSW = true;
                if (this.id == "BP") isBlockProduct = true;

                if (this.id == "SVAIVM") isSVA_internetVM = true;
                if (this.id == "SVALVM") isSVA_lineVM = true;
                // if (this.id == "DHDVM") isDecoHDVM = true;
                // if (this.id == "DSHDVM") isDecoSHDVM = true;
                // if (this.id == "DVRVM") isDecoDVRVM = true;
                if (this.id == "RSWVM") isDecoRSWVM = true;
                if (this.id == "BPVM") isBPVM = true;
                if (this.id == "DPEVM") isdpeVM = true;
                if (this.id == "PDVM") ispdVM = true;
                if (this.id == "CINTVM") iscintVM = true;
            }
        });

        for (var i = 0; i < selectedSvas.length; i++) {
            if (selectedSvas[i].code == "BTV") {
                isTvBlock = true;
                break;
            }
        }

        for (var i = 0; i < selectedSvas.length; i++) {
            if (selectedSvas[i].code == "BTV" && isTvBlock) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "SVAI" && isSVA_internet && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "SVAL" && isSVA_line && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "DHD" && isDecoHD && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "DSHD" && isDecoSHD && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "DVR" && isDecoDVR && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "RSW" && isRepetidorRSW && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "BP" && isBlockProduct && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);

            if (selectedSvas[i].code == "SVAI" && isSVA_internetVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "SVAL" && isSVA_lineVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            // if (selectedSvas[i].code == "DHD" && isDecoHDVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            // if (selectedSvas[i].code == "DSHD" && isDecoSHDVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            // if (selectedSvas[i].code == "DVR" && isDecoDVRVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "RSW" && isDecoRSWVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "BP" && isBPVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "DPE" && isdpeVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "PD" && ispdVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            if (selectedSvas[i].code == "CINT" && iscintVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
        }


        //console.log(selectedSvasFinal);

        selectedSvas = selectedSvasFinal;

        this.order.sva = selectedSvas;

        this.ls.setData(this.order);



        for (var i = 0; i < selectedSvas.length; i++) {
            if (selectedSvas[i].code == "BTV" && isTvBlock) { tvBlock = selectedSvas[i].unit; continue; }
            if (selectedSvas[i].code == "SVAI" && isSVA_internet) { SVA_internet = selectedSvas[i].unit; continue; }
            if (selectedSvas[i].code == "SVAL" && isSVA_line) { SVA_line = selectedSvas[i].unit; continue; }
            if (selectedSvas[i].code == "DHD" && isDecoHD) { decoHD = selectedSvas[i].unit; continue; }
            if (selectedSvas[i].code == "DSHD" && isDecoSHD) { decoSHD = selectedSvas[i].unit; continue; }
            if (selectedSvas[i].code == "DVR" && isDecoDVR) { decoDVR = selectedSvas[i].unit; continue; }
            if (selectedSvas[i].code == "RSW" && isRepetidorRSW) { repetidorRSW = selectedSvas[i].unit; continue; }
            if (selectedSvas[i].code == "BP" && isBlockProduct) { blockProduct = selectedSvas[i].unit; continue; }
        }

        let productName = sourceProductName + "|" + tvBlock + "|" + SVA_internet + "|" + SVA_line + "|" + decoHD +
            "|" + decoSHD + "|" + decoDVR + "|" + blockProduct + "|" + repetidorRSW;
        this.showImgValidate = true;

        this.showAlert = false;

        let nameDistrict = this.product.district;
        let nameProvince = this.product.province;
        let nameDepart = this.product.department;

        //console.log(productName);

        //console.log(selectedSvas);

        this.customerService.getValidateDuplicadoUbicacion(documentType, documentNumber, codtypeProducto, productName, nameDepart, nameProvince, nameDistrict)
            .subscribe(
                data => {
                    if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
                        this.answerValidationOffer = data;
                        if (this.answerValidationOffer.responseCode != "00") {
                            this.showImgValidate = false;
                            this.showAlert = true;
                            alert(this.answerValidationOffer.responseMessage);
                        } else {
                            this.showAlert = false;
                        }
                    } else {
                        this.showImgValidate = false;
                        this.showAlert = true;
                        this.answerValidationOffer.responseMessage = "La validacion no pudo concretarse. Intente nuevamente";
                    }
                },
                err => {
                    console.log(err)
                },
                () => {
                    this.showImgValidate = false;
                    if (!this.showAlert) {
                        this.ls.setData(this.order);
                        this.router.navigate(['saleProcess/salecondition']);
                    }
                });

    }

    addSva() {

        //this.correoElectronico = $.trim($('#email').val());

        //console.log(this.type);

        let validateDecos = false;

        let elementDSHD = document.getElementById("DSHD");
        if (elementDSHD) validateDecos = true;

        let elementDHD = document.getElementById("DHD");
        if (elementDHD) validateDecos = true;

        let elementDVR = document.getElementById("DVR");
        if (elementDVR) validateDecos = true;

        // let elementDSHDVM = document.getElementById("DSHDVM");
        // if (elementDSHDVM) validateDecos = true;

        // let elementDHDVM = document.getElementById("DHDVM");
        // if (elementDHDVM) validateDecos = true;

        // let elementDVRVM = document.getElementById("DVRVM");
        // if (elementDVRVM) validateDecos = true;

        /*let elementRSW = document.getElementById("RSW");
        if (elementRSW) validateDecos = true;*/


        //console.log("validateDecos: " + validateDecos);

        //console.log(this.maxAditionalDecos);

        if (validateDecos) {

            let decosSmartHD;
            if (document.getElementById("DSHD")) {
                decosSmartHD = document.getElementById("DSHD")["value"];
                decosSmartHD = decosSmartHD.split(":")[0];
                decosSmartHD = parseInt(decosSmartHD) + 1;
                if (isNaN(decosSmartHD)) { decosSmartHD = 0 }
            } else decosSmartHD = 0;

            let decosHD;
            if (document.getElementById("DHD")) {
                decosHD = document.getElementById("DHD")["value"];
                decosHD = decosHD.split(":")[0];
                decosHD = parseInt(decosHD) + 1;
                if (isNaN(decosHD)) { decosHD = 0 }
            } else decosHD = 0;

            let decosDVR;
            if (document.getElementById("DVR")) {
                decosDVR = document.getElementById("DVR")["value"];
                decosDVR = decosDVR.split(":")[0];
                decosDVR = parseInt(decosDVR) + 1;
                if (isNaN(decosDVR)) { decosDVR = 0 }
            } else decosDVR = 0;

            // let decosSmartHDVM;
            // if (document.getElementById("DSHDVM")) {
            //     decosSmartHDVM = document.getElementById("DSHDVM")["value"];
            //     decosSmartHDVM = decosSmartHDVM.split(":")[0];
            //     decosSmartHDVM = parseInt(decosSmartHDVM) + 1;
            //     if (isNaN(decosSmartHDVM)) { decosSmartHDVM = 0 }
            // } else decosSmartHDVM = 0;

            // let decosHDVM;
            // if (document.getElementById("DHDVM")) {
            //     decosHDVM = document.getElementById("DHDVM")["value"];
            //     decosHDVM = decosHDVM.split(":")[0];
            //     decosHDVM = parseInt(decosHDVM) + 1;
            //     if (isNaN(decosHDVM)) { decosHDVM = 0 }
            // } else decosHDVM = 0;

            // let decosDVRVM;
            // if (document.getElementById("DVRVM")) {
            //     decosDVRVM = document.getElementById("DVRVM")["value"];
            //     decosDVRVM = decosDVRVM.split(":")[0];
            //     decosDVRVM = parseInt(decosDVRVM) + 1;
            //     if (isNaN(decosDVRVM)) { decosDVRVM = 0 }
            // } else decosDVRVM = 0;

            /*let decosRSW;
            if (document.getElementById("RSW")) {
                decosRSW = document.getElementById("RSW")["value"];
                decosRSW = decosRSW.split(":")[0];
                decosRSW = parseInt(decosRSW) + 1;
                if (isNaN(decosRSW)) { decosRSW = 0 }
            } else decosRSW = 0;*/

            if (this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                if (this.order.selectedOffering.productCode.includes('MHD') || this.order.selectedOffering.productCode.includes('MTE')) {
                    if ((decosSmartHD + decosHD + decosDVR) > this.maxCountArpu) {
                        //alert("PUEDE AGREGAR UN MÁXIMO DE " + this.maxAditionalDecos + " DECOS ADICIONALES.");
                        this.messageWarning = "PUEDE AGREGAR UN MÁXIMO DE " + this.maxCountArpu + " PUNTO ADICIONAL.";
                        $('#maxDecosWarning').html(this.messageWarning);
                        $('#maxDecosWarning').show();
                        return;
                    }
                }
            }

            if (this.order.type == globals.ORDER_ALTA_NUEVA) {
                if (this.order.selectedOffering.productCode.includes('MHD') || this.order.selectedOffering.productCode.includes('MTE')) {
                    if ((decosSmartHD + decosHD + decosDVR) > 1) {
                        //alert("PUEDE AGREGAR UN MÁXIMO DE " + this.maxAditionalDecos + " DECOS ADICIONALES.");
                        this.messageWarning = "PUEDE AGREGAR UN MÁXIMO DE " + 1 + " PUNTO ADICIONAL.";
                        $('#maxDecosWarning').html(this.messageWarning);
                        $('#maxDecosWarning').show();
                        return;
                    }
                }
            }

            if (this.order.type == globals.ORDER_SVA) {
                if ((decosSmartHD + decosHD + decosDVR) > this.maxCountArpu) {
                    //alert("PUEDE AGREGAR UN MÁXIMO DE " + this.maxAditionalDecos + " DECOS ADICIONALES.");
                    this.messageWarning = "PUEDE AGREGAR UN MÁXIMO DE " + this.maxCountArpu + " PUNTOS ADICIONALES.";
                    $('#maxDecosWarning').html(this.messageWarning);
                    $('#maxDecosWarning').show();
                    return;
                }
            }


            if ((decosSmartHD + decosHD + decosDVR) > this.maxAditionalDecos) {
                //alert("PUEDE AGREGAR UN MÁXIMO DE " + this.maxAditionalDecos + " DECOS ADICIONALES.");
                this.messageWarning = "PUEDE AGREGAR UN MÁXIMO DE " + this.maxAditionalDecos + " PUNTOS ADICIONALES.";
                $('#maxDecosWarning').html(this.messageWarning);
                $('#maxDecosWarning').show();
                return;
            }
        }

        let contSelectedSva: number = 0;
        let selectedSvas = [];
        let productTypeString: string;


        //inicio del cambio

        console.log(this.svaUni);

        this.svaUni.forEach((item, index) => {
            if (item.selected == true) {
                selectedSvas.push(item);
                contSelectedSva++;

            }
        });
        this.order.sva = selectedSvas;

        if (document.getElementById("DSHD") != null) {
            if ($("#DSHD").val() == "0: Object" || $("#DSHD").val() == "1: Object" || $("#DSHD").val() == "2: Object" || $("#DSHD").val() == "3: Object") {
                //correo obligatorio
                this.correoElectronicoObligatorio = true;
            }
        }

        // if (document.getElementById("DSHDVM") != null) {
        //     if ($("#DSHDVM").val() == "0: Object" || $("#DSHDVM").val() == "1: Object" || $("#DSHDVM").val() == "2: Object" || $("#DSHDVM").val() == "3: Object") {
        //         //correo obligatorio
        //         this.correoElectronicoObligatorio = true;
        //     }
        // }

        if (this.correoElectronicoObligatorio && (this.correoElectronico == null || this.correoElectronico.length == 0)) {
            $('#maxDecosWarning').html("Por favor ingresar el correo");
            $('#maxDecosWarning').show();
            return;
        }

        if (this.correoElectronicoObligatorio) {
            if (!this.validateEmail(this.correoElectronico)) {
                $('#maxDecosWarning').html("Correo electrónico no válido.");
                $('#maxDecosWarning').show();
                return;
            }
        }

        // si es que se ingreso un correo electronico entonces lo guardamos en el objeto order
        if (this.correoElectronico != null && this.correoElectronico.length > 0) {
            this.order.customer.email = this.correoElectronico;
        }

        // si es que se ingreso un correo electronico entonces lo guardamos en el objeto order
        //if (this.correoElectronico != null && this.correoElectronico.length > 0) {
        this.order.customer.email = this.correoElectronico;
        //}
        //this.order.status = "S";
        //localStorage.setItem('order', JSON.stringify(this.order));

        if (this.order.type == globals.ORDER_SVA) {
            //console.log("seleccion sva");
            //console.log(this.selectedSva.length);

            var atLeastOne = false;
            $('.tablaDat select').each(function (index, item) {
                if (this.value != "") {
                    atLeastOne = true;
                    return false;
                }
            });

            for (var i = 0; i < selectedSvas.length; i++) {
                if (selectedSvas[i].code == "BTV") {
                    atLeastOne = true;
                    break;
                }
            }

            if (atLeastOne) {

                //REVIEW-CR
                if (Number(this.product.sourceType) == 6) {
                    productTypeString = "Mono TV"
                } else {
                    productTypeString = "N/A"
                }

                this.order.selectedOffering = JSON.parse('{"cashPrice":"' + globals.CASH_PRICE_SVA + '", "paymentMethod":"' + globals.PAYMENT_METHOD_SVA +
                    '", "campaign":"Masiva", "productName":"' + selectedSvas[0].description + '", "productType":"' + productTypeString +
                    '", "productCategory":"' + selectedSvas[0].unit + '", "price":"' + selectedSvas[0].cost + '", "productCode":"' + selectedSvas[0].code + '"}');

                if (this.hasEquipment) {
                    this.order.equipment = this.equipment;
                    this.order.equipmentName = this.equipmentName;
                }

                this.validateSva(selectedSvas);
            } else {
                //this.messageValidator = '&#161;Debe seleccionar al menos un SVA!.';
                //$('#svaModal').modal('show');
                alert("Debe seleccionar al menos un SVA.");
            }
        } else {

            var isTvBlock = false;
            var isSVA_internet = false;
            var isSVA_line = false;
            var isDecoHD = false;
            var isDecoSHD = false;
            var isDecoDVR = false;
            var isDecoRSW = false;
            var isBP = false;

            var isSVA_internetVM = false;
            var isSVA_lineVM = false;
            // var isDecoHDVM = false;
            // var isDecoSHDVM = false;
            // var isDecoDVRVM = false;
            var isDecoRSWVM = false;
            var isBPVM = false;
            var isdpeVM = false;
            var ispdVM = false;
            var iscintVM = false;

            var selectedSvasFinal = [];

            $('.tablaDat select').each(function (index, item) {
                if (this.value != "") {
                    //if (this.id == "BTV") isTvBlock = true;
                    if (this.id == "SVAI") isSVA_internet = true;
                    if (this.id == "SVAL") isSVA_line = true;
                    if (this.id == "DHD") isDecoHD = true;
                    if (this.id == "DSHD") isDecoSHD = true;
                    if (this.id == "DVR") isDecoDVR = true;
                    if (this.id == "RSW") isDecoRSW = true;
                    if (this.id == "BP") isBP = true;

                    if (this.id == "SVAIVM") isSVA_internetVM = true;
                    if (this.id == "SVALVM") isSVA_lineVM = true;
                    // if (this.id == "DHDVM") isDecoHDVM = true;
                    // if (this.id == "DSHDVM") isDecoSHDVM = true;
                    // if (this.id == "DVRVM") isDecoDVRVM = true;
                    if (this.id == "RSWVM") isDecoRSWVM = true;
                    if (this.id == "BPVM") isBPVM = true;
                    if (this.id == "DPEVM") isdpeVM = true;
                    if (this.id == "PDVM") ispdVM = true;
                    if (this.id == "CINTVM") iscintVM = true;
                }
            });

            for (var i = 0; i < selectedSvas.length; i++) {
                if (selectedSvas[i].code == "BTV") {
                    isTvBlock = true;
                    break;
                }
            }

            for (var i = 0; i < selectedSvas.length; i++) {
                if (selectedSvas[i].code == "BTV" && isTvBlock) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "SVAI" && isSVA_internet && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "SVAL" && isSVA_line && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "DHD" && isDecoHD && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "DSHD" && isDecoSHD && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "DVR" && isDecoDVR && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "RSW" && isDecoRSW && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "BP" && isBP && selectedSvas[i].svaMass == undefined) selectedSvasFinal.push(selectedSvas[i]);

                if (selectedSvas[i].code == "SVAI" && isSVA_internetVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "SVAL" && isSVA_lineVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
                // if (selectedSvas[i].code == "DHD" && isDecoHDVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
                // if (selectedSvas[i].code == "DSHD" && isDecoSHDVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
                // if (selectedSvas[i].code == "DVR" && isDecoDVRVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "RSW" && isDecoRSWVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "BP" && isBPVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "DPE" && isdpeVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "PD" && ispdVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
                if (selectedSvas[i].code == "CINT" && iscintVM && selectedSvas[i].svaMass) selectedSvasFinal.push(selectedSvas[i]);
            }
            console.log(selectedSvas);
            console.log(selectedSvasFinal);

            selectedSvas = selectedSvasFinal;

            this.order.sva = selectedSvas;

            //this.ls.setData(this.order);


            this.order.selectedOffering.financingCost = 0;//this.order.equipment.cost;
            this.ls.setData(this.order);
            //this.router.navigate(['saleProcess/salesSummary']);
            this.router.navigate(['saleProcess/salecondition']);
            //if(this.countDeco > this.maxDecoMigracion){
            //this.messageValidator = "No puede venderle al cliente mayor a "+ this.countDeco + " decos",
            //$('#svaModal').modal('show');
            //}
            //console.log(this.selectedSva.length);
            //  if(selectedSvas.length != 0){
            //     if(this.order.type == globals.ORDER_ALTA_NUEVA || this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA){
            //       //this.order.sva =  selectedSvas;
            //         this.ls.setData(this.order);
            //         this.router.navigate(['saleProcess/salesSummary']);
            //     }
            // }else {
            //         //this.messageValidator = "�Debe seleccionar al menos un SVA!.";
            //     $('#svaModal').modal('show');
            // }
        }
        //fin del cambio

    }

    // Sprint 6 - si es que el sva elegido es DecoSmartHD
    evaluateSvaDecoSmartHD(sva) {
        // Si es que el sva seleccionado es DSHD
        this.order = this.ls.getData();
        if ("DSHD" == sva.code) {
            if (this.order.customer.email == "" || this.order.customer.email == null) {
                // y si es que sva.selected es null entonces significa que se ha seleccionado una cantidad correcta (cantidad >= 1)
                if (sva.selected == null) {
                    // entonces notificamos al componente "saleprocess.component" para que pida el correo obligatoriamente
                    this.messageService.sendMessage("CORREO OBLIGATORIO");


                    this.order.ingresoCorreoObligatorio = true;
                    this.order.editandoCliente = true;
                    this.ls.setData(this.order);
                    //this.ls.setCorreoObligatorio(true);
                } else {
                    this.messageService.sendMessage("CORREO OPCIONAL SIN EDITAR");
                    this.order.ingresoCorreoObligatorio = false;
                    this.order.editandoCliente = false;
                    this.ls.setData(this.order);
                    //this.ls.setCorreoObligatorio(false);
                }
            } else {
                // solo cambiamos el flag
                if (sva.selected == null) {
                    this.messageService.sendMessage("NOTIFICACION CORREO OBLIGATORIO");
                } else {
                    this.messageService.sendMessage("NOTIFICACION CORREO OPCIONAL");
                }

            }
        }
    }

    resetSva(cod, element, groupItem, selectedSva) {

        //console.log(cod);
        //console.log(element);
        //console.log(element.checked);

        groupItem.selected = false;

        if (element.checked) return false;


        $('#' + cod).prop('selectedIndex', -1);

        this.correoElectronicoObligatorio = false;
        if (this.correoElectronico != this.order.customer.email) {
            //Yes
        } else {
            this.correoElectronico = this.order.customer.email;
        }
        this.eliminarSva(cod, "", selectedSva);
        $('#' + cod).removeClass("has-content");
        $('#' + cod + 'Cost').hide();
        $("#" + cod + "Check").prop('disabled', true);
        $("#" + cod + "Check").prop('checked', false);
        console.log(cod)
        switch (cod) {
            case 'SVAL':
                this.cerrarCard2('sva_linea_', cod);
                break;
            case 'SVAI':
                this.cerrarCard2('sva_internet_', cod);
                break;
            case 'DSHD':
                this.cerrarCard2('sva_smart_', cod);
                $('#maxDecosWarning').hide();
                break;
            case 'DVR':
                this.cerrarCard2('sva_dvr_', cod);
                $('#maxDecosWarning').hide();
                break;
            case 'DHD':
                this.cerrarCard2('sva_hd_', cod);
                $('#maxDecosWarning').hide();
                break;
            case 'BTV':
                this.cerrarCard2('sva_tv_', cod);
                break;
            case 'BP':
                this.cerrarCard2('sva_bp_', cod);
                break;
        }
    }

    resetSva2(cod) {
        $('#' + cod).prop('selectedIndex', -1);
        $('#' + cod).removeClass("has-content");
        $('#' + cod + 'Cost').hide();
        $("#" + cod + "Check").prop('disabled', true);
        $("#" + cod + "Check").prop('checked', false);
        //if (this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
        switch (cod) {
            case "SVAL":
                this.costSVAL = 0;
                break;
            case "SVAI":
                this.costSVAI = 0;
                break;
            case "BP":
                this.costBP = 0;
                break;
            case "DHD":
                this.costDHD = 0;
                $('#maxDecosWarning').hide();
                this.correoElectronicoObligatorio = false;
                if (this.correoElectronico != this.order.customer.email) {
                    //Yes
                } else {
                    this.correoElectronico = this.order.customer.email;
                } break;
            case "DSHD":
                $('#email').hide();
                $('#emailLabel').hide();
                $('#emailDiv').hide();
                this.costDSHD = 0;
                $('#maxDecosWarning').hide();
                this.correoElectronicoObligatorio = false;
                this.correoElectronico = '';
                break;
            case "DVR":
                this.costDVR = 0;
                $('#maxDecosWarning').hide();
                this.correoElectronicoObligatorio = false;
                if (this.correoElectronico != this.order.customer.email) {
                    //Yes
                } else {
                    this.correoElectronico = this.order.customer.email;
                } break;
            case "BTV":
                this.costBTV = 0;
                break;
        }
        //}
    }

    resetSva2Mas(cod) {
        $('#' + cod + 'VM').prop('selectedIndex', -1);
        $('#' + cod + 'VM').removeClass("has-content");
        $('#' + cod + 'CostVM').hide();
        $("#" + cod + "CheckVM").prop('disabled', true);
        $("#" + cod + "CheckVM").prop('checked', false);
        //if (this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
        switch (cod) {
            case "SVAL":
                this.costSVAL = 0;
                break;
            case "SVAI":
                this.costSVAI = 0;
                break;
            case "BP":
                this.costBP = 0;
                break;
            case "DHD":
                this.costDHD = 0;
                $('#maxDecosWarning').hide();
                this.correoElectronicoObligatorio = false;
                if (this.correoElectronico != this.order.customer.email) {
                    //Yes
                } else {
                    this.correoElectronico = this.order.customer.email;
                } break;
            case "DSHD":
                $('#email').hide();
                $('#emailLabel').hide();
                $('#emailDiv').hide();
                this.costDSHD = 0;
                $('#maxDecosWarning').hide();
                this.correoElectronicoObligatorio = false;
                this.correoElectronico = '';
                break;
            case "DVR":
                this.costDVR = 0;
                $('#maxDecosWarning').hide();
                this.correoElectronicoObligatorio = false;
                if (this.correoElectronico != this.order.customer.email) {
                    //Yes
                } else {
                    this.correoElectronico = this.order.customer.email;
                } break;
            case "BTV":
                this.costBTV = 0;
                break;
        }
        //}
    }

    cerrarCard2(numero, idsva) {
        console.log(numero)
        $("#" + numero).attr("style", "display:none");
        var resumen = JSON.parse(localStorage.getItem('resumen'));
        localStorage.setItem('resumen', JSON.stringify(resumen));
    }

    dropdown(sva, flag, groupItem) {

        if (groupItem != null) {
            groupItem.selected = true;
        }

        $("#" + sva.code + "Check").prop('checked', true);
        $("#" + sva.code + "Check").prop('disabled', false);
        $('#' + sva.code + 'Cost').show();
        if (sva.code != undefined) {
            this.dataCode = sva.code + '';
        }
        if (sva.code == "DSHD") {
            if ($("#DSHD").val() == "1: Object" || $("#DSHD").val() == "2: Object" || $("#DSHD").val() == "0: Object") {
                $('#email').show();
                $('#emailLabel').show();
                $('#emailDiv').show();
                //correo obligatorio
                this.correoElectronicoObligatorio = true;
                if (this.correoElectronico != this.order.customer.email) {
                    //Yes
                } else {
                    this.correoElectronico = this.order.customer.email;
                }
                // no funciona el binding de angular... usamos jquery para asegurarnos el seteo
                $('#email').val(this.correoElectronico);
            }
        } else if (sva.code == undefined) {
            $('#email').hide();
            $('#emailLabel').hide();
            $('#emailDiv').hide();
            //correo opcional
            this.correoElectronicoObligatorio = false;
            this.correoElectronico = this.order.customer.email;
            $("#" + this.dataCode + "Check").prop('checked', false);
            $("#" + this.dataCode + "Check").prop('disabled', true);
            $('#' + this.dataCode).removeClass("has-content");
        }

        this.countDeco = 0;
        this.showMessageDecos = false;
        //console.log("cantidad de sva " + this.sva.length);

        // Sprint 6 - evaluar el sva decosmarthd
        this.evaluateSvaDecoSmartHD(sva);

        for (let i = 0; i < this.sva.length; i++) {
            this.sva[i].code;
            if (this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                if (this.sva[i].id == sva.id && flag == 1) {
                    if ((this.sva[i].code == "DHD" || this.sva[i].code == "DSHD" || this.sva[i].code == "DVR")) {
                    }
                    this.sva[i].selected = true;
                } else if (sva.selected == false && this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                    $('#' + sva.code).closest("td").next().empty();
                    this.showMessageDecos = false;
                    if ((sva.code == this.sva[i].code)) {
                        this.sva[i].selected = null;
                    }
                } else if (this.sva[i].id == sva.id && sva.selected == true && flag == 2) {
                    this.sva[i].selected = true;
                } else {
                    if (this.sva[i].code == sva.code) {
                        if (sva.code != "BTV") {
                            this.sva[i].selected = null;
                        }
                    }
                }
            }
            if (this.order.type == globals.ORDER_ALTA_NUEVA || this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA
                || this.order.type == globals.ORDER_SVA) {
                if (this.sva[i].id == sva.id && sva.selected == true && flag == 2) {
                    this.sva[i].selected = true;
                } else if (this.sva[i].id == sva.id && flag == 1) {
                    this.sva[i].selected = true;
                    var code = this.sva[i].code;
                    var desc = this.sva[i].description;
                    var unit = this.sva[i].unit;
                    var pago = 0;
                    var resumen = JSON.parse(localStorage.getItem('resumen'));
                    if (this.order.type == globals.ORDER_SVA && this.countSVA == 0) {
                        this.precionormal = 0;
                        this.preciopromocional = 0;
                        this.countSVA = this.countSVA + 1;
                    }
                }
                if (this.sva[i].code == sva.code && this.sva[i].id != sva.id) {
                    if (sva.code != "BTV") {
                        this.sva[i].selected = null;
                    }
                } else if (sva.selected == false && this.sva[i].code == sva.code) {
                    this.sva[i].selected = null;
                }
            }

            if (this.order.type == globals.ORDER_ALTA_NUEVA || this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                if ((this.sva[i].code == "DHD" || this.sva[i].code == "DSHD" || this.sva[i].code == "DVR") && this.sva[i].selected == true) {
                    this.countDeco = this.countDeco + Number(this.sva[i].unit);

                    if (this.countDeco > this.maximunDecos) {
                        this.numMaxDecos = this.maximunDecos;
                        this.showMessageDecos = true;
                    } else {

                        this.showMessageDecos = false;
                    }
                }
            }
            //FIN DE OPCION DE MIGRACION
            //this.validacionBTV(this.sva[i]);
        }
        this.totales(this.sva);

        if (this.order.type == globals.ORDER_SVA) {

            let maxDecos: number = -1;
            let decosSele: number = 0;
            this.showMessageDecos = false;

            this.hasEquipment = false;
            this.sva.forEach((item, index) => {
                if (item.selected == true) {
                    if (!this.hasEquipment) {
                        this.hasEquipment = item.hasEquipment == "1" ? true : false;
                    }
                    if (item.type == 'INT') {
                        if (maxDecos < 0) {

                            maxDecos = Number(item.maxDecos);
                            this.numMaxDecos = maxDecos;
                        } else {
                        }

                        if (Number(item.unit) > 0) {
                            decosSele = Number(item.unit) + decosSele;
                            if (decosSele > maxDecos) {
                                this.showMessageDecos = true;
                            } else {
                            };
                        }
                    }

                }
            });
        }
    }

    //funcion usada solo cuando el tipo de venta es 'SVA' solito
    setearSubtotalSVAsEnTotalResta(precioSubTotal) {
        //if (this.productOffering == null) {
        /*document.getElementById("order_producto_price").innerHTML = "S/ " + precioSubTotal;
        document.getElementById("order_producto_priceProm").innerHTML = "S/ " + precioSubTotal;*/

        var precioTotalRegular = null;
        var precioTotalPromocional = null;
        if (this.productOffering != null) {
            //entonces sumamos el precio del producto principal (caso alta pura)
            precioTotalRegular = (parseFloat(precioSubTotal) - this.order.selectedOffering.price).toFixed(2);
            precioTotalPromocional = (parseFloat(precioSubTotal) - this.order.selectedOffering.promPrice).toFixed(2);

        } else {
            var resumen = JSON.parse(localStorage.getItem('resumen'));
            precioTotalRegular = (parseFloat(precioSubTotal) + parseFloat(resumen.precio)).toFixed(2);
        }

        this.setInnerHTML("order_producto_price", "S/ " + precioTotalRegular);
        //this.setInnerHTML("order_producto_priceProm", "S/ " + precioTotalPromocional);

        if (this.order.type != 'S') {
            this.setInnerHTML("order_producto_priceProm", "S/ " + precioTotalPromocional);
        }
    }

    setearSubtotalSVAsEnTotalSuma(precioSubTotal) {
        //if (this.productOffering == null) {
        /*document.getElementById("order_producto_price").innerHTML = "S/ " + precioSubTotal;
        document.getElementById("order_producto_priceProm").innerHTML = "S/ " + precioSubTotal;*/

        var precioTotalRegular = null;
        var precioTotalPromocional = null;
        if (this.productOffering != null) {
            //entonces sumamos el precio del producto principal (caso alta pura)
            precioTotalRegular = (parseFloat(precioSubTotal) + this.order.selectedOffering.price).toFixed(2);
            precioTotalPromocional = (parseFloat(precioSubTotal) + this.order.selectedOffering.promPrice).toFixed(2);

        } else {
            var resumen = JSON.parse(localStorage.getItem('resumen'));
            precioTotalRegular = (parseFloat(precioSubTotal) + parseFloat(resumen.precio)).toFixed(2);
        }

        this.setInnerHTML("order_producto_price", "S/ " + precioTotalRegular);
        if (this.order.type != 'S') {
            this.setInnerHTML("order_producto_priceProm", "S/ " + precioTotalPromocional);
        }
    }

    // Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"
    cerrarDialogo(nombreModal) {
        // $("#" + nombreModal).modal('hide');
    }

    cerrarCard(numero, idsva) {
        // 20180323 - si es q es un sva obligatorio entonces rechazamos
        var defaultSva = this.defaultMapSva.get(idsva);
        if (defaultSva != null && defaultSva == "SI") {
            return;
        }
        $("#" + numero).attr("style", "display:none");
        this.resetSva2(idsva);
        var resumen = JSON.parse(localStorage.getItem('resumen'));
        //console.log(resumen);
        var precioCard = parseFloat($("#" + idsva + "Cost").html());
        this.precionormal = this.precionormal - precioCard;
        this.preciopromocional = this.preciopromocional - precioCard;
        resumen.precio = (parseFloat(resumen.precio) - parseFloat($("#" + idsva + "Cost").html())).toFixed(2);
        if (parseFloat(resumen.precio) == 0) {
            this.costSVAL = 0;
            this.costSVAI = 0;
            this.costDSHD = 0;
            this.costDVR = 0;
            this.costDHD = 0;
            this.costBTV = 0;
            this.costBP = 0;
        } else if (this.order.type != globals.ORDER_MEJORAR_EXPERIENCIA) {
            this.costSVAL = this.costSVAL - precioCard;
            this.costSVAI = this.costSVAI - precioCard;
            this.costDSHD = this.costDSHD - precioCard;
            this.costDVR = this.costDVR - precioCard;
            this.costDHD = this.costDHD - precioCard;
            this.costBTV = this.costBTV - precioCard;
            this.costBP = this.costBP - precioCard;
        }
        localStorage.setItem('resumen', JSON.stringify(resumen));

        //console.log(this.precionormal);
        //console.log((this.precionormal).toFixed(2));

        document.getElementById("resultado_sva_prom").innerHTML = "" + resumen.precio;
        //document.getElementById("resultado_sva_regular").innerHTML = "" + resumen.precio;

        var priceNormal = (this.precionormal).toFixed(2);
        priceNormal = priceNormal != "-0.00" ? priceNormal : "0.00";
        var pricePromo = (this.preciopromocional).toFixed(2);
        pricePromo = pricePromo != "-0.00" ? pricePromo : "0.00";

        this.setInnerHTML("order_producto_price", "S/ " + priceNormal);
        this.setInnerHTML("order_producto_priceProm", "S/ " + pricePromo);

        //this.setearSubtotalSVAsEnTotalResta(precioCard);

    }

    obtenerElemento() {

        if (!this.order.type) {
            this.order = JSON.parse(localStorage.getItem('order'));
            if (this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {

            }
        }
    }

    validateOffering(selectedOffering) {
        let codtypeProducto = selectedOffering.productTypeCode;
        let documentType = this.order.customer.documentType;
        let documentNumber = this.order.customer.documentNumber;
        let productName = selectedOffering.productName;
        this.showImgValidate = true;

        this.showAlert = false;


        let resumen = JSON.parse(localStorage.getItem('resumen'));
        resumen.producto = productName
        localStorage.setItem("resumen", JSON.stringify(resumen));
        //console.log("resumen ---- .-.- -- -- - ----" + JSON.parse(localStorage.getItem('resumen')));


        this.customerService.getValidateDuplicadoUbicacion(documentType, documentNumber, codtypeProducto, productName, this.nameDepart, this.nameProvince, this.nameDistrict)
            .subscribe(
                data => {
                    if (typeof data.responseData !== 'undefined' && data.responseData !== null) {


                        this.answerValidationOffer = data;

                        if (this.answerValidationOffer.responseCode != "00") {

                            this.showImgValidate = false;
                            this.showAlert = true;
                            this.loading = false;

                        } else {

                            this.showAlert = false;
                            this.order.status = "O";
                            this.order.selectedOffering = selectedOffering;
                            this.order.sva = [];
                            if (selectedOffering.sva != null) {
                                let sva = selectedOffering.sva;
                                sva.selected = true;
                                this.order.sva.push(sva);
                            }
                            this.ls.setData(this.order);
                            // alert("redireccionando a pantalla sva.... this.showAlert: " + this.showAlert);
                            this.router.navigate(['sva']);
                            // if(!this.isEmpty(this.order.sva)){
                            //     let order  = new Order();
                            //     order.status = "O";
                            //     order.user = this.order.user;
                            //     order.customer = this.order.customer;
                            //     order.product = this.order.product;
                            //     order.selectedOffering = selectedOffering;
                            //     this.ls.setData(order);
                            //     //console.log('offeringgg');
                            // //console.log(order.selectedOffering);
                            // }else{
                            //     this.order.selectedOffering = selectedOffering;
                            //     this.order.status = "O";
                            //     this.ls.setData(this.order);
                            //     //console.log('offeringgg');
                            // //console.log(this.order.selectedOffering);
                            // }


                        }


                    } else {
                        this.loading = false
                        this.showImgValidate = false;
                        this.showAlert = true;
                        this.answerValidationOffer.responseMessage = "La validacion no pudo concretarse. Intente nuevamente";
                    }

                },
                err => {
                    console.log(err)
                }
            );



        /*this.order.status = "O";
        this.order.selectedOffering = selectedOffering;
        this.order.sva = [];
        if (selectedOffering.sva != null) {
            let sva = selectedOffering.sva;
            sva.selected = true;
            this.order.sva.push(sva);
        }
        this.ls.setData(this.order);
       // console.log("order before sva : " + JSON.stringify(this.order))
        this.ls.setData(this.order);
        this.completValidate(true)*/

    }

    comparar(a, b) {
        //alert(a);
        //alert(b);
        //alert(new String(a).valueOf()===new String(b).valueOf());
        let val = new String(a).valueOf() === new String(b).valueOf();


        return val;
    }

    validateEmail(o) {
        if (!this.correoElectronicoObligatorio && (o == null || o == "")) {
            return true;
        }
        var email_regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
        if (!email_regex.test(o)) {
            return false;
        }
        else {
            return true;
        }
    }

    desmarcarSVA(objSva, num, g, cod, subsva_id) {
        if ($('#BTVCheck' + subsva_id).prop('checked')) {
            this.dropdown(objSva, num, g);
            this.validacionBTV(objSva);
            this.totales(this.sva);
        } else {
            this.quitarSVA(objSva);
            this.validacionBTV(objSva);
            this.totales(this.sva);
        }
    }

    desmarcarSVAMas(objSva, num, g, cod, subsva_id) {
        if ($('#BTVCheck' + subsva_id+'VM').prop('checked')) {
            this.dropdownMASS(objSva, num, g);
            this.validacionBTVMas(objSva);
            this.totales(this.svaMas);
        } else {
            this.quitarSVAMas(objSva);
            this.validacionBTVMas(objSva);
            this.totales(this.svaMas);
        }
    }

    validacionBTV(btv_objSva) {
        if (btv_objSva.unit == "BLOQUE FULL HD") {
            if ($('#BTVCheck' + btv_objSva.id).prop('checked')) {
                for (let i = 0; i < this.sva.length; i++) {
                    if (this.sva[i].bloquePadre == "BLOQUE FULL HD" && !this.sva[i].clickPadre) {
                        $("#" + this.sva[i].id + "_sva").hide();
                        this.quitarSVA(this.sva[i]);
                    } else {
                        $("#" + this.sva[i].id + "_sva").show();
                    }
                }
            } else {
                for (let i = 0; i < this.sva.length; i++) {
                    if (this.sva[i].bloquePadre == "BLOQUE FULL HD" && this.sva[i].clickPadre) {
                        $("#" + this.sva[i].id + "_sva").hide();
                        this.quitarSVA(this.sva[i]);
                    } else {
                        $("#" + this.sva[i].id + "_sva").show();
                    }
                }
            }
        }
    }

    validacionBTVMas(btv_objSva) {
        if (btv_objSva.unit == "BLOQUE FULL HD") {
            if ($('#BTVCheck' + btv_objSva.id + "VM").prop('checked')) {
                for (let i = 0; i < this.svaMas.length; i++) {
                    if (this.svaMas[i].bloquePadre == "BLOQUE FULL HD" && !this.svaMas[i].clickPadre) {
                        $("#" + this.svaMas[i].id + "_sva").hide();
                        this.quitarSVAMas(this.svaMas[i]);
                    } else {
                        $("#" + this.svaMas[i].id + "_sva").show();
                    }
                }
            } else {
                for (let i = 0; i < this.svaMas.length; i++) {
                    if (this.svaMas[i].bloquePadre == "BLOQUE FULL HD" && this.svaMas[i].clickPadre) {
                        $("#" + this.svaMas[i].id + "_sva").hide();
                        this.quitarSVAMas(this.svaMas[i]);
                    } else {
                        $("#" + this.svaMas[i].id + "_sva").show();
                    }
                }
            }
        }
    }

    validacionBTVNotClick(probtv_sva) {
        if (probtv_sva.unit == "BLOQUE FULL HD") {
            if (!$('#BTVCheck' + probtv_sva.id).prop('checked')) {
                for (let i = 0; i < this.sva.length; i++) {
                    if (this.sva[i].bloquePadre == "BLOQUE FULL HD" && this.sva[i].clickPadre) {
                        $("#" + this.sva[i].id + "_sva").hide();
                    }
                }
            }
        }
        return false;
    }

    validacionBTVNotClickMas(probtv_sva) {
        if (probtv_sva.unit == "BLOQUE FULL HD") {
            if (!$('#BTVCheck' + probtv_sva.id + "VM").prop('checked')) {
                for (let i = 0; i < this.svaMas.length; i++) {
                    if (this.svaMas[i].bloquePadre == "BLOQUE FULL HD" && this.svaMas[i].clickPadre) {
                        $("#" + this.svaMas[i].id + "_sva").hide();
                    }
                }
            }
        }
        return false;
    }

    validacionBTVCosto0(btv_sva) {
        let flag = false;
        for (let i = 0; i < btv_sva.length; i++) {
            if ($('#BTVCheck' + btv_sva[i].id).prop('checked') && btv_sva[i].cost == "0") {
                flag = true;
            }
            if (flag) {
                if (btv_sva[i].bloquePadre == "BLOQUE FULL HD" && !btv_sva[i].clickPadre) {
                    $("#" + btv_sva[i].id + "_sva").hide();
                }
            }
        }
        return flag;
    }

    validacionBTVCosto0Mas(btv_sva) {
        let flag = false;
        for (let i = 0; i < btv_sva.length; i++) {
            if ($('#BTVCheck' + btv_sva[i].id + "VM").prop('checked') && btv_sva[i].cost == "0") {
                flag = true;
            }
            if (flag) {
                if (btv_sva[i].bloquePadre == "BLOQUE FULL HD" && !btv_sva[i].clickPadre) {
                    $("#" + btv_sva[i].id + "_sva").hide();
                }
            }
        }
        return flag;
    }

    validacionBTVCostoPositivo(btv_sva) {
        let flag = false;
        for (let i = 0; i < btv_sva.length; i++) {
            if (btv_sva[i].unit == "BLOQUE FULL HD" && btv_sva[i].selected) {
                flag = true;
            }
            if (flag) {
                if (btv_sva[i].bloquePadre == "BLOQUE FULL HD" && !btv_sva[i].clickPadre) {
                    $("#" + btv_sva[i].id + "_sva").hide();
                }
            }
        }
        flag = false;
        return flag;
    }

    validacionBTVCostoPositivoMas(btv_sva) {
        let flag = false;
        for (let i = 0; i < btv_sva.length; i++) {
            if (btv_sva[i].unit == "BLOQUE FULL HD" && btv_sva[i].selected) {
                flag = true;
            }
            if (flag) {
                if (btv_sva[i].bloquePadre == "BLOQUE FULL HD" && !btv_sva[i].clickPadre) {
                    $("#" + btv_sva[i].id + "_sva").hide();
                }
            }
        }
        flag = false;
        return flag;
    }

    quitarSVA(objSva) {
        for (let i = 0; i < this.sva.length; i++) {
            if (objSva.selected == true && this.sva[i].id == objSva.id) {
                this.sva[i].selected = null;
            }
        }

    }

    quitarSVAMas(objSva) {
        for (let i = 0; i < this.svaMas.length; i++) {
            if (objSva.selected == true && this.svaMas[i].id == objSva.id) {
                this.svaMas[i].selected = null;
            }
        }

    }

    eliminarSva(codigo, obligatorio, objSva) {
        console.log('eliminarSva')
        let puntero = 0;
        if (obligatorio && objSva.cost == 0) {
            //si es que el sva es obligatorio... entonces no eliminamos el sva
            return;
        }

        if (objSva.unit == "BLOQUE FULL HD" && objSva.cost != 0) {
            if ($('#BTVCheck' + objSva.id).prop('checked')) {
                this.quitarSVA(objSva);
                for (let i = 0; i < this.sva.length; i++) {
                    if (this.sva[i].bloquePadre == "BLOQUE FULL HD" && this.sva[i].clickPadre) {
                        $("#" + this.sva[i].id + "_sva").hide();
                        this.quitarSVA(this.sva[i]);
                    } else {
                        $("#" + this.sva[i].id + "_sva").show();
                    }
                }
            }
        } else {
            for (let i = 0; i < this.sva.length; i++) {
                if (this.sva[i].id == objSva.id) {
                    this.sva[i].selected = null;
                    i = this.sva.length + 1;
                    puntero = objSva.id;
                }
            }
        }

        //recalculamos el subtotal
        this.totales(this.sva);
        if (objSva.code != "BTV") {
            this.resetSva2(objSva.code)
        } else {
            $("#" + objSva.code + "Check" + puntero).prop('checked', false);
        }
    }

    totales(sva) {
        let total = 0;
        for (let i = 0; i < this.sva.length; i++) {
            if (this.sva[i].selected) {
                total += parseFloat(this.sva[i].cost);
            }
        }
        console.log("Monto Total SVA: " + total);
        for (let i = 0; i < this.svaMas.length; i++) {
            if (this.svaMas[i].selected) {
                total += parseFloat(this.svaMas[i].cost);
            }
        }
        this.svaUni = this.sva.concat(this.svaMas)
        console.log("Monto Total svaMas: " + total);
        this.sumaSvas = +(Math.fround(total * 100) / 100).toFixed(2);
    }

    deleteSpaceEmail(event) {
        //console.log(event);
        $.trim($('#email').val());


        this.checkEmailArroba()

        $('#email').mailtip({

        });

    }

    checkEmailArroba() {
        'use strict';
        (function ($) {
            // invalid email char test regexp
            var INVALIDEMAILRE = /[^\u4e00-\u9fa5_a-zA-Z0-9]/;
            // is support oninput event
            var hasInputEvent = 'oninput' in document.createElement('input');
            // is ie 9
            var ISIE9 = /MSIE 9.0/i.test(window.navigator.appVersion || window.navigator.userAgent);

            /**
             * is a number
             * @param value
             * @returns {boolean}
             */
            function isNumber(value) {
                return typeof value === 'number' && isFinite(value);
            }

            /**
             * create popup tip
             * @param input
             * @param config
             * @returns {*}
             */
            function createTip(input, config) {
                var tip = null;

                // only create tip and binding event once
                if (!input.data('data-mailtip')) {
                    var wrap = input.parent();

                    // set parent node position
                    !/absolute|relative/i.test(wrap.css('position')) && wrap.css('position', 'relative');
                    // off input autocomplete
                    input.attr('autocomplete', 'off');

                    var offset = input.offset();
                    var wrapOffset = wrap.offset();

                    tip = $('<ul class="ui-mailtip" style="display: none; float: none; '
                        + 'margin: 0; padding: 0; z-index: '
                        + config.zIndex + '"></ul>');

                    // insert tip after input
                    input.after(tip);

                    // set tip style
                    tip.css({
                        top: offset.top - wrapOffset.top + input.outerHeight() + config.offsetTop,
                        left: offset.left - wrapOffset.left + config.offsetLeft,
                        width: config.width === 'input' ? input.outerWidth() - tip.outerWidth() + tip.width() : config.width
                    });

                    // when width is auto, set min width equal input width
                    if (config.width === 'auto') {
                        tip.css('min-width', input.outerWidth() - tip.outerWidth() + tip.width());
                    }

                    // binding event
                    tip.on('mouseenter mouseleave click', 'li', function (e) {
                        var selected = $(this);

                        switch (e.type) {
                            case 'mouseenter':
                                selected.addClass('hover');
                                break;
                            case 'click':
                                var mail = selected.attr('title');

                                input.val(mail).focus();
                                config.onselected.call(input[0], mail);
                                break;
                            case 'mouseleave':
                                selected.removeClass('hover');
                                break;
                            default:
                                break;
                        }
                    });

                    // when on click if the target element not input, hide tip
                    $(document).on('click', function (e) {
                        if (e.target === input[0]) return;

                        tip.hide();
                    });

                    input.data('data-mailtip', tip);
                }

                return tip || input.data('data-mailtip');
            }

            /**
             * create mail list item
             * @param value
             * @param mails
             * @returns {*}
             */
            function createItems(value, mails) {
                var mail;
                var domain;
                var items = '';
                var atIndex = value.indexOf('@');
                var hasAt = atIndex !== -1;

                if (hasAt) {
                    domain = value.substring(atIndex + 1);
                    value = value.substring(0, atIndex);
                }

                for (var i = 0, len = mails.length; i < len; i++) {
                    mail = mails[i];

                    if (hasAt && mail.indexOf(domain) !== 0) continue;

                    items += '<li title="' + value + '@' + mail + '"><p>' + value + '@' + mail + '</p></li>';
                }

                // active first item
                return items.replace('<li', '<li class="active"');
            }

            /**
             * change list active state
             * @param tip
             * @param up
             */
            function changeActive(tip, up) {
                var itemActive = tip.find('li.active');

                if (up) {
                    var itemPrev = itemActive.prev();

                    itemPrev = itemPrev.length ? itemPrev : tip.find('li:last');
                    itemActive.removeClass('active');
                    itemPrev.addClass('active');
                } else {
                    var itemNext = itemActive.next();

                    itemNext = itemNext.length ? itemNext : tip.find('li:first');
                    itemActive.removeClass('active');
                    itemNext.addClass('active');
                }
            }

            /**
             * toggle tip
             * @param tip
             * @param value
             * @param mails
             */
            function toggleTip(tip, value, mails) {
                var atIndex = value.indexOf('@');

                // if input text is empty or has invalid char or begin with @ or more than two @, hide tip
                if (!value
                    || atIndex === 0
                    || atIndex !== value.lastIndexOf('@')
                    || INVALIDEMAILRE.test(atIndex === -1 ? value : value.substring(0, atIndex))) {
                    tip.hide();
                } else {
                    var items = createItems(value, mails);

                    // if has match mails show tip
                    if (items) {
                        tip.html(items).show();
                    } else {
                        tip.hide();
                    }
                }
            }

            /**
             * exports
             * @param config
             * @returns {*}
             */
            $.fn.mailtip = function (config) {
                var defaults = {
                    mails: [
                        'gmail.com', 'outlook.com', 'hotmail.com'
                    ],
                    onselected: $.noop,
                    width: 'auto',
                    offsetTop: -1,
                    offsetLeft: 0,
                    zIndex: 10
                };

                config = $.extend({}, defaults, config);
                config.zIndex = isNumber(config.zIndex) ? config.zIndex : defaults.zIndex;
                config.offsetTop = isNumber(config.offsetTop) ? config.offsetTop : defaults.offsetTop;
                config.offsetLeft = isNumber(config.offsetLeft) ? config.offsetLeft : defaults.offsetLeft;
                config.onselected = $.isFunction(config.onselected) ? config.onselected : defaults.onselected;
                config.width = config.width === 'input' || isNumber(config.width) ? config.width : defaults.width;

                return this.each(function () {
                    // input
                    var input = $(this);
                    // tip
                    var tip = createTip(input, config);

                    // binding key down event
                    input.on('keydown', function (e) {
                        // if tip is visible do nothing
                        if (tip.css('display') === 'none') return;

                        switch (e.keyCode) {
                            // backspace
                            case 8:
                                // shit! ie9 input event has a bug, backspace do not trigger input event
                                if (ISIE9) {
                                    input.trigger('input');
                                }
                                break;
                            // tab
                            case 9:
                                tip.hide();
                                break;
                            // up
                            case 38:
                                e.preventDefault();
                                changeActive(tip, true);
                                break;
                            // down
                            case 40:
                                e.preventDefault();
                                //changeActive(tip);
                                break;
                            // enter
                            case 13:
                                e.preventDefault();

                                var mail = tip.find('li.active').attr('title');

                                input.val(mail).focus();
                                tip.hide();
                                config.onselected.call(this, mail);
                                break;
                            default:
                                break;
                        }
                    });

                    // binding input or propertychange event
                    if (hasInputEvent) {
                        input.on('input', function () {
                            toggleTip(tip, this.value, config.mails);
                        });
                    } else {
                        input.on('propertychange', function (e) {
                            if (e.originalEvent.propertyName === 'value') {
                                toggleTip(tip, this.value, config.mails);
                            }
                        });
                    }

                    // shit! ie9 input event has a bug, backspace do not trigger input event
                    if (ISIE9) {
                        input.on('keyup', function (e) {
                            if (e.keyCode === 8) {
                                toggleTip(tip, this.value, config.mails);
                            }
                        });
                    }
                });
            };
        }($));

    }

    obtenerActual() {
        if (this.flagMostrarActual) {
            $('#abrirResumen').removeClass('on');
            this.flagMostrarActual = false;
        } else {
            $('#abrirResumen').addClass('on');
            this.flagMostrarActual = true;
        }
    }


    completeDataSVA(sva) {
        console.log('SVA List Basica SVAMAS' + sva);

        let temp = {};
        let groupSVAMAS = [];

        if (this.order.sva != null) {
            for (let i = 0; i < this.order.sva.length; i++) {
                if (this.order.sva[i].svaMass) {
                this.svaU = this.order.sva[i];
                this.mapSvaMAS.set(this.svaU.code + this.svaU.unit + this.svaU.id, this.svaU);
            }
            }

            //no es optimo... pero no hay de otra x el momento...
            if (this.type != globals.ORDER_SVA) {
                let defaultSvas = JSON.parse(localStorage.getItem("defaultSvas"));
                for (let i = 0; i < defaultSvas.length; i++) {
                    this.defaultMapSva.set(defaultSvas[i].code, "SI");
                }
            }
        }



        let groupCount = -1;

        //console.log("--- MAXIMUM ---");
        //console.log(this.maximunDecos);

        for (let i = 0; i < this.svaMas.length; i++) {
            let svaObject = this.svaMas[i];
            let svaOb
            if (!temp[svaObject.code]) {


                groupCount++;
                this.selectedSvaMAS[groupCount] = 0;
                let obj = { cod: svaObject.code, label: svaObject.description, arr: [], type: svaObject.type, selected: false };
                temp[svaObject.code] = obj;
                groupSVAMAS.push(obj);
                //determinamos si es que es un sva obligatorio (en caso de migras)
                if (this.defaultMapSvaMAS.get(svaObject.code) != null) {
                    svaObject.obligatorio = true;
                    obj.selected = true;
                } else {
                    svaObject.obligatorio = false;
                    obj.selected = false;
                }
                if (this.mapSvaMAS.get(svaObject.code + svaObject.unit + svaObject.id) != null) {
                    svaObject.selected = true;
                    obj.selected = true;
                    this.selectedSvaMAS[groupCount] = svaObject;

                    //Sprint-11 Validacion y comparación de sva 0 en vista y backend.
                    if (this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA || this.order.type == globals.ORDER_SVA) {
                        var validateZero = this.mapSvaMAS.get(svaObject.code + svaObject.unit + svaObject.id);
                        if (validateZero.cost == "0") {
                            this.selectedSvaMAS[groupCount].cost = "0";
                        }
                    }


                }
                //para el caso de alta pura:
                //para alta pura verificamos si es que el sva es un sva por default
                if (svaObject.altaPuraDefaultSVA) {
                    svaObject.obligatorio = true;
                    obj.selected = true;
                    this.selectedSvaMAS[groupCount] = svaObject;
                }

                obj.arr.push(svaObject);

            } else {
                let obj = temp[svaObject.code];
                //determinamos si es que es un sva obligatorio (en caso de migras)
                if (this.defaultMapSvaMAS.get(svaObject.code) != null) {
                    svaObject.obligatorio = true;
                    obj.selected = true;
                } else {
                    svaObject.obligatorio = false;
                    obj.selected = false;
                }
                if (this.mapSvaMAS.get(svaObject.code + svaObject.unit + svaObject.id) != null) {
                    svaObject.selected = true;
                    obj.selected = true;
                    this.selectedSvaMAS[groupCount] = svaObject;
                }
                //para el caso de alta pura:
                //para alta pura verificamos si es que el sva es un sva por default
                if (svaObject.altaPuraDefaultSVA) {
                    svaObject.obligatorio = true;
                    obj.selected = true;
                    this.selectedSvaMAS[groupCount] = svaObject;
                }
                obj.arr.push(svaObject);
            }
        }
        for (const gm of groupSVAMAS) {
            for (const value of gm.arr) {
                value.svaMass = true
            }
        }
        this.groupSVA = groupSVAMAS;
        console.log('groupSVA', this.groupSVA)

        //20180323 fix para mostrar el precio total incluyendo el precio de los sva por default... y para mostrar los divs de svas default
        //$('#emailDiv').hide();
        for (let i = 0; i < this.selectedSvaMAS.length; i++) {
            var groupItemSeleccionado = null;
            if (this.selectedSvaMAS[i].code) {
                groupItemSeleccionado = groupSVAMAS[i];
            }
            this.dropdownMASS(this.selectedSvaMAS[i], 1, groupItemSeleccionado);

            if (this.selectedSvaMAS[i].code == "BTV" && this.selectedSvaMAS[i].cost == "0" && this.order.type == globals.ORDER_ALTA_NUEVA) {
                this.defaultMapSvaMAS.set(this.selectedSvaMAS[i].code, "SI");
            }
            if (this.selectedSvaMAS[i].code == "SVAI" && this.selectedSvaMAS[i].cost == "0" && this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                this.defaultMapSvaMAS.set(this.selectedSvaMAS[i].code, "SI");
            }
            if (this.selectedSvaMAS[i].code == "SVAL" && this.selectedSvaMAS[i].cost == "0" && this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                this.defaultMapSvaMAS.set(this.selectedSvaMAS[i].code, "SI");
            }

        }

        //console.log("hola sva "+ (<HTMLSelectElement>document.getElementById('DSHD')).value);
        /*var removegroup = [];

        for (let adicional of this.groupSVA) {

            var groupfiltered = this.group.filter(item => { return adicional.cod == item.cod; });

            if (groupfiltered.length > 0) {
                var group_base = groupfiltered[0];
                var removepos = [];
                for (let listaadic of adicional.arr) {
                    var producto_repetido = group_base.arr.filter(item => { return item.id == listaadic.id; });
                    if (producto_repetido.length > 0) {
                        removepos.push(producto_repetido[0].id);
                    }
                }

                for (let val of removepos) {
                    for (var i = adicional.arr.length - 1; i > -1; i--) {
                        if (adicional.arr[i].id === val) {
                            adicional.arr.splice(i, 1);
                        }
                    }
                }

                if (adicional.arr.length == 0) {
                    removegroup.push(adicional.cod);
                }
            }
        }

        for (let val of removegroup) {
            for (var i = this.groupSVA.length - 1; i > -1; i--) {
                if (this.groupSVA[i].cod === val) {
                    this.groupSVA.splice(i, 1);
                }
            }
        }*/
    }







    resetSvaMAS(cod, element, groupItem, selectedSva) {

        //console.log(cod);
        //console.log(element);
        //console.log(element.checked);

        groupItem.selected = false;

        if (element.checked) return false;


        $('#' + cod + 'VM').prop('selectedIndex', -1);

        this.correoElectronicoObligatorio = false;
        if (this.correoElectronico != this.order.customer.email) {
            //Yes
        } else {
            this.correoElectronico = this.order.customer.email;
        }
        this.eliminarSvaMAS(cod, "", selectedSva);
        $('#' + cod + 'VM').removeClass("has-content");
        $('#' + cod + 'CostVM').hide();
        $("#" + cod + "CheckVM").prop('disabled', true);
        $("#" + cod + "CheckVM").prop('checked', false);
        switch (cod) {
            case 'SVAL':
                this.cerrarCard2('sva_linea_', cod);
                break;
            case 'SVAI':
                this.cerrarCard2('sva_internet_', cod);
                break;
            case 'DSHD':
                this.cerrarCard2('sva_smart_', cod);
                $('#maxDecosWarning').hide();
                break;
            case 'DVR':
                this.cerrarCard2('sva_dvr_', cod);
                $('#maxDecosWarning').hide();
                break;
            case 'DHD':
                this.cerrarCard2('sva_hd_', cod);
                $('#maxDecosWarning').hide();
                break;
            case 'BTV':
                this.cerrarCard2('sva_tv_', cod);
                break;
            case 'BP':
                this.cerrarCard2('sva_bp_', cod);
                break;
        }
    }

    eliminarSvaMAS(codigo, obligatorio, objSva) {
        console.log('eliminarSvaMAS')
        let puntero = 0;
        if (obligatorio && objSva.cost == 0) {
            //si es que el sva es obligatorio... entonces no eliminamos el sva
            return;
        }

        if (objSva.unit == "BLOQUE FULL HD" && objSva.cost != 0) {
            if ($('#BTVCheck' + objSva.id).prop('checked')) {
                this.quitarSVA(objSva);
                for (let i = 0; i < this.svaMas.length; i++) {
                    if (this.svaMas[i].bloquePadre == "BLOQUE FULL HD" && this.svaMas[i].clickPadre) {
                        $("#" + this.svaMas[i].id + "_sva").hide();
                        this.quitarSVA(this.svaMas[i]);
                    } else {
                        $("#" + this.svaMas[i].id + "_sva").show();
                    }
                }
            }
        } else {
            for (let i = 0; i < this.svaMas.length; i++) {
                if (this.svaMas[i].id == objSva.id) {
                    this.svaMas[i].selected = null;
                    i = this.svaMas.length + 1;
                    puntero = objSva.id;
                }
            }
        }

        //recalculamos el subtotal

        this.totales(this.svaMas);
        if (objSva.code != "BTV") {
            this.resetSva2Mas(objSva.code)
        } else {
            $("#" + objSva.code + "CheckVM" + puntero).prop('checked', false);
        }
    }


    dropdownMASS(sva, flag, groupItem) {

        if (groupItem != null) {
            groupItem.selected = true;
        }

        $("#" + sva.code + "CheckVM").prop('checked', true);
        $("#" + sva.code + "CheckVM").prop('disabled', false);
        $('#' + sva.code + 'CostVM').show();
        if (sva.code != undefined) {
            this.dataCode = sva.code + '';
        }
        if (sva.code == "DSHD") {
            if ($("#DSHDVM").val() == "1: Object" || $("#DSHDVM").val() == "2: Object" || $("#DSHDVM").val() == "0: Object") {
                $('#email').show();
                $('#emailLabel').show();
                $('#emailDiv').show();
                //correo obligatorio
                this.correoElectronicoObligatorio = true;
                if (this.correoElectronico != this.order.customer.email) {
                    //Yes
                } else {
                    this.correoElectronico = this.order.customer.email;
                }
                // no funciona el binding de angular... usamos jquery para asegurarnos el seteo
                $('#email').val(this.correoElectronico);
            }
        } else if (sva.code == undefined) {
            $('#email').hide();
            $('#emailLabel').hide();
            $('#emailDiv').hide();
            //correo opcional
            this.correoElectronicoObligatorio = false;
            this.correoElectronico = this.order.customer.email;
            $("#" + this.dataCode + "CheckVM").prop('checked', false);
            $("#" + this.dataCode + "CheckVM").prop('disabled', true);
            $('#' + this.dataCode + "VM").removeClass("has-content");
        }

        this.countDeco = 0;
        this.showMessageDecos = false;
        //console.log("cantidad de sva " + this.sva.length);

        // Sprint 6 - evaluar el sva decosmarthd
        this.evaluateSvaDecoSmartHD(sva);

        for (let i = 0; i < this.svaMas.length; i++) {
            this.svaMas[i].code;
            if (this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                if (this.svaMas[i].id == sva.id && flag == 1) {
                    if ((this.svaMas[i].code == "DHD" || this.svaMas[i].code == "DSHD" || this.svaMas[i].code == "DVR")) {
                    }
                    this.svaMas[i].selected = true;
                } else if (sva.selected == false && this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                    $('#' + sva.code + 'VM').closest("td").next().empty();
                    this.showMessageDecos = false;
                    if ((sva.code == this.svaMas[i].code)) {
                        this.svaMas[i].selected = null;
                    }
                } else if (this.svaMas[i].id == sva.id && sva.selected == true && flag == 2) {
                    this.svaMas[i].selected = true;
                } else {
                    if (this.svaMas[i].code == sva.code) {
                        if (sva.code != "BTV") {
                            this.svaMas[i].selected = null;
                        }
                    }
                }
            }
            if (this.order.type == globals.ORDER_ALTA_NUEVA || this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA
                || this.order.type == globals.ORDER_SVA) {
                if (this.svaMas[i].id == sva.id && sva.selected == true && flag == 2) {
                    this.svaMas[i].selected = true;
                } else if (this.svaMas[i].id == sva.id && flag == 1) {
                    this.svaMas[i].selected = true;
                    var code = this.svaMas[i].code;
                    var desc = this.svaMas[i].description;
                    var unit = this.svaMas[i].unit;
                    var pago = 0;
                    var resumen = JSON.parse(localStorage.getItem('resumen'));
                    if (this.order.type == globals.ORDER_SVA && this.countSVA == 0) {
                        this.precionormal = 0;
                        this.preciopromocional = 0;
                        this.countSVA = this.countSVA + 1;
                    }
                }
                if (this.svaMas[i].code == sva.code && this.svaMas[i].id != sva.id) {
                    if (sva.code != "BTV") {
                        this.svaMas[i].selected = null;
                    }
                } else if (sva.selected == false && this.svaMas[i].code == sva.code) {
                    this.svaMas[i].selected = null;
                }
            }

            if (this.order.type == globals.ORDER_ALTA_NUEVA || this.order.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
                if ((this.svaMas[i].code == "DHD" || this.svaMas[i].code == "DSHD" || this.svaMas[i].code == "DVR") && this.svaMas[i].selected == true) {
                    this.countDeco = this.countDeco + Number(this.svaMas[i].unit);

                    if (this.countDeco > this.maximunDecos) {
                        this.numMaxDecos = this.maximunDecos;
                        this.showMessageDecos = true;
                    } else {

                        this.showMessageDecos = false;
                    }
                }
            }
            //FIN DE OPCION DE MIGRACION
            //this.validacionBTV(this.sva[i]);
        }


        this.totales(this.svaMas);

        if (this.order.type == globals.ORDER_SVA) {

            let maxDecos: number = -1;
            let decosSele: number = 0;
            this.showMessageDecos = false;

            this.hasEquipment = false;
            this.svaMas.forEach((item, index) => {
                if (item.selected == true) {
                    if (!this.hasEquipment) {
                        this.hasEquipment = item.hasEquipment == "1" ? true : false;
                    }
                    if (item.type == 'INT') {
                        if (maxDecos < 0) {

                            maxDecos = Number(item.maxDecos);
                            this.numMaxDecos = maxDecos;
                        } else {
                        }

                        if (Number(item.unit) > 0) {
                            decosSele = Number(item.unit) + decosSele;
                            if (decosSele > maxDecos) {
                                this.showMessageDecos = true;
                            } else {
                            };
                        }
                    }

                }
            });
        }
    }

}