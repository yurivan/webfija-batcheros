import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DireccionService } from '../services/direccion.service';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order';
import { Customer } from '../model/customer';
import { LocalStorageService } from '../services/ls.service';
import * as globals from '../services/globals';
var $ = require('jquery');
import { ProgressBarService } from '../services/progressbar.service';
(<any>window).jQuery = $

import { global } from '@angular/core/src/facade/lang';

class RequestDepartment {
	id: string
	value: any
}

@Component({
    moduleId: 'validacionOut',
    selector: 'validacionOut',
    templateUrl: './validacionOut.template.html',
    providers: [DireccionService, CustomerService, OrderService]
})

export class ValidacionOutComponent implements OnInit {

    direccionService: DireccionService;
    customerService: CustomerService;

    objDepart: any;
    private keyDeparts: string[];
    private departSeleccionado: any = '';
    selectDepart: any;


    objProvs: any;
    showProvs = new Array();
    private keyProvs: string[];
    selectProv: any;
    private proviSeleccionado: any = '';

    objDists: any;
    showDists = new Array();
    selectDistric: any;
    private keyDistrs: string[];
    private districSeleccionado: any = '';
    months = new Array()
    listErrorMessage = new Array()

    objReniec: any;
    distModel: any;
    provModel: any;
    order: Order;
    customer: Customer;
    customerReniec: Customer;
    mensajeValidacion: String;
    channelCall: boolean;

    private loadError: boolean;
    private loading: boolean;
    private mensaje_error: String;
    cargaDept: boolean;

    private validAttempts: number;
    private reniecValidAttempts: number;
    private reniecMaxErrors: number;
    webOrder: any;

    //glazaror inicio variables para parentesco
    objParentesco: any;
    private keyParentescoList: string[];
    private parentescoSeleccionado: any;
    selectParentesco: any;

    parentescoModel: any;
    //glazaror fin variables para parentesco

    mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios
    isCheckedNameUser: boolean
    isCheckedPlaceOfBirth: boolean
    isCheckedDayOfBirth: boolean
    isItOkMothersName: boolean
    mothersName: boolean
    isValidationCompleted: boolean

    objfecnac: any;
    contValidation: number
    modalHome: boolean
    contador: number = 0;
    padrecontingencia: string = ''
    madrecontingencia: string = ''
    nombrecontingencia: string = ''
    depnacimiento: string = ''
    provnacimiento: string = ''
    distnacimiento: string = ''
    fechanacimiento: string = ''
    apellidoscontingencia: string = ''
    valorCondicional: string = ''
    cantidadIntentos: number=2;

    constructor(private router: Router,
        direccionService: DireccionService,
        customerService: CustomerService,
        private ls: LocalStorageService,
        private orderService: OrderService,
        private progresBar: ProgressBarService) {

        this.contValidation = 0
        this.router = router;
        this.direccionService = direccionService;
        this.customerService = customerService;
        this.ls = ls;
        this.cargaDept = false;
        this.distModel = 0;
        this.provModel = 0;
        this.parentescoModel = 0; //glazaror
        this.loadError = false;
        this.mensaje_error = "";
        this.validAttempts = 0;
        this.reniecValidAttempts = globals.RENIEC_VALID_ATTEMPTS;
        this.reniecMaxErrors = globals.RENIEC_MAX_ERRORS;

        this.orderService = orderService;
        this.objReniec = {}
        this.mensajeValidacionDatosCliente = "";// Sprint 6
        this.isCheckedNameUser = true
        this.isCheckedPlaceOfBirth = true
        this.isCheckedDayOfBirth = true
        this.isValidationCompleted = false
        this.isItOkMothersName = false
        this.modalHome = false
        this.months = [
            "Enero",
            "Febrero",
            "Marzo",
            "Abril",
            "Mayo",
            "Junio",
            "Julio",
            "Agosto",
            "Septiembre",
            "Octubre",
            "Noviembre",
            "Diciembre"]

        this.obtenerDepartamentos();
        this.obtenerProvincias();
        this.obtenerDistritos();
        this.obtenerIntentosReniec();
    }
        
    obtenerIntentosReniec(){
        var _scope = this;
        this.customerService.getIntentosReniec().subscribe(
            data => {
                _scope.cantidadIntentos=data.responseData.value;            
            });
    }

    ngOnInit() {
        $(document).ready(() => {
            $(".paraselect select").val("");
            $(".paraselect select").focusout(function () {
                if ($(this).val() != "") {
                    $(this).addClass("has-content");
                } else {
                    $(this).removeClass("has-content");
                }
            })

        })
        this.progresBar.getProgressStatus()
        // add loading 
        this.loading = true;

        this.obtenerDepartamentos();
        //sprint2 no se debe mostrar provincia ni distrito
        this.order = JSON.parse(localStorage.getItem('order'))
        this.customer = this.order.customer
        this.channelCall = true;
        // Sprint 6 - validacion RENIEC ahora tambien se muestra para los vendedores con perfil TIENDA
        if (this.customer) {
            this.obtainReniec();
        } else {
            this.continueSale();
        }
    }

    convertirInfoListaReniec() {

        let fecNac: String;
        let fechaExp: String;

        if (this.objReniec != null) {
            fecNac = this.objReniec.birthdate;
            this.objReniec.birthdate = fecNac.substring(6, 8) + "/" + fecNac.substring(4, 6) + "/" + fecNac.substring(0, 4);

            fechaExp = this.objReniec.expeditionDate;
            this.objReniec.expeditionDate = fechaExp.substring(6, 8) + "/" + fechaExp.substring(4, 6) + "/" + fechaExp.substring(0, 4);

            this.obtainNameDep(this.objReniec.departmentBirthLocation);

        }

    }

    obtenerDepartamentos() {
		this.objDepart = this.direccionService.getDataDepartamentoReniec()
		let arrayDepartments: RequestDepartment[] = Array()
		this.keyDeparts = []
		for (let i in this.objDepart) {
			let obj = new RequestDepartment()
			obj.id = i
			obj.value = this.objDepart[i]
			arrayDepartments.push(obj)
		}
		arrayDepartments.sort(this.sortByTwoProperty())
		for (let i = 0; i < arrayDepartments.length; i++) {
			this.keyDeparts.push(arrayDepartments[i]["id"])
		}
	}

	obtenerProvincias() {
		this.objProvs = this.direccionService.getDataProvinciasReniec();
    }
    
    obtenerDistritos() {
		this.objDists = this.direccionService.getDataDistritosReniec();
		console.log("this.objDists::::: " + this.objDists);
    }
    
	listarProvincias(skuDep) {
		this.showProvs.length = 0;
		this.keyProvs = Object.keys(this.objProvs);
		this.keyProvs.forEach((item, index) => {
			if (parseInt(this.objProvs[item].skuDep) == parseInt(skuDep)) {
				this.showProvs.push(this.objProvs[item]);
			}
		})
		this.showProvs.sort(this.sortByProperty('name'))
    }
    
	listarDistritos(skuDepPro) {

		this.showDists.length = 0;
        this.keyDistrs = Object.keys(this.objDists);
		this.keyDistrs.forEach((item, index) => {
			if (parseInt(this.objDists[item].SkuDepPro) == parseInt(skuDepPro)) {
                this.showDists.push(this.objDists[item]);
			}
		})
        this.showDists.sort(this.sortByProperty("name"))

    }

    selDepart2() {
		this.selectDepart = document.getElementById("department");
		this.departSeleccionado = this.selectDepart.value;
		this.listarProvincias(this.departSeleccionado);
		this.limpiarDireccion('department');
        this.validateCont()
	}

	selCity() {
		this.selectProv = document.getElementById("city");
		this.proviSeleccionado = this.selectProv.value;
		this.listarDistritos(this.departSeleccionado + this.proviSeleccionado);
		this.limpiarDireccion('province');
        this.validateCont()
	}

	selDistrict() {
		this.selectDistric = document.getElementById("district");
        this.districSeleccionado = this.selectDistric.value;
        this.validateCont()
    }
    
    limpiarDireccion(tipo) {
		if ('department' == tipo) {
			this.selectProv = document.getElementById("city");
			this.selectDistric = document.getElementById("district");
			this.selectProv.value = "";
			this.selectDistric.value = "";

			this.proviSeleccionado = 0;
			this.districSeleccionado = 0;
			this.listarDistritos(0);
		} else if ('province' == tipo) {
			this.selectDistric = document.getElementById("district");
			this.selectDistric.value = "";
			this.districSeleccionado = 0;
		}
	}
    sortByProperty = (property) => {
		return (x, y) => {
			return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
		}
	}
    sortByTwoProperty = () => {
		return (x, y) => {
			return ((x["value"]["name"] === y["value"]["name"]) ? 0 : ((x["value"]["name"] > y["value"]["name"]) ? 1 : -1));
		}
    }
    
    validateCont(){
        console.log(this.depnacimiento);
        console.log(this.provnacimiento);
        console.log(this.distnacimiento);
        
        this.depnacimiento=this.departSeleccionado
        this.provnacimiento=this.proviSeleccionado
        this.distnacimiento=this.districSeleccionado

        this.fechanacimiento = $("#date").val()
        if(this.nombrecontingencia!='' && this.padrecontingencia!='' && this.madrecontingencia!=''
            && this.fechanacimiento!='' && this.distnacimiento!='' && this.provnacimiento!='' && this.depnacimiento!=''){
                this.isValidationCompleted = true
        }else{
            this.isValidationCompleted = false
        }
    }

    //glazaror inicio nueva funcion para cargar parentesco
    obtenerListaParentesco(isMother) {
        let parentescoElement: any;

        if (isMother) {
            this.keyParentescoList = this.objReniec.motherNameAlternatives;
        } else {
            this.keyParentescoList = this.objReniec.fatherNameAlternatives;
        }
        this.keyParentescoList.splice(0, 1);
        //glazaror
        parentescoElement = document.getElementById("parentescoSelect");
    }
    //glazaror fin nueva funcion para cargar parentesco

    obtainNameDep(sku) {
        this.keyDeparts.forEach((item, index) => {
            if (sku != null) {
                if (sku !== 'undefined') {
                    if (this.objDepart[item].sku == parseInt(sku).toString()) {
                        this.objReniec.departmentName = (this.objDepart[item].name);
                    }
                }
            }
        });
    }

    selDepart() {
        if (this.customer.birthDep == null) {
            this.selectDepart = document.getElementById("department");
            if (this.selectDepart) {
                this.departSeleccionado = this.selectDepart.value;
                this.obtainNameDep(this.objReniec.departmentBirthLocation);
            } else {
                this.obtainNameDep(this.objReniec.departmentBirthLocation);
            }
        } else {
            this.departSeleccionado = this.customer.birthDep
            this.obtainNameDep(this.objReniec.departmentBirthLocation);
        }
    }

    obtainReniec() {

        this.loading = true;
        //console.log("obtain REniec " + JSON.stringify(this.customer))
        let resultDoc = this.customer.documentNumber
        console.log('Doc Number:' +resultDoc);
        this.valorCondicional = resultDoc.substring(0, 2)

        if(this.customer.documentType == 'RUC' && (this.valorCondicional == '20' || this.valorCondicional == '15' || this.valorCondicional == '17')){
            this.customerService.getIdentifyRuc(this.customer, this.order.id)
            .subscribe(
                data => {
                    if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
                        // add data perfil tienda
                        this.objReniec = data.responseData;

                        //console.log("objeto Reniec " + JSON.stringify(this.objReniec))
                        ////console.log("objReniec " + JSON.stringify(this.objReniec))

                        // //console.log("mother " + JSON.stringify((this.objReniec)))
                        this.obtenerListaParentesco(true);//glazaror carga de parentesco
                        /*if (this.order.user.typeFlujoTipificacion == "PRESENCIAL") {
                            this.continueSale();
                        }*/
                        ////console.log(this.objReniec);
                    } else {
                        this.mostrarError();
                    }
                },
                err => {
                    //console.log(err);
                    this.mostrarError();
                },
                () => {

                    if ("birthdate" in this.objReniec) {
                        //console.log("objReniec " + JSON.stringify(this.objReniec))
                        this.customer.birthDate = this.objReniec.birthdate.substring(0, 4) + "-" + this.objReniec.birthdate.substring(4, 6) + "-" + this.objReniec.birthdate.substring(6, 8)
                        ////console.log("this.customer.birthDate " + this.customer.birthDate)
                        if(this.objReniec.lastName !== null && this.objReniec.lastName !== ''){
                            this.customer.nombreCompletoRrll = this.objReniec.lastName + ' ';
                        }
                        if(this.objReniec.motherLastName !== null && this.objReniec.motherLastName !== ''){
                            this.customer.nombreCompletoRrll += this.objReniec.motherLastName + ' ';
                        }
                        if(this.objReniec.names !== null && this.objReniec.names !== ''){
                            this.customer.nombreCompletoRrll += this.objReniec.names;
                        }
                        if (this.customer.birthDate) {
                            let date1 = this.customer.birthDate
                            let p = date1.split("-")
                            let connector = "de"
                            if (parseInt(p[0]) >= 2000) {
                                connector = "del"
                            }
                            this.customer.birthDate = `${p[2]} de ${this.months[parseInt(p[1]) - 1]} ${connector} ${p[0]}`
                        } else {
                            this.customer.birthDate = "No se encontró la fecha de nacimiento."

                        }
                        this.loading = false;
                        this.loadError = false;
                        if (this.channelCall) {
                            if (this.customer.birthDep) {
                                this.selDepart();
                                //this.selCity();
                                //this.selDistrict();

                            } else {
                                this.selDepart();
                            }
                            //this.compararDatos();

                        } else {
                            this.convertirInfoListaReniec();
                            this.loading = true;
                        }
                    } else {
                        this.contador++
                        this.mostrarError();
                    }
                });
        }
        else {
        this.customerService.getIdentify(this.customer, this.order.id)
            .subscribe(
                data => {
                    if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
                        // add data perfil tienda
                        this.objReniec = data.responseData;

                        //console.log("objeto Reniec " + JSON.stringify(this.objReniec))

                        // console.log("mother " + JSON.stringify((this.objReniec)))
                        this.obtenerListaParentesco(true);//glazaror carga de parentesco
                        /*if (this.order.user.groupPermission == "TIENDA") {
                            this.continueSale();
                        }*/
                        //console.log(this.objReniec);
                    }
                },
                err => {
                   // console.log(err);
                    this.mostrarError();
                },
                () => {
                    
                    if ("birthdate" in this.objReniec) {
                        //console.log("objReniec " + JSON.stringify(this.objReniec))
                        this.customer.birthDate = this.objReniec.birthdate.substring(0, 4) + "-" + this.objReniec.birthdate.substring(4, 6) + "-" + this.objReniec.birthdate.substring(6, 8)
                        //console.log("this.customer.birthDate " + this.customer.birthDate)
                        if (this.customer.birthDate) {
                            let date1 = this.customer.birthDate
                            let p = date1.split("-")
                            let connector = "de"
                            if (parseInt(p[0]) >= 2000) {
                                connector = "del"
                            }
                            this.customer.birthDate = `${p[2]} de ${this.months[parseInt(p[1]) - 1]} ${connector} ${p[0]}`
                        } else {
                            this.customer.birthDate = "No se encontró la fecha de nacimiento."

                        }
                        this.loading = false;
                        this.loadError = false;
                        if (this.channelCall) {
                            if (this.customer.birthDep) {
                                this.selDepart();
                                //this.selCity();
                                //this.selDistrict();

                            } else {
                                this.selDepart();
                            }
                            //this.compararDatos();

                        } else {
                            this.convertirInfoListaReniec();
                            this.loading = true;
                        }
                    } else {
                        this.mostrarError();
                        this.contador++
                    }
                });
        }

    }

    cancelSale() {
        this.webOrder = {};
        this.order = this.ls.getData();
        this.webOrder.userId = this.order.user.userId;
        this.webOrder.customer = this.order.customer;
        if (this.order.product != null && this.order.type != globals.ORDER_ALTA_NUEVA) {
            this.webOrder.phone = this.order.product.phone;
            this.webOrder.department = this.order.product.department;
            this.webOrder.province = this.order.product.province;
            this.webOrder.district = this.order.product.district;
            this.webOrder.address = this.order.product.address;
            this.webOrder.latitudeInstallation = this.order.product.coordinateY ? this.order.product.coordinateY : 0;
            this.webOrder.longitudeInstallation = this.order.product.coordinateX ? this.order.product.coordinateX : 0;
            this.webOrder.addressAdditionalInfo = this.order.product.additionalAddress ? this.order.product.additionalAddress : null;
        }
        if (this.order.selectedOffering != null) {
            this.webOrder.product = this.order.selectedOffering;
        }
        this.webOrder.cancelDescription = "Validacion Reniec";
        this.webOrder.sva = [];
        this.webOrder.type = this.order.type;
        this.webOrder.disaffiliationWhitePagesGuide = this.order.disaffiliationWhitePagesGuide;
        this.webOrder.affiliationElectronicInvoice = this.order.affiliationElectronicInvoice;
        this.webOrder.affiliationDataProtection = this.order.affiliationDataProtection;
        this.webOrder.publicationWhitePagesGuide = this.order.publicationWhitePagesGuide;
        this.webOrder.shippingContractsForEmail = this.order.shippingContractsForEmail;

        if (this.order.sva != null) {
            for (let i = 0; i < this.order.sva.length; i++) {
                let sva = this.order.sva[i];
                this.webOrder.sva.push(sva.id);
            }
        }
        $('#modalHome').modal('hide');
        this.modalHome = false
        this.loading = true;
        this.orderService.cancelSale(this.webOrder).subscribe(
            data => {
                if (data.responseCode == "00") {
                    this.router.navigate(['/principalout']);
                } else {
                }
                this.loading = false;
            },
            err => {
                this.loading = false;
               // console.log(err);
            }
        );
    }

    mostrarError() {
        this.loading = false;
        this.loadError = true;
        this.mensaje_error = "El servicio Reniec dio un error, Por favor vuelva a intentarlo."
    }

    compararDatos() {

        if (this.channelCall) {

            let cont = 0;

            let cump = 0;
            let birthDate: any;
            let completeName: any;

            let parentescoElement: any;//glazaror variable parentesco

            birthDate = document.getElementById("bday");
            completeName = document.getElementById("fullname");

            //glazaror
            parentescoElement = document.getElementById("parentescoSelect");

            if (this.objReniec) {

                //glazaror se adiciona validacion de parentesco
                if (parentescoElement.value.length !== 0) {
                    parentescoElement = parentescoElement[parentescoElement.selectedIndex].text;
                    var divParentesco = document.getElementById("divParentesco");
                    if (parentescoElement == this.objReniec.motherName) {
                        cont++;

                    }
                }

                //glazaror se incrementa el numero de validaciones a 5
                if (cont >= 1 - this.reniecMaxErrors) {

                    $('#myModalReniec').modal('hide');
                    this.continueSale();

                } else {
                    this.mensajeValidacion = "Cliente no cumple con la validación. Reintentar";
                    this.validAttempts++;
                    if (this.validAttempts + 1 == this.reniecValidAttempts) {
                        this.mensajeValidacion = "No cumple con la validación, esta a punto de cerrar venta por validación. Reintentar";
                    }
                    if (this.validAttempts >= this.reniecValidAttempts) {
                        $('#modalHome').modal({ backdrop: 'static', keyboard: false, show: true });
                    }
                }
            }
        }
    }
    ok() {
        $('#myModalReniec').modal('hide');
    }

    // Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"... deberia ser una sola funcion pasandole como parametro el nombre del modal
    cerrarDialogo(nombreModal) {
        $("#" + nombreModal).modal('hide');
    }

    continueSale() {
        let fecNac: String;

        //console.log("this.objReniec " + JSON.stringify(this.objReniec))

        if(this.contador==this.cantidadIntentos){
            if (this.customer.documentType == "DNI" || (this.customer.documentType == 'RUC' && this.customer.documentNumber.substr(0,2) == '10')) {
                this.customerReniec = this.customer;
                this.customerReniec.firstName = this.nombrecontingencia
                this.customerReniec.lastName1 = this.apellidoscontingencia.split(' ')[0]
                this.customerReniec.lastName2 = this.apellidoscontingencia.split(' ')[1]
                this.customerReniec.birthDep = this.depnacimiento;
                this.customerReniec.birthCity = this.provnacimiento;
                this.customerReniec.birthDist = this.distnacimiento;
                this.customerReniec.birthDate = this.fechanacimiento;
                this.order.customer = this.customerReniec;

                
                if(this.order.offline.flag.indexOf('6') == -1){
                    this.order.offline.flag = this.order.offline.flag + (this.order.offline.flag?',6':'6');               
                }

                this.order.offline.body['nombrecontingencia'] = this.nombrecontingencia;
                this.order.offline.body['apellidoscontingencia'] = this.apellidoscontingencia;
                this.order.offline.body['depnacimiento'] = this.depnacimiento;
                this.order.offline.body['provnacimiento'] = this.provnacimiento;
                this.order.offline.body['distnacimiento'] = this.distnacimiento;
                this.order.offline.body['fechanacimiento'] = this.fechanacimiento;
                this.order.offline.body['documentType'] = this.customer.documentType
                this.order.offline.body['padrecontingencia'] = this.padrecontingencia
                this.order.offline.body['madrecontingencia'] = this.madrecontingencia
                
            }
            else if(this.customer.documentType == 'RUC' && this.customer.documentNumber.substr(0,2) == '20'){
               
                if(this.order.offline.flag.indexOf('6') == -1){
                    this.order.offline.flag = this.order.offline.flag + (this.order.offline.flag?',6':'6');               
                }
                
                this.order.offline.body['nombrecontingencia'] = this.nombrecontingencia;
                this.order.offline.body['apellidoscontingencia'] = this.apellidoscontingencia;
                this.order.offline.body['depnacimiento'] = this.depnacimiento;
                this.order.offline.body['provnacimiento'] = this.provnacimiento;
                this.order.offline.body['distnacimiento'] = this.distnacimiento;
                this.order.offline.body['fechanacimiento'] = this.fechanacimiento;
                this.order.offline.body['documentType'] = this.customer.tipoDocumentoRrll;
                this.order.offline.body['documentNumber'] = this.customer.numeroDocumentoRrll;
                this.order.offline.body['padrecontingencia'] = this.padrecontingencia
                this.order.offline.body['madrecontingencia'] = this.madrecontingencia

                this.customer.nombreCompletoRrll = this.apellidoscontingencia + ' ' + this.nombrecontingencia;
                this.order.customer = this.customer;

            }
        }else{
            if (this.customer.documentType == "DNI" || (this.customer.documentType == 'RUC' && this.customer.documentNumber.substr(0,2) == '10')) {
                this.customerReniec = this.customer;
                this.customerReniec.firstName = this.objReniec.names;
                this.customerReniec.lastName1 = this.objReniec.lastName;
                this.customerReniec.lastName2 = this.objReniec.motherLastName;
    
                fecNac = this.objReniec.birthdate;
                this.objReniec.birthdate = fecNac.substring(0, 4) + "-" + fecNac.substring(4, 6) + "-" + fecNac.substring(6, 8);
                this.customerReniec.birthDate = this.objReniec.birthdate;
    
                //        fecExp=this.objReniec.expeditionDate;
                //        this.objReniec.expeditionDate = fecExp.substring(0, 4) + "-"+ fecExp.substring(4, 6) + "-"  +fecExp.substring(6, 8);
                //        this.customerReniec.expDate=this.objReniec.expeditionDate;
    
                this.customerReniec.birthDep = this.objReniec.departmentBirthLocation;
                this.customerReniec.birthCity = this.objReniec.provinceBirthLocation;
                this.customerReniec.birthDist = this.objReniec.districtBirthLocation;
                this.order.customer = this.customerReniec;
    
            }else{
                fecNac = this.objReniec.birthdate;
                this.objReniec.birthdate = fecNac.substring(0, 4) + "-" + fecNac.substring(4, 6) + "-" + fecNac.substring(6, 8);
                this.customer.birthDate = this.objReniec.birthdate;
                this.order.customer = this.customer;
            }
        }


        // Sprint 3 - Solicitaron que siempre se pida registrar los datos del cliente
        this.order.showClientData = true;

        // Sprint 4 - obtenemos los datos del cliente...pueden estar actualizados los datos celular, telefono, correo
        this.ls.setData(this.order);
        this.order.status = 'T';

        this.router.navigate(['resumenOut']);
    }



    validateForm(motherName: string) {
        if (motherName) {
            motherName = motherName.substring(3, motherName.length)
            let motherNameFromReniec = this.objReniec.motherName
            this.contValidation++
            $('#pop_reniec').modal('hide')
            this.modalHome = false
            if (this.isCheckedNameUser &&
                this.isCheckedPlaceOfBirth &&
                this.isCheckedDayOfBirth) {
                if (motherName == motherNameFromReniec) {
                    this.isValidationCompleted = true
                    this.isItOkMothersName = true
                    this.changeStyles(true)
                } else {
                    this.changeStyles(false)
                    this.isValidationCompleted = false
                    this.isItOkMothersName = false
                    if (motherName != motherNameFromReniec) {
                        this.listErrorMessage.push("Aún no ha validado el nombre de la madre del cliente.")
                        this.isValidationCompleted = false
                        if (this.contValidation >= 3) {
                            this.modalHome = true
                            $('#pop_reniec').modal('show')
                        }
                    }
                }
            } else {
                if (this.isCheckedNameUser == false) {
                    this.listErrorMessage.push("Falta validar el nombre del cliente.")
                    this.isValidationCompleted = false
                }
                if (this.isCheckedPlaceOfBirth == false) {
                    this.listErrorMessage.push("Falta validar el lugar de nacimiento.")
                    this.isValidationCompleted = false
                }
                if (this.isCheckedDayOfBirth == false) {
                    this.listErrorMessage.push("Falta validar la fecha de nacimiento.")
                    this.isValidationCompleted = false
                }
                if (motherName == motherNameFromReniec) {
                    this.isItOkMothersName = true
                    this.changeStyles(true)
                } else {
                    this.changeStyles(false)
                    this.isValidationCompleted = false
                    this.isItOkMothersName = false
                    if (motherName != motherNameFromReniec) {
                        this.listErrorMessage.push("Aún no ha validado el nombre de la madre del cliente.")
                        this.isValidationCompleted = false
                        if (this.contValidation >= 3) {
                            this.modalHome = true
                            $('#pop_reniec').modal('show')
                        }
                    }
                }
                setTimeout(() => {
                    this.listErrorMessage = []
                }, 4000)
            }
        } else {
            if (this.isItOkMothersName) {
                this.contValidation++
                $('#pop_reniec').modal('hide')
                this.modalHome = false
                if (this.isCheckedNameUser &&
                    this.isCheckedPlaceOfBirth &&
                    this.isCheckedDayOfBirth) {
                    this.isValidationCompleted = true
                    this.isItOkMothersName = true
                    this.changeStyles(true)
                } else {
                    if (this.isCheckedNameUser == false) {
                        this.listErrorMessage.push("Falta validar el nombre del cliente.")
                        this.isValidationCompleted = false
                    }
                    if (this.isCheckedPlaceOfBirth == false) {
                        this.listErrorMessage.push("Falta validar el lugar de nacimiento.")
                        this.isValidationCompleted = false
                    }
                    if (this.isCheckedDayOfBirth == false) {
                        this.listErrorMessage.push("Falta validar la fecha de nacimiento.")
                        this.isValidationCompleted = false
                    }
                    setTimeout(() => {
                        this.listErrorMessage = []
                    }, 4000)
                }
            }
        }
    }

    nextPage() {
        if(this.contador==this.cantidadIntentos){
            if((this.customer.documentType != 'RUC') || (this.customer.documentType == 'RUC' && this.valorCondicional=='10')){
                this.customer.birthDate = this.fechanacimiento
            }else{
                this.customer.birthDate = "1900-01-01";
            }
            
        }else{
            if((this.customer.documentType != 'RUC') || (this.customer.documentType == 'RUC' && this.valorCondicional=='10')){
                let date1 = this.objReniec.birthdate;
                let p = date1.split("-")
                let connector = "de"
                if (parseInt(p[0]) >= 2000) {
                    connector = "del"
                }
                this.customer.birthDate = `${p[2]} de ${this.months[parseInt(p[1]) - 1]} ${connector} ${p[0]}`
            }else{
                this.customer.birthDate = "1900-01-01";
            }
        }
        
        this.continueSale();
    }


    changeStyles(valid: boolean) {
        if (valid) {
            $("#selecttr").css("background-color", "#dff0d8")
            $("#selecttr").find("option").css("color", "black")
            $("#selecttr").find("td").css("color", "black")
            $("#selecttr").find("select").css("color", "black")
            $("#selecttr").find("label").css("color", "#3399FF")
        } else {
            $("#selecttr").css("background-color", "#ff7e7e")
            $("#selecttr").find("td").css("color", "white")
            $("#selecttr").find("select").css("color", "white")
            $("#selecttr").find("option").css("color", "black")
            $("#selecttr").find("label").css("color", "white")


        }
    }

    cancelarConsulta() {
        this.loading = false;
        this.loadError = false;
        this.mensaje_error = "";
    }

}