import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { ReporteService } from '../services/reporteVenta.service';
import { LocalStorageService } from '../services/ls.service';


// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { Order } from '../model/order';
import { ActivatedRoute, Params }   from '@angular/router';
import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import {ExcelService} from '../services/sharedServices/excel.service';
import { IfObservable } from 'rxjs/observable/IfObservable';

var $ = require('jquery');

@Component({
	moduleId: 'reportes',
	selector: 'reportes',
	templateUrl: './reportes.template.html',
	providers: [ReporteService]
})
//--
export class ReportesComponent implements OnInit {

	isSocio: boolean = false;
	isCoordinador: boolean = false;
	filter: Object;
	canal: Object[];
	zonales: Object[];
	zonalesMostrados: Object[];
	regiones: Object[];
	socio: Object[];
	sociosMostrados: Object[];
	campania: Object[];
	opComercial: Object[];
	rows: Object[];
	header: Object[];
	regionesMostrados: Object[];
	ptoVenta: Object[];
	ptoVentaMostrados: Object[];
	order: Order;
	show: boolean = true
	loading: boolean = true
	consultado: boolean = false
	type: String
	title:String
	dropdownSettings = {};
	filtrado : any;
	
	constructor(private router: Router,
		private reporteService: ReporteService,
		private ls: LocalStorageService,
		private route: ActivatedRoute,
		private excelService:ExcelService) {
		this.order = JSON.parse(localStorage.getItem('order'));
		this.router = router;
		this.ls = ls;
		this.rows = []
		this.header = []
		if(this.order["user"].niveles=='3'){
			this.isCoordinador=true
		}else if(this.order["user"].niveles=='5'){
			this.isSocio=true
		}
		
	}

	ngOnInit() {
		this.dropdownSettings = { 
			singleSelection: false, 
			text:"Seleccionar",
			selectAllText:'Seleccionar Todos',
			unSelectAllText:'Deseleccionar Todos',
			primaryKey:'code',
			labelKey:'name',
			noDataLabel: 'Seleccionar un valor previamente'
		};

		var _scope = this
		$("#progress_bar_main").hide()
		this.route.params.subscribe((params: Params) => {
			_scope.consultado = false;
			_scope.type=params["type"];
			if(_scope.type=='opCom'){
				_scope.title = "Operacion comercial";
			}else if(_scope.type=='estado'){
				_scope.title = "estado";
			}else if(_scope.type=='producto'){
				_scope.title = "producto";
			}else if(_scope.type=='asesor'){
				_scope.title = "asesor";
			}else{
				_scope.title = "default";
			}

			_scope.filter = {
				canal : [],
				region: [],
				zonal: [],
				ptoVenta: [],
				dateStart: "",
				dateEnd: "",
				campania: [],
				opComercial: [],
				socio: [],
				codeFilter: ""
			}
			_scope.canal = [];
			_scope.sociosMostrados = [];
			_scope.regionesMostrados = [];
			_scope.zonalesMostrados = [];
			_scope.ptoVentaMostrados = [];
			_scope.opComercial = [];
			_scope.campania = [];
			_scope.header = []
			_scope.rows = []
			
			_scope.reporteService.getHeaders(this.order["user"].userId,this.order["user"].niveles).subscribe(
				data => {
					_scope.loading = false;
					if(data.responseCode!="1"){
						_scope.canal = data.responseData.canal
						_scope.regiones = data.responseData.region
						_scope.zonales = data.responseData.zonal
						_scope.ptoVenta = data.responseData.ptoVenta
						_scope.socio = data.responseData.socio
						_scope.campania = data.responseData.campania
						_scope.opComercial = data.responseData.opComercial
						_scope.filter["dateStart"]=data.responseData.fechaInicio
						_scope.filter["dateEnd"]=data.responseData.fechaFinal
					}else{
						this.show = false;
					}
				},
				error => {

				}
			);
		});
	}

	listarZonaOsocio(){
		
		if(this.isSocio){
			this.listarZona();
		}else{
			this.listarSocio();
		}
	}

	listarSocio(){

		this.sociosMostrados = this.socio.filter(x => this.filter["canal"].filter(y=> (x['codeHereditary'].includes(y["code"]))).length>0 );

		var eliminaciones = []
		for(let fil of this.filter["socio"]){
			if(this.sociosMostrados.filter(x => x['code']==fil.code).length==0){
				eliminaciones.push(fil.code);
			}
		}
		for(let val of eliminaciones){
			for( var i = this.filter["socio"].length-1; i>-1 ;i--){
				if ( this.filter["socio"][i].code === val) {
					this.filter["socio"].splice(i, 1);
				}
			}
		}
		this.listarZona();
	}

	isConteined(array,value){
		for(var i=0;i<array.length;i++) {
			if(value.indexOf(array[i].code)>-1){
				return true;
			}
		}
		return false;
	}
	
	listarZona(){
		if(this.isSocio){
			this.regionesMostrados = this.regiones.filter(x => this.isConteined(this.filter["canal"],x['codeHereditary']));
		}else{
			this.regionesMostrados = this.regiones.filter(x => this.isConteined(this.filter["socio"],x['codeHereditary']));
		}
		var eliminaciones = []
		for(let fil of this.filter["region"]){
			if(this.regionesMostrados.filter(x => x['code']==fil.code).length==0){
				eliminaciones.push(fil.code);
			}
		}
		for(let val of eliminaciones){
			for( var i = this.filter["region"].length-1; i>-1 ;i--){
				if ( this.filter["region"][i].code === val) {
					this.filter["region"].splice(i, 1);
				}
			}
		}
		this.listarZonales();
	}

	listarZonales(){
		var eliminaciones = []
		this.zonalesMostrados = this.zonales.filter(x => this.filter["region"].filter(y=> (y["code"]==x['codeHereditary'])).length>0);
		for(let fil of this.filter["zonal"]){
			if(this.zonalesMostrados.filter(x => x['code']==fil.code).length==0){
				eliminaciones.push(fil.code);				
			}
		}
		for(let val of eliminaciones){
			for( var i = this.filter["zonal"].length-1; i>-1 ;i--){
				if ( this.filter["zonal"][i].code === val) {
					this.filter["zonal"].splice(i, 1);
				}
			}
		}
		this.listarPtoVenta();
	}

	listarPtoVenta(){

		var eliminaciones = []
		this.ptoVentaMostrados = this.ptoVenta.filter(x => this.filter["zonal"].filter(y=> (y["code"]==x['codeHereditary'])).length>0);	
		for(let fil of this.filter["ptoVenta"]){
			if(this.ptoVentaMostrados.filter(x => x['code']==fil.code).length==0){
				eliminaciones.push(fil.code);												
			}
		}
		for(let val of eliminaciones){
			for( var i = this.filter["ptoVenta"].length-1; i>-1 ;i--){
				if ( this.filter["ptoVenta"][i].code === val) {
					this.filter["ptoVenta"].splice(i, 1);
				}
			}
		}
	}

	FILTRAR(){

		if(this.filter["dateStart"] == "" || this.filter["dateEnd"] == ""){
			alert("Ingrese una fecha correcta!");
			return false;
		}
		else{
			var fecha1 = new Date(this.filter["dateEnd"]);
			var fecha2 = new Date(this.filter["dateStart"]);

			fecha1.setDate(fecha1.getDate() + 1);
			fecha2.setDate(fecha2.getDate() + 1);

			if(fecha1 < fecha2){
				alert("La fecha fin debe ser mayor que la fecha de inicio");
				return false;
			}

			var diferencia = (fecha1.getTime() - fecha2.getTime()) / (1000 * 3600 * 24);

			if(diferencia > 31){
				alert("El rango de fechas maximo es de 31 dias.");
				return false;
			}
			
		}

		var dataSend = this.copy(this.filter);
		
		dataSend["canal"]=  (this.isCoordinador || this.isSocio)?(dataSend["canal"].length>0)?dataSend["canal"].map(a => a["code"]):(this.canal.map(a => a["code"])):[]; 
		dataSend["region"] =  (this.isCoordinador || this.isSocio)?(dataSend["region"].length>0)?dataSend["region"].map(a => a["code"]):(this.regiones.map(a => a["code"])):[];
		dataSend["zonal"] =  (this.isCoordinador || this.isSocio)?(dataSend["zonal"].length>0)?dataSend["zonal"].map(a => a["code"]):(this.zonales.map(a => a["code"])):[];
		dataSend["ptoVenta"] =  (this.isCoordinador || this.isSocio)?(dataSend["ptoVenta"].length>0)?dataSend["ptoVenta"].map(a => a["code"]):(this.ptoVenta.map(a => a["code"])):[];
		
		dataSend["socio"] = (this.isCoordinador)?((dataSend["socio"].length>0)?dataSend["socio"].map(a => a["code"]):this.socio.map(a => a["code"])):[]; 
		dataSend["campania"] = (this.isCoordinador)?((dataSend["campania"].length>0)?dataSend["campania"].map(a => a["code"]):this.campania.map(a => a["code"])):[]; 
		dataSend["opComercial"] =(this.isCoordinador)?((dataSend["opComercial"].length>0)?dataSend["opComercial"].map(a => a["code"]):this.opComercial.map(a => a["code"])):[]; 
		
		if(this.type=='opCom')
			dataSend["codeFilter"] = 1
		else if(this.type=='estado')
			dataSend["codeFilter"] = 2
		else if(this.type=='producto')
			dataSend["codeFilter"] = 3
		else if(this.type=='asesor')
			dataSend["codeFilter"] = 4
		
		var _scope = this;
		_scope.loading = true;

		this.filtrado = dataSend;
		
		this.reporteService.getData(dataSend).subscribe(
			data => {
				_scope.loading = false;
				_scope.header = []
				_scope.rows = []
				_scope.consultado = true;

				for(let fil of data.responseData.reporteVentaList[0].dataReporteventaList){
					var ro = {};
					
					if(_scope.rows.filter(x => x["name"]==fil["row"]).length==0){
						ro["name"]=fil["row"]
						ro["data"]=[]
						_scope.rows.push(ro)
					}else{
						ro=_scope.rows.filter(x => x["name"]==fil["row"])[0]
					}
					
					ro["data"][fil["column"]]=fil["value"];
			
					if(!_scope.header.includes(fil["column"])){
						_scope.header.push(fil["column"]);					
					}	
				}

				for(let col of _scope.rows){
					console.log("Columna " + JSON.stringify(col));
					
					var totalesinstalado=0;
					var totalescodigo=0;
					var totalesventa=0;
					var indice1;
					var indice2;

					for(let header in col["data"]){
						console.log("Data: " + JSON.stringify(col["data"][header]));
						if(header=='Instalado'){
							totalesinstalado=parseInt(col["data"][header]);
						}else{
							if(header=='Ingresado con Pedido'){
								totalescodigo=parseInt(col["data"][header]);
							}
							else if(header == 'Pre-Venta')
							totalesventa=parseInt(col["data"][header]);
						}	
					}

					if(_scope.header.includes('Venta') || _scope.header.includes('INGRESADO'))
						indice1=(Math.round((totalescodigo/totalesventa)*10000)/100) + "%";
					else
						indice1='--'

					if(_scope.header.includes('Instalado'))
						indice2=(Math.round((totalesinstalado/((totalescodigo>totalesinstalado)?totalescodigo:totalesinstalado))*10000)/100) + "%"; 
					else
						indice2='--'


					indice1 = indice1.replace("NaN%","--");
					indice2 = indice2.replace("NaN%","--");

					col["data"]["Convertibilidad de Registro"]=indice1;
					col["data"]["Convertibilidad de Instalacion"]=indice2;

				}
				
				_scope.header.push("Convertibilidad de Registro");					
				_scope.header.push("Convertibilidad de Instalacion");					

				/*
				if(_scope.header.includes('Venta') || _scope.header.includes('INGRESADO'))
					_scope.indice1=Math.round((totalescodigo/totalesventa)*10000)/100;
				else
					_scope.indice1='--'

				if(_scope.header.includes('Instalado'))
					_scope.indice2=Math.round((totalesinstalado/((totalescodigo>totalesinstalado)?totalescodigo:totalesinstalado))*10000)/100;
				else
					_scope.indice2='--'*/
			},
			error => {

			}
		)
	}

	export(){


		var dataSend; //
		
		if(this.filtrado){
			dataSend = this.filtrado
		}
		else{
			
			if(this.filter["dateStart"] == "" || this.filter["dateEnd"] == ""){
				alert("Ingrese una fecha correcta!");
				return false;
			}
			else{
				var fecha1 = new Date(this.filter["dateEnd"]);
				var fecha2 = new Date(this.filter["dateStart"]);
	
				fecha1.setDate(fecha1.getDate() + 1);
				fecha2.setDate(fecha2.getDate() + 1);
	
				if(fecha1 < fecha2){
					alert("La fecha fin debe ser mayor que la fecha de inicio");
					return false;
				}
	
				var diferencia = (fecha1.getTime() - fecha2.getTime()) / (1000 * 3600 * 24);
	
				if(diferencia > 31){
					alert("El rango de fechas maximo es de 31 dias.");
					return false;
				}
				
			}

			dataSend = this.copy(this.filter);
			dataSend["canal"]=  (this.isCoordinador || this.isSocio)?(dataSend["canal"].length>0)?dataSend["canal"].map(a => a["code"]):(this.canal.map(a => a["code"])):[]; 
			dataSend["region"] =  (this.isCoordinador || this.isSocio)?(dataSend["region"].length>0)?dataSend["region"].map(a => a["code"]):(this.regiones.map(a => a["code"])):[];
			dataSend["zonal"] =  (this.isCoordinador || this.isSocio)?(dataSend["zonal"].length>0)?dataSend["zonal"].map(a => a["code"]):(this.zonales.map(a => a["code"])):[];
			dataSend["ptoVenta"] =  (this.isCoordinador || this.isSocio)?(dataSend["ptoVenta"].length>0)?dataSend["ptoVenta"].map(a => a["code"]):(this.ptoVenta.map(a => a["code"])):[];
			
			dataSend["socio"] = (this.isCoordinador)?((dataSend["socio"].length>0)?dataSend["socio"].map(a => a["code"]):this.socio.map(a => a["code"])):[]; 
			dataSend["campania"] = (this.isCoordinador)?((dataSend["campania"].length>0)?dataSend["campania"].map(a => a["code"]):this.campania.map(a => a["code"])):[]; 
			dataSend["opComercial"] =(this.isCoordinador)?((dataSend["opComercial"].length>0)?dataSend["opComercial"].map(a => a["code"]):this.opComercial.map(a => a["code"])):[]; 
			
			if(this.type=='opCom')
				dataSend["codeFilter"] = 1
			else if(this.type=='estado')
				dataSend["codeFilter"] = 2
			else if(this.type=='producto')
				dataSend["codeFilter"] = 3
			else if(this.type=='asesor')
				dataSend["codeFilter"] = 4
		}
		
		var _scope = this;
		_scope.loading = true;
		
		this.reporteService.getDataExport(dataSend).subscribe(
			data => {
				if(data.responseCode == "0"){
					console.log(data.responseData);
					//let data2 = JSON.stringify();
					this.excelService.exportAsExcelFile(data.responseData.reporteVentas, 'ventas' );
					_scope.loading = false;
				}
				else{
					_scope.loading = false;
				}

			},
			error => {

			}
		)
	}

	/*export(){
		var data2 = [];
		var i=0;
		for(let row of this.rows){
			data2[i]={}
			data2[i]['Descripcion']=row['name']
			for(let head of this.header){
				var value=row["data"][head.toString()]?row["data"][head.toString()]:'0'
				data2[i][head.toString()]=value
			}
			i++;
		}
		this.excelService.exportAsExcelFile(data2, 'sample');
	}*/

	copy(mainObj) {
		let objCopy = {}; // objCopy will store a copy of the mainObj
		let key;
		for (key in mainObj) {
		  objCopy[key] = mainObj[key]; // copies each property to the objCopy object
		}
		return objCopy;
	}

	numberFormat(numero){

		var num = numero
		//console.log("Numero "+num);

		if(num.indexOf('.')>-1){
			num = num.substring(0,num.indexOf('.'))			
		}else if(num.charAt(num.length-1)=='%')
			num=num.substring(0,num.length-1)


		//console.log("Parte numerica "+num);
		var texto=""

		for(var i=num.length-1 ; i>=0 ; i--){
			texto = num.charAt(i) + texto

			if((num.length-i)%3==0 && i>0)
				texto = "," + texto
		}

		//console.log("Parte numerica formateada"+texto);
		
		if(numero.indexOf('.')>-1){
			texto = texto + numero.substring(numero.indexOf('.'),numero.length)			
		}else if(numero.charAt(numero.length-1)=='%')
			texto = texto + "%"
		
		return texto
    }
}
