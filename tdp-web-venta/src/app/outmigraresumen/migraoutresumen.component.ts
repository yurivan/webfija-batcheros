import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common'
import { Router } from '@angular/router';

import { CustomerService } from '../services/customer.service';
import { LocalStorageService } from '../services/ls.service';

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { AgendamientoService } from '../services/agendamiento.service';



import { Agendamiento } from '../model/agendamientos';
import { Order } from '../model/order';



import { locale } from 'moment';

var $ = require('jquery');
class RequestDepartment {
	id: string
	value: any
}

@Component({
	moduleId: 'migraoutresumen',
	selector: 'migraoutresumen',
	templateUrl: './migraoutresumen.template.html',
	providers: [DatePipe]
})

export class migraoutresumencomponent implements OnInit {
	loading = false;

  
	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService,
		private datepipe: DatePipe,
		private agendamientoService:AgendamientoService) {
		this.router = router;
		this.ls = ls;
	}

	ngOnInit() {
	 

	}
	migrarOut(){
		this.router.navigate(['migraOutReniec']);
	}	 	
}