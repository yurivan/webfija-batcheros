import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DireccionService } from '../services/direccion.service';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order';
import { Customer } from '../model/customer';
import { LocalStorageService } from '../services/ls.service';
import { ScoringService } from '../services/scoring.service'; //glazaror sprint2
import * as globals from '../services/globals';
var $ = require('jquery');
(<any>window).jQuery = $

@Component({
    moduleId: 'scoring',
    selector: 'scoring',
    templateUrl: 'scoring.template.html',
    providers: [DireccionService, CustomerService, OrderService, ScoringService]
})
export class ScoringComponent implements OnInit{

    direccionService : DireccionService;
    customerService : CustomerService;
	scoringService : ScoringService;//glazaror sprint2

    objDepart : any;
    private keyDeparts : string[];
    private departSeleccionado = 0;
    selectDepart : any;

    objProvs : any;
    showProvs =new Array();
    private keyProvs : string[];
    selectProv : any;
    private proviSeleccionado = 0;

    objDists : any;
    showDists =new Array();
    selectDistric : any;
    private keyDistrs : string[];
    private districSeleccionado = 0;

    //distModel : any;
    //provModel : any;
    order : Order;
    customer : Customer;
    mensajeValidacion : String;
    channelCall : boolean;

    private loadError : boolean;
    private loading : boolean;
    private mensaje_error : String;
    cargaDept : boolean;

	scoringDataModel : any;
	respuestaScoring : boolean;

	mensajeValidacionDatosCliente : string;// Sprint 6 - mensaje de validacion celular/correo obligatorios

    constructor(private router: Router, direccionService: DireccionService, customerService: CustomerService, private ls: LocalStorageService,
		private orderService: OrderService, scoringService: ScoringService) {

        this.router = router;
        this.direccionService = direccionService;
        this.customerService = customerService;
		this.scoringService = scoringService;//glazaror sprint2
        this.ls = ls;
        this.cargaDept= false;
        //this.distModel=0;
        //this.provModel=0;
        this.loadError= false;
        this.mensaje_error= "";
        this.orderService = orderService;

		this.mensajeValidacionDatosCliente = "";//Sprint 6
    }

    ngOnInit(){
        // add loading
        this.loading = true;

        this.obtenerDepartamentos();
        this.obtenerProvincias();
        this.obtenerDistritos();

        this.order = JSON.parse(localStorage.getItem('order'));

        this.customer = this.order.customer;
		this.loading = false;

		//verificamos valores por default
        this.cargarValoresDefault();
    }

	cargarValoresDefault() {
		this.selectDepart = document.getElementById("department");
		this.selectProv = document.getElementById("city");
		this.selectDistric = document.getElementById("district");

		if (this.order.departmentScoring) {
			this.departSeleccionado = this.order.departmentScoring;
			this.proviSeleccionado = this.order.provinceScoring;
			this.listarProvincias(this.departSeleccionado);
			this.districSeleccionado = this.order.districtScoring;
			this.listarDistritos(this.departSeleccionado+this.proviSeleccionado);
			this.respuestaScoring = true;
			this.scoringDataModel = this.order.scoringDataModel;
		} else {
			//primero verificamos si es que la ubicación fue persistida en una venta anterior
			var ubicacionDefault = this.ls.getUbicacionDefault();
			if (ubicacionDefault != null) {
				this.departSeleccionado = ubicacionDefault.codigoDepartamento;
				this.proviSeleccionado = ubicacionDefault.codigoProvincia;
				this.listarProvincias(this.departSeleccionado);
				this.districSeleccionado = ubicacionDefault.codigoDistrito;
				this.listarDistritos(this.departSeleccionado+this.proviSeleccionado);

				if (this.selectDepart != null) {
					this.selectDepart.value = this.departSeleccionado;
					this.selectProv.value = this.proviSeleccionado;
					this.selectDistric.value = this.districSeleccionado;
				}
			}
		}
	}

    obtenerDepartamentos() {
		this.objDepart=this.direccionService.getDataDepartamento();
        this.keyDeparts = Object.keys(this.objDepart);
        this.keyDeparts.sort()
    }

    obtenerProvincias() {
        this.objProvs=this.direccionService.getDataProvincias();
    }

    listarProvincias(skuDep) {
        this.showProvs.length = 0;
        this.keyProvs = Object.keys(this.objProvs);
        this.keyProvs.forEach((item, index) => {
		  if(this.objProvs[item].skuDep == skuDep) {
			  this.showProvs.push(this.objProvs[item]);
              this.showProvs.sort(this.ordenPlaceByName)
		  }
		  //this.showProvs.sort()
        });
		//this.showProvs.sort()
    }

    obtenerDistritos() {
        this.objDists = this.direccionService.getDataDistritos();
    }

    listarDistritos(skuDepPro){

        this.showDists.length = 0;
        this.keyDistrs = Object.keys(this.objDists);
        this.keyDistrs.sort()
        this.keyDistrs.forEach((item, index) => {
			if(this.objDists[item].skuDepPro == skuDepPro) {
				this.showDists.push(this.objDists[item]);
                this.showDists.sort(this.ordenPlaceByName)
			}
        })
    }

    selDepart() {
		this.selectDepart = document.getElementById("department");
        this.departSeleccionado = this.selectDepart.value;
        this.listarProvincias(this.departSeleccionado);
		this.limpiarDireccion('department');
    }

    selCity() {
		this.selectProv = document.getElementById("city");
		this.proviSeleccionado = this.selectProv.value;
		this.listarDistritos(this.departSeleccionado+this.proviSeleccionado);
		this.limpiarDireccion('province');
    }

    selDistrict() {
		this.selectDistric = document.getElementById("district");
        this.districSeleccionado = this.selectDistric.value;
    }

	limpiarDireccion(tipo) {
        if ('department' == tipo) {
			this.selectProv = document.getElementById("city");
			this.selectDistric = document.getElementById("district");
            this.selectProv.value = "";
			this.selectDistric.value = "";

			this.proviSeleccionado = 0;
			this.districSeleccionado = 0;
			this.listarDistritos(0);
        } else if ('province' == tipo) {
			this.selectDistric = document.getElementById("district");
            this.selectDistric.value = "";
			this.districSeleccionado = 0;
        }
    }

    mostrarError(mensaje) {
        this.loading = false;
        this.loadError = true;
        this.mensaje_error = mensaje;
		this.respuestaScoring = false;
    }

	//glazaror sprint2 scoring
	evaluar() {
		this.loading = true;

		//
		this.selectDepart = document.getElementById("department");
		this.selectProv = document.getElementById("city");
		this.selectDistric = document.getElementById("district");

		var departamentoLabel = this.selectDepart[this.selectDepart.selectedIndex].innerHTML;
		var provinciaLabel = this.selectProv[this.selectProv.selectedIndex].innerHTML;
		var distritoLabel = this.selectDistric[this.selectDistric.selectedIndex].innerHTML;

		//guardamos el departamento, ciudad y distrito seleccionados para el scoring
		//this.order = JSON.parse(localStorage.getItem('order'));
		this.order.departmentScoring = this.selectDepart.value;
		this.order.provinceScoring = this.selectProv.value;
		this.order.districtScoring = this.selectDistric.value;

        this.ls.setData(this.order);

		var ubigeo = "" + this.selectDepart.value + this.selectProv.value + this.selectDistric.value;
		this.scoringService.getData(this.customer, this.order.user.userId, departamentoLabel, provinciaLabel, distritoLabel, ubigeo, this.order.user.channel)
                .subscribe(
                data => this.cargarScoringData(data),
                err => this.errorScoringData(err));

        //this.router.navigate( ['saleProcess/direccion'] );//sprint2 ahora se debe cargar el scoring como 3ra pantalla

	}

	cargarScoringData(data) {

		if (data.responseCode == -1) {
			this.mostrarError(data.responseMessage);
			return;
		}

		let selectDepartment : any;
		let selectCity : any;
		let selectDistrict : any;

		selectDepartment = document.getElementById("department");
		selectCity = document.getElementById("city");
		selectDistrict = document.getElementById("district");

		var departmentValue = selectDepartment.value;
		var cityValue = selectCity.value;
		var districtValue = selectDistrict.value;

		this.respuestaScoring = true;
		this.scoringDataModel = data;

		this.order.scoringDataModel = data;
		this.ls.setData(this.order);
		this.ls.setUbicacionDefault(departmentValue, cityValue, districtValue);

		this.loadError = false;
		this.loading = false;
    }

	/*guardarUbicacionDefault(departmentValue, cityValue, districtValue) {
		this.ls.setDepartamentoDefault(departmentValue);
		this.ls.setProvinciaDefault(cityValue);
		this.ls.setDistritoDefault(districtValue);
	}*/

	errorScoringData( err ) {
        this.loading = false;
        this.loadError = true;
        this.mensaje_error = globals.ERROR_MESSAGE;

		//mensajeValidacion
		$('#myModalReniec').modal('show');
        //this.mensaje_error = "El servicio customer dio un error desconocido, Por favor vuelva a intentarlo";
    }

	cargarParque( ) {
		this.loading = false;
        return "";
    }

	//glazaror sprint2 scoring
	iniciarVenta() {
		if (this.ls.getData().editandoCliente) {
			this.mensajeValidacionDatosCliente = "Celular";
			$('#modalCliente').modal('show');
			return;
		}
		this.router.navigate( ['saleProcess/direccion'] );//sprint2 ahora se debe cargar el scoring como 3ra pantalla

	}

	// Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"
	cerrarDialogo(nombreModal) {
        $("#" + nombreModal).modal('hide');
    }

    ordenPlaceByName(placeA, placeB) {
        // Use toUpperCase() to ignore character casing
        const firstPlace = placeA.name.toUpperCase();
        const nextPlace = placeB.name.toUpperCase();

        let comparison = 0;
        if (firstPlace > nextPlace) {
          comparison = 1;
        } else if (firstPlace < nextPlace) {
          comparison = -1;
        }
        return comparison;
      }
}