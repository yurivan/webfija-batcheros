import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common'
import { Router } from '@angular/router';

import { CustomerService } from '../services/customer.service';
import { LocalStorageService } from '../services/ls.service';

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';

import { Order } from '../model/order';
import { Producto } from '../model/producto';
import { OutOfferingPlanta } from '../model/outOfferingPlanta';
import { ProductOffering } from '../model/productOffering';

var $ = require('jquery');
class RequestDepartment {
	id: string
	value: any
}

@Component({
	moduleId: 'migraoutoffering',
	selector: 'migraoutoffering',
	templateUrl: './migraoutoffering.template.html',
	providers: [CustomerService]
})

export class migraoutofferingcomponent implements OnInit {
	loading = false;
	productoActual: Producto;
	order: Order;
	offeringPlanta: OutOfferingPlanta[];

	listServices: String;

	mensajeValidacionDatosCliente: string = "";
	private showImgValidate: boolean = false;
	private showAlert: boolean = false;

	answerValidationOffer = {
		responseCode: '',
		responseMessage: '',
		responseData: ''

	}


	type: string;

	private nameDistrict: string;
	private nameProvince: string;
	private nameDepart: string;
	private arpu: number;

	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService,
		private customerService: CustomerService) {
		this.router = router;
		this.ls = ls;
		this.customerService = customerService;
	}

	ngOnInit() {

		this.order = this.ls.getData();
		this.type = this.order.type;


		if (this.type == globals.ORDER_ALTA_NUEVA) {
			var ubicacionPedido = this.ls.getUbicacionPedido();
			this.nameDistrict = ubicacionPedido.distrito;
			this.nameProvince = ubicacionPedido.provincia;
			this.nameDepart = ubicacionPedido.departamento;
		} else if (this.type == globals.ORDER_MEJORAR_EXPERIENCIA || this.type == globals.ORDER_SVA) {
			this.nameDistrict = this.order.product.district;
			this.nameProvince = this.order.product.province;
			this.nameDepart = this.order.product.department;
		}
		this.progresBar.getProgressStatus();
		$(".progres").prop("style", "visibility: hidden !important;");

		this.productoActual = JSON.parse(localStorage.getItem('productoActual'));
		this.offeringPlanta = JSON.parse(localStorage.getItem('offeringPlanta'));
		this.arpu = Number(this.productoActual.rentaTotal);

		this.listarServicio(this.offeringPlanta);


	}

	listarServicio(offeringPlanta: OutOfferingPlanta[]) {
		this.listServices = '';
		let count = offeringPlanta.length;
		for (let i = 0; i < offeringPlanta.length; i++) {
			if (offeringPlanta.length == 1) {
				this.listServices = offeringPlanta[i].productName
			} else if (i < count -1 ) {
				this.listServices = this.listServices + offeringPlanta[i].productName + ", ";
			} else {
				this.listServices = this.listServices + " y " + offeringPlanta[i].productName;
			}
		}
	}


	setClasses(prod) {
		let classes = {}
		switch (prod.productTypeCode) {
			case '1':
				classes = { mono_inf: true, acordeon: true }
				break
			case '2':
				if (prod.productCategory == "Internet Naked") {
					classes = { mono_naked_inf: true, acordeon: true }
				} else {
					classes = { mono_inf: true, acordeon: true }
				}
				break
			case '3':
				classes = { trio_inf: true, acordeon: true }
				break
			case '4':
				classes = { duo_inf: true, acordeon: true }
				break
			case '5':
				classes = { duo_inf: true, acordeon: true }
				break
			case '6':
				classes = { mono_inf: true, acordeon: true }
				break
			default:
				classes = { mono_inf: true, acordeon: true }
		}
		return classes
	}

	setButtonColor = prod => {
		let classes = {}
		switch (prod.productTypeCode) {
			case '1':
			case '2':
			case '6':
				if (prod.productCategory == "Internet Naked") {
					classes = { boton_naked: true }
				} else {
					classes = { boton_lila: true }
				}
				break
			case '3':
				classes = { boton_greentdp: true }
				break
			case '4':
			case '5':
				classes = { boton_cyan: true }
				break
		}
		return classes
	}

	toggleMove(ind: string) {
		this.selectAccordionElement("toggle", ind)
	}

	selectAccordionElement(pre: string, ind: string) {
		let parent: string = pre + ind
		$(".accordion-toggle").parent().toggleClass('ic')
		$('#' + parent).next().slideToggle('fast')
		$(".accordion-content").not($('#' + parent).next()).slideUp('fast')
	}


	sendOffering(prod) {

		//console.log("prod => " + prod);
		//console.log(this.order);

		if (this.ls.getData().editandoCliente) {
			this.mensajeValidacionDatosCliente = "Celular";
			$('#modalCliente').modal('show');
			return;
		}
		this.loading = true
		if (this.order.product.parkType == '') {
			this.order.type = globals.ORDER_ALTA_NUEVA;
		} else {
			this.order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
		}
		this.validateOffering(prod);
		//console.log(this.order);
	}

	validateOffering(selectedOffering) {
		let codtypeProducto = selectedOffering.productTypeCode;
		let documentType = this.order.customer.documentType;
		let documentNumber = this.order.customer.documentNumber;
		let productName = selectedOffering.productName;
		this.showImgValidate = true;

		this.showAlert = false;


		let resumen = JSON.parse(localStorage.getItem('resumen'));

		///console.log("resumen");
		//console.log(resumen);

		if (resumen == null) {
			resumen = {
				producto: productName,
				direccion: "direccion",
				costo: selectedOffering.installCost,
				precio: selectedOffering.price
			}
		}

		resumen.producto = productName
		localStorage.setItem("resumen", JSON.stringify(resumen));

		this.customerService.getValidateDuplicadoUbicacion(documentType, documentNumber, codtypeProducto, productName, this.nameDepart, this.nameProvince, this.nameDistrict)
			.subscribe(
				data => {
					if (typeof data.responseData !== 'undefined' && data.responseData !== null) {

						this.answerValidationOffer = data;

						if (this.answerValidationOffer.responseCode != "00") {

							this.showImgValidate = false;
							this.showAlert = true;
							this.loading = false;

							document.getElementsByTagName('html')[0].scrollTop = 150;

						} else {

							this.showAlert = false;
							this.order.status = "O";
							this.order.selectedOffering = new ProductOffering();
							this.order.selectedOffering.id = selectedOffering.id;
							this.order.selectedOffering.commercialOperation = selectedOffering.commercialOperation;
							this.order.selectedOffering.campaign = selectedOffering.campaign;
							this.order.campaniaSeleccionada = selectedOffering.campaign;
							this.order.selectedOffering.productTypeCode = selectedOffering.productTypeCode;
							this.order.selectedOffering.productCategoryCode = selectedOffering.productCategoryCode;
							this.order.selectedOffering.productCategory = selectedOffering.productCategory;
							this.order.selectedOffering.productCode = selectedOffering.productCode;
							this.order.selectedOffering.productName = selectedOffering.productName;
							this.order.selectedOffering.discount = selectedOffering.discount;
							this.order.selectedOffering.price = selectedOffering.price;
							this.order.selectedOffering.promPrice = selectedOffering.promPrice;
							this.order.selectedOffering.monthPeriod = selectedOffering.monthPeriod;
							this.order.selectedOffering.installCost = selectedOffering.installCost;
							this.order.selectedOffering.lineType = selectedOffering.lineType;
							this.order.selectedOffering.paymentMethod = selectedOffering.paymentMethod;
							this.order.selectedOffering.cashPrice = selectedOffering.cashPrice;
							this.order.selectedOffering.financingCost = selectedOffering.financingCost;
							this.order.selectedOffering.financingMonth = selectedOffering.financingMonth;
							this.order.selectedOffering.equipmentType = selectedOffering.equipmentType;
							this.order.selectedOffering.returnMonth = selectedOffering.returnMonth;
							this.order.selectedOffering.returnPeriod = selectedOffering.returnPeriod;
							this.order.selectedOffering.internetSpeed = selectedOffering.internetSpeed;
							this.order.selectedOffering.promoInternetSpeed = selectedOffering.promoInternetSpeed;
							this.order.selectedOffering.periodoPromoInternetSpeed = selectedOffering.periodoPromoInternetSpeed;
							this.order.selectedOffering.internetTech = selectedOffering.internetTech;
							this.order.selectedOffering.tvSignal = selectedOffering.tvSignal;
							this.order.selectedOffering.tvTech = selectedOffering.tvTech;
							this.order.selectedOffering.equipamientoLinea = selectedOffering.equipamientoLinea;
							this.order.selectedOffering.equipamientoInternet = selectedOffering.equipamientoInternet;
							this.order.selectedOffering.equipamientoTv = selectedOffering.equipamientoTv;
							this.order.selectedOffering.expertCode = selectedOffering.expertCode;
							this.order.selectedOffering.cantidaddecos = selectedOffering.cantidaddecos;
							this.order.sva = [];
							this.order.selectedOffering.altaPuraSvasBloqueTV = selectedOffering.altaPuraSvasBloqueTV;
							this.order.selectedOffering.altaPuraSvasLinea = selectedOffering.altaPuraSvasLinea;
							this.order.selectedOffering.altaPuraSvasInternet = selectedOffering.altaPuraSvasInternet;
							this.order.selectedOffering.msx_cbr_voi_ges_in = selectedOffering.msx_cbr_voi_ges_in;
							this.order.selectedOffering.msx_cbr_voi_gis_in = selectedOffering.msx_cbr_voi_gis_in;
							this.order.selectedOffering.msx_ind_snl_gis_cd = selectedOffering.msx_ind_snl_gis_cd;
							this.order.selectedOffering.msx_ind_gpo_gis_cd = selectedOffering.msx_ind_gpo_gis_cd;
							this.order.selectedOffering.cod_ind_sen_cms = selectedOffering.cod_ind_sen_cms;
							this.order.selectedOffering.cod_cab_cms = selectedOffering.cod_cab_cms;
							this.order.selectedOffering.cod_fac_tec_cd = selectedOffering.cod_fac_tec_cd;
							this.order.selectedOffering.cobre_blq_vta = selectedOffering.cobre_blq_vta;
							this.order.selectedOffering.cobre_blq_trm = selectedOffering.cobre_blq_trm;
							if (selectedOffering.sva != null) {
								let sva = selectedOffering.sva;
								sva.selected = true;
								this.order.sva.push(sva);
							}
							//seteamos los sva por default en el localstorage... 
							localStorage.setItem("defaultSvas", JSON.stringify(this.order.sva));
							this.ls.setData(this.order);
							// alert("redireccionando a pantalla sva.... this.showAlert: " + this.showAlert);
							this.router.navigate(['svaOut']);

						}


					} else {
						this.loading = false
						this.showImgValidate = false;
						this.showAlert = true;
						this.answerValidationOffer.responseMessage = "La validacion no pudo concretarse. Intente nuevamente";
					}

				},
				err => {
					//console.log(err)
				}
			);



        /*this.order.status = "O";
        this.order.selectedOffering = selectedOffering;
        this.order.sva = [];
        if (selectedOffering.sva != null) {
            let sva = selectedOffering.sva;
            sva.selected = true;
            this.order.sva.push(sva);
        }
        this.ls.setData(this.order);
       // console.log("order before sva : " + JSON.stringify(this.order))
        this.ls.setData(this.order);
        this.completValidate(true)*/

	}

	migrarOut() {
		this.router.navigate(['migraoutsva']);
	}
}