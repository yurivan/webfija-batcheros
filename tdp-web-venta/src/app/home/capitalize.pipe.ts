import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalize',
  pure: false
})
export class Capitalize implements PipeTransform {
	transform(value: string): string {
		if (value === null) return '____';
		return value.charAt(0).toUpperCase() + value.slice(1);
	}
}