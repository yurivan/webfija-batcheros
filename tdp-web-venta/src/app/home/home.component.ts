import { Component, OnInit } from '@angular/core';

import { HomeService } from '../services/home.service';

import { InfoVenta } from '../model/infoVenta';

import { Order } from '../model/order';

import { LocalStorageService } from '../services/ls.service';
import { ProgressBarService } from '../services/progressbar.service';

import { Router } from '@angular/router';
import * as globals from '../services/globals';
var $ = require('jquery');

@Component({
  moduleId: 'home',
  selector: 'home',
  templateUrl: './home.template.html',
  providers: [HomeService]
})

export class HomeComponent implements OnInit {

  mensaje_error: string;
  infoVenta: InfoVenta[];
  load_info_venta: boolean;
  visible_info_venta: boolean;
  order;

  is_venta_empty: boolean;
  loadError: boolean;
  //mapHome = new Map<string,Array<InfoVenta>>();

  valoresVena = [];
  //sells = new Array();

  numDaysSales: number;
  constructor(private homeService: HomeService,
    private router: Router,
    private ls: LocalStorageService,  private progresBar: ProgressBarService) {


    this.numDaysSales = globals.DIAS_VENTAS;

    //console.log("numero de dias de ventas "+ this.numDaysSales);
    this.ls = ls;
    this.mensaje_error = "";
    this.homeService = homeService;
    this.load_info_venta = true;
    this.visible_info_venta = false;
    this.is_venta_empty = false;
    this.loadError = false;


    //this.order = JSON.parse(localStorage.getItem('order'));
    //this.order.status="N";
    //this.ls.setData(this.order);

    this.order = this.ls.getData();
    let order = new Order();
    order.user = this.order.user;
    this.ls.setData(order);

    this.homeService.getData(this.numDaysSales).subscribe(
      data => this.cargarDatos(data),
      err => this.cargarError(err),
      () => this.cargaCompleta());
  }

  listarVentas(stado) {

    stado = stado == null ? 'CAIDA APP' : stado;
    this.router.navigate(['/home/venta'], { queryParams: { numDays: 30, status: stado } });
  }

  cargarDatos(data) {
    this.infoVenta = data.responseData;
  }

  cargarError(err) {
    //console.log(err);
    this.load_info_venta = false;
    this.loadError = true;
    this.mensaje_error = "El servicio salesReport/count dio un error desconocido, Por favor vuelva a intentarlo";
  }

  cargaCompleta() {
    //console.log("ventasssss");
    //console.log(this.infoVenta);
    this.load_info_venta = false;
    if (this.infoVenta != null && this.infoVenta != undefined) {
      //console.log("entro sin ventas");
      if (this.infoVenta.length <= 0) {
        this.is_venta_empty = true;

      } else {
        this.infoVenta.sort(function (elem1: any, elem2: any) { if (elem1.status > elem2.status) { return 1; } else { return 0; } });
        this.visible_info_venta = true;

      }
    } else {
      this.is_venta_empty = true;//"El usuario no tiene ventas en los �ltimos "+ this.numDaysSales + ' d�as'
    }
  }
  ngOnInit() {
    $(document).ready(() => {
      $('#reportes').hide()
    })
    this.progresBar.getProgressStatus()

    
  
  }

}