import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { Router } from '@angular/router';
import { Customer } from '../model/customer';
import { ProductOffering } from '../model/productOffering';
import { OfferingService } from '../services/offering.service';
import { Order } from '../model/order';
import { Producto } from '../model/producto';
import { LocalStorageService } from '../services/ls.service';
import { CustomerService } from '../services/customer.service';
import * as globals from '../services/globals';
var $ = require('jquery');
(<any>window).jQuery = $
import { ProgressBarService } from '../services/progressbar.service';

@Component({
    moduleId: 'offeringOut',
    selector: 'offeringOut',
    templateUrl: './offeringOut.template.html',
    providers: [OfferingService, CustomerService]
})

export class OfferingOutComponent implements OnInit {
    currentProductIndex: -1;
    img_upgrades: String[];
    imagenes1: String[];
    productOffer: ProductOffering[];
    productOfferTrio: ProductOffering[]
    productOfferDuo: ProductOffering[]
    productOfferMono: ProductOffering[]
    campaigns: String[];
    typeProducts: String[];
    private loading: boolean;
    private visible_accordion;
    private is_product_empty: boolean;
    private is_month_zero: boolean;

    private order: Order;
    private product: Producto;
    private customer: Customer;
    selected: any;
    private showAlert: boolean;
    private showImgValidate: boolean;
    private showButtonSvas: boolean;
    private showButtonOnlySvas: boolean;
    private arpu: number;

    private nameDistrict: string;
    private nameProvince: string;
    private nameDepart: string;
    state: string = 'inactive';
    tab1: boolean
    tab2: boolean
    tab3: boolean
    tab4: boolean

    CategoriaSeleccionada: boolean

    mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios

    model: any;
    type: string;

    filter = {
        productName: '',
        campaign: '',
        productType: ''
    }

    answerValidationOffer = {
        responseCode: '',
        responseMessage: '',
        responseData: ''

    }
    errorMessage: string;
    errorCode: string;

    panelFlag: boolean[];
    panelFlagMsg: string;
    panelFlagCount: number;
    productsEmpty: boolean = false;

    nameselectcategory: string

    productActual: Producto;
    flagProductoActual: boolean = false;

    constructor(private offeringService: OfferingService,
        private router: Router,
        private ls: LocalStorageService,
        private customerService: CustomerService,
        private progresBar: ProgressBarService) {
        this.imagenes1 = [];
        this.offeringService = offeringService;
        this.img_upgrades = [];
        this.loading = true;
        this.visible_accordion = false;
        this.is_product_empty = false;
        this.ls = ls;
        this.customerService = customerService;
        this.showAlert = false;
        this.showImgValidate = false;
        this.showButtonSvas = true;
        this.showButtonOnlySvas = false;
        this.panelFlag = [];
        this.panelFlagMsg = "Ver Mas";
        this.arpu = 0;
        this.panelFlagCount = globals.PANEL_FLAG;
        this.productOfferTrio = []
        this.productOfferDuo = []
        this.productOfferMono = []
        this.tab1 = true
        this.tab2 = false
        this.tab3 = false
        this.tab4 = false
        this.nameselectcategory = "";
        this.mensajeValidacionDatosCliente = "";//Sprint 6
        this.CategoriaSeleccionada = false;
    }

    ngOnInit() {

        this.order = this.ls.getData();
        this.type = this.order.type;

        if (!this.order || !this.order.product || !this.order.customer) {
            this.loading = false;
            this.router.navigate(['home']);
        } else {
            this.product = this.order.product;
            this.customer = this.order.customer;
            this.arpu = Number(this.product.rentaTotal);
            this.model = (this.product);
            this.order.sva = [];
            this.ls.setData(this.order);
            this.getData();
        }

        if (this.type == globals.ORDER_ALTA_NUEVA) {
            var ubicacionPedido = this.ls.getUbicacionPedido();
            this.nameDistrict = ubicacionPedido.distrito;
            this.nameProvince = ubicacionPedido.provincia;
            this.nameDepart = ubicacionPedido.departamento;
        } else if (this.type == globals.ORDER_MEJORAR_EXPERIENCIA || this.type == globals.ORDER_SVA) {
            this.nameDistrict = this.order.product.district;
            this.nameProvince = this.order.product.province;
            this.nameDepart = this.order.product.department;
            this.productActual = this.order.product;
        }
        this.progresBar.getProgressStatus();

        if (this.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
            this.flagProductoActual = true;
        }
    }

    ngAfterViewInit() {
        $("#a1").click()
    }


    getData() {
        let a = localStorage.getItem("productsCampaniaSeleccionada");
        let o = JSON.parse(a);
        this.loading = true;
        if (o) {
            let data = { responseData: o };
            console.log("a: " + o);
            this.cargarServicio(data);
        } else {
            this.offeringService.getData(this.customer, this.product, this.order.user.userId, this.order.user.entity,
                this.order.user.sellerChannelAtis, this.order.user.sellerSegment,
                this.order.user.sellerChannelEquivalentCampaign, this.order.user.sellerEntityEquivalentCampaign, this.order.id).subscribe(
                    data => {
                        this.cargarServicio(data)
                    },
                    err => this.errorCargaOffering(err)
                );
        }

        //obtener localstorage 
        let selectcampana = localStorage.getItem("select-campana");
        if (localStorage.getItem("select-campana")) {
            this.CategoriaSeleccionada = true;
            this.nameselectcategory = selectcampana;
        } else {
            this.CategoriaSeleccionada = false;
            this.nameselectcategory = "";
        }
        this.order.campaniaSeleccionada = this.nameselectcategory;
        this.ls.setData(this.order);
    }

    cargarServicio(data) {
        this.productOffer = data.responseData;

        console.log("Hello");
        console.log("data " + JSON.stringify(data));
        console.log(data.responseData);
        console.log(data.responseCode);

        if(this.productOffer[0].flag_offline){
            this.order.offline.flag = this.order.offline.flag == '' ? '4' : this.order.offline.flag + ',4';
            this.ls.setData(this.order);
        }

        if (data.responseData == undefined) {
            alert(data.responseMessage);
            this.loading = false;
            return false;
        }

        this.productOffer.filter(
            (item) => {
                if (item.productTypeCode == '3') {
                    console.log("11")
                    this.productOfferTrio.push(item)
                } else if (item.productTypeCode == '4' || item.productTypeCode == '5') {
                    console.log("22")
                    this.productOfferDuo.push(item)
                } else if (item.productTypeCode == '6' || item.productTypeCode == '1' || item.productTypeCode == '2') {
                    console.log("33")
                    this.productOfferMono.push(item)
                }
            }
        )
        console.log("DUO " + this.productOfferDuo.length)
        console.log(this.productOfferTrio)
        try {
            if (data.responseCode != 5)
                this.errorCode = data.responseCode;
        } catch (e) {
        }
        this.errorMessage = data.responseMessage;
        if (this.type == globals.ORDER_ALTA_NUEVA) {
            this.showButtonSvas = false;
        }
        if (data.responseCode && data.responseCode == "-1") {
            if (this.type !== globals.ORDER_ALTA_NUEVA) {
                this.showButtonSvas = true;
                this.showButtonOnlySvas = true;
            }
        }

        if (this.productOffer && this.productOffer.length > 0) {
            this.productsEmpty = false;
        } else {
            this.productsEmpty = true;
        }
        this.cargarCombos(this.productOffer);
        this.cargarImagenes(this.productOffer);
        this.loadPanelFlag();

    }

    cargarCombos(productOffer: ProductOffering[]) {
        let tmpCamp: Array<string> = [];
        let tmpTP: Array<string> = [];

        if (productOffer) {
            productOffer.forEach((item, index) => {
                if (!tmpCamp.includes(item.campaign)) {
                    tmpCamp.push(item.campaign);
                }
                if (!tmpTP.includes(item.productType)) {
                    tmpTP.push(item.productType);
                }
            });
        }
        this.campaigns = tmpCamp;
        this.typeProducts = tmpTP;
    }

    cargarImagenes(pro: ProductOffering[]) {
        this.loading = false;
        if (pro) {
            if (pro.length <= 0) {
                this.is_product_empty = true;
                this.visible_accordion = false;
            } else {
                this.visible_accordion = true;
                for (let i = 0; i < pro.length; i++) {
                    this.imagenes1[i] = "abrir_accordion.png";
                    this.img_upgrades[i] = "upgrade2.png";
                }
            }
        } else {
            console.log("Error al consumir servicio");
        }
        return "";
    }

    errorCargaOffering(err) {
        this.loading = false;
        this.errorMessage = "Por favor vuelva a intentarlo";
    }

    cambiarIcono(index) {
        if (this.currentProductIndex == index) {
            this.currentProductIndex = -1;
        } else {
            this.currentProductIndex = index;
        }
    }

    sendOffering(prod) {

        console.log("prod => " + prod);
        console.log(this.order);

        if (this.ls.getData().editandoCliente) {
            this.mensajeValidacionDatosCliente = "Celular";
            $('#modalCliente').modal('show');
            return;
        }
        this.loading = true
        if (this.order.product.parkType == '') {
            this.order.type = globals.ORDER_ALTA_NUEVA;
        } else {
            this.order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
        }
        this.validateOffering(prod);
    }

    computeNgClassObject(index: number, producto: Producto) {
        return {
            'panel-active': index == this.currentProductIndex,
            'panel-color-0': producto.productTypeCode % 10 == 0,
            'panel-color-1': producto.productTypeCode % 10 == 1,
            'panel-color-2': producto.productTypeCode % 10 == 2,
            'panel-color-3': producto.productTypeCode % 10 == 3,
            'panel-color-4': producto.productTypeCode % 10 == 4,
            'panel-color-5': producto.productTypeCode % 10 == 5,
            'panel-color-6': producto.productTypeCode % 10 == 6,
            'panel-color-7': producto.productTypeCode % 10 == 7,
            'panel-color-8': producto.productTypeCode % 10 == 8,
            'panel-color-9': producto.productTypeCode % 10 == 9
        };
    }

    validateOffering(selectedOffering) {
        let codtypeProducto = selectedOffering.productTypeCode;
        let documentType = this.order.customer.documentType;
        let documentNumber = this.order.customer.documentNumber;
        let productName = selectedOffering.productName;
        this.showImgValidate = true;

        this.showAlert = false;


        let resumen = JSON.parse(localStorage.getItem('resumen'));

        console.log("resumen");
        console.log(resumen);

        if (resumen == null) {
            resumen = {
                producto: productName,
                direccion: "direccion",
                costo: selectedOffering.installCost,
                precio: selectedOffering.price
            }
        }

        resumen.producto = productName
        localStorage.setItem("resumen", JSON.stringify(resumen));

        if(documentType == 'RUC'){
            this.showAlert = false;
            this.order.status = "O";
            this.order.selectedOffering = selectedOffering;
            this.order.sva = [];
            if (selectedOffering.sva != null) {
                let sva = selectedOffering.sva;
                sva.selected = true;
                this.order.sva.push(sva);
            }
            //seteamos los sva por default en el localstorage... 
            localStorage.setItem("defaultSvas", JSON.stringify(this.order.sva));
            localStorage.setItem("productActual", JSON.stringify(this.productActual));
            this.ls.setData(this.order);
            this.router.navigate(['svaOut']);
        }
        else{
            this.customerService.getValidateDuplicadoUbicacion(documentType, documentNumber, codtypeProducto, productName, this.nameDepart, this.nameProvince, this.nameDistrict)
            .subscribe(
                data => {
                    if (typeof data.responseData !== 'undefined' && data.responseData !== null) {

                        this.answerValidationOffer = data;

                        if (this.answerValidationOffer.responseCode != "00") {

                            this.showImgValidate = false;
                            this.showAlert = true;
                            this.loading = false;

                            document.getElementsByTagName('html')[0].scrollTop = 150;

                        } else {

                            this.showAlert = false;
                            this.order.status = "O";
                            this.order.selectedOffering = selectedOffering;
                            this.order.sva = [];
                            if (selectedOffering.sva != null) {
                                let sva = selectedOffering.sva;
                                sva.selected = true;
                                this.order.sva.push(sva);
                            }
                            //seteamos los sva por default en el localstorage... 
                            localStorage.setItem("defaultSvas", JSON.stringify(this.order.sva));
                            localStorage.setItem("productActual", JSON.stringify(this.productActual));
                            this.ls.setData(this.order);
                            this.router.navigate(['svaOut']);
                        }


                    } else {
                        this.loading = false
                        this.showImgValidate = false;
                        this.showAlert = true;
                        this.answerValidationOffer.responseMessage = "La validacion no pudo concretarse. Intente nuevamente";
                    }

                },
                err => {
                    console.log(err)
                }
            );
        }
        
    }

    completValidate(showMessage) {

        this.showImgValidate = false;
        if (!showMessage) {
            this.router.navigate(['svaOut']);
        }

    }

    isEmpty(o) {
        for (var i in o) { return false; }
        return true;
    }

    loadPanelFlag() {

        if (this.productOffer) {
            this.productOffer.forEach((item, index) => {
                if (index < globals.PANEL_FLAG) {
                    this.panelFlag.push(true);
                } else {
                    this.panelFlag.push(false);
                }

            });
        }

    }

    showAllPanel() {
        this.panelFlag.forEach((item, index) => {
            if (index >= globals.PANEL_FLAG) {
                if (this.panelFlag[index] == true) {
                    this.panelFlag[index] = false;
                    this.panelFlagMsg = "Ver Mas";
                } else {
                    this.panelFlag[index] = true;
                    this.panelFlagMsg = "Ver Menos";
                }
            }
        });
    }

    // Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"
    cerrarDialogo(nombreModal) {
        $("#" + nombreModal).modal('hide');
    }

    setButtonColor = prod => {
        let classes = {}
        switch (prod.productTypeCode) {
            case '1':
            case '2':
            case '6':
                if (prod.productCategory == "Internet Naked") {
                    classes = { boton_naked: true }
                } else {
                    classes = { boton_lila: true }
                }
                break
            case '3':
                classes = { boton_greentdp: true }
                break
            case '4':
            case '5':
                classes = { boton_cyan: true }
                break
        }
        return classes
    }
    setClasses(prod) {
        let classes = {}
        switch (prod.productTypeCode) {
            case '1':
                classes = { mono_inf: true, acordeon: true }
                break
            case '2':
                if (prod.productCategory == "Internet Naked") {
                    classes = { mono_naked_inf: true, acordeon: true }
                } else {
                    classes = { mono_inf: true, acordeon: true }
                }
                break
            case '3':
                classes = { trio_inf: true, acordeon: true }
                break
            case '4':
                classes = { duo_inf: true, acordeon: true }
                break
            case '5':
                classes = { duo_inf: true, acordeon: true }
                break
            case '6':
                classes = { mono_inf: true, acordeon: true }
                break
            default:
                classes = { mono_inf: true, acordeon: true }
        }
        return classes
    }

    toggleMove(ind: string) {
        if (this.tab1) {
            this.selectAccordionElement("toggle", ind)
        }
        if (this.tab2) {
            this.selectAccordionElement("trio", ind)
        }
        if (this.tab3) {
            this.selectAccordionElement("duo", ind)
        }
        if (this.tab4) {
            this.selectAccordionElement("mono", ind)
        }
    }

    selectAccordionElement(pre: string, ind: string) {
        let parent: string = pre + ind
        $(".accordion-toggle").parent().toggleClass('ic')
        $('#' + parent).next().slideToggle('fast')
        $(".accordion-content").not($('#' + parent).next()).slideUp('fast')
    }

    visibility(id) {
        for (let i = 1; i <= 4; i++) {
            if (i == id) {
                $("#tab" + i + "").show()
                this.elementChoosen(i)
                if (i == 1) {
                    this.tab1 = true
                    this.tab2 = false
                    this.tab3 = false
                    this.tab4 = false
                }
                if (i == 2) {
                    this.tab2 = true
                    this.tab1 = false
                    this.tab3 = false
                    this.tab4 = false
                }
                if (i == 3) {
                    this.tab3 = true
                    this.tab1 = false
                    this.tab2 = false
                    this.tab4 = false
                }
                if (i == 4) {
                    this.tab4 = true
                    this.tab1 = false
                    this.tab2 = false
                    this.tab3 = false
                }
            } else {
                $("#tab" + i + "").hide()
            }
        }
    }

    elementChoosen = (id) => {
        for (let i = 1; i <= 4; i++) {
            if (i == id) {
                $("#a" + i + "").parent().addClass("active")
                if (i == 1) {
                    $("#a" + i + "").parent().css({ "color": "#50535a" });
                }
                if (i == 2) {
                    $("#a" + i + "").parent().css({ "color": "#5bc500" });
                }
                if (i == 3) {
                    $("#a" + i + "").parent().css({ "color": "#00a9e0" });
                }
                if (i == 4) {
                    $("#a" + i + "").parent().css({ "color": "#954B97" });
                }
            } else {
                $("#a" + i + "").parent().removeClass("active")
                $("#a" + i + "").parent().css({ "color": "#50535a" });
            }
        }
    }
}
