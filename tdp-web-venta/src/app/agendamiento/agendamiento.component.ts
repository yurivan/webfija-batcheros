import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common'
import { Router } from '@angular/router';

import { CustomerService } from '../services/customer.service';


import { LocalStorageService } from '../services/ls.service';

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { AgendamientoService } from '../services/agendamiento.service';



import { Agendamiento } from '../model/agendamientos';
import { Calendario } from '../model/calendario';
import { Order } from '../model/order';


import { Customer } from '../model/customer';
import { locale } from 'moment';
import { error } from 'util';
import { setInterval } from 'timers';

var $ = require('jquery');
class RequestDepartment {
	id: string
	value: any
}

@Component({
	moduleId: 'agendamiento',
	selector: 'agendamiento',
	templateUrl: './agendamiento.template.html',
	providers: [DatePipe]
})



export class AgendamientoComponent implements OnInit {
	loading = false;
	order:Order

	id_visor: string
	agentClientName: string
	agentClientTel: string
	agentClientFrangaHoraria: string
	agentServeFechaAuditoria: string
	agentClientFechaSeleccionada: string

 
	model: Customer = new Customer;
	mobilePhone: string = "";

	
	active: boolean
	
	i:number

	calendario:Calendario
	cliente_name:string
	cliente_telefono:string
	


	dia1:string
	dia2:string
	dia3:string
	dia4:string
	dia5:string
	dia6:string
	dia7:string
	dia8:string
	dia9:string
	dia10:string
 
	nameDia1:string
	nameDia2:string
	nameDia3:string
	nameDia4:string
	nameDia5:string
	nameDia6:string
	nameDia7:string
	nameDia8:string
	nameDia9:string
	nameDia10:string

	cuponesUtilizadoActualizado:number
	id_cupon:number
	temp_hora:number

	
	 diaNameArrayA = []
	 diaNumArrayA = []

	 diaNameArrayB = []
	 diaNumArrayB = []

	 diaNameArray = []
	 diaNumArray = []
	

	diaNumCuponUtilizadoA = []
	diaNumCupomA = []

	 diaNumCuponUtilizadoB = []
	 diaNumCupomB = []

	diaNumCuponUtilizado = []
	diaNumCupom = []

	id_allA = []
	id_allB = []
	id_all = []
	
	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService,
		private datepipe: DatePipe,
		private agendamientoService:AgendamientoService) {
		this.router = router;
		this.ls = ls;
		this.i = 1
		this.active = false
		}

	ngOnInit() {

		let now = new Date();
		var hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
        let minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }

		let time = hour+minute
		this.temp_hora = Number(time) 

		
		this.loading= true
		this.order = JSON.parse(localStorage.getItem("order"))
		this.order.flujoAgendamiento=true;
		this.ls.setData(this.order);

	
		
		let order_ = JSON.parse(localStorage.getItem("order"))
		// this.cliente_name = order_.customer.firstName + " " + order_.customer.lastName1  + " " + order_.customer.lastName2;
		 this.cliente_name = order_.customer.firstName;
		 this.cliente_telefono = order_.customer.mobilePhone;
		
		this.agendamientoService.getCalendarioInit().subscribe(
			data =>{
				console.log(this.calendario = data.responseData)
				 
			    for (let entry of data.responseData) {	
					
				}
				
			 
				var insA = 0
				
				for (let indiceCampaign in data.responseData) { 
					if(data.responseData[indiceCampaign].cupones_franja == 'AM'){
						 
						let value =  this.getDayWeek(JSON.stringify(this.calendario[indiceCampaign].cupones_fecha))
						let cuponUtilizados =  JSON.stringify(this.calendario[indiceCampaign].cupones_utilizados)
						let cupon =  JSON.stringify(this.calendario[indiceCampaign].cupones)
						let id =  JSON.stringify(this.calendario[indiceCampaign].cupones_id)
						if(value != "domingo"){

							let dia =  this.calendario[indiceCampaign].cupones_fecha
							this.diaNameArrayA[insA] = value;
							this.diaNumArrayA[insA] = dia;
							this.diaNumCuponUtilizadoA[insA] = cuponUtilizados;
							this.diaNumCupomA[insA] = cupon;

							 this.id_allA[insA]= id
							

							insA++

						}


					
					}
				}

		 
				var insB = 0
				
				for (let indiceCampaign in data.responseData) { 
					if(data.responseData[indiceCampaign].cupones_franja == 'PM'){
						let value =  this.getDayWeek(JSON.stringify(this.calendario[indiceCampaign].cupones_fecha))
						let cuponUtilizados =  JSON.stringify(this.calendario[indiceCampaign].cupones_utilizados)
						let cupon =  JSON.stringify(this.calendario[indiceCampaign].cupones)
						let id =  JSON.stringify(this.calendario[indiceCampaign].cupones_id)
						if(value != "domingo"){
							let dia =  this.calendario[indiceCampaign].cupones_fecha
							this.diaNameArrayB[insB] = value
							this.diaNumArrayB[insB] = dia
							this.diaNumCuponUtilizadoB[insB] = cuponUtilizados;
							this.diaNumCupomB[insB] = cupon;
							this.id_allB[insB]= id
							insB++
						}
					}
				}

				this.initCalendarioDefault();

				this.loading = false;
				
			},
			err=>{
				alert("error")
				this.loading = false;
			}
		)

		
	}
	initCalendarioDefault(){
		 this.diaNameArray = this.diaNameArrayA
		  this.diaNumArray = this.diaNumArrayA
		  this.agentClientFechaSeleccionada = ""

		  this.diaNumCuponUtilizado = this.diaNumCuponUtilizadoA
		  this.diaNumCupom = this.diaNumCupomA
		  this.id_all = this.id_allA
 
	
		   

	}

	ngAfterViewChecked(){
		for(let i in this.diaNumCupom){ 
			this.diaNumCupom[i]
			if(this.diaNumCupom[i] === this.diaNumCuponUtilizado[i]){
				$(".desa"+i).addClass("desab");

				console.log("Desabilitado"+i);
			}
		}
	} 
	//obtener el día de la fecha seleccionada


	//  js_yyyy_mm_dd_hh_mm_ss () {
		// var hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
	// 	// let year = "" + now.getFullYear();
	// 	// let month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
	// 	// let day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
	// 	// let minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
	// 	// let second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
	// 	// return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
	// 	return year + "-" + month + "-" + day + " " + hour;
	//   }

	initCalendario(data:string){
		let now = new Date();
		var hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
        let minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }

		let time = hour+minute
		this.temp_hora = Number(time) 
		let a = this.temp_hora 
		
		this.agentClientFechaSeleccionada = ""

		for(let i = 0; i<11;i++){
			$(".desa"+i).removeClass("chosen");
			 
			
		}

		for(let i = 0; i<11;i++){
			$(".desa"+i).removeClass("desab");
			
		}
		// alert(a)
		
		if(data == "AM"){
			this.diaNameArray = this.diaNameArrayA
			this.diaNumArray = this.diaNumArrayA
			this.diaNumCuponUtilizado = this.diaNumCuponUtilizadoA
			this.diaNumCupom = this.diaNumCupomA
			this.id_all = this.id_allA
			if(a<1301){
				
			//    $(".desa1").addClass("ooo1");
			   $(".ooo1").removeClass("ooo");
			   $(".ooo1").addClass("desa1");
			   $(".ooo1").addClass("desab");
			   
			   
			//    $(".ooo1").addClass("ooo");
			//    $(".ooo1").removeClass("desab");
			   // $(".desa1").addClass(".chosen");
			   // alert("entroooo ")
			   console.log("Entrooooo")
		   }

			
		}else{
			this.diaNameArray = this.diaNameArrayB
			this.diaNumArray = this.diaNumArrayB
			this.diaNumCuponUtilizado = this.diaNumCuponUtilizadoB
			this.diaNumCupom = this.diaNumCupomB
			this.id_all = this.id_allB
			//Si presiono la 
			// =>=>=>=>=>=> PM
			//Se activa siempre y cuando 
			if(a<1301){
				 
				$(".desa1").addClass("ooo1");
				$(".desa1").removeClass("desa1");
				$(".ooo1").addClass("ooo");
				$(".ooo1").removeClass("desab");
				// $(".desa1").addClass(".chosen");
				// alert("entroooo ")
				console.log("Entrooooo")
			}

		}



		for(let i in this.diaNumCupom){ 
			this.diaNumCupom[i]
			if(this.diaNumCupom[i] === this.diaNumCuponUtilizado[i]){
				$(".desa"+i).addClass("desab");
				console.log("Desabilitado"+i);
			}
		}
		
	   
	}

	getDayWeek(data:string){
		var d,
		days;
		d = new Date(data);
		days = ['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
		console.log(days[d.getUTCDay()]);
		// alert(days[d.getUTCDay()]);
		return days[d.getUTCDay()]
	}
	

	addPersona(){

 
	  
		let name0 = $('#name0').val();
		let name = $('#name'+this.i+'').val();

		let nameLast = this.i-1
		let valLast = $('#name'+nameLast+'').val();
		let telLast = $('#tel'+nameLast+'').val();
		// if(!name0){
		// 	alert("No puede agregar más")
		// 	return
		// }

		

		let active = this.active

		if(!valLast){
			alert("complete el campo nombre")
			return
		}else if(!telLast || telLast.substring(0, 1) != "9"){
			alert("complete el campo teléfono y el teléfono debe empezar con '9'")
			return
		} 
			
		if(active){
			if(!valLast){
				alert("complete el campo nombre")
				return
			}else if(!telLast){
				alert("complete el campo teléfono")
				return
			}
		}
		$(".item").append('<div class="col-4 input-effect"> <input class="effect-16" type="text" placeholder="" id="name'+this.i+'" > <label style="position: absolute;left: 0;width: 100%;top: 0px;top: -16px;font-size: 14px;color: #3399FF;z-index: -1;letter-spacing: 0.5px;text-align: left;">Ingresa nombre</label> <span class="focus-border"></span> </div> <div class="col-4 input-effect"> <input class="effect-16 tels" type="text" id="tel'+this.i+'" requerid  maxlength="9" > <label style="position: absolute;left: 0;width: 100%;top: 0px;top: -16px;font-size: 14px;color: #3399FF;z-index: -1;letter-spacing: 0.5px;text-align: left;">Ingresa teléfono</label> <span class="focus-border"></span> </div> <div class="col-3 "> </div>');
 
		

		this.i = this.i + 1
		this.active = true
		
	}
 
	saveAgendamiento(){	


		let order = JSON.parse(localStorage.getItem('order'));
		order.customer = this.model;
		order.customer.mobilePhone = $('#tel0').val();
		let telLast = order.customer.mobilePhone;



		
	    if(!telLast || telLast.substring(0, 1) != "9"){
			alert("complete el campo teléfono y el teléfono debe empezar con '9'")
			return
		} 
	  
		this.loading = true;
		// Recuperar data de agendamiento
		let contadorActual = this.i
		let arrayNames = []
		let arrayTel = []
		for(let i = 0; i<this.i;i++){
			let value = $('#name'+i+'').val(); 
			let tels = $('#tel'+i+'').val(); 
				arrayNames[i] = value;
				arrayTel[i] = tels;
			
		}
		let frangaVal =  $('input[name=radioName]:checked', '#myForm').val(); 

		this.id_visor = this.order.id
		console.log("this.order.id"+this.order.id)
		this.agentClientName = arrayNames.toString()
		this.agentClientTel = arrayTel.toString()
		this.agentClientFrangaHoraria = frangaVal
		this.agentServeFechaAuditoria = ""

	 

		// this.agentClientFechaSeleccionada = ""
		if(!this.agentClientFechaSeleccionada){
			alert("Seleccione una fecha agendada: ");
			this.loading = false;
			return
		}


		let departamento = "";
		let canal = "";
		let tecnologia = "";
	 

		let request = {
			departamento:departamento,
			canal:canal,
			tecnologia:departamento,

			id_visor:this.id_visor,
			agentClientName:this.agentClientName,
			agentClientTel:this.agentClientTel,
			agentClientFrangaHoraria: this.agentClientFrangaHoraria,
			agentClientFechaSeleccionada:this.agentClientFechaSeleccionada,
			cuponesUtilizadoActualizado:this.cuponesUtilizadoActualizado,
			id_cupon:this.id_cupon
		};

	
		localStorage.setItem("agendamientoGuardar",JSON.stringify(request));

		// if(this.order.user.typeFlujoTipificacion =='PRESENCIAL'){
		// 	this.router.navigate(['contactoinstalacion']);
		// 	//  this.router.navigate(['saleProcess/close']);
		// 	this.loading = false;
			
		// }else{
		// 	 this.router.navigate(['saleProcess/contract']);
		// 	//  this.router.navigate(['saleProcess/close']);
		// 	//this.router.navigate(['contactoinstalacion']);
			
		// 	this.loading = false;
		// }
		this.agendamientoService.saveAgendamiento(this.id_visor,this.agentClientName,this.agentClientTel,this.agentClientFrangaHoraria,this.agentClientFechaSeleccionada,this.cuponesUtilizadoActualizado,this.id_cupon).subscribe(
			data =>{
				// alert("enviando") 
				if(this.order.user.typeFlujoTipificacion =='PRESENCIAL'){
					this.router.navigate(['contactoinstalacion']);
					//  this.router.navigate(['saleProcess/close']);
					this.loading = false;
					
				}else{
					 this.router.navigate(['saleProcess/contract']);
					//  this.router.navigate(['saleProcess/close']);
					//this.router.navigate(['contactoinstalacion']);
					
					this.loading = false;
				}

			
			},
			err=>{
				alert("error")
				this.loading = false;
			}
		)
	}
 
	saveDay(num:number){
	
		if($('.desa'+num).hasClass("desab")){
			// alert("desabilitado")
			return
		}

		
		
		let id = this.id_all[num]
		let cuponesUtilizado = this.diaNumCuponUtilizado[num]
		let cuponTotal = this.diaNumCupom[num]
		  
		let o = Number(cuponesUtilizado) 
	 
 
		let suma = o+1
	 
		// if(cuponTotal > cuponesUtilizado){ 
			this.cuponesUtilizadoActualizado = suma
			this.id_cupon = Number(id) 
		// }


		this.agentClientFechaSeleccionada = this.diaNumArray[num]

		console.log(this.agentClientFechaSeleccionada);
		
		console.log(this.diaNumCuponUtilizado[num])
		console.log(this.diaNumCupom[num])

		let temp_hora = this.temp_hora
	

		if(num == 0){
			this.agentClientFechaSeleccionada = ""
			return
		} 
		else{
			if(num == 1){
				if(temp_hora>1301){
			this.agentClientFechaSeleccionada = ""
			return
		}else{
					for(let i = 0; i<11;i++){
						$(".desa"+i).removeClass("chosen");
						$(".ooo"+i).removeClass("chosen");
						
					} 
					$(".desa"+num).addClass("chosen");
					if(num === 1){
						$(".ooo1").addClass("chosen");
					}
					$(".ooo1").removeClass("ooo");
				}	
			}

			for(let i = 0; i<11;i++){
				$(".desa"+i).removeClass("chosen");
				$(".ooo"+i).removeClass("chosen");
				
			} 
			$(".desa"+num).addClass("chosen");
			if(num === 1){
				$(".ooo1").addClass("chosen");
			}
			$(".ooo1").removeClass("ooo");
			
			 
		}
	 
		// console.log(result)
		
	}

}