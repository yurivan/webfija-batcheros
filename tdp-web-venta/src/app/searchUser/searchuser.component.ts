import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { CustomerService } from '../services/customer.service';
import { Customer } from '../model/customer';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';
import { DireccionService } from '../services/direccion.service';

import { ScoringService } from '../services/scoring.service'; // Sprint 8

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { CancelSaleService } from '../services/cancelsaleservice';
import { global } from '@angular/core/src/facade/lang';
import { Argumentarios } from '../model/argumentarios';
import { Parameters } from '../model/parameters';

//Sprint 24
import { BlackListService } from '../services/blacklist.service';
import { empty } from 'rxjs/Observer';

var $ = require('jquery');
class RequestDepartment {
	id: string
	value: any
}
@Component({
	moduleId: 'searchUser',
	selector: 'searchUser',
	templateUrl: './searchuser.template.html',
	providers: [CustomerService, DireccionService, ScoringService]
})

export class SearchUserComponent implements OnInit {
	messageScoringError: String;

	messageScoringCod: boolean

	messageScoringErrorVisual: boolean
	
	// codinterconect: String;
	cod_int: string
	renta_Inter_Conect: string

	showCancelSaleLink: boolean = false
	newCustomer: Customer[];
	order: Order;
	canalEntidad : Parameters;
	argumentarios : Argumentarios[];
	mjsSearchUser : string;
	modelDirectio: string = "";
	selectedType: any;
	documentNumber: any;
	isTrue = true;
	loading = false;
	previousUrl;
	errorDocumentType: boolean;
	errorDocumentNumber: boolean;

	docNroRuc:string

	showDni: boolean = true;
	showCarneExtranjeria: boolean = false;
	showOtrosCE: boolean = false;
	docNroDni: string;
	docNroCarnetExt: string;
	docNroOtrosCE: string;
	errorConexion: string;
	msgError: boolean = false;

	// Sprint 7... tipos de documento a mostrar son configurables
	showTipoDocumentoDNI: boolean = true;
	showTipoDocumentoCEX: boolean = false;
	showTipoDocumentoPAS: boolean = false;
	showTipoDocumentoRUC: boolean = false;
	showTipoDocumentoOtrosCE: boolean = false;

	showPasaporte: boolean = false;
	showRUC: boolean = false;

	/* Sprint 8 - se adicionan listas de departamentos, provincias y distritos */
	objDepart: any;
	private keyDeparts: string[];
	private departSeleccionado = 0;
	selectDepart: any;

	objProvs: any;
	showProvs = new Array();
	private keyProvs: string[];
	selectProv: any;
	private proviSeleccionado = 0;

	objDists: any;
	showDists = new Array();
	selectDistric: any;
	private keyDistrs: string[];
	private districSeleccionado = 0;

	private loadError: boolean;
	scoringDataModel: any;
	respuestaScoring: boolean;
	private mensaje_error: String;
	customer: Customer;
	scoringService: ScoringService;// Sprint 8
	direccionService: DireccionService; // Sprint 8
	code: string
	isItMigraProcess: boolean
	showUbicacionDefault: boolean;
	isAltaPura: boolean = false;

	deudaClienteMensaje: string;
	deudaClienteMensajeEstado: boolean = false;
	
	accionNull:string

	//Sprint 24
	respuestaDataCustomer :boolean;
	deuda_tot : string;
	deuda_dec : string;
	nom_cliente : string;
	ape_cliente : string;
	lineas_cliente : string;
	tip_doc_cliente : string;
	num_doc_cliente : string;
	fec_ult_deuda : Date;
	fec_ult_rec_emi : Date;
	linea_desc : string;
	mes_desc : string;
	tiene_deuda : boolean;
	tot_meses : number;
	fec_actual : Date;
	cli_sin_datos :boolean = false;
	serv_deuda_act : boolean = true;
	tiempo_tot : string;
	loadErrorDeuda : boolean = false;
	mensaje_error_deuda : string;
	strmsgerror : string[];
	error_respuesta : boolean = false;

	//Sprint 24 RUC
	objRuc: any;
	ruc_SinData : boolean = false;

	//Sprint 28 Contingencia Scoring
	scoringContingencia : boolean = false;

	@ViewChild('buttonEvaluar') buttonEvaluarRef: ElementRef;

	constructor(private router: Router,
		private customerService: CustomerService,
		private ls: LocalStorageService,
		direccionService: DireccionService,
		scoringService: ScoringService,
		private progresBar: ProgressBarService,
		private cancelSaleService: CancelSaleService, 
		private blackListService: BlackListService) {
		this.router = router;
		this.customerService = customerService;
		this.ls = ls;
		this.scoringService = scoringService;// Sprint 8
		this.direccionService = direccionService; // Sprint 8
		this.code = ''
		this.isItMigraProcess = false
		this.messageScoringCod = false
		this.deudaClienteMensaje = ""
		this.deudaClienteMensajeEstado = false
		this.messageScoringErrorVisual = false
		
		//Sprint 24
		this.blackListService = blackListService;
	}
	ngOnDestroy(){
		if(localStorage.getItem("ventaFinalizada")){
			localStorage.removeItem("ventaFinalizada")
		}
	}
	ngOnInit() {
		//this.order = JSON.parse(localStorage.getItem('order'));
		this.order = this.ls.getData();
		this.docNroRuc = ""
		this.isAltaPura = false;
		switch (this.order.type) {
			case 'A':
				//todo
				this.isAltaPura = true;
				break
			case 'M'://migras
				this.isItMigraProcess = true
				break
			case 'S'://servicios adicionales (sva)
				this.isItMigraProcess = true
				break
			default:
				//todo
				break
		}

		this.progresBar.getProgressStatus()

		this.selectedType = 'DNI';
		this.documentNumber = null;
		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}

		this.docNroDni = '';
		//$('#documentType').focus();

		// Sprint 7... tipos de documento a mostrar son configurables
		this.showTipoDocumentoCEX = globals.SHOW_CEX;
		this.showTipoDocumentoPAS = globals.SHOW_PAS;
		this.showTipoDocumentoRUC = globals.SHOW_RUC;
		this.showTipoDocumentoOtrosCE = globals.SHOW_OTROSCE;

		// Sprint 8... carga de departamentos, provincias, distritos
		this.obtenerDepartamentos();
		this.obtenerProvincias();
		this.obtenerDistritos();

		this.showUbicacionDefault = false;

		// Sprint 8... verificamos valores por default ubicacion
		this.cargarValoresDefaultUbicacion();

		this.aplicarEfectos();
		this.canalEntidad = JSON.parse(localStorage.getItem('canalEntidad'))
		this.argumentarios = JSON.parse(localStorage.getItem('argumentarios'));
		this.mjsSearchUser="";
		for (let i in this.argumentarios) {
			let obj = new Argumentarios;
			obj = this.argumentarios[i];
			if(obj.pantalla.toUpperCase() == "searchUser".toUpperCase()){
				if(obj.nivel == "COMPLETO"){
					this.mjsSearchUser=obj.mensaje;
				}
				if(obj.flujo == "A"){
					this.mjsSearchUser=this.mjsSearchUser + obj.mensaje;
				}
			}
			//console.log("Mensaje Argumntario  mjsSearchUser: "+ this.mjsSearchUser);
		}

		if(this.order.type == 'M' || this.order.type == 'S'){
			this.showTipoDocumentoRUC = false; //Sprint 24 RUC Eliminar
		}
	}

	aplicarEfectosChange() {

		$("#tablaDat").addClass("none");

		$(".input-effect input").val("");

		$(".input-effect input").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
		$(".paraselect select").val("");
		$(".paraselect select").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
	}

	focusout(target) {
		this.focusoutById(target.id);
		/*var target = $("#" + target.id);
		if (target.val() != "") {
			target.addClass("has-content");
		} else {
			target.removeClass("has-content");
		}*/

	}

	restringirScoring(numdoc){
		if(("customer" in this.order)){
			if(numdoc !== this.order.customer.documentNumber || (this.selectedType !== this.order.customer.documentType && numdoc == this.order.customer.documentNumber)){
				this.scoringContingencia = false;
			}
			else if(numdoc == this.order.customer.documentNumber && this.selectedType == this.order.customer.documentType){
				this.scoringContingencia = true;
			}
		}
	}

	focusoutById(id) {
		var target = $("#" + id);
		if (target.val() != "") {
			target.addClass("has-content");
		} else {
			target.removeClass("has-content");
		}
	}

	aplicarEfectos() {
		var _this = this;
		$(document).ready(function () {
			//$(".input-effect input").val("");
			var inputsIdSinEfecto = "";
			if (_this.order.departmentScoring) {
				inputsIdSinEfecto = "#divDocumentNumber";
				_this.focusoutById("docNroDni");
			}
			$(".input-effect:not(" + inputsIdSinEfecto + ") input").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})
			// obviamos el efecto para el tipo de documento
			// si es que tenemos una ubicacion por default entonces tambien debemos obviar el departamento, provincia y distrito
			var idsSinEfecto = "#divDocumentType";
			if (_this.showUbicacionDefault) {
				idsSinEfecto += ",#divDepartament,#divCity,#divDistrict";
			}
			$(".paraselect:not(" + idsSinEfecto + ") select").val("");
			$(".paraselect select").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})
		})
	}

	// Sprint 8... verificamos valores por default ubicacion
	cargarValoresDefaultUbicacion() {
		this.selectDepart = document.getElementById("department");
		this.selectProv = document.getElementById("city");
		this.selectDistric = document.getElementById("district");




		if (this.order.departmentScoring) {
			this.verificarUbicacionDefault();
			this.docNroDni = this.order.customer.documentNumber;
			this.departSeleccionado = this.order.departmentScoring;
			this.proviSeleccionado = this.order.provinceScoring;
			this.listarProvincias(this.departSeleccionado);
			this.districSeleccionado = this.order.districtScoring;
			this.listarDistritos(this.departSeleccionado + this.proviSeleccionado);
			this.respuestaScoring = true;
			this.scoringDataModel = this.order.scoringDataModel;

			if (this.order.type == "A") {
				/*Sprint-11 */
				//console.log("JSON " + JSON.stringify(this.scoringDataModel.responseData.result[0].detalle))
				var arrayDecosList = [];
				for (let i = 0; i < this.scoringDataModel.responseData.result[0].detalle.length; i++) {
					//console.log("JSON " + JSON.stringify(this.scoringDataModel.responseData.result[0].detalle[i].nroDecosAdic))
					arrayDecosList.push(this.scoringDataModel.responseData.result[0].detalle[i].nroDecosAdic);
				}
				//console.log("arrayDecos " + arrayDecosList);

				localStorage.setItem('arrayDecosList', JSON.stringify(arrayDecosList));
				/*End Sprint-11 */
			}


		} else {
			//primero verificamos si es que la ubicación fue persistida en una venta anterior
			this.verificarUbicacionDefault();
		}
	}

	verificarUbicacionDefault() {
		//primero verificamos si es que la ubicación fue persistida en una venta anterior
		var ubicacionDefault = this.ls.getUbicacionDefault();



		var codInterconectDefault = this.ls.getCodInterconectDefault();
		var lss = this.ls;
		// let cod_interconect = JSON.stringify(data.responseData.codConsulta);

		// alert("getUbicacionDefault "+JSON.stringify(ubicacionDefault));
		// alert("codiInterconect "+codInterconectDefault)
		if (ubicacionDefault != null) {
			this.showUbicacionDefault = true;
			this.departSeleccionado = ubicacionDefault.codigoDepartamento;
			this.proviSeleccionado = ubicacionDefault.codigoProvincia;
			this.listarProvincias(this.departSeleccionado);
			this.districSeleccionado = ubicacionDefault.codigoDistrito;
			this.listarDistritos(this.departSeleccionado + this.proviSeleccionado);

			if (this.selectDepart != null) {
				this.selectDepart.value = this.departSeleccionado;
				this.selectProv.value = this.proviSeleccionado;
				this.selectDistric.value = this.districSeleccionado;
			}
		}

		if (localStorage.getItem('codigoInterConect') != null) {
			this.messageScoringCod = true;

			let codInterconect = localStorage.getItem('codigoInterConect')
			let rentaInterConect = localStorage.getItem('rentaInterConect')

			this.cod_int = codInterconect.slice(1, -1);
			this.renta_Inter_Conect = "S/" + rentaInterConect.slice(1, -1);


		} else {
			this.messageScoringCod = false;
			
		}
	}

	// Sprint 8... se copian funciones obtenerDepartamentos, obtenerProvincias, listarProvincias, obtenerDistritos, listarDistritos
	// selDepart, selCity, selDistrict, limpiarDireccion
	obtenerDepartamentos() {
		this.objDepart = this.direccionService.getDataDepartamento()
		let arrayDepartments: RequestDepartment[] = Array()
		this.keyDeparts = []
		for (let i in this.objDepart) {
			let obj = new RequestDepartment()
			obj.id = i
			obj.value = this.objDepart[i]
			arrayDepartments.push(obj)
		}
		arrayDepartments.sort(this.sortByTwoProperty())
		for (let i = 0; i < arrayDepartments.length; i++) {
			this.keyDeparts.push(arrayDepartments[i]["id"])
		}
	}

	obtenerProvincias() {
		this.objProvs = this.direccionService.getDataProvincias();
	}

	listarProvincias(skuDep) {
		this.showProvs.length = 0;
		this.keyProvs = Object.keys(this.objProvs);
		this.keyProvs.forEach((item, index) => {

			if (this.objProvs[item].skuDep == skuDep) {
				this.showProvs.push(this.objProvs[item]);
			}

		})
		this.showProvs.sort(this.sortByProperty('name'))
	}

	obtenerDistritos() {
		this.objDists = this.direccionService.getDataDistritos();
		//console.log("this.objDists::::: " + this.objDists);
	}

	listarDistritos(skuDepPro) {

		this.showDists.length = 0;
		this.keyDistrs = Object.keys(this.objDists);
		this.keyDistrs.forEach((item, index) => {

			if (this.objDists[item].skuDepPro == skuDepPro) {
				this.showDists.push(this.objDists[item]);

			}
			this.showDists.sort(this.sortByProperty("name"))
		})

	}

	selDepart() {
		this.messageScoringErrorVisual = false
		
		this.deudaClienteMensajeEstado = false
		this.selectDepart = document.getElementById("department");
		this.departSeleccionado = this.selectDepart.value;
		this.listarProvincias(this.departSeleccionado);
		this.limpiarDireccion('department');
		this.messageScoringCod = false;
		this.respuestaScoring = false;

		this.scoringContingencia = false; //Sprint 28 Contingencia Scoring

		this.limpiarDatosDeuda();
	}

	selCity() {
		this.messageScoringErrorVisual = false
		
		this.deudaClienteMensajeEstado = false
		this.selectProv = document.getElementById("city");
		this.proviSeleccionado = this.selectProv.value;
		this.listarDistritos(this.departSeleccionado + this.proviSeleccionado);
		this.limpiarDireccion('province');
		this.messageScoringCod = false;
		this.respuestaScoring = false;

		this.scoringContingencia = false; //Sprint 28 Contingencia Scoring
		
		this.limpiarDatosDeuda();
	}

	selDistrict() {
		this.messageScoringErrorVisual = false
		
		this.deudaClienteMensajeEstado = false
		this.selectDistric = document.getElementById("district");
		this.districSeleccionado = this.selectDistric.value;
		this.messageScoringCod = false;
		this.respuestaScoring = false;
		
		this.scoringContingencia = false; //Sprint 28 Contingencia Scoring

		this.limpiarDatosDeuda();
		// Sprint8 Por UX ahora cuando se cambie algun dato filtro entonces se debe evaluar scoring...
		//this.onSubmit();
	}

	limpiarDireccion(tipo) {
		if ('department' == tipo) {
			this.selectProv = document.getElementById("city");
			this.selectDistric = document.getElementById("district");
			this.selectProv.value = "";
			this.selectDistric.value = "";

			this.proviSeleccionado = 0;
			this.districSeleccionado = 0;
			this.listarDistritos(0);
		} else if ('province' == tipo) {
			this.selectDistric = document.getElementById("district");
			this.selectDistric.value = "";
			this.districSeleccionado = 0;
		}
	}

	//function onChage
	onChangeSelectDocument(value) {
		this.messageScoringCod = false;
		this.scoringContingencia = false;

		if (value == "DNI") {
			this.docNroCarnetExt = '';
			this.docNroOtrosCE = '';
		} else if (value == "CEX") {
			this.docNroDni = '';
			this.docNroOtrosCE = ''
		} else {
			this.docNroCarnetExt = ''
			this.docNroDni = ''
		}


		//this.docNroCarnetExt = '';
		//this.docNroDni = '';

		switch (value) {

			case "DNI":

				this.showDni = true;
				this.showCarneExtranjeria = false;

				this.showPasaporte = false;
				this.showRUC = false;
				this.showOtrosCE = false;
				break;

			case "CEX":

				this.showCarneExtranjeria = true;
				this.showDni = false;

				this.showPasaporte = false;
				this.showRUC = false;
				this.showOtrosCE = false;

				break;

			case "Otros":

				this.showOtrosCE = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;

				this.showPasaporte = false;
				this.showRUC = false;
				break;

			case "PAS":

				this.showPasaporte = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;	
				this.showRUC = false;
				this.showOtrosCE = false;

				break;

			case "RUC":

				this.showRUC = true;
				// this.docNroRuc = "10101"
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showPasaporte = false;
				this.showOtrosCE = false;

				break;

			default:
				this.showDni = true;
				this.showCarneExtranjeria = false;
				this.showPasaporte = false;
				this.showRUC = false;
				this.showOtrosCE = false;
				this.selectedType = "DNI";
				break;
		}
		//this.aplicarEfectosChange();
	}

	onSubmitInMigration() {
		if (this.isItMigraProcess) {
			this.getData()
		} else {
			this.getData()
		}
	}


	getData() {
		this.documentNumber = this.docNroDni || this.docNroCarnetExt || this.docNroOtrosCE || this.docNroRuc;

		this.scoringContingencia = true;
		if (this.documentNumber) {

			if (this.selectedType == 'RUC' && (this.documentNumber.substr(0, 2) !== '10' &&
				this.documentNumber.substr(0, 2) !== '15' &&
				this.documentNumber.substr(0, 2) !== '17' &&
				this.documentNumber.substr(0, 2) !== '20')) {
				//console.log('numero : ' + this.documentNumber.substr(0,2));
				this.ruc_SinData = true;
				this.mostrarError('Aún esos tipos de RUCs por este sistema, ' +
					'por favor ingrese su venta por flujo regular.');
				return;
			}
			this.msgError = false;
			this.loading = true;
			this.customerService.getData(this.selectedType, this.documentNumber).subscribe(
				data => {
				if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
					//console.log("data " + JSON.stringify(data))
					this.cancelSaleService.sendStatus(true)
					let order = new Order();
					order.user = this.order.user;
					order.type = this.order.type;
					//localStorage.setItem('order', JSON.stringify(order));
					//console.log("responseData " + JSON.stringify(data.responseData))
					let telefono = ""

					if (this.order.customer) {
						//console.log("this.customer.telephone" + this.order.customer.telephone)
						telefono = this.order.customer.mobilePhone
					}
					//console.log("telefono " + telefono)
					this.customer = data.responseData
					//telefono por mobilephone	
					if (telefono != null && telefono != "") {
						this.customer.mobilePhone = telefono
					}

					this.customer.documentType = this.selectedType;
					this.customer.documentNumber = this.documentNumber;
					order.customer = this.customer;
					//localStorage.setItem('order', JSON.stringify(order));
					this.ls.setData(order);
					this.loading = false;
					if (this.isItMigraProcess) {
							this.customerService.getOrderIdCustomer().subscribe(
								data => {
									order.id = data.responseMessage;
									this.ls.setData(order)
									this.router.navigate(['/saleProcess/park']);
								},
								err => { }
							);
						//this.router.navigate(['/saleProcess']);
							//this.router.navigate(['/saleProcess/park']);
						//this.router.navigate(['saleProcess/scoring']);
					} else {
							if(this.selectedType == 'RUC'){
								this.consultaRuc();
							}
							else{
							this.evaluarDeuda();
					}
						}

				} else {
					let order = new Order();
					order.status = "N";
					order.user = this.order.user;
					//guardar posicion de type user
					order.type = this.order.type;
					let telefono = ""
					////console.log("telefono")
					if (this.order.customer) {
						//console.log("this.order.customer.telephone" + this.order.customer.telephone)
						telefono = this.order.customer.mobilePhone
					}

					let newCustomer;
					newCustomer = {
						documentType: this.selectedType,
						documentNumber: this.documentNumber,
						firstName: "",
						lastName1: "",
						lastName2: "",
						email: "",
						telephone: "",
						mobilePhone: telefono,
						nationality: ""
					}

					order.customer = newCustomer;

					this.customer = newCustomer;
					//cambiar telephone por mobilephone
					this.customer.mobilePhone = telefono
					this.ls.setData(order);
						//this.loading = false;
					if (this.isItMigraProcess) {
							this.customerService.getOrderIdCustomer().subscribe(
								data => {
									order.id = data.responseMessage;
									this.ls.setData(order)
									this.router.navigate(['/saleProcess/park']);
								},
								err => { }
							);
							//this.router.navigate(['/saleProcess']);
							//this.router.navigate(['/saleProcess/park']);
							//this.router.navigate(['saleProcess/scoring']);
					} else {
							if(this.selectedType == 'RUC'){
								this.consultaRuc();
							}
							else{
							this.evaluarDeuda();
							}
					}

				}
			},
				err => {
					console.log('error', err);
					this.cancelSaleService.sendStatus(false)
					this.loading = false;
					this.msgError = true;
					this.errorConexion = 'Vuelve a Intentar';
				})
		}
	}

	onSubmit() {
		this.getData();
}

	isEmpty(o) {
		for (var i in o) { return false; }
		return true;
	}

	isNumber(o) {
		if (!isNaN(o) && !this.isEmpty(o)) { return true; }
		return false;
	}

	// Sprint 8... copiado del componente scoring
	evaluar() {

		//console.log("Iniciar evaluar");
		this.messageScoringCod = true;

		this.messageScoringError = "";

		//
		this.selectDepart = document.getElementById("department");
		this.selectProv = document.getElementById("city");
		this.selectDistric = document.getElementById("district");


		var departamentoLabel = this.selectDepart[this.selectDepart.selectedIndex].innerHTML;
		var provinciaLabel = this.selectProv[this.selectProv.selectedIndex].innerHTML;
		var distritoLabel = this.selectDistric[this.selectDistric.selectedIndex].innerHTML;

		localStorage.setItem("valid-crediti",distritoLabel)
		
		this.messageScoringErrorVisual = false
		
		if (departamentoLabel == "") {
			this.messageScoringError = "Seleccione un departamento antes de Evaluar.";
			this.messageScoringErrorVisual = true
			this.loading = false;
			this.messageScoringCod = false;
			return;
		}
		if (provinciaLabel == "") {
			this.messageScoringError = "Seleccione una provincia antes de Evaluar.";
			this.loading = false;
			this.messageScoringCod = false;
			this.messageScoringErrorVisual = true
			return;
		}
		if (distritoLabel == "") {
			this.messageScoringError = "Seleccione un distrito antes de Evaluar.";
			this.loading = false;
			this.messageScoringCod = false;
			this.messageScoringErrorVisual = true
			return;
		}

		this.order = this.ls.getData();
		//guardamos el departamento, ciudad y distrito seleccionados para el scoring
		//this.order = JSON.parse(localStorage.getItem('order'));
		this.order.departmentScoring = this.selectDepart.value;
		this.order.provinceScoring = this.selectProv.value;
		this.order.districtScoring = this.selectDistric.value;
		//console.log('Seteando data de Order-->' + JSON.stringify(this.order.customer));
		this.ls.setData(this.order);

		var ubigeo = "" + this.selectDepart.value + this.selectProv.value + this.selectDistric.value;
		let request = {}

		//Sprint 24 RUC
		this.scoringService.getData(this.customer, this.order.user.userId, departamentoLabel, provinciaLabel, distritoLabel, ubigeo, this.order.user.channel)
			.subscribe(
			data => {
				this.cargarScoringData(data),
				this.cancelSaleService.sendStatus(true),
 
				request = {
					codConsulta: data.responseData.codConsulta,
					accion: data.responseData.accion,
					deudaAtis: data.responseData.deudaAtis,
					deudaCms: data.responseData.deudaCms,
					result: data.responseData.result
				}
				localStorage.setItem("scoring_offering",JSON.stringify(request))
					//console.log("Data_Scoring -->" + JSON.stringify(request));
				this.accionNull = data.responseData.accion
					this.loading = false;
			},
			err => this.errorScoringData(err),
		);

		//this.router.navigate( ['saleProcess/direccion'] );//sprint2 ahora se debe cargar el scoring como 3ra pantalla

		// let ooo = localStorage.getItem("a_scoring_offering")
		// //console.log(JSON.parse(ooo));

	}

	evaluarDeuda(){
		let _scope = this
		this.loading = true;
		this.order.offline.flag = '';
		if(this.selectedType == 'RUC' || this.selectedType == 'Otros'){
			_scope.respuestaDataCustomer = true;
			_scope.serv_deuda_act = false;
			_scope.tiene_deuda = false;
			_scope.evaluar();
		}else{
			this.customerService.getdataCustomer(this.selectedType, this.documentNumber)
			.subscribe(
				data =>{
					//console.log("data deuda --> " + JSON.stringify(data))
					_scope.respuestaDataCustomer = false;
					if (data.responseCode == -1) {
						_scope.respuestaDataCustomer = true;
						_scope.tiene_deuda = false;
						_scope.serv_deuda_act = false;
						_scope.loadErrorDeuda = true;
						_scope.loading = false;
						_scope.strmsgerror = data.responseMessage.split('|');
						_scope.mensaje_error_deuda = _scope.strmsgerror[0];
						_scope.order.offline.flag += _scope.order.offline.flag == '' ? '2' : ',2';
						_scope.ls.setData(_scope.order);
						//console.log('Servicio Consulta deuda - Error Interno --> Detalle: ' + _scope.strmsgerror[1]);
						return;
					}

					else if(data.responseMessage == "SUCCESS" || data.responseMessage == "SIN_DEUDA"){
						_scope.respuestaDataCustomer = true;
						_scope.deuda_tot = data.responseData.deudaCuenta;
						_scope.deuda_dec = _scope.deuda_tot.substr(_scope.deuda_tot.indexOf('.'),2).trim();

						_scope.deuda_tot = _scope.deuda_tot.substr(0,_scope.deuda_tot.indexOf('.'));
						_scope.deuda_dec = _scope.deuda_dec.length == 1 ? _scope.deuda_dec + '0' : _scope.deuda_dec;

						_scope.nom_cliente = data.responseData.nombreCustomer;
						_scope.ape_cliente = data.responseData.apePatCustomer + ' ' + data.responseData.apeMatCustomer;
						_scope.tip_doc_cliente = data.responseData.tipDocCustomer;
						_scope.num_doc_cliente = data.responseData.numDocCustomer;
						_scope.lineas_cliente = data.responseData.totLineas;
						_scope.tiene_deuda = data.responseMessage == "SIN_DEUDA" || parseInt(_scope.deuda_tot) <= parseInt(data.responseData.topeDeudaAlta) ? false : true;
						_scope.linea_desc = _scope.lineas_cliente == "1" || _scope.lineas_cliente == "0" ? "linea" : "lineas";
						//console.log('Deuda: ' + parseInt(_scope.deuda_tot)  + "  -  Tope: " + parseInt(data.responseData.topeDeudaAlta));
						_scope.cli_sin_datos = false;
						//console.log("Control de deuda Maximo: " + data.responseData.topeDeudaAlta);
		
						let fecVenc = data.responseData.fecReciboVencidoUlt.split('-');
						let fecEmi = data.responseData.fecReciboEmitidoUlt.split('-');

						_scope.fec_ult_deuda = new Date(fecVenc[0],fecVenc[1] - 1,fecVenc[2]);
						_scope.fec_ult_rec_emi = new Date(fecEmi[0],fecEmi[1] - 1,fecEmi[2]);

						if(data.responseMessage == "SUCCESS" ){
							_scope.blackListService.getDateTime().subscribe(
								data => {
									let fecArray = data.responseData.substr(0,10).split('-');
									_scope.fec_actual = new Date(fecArray[0],fecArray[1] - 1, fecArray[2]);
									let diff = _scope.fec_actual.getTime() - _scope.fec_ult_deuda.getTime();
									var day = 1000 * 60 * 60 * 24;
									let days = Math.floor(diff/day);
                                    let months = Math.floor(days/31);
                                    
									_scope.tot_meses = months;
                                    _scope.mes_desc = _scope.tot_meses < 1 ? "mes" : "meses";
                                    
                                    if(days < 31) {
                                        _scope.tiempo_tot = days + " " + (days < 2 ? "dia vencido" : "dias vencidos");

                                    }
                                    else{
                                        _scope.tiempo_tot = months + " " + (months <= 1 ? "mes vencido" : "meses vencidos");
                                    }
								}
							)
						}
						
					}
					else if(data.responseMessage == "NOT_FOUND"){
						_scope.respuestaDataCustomer = true;
						_scope.tiene_deuda = false;
						_scope.cli_sin_datos = true;
					}
					
					else if(data.responseMessage == "SERVICE_INACTIVED"){
						_scope.respuestaDataCustomer = true;
						_scope.serv_deuda_act = false;
						_scope.tiene_deuda = false;
					}

					_scope.evaluar();

				},
				err => {
					_scope.respuestaDataCustomer = true;
					_scope.tiene_deuda = false;
					_scope.serv_deuda_act = false;
					_scope.loadErrorDeuda = true;
					_scope.loading = false;
					_scope.error_respuesta = true;
					_scope.mensaje_error_deuda = "Servicios no disponibles en este momento.";
					//console.log('Problemas de conexion a los Servicios de back');
				}

			);
	}
	}

	consultaRuc(){
		this.ruc_SinData = false;
		this.loading = true;
		let _scope = this;
		this.order.offline.flag = '';
		this.customerService.getdataCustomerRuc(this.customer.documentNumber)
            .subscribe(
                data => {
					//console.log('Data_Response_RUC' + JSON.stringify(data));
					if(data.responseCode == '-2' && data.responseMessage.indexOf('FLUJO_OFFLINE') !== -1){
						let msj : string[] = data.responseMessage.split('|');
						_scope.order.offline.flag = _scope.order.offline.flag == '' ? '1' : _scope.order.offline.flag + ',1';
						_scope.ls.setData(_scope.order);
						console.log(JSON.stringify(_scope.order.offline));
						_scope.respuestaDataCustomer = true;
						_scope.tiene_deuda = false;
						_scope.serv_deuda_act = false;
						_scope.loadErrorDeuda = true;
						_scope.loading = false;
						_scope.loadError = false;
						_scope.mensaje_error_deuda = msj[1];
						return;
					}
                    else if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
                        // add data perfil tienda
                        _scope.objRuc = data.responseData;
						//console.log("objeto Ruc " + JSON.stringify(_scope.objRuc))
						if(_scope.objRuc.estContrib !== 'ACTIVO'){
							_scope.mostrarError('El estado del contribuyente no es activo.');
							_scope.loading = false;
							_scope.ruc_SinData = true;
						}
						else if(_scope.objRuc.condDomicilio !== 'HABIDO'){
							_scope.mostrarError('La condición del domicilio del contribuyente no es valida.');
							_scope.loading = false;
							_scope.ruc_SinData = true;
						}
						else{
							_scope.customer.razonSocial = _scope.objRuc.razonSocial;
							_scope.customer.estContrib = _scope.objRuc.estContrib;
							_scope.customer.condDomicilio = _scope.objRuc.condDomicilio;
							_scope.customer.direccion = _scope.objRuc.direccion;
							_scope.customer.firstName = _scope.objRuc.razonSocial;
							_scope.order.customer = _scope.customer;
							_scope.evaluarDeuda();
						}

                    } else {
						_scope.mostrarError('El RUC ingresado no existe o no está activo.');
						_scope.loading = false;
						_scope.ruc_SinData = true;
                    }
                },
                err => {
					console.log(err);
					_scope.loading = false;
					_scope.mostrarError('Ocurrio un Error al Consultar al contribuyente');
					_scope.ruc_SinData = true;
                },
                () => {

                    /*if (_scope.objRuc != null) {
                        _scope.loadError = false;
                    } else {
						//console.log(JSON.stringify(_scope.objRuc));
						_scope.mostrarError('El RUC ingresado no existe o no está activo.');
						_scope.loading = false;
						_scope.ruc_SinData = true;
					}*/
                });

	}


	// Sprint 8
	errorScoringData(err) {
		this.loading = false;
		this.loadError = true;
		this.mensaje_error = globals.ERROR_MESSAGE;

		//mensajeValidacion
		$('#myModalReniec').modal('show');
		//this.mensaje_error = "El servicio customer dio un error desconocido, Por favor vuelva a intentarlo";
	}

	// Sprint 8
	cargarScoringData(data) {
		this.deudaClienteMensajeEstado = false
		this.respuestaScoring = false;
		this.deudaClienteMensaje = ""

		if (data.responseCode == -1) {
			this.mostrarError(data.responseMessage);
			return;
		}

		if (data.responseCode == 1 && data.responseData == undefined) {
			this.mostrarRegistroPlanta(data.responseMessage);
		
			return;
		}

		let selectDepartment: any;
		let selectCity: any;
		let selectDistrict: any;

		selectDepartment = document.getElementById("department");
		selectCity = document.getElementById("city");
		selectDistrict = document.getElementById("district");

		var departmentValue = selectDepartment.value;
		var cityValue = selectCity.value;
		var districtValue = selectDistrict.value;

		this.respuestaScoring = true;
		this.scoringDataModel = data;

		// alert(JSON.stringify(data.responseData.result));

		let cod_interconect = JSON.stringify(data.responseData.codConsulta);

		let renta = JSON.stringify(data.responseData.result[0].renta);


		this.cod_int = cod_interconect.slice(1, -1);
		this.renta_Inter_Conect = "S/" + renta.slice(1, -1);


		// document.getElementById('cod_interconect').innerHTML = ''+cod_interconect.slice(1, -1) ;
		// document.getElementById('interconect_renta').innerHTML = "$"+renta.slice(1, -1) ;

		this.order.scoringDataModel = data;
		this.order.id = data.responseData.orderId
		this.order.customer = this.customer; //Sprint 24 RUC
		//console.log('Ultima Carga de data order customer' + JSON.stringify(this.order.customer));
		this.ls.setData(this.order);
		this.ls.setUbicacionDefault(departmentValue, cityValue, districtValue);


		this.ls.setCodInterconectDefault(cod_interconect, renta);

		// alert("cod_interconect" +cod_interconect)

		this.loadError = false;
		this.loading = false;
	}

	// Sprint 8
	mostrarError(mensaje) {
		this.loading = false;
		this.loadError = true;
		this.mensaje_error = mensaje;
		this.respuestaScoring = false;
	}

	mostrarRegistroPlanta(mensaje) {
		this.loading = false;
		this.isAltaPura = false;		
		this.deudaClienteMensajeEstado = true
		this.deudaClienteMensaje = mensaje
		//this.loadError = true;
		//this.mensaje_error = mensaje;
		//this.respuestaScoring = false;
		// alert(mensaje)
		
	}

	cancelarConsulta() {
		this.loadError = false;
		this.mensaje_error = "";
		this.respuestaScoring = false;
	}

	cancelarConsultaDeuda() {
		this.loadErrorDeuda = false;
		this.mensaje_error_deuda = "";
		this.loading = true;
		this.evaluar();
	}

	limpiarDatosDeuda(){
		this.respuestaDataCustomer = false;
		this.tiene_deuda = false;
		this.cli_sin_datos = false;
		this.serv_deuda_act = true;
		this.loadErrorDeuda = false;
		this.error_respuesta = false;
	}

	salirVenta(){
		this.router.navigate(['/acciones']);
	}

	// Sprint 8
	iniciarVenta() {

		let accion = this.accionNull
		/*if(accion == ""){
			alert("No se encontrarón resultados para el documento ingresado.")
			return
		}*/
		if (this.order.type == "A") {
			/*Sprint-11 */
			//console.log("JSON " + JSON.stringify(this.scoringDataModel.responseData.result[0].detalle))
			var arrayDecosList = [];
			let desProdList = ['Trío', 'Dúo BA', 'Dúo TV', 'Mono BA', 'Mono TV', 'Mono Línea']
			for (let i = 0; i < 6; i++) {
				////console.log("JSON "+JSON.stringify(this.scoringDataModel.responseData.result[0].detalle[i].nroDecosAdic))
				//arrayDecosList.push(this.scoringDataModel.responseData.result[0].detalle[i].nroDecosAdic);
				if (this.scoringDataModel.responseData.result[0].detalle[i] != undefined) {
					let ndr = this.scoringDataModel.responseData.result[0].detalle[i].nroDecosAdic;
					let descpProduct = this.scoringDataModel.responseData.result[0].detalle[i].desProd;
					for (let index = 0; index < desProdList.length; index++) {
						descpProduct === desProdList[index] && ndr != null ? arrayDecosList[index] = ndr : null;
					}
					}
				}
			console.log("arrayDecos " + arrayDecosList);
			for (let index = 0; index < 6; index++) {
				arrayDecosList[index] === undefined ? arrayDecosList[index] = 0 : null;
			}
			//console.log("arrayDecos " + arrayDecosList);

			localStorage.setItem('arrayDecosList', JSON.stringify(arrayDecosList));
			/*End Sprint-11 */

			this.selectDistric = document.getElementById("district")
			var distritoLabel = this.selectDistric[this.selectDistric.selectedIndex].innerHTML


			var districtValidar = distritoLabel
			var districtValidarLocal = localStorage.getItem("valid-crediti")

			if(districtValidar != districtValidarLocal){
				alert("Necesitas volver a evaluar la nueva dirección");
				return
			}


		}


		//console.log("editando cliente " + JSON.stringify(this.ls.getData().editandoCliente))
		if (this.ls.getData().editandoCliente) {
			//this.mensajeValidacionDatosCliente = "Celular";
			//$('#modalCliente').modal('show');
			return;
		}
		//this.router.navigate( ['saleProcess/direccion'] );//sprint2 ahora se debe cargar el scoring como 3ra pantalla
		
		let resultDoc = this.order.customer.documentNumber
		var valorCondicional = resultDoc.substring(0, 2)
		let validacion = false
		if ("10" == valorCondicional) {
			validacion = false
		}else{ 
			validacion = true
			
		}
		
		if(this.order.user.typeFlujoTipificacion=='REMOTO'){
			if((this.order.customer.documentType == 'RUC' && "10" == valorCondicional) || this.order.customer.documentType == 'DNI'){
				this.router.navigate(['saleProcess/reniec']);
				
			}else if(this.order.customer.documentType == 'CEX'){
				this.router.navigate(['direccion']);
			}	
			else if(this.order.customer.documentType == 'PAS'){
				this.router.navigate(['direccion']);
			}	
			else if(this.order.customer.documentType == 'Otros'){
				this.router.navigate(['direccion']);
			}		 
		 	else if(this.order.customer.documentType == 'RUC' && "10" !== valorCondicional) {
				this.router.navigate(['/saleProcess/searchRuc']);
			}
		 else {
				this.router.navigate(['/documenttype']); 
			}
		}else{
			   this.router.navigate(['direccion']);
			
		}
	 
		
		////console.log(" flores ")
	}

	doEnter() {
		if (this.order.type != 'A') {
			this.onSubmit();
		}
	}

	sortByProperty = (property) => {
		return (x, y) => {
			return ((x[property] === y[property]) ? 0 : ((x[property] > y[property]) ? 1 : -1));
		}
	}

	sortByTwoProperty = () => {
		return (x, y) => {
			return ((x["value"]["name"] === y["value"]["name"]) ? 0 : ((x["value"]["name"] > y["value"]["name"]) ? 1 : -1));
		}
	}

	onKeyTab(event) {
		alert("event!!!! " + event);
		$("#buttonEvaluar").focus();
		//this.buttonEvaluarRef.nativeElement.focus();
		alert("#buttonEvaluar::: " + $("#buttonEvaluar"));
	}
}