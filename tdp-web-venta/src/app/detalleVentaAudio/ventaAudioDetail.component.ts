import { Component, OnInit, ViewChild,ElementRef } from '@angular/core';
import { Location } from '@angular/common';
import { SalesReportService } from '../services/salesReport.service';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../services/order.service';

@Component({
	moduleId: 'ventaAudioDetail',
	selector: 'ventaAudioDetail',
	templateUrl: './ventaAudioDetail.template.html',
	providers: [SalesReportService, OrderService]
})

export class VentaAudioDetailComponent implements OnInit{
	@ViewChild('fileInput') inputEl: ElementRef;
	public data;
    public filterQuery = "";
    public rowsOnPage = 10;
    public sortBy = "email";
    public sortOrder = "asc";
	private loading: boolean;
	private audioCargado : boolean;
	private uploadMaximumSize = 94371840;
	
	salesReportDetail=[];

    idOrder: string;
	constructor(private salesReportService: SalesReportService, private location :Location, private activeRoute : ActivatedRoute,
			private orderService: OrderService){
		this.salesReportService =  salesReportService;
        this.idOrder = "";
        this.cargarParametros(this.activeRoute);

	}

    cargarParametros(activeRoute){
        activeRoute.queryParams.subscribe(params => {

      this.idOrder = params['id'];
     // console.log("orderrr "+ this.idOrder);
    });
        }

	ngOnInit() {
		this.audioCargado = false;
		if (this.idOrder != '0') {
			this.loading = true;
			this.salesReportService.getData(this.idOrder)//"-Ka2uHX_ZFuk62SqEAaa"
				.subscribe(
					data => {
						if(typeof data.responseData !== 'undefined' && data.responseData !== null){
							this.salesReportDetail = data.responseData
							localStorage.setItem('salesReportDetail', JSON.stringify(Object.assign(this.salesReportDetail)))
							//console.log(data.responseData);
							this.loading = false;

						}else{
							alert(data.responseMessage);
							this.loading = false;
						}
					},
					err => {
					//	console.log(err)
						this.loading = false;
					}
					);
		}
	}
	
	subirAudio() {
		if (this.idOrder != '0') {
			this.subirAudioIndividual();
		} else {
			this.subirAudioMasivo();
		}
	}
	
	subirAudioIndividual() {
		let audioElement = this.inputEl.nativeElement;
		var fileNameOrderId = this.idOrder + ".gsm";
		if (audioElement.files.length < 1) {
			alert('seleccione el audio a cargar...');
			return;
		}
		let file = audioElement.files[0];
		let fileName = audioElement.value;
		if (!fileName.toLowerCase().endsWith('.gsm')) {
			this.data = [
				{
					"numero":"1",
					"nombreArchivo":file.name,
					"mensaje":"Archivo seleccionado debe ser del formato GSM.",
					"responseCode":1
				}
			];
			return;
		} else if (file.size == 0) {
			this.data = [
				{
					"numero":"1",
					"nombreArchivo":file.name,
					"mensaje":"Tamaño del archivo debe ser mayor a 0.",
					"responseCode":1
				}
			];
			return;
		} else if (fileNameOrderId != file.name) {
			this.data = [
				{
					"numero":"1",
					"nombreArchivo":file.name,
					"mensaje":"Nombre del archivo no coincide con el id de la venta",
					"responseCode":1
				}
			];
			return;
		}
		
		this.loading = true;
		this.orderService.uploadAudio(this.idOrder, file)
		.then(
			response => {
				var responseMessage = response["responseMessage"];
				var responseCode = response["responseCode"];
				var satisfactorio = true;
				this.data = [
					{
						"numero":"1",
						"nombreArchivo":file.name,
						"mensaje":responseMessage,
						"responseCode":responseCode
					}
				];
				this.loading = false;
				this.audioCargado = true;
			}
		);
	}
	
	subirAudioMasivo() {
		let audioElement = this.inputEl.nativeElement;
		let file = audioElement.files[0];
		//this.orderService.uploadAudioMasivo(audioElement.files);
		this.data = [];
		var total = 0;
		
		if (audioElement.files.length == 0) {
			var mensajeCantidadArchivos = audioElement.files.length + " archivos";
			this.data = [
				{
					"numero":"1",
					"nombreArchivo": "No audio seleccionado",
					"mensaje":"Por favor seleccione al menos un archivo de audio en formato GSM.",
					"responseCode":1,
					"satisfactorio":false
				}
			];
			return
		}
		
		for (var i = 0; i < audioElement.files.length; i+=1) {
			total += audioElement.files[i].size;
		}
		
		if (total > this.uploadMaximumSize) {
			var mensajeCantidadArchivos = audioElement.files.length + " archivos";
			this.data = [
				{
					"numero":"1",
					"nombreArchivo": mensajeCantidadArchivos,
					"mensaje":"El tamaño de los archivos seleccionados en total superan los 90MB permitidos. Por favor seleccione menos archivos.",
					"responseCode":1,
					"satisfactorio":false
				}
			];
			return;
		}
		
		this.loading = true;
		this.orderService.uploadAudioMasivo(audioElement.files)
		.then(
			response => {
				this.data = response["responseData"];
				this.loading = false;
				this.inputEl.nativeElement.value = "";
			}
		);
	}

	goBack () {
		this.location.back();
	}
	
	public toInt(num: string) {
        return +num;
    }

    public sortByWordLength = (a: any) => {
        return a.city.length;
    }
}
