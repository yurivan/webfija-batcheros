import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from '../services/ls.service';


import * as globals from '../services/globals';
import { global } from '@angular/core/src/facade/lang';

import { ProgressBarService } from '../services/progressbar.service';
import { OfertaBi } from '../model/ofertaBi';
import { Order } from '../model/order';
import { ProductOffering } from '../model/productOffering';

//Sprint 24
import { BlackListService } from '../services/blacklist.service';
import { CustomerService } from '../services/customer.service';

var $ = require('jquery'); //utilizar funciones de Jquery
@Component({
	moduleId: 'ofertasBiOut',
	selector: 'ofertasBiOut',
	templateUrl: './ofertasBiOut.template.html',
	providers: [CustomerService]
})

export class OfertasBiOutComponent implements OnInit {

	order: Order;
	ofertasBi : OfertaBi[];
	ofertasBiDetalle: OfertaBi;
	isTrue: boolean = true;
	isFlagDetalleProduc: boolean =false;
	tiposLineas:String;
	tiposLineas1:String;
	tipoVelocidad:String;
	style: boolean = false;

	//Sprint 24
	respuestaDataCustomer :boolean;
	deuda_tot : string;
	deuda_dec : string;
	nom_cliente : string;
	ape_cliente : string;
	lineas_cliente : string;
	tip_doc_cliente : string;
	num_doc_cliente : string;
	fec_ult_deuda : Date;
	fec_ult_rec_emi : Date;
	linea_desc : string;
	mes_desc : string;
	tiene_deuda : boolean;
	tot_meses : number;
	fec_actual : Date;
	cli_sin_datos : boolean = false;
	serv_deuda_act : boolean = true;
	loading = false;
	tiempo_tot : string;
	loadErrorDeuda : boolean = false;
	mensaje_error_deuda : string;
	strmsgerror : string[];
	error_respuesta : boolean = false;

	constructor(private router: Router,
		private ls: LocalStorageService,
		private progresBar: ProgressBarService, 
		private blackListService: BlackListService,
		private customerService: CustomerService) {
		this.router = router;
		this.ls = ls;
		//Sprint 24
		this.blackListService = blackListService;
		this.customerService = customerService;
	}

	ngOnInit() {
		$('ul > #reportes').removeClass('none');
		$('ul > #Venderr').addClass('none');
		//})
		this.progresBar.getProgressStatus()
		$(".progres").prop("style", "visibility: hidden !important;");
		
		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}

		this.order=this.ls.getData();
		this.ofertasBi = JSON.parse(localStorage.getItem("ofertasBi"));
		this.cargartiposLineas();
		if(this.order.customer.documentType !== 'RUC'){
		this.evaluarDeuda();
		}
		else{
			this.respuestaDataCustomer = true;
			this.serv_deuda_act = false;
			this.tiene_deuda = false;
		}

	}

	regresar(){
		localStorage.removeItem("ofertasBi0")
		this.router.navigate(['inicioOut']);
	}

	verProductos(){
		this.order.ofertasBi=false;
		this.ls.setData(this.order);
		this.router.navigate(['scoringOut']);
	}

	getDetailProduct(ofertBi:OfertaBi){
		this.ofertasBiDetalle = null;
		this.ofertasBiDetalle = ofertBi;
		this.isFlagDetalleProduc = true;
	}

	cargartiposLineas(){
		this.tiposLineas=null;
		if(this.ofertasBi.length >= 2){
			var cont = 2;
		}else{
			var cont = this.ofertasBi.length;
			this.style = true;
		}
		
		for(var i=0; i<cont;i++){
		
			if(this.ofertasBi[i].equipamientoInternet != null && this.ofertasBi[i].equipamientoInternet.length>2){
				if(i==0){
					this.tiposLineas="Internet";
				}else{
					this.tiposLineas1="Internet";
				}
			}
			if(this.ofertasBi[i].equipamientoLinea != null && this.ofertasBi[i].equipamientoLinea.length>2){
				
				if(i==0){
					if(this.tiposLineas != null){
						this.tiposLineas=this.tiposLineas + " + Fijo";
					}else{
						this.tiposLineas="Fijo"
					}
				}else{
					if(this.tiposLineas1 != null){
						this.tiposLineas1=this.tiposLineas1 + " + Fijo";
					}else{
						this.tiposLineas1="Fijo"
					}
				}
				
			}
			if(this.ofertasBi[i].equipamientoTv != null && this.ofertasBi[i].equipamientoTv.length>2){
				
				if(i==0){
					if(this.tiposLineas != null){
						this.tiposLineas=this.tiposLineas + " + Television";
					}else{
						this.tiposLineas="Television";
					}
				}else{
					if(this.tiposLineas1 != null){
						this.tiposLineas1=this.tiposLineas1 + " + Television";
					}else{
						this.tiposLineas1="Television";
					}
				}
			}
		}
		
	}

	doEnter(){
		localStorage.setItem('ofertasBiDetalle', JSON.stringify(this.ofertasBiDetalle));
		this.order.ofertasBi = true;
		this.order.selectedOffering= new ProductOffering();
		this.order.selectedOffering.id=this.ofertasBiDetalle.id;
		this.order.selectedOffering.commercialOperation=this.ofertasBiDetalle.commercialOperation;
		this.order.selectedOffering.campaign=this.ofertasBiDetalle.campaign;
		this.order.campaniaSeleccionada=this.ofertasBiDetalle.campaign;
		this.order.selectedOffering.productTypeCode=this.ofertasBiDetalle.productTypeCode;
		this.order.selectedOffering.productType=this.ofertasBiDetalle.productType;
		this.order.selectedOffering.productCategoryCode=this.ofertasBiDetalle.productCategoryCode;
		this.order.selectedOffering.productCategory=this.ofertasBiDetalle.productCategory;
		this.order.selectedOffering.productCode=this.ofertasBiDetalle.productCode;
		this.order.selectedOffering.productName=this.ofertasBiDetalle.productName;
		this.order.selectedOffering.discount=this.ofertasBiDetalle.discount;
		this.order.selectedOffering.price=this.ofertasBiDetalle.price;
		this.order.selectedOffering.promPrice=this.ofertasBiDetalle.promPrice;
		this.order.selectedOffering.monthPeriod=this.ofertasBiDetalle.monthPeriod;
		this.order.selectedOffering.installCost=this.ofertasBiDetalle.installCost;
		this.order.selectedOffering.lineType=this.ofertasBiDetalle.lineType;
		this.order.selectedOffering.paymentMethod=this.ofertasBiDetalle.paymentMethod;
		this.order.selectedOffering.cashPrice=this.ofertasBiDetalle.cashPrice;
		this.order.selectedOffering.financingCost=this.ofertasBiDetalle.financingCost;
		this.order.selectedOffering.financingMonth=this.ofertasBiDetalle.financingMonth;
		this.order.selectedOffering.equipmentType=this.ofertasBiDetalle.equipmentType;
		this.order.selectedOffering.returnMonth=this.ofertasBiDetalle.returnMonth;
		this.order.selectedOffering.returnPeriod=this.ofertasBiDetalle.returnPeriod;
		this.order.selectedOffering.internetSpeed=this.ofertasBiDetalle.internetSpeed;
		this.order.selectedOffering.promoInternetSpeed=this.ofertasBiDetalle.promoInternetSpeed;
		this.order.selectedOffering.periodoPromoInternetSpeed=this.ofertasBiDetalle.periodoPromoInternetSpeed;
		this.order.selectedOffering.internetTech=this.ofertasBiDetalle.internetTech;
		this.order.selectedOffering.tvSignal=this.ofertasBiDetalle.tvSignal;
		this.order.selectedOffering.tvTech=this.ofertasBiDetalle.tvTech;
		this.order.selectedOffering.equipamientoLinea=this.ofertasBiDetalle.equipamientoLinea;
		this.order.selectedOffering.equipamientoInternet=this.ofertasBiDetalle.equipamientoInternet;
		this.order.selectedOffering.equipamientoTv=this.ofertasBiDetalle.equipamientoTv;
		this.order.selectedOffering.expertCode=this.ofertasBiDetalle.expertCode;
		this.order.selectedOffering.cantidaddecos=this.ofertasBiDetalle.cantidaddecos;
		this.order.selectedOffering.sva=this.ofertasBiDetalle.sva;
		this.order.selectedOffering.altaPuraSvasBloqueTV=this.ofertasBiDetalle.altaPuraSvasBloqueTV;
		this.order.selectedOffering.altaPuraSvasLinea=this.ofertasBiDetalle.altaPuraSvasLinea;
		this.order.selectedOffering.altaPuraSvasInternet=this.ofertasBiDetalle.altaPuraSvasInternet;
		this.order.selectedOffering.msx_cbr_voi_ges_in=this.ofertasBiDetalle.msx_cbr_voi_ges_in;
		this.order.selectedOffering.msx_cbr_voi_gis_in=this.ofertasBiDetalle.msx_cbr_voi_gis_in;
		this.order.selectedOffering.msx_ind_snl_gis_cd=this.ofertasBiDetalle.msx_ind_snl_gis_cd;
		this.order.selectedOffering.msx_ind_gpo_gis_cd=this.ofertasBiDetalle.msx_ind_gpo_gis_cd;
		this.order.selectedOffering.cod_ind_sen_cms=this.ofertasBiDetalle.cod_ind_sen_cms;
		this.order.selectedOffering.cod_cab_cms=this.ofertasBiDetalle.cod_cab_cms;
		this.order.selectedOffering.cod_fac_tec_cd=this.ofertasBiDetalle.cod_fac_tec_cd;
		this.order.selectedOffering.cobre_blq_vta=this.ofertasBiDetalle.cobre_blq_vta;
		this.order.selectedOffering.cobre_blq_trm=this.ofertasBiDetalle.cobre_blq_trm;
		
		this.ls.setData(this.order);
		this.router.navigate(['direccionOut']);
	}

	cerrarModal() {
		this.isFlagDetalleProduc = false;
	}

	ngAfterViewInit(){
		if(this.style){
			$("#1produc").prop("style", "margin-left: 40%;");
		}
	}

	evaluarDeuda(){
		let _scope = this;
		this.loading = true;
		this.order.offline.flag = '';
			this.customerService.getdataCustomer(this.order.customer.documentType, this.order.customer.documentNumber)
			.subscribe(
				data =>{
					//console.log("data deuda --> " + JSON.stringify(data))
					_scope.respuestaDataCustomer = false;
					if (data.responseCode == -1) {
						_scope.respuestaDataCustomer = true;
						_scope.tiene_deuda = false;
						_scope.serv_deuda_act = false;
						_scope.loadErrorDeuda = true;
						_scope.loading = false;
						_scope.strmsgerror = data.responseMessage.split('|');
						_scope.mensaje_error_deuda = _scope.strmsgerror[0];
						_scope.order.offline.flag += _scope.order.offline.flag == '' ? '2' : ',2';
						_scope.ls.setData(_scope.order);
						//console.log('Servicio Consulta deuda - Error Interno --> Detalle: ' + _scope.strmsgerror[1]);
						return;
					}

					if(data.responseMessage == "SUCCESS" || data.responseMessage == "SIN_DEUDA"){
						_scope.respuestaDataCustomer = true;
						_scope.deuda_tot = data.responseData.deudaCuenta;
						_scope.deuda_dec = _scope.deuda_tot.substr(_scope.deuda_tot.indexOf('.'),2).trim();

						_scope.deuda_tot = _scope.deuda_tot.substr(0,_scope.deuda_tot.indexOf('.'));
						_scope.deuda_dec = _scope.deuda_dec.length == 1 ? _scope.deuda_dec + '0' : _scope.deuda_dec;

						_scope.nom_cliente = data.responseData.nombreCustomer;
						_scope.ape_cliente = data.responseData.apePatCustomer + ' ' + data.responseData.apeMatCustomer;
						_scope.tip_doc_cliente = data.responseData.tipDocCustomer;
						_scope.num_doc_cliente = data.responseData.numDocCustomer;
						_scope.lineas_cliente = data.responseData.totLineas;
						_scope.tiene_deuda = data.responseMessage == "SIN_DEUDA" || parseInt(_scope.deuda_tot) <= parseInt(data.responseData.topeDeudaAlta) ? false : true;
						_scope.linea_desc = _scope.lineas_cliente == "1" || _scope.lineas_cliente == "0" ? "linea" : "lineas";
						_scope.cli_sin_datos = false;
						//console.log("Control de deuda Maximo:" + data.responseData.topeDeudaAlta);

						let fecVenc = data.responseData.fecReciboVencidoUlt.split('-');
						let fecEmi = data.responseData.fecReciboEmitidoUlt.split('-');
						
						_scope.fec_ult_deuda = new Date(fecVenc[0],fecVenc[1] - 1,fecVenc[2]);
						_scope.fec_ult_rec_emi = new Date(fecEmi[0],fecEmi[1] - 1,fecEmi[2]);

						if(data.responseMessage == "SUCCESS" ){
							_scope.blackListService.getDateTime().subscribe(
								data => {
									let fecArray = data.responseData.substr(0,10).split('-');
									_scope.fec_actual = new Date(fecArray[0],fecArray[1] - 1, fecArray[2]);
									let diff = _scope.fec_actual.getTime() - _scope.fec_ult_deuda.getTime();
									var day = 1000 * 60 * 60 * 24;
									let days = Math.floor(diff/day);
                                    let months = Math.floor(days/31);
                                    
									_scope.tot_meses = months;
                                    _scope.mes_desc = _scope.tot_meses < 1 ? "mes" : "meses";
                                    
                                    if(days < 31) {
                                        _scope.tiempo_tot = days + " " + (days < 2 ? "dia vencido" : "dias vencidos");

                                    }
                                    else{
                                        _scope.tiempo_tot = months + " " + (months <= 1 ? "mes vencido" : "meses vencidos");
                                    }
								}
							)
						}
						
					}
					else if(data.responseMessage == "NOT_FOUND"){
						_scope.respuestaDataCustomer = true;
						_scope.tiene_deuda = false;
						_scope.cli_sin_datos = true;
					}
					
					else if(data.responseMessage == "SERVICE_INACTIVED"){
						_scope.respuestaDataCustomer = true;
						_scope.serv_deuda_act = false;
						_scope.tiene_deuda = false;
					}

				this.loading = false;
				},
				err => {
					_scope.respuestaDataCustomer = true;
					_scope.tiene_deuda = false;
					_scope.serv_deuda_act = false;
					_scope.loadErrorDeuda = true;
					_scope.loading = false;
					_scope.error_respuesta = true;
					_scope.mensaje_error_deuda = "Servicios no disponibles en este momento.";
					//console.log('Problemas de conexion a los Servicios de back');
				}

			);
	}

	cancelarConsultaDeuda() {
		this.loadErrorDeuda = false;
		this.mensaje_error_deuda = "";
	}
	salirVenta(){
		this.router.navigate(['/acciones']);
	}

}