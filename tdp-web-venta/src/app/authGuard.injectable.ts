import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import 'rxjs/Rx';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router){}

  canActivate (route, state) {
    let token = localStorage.getItem('token');
    let order = JSON.parse(localStorage.getItem('order'));
	// Sprint 6 - se adiciona control del resetpwd para acceder a los recursos...
    if (token && order && order.user && order.user.name /*&& '0' == order.user.resetPwd*/) {
      return true;
    }
	/*if ('1' == order.user.resetPwd) {
		this.router.navigate(['/changePassword'], { queryParams: { returnUrl: state.url } });
	} else {*/
		this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
	//}
    return false;
  }
}
