import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { CustomerService } from '../services/customer.service';
import { Customer } from '../model/customer';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';
import { HistoricoService } from '../services/historico.service';
import { Tracking } from '../model/tracking';


// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';

import { ProgressBarService } from '../services/progressbar.service';
import { OnOff } from '../model/onoff';
import { BlackListService } from '../services/blacklist.service';
import { UserService } from '../services/user.service';

var $ = require('jquery');

@Component({
	moduleId: 'principalout',
	selector: 'principalout',
	templateUrl: './principalout.template.html',
	providers: [CustomerService]
})
//--
export class principaloutcomponent implements OnInit {

	newCustomer: Customer[];

	order: Order;
	onoff: OnOff;
	modelDirectio: string = "";
	selectedType: any;
	documentNumber: any;
	isTrue = true;
	loading = false;
	previousUrl;
	errorDocumentType: boolean;
	errorDocumentNumber: boolean;


	showDni: boolean = true;
	showCarneExtranjeria: boolean = false;
	docNroDni: string;
	docNroCarnetExt: string;
	errorConexion: string;
	msgError: boolean = false;

	// Sprint 7... tipos de documento a mostrar son configurables
	showTipoDocumentoDNI: boolean = true;
	showTipoDocumentoCEX: boolean = false;
	showTipoDocumentoPAS: boolean = false;
	showTipoDocumentoRUC: boolean = false;

	showPasaporte: boolean = false;
	showRUC: boolean = false;
	blackListPhone: string[];

	mjsOnOff: string;

	showModalCancelSale: boolean = false;
	enabled: boolean = false;
	

	constructor(private router: Router,
		private customerService: CustomerService,
		private ls: LocalStorageService,
		private blackList: BlackListService,
		private progresBar: ProgressBarService,
		private userService: UserService) {

		this.router = router;
		this.customerService = customerService;
		this.ls = ls;
	}

 
	ngOnInit() {
		localStorage.setItem("capta","capta");
		// $(".overlay").style("Display","none")
		this.onoff = JSON.parse(localStorage.getItem("dataOnOff")); 
		this.order = JSON.parse(localStorage.getItem('order'));
		let _xthis = this;
		this.blackList.getDateTime().subscribe(
			data => {
				
				if(_xthis.ls.validarOnOff(_xthis.onoff,data.responseData)){
					$(".mensajeOnOff").prop("style", "visibility: hidden !important;");
					$(".cnt_forcnt").prop("style", "visibility: visible !important;");
				}else{
					_xthis.mjsOnOff = _xthis.onoff.message.replace("[NOMBRE]",_xthis.order.user.name);
					$(".mensajeOnOff").prop("style", "visibility: visible !important;");
					$(".cnt_forcnt").prop("style", "visibility: hidden !important;");			
				}
			},
			err => { }
		);

		this.userService.loginConvergencia(this.order.user.group).subscribe(
			data => {
				if(data.responseCode!=1){
					if(data.responseData.enable){
						_xthis.enabled=true
					}
				}
			},
			err => {

			}
		);
		
		localStorage.removeItem('codigoInterConect');
		localStorage.removeItem('rentaInterConect');
		if (localStorage.getItem("dataRed")) {
			localStorage.removeItem("dataRed")
		}
		//$(document).ready(() => {
		//console.log("acciones")
		$('ul > #reportes').removeClass('none');
		$('ul > #Venderr').addClass('none');
		//})
		this.progresBar.getProgressStatus()
		$(".progres").prop("style", "visibility: hidden !important;");
		this.selectedType = 'DNI';
		this.documentNumber = null;
		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}
		this.order = JSON.parse(localStorage.getItem('order'));
		this.docNroDni = '';
		//$('#documentType').focus();

		// Sprint 7... tipos de documento a mostrar son configurables
		this.showTipoDocumentoCEX = globals.SHOW_CEX;
		this.showTipoDocumentoPAS = globals.SHOW_PAS;
		this.showTipoDocumentoRUC = globals.SHOW_RUC;
		//$("#cierrate").attr("href", "/login");

		/*$("#cierrate").click(function() {
			localStorage.removeItem('token');
			localStorage.removeItem('refresh_token');
			localStorage.removeItem('user');
			localStorage.removeItem('order');
		});*/

	}

	NUEVOS_SERVICIOS_LEAVE() {
	}


	NUEVOS_SERVICIOS() {
		$(".altaNueva").removeClass("none");
		$(".migra").addClass("none");
		$(".addserv").addClass("none");
		$(".conv").addClass("none");
		// $("a>#pagina_anterior").removeClass("none");
	}

	MIGRACIONES() {
		$(".migra").removeClass("none");
		$(".altaNueva").addClass("none");
		$(".addserv").addClass("none");
		$(".conv").addClass("none");
		// $("a>#pagina_anterior").removeClass("none");
	}

	SERVICIOS_ADICIONALES() {
		$(".addserv").removeClass("none");
		$(".altaNueva").addClass("none");
		$(".migra").addClass("none");
		$(".conv").addClass("none");
		// $("a>#pagina_anterior").removeClass("none");
	}

	CONVERGENCIA() {
		$(".addserv").addClass("none");
		$(".altaNueva").addClass("none");
		$(".migra").addClass("none");
		$(".conv").removeClass("none");
		// $("a>#pagina_anterior").removeClass("none");
	}


	//function onChage
	onChangeSelectDocument(value) {
		if (value == "DNI") {
			this.docNroCarnetExt = '';
		}


		//this.docNroCarnetExt = '';
		this.docNroDni = '';

		switch (value) {

			case "DNI":

				this.showDni = true;
				this.showCarneExtranjeria = false;

				this.showPasaporte = false;
				this.showRUC = false;

				break;

			case "CEX":

				this.showCarneExtranjeria = true;
				this.showDni = false;

				this.showPasaporte = false;
				this.showRUC = false;
				break;

			case "PAS":

				this.showPasaporte = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showRUC = false;

				break;

			case "RUC":

				this.showRUC = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showPasaporte = false;
				break;

			default:
				this.showDni = true;
				this.showCarneExtranjeria = false;
				this.showPasaporte = false;
				this.showRUC = false;
				this.selectedType = null;
				break;
		}
	}

	onSubmit() {

		this.documentNumber = this.docNroDni || this.docNroCarnetExt;

		if (this.documentNumber) {
			this.msgError = false;
			this.loading = true;
			this.customerService.getData(this.selectedType, this.documentNumber).subscribe(data => {
				if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
					let order = new Order();
					order.user = this.order.user;
					//localStorage.setItem('order', JSON.stringify(order));
					let customer = data.responseData
					customer.documentType = this.selectedType;
					customer.documentNumber = this.documentNumber;
					order.customer = customer;
					//localStorage.setItem('order', JSON.stringify(order));
					this.ls.setData(order);
					this.loading = false;
					//this.router.navigate(['/saleProcess']);
					this.router.navigate(['/saleProcess/park']);
					//this.router.navigate(['saleProcess/scoring']);


				} else {
					let newCustomer;
					newCustomer = {
						documentType: this.selectedType,
						documentNumber: this.documentNumber,
						firstName: "",
						lastName1: "",
						lastName2: "",
						email: "",
						telephone: "",
						mobilePhone: "",

						nationality: ""
					}
					let order = new Order();
					order.status = "N";
					order.user = this.order.user;
					order.customer = newCustomer;
					this.ls.setData(order);
					this.loading = false;
					//this.router.navigate(['/saleProcess']);//glazaror ahora ya no se debe pedir telefono, celular, correo al inicio
					this.router.navigate(['/saleProcess/park']);//glazaror ahora se debe cargar los productos cliente directamente
				}
			},
				err => {
					//console.log('error', err);
					this.loading = false;
					this.msgError = true;
					this.errorConexion = 'Vuelve a Intentar';
				})
		}

	}
	isEmpty(o) {
		for (var i in o) { return false; }
		return true;
	}
	isNumber(o) {
		if (!isNaN(o) && !this.isEmpty(o)) { return true; }
		return false;
	}


	iniciarVenta(accion) {

		let route = ''
		// $("#Venderr").removeClass("none")

		$(".progres").prop("style", "visibility: visible !important;");

		let ooo = JSON.parse(localStorage.getItem('order'));

		let grupo = ooo.user.groupPermission;

		// reseteamos datos del scoring
		let order = new Order();
		order.user = this.order.user;
		// En este caso se trata de alta pura... para los otros botones "Migraciones" "Servicios Adicionales" setear los tipos respectivos
		switch (accion) {
			case 'alta':
				order.type = 'A';
				route = 'inicioOut'
				break
			case 'migra':
				if (localStorage.getItem("productsCampaniaSeleccionada")) {
					localStorage.removeItem("productsCampaniaSeleccionada");
				}
				this.setCampaniaDefault();
				order.type = 'M';
				route = 'migraoutevaluar'
				break
			//Debe haber un caso más por agregar
			case 'sva':
				if (localStorage.getItem("productsCampaniaSeleccionada")) {
					localStorage.removeItem("productsCampaniaSeleccionada");
				}
				this.setCampaniaDefault();
				order.type = 'S';
				route = 'migraoutevaluar'
				break
			default:
				break
		}
		this.ls.setData(order);

		this.navigateToward(route)

	}
	navigateToward(route: string) {
		if (route != '') {
			this.router.navigate([route]);
		}
	}

	setCampaniaDefault() {
		localStorage.setItem("select-campana", 'Masiva');
	}

	GET_TOKEN(){
		this.loading = true;
		var _scope = this;
		this.userService.getToken().subscribe(
			data => {
				_scope.loading = false;
				var win = window.open('https://convergencia-web-prd.mybluemix.net/loginSales?token='+data.responseData.token, '_blank');
				if (win) {
					win.focus();
				} else {
					alert('Por favor habilitar ventanas emergentes');
				}
			},
			err => {
				_scope.loading = false;
			});
	}	
}
