import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { ParqueService } from '../services/parque.service';

import { Producto } from '../model/producto';
import { Customer } from '../model/customer';
import { Order } from '../model/order';
import * as globals from '../services/globals';
import { LocalStorageService } from '../services/ls.service';
import { CustomerService } from '../services/customer.service';
import { ProgressBarService } from '../services/progressbar.service';
import { Argumentarios } from '../model/argumentarios';

//Sprint 24
import { BlackListService } from '../services/blacklist.service';
var $ = require('jquery');

@Component({
    moduleId: 'park',
    selector: 'park',
    templateUrl: './park.template.html',
    providers: [ParqueService]
})

export class ParkComponent implements OnInit {
    currentProductIndex: -1;
    product: Producto[];
    imagenes1: String[];
    img_upgrades: String[];
    showModalPark: boolean[] = [false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false];
    private loading: boolean;
    private loadError: boolean;
    private visible_accordion;
    private customer: Customer;
    private documentType: string;
    private is_product_empty: boolean;
    private mensaje_error: string;
    private order: Order;
    private color: string;
    private selectedProduct: any[];
    private fusionButton: boolean;
    private prodSelected: number
    private counterElementSelected: string[]
    private elementSelected: boolean
    private amountOfRows: string[]
    private amountOfRowsLinea: string[]
    private showAlert: boolean;
    private showImgValidate: boolean;
    answerValidationOffer = {
        responseCode: '',
        responseMessage: '',
        responseData: ''
    }
    private nameDistrict: string
    private nameProvince: string
    private nameDepart: string
    isThereAnyLineaOrBAProduct: boolean
    isThereAnyTvProduct: boolean

    argumentarios : Argumentarios[];
    mjsPark :string;

    mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios

    //@Output() sendDataProduct = new EventEmitter();

    //Sprint 24
	respuestaDataCustomer :boolean = false;
	deuda_tot : string;
	deuda_dec : string;
	nom_cliente : string;
	ape_cliente : string;
	lineas_cliente : string;
	tip_doc_cliente : string;
	num_doc_cliente : string;
	fec_ult_deuda : Date;
	fec_ult_rec_emi : Date;
	linea_desc : string;
	mes_desc : string;
	tiene_deuda : boolean = false;
	tot_meses : number;
	fec_actual : Date;
	cli_sin_datos :boolean = false;
    serv_deuda_act : boolean = false;
    tiempo_tot : string;
    loadErrorDeuda : boolean = false;
	mensaje_error_deuda : string;
    strmsgerror : string[];
    error_respuesta : boolean = false;
    
    constructor(private parqueService: ParqueService,
        private router: Router,
        private ls: LocalStorageService,
        private customerService: CustomerService,
        private progresBar: ProgressBarService, 
		private blackListService: BlackListService) {
        this.parqueService = parqueService;
        this.router = router;
        this.ls = ls;
        this.loading = true;
        this.imagenes1 = [];
        this.img_upgrades = [];
        this.visible_accordion = false;
        this.customer = null;
        this.documentType = "";
        this.loadError = false;
        this.is_product_empty = false;
        this.mensaje_error = "";
        this.color = "";
        this.fusionButton = true;
        this.prodSelected = -1;
        this.mensajeValidacionDatosCliente = "";//Sprint 6
        this.counterElementSelected = []
        this.elementSelected = false
        this.amountOfRows = []
        this.amountOfRowsLinea = []
        this.showAlert = false
        this.showImgValidate = false
        this.customerService = customerService;
        this.isThereAnyLineaOrBAProduct = false
        this.isThereAnyTvProduct = false
        //Sprint 24
		this.blackListService = blackListService;
    }


    cambiarIcono(index) {
        if (this.currentProductIndex == index) {
            this.currentProductIndex = -1;
        } else {
            this.currentProductIndex = index;
        }
    }

    ngOnInit() {


        this.progresBar.getProgressStatus()
        //row clickable
        /*$(".clickable-row").click(function () {
            window.location = $(this).data("href");
        });*/
        $(document).ready(function () {

            $('.inputfile').each(function () {
                var $input = $(this),
                    $label = $input.next('label'),
                    labelVal = $label.html();

                $input.on('change', function (e) {
                    var fileName = '';

                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    else if (e.target.value)
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        $label.find('span').html(fileName);
                    else
                        $label.html(labelVal);
                });

                // Firefox bug fix
                $input
                    .on('focus', function () { $input.addClass('has-focus'); })
                    .on('blur', function () { $input.removeClass('has-focus'); });
            });

            let visibility = (index) => {
                for (let i = 5; i <= 6; i++) {
                    if (i == index) {
                        $("#tab" + i + "").show()
                    } else {
                        $("#tab" + i + "").hide()
                    }
                }
            }

            let elementChoosen = (id) => {
                for (let i = 5; i <= 6; i++) {
                    if (i == id) {
                        $("#a" + i + "").parent().addClass("active")
                        if (i == 5) {
                            $("#a" + i + "").parent().css({ "color": "#50535a" });
                        }
                        if (i == 6) {
                            $("#a" + i + "").parent().css({ "color": "#5bc500" });
                        }
                    } else {
                        $("#a" + i + "").parent().removeClass("active")
                        $("#a" + i + "").parent().css({ "color": "#50535a" });
                    }
                }
            }

            $("#a5").click(() => {
                visibility(5)
                elementChoosen(5)
            })
            $("#a6").click(() => {
                visibility(6)
                elementChoosen(6)
            })
            $("#tab6").hide()
        })


        this.argumentarios = JSON.parse(localStorage.getItem('argumentarios'));

        for (let i in this.argumentarios) {
            let obj = new Argumentarios;
            obj = this.argumentarios[i];
            if(obj.pantalla.toUpperCase() == "park".toUpperCase()){
                this.mjsPark = obj.mensaje;
            }
        }

        this.order = JSON.parse(localStorage.getItem('order'));
        this.order.selectedOffering = null;
        if (this.order.type == null) {
            //por default es migra
            this.order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
        }
        this.ls.setData(this.order);
        this.customer = this.order.customer;
        if (!this.customer) {
            this.loading = false;
            this.loadError = true;
            this.mensaje_error = "La operacion no envio los datos correspondiente del cliente";
        } else {
            this.parqueService.getData(this.customer, this.order.id)
                .subscribe(
                data => {
                    //console.log("data parque " + JSON.stringify(data))
                    this.cargarServicio(data)
                        if(data.responseData != null && data.responseData != "" && this.order.type == "M"){
                            this.evaluarDeuda(this);
                        }
                        else{
                            this.respuestaDataCustomer = true;
                            this.tiene_deuda = false;
                            this.serv_deuda_act = false;
                        }
                },
                err => this.errorCargaParque(err)
                )//this.cargarImagenes(this.product));
        }

        this.order.status = "P";
        this.ls.setData(this.order);
    }

    errorCargaParque(err) {
        this.loading = false;
        this.loadError = true;
        this.mensaje_error = globals.ERROR_MESSAGE;
        //this.mensaje_error = "El servicio customer dio un error desconocido, Por favor vuelva a intentarlo";
    }

    cargarServicio(data) {
        if (data.responseCode == '0') {
            this.product = data.responseData
            for (let i = 0; i < this.product.length; i++) {
                this.showProduct(this.product[i])
                /*if (Number(this.product[i].sourceType) == 1 || Number(this.product[i].sourceType) == 2 || Number(this.product[i].sourceType) == 3 || Number(this.product[i].sourceType) == 4) {
                    this.amountOfRowsLinea.push("There's an element")
                } else if (Number(this.product[i].sourceType) == 5 || Number(this.product[i].sourceType) == 6) {
                    this.amountOfRows.push("There's an element")
                }*/
                if (this.product[i].parkType == 'ATIS') {
                    this.amountOfRowsLinea.push("There's an element")
                } else if (this.product[i].parkType == 'CMS') {
                    this.amountOfRows.push("There's an element")
                }
            }

            if (this.product.length > 0) {
                this.visible_accordion = true;
            }
            else {
                this.is_product_empty = true;
                this.visible_accordion = false;
            }
            this.selectedProduct = this.product;
            for (let i = 0; i < this.selectedProduct.length; i++) {
                this.selectedProduct[i].selected = false;
            }
            this.loadError = false;
            //this.loading = false;
        } else {
            //this.loading = false;
            this.loadError = true;
            if (data.responseMessage)
                this.mensaje_error = data.responseMessage;
            else
                this.mensaje_error = globals.ERROR_MESSAGE;
            //this.mensaje_error = "El servicio customer dio un error desconocido, Por favor vuelva a intentarlo" ;
        }
        this.loading = false;
    }
    getData() {
        // go back getData()
        //this.loadError = false;
        this.loading = true;
        //console.log("this.customer " + this.customer)
        this.parqueService.getData(this.customer, this.order.id)
            .subscribe(
            data => this.cargarServicio(data),
            err => this.errorCargaParque(err)
            );
    }

    /**
     * Accordion no se usará
     */
    /*cargarImagenes(pro: Producto[]) {
        this.loading = false;
        if (pro) {
            //if ( pro.length <= 0 ) {
            if (pro.length <= 0) {
                this.is_product_empty = true;
                this.visible_accordion = false;
            } else {
                this.visible_accordion = true;
                for (let i = 0; i < pro.length; i++) {
                    this.imagenes1[i] = "abrir_accordion.png";
                    this.img_upgrades[i] = "upgrade2.png";
                }
            }
        } else {
            this.loadError = true;
            this.mensaje_error = "Vuelva a intentarlo.";
        }
        return "";
    }*/

    // Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"
    cerrarDialogo(nombreModal) {
        $("#" + nombreModal).modal('hide');
    }

    agregarProducto() {
        // Sprint 4 - Si es que los datos del cliente estan en modo edicion y estan incorrectos entonces no dejamos avanzar el flujo
        if (this.ls.getData().editandoCliente) {
            this.mensajeValidacionDatosCliente = "Celular";
            $('#modalCliente').modal('show');
            return;
        }
        let order = new Order();
        this.order.type = globals.ORDER_ALTA_NUEVA;
        order.type = this.order.type;
        order.status = "P";
        order.user = this.order.user;
        order.customer = this.ls.getData().customer;
        //localStorage.setItem('order', JSON.stringify(order));
        this.ls.setData(order);
        //this.router.navigate( ['saleProcess/direccion'] );//sprint2 ahora se debe cargar el scoring como 3ra pantalla
        this.router.navigate(['saleProcess/scoring']);//sprint2 ahora se debe cargar el scoring como 3ra pantalla
    }

    /*send(prod) {
        prod = this.product[this.prodSelected]
        // Sprint 4 - Si es que los datos del cliente estan en modo edicion y estan incorrectos entonces no dejamos avanzar el flujo
        //var customer = this.ls.getData().customer;
        if (this.ls.getData().editandoCliente) {
            this.mensajeValidacionDatosCliente = "Celular";
            $('#modalCliente').modal('show');
            return;
        }

        this.order.product = prod;
        this.order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
        this.order.status = "P";
        //localStorage.setItem('order', JSON.stringify(this.order));
        this.ls.setData(this.order);
        ////console.log( this.order.status );
        //this.router.navigate( ['saleProcess/offering'] );
        this.router.navigate(['offering']);
    }*/

    sendMigra() {
        // Sprint 4 - Si es que los datos del cliente estan en modo edicion y estan incorrectos entonces no dejamos avanzar el flujo
        //var customer = this.ls.getData().customer;
        let hayMasDeDosPelotudo: number = 0;
        var atLeastOne = false;
        $('input[name=check]').each(function () {
            if (this.checked) {
                hayMasDeDosPelotudo++;
                atLeastOne = true;
            }
        })

        if (!atLeastOne) {
            alert("Debe elegir un producto");
            return;
        }

        if (hayMasDeDosPelotudo > 1) {
            alert("Debe elegir sólo un producto");
            return;
        }


        if (this.ls.getData().editandoCliente) {
            this.mensajeValidacionDatosCliente = "Celular";
            $('#modalCliente').modal('show');
            return;
        }


        // //console.log(this.order);

        this.order.product = this.product[this.prodSelected]
        if (this.order.product.parkType == "CMS") {
            this.order.product.department=this.order.product.department.substring(4,this.order.product.department.length)
            this.order.product.district=this.order.product.district.substring(4,this.order.product.district.length)
            this.order.product.province=this.order.product.province.substring(4,this.order.product.province.length)            
        }
        this.order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
        this.order.status = "P";
        //localStorage.setItem('order', JSON.stringify(this.order));
        this.ls.setData(this.order);
        ////console.log( this.order.status );
        //this.router.navigate( ['saleProcess/offering'] );

        let status = this.order.product.status;

        if (status == "SUSPENDIDO") {
            alert("El cliente no puede continuar por estar SUSPENDIDO");
        }
        else if (status == "ACTIVO") {
            // this.router.navigate(['offering']);
            if (localStorage.getItem("visita")) {
                localStorage.removeItem("visita")
            }
    
            if (localStorage.getItem("dataRed")) {
                localStorage.removeItem("dataRed")
            }
            this.router.navigate(['campanas']);
        }


    }

    activateProduct(ind) {

        let contSelectedProduct: number = 0;
        this.selectedProduct.forEach((item, index) => {
            if (item.selected == true) {
                contSelectedProduct++;
            }
        });

        if (this.selectedProduct[ind].selected == false && contSelectedProduct < 3) {
            this.selectedProduct[ind].selected = true;
            contSelectedProduct++;
        } else {
            this.selectedProduct[ind].selected = false;
            contSelectedProduct--;
        }

        if (contSelectedProduct > 1) {
            this.fusionButton = false;
        } else {
            this.fusionButton = true;
        }
    }

    toggleFusionProduct(index: number) {
        return {
            'activate-product': this.selectedProduct[index].selected == true
            , 'deactivate-product': this.selectedProduct[index].selected == false
        };
    }

    /*fusionarProducto() {
        let fusionProduct: Producto = new Producto();
        let defaultProduct: Producto = new Producto();
        let contATIS: number = 0;
        //let contCMS: number = 0;
        //let contUnselect: number = 0;
        let arrayValid = [];
        let arraySourceType = [];
        this.selectedProduct.forEach((item, index) => {
            if (item.selected == true && item.parkType == 'ATIS') {
                ////console.log( item );
                if (contATIS) {
                    defaultProduct = item;
                }
                contATIS++;
                arrayValid.push({
                    department: item.department,
                    province: item.province,
                    district: item.district
                });
                arraySourceType.push(item.sourceType);
            } else if (item.selected == true && item.parkType == 'CMS') {
                ////console.log( item );
                //contCMS++;
                arrayValid.push({
                    department: item.department.substring(4),
                    province: item.province.substring(4),
                    district: item.district.substring(4)
                });
                arraySourceType.push(item.sourceType.substring(1));
            }
            //            else {
            //                contUnselect++;
            //            }
        });

        ////console.log(arrayValid);
        ////console.log(arraySourceType.sort().join(""));

        var validAddress = false;
        for (let i = 0; i < arrayValid.length - 1; i++) {
            ////console.log(i+"|"+arrayValid[i].department+"|"+arrayValid[i].province+"|"+arrayValid[i].district);
            if (arrayValid[i].department == arrayValid[i + 1].department && arrayValid[i].province == arrayValid[i + 1].province && arrayValid[i].district == arrayValid[i + 1].district) {
                validAddress = true;
            } else {
                validAddress = false;
                $('#validAddressModal').modal('show');
                return;
            }
        }

        if (contATIS > 0) {
            fusionProduct.parkType = defaultProduct.parkType;
            fusionProduct.status = defaultProduct.status;
            fusionProduct.sourceType = arraySourceType.sort().join("");
            fusionProduct.department = defaultProduct.department;
            fusionProduct.province = defaultProduct.province;
            fusionProduct.district = defaultProduct.district;
            fusionProduct.ubigeoCode = defaultProduct.ubigeoCode;
            fusionProduct.phone = defaultProduct.phone;
            fusionProduct.psprincipal = defaultProduct.psprincipal;
            fusionProduct.pslinea = defaultProduct.pslinea;
            fusionProduct.psinternet = defaultProduct.psinternet;
            fusionProduct.pstv = defaultProduct.pstv;
            fusionProduct.productDescription = defaultProduct.productDescription;
            fusionProduct.address = defaultProduct.address;
        } else {
            $('#validParkTypeModal').modal('show');
            return;
        }
        fusionProduct.productTypeCode = 0;
        fusionProduct.additionalAddress = "";
        fusionProduct.productType = "";

        this.order.product = fusionProduct;
        this.order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
        this.order.status = "P";
        this.ls.setData(this.order);
        //this.router.navigate( ['saleProcess/offering'] );
        this.router.navigate(['offering']);
    }*/

    selectionChange(input: HTMLInputElement, ind) {
        if (this.isChecked) {
            this.prodSelected = ind
        }
    }

    isChecked(input: HTMLInputElement) {
        if (input.checked === true) {
            return true
        } else {
            return false
        }
    }

    sendSVA() {


        let hayMasDeDosPelotudo: number = 0;
        var atLeastOne = false;
        $('input[name=check]').each(function () {
            if (this.checked) {
                hayMasDeDosPelotudo++;
                atLeastOne = true;
            }
        })

        if (!atLeastOne) {
            alert("Debe elegir un producto");
            return;
        }

        if (hayMasDeDosPelotudo > 1) {
            alert("Debe elegir sólo un producto");
            return;
        }

        if (this.prodSelected == -1) {

        }

        this.order.product = this.product[this.prodSelected];

        //console.log(this.order);

        this.order.type = globals.ORDER_SVA;
        //localStorage.setItem('order', JSON.stringify(this.order));
        this.ls.setData(this.order);
        ////console.log( this.order.status );
        //this.router.navigate( ['saleProcess/offering'] );

        //console.log("t " + this.order.product.parkType)


        /*if (this.order.product.parkType == '') {
            this.order.type = globals.ORDER_ALTA_NUEVA;
        } else {
            this.order.type = globals.ORDER_MEJORAR_EXPERIENCIA;
        }*/


        let status = this.order.product.status;

        if (status == "SUSPENDIDO") {
            alert("El cliente no puede continuar por estar SUSPENDIDO");
        }
        else if (status == "ACTIVO") {
            this.router.navigate(['sva']);
            this.loading = true
        }



        //this.validateOffering(this.product[this.prodSelected]);

        ////console.log("producto " + JSON.stringify(this.product[this.prodSelected]))
        /*this.validateOffering(this.product[this.prodSelected])*/
        //this.order.product = this.product[this.prodSelected]
        //localStorage.setItem('order', JSON.stringify(this.order));
        //this.showAlert = false;
        //this.order.status = "O";
        //this.order.selectedOffering = this.product[this.prodSelected]
        //this.order.sva = [];
        /*if (selectedOffering.sva != null) {
            let sva = selectedOffering.sva;
            sva.selected = true;
            this.order.sva.push(sva);
        }*/
        //this.ls.setData(this.order);
        // alert("redireccionando a pantalla sva.... this.showAlert: " + this.showAlert);
        //this.router.navigate(['sva']);


        //this.ls.setData(this.order);
        ////console.log( this.order.status );
        //this.router.navigate( ['saleProcess/offering'] );
        //this.router.navigate(['offering']);

        //this.order.type = globals.ORDER_SVA;
        //this.order.status = "O";
        //      productOffer.cashPrice = globals.CASH_PRICE_SVA;
        //      productOffer.paymentMethod = globals.PAYMENT_METHOD_SVA;
        //this.order.selectedOffering = this.productOffer;

        //this.ls.setData(this.order);
        //this.router.navigate(['saleProcess/sva']);
        //this.router.navigate(['sva']);*/
    }

    validateOffering(selectedOffering) {
        let codtypeProducto = selectedOffering.productTypeCode;
        let documentType = this.order.customer.documentType;
        let documentNumber = this.order.customer.documentNumber;
        let productName = selectedOffering.productName;
        this.showImgValidate = true;

        this.showAlert = false;


        let resumen = JSON.parse(localStorage.getItem('resumen'));
        resumen.producto = productName
        localStorage.setItem("resumen", JSON.stringify(resumen));
        //console.log("resumen ---- .-.- -- -- - ----" + JSON.parse(localStorage.getItem('resumen')));


        this.customerService.getValidateDuplicadoUbicacion(documentType, documentNumber, codtypeProducto, productName, this.nameDepart, this.nameProvince, this.nameDistrict)
            .subscribe(
            data => {
                //console.log('data 1 ')

                this.showAlert = false;
                this.order.status = "O";
                this.order.selectedOffering = selectedOffering;
                this.order.sva = [];
                if (selectedOffering.sva != null) {
                    //console.log('data 5 ')
                    let sva = selectedOffering.sva;
                    sva.selected = true;
                    this.order.sva.push(sva);
                }
                this.ls.setData(this.order);
                // alert("redireccionando a pantalla sva.... this.showAlert: " + this.showAlert);
                this.router.navigate(['sva']);


                if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
                    //console.log('data 2 ')
                    //console.log(" answerValidationOffer " + this.answerValidationOffer)
                    this.answerValidationOffer = data;

                    if (this.answerValidationOffer.responseCode != "00") {
                        //console.log('data 3 ')
                        this.showImgValidate = false;
                        this.showAlert = true;
                        this.loading = false;

                    } else {
                        //console.log('data 4 ')
                        this.showAlert = false;
                        this.order.status = "O";
                        this.order.selectedOffering = selectedOffering;
                        this.order.sva = [];
                        if (selectedOffering.sva != null) {
                            //console.log('data 5 ')
                            let sva = selectedOffering.sva;
                            sva.selected = true;
                            this.order.sva.push(sva);
                        }
                        this.ls.setData(this.order);
                        // alert("redireccionando a pantalla sva.... this.showAlert: " + this.showAlert);
                        this.router.navigate(['sva']);
                        // if(!this.isEmpty(this.order.sva)){
                        //     let order  = new Order();
                        //     order.status = "O";
                        //     order.user = this.order.user;
                        //     order.customer = this.order.customer;
                        //     order.product = this.order.product;
                        //     order.selectedOffering = selectedOffering;
                        //     this.ls.setData(order);
                        //     ////console.log('offeringgg');
                        // ////console.log(order.selectedOffering);
                        // }else{
                        //     this.order.selectedOffering = selectedOffering;
                        //     this.order.status = "O";
                        //     this.ls.setData(this.order);
                        //     ////console.log('offeringgg');
                        // ////console.log(this.order.selectedOffering);
                        // }


                    }


                } else {
                    this.loading = false
                    this.showImgValidate = false;
                    this.showAlert = true;
                    this.answerValidationOffer.responseMessage = "La validacion no pudo concretarse. Intente nuevamente";
                }

            },
            err => {
                console.log(err)
            }
            );



        /*this.order.status = "O";
        this.order.selectedOffering = selectedOffering;
        this.order.sva = [];
        if (selectedOffering.sva != null) {
            let sva = selectedOffering.sva;
            sva.selected = true;
            this.order.sva.push(sva);
        }
        this.ls.setData(this.order);
       // //console.log("order before sva : " + JSON.stringify(this.order))
        this.ls.setData(this.order);
        this.completValidate(true)*/

    }

    showProduct = prod => {
        /*if (Number(prod.sourceType) == 1 || Number(prod.sourceType) == 2 || Number(prod.sourceType) == 3 || Number(prod.sourceType) == 4)
            this.isThereAnyLineaOrBAProduct = true

        if (Number(prod.sourceType) == 5 || Number(prod.sourceType) == 6)
            this.isThereAnyTvProduct = true*/

        if (prod.parkType == 'ATIS')
            this.isThereAnyLineaOrBAProduct = true

        if (prod.parkType == 'CMS')
            this.isThereAnyTvProduct = true
    }


    //Sprint 11 - Mostrar detalle de Migraciones o SVA
    toggleMove(ind: string) {
        this.selectAccordionElement("toggle", ind);
    }

    toggleMove2(ind: string) {
        this.selectAccordionElement("toggle2", ind);
    }

    selectAccordionElement(pre: string, ind: string) {
        let parent: string = pre + ind
        $(".accordion-toggle").parent().toggleClass('ic')
        $('#' + parent).next().slideToggle('fast')
        $(".accordion-content").not($('#' + parent).next()).slideUp('fast')
    }

    getModal(ind) {
        this.showModalPark[ind] = true;
    }

    cancelCancelacionVenta(ind) {
        this.showModalPark[ind] = false;
    }

    //Sprint 24
    evaluarDeuda(_scope : any){
	
        this.loading = true;
        this.order.offline.flag = '';
			this.customerService.getdataCustomer(this.customer.documentType, this.customer.documentNumber)
			.subscribe(
				data =>{
                    //console.log("data deuda --> " + JSON.stringify(data))
					_scope.respuestaDataCustomer = false;
					if (data.responseCode == -1) {
						_scope.respuestaDataCustomer = true;
						_scope.tiene_deuda = false;
						_scope.serv_deuda_act = false;
						_scope.loadErrorDeuda = true;
						_scope.loading = false;
						_scope.strmsgerror = data.responseMessage.split('|');
                        _scope.mensaje_error_deuda = _scope.strmsgerror[0];
                        _scope.order.offline.flag += _scope.order.offline.flag == '' ? '2' : ',2';
						_scope.ls.setData(_scope.order);
						//console.log('Servicio Consulta deuda - Error Interno --> Detalle: ' + _scope.strmsgerror[1]);
						return;
					}

					if(data.responseMessage == "SUCCESS" || data.responseMessage == "SIN_DEUDA"){
                        _scope.respuestaDataCustomer = true;
                        _scope.serv_deuda_act = true;
						_scope.deuda_tot = data.responseData.deudaCuenta;
						_scope.deuda_dec = _scope.deuda_tot.substr(_scope.deuda_tot.indexOf('.'),2).trim();

						_scope.deuda_tot = _scope.deuda_tot.substr(0,_scope.deuda_tot.indexOf('.'));
						_scope.deuda_dec = _scope.deuda_dec.length == 1 ? _scope.deuda_dec + '0' : _scope.deuda_dec;

						_scope.nom_cliente = data.responseData.nombreCustomer;
						_scope.ape_cliente = data.responseData.apePatCustomer + ' ' + data.responseData.apeMatCustomer;
						_scope.tip_doc_cliente = data.responseData.tipDocCustomer;
						_scope.num_doc_cliente = data.responseData.numDocCustomer;
						_scope.lineas_cliente = data.responseData.totLineas;
						_scope.tiene_deuda = data.responseMessage == "SIN_DEUDA" || parseInt(_scope.deuda_tot) <= parseInt(data.responseData.topeDeudaPark) ? false : true;
						_scope.linea_desc = _scope.lineas_cliente == "1" || _scope.lineas_cliente == "0" ? "linea" : "lineas";
                        _scope.cli_sin_datos = false;
                        //console.log("Control de deuda Maximo: " + data.responseData.topeDeudaPark);

						let fecVenc = data.responseData.fecReciboVencidoUlt.split('-');
						let fecEmi = data.responseData.fecReciboEmitidoUlt.split('-');
						
						_scope.fec_ult_deuda = new Date(fecVenc[0],fecVenc[1] - 1,fecVenc[2]);
						_scope.fec_ult_rec_emi = new Date(fecEmi[0],fecEmi[1] - 1,fecEmi[2]);

						if(data.responseMessage == "SUCCESS" ){
							_scope.blackListService.getDateTime().subscribe(
								data => {
									let fecArray = data.responseData.substr(0,10).split('-');
									_scope.fec_actual = new Date(fecArray[0],fecArray[1] - 1, fecArray[2]);
									let diff = _scope.fec_actual.getTime() - _scope.fec_ult_deuda.getTime();
									var day = 1000 * 60 * 60 * 24;
									let days = Math.floor(diff/day);
                                    let months = Math.floor(days/31);
                                    
									_scope.tot_meses = months;
                                    _scope.mes_desc = _scope.tot_meses < 1 ? "mes" : "meses";
                                    
                                    if(days < 31) {
                                        _scope.tiempo_tot = days + " " + (days < 2 ? "dia vencido" : "dias vencidos");

                                    }
                                    else{
                                        _scope.tiempo_tot = months + " " + (months <= 1 ? "mes vencido" : "meses vencidos");
                                    }
								}
							)
						}
						
					}
					else if(data.responseMessage == "NOT_FOUND"){
                        _scope.respuestaDataCustomer = true;
                        _scope.serv_deuda_act = true;
						_scope.tiene_deuda = false;
						_scope.cli_sin_datos = true;
					}
					
					else if(data.responseMessage == "SERVICE_INACTIVED"){
						_scope.respuestaDataCustomer = true;
						_scope.serv_deuda_act = false;
						_scope.tiene_deuda = false;
					}

                    _scope.loading = false;
				},
				err => {
					_scope.respuestaDataCustomer = true;
					_scope.tiene_deuda = false;
					_scope.serv_deuda_act = false;
					_scope.loadErrorDeuda = true;
					_scope.loading = false;
					_scope.error_respuesta = true;
					_scope.mensaje_error_deuda = "Servicios no disponibles en este momento.";
					//console.log('Problemas de conexion a los Servicios de back');
				}

			);
    }
    
    cancelarConsultaDeuda() {
		this.loadErrorDeuda = false;
		this.mensaje_error_deuda = "";
    }
    salirVenta(){
		this.router.navigate(['/acciones']);
	}
}
