
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Sva } from '../model/sva';
import { Order } from '../model/order';
import { ProductOffering } from '../model/productOffering';
import { Producto } from '../model/producto';
import { LocalStorageService } from '../services/ls.service';
import * as globals from '../services/globals';
import { Customer } from '../model/customer';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
var $ = require('jquery');
import { ProgressBarService } from '../services/progressbar.service';
@Component({
	moduleId: 'resumenOut',
	selector: 'resumenOut',
	templateUrl: './resumenOut.template.html',
	providers: [CustomerService, OrderService]
})

export class ResumenOutComponent implements OnInit {
	order: Order;
	productOffering: ProductOffering;
	product: Producto;
	sva = [];
	sumaSvas = 0;
	sumaRegular = 0;
	sumaPromocional = 0;
	sumaContado = 0;
	type: string;
	orderSva: string;
	private showButtonDelete: boolean;

	objReniec: any;
	customer: Customer;
	customerReniec: Customer;
	contract = [];
	closeSaleError: boolean;
	closeSaleErrorMessage: string;
	private loading: boolean;
	disaffiliationWhitePagesGuide = true;
	affiliationElectronicInvoice = true;
	affiliationDataProtection = true;
	publicationWhitePagesGuide = true;
	shippingContractsForEmail = true;
	private loadError: boolean;
	private mensaje_error: String;
	shouldIShowPopUp: boolean;

	mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios

	productActual: Producto;

	constructor(private router: Router,
		private ls: LocalStorageService,
		private customerService: CustomerService,
		private orderService: OrderService,
		private progresBar: ProgressBarService) {
		this.router = router;
		this.ls = ls;
		this.showButtonDelete = true;
		this.customerService = customerService;
		this.loadError = false;
		this.mensaje_error = "";

		this.mensajeValidacionDatosCliente = "";//Sprint 6
		this.shouldIShowPopUp = false
	}

	ngOnInit() {

		this.progresBar.getProgressStatus()

		this.order = JSON.parse(localStorage.getItem('order'));
		this.customer = this.order.customer;
		//console.log("grupo " + JSON.stringify(this.order.user.groupPermission))
		//this.model = this.order;
		this.productOffering = this.order.selectedOffering;

		this.sva = this.order.sva;

		this.product = this.order.product;
		this.orderSva = globals.ORDER_SVA;
		this.type = this.order.type;
		this.totales(this.sva);
		if (this.type == globals.ORDER_SVA) {
			this.showButtonDelete = false;
		}

		if (this.type == globals.ORDER_MEJORAR_EXPERIENCIA) {
			this.productActual = JSON.parse(localStorage.getItem('productoActual'));
		}

		////console.log(this.sva)
	}

	totales(sva) {
		let total = 0;
		this.sva.forEach((item, index) => {
			total += parseFloat(item.cost);
		});
		this.sumaSvas = Math.fround(total * 100) / 100;
		if (this.type == this.orderSva) {
			this.sumaRegular = this.sumaSvas;
			this.sumaPromocional = this.sumaSvas;
			this.sumaContado = 0;
		} else {
			this.sumaRegular = Math.fround((total + this.productOffering.price) * 100) / 100;
			this.sumaPromocional = Math.fround((total + this.productOffering.promPrice) * 100) / 100;
			this.sumaContado = this.productOffering.cashPrice;
		}
	}

	updateSvas(objSva: Sva) {

		let arraySva = this.sva;

		var index = arraySva.indexOf(objSva);
		if (index > -1) {
			arraySva.splice(index, 1);
		}
		this.sva = arraySva;
		this.totales(arraySva);
		this.order.sva = this.sva;
		//localStorage.setItem('order', JSON.stringify(this.order));
		this.ls.setData(this.order);
		if (this.order.type == globals.ORDER_SVA && this.sva.length == 0) {
			this.router.navigate(['svaOut']);

			this.order.equipment = undefined;
			this.order.equipmentName = undefined;
			this.ls.setData(this.order);
		}
	}

	validReniec() {
		//console.log("CALL");
		//console.log(this.ls.getData());
		this.loading = true;
		this.order.status = 'R';

		// Sprint 4 - obtenemos los datos del cliente...pueden estar actualizados los datos celular, telefono, correo
		var customer = this.ls.getData().customer;
		// Sprint 4 - seteamos los datos del cliente
		this.order.customer = customer;

		this.ls.setData(this.order);

		// Sprint 6 - Para todos los perfiles ahora se muestra la validacion RENIEC
		this.router.navigate(['contratosOut']); //glazaror ahora debe cargar contract
	}

	continueSale() {
		let fecNac: String;
		//let fecExp : String;

		if (this.customer.documentType == "DNI") {

			this.customerReniec = this.customer;
			this.customerReniec.firstName = this.objReniec.names;
			this.customerReniec.lastName1 = this.objReniec.lastName;
			this.customerReniec.lastName2 = this.objReniec.motherLastName;

			fecNac = this.objReniec.birthdate;
			this.objReniec.birthdate = fecNac.substring(0, 4) + "-" + fecNac.substring(4, 6) + "-" + fecNac.substring(6, 8);
			this.customerReniec.birthDate = this.objReniec.birthdate;

			this.customerReniec.birthDep = this.objReniec.departmentBirthLocation;
			this.customerReniec.birthCity = this.objReniec.provinceBirthLocation;
			this.customerReniec.birthDist = this.objReniec.districtBirthLocation;
			this.order.customer = this.customerReniec;

		}


		this.ls.setData(this.order);
		this.order.status = 'T';
	}

	closeMessageError() {
		$('#pop').modal('hide')
	}

	mostrarError(err) {
		this.loading = false;
		this.loadError = true
		$('#pop').modal('show')
		if (err.responseMessage)
			this.mensaje_error = err.responseMessage;
		else
			this.mensaje_error = "No se pudo cerrar la venta, Por favor vuelva a intentarlo."
	}

	// Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"
	cerrarDialogo(nombreModal) {
		$("#" + nombreModal).modal('hide');
	}

	eliminarProductoPrincipal() {
		this.productOffering = null;
		this.order.selectedOffering = null;
		this.ls.setData(this.order);
		this.router.navigate(['offeringOut']);
	}

	eliminarSva(codigo, obligatorio, objSva) {
		if (obligatorio && objSva.cost == 0) {
			//si es que el sva es obligatorio... entonces no eliminamos el sva
			return;
		}

		if (objSva.unit == "BLOQUE FULL HD" && objSva.cost != 0) {
			this.sva.forEach((item, index) => {
				if (item.unit == objSva.unit) {
					this.sva.splice(index);
				} else if (item.bloquePadre == objSva.unit && item.clickPadre) {
					this.sva.splice(index);
				}
			});
		} else if (objSva.code == "BTV") {
			var indexToRemove = -1;
			this.sva.forEach((item, index) => {
				if (item.id == objSva.id) {
					indexToRemove = index;
				}
			});
			this.sva.splice(indexToRemove, 1);
		} else {
			var indexToRemove = -1;
			this.sva.forEach((item, index) => {
				if (item.code == codigo) {
					indexToRemove = index;
				}
			});
			this.sva.splice(indexToRemove, 1);
		}
		//recalculamos el subtotal
		this.totales(this.sva);
		this.order.sva = this.sva;

		if (this.order.sva.length == 0 && this.type == 'S') {
			this.order.sva = null;
			this.ls.setData(this.order);
			//Si es flujo de SVA y se eliminaron todos los svas entonces redirigmos a la pantalla de SVA
			this.router.navigate(['svaOut']);
		}
		this.ls.setData(this.order);

	}

	eliminarCondicionVenta(condicion) {
		//console.log("condicion de venta " + condicion)

		switch (condicion) {
			case "condicionTratamientoDatos":
				this.order.condicionTratamientoDatos = false;
				this.order.affiliationDataProtection = false
				break
			case "condicionFiltroWebParental":
				this.order.condicionFiltroWebParental = false;
				this.order.parentalProtection = false
				break
			case "condicionPackVerde":
				this.order.condicionPackVerde = false;
				this.order.disaffiliationWhitePagesGuide = false;
				this.order.affiliationElectronicInvoice = false;
				this.order.publicationWhitePagesGuide = true;
				break
			case "condicionDebitoAutomatico":
				this.order.condicionDebitoAutomatico = false;
				this.order.automaticDebit = false
				break
		}
		/*if ("condicionTratamientoDatos" == condicion) {
			this.order.condicionTratamientoDatos = false;
			this.order.affiliationDataProtection = false
		}else {
			this.order.affiliationDataProtection = true
		}*/

		/*if ("condicionFiltroWebParental" == condicion) {
			this.order.condicionFiltroWebParental = false;
			this.order.parentalProtection = false
		}else {
			this.order.parentalProtection = true
		}*/

		/*if ("condicionPackVerde" == condicion) {
			this.order.condicionPackVerde = false;
		}else {
			this.order.condicionFiltroWebParental = true
		}*/

		/*if (this.order.condicionPackVerde) {
			this.order.disaffiliationWhitePagesGuide = false;
			this.order.affiliationElectronicInvoice = false;
			this.order.publicationWhitePagesGuide = true;
		}*/

		/*if (this.order.condicionTratamientoDatos) {
			this.order.affiliationDataProtection = false;
		}else {
			this.order.affiliationDataProtection = true;
		}*/

		/*if (this.order.condicionFiltroWebParental) {
			this.order.parentalProtection = false;
		}else {
			this.order.parentalProtection = true
		}*/
		this.ls.setData(this.order);
	}


	// Copiado de SaleSummary... solo debe ser ejecutado cuando el perfil del vendedor es TIENDA
	sendTiendaVenta() {
		//console.log("PRESENCIAL");
		this.loading = true;
		this.order.disaffiliationWhitePagesGuide = this.disaffiliationWhitePagesGuide;
		this.order.affiliationElectronicInvoice = this.affiliationElectronicInvoice;
		this.order.affiliationDataProtection = this.affiliationDataProtection;
		this.order.publicationWhitePagesGuide = this.publicationWhitePagesGuide;
		this.order.shippingContractsForEmail = this.shippingContractsForEmail;

		//this.order.campaniaSeleccionada = 'PRUEBA';

		var svaList = [];
		$.each(this.order.sva, function (index, item) {
			svaList.push(item.id);
		});

		let webOrder = {
			userId: this.order.user.userId,
			phone: this.order.product.phone,
			department: this.order.product.department,
			province: this.order.product.province,
			district: this.order.product.district,
			address: this.order.product.address,
			latitudeInstallation: this.order.product.coordinateY ? this.order.product.coordinateY : 0,
			longitudeInstallation: this.order.product.coordinateX ? this.order.product.coordinateX : 0,
			addressAdditionalInfo: this.order.product.additionalAddress ? this.order.product.additionalAddress : '',
			sva: svaList,
			customer: this.order.customer,
			product: this.order.selectedOffering,
			type: this.order.type,
			disaffiliationWhitePagesGuide: this.order.disaffiliationWhitePagesGuide,
			affiliationElectronicInvoice: this.order.affiliationElectronicInvoice,
			affiliationDataProtection: this.order.affiliationDataProtection,
			publicationWhitePagesGuide: this.order.publicationWhitePagesGuide,
			automaticDebit: this.order.automaticDebit,
			shippingContractsForEmail: this.order.shippingContractsForEmail,
			originOrder: this.order.user.groupPermission,
			// Sprint 5 - campos adicionales al cerrar la venta: nombrePuntoVenta y entidad
			entidad: this.order.user.entity,
			nombrePuntoVenta: this.order.user.group,
			// Sprint 6 - condicion preventa filtro web parental
			parentalProtection: this.order.parentalProtection,
			// Sprint 9 - Nombre del producto de origen (solo servirá para flujo SVA)
			sourceProductName: this.order.product.productDescription,
			// Sprint 10 - Campania seleccionada para altas y migras
			campaniaSeleccionada: this.order.campaniaSeleccionada,
			// Sprint 13 - Envio datos normalizador hacia automatizador parte 2
			address_referencia: this.order.address_referencia,
			address_ubigeoGeocodificado: this.order.address_ubigeoGeocodificado,
			address_descripcionUbigeo: this.order.address_descripcionUbigeo,
			address_direccionGeocodificada: this.order.address_direccionGeocodificada,
			address_tipoVia: this.order.address_tipoVia,
			address_nombreVia: this.order.address_nombreVia,
			address_numeroPuerta1: this.order.address_numeroPuerta1,
			address_numeroPuerta2: this.order.address_numeroPuerta2,
			address_cuadra: this.order.address_cuadra,
			address_tipoInterior: this.order.address_tipoInterior,
			address_numeroInterior: this.order.address_numeroInterior,
			address_piso: this.order.address_piso,
			address_tipoVivienda: this.order.address_tipoVivienda,
			address_nombreVivienda: this.order.address_nombreVivienda,
			address_tipoUrbanizacion: this.order.address_tipoUrbanizacion,
			address_nombreUrbanizacion: this.order.address_nombreUrbanizacion,
			address_manzana: this.order.address_manzana,
			address_lote: this.order.address_lote,
			address_kilometro: this.order.address_kilometro,
			address_nivelConfianza: this.order.address_nivelConfianza,
			// Anthony - Sprint 15
			serviceType: this.order.product.parkType,
			cmsServiceCode: this.order.product.serviceCode,
			cmsCustomer: this.order.product.subscriber,
			//Sprint 16
			datetimeSalesCondition: this.order.datetimeSalesCondition
		};

		////console.log(this.order);

		//console.log("webOrder Tienda");
		//console.log(webOrder);

		//salvamos el weborder en localstorage.... otro objeto mala practica... pendiente optimizacion
		localStorage.setItem('webOrder', JSON.stringify(webOrder));
		this.router.navigate(['contratosOut']);
	}

}
