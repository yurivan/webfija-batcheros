import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';
import { Recupero } from '../model/recupero';

@Injectable()
export class HistoricoService {

    // baseUrl = globals.BASE_URL+"product";
    baseUrl = globals.PRODUCT_BASE_URL + "order/web";

    constructor(private http: HttpService) {
        this.http = http;
    }


    getFilterHistoric(codatis: string,state: string, dateBegin: string, dateEnd: string) {
        let request = {};
        request = {
            codAtis: codatis,
            state: state,
            dateStart: dateBegin,
            dateEnd: dateEnd,
        };
        let body = JSON.stringify(request);

        return this.http.post(this.baseUrl + '/report', body)
            .map(res => res.json());
    }

    getFilterTracking(codatis: string) {
        let request = {};
        request = {
            codAtis: codatis
        };
        let body = JSON.stringify(request);

        return this.http.post(this.baseUrl + '/report2', body)
            .map(res => res.json());
    }

    getRecuperoVenta(recupero: Recupero){
        let request = {};
        request = {
            orderId: recupero.orderId,
            motivoCaida: recupero.motivoCaida,
            fechaPago: recupero.fechaPago,
            direccion: recupero.direccion,
            telefono: recupero.telefono,
            dvr: recupero.svaDvr,
            hd: recupero.svaHd,
            smart: recupero.svaSmart,
            dni: recupero.docnumber
        };
        let body = JSON.stringify(request);

        return this.http.post(this.baseUrl + '/recupero', body)
            .map(res => res.json());
    }

}
