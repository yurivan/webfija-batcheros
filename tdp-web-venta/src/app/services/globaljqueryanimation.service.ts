import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import { Customer } from '../model/customer';
import * as globals from './globals';
var $ = require('jquery');

@Injectable()
export class JqueryAnimation {

    animationInput(){
        $(".input-effect input").val("");

		$(".input-effect input").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})

		$(".paraselect select").val("");

		$(".paraselect select").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
    }
}
