import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class BlackListService {

    constructor(private http: HttpService, private http2: Http) {
        this.http = http;
    }

    blackList = globals.CUSTOMER_BASE_URL + "config/initial";

    getFecha = globals.CUSTOMER_BASE_URL + "config/fecha";

    argumentarios = globals.CUSTOMER_BASE_URL + "config/argumentarios";

    canalEntidad = globals.CUSTOMER_BASE_URL + "config/canalEntidad";

    getInitial() {
        let request = {
        };
        let body = JSON.stringify(request);

        return this.http.post(this.blackList, body)
            .map(res => res.json());
    }

    getDateTime() {
        let request = {
        };
        let body = JSON.stringify(request);

        return this.http.post(this.getFecha, body)
            .map(res => res.json());
    }
    
    getArgumentarios(perfil:string){
        let request = {
            perfil: perfil,
        };
        let body = JSON.stringify(request);
        return this.http.post(this.argumentarios, body)
        .map(res => res.json());

    }

    getCanalEntidad(canal:string){
        let request = {
            canal: canal,
        };
        let body = JSON.stringify(request);
        return this.http.post(this.canalEntidad, body)
        .map(res => res.json());
    }
}    