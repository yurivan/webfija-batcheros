import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';

@Injectable()
export class ReporteService {
	baseUrl = globals.BASE_URL + "order/web/report";

	constructor(private http: HttpService) {
		this.http = http;
	}

	getHeaders(codatis,niveles) {
		let body = {codatis : codatis, niveles : niveles};

		return this.http.post(this.baseUrl + '/obtenerFiltroReporte', body)
			.map(res => res.json());
	}

	getData(filters) {
		let body = {
			"socio": (filters.socio.length>0)?filters.socio.toString().toUpperCase():null,
			"campania": (filters.campania.length>0)?filters.campania.toString().toUpperCase():null,
			"opComercial": (filters.opComercial.length>0)?filters.opComercial.toString().toUpperCase():null,
			"canal": (filters.canal.length>0)?filters.canal.toString().toUpperCase():null,
			"region": (filters.region.length>0)?filters.region.toString().toUpperCase():null,
			"zonal": (filters.zonal.length>0)?filters.zonal.toString().toUpperCase():null,
			"ptoVenta": (filters.ptoVenta.length>0)?filters.ptoVenta.toString().toUpperCase():null,
			"fechaInicio": filters.dateStart,
			"fechaFinal": filters.dateEnd,
			"codeFilter": filters.codeFilter
		  }
		
		return this.http.post(this.baseUrl + '/filtrarReporte', body)
			.map(res => res.json());
	}

	getDataExport(filters) {
		let body = {
			"socio": (filters.socio.length>0)?filters.socio.toString().toUpperCase():null,
			"campania": (filters.campania.length>0)?filters.campania.toString().toUpperCase():null,
			"opComercial": (filters.opComercial.length>0)?filters.opComercial.toString().toUpperCase():null,
			"canal": (filters.canal.length>0)?filters.canal.toString().toUpperCase():null,
			"region": (filters.region.length>0)?filters.region.toString().toUpperCase():null,
			"zonal": (filters.zonal.length>0)?filters.zonal.toString().toUpperCase():null,
			"ptoVenta": (filters.ptoVenta.length>0)?filters.ptoVenta.toString().toUpperCase():null,
			"fechaInicio": filters.dateStart,
			"fechaFinal": filters.dateEnd,
			"codeFilter": filters.codeFilter
		  }
		
		return this.http.post(this.baseUrl + '/exportReporte', body)
			.map(res => res.json());
	}

}