import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';

@Injectable()
export class OutDetalleService {
	baseUrl = globals.BASE_URL + "order/web/report";

	constructor(private http: HttpService) {
		this.http = http;
	}

	getData(orderId: string) {
		let body = JSON.stringify({ rol: orderId });

		return this.http.post(this.baseUrl + '/filtrarReporte', body)
			.map(res => res.json());
	}


}