import { Injectable } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';

@Injectable()
export class DireccionService {

    // baseUrl = globals.BASE_URL+"product";
    baseUrl = globals.PRODUCT_BASE_URL + "address";

    constructor(private http: HttpService) {
        this.http = http;
    }


    getDataDepartamento() {

        //	return this.http.get('https://app-venta-fija.firebaseio.com/departments.json').map(res => res.json());
        return globals.DEPARTMENTS_DIRECTION;

    }

    getDataProvincias() {

        //return this.http.get('https://app-venta-fija.firebaseio.com/provinces.json').map(res => res.json());
        return globals.PROVINCE_DIRECTION;
    }

    getDataDistritos() {

        //return this.http.get('https://app-venta-fija.firebaseio.com/districts.json').map(res => res.json());
        return globals.DISTRICT_DIRECTION;

    }

    //RENIEC

    getDataDepartamentoReniec() {
        //        return this.http.get('https://app-venta-fija.firebaseio.com/reniec_departments.json').map(res => res.json());
        return globals.DEPARTMENTS_RENIEC;

    }




    getDataProvinciasReniec() {
        //        return this.http.get('https://app-venta-fija.firebaseio.com/reniec_provinces.json').map(res => res.json());
        return globals.PROVINCES_RENIEC;

    }




    getDataDistritosReniec() {
        //        return this.http.get('https://app-venta-fija.firebaseio.com/reniec_districts.json').map(res => res.json());
        return globals.DISTRICTS_RENIEC;

    }

    /*getDataLocationMap(direccion,departamento,provincia,distrito) {
      //getDataLocationMap(direccion) {  
        //console.log('https://maps.googleapis.com/maps/api/geocode/json?address='+direccion+'&components=administrative_area_level_1:|administrative_area_level_2:|locality:|country:PE&key=AIzaSyAEu3Eds2p2jLM4rOT4mrCzUTIlTPDS4H8');
        //return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+direccion+'&components=country:PE|administrative_area_level_1:|administrative_area_level_2:|locality:&key=AIzaSyAEu3Eds2p2jLM4rOT4mrCzUTIlTPDS4H8').map(res => res.json());

        console.log('https://maps.googleapis.com/maps/api/geocode/json?address='+direccion+'&components=administrative_area_level_1:'+departamento+'|administrative_area_level_2:'+provincia+'|locality:'+distrito+'|country:PE&key='+globals.GOOGLE_MAPS_API_KEY);
        return this.http.get('https://maps.googleapis.com/maps/api/geocode/json?address='+direccion+'&components=country:PE|administrative_area_level_1:'+departamento+'|administrative_area_level_2:'+provincia+'|locality:'+distrito+'&key='+globals.GOOGLE_MAPS_API_KEY).map(res => res.json());


    }*/

    getGeocodificarDireccion(ubi: string, dir: string, distrito: string, provincia: string, orderId: string) {
        let request = {};
        request = {
            ubigeo: ubi,
            direccion: dir,
            distrito: distrito,
            provincia: provincia,
            id: orderId
        };
        let body = JSON.stringify(request);

        return this.http.post(this.baseUrl + '/geocodificardireccion', body)
            .map(res => res.json());
    }


    getMessageValidationAddress(dir: string) {
        let request = {};
        request = {
            address: dir
        };
        let body = JSON.stringify(request);

        return this.http.post(this.baseUrl + '/validateAddress', body)
            .map(res => res.json());
    }

    getValidationAtis(provincia: string,tipoVia: string, nombreVia: string,
                    numeroPuerta1: string, numeroPuerta2: string,
                    cuadra: string, tipoInterior: string,
                    numeroInterior: string, piso: string,
                    tipoVivienda: string, nombreVivienda: string,
                    tipoUrbanizacion: string, nombreUrbanizacion: string,
                    manzana: string, lote: string, kilometro: string){
        let request = {};
        request = {
            provincia: provincia,
            tipoVia: tipoVia,
            nombreVia: nombreVia,
            numeroPuerta1: numeroPuerta1,
            numeroPuerta2: numeroPuerta2,
            cuadra: cuadra,
            tipoInterior: tipoInterior,
            numeroInterior: numeroInterior,
            piso: piso,
            tipoVivienda: tipoVivienda,
            nombreVivienda: nombreVivienda,
            tipoUrbanizacion: tipoUrbanizacion,
            nombreUrbanizacion: nombreUrbanizacion,
            manzana: manzana,
            lote: lote,
            kilometro: kilometro
        };
        let body = JSON.stringify(request);

        return this.http.post(this.baseUrl + '/validateAtis', body)
            .map(res => res.json());
    }

    getFtth(departamento: string,provincia: string,distrito: string){
        let request = {};
        request = {
            departamento: departamento,
            provincia: provincia,
            distrito: distrito
        };
        let body = JSON.stringify(request);

        return this.http.post(this.baseUrl + '/getCoordenadaXY', body)
        .map(res => res.json());
    }
}
