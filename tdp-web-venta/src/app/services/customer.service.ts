import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import { Customer } from '../model/customer';
import * as globals from './globals';
import { Order } from '../model/order';

@Injectable()
export class CustomerService {

	baseUrl = globals.CUSTOMER_BASE_URL+"customer/getInfo";

	identifyUrl = globals.CUSTOMER_BASE_URL+"customer/identify";

	contractUrl = globals.CUSTOMER_BASE_URL+"customer/web/contract";

    validateDuplicado = globals.CUSTOMER_BASE_URL+"customer/validateCategoryActive";

	// Sprint 6
	saleConditionsUrl = globals.CUSTOMER_BASE_URL+"customer/web/salecondition";

	contactosUrl = globals.CUSTOMER_BASE_URL+"customer/web/contactos";

	customerOutUrl = globals.CUSTOMER_BASE_URL+"customer/out";

	customerGetId = globals.CUSTOMER_BASE_URL + "customer/orderid";
	
	updateTpdOrderUrl = globals.CUSTOMER_BASE_URL+"customer/web/updateTdpOrder";

data : any;

	//Mijail
	customerGetDue = globals.CUSTOMER_BASE_URL + "customer/getCustomerDue"

	//Sprint 24 RUC
	customerSearchRuc = globals.CUSTOMER_BASE_URL + "customer/consultaRuc"
	getReniecIntentos = globals.CUSTOMER_BASE_URL+"user/web/getReintentosReniec";

	constructor(private http: HttpService, private http2: Http){
		this.http = http;
	}

	getData(documentType, documentNumber){
		let request = {
						documentType:documentType,
						documentNumber: documentNumber
					};
		let body = JSON.stringify(request);

		return this.http.post(this.baseUrl, body)
			.map(res => res.json());
	}

	getContract(data){
		//console.log(data);
		let body = JSON.stringify(data);
		//console.log(body);
		return this.http.post(this.contractUrl, body)
			.map(res => res.json());

	}
	
	// Sprint 7
	getPaises() {
		if (globals.COUNTRIES_LIST == null) {
			globals.setCountries(this.http2.get("https://restcountries.eu/rest/v2/all?fields=name;alpha2Code;translations")
				.map(res => res.json()));
		}
		return globals.COUNTRIES_LIST;
		
	}
	
	// Sprint 6
	getSaleConditions(data){
		//console.log(data);
		let body = JSON.stringify(data);
		//console.log(body);
		return this.http.post(this.saleConditionsUrl, body)
			.map(res => res.json());

	}

    getValidateDuplicado(documentType, documentNumber, productTypeCode, productName){
        let request = {
                        documentType:documentType,
                        documentNumber: documentNumber,
                        productTypeCode: productTypeCode,
                        productName: productName
                    };
        let body = JSON.stringify(request);

        return this.http.post(this.validateDuplicado, body)
            .map(res => res.json());
    }

	getValidateDuplicadoUbicacion(documentType, documentNumber, productTypeCode, productName, departmentName, provinceName, districtName){
        let request = {
                        documentType:documentType,
                        documentNumber: documentNumber,
                        productTypeCode: productTypeCode,
                        productName: productName,
						departmentName: departmentName,
						provinceName: provinceName,
						districtName: districtName
                    };
        let body = JSON.stringify(request);

		//console.log(body);

        return this.http.post(this.validateDuplicado, body)
            .map(res => res.json());
    }


	getIdentify(customer: Customer, orderId: string) {

		let body = '{"documentType":' + '"' + customer.documentType + '","documentNumber":' + '"' + customer.documentNumber + '", "id": "' + orderId + '"}';

        return this.http.post(this.identifyUrl, body)
            .map(res => res.json());
//      let data={
//      "documentType":"DNI",
//      "documentNumber":"10526062"
//       };
	}
	
	getIdentifyRuc(customer: Customer, orderId: string) {

		let body = '{"documentType":' + '"' + customer.tipoDocumentoRrll + '","documentNumber":' + '"' + customer.numeroDocumentoRrll + '", "id": "' + orderId + '"}';

		return this.http.post(this.identifyUrl, body)
			.map(res => res.json());
	}

	actualizarContacto(customer : Customer){
		
		let body = JSON.stringify(customer);

			return this.http.post(this.contactosUrl, body)
				.map(res => res.json());
	}

	updateContactTdpOrder(order : Order){
		
		let request: any;
		request = {
			order_id: order.id,
			customer: order.customer
		}

		let body = JSON.stringify(request);

			return this.http.post(this.updateTpdOrderUrl, body)
				.map(res => res.json());
	}

	getCustomerOutWebService(documentNumber: string){
		let request: any;
		request = {
			phoneOrCms: documentNumber
		}

		let body = JSON.stringify(request);

		return this.http.post(this.customerOutUrl + '/web/services', body)
			.map(res => res.json());
	}

	getOrderIdCustomer() {
		let request: any;
		request = {
		}
		let body = JSON.stringify(request);
		return this.http.post(this.customerGetId, body)
			.map(res => res.json());
	}

	//Mijail
	getdataCustomer(documentType, documentNumber){
		let data = '{"COD-CRI-BUS-CD": "D","TIP-DOC-CLT-SN":'+ '"' +documentType+ '","DOC-CLI-CLT-SN":'+ '"' +documentNumber+'"}';
		//return this.http.post('http://localhost:8080/prueba',data).map( res => res.json());
        return this.http.post(this.customerGetDue,data).map( res => res.json());
    }

	//Sprint 24 RUC
	getdataCustomerRuc(documentNumber){
		let request = '{"numDocumento": "' + documentNumber + '" ,"sisOrigen" : "1"}';
		return this.http.post(this.customerSearchRuc,request).map( res => res.json());
	}

	getIntentosReniec(){
		return this.http.get(this.getReniecIntentos).map( res => res.json());
	}
}
