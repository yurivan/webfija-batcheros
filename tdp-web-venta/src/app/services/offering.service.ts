import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Producto } from '../model/producto';
import { Customer } from '../model/customer';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';
import { locales } from 'moment';

@Injectable()
export class OfferingService {

		// baseUrl = globals.BASE_URL+"product";
		baseUrl = globals.PRODUCT_BASE_URL+"product";

	constructor(private http: HttpService){
		this.http = http;
	}
	//getData(){
	getData(customer : Customer, product : Producto, codigoAtis : string, entidad : string, canalAtis : string, segmento : string,
			canalEquivalenciaCampania : string, entidadEquivalenciaCampania : string, orderId: string,flagFtth?: boolean){
		
		localStorage.setItem('calculaladoraOffLine', JSON.stringify(product));

		let request = {};
		let department = product.department;
		let province = product.province;
		let district = product.district;
		let zonal = product.district;
		let ubigeoCode = product.ubigeoCode;
		let prodTypeCode= product.sourceType;
		let psprincipal= product.psprincipal;
		let productDescription= product.productDescription;
        let svcCode = '';

		if(product.parkType == "CMS"){
			let dep = product.department;
			let prov = product.province;
			let dis = product.district;
			let ubigeo = "";
			ubigeo=dep.substring(0, 3) + prov.substring(0, 3) + dis.substring(0, 3);
			department= "" + dep.substring(4);
			province= "" + prov.substring(4);
			district= "" + dis.substring(4);
			prodTypeCode= "" + product.sourceType.substring(1);
			ubigeoCode = ubigeo;
		}
		request = {
			documentType: customer.documentType,
			documentNumber: customer.documentNumber,
			department: department,
			province: province,
			district: district,
			zonal: district,
			ubigeoCode: ubigeoCode,
			atisSellerCode: codigoAtis, //"93693",
			legacyCode: product.parkType,
			prodTypeCode: prodTypeCode,
			sellerEntity : entidad,
			sellerChannel : canalAtis,
			sellerSegment : segmento,
			
			sellerChannelEquivalentCampaign : canalEquivalenciaCampania,
			sellerEntityEquivalentCampaign : entidadEquivalenciaCampania,

			orderId: orderId,
			flagFtth: flagFtth?flagFtth:false,
			psprincipal: psprincipal,
			productDescription: productDescription
		};

		if (product.parkType == "ATIS") {
			request['phoneNumber'] = product.phone;

            request['productSvcCode'] = product.phone;

		} else {
			request['coordinateX'] = product.coordinateX+'';
			request['coordinateY'] = product.coordinateY+'';
            if(product.parkType){
                request['productSvcCode'] = product.serviceCode;
            }else{
                request['productSvcCode'] = '';
            }

		}

		  request['scoringResponse'] = JSON.parse(localStorage.getItem("scoring_offering"))

		//console.log(request);
		let body = JSON.stringify(request);
		//let body = '{"documentType": "DNI", "documentNumber": "44936062", "department": "lima", "province": "lima", "district": "san juan de miraflores", "zonal": "barrios altos", "ubigeoCode": "001001024", "atisSellerCode": "93693", "legacyCode": "CMS", "prodTypeCode": "", "coordinateX": "-77.025388", "coordinateY": "-12.127690" }';


		//return this.http.post(this.baseUrl + '/offering', body)
        return this.http.post(this.baseUrl + '/v2/offering', body)
			.map(res => res.json());

	}
}
