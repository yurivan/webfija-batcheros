import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';
import { Producto } from '../model/producto';

@Injectable()
export class SvaService {
	baseUrl = globals.PRODUCT_BASE_URL + "product";

	constructor(private http: HttpService, private httpRequest: Http) {
		this.http = http;
		this.httpRequest = httpRequest;
	}
	//to push

	getData(productCode: string, svasBloqueTV: string, svasLinea: string, svasInternet: string, productId: string) {
		let body = JSON.stringify({ productCode: productCode, applicationCode: 'WEBVF', altaPuraSvasBloqueTV: svasBloqueTV, altaPuraSvasLinea: svasLinea, altaPuraSvasInternet: svasInternet, productId: productId });

		return this.http.post(this.baseUrl + '/complements', body)
			.map(res => res.json());
	}

	getData_tvTech(productCode: string, svasBloqueTV: string, svasLinea: string, svasInternet: string, productId: string, tvTech: string) {
				let body = JSON.stringify({ productCode: productCode, applicationCode: 'WEBVF', altaPuraSvasBloqueTV: svasBloqueTV, altaPuraSvasLinea: svasLinea, altaPuraSvasInternet: svasInternet, productId: productId, tvTech: tvTech });
		
				return this.http.post(this.baseUrl + '/complements', body)
					.map(res => res.json());
			}

	/*getNumDecos(type: string){
		let body = {type: type};

		let headers = new Headers();
    	headers.append('Content-Type', 'application/x-www-form-urlencoded');

		let urlSearchParams = new URLSearchParams();
		urlSearchParams.append('type', type);

		return this.httpRequest.post(this.baseUrl + '/numdecos', urlSearchParams.toString(), { headers: headers })
			.map(res => res.json());
	}*/

	getNumDecos(type: string) {
		let body = { type: type };

		let urlSearchParams = new URLSearchParams();
		urlSearchParams.append('type', type);

		return this.http.post(this.baseUrl + '/numdecos', urlSearchParams.toString())
			.map(res => res.json());
	}

	getDatav2(productCode: string, serviceCode: string, productType: string, legacyCode: string, productId: string, code: string, unit: string, canalVenta: string, tvTech:string, targetType:string) {

		let request: any;
		var product= JSON.parse(localStorage.getItem('calculaladoraOffLine'));
		if (productCode == null) {
			request = {
				sellerChannelEquivalentCampaign: canalVenta,
				serviceCode: serviceCode,
				productType: productType,
				legacyCode: legacyCode,
				applicationCode: 'WEBVF',
				productId: productId,
				svaMigracionesUnit: unit,
				psprincipal: product.psprincipal,
				svaMigracionesCode: code,
				tvTech:tvTech, 
				targetType:targetType
			};
		} else {
			request = {
				sellerChannelEquivalentCampaign: canalVenta,
				productCode: productCode,
				serviceCode: serviceCode,
				productType: productType,
				legacyCode: legacyCode,
				applicationCode: 'WEBVF',
				productId: productId,
				svaMigracionesUnit: unit,
				psprincipal: product.psprincipal,
				svaMigracionesCode: code,
				tvTech:tvTech, 
				targetType:targetType
			};
		}



		let body = JSON.stringify(request);

		return this.http.post(this.baseUrl + '/v2/complements', body)
			.map(res => res.json());
	}

	getDatav2out(productCode: string, 
				 serviceCode: string, 
				 productType: string, 
				 legacyCode: string, 
				 productId: string, 
				 code: string, 
				 unit: string, 
				 migracionSvasBloqueTV: string,
				 tvTech: string,
				 internetSpeed: string) {

		let request: any;

		/*if (productCode == null) {
			request = {
				sellerChannelEquivalentCampaign: canalVenta,
				serviceCode: serviceCode,
				productType: productType,
				legacyCode: legacyCode,
				applicationCode: 'WEBVF',
				productId: productId,
				svaMigracionesUnit: unit,
				svaMigracionesCode: code
			};
		} else {*/
		let canalEntidad = JSON.parse(localStorage.getItem('canalEntidad'))
		
		
			request = {
				productCode: productCode,
				serviceCode: serviceCode,
				productType: productType,
				legacyCode: legacyCode,
				applicationCode: 'WEBVF',
				productId: productId,
				svaMigracionesUnit: unit,
				svaMigracionesCode: code,
				migracionSvasBloqueTV: migracionSvasBloqueTV,
				tvTech: tvTech,
				sellerChannelEquivalentCampaign: canalEntidad.element,
				internetSpeed: internetSpeed
			};
		/*}*/



		let body = JSON.stringify(request);

		return this.http.post(this.baseUrl + '/outcall/v2/complements', body)
			.map(res => res.json());
	}

	getEnvioDeCoordenadas(x: string, y: string) {
		let request: any;
		request = {
			NUM_COD_CDX: x,
			NUM_COD_CDY: y
		};
		let body = JSON.stringify(request);

		return this.http.post(this.baseUrl + '/out/gis1', body)
			.map(res => res.json());
	}

	getFacilidadesTecnicas(x: string, y: string) {
		let request: any;
		request = {
			NUM_COD_CDX: x,
			NUM_COD_CDY: y
		};
		let body = JSON.stringify(request);

		return this.http.post(this.baseUrl + '/out/gis', body)
			.map(res => res.json());
	}

	getOfertasProduct(documentType: string, document: string) {
		let request: any;
		request = {
			documentType: documentType,
			documentNumber: document
		};

		let body = JSON.stringify(request);

		return this.http.post(this.baseUrl + '/outcall/offeringcapta', body)
			.map(res => res.json());
	}
        //Sprint 25 calculadora
	getOfferingPlanta(atis_cms: string, legacyCode: string, commercialOperation: string, product:Producto) {
		let request: any;
		request = {
			phoneOrCms: atis_cms,
			legacyCode: legacyCode,
			commercialOperation: commercialOperation,
			psprincipal: product.psprincipal,
			productDescription: product.productDescription
		};

		let body = JSON.stringify(request);

		return this.http.post(this.baseUrl + '/outcall/offeringplanta', body)
			.map(res => res.json());
	}


	




	getAgregarSva(productCode: string, serviceCode: string, productType: string, legacyCode: string, productId: string, code: string, unit: string, canalVenta: string,tvTech:string) {

		let request: any;
		var product= JSON.parse(localStorage.getItem('calculaladoraOffLine'));
		if (productCode == null) {
			request = {
				sellerChannelEquivalentCampaign: canalVenta,
				serviceCode: serviceCode,
				productType: productType,
				legacyCode: legacyCode,
				applicationCode: 'WEBVF',
				productId: productId,
				svaMigracionesUnit: unit,
				psprincipal: product.psprincipal,
				svaMigracionesCode: code,
				tvTech: tvTech?tvTech:"",
			};
		} else {
			request = {
				sellerChannelEquivalentCampaign: canalVenta,
				productCode: productCode,
				serviceCode: serviceCode,
				productType: productType,
				legacyCode: legacyCode,
				applicationCode: 'WEBVF',
				productId: productId,
				svaMigracionesUnit: unit,
				psprincipal: product.psprincipal,
				svaMigracionesCode: code,
				tvTech: tvTech?tvTech:"",
			};
		}



		let body = JSON.stringify(request);

		return this.http.post(this.baseUrl + '/add/sva', body)
			.map(res => res.json());
	}

}
