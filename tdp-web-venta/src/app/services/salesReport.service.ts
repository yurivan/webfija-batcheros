import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';

@Injectable()
export class SalesReportService {
	baseUrl = globals.ORDER_BASE_URL+"order/salesReport";

	constructor(private http: HttpService){
		this.http = http;
	}

	getData(orderId: string){
		let body = JSON.stringify({orderId:orderId});

		return this.http.post(this.baseUrl+'/detail', body)
			.map(res => res.json());
	}
}
