import { Injectable } from '@angular/core';
import { Http,Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import { Customer } from '../model/customer';
import * as globals from './globals';
import 'rxjs/Rx';

@Injectable()
export class ScoringService {
	baseUrl = globals.CUSTOMER_BASE_URL + "customer";

	constructor(private http: HttpService){
		this.http = http;
	}
	
	getData(customer : Customer, codigoAtis : string, departamento : string, provincia : string, distrito : string, ubigeo : string, canal : string) {
		//let data = '{"documentType":'+ '"' +customer.documentType+ '","documentNumber":'+ '"' +customer.documentNumber+'"}';
		let data = '{"codVendedor":"' + codigoAtis + '","departamento":"' + departamento + '","distrito":"' + distrito + 
			'","numeroDeDocumento":"' + customer.documentNumber + '","provincia":"' + provincia + 
			'","tipoDeDocumento":"' + customer.documentType + '","ubigeo":"' + ubigeo + '","zonal":"","canal":"'+ canal +'"}';
		return this.http.post(this.baseUrl + '/scoring', data).map(res => res.json());
	}
}
