import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';

@Injectable()
export class OutDetalleService {
	baseUrl = globals.ORDER_BASE_URL + "outcall";

	constructor(private http: HttpService) {
		this.http = http;
	}
// Compromando FIX
	getData(orderId: string) {
		let body = JSON.stringify({ order_id: orderId });

		return this.http.post(this.baseUrl + '/detalleVentaArchivo', body)
			.map(res => res.json());
	}



	setAprobado(order_id: string, user_id: string, name: string, canal_atis: string, extension: string, direccion: string, referencia: string) {
		let body = JSON.stringify({ 
			order_id: order_id, 
			user_id: user_id, 
			name: name, 
			canal_atis: canal_atis, 
			extension: extension,
			address_direccion: direccion,
			address_referencia: referencia
		});

		return this.http.post(this.baseUrl + '/aprobar', body)
			.map(res => res.json());
	}

	setCaida(order_id: string, user_id: string, name: string, canal_atis: string, extension: string, motivo_estado:string) {
		let body = JSON.stringify({ order_id: order_id, user_id: user_id, name: name, canal_atis: canal_atis, extension: extension,motivo_estado:motivo_estado });

		return this.http.post(this.baseUrl + '/caida', body)
			.map(res => res.json());
	}


}