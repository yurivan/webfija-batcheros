'use strict';

//export var BASE_URL = 'https://api.us.apiconnect.ibmcloud.com/telefonicaperu-developers/sb/';
//export var CLIENT_ID = '322cfbf5-772f-4b03-8b99-a5d2c38a39e6';

/*
export var BASE_URL = 'https://api.us.apiconnect.ibmcloud.com/everis-peru-production/sb/';
export var CLIENT_ID = 'a4407586-274b-4da5-b875-eb74efda4238';
*/

// Testing
export var BASE_URL = 'https://tdp-rest-backend-fija.mybluemix.net/';
export var USER_BASE_URL = 'https://tdp-rest-backend-fija.mybluemix.net/';
export var ORDER_BASE_URL = 'https://tdp-rest-backend-fija.mybluemix.net/';
export var PRODUCT_BASE_URL = 'https://tdp-rest-backend-fija.mybluemix.net/';
export var CUSTOMER_BASE_URL = 'https://tdp-rest-backend-fija.mybluemix.net/';
/*
//Local --> No cambiar el puerto 8080, se mantiene ese puerto
 export var BASE_URL = 'http://localhost:8080/';
 export var USER_BASE_URL = 'http://localhost:8080/';
 export var ORDER_BASE_URL = 'http://localhost:8080/';
 export var PRODUCT_BASE_URL = 'http://localhost:8080/';
 export var CUSTOMER_BASE_URL = 'http://localhost:8080/';
*/
export var CLIENT_ID = '322cfbf5-772f-4b03-8b99-a5d2c38a39e6';
	
/*
if (process.env.NODE_ENV === 'production') {s
    BASE_URL = 'https://api.us.apiconnect.ibmcloud.com/everis-peru-production/sb/';
    CLIENT_ID = 'a4407586-274b-4da5-b875-eb74efda4238';
} else if (process.env.NODE_ENV === 'piloto') {
    BASE_URL = 'https://api.us.apiconnect.ibmcloud.com/telefonicaperu-des/sb/';
    CLIENT_ID = 'd16e169c-b698-427a-967d-6bd49a137fb8';
}
*/

export var VERSION = '1.0';
export var ORDER_ALTA_NUEVA = 'A';
export var ORDER_MEJORAR_EXPERIENCIA = 'M';
export var ORDER_SVA = 'S';
export var PANEL_FLAG=6;
export var DIAS_VENTAS=30;
export var DEFAULT_MAX_DECOS=0;
export var DEFAULT_NUM_DECOS=0;
export var DEFAULT_MAX_DECOS_FLOOR=0;
export var RENIEC_VALID_ATTEMPTS=3;
export var RENIEC_MAX_ERRORS=0;
export var SOURCE = 'WEBVF';
export var ERROR_MESSAGE = 'Problema en comunicación con servicios';
// Sprint 7... variables para los tipos de documento a mostrar
export var SHOW_DNI = true;//por defecto true
export var SHOW_CEX = false;//por defecto false
export var SHOW_PAS = false;//por defecto false
export var SHOW_RUC = false;//por defecto false
export var SHOW_OTROSCE = false;

export var COUNTRIES_LIST = null;//para almacenar los nombres de los paises

export function setCountries(list){
  COUNTRIES_LIST = list;
}

export function parameter(list){
    list.forEach((item, index) => {
        if(item.element=='PANEL_FLAG'){
            PANEL_FLAG=item.strValue;
        }
        if(item.element=='DIAS_VENTAS'){
            DIAS_VENTAS= item.strValue;
        }

        if(item.element=='DEFAULT_MAX_DECOS'){
            DEFAULT_MAX_DECOS= item.strValue;
        }

        if(item.element=='RENIEC_VALID_ATTEMPTS'){
            RENIEC_VALID_ATTEMPTS= item.strValue;
        }
        if(item.element=='RENIEC_MAX_ERRORS'){
            RENIEC_MAX_ERRORS= item.strValue;
        }

		//Sprint 6 - tiempo expiracion de la sesion ahora es configurable
		if(item.element=='min_expired_session'){
            SESSION_IDLE = 60 * item.strValue;
        }

		//Sprint 7 - tipos de documento configurables
		if(item.element=='DNI'){
			SHOW_DNI = (item.strValue == '2') ? true : false;
		}
		if(item.element=='CEX'){
			SHOW_CEX = (item.strValue == '2') ? true : false;
		}
		if(item.element=='PAS'){
			SHOW_PAS = (item.strValue == '2') ? true : false;
		}
		if(item.element=='RUC'){
			SHOW_RUC = (item.strValue == '2') ? true : false;
		}
    if(item.element=='Otros Extranjeros - Aut. SNM'){
			SHOW_OTROSCE = (item.strValue == '2') ? true : false;
		}
    });
}

export var PAYMENT_METHOD_SVA = 'Financiado';
export var CASH_PRICE_SVA = 0;
export var SESSION_IDLE=1800;
export var SESSION_TIMEOUT=10;
export var GOOGLE_MAPS_API_KEY = 'AIzaSyClK74hNsEW6_FwUMNE2IW3AfnlWeyT1xA';
export var CANCEL_SALE = {
  "reason": [
    {"id":1,"value": "Cliente desiste por no tener interés en productos ofrecidos"},
    {"id":2,"value": "Cliente desiste por precio"},
    {"id":3,"value": "Cliente no pudo esperar al cierre del proceso completo"},
    {"id":4,"value": "Otras razones"}
  ]
};
export var DEPARTMENTS_RENIEC = {
        "1": {
            "name": "Amazonas",
            "sku": "1"
          },
          "2": {
            "name": "Ancash",
            "sku": "2"
          },
          "3": {
            "name": "Apurímac",
            "sku": "3"
          },
          "4": {
            "name": "Arequipa",
            "sku": "4"
          },
          "5": {
            "name": "Ayacucho",
            "sku": "5"
          },
          "6": {
            "name": "Cajamarca",
            "sku": "6"
          },
          "24": {
            "name": "Callao",
            "sku": "24"
          },
          "7": {
            "name": "Cusco",
            "sku": "7"
          },
          "8": {
            "name": "Huancavelica",
            "sku": "8"
          },
          "9": {
            "name": "Huánuco",
            "sku": "9"
          },
        "10": {
            "name": "Ica",
            "sku": "10"
          },
          "11": {
            "name": "Junín",
            "sku": "11"
          },
          "13": {
            "name": "Lambayeque",
            "sku": "13"
          },
          "12": {
            "name": "Libertad  ",
            "sku": "12"
          },
          "14": {
            "name": "Lima",
            "sku": "14"
          },
          "15": {
            "name": "Loreto",
            "sku": "15"
          },
          "16": {
            "name": "Madre De Dios",
            "sku": "16"
          },
          "17": {
            "name": "Moquegua",
            "sku": "17"
          },
          "18": {
            "name": "Pasco",
            "sku": "18"
          },
          "19": {
            "name": "Piura",
            "sku": "19"
          },
          "20": {
            "name": "Puno",
            "sku": "20"
          },
          "21": {
            "name": "San Martín",
            "sku": "21"
          },
          "22": {
            "name": "Tacna",
            "sku": "22"
          },
          "23": {
            "name": "Tumbes",
            "sku": "23"
          },
          "25": {
            "name": "Ucayali",
            "sku": "25"
          }
        };
export var PROVINCES_RENIEC = {
        "1001": {
            "name": "Ica",
            "sku": "01",
            "skuDep": "10"
          },
          "1002": {
            "name": "Chincha",
            "sku": "02",
            "skuDep": "10"
          },
          "1003": {
            "name": "Nazca",
            "sku": "03",
            "skuDep": "10"
          },
          "1004": {
            "name": "Pisco",
            "sku": "04",
            "skuDep": "10"
          },
          "1005": {
            "name": "Palpa",
            "sku": "05",
            "skuDep": "10"
          },
          "1101": {
            "name": "Huancayo",
            "sku": "01",
            "skuDep": "11"
          },
          "1102": {
            "name": "Concepción",
            "sku": "02",
            "skuDep": "11"
          },
          "1103": {
            "name": "Jauja",
            "sku": "03",
            "skuDep": "11"
          },
          "1104": {
            "name": "Junín",
            "sku": "04",
            "skuDep": "11"
          },
          "1105": {
            "name": "Tarma",
            "sku": "05",
            "skuDep": "11"
          },
          "1106": {
            "name": "Yauli",
            "sku": "06",
            "skuDep": "11"
          },
          "1107": {
            "name": "Satipo",
            "sku": "07",
            "skuDep": "11"
          },
          "1108": {
            "name": "Chanchamayo",
            "sku": "08",
            "skuDep": "11"
          },
          "1109": {
            "name": "Chupaca",
            "sku": "09",
            "skuDep": "11"
          },
          "1201": {
            "name": "Trujillo",
            "sku": "01",
            "skuDep": "12"
          },
          "1202": {
            "name": "Bolívar",
            "sku": "02",
            "skuDep": "12"
          },
          "1203": {
            "name": "Sánchez Carrión",
            "sku": "03",
            "skuDep": "12"
          },
          "1204": {
            "name": "Otuzco",
            "sku": "04",
            "skuDep": "12"
          },
          "1205": {
            "name": "Pacasmayo",
            "sku": "05",
            "skuDep": "12"
          },
          "1206": {
            "name": "Pataz",
            "sku": "06",
            "skuDep": "12"
          },
          "1207": {
            "name": "Santiago De Chuco",
            "sku": "07",
            "skuDep": "12"
          },
          "1208": {
            "name": "Ascope",
            "sku": "08",
            "skuDep": "12"
          },
          "1209": {
            "name": "Chepén",
            "sku": "09",
            "skuDep": "12"
          },
          "1210": {
            "name": "Julcán",
            "sku": "10",
            "skuDep": "12"
          },
          "1211": {
            "name": "Gran Chimú",
            "sku": "11",
            "skuDep": "12"
          },
          "1212": {
            "name": "Virú",
            "sku": "12",
            "skuDep": "12"
          },
          "1301": {
            "name": "Chiclayo",
            "sku": "01",
            "skuDep": "13"
          },
          "1302": {
            "name": "Ferreñafe",
            "sku": "02",
            "skuDep": "13"
          },
          "1303": {
            "name": "Lambayeque",
            "sku": "03",
            "skuDep": "13"
          },
          "1401": {
            "name": "Lima",
            "sku": "01",
            "skuDep": "14"
          },
          "1402": {
            "name": "Cajatambo",
            "sku": "02",
            "skuDep": "14"
          },
          "1403": {
            "name": "Canta",
            "sku": "03",
            "skuDep": "14"
          },
          "1404": {
            "name": "Cañete",
            "sku": "04",
            "skuDep": "14"
          },
          "1405": {
            "name": "Huaura",
            "sku": "05",
            "skuDep": "14"
          },
          "1406": {
            "name": "Huarochirí",
            "sku": "06",
            "skuDep": "14"
          },
          "1407": {
            "name": "Yauyos",
            "sku": "07",
            "skuDep": "14"
          },
          "1408": {
            "name": "Huaral",
            "sku": "08",
            "skuDep": "14"
          },
          "1409": {
            "name": "Barranca",
            "sku": "09",
            "skuDep": "14"
          },
          "1410": {
            "name": "Oyón",
            "sku": "10",
            "skuDep": "14"
          },
          "1501": {
            "name": "Maynas",
            "sku": "01",
            "skuDep": "15"
          },
          "1502": {
            "name": "Alto Amazonas",
            "sku": "02",
            "skuDep": "15"
          },
          "1503": {
            "name": "Loreto",
            "sku": "03",
            "skuDep": "15"
          },
          "1504": {
            "name": "Requena",
            "sku": "04",
            "skuDep": "15"
          },
          "1505": {
            "name": "Ucayali",
            "sku": "05",
            "skuDep": "15"
          },
          "1506": {
            "name": "Mariscal Ramón Castilla",
            "sku": "06",
            "skuDep": "15"
          },
          "1507": {
            "name": "Datem Del Marañón",
            "sku": "07",
            "skuDep": "15"
          },
          "1601": {
            "name": "Tambopata",
            "sku": "01",
            "skuDep": "16"
          },
          "1602": {
            "name": "Manu",
            "sku": "02",
            "skuDep": "16"
          },
          "1603": {
            "name": "Tahuamanu",
            "sku": "03",
            "skuDep": "16"
          },
          "1701": {
            "name": "Mariscal Nieto",
            "sku": "01",
            "skuDep": "17"
          },
          "1702": {
            "name": "General Sánchez Cerro",
            "sku": "02",
            "skuDep": "17"
          },
          "1703": {
            "name": "Ilo",
            "sku": "03",
            "skuDep": "17"
          },
          "1801": {
            "name": "Pasco",
            "sku": "01",
            "skuDep": "18"
          },
          "1802": {
            "name": "Daniel Alcides Carrión",
            "sku": "02",
            "skuDep": "18"
          },
          "1803": {
            "name": "Oxapampa",
            "sku": "03",
            "skuDep": "18"
          },
          "1901": {
            "name": "Piura",
            "sku": "01",
            "skuDep": "19"
          },
          "1902": {
            "name": "Ayabaca",
            "sku": "02",
            "skuDep": "19"
          },
          "1903": {
            "name": "Huancabamba",
            "sku": "03",
            "skuDep": "19"
          },
          "1904": {
            "name": "Morropón",
            "sku": "04",
            "skuDep": "19"
          },
          "1905": {
            "name": "Paita",
            "sku": "05",
            "skuDep": "19"
          },
          "1906": {
            "name": "Sullana",
            "sku": "06",
            "skuDep": "19"
          },
          "1907": {
            "name": "Talara",
            "sku": "07",
            "skuDep": "19"
          },
          "1908": {
            "name": "Sechura",
            "sku": "08",
            "skuDep": "19"
          },
          "2001": {
            "name": "Puno",
            "sku": "01",
            "skuDep": "20"
          },
          "2002": {
            "name": "Azángaro",
            "sku": "02",
            "skuDep": "20"
          },
          "2003": {
            "name": "Carabaya",
            "sku": "03",
            "skuDep": "20"
          },
          "2004": {
            "name": "Chucuito",
            "sku": "04",
            "skuDep": "20"
          },
          "2005": {
            "name": "Huancané",
            "sku": "05",
            "skuDep": "20"
          },
          "2006": {
            "name": "Lampa",
            "sku": "06",
            "skuDep": "20"
          },
          "2007": {
            "name": "Melgar",
            "sku": "07",
            "skuDep": "20"
          },
          "2008": {
            "name": "Sandia",
            "sku": "08",
            "skuDep": "20"
          },
          "2009": {
            "name": "San Roman",
            "sku": "09",
            "skuDep": "20"
          },
          "2010": {
            "name": "Yunguyo",
            "sku": "10",
            "skuDep": "20"
          },
          "2011": {
            "name": "San Antonio De Putina",
            "sku": "11",
            "skuDep": "20"
          },
          "2012": {
            "name": "El Collao",
            "sku": "12",
            "skuDep": "20"
          },
          "2013": {
            "name": "Moho",
            "sku": "13",
            "skuDep": "20"
          },
          "2101": {
            "name": "Moyobamba",
            "sku": "01",
            "skuDep": "21"
          },
          "2102": {
            "name": "Huallaga",
            "sku": "02",
            "skuDep": "21"
          },
          "2103": {
            "name": "Lamas",
            "sku": "03",
            "skuDep": "21"
          },
          "2104": {
            "name": "Mariscal Caceres",
            "sku": "04",
            "skuDep": "21"
          },
          "2105": {
            "name": "Rioja",
            "sku": "05",
            "skuDep": "21"
          },
          "2106": {
            "name": "San Martín",
            "sku": "06",
            "skuDep": "21"
          },
          "2107": {
            "name": "Bellavista",
            "sku": "07",
            "skuDep": "21"
          },
          "2108": {
            "name": "Tocache",
            "sku": "08",
            "skuDep": "21"
          },
          "2109": {
            "name": "Picota",
            "sku": "09",
            "skuDep": "21"
          },
          "2110": {
            "name": "El Dorado",
            "sku": "10",
            "skuDep": "21"
          },
          "2201": {
            "name": "Tacna",
            "sku": "01",
            "skuDep": "22"
          },
          "2202": {
            "name": "Tarata",
            "sku": "02",
            "skuDep": "22"
          },
          "2203": {
            "name": "Jorge Basadre",
            "sku": "03",
            "skuDep": "22"
          },
          "2204": {
            "name": "Candarave",
            "sku": "04",
            "skuDep": "22"
          },
          "2301": {
            "name": "Tumbes",
            "sku": "01",
            "skuDep": "23"
          },
          "2302": {
            "name": "Contralmirante Villar",
            "sku": "02",
            "skuDep": "23"
          },
          "2303": {
            "name": "Zarumilla",
            "sku": "03",
            "skuDep": "23"
          },
          "2401": {
            "name": "Callao",
            "sku": "01",
            "skuDep": "24"
          },
          "2501": {
            "name": "Coronel Portillo",
            "sku": "01",
            "skuDep": "25"
          },
          "2502": {
            "name": "Padre Abad",
            "sku": "02",
            "skuDep": "25"
          },
          "2503": {
            "name": "Atalaya",
            "sku": "03",
            "skuDep": "25"
          },
          "2504": {
            "name": "Purus",
            "sku": "04",
            "skuDep": "25"
          },
          "0101": {
            "name": "Chachapoyas",
            "sku": "01",
            "skuDep": "01"
          },
          "0102": {
            "name": "Bagua",
            "sku": "02",
            "skuDep": "01"
          },
          "0103": {
            "name": "Bongara",
            "sku": "03",
            "skuDep": "01"
          },
          "0104": {
            "name": "Luya",
            "sku": "04",
            "skuDep": "01"
          },
          "0105": {
            "name": "Rodriguez De Mendoza",
            "sku": "05",
            "skuDep": "01"
          },
          "0106": {
            "name": "Condorcanqui",
            "sku": "06",
            "skuDep": "01"
          },
          "0107": {
            "name": "Utcubamba",
            "sku": "07",
            "skuDep": "01"
          },
          "0201": {
            "name": "Huaraz",
            "sku": "01",
            "skuDep": "02"
          },
          "0202": {
            "name": "Aija",
            "sku": "02",
            "skuDep": "02"
          },
          "0203": {
            "name": "Bolognesi",
            "sku": "03",
            "skuDep": "02"
          },
          "0204": {
            "name": "Carhuaz",
            "sku": "04",
            "skuDep": "02"
          },
          "0205": {
            "name": "Casma",
            "sku": "05",
            "skuDep": "02"
          },
          "0206": {
            "name": "Corongo",
            "sku": "06",
            "skuDep": "02"
          },
          "0207": {
            "name": "Huaylas",
            "sku": "07",
            "skuDep": "02"
          },
          "0208": {
            "name": "Huari",
            "sku": "08",
            "skuDep": "02"
          },
          "0209": {
            "name": "Mariscal Luzuriaga",
            "sku": "09",
            "skuDep": "02"
          },
          "0210": {
            "name": "Pallasca",
            "sku": "10",
            "skuDep": "02"
          },
          "0211": {
            "name": "Pomabamba",
            "sku": "11",
            "skuDep": "02"
          },
          "0212": {
            "name": "Recuay",
            "sku": "12",
            "skuDep": "02"
          },
          "0213": {
            "name": "Santa",
            "sku": "13",
            "skuDep": "02"
          },
          "0214": {
            "name": "Sihuas",
            "sku": "14",
            "skuDep": "02"
          },
          "0215": {
            "name": "Yungay",
            "sku": "15",
            "skuDep": "02"
          },
          "0216": {
            "name": "Antonio Raimondi",
            "sku": "16",
            "skuDep": "02"
          },
          "0217": {
            "name": "Carlos Fermin Fitzcarrald",
            "sku": "17",
            "skuDep": "02"
          },
          "0218": {
            "name": "Asuncion",
            "sku": "18",
            "skuDep": "02"
          },
          "0219": {
            "name": "Huarmey",
            "sku": "19",
            "skuDep": "02"
          },
          "0220": {
            "name": "Ocros",
            "sku": "20",
            "skuDep": "02"
          },
          "0301": {
            "name": "Abancay",
            "sku": "01",
            "skuDep": "03"
          },
          "0302": {
            "name": "Aymaraes",
            "sku": "02",
            "skuDep": "03"
          },
          "0303": {
            "name": "Andahuaylas",
            "sku": "03",
            "skuDep": "03"
          },
          "0304": {
            "name": "Antabamba",
            "sku": "04",
            "skuDep": "03"
          },
          "0305": {
            "name": "Cotabambas",
            "sku": "05",
            "skuDep": "03"
          },
          "0306": {
            "name": "Grau",
            "sku": "06",
            "skuDep": "03"
          },
          "0307": {
            "name": "Chincheros",
            "sku": "07",
            "skuDep": "03"
          },
          "0401": {
            "name": "Arequipa",
            "sku": "01",
            "skuDep": "04"
          },
          "0402": {
            "name": "Caylloma",
            "sku": "02",
            "skuDep": "04"
          },
          "0403": {
            "name": "Camana",
            "sku": "03",
            "skuDep": "04"
          },
          "0404": {
            "name": "Caraveli",
            "sku": "04",
            "skuDep": "04"
          },
          "0405": {
            "name": "Castilla",
            "sku": "05",
            "skuDep": "04"
          },
          "0406": {
            "name": "Condesuyos",
            "sku": "06",
            "skuDep": "04"
          },
          "0407": {
            "name": "Islay",
            "sku": "07",
            "skuDep": "04"
          },
          "0408": {
            "name": "La Union",
            "sku": "08",
            "skuDep": "04"
          },
          "0501": {
            "name": "Huamanga",
            "sku": "01",
            "skuDep": "05"
          },
          "0502": {
            "name": "Cangallo",
            "sku": "02",
            "skuDep": "05"
          },
          "0503": {
            "name": "Huanta",
            "sku": "03",
            "skuDep": "05"
          },
          "0504": {
            "name": "La Mar",
            "sku": "04",
            "skuDep": "05"
          },
          "0505": {
            "name": "Lucanas",
            "sku": "05",
            "skuDep": "05"
          },
          "0506": {
            "name": "Parinacochas",
            "sku": "06",
            "skuDep": "05"
          },
          "0507": {
            "name": "Victor Fajardo",
            "sku": "07",
            "skuDep": "05"
          },
          "0508": {
            "name": "Huanca Sancos",
            "sku": "08",
            "skuDep": "05"
          },
          "0509": {
            "name": "Vilcas Huaman",
            "sku": "09",
            "skuDep": "05"
          },
          "0510": {
            "name": "Paucar Del Sara Sara",
            "sku": "10",
            "skuDep": "05"
          },
          "0511": {
            "name": "Sucre",
            "sku": "11",
            "skuDep": "05"
          },
          "0601": {
            "name": "Cajamarca",
            "sku": "01",
            "skuDep": "06"
          },
          "0602": {
            "name": "Cajabamba",
            "sku": "02",
            "skuDep": "06"
          },
          "0603": {
            "name": "Celendin",
            "sku": "03",
            "skuDep": "06"
          },
          "0604": {
            "name": "Contumaza",
            "sku": "04",
            "skuDep": "06"
          },
          "0605": {
            "name": "Cutervo",
            "sku": "05",
            "skuDep": "06"
          },
          "0606": {
            "name": "Chota",
            "sku": "06",
            "skuDep": "06"
          },
          "0607": {
            "name": "Hualgayoc",
            "sku": "07",
            "skuDep": "06"
          },
          "0608": {
            "name": "Jaen",
            "sku": "08",
            "skuDep": "06"
          },
          "0609": {
            "name": "Santa Cruz",
            "sku": "09",
            "skuDep": "06"
          },
          "0610": {
            "name": "San Miguel",
            "sku": "10",
            "skuDep": "06"
          },
          "0611": {
            "name": "San Ignacio",
            "sku": "11",
            "skuDep": "06"
          },
          "0612": {
            "name": "San Marcos",
            "sku": "12",
            "skuDep": "06"
          },
          "0613": {
            "name": "San Pablo",
            "sku": "13",
            "skuDep": "06"
          },
          "0701": {
            "name": "Cusco",
            "sku": "01",
            "skuDep": "07"
          },
          "0702": {
            "name": "Acomayo",
            "sku": "02",
            "skuDep": "07"
          },
          "0703": {
            "name": "Anta",
            "sku": "03",
            "skuDep": "07"
          },
          "0704": {
            "name": "Calca",
            "sku": "04",
            "skuDep": "07"
          },
          "0705": {
            "name": "Canas",
            "sku": "05",
            "skuDep": "07"
          },
          "0706": {
            "name": "Canchis",
            "sku": "06",
            "skuDep": "07"
          },
          "0707": {
            "name": "Chumbivilcas",
            "sku": "07",
            "skuDep": "07"
          },
          "0708": {
            "name": "Espinar",
            "sku": "08",
            "skuDep": "07"
          },
          "0709": {
            "name": "La Convencion",
            "sku": "09",
            "skuDep": "07"
          },
          "0710": {
            "name": "Paruro",
            "sku": "10",
            "skuDep": "07"
          },
          "0711": {
            "name": "Paucartambo",
            "sku": "11",
            "skuDep": "07"
          },
          "0712": {
            "name": "Quispicanchi",
            "sku": "12",
            "skuDep": "07"
          },
          "0713": {
            "name": "Urubamba",
            "sku": "13",
            "skuDep": "07"
          },
          "0801": {
            "name": "Huancavelica",
            "sku": "01",
            "skuDep": "08"
          },
          "0802": {
            "name": "Acobamba",
            "sku": "02",
            "skuDep": "08"
          },
          "0803": {
            "name": "Angaraes",
            "sku": "03",
            "skuDep": "08"
          },
          "0804": {
            "name": "Castrovirreyna",
            "sku": "04",
            "skuDep": "08"
          },
          "0805": {
            "name": "Tayacaja",
            "sku": "05",
            "skuDep": "08"
          },
          "0806": {
            "name": "Huaytara",
            "sku": "06",
            "skuDep": "08"
          },
          "0807": {
            "name": "Churcampa",
            "sku": "07",
            "skuDep": "08"
          },
          "0901": {
            "name": "Huánuco",
            "sku": "01",
            "skuDep": "09"
          },
          "0902": {
            "name": "Ambo",
            "sku": "02",
            "skuDep": "09"
          },
          "0903": {
            "name": "Dos De Mayo",
            "sku": "03",
            "skuDep": "09"
          },
          "0904": {
            "name": "Huamalies",
            "sku": "04",
            "skuDep": "09"
          },
          "0905": {
            "name": "Marañon",
            "sku": "05",
            "skuDep": "09"
          },
          "0906": {
            "name": "Leoncio Prado",
            "sku": "06",
            "skuDep": "09"
          },
          "0907": {
            "name": "Pachitea",
            "sku": "07",
            "skuDep": "09"
          },
          "0908": {
            "name": "Puerto Inca",
            "sku": "08",
            "skuDep": "09"
          },
          "0909": {
            "name": "Huacaybamba",
            "sku": "09",
            "skuDep": "09"
          },
          "0910": {
            "name": "Lauricocha",
            "sku": "10",
            "skuDep": "09"
          },
          "0911": {
            "name": "Yarowilca",
            "sku": "11",
            "skuDep": "09"
          }
        };
export var DISTRICTS_RENIEC = {
        "100101": {
            "SkuDepPro": "1001",
            "name": "Ica",
            "sku": "01"
          },
          "100102": {
            "SkuDepPro": "1001",
            "name": "La Tinguiña",
            "sku": "02"
          },
          "100103": {
            "SkuDepPro": "1001",
            "name": "Los Aquijes",
            "sku": "03"
          },
          "100104": {
            "SkuDepPro": "1001",
            "name": "Parcona",
            "sku": "04"
          },
          "100105": {
            "SkuDepPro": "1001",
            "name": "Pueblo Nuevo",
            "sku": "05"
          },
          "100106": {
            "SkuDepPro": "1001",
            "name": "Salas",
            "sku": "06"
          },
          "100107": {
            "SkuDepPro": "1001",
            "name": "San Jose De Los Molinos",
            "sku": "07"
          },
          "100108": {
            "SkuDepPro": "1001",
            "name": "San Juan Bautista",
            "sku": "08"
          },
          "100109": {
            "SkuDepPro": "1001",
            "name": "Santiago",
            "sku": "09"
          },
          "100110": {
            "SkuDepPro": "1001",
            "name": "Subtanjalla",
            "sku": "10"
          },
          "100111": {
            "SkuDepPro": "1001",
            "name": "Yauca Del Rosario",
            "sku": "11"
          },
          "100112": {
            "SkuDepPro": "1001",
            "name": "Tate",
            "sku": "12"
          },
          "100113": {
            "SkuDepPro": "1001",
            "name": "Pachacutec",
            "sku": "13"
          },
          "100114": {
            "SkuDepPro": "1001",
            "name": "Ocucaje",
            "sku": "14"
          },
          "100201": {
            "SkuDepPro": "1002",
            "name": "Chincha Alta",
            "sku": "01"
          },
          "100202": {
            "SkuDepPro": "1002",
            "name": "Chavin",
            "sku": "02"
          },
          "100203": {
            "SkuDepPro": "1002",
            "name": "Chincha Baja",
            "sku": "03"
          },
          "100204": {
            "SkuDepPro": "1002",
            "name": "El Carmen",
            "sku": "04"
          },
          "100205": {
            "SkuDepPro": "1002",
            "name": "Grocio Prado",
            "sku": "05"
          },
          "100206": {
            "SkuDepPro": "1002",
            "name": "San Pedro De Huacarpana",
            "sku": "06"
          },
          "100207": {
            "SkuDepPro": "1002",
            "name": "Sunampe",
            "sku": "07"
          },
          "100208": {
            "SkuDepPro": "1002",
            "name": "Tambo De Mora",
            "sku": "08"
          },
          "100209": {
            "SkuDepPro": "1002",
            "name": "Alto Laran",
            "sku": "09"
          },
          "100210": {
            "SkuDepPro": "1002",
            "name": "Pueblo Nuevo",
            "sku": "10"
          },
          "100211": {
            "SkuDepPro": "1002",
            "name": "San Juan De Yanac",
            "sku": "11"
          },
          "100301": {
            "SkuDepPro": "1003",
            "name": "Nazca",
            "sku": "01"
          },
          "100302": {
            "SkuDepPro": "1003",
            "name": "Changuillo",
            "sku": "02"
          },
          "100303": {
            "SkuDepPro": "1003",
            "name": "El Ingenio",
            "sku": "03"
          },
          "100304": {
            "SkuDepPro": "1003",
            "name": "Marcona",
            "sku": "04"
          },
          "100305": {
            "SkuDepPro": "1003",
            "name": "Vista Alegre",
            "sku": "05"
          },
          "100401": {
            "SkuDepPro": "1004",
            "name": "Pisco",
            "sku": "01"
          },
          "100402": {
            "SkuDepPro": "1004",
            "name": "Huancano",
            "sku": "02"
          },
          "100403": {
            "SkuDepPro": "1004",
            "name": "Humay",
            "sku": "03"
          },
          "100404": {
            "SkuDepPro": "1004",
            "name": "Independencia",
            "sku": "04"
          },
          "100405": {
            "SkuDepPro": "1004",
            "name": "Paracas",
            "sku": "05"
          },
          "100406": {
            "SkuDepPro": "1004",
            "name": "San Andres",
            "sku": "06"
          },
          "100407": {
            "SkuDepPro": "1004",
            "name": "San Clemente",
            "sku": "07"
          },
          "100408": {
            "SkuDepPro": "1004",
            "name": "Tupac Amaru Inca",
            "sku": "08"
          },
          "100501": {
            "SkuDepPro": "1005",
            "name": "Palpa",
            "sku": "01"
          },
          "100502": {
            "SkuDepPro": "1005",
            "name": "Llipata",
            "sku": "02"
          },
          "100503": {
            "SkuDepPro": "1005",
            "name": "Rio Grande",
            "sku": "03"
          },
          "100504": {
            "SkuDepPro": "1005",
            "name": "Santa Cruz",
            "sku": "04"
          },
          "100505": {
            "SkuDepPro": "1005",
            "name": "Tibillo",
            "sku": "05"
          },
          "110101": {
            "SkuDepPro": "1101",
            "name": "Huancayo",
            "sku": "01"
          },
          "110103": {
            "SkuDepPro": "1101",
            "name": "Carhuacallanga",
            "sku": "03"
          },
          "110104": {
            "SkuDepPro": "1101",
            "name": "Colca",
            "sku": "04"
          },
          "110105": {
            "SkuDepPro": "1101",
            "name": "Cullhuas",
            "sku": "05"
          },
          "110106": {
            "SkuDepPro": "1101",
            "name": "Chacapampa",
            "sku": "06"
          },
          "110107": {
            "SkuDepPro": "1101",
            "name": "Chicche",
            "sku": "07"
          },
          "110108": {
            "SkuDepPro": "1101",
            "name": "Chilca",
            "sku": "08"
          },
          "110109": {
            "SkuDepPro": "1101",
            "name": "Chongos Alto",
            "sku": "09"
          },
          "110112": {
            "SkuDepPro": "1101",
            "name": "Chupuro",
            "sku": "12"
          },
          "110113": {
            "SkuDepPro": "1101",
            "name": "El Tambo",
            "sku": "13"
          },
          "110114": {
            "SkuDepPro": "1101",
            "name": "Huacrapuquio",
            "sku": "14"
          },
          "110116": {
            "SkuDepPro": "1101",
            "name": "Hualhuas",
            "sku": "16"
          },
          "110118": {
            "SkuDepPro": "1101",
            "name": "Huancan",
            "sku": "18"
          },
          "110119": {
            "SkuDepPro": "1101",
            "name": "Huasicancha",
            "sku": "19"
          },
          "110120": {
            "SkuDepPro": "1101",
            "name": "Huayucachi",
            "sku": "20"
          },
          "110121": {
            "SkuDepPro": "1101",
            "name": "Ingenio",
            "sku": "21"
          },
          "110122": {
            "SkuDepPro": "1101",
            "name": "Pariahuanca",
            "sku": "22"
          },
          "110123": {
            "SkuDepPro": "1101",
            "name": "Pilcomayo",
            "sku": "23"
          },
          "110124": {
            "SkuDepPro": "1101",
            "name": "Pucara",
            "sku": "24"
          },
          "110125": {
            "SkuDepPro": "1101",
            "name": "Quichuay",
            "sku": "25"
          },
          "110126": {
            "SkuDepPro": "1101",
            "name": "Quilcas",
            "sku": "26"
          },
          "110127": {
            "SkuDepPro": "1101",
            "name": "San Agustin",
            "sku": "27"
          },
          "110128": {
            "SkuDepPro": "1101",
            "name": "San Jeronimo De Tunan",
            "sku": "28"
          },
          "110131": {
            "SkuDepPro": "1101",
            "name": "Santo Domingo De Acobamba",
            "sku": "31"
          },
          "110132": {
            "SkuDepPro": "1101",
            "name": "Saño",
            "sku": "32"
          },
          "110133": {
            "SkuDepPro": "1101",
            "name": "Sapallanga",
            "sku": "33"
          },
          "110134": {
            "SkuDepPro": "1101",
            "name": "Sicaya",
            "sku": "34"
          },
          "110136": {
            "SkuDepPro": "1101",
            "name": "Viques",
            "sku": "36"
          },
          "110201": {
            "SkuDepPro": "1102",
            "name": "Concepción",
            "sku": "01"
          },
          "110202": {
            "SkuDepPro": "1102",
            "name": "Aco",
            "sku": "02"
          },
          "110203": {
            "SkuDepPro": "1102",
            "name": "Andamarca",
            "sku": "03"
          },
          "110204": {
            "SkuDepPro": "1102",
            "name": "Comas",
            "sku": "04"
          },
          "110205": {
            "SkuDepPro": "1102",
            "name": "Cochas",
            "sku": "05"
          },
          "110206": {
            "SkuDepPro": "1102",
            "name": "Chambara",
            "sku": "06"
          },
          "110207": {
            "SkuDepPro": "1102",
            "name": "Heroinas Toledo",
            "sku": "07"
          },
          "110208": {
            "SkuDepPro": "1102",
            "name": "Manzanares",
            "sku": "08"
          },
          "110209": {
            "SkuDepPro": "1102",
            "name": "Mariscal Castilla",
            "sku": "09"
          },
          "110210": {
            "SkuDepPro": "1102",
            "name": "Matahuasi",
            "sku": "10"
          },
          "110211": {
            "SkuDepPro": "1102",
            "name": "Mito",
            "sku": "11"
          },
          "110212": {
            "SkuDepPro": "1102",
            "name": "Nueve De Julio",
            "sku": "12"
          },
          "110213": {
            "SkuDepPro": "1102",
            "name": "Orcotuna",
            "sku": "13"
          },
          "110214": {
            "SkuDepPro": "1102",
            "name": "Santa Rosa De Ocopa",
            "sku": "14"
          },
          "110215": {
            "SkuDepPro": "1102",
            "name": "San Jose De Quero",
            "sku": "15"
          },
          "110301": {
            "SkuDepPro": "1103",
            "name": "Jauja",
            "sku": "01"
          },
          "110302": {
            "SkuDepPro": "1103",
            "name": "Acolla",
            "sku": "02"
          },
          "110303": {
            "SkuDepPro": "1103",
            "name": "Apata",
            "sku": "03"
          },
          "110304": {
            "SkuDepPro": "1103",
            "name": "Ataura",
            "sku": "04"
          },
          "110305": {
            "SkuDepPro": "1103",
            "name": "Canchayllo",
            "sku": "05"
          },
          "110306": {
            "SkuDepPro": "1103",
            "name": "El Mantaro",
            "sku": "06"
          },
          "110307": {
            "SkuDepPro": "1103",
            "name": "Huamali",
            "sku": "07"
          },
          "110308": {
            "SkuDepPro": "1103",
            "name": "Huaripampa",
            "sku": "08"
          },
          "110309": {
            "SkuDepPro": "1103",
            "name": "Huertas",
            "sku": "09"
          },
          "110310": {
            "SkuDepPro": "1103",
            "name": "Janjaillo",
            "sku": "10"
          },
          "110311": {
            "SkuDepPro": "1103",
            "name": "Julcán",
            "sku": "11"
          },
          "110312": {
            "SkuDepPro": "1103",
            "name": "Leonor Ordoñez",
            "sku": "12"
          },
          "110313": {
            "SkuDepPro": "1103",
            "name": "Llocllapampa",
            "sku": "13"
          },
          "110314": {
            "SkuDepPro": "1103",
            "name": "Marco",
            "sku": "14"
          },
          "110315": {
            "SkuDepPro": "1103",
            "name": "Masma",
            "sku": "15"
          },
          "110316": {
            "SkuDepPro": "1103",
            "name": "Molinos",
            "sku": "16"
          },
          "110317": {
            "SkuDepPro": "1103",
            "name": "Monobamba",
            "sku": "17"
          },
          "110318": {
            "SkuDepPro": "1103",
            "name": "Muqui",
            "sku": "18"
          },
          "110319": {
            "SkuDepPro": "1103",
            "name": "Muquiyauyo",
            "sku": "19"
          },
          "110320": {
            "SkuDepPro": "1103",
            "name": "Paca",
            "sku": "20"
          },
          "110321": {
            "SkuDepPro": "1103",
            "name": "Paccha",
            "sku": "21"
          },
          "110322": {
            "SkuDepPro": "1103",
            "name": "Pancan",
            "sku": "22"
          },
          "110323": {
            "SkuDepPro": "1103",
            "name": "Parco",
            "sku": "23"
          },
          "110324": {
            "SkuDepPro": "1103",
            "name": "Pomacancha",
            "sku": "24"
          },
          "110325": {
            "SkuDepPro": "1103",
            "name": "Ricran",
            "sku": "25"
          },
          "110326": {
            "SkuDepPro": "1103",
            "name": "San Lorenzo",
            "sku": "26"
          },
          "110327": {
            "SkuDepPro": "1103",
            "name": "San Pedro De Chunan",
            "sku": "27"
          },
          "110328": {
            "SkuDepPro": "1103",
            "name": "Sincos",
            "sku": "28"
          },
          "110329": {
            "SkuDepPro": "1103",
            "name": "Tunan Marca",
            "sku": "29"
          },
          "110330": {
            "SkuDepPro": "1103",
            "name": "Yauli",
            "sku": "30"
          },
          "110331": {
            "SkuDepPro": "1103",
            "name": "Curicaca",
            "sku": "31"
          },
          "110332": {
            "SkuDepPro": "1103",
            "name": "Masma Chicche",
            "sku": "32"
          },
          "110333": {
            "SkuDepPro": "1103",
            "name": "Sausa",
            "sku": "33"
          },
          "110334": {
            "SkuDepPro": "1103",
            "name": "Yauyos",
            "sku": "34"
          },
          "110401": {
            "SkuDepPro": "1104",
            "name": "Junín",
            "sku": "01"
          },
          "110402": {
            "SkuDepPro": "1104",
            "name": "Carhuamayo",
            "sku": "02"
          },
          "110403": {
            "SkuDepPro": "1104",
            "name": "Ondores",
            "sku": "03"
          },
          "110404": {
            "SkuDepPro": "1104",
            "name": "Ulcumayo",
            "sku": "04"
          },
          "110501": {
            "SkuDepPro": "1105",
            "name": "Tarma",
            "sku": "01"
          },
          "110502": {
            "SkuDepPro": "1105",
            "name": "Acobamba",
            "sku": "02"
          },
          "110503": {
            "SkuDepPro": "1105",
            "name": "Huaricolca",
            "sku": "03"
          },
          "110504": {
            "SkuDepPro": "1105",
            "name": "Huasahuasi",
            "sku": "04"
          },
          "110505": {
            "SkuDepPro": "1105",
            "name": "La Union",
            "sku": "05"
          },
          "110506": {
            "SkuDepPro": "1105",
            "name": "Palca",
            "sku": "06"
          },
          "110507": {
            "SkuDepPro": "1105",
            "name": "Palcamayo",
            "sku": "07"
          },
          "110508": {
            "SkuDepPro": "1105",
            "name": "San Pedro De Cajas",
            "sku": "08"
          },
          "110509": {
            "SkuDepPro": "1105",
            "name": "Tapo",
            "sku": "09"
          },
          "110601": {
            "SkuDepPro": "1106",
            "name": "La Oroya",
            "sku": "01"
          },
          "110602": {
            "SkuDepPro": "1106",
            "name": "Chacapalpa",
            "sku": "02"
          },
          "110603": {
            "SkuDepPro": "1106",
            "name": "Huay Huay",
            "sku": "03"
          },
          "110604": {
            "SkuDepPro": "1106",
            "name": "Marcapomacocha",
            "sku": "04"
          },
          "110605": {
            "SkuDepPro": "1106",
            "name": "Morococha",
            "sku": "05"
          },
          "110606": {
            "SkuDepPro": "1106",
            "name": "Paccha",
            "sku": "06"
          },
          "110607": {
            "SkuDepPro": "1106",
            "name": "Santa Barbara De Carhuacayan",
            "sku": "07"
          },
          "110608": {
            "SkuDepPro": "1106",
            "name": "Suitucancha",
            "sku": "08"
          },
          "110609": {
            "SkuDepPro": "1106",
            "name": "Yauli",
            "sku": "09"
          },
          "110610": {
            "SkuDepPro": "1106",
            "name": "Santa Rosa De Sacco",
            "sku": "10"
          },
          "110701": {
            "SkuDepPro": "1107",
            "name": "Satipo",
            "sku": "01"
          },
          "110702": {
            "SkuDepPro": "1107",
            "name": "Coviriali",
            "sku": "02"
          },
          "110703": {
            "SkuDepPro": "1107",
            "name": "Llaylla",
            "sku": "03"
          },
          "110704": {
            "SkuDepPro": "1107",
            "name": "Mazamari",
            "sku": "04"
          },
          "110705": {
            "SkuDepPro": "1107",
            "name": "Pampa Hermosa",
            "sku": "05"
          },
          "110706": {
            "SkuDepPro": "1107",
            "name": "Pangoa",
            "sku": "06"
          },
          "110707": {
            "SkuDepPro": "1107",
            "name": "Rio Negro",
            "sku": "07"
          },
          "110708": {
            "SkuDepPro": "1107",
            "name": "Rio Tambo",
            "sku": "08"
          },
          "110801": {
            "SkuDepPro": "1108",
            "name": "Chanchamayo",
            "sku": "01"
          },
          "110802": {
            "SkuDepPro": "1108",
            "name": "San Ramon",
            "sku": "02"
          },
          "110803": {
            "SkuDepPro": "1108",
            "name": "Vitoc",
            "sku": "03"
          },
          "110804": {
            "SkuDepPro": "1108",
            "name": "San Luis De Shuaro",
            "sku": "04"
          },
          "110805": {
            "SkuDepPro": "1108",
            "name": "Pichanaqui",
            "sku": "05"
          },
          "110806": {
            "SkuDepPro": "1108",
            "name": "Perene",
            "sku": "06"
          },
          "110901": {
            "SkuDepPro": "1109",
            "name": "Chupaca",
            "sku": "01"
          },
          "110902": {
            "SkuDepPro": "1109",
            "name": "Ahuac",
            "sku": "02"
          },
          "110903": {
            "SkuDepPro": "1109",
            "name": "Chongos Bajo",
            "sku": "03"
          },
          "110904": {
            "SkuDepPro": "1109",
            "name": "Huachac",
            "sku": "04"
          },
          "110905": {
            "SkuDepPro": "1109",
            "name": "Huamancaca Chico",
            "sku": "05"
          },
          "110906": {
            "SkuDepPro": "1109",
            "name": "San Juan De Yscos",
            "sku": "06"
          },
          "110907": {
            "SkuDepPro": "1109",
            "name": "San Juan De Jarpa",
            "sku": "07"
          },
          "110908": {
            "SkuDepPro": "1109",
            "name": "Tres De Diciembre",
            "sku": "08"
          },
          "110909": {
            "SkuDepPro": "1109",
            "name": "Yanacancha",
            "sku": "09"
          },
          "120101": {
            "SkuDepPro": "1201",
            "name": "Trujillo",
            "sku": "01"
          },
          "120102": {
            "SkuDepPro": "1201",
            "name": "Huanchaco",
            "sku": "02"
          },
          "120103": {
            "SkuDepPro": "1201",
            "name": "Laredo",
            "sku": "03"
          },
          "120104": {
            "SkuDepPro": "1201",
            "name": "Moche",
            "sku": "04"
          },
          "120105": {
            "SkuDepPro": "1201",
            "name": "Salaverry",
            "sku": "05"
          },
          "120106": {
            "SkuDepPro": "1201",
            "name": "Simbal",
            "sku": "06"
          },
          "120107": {
            "SkuDepPro": "1201",
            "name": "Victor Larco Herrera",
            "sku": "07"
          },
          "120109": {
            "SkuDepPro": "1201",
            "name": "Poroto",
            "sku": "09"
          },
          "120110": {
            "SkuDepPro": "1201",
            "name": "El Porvenir",
            "sku": "10"
          },
          "120111": {
            "SkuDepPro": "1201",
            "name": "La Esperanza",
            "sku": "11"
          },
          "120112": {
            "SkuDepPro": "1201",
            "name": "Florencia De Mora",
            "sku": "12"
          },
          "120201": {
            "SkuDepPro": "1202",
            "name": "Bolívar",
            "sku": "01"
          },
          "120202": {
            "SkuDepPro": "1202",
            "name": "Bambamarca",
            "sku": "02"
          },
          "120203": {
            "SkuDepPro": "1202",
            "name": "Condormarca",
            "sku": "03"
          },
          "120204": {
            "SkuDepPro": "1202",
            "name": "Longotea",
            "sku": "04"
          },
          "120205": {
            "SkuDepPro": "1202",
            "name": "Ucuncha",
            "sku": "05"
          },
          "120206": {
            "SkuDepPro": "1202",
            "name": "Uchumarca",
            "sku": "06"
          },
          "120301": {
            "SkuDepPro": "1203",
            "name": "Huamachuco",
            "sku": "01"
          },
          "120302": {
            "SkuDepPro": "1203",
            "name": "Cochorco",
            "sku": "02"
          },
          "120303": {
            "SkuDepPro": "1203",
            "name": "Curgos",
            "sku": "03"
          },
          "120304": {
            "SkuDepPro": "1203",
            "name": "Chugay",
            "sku": "04"
          },
          "120305": {
            "SkuDepPro": "1203",
            "name": "Marcabal",
            "sku": "05"
          },
          "120306": {
            "SkuDepPro": "1203",
            "name": "Sanagoran",
            "sku": "06"
          },
          "120307": {
            "SkuDepPro": "1203",
            "name": "Sarin",
            "sku": "07"
          },
          "120308": {
            "SkuDepPro": "1203",
            "name": "Sartimbamba",
            "sku": "08"
          },
          "120401": {
            "SkuDepPro": "1204",
            "name": "Otuzco",
            "sku": "01"
          },
          "120402": {
            "SkuDepPro": "1204",
            "name": "Agallpampa",
            "sku": "02"
          },
          "120403": {
            "SkuDepPro": "1204",
            "name": "Charat",
            "sku": "03"
          },
          "120404": {
            "SkuDepPro": "1204",
            "name": "Huaranchal",
            "sku": "04"
          },
          "120405": {
            "SkuDepPro": "1204",
            "name": "La Cuesta",
            "sku": "05"
          },
          "120408": {
            "SkuDepPro": "1204",
            "name": "Paranday",
            "sku": "08"
          },
          "120409": {
            "SkuDepPro": "1204",
            "name": "Salpo",
            "sku": "09"
          },
          "120410": {
            "SkuDepPro": "1204",
            "name": "Sinsicap",
            "sku": "10"
          },
          "120411": {
            "SkuDepPro": "1204",
            "name": "Usquil",
            "sku": "11"
          },
          "120413": {
            "SkuDepPro": "1204",
            "name": "Mache",
            "sku": "13"
          },
          "120501": {
            "SkuDepPro": "1205",
            "name": "San Pedro De Lloc",
            "sku": "01"
          },
          "120503": {
            "SkuDepPro": "1205",
            "name": "Guadalupe",
            "sku": "03"
          },
          "120504": {
            "SkuDepPro": "1205",
            "name": "Jequetepeque",
            "sku": "04"
          },
          "120506": {
            "SkuDepPro": "1205",
            "name": "Pacasmayo",
            "sku": "06"
          },
          "120508": {
            "SkuDepPro": "1205",
            "name": "San Jose",
            "sku": "08"
          },
          "120601": {
            "SkuDepPro": "1206",
            "name": "Tayabamba",
            "sku": "01"
          },
          "120602": {
            "SkuDepPro": "1206",
            "name": "Buldibuyo",
            "sku": "02"
          },
          "120603": {
            "SkuDepPro": "1206",
            "name": "Chillia",
            "sku": "03"
          },
          "120604": {
            "SkuDepPro": "1206",
            "name": "Huaylillas",
            "sku": "04"
          },
          "120605": {
            "SkuDepPro": "1206",
            "name": "Huancaspata",
            "sku": "05"
          },
          "120606": {
            "SkuDepPro": "1206",
            "name": "Huayo",
            "sku": "06"
          },
          "120607": {
            "SkuDepPro": "1206",
            "name": "Ongon",
            "sku": "07"
          },
          "120608": {
            "SkuDepPro": "1206",
            "name": "Parcoy",
            "sku": "08"
          },
          "120609": {
            "SkuDepPro": "1206",
            "name": "Pataz",
            "sku": "09"
          },
          "120610": {
            "SkuDepPro": "1206",
            "name": "Pias",
            "sku": "10"
          },
          "120611": {
            "SkuDepPro": "1206",
            "name": "Taurija",
            "sku": "11"
          },
          "120612": {
            "SkuDepPro": "1206",
            "name": "Urpay",
            "sku": "12"
          },
          "120613": {
            "SkuDepPro": "1206",
            "name": "Santiago De Challas",
            "sku": "13"
          },
          "120701": {
            "SkuDepPro": "1207",
            "name": "Santiago De Chuco",
            "sku": "01"
          },
          "120702": {
            "SkuDepPro": "1207",
            "name": "Cachicadan",
            "sku": "02"
          },
          "120703": {
            "SkuDepPro": "1207",
            "name": "Mollebamba",
            "sku": "03"
          },
          "120704": {
            "SkuDepPro": "1207",
            "name": "Mollepata",
            "sku": "04"
          },
          "120705": {
            "SkuDepPro": "1207",
            "name": "Quiruvilca",
            "sku": "05"
          },
          "120706": {
            "SkuDepPro": "1207",
            "name": "Santa Cruz De Chuca",
            "sku": "06"
          },
          "120707": {
            "SkuDepPro": "1207",
            "name": "Sitabamba",
            "sku": "07"
          },
          "120708": {
            "SkuDepPro": "1207",
            "name": "Angasmarca",
            "sku": "08"
          },
          "120801": {
            "SkuDepPro": "1208",
            "name": "Ascope",
            "sku": "01"
          },
          "120802": {
            "SkuDepPro": "1208",
            "name": "Chicama",
            "sku": "02"
          },
          "120803": {
            "SkuDepPro": "1208",
            "name": "Chocope",
            "sku": "03"
          },
          "120804": {
            "SkuDepPro": "1208",
            "name": "Santiago De Cao",
            "sku": "04"
          },
          "120805": {
            "SkuDepPro": "1208",
            "name": "Magdalena De Cao",
            "sku": "05"
          },
          "120806": {
            "SkuDepPro": "1208",
            "name": "Paijan",
            "sku": "06"
          },
          "120807": {
            "SkuDepPro": "1208",
            "name": "Razuri",
            "sku": "07"
          },
          "120808": {
            "SkuDepPro": "1208",
            "name": "Casa Grande",
            "sku": "08"
          },
          "120901": {
            "SkuDepPro": "1209",
            "name": "Chepén",
            "sku": "01"
          },
          "120902": {
            "SkuDepPro": "1209",
            "name": "Pacanga",
            "sku": "02"
          },
          "120903": {
            "SkuDepPro": "1209",
            "name": "Pueblo Nuevo",
            "sku": "03"
          },
          "121001": {
            "SkuDepPro": "1210",
            "name": "Julcán",
            "sku": "01"
          },
          "121002": {
            "SkuDepPro": "1210",
            "name": "Carabamba",
            "sku": "02"
          },
          "121003": {
            "SkuDepPro": "1210",
            "name": "Calamarca",
            "sku": "03"
          },
          "121004": {
            "SkuDepPro": "1210",
            "name": "Huaso",
            "sku": "04"
          },
          "121101": {
            "SkuDepPro": "1211",
            "name": "Cascas",
            "sku": "01"
          },
          "121102": {
            "SkuDepPro": "1211",
            "name": "Lucma",
            "sku": "02"
          },
          "121103": {
            "SkuDepPro": "1211",
            "name": "Marmot",
            "sku": "03"
          },
          "121104": {
            "SkuDepPro": "1211",
            "name": "Sayapullo",
            "sku": "04"
          },
          "121201": {
            "SkuDepPro": "1212",
            "name": "Virú",
            "sku": "01"
          },
          "121202": {
            "SkuDepPro": "1212",
            "name": "Chao",
            "sku": "02"
          },
          "121203": {
            "SkuDepPro": "1212",
            "name": "Guadalupito",
            "sku": "03"
          },
          "130101": {
            "SkuDepPro": "1301",
            "name": "Chiclayo",
            "sku": "01"
          },
          "130102": {
            "SkuDepPro": "1301",
            "name": "Chongoyape",
            "sku": "02"
          },
          "130103": {
            "SkuDepPro": "1301",
            "name": "Eten",
            "sku": "03"
          },
          "130104": {
            "SkuDepPro": "1301",
            "name": "Eten Puerto",
            "sku": "04"
          },
          "130105": {
            "SkuDepPro": "1301",
            "name": "Lagunas",
            "sku": "05"
          },
          "130106": {
            "SkuDepPro": "1301",
            "name": "Monsefu",
            "sku": "06"
          },
          "130107": {
            "SkuDepPro": "1301",
            "name": "Nueva Arica",
            "sku": "07"
          },
          "130108": {
            "SkuDepPro": "1301",
            "name": "Oyotun",
            "sku": "08"
          },
          "130109": {
            "SkuDepPro": "1301",
            "name": "Picsi",
            "sku": "09"
          },
          "130110": {
            "SkuDepPro": "1301",
            "name": "Pimentel",
            "sku": "10"
          },
          "130111": {
            "SkuDepPro": "1301",
            "name": "Reque",
            "sku": "11"
          },
          "130112": {
            "SkuDepPro": "1301",
            "name": "Jose Leonardo Ortiz",
            "sku": "12"
          },
          "130113": {
            "SkuDepPro": "1301",
            "name": "Santa Rosa",
            "sku": "13"
          },
          "130114": {
            "SkuDepPro": "1301",
            "name": "Saña",
            "sku": "14"
          },
          "130115": {
            "SkuDepPro": "1301",
            "name": "La Victoria",
            "sku": "15"
          },
          "130116": {
            "SkuDepPro": "1301",
            "name": "Cayalti",
            "sku": "16"
          },
          "130117": {
            "SkuDepPro": "1301",
            "name": "Patapo",
            "sku": "17"
          },
          "130118": {
            "SkuDepPro": "1301",
            "name": "Pomalca",
            "sku": "18"
          },
          "130119": {
            "SkuDepPro": "1301",
            "name": "Pucala",
            "sku": "19"
          },
          "130120": {
            "SkuDepPro": "1301",
            "name": "Tuman",
            "sku": "20"
          },
          "130201": {
            "SkuDepPro": "1302",
            "name": "Ferreñafe",
            "sku": "01"
          },
          "130202": {
            "SkuDepPro": "1302",
            "name": "Incahuasi",
            "sku": "02"
          },
          "130203": {
            "SkuDepPro": "1302",
            "name": "Cañaris",
            "sku": "03"
          },
          "130204": {
            "SkuDepPro": "1302",
            "name": "Pitipo",
            "sku": "04"
          },
          "130205": {
            "SkuDepPro": "1302",
            "name": "Pueblo Nuevo",
            "sku": "05"
          },
          "130206": {
            "SkuDepPro": "1302",
            "name": "Manuel Antonio Mesones Muro",
            "sku": "06"
          },
          "130301": {
            "SkuDepPro": "1303",
            "name": "Lambayeque",
            "sku": "01"
          },
          "130302": {
            "SkuDepPro": "1303",
            "name": "Chochope",
            "sku": "02"
          },
          "130303": {
            "SkuDepPro": "1303",
            "name": "Illimo",
            "sku": "03"
          },
          "130304": {
            "SkuDepPro": "1303",
            "name": "Jayanca",
            "sku": "04"
          },
          "130305": {
            "SkuDepPro": "1303",
            "name": "Mochumi",
            "sku": "05"
          },
          "130306": {
            "SkuDepPro": "1303",
            "name": "Morrope",
            "sku": "06"
          },
          "130307": {
            "SkuDepPro": "1303",
            "name": "Motupe",
            "sku": "07"
          },
          "130308": {
            "SkuDepPro": "1303",
            "name": "Olmos",
            "sku": "08"
          },
          "130309": {
            "SkuDepPro": "1303",
            "name": "Pacora",
            "sku": "09"
          },
          "130310": {
            "SkuDepPro": "1303",
            "name": "Salas",
            "sku": "10"
          },
          "130311": {
            "SkuDepPro": "1303",
            "name": "San Jose",
            "sku": "11"
          },
          "130312": {
            "SkuDepPro": "1303",
            "name": "Tucume",
            "sku": "12"
          },
          "140101": {
            "SkuDepPro": "1401",
            "name": "Lima",
            "sku": "01"
          },
          "140102": {
            "SkuDepPro": "1401",
            "name": "Ancon",
            "sku": "02"
          },
          "140103": {
            "SkuDepPro": "1401",
            "name": "Ate",
            "sku": "03"
          },
          "140104": {
            "SkuDepPro": "1401",
            "name": "Breña",
            "sku": "04"
          },
          "140105": {
            "SkuDepPro": "1401",
            "name": "Carabayllo",
            "sku": "05"
          },
          "140106": {
            "SkuDepPro": "1401",
            "name": "Comas",
            "sku": "06"
          },
          "140107": {
            "SkuDepPro": "1401",
            "name": "Chaclacayo",
            "sku": "07"
          },
          "140108": {
            "SkuDepPro": "1401",
            "name": "Chorrillos",
            "sku": "08"
          },
          "140109": {
            "SkuDepPro": "1401",
            "name": "La Victoria",
            "sku": "09"
          },
          "140110": {
            "SkuDepPro": "1401",
            "name": "La Molina",
            "sku": "10"
          },
          "140111": {
            "SkuDepPro": "1401",
            "name": "Lince",
            "sku": "11"
          },
          "140112": {
            "SkuDepPro": "1401",
            "name": "Lurigancho",
            "sku": "12"
          },
          "140113": {
            "SkuDepPro": "1401",
            "name": "Lurín",
            "sku": "13"
          },
          "140114": {
            "SkuDepPro": "1401",
            "name": "Magdalena Del Mar",
            "sku": "14"
          },
          "140115": {
            "SkuDepPro": "1401",
            "name": "Miraflores",
            "sku": "15"
          },
          "140116": {
            "SkuDepPro": "1401",
            "name": "Pachacamac",
            "sku": "16"
          },
          "140117": {
            "SkuDepPro": "1401",
            "name": "Pueblo Libre",
            "sku": "17"
          },
          "140118": {
            "SkuDepPro": "1401",
            "name": "Pucusana",
            "sku": "18"
          },
          "140119": {
            "SkuDepPro": "1401",
            "name": "Puente Piedra",
            "sku": "19"
          },
          "140120": {
            "SkuDepPro": "1401",
            "name": "Punta Hermosa",
            "sku": "20"
          },
          "140121": {
            "SkuDepPro": "1401",
            "name": "Punta Negra",
            "sku": "21"
          },
          "140122": {
            "SkuDepPro": "1401",
            "name": "Rimac",
            "sku": "22"
          },
          "140123": {
            "SkuDepPro": "1401",
            "name": "San Bartolo",
            "sku": "23"
          },
          "140124": {
            "SkuDepPro": "1401",
            "name": "San Isidro",
            "sku": "24"
          },
          "140125": {
            "SkuDepPro": "1401",
            "name": "Barranco",
            "sku": "25"
          },
          "140126": {
            "SkuDepPro": "1401",
            "name": "San Martín De Porres",
            "sku": "26"
          },
          "140127": {
            "SkuDepPro": "1401",
            "name": "San Miguel",
            "sku": "27"
          },
          "140128": {
            "SkuDepPro": "1401",
            "name": "Santa Maria Del Mar",
            "sku": "28"
          },
          "140129": {
            "SkuDepPro": "1401",
            "name": "Santa Rosa",
            "sku": "29"
          },
          "140130": {
            "SkuDepPro": "1401",
            "name": "Santiago De Surco",
            "sku": "30"
          },
          "140131": {
            "SkuDepPro": "1401",
            "name": "Surquillo",
            "sku": "31"
          },
          "140132": {
            "SkuDepPro": "1401",
            "name": "Villa Maria Del Triunfo",
            "sku": "32"
          },
          "140133": {
            "SkuDepPro": "1401",
            "name": "Jesus Maria",
            "sku": "33"
          },
          "140134": {
            "SkuDepPro": "1401",
            "name": "Independencia",
            "sku": "34"
          },
          "140135": {
            "SkuDepPro": "1401",
            "name": "El Agustino",
            "sku": "35"
          },
          "140136": {
            "SkuDepPro": "1401",
            "name": "San Juan De Miraflores",
            "sku": "36"
          },
          "140137": {
            "SkuDepPro": "1401",
            "name": "San Juan De Lurigancho",
            "sku": "37"
          },
          "140138": {
            "SkuDepPro": "1401",
            "name": "San Luis",
            "sku": "38"
          },
          "140139": {
            "SkuDepPro": "1401",
            "name": "Cieneguilla",
            "sku": "39"
          },
          "140140": {
            "SkuDepPro": "1401",
            "name": "San Borja",
            "sku": "40"
          },
          "140141": {
            "SkuDepPro": "1401",
            "name": "Villa El Salvador",
            "sku": "41"
          },
          "140142": {
            "SkuDepPro": "1401",
            "name": "Los Olivos",
            "sku": "42"
          },
          "140143": {
            "SkuDepPro": "1401",
            "name": "Santa Anita",
            "sku": "43"
          },
          "140201": {
            "SkuDepPro": "1402",
            "name": "Cajatambo",
            "sku": "01"
          },
          "140205": {
            "SkuDepPro": "1402",
            "name": "Copa",
            "sku": "05"
          },
          "140206": {
            "SkuDepPro": "1402",
            "name": "Gorgor",
            "sku": "06"
          },
          "140207": {
            "SkuDepPro": "1402",
            "name": "Huancapon",
            "sku": "07"
          },
          "140208": {
            "SkuDepPro": "1402",
            "name": "Manas",
            "sku": "08"
          },
          "140301": {
            "SkuDepPro": "1403",
            "name": "Canta",
            "sku": "01"
          },
          "140302": {
            "SkuDepPro": "1403",
            "name": "Arahuay",
            "sku": "02"
          },
          "140303": {
            "SkuDepPro": "1403",
            "name": "Huamantanga",
            "sku": "03"
          },
          "140304": {
            "SkuDepPro": "1403",
            "name": "Huaros",
            "sku": "04"
          },
          "140305": {
            "SkuDepPro": "1403",
            "name": "Lachaqui",
            "sku": "05"
          },
          "140306": {
            "SkuDepPro": "1403",
            "name": "San Buenaventura",
            "sku": "06"
          },
          "140307": {
            "SkuDepPro": "1403",
            "name": "Santa Rosa De Quives",
            "sku": "07"
          },
          "140401": {
            "SkuDepPro": "1404",
            "name": "San Vicente De Cañete",
            "sku": "01"
          },
          "140402": {
            "SkuDepPro": "1404",
            "name": "Calango",
            "sku": "02"
          },
          "140403": {
            "SkuDepPro": "1404",
            "name": "Cerro Azul",
            "sku": "03"
          },
          "140404": {
            "SkuDepPro": "1404",
            "name": "Coayllo",
            "sku": "04"
          },
          "140405": {
            "SkuDepPro": "1404",
            "name": "Chilca",
            "sku": "05"
          },
          "140406": {
            "SkuDepPro": "1404",
            "name": "Imperial",
            "sku": "06"
          },
          "140407": {
            "SkuDepPro": "1404",
            "name": "Lunahuana",
            "sku": "07"
          },
          "140408": {
            "SkuDepPro": "1404",
            "name": "Mala",
            "sku": "08"
          },
          "140409": {
            "SkuDepPro": "1404",
            "name": "Nuevo Imperial",
            "sku": "09"
          },
          "140410": {
            "SkuDepPro": "1404",
            "name": "Pacaran",
            "sku": "10"
          },
          "140411": {
            "SkuDepPro": "1404",
            "name": "Quilmana",
            "sku": "11"
          },
          "140412": {
            "SkuDepPro": "1404",
            "name": "San Antonio",
            "sku": "12"
          },
          "140413": {
            "SkuDepPro": "1404",
            "name": "San Luis",
            "sku": "13"
          },
          "140414": {
            "SkuDepPro": "1404",
            "name": "Santa Cruz De Flores",
            "sku": "14"
          },
          "140415": {
            "SkuDepPro": "1404",
            "name": "Zuñiga",
            "sku": "15"
          },
          "140416": {
            "SkuDepPro": "1404",
            "name": "Asia",
            "sku": "16"
          },
          "140501": {
            "SkuDepPro": "1405",
            "name": "Huacho",
            "sku": "01"
          },
          "140502": {
            "SkuDepPro": "1405",
            "name": "Ambar",
            "sku": "02"
          },
          "140504": {
            "SkuDepPro": "1405",
            "name": "Caleta De Carquin",
            "sku": "04"
          },
          "140505": {
            "SkuDepPro": "1405",
            "name": "Checras",
            "sku": "05"
          },
          "140506": {
            "SkuDepPro": "1405",
            "name": "Hualmay",
            "sku": "06"
          },
          "140507": {
            "SkuDepPro": "1405",
            "name": "Huaura",
            "sku": "07"
          },
          "140508": {
            "SkuDepPro": "1405",
            "name": "Leoncio Prado",
            "sku": "08"
          },
          "140509": {
            "SkuDepPro": "1405",
            "name": "Paccho",
            "sku": "09"
          },
          "140511": {
            "SkuDepPro": "1405",
            "name": "Santa Leonor",
            "sku": "11"
          },
          "140512": {
            "SkuDepPro": "1405",
            "name": "Santa Maria",
            "sku": "12"
          },
          "140513": {
            "SkuDepPro": "1405",
            "name": "Sayan",
            "sku": "13"
          },
          "140516": {
            "SkuDepPro": "1405",
            "name": "Vegueta",
            "sku": "16"
          },
          "140601": {
            "SkuDepPro": "1406",
            "name": "Matucana",
            "sku": "01"
          },
          "140602": {
            "SkuDepPro": "1406",
            "name": "Antioquia",
            "sku": "02"
          },
          "140603": {
            "SkuDepPro": "1406",
            "name": "Callahuanca",
            "sku": "03"
          },
          "140604": {
            "SkuDepPro": "1406",
            "name": "Carampoma",
            "sku": "04"
          },
          "140605": {
            "SkuDepPro": "1406",
            "name": "Casta",
            "sku": "05"
          },
          "140606": {
            "SkuDepPro": "1406",
            "name": "San Jose De Los Chorrillos",
            "sku": "06"
          },
          "140607": {
            "SkuDepPro": "1406",
            "name": "Chicla",
            "sku": "07"
          },
          "140608": {
            "SkuDepPro": "1406",
            "name": "Huanza",
            "sku": "08"
          },
          "140609": {
            "SkuDepPro": "1406",
            "name": "Huarochirí",
            "sku": "09"
          },
          "140610": {
            "SkuDepPro": "1406",
            "name": "Lahuaytambo",
            "sku": "10"
          },
          "140611": {
            "SkuDepPro": "1406",
            "name": "Langa",
            "sku": "11"
          },
          "140612": {
            "SkuDepPro": "1406",
            "name": "Mariatana",
            "sku": "12"
          },
          "140613": {
            "SkuDepPro": "1406",
            "name": "Ricardo Palma",
            "sku": "13"
          },
          "140614": {
            "SkuDepPro": "1406",
            "name": "San Andres De Tupicocha",
            "sku": "14"
          },
          "140615": {
            "SkuDepPro": "1406",
            "name": "San Antonio",
            "sku": "15"
          },
          "140616": {
            "SkuDepPro": "1406",
            "name": "San Bartolome",
            "sku": "16"
          },
          "140617": {
            "SkuDepPro": "1406",
            "name": "San Damian",
            "sku": "17"
          },
          "140618": {
            "SkuDepPro": "1406",
            "name": "Sangallaya",
            "sku": "18"
          },
          "140619": {
            "SkuDepPro": "1406",
            "name": "San Juan De Tantaranche",
            "sku": "19"
          },
          "140620": {
            "SkuDepPro": "1406",
            "name": "San Lorenzo De Quinti",
            "sku": "20"
          },
          "140621": {
            "SkuDepPro": "1406",
            "name": "San Mateo",
            "sku": "21"
          },
          "140622": {
            "SkuDepPro": "1406",
            "name": "San Mateo De Otao",
            "sku": "22"
          },
          "140623": {
            "SkuDepPro": "1406",
            "name": "San Pedro De Huancayre",
            "sku": "23"
          },
          "140624": {
            "SkuDepPro": "1406",
            "name": "Santa Cruz De Cocachacra",
            "sku": "24"
          },
          "140625": {
            "SkuDepPro": "1406",
            "name": "Santa Eulalia",
            "sku": "25"
          },
          "140626": {
            "SkuDepPro": "1406",
            "name": "Santiago De Anchucaya",
            "sku": "26"
          },
          "140627": {
            "SkuDepPro": "1406",
            "name": "Santiago De Tuna",
            "sku": "27"
          },
          "140628": {
            "SkuDepPro": "1406",
            "name": "Santo Domingo De Los Olleros",
            "sku": "28"
          },
          "140629": {
            "SkuDepPro": "1406",
            "name": "Surco",
            "sku": "29"
          },
          "140630": {
            "SkuDepPro": "1406",
            "name": "Huachupampa",
            "sku": "30"
          },
          "140631": {
            "SkuDepPro": "1406",
            "name": "Laraos",
            "sku": "31"
          },
          "140632": {
            "SkuDepPro": "1406",
            "name": "San Juan De Iris",
            "sku": "32"
          },
          "140701": {
            "SkuDepPro": "1407",
            "name": "Yauyos",
            "sku": "01"
          },
          "140702": {
            "SkuDepPro": "1407",
            "name": "Alis",
            "sku": "02"
          },
          "140703": {
            "SkuDepPro": "1407",
            "name": "Allauca",
            "sku": "03"
          },
          "140704": {
            "SkuDepPro": "1407",
            "name": "Ayaviri",
            "sku": "04"
          },
          "140705": {
            "SkuDepPro": "1407",
            "name": "Azángaro",
            "sku": "05"
          },
          "140706": {
            "SkuDepPro": "1407",
            "name": "Cacra",
            "sku": "06"
          },
          "140707": {
            "SkuDepPro": "1407",
            "name": "Carania",
            "sku": "07"
          },
          "140708": {
            "SkuDepPro": "1407",
            "name": "Cochas",
            "sku": "08"
          },
          "140709": {
            "SkuDepPro": "1407",
            "name": "Colonia",
            "sku": "09"
          },
          "140710": {
            "SkuDepPro": "1407",
            "name": "Chocos",
            "sku": "10"
          },
          "140711": {
            "SkuDepPro": "1407",
            "name": "Huampara",
            "sku": "11"
          },
          "140712": {
            "SkuDepPro": "1407",
            "name": "Huancaya",
            "sku": "12"
          },
          "140713": {
            "SkuDepPro": "1407",
            "name": "Huangascar",
            "sku": "13"
          },
          "140714": {
            "SkuDepPro": "1407",
            "name": "Huantan",
            "sku": "14"
          },
          "140715": {
            "SkuDepPro": "1407",
            "name": "Huañec",
            "sku": "15"
          },
          "140716": {
            "SkuDepPro": "1407",
            "name": "Laraos",
            "sku": "16"
          },
          "140717": {
            "SkuDepPro": "1407",
            "name": "Lincha",
            "sku": "17"
          },
          "140718": {
            "SkuDepPro": "1407",
            "name": "Miraflores",
            "sku": "18"
          },
          "140719": {
            "SkuDepPro": "1407",
            "name": "Omas",
            "sku": "19"
          },
          "140720": {
            "SkuDepPro": "1407",
            "name": "Quinches",
            "sku": "20"
          },
          "140721": {
            "SkuDepPro": "1407",
            "name": "Quinocay",
            "sku": "21"
          },
          "140722": {
            "SkuDepPro": "1407",
            "name": "San Joaquin",
            "sku": "22"
          },
          "140723": {
            "SkuDepPro": "1407",
            "name": "San Pedro De Pilas",
            "sku": "23"
          },
          "140724": {
            "SkuDepPro": "1407",
            "name": "Tanta",
            "sku": "24"
          },
          "140725": {
            "SkuDepPro": "1407",
            "name": "Tauripampa",
            "sku": "25"
          },
          "140726": {
            "SkuDepPro": "1407",
            "name": "Tupe",
            "sku": "26"
          },
          "140727": {
            "SkuDepPro": "1407",
            "name": "Tomas",
            "sku": "27"
          },
          "140728": {
            "SkuDepPro": "1407",
            "name": "Viñac",
            "sku": "28"
          },
          "140729": {
            "SkuDepPro": "1407",
            "name": "Vitis",
            "sku": "29"
          },
          "140730": {
            "SkuDepPro": "1407",
            "name": "Hongos",
            "sku": "30"
          },
          "140731": {
            "SkuDepPro": "1407",
            "name": "Madean",
            "sku": "31"
          },
          "140732": {
            "SkuDepPro": "1407",
            "name": "Putinza",
            "sku": "32"
          },
          "140733": {
            "SkuDepPro": "1407",
            "name": "Catahuasi",
            "sku": "33"
          },
          "140801": {
            "SkuDepPro": "1408",
            "name": "Huaral",
            "sku": "01"
          },
          "140802": {
            "SkuDepPro": "1408",
            "name": "Atavillos Alto",
            "sku": "02"
          },
          "140803": {
            "SkuDepPro": "1408",
            "name": "Atavillos Bajo",
            "sku": "03"
          },
          "140804": {
            "SkuDepPro": "1408",
            "name": "Aucallama",
            "sku": "04"
          },
          "140805": {
            "SkuDepPro": "1408",
            "name": "Chancay",
            "sku": "05"
          },
          "140806": {
            "SkuDepPro": "1408",
            "name": "Ihuari",
            "sku": "06"
          },
          "140807": {
            "SkuDepPro": "1408",
            "name": "Lampian",
            "sku": "07"
          },
          "140808": {
            "SkuDepPro": "1408",
            "name": "Pacaraos",
            "sku": "08"
          },
          "140809": {
            "SkuDepPro": "1408",
            "name": "San Miguel De Acos",
            "sku": "09"
          },
          "140810": {
            "SkuDepPro": "1408",
            "name": "Veintisiete De Noviembre",
            "sku": "10"
          },
          "140811": {
            "SkuDepPro": "1408",
            "name": "Santa Cruz De Andamarca",
            "sku": "11"
          },
          "140812": {
            "SkuDepPro": "1408",
            "name": "Sumbilca",
            "sku": "12"
          },
          "140901": {
            "SkuDepPro": "1409",
            "name": "Barranca",
            "sku": "01"
          },
          "140902": {
            "SkuDepPro": "1409",
            "name": "Paramonga",
            "sku": "02"
          },
          "140903": {
            "SkuDepPro": "1409",
            "name": "Pativilca",
            "sku": "03"
          },
          "140904": {
            "SkuDepPro": "1409",
            "name": "Supe",
            "sku": "04"
          },
          "140905": {
            "SkuDepPro": "1409",
            "name": "Supe Puerto",
            "sku": "05"
          },
          "141001": {
            "SkuDepPro": "1410",
            "name": "Oyón",
            "sku": "01"
          },
          "141002": {
            "SkuDepPro": "1410",
            "name": "Navan",
            "sku": "02"
          },
          "141003": {
            "SkuDepPro": "1410",
            "name": "Caujul",
            "sku": "03"
          },
          "141004": {
            "SkuDepPro": "1410",
            "name": "Andajes",
            "sku": "04"
          },
          "141005": {
            "SkuDepPro": "1410",
            "name": "Pachangara",
            "sku": "05"
          },
          "141006": {
            "SkuDepPro": "1410",
            "name": "Cochamarca",
            "sku": "06"
          },
          "150101": {
            "SkuDepPro": "1501",
            "name": "Iquitos",
            "sku": "01"
          },
          "150102": {
            "SkuDepPro": "1501",
            "name": "Alto Nanay",
            "sku": "02"
          },
          "150103": {
            "SkuDepPro": "1501",
            "name": "Fernando Lores",
            "sku": "03"
          },
          "150104": {
            "SkuDepPro": "1501",
            "name": "Las Amazonas",
            "sku": "04"
          },
          "150105": {
            "SkuDepPro": "1501",
            "name": "Mazan",
            "sku": "05"
          },
          "150106": {
            "SkuDepPro": "1501",
            "name": "Napo",
            "sku": "06"
          },
          "150107": {
            "SkuDepPro": "1501",
            "name": "Putumayo",
            "sku": "07"
          },
          "150108": {
            "SkuDepPro": "1501",
            "name": "Torres Causana",
            "sku": "08"
          },
          "150110": {
            "SkuDepPro": "1501",
            "name": "Indiana",
            "sku": "10"
          },
          "150111": {
            "SkuDepPro": "1501",
            "name": "Punchana",
            "sku": "11"
          },
          "150112": {
            "SkuDepPro": "1501",
            "name": "Belen",
            "sku": "12"
          },
          "150113": {
            "SkuDepPro": "1501",
            "name": "San Juan Bautista",
            "sku": "13"
          },
          "150114": {
            "SkuDepPro": "1501",
            "name": "Teniente Manuel Clavero",
            "sku": "14"
          },
          "150201": {
            "SkuDepPro": "1502",
            "name": "Yurimaguas",
            "sku": "01"
          },
          "150202": {
            "SkuDepPro": "1502",
            "name": "Balsapuerto",
            "sku": "02"
          },
          "150205": {
            "SkuDepPro": "1502",
            "name": "Jeberos",
            "sku": "05"
          },
          "150206": {
            "SkuDepPro": "1502",
            "name": "Lagunas",
            "sku": "06"
          },
          "150210": {
            "SkuDepPro": "1502",
            "name": "Santa Cruz",
            "sku": "10"
          },
          "150211": {
            "SkuDepPro": "1502",
            "name": "Teniente Cesar Lopez Rojas",
            "sku": "11"
          },
          "150301": {
            "SkuDepPro": "1503",
            "name": "Nauta",
            "sku": "01"
          },
          "150302": {
            "SkuDepPro": "1503",
            "name": "Parinari",
            "sku": "02"
          },
          "150303": {
            "SkuDepPro": "1503",
            "name": "Tigre",
            "sku": "03"
          },
          "150304": {
            "SkuDepPro": "1503",
            "name": "Urarinas",
            "sku": "04"
          },
          "150305": {
            "SkuDepPro": "1503",
            "name": "Trompeteros",
            "sku": "05"
          },
          "150401": {
            "SkuDepPro": "1504",
            "name": "Requena",
            "sku": "01"
          },
          "150402": {
            "SkuDepPro": "1504",
            "name": "Alto Tapiche",
            "sku": "02"
          },
          "150403": {
            "SkuDepPro": "1504",
            "name": "Capelo",
            "sku": "03"
          },
          "150404": {
            "SkuDepPro": "1504",
            "name": "Emilio San Martín",
            "sku": "04"
          },
          "150405": {
            "SkuDepPro": "1504",
            "name": "Maquia",
            "sku": "05"
          },
          "150406": {
            "SkuDepPro": "1504",
            "name": "Puinahua",
            "sku": "06"
          },
          "150407": {
            "SkuDepPro": "1504",
            "name": "Saquena",
            "sku": "07"
          },
          "150408": {
            "SkuDepPro": "1504",
            "name": "Soplin",
            "sku": "08"
          },
          "150409": {
            "SkuDepPro": "1504",
            "name": "Tapiche",
            "sku": "09"
          },
          "150410": {
            "SkuDepPro": "1504",
            "name": "Jenaro Herrera",
            "sku": "10"
          },
          "150411": {
            "SkuDepPro": "1504",
            "name": "Yaquerana",
            "sku": "11"
          },
          "150501": {
            "SkuDepPro": "1505",
            "name": "Contamana",
            "sku": "01"
          },
          "150502": {
            "SkuDepPro": "1505",
            "name": "Vargas Guerra",
            "sku": "02"
          },
          "150503": {
            "SkuDepPro": "1505",
            "name": "Padre Marquez",
            "sku": "03"
          },
          "150504": {
            "SkuDepPro": "1505",
            "name": "Pampa Hermosa",
            "sku": "04"
          },
          "150505": {
            "SkuDepPro": "1505",
            "name": "Sarayacu",
            "sku": "05"
          },
          "150506": {
            "SkuDepPro": "1505",
            "name": "Inahuaya",
            "sku": "06"
          },
          "150601": {
            "SkuDepPro": "1506",
            "name": "Ramon Castilla",
            "sku": "01"
          },
          "150602": {
            "SkuDepPro": "1506",
            "name": "Pebas",
            "sku": "02"
          },
          "150603": {
            "SkuDepPro": "1506",
            "name": "Yavari",
            "sku": "03"
          },
          "150604": {
            "SkuDepPro": "1506",
            "name": "San Pablo",
            "sku": "04"
          },
          "150701": {
            "SkuDepPro": "1507",
            "name": "Barranca",
            "sku": "01"
          },
          "150702": {
            "SkuDepPro": "1507",
            "name": "Andoas",
            "sku": "02"
          },
          "150703": {
            "SkuDepPro": "1507",
            "name": "Cahuapanas",
            "sku": "03"
          },
          "150704": {
            "SkuDepPro": "1507",
            "name": "Manseriche",
            "sku": "04"
          },
          "150705": {
            "SkuDepPro": "1507",
            "name": "Morona",
            "sku": "05"
          },
          "150706": {
            "SkuDepPro": "1507",
            "name": "Pastaza",
            "sku": "06"
          },
          "160101": {
            "SkuDepPro": "1601",
            "name": "Tambopata",
            "sku": "01"
          },
          "160102": {
            "SkuDepPro": "1601",
            "name": "Inambari",
            "sku": "02"
          },
          "160103": {
            "SkuDepPro": "1601",
            "name": "Las Piedras",
            "sku": "03"
          },
          "160104": {
            "SkuDepPro": "1601",
            "name": "Laberinto",
            "sku": "04"
          },
          "160201": {
            "SkuDepPro": "1602",
            "name": "Manu",
            "sku": "01"
          },
          "160202": {
            "SkuDepPro": "1602",
            "name": "Fitzcarrald",
            "sku": "02"
          },
          "160203": {
            "SkuDepPro": "1602",
            "name": "Madre De Dios",
            "sku": "03"
          },
          "160204": {
            "SkuDepPro": "1602",
            "name": "Huepetuhe",
            "sku": "04"
          },
          "160301": {
            "SkuDepPro": "1603",
            "name": "Iñapari",
            "sku": "01"
          },
          "160302": {
            "SkuDepPro": "1603",
            "name": "Iberia",
            "sku": "02"
          },
          "160303": {
            "SkuDepPro": "1603",
            "name": "Tahuamanu",
            "sku": "03"
          },
          "170101": {
            "SkuDepPro": "1701",
            "name": "Moquegua",
            "sku": "01"
          },
          "170102": {
            "SkuDepPro": "1701",
            "name": "Carumas",
            "sku": "02"
          },
          "170103": {
            "SkuDepPro": "1701",
            "name": "Cuchumbaya",
            "sku": "03"
          },
          "170104": {
            "SkuDepPro": "1701",
            "name": "San Cristobal",
            "sku": "04"
          },
          "170105": {
            "SkuDepPro": "1701",
            "name": "Torata",
            "sku": "05"
          },
          "170106": {
            "SkuDepPro": "1701",
            "name": "Samegua",
            "sku": "06"
          },
          "170201": {
            "SkuDepPro": "1702",
            "name": "Omate",
            "sku": "01"
          },
          "170202": {
            "SkuDepPro": "1702",
            "name": "Coalaque",
            "sku": "02"
          },
          "170203": {
            "SkuDepPro": "1702",
            "name": "Chojata",
            "sku": "03"
          },
          "170204": {
            "SkuDepPro": "1702",
            "name": "Ichuña",
            "sku": "04"
          },
          "170205": {
            "SkuDepPro": "1702",
            "name": "La Capilla",
            "sku": "05"
          },
          "170206": {
            "SkuDepPro": "1702",
            "name": "Lloque",
            "sku": "06"
          },
          "170207": {
            "SkuDepPro": "1702",
            "name": "Matalaque",
            "sku": "07"
          },
          "170208": {
            "SkuDepPro": "1702",
            "name": "Puquina",
            "sku": "08"
          },
          "170209": {
            "SkuDepPro": "1702",
            "name": "Quinistaquillas",
            "sku": "09"
          },
          "170210": {
            "SkuDepPro": "1702",
            "name": "Ubinas",
            "sku": "10"
          },
          "170211": {
            "SkuDepPro": "1702",
            "name": "Yunga",
            "sku": "11"
          },
          "170301": {
            "SkuDepPro": "1703",
            "name": "Ilo",
            "sku": "01"
          },
          "170302": {
            "SkuDepPro": "1703",
            "name": "El Algarrobal",
            "sku": "02"
          },
          "170303": {
            "SkuDepPro": "1703",
            "name": "Pacocha",
            "sku": "03"
          },
          "180101": {
            "SkuDepPro": "1801",
            "name": "Chaupimarca",
            "sku": "01"
          },
          "180103": {
            "SkuDepPro": "1801",
            "name": "Huachon",
            "sku": "03"
          },
          "180104": {
            "SkuDepPro": "1801",
            "name": "Huariaca",
            "sku": "04"
          },
          "180105": {
            "SkuDepPro": "1801",
            "name": "Huayllay",
            "sku": "05"
          },
          "180106": {
            "SkuDepPro": "1801",
            "name": "Ninacaca",
            "sku": "06"
          },
          "180107": {
            "SkuDepPro": "1801",
            "name": "Pallanchacra",
            "sku": "07"
          },
          "180108": {
            "SkuDepPro": "1801",
            "name": "Paucartambo",
            "sku": "08"
          },
          "180109": {
            "SkuDepPro": "1801",
            "name": "San Fco De Asis De Yarusyacan",
            "sku": "09"
          },
          "180110": {
            "SkuDepPro": "1801",
            "name": "Simon Bolívar",
            "sku": "10"
          },
          "180111": {
            "SkuDepPro": "1801",
            "name": "Ticlacayan",
            "sku": "11"
          },
          "180112": {
            "SkuDepPro": "1801",
            "name": "Tinyahuarco",
            "sku": "12"
          },
          "180113": {
            "SkuDepPro": "1801",
            "name": "Vicco",
            "sku": "13"
          },
          "180114": {
            "SkuDepPro": "1801",
            "name": "Yanacancha",
            "sku": "14"
          },
          "180201": {
            "SkuDepPro": "1802",
            "name": "Yanahuanca",
            "sku": "01"
          },
          "180202": {
            "SkuDepPro": "1802",
            "name": "Chacayan",
            "sku": "02"
          },
          "180203": {
            "SkuDepPro": "1802",
            "name": "Goyllarisquizga",
            "sku": "03"
          },
          "180204": {
            "SkuDepPro": "1802",
            "name": "Paucar",
            "sku": "04"
          },
          "180205": {
            "SkuDepPro": "1802",
            "name": "San Pedro De Pillao",
            "sku": "05"
          },
          "180206": {
            "SkuDepPro": "1802",
            "name": "Santa Ana De Tusi",
            "sku": "06"
          },
          "180207": {
            "SkuDepPro": "1802",
            "name": "Tapuc",
            "sku": "07"
          },
          "180208": {
            "SkuDepPro": "1802",
            "name": "Vilcabamba",
            "sku": "08"
          },
          "180301": {
            "SkuDepPro": "1803",
            "name": "Oxapampa",
            "sku": "01"
          },
          "180302": {
            "SkuDepPro": "1803",
            "name": "Chontabamba",
            "sku": "02"
          },
          "180303": {
            "SkuDepPro": "1803",
            "name": "Huancabamba",
            "sku": "03"
          },
          "180304": {
            "SkuDepPro": "1803",
            "name": "Puerto Bermudez",
            "sku": "04"
          },
          "180305": {
            "SkuDepPro": "1803",
            "name": "Villa Rica",
            "sku": "05"
          },
          "180306": {
            "SkuDepPro": "1803",
            "name": "Pozuzo",
            "sku": "06"
          },
          "180307": {
            "SkuDepPro": "1803",
            "name": "Palcazu",
            "sku": "07"
          },
          "180308": {
            "SkuDepPro": "1803",
            "name": "Constitucion",
            "sku": "08"
          },
          "190101": {
            "SkuDepPro": "1901",
            "name": "Piura",
            "sku": "01"
          },
          "190103": {
            "SkuDepPro": "1901",
            "name": "Castilla",
            "sku": "03"
          },
          "190104": {
            "SkuDepPro": "1901",
            "name": "Catacaos",
            "sku": "04"
          },
          "190105": {
            "SkuDepPro": "1901",
            "name": "La Arena",
            "sku": "05"
          },
          "190106": {
            "SkuDepPro": "1901",
            "name": "La Union",
            "sku": "06"
          },
          "190107": {
            "SkuDepPro": "1901",
            "name": "Las Lomas",
            "sku": "07"
          },
          "190109": {
            "SkuDepPro": "1901",
            "name": "Tambo Grande",
            "sku": "09"
          },
          "190113": {
            "SkuDepPro": "1901",
            "name": "Cura Mori",
            "sku": "13"
          },
          "190114": {
            "SkuDepPro": "1901",
            "name": "El Tallan",
            "sku": "14"
          },
          "190115": {
            "SkuDepPro": "1901",
            "name": "Veintiseis De Octubre",
            "sku": "15"
          },
          "190201": {
            "SkuDepPro": "1902",
            "name": "Ayabaca",
            "sku": "01"
          },
          "190202": {
            "SkuDepPro": "1902",
            "name": "Frias",
            "sku": "02"
          },
          "190203": {
            "SkuDepPro": "1902",
            "name": "Lagunas",
            "sku": "03"
          },
          "190204": {
            "SkuDepPro": "1902",
            "name": "Montero",
            "sku": "04"
          },
          "190205": {
            "SkuDepPro": "1902",
            "name": "Pacaipampa",
            "sku": "05"
          },
          "190206": {
            "SkuDepPro": "1902",
            "name": "Sapillica",
            "sku": "06"
          },
          "190207": {
            "SkuDepPro": "1902",
            "name": "Sicchez",
            "sku": "07"
          },
          "190208": {
            "SkuDepPro": "1902",
            "name": "Suyo",
            "sku": "08"
          },
          "190209": {
            "SkuDepPro": "1902",
            "name": "Jilili",
            "sku": "09"
          },
          "190210": {
            "SkuDepPro": "1902",
            "name": "Paimas",
            "sku": "10"
          },
          "190301": {
            "SkuDepPro": "1903",
            "name": "Huancabamba",
            "sku": "01"
          },
          "190302": {
            "SkuDepPro": "1903",
            "name": "Canchaque",
            "sku": "02"
          },
          "190303": {
            "SkuDepPro": "1903",
            "name": "Huarmaca",
            "sku": "03"
          },
          "190304": {
            "SkuDepPro": "1903",
            "name": "Sondor",
            "sku": "04"
          },
          "190305": {
            "SkuDepPro": "1903",
            "name": "Sondorillo",
            "sku": "05"
          },
          "190306": {
            "SkuDepPro": "1903",
            "name": "El Carmen De La Frontera",
            "sku": "06"
          },
          "190307": {
            "SkuDepPro": "1903",
            "name": "San Miguel De El Faique",
            "sku": "07"
          },
          "190308": {
            "SkuDepPro": "1903",
            "name": "Lalaquiz",
            "sku": "08"
          },
          "190401": {
            "SkuDepPro": "1904",
            "name": "Chulucanas",
            "sku": "01"
          },
          "190402": {
            "SkuDepPro": "1904",
            "name": "Buenos Aires",
            "sku": "02"
          },
          "190403": {
            "SkuDepPro": "1904",
            "name": "Chalaco",
            "sku": "03"
          },
          "190404": {
            "SkuDepPro": "1904",
            "name": "Morropón",
            "sku": "04"
          },
          "190405": {
            "SkuDepPro": "1904",
            "name": "Salitral",
            "sku": "05"
          },
          "190406": {
            "SkuDepPro": "1904",
            "name": "Santa Catalina De Mossa",
            "sku": "06"
          },
          "190407": {
            "SkuDepPro": "1904",
            "name": "Santo Domingo",
            "sku": "07"
          },
          "190408": {
            "SkuDepPro": "1904",
            "name": "La Matanza",
            "sku": "08"
          },
          "190409": {
            "SkuDepPro": "1904",
            "name": "Yamango",
            "sku": "09"
          },
          "190410": {
            "SkuDepPro": "1904",
            "name": "San Juan De Bigote",
            "sku": "10"
          },
          "190501": {
            "SkuDepPro": "1905",
            "name": "Paita",
            "sku": "01"
          },
          "190502": {
            "SkuDepPro": "1905",
            "name": "Amotape",
            "sku": "02"
          },
          "190503": {
            "SkuDepPro": "1905",
            "name": "Arenal",
            "sku": "03"
          },
          "190504": {
            "SkuDepPro": "1905",
            "name": "La Huaca",
            "sku": "04"
          },
          "190505": {
            "SkuDepPro": "1905",
            "name": "Colan",
            "sku": "05"
          },
          "190506": {
            "SkuDepPro": "1905",
            "name": "Tamarindo",
            "sku": "06"
          },
          "190507": {
            "SkuDepPro": "1905",
            "name": "Vichayal",
            "sku": "07"
          },
          "190601": {
            "SkuDepPro": "1906",
            "name": "Sullana",
            "sku": "01"
          },
          "190602": {
            "SkuDepPro": "1906",
            "name": "Bellavista",
            "sku": "02"
          },
          "190603": {
            "SkuDepPro": "1906",
            "name": "Lancones",
            "sku": "03"
          },
          "190604": {
            "SkuDepPro": "1906",
            "name": "Marcavelica",
            "sku": "04"
          },
          "190605": {
            "SkuDepPro": "1906",
            "name": "Miguel Checa",
            "sku": "05"
          },
          "190606": {
            "SkuDepPro": "1906",
            "name": "Querecotillo",
            "sku": "06"
          },
          "190607": {
            "SkuDepPro": "1906",
            "name": "Salitral",
            "sku": "07"
          },
          "190608": {
            "SkuDepPro": "1906",
            "name": "Ignacio Escudero",
            "sku": "08"
          },
          "190701": {
            "SkuDepPro": "1907",
            "name": "Pariñas",
            "sku": "01"
          },
          "190702": {
            "SkuDepPro": "1907",
            "name": "El Alto",
            "sku": "02"
          },
          "190703": {
            "SkuDepPro": "1907",
            "name": "La Brea",
            "sku": "03"
          },
          "190704": {
            "SkuDepPro": "1907",
            "name": "Lobitos",
            "sku": "04"
          },
          "190705": {
            "SkuDepPro": "1907",
            "name": "Mancora",
            "sku": "05"
          },
          "190706": {
            "SkuDepPro": "1907",
            "name": "Los Organos",
            "sku": "06"
          },
          "190801": {
            "SkuDepPro": "1908",
            "name": "Sechura",
            "sku": "01"
          },
          "190802": {
            "SkuDepPro": "1908",
            "name": "Vice",
            "sku": "02"
          },
          "190803": {
            "SkuDepPro": "1908",
            "name": "Bernal",
            "sku": "03"
          },
          "190804": {
            "SkuDepPro": "1908",
            "name": "Bellavista De La Union",
            "sku": "04"
          },
          "190805": {
            "SkuDepPro": "1908",
            "name": "Cristo Nos Valga",
            "sku": "05"
          },
          "190806": {
            "SkuDepPro": "1908",
            "name": "Rinconada-Llicuar",
            "sku": "06"
          },
          "200101": {
            "SkuDepPro": "2001",
            "name": "Puno",
            "sku": "01"
          },
          "200102": {
            "SkuDepPro": "2001",
            "name": "Acora",
            "sku": "02"
          },
          "200103": {
            "SkuDepPro": "2001",
            "name": "Atuncolla",
            "sku": "03"
          },
          "200104": {
            "SkuDepPro": "2001",
            "name": "Capachica",
            "sku": "04"
          },
          "200105": {
            "SkuDepPro": "2001",
            "name": "Coata",
            "sku": "05"
          },
          "200106": {
            "SkuDepPro": "2001",
            "name": "Chucuito",
            "sku": "06"
          },
          "200107": {
            "SkuDepPro": "2001",
            "name": "Huata",
            "sku": "07"
          },
          "200108": {
            "SkuDepPro": "2001",
            "name": "Mañazo",
            "sku": "08"
          },
          "200109": {
            "SkuDepPro": "2001",
            "name": "Paucarcolla",
            "sku": "09"
          },
          "200110": {
            "SkuDepPro": "2001",
            "name": "Pichacani",
            "sku": "10"
          },
          "200111": {
            "SkuDepPro": "2001",
            "name": "San Antonio",
            "sku": "11"
          },
          "200112": {
            "SkuDepPro": "2001",
            "name": "Tiquillaca",
            "sku": "12"
          },
          "200113": {
            "SkuDepPro": "2001",
            "name": "Vilque",
            "sku": "13"
          },
          "200114": {
            "SkuDepPro": "2001",
            "name": "Plateria",
            "sku": "14"
          },
          "200115": {
            "SkuDepPro": "2001",
            "name": "Amantani",
            "sku": "15"
          },
          "200201": {
            "SkuDepPro": "2002",
            "name": "Azángaro",
            "sku": "01"
          },
          "200202": {
            "SkuDepPro": "2002",
            "name": "Achaya",
            "sku": "02"
          },
          "200203": {
            "SkuDepPro": "2002",
            "name": "Arapa",
            "sku": "03"
          },
          "200204": {
            "SkuDepPro": "2002",
            "name": "Asillo",
            "sku": "04"
          },
          "200205": {
            "SkuDepPro": "2002",
            "name": "Caminaca",
            "sku": "05"
          },
          "200206": {
            "SkuDepPro": "2002",
            "name": "Chupa",
            "sku": "06"
          },
          "200207": {
            "SkuDepPro": "2002",
            "name": "Jose Domingo Choquehuanca",
            "sku": "07"
          },
          "200208": {
            "SkuDepPro": "2002",
            "name": "Muñani",
            "sku": "08"
          },
          "200210": {
            "SkuDepPro": "2002",
            "name": "Potoni",
            "sku": "10"
          },
          "200212": {
            "SkuDepPro": "2002",
            "name": "Saman",
            "sku": "12"
          },
          "200213": {
            "SkuDepPro": "2002",
            "name": "San Anton",
            "sku": "13"
          },
          "200214": {
            "SkuDepPro": "2002",
            "name": "San Jose",
            "sku": "14"
          },
          "200215": {
            "SkuDepPro": "2002",
            "name": "San Juan De Salinas",
            "sku": "15"
          },
          "200216": {
            "SkuDepPro": "2002",
            "name": "Santiago De Pupuja",
            "sku": "16"
          },
          "200217": {
            "SkuDepPro": "2002",
            "name": "Tirapata",
            "sku": "17"
          },
          "200301": {
            "SkuDepPro": "2003",
            "name": "Macusani",
            "sku": "01"
          },
          "200302": {
            "SkuDepPro": "2003",
            "name": "Ajoyani",
            "sku": "02"
          },
          "200303": {
            "SkuDepPro": "2003",
            "name": "Ayapata",
            "sku": "03"
          },
          "200304": {
            "SkuDepPro": "2003",
            "name": "Coasa",
            "sku": "04"
          },
          "200305": {
            "SkuDepPro": "2003",
            "name": "Corani",
            "sku": "05"
          },
          "200306": {
            "SkuDepPro": "2003",
            "name": "Crucero",
            "sku": "06"
          },
          "200307": {
            "SkuDepPro": "2003",
            "name": "Ituata",
            "sku": "07"
          },
          "200308": {
            "SkuDepPro": "2003",
            "name": "Ollachea",
            "sku": "08"
          },
          "200309": {
            "SkuDepPro": "2003",
            "name": "San Gaban",
            "sku": "09"
          },
          "200310": {
            "SkuDepPro": "2003",
            "name": "Usicayos",
            "sku": "10"
          },
          "200401": {
            "SkuDepPro": "2004",
            "name": "Juli",
            "sku": "01"
          },
          "200402": {
            "SkuDepPro": "2004",
            "name": "Desaguadero",
            "sku": "02"
          },
          "200403": {
            "SkuDepPro": "2004",
            "name": "Huacullani",
            "sku": "03"
          },
          "200406": {
            "SkuDepPro": "2004",
            "name": "Pisacoma",
            "sku": "06"
          },
          "200407": {
            "SkuDepPro": "2004",
            "name": "Pomata",
            "sku": "07"
          },
          "200410": {
            "SkuDepPro": "2004",
            "name": "Zepita",
            "sku": "10"
          },
          "200412": {
            "SkuDepPro": "2004",
            "name": "Kelluyo",
            "sku": "12"
          },
          "200501": {
            "SkuDepPro": "2005",
            "name": "Huancané",
            "sku": "01"
          },
          "200502": {
            "SkuDepPro": "2005",
            "name": "Cojata",
            "sku": "02"
          },
          "200504": {
            "SkuDepPro": "2005",
            "name": "Inchupalla",
            "sku": "04"
          },
          "200506": {
            "SkuDepPro": "2005",
            "name": "Pusi",
            "sku": "06"
          },
          "200507": {
            "SkuDepPro": "2005",
            "name": "Rosaspata",
            "sku": "07"
          },
          "200508": {
            "SkuDepPro": "2005",
            "name": "Taraco",
            "sku": "08"
          },
          "200509": {
            "SkuDepPro": "2005",
            "name": "Vilque Chico",
            "sku": "09"
          },
          "200511": {
            "SkuDepPro": "2005",
            "name": "Huatasani",
            "sku": "11"
          },
          "200601": {
            "SkuDepPro": "2006",
            "name": "Lampa",
            "sku": "01"
          },
          "200602": {
            "SkuDepPro": "2006",
            "name": "Cabanilla",
            "sku": "02"
          },
          "200603": {
            "SkuDepPro": "2006",
            "name": "Calapuja",
            "sku": "03"
          },
          "200604": {
            "SkuDepPro": "2006",
            "name": "Nicasio",
            "sku": "04"
          },
          "200605": {
            "SkuDepPro": "2006",
            "name": "Ocuviri",
            "sku": "05"
          },
          "200606": {
            "SkuDepPro": "2006",
            "name": "Palca",
            "sku": "06"
          },
          "200607": {
            "SkuDepPro": "2006",
            "name": "Paratia",
            "sku": "07"
          },
          "200608": {
            "SkuDepPro": "2006",
            "name": "Pucara",
            "sku": "08"
          },
          "200609": {
            "SkuDepPro": "2006",
            "name": "Santa Lucia",
            "sku": "09"
          },
          "200610": {
            "SkuDepPro": "2006",
            "name": "Vilavila",
            "sku": "10"
          },
          "200701": {
            "SkuDepPro": "2007",
            "name": "Ayaviri",
            "sku": "01"
          },
          "200702": {
            "SkuDepPro": "2007",
            "name": "Antauta",
            "sku": "02"
          },
          "200703": {
            "SkuDepPro": "2007",
            "name": "Cupi",
            "sku": "03"
          },
          "200704": {
            "SkuDepPro": "2007",
            "name": "Llalli",
            "sku": "04"
          },
          "200705": {
            "SkuDepPro": "2007",
            "name": "Macari",
            "sku": "05"
          },
          "200706": {
            "SkuDepPro": "2007",
            "name": "Nuñoa",
            "sku": "06"
          },
          "200707": {
            "SkuDepPro": "2007",
            "name": "Orurillo",
            "sku": "07"
          },
          "200708": {
            "SkuDepPro": "2007",
            "name": "Santa Rosa",
            "sku": "08"
          },
          "200709": {
            "SkuDepPro": "2007",
            "name": "Umachiri",
            "sku": "09"
          },
          "200801": {
            "SkuDepPro": "2008",
            "name": "Sandia",
            "sku": "01"
          },
          "200803": {
            "SkuDepPro": "2008",
            "name": "Cuyocuyo",
            "sku": "03"
          },
          "200804": {
            "SkuDepPro": "2008",
            "name": "Limbani",
            "sku": "04"
          },
          "200805": {
            "SkuDepPro": "2008",
            "name": "Phara",
            "sku": "05"
          },
          "200806": {
            "SkuDepPro": "2008",
            "name": "Patambuco",
            "sku": "06"
          },
          "200807": {
            "SkuDepPro": "2008",
            "name": "Quiaca",
            "sku": "07"
          },
          "200808": {
            "SkuDepPro": "2008",
            "name": "San Juan Del Oro",
            "sku": "08"
          },
          "200810": {
            "SkuDepPro": "2008",
            "name": "Yanahuaya",
            "sku": "10"
          },
          "200811": {
            "SkuDepPro": "2008",
            "name": "Alto Inambari",
            "sku": "11"
          },
          "200812": {
            "SkuDepPro": "2008",
            "name": "San Pedro De Putina Punco",
            "sku": "12"
          },
          "200901": {
            "SkuDepPro": "2009",
            "name": "Juliaca",
            "sku": "01"
          },
          "200902": {
            "SkuDepPro": "2009",
            "name": "Cabana",
            "sku": "02"
          },
          "200903": {
            "SkuDepPro": "2009",
            "name": "Cabanillas",
            "sku": "03"
          },
          "200904": {
            "SkuDepPro": "2009",
            "name": "Caracoto",
            "sku": "04"
          },
          "201001": {
            "SkuDepPro": "2010",
            "name": "Yunguyo",
            "sku": "01"
          },
          "201002": {
            "SkuDepPro": "2010",
            "name": "Unicachi",
            "sku": "02"
          },
          "201003": {
            "SkuDepPro": "2010",
            "name": "Anapia",
            "sku": "03"
          },
          "201004": {
            "SkuDepPro": "2010",
            "name": "Copani",
            "sku": "04"
          },
          "201005": {
            "SkuDepPro": "2010",
            "name": "Cuturapi",
            "sku": "05"
          },
          "201006": {
            "SkuDepPro": "2010",
            "name": "Ollaraya",
            "sku": "06"
          },
          "201007": {
            "SkuDepPro": "2010",
            "name": "Tinicachi",
            "sku": "07"
          },
          "201101": {
            "SkuDepPro": "2011",
            "name": "Putina",
            "sku": "01"
          },
          "201102": {
            "SkuDepPro": "2011",
            "name": "Pedro Vilca Apaza",
            "sku": "02"
          },
          "201103": {
            "SkuDepPro": "2011",
            "name": "Quilcapuncu",
            "sku": "03"
          },
          "201104": {
            "SkuDepPro": "2011",
            "name": "Ananea",
            "sku": "04"
          },
          "201105": {
            "SkuDepPro": "2011",
            "name": "Sina",
            "sku": "05"
          },
          "201201": {
            "SkuDepPro": "2012",
            "name": "Ilave",
            "sku": "01"
          },
          "201202": {
            "SkuDepPro": "2012",
            "name": "Pilcuyo",
            "sku": "02"
          },
          "201203": {
            "SkuDepPro": "2012",
            "name": "Santa Rosa",
            "sku": "03"
          },
          "201204": {
            "SkuDepPro": "2012",
            "name": "Capaso",
            "sku": "04"
          },
          "201205": {
            "SkuDepPro": "2012",
            "name": "Conduriri",
            "sku": "05"
          },
          "201301": {
            "SkuDepPro": "2013",
            "name": "Moho",
            "sku": "01"
          },
          "201302": {
            "SkuDepPro": "2013",
            "name": "Conima",
            "sku": "02"
          },
          "201303": {
            "SkuDepPro": "2013",
            "name": "Tilali",
            "sku": "03"
          },
          "201304": {
            "SkuDepPro": "2013",
            "name": "Huayrapata",
            "sku": "04"
          },
          "210101": {
            "SkuDepPro": "2101",
            "name": "Moyobamba",
            "sku": "01"
          },
          "210102": {
            "SkuDepPro": "2101",
            "name": "Calzada",
            "sku": "02"
          },
          "210103": {
            "SkuDepPro": "2101",
            "name": "Habana",
            "sku": "03"
          },
          "210104": {
            "SkuDepPro": "2101",
            "name": "Jepelacio",
            "sku": "04"
          },
          "210105": {
            "SkuDepPro": "2101",
            "name": "Soritor",
            "sku": "05"
          },
          "210106": {
            "SkuDepPro": "2101",
            "name": "Yantalo",
            "sku": "06"
          },
          "210201": {
            "SkuDepPro": "2102",
            "name": "Saposoa",
            "sku": "01"
          },
          "210202": {
            "SkuDepPro": "2102",
            "name": "Piscoyacu",
            "sku": "02"
          },
          "210203": {
            "SkuDepPro": "2102",
            "name": "Sacanche",
            "sku": "03"
          },
          "210204": {
            "SkuDepPro": "2102",
            "name": "Tingo De Saposoa",
            "sku": "04"
          },
          "210205": {
            "SkuDepPro": "2102",
            "name": "Alto Saposoa",
            "sku": "05"
          },
          "210206": {
            "SkuDepPro": "2102",
            "name": "El Eslabon",
            "sku": "06"
          },
          "210301": {
            "SkuDepPro": "2103",
            "name": "Lamas",
            "sku": "01"
          },
          "210303": {
            "SkuDepPro": "2103",
            "name": "Barranquita",
            "sku": "03"
          },
          "210304": {
            "SkuDepPro": "2103",
            "name": "Caynarachi",
            "sku": "04"
          },
          "210305": {
            "SkuDepPro": "2103",
            "name": "Cuñumbuqui",
            "sku": "05"
          },
          "210306": {
            "SkuDepPro": "2103",
            "name": "Pinto Recodo",
            "sku": "06"
          },
          "210307": {
            "SkuDepPro": "2103",
            "name": "Rumisapa",
            "sku": "07"
          },
          "210311": {
            "SkuDepPro": "2103",
            "name": "Shanao",
            "sku": "11"
          },
          "210313": {
            "SkuDepPro": "2103",
            "name": "Tabalosos",
            "sku": "13"
          },
          "210314": {
            "SkuDepPro": "2103",
            "name": "Zapatero",
            "sku": "14"
          },
          "210315": {
            "SkuDepPro": "2103",
            "name": "Alonso De Alvarado",
            "sku": "15"
          },
          "210316": {
            "SkuDepPro": "2103",
            "name": "San Roque De Cumbaza",
            "sku": "16"
          },
          "210401": {
            "SkuDepPro": "2104",
            "name": "Juanjui",
            "sku": "01"
          },
          "210402": {
            "SkuDepPro": "2104",
            "name": "Campanilla",
            "sku": "02"
          },
          "210403": {
            "SkuDepPro": "2104",
            "name": "Huicungo",
            "sku": "03"
          },
          "210404": {
            "SkuDepPro": "2104",
            "name": "Pachiza",
            "sku": "04"
          },
          "210405": {
            "SkuDepPro": "2104",
            "name": "Pajarillo",
            "sku": "05"
          },
          "210501": {
            "SkuDepPro": "2105",
            "name": "Rioja",
            "sku": "01"
          },
          "210502": {
            "SkuDepPro": "2105",
            "name": "Posic",
            "sku": "02"
          },
          "210503": {
            "SkuDepPro": "2105",
            "name": "Yorongos",
            "sku": "03"
          },
          "210504": {
            "SkuDepPro": "2105",
            "name": "Yuracyacu",
            "sku": "04"
          },
          "210505": {
            "SkuDepPro": "2105",
            "name": "Nueva Cajamarca",
            "sku": "05"
          },
          "210506": {
            "SkuDepPro": "2105",
            "name": "Elias Soplin Vargas",
            "sku": "06"
          },
          "210507": {
            "SkuDepPro": "2105",
            "name": "San Fernando",
            "sku": "07"
          },
          "210508": {
            "SkuDepPro": "2105",
            "name": "Pardo Miguel",
            "sku": "08"
          },
          "210509": {
            "SkuDepPro": "2105",
            "name": "Awajun",
            "sku": "09"
          },
          "210601": {
            "SkuDepPro": "2106",
            "name": "Tarapoto",
            "sku": "01"
          },
          "210602": {
            "SkuDepPro": "2106",
            "name": "Alberto Leveau",
            "sku": "02"
          },
          "210604": {
            "SkuDepPro": "2106",
            "name": "Cacatachi",
            "sku": "04"
          },
          "210606": {
            "SkuDepPro": "2106",
            "name": "Chazuta",
            "sku": "06"
          },
          "210607": {
            "SkuDepPro": "2106",
            "name": "Chipurana",
            "sku": "07"
          },
          "210608": {
            "SkuDepPro": "2106",
            "name": "El Porvenir",
            "sku": "08"
          },
          "210609": {
            "SkuDepPro": "2106",
            "name": "Huimbayoc",
            "sku": "09"
          },
          "210610": {
            "SkuDepPro": "2106",
            "name": "Juan Guerra",
            "sku": "10"
          },
          "210611": {
            "SkuDepPro": "2106",
            "name": "Morales",
            "sku": "11"
          },
          "210612": {
            "SkuDepPro": "2106",
            "name": "Papaplaya",
            "sku": "12"
          },
          "210616": {
            "SkuDepPro": "2106",
            "name": "San Antonio",
            "sku": "16"
          },
          "210619": {
            "SkuDepPro": "2106",
            "name": "Sauce",
            "sku": "19"
          },
          "210620": {
            "SkuDepPro": "2106",
            "name": "Shapaja",
            "sku": "20"
          },
          "210621": {
            "SkuDepPro": "2106",
            "name": "La Banda De Shilcayo",
            "sku": "21"
          },
          "210701": {
            "SkuDepPro": "2107",
            "name": "Bellavista",
            "sku": "01"
          },
          "210702": {
            "SkuDepPro": "2107",
            "name": "San Rafael",
            "sku": "02"
          },
          "210703": {
            "SkuDepPro": "2107",
            "name": "San Pablo",
            "sku": "03"
          },
          "210704": {
            "SkuDepPro": "2107",
            "name": "Alto Biavo",
            "sku": "04"
          },
          "210705": {
            "SkuDepPro": "2107",
            "name": "Huallaga",
            "sku": "05"
          },
          "210706": {
            "SkuDepPro": "2107",
            "name": "Bajo Biavo",
            "sku": "06"
          },
          "210801": {
            "SkuDepPro": "2108",
            "name": "Tocache",
            "sku": "01"
          },
          "210802": {
            "SkuDepPro": "2108",
            "name": "Nuevo Progreso",
            "sku": "02"
          },
          "210803": {
            "SkuDepPro": "2108",
            "name": "Polvora",
            "sku": "03"
          },
          "210804": {
            "SkuDepPro": "2108",
            "name": "Shunte",
            "sku": "04"
          },
          "210805": {
            "SkuDepPro": "2108",
            "name": "Uchiza",
            "sku": "05"
          },
          "210901": {
            "SkuDepPro": "2109",
            "name": "Picota",
            "sku": "01"
          },
          "210902": {
            "SkuDepPro": "2109",
            "name": "Buenos Aires",
            "sku": "02"
          },
          "210903": {
            "SkuDepPro": "2109",
            "name": "Caspizapa",
            "sku": "03"
          },
          "210904": {
            "SkuDepPro": "2109",
            "name": "Pilluana",
            "sku": "04"
          },
          "210905": {
            "SkuDepPro": "2109",
            "name": "Pucacaca",
            "sku": "05"
          },
          "210906": {
            "SkuDepPro": "2109",
            "name": "San Cristobal",
            "sku": "06"
          },
          "210907": {
            "SkuDepPro": "2109",
            "name": "San Hilarion",
            "sku": "07"
          },
          "210908": {
            "SkuDepPro": "2109",
            "name": "Tingo De Ponasa",
            "sku": "08"
          },
          "210909": {
            "SkuDepPro": "2109",
            "name": "Tres Unidos",
            "sku": "09"
          },
          "210910": {
            "SkuDepPro": "2109",
            "name": "Shamboyacu",
            "sku": "10"
          },
          "211001": {
            "SkuDepPro": "2110",
            "name": "San Jose De Sisa",
            "sku": "01"
          },
          "211002": {
            "SkuDepPro": "2110",
            "name": "Agua Blanca",
            "sku": "02"
          },
          "211003": {
            "SkuDepPro": "2110",
            "name": "Shatoja",
            "sku": "03"
          },
          "211004": {
            "SkuDepPro": "2110",
            "name": "San Martín",
            "sku": "04"
          },
          "211005": {
            "SkuDepPro": "2110",
            "name": "Santa Rosa",
            "sku": "05"
          },
          "220101": {
            "SkuDepPro": "2201",
            "name": "Tacna",
            "sku": "01"
          },
          "220102": {
            "SkuDepPro": "2201",
            "name": "Calana",
            "sku": "02"
          },
          "220104": {
            "SkuDepPro": "2201",
            "name": "Inclan",
            "sku": "04"
          },
          "220107": {
            "SkuDepPro": "2201",
            "name": "Pachia",
            "sku": "07"
          },
          "220108": {
            "SkuDepPro": "2201",
            "name": "Palca",
            "sku": "08"
          },
          "220109": {
            "SkuDepPro": "2201",
            "name": "Pocollay",
            "sku": "09"
          },
          "220110": {
            "SkuDepPro": "2201",
            "name": "Sama",
            "sku": "10"
          },
          "220111": {
            "SkuDepPro": "2201",
            "name": "Alto De La Alianza",
            "sku": "11"
          },
          "220112": {
            "SkuDepPro": "2201",
            "name": "Ciudad Nueva",
            "sku": "12"
          },
          "220113": {
            "SkuDepPro": "2201",
            "name": "Coronel Gregorio Albarracin L.",
            "sku": "13"
          },
          "220201": {
            "SkuDepPro": "2202",
            "name": "Tarata",
            "sku": "01"
          },
          "220205": {
            "SkuDepPro": "2202",
            "name": "Heroes Albarracin",
            "sku": "05"
          },
          "220206": {
            "SkuDepPro": "2202",
            "name": "Estique",
            "sku": "06"
          },
          "220207": {
            "SkuDepPro": "2202",
            "name": "Estique Pampa",
            "sku": "07"
          },
          "220210": {
            "SkuDepPro": "2202",
            "name": "Sitajara",
            "sku": "10"
          },
          "220211": {
            "SkuDepPro": "2202",
            "name": "Susapaya",
            "sku": "11"
          },
          "220212": {
            "SkuDepPro": "2202",
            "name": "Tarucachi",
            "sku": "12"
          },
          "220213": {
            "SkuDepPro": "2202",
            "name": "Ticaco",
            "sku": "13"
          },
          "220301": {
            "SkuDepPro": "2203",
            "name": "Locumba",
            "sku": "01"
          },
          "220302": {
            "SkuDepPro": "2203",
            "name": "Ite",
            "sku": "02"
          },
          "220303": {
            "SkuDepPro": "2203",
            "name": "Ilabaya",
            "sku": "03"
          },
          "220401": {
            "SkuDepPro": "2204",
            "name": "Candarave",
            "sku": "01"
          },
          "220402": {
            "SkuDepPro": "2204",
            "name": "Cairani",
            "sku": "02"
          },
          "220403": {
            "SkuDepPro": "2204",
            "name": "Curibaya",
            "sku": "03"
          },
          "220404": {
            "SkuDepPro": "2204",
            "name": "Huanuara",
            "sku": "04"
          },
          "220405": {
            "SkuDepPro": "2204",
            "name": "Quilahuani",
            "sku": "05"
          },
          "220406": {
            "SkuDepPro": "2204",
            "name": "Camilaca",
            "sku": "06"
          },
          "230101": {
            "SkuDepPro": "2301",
            "name": "Tumbes",
            "sku": "01"
          },
          "230102": {
            "SkuDepPro": "2301",
            "name": "Corrales",
            "sku": "02"
          },
          "230103": {
            "SkuDepPro": "2301",
            "name": "La Cruz",
            "sku": "03"
          },
          "230104": {
            "SkuDepPro": "2301",
            "name": "Pampas De Hospital",
            "sku": "04"
          },
          "230105": {
            "SkuDepPro": "2301",
            "name": "San Jacinto",
            "sku": "05"
          },
          "230106": {
            "SkuDepPro": "2301",
            "name": "San Juan De La Virgen",
            "sku": "06"
          },
          "230201": {
            "SkuDepPro": "2302",
            "name": "Zorritos",
            "sku": "01"
          },
          "230202": {
            "SkuDepPro": "2302",
            "name": "Casitas",
            "sku": "02"
          },
          "230203": {
            "SkuDepPro": "2302",
            "name": "Canoas De Punta Sal",
            "sku": "03"
          },
          "230301": {
            "SkuDepPro": "2303",
            "name": "Zarumilla",
            "sku": "01"
          },
          "230302": {
            "SkuDepPro": "2303",
            "name": "Matapalo",
            "sku": "02"
          },
          "230303": {
            "SkuDepPro": "2303",
            "name": "Papayal",
            "sku": "03"
          },
          "230304": {
            "SkuDepPro": "2303",
            "name": "Aguas Verdes",
            "sku": "04"
          },
          "240101": {
            "SkuDepPro": "2401",
            "name": "Callao",
            "sku": "01"
          },
          "240102": {
            "SkuDepPro": "2401",
            "name": "Bellavista",
            "sku": "02"
          },
          "240103": {
            "SkuDepPro": "2401",
            "name": "La Punta",
            "sku": "03"
          },
          "240104": {
            "SkuDepPro": "2401",
            "name": "Carmen De La Legua-Reynoso",
            "sku": "04"
          },
          "240105": {
            "SkuDepPro": "2401",
            "name": "La Perla",
            "sku": "05"
          },
          "240106": {
            "SkuDepPro": "2401",
            "name": "Ventanilla",
            "sku": "06"
          },
          "250101": {
            "SkuDepPro": "2501",
            "name": "Calleria",
            "sku": "01"
          },
          "250102": {
            "SkuDepPro": "2501",
            "name": "Yarinacocha",
            "sku": "02"
          },
          "250103": {
            "SkuDepPro": "2501",
            "name": "Masisea",
            "sku": "03"
          },
          "250104": {
            "SkuDepPro": "2501",
            "name": "Campoverde",
            "sku": "04"
          },
          "250105": {
            "SkuDepPro": "2501",
            "name": "Iparia",
            "sku": "05"
          },
          "250106": {
            "SkuDepPro": "2501",
            "name": "Nueva Requena",
            "sku": "06"
          },
          "250107": {
            "SkuDepPro": "2501",
            "name": "Manantay",
            "sku": "07"
          },
          "250201": {
            "SkuDepPro": "2502",
            "name": "Padre Abad",
            "sku": "01"
          },
          "250202": {
            "SkuDepPro": "2502",
            "name": "Irazola",
            "sku": "02"
          },
          "250203": {
            "SkuDepPro": "2502",
            "name": "Curimana",
            "sku": "03"
          },
          "250301": {
            "SkuDepPro": "2503",
            "name": "Raimondi",
            "sku": "01"
          },
          "250302": {
            "SkuDepPro": "2503",
            "name": "Tahuania",
            "sku": "02"
          },
          "250303": {
            "SkuDepPro": "2503",
            "name": "Yurua",
            "sku": "03"
          },
          "250304": {
            "SkuDepPro": "2503",
            "name": "Sepahua",
            "sku": "04"
          },
          "250401": {
            "SkuDepPro": "2504",
            "name": "Purus",
            "sku": "01"
          },
          "010101": {
            "SkuDepPro": "0101",
            "name": "Chachapoyas",
            "sku": "01"
          },
          "010102": {
            "SkuDepPro": "0101",
            "name": "Asuncion",
            "sku": "02"
          },
          "010103": {
            "SkuDepPro": "0101",
            "name": "Balsas",
            "sku": "03"
          },
          "010104": {
            "SkuDepPro": "0101",
            "name": "Cheto",
            "sku": "04"
          },
          "010105": {
            "SkuDepPro": "0101",
            "name": "Chiliquin",
            "sku": "05"
          },
          "010106": {
            "SkuDepPro": "0101",
            "name": "Chuquibamba",
            "sku": "06"
          },
          "010107": {
            "SkuDepPro": "0101",
            "name": "Granada",
            "sku": "07"
          },
          "010108": {
            "SkuDepPro": "0101",
            "name": "Huancas",
            "sku": "08"
          },
          "010109": {
            "SkuDepPro": "0101",
            "name": "La Jalca",
            "sku": "09"
          },
          "010110": {
            "SkuDepPro": "0101",
            "name": "Leimebamba",
            "sku": "10"
          },
          "010111": {
            "SkuDepPro": "0101",
            "name": "Levanto",
            "sku": "11"
          },
          "010112": {
            "SkuDepPro": "0101",
            "name": "Magdalena",
            "sku": "12"
          },
          "010113": {
            "SkuDepPro": "0101",
            "name": "Mariscal Castilla",
            "sku": "13"
          },
          "010114": {
            "SkuDepPro": "0101",
            "name": "Molinopampa",
            "sku": "14"
          },
          "010115": {
            "SkuDepPro": "0101",
            "name": "Montevideo",
            "sku": "15"
          },
          "010116": {
            "SkuDepPro": "0101",
            "name": "Olleros",
            "sku": "16"
          },
          "010117": {
            "SkuDepPro": "0101",
            "name": "Quinjalca",
            "sku": "17"
          },
          "010118": {
            "SkuDepPro": "0101",
            "name": "San Francisco De Daguas",
            "sku": "18"
          },
          "010119": {
            "SkuDepPro": "0101",
            "name": "San Isidro De Maino",
            "sku": "19"
          },
          "010120": {
            "SkuDepPro": "0101",
            "name": "Soloco",
            "sku": "20"
          },
          "010121": {
            "SkuDepPro": "0101",
            "name": "Sonche",
            "sku": "21"
          },
          "010201": {
            "SkuDepPro": "0102",
            "name": "La Peca",
            "sku": "01"
          },
          "010202": {
            "SkuDepPro": "0102",
            "name": "Aramango",
            "sku": "02"
          },
          "010203": {
            "SkuDepPro": "0102",
            "name": "Copallin",
            "sku": "03"
          },
          "010204": {
            "SkuDepPro": "0102",
            "name": "El Parco",
            "sku": "04"
          },
          "010205": {
            "SkuDepPro": "0102",
            "name": "Bagua",
            "sku": "05"
          },
          "010206": {
            "SkuDepPro": "0102",
            "name": "Imaza",
            "sku": "06"
          },
          "010301": {
            "SkuDepPro": "0103",
            "name": "Jumbilla",
            "sku": "01"
          },
          "010302": {
            "SkuDepPro": "0103",
            "name": "Corosha",
            "sku": "02"
          },
          "010303": {
            "SkuDepPro": "0103",
            "name": "Cuispes",
            "sku": "03"
          },
          "010304": {
            "SkuDepPro": "0103",
            "name": "Chisquilla",
            "sku": "04"
          },
          "010305": {
            "SkuDepPro": "0103",
            "name": "Churuja",
            "sku": "05"
          },
          "010306": {
            "SkuDepPro": "0103",
            "name": "Florida",
            "sku": "06"
          },
          "010307": {
            "SkuDepPro": "0103",
            "name": "Recta",
            "sku": "07"
          },
          "010308": {
            "SkuDepPro": "0103",
            "name": "San Carlos",
            "sku": "08"
          },
          "010309": {
            "SkuDepPro": "0103",
            "name": "Shipasbamba",
            "sku": "09"
          },
          "010310": {
            "SkuDepPro": "0103",
            "name": "Valera",
            "sku": "10"
          },
          "010311": {
            "SkuDepPro": "0103",
            "name": "Yambrasbamba",
            "sku": "11"
          },
          "010312": {
            "SkuDepPro": "0103",
            "name": "Jazan",
            "sku": "12"
          },
          "010401": {
            "SkuDepPro": "0104",
            "name": "Lamud",
            "sku": "01"
          },
          "010402": {
            "SkuDepPro": "0104",
            "name": "Camporredondo",
            "sku": "02"
          },
          "010403": {
            "SkuDepPro": "0104",
            "name": "Cocabamba",
            "sku": "03"
          },
          "010404": {
            "SkuDepPro": "0104",
            "name": "Colcamar",
            "sku": "04"
          },
          "010405": {
            "SkuDepPro": "0104",
            "name": "Conila",
            "sku": "05"
          },
          "010406": {
            "SkuDepPro": "0104",
            "name": "Inguilpata",
            "sku": "06"
          },
          "010407": {
            "SkuDepPro": "0104",
            "name": "Longuita",
            "sku": "07"
          },
          "010408": {
            "SkuDepPro": "0104",
            "name": "Lonya Chico",
            "sku": "08"
          },
          "010409": {
            "SkuDepPro": "0104",
            "name": "Luya",
            "sku": "09"
          },
          "010410": {
            "SkuDepPro": "0104",
            "name": "Luya Viejo",
            "sku": "10"
          },
          "010411": {
            "SkuDepPro": "0104",
            "name": "Maria",
            "sku": "11"
          },
          "010412": {
            "SkuDepPro": "0104",
            "name": "Ocalli",
            "sku": "12"
          },
          "010413": {
            "SkuDepPro": "0104",
            "name": "Ocumal",
            "sku": "13"
          },
          "010414": {
            "SkuDepPro": "0104",
            "name": "Pisuquia",
            "sku": "14"
          },
          "010415": {
            "SkuDepPro": "0104",
            "name": "San Cristobal",
            "sku": "15"
          },
          "010416": {
            "SkuDepPro": "0104",
            "name": "San Francisco De Yeso",
            "sku": "16"
          },
          "010417": {
            "SkuDepPro": "0104",
            "name": "San Jeronimo",
            "sku": "17"
          },
          "010418": {
            "SkuDepPro": "0104",
            "name": "San Juan De Lopecancha",
            "sku": "18"
          },
          "010419": {
            "SkuDepPro": "0104",
            "name": "Santa Catalina",
            "sku": "19"
          },
          "010420": {
            "SkuDepPro": "0104",
            "name": "Santo Tomas",
            "sku": "20"
          },
          "010421": {
            "SkuDepPro": "0104",
            "name": "Tingo",
            "sku": "21"
          },
          "010422": {
            "SkuDepPro": "0104",
            "name": "Trita",
            "sku": "22"
          },
          "010423": {
            "SkuDepPro": "0104",
            "name": "Providencia",
            "sku": "23"
          },
          "010501": {
            "SkuDepPro": "0105",
            "name": "San Nicolas",
            "sku": "01"
          },
          "010502": {
            "SkuDepPro": "0105",
            "name": "Cochamal",
            "sku": "02"
          },
          "010503": {
            "SkuDepPro": "0105",
            "name": "Chirimoto",
            "sku": "03"
          },
          "010504": {
            "SkuDepPro": "0105",
            "name": "Huambo",
            "sku": "04"
          },
          "010505": {
            "SkuDepPro": "0105",
            "name": "Limabamba",
            "sku": "05"
          },
          "010506": {
            "SkuDepPro": "0105",
            "name": "Longar",
            "sku": "06"
          },
          "010507": {
            "SkuDepPro": "0105",
            "name": "Milpucc",
            "sku": "07"
          },
          "010508": {
            "SkuDepPro": "0105",
            "name": "Mariscal Benavides",
            "sku": "08"
          },
          "010509": {
            "SkuDepPro": "0105",
            "name": "Omia",
            "sku": "09"
          },
          "010510": {
            "SkuDepPro": "0105",
            "name": "Santa Rosa",
            "sku": "10"
          },
          "010511": {
            "SkuDepPro": "0105",
            "name": "Totora",
            "sku": "11"
          },
          "010512": {
            "SkuDepPro": "0105",
            "name": "Vista Alegre",
            "sku": "12"
          },
          "010601": {
            "SkuDepPro": "0106",
            "name": "Nieva",
            "sku": "01"
          },
          "010602": {
            "SkuDepPro": "0106",
            "name": "Rio Santiago",
            "sku": "02"
          },
          "010603": {
            "SkuDepPro": "0106",
            "name": "El Cenepa",
            "sku": "03"
          },
          "010701": {
            "SkuDepPro": "0107",
            "name": "Bagua Grande",
            "sku": "01"
          },
          "010702": {
            "SkuDepPro": "0107",
            "name": "Cajaruro",
            "sku": "02"
          },
          "010703": {
            "SkuDepPro": "0107",
            "name": "Cumba",
            "sku": "03"
          },
          "010704": {
            "SkuDepPro": "0107",
            "name": "El Milagro",
            "sku": "04"
          },
          "010705": {
            "SkuDepPro": "0107",
            "name": "Jamalca",
            "sku": "05"
          },
          "010706": {
            "SkuDepPro": "0107",
            "name": "Lonya Grande",
            "sku": "06"
          },
          "010707": {
            "SkuDepPro": "0107",
            "name": "Yamon",
            "sku": "07"
          },
          "020101": {
            "SkuDepPro": "0201",
            "name": "Huaraz",
            "sku": "01"
          },
          "020102": {
            "SkuDepPro": "0201",
            "name": "Independencia",
            "sku": "02"
          },
          "020103": {
            "SkuDepPro": "0201",
            "name": "Cochabamba",
            "sku": "03"
          },
          "020104": {
            "SkuDepPro": "0201",
            "name": "Colcabamba",
            "sku": "04"
          },
          "020105": {
            "SkuDepPro": "0201",
            "name": "Huanchay",
            "sku": "05"
          },
          "020106": {
            "SkuDepPro": "0201",
            "name": "Jangas",
            "sku": "06"
          },
          "020107": {
            "SkuDepPro": "0201",
            "name": "La Libertad",
            "sku": "07"
          },
          "020108": {
            "SkuDepPro": "0201",
            "name": "Olleros",
            "sku": "08"
          },
          "020109": {
            "SkuDepPro": "0201",
            "name": "Pampas Grande",
            "sku": "09"
          },
          "020110": {
            "SkuDepPro": "0201",
            "name": "Pariacoto",
            "sku": "10"
          },
          "020111": {
            "SkuDepPro": "0201",
            "name": "Pira",
            "sku": "11"
          },
          "020112": {
            "SkuDepPro": "0201",
            "name": "Tarica",
            "sku": "12"
          },
          "020201": {
            "SkuDepPro": "0202",
            "name": "Aija",
            "sku": "01"
          },
          "020203": {
            "SkuDepPro": "0202",
            "name": "Coris",
            "sku": "03"
          },
          "020205": {
            "SkuDepPro": "0202",
            "name": "Huacllan",
            "sku": "05"
          },
          "020206": {
            "SkuDepPro": "0202",
            "name": "La Merced",
            "sku": "06"
          },
          "020208": {
            "SkuDepPro": "0202",
            "name": "Succha",
            "sku": "08"
          },
          "020301": {
            "SkuDepPro": "0203",
            "name": "Chiquian",
            "sku": "01"
          },
          "020302": {
            "SkuDepPro": "0203",
            "name": "Abelardo Pardo Lezameta",
            "sku": "02"
          },
          "020304": {
            "SkuDepPro": "0203",
            "name": "Aquia",
            "sku": "04"
          },
          "020305": {
            "SkuDepPro": "0203",
            "name": "Cajacay",
            "sku": "05"
          },
          "020310": {
            "SkuDepPro": "0203",
            "name": "Huayllacayan",
            "sku": "10"
          },
          "020311": {
            "SkuDepPro": "0203",
            "name": "Huasta",
            "sku": "11"
          },
          "020313": {
            "SkuDepPro": "0203",
            "name": "Mangas",
            "sku": "13"
          },
          "020315": {
            "SkuDepPro": "0203",
            "name": "Pacllon",
            "sku": "15"
          },
          "020317": {
            "SkuDepPro": "0203",
            "name": "San Miguel De Corpanqui",
            "sku": "17"
          },
          "020320": {
            "SkuDepPro": "0203",
            "name": "Ticllos",
            "sku": "20"
          },
          "020321": {
            "SkuDepPro": "0203",
            "name": "Antonio Raimondi",
            "sku": "21"
          },
          "020322": {
            "SkuDepPro": "0203",
            "name": "Canis",
            "sku": "22"
          },
          "020323": {
            "SkuDepPro": "0203",
            "name": "Colquioc",
            "sku": "23"
          },
          "020324": {
            "SkuDepPro": "0203",
            "name": "La Primavera",
            "sku": "24"
          },
          "020325": {
            "SkuDepPro": "0203",
            "name": "Huallanca",
            "sku": "25"
          },
          "020401": {
            "SkuDepPro": "0204",
            "name": "Carhuaz",
            "sku": "01"
          },
          "020402": {
            "SkuDepPro": "0204",
            "name": "Acopampa",
            "sku": "02"
          },
          "020403": {
            "SkuDepPro": "0204",
            "name": "Amashca",
            "sku": "03"
          },
          "020404": {
            "SkuDepPro": "0204",
            "name": "Anta",
            "sku": "04"
          },
          "020405": {
            "SkuDepPro": "0204",
            "name": "Ataquero",
            "sku": "05"
          },
          "020406": {
            "SkuDepPro": "0204",
            "name": "Marcara",
            "sku": "06"
          },
          "020407": {
            "SkuDepPro": "0204",
            "name": "Pariahuanca",
            "sku": "07"
          },
          "020408": {
            "SkuDepPro": "0204",
            "name": "San Miguel De Aco",
            "sku": "08"
          },
          "020409": {
            "SkuDepPro": "0204",
            "name": "Shilla",
            "sku": "09"
          },
          "020410": {
            "SkuDepPro": "0204",
            "name": "Tinco",
            "sku": "10"
          },
          "020411": {
            "SkuDepPro": "0204",
            "name": "Yungar",
            "sku": "11"
          },
          "020501": {
            "SkuDepPro": "0205",
            "name": "Casma",
            "sku": "01"
          },
          "020502": {
            "SkuDepPro": "0205",
            "name": "Buena Vista Alta",
            "sku": "02"
          },
          "020503": {
            "SkuDepPro": "0205",
            "name": "Comandante Noel",
            "sku": "03"
          },
          "020505": {
            "SkuDepPro": "0205",
            "name": "Yautan",
            "sku": "05"
          },
          "020601": {
            "SkuDepPro": "0206",
            "name": "Corongo",
            "sku": "01"
          },
          "020602": {
            "SkuDepPro": "0206",
            "name": "Aco",
            "sku": "02"
          },
          "020603": {
            "SkuDepPro": "0206",
            "name": "Bambas",
            "sku": "03"
          },
          "020604": {
            "SkuDepPro": "0206",
            "name": "Cusca",
            "sku": "04"
          },
          "020605": {
            "SkuDepPro": "0206",
            "name": "La Pampa",
            "sku": "05"
          },
          "020606": {
            "SkuDepPro": "0206",
            "name": "Yanac",
            "sku": "06"
          },
          "020607": {
            "SkuDepPro": "0206",
            "name": "Yupan",
            "sku": "07"
          },
          "020701": {
            "SkuDepPro": "0207",
            "name": "Caraz",
            "sku": "01"
          },
          "020702": {
            "SkuDepPro": "0207",
            "name": "Huallanca",
            "sku": "02"
          },
          "020703": {
            "SkuDepPro": "0207",
            "name": "Huata",
            "sku": "03"
          },
          "020704": {
            "SkuDepPro": "0207",
            "name": "Huaylas",
            "sku": "04"
          },
          "020705": {
            "SkuDepPro": "0207",
            "name": "Mato",
            "sku": "05"
          },
          "020706": {
            "SkuDepPro": "0207",
            "name": "Pamparomas",
            "sku": "06"
          },
          "020707": {
            "SkuDepPro": "0207",
            "name": "Pueblo Libre",
            "sku": "07"
          },
          "020708": {
            "SkuDepPro": "0207",
            "name": "Santa Cruz",
            "sku": "08"
          },
          "020709": {
            "SkuDepPro": "0207",
            "name": "Yuracmarca",
            "sku": "09"
          },
          "020710": {
            "SkuDepPro": "0207",
            "name": "Santo Toribio",
            "sku": "10"
          },
          "020801": {
            "SkuDepPro": "0208",
            "name": "Huari",
            "sku": "01"
          },
          "020802": {
            "SkuDepPro": "0208",
            "name": "Cajay",
            "sku": "02"
          },
          "020803": {
            "SkuDepPro": "0208",
            "name": "Chavin De Huantar",
            "sku": "03"
          },
          "020804": {
            "SkuDepPro": "0208",
            "name": "Huacachi",
            "sku": "04"
          },
          "020805": {
            "SkuDepPro": "0208",
            "name": "Huachis",
            "sku": "05"
          },
          "020806": {
            "SkuDepPro": "0208",
            "name": "Huacchis",
            "sku": "06"
          },
          "020807": {
            "SkuDepPro": "0208",
            "name": "Huantar",
            "sku": "07"
          },
          "020808": {
            "SkuDepPro": "0208",
            "name": "Masin",
            "sku": "08"
          },
          "020809": {
            "SkuDepPro": "0208",
            "name": "Paucas",
            "sku": "09"
          },
          "020810": {
            "SkuDepPro": "0208",
            "name": "Ponto",
            "sku": "10"
          },
          "020811": {
            "SkuDepPro": "0208",
            "name": "Rahuapampa",
            "sku": "11"
          },
          "020812": {
            "SkuDepPro": "0208",
            "name": "Rapayan",
            "sku": "12"
          },
          "020813": {
            "SkuDepPro": "0208",
            "name": "San Marcos",
            "sku": "13"
          },
          "020814": {
            "SkuDepPro": "0208",
            "name": "San Pedro De Chana",
            "sku": "14"
          },
          "020815": {
            "SkuDepPro": "0208",
            "name": "Uco",
            "sku": "15"
          },
          "020816": {
            "SkuDepPro": "0208",
            "name": "Anra",
            "sku": "16"
          },
          "020901": {
            "SkuDepPro": "0209",
            "name": "Piscobamba",
            "sku": "01"
          },
          "020902": {
            "SkuDepPro": "0209",
            "name": "Casca",
            "sku": "02"
          },
          "020903": {
            "SkuDepPro": "0209",
            "name": "Lucma",
            "sku": "03"
          },
          "020904": {
            "SkuDepPro": "0209",
            "name": "Fidel Olivas Escudero",
            "sku": "04"
          },
          "020905": {
            "SkuDepPro": "0209",
            "name": "Llama",
            "sku": "05"
          },
          "020906": {
            "SkuDepPro": "0209",
            "name": "Llumpa",
            "sku": "06"
          },
          "020907": {
            "SkuDepPro": "0209",
            "name": "Musga",
            "sku": "07"
          },
          "020908": {
            "SkuDepPro": "0209",
            "name": "Eleazar Guzman Barron",
            "sku": "08"
          },
          "021001": {
            "SkuDepPro": "0210",
            "name": "Cabana",
            "sku": "01"
          },
          "021002": {
            "SkuDepPro": "0210",
            "name": "Bolognesi",
            "sku": "02"
          },
          "021003": {
            "SkuDepPro": "0210",
            "name": "Conchucos",
            "sku": "03"
          },
          "021004": {
            "SkuDepPro": "0210",
            "name": "Huacaschuque",
            "sku": "04"
          },
          "021005": {
            "SkuDepPro": "0210",
            "name": "Huandoval",
            "sku": "05"
          },
          "021006": {
            "SkuDepPro": "0210",
            "name": "Lacabamba",
            "sku": "06"
          },
          "021007": {
            "SkuDepPro": "0210",
            "name": "Llapo",
            "sku": "07"
          },
          "021008": {
            "SkuDepPro": "0210",
            "name": "Pallasca",
            "sku": "08"
          },
          "021009": {
            "SkuDepPro": "0210",
            "name": "Pampas",
            "sku": "09"
          },
          "021010": {
            "SkuDepPro": "0210",
            "name": "Santa Rosa",
            "sku": "10"
          },
          "021011": {
            "SkuDepPro": "0210",
            "name": "Tauca",
            "sku": "11"
          },
          "021101": {
            "SkuDepPro": "0211",
            "name": "Pomabamba",
            "sku": "01"
          },
          "021102": {
            "SkuDepPro": "0211",
            "name": "Huayllan",
            "sku": "02"
          },
          "021103": {
            "SkuDepPro": "0211",
            "name": "Parobamba",
            "sku": "03"
          },
          "021104": {
            "SkuDepPro": "0211",
            "name": "Quinuabamba",
            "sku": "04"
          },
          "021201": {
            "SkuDepPro": "0212",
            "name": "Recuay",
            "sku": "01"
          },
          "021202": {
            "SkuDepPro": "0212",
            "name": "Cotaparaco",
            "sku": "02"
          },
          "021203": {
            "SkuDepPro": "0212",
            "name": "Huayllapampa",
            "sku": "03"
          },
          "021204": {
            "SkuDepPro": "0212",
            "name": "Marca",
            "sku": "04"
          },
          "021205": {
            "SkuDepPro": "0212",
            "name": "Pampas Chico",
            "sku": "05"
          },
          "021206": {
            "SkuDepPro": "0212",
            "name": "Pararin",
            "sku": "06"
          },
          "021207": {
            "SkuDepPro": "0212",
            "name": "Tapacocha",
            "sku": "07"
          },
          "021208": {
            "SkuDepPro": "0212",
            "name": "Ticapampa",
            "sku": "08"
          },
          "021209": {
            "SkuDepPro": "0212",
            "name": "Llacllin",
            "sku": "09"
          },
          "021210": {
            "SkuDepPro": "0212",
            "name": "Catac",
            "sku": "10"
          },
          "021301": {
            "SkuDepPro": "0213",
            "name": "Chimbote",
            "sku": "01"
          },
          "021302": {
            "SkuDepPro": "0213",
            "name": "Caceres Del Peru",
            "sku": "02"
          },
          "021303": {
            "SkuDepPro": "0213",
            "name": "Macate",
            "sku": "03"
          },
          "021304": {
            "SkuDepPro": "0213",
            "name": "Moro",
            "sku": "04"
          },
          "021305": {
            "SkuDepPro": "0213",
            "name": "Nepeña",
            "sku": "05"
          },
          "021306": {
            "SkuDepPro": "0213",
            "name": "Samanco",
            "sku": "06"
          },
          "021307": {
            "SkuDepPro": "0213",
            "name": "Santa",
            "sku": "07"
          },
          "021308": {
            "SkuDepPro": "0213",
            "name": "Coishco",
            "sku": "08"
          },
          "021309": {
            "SkuDepPro": "0213",
            "name": "Nuevo Chimbote",
            "sku": "09"
          },
          "021401": {
            "SkuDepPro": "0214",
            "name": "Sihuas",
            "sku": "01"
          },
          "021402": {
            "SkuDepPro": "0214",
            "name": "Alfonso Ugarte",
            "sku": "02"
          },
          "021403": {
            "SkuDepPro": "0214",
            "name": "Chingalpo",
            "sku": "03"
          },
          "021404": {
            "SkuDepPro": "0214",
            "name": "Huayllabamba",
            "sku": "04"
          },
          "021405": {
            "SkuDepPro": "0214",
            "name": "Quiches",
            "sku": "05"
          },
          "021406": {
            "SkuDepPro": "0214",
            "name": "Sicsibamba",
            "sku": "06"
          },
          "021407": {
            "SkuDepPro": "0214",
            "name": "Acobamba",
            "sku": "07"
          },
          "021408": {
            "SkuDepPro": "0214",
            "name": "Cashapampa",
            "sku": "08"
          },
          "021409": {
            "SkuDepPro": "0214",
            "name": "Ragash",
            "sku": "09"
          },
          "021410": {
            "SkuDepPro": "0214",
            "name": "San Juan",
            "sku": "10"
          },
          "021501": {
            "SkuDepPro": "0215",
            "name": "Yungay",
            "sku": "01"
          },
          "021502": {
            "SkuDepPro": "0215",
            "name": "Cascapara",
            "sku": "02"
          },
          "021503": {
            "SkuDepPro": "0215",
            "name": "Mancos",
            "sku": "03"
          },
          "021504": {
            "SkuDepPro": "0215",
            "name": "Matacoto",
            "sku": "04"
          },
          "021505": {
            "SkuDepPro": "0215",
            "name": "Quillo",
            "sku": "05"
          },
          "021506": {
            "SkuDepPro": "0215",
            "name": "Ranrahirca",
            "sku": "06"
          },
          "021507": {
            "SkuDepPro": "0215",
            "name": "Shupluy",
            "sku": "07"
          },
          "021508": {
            "SkuDepPro": "0215",
            "name": "Yanama",
            "sku": "08"
          },
          "021601": {
            "SkuDepPro": "0216",
            "name": "Llamellin",
            "sku": "01"
          },
          "021602": {
            "SkuDepPro": "0216",
            "name": "Aczo",
            "sku": "02"
          },
          "021603": {
            "SkuDepPro": "0216",
            "name": "Chaccho",
            "sku": "03"
          },
          "021604": {
            "SkuDepPro": "0216",
            "name": "Chingas",
            "sku": "04"
          },
          "021605": {
            "SkuDepPro": "0216",
            "name": "Mirgas",
            "sku": "05"
          },
          "021606": {
            "SkuDepPro": "0216",
            "name": "San Juan De Rontoy",
            "sku": "06"
          },
          "021701": {
            "SkuDepPro": "0217",
            "name": "San Luis",
            "sku": "01"
          },
          "021702": {
            "SkuDepPro": "0217",
            "name": "Yauya",
            "sku": "02"
          },
          "021703": {
            "SkuDepPro": "0217",
            "name": "San Nicolas",
            "sku": "03"
          },
          "021801": {
            "SkuDepPro": "0218",
            "name": "Chacas",
            "sku": "01"
          },
          "021802": {
            "SkuDepPro": "0218",
            "name": "Acochaca",
            "sku": "02"
          },
          "021901": {
            "SkuDepPro": "0219",
            "name": "Huarmey",
            "sku": "01"
          },
          "021902": {
            "SkuDepPro": "0219",
            "name": "Cochapeti",
            "sku": "02"
          },
          "021903": {
            "SkuDepPro": "0219",
            "name": "Huayan",
            "sku": "03"
          },
          "021904": {
            "SkuDepPro": "0219",
            "name": "Malvas",
            "sku": "04"
          },
          "021905": {
            "SkuDepPro": "0219",
            "name": "Culebras",
            "sku": "05"
          },
          "022001": {
            "SkuDepPro": "0220",
            "name": "Acas",
            "sku": "01"
          },
          "022002": {
            "SkuDepPro": "0220",
            "name": "Cajamarquilla",
            "sku": "02"
          },
          "022003": {
            "SkuDepPro": "0220",
            "name": "Carhuapampa",
            "sku": "03"
          },
          "022004": {
            "SkuDepPro": "0220",
            "name": "Cochas",
            "sku": "04"
          },
          "022005": {
            "SkuDepPro": "0220",
            "name": "Congas",
            "sku": "05"
          },
          "022006": {
            "SkuDepPro": "0220",
            "name": "Llipa",
            "sku": "06"
          },
          "022007": {
            "SkuDepPro": "0220",
            "name": "Ocros",
            "sku": "07"
          },
          "022008": {
            "SkuDepPro": "0220",
            "name": "San Cristobal De Rajan",
            "sku": "08"
          },
          "022009": {
            "SkuDepPro": "0220",
            "name": "San Pedro",
            "sku": "09"
          },
          "022010": {
            "SkuDepPro": "0220",
            "name": "Santiago De Chilcas",
            "sku": "10"
          },
          "030101": {
            "SkuDepPro": "0301",
            "name": "Abancay",
            "sku": "01"
          },
          "030102": {
            "SkuDepPro": "0301",
            "name": "Circa",
            "sku": "02"
          },
          "030103": {
            "SkuDepPro": "0301",
            "name": "Curahuasi",
            "sku": "03"
          },
          "030104": {
            "SkuDepPro": "0301",
            "name": "Chacoche",
            "sku": "04"
          },
          "030105": {
            "SkuDepPro": "0301",
            "name": "Huanipaca",
            "sku": "05"
          },
          "030106": {
            "SkuDepPro": "0301",
            "name": "Lambrama",
            "sku": "06"
          },
          "030107": {
            "SkuDepPro": "0301",
            "name": "Pichirhua",
            "sku": "07"
          },
          "030108": {
            "SkuDepPro": "0301",
            "name": "San Pedro De Cachora",
            "sku": "08"
          },
          "030109": {
            "SkuDepPro": "0301",
            "name": "Tamburco",
            "sku": "09"
          },
          "030201": {
            "SkuDepPro": "0302",
            "name": "Chalhuanca",
            "sku": "01"
          },
          "030202": {
            "SkuDepPro": "0302",
            "name": "Capaya",
            "sku": "02"
          },
          "030203": {
            "SkuDepPro": "0302",
            "name": "Caraybamba",
            "sku": "03"
          },
          "030204": {
            "SkuDepPro": "0302",
            "name": "Colcabamba",
            "sku": "04"
          },
          "030205": {
            "SkuDepPro": "0302",
            "name": "Cotaruse",
            "sku": "05"
          },
          "030206": {
            "SkuDepPro": "0302",
            "name": "Chapimarca",
            "sku": "06"
          },
          "030207": {
            "SkuDepPro": "0302",
            "name": "Huayllo",
            "sku": "07"
          },
          "030208": {
            "SkuDepPro": "0302",
            "name": "Lucre",
            "sku": "08"
          },
          "030209": {
            "SkuDepPro": "0302",
            "name": "Pocohuanca",
            "sku": "09"
          },
          "030210": {
            "SkuDepPro": "0302",
            "name": "Sañayca",
            "sku": "10"
          },
          "030211": {
            "SkuDepPro": "0302",
            "name": "Soraya",
            "sku": "11"
          },
          "030212": {
            "SkuDepPro": "0302",
            "name": "Tapairihua",
            "sku": "12"
          },
          "030213": {
            "SkuDepPro": "0302",
            "name": "Tintay",
            "sku": "13"
          },
          "030214": {
            "SkuDepPro": "0302",
            "name": "Toraya",
            "sku": "14"
          },
          "030215": {
            "SkuDepPro": "0302",
            "name": "Yanaca",
            "sku": "15"
          },
          "030216": {
            "SkuDepPro": "0302",
            "name": "San Juan De Chacña",
            "sku": "16"
          },
          "030217": {
            "SkuDepPro": "0302",
            "name": "Justo Apu Sahuaraura",
            "sku": "17"
          },
          "030301": {
            "SkuDepPro": "0303",
            "name": "Andahuaylas",
            "sku": "01"
          },
          "030302": {
            "SkuDepPro": "0303",
            "name": "Andarapa",
            "sku": "02"
          },
          "030303": {
            "SkuDepPro": "0303",
            "name": "Chiara",
            "sku": "03"
          },
          "030304": {
            "SkuDepPro": "0303",
            "name": "Huancarama",
            "sku": "04"
          },
          "030305": {
            "SkuDepPro": "0303",
            "name": "Huancaray",
            "sku": "05"
          },
          "030306": {
            "SkuDepPro": "0303",
            "name": "Kishuara",
            "sku": "06"
          },
          "030307": {
            "SkuDepPro": "0303",
            "name": "Pacobamba",
            "sku": "07"
          },
          "030308": {
            "SkuDepPro": "0303",
            "name": "Pampachiri",
            "sku": "08"
          },
          "030309": {
            "SkuDepPro": "0303",
            "name": "San Antonio De Cachi",
            "sku": "09"
          },
          "030310": {
            "SkuDepPro": "0303",
            "name": "San Jeronimo",
            "sku": "10"
          },
          "030311": {
            "SkuDepPro": "0303",
            "name": "Talavera",
            "sku": "11"
          },
          "030312": {
            "SkuDepPro": "0303",
            "name": "Turpo",
            "sku": "12"
          },
          "030313": {
            "SkuDepPro": "0303",
            "name": "Pacucha",
            "sku": "13"
          },
          "030314": {
            "SkuDepPro": "0303",
            "name": "Pomacocha",
            "sku": "14"
          },
          "030315": {
            "SkuDepPro": "0303",
            "name": "Santa Maria De Chicmo",
            "sku": "15"
          },
          "030316": {
            "SkuDepPro": "0303",
            "name": "Tumay Huaraca",
            "sku": "16"
          },
          "030317": {
            "SkuDepPro": "0303",
            "name": "Huayana",
            "sku": "17"
          },
          "030318": {
            "SkuDepPro": "0303",
            "name": "San Miguel De Chaccrampa",
            "sku": "18"
          },
          "030319": {
            "SkuDepPro": "0303",
            "name": "Kaquiabamba",
            "sku": "19"
          },
          "030401": {
            "SkuDepPro": "0304",
            "name": "Antabamba",
            "sku": "01"
          },
          "030402": {
            "SkuDepPro": "0304",
            "name": "El Oro",
            "sku": "02"
          },
          "030403": {
            "SkuDepPro": "0304",
            "name": "Huaquirca",
            "sku": "03"
          },
          "030404": {
            "SkuDepPro": "0304",
            "name": "Juan Espinoza Medrano",
            "sku": "04"
          },
          "030405": {
            "SkuDepPro": "0304",
            "name": "Oropesa",
            "sku": "05"
          },
          "030406": {
            "SkuDepPro": "0304",
            "name": "Pachaconas",
            "sku": "06"
          },
          "030407": {
            "SkuDepPro": "0304",
            "name": "Sabaino",
            "sku": "07"
          },
          "030501": {
            "SkuDepPro": "0305",
            "name": "Tambobamba",
            "sku": "01"
          },
          "030502": {
            "SkuDepPro": "0305",
            "name": "Coyllurqui",
            "sku": "02"
          },
          "030503": {
            "SkuDepPro": "0305",
            "name": "Cotabambas",
            "sku": "03"
          },
          "030504": {
            "SkuDepPro": "0305",
            "name": "Haquira",
            "sku": "04"
          },
          "030505": {
            "SkuDepPro": "0305",
            "name": "Mara",
            "sku": "05"
          },
          "030506": {
            "SkuDepPro": "0305",
            "name": "Challhuahuacho",
            "sku": "06"
          },
          "030601": {
            "SkuDepPro": "0306",
            "name": "Chuquibambilla",
            "sku": "01"
          },
          "030602": {
            "SkuDepPro": "0306",
            "name": "Curpahuasi",
            "sku": "02"
          },
          "030603": {
            "SkuDepPro": "0306",
            "name": "Huaillati",
            "sku": "03"
          },
          "030604": {
            "SkuDepPro": "0306",
            "name": "Mamara",
            "sku": "04"
          },
          "030605": {
            "SkuDepPro": "0306",
            "name": "Mariscal Gamarra",
            "sku": "05"
          },
          "030606": {
            "SkuDepPro": "0306",
            "name": "Micaela Bastidas",
            "sku": "06"
          },
          "030607": {
            "SkuDepPro": "0306",
            "name": "Progreso",
            "sku": "07"
          },
          "030608": {
            "SkuDepPro": "0306",
            "name": "Pataypampa",
            "sku": "08"
          },
          "030609": {
            "SkuDepPro": "0306",
            "name": "San Antonio",
            "sku": "09"
          },
          "030610": {
            "SkuDepPro": "0306",
            "name": "Turpay",
            "sku": "10"
          },
          "030611": {
            "SkuDepPro": "0306",
            "name": "Vilcabamba",
            "sku": "11"
          },
          "030612": {
            "SkuDepPro": "0306",
            "name": "Virundo",
            "sku": "12"
          },
          "030613": {
            "SkuDepPro": "0306",
            "name": "Santa Rosa",
            "sku": "13"
          },
          "030614": {
            "SkuDepPro": "0306",
            "name": "Curasco",
            "sku": "14"
          },
          "030701": {
            "SkuDepPro": "0307",
            "name": "Chincheros",
            "sku": "01"
          },
          "030702": {
            "SkuDepPro": "0307",
            "name": "Ongoy",
            "sku": "02"
          },
          "030703": {
            "SkuDepPro": "0307",
            "name": "Ocobamba",
            "sku": "03"
          },
          "030704": {
            "SkuDepPro": "0307",
            "name": "Cocharcas",
            "sku": "04"
          },
          "030705": {
            "SkuDepPro": "0307",
            "name": "Anco Huallo",
            "sku": "05"
          },
          "030706": {
            "SkuDepPro": "0307",
            "name": "Huaccana",
            "sku": "06"
          },
          "030707": {
            "SkuDepPro": "0307",
            "name": "Uranmarca",
            "sku": "07"
          },
          "030708": {
            "SkuDepPro": "0307",
            "name": "Ranracancha",
            "sku": "08"
          },
          "040101": {
            "SkuDepPro": "0401",
            "name": "Arequipa",
            "sku": "01"
          },
          "040102": {
            "SkuDepPro": "0401",
            "name": "Cayma",
            "sku": "02"
          },
          "040103": {
            "SkuDepPro": "0401",
            "name": "Cerro Colorado",
            "sku": "03"
          },
          "040104": {
            "SkuDepPro": "0401",
            "name": "Characato",
            "sku": "04"
          },
          "040105": {
            "SkuDepPro": "0401",
            "name": "Chiguata",
            "sku": "05"
          },
          "040106": {
            "SkuDepPro": "0401",
            "name": "La Joya",
            "sku": "06"
          },
          "040107": {
            "SkuDepPro": "0401",
            "name": "Miraflores",
            "sku": "07"
          },
          "040108": {
            "SkuDepPro": "0401",
            "name": "Mollebaya",
            "sku": "08"
          },
          "040109": {
            "SkuDepPro": "0401",
            "name": "Paucarpata",
            "sku": "09"
          },
          "040110": {
            "SkuDepPro": "0401",
            "name": "Pocsi",
            "sku": "10"
          },
          "040111": {
            "SkuDepPro": "0401",
            "name": "Polobaya",
            "sku": "11"
          },
          "040112": {
            "SkuDepPro": "0401",
            "name": "Quequeña",
            "sku": "12"
          },
          "040113": {
            "SkuDepPro": "0401",
            "name": "Sabandia",
            "sku": "13"
          },
          "040114": {
            "SkuDepPro": "0401",
            "name": "Sachaca",
            "sku": "14"
          },
          "040115": {
            "SkuDepPro": "0401",
            "name": "San Juan De Siguas",
            "sku": "15"
          },
          "040116": {
            "SkuDepPro": "0401",
            "name": "San Juan De Tarucani",
            "sku": "16"
          },
          "040117": {
            "SkuDepPro": "0401",
            "name": "Santa Isabel De Siguas",
            "sku": "17"
          },
          "040118": {
            "SkuDepPro": "0401",
            "name": "Santa Rita De Sihuas",
            "sku": "18"
          },
          "040119": {
            "SkuDepPro": "0401",
            "name": "Socabaya",
            "sku": "19"
          },
          "040120": {
            "SkuDepPro": "0401",
            "name": "Tiabaya",
            "sku": "20"
          },
          "040121": {
            "SkuDepPro": "0401",
            "name": "Uchumayo",
            "sku": "21"
          },
          "040122": {
            "SkuDepPro": "0401",
            "name": "Vitor",
            "sku": "22"
          },
          "040123": {
            "SkuDepPro": "0401",
            "name": "Yanahuara",
            "sku": "23"
          },
          "040124": {
            "SkuDepPro": "0401",
            "name": "Yarabamba",
            "sku": "24"
          },
          "040125": {
            "SkuDepPro": "0401",
            "name": "Yura",
            "sku": "25"
          },
          "040126": {
            "SkuDepPro": "0401",
            "name": "Mariano Melgar",
            "sku": "26"
          },
          "040127": {
            "SkuDepPro": "0401",
            "name": "Jacobo Hunter",
            "sku": "27"
          },
          "040128": {
            "SkuDepPro": "0401",
            "name": "Alto Selva Alegre",
            "sku": "28"
          },
          "040129": {
            "SkuDepPro": "0401",
            "name": "Jose Luis Bustamante Y Rivero",
            "sku": "29"
          },
          "040201": {
            "SkuDepPro": "0402",
            "name": "Chivay",
            "sku": "01"
          },
          "040202": {
            "SkuDepPro": "0402",
            "name": "Achoma",
            "sku": "02"
          },
          "040203": {
            "SkuDepPro": "0402",
            "name": "Cabanaconde",
            "sku": "03"
          },
          "040204": {
            "SkuDepPro": "0402",
            "name": "Caylloma",
            "sku": "04"
          },
          "040205": {
            "SkuDepPro": "0402",
            "name": "Callalli",
            "sku": "05"
          },
          "040206": {
            "SkuDepPro": "0402",
            "name": "Coporaque",
            "sku": "06"
          },
          "040207": {
            "SkuDepPro": "0402",
            "name": "Huambo",
            "sku": "07"
          },
          "040208": {
            "SkuDepPro": "0402",
            "name": "Huanca",
            "sku": "08"
          },
          "040209": {
            "SkuDepPro": "0402",
            "name": "Ichupampa",
            "sku": "09"
          },
          "040210": {
            "SkuDepPro": "0402",
            "name": "Lari",
            "sku": "10"
          },
          "040211": {
            "SkuDepPro": "0402",
            "name": "Lluta",
            "sku": "11"
          },
          "040212": {
            "SkuDepPro": "0402",
            "name": "Maca",
            "sku": "12"
          },
          "040213": {
            "SkuDepPro": "0402",
            "name": "Madrigal",
            "sku": "13"
          },
          "040214": {
            "SkuDepPro": "0402",
            "name": "San Antonio De Chuca",
            "sku": "14"
          },
          "040215": {
            "SkuDepPro": "0402",
            "name": "Sibayo",
            "sku": "15"
          },
          "040216": {
            "SkuDepPro": "0402",
            "name": "Tapay",
            "sku": "16"
          },
          "040217": {
            "SkuDepPro": "0402",
            "name": "Tisco",
            "sku": "17"
          },
          "040218": {
            "SkuDepPro": "0402",
            "name": "Tuti",
            "sku": "18"
          },
          "040219": {
            "SkuDepPro": "0402",
            "name": "Yanque",
            "sku": "19"
          },
          "040220": {
            "SkuDepPro": "0402",
            "name": "Majes",
            "sku": "20"
          },
          "040301": {
            "SkuDepPro": "0403",
            "name": "Camana",
            "sku": "01"
          },
          "040302": {
            "SkuDepPro": "0403",
            "name": "Jose Maria Quimper",
            "sku": "02"
          },
          "040303": {
            "SkuDepPro": "0403",
            "name": "Mariano Nicolas Valcarcel",
            "sku": "03"
          },
          "040304": {
            "SkuDepPro": "0403",
            "name": "Mariscal Caceres",
            "sku": "04"
          },
          "040305": {
            "SkuDepPro": "0403",
            "name": "Nicolas De Pierola",
            "sku": "05"
          },
          "040306": {
            "SkuDepPro": "0403",
            "name": "Ocoña",
            "sku": "06"
          },
          "040307": {
            "SkuDepPro": "0403",
            "name": "Quilca",
            "sku": "07"
          },
          "040308": {
            "SkuDepPro": "0403",
            "name": "Samuel Pastor",
            "sku": "08"
          },
          "040401": {
            "SkuDepPro": "0404",
            "name": "Caraveli",
            "sku": "01"
          },
          "040402": {
            "SkuDepPro": "0404",
            "name": "Acari",
            "sku": "02"
          },
          "040403": {
            "SkuDepPro": "0404",
            "name": "Atico",
            "sku": "03"
          },
          "040404": {
            "SkuDepPro": "0404",
            "name": "Atiquipa",
            "sku": "04"
          },
          "040405": {
            "SkuDepPro": "0404",
            "name": "Bella Union",
            "sku": "05"
          },
          "040406": {
            "SkuDepPro": "0404",
            "name": "Cahuacho",
            "sku": "06"
          },
          "040407": {
            "SkuDepPro": "0404",
            "name": "Chala",
            "sku": "07"
          },
          "040408": {
            "SkuDepPro": "0404",
            "name": "Chaparra",
            "sku": "08"
          },
          "040409": {
            "SkuDepPro": "0404",
            "name": "Huanuhuanu",
            "sku": "09"
          },
          "040410": {
            "SkuDepPro": "0404",
            "name": "Jaqui",
            "sku": "10"
          },
          "040411": {
            "SkuDepPro": "0404",
            "name": "Lomas",
            "sku": "11"
          },
          "040412": {
            "SkuDepPro": "0404",
            "name": "Quicacha",
            "sku": "12"
          },
          "040413": {
            "SkuDepPro": "0404",
            "name": "Yauca",
            "sku": "13"
          },
          "040501": {
            "SkuDepPro": "0405",
            "name": "Aplao",
            "sku": "01"
          },
          "040502": {
            "SkuDepPro": "0405",
            "name": "Andagua",
            "sku": "02"
          },
          "040503": {
            "SkuDepPro": "0405",
            "name": "Ayo",
            "sku": "03"
          },
          "040504": {
            "SkuDepPro": "0405",
            "name": "Chachas",
            "sku": "04"
          },
          "040505": {
            "SkuDepPro": "0405",
            "name": "Chilcaymarca",
            "sku": "05"
          },
          "040506": {
            "SkuDepPro": "0405",
            "name": "Choco",
            "sku": "06"
          },
          "040507": {
            "SkuDepPro": "0405",
            "name": "Huancarqui",
            "sku": "07"
          },
          "040508": {
            "SkuDepPro": "0405",
            "name": "Machaguay",
            "sku": "08"
          },
          "040509": {
            "SkuDepPro": "0405",
            "name": "Orcopampa",
            "sku": "09"
          },
          "040510": {
            "SkuDepPro": "0405",
            "name": "Pampacolca",
            "sku": "10"
          },
          "040511": {
            "SkuDepPro": "0405",
            "name": "Tipan",
            "sku": "11"
          },
          "040512": {
            "SkuDepPro": "0405",
            "name": "Uraca",
            "sku": "12"
          },
          "040513": {
            "SkuDepPro": "0405",
            "name": "Uñon",
            "sku": "13"
          },
          "040514": {
            "SkuDepPro": "0405",
            "name": "Viraco",
            "sku": "14"
          },
          "040601": {
            "SkuDepPro": "0406",
            "name": "Chuquibamba",
            "sku": "01"
          },
          "040602": {
            "SkuDepPro": "0406",
            "name": "Andaray",
            "sku": "02"
          },
          "040603": {
            "SkuDepPro": "0406",
            "name": "Cayarani",
            "sku": "03"
          },
          "040604": {
            "SkuDepPro": "0406",
            "name": "Chichas",
            "sku": "04"
          },
          "040605": {
            "SkuDepPro": "0406",
            "name": "Iray",
            "sku": "05"
          },
          "040606": {
            "SkuDepPro": "0406",
            "name": "Salamanca",
            "sku": "06"
          },
          "040607": {
            "SkuDepPro": "0406",
            "name": "Yanaquihua",
            "sku": "07"
          },
          "040608": {
            "SkuDepPro": "0406",
            "name": "Rio Grande",
            "sku": "08"
          },
          "040701": {
            "SkuDepPro": "0407",
            "name": "Mollendo",
            "sku": "01"
          },
          "040702": {
            "SkuDepPro": "0407",
            "name": "Cocachacra",
            "sku": "02"
          },
          "040703": {
            "SkuDepPro": "0407",
            "name": "Dean Valdivia",
            "sku": "03"
          },
          "040704": {
            "SkuDepPro": "0407",
            "name": "Islay",
            "sku": "04"
          },
          "040705": {
            "SkuDepPro": "0407",
            "name": "Mejia",
            "sku": "05"
          },
          "040706": {
            "SkuDepPro": "0407",
            "name": "Punta De Bombon",
            "sku": "06"
          },
          "040801": {
            "SkuDepPro": "0408",
            "name": "Cotahuasi",
            "sku": "01"
          },
          "040802": {
            "SkuDepPro": "0408",
            "name": "Alca",
            "sku": "02"
          },
          "040803": {
            "SkuDepPro": "0408",
            "name": "Charcana",
            "sku": "03"
          },
          "040804": {
            "SkuDepPro": "0408",
            "name": "Huaynacotas",
            "sku": "04"
          },
          "040805": {
            "SkuDepPro": "0408",
            "name": "Pampamarca",
            "sku": "05"
          },
          "040806": {
            "SkuDepPro": "0408",
            "name": "Puyca",
            "sku": "06"
          },
          "040807": {
            "SkuDepPro": "0408",
            "name": "Quechualla",
            "sku": "07"
          },
          "040808": {
            "SkuDepPro": "0408",
            "name": "Sayla",
            "sku": "08"
          },
          "040809": {
            "SkuDepPro": "0408",
            "name": "Tauria",
            "sku": "09"
          },
          "040810": {
            "SkuDepPro": "0408",
            "name": "Tomepampa",
            "sku": "10"
          },
          "040811": {
            "SkuDepPro": "0408",
            "name": "Toro",
            "sku": "11"
          },
          "050101": {
            "SkuDepPro": "0501",
            "name": "Ayacucho",
            "sku": "01"
          },
          "050102": {
            "SkuDepPro": "0501",
            "name": "Acos Vinchos",
            "sku": "02"
          },
          "050103": {
            "SkuDepPro": "0501",
            "name": "Carmen Alto",
            "sku": "03"
          },
          "050104": {
            "SkuDepPro": "0501",
            "name": "Chiara",
            "sku": "04"
          },
          "050105": {
            "SkuDepPro": "0501",
            "name": "Quinua",
            "sku": "05"
          },
          "050106": {
            "SkuDepPro": "0501",
            "name": "San Jose De Ticllas",
            "sku": "06"
          },
          "050107": {
            "SkuDepPro": "0501",
            "name": "San Juan Bautista",
            "sku": "07"
          },
          "050108": {
            "SkuDepPro": "0501",
            "name": "Santiago De Pischa",
            "sku": "08"
          },
          "050109": {
            "SkuDepPro": "0501",
            "name": "Vinchos",
            "sku": "09"
          },
          "050110": {
            "SkuDepPro": "0501",
            "name": "Tambillo",
            "sku": "10"
          },
          "050111": {
            "SkuDepPro": "0501",
            "name": "Acocro",
            "sku": "11"
          },
          "050112": {
            "SkuDepPro": "0501",
            "name": "Socos",
            "sku": "12"
          },
          "050113": {
            "SkuDepPro": "0501",
            "name": "Ocros",
            "sku": "13"
          },
          "050114": {
            "SkuDepPro": "0501",
            "name": "Pacaycasa",
            "sku": "14"
          },
          "050115": {
            "SkuDepPro": "0501",
            "name": "Jesus Nazareno",
            "sku": "15"
          },
          "050201": {
            "SkuDepPro": "0502",
            "name": "Cangallo",
            "sku": "01"
          },
          "050204": {
            "SkuDepPro": "0502",
            "name": "Chuschi",
            "sku": "04"
          },
          "050206": {
            "SkuDepPro": "0502",
            "name": "Los Morochucos",
            "sku": "06"
          },
          "050207": {
            "SkuDepPro": "0502",
            "name": "Paras",
            "sku": "07"
          },
          "050208": {
            "SkuDepPro": "0502",
            "name": "Totos",
            "sku": "08"
          },
          "050211": {
            "SkuDepPro": "0502",
            "name": "Maria Parado De Bellido",
            "sku": "11"
          },
          "050301": {
            "SkuDepPro": "0503",
            "name": "Huanta",
            "sku": "01"
          },
          "050302": {
            "SkuDepPro": "0503",
            "name": "Ayahuanco",
            "sku": "02"
          },
          "050303": {
            "SkuDepPro": "0503",
            "name": "Huamanguilla",
            "sku": "03"
          },
          "050304": {
            "SkuDepPro": "0503",
            "name": "Iguain",
            "sku": "04"
          },
          "050305": {
            "SkuDepPro": "0503",
            "name": "Luricocha",
            "sku": "05"
          },
          "050307": {
            "SkuDepPro": "0503",
            "name": "Santillana",
            "sku": "07"
          },
          "050308": {
            "SkuDepPro": "0503",
            "name": "Sivia",
            "sku": "08"
          },
          "050309": {
            "SkuDepPro": "0503",
            "name": "Llochegua",
            "sku": "09"
          },
          "050401": {
            "SkuDepPro": "0504",
            "name": "San Miguel",
            "sku": "01"
          },
          "050402": {
            "SkuDepPro": "0504",
            "name": "Anco",
            "sku": "02"
          },
          "050403": {
            "SkuDepPro": "0504",
            "name": "Ayna",
            "sku": "03"
          },
          "050404": {
            "SkuDepPro": "0504",
            "name": "Chilcas",
            "sku": "04"
          },
          "050405": {
            "SkuDepPro": "0504",
            "name": "Chungui",
            "sku": "05"
          },
          "050406": {
            "SkuDepPro": "0504",
            "name": "Tambo",
            "sku": "06"
          },
          "050407": {
            "SkuDepPro": "0504",
            "name": "Luis Carranza",
            "sku": "07"
          },
          "050408": {
            "SkuDepPro": "0504",
            "name": "Santa Rosa",
            "sku": "08"
          },
          "050409": {
            "SkuDepPro": "0504",
            "name": "Samugari",
            "sku": "09"
          },
          "050501": {
            "SkuDepPro": "0505",
            "name": "Puquio",
            "sku": "01"
          },
          "050502": {
            "SkuDepPro": "0505",
            "name": "Aucara",
            "sku": "02"
          },
          "050503": {
            "SkuDepPro": "0505",
            "name": "Cabana",
            "sku": "03"
          },
          "050504": {
            "SkuDepPro": "0505",
            "name": "Carmen Salcedo",
            "sku": "04"
          },
          "050506": {
            "SkuDepPro": "0505",
            "name": "Chaviña",
            "sku": "06"
          },
          "050508": {
            "SkuDepPro": "0505",
            "name": "Chipao",
            "sku": "08"
          },
          "050510": {
            "SkuDepPro": "0505",
            "name": "Huac-Huas",
            "sku": "10"
          },
          "050511": {
            "SkuDepPro": "0505",
            "name": "Laramate",
            "sku": "11"
          },
          "050512": {
            "SkuDepPro": "0505",
            "name": "Leoncio Prado",
            "sku": "12"
          },
          "050513": {
            "SkuDepPro": "0505",
            "name": "Lucanas",
            "sku": "13"
          },
          "050514": {
            "SkuDepPro": "0505",
            "name": "Llauta",
            "sku": "14"
          },
          "050516": {
            "SkuDepPro": "0505",
            "name": "Ocaña",
            "sku": "16"
          },
          "050517": {
            "SkuDepPro": "0505",
            "name": "Otoca",
            "sku": "17"
          },
          "050520": {
            "SkuDepPro": "0505",
            "name": "Sancos",
            "sku": "20"
          },
          "050521": {
            "SkuDepPro": "0505",
            "name": "San Juan",
            "sku": "21"
          },
          "050522": {
            "SkuDepPro": "0505",
            "name": "San Pedro",
            "sku": "22"
          },
          "050524": {
            "SkuDepPro": "0505",
            "name": "Santa Ana De Huaycahuacho",
            "sku": "24"
          },
          "050525": {
            "SkuDepPro": "0505",
            "name": "Santa Lucia",
            "sku": "25"
          },
          "050529": {
            "SkuDepPro": "0505",
            "name": "Saisa",
            "sku": "29"
          },
          "050531": {
            "SkuDepPro": "0505",
            "name": "San Pedro De Palco",
            "sku": "31"
          },
          "050532": {
            "SkuDepPro": "0505",
            "name": "San Cristobal",
            "sku": "32"
          },
          "050601": {
            "SkuDepPro": "0506",
            "name": "Coracora",
            "sku": "01"
          },
          "050604": {
            "SkuDepPro": "0506",
            "name": "Coronel Castañeda",
            "sku": "04"
          },
          "050605": {
            "SkuDepPro": "0506",
            "name": "Chumpi",
            "sku": "05"
          },
          "050608": {
            "SkuDepPro": "0506",
            "name": "Pacapausa",
            "sku": "08"
          },
          "050611": {
            "SkuDepPro": "0506",
            "name": "Pullo",
            "sku": "11"
          },
          "050612": {
            "SkuDepPro": "0506",
            "name": "Puyusca",
            "sku": "12"
          },
          "050615": {
            "SkuDepPro": "0506",
            "name": "San Francisco De Ravacayco",
            "sku": "15"
          },
          "050616": {
            "SkuDepPro": "0506",
            "name": "Upahuacho",
            "sku": "16"
          },
          "050701": {
            "SkuDepPro": "0507",
            "name": "Huancapi",
            "sku": "01"
          },
          "050702": {
            "SkuDepPro": "0507",
            "name": "Alcamenca",
            "sku": "02"
          },
          "050703": {
            "SkuDepPro": "0507",
            "name": "Apongo",
            "sku": "03"
          },
          "050704": {
            "SkuDepPro": "0507",
            "name": "Canaria",
            "sku": "04"
          },
          "050706": {
            "SkuDepPro": "0507",
            "name": "Cayara",
            "sku": "06"
          },
          "050707": {
            "SkuDepPro": "0507",
            "name": "Colca",
            "sku": "07"
          },
          "050708": {
            "SkuDepPro": "0507",
            "name": "Hualla",
            "sku": "08"
          },
          "050709": {
            "SkuDepPro": "0507",
            "name": "Huamanquiquia",
            "sku": "09"
          },
          "050710": {
            "SkuDepPro": "0507",
            "name": "Huancaraylla",
            "sku": "10"
          },
          "050713": {
            "SkuDepPro": "0507",
            "name": "Sarhua",
            "sku": "13"
          },
          "050714": {
            "SkuDepPro": "0507",
            "name": "Vilcanchos",
            "sku": "14"
          },
          "050715": {
            "SkuDepPro": "0507",
            "name": "Asquipata",
            "sku": "15"
          },
          "050801": {
            "SkuDepPro": "0508",
            "name": "Sancos",
            "sku": "01"
          },
          "050802": {
            "SkuDepPro": "0508",
            "name": "Sacsamarca",
            "sku": "02"
          },
          "050803": {
            "SkuDepPro": "0508",
            "name": "Santiago De Lucanamarca",
            "sku": "03"
          },
          "050804": {
            "SkuDepPro": "0508",
            "name": "Carapo",
            "sku": "04"
          },
          "050901": {
            "SkuDepPro": "0509",
            "name": "Vilcas Huaman",
            "sku": "01"
          },
          "050902": {
            "SkuDepPro": "0509",
            "name": "Vischongo",
            "sku": "02"
          },
          "050903": {
            "SkuDepPro": "0509",
            "name": "Accomarca",
            "sku": "03"
          },
          "050904": {
            "SkuDepPro": "0509",
            "name": "Carhuanca",
            "sku": "04"
          },
          "050905": {
            "SkuDepPro": "0509",
            "name": "Concepción",
            "sku": "05"
          },
          "050906": {
            "SkuDepPro": "0509",
            "name": "Huambalpa",
            "sku": "06"
          },
          "050907": {
            "SkuDepPro": "0509",
            "name": "Saurama",
            "sku": "07"
          },
          "050908": {
            "SkuDepPro": "0509",
            "name": "Independencia",
            "sku": "08"
          },
          "051001": {
            "SkuDepPro": "0510",
            "name": "Pausa",
            "sku": "01"
          },
          "051002": {
            "SkuDepPro": "0510",
            "name": "Colta",
            "sku": "02"
          },
          "051003": {
            "SkuDepPro": "0510",
            "name": "Corculla",
            "sku": "03"
          },
          "051004": {
            "SkuDepPro": "0510",
            "name": "Lampa",
            "sku": "04"
          },
          "051005": {
            "SkuDepPro": "0510",
            "name": "Marcabamba",
            "sku": "05"
          },
          "051006": {
            "SkuDepPro": "0510",
            "name": "Oyolo",
            "sku": "06"
          },
          "051007": {
            "SkuDepPro": "0510",
            "name": "Pararca",
            "sku": "07"
          },
          "051008": {
            "SkuDepPro": "0510",
            "name": "San Javier De Alpabamba",
            "sku": "08"
          },
          "051009": {
            "SkuDepPro": "0510",
            "name": "San Jose De Ushua",
            "sku": "09"
          },
          "051010": {
            "SkuDepPro": "0510",
            "name": "Sara Sara",
            "sku": "10"
          },
          "051101": {
            "SkuDepPro": "0511",
            "name": "Querobamba",
            "sku": "01"
          },
          "051102": {
            "SkuDepPro": "0511",
            "name": "Belen",
            "sku": "02"
          },
          "051103": {
            "SkuDepPro": "0511",
            "name": "Chalcos",
            "sku": "03"
          },
          "051104": {
            "SkuDepPro": "0511",
            "name": "San Salvador De Quije",
            "sku": "04"
          },
          "051105": {
            "SkuDepPro": "0511",
            "name": "Paico",
            "sku": "05"
          },
          "051106": {
            "SkuDepPro": "0511",
            "name": "Santiago De Paucaray",
            "sku": "06"
          },
          "051107": {
            "SkuDepPro": "0511",
            "name": "San Pedro De Larcay",
            "sku": "07"
          },
          "051108": {
            "SkuDepPro": "0511",
            "name": "Soras",
            "sku": "08"
          },
          "051109": {
            "SkuDepPro": "0511",
            "name": "Huacaña",
            "sku": "09"
          },
          "051110": {
            "SkuDepPro": "0511",
            "name": "Chilcayoc",
            "sku": "10"
          },
          "051111": {
            "SkuDepPro": "0511",
            "name": "Morcolla",
            "sku": "11"
          },
          "060101": {
            "SkuDepPro": "0601",
            "name": "Cajamarca",
            "sku": "01"
          },
          "060102": {
            "SkuDepPro": "0601",
            "name": "Asuncion",
            "sku": "02"
          },
          "060103": {
            "SkuDepPro": "0601",
            "name": "Cospan",
            "sku": "03"
          },
          "060104": {
            "SkuDepPro": "0601",
            "name": "Chetilla",
            "sku": "04"
          },
          "060105": {
            "SkuDepPro": "0601",
            "name": "Encañada",
            "sku": "05"
          },
          "060106": {
            "SkuDepPro": "0601",
            "name": "Jesus",
            "sku": "06"
          },
          "060107": {
            "SkuDepPro": "0601",
            "name": "Los Baños Del Inca",
            "sku": "07"
          },
          "060108": {
            "SkuDepPro": "0601",
            "name": "Llacanora",
            "sku": "08"
          },
          "060109": {
            "SkuDepPro": "0601",
            "name": "Magdalena",
            "sku": "09"
          },
          "060110": {
            "SkuDepPro": "0601",
            "name": "Matara",
            "sku": "10"
          },
          "060111": {
            "SkuDepPro": "0601",
            "name": "Namora",
            "sku": "11"
          },
          "060112": {
            "SkuDepPro": "0601",
            "name": "San Juan",
            "sku": "12"
          },
          "060201": {
            "SkuDepPro": "0602",
            "name": "Cajabamba",
            "sku": "01"
          },
          "060202": {
            "SkuDepPro": "0602",
            "name": "Cachachi",
            "sku": "02"
          },
          "060203": {
            "SkuDepPro": "0602",
            "name": "Condebamba",
            "sku": "03"
          },
          "060205": {
            "SkuDepPro": "0602",
            "name": "Sitacocha",
            "sku": "05"
          },
          "060301": {
            "SkuDepPro": "0603",
            "name": "Celendin",
            "sku": "01"
          },
          "060302": {
            "SkuDepPro": "0603",
            "name": "Cortegana",
            "sku": "02"
          },
          "060303": {
            "SkuDepPro": "0603",
            "name": "Chumuch",
            "sku": "03"
          },
          "060304": {
            "SkuDepPro": "0603",
            "name": "Huasmin",
            "sku": "04"
          },
          "060305": {
            "SkuDepPro": "0603",
            "name": "Jorge Chavez",
            "sku": "05"
          },
          "060306": {
            "SkuDepPro": "0603",
            "name": "Jose Galvez",
            "sku": "06"
          },
          "060307": {
            "SkuDepPro": "0603",
            "name": "Miguel Iglesias",
            "sku": "07"
          },
          "060308": {
            "SkuDepPro": "0603",
            "name": "Oxamarca",
            "sku": "08"
          },
          "060309": {
            "SkuDepPro": "0603",
            "name": "Sorochuco",
            "sku": "09"
          },
          "060310": {
            "SkuDepPro": "0603",
            "name": "Sucre",
            "sku": "10"
          },
          "060311": {
            "SkuDepPro": "0603",
            "name": "Utco",
            "sku": "11"
          },
          "060312": {
            "SkuDepPro": "0603",
            "name": "La Libertad De Pallan",
            "sku": "12"
          },
          "060401": {
            "SkuDepPro": "0604",
            "name": "Contumaza",
            "sku": "01"
          },
          "060403": {
            "SkuDepPro": "0604",
            "name": "Chilete",
            "sku": "03"
          },
          "060404": {
            "SkuDepPro": "0604",
            "name": "Guzmango",
            "sku": "04"
          },
          "060405": {
            "SkuDepPro": "0604",
            "name": "San Benito",
            "sku": "05"
          },
          "060406": {
            "SkuDepPro": "0604",
            "name": "Cupisnique",
            "sku": "06"
          },
          "060407": {
            "SkuDepPro": "0604",
            "name": "Tantarica",
            "sku": "07"
          },
          "060408": {
            "SkuDepPro": "0604",
            "name": "Yonan",
            "sku": "08"
          },
          "060409": {
            "SkuDepPro": "0604",
            "name": "Santa Cruz De Toled",
            "sku": "09"
          },
          "060501": {
            "SkuDepPro": "0605",
            "name": "Cutervo",
            "sku": "01"
          },
          "060502": {
            "SkuDepPro": "0605",
            "name": "Callayuc",
            "sku": "02"
          },
          "060503": {
            "SkuDepPro": "0605",
            "name": "Cujillo",
            "sku": "03"
          },
          "060504": {
            "SkuDepPro": "0605",
            "name": "Choros",
            "sku": "04"
          },
          "060505": {
            "SkuDepPro": "0605",
            "name": "La Ramada",
            "sku": "05"
          },
          "060506": {
            "SkuDepPro": "0605",
            "name": "Pimpingos",
            "sku": "06"
          },
          "060507": {
            "SkuDepPro": "0605",
            "name": "Querocotillo",
            "sku": "07"
          },
          "060508": {
            "SkuDepPro": "0605",
            "name": "San Andres De Cutervo",
            "sku": "08"
          },
          "060509": {
            "SkuDepPro": "0605",
            "name": "San Juan De Cutervo",
            "sku": "09"
          },
          "060510": {
            "SkuDepPro": "0605",
            "name": "San Luis De Lucma",
            "sku": "10"
          },
          "060511": {
            "SkuDepPro": "0605",
            "name": "Santa Cruz",
            "sku": "11"
          },
          "060512": {
            "SkuDepPro": "0605",
            "name": "Santo Domingo De La Capilla",
            "sku": "12"
          },
          "060513": {
            "SkuDepPro": "0605",
            "name": "Santo Tomas",
            "sku": "13"
          },
          "060514": {
            "SkuDepPro": "0605",
            "name": "Socota",
            "sku": "14"
          },
          "060515": {
            "SkuDepPro": "0605",
            "name": "Toribio Casanova",
            "sku": "15"
          },
          "060601": {
            "SkuDepPro": "0606",
            "name": "Chota",
            "sku": "01"
          },
          "060602": {
            "SkuDepPro": "0606",
            "name": "Anguia",
            "sku": "02"
          },
          "060603": {
            "SkuDepPro": "0606",
            "name": "Cochabamba",
            "sku": "03"
          },
          "060604": {
            "SkuDepPro": "0606",
            "name": "Conchan",
            "sku": "04"
          },
          "060605": {
            "SkuDepPro": "0606",
            "name": "Chadin",
            "sku": "05"
          },
          "060606": {
            "SkuDepPro": "0606",
            "name": "Chiguirip",
            "sku": "06"
          },
          "060607": {
            "SkuDepPro": "0606",
            "name": "Chimban",
            "sku": "07"
          },
          "060608": {
            "SkuDepPro": "0606",
            "name": "Huambos",
            "sku": "08"
          },
          "060609": {
            "SkuDepPro": "0606",
            "name": "Lajas",
            "sku": "09"
          },
          "060610": {
            "SkuDepPro": "0606",
            "name": "Llama",
            "sku": "10"
          },
          "060611": {
            "SkuDepPro": "0606",
            "name": "Miracosta",
            "sku": "11"
          },
          "060612": {
            "SkuDepPro": "0606",
            "name": "Paccha",
            "sku": "12"
          },
          "060613": {
            "SkuDepPro": "0606",
            "name": "Pion",
            "sku": "13"
          },
          "060614": {
            "SkuDepPro": "0606",
            "name": "Querocoto",
            "sku": "14"
          },
          "060615": {
            "SkuDepPro": "0606",
            "name": "Tacabamba",
            "sku": "15"
          },
          "060616": {
            "SkuDepPro": "0606",
            "name": "Tocmoche",
            "sku": "16"
          },
          "060617": {
            "SkuDepPro": "0606",
            "name": "San Juan De Licupis",
            "sku": "17"
          },
          "060618": {
            "SkuDepPro": "0606",
            "name": "Choropampa",
            "sku": "18"
          },
          "060619": {
            "SkuDepPro": "0606",
            "name": "Chalamarca",
            "sku": "19"
          },
          "060701": {
            "SkuDepPro": "0607",
            "name": "Bambamarca",
            "sku": "01"
          },
          "060702": {
            "SkuDepPro": "0607",
            "name": "Chugur",
            "sku": "02"
          },
          "060703": {
            "SkuDepPro": "0607",
            "name": "Hualgayoc",
            "sku": "03"
          },
          "060801": {
            "SkuDepPro": "0608",
            "name": "Jaen",
            "sku": "01"
          },
          "060802": {
            "SkuDepPro": "0608",
            "name": "Bellavista",
            "sku": "02"
          },
          "060803": {
            "SkuDepPro": "0608",
            "name": "Colasay",
            "sku": "03"
          },
          "060804": {
            "SkuDepPro": "0608",
            "name": "Chontali",
            "sku": "04"
          },
          "060805": {
            "SkuDepPro": "0608",
            "name": "Pomahuaca",
            "sku": "05"
          },
          "060806": {
            "SkuDepPro": "0608",
            "name": "Pucara",
            "sku": "06"
          },
          "060807": {
            "SkuDepPro": "0608",
            "name": "Sallique",
            "sku": "07"
          },
          "060808": {
            "SkuDepPro": "0608",
            "name": "San Felipe",
            "sku": "08"
          },
          "060809": {
            "SkuDepPro": "0608",
            "name": "San Jose Del Alto",
            "sku": "09"
          },
          "060810": {
            "SkuDepPro": "0608",
            "name": "Santa Rosa",
            "sku": "10"
          },
          "060811": {
            "SkuDepPro": "0608",
            "name": "Las Pirias",
            "sku": "11"
          },
          "060812": {
            "SkuDepPro": "0608",
            "name": "Huabal",
            "sku": "12"
          },
          "060901": {
            "SkuDepPro": "0609",
            "name": "Santa Cruz",
            "sku": "01"
          },
          "060902": {
            "SkuDepPro": "0609",
            "name": "Catache",
            "sku": "02"
          },
          "060903": {
            "SkuDepPro": "0609",
            "name": "Chancaybaños",
            "sku": "03"
          },
          "060904": {
            "SkuDepPro": "0609",
            "name": "La Esperanza",
            "sku": "04"
          },
          "060905": {
            "SkuDepPro": "0609",
            "name": "Ninabamba",
            "sku": "05"
          },
          "060906": {
            "SkuDepPro": "0609",
            "name": "Pulan",
            "sku": "06"
          },
          "060907": {
            "SkuDepPro": "0609",
            "name": "Sexi",
            "sku": "07"
          },
          "060908": {
            "SkuDepPro": "0609",
            "name": "Uticyacu",
            "sku": "08"
          },
          "060909": {
            "SkuDepPro": "0609",
            "name": "Yauyucan",
            "sku": "09"
          },
          "060910": {
            "SkuDepPro": "0609",
            "name": "Andabamba",
            "sku": "10"
          },
          "060911": {
            "SkuDepPro": "0609",
            "name": "Saucepampa",
            "sku": "11"
          },
          "061001": {
            "SkuDepPro": "0610",
            "name": "San Miguel",
            "sku": "01"
          },
          "061002": {
            "SkuDepPro": "0610",
            "name": "Calquis",
            "sku": "02"
          },
          "061003": {
            "SkuDepPro": "0610",
            "name": "La Florida",
            "sku": "03"
          },
          "061004": {
            "SkuDepPro": "0610",
            "name": "Llapa",
            "sku": "04"
          },
          "061005": {
            "SkuDepPro": "0610",
            "name": "Nanchoc",
            "sku": "05"
          },
          "061006": {
            "SkuDepPro": "0610",
            "name": "Niepos",
            "sku": "06"
          },
          "061007": {
            "SkuDepPro": "0610",
            "name": "San Gregorio",
            "sku": "07"
          },
          "061008": {
            "SkuDepPro": "0610",
            "name": "San Silvestre De Cochan",
            "sku": "08"
          },
          "061009": {
            "SkuDepPro": "0610",
            "name": "El Prado",
            "sku": "09"
          },
          "061010": {
            "SkuDepPro": "0610",
            "name": "Union Agua Blanca",
            "sku": "10"
          },
          "061011": {
            "SkuDepPro": "0610",
            "name": "Tongod",
            "sku": "11"
          },
          "061012": {
            "SkuDepPro": "0610",
            "name": "Catilluc",
            "sku": "12"
          },
          "061013": {
            "SkuDepPro": "0610",
            "name": "Bolívar",
            "sku": "13"
          },
          "061101": {
            "SkuDepPro": "0611",
            "name": "San Ignacio",
            "sku": "01"
          },
          "061102": {
            "SkuDepPro": "0611",
            "name": "Chirinos",
            "sku": "02"
          },
          "061103": {
            "SkuDepPro": "0611",
            "name": "Huarango",
            "sku": "03"
          },
          "061104": {
            "SkuDepPro": "0611",
            "name": "Namballe",
            "sku": "04"
          },
          "061105": {
            "SkuDepPro": "0611",
            "name": "La Coipa",
            "sku": "05"
          },
          "061106": {
            "SkuDepPro": "0611",
            "name": "San Jose De Lourdes",
            "sku": "06"
          },
          "061107": {
            "SkuDepPro": "0611",
            "name": "Tabaconas",
            "sku": "07"
          },
          "061201": {
            "SkuDepPro": "0612",
            "name": "Pedro Galvez",
            "sku": "01"
          },
          "061202": {
            "SkuDepPro": "0612",
            "name": "Ichocan",
            "sku": "02"
          },
          "061203": {
            "SkuDepPro": "0612",
            "name": "Gregorio Pita",
            "sku": "03"
          },
          "061204": {
            "SkuDepPro": "0612",
            "name": "Jose Manuel Quiroz",
            "sku": "04"
          },
          "061205": {
            "SkuDepPro": "0612",
            "name": "Eduardo Villanueva",
            "sku": "05"
          },
          "061206": {
            "SkuDepPro": "0612",
            "name": "Jose Sabogal",
            "sku": "06"
          },
          "061207": {
            "SkuDepPro": "0612",
            "name": "Chancay",
            "sku": "07"
          },
          "061301": {
            "SkuDepPro": "0613",
            "name": "San Pablo",
            "sku": "01"
          },
          "061302": {
            "SkuDepPro": "0613",
            "name": "San Bernardino",
            "sku": "02"
          },
          "061303": {
            "SkuDepPro": "0613",
            "name": "San Luis",
            "sku": "03"
          },
          "061304": {
            "SkuDepPro": "0613",
            "name": "Tumbaden",
            "sku": "04"
          },
          "070101": {
            "SkuDepPro": "0701",
            "name": "Cusco",
            "sku": "01"
          },
          "070102": {
            "SkuDepPro": "0701",
            "name": "Ccorca",
            "sku": "02"
          },
          "070103": {
            "SkuDepPro": "0701",
            "name": "Poroy",
            "sku": "03"
          },
          "070104": {
            "SkuDepPro": "0701",
            "name": "San Jeronimo",
            "sku": "04"
          },
          "070105": {
            "SkuDepPro": "0701",
            "name": "San Sebastian",
            "sku": "05"
          },
          "070106": {
            "SkuDepPro": "0701",
            "name": "Santiago",
            "sku": "06"
          },
          "070107": {
            "SkuDepPro": "0701",
            "name": "Saylla",
            "sku": "07"
          },
          "070108": {
            "SkuDepPro": "0701",
            "name": "Wanchaq",
            "sku": "08"
          },
          "070201": {
            "SkuDepPro": "0702",
            "name": "Acomayo",
            "sku": "01"
          },
          "070202": {
            "SkuDepPro": "0702",
            "name": "Acopia",
            "sku": "02"
          },
          "070203": {
            "SkuDepPro": "0702",
            "name": "Acos",
            "sku": "03"
          },
          "070204": {
            "SkuDepPro": "0702",
            "name": "Pomacanchi",
            "sku": "04"
          },
          "070205": {
            "SkuDepPro": "0702",
            "name": "Rondocan",
            "sku": "05"
          },
          "070206": {
            "SkuDepPro": "0702",
            "name": "Sangarara",
            "sku": "06"
          },
          "070207": {
            "SkuDepPro": "0702",
            "name": "Mosoc Llacta",
            "sku": "07"
          },
          "070301": {
            "SkuDepPro": "0703",
            "name": "Anta",
            "sku": "01"
          },
          "070302": {
            "SkuDepPro": "0703",
            "name": "Chinchaypujio",
            "sku": "02"
          },
          "070303": {
            "SkuDepPro": "0703",
            "name": "Huarocondo",
            "sku": "03"
          },
          "070304": {
            "SkuDepPro": "0703",
            "name": "Limatambo",
            "sku": "04"
          },
          "070305": {
            "SkuDepPro": "0703",
            "name": "Mollepata",
            "sku": "05"
          },
          "070306": {
            "SkuDepPro": "0703",
            "name": "Pucyura",
            "sku": "06"
          },
          "070307": {
            "SkuDepPro": "0703",
            "name": "Zurite",
            "sku": "07"
          },
          "070308": {
            "SkuDepPro": "0703",
            "name": "Cachimayo",
            "sku": "08"
          },
          "070309": {
            "SkuDepPro": "0703",
            "name": "Ancahuasi",
            "sku": "09"
          },
          "070401": {
            "SkuDepPro": "0704",
            "name": "Calca",
            "sku": "01"
          },
          "070402": {
            "SkuDepPro": "0704",
            "name": "Coya",
            "sku": "02"
          },
          "070403": {
            "SkuDepPro": "0704",
            "name": "Lamay",
            "sku": "03"
          },
          "070404": {
            "SkuDepPro": "0704",
            "name": "Lares",
            "sku": "04"
          },
          "070405": {
            "SkuDepPro": "0704",
            "name": "Pisac",
            "sku": "05"
          },
          "070406": {
            "SkuDepPro": "0704",
            "name": "San Salvador",
            "sku": "06"
          },
          "070407": {
            "SkuDepPro": "0704",
            "name": "Taray",
            "sku": "07"
          },
          "070408": {
            "SkuDepPro": "0704",
            "name": "Yanatile",
            "sku": "08"
          },
          "070501": {
            "SkuDepPro": "0705",
            "name": "Yanaoca",
            "sku": "01"
          },
          "070502": {
            "SkuDepPro": "0705",
            "name": "Checca",
            "sku": "02"
          },
          "070503": {
            "SkuDepPro": "0705",
            "name": "Kunturkanki",
            "sku": "03"
          },
          "070504": {
            "SkuDepPro": "0705",
            "name": "Langui",
            "sku": "04"
          },
          "070505": {
            "SkuDepPro": "0705",
            "name": "Layo",
            "sku": "05"
          },
          "070506": {
            "SkuDepPro": "0705",
            "name": "Pampamarca",
            "sku": "06"
          },
          "070507": {
            "SkuDepPro": "0705",
            "name": "Quehue",
            "sku": "07"
          },
          "070508": {
            "SkuDepPro": "0705",
            "name": "Tupac Amaru",
            "sku": "08"
          },
          "070601": {
            "SkuDepPro": "0706",
            "name": "Sicuani",
            "sku": "01"
          },
          "070602": {
            "SkuDepPro": "0706",
            "name": "Combapata",
            "sku": "02"
          },
          "070603": {
            "SkuDepPro": "0706",
            "name": "Checacupe",
            "sku": "03"
          },
          "070604": {
            "SkuDepPro": "0706",
            "name": "Marangani",
            "sku": "04"
          },
          "070605": {
            "SkuDepPro": "0706",
            "name": "Pitumarca",
            "sku": "05"
          },
          "070606": {
            "SkuDepPro": "0706",
            "name": "San Pablo",
            "sku": "06"
          },
          "070607": {
            "SkuDepPro": "0706",
            "name": "San Pedro",
            "sku": "07"
          },
          "070608": {
            "SkuDepPro": "0706",
            "name": "Tinta",
            "sku": "08"
          },
          "070701": {
            "SkuDepPro": "0707",
            "name": "Santo Tomas",
            "sku": "01"
          },
          "070702": {
            "SkuDepPro": "0707",
            "name": "Capacmarca",
            "sku": "02"
          },
          "070703": {
            "SkuDepPro": "0707",
            "name": "Colquemarca",
            "sku": "03"
          },
          "070704": {
            "SkuDepPro": "0707",
            "name": "Chamaca",
            "sku": "04"
          },
          "070705": {
            "SkuDepPro": "0707",
            "name": "Livitaca",
            "sku": "05"
          },
          "070706": {
            "SkuDepPro": "0707",
            "name": "Llusco",
            "sku": "06"
          },
          "070707": {
            "SkuDepPro": "0707",
            "name": "Quiñota",
            "sku": "07"
          },
          "070708": {
            "SkuDepPro": "0707",
            "name": "Velille",
            "sku": "08"
          },
          "070801": {
            "SkuDepPro": "0708",
            "name": "Espinar",
            "sku": "01"
          },
          "070802": {
            "SkuDepPro": "0708",
            "name": "Condoroma",
            "sku": "02"
          },
          "070803": {
            "SkuDepPro": "0708",
            "name": "Coporaque",
            "sku": "03"
          },
          "070804": {
            "SkuDepPro": "0708",
            "name": "Ocoruro",
            "sku": "04"
          },
          "070805": {
            "SkuDepPro": "0708",
            "name": "Pallpata",
            "sku": "05"
          },
          "070806": {
            "SkuDepPro": "0708",
            "name": "Pichigua",
            "sku": "06"
          },
          "070807": {
            "SkuDepPro": "0708",
            "name": "Suyckutambo",
            "sku": "07"
          },
          "070808": {
            "SkuDepPro": "0708",
            "name": "Alto Pichigua",
            "sku": "08"
          },
          "070901": {
            "SkuDepPro": "0709",
            "name": "Santa Ana",
            "sku": "01"
          },
          "070902": {
            "SkuDepPro": "0709",
            "name": "Echarate",
            "sku": "02"
          },
          "070903": {
            "SkuDepPro": "0709",
            "name": "Huayopata",
            "sku": "03"
          },
          "070904": {
            "SkuDepPro": "0709",
            "name": "Maranura",
            "sku": "04"
          },
          "070905": {
            "SkuDepPro": "0709",
            "name": "Ocobamba",
            "sku": "05"
          },
          "070906": {
            "SkuDepPro": "0709",
            "name": "Santa Teresa",
            "sku": "06"
          },
          "070907": {
            "SkuDepPro": "0709",
            "name": "Vilcabamba",
            "sku": "07"
          },
          "070908": {
            "SkuDepPro": "0709",
            "name": "Quellouno",
            "sku": "08"
          },
          "070909": {
            "SkuDepPro": "0709",
            "name": "Kimbiri",
            "sku": "09"
          },
          "070910": {
            "SkuDepPro": "0709",
            "name": "Pichari",
            "sku": "10"
          },
          "071001": {
            "SkuDepPro": "0710",
            "name": "Paruro",
            "sku": "01"
          },
          "071002": {
            "SkuDepPro": "0710",
            "name": "Accha",
            "sku": "02"
          },
          "071003": {
            "SkuDepPro": "0710",
            "name": "Ccapi",
            "sku": "03"
          },
          "071004": {
            "SkuDepPro": "0710",
            "name": "Colcha",
            "sku": "04"
          },
          "071005": {
            "SkuDepPro": "0710",
            "name": "Huanoquite",
            "sku": "05"
          },
          "071006": {
            "SkuDepPro": "0710",
            "name": "Omacha",
            "sku": "06"
          },
          "071007": {
            "SkuDepPro": "0710",
            "name": "Yaurisque",
            "sku": "07"
          },
          "071008": {
            "SkuDepPro": "0710",
            "name": "Paccaritambo",
            "sku": "08"
          },
          "071009": {
            "SkuDepPro": "0710",
            "name": "Pillpinto",
            "sku": "09"
          },
          "071101": {
            "SkuDepPro": "0711",
            "name": "Paucartambo",
            "sku": "01"
          },
          "071102": {
            "SkuDepPro": "0711",
            "name": "Caicay",
            "sku": "02"
          },
          "071103": {
            "SkuDepPro": "0711",
            "name": "Colquepata",
            "sku": "03"
          },
          "071104": {
            "SkuDepPro": "0711",
            "name": "Challabamba",
            "sku": "04"
          },
          "071105": {
            "SkuDepPro": "0711",
            "name": "Kosñipata",
            "sku": "05"
          },
          "071106": {
            "SkuDepPro": "0711",
            "name": "Huancarani",
            "sku": "06"
          },
          "071201": {
            "SkuDepPro": "0712",
            "name": "Urcos",
            "sku": "01"
          },
          "071202": {
            "SkuDepPro": "0712",
            "name": "Andahuaylillas",
            "sku": "02"
          },
          "071203": {
            "SkuDepPro": "0712",
            "name": "Camanti",
            "sku": "03"
          },
          "071204": {
            "SkuDepPro": "0712",
            "name": "Ccarhuayo",
            "sku": "04"
          },
          "071205": {
            "SkuDepPro": "0712",
            "name": "Ccatca",
            "sku": "05"
          },
          "071206": {
            "SkuDepPro": "0712",
            "name": "Cusipata",
            "sku": "06"
          },
          "071207": {
            "SkuDepPro": "0712",
            "name": "Huaro",
            "sku": "07"
          },
          "071208": {
            "SkuDepPro": "0712",
            "name": "Lucre",
            "sku": "08"
          },
          "071209": {
            "SkuDepPro": "0712",
            "name": "Marcapata",
            "sku": "09"
          },
          "071210": {
            "SkuDepPro": "0712",
            "name": "Ocongate",
            "sku": "10"
          },
          "071211": {
            "SkuDepPro": "0712",
            "name": "Oropesa",
            "sku": "11"
          },
          "071212": {
            "SkuDepPro": "0712",
            "name": "Quiquijana",
            "sku": "12"
          },
          "071301": {
            "SkuDepPro": "0713",
            "name": "Urubamba",
            "sku": "01"
          },
          "071302": {
            "SkuDepPro": "0713",
            "name": "Chinchero",
            "sku": "02"
          },
          "071303": {
            "SkuDepPro": "0713",
            "name": "Huayllabamba",
            "sku": "03"
          },
          "071304": {
            "SkuDepPro": "0713",
            "name": "Machupicchu",
            "sku": "04"
          },
          "071305": {
            "SkuDepPro": "0713",
            "name": "Maras",
            "sku": "05"
          },
          "071306": {
            "SkuDepPro": "0713",
            "name": "Ollantaytambo",
            "sku": "06"
          },
          "071307": {
            "SkuDepPro": "0713",
            "name": "Yucay",
            "sku": "07"
          },
          "080101": {
            "SkuDepPro": "0801",
            "name": "Huancavelica",
            "sku": "01"
          },
          "080102": {
            "SkuDepPro": "0801",
            "name": "Acobambilla",
            "sku": "02"
          },
          "080103": {
            "SkuDepPro": "0801",
            "name": "Acoria",
            "sku": "03"
          },
          "080104": {
            "SkuDepPro": "0801",
            "name": "Conayca",
            "sku": "04"
          },
          "080105": {
            "SkuDepPro": "0801",
            "name": "Cuenca",
            "sku": "05"
          },
          "080106": {
            "SkuDepPro": "0801",
            "name": "Huachocolpa",
            "sku": "06"
          },
          "080108": {
            "SkuDepPro": "0801",
            "name": "Huayllahuara",
            "sku": "08"
          },
          "080109": {
            "SkuDepPro": "0801",
            "name": "Izcuchaca",
            "sku": "09"
          },
          "080110": {
            "SkuDepPro": "0801",
            "name": "Laria",
            "sku": "10"
          },
          "080111": {
            "SkuDepPro": "0801",
            "name": "Manta",
            "sku": "11"
          },
          "080112": {
            "SkuDepPro": "0801",
            "name": "Mariscal Caceres",
            "sku": "12"
          },
          "080113": {
            "SkuDepPro": "0801",
            "name": "Moya",
            "sku": "13"
          },
          "080114": {
            "SkuDepPro": "0801",
            "name": "Nuevo Occoro",
            "sku": "14"
          },
          "080115": {
            "SkuDepPro": "0801",
            "name": "Palca",
            "sku": "15"
          },
          "080116": {
            "SkuDepPro": "0801",
            "name": "Pilchaca",
            "sku": "16"
          },
          "080117": {
            "SkuDepPro": "0801",
            "name": "Vilca",
            "sku": "17"
          },
          "080118": {
            "SkuDepPro": "0801",
            "name": "Yauli",
            "sku": "18"
          },
          "080119": {
            "SkuDepPro": "0801",
            "name": "Ascension",
            "sku": "19"
          },
          "080120": {
            "SkuDepPro": "0801",
            "name": "Huando",
            "sku": "20"
          },
          "080201": {
            "SkuDepPro": "0802",
            "name": "Acobamba",
            "sku": "01"
          },
          "080202": {
            "SkuDepPro": "0802",
            "name": "Anta",
            "sku": "02"
          },
          "080203": {
            "SkuDepPro": "0802",
            "name": "Andabamba",
            "sku": "03"
          },
          "080204": {
            "SkuDepPro": "0802",
            "name": "Caja",
            "sku": "04"
          },
          "080205": {
            "SkuDepPro": "0802",
            "name": "Marcas",
            "sku": "05"
          },
          "080206": {
            "SkuDepPro": "0802",
            "name": "Paucara",
            "sku": "06"
          },
          "080207": {
            "SkuDepPro": "0802",
            "name": "Pomacocha",
            "sku": "07"
          },
          "080208": {
            "SkuDepPro": "0802",
            "name": "Rosario",
            "sku": "08"
          },
          "080301": {
            "SkuDepPro": "0803",
            "name": "Lircay",
            "sku": "01"
          },
          "080302": {
            "SkuDepPro": "0803",
            "name": "Anchonga",
            "sku": "02"
          },
          "080303": {
            "SkuDepPro": "0803",
            "name": "Callanmarca",
            "sku": "03"
          },
          "080304": {
            "SkuDepPro": "0803",
            "name": "Congalla",
            "sku": "04"
          },
          "080305": {
            "SkuDepPro": "0803",
            "name": "Chincho",
            "sku": "05"
          },
          "080306": {
            "SkuDepPro": "0803",
            "name": "Huallay-Grande",
            "sku": "06"
          },
          "080307": {
            "SkuDepPro": "0803",
            "name": "Huanca-Huanca",
            "sku": "07"
          },
          "080308": {
            "SkuDepPro": "0803",
            "name": "Julcamarca",
            "sku": "08"
          },
          "080309": {
            "SkuDepPro": "0803",
            "name": "San Antonio De Antaparco",
            "sku": "09"
          },
          "080310": {
            "SkuDepPro": "0803",
            "name": "Santo Tomas De Pata",
            "sku": "10"
          },
          "080311": {
            "SkuDepPro": "0803",
            "name": "Secclla",
            "sku": "11"
          },
          "080312": {
            "SkuDepPro": "0803",
            "name": "Ccochaccasa",
            "sku": "12"
          },
          "080401": {
            "SkuDepPro": "0804",
            "name": "Castrovirreyna",
            "sku": "01"
          },
          "080402": {
            "SkuDepPro": "0804",
            "name": "Arma",
            "sku": "02"
          },
          "080403": {
            "SkuDepPro": "0804",
            "name": "Aurahua",
            "sku": "03"
          },
          "080405": {
            "SkuDepPro": "0804",
            "name": "Capillas",
            "sku": "05"
          },
          "080406": {
            "SkuDepPro": "0804",
            "name": "Cocas",
            "sku": "06"
          },
          "080408": {
            "SkuDepPro": "0804",
            "name": "Chupamarca",
            "sku": "08"
          },
          "080409": {
            "SkuDepPro": "0804",
            "name": "Huachos",
            "sku": "09"
          },
          "080410": {
            "SkuDepPro": "0804",
            "name": "Huamatambo",
            "sku": "10"
          },
          "080414": {
            "SkuDepPro": "0804",
            "name": "Mollepampa",
            "sku": "14"
          },
          "080422": {
            "SkuDepPro": "0804",
            "name": "San Juan",
            "sku": "22"
          },
          "080427": {
            "SkuDepPro": "0804",
            "name": "Tantara",
            "sku": "27"
          },
          "080428": {
            "SkuDepPro": "0804",
            "name": "Ticrapo",
            "sku": "28"
          },
          "080429": {
            "SkuDepPro": "0804",
            "name": "Santa Ana",
            "sku": "29"
          },
          "080501": {
            "SkuDepPro": "0805",
            "name": "Pampas",
            "sku": "01"
          },
          "080502": {
            "SkuDepPro": "0805",
            "name": "Acostambo",
            "sku": "02"
          },
          "080503": {
            "SkuDepPro": "0805",
            "name": "Acraquia",
            "sku": "03"
          },
          "080504": {
            "SkuDepPro": "0805",
            "name": "Ahuaycha",
            "sku": "04"
          },
          "080506": {
            "SkuDepPro": "0805",
            "name": "Colcabamba",
            "sku": "06"
          },
          "080509": {
            "SkuDepPro": "0805",
            "name": "Daniel Hernandez",
            "sku": "09"
          },
          "080511": {
            "SkuDepPro": "0805",
            "name": "Huachocolpa",
            "sku": "11"
          },
          "080512": {
            "SkuDepPro": "0805",
            "name": "Huaribamba",
            "sku": "12"
          },
          "080515": {
            "SkuDepPro": "0805",
            "name": "Ñahuimpuquio",
            "sku": "15"
          },
          "080517": {
            "SkuDepPro": "0805",
            "name": "Pazos",
            "sku": "17"
          },
          "080518": {
            "SkuDepPro": "0805",
            "name": "Quishuar",
            "sku": "18"
          },
          "080519": {
            "SkuDepPro": "0805",
            "name": "Salcabamba",
            "sku": "19"
          },
          "080520": {
            "SkuDepPro": "0805",
            "name": "San Marcos De Rocchac",
            "sku": "20"
          },
          "080523": {
            "SkuDepPro": "0805",
            "name": "Surcubamba",
            "sku": "23"
          },
          "080525": {
            "SkuDepPro": "0805",
            "name": "Tintay Puncu",
            "sku": "25"
          },
          "080526": {
            "SkuDepPro": "0805",
            "name": "Salcahuasi",
            "sku": "26"
          },
          "080601": {
            "SkuDepPro": "0806",
            "name": "Ayavi",
            "sku": "01"
          },
          "080602": {
            "SkuDepPro": "0806",
            "name": "Cordova",
            "sku": "02"
          },
          "080603": {
            "SkuDepPro": "0806",
            "name": "Huayacundo Arma",
            "sku": "03"
          },
          "080604": {
            "SkuDepPro": "0806",
            "name": "Huaytara",
            "sku": "04"
          },
          "080605": {
            "SkuDepPro": "0806",
            "name": "Laramarca",
            "sku": "05"
          },
          "080606": {
            "SkuDepPro": "0806",
            "name": "Ocoyo",
            "sku": "06"
          },
          "080607": {
            "SkuDepPro": "0806",
            "name": "Pilpichaca",
            "sku": "07"
          },
          "080608": {
            "SkuDepPro": "0806",
            "name": "Querco",
            "sku": "08"
          },
          "080609": {
            "SkuDepPro": "0806",
            "name": "Quito Arma",
            "sku": "09"
          },
          "080610": {
            "SkuDepPro": "0806",
            "name": "San Antonio De Cusicancha",
            "sku": "10"
          },
          "080611": {
            "SkuDepPro": "0806",
            "name": "San Francisco De Sangayaico",
            "sku": "11"
          },
          "080612": {
            "SkuDepPro": "0806",
            "name": "San Isidro",
            "sku": "12"
          },
          "080613": {
            "SkuDepPro": "0806",
            "name": "Santiago De Chocorvos",
            "sku": "13"
          },
          "080614": {
            "SkuDepPro": "0806",
            "name": "Santiago De Quirahuara",
            "sku": "14"
          },
          "080615": {
            "SkuDepPro": "0806",
            "name": "Santo Domingo De Capillas",
            "sku": "15"
          },
          "080616": {
            "SkuDepPro": "0806",
            "name": "Tambo",
            "sku": "16"
          },
          "080701": {
            "SkuDepPro": "0807",
            "name": "Churcampa",
            "sku": "01"
          },
          "080702": {
            "SkuDepPro": "0807",
            "name": "Anco",
            "sku": "02"
          },
          "080703": {
            "SkuDepPro": "0807",
            "name": "Chinchihuasi",
            "sku": "03"
          },
          "080704": {
            "SkuDepPro": "0807",
            "name": "El Carmen",
            "sku": "04"
          },
          "080705": {
            "SkuDepPro": "0807",
            "name": "La Merced",
            "sku": "05"
          },
          "080706": {
            "SkuDepPro": "0807",
            "name": "Locroja",
            "sku": "06"
          },
          "080707": {
            "SkuDepPro": "0807",
            "name": "Paucarbamba",
            "sku": "07"
          },
          "080708": {
            "SkuDepPro": "0807",
            "name": "San Miguel De Mayocc",
            "sku": "08"
          },
          "080709": {
            "SkuDepPro": "0807",
            "name": "San Pedro De Coris",
            "sku": "09"
          },
          "080710": {
            "SkuDepPro": "0807",
            "name": "Pachamarca",
            "sku": "10"
          },
          "080711": {
            "SkuDepPro": "0807",
            "name": "Cosme",
            "sku": "11"
          },
          "090101": {
            "SkuDepPro": "0901",
            "name": "Huánuco",
            "sku": "01"
          },
          "090102": {
            "SkuDepPro": "0901",
            "name": "Chinchao",
            "sku": "02"
          },
          "090103": {
            "SkuDepPro": "0901",
            "name": "Churubamba",
            "sku": "03"
          },
          "090104": {
            "SkuDepPro": "0901",
            "name": "Margos",
            "sku": "04"
          },
          "090105": {
            "SkuDepPro": "0901",
            "name": "Quisqui",
            "sku": "05"
          },
          "090106": {
            "SkuDepPro": "0901",
            "name": "San Francisco De Cayran",
            "sku": "06"
          },
          "090107": {
            "SkuDepPro": "0901",
            "name": "San Pedro De Chaulan",
            "sku": "07"
          },
          "090108": {
            "SkuDepPro": "0901",
            "name": "Santa Maria Del Valle",
            "sku": "08"
          },
          "090109": {
            "SkuDepPro": "0901",
            "name": "Yarumayo",
            "sku": "09"
          },
          "090110": {
            "SkuDepPro": "0901",
            "name": "Amarilis",
            "sku": "10"
          },
          "090111": {
            "SkuDepPro": "0901",
            "name": "Pillco Marca",
            "sku": "11"
          },
          "090112": {
            "SkuDepPro": "0901",
            "name": "Yacus",
            "sku": "12"
          },
          "090201": {
            "SkuDepPro": "0902",
            "name": "Ambo",
            "sku": "01"
          },
          "090202": {
            "SkuDepPro": "0902",
            "name": "Cayna",
            "sku": "02"
          },
          "090203": {
            "SkuDepPro": "0902",
            "name": "Colpas",
            "sku": "03"
          },
          "090204": {
            "SkuDepPro": "0902",
            "name": "Conchamarca",
            "sku": "04"
          },
          "090205": {
            "SkuDepPro": "0902",
            "name": "Huacar",
            "sku": "05"
          },
          "090206": {
            "SkuDepPro": "0902",
            "name": "San Francisco",
            "sku": "06"
          },
          "090207": {
            "SkuDepPro": "0902",
            "name": "San Rafael",
            "sku": "07"
          },
          "090208": {
            "SkuDepPro": "0902",
            "name": "Tomay-Kichwa",
            "sku": "08"
          },
          "090301": {
            "SkuDepPro": "0903",
            "name": "La Union",
            "sku": "01"
          },
          "090307": {
            "SkuDepPro": "0903",
            "name": "Chuquis",
            "sku": "07"
          },
          "090312": {
            "SkuDepPro": "0903",
            "name": "Marias",
            "sku": "12"
          },
          "090314": {
            "SkuDepPro": "0903",
            "name": "Pachas",
            "sku": "14"
          },
          "090316": {
            "SkuDepPro": "0903",
            "name": "Quivilla",
            "sku": "16"
          },
          "090317": {
            "SkuDepPro": "0903",
            "name": "Ripan",
            "sku": "17"
          },
          "090321": {
            "SkuDepPro": "0903",
            "name": "Shunqui",
            "sku": "21"
          },
          "090322": {
            "SkuDepPro": "0903",
            "name": "Sillapata",
            "sku": "22"
          },
          "090323": {
            "SkuDepPro": "0903",
            "name": "Yanas",
            "sku": "23"
          },
          "090401": {
            "SkuDepPro": "0904",
            "name": "Llata",
            "sku": "01"
          },
          "090402": {
            "SkuDepPro": "0904",
            "name": "Arancay",
            "sku": "02"
          },
          "090403": {
            "SkuDepPro": "0904",
            "name": "Chavin De Pariarca",
            "sku": "03"
          },
          "090404": {
            "SkuDepPro": "0904",
            "name": "Jacas Grande",
            "sku": "04"
          },
          "090405": {
            "SkuDepPro": "0904",
            "name": "Jircan",
            "sku": "05"
          },
          "090406": {
            "SkuDepPro": "0904",
            "name": "Miraflores",
            "sku": "06"
          },
          "090407": {
            "SkuDepPro": "0904",
            "name": "Monzon",
            "sku": "07"
          },
          "090408": {
            "SkuDepPro": "0904",
            "name": "Punchao",
            "sku": "08"
          },
          "090409": {
            "SkuDepPro": "0904",
            "name": "Puños",
            "sku": "09"
          },
          "090410": {
            "SkuDepPro": "0904",
            "name": "Singa",
            "sku": "10"
          },
          "090411": {
            "SkuDepPro": "0904",
            "name": "Tantamayo",
            "sku": "11"
          },
          "090501": {
            "SkuDepPro": "0905",
            "name": "Huacrachuco",
            "sku": "01"
          },
          "090502": {
            "SkuDepPro": "0905",
            "name": "Cholon",
            "sku": "02"
          },
          "090505": {
            "SkuDepPro": "0905",
            "name": "San Buenaventura",
            "sku": "05"
          },
          "090601": {
            "SkuDepPro": "0906",
            "name": "Rupa-Rupa",
            "sku": "01"
          },
          "090602": {
            "SkuDepPro": "0906",
            "name": "Daniel Alomia Robles",
            "sku": "02"
          },
          "090603": {
            "SkuDepPro": "0906",
            "name": "Hermilio Valdizan",
            "sku": "03"
          },
          "090604": {
            "SkuDepPro": "0906",
            "name": "Luyando",
            "sku": "04"
          },
          "090605": {
            "SkuDepPro": "0906",
            "name": "Mariano Damaso Beraun",
            "sku": "05"
          },
          "090606": {
            "SkuDepPro": "0906",
            "name": "Jose Crespo Y Castillo",
            "sku": "06"
          },
          "090701": {
            "SkuDepPro": "0907",
            "name": "Panao",
            "sku": "01"
          },
          "090702": {
            "SkuDepPro": "0907",
            "name": "Chaglla",
            "sku": "02"
          },
          "090704": {
            "SkuDepPro": "0907",
            "name": "Molino",
            "sku": "04"
          },
          "090706": {
            "SkuDepPro": "0907",
            "name": "Umari",
            "sku": "06"
          },
          "090801": {
            "SkuDepPro": "0908",
            "name": "Honoria",
            "sku": "01"
          },
          "090802": {
            "SkuDepPro": "0908",
            "name": "Puerto Inca",
            "sku": "02"
          },
          "090803": {
            "SkuDepPro": "0908",
            "name": "Codo Del Pozuzo",
            "sku": "03"
          },
          "090804": {
            "SkuDepPro": "0908",
            "name": "Tournavista",
            "sku": "04"
          },
          "090805": {
            "SkuDepPro": "0908",
            "name": "Yuyapichis",
            "sku": "05"
          },
          "090901": {
            "SkuDepPro": "0909",
            "name": "Huacaybamba",
            "sku": "01"
          },
          "090902": {
            "SkuDepPro": "0909",
            "name": "Pinra",
            "sku": "02"
          },
          "090903": {
            "SkuDepPro": "0909",
            "name": "Canchabamba",
            "sku": "03"
          },
          "090904": {
            "SkuDepPro": "0909",
            "name": "Cochabamba",
            "sku": "04"
          },
          "091001": {
            "SkuDepPro": "0910",
            "name": "Jesus",
            "sku": "01"
          },
          "091002": {
            "SkuDepPro": "0910",
            "name": "Baños",
            "sku": "02"
          },
          "091003": {
            "SkuDepPro": "0910",
            "name": "San Francisco De Asis",
            "sku": "03"
          },
          "091004": {
            "SkuDepPro": "0910",
            "name": "Queropalca",
            "sku": "04"
          },
          "091005": {
            "SkuDepPro": "0910",
            "name": "San Miguel De Cauri",
            "sku": "05"
          },
          "091006": {
            "SkuDepPro": "0910",
            "name": "Rondos",
            "sku": "06"
          },
          "091007": {
            "SkuDepPro": "0910",
            "name": "Jivia",
            "sku": "07"
          },
          "091101": {
            "SkuDepPro": "0911",
            "name": "Chavinillo",
            "sku": "01"
          },
          "091102": {
            "SkuDepPro": "0911",
            "name": "Aparicio Pomares",
            "sku": "02"
          },
          "091103": {
            "SkuDepPro": "0911",
            "name": "Cahuac",
            "sku": "03"
          },
          "091104": {
            "SkuDepPro": "0911",
            "name": "Chacabamba",
            "sku": "04"
          },
          "091105": {
            "SkuDepPro": "0911",
            "name": "Jacas Chico",
            "sku": "05"
          },
          "091106": {
            "SkuDepPro": "0911",
            "name": "Obas",
            "sku": "06"
          },
          "091107": {
            "SkuDepPro": "0911",
            "name": "Pampamarca",
            "sku": "07"
          },
          "091108": {
            "SkuDepPro": "0911",
            "name": "Choras",
            "sku": "08"
          }
        };



//

export var DEPARTMENTS_DIRECTION = {
  "10": {
    "name": "Huánuco",
    "sku": "10"
  },
  "11": {
    "name": "Ica",
    "sku": "11"
  },
  "12": {
    "name": "Junín",
    "sku": "12"
  },
  "13": {
    "name": "La Libertad",
    "sku": "13"
  },
  "14": {
    "name": "Lambayeque",
    "sku": "14"
  },
  "15": {
    "name": "Lima",
    "sku": "15"
  },
  "16": {
    "name": "Loreto",
    "sku": "16"
  },
  "17": {
    "name": "Madre de Dios",
    "sku": "17"
  },
  "18": {
    "name": "Moquegua",
    "sku": "18"
  },
  "19": {
    "name": "Pasco",
    "sku": "19"
  },
  "20": {
    "name": "Piura",
    "sku": "20"
  },
  "21": {
    "name": "Puno",
    "sku": "21"
  },
  "22": {
    "name": "San Martín",
    "sku": "22"
  },
  "23": {
    "name": "Tacna",
    "sku": "23"
  },
  "24": {
    "name": "Tumbes",
    "sku": "24"
  },
  "25": {
    "name": "Ucayali",
    "sku": "25"
  },
  "01": {
    "name": "Amazonas",
    "sku": "01"
  },
  "02": {
    "name": "Ancash",
    "sku": "02"
  },
  "03": {
    "name": "Apurímac",
    "sku": "03"
  },
  "04": {
    "name": "Arequipa",
    "sku": "04"
  },
  "05": {
    "name": "Ayacucho",
    "sku": "05"
  },
  "06": {
    "name": "Cajamarca",
    "sku": "06"
  },
  "07": {
    "name": "Callao",
    "sku": "07"
  },
  "08": {
    "name": "Cusco",
    "sku": "08"
  },
  "09": {
    "name": "Huancavelica",
    "sku": "09"
  }
};


export var PROVINCE_DIRECTION = {
  "1001": {
    "name": "Huánuco",
    "sku": "01",
    "skuDep": "10"
  },
  "1002": {
    "name": "Ambo",
    "sku": "02",
    "skuDep": "10"
  },
  "1003": {
    "name": "Dos de Mayo",
    "sku": "03",
    "skuDep": "10"
  },
  "1004": {
    "name": "Huacaybamba",
    "sku": "04",
    "skuDep": "10"
  },
  "1005": {
    "name": "Huamalies",
    "sku": "05",
    "skuDep": "10"
  },
  "1006": {
    "name": "Leoncio Prado",
    "sku": "06",
    "skuDep": "10"
  },
  "1007": {
    "name": "Marañon",
    "sku": "07",
    "skuDep": "10"
  },
  "1008": {
    "name": "Pachitea",
    "sku": "08",
    "skuDep": "10"
  },
  "1009": {
    "name": "Puerto Inca",
    "sku": "09",
    "skuDep": "10"
  },
  "1010": {
    "name": "Lauricocha",
    "sku": "10",
    "skuDep": "10"
  },
  "1011": {
    "name": "Yarowilca",
    "sku": "11",
    "skuDep": "10"
  },
  "1101": {
    "name": "Ica",
    "sku": "01",
    "skuDep": "11"
  },
  "1102": {
    "name": "Chincha",
    "sku": "02",
    "skuDep": "11"
  },
  "1103": {
    "name": "Nasca",
    "sku": "03",
    "skuDep": "11"
  },
  "1104": {
    "name": "Palpa",
    "sku": "04",
    "skuDep": "11"
  },
  "1105": {
    "name": "Pisco",
    "sku": "05",
    "skuDep": "11"
  },
  "1201": {
    "name": "Huancayo",
    "sku": "01",
    "skuDep": "12"
  },
  "1202": {
    "name": "Concepción",
    "sku": "02",
    "skuDep": "12"
  },
  "1203": {
    "name": "Chanchamayo",
    "sku": "03",
    "skuDep": "12"
  },
  "1204": {
    "name": "Jauja",
    "sku": "04",
    "skuDep": "12"
  },
  "1205": {
    "name": "Junín",
    "sku": "05",
    "skuDep": "12"
  },
  "1206": {
    "name": "Satipo",
    "sku": "06",
    "skuDep": "12"
  },
  "1207": {
    "name": "Tarma",
    "sku": "07",
    "skuDep": "12"
  },
  "1208": {
    "name": "Yauli",
    "sku": "08",
    "skuDep": "12"
  },
  "1209": {
    "name": "Chupaca",
    "sku": "09",
    "skuDep": "12"
  },
  "1301": {
    "name": "Trujillo",
    "sku": "01",
    "skuDep": "13"
  },
  "1302": {
    "name": "Ascope",
    "sku": "02",
    "skuDep": "13"
  },
  "1303": {
    "name": "Bolívar",
    "sku": "03",
    "skuDep": "13"
  },
  "1304": {
    "name": "Chepén",
    "sku": "04",
    "skuDep": "13"
  },
  "1305": {
    "name": "Julcán",
    "sku": "05",
    "skuDep": "13"
  },
  "1306": {
    "name": "Otuzco",
    "sku": "06",
    "skuDep": "13"
  },
  "1307": {
    "name": "Pacasmayo",
    "sku": "07",
    "skuDep": "13"
  },
  "1308": {
    "name": "Pataz",
    "sku": "08",
    "skuDep": "13"
  },
  "1309": {
    "name": "Sánchez Carrión",
    "sku": "09",
    "skuDep": "13"
  },
  "1310": {
    "name": "Santiago de Chuco",
    "sku": "10",
    "skuDep": "13"
  },
  "1311": {
    "name": "Gran Chimú",
    "sku": "11",
    "skuDep": "13"
  },
  "1312": {
    "name": "Virú",
    "sku": "12",
    "skuDep": "13"
  },
  "1401": {
    "name": "Chiclayo",
    "sku": "01",
    "skuDep": "14"
  },
  "1402": {
    "name": "Ferreñafe",
    "sku": "02",
    "skuDep": "14"
  },
  "1403": {
    "name": "Lambayeque",
    "sku": "03",
    "skuDep": "14"
  },
  "1501": {
    "name": "Lima",
    "sku": "01",
    "skuDep": "15"
  },
  "1502": {
    "name": "Barranca",
    "sku": "02",
    "skuDep": "15"
  },
  "1503": {
    "name": "Cajatambo",
    "sku": "03",
    "skuDep": "15"
  },
  "1504": {
    "name": "Canta",
    "sku": "04",
    "skuDep": "15"
  },
  "1505": {
    "name": "Cañete",
    "sku": "05",
    "skuDep": "15"
  },
  "1506": {
    "name": "Huaral",
    "sku": "06",
    "skuDep": "15"
  },
  "1507": {
    "name": "Huarochirí",
    "sku": "07",
    "skuDep": "15"
  },
  "1508": {
    "name": "Huaura",
    "sku": "08",
    "skuDep": "15"
  },
  "1509": {
    "name": "Oyón",
    "sku": "09",
    "skuDep": "15"
  },
  "1510": {
    "name": "Yauyos",
    "sku": "10",
    "skuDep": "15"
  },
  "1601": {
    "name": "Maynas",
    "sku": "01",
    "skuDep": "16"
  },
  "1602": {
    "name": "Alto Amazonas",
    "sku": "02",
    "skuDep": "16"
  },
  "1603": {
    "name": "Loreto",
    "sku": "03",
    "skuDep": "16"
  },
  "1604": {
    "name": "Mariscal Ramón Castilla",
    "sku": "04",
    "skuDep": "16"
  },
  "1605": {
    "name": "Requena",
    "sku": "05",
    "skuDep": "16"
  },
  "1606": {
    "name": "Ucayali",
    "sku": "06",
    "skuDep": "16"
  },
  "1607": {
    "name": "Datem Del Marañón",
    "sku": "07",
    "skuDep": "16"
  },
  "1608": {
    "name": "Putumayo",
    "sku": "08",
    "skuDep": "16"
  },
  "1701": {
    "name": "Tambopata",
    "sku": "01",
    "skuDep": "17"
  },
  "1702": {
    "name": "Manu",
    "sku": "02",
    "skuDep": "17"
  },
  "1703": {
    "name": "Tahuamanu",
    "sku": "03",
    "skuDep": "17"
  },
  "1801": {
    "name": "Mariscal Nieto",
    "sku": "01",
    "skuDep": "18"
  },
  "1802": {
    "name": "General Sánchez Cerro",
    "sku": "02",
    "skuDep": "18"
  },
  "1803": {
    "name": "Ilo",
    "sku": "03",
    "skuDep": "18"
  },
  "1901": {
    "name": "Pasco",
    "sku": "01",
    "skuDep": "19"
  },
  "1902": {
    "name": "Daniel Alcides Carrión",
    "sku": "02",
    "skuDep": "19"
  },
  "1903": {
    "name": "Oxapampa",
    "sku": "03",
    "skuDep": "19"
  },
  "2001": {
    "name": "Piura",
    "sku": "01",
    "skuDep": "20"
  },
  "2002": {
    "name": "Ayabaca",
    "sku": "02",
    "skuDep": "20"
  },
  "2003": {
    "name": "Huancabamba",
    "sku": "03",
    "skuDep": "20"
  },
  "2004": {
    "name": "Morropón",
    "sku": "04",
    "skuDep": "20"
  },
  "2005": {
    "name": "Paita",
    "sku": "05",
    "skuDep": "20"
  },
  "2006": {
    "name": "Sullana",
    "sku": "06",
    "skuDep": "20"
  },
  "2007": {
    "name": "Talara",
    "sku": "07",
    "skuDep": "20"
  },
  "2008": {
    "name": "Sechura",
    "sku": "08",
    "skuDep": "20"
  },
  "2101": {
    "name": "Puno",
    "sku": "01",
    "skuDep": "21"
  },
  "2102": {
    "name": "Azángaro",
    "sku": "02",
    "skuDep": "21"
  },
  "2103": {
    "name": "Carabaya",
    "sku": "03",
    "skuDep": "21"
  },
  "2104": {
    "name": "Chucuito",
    "sku": "04",
    "skuDep": "21"
  },
  "2105": {
    "name": "El Collao",
    "sku": "05",
    "skuDep": "21"
  },
  "2106": {
    "name": "Huancané",
    "sku": "06",
    "skuDep": "21"
  },
  "2107": {
    "name": "Lampa",
    "sku": "07",
    "skuDep": "21"
  },
  "2108": {
    "name": "Melgar",
    "sku": "08",
    "skuDep": "21"
  },
  "2109": {
    "name": "Moho",
    "sku": "09",
    "skuDep": "21"
  },
  "2110": {
    "name": "San Antonio de Putina",
    "sku": "10",
    "skuDep": "21"
  },
  "2111": {
    "name": "San Roman",
    "sku": "11",
    "skuDep": "21"
  },
  "2112": {
    "name": "Sandia",
    "sku": "12",
    "skuDep": "21"
  },
  "2113": {
    "name": "Yunguyo",
    "sku": "13",
    "skuDep": "21"
  },
  "2201": {
    "name": "Moyobamba",
    "sku": "01",
    "skuDep": "22"
  },
  "2202": {
    "name": "Bellavista",
    "sku": "02",
    "skuDep": "22"
  },
  "2203": {
    "name": "El Dorado",
    "sku": "03",
    "skuDep": "22"
  },
  "2204": {
    "name": "Huallaga",
    "sku": "04",
    "skuDep": "22"
  },
  "2205": {
    "name": "Lamas",
    "sku": "05",
    "skuDep": "22"
  },
  "2206": {
    "name": "Mariscal Caceres",
    "sku": "06",
    "skuDep": "22"
  },
  "2207": {
    "name": "Picota",
    "sku": "07",
    "skuDep": "22"
  },
  "2208": {
    "name": "Rioja",
    "sku": "08",
    "skuDep": "22"
  },
  "2209": {
    "name": "San Martín",
    "sku": "09",
    "skuDep": "22"
  },
  "2210": {
    "name": "Tocache",
    "sku": "10",
    "skuDep": "22"
  },
  "2301": {
    "name": "Tacna",
    "sku": "01",
    "skuDep": "23"
  },
  "2302": {
    "name": "Candarave",
    "sku": "02",
    "skuDep": "23"
  },
  "2303": {
    "name": "Jorge Basadre",
    "sku": "03",
    "skuDep": "23"
  },
  "2304": {
    "name": "Tarata",
    "sku": "04",
    "skuDep": "23"
  },
  "2401": {
    "name": "Tumbes",
    "sku": "01",
    "skuDep": "24"
  },
  "2402": {
    "name": "Contralmirante Villar",
    "sku": "02",
    "skuDep": "24"
  },
  "2403": {
    "name": "Zarumilla",
    "sku": "03",
    "skuDep": "24"
  },
  "2501": {
    "name": "Coronel Portillo",
    "sku": "01",
    "skuDep": "25"
  },
  "2502": {
    "name": "Atalaya",
    "sku": "02",
    "skuDep": "25"
  },
  "2503": {
    "name": "Padre Abad",
    "sku": "03",
    "skuDep": "25"
  },
  "2504": {
    "name": "Purus",
    "sku": "04",
    "skuDep": "25"
  },
  "0101": {
    "name": "Chachapoyas",
    "sku": "01",
    "skuDep": "01"
  },
  "0102": {
    "name": "Bagua",
    "sku": "02",
    "skuDep": "01"
  },
  "0103": {
    "name": "Bongara",
    "sku": "03",
    "skuDep": "01"
  },
  "0104": {
    "name": "Condorcanqui",
    "sku": "04",
    "skuDep": "01"
  },
  "0105": {
    "name": "Luya",
    "sku": "05",
    "skuDep": "01"
  },
  "0106": {
    "name": "Rodriguez de Mendoza",
    "sku": "06",
    "skuDep": "01"
  },
  "0107": {
    "name": "Utcubamba",
    "sku": "07",
    "skuDep": "01"
  },
  "0201": {
    "name": "Huaraz",
    "sku": "01",
    "skuDep": "02"
  },
  "0202": {
    "name": "Aija",
    "sku": "02",
    "skuDep": "02"
  },
  "0203": {
    "name": "Antonio Raymondi",
    "sku": "03",
    "skuDep": "02"
  },
  "0204": {
    "name": "Asuncion",
    "sku": "04",
    "skuDep": "02"
  },
  "0205": {
    "name": "Bolognesi",
    "sku": "05",
    "skuDep": "02"
  },
  "0206": {
    "name": "Carhuaz",
    "sku": "06",
    "skuDep": "02"
  },
  "0207": {
    "name": "Carlos Fermin Fitzcarrald",
    "sku": "07",
    "skuDep": "02"
  },
  "0208": {
    "name": "Casma",
    "sku": "08",
    "skuDep": "02"
  },
  "0209": {
    "name": "Corongo",
    "sku": "09",
    "skuDep": "02"
  },
  "0210": {
    "name": "Huari",
    "sku": "10",
    "skuDep": "02"
  },
  "0211": {
    "name": "Huarmey",
    "sku": "11",
    "skuDep": "02"
  },
  "0212": {
    "name": "Huaylas",
    "sku": "12",
    "skuDep": "02"
  },
  "0213": {
    "name": "Mariscal Luzuriaga",
    "sku": "13",
    "skuDep": "02"
  },
  "0214": {
    "name": "Ocros",
    "sku": "14",
    "skuDep": "02"
  },
  "0215": {
    "name": "Pallasca",
    "sku": "15",
    "skuDep": "02"
  },
  "0216": {
    "name": "Pomabamba",
    "sku": "16",
    "skuDep": "02"
  },
  "0217": {
    "name": "Recuay",
    "sku": "17",
    "skuDep": "02"
  },
  "0218": {
    "name": "Santa",
    "sku": "18",
    "skuDep": "02"
  },
  "0219": {
    "name": "Sihuas",
    "sku": "19",
    "skuDep": "02"
  },
  "0220": {
    "name": "Yungay",
    "sku": "20",
    "skuDep": "02"
  },
  "0301": {
    "name": "Abancay",
    "sku": "01",
    "skuDep": "03"
  },
  "0302": {
    "name": "Andahuaylas",
    "sku": "02",
    "skuDep": "03"
  },
  "0303": {
    "name": "Antabamba",
    "sku": "03",
    "skuDep": "03"
  },
  "0304": {
    "name": "Aymaraes",
    "sku": "04",
    "skuDep": "03"
  },
  "0305": {
    "name": "Cotabambas",
    "sku": "05",
    "skuDep": "03"
  },
  "0306": {
    "name": "Chincheros",
    "sku": "06",
    "skuDep": "03"
  },
  "0307": {
    "name": "Grau",
    "sku": "07",
    "skuDep": "03"
  },
  "0401": {
    "name": "Arequipa",
    "sku": "01",
    "skuDep": "04"
  },
  "0402": {
    "name": "Camana",
    "sku": "02",
    "skuDep": "04"
  },
  "0403": {
    "name": "Caraveli",
    "sku": "03",
    "skuDep": "04"
  },
  "0404": {
    "name": "Castilla",
    "sku": "04",
    "skuDep": "04"
  },
  "0405": {
    "name": "Caylloma",
    "sku": "05",
    "skuDep": "04"
  },
  "0406": {
    "name": "Condesuyos",
    "sku": "06",
    "skuDep": "04"
  },
  "0407": {
    "name": "Islay",
    "sku": "07",
    "skuDep": "04"
  },
  "0408": {
    "name": "La Unión",
    "sku": "08",
    "skuDep": "04"
  },
  "0501": {
    "name": "Huamanga",
    "sku": "01",
    "skuDep": "05"
  },
  "0502": {
    "name": "Cangallo",
    "sku": "02",
    "skuDep": "05"
  },
  "0503": {
    "name": "Huanca Sancos",
    "sku": "03",
    "skuDep": "05"
  },
  "0504": {
    "name": "Huanta",
    "sku": "04",
    "skuDep": "05"
  },
  "0505": {
    "name": "La Mar",
    "sku": "05",
    "skuDep": "05"
  },
  "0506": {
    "name": "Lucanas",
    "sku": "06",
    "skuDep": "05"
  },
  "0507": {
    "name": "Parinacochas",
    "sku": "07",
    "skuDep": "05"
  },
  "0508": {
    "name": "Páucar del Sara Sara",
    "sku": "08",
    "skuDep": "05"
  },
  "0509": {
    "name": "Sucre",
    "sku": "09",
    "skuDep": "05"
  },
  "0510": {
    "name": "Victor Fajardo",
    "sku": "10",
    "skuDep": "05"
  },
  "0511": {
    "name": "Vilcas Huaman",
    "sku": "11",
    "skuDep": "05"
  },
  "0601": {
    "name": "Cajamarca",
    "sku": "01",
    "skuDep": "06"
  },
  "0602": {
    "name": "Cajabamba",
    "sku": "02",
    "skuDep": "06"
  },
  "0603": {
    "name": "Celendin",
    "sku": "03",
    "skuDep": "06"
  },
  "0604": {
    "name": "Chota",
    "sku": "04",
    "skuDep": "06"
  },
  "0605": {
    "name": "Contumaza",
    "sku": "05",
    "skuDep": "06"
  },
  "0606": {
    "name": "Cutervo",
    "sku": "06",
    "skuDep": "06"
  },
  "0607": {
    "name": "Hualgayoc",
    "sku": "07",
    "skuDep": "06"
  },
  "0608": {
    "name": "Jaen",
    "sku": "08",
    "skuDep": "06"
  },
  "0609": {
    "name": "San Ignacio",
    "sku": "09",
    "skuDep": "06"
  },
  "0610": {
    "name": "San Marcos",
    "sku": "10",
    "skuDep": "06"
  },
  "0611": {
    "name": "San Miguel",
    "sku": "11",
    "skuDep": "06"
  },
  "0612": {
    "name": "San Pablo",
    "sku": "12",
    "skuDep": "06"
  },
  "0613": {
    "name": "Santa Cruz",
    "sku": "13",
    "skuDep": "06"
  },
  "0701": {
    "name": "Prov. Const. del Callao",
    "sku": "01",
    "skuDep": "07"
  },
  "0801": {
    "name": "Cusco",
    "sku": "01",
    "skuDep": "08"
  },
  "0802": {
    "name": "Acomayo",
    "sku": "02",
    "skuDep": "08"
  },
  "0803": {
    "name": "Anta",
    "sku": "03",
    "skuDep": "08"
  },
  "0804": {
    "name": "Calca",
    "sku": "04",
    "skuDep": "08"
  },
  "0805": {
    "name": "Canas",
    "sku": "05",
    "skuDep": "08"
  },
  "0806": {
    "name": "Canchis",
    "sku": "06",
    "skuDep": "08"
  },
  "0807": {
    "name": "Chumbivilcas",
    "sku": "07",
    "skuDep": "08"
  },
  "0808": {
    "name": "Espinar",
    "sku": "08",
    "skuDep": "08"
  },
  "0809": {
    "name": "La Convencion",
    "sku": "09",
    "skuDep": "08"
  },
  "0810": {
    "name": "Paruro",
    "sku": "10",
    "skuDep": "08"
  },
  "0811": {
    "name": "Paucartambo",
    "sku": "11",
    "skuDep": "08"
  },
  "0812": {
    "name": "Quispicanchi",
    "sku": "12",
    "skuDep": "08"
  },
  "0813": {
    "name": "Urubamba",
    "sku": "13",
    "skuDep": "08"
  },
  "0901": {
    "name": "Huancavelica",
    "sku": "01",
    "skuDep": "09"
  },
  "0902": {
    "name": "Acobamba",
    "sku": "02",
    "skuDep": "09"
  },
  "0903": {
    "name": "Angaraes",
    "sku": "03",
    "skuDep": "09"
  },
  "0904": {
    "name": "Castrovirreyna",
    "sku": "04",
    "skuDep": "09"
  },
  "0905": {
    "name": "Churcampa",
    "sku": "05",
    "skuDep": "09"
  },
  "0906": {
    "name": "Huaytara",
    "sku": "06",
    "skuDep": "09"
  },
  "0907": {
    "name": "Tayacaja",
    "sku": "07",
    "skuDep": "09"
  }
};

export var DISTRICT_DIRECTION = {
  "100101": {
    "name": "Huánuco",
    "sku": "01",
    "skuDepPro": "1001"
  },
  "100102": {
    "name": "Amarilis",
    "sku": "02",
    "skuDepPro": "1001"
  },
  "100103": {
    "name": "Chinchao",
    "sku": "03",
    "skuDepPro": "1001"
  },
  "100104": {
    "name": "Churubamba",
    "sku": "04",
    "skuDepPro": "1001"
  },
  "100105": {
    "name": "Margos",
    "sku": "05",
    "skuDepPro": "1001"
  },
  "100106": {
    "name": "Quisqui (Kichki)",
    "sku": "06",
    "skuDepPro": "1001"
  },
  "100107": {
    "name": "San Francisco de Cayran",
    "sku": "07",
    "skuDepPro": "1001"
  },
  "100108": {
    "name": "San Pedro de Chaulan",
    "sku": "08",
    "skuDepPro": "1001"
  },
  "100109": {
    "name": "Santa Maria del Valle",
    "sku": "09",
    "skuDepPro": "1001"
  },
  "100110": {
    "name": "Yarumayo",
    "sku": "10",
    "skuDepPro": "1001"
  },
  "100111": {
    "name": "Pillco Marca",
    "sku": "11",
    "skuDepPro": "1001"
  },
  "100112": {
    "name": "Yacus",
    "sku": "12",
    "skuDepPro": "1001"
  },
  "100113": {
    "name": "San Pablo de Pillao",
    "sku": "13",
    "skuDepPro": "1001"
  },
  "100201": {
    "name": "Ambo",
    "sku": "01",
    "skuDepPro": "1002"
  },
  "100202": {
    "name": "Cayna",
    "sku": "02",
    "skuDepPro": "1002"
  },
  "100203": {
    "name": "Colpas",
    "sku": "03",
    "skuDepPro": "1002"
  },
  "100204": {
    "name": "Conchamarca",
    "sku": "04",
    "skuDepPro": "1002"
  },
  "100205": {
    "name": "Huacar",
    "sku": "05",
    "skuDepPro": "1002"
  },
  "100206": {
    "name": "San Francisco",
    "sku": "06",
    "skuDepPro": "1002"
  },
  "100207": {
    "name": "San Rafael",
    "sku": "07",
    "skuDepPro": "1002"
  },
  "100208": {
    "name": "Tomay Kichwa",
    "sku": "08",
    "skuDepPro": "1002"
  },
  "100301": {
    "name": "La Union",
    "sku": "01",
    "skuDepPro": "1003"
  },
  "100307": {
    "name": "Chuquis",
    "sku": "07",
    "skuDepPro": "1003"
  },
  "100311": {
    "name": "Marias",
    "sku": "11",
    "skuDepPro": "1003"
  },
  "100313": {
    "name": "Pachas",
    "sku": "13",
    "skuDepPro": "1003"
  },
  "100316": {
    "name": "Quivilla",
    "sku": "16",
    "skuDepPro": "1003"
  },
  "100317": {
    "name": "Ripan",
    "sku": "17",
    "skuDepPro": "1003"
  },
  "100321": {
    "name": "Shunqui",
    "sku": "21",
    "skuDepPro": "1003"
  },
  "100322": {
    "name": "Sillapata",
    "sku": "22",
    "skuDepPro": "1003"
  },
  "100323": {
    "name": "Yanas",
    "sku": "23",
    "skuDepPro": "1003"
  },
  "100401": {
    "name": "Huacaybamba",
    "sku": "01",
    "skuDepPro": "1004"
  },
  "100402": {
    "name": "Canchabamba",
    "sku": "02",
    "skuDepPro": "1004"
  },
  "100403": {
    "name": "Cochabamba",
    "sku": "03",
    "skuDepPro": "1004"
  },
  "100404": {
    "name": "Pinra",
    "sku": "04",
    "skuDepPro": "1004"
  },
  "100501": {
    "name": "Llata",
    "sku": "01",
    "skuDepPro": "1005"
  },
  "100502": {
    "name": "Arancay",
    "sku": "02",
    "skuDepPro": "1005"
  },
  "100503": {
    "name": "Chavin de Pariarca",
    "sku": "03",
    "skuDepPro": "1005"
  },
  "100504": {
    "name": "Jacas Grande",
    "sku": "04",
    "skuDepPro": "1005"
  },
  "100505": {
    "name": "Jircan",
    "sku": "05",
    "skuDepPro": "1005"
  },
  "100506": {
    "name": "Miraflores",
    "sku": "06",
    "skuDepPro": "1005"
  },
  "100507": {
    "name": "Monzon",
    "sku": "07",
    "skuDepPro": "1005"
  },
  "100508": {
    "name": "Punchao",
    "sku": "08",
    "skuDepPro": "1005"
  },
  "100509": {
    "name": "Puños",
    "sku": "09",
    "skuDepPro": "1005"
  },
  "100510": {
    "name": "Singa",
    "sku": "10",
    "skuDepPro": "1005"
  },
  "100511": {
    "name": "Tantamayo",
    "sku": "11",
    "skuDepPro": "1005"
  },
  "100601": {
    "name": "Rupa-Rupa",
    "sku": "01",
    "skuDepPro": "1006"
  },
  "100602": {
    "name": "Daniel Alomia Robles",
    "sku": "02",
    "skuDepPro": "1006"
  },
  "100603": {
    "name": "Hermilio Valdizan",
    "sku": "03",
    "skuDepPro": "1006"
  },
  "100604": {
    "name": "Jose Crespo y Castillo",
    "sku": "04",
    "skuDepPro": "1006"
  },
  "100605": {
    "name": "Luyando",
    "sku": "05",
    "skuDepPro": "1006"
  },
  "100606": {
    "name": "Mariano Damaso Beraun",
    "sku": "06",
    "skuDepPro": "1006"
  },
  "100607": {
    "name": "Pucayacu",
    "sku": "07",
    "skuDepPro": "1006"
  },
  "100608": {
    "name": "Castillo Grande",
    "sku": "08",
    "skuDepPro": "1006"
  },
  "100609": {
    "name": "Pueblo Nuevo",
    "sku": "09",
    "skuDepPro": "1006"
  },
  "100610": {
    "name": "Santo Domingo de Anda",
    "sku": "10",
    "skuDepPro": "1006"
  },
  "100701": {
    "name": "Huacrachuco",
    "sku": "01",
    "skuDepPro": "1007"
  },
  "100702": {
    "name": "Cholon",
    "sku": "02",
    "skuDepPro": "1007"
  },
  "100703": {
    "name": "San Buenaventura",
    "sku": "03",
    "skuDepPro": "1007"
  },
  "100704": {
    "name": "La Morada",
    "sku": "04",
    "skuDepPro": "1007"
  },
  "100705": {
    "name": "Santa Rosa de Alto Yanajanca",
    "sku": "05",
    "skuDepPro": "1007"
  },
  "100801": {
    "name": "Panao",
    "sku": "01",
    "skuDepPro": "1008"
  },
  "100802": {
    "name": "Chaglla",
    "sku": "02",
    "skuDepPro": "1008"
  },
  "100803": {
    "name": "Molino",
    "sku": "03",
    "skuDepPro": "1008"
  },
  "100804": {
    "name": "Umari",
    "sku": "04",
    "skuDepPro": "1008"
  },
  "100901": {
    "name": "Puerto Inca",
    "sku": "01",
    "skuDepPro": "1009"
  },
  "100902": {
    "name": "Codo del Pozuzo",
    "sku": "02",
    "skuDepPro": "1009"
  },
  "100903": {
    "name": "Honoria",
    "sku": "03",
    "skuDepPro": "1009"
  },
  "100904": {
    "name": "Tournavista",
    "sku": "04",
    "skuDepPro": "1009"
  },
  "100905": {
    "name": "Yuyapichis",
    "sku": "05",
    "skuDepPro": "1009"
  },
  "101001": {
    "name": "Jesus",
    "sku": "01",
    "skuDepPro": "1010"
  },
  "101002": {
    "name": "Baños",
    "sku": "02",
    "skuDepPro": "1010"
  },
  "101003": {
    "name": "Jivia",
    "sku": "03",
    "skuDepPro": "1010"
  },
  "101004": {
    "name": "Queropalca",
    "sku": "04",
    "skuDepPro": "1010"
  },
  "101005": {
    "name": "Rondos",
    "sku": "05",
    "skuDepPro": "1010"
  },
  "101006": {
    "name": "San Francisco de Asis",
    "sku": "06",
    "skuDepPro": "1010"
  },
  "101007": {
    "name": "San Miguel de Cauri",
    "sku": "07",
    "skuDepPro": "1010"
  },
  "101101": {
    "name": "Chavinillo",
    "sku": "01",
    "skuDepPro": "1011"
  },
  "101102": {
    "name": "Cahuac",
    "sku": "02",
    "skuDepPro": "1011"
  },
  "101103": {
    "name": "Chacabamba",
    "sku": "03",
    "skuDepPro": "1011"
  },
  "101104": {
    "name": "Aparicio Pomares",
    "sku": "04",
    "skuDepPro": "1011"
  },
  "101105": {
    "name": "Jacas Chico",
    "sku": "05",
    "skuDepPro": "1011"
  },
  "101106": {
    "name": "Obas",
    "sku": "06",
    "skuDepPro": "1011"
  },
  "101107": {
    "name": "Pampamarca",
    "sku": "07",
    "skuDepPro": "1011"
  },
  "101108": {
    "name": "Choras",
    "sku": "08",
    "skuDepPro": "1011"
  },
  "110101": {
    "name": "Ica",
    "sku": "01",
    "skuDepPro": "1101"
  },
  "110102": {
    "name": "La Tinguiña",
    "sku": "02",
    "skuDepPro": "1101"
  },
  "110103": {
    "name": "Los Aquijes",
    "sku": "03",
    "skuDepPro": "1101"
  },
  "110104": {
    "name": "Ocucaje",
    "sku": "04",
    "skuDepPro": "1101"
  },
  "110105": {
    "name": "Pachacutec",
    "sku": "05",
    "skuDepPro": "1101"
  },
  "110106": {
    "name": "Parcona",
    "sku": "06",
    "skuDepPro": "1101"
  },
  "110107": {
    "name": "Pueblo Nuevo",
    "sku": "07",
    "skuDepPro": "1101"
  },
  "110108": {
    "name": "Salas",
    "sku": "08",
    "skuDepPro": "1101"
  },
  "110109": {
    "name": "San Jose de Los Molinos",
    "sku": "09",
    "skuDepPro": "1101"
  },
  "110110": {
    "name": "San Juan Bautista",
    "sku": "10",
    "skuDepPro": "1101"
  },
  "110111": {
    "name": "Santiago",
    "sku": "11",
    "skuDepPro": "1101"
  },
  "110112": {
    "name": "Subtanjalla",
    "sku": "12",
    "skuDepPro": "1101"
  },
  "110113": {
    "name": "Tate",
    "sku": "13",
    "skuDepPro": "1101"
  },
  "110114": {
    "name": "Yauca del Rosario",
    "sku": "14",
    "skuDepPro": "1101"
  },
  "110201": {
    "name": "Chincha Alta",
    "sku": "01",
    "skuDepPro": "1102"
  },
  "110202": {
    "name": "Alto Laran",
    "sku": "02",
    "skuDepPro": "1102"
  },
  "110203": {
    "name": "Chavin",
    "sku": "03",
    "skuDepPro": "1102"
  },
  "110204": {
    "name": "Chincha Baja",
    "sku": "04",
    "skuDepPro": "1102"
  },
  "110205": {
    "name": "El Carmen",
    "sku": "05",
    "skuDepPro": "1102"
  },
  "110206": {
    "name": "Grocio Prado",
    "sku": "06",
    "skuDepPro": "1102"
  },
  "110207": {
    "name": "Pueblo Nuevo",
    "sku": "07",
    "skuDepPro": "1102"
  },
  "110208": {
    "name": "San Juan de Yanac",
    "sku": "08",
    "skuDepPro": "1102"
  },
  "110209": {
    "name": "San Pedro de Huacarpana",
    "sku": "09",
    "skuDepPro": "1102"
  },
  "110210": {
    "name": "Sunampe",
    "sku": "10",
    "skuDepPro": "1102"
  },
  "110211": {
    "name": "Tambo de Mora",
    "sku": "11",
    "skuDepPro": "1102"
  },
  "110301": {
    "name": "Nasca",
    "sku": "01",
    "skuDepPro": "1103"
  },
  "110302": {
    "name": "Changuillo",
    "sku": "02",
    "skuDepPro": "1103"
  },
  "110303": {
    "name": "El Ingenio",
    "sku": "03",
    "skuDepPro": "1103"
  },
  "110304": {
    "name": "Marcona",
    "sku": "04",
    "skuDepPro": "1103"
  },
  "110305": {
    "name": "Vista Alegre",
    "sku": "05",
    "skuDepPro": "1103"
  },
  "110401": {
    "name": "Palpa",
    "sku": "01",
    "skuDepPro": "1104"
  },
  "110402": {
    "name": "Llipata",
    "sku": "02",
    "skuDepPro": "1104"
  },
  "110403": {
    "name": "Rio Grande",
    "sku": "03",
    "skuDepPro": "1104"
  },
  "110404": {
    "name": "Santa Cruz",
    "sku": "04",
    "skuDepPro": "1104"
  },
  "110405": {
    "name": "Tibillo",
    "sku": "05",
    "skuDepPro": "1104"
  },
  "110501": {
    "name": "Pisco",
    "sku": "01",
    "skuDepPro": "1105"
  },
  "110502": {
    "name": "Huancano",
    "sku": "02",
    "skuDepPro": "1105"
  },
  "110503": {
    "name": "Humay",
    "sku": "03",
    "skuDepPro": "1105"
  },
  "110504": {
    "name": "Independencia",
    "sku": "04",
    "skuDepPro": "1105"
  },
  "110505": {
    "name": "Paracas",
    "sku": "05",
    "skuDepPro": "1105"
  },
  "110506": {
    "name": "San Andres",
    "sku": "06",
    "skuDepPro": "1105"
  },
  "110507": {
    "name": "San Clemente",
    "sku": "07",
    "skuDepPro": "1105"
  },
  "110508": {
    "name": "Tupac Amaru Inca",
    "sku": "08",
    "skuDepPro": "1105"
  },
  "120101": {
    "name": "Huancayo",
    "sku": "01",
    "skuDepPro": "1201"
  },
  "120104": {
    "name": "Carhuacallanga",
    "sku": "04",
    "skuDepPro": "1201"
  },
  "120105": {
    "name": "Chacapampa",
    "sku": "05",
    "skuDepPro": "1201"
  },
  "120106": {
    "name": "Chicche",
    "sku": "06",
    "skuDepPro": "1201"
  },
  "120107": {
    "name": "Chilca",
    "sku": "07",
    "skuDepPro": "1201"
  },
  "120108": {
    "name": "Chongos Alto",
    "sku": "08",
    "skuDepPro": "1201"
  },
  "120111": {
    "name": "Chupuro",
    "sku": "11",
    "skuDepPro": "1201"
  },
  "120112": {
    "name": "Colca",
    "sku": "12",
    "skuDepPro": "1201"
  },
  "120113": {
    "name": "Cullhuas",
    "sku": "13",
    "skuDepPro": "1201"
  },
  "120114": {
    "name": "El Tambo",
    "sku": "14",
    "skuDepPro": "1201"
  },
  "120116": {
    "name": "Huacrapuquio",
    "sku": "16",
    "skuDepPro": "1201"
  },
  "120117": {
    "name": "Hualhuas",
    "sku": "17",
    "skuDepPro": "1201"
  },
  "120119": {
    "name": "Huancan",
    "sku": "19",
    "skuDepPro": "1201"
  },
  "120120": {
    "name": "Huasicancha",
    "sku": "20",
    "skuDepPro": "1201"
  },
  "120121": {
    "name": "Huayucachi",
    "sku": "21",
    "skuDepPro": "1201"
  },
  "120122": {
    "name": "Ingenio",
    "sku": "22",
    "skuDepPro": "1201"
  },
  "120124": {
    "name": "Pariahuanca",
    "sku": "24",
    "skuDepPro": "1201"
  },
  "120125": {
    "name": "Pilcomayo",
    "sku": "25",
    "skuDepPro": "1201"
  },
  "120126": {
    "name": "Pucara",
    "sku": "26",
    "skuDepPro": "1201"
  },
  "120127": {
    "name": "Quichuay",
    "sku": "27",
    "skuDepPro": "1201"
  },
  "120128": {
    "name": "Quilcas",
    "sku": "28",
    "skuDepPro": "1201"
  },
  "120129": {
    "name": "San Agustin",
    "sku": "29",
    "skuDepPro": "1201"
  },
  "120130": {
    "name": "San Jeronimo de Tunan",
    "sku": "30",
    "skuDepPro": "1201"
  },
  "120132": {
    "name": "Saño",
    "sku": "32",
    "skuDepPro": "1201"
  },
  "120133": {
    "name": "Sapallanga",
    "sku": "33",
    "skuDepPro": "1201"
  },
  "120134": {
    "name": "Sicaya",
    "sku": "34",
    "skuDepPro": "1201"
  },
  "120135": {
    "name": "Santo Domingo de Acobamba",
    "sku": "35",
    "skuDepPro": "1201"
  },
  "120136": {
    "name": "Viques",
    "sku": "36",
    "skuDepPro": "1201"
  },
  "120201": {
    "name": "Concepción",
    "sku": "01",
    "skuDepPro": "1202"
  },
  "120202": {
    "name": "Aco",
    "sku": "02",
    "skuDepPro": "1202"
  },
  "120203": {
    "name": "Andamarca",
    "sku": "03",
    "skuDepPro": "1202"
  },
  "120204": {
    "name": "Chambara",
    "sku": "04",
    "skuDepPro": "1202"
  },
  "120205": {
    "name": "Cochas",
    "sku": "05",
    "skuDepPro": "1202"
  },
  "120206": {
    "name": "Comas",
    "sku": "06",
    "skuDepPro": "1202"
  },
  "120207": {
    "name": "Heroinas Toledo",
    "sku": "07",
    "skuDepPro": "1202"
  },
  "120208": {
    "name": "Manzanares",
    "sku": "08",
    "skuDepPro": "1202"
  },
  "120209": {
    "name": "Mariscal Castilla",
    "sku": "09",
    "skuDepPro": "1202"
  },
  "120210": {
    "name": "Matahuasi",
    "sku": "10",
    "skuDepPro": "1202"
  },
  "120211": {
    "name": "Mito",
    "sku": "11",
    "skuDepPro": "1202"
  },
  "120212": {
    "name": "Nueve de Julio",
    "sku": "12",
    "skuDepPro": "1202"
  },
  "120213": {
    "name": "Orcotuna",
    "sku": "13",
    "skuDepPro": "1202"
  },
  "120214": {
    "name": "San Jose de Quero",
    "sku": "14",
    "skuDepPro": "1202"
  },
  "120215": {
    "name": "Santa Rosa de Ocopa",
    "sku": "15",
    "skuDepPro": "1202"
  },
  "120301": {
    "name": "Chanchamayo",
    "sku": "01",
    "skuDepPro": "1203"
  },
  "120302": {
    "name": "Perene",
    "sku": "02",
    "skuDepPro": "1203"
  },
  "120303": {
    "name": "Pichanaqui",
    "sku": "03",
    "skuDepPro": "1203"
  },
  "120304": {
    "name": "San Luis de Shuaro",
    "sku": "04",
    "skuDepPro": "1203"
  },
  "120305": {
    "name": "San Ramon",
    "sku": "05",
    "skuDepPro": "1203"
  },
  "120306": {
    "name": "Vitoc",
    "sku": "06",
    "skuDepPro": "1203"
  },
  "120401": {
    "name": "Jauja",
    "sku": "01",
    "skuDepPro": "1204"
  },
  "120402": {
    "name": "Acolla",
    "sku": "02",
    "skuDepPro": "1204"
  },
  "120403": {
    "name": "Apata",
    "sku": "03",
    "skuDepPro": "1204"
  },
  "120404": {
    "name": "Ataura",
    "sku": "04",
    "skuDepPro": "1204"
  },
  "120405": {
    "name": "Canchayllo",
    "sku": "05",
    "skuDepPro": "1204"
  },
  "120406": {
    "name": "Curicaca",
    "sku": "06",
    "skuDepPro": "1204"
  },
  "120407": {
    "name": "El Mantaro",
    "sku": "07",
    "skuDepPro": "1204"
  },
  "120408": {
    "name": "Huamali",
    "sku": "08",
    "skuDepPro": "1204"
  },
  "120409": {
    "name": "Huaripampa",
    "sku": "09",
    "skuDepPro": "1204"
  },
  "120410": {
    "name": "Huertas",
    "sku": "10",
    "skuDepPro": "1204"
  },
  "120411": {
    "name": "Janjaillo",
    "sku": "11",
    "skuDepPro": "1204"
  },
  "120412": {
    "name": "Julcán",
    "sku": "12",
    "skuDepPro": "1204"
  },
  "120413": {
    "name": "Leonor Ordoñez",
    "sku": "13",
    "skuDepPro": "1204"
  },
  "120414": {
    "name": "Llocllapampa",
    "sku": "14",
    "skuDepPro": "1204"
  },
  "120415": {
    "name": "Marco",
    "sku": "15",
    "skuDepPro": "1204"
  },
  "120416": {
    "name": "Masma",
    "sku": "16",
    "skuDepPro": "1204"
  },
  "120417": {
    "name": "Masma Chicche",
    "sku": "17",
    "skuDepPro": "1204"
  },
  "120418": {
    "name": "Molinos",
    "sku": "18",
    "skuDepPro": "1204"
  },
  "120419": {
    "name": "Monobamba",
    "sku": "19",
    "skuDepPro": "1204"
  },
  "120420": {
    "name": "Muqui",
    "sku": "20",
    "skuDepPro": "1204"
  },
  "120421": {
    "name": "Muquiyauyo",
    "sku": "21",
    "skuDepPro": "1204"
  },
  "120422": {
    "name": "Paca",
    "sku": "22",
    "skuDepPro": "1204"
  },
  "120423": {
    "name": "Paccha",
    "sku": "23",
    "skuDepPro": "1204"
  },
  "120424": {
    "name": "Pancan",
    "sku": "24",
    "skuDepPro": "1204"
  },
  "120425": {
    "name": "Parco",
    "sku": "25",
    "skuDepPro": "1204"
  },
  "120426": {
    "name": "Pomacancha",
    "sku": "26",
    "skuDepPro": "1204"
  },
  "120427": {
    "name": "Ricran",
    "sku": "27",
    "skuDepPro": "1204"
  },
  "120428": {
    "name": "San Lorenzo",
    "sku": "28",
    "skuDepPro": "1204"
  },
  "120429": {
    "name": "San Pedro de Chunan",
    "sku": "29",
    "skuDepPro": "1204"
  },
  "120430": {
    "name": "Sausa",
    "sku": "30",
    "skuDepPro": "1204"
  },
  "120431": {
    "name": "Sincos",
    "sku": "31",
    "skuDepPro": "1204"
  },
  "120432": {
    "name": "Tunan Marca",
    "sku": "32",
    "skuDepPro": "1204"
  },
  "120433": {
    "name": "Yauli",
    "sku": "33",
    "skuDepPro": "1204"
  },
  "120434": {
    "name": "Yauyos",
    "sku": "34",
    "skuDepPro": "1204"
  },
  "120501": {
    "name": "Junín",
    "sku": "01",
    "skuDepPro": "1205"
  },
  "120502": {
    "name": "Carhuamayo",
    "sku": "02",
    "skuDepPro": "1205"
  },
  "120503": {
    "name": "Ondores",
    "sku": "03",
    "skuDepPro": "1205"
  },
  "120504": {
    "name": "Ulcumayo",
    "sku": "04",
    "skuDepPro": "1205"
  },
  "120601": {
    "name": "Satipo",
    "sku": "01",
    "skuDepPro": "1206"
  },
  "120602": {
    "name": "Coviriali",
    "sku": "02",
    "skuDepPro": "1206"
  },
  "120603": {
    "name": "Llaylla",
    "sku": "03",
    "skuDepPro": "1206"
  },
  "120604": {
    "name": "Mazamari",
    "sku": "04",
    "skuDepPro": "1206"
  },
  "120605": {
    "name": "Pampa Hermosa",
    "sku": "05",
    "skuDepPro": "1206"
  },
  "120606": {
    "name": "Pangoa",
    "sku": "06",
    "skuDepPro": "1206"
  },
  "120607": {
    "name": "Rio Negro",
    "sku": "07",
    "skuDepPro": "1206"
  },
  "120608": {
    "name": "Rio Tambo",
    "sku": "08",
    "skuDepPro": "1206"
  },
  "120609": {
    "name": "Vizcatan del Ene",
    "sku": "09",
    "skuDepPro": "1206"
  },
  "120701": {
    "name": "Tarma",
    "sku": "01",
    "skuDepPro": "1207"
  },
  "120702": {
    "name": "Acobamba",
    "sku": "02",
    "skuDepPro": "1207"
  },
  "120703": {
    "name": "Huaricolca",
    "sku": "03",
    "skuDepPro": "1207"
  },
  "120704": {
    "name": "Huasahuasi",
    "sku": "04",
    "skuDepPro": "1207"
  },
  "120705": {
    "name": "La Union",
    "sku": "05",
    "skuDepPro": "1207"
  },
  "120706": {
    "name": "Palca",
    "sku": "06",
    "skuDepPro": "1207"
  },
  "120707": {
    "name": "Palcamayo",
    "sku": "07",
    "skuDepPro": "1207"
  },
  "120708": {
    "name": "San Pedro de Cajas",
    "sku": "08",
    "skuDepPro": "1207"
  },
  "120709": {
    "name": "Tapo",
    "sku": "09",
    "skuDepPro": "1207"
  },
  "120801": {
    "name": "La Oroya",
    "sku": "01",
    "skuDepPro": "1208"
  },
  "120802": {
    "name": "Chacapalpa",
    "sku": "02",
    "skuDepPro": "1208"
  },
  "120803": {
    "name": "Huay-Huay",
    "sku": "03",
    "skuDepPro": "1208"
  },
  "120804": {
    "name": "Marcapomacocha",
    "sku": "04",
    "skuDepPro": "1208"
  },
  "120805": {
    "name": "Morococha",
    "sku": "05",
    "skuDepPro": "1208"
  },
  "120806": {
    "name": "Paccha",
    "sku": "06",
    "skuDepPro": "1208"
  },
  "120807": {
    "name": "Santa Barbara de Carhuacayan",
    "sku": "07",
    "skuDepPro": "1208"
  },
  "120808": {
    "name": "Santa Rosa de Sacco",
    "sku": "08",
    "skuDepPro": "1208"
  },
  "120809": {
    "name": "Suitucancha",
    "sku": "09",
    "skuDepPro": "1208"
  },
  "120810": {
    "name": "Yauli",
    "sku": "10",
    "skuDepPro": "1208"
  },
  "120901": {
    "name": "Chupaca",
    "sku": "01",
    "skuDepPro": "1209"
  },
  "120902": {
    "name": "Ahuac",
    "sku": "02",
    "skuDepPro": "1209"
  },
  "120903": {
    "name": "Chongos Bajo",
    "sku": "03",
    "skuDepPro": "1209"
  },
  "120904": {
    "name": "Huachac",
    "sku": "04",
    "skuDepPro": "1209"
  },
  "120905": {
    "name": "Huamancaca Chico",
    "sku": "05",
    "skuDepPro": "1209"
  },
  "120906": {
    "name": "San Juan de Iscos",
    "sku": "06",
    "skuDepPro": "1209"
  },
  "120907": {
    "name": "San Juan de Jarpa",
    "sku": "07",
    "skuDepPro": "1209"
  },
  "120908": {
    "name": "Tres de Diciembre",
    "sku": "08",
    "skuDepPro": "1209"
  },
  "120909": {
    "name": "Yanacancha",
    "sku": "09",
    "skuDepPro": "1209"
  },
  "130101": {
    "name": "Trujillo",
    "sku": "01",
    "skuDepPro": "1301"
  },
  "130102": {
    "name": "El Porvenir",
    "sku": "02",
    "skuDepPro": "1301"
  },
  "130103": {
    "name": "Florencia de Mora",
    "sku": "03",
    "skuDepPro": "1301"
  },
  "130104": {
    "name": "Huanchaco",
    "sku": "04",
    "skuDepPro": "1301"
  },
  "130105": {
    "name": "La Esperanza",
    "sku": "05",
    "skuDepPro": "1301"
  },
  "130106": {
    "name": "Laredo",
    "sku": "06",
    "skuDepPro": "1301"
  },
  "130107": {
    "name": "Moche",
    "sku": "07",
    "skuDepPro": "1301"
  },
  "130108": {
    "name": "Poroto",
    "sku": "08",
    "skuDepPro": "1301"
  },
  "130109": {
    "name": "Salaverry",
    "sku": "09",
    "skuDepPro": "1301"
  },
  "130110": {
    "name": "Simbal",
    "sku": "10",
    "skuDepPro": "1301"
  },
  "130111": {
    "name": "Victor Larco Herrera",
    "sku": "11",
    "skuDepPro": "1301"
  },
  "130201": {
    "name": "Ascope",
    "sku": "01",
    "skuDepPro": "1302"
  },
  "130202": {
    "name": "Chicama",
    "sku": "02",
    "skuDepPro": "1302"
  },
  "130203": {
    "name": "Chocope",
    "sku": "03",
    "skuDepPro": "1302"
  },
  "130204": {
    "name": "Magdalena de Cao",
    "sku": "04",
    "skuDepPro": "1302"
  },
  "130205": {
    "name": "Paijan",
    "sku": "05",
    "skuDepPro": "1302"
  },
  "130206": {
    "name": "Razuri",
    "sku": "06",
    "skuDepPro": "1302"
  },
  "130207": {
    "name": "Santiago de Cao",
    "sku": "07",
    "skuDepPro": "1302"
  },
  "130208": {
    "name": "Casa Grande",
    "sku": "08",
    "skuDepPro": "1302"
  },
  "130301": {
    "name": "Bolívar",
    "sku": "01",
    "skuDepPro": "1303"
  },
  "130302": {
    "name": "Bambamarca",
    "sku": "02",
    "skuDepPro": "1303"
  },
  "130303": {
    "name": "Condormarca",
    "sku": "03",
    "skuDepPro": "1303"
  },
  "130304": {
    "name": "Longotea",
    "sku": "04",
    "skuDepPro": "1303"
  },
  "130305": {
    "name": "Uchumarca",
    "sku": "05",
    "skuDepPro": "1303"
  },
  "130306": {
    "name": "Ucuncha",
    "sku": "06",
    "skuDepPro": "1303"
  },
  "130401": {
    "name": "Chepén",
    "sku": "01",
    "skuDepPro": "1304"
  },
  "130402": {
    "name": "Pacanga",
    "sku": "02",
    "skuDepPro": "1304"
  },
  "130403": {
    "name": "Pueblo Nuevo",
    "sku": "03",
    "skuDepPro": "1304"
  },
  "130501": {
    "name": "Julcán",
    "sku": "01",
    "skuDepPro": "1305"
  },
  "130502": {
    "name": "Calamarca",
    "sku": "02",
    "skuDepPro": "1305"
  },
  "130503": {
    "name": "Carabamba",
    "sku": "03",
    "skuDepPro": "1305"
  },
  "130504": {
    "name": "Huaso",
    "sku": "04",
    "skuDepPro": "1305"
  },
  "130601": {
    "name": "Otuzco",
    "sku": "01",
    "skuDepPro": "1306"
  },
  "130602": {
    "name": "Agallpampa",
    "sku": "02",
    "skuDepPro": "1306"
  },
  "130604": {
    "name": "Charat",
    "sku": "04",
    "skuDepPro": "1306"
  },
  "130605": {
    "name": "Huaranchal",
    "sku": "05",
    "skuDepPro": "1306"
  },
  "130606": {
    "name": "La Cuesta",
    "sku": "06",
    "skuDepPro": "1306"
  },
  "130608": {
    "name": "Mache",
    "sku": "08",
    "skuDepPro": "1306"
  },
  "130610": {
    "name": "Paranday",
    "sku": "10",
    "skuDepPro": "1306"
  },
  "130611": {
    "name": "Salpo",
    "sku": "11",
    "skuDepPro": "1306"
  },
  "130613": {
    "name": "Sinsicap",
    "sku": "13",
    "skuDepPro": "1306"
  },
  "130614": {
    "name": "Usquil",
    "sku": "14",
    "skuDepPro": "1306"
  },
  "130701": {
    "name": "San Pedro de Lloc",
    "sku": "01",
    "skuDepPro": "1307"
  },
  "130702": {
    "name": "Guadalupe",
    "sku": "02",
    "skuDepPro": "1307"
  },
  "130703": {
    "name": "Jequetepeque",
    "sku": "03",
    "skuDepPro": "1307"
  },
  "130704": {
    "name": "Pacasmayo",
    "sku": "04",
    "skuDepPro": "1307"
  },
  "130705": {
    "name": "San Jose",
    "sku": "05",
    "skuDepPro": "1307"
  },
  "130801": {
    "name": "Tayabamba",
    "sku": "01",
    "skuDepPro": "1308"
  },
  "130802": {
    "name": "Buldibuyo",
    "sku": "02",
    "skuDepPro": "1308"
  },
  "130803": {
    "name": "Chillia",
    "sku": "03",
    "skuDepPro": "1308"
  },
  "130804": {
    "name": "Huancaspata",
    "sku": "04",
    "skuDepPro": "1308"
  },
  "130805": {
    "name": "Huaylillas",
    "sku": "05",
    "skuDepPro": "1308"
  },
  "130806": {
    "name": "Huayo",
    "sku": "06",
    "skuDepPro": "1308"
  },
  "130807": {
    "name": "Ongon",
    "sku": "07",
    "skuDepPro": "1308"
  },
  "130808": {
    "name": "Parcoy",
    "sku": "08",
    "skuDepPro": "1308"
  },
  "130809": {
    "name": "Pataz",
    "sku": "09",
    "skuDepPro": "1308"
  },
  "130810": {
    "name": "Pias",
    "sku": "10",
    "skuDepPro": "1308"
  },
  "130811": {
    "name": "Santiago de Challas",
    "sku": "11",
    "skuDepPro": "1308"
  },
  "130812": {
    "name": "Taurija",
    "sku": "12",
    "skuDepPro": "1308"
  },
  "130813": {
    "name": "Urpay",
    "sku": "13",
    "skuDepPro": "1308"
  },
  "130901": {
    "name": "Huamachuco",
    "sku": "01",
    "skuDepPro": "1309"
  },
  "130902": {
    "name": "Chugay",
    "sku": "02",
    "skuDepPro": "1309"
  },
  "130903": {
    "name": "Cochorco",
    "sku": "03",
    "skuDepPro": "1309"
  },
  "130904": {
    "name": "Curgos",
    "sku": "04",
    "skuDepPro": "1309"
  },
  "130905": {
    "name": "Marcabal",
    "sku": "05",
    "skuDepPro": "1309"
  },
  "130906": {
    "name": "Sanagoran",
    "sku": "06",
    "skuDepPro": "1309"
  },
  "130907": {
    "name": "Sarin",
    "sku": "07",
    "skuDepPro": "1309"
  },
  "130908": {
    "name": "Sartimbamba",
    "sku": "08",
    "skuDepPro": "1309"
  },
  "131001": {
    "name": "Santiago de Chuco",
    "sku": "01",
    "skuDepPro": "1310"
  },
  "131002": {
    "name": "Angasmarca",
    "sku": "02",
    "skuDepPro": "1310"
  },
  "131003": {
    "name": "Cachicadan",
    "sku": "03",
    "skuDepPro": "1310"
  },
  "131004": {
    "name": "Mollebamba",
    "sku": "04",
    "skuDepPro": "1310"
  },
  "131005": {
    "name": "Mollepata",
    "sku": "05",
    "skuDepPro": "1310"
  },
  "131006": {
    "name": "Quiruvilca",
    "sku": "06",
    "skuDepPro": "1310"
  },
  "131007": {
    "name": "Santa Cruz de Chuca",
    "sku": "07",
    "skuDepPro": "1310"
  },
  "131008": {
    "name": "Sitabamba",
    "sku": "08",
    "skuDepPro": "1310"
  },
  "131101": {
    "name": "Cascas",
    "sku": "01",
    "skuDepPro": "1311"
  },
  "131102": {
    "name": "Lucma",
    "sku": "02",
    "skuDepPro": "1311"
  },
  "131103": {
    "name": "Marmot",
    "sku": "03",
    "skuDepPro": "1311"
  },
  "131104": {
    "name": "Sayapullo",
    "sku": "04",
    "skuDepPro": "1311"
  },
  "131201": {
    "name": "Virú",
    "sku": "01",
    "skuDepPro": "1312"
  },
  "131202": {
    "name": "Chao",
    "sku": "02",
    "skuDepPro": "1312"
  },
  "131203": {
    "name": "Guadalupito",
    "sku": "03",
    "skuDepPro": "1312"
  },
  "140101": {
    "name": "Chiclayo",
    "sku": "01",
    "skuDepPro": "1401"
  },
  "140102": {
    "name": "Chongoyape",
    "sku": "02",
    "skuDepPro": "1401"
  },
  "140103": {
    "name": "Eten",
    "sku": "03",
    "skuDepPro": "1401"
  },
  "140104": {
    "name": "Eten Puerto",
    "sku": "04",
    "skuDepPro": "1401"
  },
  "140105": {
    "name": "Jose Leonardo Ortiz",
    "sku": "05",
    "skuDepPro": "1401"
  },
  "140106": {
    "name": "La Victoria",
    "sku": "06",
    "skuDepPro": "1401"
  },
  "140107": {
    "name": "Lagunas",
    "sku": "07",
    "skuDepPro": "1401"
  },
  "140108": {
    "name": "Monsefu",
    "sku": "08",
    "skuDepPro": "1401"
  },
  "140109": {
    "name": "Nueva Arica",
    "sku": "09",
    "skuDepPro": "1401"
  },
  "140110": {
    "name": "Oyotun",
    "sku": "10",
    "skuDepPro": "1401"
  },
  "140111": {
    "name": "Picsi",
    "sku": "11",
    "skuDepPro": "1401"
  },
  "140112": {
    "name": "Pimentel",
    "sku": "12",
    "skuDepPro": "1401"
  },
  "140113": {
    "name": "Reque",
    "sku": "13",
    "skuDepPro": "1401"
  },
  "140114": {
    "name": "Santa Rosa",
    "sku": "14",
    "skuDepPro": "1401"
  },
  "140115": {
    "name": "Saña",
    "sku": "15",
    "skuDepPro": "1401"
  },
  "140116": {
    "name": "Cayalti",
    "sku": "16",
    "skuDepPro": "1401"
  },
  "140117": {
    "name": "Patapo",
    "sku": "17",
    "skuDepPro": "1401"
  },
  "140118": {
    "name": "Pomalca",
    "sku": "18",
    "skuDepPro": "1401"
  },
  "140119": {
    "name": "Pucala",
    "sku": "19",
    "skuDepPro": "1401"
  },
  "140120": {
    "name": "Tuman",
    "sku": "20",
    "skuDepPro": "1401"
  },
  "140201": {
    "name": "Ferreñafe",
    "sku": "01",
    "skuDepPro": "1402"
  },
  "140202": {
    "name": "Cañaris",
    "sku": "02",
    "skuDepPro": "1402"
  },
  "140203": {
    "name": "Incahuasi",
    "sku": "03",
    "skuDepPro": "1402"
  },
  "140204": {
    "name": "Manuel Antonio Mesones Muro",
    "sku": "04",
    "skuDepPro": "1402"
  },
  "140205": {
    "name": "Pitipo",
    "sku": "05",
    "skuDepPro": "1402"
  },
  "140206": {
    "name": "Pueblo Nuevo",
    "sku": "06",
    "skuDepPro": "1402"
  },
  "140301": {
    "name": "Lambayeque",
    "sku": "01",
    "skuDepPro": "1403"
  },
  "140302": {
    "name": "Chochope",
    "sku": "02",
    "skuDepPro": "1403"
  },
  "140303": {
    "name": "Illimo",
    "sku": "03",
    "skuDepPro": "1403"
  },
  "140304": {
    "name": "Jayanca",
    "sku": "04",
    "skuDepPro": "1403"
  },
  "140305": {
    "name": "Mochumi",
    "sku": "05",
    "skuDepPro": "1403"
  },
  "140306": {
    "name": "Morrope",
    "sku": "06",
    "skuDepPro": "1403"
  },
  "140307": {
    "name": "Motupe",
    "sku": "07",
    "skuDepPro": "1403"
  },
  "140308": {
    "name": "Olmos",
    "sku": "08",
    "skuDepPro": "1403"
  },
  "140309": {
    "name": "Pacora",
    "sku": "09",
    "skuDepPro": "1403"
  },
  "140310": {
    "name": "Salas",
    "sku": "10",
    "skuDepPro": "1403"
  },
  "140311": {
    "name": "San Jose",
    "sku": "11",
    "skuDepPro": "1403"
  },
  "140312": {
    "name": "Tucume",
    "sku": "12",
    "skuDepPro": "1403"
  },
  "150101": {
    "name": "Lima",
    "sku": "01",
    "skuDepPro": "1501"
  },
  "150102": {
    "name": "Ancon",
    "sku": "02",
    "skuDepPro": "1501"
  },
  "150103": {
    "name": "Ate",
    "sku": "03",
    "skuDepPro": "1501"
  },
  "150104": {
    "name": "Barranco",
    "sku": "04",
    "skuDepPro": "1501"
  },
  "150105": {
    "name": "Breña",
    "sku": "05",
    "skuDepPro": "1501"
  },
  "150106": {
    "name": "Carabayllo",
    "sku": "06",
    "skuDepPro": "1501"
  },
  "150107": {
    "name": "Chaclacayo",
    "sku": "07",
    "skuDepPro": "1501"
  },
  "150108": {
    "name": "Chorrillos",
    "sku": "08",
    "skuDepPro": "1501"
  },
  "150109": {
    "name": "Cieneguilla",
    "sku": "09",
    "skuDepPro": "1501"
  },
  "150110": {
    "name": "Comas",
    "sku": "10",
    "skuDepPro": "1501"
  },
  "150111": {
    "name": "El Agustino",
    "sku": "11",
    "skuDepPro": "1501"
  },
  "150112": {
    "name": "Independencia",
    "sku": "12",
    "skuDepPro": "1501"
  },
  "150113": {
    "name": "Jesus Maria",
    "sku": "13",
    "skuDepPro": "1501"
  },
  "150114": {
    "name": "La Molina",
    "sku": "14",
    "skuDepPro": "1501"
  },
  "150115": {
    "name": "La Victoria",
    "sku": "15",
    "skuDepPro": "1501"
  },
  "150116": {
    "name": "Lince",
    "sku": "16",
    "skuDepPro": "1501"
  },
  "150117": {
    "name": "Los Olivos",
    "sku": "17",
    "skuDepPro": "1501"
  },
  "150118": {
    "name": "Lurigancho",
    "sku": "18",
    "skuDepPro": "1501"
  },
  "150119": {
    "name": "Lurin",
    "sku": "19",
    "skuDepPro": "1501"
  },
  "150120": {
    "name": "Magdalena del Mar",
    "sku": "20",
    "skuDepPro": "1501"
  },
  "150121": {
    "name": "Pueblo Libre",
    "sku": "21",
    "skuDepPro": "1501"
  },
  "150122": {
    "name": "Miraflores",
    "sku": "22",
    "skuDepPro": "1501"
  },
  "150123": {
    "name": "Pachacamac",
    "sku": "23",
    "skuDepPro": "1501"
  },
  "150124": {
    "name": "Pucusana",
    "sku": "24",
    "skuDepPro": "1501"
  },
  "150125": {
    "name": "Puente Piedra",
    "sku": "25",
    "skuDepPro": "1501"
  },
  "150126": {
    "name": "Punta Hermosa",
    "sku": "26",
    "skuDepPro": "1501"
  },
  "150127": {
    "name": "Punta Negra",
    "sku": "27",
    "skuDepPro": "1501"
  },
  "150128": {
    "name": "Rimac",
    "sku": "28",
    "skuDepPro": "1501"
  },
  "150129": {
    "name": "San Bartolo",
    "sku": "29",
    "skuDepPro": "1501"
  },
  "150130": {
    "name": "San Borja",
    "sku": "30",
    "skuDepPro": "1501"
  },
  "150131": {
    "name": "San Isidro",
    "sku": "31",
    "skuDepPro": "1501"
  },
  "150132": {
    "name": "San Juan de Lurigancho",
    "sku": "32",
    "skuDepPro": "1501"
  },
  "150133": {
    "name": "San Juan de Miraflores",
    "sku": "33",
    "skuDepPro": "1501"
  },
  "150134": {
    "name": "San Luis",
    "sku": "34",
    "skuDepPro": "1501"
  },
  "150135": {
    "name": "San Martín de Porres",
    "sku": "35",
    "skuDepPro": "1501"
  },
  "150136": {
    "name": "San Miguel",
    "sku": "36",
    "skuDepPro": "1501"
  },
  "150137": {
    "name": "Santa Anita",
    "sku": "37",
    "skuDepPro": "1501"
  },
  "150138": {
    "name": "Santa Maria del Mar",
    "sku": "38",
    "skuDepPro": "1501"
  },
  "150139": {
    "name": "Santa Rosa",
    "sku": "39",
    "skuDepPro": "1501"
  },
  "150140": {
    "name": "Santiago de Surco",
    "sku": "40",
    "skuDepPro": "1501"
  },
  "150141": {
    "name": "Surquillo",
    "sku": "41",
    "skuDepPro": "1501"
  },
  "150142": {
    "name": "Villa El Salvador",
    "sku": "42",
    "skuDepPro": "1501"
  },
  "150143": {
    "name": "Villa Maria del Triunfo",
    "sku": "43",
    "skuDepPro": "1501"
  },
  "150201": {
    "name": "Barranca",
    "sku": "01",
    "skuDepPro": "1502"
  },
  "150202": {
    "name": "Paramonga",
    "sku": "02",
    "skuDepPro": "1502"
  },
  "150203": {
    "name": "Pativilca",
    "sku": "03",
    "skuDepPro": "1502"
  },
  "150204": {
    "name": "Supe",
    "sku": "04",
    "skuDepPro": "1502"
  },
  "150205": {
    "name": "Supe Puerto",
    "sku": "05",
    "skuDepPro": "1502"
  },
  "150301": {
    "name": "Cajatambo",
    "sku": "01",
    "skuDepPro": "1503"
  },
  "150302": {
    "name": "Copa",
    "sku": "02",
    "skuDepPro": "1503"
  },
  "150303": {
    "name": "Gorgor",
    "sku": "03",
    "skuDepPro": "1503"
  },
  "150304": {
    "name": "Huancapon",
    "sku": "04",
    "skuDepPro": "1503"
  },
  "150305": {
    "name": "Manas",
    "sku": "05",
    "skuDepPro": "1503"
  },
  "150401": {
    "name": "Canta",
    "sku": "01",
    "skuDepPro": "1504"
  },
  "150402": {
    "name": "Arahuay",
    "sku": "02",
    "skuDepPro": "1504"
  },
  "150403": {
    "name": "Huamantanga",
    "sku": "03",
    "skuDepPro": "1504"
  },
  "150404": {
    "name": "Huaros",
    "sku": "04",
    "skuDepPro": "1504"
  },
  "150405": {
    "name": "Lachaqui",
    "sku": "05",
    "skuDepPro": "1504"
  },
  "150406": {
    "name": "San Buenaventura",
    "sku": "06",
    "skuDepPro": "1504"
  },
  "150407": {
    "name": "Santa Rosa de Quives",
    "sku": "07",
    "skuDepPro": "1504"
  },
  "150501": {
    "name": "San Vicente de Cañete",
    "sku": "01",
    "skuDepPro": "1505"
  },
  "150502": {
    "name": "Asia",
    "sku": "02",
    "skuDepPro": "1505"
  },
  "150503": {
    "name": "Calango",
    "sku": "03",
    "skuDepPro": "1505"
  },
  "150504": {
    "name": "Cerro Azul",
    "sku": "04",
    "skuDepPro": "1505"
  },
  "150505": {
    "name": "Chilca",
    "sku": "05",
    "skuDepPro": "1505"
  },
  "150506": {
    "name": "Coayllo",
    "sku": "06",
    "skuDepPro": "1505"
  },
  "150507": {
    "name": "Imperial",
    "sku": "07",
    "skuDepPro": "1505"
  },
  "150508": {
    "name": "Lunahuana",
    "sku": "08",
    "skuDepPro": "1505"
  },
  "150509": {
    "name": "Mala",
    "sku": "09",
    "skuDepPro": "1505"
  },
  "150510": {
    "name": "Nuevo Imperial",
    "sku": "10",
    "skuDepPro": "1505"
  },
  "150511": {
    "name": "Pacaran",
    "sku": "11",
    "skuDepPro": "1505"
  },
  "150512": {
    "name": "Quilmana",
    "sku": "12",
    "skuDepPro": "1505"
  },
  "150513": {
    "name": "San Antonio",
    "sku": "13",
    "skuDepPro": "1505"
  },
  "150514": {
    "name": "San Luis",
    "sku": "14",
    "skuDepPro": "1505"
  },
  "150515": {
    "name": "Santa Cruz de Flores",
    "sku": "15",
    "skuDepPro": "1505"
  },
  "150516": {
    "name": "Zuñiga",
    "sku": "16",
    "skuDepPro": "1505"
  },
  "150601": {
    "name": "Huaral",
    "sku": "01",
    "skuDepPro": "1506"
  },
  "150602": {
    "name": "Atavillos Alto",
    "sku": "02",
    "skuDepPro": "1506"
  },
  "150603": {
    "name": "Atavillos Bajo",
    "sku": "03",
    "skuDepPro": "1506"
  },
  "150604": {
    "name": "Aucallama",
    "sku": "04",
    "skuDepPro": "1506"
  },
  "150605": {
    "name": "Chancay",
    "sku": "05",
    "skuDepPro": "1506"
  },
  "150606": {
    "name": "Ihuari",
    "sku": "06",
    "skuDepPro": "1506"
  },
  "150607": {
    "name": "Lampian",
    "sku": "07",
    "skuDepPro": "1506"
  },
  "150608": {
    "name": "Pacaraos",
    "sku": "08",
    "skuDepPro": "1506"
  },
  "150609": {
    "name": "San Miguel de Acos",
    "sku": "09",
    "skuDepPro": "1506"
  },
  "150610": {
    "name": "Santa Cruz de Andamarca",
    "sku": "10",
    "skuDepPro": "1506"
  },
  "150611": {
    "name": "Sumbilca",
    "sku": "11",
    "skuDepPro": "1506"
  },
  "150612": {
    "name": "Veintisiete de Noviembre",
    "sku": "12",
    "skuDepPro": "1506"
  },
  "150701": {
    "name": "Matucana",
    "sku": "01",
    "skuDepPro": "1507"
  },
  "150702": {
    "name": "Antioquia",
    "sku": "02",
    "skuDepPro": "1507"
  },
  "150703": {
    "name": "Callahuanca",
    "sku": "03",
    "skuDepPro": "1507"
  },
  "150704": {
    "name": "Carampoma",
    "sku": "04",
    "skuDepPro": "1507"
  },
  "150705": {
    "name": "Chicla",
    "sku": "05",
    "skuDepPro": "1507"
  },
  "150706": {
    "name": "Cuenca",
    "sku": "06",
    "skuDepPro": "1507"
  },
  "150707": {
    "name": "Huachupampa",
    "sku": "07",
    "skuDepPro": "1507"
  },
  "150708": {
    "name": "Huanza",
    "sku": "08",
    "skuDepPro": "1507"
  },
  "150709": {
    "name": "Huarochirí",
    "sku": "09",
    "skuDepPro": "1507"
  },
  "150710": {
    "name": "Lahuaytambo",
    "sku": "10",
    "skuDepPro": "1507"
  },
  "150711": {
    "name": "Langa",
    "sku": "11",
    "skuDepPro": "1507"
  },
  "150712": {
    "name": "Laraos",
    "sku": "12",
    "skuDepPro": "1507"
  },
  "150713": {
    "name": "Mariatana",
    "sku": "13",
    "skuDepPro": "1507"
  },
  "150714": {
    "name": "Ricardo Palma",
    "sku": "14",
    "skuDepPro": "1507"
  },
  "150715": {
    "name": "San Andres de Tupicocha",
    "sku": "15",
    "skuDepPro": "1507"
  },
  "150716": {
    "name": "San Antonio",
    "sku": "16",
    "skuDepPro": "1507"
  },
  "150717": {
    "name": "San Bartolome",
    "sku": "17",
    "skuDepPro": "1507"
  },
  "150718": {
    "name": "San Damian",
    "sku": "18",
    "skuDepPro": "1507"
  },
  "150719": {
    "name": "San Juan de Iris",
    "sku": "19",
    "skuDepPro": "1507"
  },
  "150720": {
    "name": "San Juan de Tantaranche",
    "sku": "20",
    "skuDepPro": "1507"
  },
  "150721": {
    "name": "San Lorenzo de Quinti",
    "sku": "21",
    "skuDepPro": "1507"
  },
  "150722": {
    "name": "San Mateo",
    "sku": "22",
    "skuDepPro": "1507"
  },
  "150723": {
    "name": "San Mateo de Otao",
    "sku": "23",
    "skuDepPro": "1507"
  },
  "150724": {
    "name": "San Pedro de Casta",
    "sku": "24",
    "skuDepPro": "1507"
  },
  "150725": {
    "name": "San Pedro de Huancayre",
    "sku": "25",
    "skuDepPro": "1507"
  },
  "150726": {
    "name": "Sangallaya",
    "sku": "26",
    "skuDepPro": "1507"
  },
  "150727": {
    "name": "Santa Cruz de Cocachacra",
    "sku": "27",
    "skuDepPro": "1507"
  },
  "150728": {
    "name": "Santa Eulalia",
    "sku": "28",
    "skuDepPro": "1507"
  },
  "150729": {
    "name": "Santiago de Anchucaya",
    "sku": "29",
    "skuDepPro": "1507"
  },
  "150730": {
    "name": "Santiago de Tuna",
    "sku": "30",
    "skuDepPro": "1507"
  },
  "150731": {
    "name": "Santo Domingo de Los Olleros",
    "sku": "31",
    "skuDepPro": "1507"
  },
  "150732": {
    "name": "Surco",
    "sku": "32",
    "skuDepPro": "1507"
  },
  "150801": {
    "name": "Huacho",
    "sku": "01",
    "skuDepPro": "1508"
  },
  "150802": {
    "name": "Ambar",
    "sku": "02",
    "skuDepPro": "1508"
  },
  "150803": {
    "name": "Caleta de Carquin",
    "sku": "03",
    "skuDepPro": "1508"
  },
  "150804": {
    "name": "Checras",
    "sku": "04",
    "skuDepPro": "1508"
  },
  "150805": {
    "name": "Hualmay",
    "sku": "05",
    "skuDepPro": "1508"
  },
  "150806": {
    "name": "Huaura",
    "sku": "06",
    "skuDepPro": "1508"
  },
  "150807": {
    "name": "Leoncio Prado",
    "sku": "07",
    "skuDepPro": "1508"
  },
  "150808": {
    "name": "Paccho",
    "sku": "08",
    "skuDepPro": "1508"
  },
  "150809": {
    "name": "Santa Leonor",
    "sku": "09",
    "skuDepPro": "1508"
  },
  "150810": {
    "name": "Santa Maria",
    "sku": "10",
    "skuDepPro": "1508"
  },
  "150811": {
    "name": "Sayan",
    "sku": "11",
    "skuDepPro": "1508"
  },
  "150812": {
    "name": "Vegueta",
    "sku": "12",
    "skuDepPro": "1508"
  },
  "150901": {
    "name": "Oyón",
    "sku": "01",
    "skuDepPro": "1509"
  },
  "150902": {
    "name": "Andajes",
    "sku": "02",
    "skuDepPro": "1509"
  },
  "150903": {
    "name": "Caujul",
    "sku": "03",
    "skuDepPro": "1509"
  },
  "150904": {
    "name": "Cochamarca",
    "sku": "04",
    "skuDepPro": "1509"
  },
  "150905": {
    "name": "Navan",
    "sku": "05",
    "skuDepPro": "1509"
  },
  "150906": {
    "name": "Pachangara",
    "sku": "06",
    "skuDepPro": "1509"
  },
  "151001": {
    "name": "Yauyos",
    "sku": "01",
    "skuDepPro": "1510"
  },
  "151002": {
    "name": "Alis",
    "sku": "02",
    "skuDepPro": "1510"
  },
  "151003": {
    "name": "Allauca",
    "sku": "03",
    "skuDepPro": "1510"
  },
  "151004": {
    "name": "Ayaviri",
    "sku": "04",
    "skuDepPro": "1510"
  },
  "151005": {
    "name": "Azángaro",
    "sku": "05",
    "skuDepPro": "1510"
  },
  "151006": {
    "name": "Cacra",
    "sku": "06",
    "skuDepPro": "1510"
  },
  "151007": {
    "name": "Carania",
    "sku": "07",
    "skuDepPro": "1510"
  },
  "151008": {
    "name": "Catahuasi",
    "sku": "08",
    "skuDepPro": "1510"
  },
  "151009": {
    "name": "Chocos",
    "sku": "09",
    "skuDepPro": "1510"
  },
  "151010": {
    "name": "Cochas",
    "sku": "10",
    "skuDepPro": "1510"
  },
  "151011": {
    "name": "Colonia",
    "sku": "11",
    "skuDepPro": "1510"
  },
  "151012": {
    "name": "Hongos",
    "sku": "12",
    "skuDepPro": "1510"
  },
  "151013": {
    "name": "Huampara",
    "sku": "13",
    "skuDepPro": "1510"
  },
  "151014": {
    "name": "Huancaya",
    "sku": "14",
    "skuDepPro": "1510"
  },
  "151015": {
    "name": "Huangascar",
    "sku": "15",
    "skuDepPro": "1510"
  },
  "151016": {
    "name": "Huantan",
    "sku": "16",
    "skuDepPro": "1510"
  },
  "151017": {
    "name": "Huañec",
    "sku": "17",
    "skuDepPro": "1510"
  },
  "151018": {
    "name": "Laraos",
    "sku": "18",
    "skuDepPro": "1510"
  },
  "151019": {
    "name": "Lincha",
    "sku": "19",
    "skuDepPro": "1510"
  },
  "151020": {
    "name": "Madean",
    "sku": "20",
    "skuDepPro": "1510"
  },
  "151021": {
    "name": "Miraflores",
    "sku": "21",
    "skuDepPro": "1510"
  },
  "151022": {
    "name": "Omas",
    "sku": "22",
    "skuDepPro": "1510"
  },
  "151023": {
    "name": "Putinza",
    "sku": "23",
    "skuDepPro": "1510"
  },
  "151024": {
    "name": "Quinches",
    "sku": "24",
    "skuDepPro": "1510"
  },
  "151025": {
    "name": "Quinocay",
    "sku": "25",
    "skuDepPro": "1510"
  },
  "151026": {
    "name": "San Joaquin",
    "sku": "26",
    "skuDepPro": "1510"
  },
  "151027": {
    "name": "San Pedro de Pilas",
    "sku": "27",
    "skuDepPro": "1510"
  },
  "151028": {
    "name": "Tanta",
    "sku": "28",
    "skuDepPro": "1510"
  },
  "151029": {
    "name": "Tauripampa",
    "sku": "29",
    "skuDepPro": "1510"
  },
  "151030": {
    "name": "Tomas",
    "sku": "30",
    "skuDepPro": "1510"
  },
  "151031": {
    "name": "Tupe",
    "sku": "31",
    "skuDepPro": "1510"
  },
  "151032": {
    "name": "Viñac",
    "sku": "32",
    "skuDepPro": "1510"
  },
  "151033": {
    "name": "Vitis",
    "sku": "33",
    "skuDepPro": "1510"
  },
  "160101": {
    "name": "Iquitos",
    "sku": "01",
    "skuDepPro": "1601"
  },
  "160102": {
    "name": "Alto Nanay",
    "sku": "02",
    "skuDepPro": "1601"
  },
  "160103": {
    "name": "Fernando Lores",
    "sku": "03",
    "skuDepPro": "1601"
  },
  "160104": {
    "name": "Indiana",
    "sku": "04",
    "skuDepPro": "1601"
  },
  "160105": {
    "name": "Las Amazonas",
    "sku": "05",
    "skuDepPro": "1601"
  },
  "160106": {
    "name": "Mazan",
    "sku": "06",
    "skuDepPro": "1601"
  },
  "160107": {
    "name": "Napo",
    "sku": "07",
    "skuDepPro": "1601"
  },
  "160108": {
    "name": "Punchana",
    "sku": "08",
    "skuDepPro": "1601"
  },
  "160110": {
    "name": "Torres Causana",
    "sku": "10",
    "skuDepPro": "1601"
  },
  "160112": {
    "name": "Belen",
    "sku": "12",
    "skuDepPro": "1601"
  },
  "160113": {
    "name": "San Juan Bautista",
    "sku": "13",
    "skuDepPro": "1601"
  },
  "160201": {
    "name": "Yurimaguas",
    "sku": "01",
    "skuDepPro": "1602"
  },
  "160202": {
    "name": "Balsapuerto",
    "sku": "02",
    "skuDepPro": "1602"
  },
  "160205": {
    "name": "Jeberos",
    "sku": "05",
    "skuDepPro": "1602"
  },
  "160206": {
    "name": "Lagunas",
    "sku": "06",
    "skuDepPro": "1602"
  },
  "160210": {
    "name": "Santa Cruz",
    "sku": "10",
    "skuDepPro": "1602"
  },
  "160211": {
    "name": "Teniente Cesar Lopez Rojas",
    "sku": "11",
    "skuDepPro": "1602"
  },
  "160301": {
    "name": "Nauta",
    "sku": "01",
    "skuDepPro": "1603"
  },
  "160302": {
    "name": "Parinari",
    "sku": "02",
    "skuDepPro": "1603"
  },
  "160303": {
    "name": "Tigre",
    "sku": "03",
    "skuDepPro": "1603"
  },
  "160304": {
    "name": "Trompeteros",
    "sku": "04",
    "skuDepPro": "1603"
  },
  "160305": {
    "name": "Urarinas",
    "sku": "05",
    "skuDepPro": "1603"
  },
  "160401": {
    "name": "Ramon Castilla",
    "sku": "01",
    "skuDepPro": "1604"
  },
  "160402": {
    "name": "Pebas",
    "sku": "02",
    "skuDepPro": "1604"
  },
  "160403": {
    "name": "Yavari",
    "sku": "03",
    "skuDepPro": "1604"
  },
  "160404": {
    "name": "San Pablo",
    "sku": "04",
    "skuDepPro": "1604"
  },
  "160501": {
    "name": "Requena",
    "sku": "01",
    "skuDepPro": "1605"
  },
  "160502": {
    "name": "Alto Tapiche",
    "sku": "02",
    "skuDepPro": "1605"
  },
  "160503": {
    "name": "Capelo",
    "sku": "03",
    "skuDepPro": "1605"
  },
  "160504": {
    "name": "Emilio San Martín",
    "sku": "04",
    "skuDepPro": "1605"
  },
  "160505": {
    "name": "Maquia",
    "sku": "05",
    "skuDepPro": "1605"
  },
  "160506": {
    "name": "Puinahua",
    "sku": "06",
    "skuDepPro": "1605"
  },
  "160507": {
    "name": "Saquena",
    "sku": "07",
    "skuDepPro": "1605"
  },
  "160508": {
    "name": "Soplin",
    "sku": "08",
    "skuDepPro": "1605"
  },
  "160509": {
    "name": "Tapiche",
    "sku": "09",
    "skuDepPro": "1605"
  },
  "160510": {
    "name": "Jenaro Herrera",
    "sku": "10",
    "skuDepPro": "1605"
  },
  "160511": {
    "name": "Yaquerana",
    "sku": "11",
    "skuDepPro": "1605"
  },
  "160601": {
    "name": "Contamana",
    "sku": "01",
    "skuDepPro": "1606"
  },
  "160602": {
    "name": "Inahuaya",
    "sku": "02",
    "skuDepPro": "1606"
  },
  "160603": {
    "name": "Padre Marquez",
    "sku": "03",
    "skuDepPro": "1606"
  },
  "160604": {
    "name": "Pampa Hermosa",
    "sku": "04",
    "skuDepPro": "1606"
  },
  "160605": {
    "name": "Sarayacu",
    "sku": "05",
    "skuDepPro": "1606"
  },
  "160606": {
    "name": "Vargas Guerra",
    "sku": "06",
    "skuDepPro": "1606"
  },
  "160701": {
    "name": "Barranca",
    "sku": "01",
    "skuDepPro": "1607"
  },
  "160702": {
    "name": "Cahuapanas",
    "sku": "02",
    "skuDepPro": "1607"
  },
  "160703": {
    "name": "Manseriche",
    "sku": "03",
    "skuDepPro": "1607"
  },
  "160704": {
    "name": "Morona",
    "sku": "04",
    "skuDepPro": "1607"
  },
  "160705": {
    "name": "Pastaza",
    "sku": "05",
    "skuDepPro": "1607"
  },
  "160706": {
    "name": "Andoas",
    "sku": "06",
    "skuDepPro": "1607"
  },
  "160801": {
    "name": "Putumayo",
    "sku": "01",
    "skuDepPro": "1608"
  },
  "160802": {
    "name": "Rosa Panduro",
    "sku": "02",
    "skuDepPro": "1608"
  },
  "160803": {
    "name": "Teniente Manuel Clavero",
    "sku": "03",
    "skuDepPro": "1608"
  },
  "160804": {
    "name": "Yaguas",
    "sku": "04",
    "skuDepPro": "1608"
  },
  "170101": {
    "name": "Tambopata",
    "sku": "01",
    "skuDepPro": "1701"
  },
  "170102": {
    "name": "Inambari",
    "sku": "02",
    "skuDepPro": "1701"
  },
  "170103": {
    "name": "Las Piedras",
    "sku": "03",
    "skuDepPro": "1701"
  },
  "170104": {
    "name": "Laberinto",
    "sku": "04",
    "skuDepPro": "1701"
  },
  "170201": {
    "name": "Manu",
    "sku": "01",
    "skuDepPro": "1702"
  },
  "170202": {
    "name": "Fitzcarrald",
    "sku": "02",
    "skuDepPro": "1702"
  },
  "170203": {
    "name": "Madre de Dios",
    "sku": "03",
    "skuDepPro": "1702"
  },
  "170204": {
    "name": "Huepetuhe",
    "sku": "04",
    "skuDepPro": "1702"
  },
  "170301": {
    "name": "Iñapari",
    "sku": "01",
    "skuDepPro": "1703"
  },
  "170302": {
    "name": "Iberia",
    "sku": "02",
    "skuDepPro": "1703"
  },
  "170303": {
    "name": "Tahuamanu",
    "sku": "03",
    "skuDepPro": "1703"
  },
  "180101": {
    "name": "Moquegua",
    "sku": "01",
    "skuDepPro": "1801"
  },
  "180102": {
    "name": "Carumas",
    "sku": "02",
    "skuDepPro": "1801"
  },
  "180103": {
    "name": "Cuchumbaya",
    "sku": "03",
    "skuDepPro": "1801"
  },
  "180104": {
    "name": "Samegua",
    "sku": "04",
    "skuDepPro": "1801"
  },
  "180105": {
    "name": "San Cristobal",
    "sku": "05",
    "skuDepPro": "1801"
  },
  "180106": {
    "name": "Torata",
    "sku": "06",
    "skuDepPro": "1801"
  },
  "180201": {
    "name": "Omate",
    "sku": "01",
    "skuDepPro": "1802"
  },
  "180202": {
    "name": "Chojata",
    "sku": "02",
    "skuDepPro": "1802"
  },
  "180203": {
    "name": "Coalaque",
    "sku": "03",
    "skuDepPro": "1802"
  },
  "180204": {
    "name": "Ichuña",
    "sku": "04",
    "skuDepPro": "1802"
  },
  "180205": {
    "name": "La Capilla",
    "sku": "05",
    "skuDepPro": "1802"
  },
  "180206": {
    "name": "Lloque",
    "sku": "06",
    "skuDepPro": "1802"
  },
  "180207": {
    "name": "Matalaque",
    "sku": "07",
    "skuDepPro": "1802"
  },
  "180208": {
    "name": "Puquina",
    "sku": "08",
    "skuDepPro": "1802"
  },
  "180209": {
    "name": "Quinistaquillas",
    "sku": "09",
    "skuDepPro": "1802"
  },
  "180210": {
    "name": "Ubinas",
    "sku": "10",
    "skuDepPro": "1802"
  },
  "180211": {
    "name": "Yunga",
    "sku": "11",
    "skuDepPro": "1802"
  },
  "180301": {
    "name": "Ilo",
    "sku": "01",
    "skuDepPro": "1803"
  },
  "180302": {
    "name": "El Algarrobal",
    "sku": "02",
    "skuDepPro": "1803"
  },
  "180303": {
    "name": "Pacocha",
    "sku": "03",
    "skuDepPro": "1803"
  },
  "190101": {
    "name": "Chaupimarca",
    "sku": "01",
    "skuDepPro": "1901"
  },
  "190102": {
    "name": "Huachon",
    "sku": "02",
    "skuDepPro": "1901"
  },
  "190103": {
    "name": "Huariaca",
    "sku": "03",
    "skuDepPro": "1901"
  },
  "190104": {
    "name": "Huayllay",
    "sku": "04",
    "skuDepPro": "1901"
  },
  "190105": {
    "name": "Ninacaca",
    "sku": "05",
    "skuDepPro": "1901"
  },
  "190106": {
    "name": "Pallanchacra",
    "sku": "06",
    "skuDepPro": "1901"
  },
  "190107": {
    "name": "Paucartambo",
    "sku": "07",
    "skuDepPro": "1901"
  },
  "190108": {
    "name": "San Francisco de Asis de Yarusyacan",
    "sku": "08",
    "skuDepPro": "1901"
  },
  "190109": {
    "name": "Simon Bolívar",
    "sku": "09",
    "skuDepPro": "1901"
  },
  "190110": {
    "name": "Ticlacayan",
    "sku": "10",
    "skuDepPro": "1901"
  },
  "190111": {
    "name": "Tinyahuarco",
    "sku": "11",
    "skuDepPro": "1901"
  },
  "190112": {
    "name": "Vicco",
    "sku": "12",
    "skuDepPro": "1901"
  },
  "190113": {
    "name": "Yanacancha",
    "sku": "13",
    "skuDepPro": "1901"
  },
  "190201": {
    "name": "Yanahuanca",
    "sku": "01",
    "skuDepPro": "1902"
  },
  "190202": {
    "name": "Chacayan",
    "sku": "02",
    "skuDepPro": "1902"
  },
  "190203": {
    "name": "Goyllarisquizga",
    "sku": "03",
    "skuDepPro": "1902"
  },
  "190204": {
    "name": "Paucar",
    "sku": "04",
    "skuDepPro": "1902"
  },
  "190205": {
    "name": "San Pedro de Pillao",
    "sku": "05",
    "skuDepPro": "1902"
  },
  "190206": {
    "name": "Santa Ana de Tusi",
    "sku": "06",
    "skuDepPro": "1902"
  },
  "190207": {
    "name": "Tapuc",
    "sku": "07",
    "skuDepPro": "1902"
  },
  "190208": {
    "name": "Vilcabamba",
    "sku": "08",
    "skuDepPro": "1902"
  },
  "190301": {
    "name": "Oxapampa",
    "sku": "01",
    "skuDepPro": "1903"
  },
  "190302": {
    "name": "Chontabamba",
    "sku": "02",
    "skuDepPro": "1903"
  },
  "190303": {
    "name": "Huancabamba",
    "sku": "03",
    "skuDepPro": "1903"
  },
  "190304": {
    "name": "Palcazu",
    "sku": "04",
    "skuDepPro": "1903"
  },
  "190305": {
    "name": "Pozuzo",
    "sku": "05",
    "skuDepPro": "1903"
  },
  "190306": {
    "name": "Puerto Bermudez",
    "sku": "06",
    "skuDepPro": "1903"
  },
  "190307": {
    "name": "Villa Rica",
    "sku": "07",
    "skuDepPro": "1903"
  },
  "190308": {
    "name": "Constitucion",
    "sku": "08",
    "skuDepPro": "1903"
  },
  "200101": {
    "name": "Piura",
    "sku": "01",
    "skuDepPro": "2001"
  },
  "200104": {
    "name": "Castilla",
    "sku": "04",
    "skuDepPro": "2001"
  },
  "200105": {
    "name": "Catacaos",
    "sku": "05",
    "skuDepPro": "2001"
  },
  "200107": {
    "name": "Cura Mori",
    "sku": "07",
    "skuDepPro": "2001"
  },
  "200108": {
    "name": "El Tallan",
    "sku": "08",
    "skuDepPro": "2001"
  },
  "200109": {
    "name": "La Arena",
    "sku": "09",
    "skuDepPro": "2001"
  },
  "200110": {
    "name": "La Union",
    "sku": "10",
    "skuDepPro": "2001"
  },
  "200111": {
    "name": "Las Lomas",
    "sku": "11",
    "skuDepPro": "2001"
  },
  "200114": {
    "name": "Tambo Grande",
    "sku": "14",
    "skuDepPro": "2001"
  },
  "200115": {
    "name": "Veintiseis de Octubre",
    "sku": "15",
    "skuDepPro": "2001"
  },
  "200201": {
    "name": "Ayabaca",
    "sku": "01",
    "skuDepPro": "2002"
  },
  "200202": {
    "name": "Frias",
    "sku": "02",
    "skuDepPro": "2002"
  },
  "200203": {
    "name": "Jilili",
    "sku": "03",
    "skuDepPro": "2002"
  },
  "200204": {
    "name": "Lagunas",
    "sku": "04",
    "skuDepPro": "2002"
  },
  "200205": {
    "name": "Montero",
    "sku": "05",
    "skuDepPro": "2002"
  },
  "200206": {
    "name": "Pacaipampa",
    "sku": "06",
    "skuDepPro": "2002"
  },
  "200207": {
    "name": "Paimas",
    "sku": "07",
    "skuDepPro": "2002"
  },
  "200208": {
    "name": "Sapillica",
    "sku": "08",
    "skuDepPro": "2002"
  },
  "200209": {
    "name": "Sicchez",
    "sku": "09",
    "skuDepPro": "2002"
  },
  "200210": {
    "name": "Suyo",
    "sku": "10",
    "skuDepPro": "2002"
  },
  "200301": {
    "name": "Huancabamba",
    "sku": "01",
    "skuDepPro": "2003"
  },
  "200302": {
    "name": "Canchaque",
    "sku": "02",
    "skuDepPro": "2003"
  },
  "200303": {
    "name": "El Carmen de la Frontera",
    "sku": "03",
    "skuDepPro": "2003"
  },
  "200304": {
    "name": "Huarmaca",
    "sku": "04",
    "skuDepPro": "2003"
  },
  "200305": {
    "name": "Lalaquiz",
    "sku": "05",
    "skuDepPro": "2003"
  },
  "200306": {
    "name": "San Miguel de El Faique",
    "sku": "06",
    "skuDepPro": "2003"
  },
  "200307": {
    "name": "Sondor",
    "sku": "07",
    "skuDepPro": "2003"
  },
  "200308": {
    "name": "Sondorillo",
    "sku": "08",
    "skuDepPro": "2003"
  },
  "200401": {
    "name": "Chulucanas",
    "sku": "01",
    "skuDepPro": "2004"
  },
  "200402": {
    "name": "Buenos Aires",
    "sku": "02",
    "skuDepPro": "2004"
  },
  "200403": {
    "name": "Chalaco",
    "sku": "03",
    "skuDepPro": "2004"
  },
  "200404": {
    "name": "La Matanza",
    "sku": "04",
    "skuDepPro": "2004"
  },
  "200405": {
    "name": "Morropón",
    "sku": "05",
    "skuDepPro": "2004"
  },
  "200406": {
    "name": "Salitral",
    "sku": "06",
    "skuDepPro": "2004"
  },
  "200407": {
    "name": "San Juan de Bigote",
    "sku": "07",
    "skuDepPro": "2004"
  },
  "200408": {
    "name": "Santa Catalina de Mossa",
    "sku": "08",
    "skuDepPro": "2004"
  },
  "200409": {
    "name": "Santo Domingo",
    "sku": "09",
    "skuDepPro": "2004"
  },
  "200410": {
    "name": "Yamango",
    "sku": "10",
    "skuDepPro": "2004"
  },
  "200501": {
    "name": "Paita",
    "sku": "01",
    "skuDepPro": "2005"
  },
  "200502": {
    "name": "Amotape",
    "sku": "02",
    "skuDepPro": "2005"
  },
  "200503": {
    "name": "Arenal",
    "sku": "03",
    "skuDepPro": "2005"
  },
  "200504": {
    "name": "Colan",
    "sku": "04",
    "skuDepPro": "2005"
  },
  "200505": {
    "name": "La Huaca",
    "sku": "05",
    "skuDepPro": "2005"
  },
  "200506": {
    "name": "Tamarindo",
    "sku": "06",
    "skuDepPro": "2005"
  },
  "200507": {
    "name": "Vichayal",
    "sku": "07",
    "skuDepPro": "2005"
  },
  "200601": {
    "name": "Sullana",
    "sku": "01",
    "skuDepPro": "2006"
  },
  "200602": {
    "name": "Bellavista",
    "sku": "02",
    "skuDepPro": "2006"
  },
  "200603": {
    "name": "Ignacio Escudero",
    "sku": "03",
    "skuDepPro": "2006"
  },
  "200604": {
    "name": "Lancones",
    "sku": "04",
    "skuDepPro": "2006"
  },
  "200605": {
    "name": "Marcavelica",
    "sku": "05",
    "skuDepPro": "2006"
  },
  "200606": {
    "name": "Miguel Checa",
    "sku": "06",
    "skuDepPro": "2006"
  },
  "200607": {
    "name": "Querecotillo",
    "sku": "07",
    "skuDepPro": "2006"
  },
  "200608": {
    "name": "Salitral",
    "sku": "08",
    "skuDepPro": "2006"
  },
  "200701": {
    "name": "Pariñas",
    "sku": "01",
    "skuDepPro": "2007"
  },
  "200702": {
    "name": "El Alto",
    "sku": "02",
    "skuDepPro": "2007"
  },
  "200703": {
    "name": "La Brea",
    "sku": "03",
    "skuDepPro": "2007"
  },
  "200704": {
    "name": "Lobitos",
    "sku": "04",
    "skuDepPro": "2007"
  },
  "200705": {
    "name": "Los Organos",
    "sku": "05",
    "skuDepPro": "2007"
  },
  "200706": {
    "name": "Mancora",
    "sku": "06",
    "skuDepPro": "2007"
  },
  "200801": {
    "name": "Sechura",
    "sku": "01",
    "skuDepPro": "2008"
  },
  "200802": {
    "name": "Bellavista de la Union",
    "sku": "02",
    "skuDepPro": "2008"
  },
  "200803": {
    "name": "Bernal",
    "sku": "03",
    "skuDepPro": "2008"
  },
  "200804": {
    "name": "Cristo Nos Valga",
    "sku": "04",
    "skuDepPro": "2008"
  },
  "200805": {
    "name": "Vice",
    "sku": "05",
    "skuDepPro": "2008"
  },
  "200806": {
    "name": "Rinconada Llicuar",
    "sku": "06",
    "skuDepPro": "2008"
  },
  "210101": {
    "name": "Puno",
    "sku": "01",
    "skuDepPro": "2101"
  },
  "210102": {
    "name": "Acora",
    "sku": "02",
    "skuDepPro": "2101"
  },
  "210103": {
    "name": "Amantani",
    "sku": "03",
    "skuDepPro": "2101"
  },
  "210104": {
    "name": "Atuncolla",
    "sku": "04",
    "skuDepPro": "2101"
  },
  "210105": {
    "name": "Capachica",
    "sku": "05",
    "skuDepPro": "2101"
  },
  "210106": {
    "name": "Chucuito",
    "sku": "06",
    "skuDepPro": "2101"
  },
  "210107": {
    "name": "Coata",
    "sku": "07",
    "skuDepPro": "2101"
  },
  "210108": {
    "name": "Huata",
    "sku": "08",
    "skuDepPro": "2101"
  },
  "210109": {
    "name": "Mañazo",
    "sku": "09",
    "skuDepPro": "2101"
  },
  "210110": {
    "name": "Paucarcolla",
    "sku": "10",
    "skuDepPro": "2101"
  },
  "210111": {
    "name": "Pichacani",
    "sku": "11",
    "skuDepPro": "2101"
  },
  "210112": {
    "name": "Plateria",
    "sku": "12",
    "skuDepPro": "2101"
  },
  "210113": {
    "name": "San Antonio",
    "sku": "13",
    "skuDepPro": "2101"
  },
  "210114": {
    "name": "Tiquillaca",
    "sku": "14",
    "skuDepPro": "2101"
  },
  "210115": {
    "name": "Vilque",
    "sku": "15",
    "skuDepPro": "2101"
  },
  "210201": {
    "name": "Azángaro",
    "sku": "01",
    "skuDepPro": "2102"
  },
  "210202": {
    "name": "Achaya",
    "sku": "02",
    "skuDepPro": "2102"
  },
  "210203": {
    "name": "Arapa",
    "sku": "03",
    "skuDepPro": "2102"
  },
  "210204": {
    "name": "Asillo",
    "sku": "04",
    "skuDepPro": "2102"
  },
  "210205": {
    "name": "Caminaca",
    "sku": "05",
    "skuDepPro": "2102"
  },
  "210206": {
    "name": "Chupa",
    "sku": "06",
    "skuDepPro": "2102"
  },
  "210207": {
    "name": "Jose Domingo Choquehuanca",
    "sku": "07",
    "skuDepPro": "2102"
  },
  "210208": {
    "name": "Muñani",
    "sku": "08",
    "skuDepPro": "2102"
  },
  "210209": {
    "name": "Potoni",
    "sku": "09",
    "skuDepPro": "2102"
  },
  "210210": {
    "name": "Saman",
    "sku": "10",
    "skuDepPro": "2102"
  },
  "210211": {
    "name": "San Anton",
    "sku": "11",
    "skuDepPro": "2102"
  },
  "210212": {
    "name": "San Jose",
    "sku": "12",
    "skuDepPro": "2102"
  },
  "210213": {
    "name": "San Juan de Salinas",
    "sku": "13",
    "skuDepPro": "2102"
  },
  "210214": {
    "name": "Santiago de Pupuja",
    "sku": "14",
    "skuDepPro": "2102"
  },
  "210215": {
    "name": "Tirapata",
    "sku": "15",
    "skuDepPro": "2102"
  },
  "210301": {
    "name": "Macusani",
    "sku": "01",
    "skuDepPro": "2103"
  },
  "210302": {
    "name": "Ajoyani",
    "sku": "02",
    "skuDepPro": "2103"
  },
  "210303": {
    "name": "Ayapata",
    "sku": "03",
    "skuDepPro": "2103"
  },
  "210304": {
    "name": "Coasa",
    "sku": "04",
    "skuDepPro": "2103"
  },
  "210305": {
    "name": "Corani",
    "sku": "05",
    "skuDepPro": "2103"
  },
  "210306": {
    "name": "Crucero",
    "sku": "06",
    "skuDepPro": "2103"
  },
  "210307": {
    "name": "Ituata",
    "sku": "07",
    "skuDepPro": "2103"
  },
  "210308": {
    "name": "Ollachea",
    "sku": "08",
    "skuDepPro": "2103"
  },
  "210309": {
    "name": "San Gaban",
    "sku": "09",
    "skuDepPro": "2103"
  },
  "210310": {
    "name": "Usicayos",
    "sku": "10",
    "skuDepPro": "2103"
  },
  "210401": {
    "name": "Juli",
    "sku": "01",
    "skuDepPro": "2104"
  },
  "210402": {
    "name": "Desaguadero",
    "sku": "02",
    "skuDepPro": "2104"
  },
  "210403": {
    "name": "Huacullani",
    "sku": "03",
    "skuDepPro": "2104"
  },
  "210404": {
    "name": "Kelluyo",
    "sku": "04",
    "skuDepPro": "2104"
  },
  "210405": {
    "name": "Pisacoma",
    "sku": "05",
    "skuDepPro": "2104"
  },
  "210406": {
    "name": "Pomata",
    "sku": "06",
    "skuDepPro": "2104"
  },
  "210407": {
    "name": "Zepita",
    "sku": "07",
    "skuDepPro": "2104"
  },
  "210501": {
    "name": "Ilave",
    "sku": "01",
    "skuDepPro": "2105"
  },
  "210502": {
    "name": "Capazo",
    "sku": "02",
    "skuDepPro": "2105"
  },
  "210503": {
    "name": "Pilcuyo",
    "sku": "03",
    "skuDepPro": "2105"
  },
  "210504": {
    "name": "Santa Rosa",
    "sku": "04",
    "skuDepPro": "2105"
  },
  "210505": {
    "name": "Conduriri",
    "sku": "05",
    "skuDepPro": "2105"
  },
  "210601": {
    "name": "Huancané",
    "sku": "01",
    "skuDepPro": "2106"
  },
  "210602": {
    "name": "Cojata",
    "sku": "02",
    "skuDepPro": "2106"
  },
  "210603": {
    "name": "Huatasani",
    "sku": "03",
    "skuDepPro": "2106"
  },
  "210604": {
    "name": "Inchupalla",
    "sku": "04",
    "skuDepPro": "2106"
  },
  "210605": {
    "name": "Pusi",
    "sku": "05",
    "skuDepPro": "2106"
  },
  "210606": {
    "name": "Rosaspata",
    "sku": "06",
    "skuDepPro": "2106"
  },
  "210607": {
    "name": "Taraco",
    "sku": "07",
    "skuDepPro": "2106"
  },
  "210608": {
    "name": "Vilque Chico",
    "sku": "08",
    "skuDepPro": "2106"
  },
  "210701": {
    "name": "Lampa",
    "sku": "01",
    "skuDepPro": "2107"
  },
  "210702": {
    "name": "Cabanilla",
    "sku": "02",
    "skuDepPro": "2107"
  },
  "210703": {
    "name": "Calapuja",
    "sku": "03",
    "skuDepPro": "2107"
  },
  "210704": {
    "name": "Nicasio",
    "sku": "04",
    "skuDepPro": "2107"
  },
  "210705": {
    "name": "Ocuviri",
    "sku": "05",
    "skuDepPro": "2107"
  },
  "210706": {
    "name": "Palca",
    "sku": "06",
    "skuDepPro": "2107"
  },
  "210707": {
    "name": "Paratia",
    "sku": "07",
    "skuDepPro": "2107"
  },
  "210708": {
    "name": "Pucara",
    "sku": "08",
    "skuDepPro": "2107"
  },
  "210709": {
    "name": "Santa Lucia",
    "sku": "09",
    "skuDepPro": "2107"
  },
  "210710": {
    "name": "Vilavila",
    "sku": "10",
    "skuDepPro": "2107"
  },
  "210801": {
    "name": "Ayaviri",
    "sku": "01",
    "skuDepPro": "2108"
  },
  "210802": {
    "name": "Antauta",
    "sku": "02",
    "skuDepPro": "2108"
  },
  "210803": {
    "name": "Cupi",
    "sku": "03",
    "skuDepPro": "2108"
  },
  "210804": {
    "name": "Llalli",
    "sku": "04",
    "skuDepPro": "2108"
  },
  "210805": {
    "name": "Macari",
    "sku": "05",
    "skuDepPro": "2108"
  },
  "210806": {
    "name": "Nuñoa",
    "sku": "06",
    "skuDepPro": "2108"
  },
  "210807": {
    "name": "Orurillo",
    "sku": "07",
    "skuDepPro": "2108"
  },
  "210808": {
    "name": "Santa Rosa",
    "sku": "08",
    "skuDepPro": "2108"
  },
  "210809": {
    "name": "Umachiri",
    "sku": "09",
    "skuDepPro": "2108"
  },
  "210901": {
    "name": "Moho",
    "sku": "01",
    "skuDepPro": "2109"
  },
  "210902": {
    "name": "Conima",
    "sku": "02",
    "skuDepPro": "2109"
  },
  "210903": {
    "name": "Huayrapata",
    "sku": "03",
    "skuDepPro": "2109"
  },
  "210904": {
    "name": "Tilali",
    "sku": "04",
    "skuDepPro": "2109"
  },
  "211001": {
    "name": "Putina",
    "sku": "01",
    "skuDepPro": "2110"
  },
  "211002": {
    "name": "Ananea",
    "sku": "02",
    "skuDepPro": "2110"
  },
  "211003": {
    "name": "Pedro Vilca Apaza",
    "sku": "03",
    "skuDepPro": "2110"
  },
  "211004": {
    "name": "Quilcapuncu",
    "sku": "04",
    "skuDepPro": "2110"
  },
  "211005": {
    "name": "Sina",
    "sku": "05",
    "skuDepPro": "2110"
  },
  "211101": {
    "name": "Juliaca",
    "sku": "01",
    "skuDepPro": "2111"
  },
  "211102": {
    "name": "Cabana",
    "sku": "02",
    "skuDepPro": "2111"
  },
  "211103": {
    "name": "Cabanillas",
    "sku": "03",
    "skuDepPro": "2111"
  },
  "211104": {
    "name": "Caracoto",
    "sku": "04",
    "skuDepPro": "2111"
  },
  "211105": {
    "name": "San Miguel",
    "sku": "05",
    "skuDepPro": "2111"
  },
  "211201": {
    "name": "Sandia",
    "sku": "01",
    "skuDepPro": "2112"
  },
  "211202": {
    "name": "Cuyocuyo",
    "sku": "02",
    "skuDepPro": "2112"
  },
  "211203": {
    "name": "Limbani",
    "sku": "03",
    "skuDepPro": "2112"
  },
  "211204": {
    "name": "Patambuco",
    "sku": "04",
    "skuDepPro": "2112"
  },
  "211205": {
    "name": "Phara",
    "sku": "05",
    "skuDepPro": "2112"
  },
  "211206": {
    "name": "Quiaca",
    "sku": "06",
    "skuDepPro": "2112"
  },
  "211207": {
    "name": "San Juan del Oro",
    "sku": "07",
    "skuDepPro": "2112"
  },
  "211208": {
    "name": "Yanahuaya",
    "sku": "08",
    "skuDepPro": "2112"
  },
  "211209": {
    "name": "Alto Inambari",
    "sku": "09",
    "skuDepPro": "2112"
  },
  "211210": {
    "name": "San Pedro de Putina Punco",
    "sku": "10",
    "skuDepPro": "2112"
  },
  "211301": {
    "name": "Yunguyo",
    "sku": "01",
    "skuDepPro": "2113"
  },
  "211302": {
    "name": "Anapia",
    "sku": "02",
    "skuDepPro": "2113"
  },
  "211303": {
    "name": "Copani",
    "sku": "03",
    "skuDepPro": "2113"
  },
  "211304": {
    "name": "Cuturapi",
    "sku": "04",
    "skuDepPro": "2113"
  },
  "211305": {
    "name": "Ollaraya",
    "sku": "05",
    "skuDepPro": "2113"
  },
  "211306": {
    "name": "Tinicachi",
    "sku": "06",
    "skuDepPro": "2113"
  },
  "211307": {
    "name": "Unicachi",
    "sku": "07",
    "skuDepPro": "2113"
  },
  "220101": {
    "name": "Moyobamba",
    "sku": "01",
    "skuDepPro": "2201"
  },
  "220102": {
    "name": "Calzada",
    "sku": "02",
    "skuDepPro": "2201"
  },
  "220103": {
    "name": "Habana",
    "sku": "03",
    "skuDepPro": "2201"
  },
  "220104": {
    "name": "Jepelacio",
    "sku": "04",
    "skuDepPro": "2201"
  },
  "220105": {
    "name": "Soritor",
    "sku": "05",
    "skuDepPro": "2201"
  },
  "220106": {
    "name": "Yantalo",
    "sku": "06",
    "skuDepPro": "2201"
  },
  "220201": {
    "name": "Bellavista",
    "sku": "01",
    "skuDepPro": "2202"
  },
  "220202": {
    "name": "Alto Biavo",
    "sku": "02",
    "skuDepPro": "2202"
  },
  "220203": {
    "name": "Bajo Biavo",
    "sku": "03",
    "skuDepPro": "2202"
  },
  "220204": {
    "name": "Huallaga",
    "sku": "04",
    "skuDepPro": "2202"
  },
  "220205": {
    "name": "San Pablo",
    "sku": "05",
    "skuDepPro": "2202"
  },
  "220206": {
    "name": "San Rafael",
    "sku": "06",
    "skuDepPro": "2202"
  },
  "220301": {
    "name": "San Jose de Sisa",
    "sku": "01",
    "skuDepPro": "2203"
  },
  "220302": {
    "name": "Agua Blanca",
    "sku": "02",
    "skuDepPro": "2203"
  },
  "220303": {
    "name": "San Martín",
    "sku": "03",
    "skuDepPro": "2203"
  },
  "220304": {
    "name": "Santa Rosa",
    "sku": "04",
    "skuDepPro": "2203"
  },
  "220305": {
    "name": "Shatoja",
    "sku": "05",
    "skuDepPro": "2203"
  },
  "220401": {
    "name": "Saposoa",
    "sku": "01",
    "skuDepPro": "2204"
  },
  "220402": {
    "name": "Alto Saposoa",
    "sku": "02",
    "skuDepPro": "2204"
  },
  "220403": {
    "name": "El Eslabon",
    "sku": "03",
    "skuDepPro": "2204"
  },
  "220404": {
    "name": "Piscoyacu",
    "sku": "04",
    "skuDepPro": "2204"
  },
  "220405": {
    "name": "Sacanche",
    "sku": "05",
    "skuDepPro": "2204"
  },
  "220406": {
    "name": "Tingo de Saposoa",
    "sku": "06",
    "skuDepPro": "2204"
  },
  "220501": {
    "name": "Lamas",
    "sku": "01",
    "skuDepPro": "2205"
  },
  "220502": {
    "name": "Alonso de Alvarado",
    "sku": "02",
    "skuDepPro": "2205"
  },
  "220503": {
    "name": "Barranquita",
    "sku": "03",
    "skuDepPro": "2205"
  },
  "220504": {
    "name": "Caynarachi",
    "sku": "04",
    "skuDepPro": "2205"
  },
  "220505": {
    "name": "Cuñumbuqui",
    "sku": "05",
    "skuDepPro": "2205"
  },
  "220506": {
    "name": "Pinto Recodo",
    "sku": "06",
    "skuDepPro": "2205"
  },
  "220507": {
    "name": "Rumisapa",
    "sku": "07",
    "skuDepPro": "2205"
  },
  "220508": {
    "name": "San Roque de Cumbaza",
    "sku": "08",
    "skuDepPro": "2205"
  },
  "220509": {
    "name": "Shanao",
    "sku": "09",
    "skuDepPro": "2205"
  },
  "220510": {
    "name": "Tabalosos",
    "sku": "10",
    "skuDepPro": "2205"
  },
  "220511": {
    "name": "Zapatero",
    "sku": "11",
    "skuDepPro": "2205"
  },
  "220601": {
    "name": "Juanjui",
    "sku": "01",
    "skuDepPro": "2206"
  },
  "220602": {
    "name": "Campanilla",
    "sku": "02",
    "skuDepPro": "2206"
  },
  "220603": {
    "name": "Huicungo",
    "sku": "03",
    "skuDepPro": "2206"
  },
  "220604": {
    "name": "Pachiza",
    "sku": "04",
    "skuDepPro": "2206"
  },
  "220605": {
    "name": "Pajarillo",
    "sku": "05",
    "skuDepPro": "2206"
  },
  "220701": {
    "name": "Picota",
    "sku": "01",
    "skuDepPro": "2207"
  },
  "220702": {
    "name": "Buenos Aires",
    "sku": "02",
    "skuDepPro": "2207"
  },
  "220703": {
    "name": "Caspisapa",
    "sku": "03",
    "skuDepPro": "2207"
  },
  "220704": {
    "name": "Pilluana",
    "sku": "04",
    "skuDepPro": "2207"
  },
  "220705": {
    "name": "Pucacaca",
    "sku": "05",
    "skuDepPro": "2207"
  },
  "220706": {
    "name": "San Cristobal",
    "sku": "06",
    "skuDepPro": "2207"
  },
  "220707": {
    "name": "San Hilarion",
    "sku": "07",
    "skuDepPro": "2207"
  },
  "220708": {
    "name": "Shamboyacu",
    "sku": "08",
    "skuDepPro": "2207"
  },
  "220709": {
    "name": "Tingo de Ponasa",
    "sku": "09",
    "skuDepPro": "2207"
  },
  "220710": {
    "name": "Tres Unidos",
    "sku": "10",
    "skuDepPro": "2207"
  },
  "220801": {
    "name": "Rioja",
    "sku": "01",
    "skuDepPro": "2208"
  },
  "220802": {
    "name": "Awajun",
    "sku": "02",
    "skuDepPro": "2208"
  },
  "220803": {
    "name": "Elias Soplin Vargas",
    "sku": "03",
    "skuDepPro": "2208"
  },
  "220804": {
    "name": "Nueva Cajamarca",
    "sku": "04",
    "skuDepPro": "2208"
  },
  "220805": {
    "name": "Pardo Miguel",
    "sku": "05",
    "skuDepPro": "2208"
  },
  "220806": {
    "name": "Posic",
    "sku": "06",
    "skuDepPro": "2208"
  },
  "220807": {
    "name": "San Fernando",
    "sku": "07",
    "skuDepPro": "2208"
  },
  "220808": {
    "name": "Yorongos",
    "sku": "08",
    "skuDepPro": "2208"
  },
  "220809": {
    "name": "Yuracyacu",
    "sku": "09",
    "skuDepPro": "2208"
  },
  "220901": {
    "name": "Tarapoto",
    "sku": "01",
    "skuDepPro": "2209"
  },
  "220902": {
    "name": "Alberto Leveau",
    "sku": "02",
    "skuDepPro": "2209"
  },
  "220903": {
    "name": "Cacatachi",
    "sku": "03",
    "skuDepPro": "2209"
  },
  "220904": {
    "name": "Chazuta",
    "sku": "04",
    "skuDepPro": "2209"
  },
  "220905": {
    "name": "Chipurana",
    "sku": "05",
    "skuDepPro": "2209"
  },
  "220906": {
    "name": "El Porvenir",
    "sku": "06",
    "skuDepPro": "2209"
  },
  "220907": {
    "name": "Huimbayoc",
    "sku": "07",
    "skuDepPro": "2209"
  },
  "220908": {
    "name": "Juan Guerra",
    "sku": "08",
    "skuDepPro": "2209"
  },
  "220909": {
    "name": "La Banda de Shilcayo",
    "sku": "09",
    "skuDepPro": "2209"
  },
  "220910": {
    "name": "Morales",
    "sku": "10",
    "skuDepPro": "2209"
  },
  "220911": {
    "name": "Papaplaya",
    "sku": "11",
    "skuDepPro": "2209"
  },
  "220912": {
    "name": "San Antonio",
    "sku": "12",
    "skuDepPro": "2209"
  },
  "220913": {
    "name": "Sauce",
    "sku": "13",
    "skuDepPro": "2209"
  },
  "220914": {
    "name": "Shapaja",
    "sku": "14",
    "skuDepPro": "2209"
  },
  "221001": {
    "name": "Tocache",
    "sku": "01",
    "skuDepPro": "2210"
  },
  "221002": {
    "name": "Nuevo Progreso",
    "sku": "02",
    "skuDepPro": "2210"
  },
  "221003": {
    "name": "Polvora",
    "sku": "03",
    "skuDepPro": "2210"
  },
  "221004": {
    "name": "Shunte",
    "sku": "04",
    "skuDepPro": "2210"
  },
  "221005": {
    "name": "Uchiza",
    "sku": "05",
    "skuDepPro": "2210"
  },
  "230101": {
    "name": "Tacna",
    "sku": "01",
    "skuDepPro": "2301"
  },
  "230102": {
    "name": "Alto de la Alianza",
    "sku": "02",
    "skuDepPro": "2301"
  },
  "230103": {
    "name": "Calana",
    "sku": "03",
    "skuDepPro": "2301"
  },
  "230104": {
    "name": "Ciudad Nueva",
    "sku": "04",
    "skuDepPro": "2301"
  },
  "230105": {
    "name": "Inclan",
    "sku": "05",
    "skuDepPro": "2301"
  },
  "230106": {
    "name": "Pachia",
    "sku": "06",
    "skuDepPro": "2301"
  },
  "230107": {
    "name": "Palca",
    "sku": "07",
    "skuDepPro": "2301"
  },
  "230108": {
    "name": "Pocollay",
    "sku": "08",
    "skuDepPro": "2301"
  },
  "230109": {
    "name": "Sama",
    "sku": "09",
    "skuDepPro": "2301"
  },
  "230110": {
    "name": "Coronel Gregorio Albarracin Lanchipa",
    "sku": "10",
    "skuDepPro": "2301"
  },
  "230111": {
    "name": "La Yarada los Palos",
    "sku": "11",
    "skuDepPro": "2301"
  },
  "230201": {
    "name": "Candarave",
    "sku": "01",
    "skuDepPro": "2302"
  },
  "230202": {
    "name": "Cairani",
    "sku": "02",
    "skuDepPro": "2302"
  },
  "230203": {
    "name": "Camilaca",
    "sku": "03",
    "skuDepPro": "2302"
  },
  "230204": {
    "name": "Curibaya",
    "sku": "04",
    "skuDepPro": "2302"
  },
  "230205": {
    "name": "Huanuara",
    "sku": "05",
    "skuDepPro": "2302"
  },
  "230206": {
    "name": "Quilahuani",
    "sku": "06",
    "skuDepPro": "2302"
  },
  "230301": {
    "name": "Locumba",
    "sku": "01",
    "skuDepPro": "2303"
  },
  "230302": {
    "name": "Ilabaya",
    "sku": "02",
    "skuDepPro": "2303"
  },
  "230303": {
    "name": "Ite",
    "sku": "03",
    "skuDepPro": "2303"
  },
  "230401": {
    "name": "Tarata",
    "sku": "01",
    "skuDepPro": "2304"
  },
  "230402": {
    "name": "Heroes Albarracin",
    "sku": "02",
    "skuDepPro": "2304"
  },
  "230403": {
    "name": "Estique",
    "sku": "03",
    "skuDepPro": "2304"
  },
  "230404": {
    "name": "Estique-Pampa",
    "sku": "04",
    "skuDepPro": "2304"
  },
  "230405": {
    "name": "Sitajara",
    "sku": "05",
    "skuDepPro": "2304"
  },
  "230406": {
    "name": "Susapaya",
    "sku": "06",
    "skuDepPro": "2304"
  },
  "230407": {
    "name": "Tarucachi",
    "sku": "07",
    "skuDepPro": "2304"
  },
  "230408": {
    "name": "Ticaco",
    "sku": "08",
    "skuDepPro": "2304"
  },
  "240101": {
    "name": "Tumbes",
    "sku": "01",
    "skuDepPro": "2401"
  },
  "240102": {
    "name": "Corrales",
    "sku": "02",
    "skuDepPro": "2401"
  },
  "240103": {
    "name": "La Cruz",
    "sku": "03",
    "skuDepPro": "2401"
  },
  "240104": {
    "name": "Pampas de Hospital",
    "sku": "04",
    "skuDepPro": "2401"
  },
  "240105": {
    "name": "San Jacinto",
    "sku": "05",
    "skuDepPro": "2401"
  },
  "240106": {
    "name": "San Juan de la Virgen",
    "sku": "06",
    "skuDepPro": "2401"
  },
  "240201": {
    "name": "Zorritos",
    "sku": "01",
    "skuDepPro": "2402"
  },
  "240202": {
    "name": "Casitas",
    "sku": "02",
    "skuDepPro": "2402"
  },
  "240203": {
    "name": "Canoas de Punta Sal",
    "sku": "03",
    "skuDepPro": "2402"
  },
  "240301": {
    "name": "Zarumilla",
    "sku": "01",
    "skuDepPro": "2403"
  },
  "240302": {
    "name": "Aguas Verdes",
    "sku": "02",
    "skuDepPro": "2403"
  },
  "240303": {
    "name": "Matapalo",
    "sku": "03",
    "skuDepPro": "2403"
  },
  "240304": {
    "name": "Papayal",
    "sku": "04",
    "skuDepPro": "2403"
  },
  "250101": {
    "name": "Calleria",
    "sku": "01",
    "skuDepPro": "2501"
  },
  "250102": {
    "name": "Campoverde",
    "sku": "02",
    "skuDepPro": "2501"
  },
  "250103": {
    "name": "Iparia",
    "sku": "03",
    "skuDepPro": "2501"
  },
  "250104": {
    "name": "Masisea",
    "sku": "04",
    "skuDepPro": "2501"
  },
  "250105": {
    "name": "Yarinacocha",
    "sku": "05",
    "skuDepPro": "2501"
  },
  "250106": {
    "name": "Nueva Requena",
    "sku": "06",
    "skuDepPro": "2501"
  },
  "250107": {
    "name": "Manantay",
    "sku": "07",
    "skuDepPro": "2501"
  },
  "250201": {
    "name": "Raymondi",
    "sku": "01",
    "skuDepPro": "2502"
  },
  "250202": {
    "name": "Sepahua",
    "sku": "02",
    "skuDepPro": "2502"
  },
  "250203": {
    "name": "Tahuania",
    "sku": "03",
    "skuDepPro": "2502"
  },
  "250204": {
    "name": "Yurua",
    "sku": "04",
    "skuDepPro": "2502"
  },
  "250301": {
    "name": "Padre Abad",
    "sku": "01",
    "skuDepPro": "2503"
  },
  "250302": {
    "name": "Irazola",
    "sku": "02",
    "skuDepPro": "2503"
  },
  "250303": {
    "name": "Curimana",
    "sku": "03",
    "skuDepPro": "2503"
  },
  "250304": {
    "name": "Neshuya",
    "sku": "04",
    "skuDepPro": "2503"
  },
  "250305": {
    "name": "Alexander Von Humboldt",
    "sku": "05",
    "skuDepPro": "2503"
  },
  "250401": {
    "name": "Purus",
    "sku": "01",
    "skuDepPro": "2504"
  },
  "010101": {
    "name": "Chachapoyas",
    "sku": "01",
    "skuDepPro": "0101"
  },
  "010102": {
    "name": "Asuncion",
    "sku": "02",
    "skuDepPro": "0101"
  },
  "010103": {
    "name": "Balsas",
    "sku": "03",
    "skuDepPro": "0101"
  },
  "010104": {
    "name": "Cheto",
    "sku": "04",
    "skuDepPro": "0101"
  },
  "010105": {
    "name": "Chiliquin",
    "sku": "05",
    "skuDepPro": "0101"
  },
  "010106": {
    "name": "Chuquibamba",
    "sku": "06",
    "skuDepPro": "0101"
  },
  "010107": {
    "name": "Granada",
    "sku": "07",
    "skuDepPro": "0101"
  },
  "010108": {
    "name": "Huancas",
    "sku": "08",
    "skuDepPro": "0101"
  },
  "010109": {
    "name": "La Jalca",
    "sku": "09",
    "skuDepPro": "0101"
  },
  "010110": {
    "name": "Leimebamba",
    "sku": "10",
    "skuDepPro": "0101"
  },
  "010111": {
    "name": "Levanto",
    "sku": "11",
    "skuDepPro": "0101"
  },
  "010112": {
    "name": "Magdalena",
    "sku": "12",
    "skuDepPro": "0101"
  },
  "010113": {
    "name": "Mariscal Castilla",
    "sku": "13",
    "skuDepPro": "0101"
  },
  "010114": {
    "name": "Molinopampa",
    "sku": "14",
    "skuDepPro": "0101"
  },
  "010115": {
    "name": "Montevideo",
    "sku": "15",
    "skuDepPro": "0101"
  },
  "010116": {
    "name": "Olleros",
    "sku": "16",
    "skuDepPro": "0101"
  },
  "010117": {
    "name": "Quinjalca",
    "sku": "17",
    "skuDepPro": "0101"
  },
  "010118": {
    "name": "San Francisco de Daguas",
    "sku": "18",
    "skuDepPro": "0101"
  },
  "010119": {
    "name": "San Isidro de Maino",
    "sku": "19",
    "skuDepPro": "0101"
  },
  "010120": {
    "name": "Soloco",
    "sku": "20",
    "skuDepPro": "0101"
  },
  "010121": {
    "name": "Sonche",
    "sku": "21",
    "skuDepPro": "0101"
  },
  "010201": {
    "name": "Bagua",
    "sku": "01",
    "skuDepPro": "0102"
  },
  "010202": {
    "name": "Aramango",
    "sku": "02",
    "skuDepPro": "0102"
  },
  "010203": {
    "name": "Copallin",
    "sku": "03",
    "skuDepPro": "0102"
  },
  "010204": {
    "name": "El Parco",
    "sku": "04",
    "skuDepPro": "0102"
  },
  "010205": {
    "name": "Imaza",
    "sku": "05",
    "skuDepPro": "0102"
  },
  "010206": {
    "name": "La Peca",
    "sku": "06",
    "skuDepPro": "0102"
  },
  "010301": {
    "name": "Jumbilla",
    "sku": "01",
    "skuDepPro": "0103"
  },
  "010302": {
    "name": "Chisquilla",
    "sku": "02",
    "skuDepPro": "0103"
  },
  "010303": {
    "name": "Churuja",
    "sku": "03",
    "skuDepPro": "0103"
  },
  "010304": {
    "name": "Corosha",
    "sku": "04",
    "skuDepPro": "0103"
  },
  "010305": {
    "name": "Cuispes",
    "sku": "05",
    "skuDepPro": "0103"
  },
  "010306": {
    "name": "Florida",
    "sku": "06",
    "skuDepPro": "0103"
  },
  "010307": {
    "name": "Jazan",
    "sku": "07",
    "skuDepPro": "0103"
  },
  "010308": {
    "name": "Recta",
    "sku": "08",
    "skuDepPro": "0103"
  },
  "010309": {
    "name": "San Carlos",
    "sku": "09",
    "skuDepPro": "0103"
  },
  "010310": {
    "name": "Shipasbamba",
    "sku": "10",
    "skuDepPro": "0103"
  },
  "010311": {
    "name": "Valera",
    "sku": "11",
    "skuDepPro": "0103"
  },
  "010312": {
    "name": "Yambrasbamba",
    "sku": "12",
    "skuDepPro": "0103"
  },
  "010401": {
    "name": "Nieva",
    "sku": "01",
    "skuDepPro": "0104"
  },
  "010402": {
    "name": "El Cenepa",
    "sku": "02",
    "skuDepPro": "0104"
  },
  "010403": {
    "name": "Rio Santiago",
    "sku": "03",
    "skuDepPro": "0104"
  },
  "010501": {
    "name": "Lamud",
    "sku": "01",
    "skuDepPro": "0105"
  },
  "010502": {
    "name": "Camporredondo",
    "sku": "02",
    "skuDepPro": "0105"
  },
  "010503": {
    "name": "Cocabamba",
    "sku": "03",
    "skuDepPro": "0105"
  },
  "010504": {
    "name": "Colcamar",
    "sku": "04",
    "skuDepPro": "0105"
  },
  "010505": {
    "name": "Conila",
    "sku": "05",
    "skuDepPro": "0105"
  },
  "010506": {
    "name": "Inguilpata",
    "sku": "06",
    "skuDepPro": "0105"
  },
  "010507": {
    "name": "Longuita",
    "sku": "07",
    "skuDepPro": "0105"
  },
  "010508": {
    "name": "Lonya Chico",
    "sku": "08",
    "skuDepPro": "0105"
  },
  "010509": {
    "name": "Luya",
    "sku": "09",
    "skuDepPro": "0105"
  },
  "010510": {
    "name": "Luya Viejo",
    "sku": "10",
    "skuDepPro": "0105"
  },
  "010511": {
    "name": "Maria",
    "sku": "11",
    "skuDepPro": "0105"
  },
  "010512": {
    "name": "Ocalli",
    "sku": "12",
    "skuDepPro": "0105"
  },
  "010513": {
    "name": "Ocumal",
    "sku": "13",
    "skuDepPro": "0105"
  },
  "010514": {
    "name": "Pisuquia",
    "sku": "14",
    "skuDepPro": "0105"
  },
  "010515": {
    "name": "Providencia",
    "sku": "15",
    "skuDepPro": "0105"
  },
  "010516": {
    "name": "San Cristobal",
    "sku": "16",
    "skuDepPro": "0105"
  },
  "010517": {
    "name": "San Francisco de Yeso",
    "sku": "17",
    "skuDepPro": "0105"
  },
  "010518": {
    "name": "San Jeronimo",
    "sku": "18",
    "skuDepPro": "0105"
  },
  "010519": {
    "name": "San Juan de Lopecancha",
    "sku": "19",
    "skuDepPro": "0105"
  },
  "010520": {
    "name": "Santa Catalina",
    "sku": "20",
    "skuDepPro": "0105"
  },
  "010521": {
    "name": "Santo Tomas",
    "sku": "21",
    "skuDepPro": "0105"
  },
  "010522": {
    "name": "Tingo",
    "sku": "22",
    "skuDepPro": "0105"
  },
  "010523": {
    "name": "Trita",
    "sku": "23",
    "skuDepPro": "0105"
  },
  "010601": {
    "name": "San Nicolas",
    "sku": "01",
    "skuDepPro": "0106"
  },
  "010602": {
    "name": "Chirimoto",
    "sku": "02",
    "skuDepPro": "0106"
  },
  "010603": {
    "name": "Cochamal",
    "sku": "03",
    "skuDepPro": "0106"
  },
  "010604": {
    "name": "Huambo",
    "sku": "04",
    "skuDepPro": "0106"
  },
  "010605": {
    "name": "Limabamba",
    "sku": "05",
    "skuDepPro": "0106"
  },
  "010606": {
    "name": "Longar",
    "sku": "06",
    "skuDepPro": "0106"
  },
  "010607": {
    "name": "Mariscal Benavides",
    "sku": "07",
    "skuDepPro": "0106"
  },
  "010608": {
    "name": "Milpuc",
    "sku": "08",
    "skuDepPro": "0106"
  },
  "010609": {
    "name": "Omia",
    "sku": "09",
    "skuDepPro": "0106"
  },
  "010610": {
    "name": "Santa Rosa",
    "sku": "10",
    "skuDepPro": "0106"
  },
  "010611": {
    "name": "Totora",
    "sku": "11",
    "skuDepPro": "0106"
  },
  "010612": {
    "name": "Vista Alegre",
    "sku": "12",
    "skuDepPro": "0106"
  },
  "010701": {
    "name": "Bagua Grande",
    "sku": "01",
    "skuDepPro": "0107"
  },
  "010702": {
    "name": "Cajaruro",
    "sku": "02",
    "skuDepPro": "0107"
  },
  "010703": {
    "name": "Cumba",
    "sku": "03",
    "skuDepPro": "0107"
  },
  "010704": {
    "name": "El Milagro",
    "sku": "04",
    "skuDepPro": "0107"
  },
  "010705": {
    "name": "Jamalca",
    "sku": "05",
    "skuDepPro": "0107"
  },
  "010706": {
    "name": "Lonya Grande",
    "sku": "06",
    "skuDepPro": "0107"
  },
  "010707": {
    "name": "Yamon",
    "sku": "07",
    "skuDepPro": "0107"
  },
  "020101": {
    "name": "Huaraz",
    "sku": "01",
    "skuDepPro": "0201"
  },
  "020102": {
    "name": "Cochabamba",
    "sku": "02",
    "skuDepPro": "0201"
  },
  "020103": {
    "name": "Colcabamba",
    "sku": "03",
    "skuDepPro": "0201"
  },
  "020104": {
    "name": "Huanchay",
    "sku": "04",
    "skuDepPro": "0201"
  },
  "020105": {
    "name": "Independencia",
    "sku": "05",
    "skuDepPro": "0201"
  },
  "020106": {
    "name": "Jangas",
    "sku": "06",
    "skuDepPro": "0201"
  },
  "020107": {
    "name": "La Libertad",
    "sku": "07",
    "skuDepPro": "0201"
  },
  "020108": {
    "name": "Olleros",
    "sku": "08",
    "skuDepPro": "0201"
  },
  "020109": {
    "name": "Pampas Grande",
    "sku": "09",
    "skuDepPro": "0201"
  },
  "020110": {
    "name": "Pariacoto",
    "sku": "10",
    "skuDepPro": "0201"
  },
  "020111": {
    "name": "Pira",
    "sku": "11",
    "skuDepPro": "0201"
  },
  "020112": {
    "name": "Tarica",
    "sku": "12",
    "skuDepPro": "0201"
  },
  "020201": {
    "name": "Aija",
    "sku": "01",
    "skuDepPro": "0202"
  },
  "020202": {
    "name": "Coris",
    "sku": "02",
    "skuDepPro": "0202"
  },
  "020203": {
    "name": "Huacllan",
    "sku": "03",
    "skuDepPro": "0202"
  },
  "020204": {
    "name": "La Merced",
    "sku": "04",
    "skuDepPro": "0202"
  },
  "020205": {
    "name": "Succha",
    "sku": "05",
    "skuDepPro": "0202"
  },
  "020301": {
    "name": "Llamellin",
    "sku": "01",
    "skuDepPro": "0203"
  },
  "020302": {
    "name": "Aczo",
    "sku": "02",
    "skuDepPro": "0203"
  },
  "020303": {
    "name": "Chaccho",
    "sku": "03",
    "skuDepPro": "0203"
  },
  "020304": {
    "name": "Chingas",
    "sku": "04",
    "skuDepPro": "0203"
  },
  "020305": {
    "name": "Mirgas",
    "sku": "05",
    "skuDepPro": "0203"
  },
  "020306": {
    "name": "San Juan de Rontoy",
    "sku": "06",
    "skuDepPro": "0203"
  },
  "020401": {
    "name": "Chacas",
    "sku": "01",
    "skuDepPro": "0204"
  },
  "020402": {
    "name": "Acochaca",
    "sku": "02",
    "skuDepPro": "0204"
  },
  "020501": {
    "name": "Chiquian",
    "sku": "01",
    "skuDepPro": "0205"
  },
  "020502": {
    "name": "Abelardo Pardo Lezameta",
    "sku": "02",
    "skuDepPro": "0205"
  },
  "020503": {
    "name": "Antonio Raymondi",
    "sku": "03",
    "skuDepPro": "0205"
  },
  "020504": {
    "name": "Aquia",
    "sku": "04",
    "skuDepPro": "0205"
  },
  "020505": {
    "name": "Cajacay",
    "sku": "05",
    "skuDepPro": "0205"
  },
  "020506": {
    "name": "Canis",
    "sku": "06",
    "skuDepPro": "0205"
  },
  "020507": {
    "name": "Colquioc",
    "sku": "07",
    "skuDepPro": "0205"
  },
  "020508": {
    "name": "Huallanca",
    "sku": "08",
    "skuDepPro": "0205"
  },
  "020509": {
    "name": "Huasta",
    "sku": "09",
    "skuDepPro": "0205"
  },
  "020510": {
    "name": "Huayllacayan",
    "sku": "10",
    "skuDepPro": "0205"
  },
  "020511": {
    "name": "La Primavera",
    "sku": "11",
    "skuDepPro": "0205"
  },
  "020512": {
    "name": "Mangas",
    "sku": "12",
    "skuDepPro": "0205"
  },
  "020513": {
    "name": "Pacllon",
    "sku": "13",
    "skuDepPro": "0205"
  },
  "020514": {
    "name": "San Miguel de Corpanqui",
    "sku": "14",
    "skuDepPro": "0205"
  },
  "020515": {
    "name": "Ticllos",
    "sku": "15",
    "skuDepPro": "0205"
  },
  "020601": {
    "name": "Carhuaz",
    "sku": "01",
    "skuDepPro": "0206"
  },
  "020602": {
    "name": "Acopampa",
    "sku": "02",
    "skuDepPro": "0206"
  },
  "020603": {
    "name": "Amashca",
    "sku": "03",
    "skuDepPro": "0206"
  },
  "020604": {
    "name": "Anta",
    "sku": "04",
    "skuDepPro": "0206"
  },
  "020605": {
    "name": "Ataquero",
    "sku": "05",
    "skuDepPro": "0206"
  },
  "020606": {
    "name": "Marcara",
    "sku": "06",
    "skuDepPro": "0206"
  },
  "020607": {
    "name": "Pariahuanca",
    "sku": "07",
    "skuDepPro": "0206"
  },
  "020608": {
    "name": "San Miguel de Aco",
    "sku": "08",
    "skuDepPro": "0206"
  },
  "020609": {
    "name": "Shilla",
    "sku": "09",
    "skuDepPro": "0206"
  },
  "020610": {
    "name": "Tinco",
    "sku": "10",
    "skuDepPro": "0206"
  },
  "020611": {
    "name": "Yungar",
    "sku": "11",
    "skuDepPro": "0206"
  },
  "020701": {
    "name": "San Luis",
    "sku": "01",
    "skuDepPro": "0207"
  },
  "020702": {
    "name": "San Nicolas",
    "sku": "02",
    "skuDepPro": "0207"
  },
  "020703": {
    "name": "Yauya",
    "sku": "03",
    "skuDepPro": "0207"
  },
  "020801": {
    "name": "Casma",
    "sku": "01",
    "skuDepPro": "0208"
  },
  "020802": {
    "name": "Buena Vista Alta",
    "sku": "02",
    "skuDepPro": "0208"
  },
  "020803": {
    "name": "Comandante Noel",
    "sku": "03",
    "skuDepPro": "0208"
  },
  "020804": {
    "name": "Yautan",
    "sku": "04",
    "skuDepPro": "0208"
  },
  "020901": {
    "name": "Corongo",
    "sku": "01",
    "skuDepPro": "0209"
  },
  "020902": {
    "name": "Aco",
    "sku": "02",
    "skuDepPro": "0209"
  },
  "020903": {
    "name": "Bambas",
    "sku": "03",
    "skuDepPro": "0209"
  },
  "020904": {
    "name": "Cusca",
    "sku": "04",
    "skuDepPro": "0209"
  },
  "020905": {
    "name": "La Pampa",
    "sku": "05",
    "skuDepPro": "0209"
  },
  "020906": {
    "name": "Yanac",
    "sku": "06",
    "skuDepPro": "0209"
  },
  "020907": {
    "name": "Yupan",
    "sku": "07",
    "skuDepPro": "0209"
  },
  "021001": {
    "name": "Huari",
    "sku": "01",
    "skuDepPro": "0210"
  },
  "021002": {
    "name": "Anra",
    "sku": "02",
    "skuDepPro": "0210"
  },
  "021003": {
    "name": "Cajay",
    "sku": "03",
    "skuDepPro": "0210"
  },
  "021004": {
    "name": "Chavin de Huantar",
    "sku": "04",
    "skuDepPro": "0210"
  },
  "021005": {
    "name": "Huacachi",
    "sku": "05",
    "skuDepPro": "0210"
  },
  "021006": {
    "name": "Huacchis",
    "sku": "06",
    "skuDepPro": "0210"
  },
  "021007": {
    "name": "Huachis",
    "sku": "07",
    "skuDepPro": "0210"
  },
  "021008": {
    "name": "Huantar",
    "sku": "08",
    "skuDepPro": "0210"
  },
  "021009": {
    "name": "Masin",
    "sku": "09",
    "skuDepPro": "0210"
  },
  "021010": {
    "name": "Paucas",
    "sku": "10",
    "skuDepPro": "0210"
  },
  "021011": {
    "name": "Ponto",
    "sku": "11",
    "skuDepPro": "0210"
  },
  "021012": {
    "name": "Rahuapampa",
    "sku": "12",
    "skuDepPro": "0210"
  },
  "021013": {
    "name": "Rapayan",
    "sku": "13",
    "skuDepPro": "0210"
  },
  "021014": {
    "name": "San Marcos",
    "sku": "14",
    "skuDepPro": "0210"
  },
  "021015": {
    "name": "San Pedro de Chana",
    "sku": "15",
    "skuDepPro": "0210"
  },
  "021016": {
    "name": "Uco",
    "sku": "16",
    "skuDepPro": "0210"
  },
  "021101": {
    "name": "Huarmey",
    "sku": "01",
    "skuDepPro": "0211"
  },
  "021102": {
    "name": "Cochapeti",
    "sku": "02",
    "skuDepPro": "0211"
  },
  "021103": {
    "name": "Culebras",
    "sku": "03",
    "skuDepPro": "0211"
  },
  "021104": {
    "name": "Huayan",
    "sku": "04",
    "skuDepPro": "0211"
  },
  "021105": {
    "name": "Malvas",
    "sku": "05",
    "skuDepPro": "0211"
  },
  "021201": {
    "name": "Caraz",
    "sku": "01",
    "skuDepPro": "0212"
  },
  "021202": {
    "name": "Huallanca",
    "sku": "02",
    "skuDepPro": "0212"
  },
  "021203": {
    "name": "Huata",
    "sku": "03",
    "skuDepPro": "0212"
  },
  "021204": {
    "name": "Huaylas",
    "sku": "04",
    "skuDepPro": "0212"
  },
  "021205": {
    "name": "Mato",
    "sku": "05",
    "skuDepPro": "0212"
  },
  "021206": {
    "name": "Pamparomas",
    "sku": "06",
    "skuDepPro": "0212"
  },
  "021207": {
    "name": "Pueblo Libre",
    "sku": "07",
    "skuDepPro": "0212"
  },
  "021208": {
    "name": "Santa Cruz",
    "sku": "08",
    "skuDepPro": "0212"
  },
  "021209": {
    "name": "Santo Toribio",
    "sku": "09",
    "skuDepPro": "0212"
  },
  "021210": {
    "name": "Yuracmarca",
    "sku": "10",
    "skuDepPro": "0212"
  },
  "021301": {
    "name": "Piscobamba",
    "sku": "01",
    "skuDepPro": "0213"
  },
  "021302": {
    "name": "Casca",
    "sku": "02",
    "skuDepPro": "0213"
  },
  "021303": {
    "name": "Eleazar Guzman Barron",
    "sku": "03",
    "skuDepPro": "0213"
  },
  "021304": {
    "name": "Fidel Olivas Escudero",
    "sku": "04",
    "skuDepPro": "0213"
  },
  "021305": {
    "name": "Llama",
    "sku": "05",
    "skuDepPro": "0213"
  },
  "021306": {
    "name": "Llumpa",
    "sku": "06",
    "skuDepPro": "0213"
  },
  "021307": {
    "name": "Lucma",
    "sku": "07",
    "skuDepPro": "0213"
  },
  "021308": {
    "name": "Musga",
    "sku": "08",
    "skuDepPro": "0213"
  },
  "021401": {
    "name": "Ocros",
    "sku": "01",
    "skuDepPro": "0214"
  },
  "021402": {
    "name": "Acas",
    "sku": "02",
    "skuDepPro": "0214"
  },
  "021403": {
    "name": "Cajamarquilla",
    "sku": "03",
    "skuDepPro": "0214"
  },
  "021404": {
    "name": "Carhuapampa",
    "sku": "04",
    "skuDepPro": "0214"
  },
  "021405": {
    "name": "Cochas",
    "sku": "05",
    "skuDepPro": "0214"
  },
  "021406": {
    "name": "Congas",
    "sku": "06",
    "skuDepPro": "0214"
  },
  "021407": {
    "name": "Llipa",
    "sku": "07",
    "skuDepPro": "0214"
  },
  "021408": {
    "name": "San Cristobal de Rajan",
    "sku": "08",
    "skuDepPro": "0214"
  },
  "021409": {
    "name": "San Pedro",
    "sku": "09",
    "skuDepPro": "0214"
  },
  "021410": {
    "name": "Santiago de Chilcas",
    "sku": "10",
    "skuDepPro": "0214"
  },
  "021501": {
    "name": "Cabana",
    "sku": "01",
    "skuDepPro": "0215"
  },
  "021502": {
    "name": "Bolognesi",
    "sku": "02",
    "skuDepPro": "0215"
  },
  "021503": {
    "name": "Conchucos",
    "sku": "03",
    "skuDepPro": "0215"
  },
  "021504": {
    "name": "Huacaschuque",
    "sku": "04",
    "skuDepPro": "0215"
  },
  "021505": {
    "name": "Huandoval",
    "sku": "05",
    "skuDepPro": "0215"
  },
  "021506": {
    "name": "Lacabamba",
    "sku": "06",
    "skuDepPro": "0215"
  },
  "021507": {
    "name": "Llapo",
    "sku": "07",
    "skuDepPro": "0215"
  },
  "021508": {
    "name": "Pallasca",
    "sku": "08",
    "skuDepPro": "0215"
  },
  "021509": {
    "name": "Pampas",
    "sku": "09",
    "skuDepPro": "0215"
  },
  "021510": {
    "name": "Santa Rosa",
    "sku": "10",
    "skuDepPro": "0215"
  },
  "021511": {
    "name": "Tauca",
    "sku": "11",
    "skuDepPro": "0215"
  },
  "021601": {
    "name": "Pomabamba",
    "sku": "01",
    "skuDepPro": "0216"
  },
  "021602": {
    "name": "Huayllan",
    "sku": "02",
    "skuDepPro": "0216"
  },
  "021603": {
    "name": "Parobamba",
    "sku": "03",
    "skuDepPro": "0216"
  },
  "021604": {
    "name": "Quinuabamba",
    "sku": "04",
    "skuDepPro": "0216"
  },
  "021701": {
    "name": "Recuay",
    "sku": "01",
    "skuDepPro": "0217"
  },
  "021702": {
    "name": "Catac",
    "sku": "02",
    "skuDepPro": "0217"
  },
  "021703": {
    "name": "Cotaparaco",
    "sku": "03",
    "skuDepPro": "0217"
  },
  "021704": {
    "name": "Huayllapampa",
    "sku": "04",
    "skuDepPro": "0217"
  },
  "021705": {
    "name": "Llacllin",
    "sku": "05",
    "skuDepPro": "0217"
  },
  "021706": {
    "name": "Marca",
    "sku": "06",
    "skuDepPro": "0217"
  },
  "021707": {
    "name": "Pampas Chico",
    "sku": "07",
    "skuDepPro": "0217"
  },
  "021708": {
    "name": "Pararin",
    "sku": "08",
    "skuDepPro": "0217"
  },
  "021709": {
    "name": "Tapacocha",
    "sku": "09",
    "skuDepPro": "0217"
  },
  "021710": {
    "name": "Ticapampa",
    "sku": "10",
    "skuDepPro": "0217"
  },
  "021801": {
    "name": "Chimbote",
    "sku": "01",
    "skuDepPro": "0218"
  },
  "021802": {
    "name": "Caceres del Peru",
    "sku": "02",
    "skuDepPro": "0218"
  },
  "021803": {
    "name": "Coishco",
    "sku": "03",
    "skuDepPro": "0218"
  },
  "021804": {
    "name": "Macate",
    "sku": "04",
    "skuDepPro": "0218"
  },
  "021805": {
    "name": "Moro",
    "sku": "05",
    "skuDepPro": "0218"
  },
  "021806": {
    "name": "Nepeña",
    "sku": "06",
    "skuDepPro": "0218"
  },
  "021807": {
    "name": "Samanco",
    "sku": "07",
    "skuDepPro": "0218"
  },
  "021808": {
    "name": "Santa",
    "sku": "08",
    "skuDepPro": "0218"
  },
  "021809": {
    "name": "Nuevo Chimbote",
    "sku": "09",
    "skuDepPro": "0218"
  },
  "021901": {
    "name": "Sihuas",
    "sku": "01",
    "skuDepPro": "0219"
  },
  "021902": {
    "name": "Acobamba",
    "sku": "02",
    "skuDepPro": "0219"
  },
  "021903": {
    "name": "Alfonso Ugarte",
    "sku": "03",
    "skuDepPro": "0219"
  },
  "021904": {
    "name": "Cashapampa",
    "sku": "04",
    "skuDepPro": "0219"
  },
  "021905": {
    "name": "Chingalpo",
    "sku": "05",
    "skuDepPro": "0219"
  },
  "021906": {
    "name": "Huayllabamba",
    "sku": "06",
    "skuDepPro": "0219"
  },
  "021907": {
    "name": "Quiches",
    "sku": "07",
    "skuDepPro": "0219"
  },
  "021908": {
    "name": "Ragash",
    "sku": "08",
    "skuDepPro": "0219"
  },
  "021909": {
    "name": "San Juan",
    "sku": "09",
    "skuDepPro": "0219"
  },
  "021910": {
    "name": "Sicsibamba",
    "sku": "10",
    "skuDepPro": "0219"
  },
  "022001": {
    "name": "Yungay",
    "sku": "01",
    "skuDepPro": "0220"
  },
  "022002": {
    "name": "Cascapara",
    "sku": "02",
    "skuDepPro": "0220"
  },
  "022003": {
    "name": "Mancos",
    "sku": "03",
    "skuDepPro": "0220"
  },
  "022004": {
    "name": "Matacoto",
    "sku": "04",
    "skuDepPro": "0220"
  },
  "022005": {
    "name": "Quillo",
    "sku": "05",
    "skuDepPro": "0220"
  },
  "022006": {
    "name": "Ranrahirca",
    "sku": "06",
    "skuDepPro": "0220"
  },
  "022007": {
    "name": "Shupluy",
    "sku": "07",
    "skuDepPro": "0220"
  },
  "022008": {
    "name": "Yanama",
    "sku": "08",
    "skuDepPro": "0220"
  },
  "030101": {
    "name": "Abancay",
    "sku": "01",
    "skuDepPro": "0301"
  },
  "030102": {
    "name": "Chacoche",
    "sku": "02",
    "skuDepPro": "0301"
  },
  "030103": {
    "name": "Circa",
    "sku": "03",
    "skuDepPro": "0301"
  },
  "030104": {
    "name": "Curahuasi",
    "sku": "04",
    "skuDepPro": "0301"
  },
  "030105": {
    "name": "Huanipaca",
    "sku": "05",
    "skuDepPro": "0301"
  },
  "030106": {
    "name": "Lambrama",
    "sku": "06",
    "skuDepPro": "0301"
  },
  "030107": {
    "name": "Pichirhua",
    "sku": "07",
    "skuDepPro": "0301"
  },
  "030108": {
    "name": "San Pedro de Cachora",
    "sku": "08",
    "skuDepPro": "0301"
  },
  "030109": {
    "name": "Tamburco",
    "sku": "09",
    "skuDepPro": "0301"
  },
  "030201": {
    "name": "Andahuaylas",
    "sku": "01",
    "skuDepPro": "0302"
  },
  "030202": {
    "name": "Andarapa",
    "sku": "02",
    "skuDepPro": "0302"
  },
  "030203": {
    "name": "Chiara",
    "sku": "03",
    "skuDepPro": "0302"
  },
  "030204": {
    "name": "Huancarama",
    "sku": "04",
    "skuDepPro": "0302"
  },
  "030205": {
    "name": "Huancaray",
    "sku": "05",
    "skuDepPro": "0302"
  },
  "030206": {
    "name": "Huayana",
    "sku": "06",
    "skuDepPro": "0302"
  },
  "030207": {
    "name": "Kishuara",
    "sku": "07",
    "skuDepPro": "0302"
  },
  "030208": {
    "name": "Pacobamba",
    "sku": "08",
    "skuDepPro": "0302"
  },
  "030209": {
    "name": "Pacucha",
    "sku": "09",
    "skuDepPro": "0302"
  },
  "030210": {
    "name": "Pampachiri",
    "sku": "10",
    "skuDepPro": "0302"
  },
  "030211": {
    "name": "Pomacocha",
    "sku": "11",
    "skuDepPro": "0302"
  },
  "030212": {
    "name": "San Antonio de Cachi",
    "sku": "12",
    "skuDepPro": "0302"
  },
  "030213": {
    "name": "San Jeronimo",
    "sku": "13",
    "skuDepPro": "0302"
  },
  "030214": {
    "name": "San Miguel de Chaccrampa",
    "sku": "14",
    "skuDepPro": "0302"
  },
  "030215": {
    "name": "Santa Maria de Chicmo",
    "sku": "15",
    "skuDepPro": "0302"
  },
  "030216": {
    "name": "Talavera",
    "sku": "16",
    "skuDepPro": "0302"
  },
  "030217": {
    "name": "Tumay Huaraca",
    "sku": "17",
    "skuDepPro": "0302"
  },
  "030218": {
    "name": "Turpo",
    "sku": "18",
    "skuDepPro": "0302"
  },
  "030219": {
    "name": "Kaquiabamba",
    "sku": "19",
    "skuDepPro": "0302"
  },
  "030220": {
    "name": "Jose Maria Arguedas",
    "sku": "20",
    "skuDepPro": "0302"
  },
  "030301": {
    "name": "Antabamba",
    "sku": "01",
    "skuDepPro": "0303"
  },
  "030302": {
    "name": "El Oro",
    "sku": "02",
    "skuDepPro": "0303"
  },
  "030303": {
    "name": "Huaquirca",
    "sku": "03",
    "skuDepPro": "0303"
  },
  "030304": {
    "name": "Juan Espinoza Medrano",
    "sku": "04",
    "skuDepPro": "0303"
  },
  "030305": {
    "name": "Oropesa",
    "sku": "05",
    "skuDepPro": "0303"
  },
  "030306": {
    "name": "Pachaconas",
    "sku": "06",
    "skuDepPro": "0303"
  },
  "030307": {
    "name": "Sabaino",
    "sku": "07",
    "skuDepPro": "0303"
  },
  "030401": {
    "name": "Chalhuanca",
    "sku": "01",
    "skuDepPro": "0304"
  },
  "030402": {
    "name": "Capaya",
    "sku": "02",
    "skuDepPro": "0304"
  },
  "030403": {
    "name": "Caraybamba",
    "sku": "03",
    "skuDepPro": "0304"
  },
  "030404": {
    "name": "Chapimarca",
    "sku": "04",
    "skuDepPro": "0304"
  },
  "030405": {
    "name": "Colcabamba",
    "sku": "05",
    "skuDepPro": "0304"
  },
  "030406": {
    "name": "Cotaruse",
    "sku": "06",
    "skuDepPro": "0304"
  },
  "030407": {
    "name": "Ihuayllo",
    "sku": "07",
    "skuDepPro": "0304"
  },
  "030408": {
    "name": "Justo Apu Sahuaraura",
    "sku": "08",
    "skuDepPro": "0304"
  },
  "030409": {
    "name": "Lucre",
    "sku": "09",
    "skuDepPro": "0304"
  },
  "030410": {
    "name": "Pocohuanca",
    "sku": "10",
    "skuDepPro": "0304"
  },
  "030411": {
    "name": "San Juan de Chacña",
    "sku": "11",
    "skuDepPro": "0304"
  },
  "030412": {
    "name": "Sañayca",
    "sku": "12",
    "skuDepPro": "0304"
  },
  "030413": {
    "name": "Soraya",
    "sku": "13",
    "skuDepPro": "0304"
  },
  "030414": {
    "name": "Tapairihua",
    "sku": "14",
    "skuDepPro": "0304"
  },
  "030415": {
    "name": "Tintay",
    "sku": "15",
    "skuDepPro": "0304"
  },
  "030416": {
    "name": "Toraya",
    "sku": "16",
    "skuDepPro": "0304"
  },
  "030417": {
    "name": "Yanaca",
    "sku": "17",
    "skuDepPro": "0304"
  },
  "030501": {
    "name": "Tambobamba",
    "sku": "01",
    "skuDepPro": "0305"
  },
  "030502": {
    "name": "Cotabambas",
    "sku": "02",
    "skuDepPro": "0305"
  },
  "030503": {
    "name": "Coyllurqui",
    "sku": "03",
    "skuDepPro": "0305"
  },
  "030504": {
    "name": "Haquira",
    "sku": "04",
    "skuDepPro": "0305"
  },
  "030505": {
    "name": "Mara",
    "sku": "05",
    "skuDepPro": "0305"
  },
  "030506": {
    "name": "Challhuahuacho",
    "sku": "06",
    "skuDepPro": "0305"
  },
  "030601": {
    "name": "Chincheros",
    "sku": "01",
    "skuDepPro": "0306"
  },
  "030602": {
    "name": "Anco_Huallo",
    "sku": "02",
    "skuDepPro": "0306"
  },
  "030603": {
    "name": "Cocharcas",
    "sku": "03",
    "skuDepPro": "0306"
  },
  "030604": {
    "name": "Huaccana",
    "sku": "04",
    "skuDepPro": "0306"
  },
  "030605": {
    "name": "Ocobamba",
    "sku": "05",
    "skuDepPro": "0306"
  },
  "030606": {
    "name": "Ongoy",
    "sku": "06",
    "skuDepPro": "0306"
  },
  "030607": {
    "name": "Uranmarca",
    "sku": "07",
    "skuDepPro": "0306"
  },
  "030608": {
    "name": "Ranracancha",
    "sku": "08",
    "skuDepPro": "0306"
  },
  "030609": {
    "name": "Rocchacc",
    "sku": "09",
    "skuDepPro": "0306"
  },
  "030610": {
    "name": "El Porvenir",
    "sku": "10",
    "skuDepPro": "0306"
  },
  "030611": {
    "name": "Los Chankas",
    "sku": "11",
    "skuDepPro": "0306"
  },
  "030701": {
    "name": "Chuquibambilla",
    "sku": "01",
    "skuDepPro": "0307"
  },
  "030702": {
    "name": "Curpahuasi",
    "sku": "02",
    "skuDepPro": "0307"
  },
  "030703": {
    "name": "Gamarra",
    "sku": "03",
    "skuDepPro": "0307"
  },
  "030704": {
    "name": "Huayllati",
    "sku": "04",
    "skuDepPro": "0307"
  },
  "030705": {
    "name": "Mamara",
    "sku": "05",
    "skuDepPro": "0307"
  },
  "030706": {
    "name": "Micaela Bastidas",
    "sku": "06",
    "skuDepPro": "0307"
  },
  "030707": {
    "name": "Pataypampa",
    "sku": "07",
    "skuDepPro": "0307"
  },
  "030708": {
    "name": "Progreso",
    "sku": "08",
    "skuDepPro": "0307"
  },
  "030709": {
    "name": "San Antonio",
    "sku": "09",
    "skuDepPro": "0307"
  },
  "030710": {
    "name": "Santa Rosa",
    "sku": "10",
    "skuDepPro": "0307"
  },
  "030711": {
    "name": "Turpay",
    "sku": "11",
    "skuDepPro": "0307"
  },
  "030712": {
    "name": "Vilcabamba",
    "sku": "12",
    "skuDepPro": "0307"
  },
  "030713": {
    "name": "Virundo",
    "sku": "13",
    "skuDepPro": "0307"
  },
  "030714": {
    "name": "Curasco",
    "sku": "14",
    "skuDepPro": "0307"
  },
  "040101": {
    "name": "Arequipa",
    "sku": "01",
    "skuDepPro": "0401"
  },
  "040102": {
    "name": "Alto Selva Alegre",
    "sku": "02",
    "skuDepPro": "0401"
  },
  "040103": {
    "name": "Cayma",
    "sku": "03",
    "skuDepPro": "0401"
  },
  "040104": {
    "name": "Cerro Colorado",
    "sku": "04",
    "skuDepPro": "0401"
  },
  "040105": {
    "name": "Characato",
    "sku": "05",
    "skuDepPro": "0401"
  },
  "040106": {
    "name": "Chiguata",
    "sku": "06",
    "skuDepPro": "0401"
  },
  "040107": {
    "name": "Jacobo Hunter",
    "sku": "07",
    "skuDepPro": "0401"
  },
  "040108": {
    "name": "La Joya",
    "sku": "08",
    "skuDepPro": "0401"
  },
  "040109": {
    "name": "Mariano Melgar",
    "sku": "09",
    "skuDepPro": "0401"
  },
  "040110": {
    "name": "Miraflores",
    "sku": "10",
    "skuDepPro": "0401"
  },
  "040111": {
    "name": "Mollebaya",
    "sku": "11",
    "skuDepPro": "0401"
  },
  "040112": {
    "name": "Paucarpata",
    "sku": "12",
    "skuDepPro": "0401"
  },
  "040113": {
    "name": "Pocsi",
    "sku": "13",
    "skuDepPro": "0401"
  },
  "040114": {
    "name": "Polobaya",
    "sku": "14",
    "skuDepPro": "0401"
  },
  "040115": {
    "name": "Quequeña",
    "sku": "15",
    "skuDepPro": "0401"
  },
  "040116": {
    "name": "Sabandia",
    "sku": "16",
    "skuDepPro": "0401"
  },
  "040117": {
    "name": "Sachaca",
    "sku": "17",
    "skuDepPro": "0401"
  },
  "040118": {
    "name": "San Juan de Siguas",
    "sku": "18",
    "skuDepPro": "0401"
  },
  "040119": {
    "name": "San Juan de Tarucani",
    "sku": "19",
    "skuDepPro": "0401"
  },
  "040120": {
    "name": "Santa Isabel de Siguas",
    "sku": "20",
    "skuDepPro": "0401"
  },
  "040121": {
    "name": "Santa Rita de Siguas",
    "sku": "21",
    "skuDepPro": "0401"
  },
  "040122": {
    "name": "Socabaya",
    "sku": "22",
    "skuDepPro": "0401"
  },
  "040123": {
    "name": "Tiabaya",
    "sku": "23",
    "skuDepPro": "0401"
  },
  "040124": {
    "name": "Uchumayo",
    "sku": "24",
    "skuDepPro": "0401"
  },
  "040125": {
    "name": "Vitor",
    "sku": "25",
    "skuDepPro": "0401"
  },
  "040126": {
    "name": "Yanahuara",
    "sku": "26",
    "skuDepPro": "0401"
  },
  "040127": {
    "name": "Yarabamba",
    "sku": "27",
    "skuDepPro": "0401"
  },
  "040128": {
    "name": "Yura",
    "sku": "28",
    "skuDepPro": "0401"
  },
  "040129": {
    "name": "Jose Luis Bustamante Y Rivero",
    "sku": "29",
    "skuDepPro": "0401"
  },
  "040201": {
    "name": "Camana",
    "sku": "01",
    "skuDepPro": "0402"
  },
  "040202": {
    "name": "Jose Maria Quimper",
    "sku": "02",
    "skuDepPro": "0402"
  },
  "040203": {
    "name": "Mariano Nicolas Valcarcel",
    "sku": "03",
    "skuDepPro": "0402"
  },
  "040204": {
    "name": "Mariscal Caceres",
    "sku": "04",
    "skuDepPro": "0402"
  },
  "040205": {
    "name": "Nicolas de Pierola",
    "sku": "05",
    "skuDepPro": "0402"
  },
  "040206": {
    "name": "Ocoña",
    "sku": "06",
    "skuDepPro": "0402"
  },
  "040207": {
    "name": "Quilca",
    "sku": "07",
    "skuDepPro": "0402"
  },
  "040208": {
    "name": "Samuel Pastor",
    "sku": "08",
    "skuDepPro": "0402"
  },
  "040301": {
    "name": "Caraveli",
    "sku": "01",
    "skuDepPro": "0403"
  },
  "040302": {
    "name": "Acari",
    "sku": "02",
    "skuDepPro": "0403"
  },
  "040303": {
    "name": "Atico",
    "sku": "03",
    "skuDepPro": "0403"
  },
  "040304": {
    "name": "Atiquipa",
    "sku": "04",
    "skuDepPro": "0403"
  },
  "040305": {
    "name": "Bella Union",
    "sku": "05",
    "skuDepPro": "0403"
  },
  "040306": {
    "name": "Cahuacho",
    "sku": "06",
    "skuDepPro": "0403"
  },
  "040307": {
    "name": "Chala",
    "sku": "07",
    "skuDepPro": "0403"
  },
  "040308": {
    "name": "Chaparra",
    "sku": "08",
    "skuDepPro": "0403"
  },
  "040309": {
    "name": "Huanuhuanu",
    "sku": "09",
    "skuDepPro": "0403"
  },
  "040310": {
    "name": "Jaqui",
    "sku": "10",
    "skuDepPro": "0403"
  },
  "040311": {
    "name": "Lomas",
    "sku": "11",
    "skuDepPro": "0403"
  },
  "040312": {
    "name": "Quicacha",
    "sku": "12",
    "skuDepPro": "0403"
  },
  "040313": {
    "name": "Yauca",
    "sku": "13",
    "skuDepPro": "0403"
  },
  "040401": {
    "name": "Aplao",
    "sku": "01",
    "skuDepPro": "0404"
  },
  "040402": {
    "name": "Andagua",
    "sku": "02",
    "skuDepPro": "0404"
  },
  "040403": {
    "name": "Ayo",
    "sku": "03",
    "skuDepPro": "0404"
  },
  "040404": {
    "name": "Chachas",
    "sku": "04",
    "skuDepPro": "0404"
  },
  "040405": {
    "name": "Chilcaymarca",
    "sku": "05",
    "skuDepPro": "0404"
  },
  "040406": {
    "name": "Choco",
    "sku": "06",
    "skuDepPro": "0404"
  },
  "040407": {
    "name": "Huancarqui",
    "sku": "07",
    "skuDepPro": "0404"
  },
  "040408": {
    "name": "Machaguay",
    "sku": "08",
    "skuDepPro": "0404"
  },
  "040409": {
    "name": "Orcopampa",
    "sku": "09",
    "skuDepPro": "0404"
  },
  "040410": {
    "name": "Pampacolca",
    "sku": "10",
    "skuDepPro": "0404"
  },
  "040411": {
    "name": "Tipan",
    "sku": "11",
    "skuDepPro": "0404"
  },
  "040412": {
    "name": "Uñon",
    "sku": "12",
    "skuDepPro": "0404"
  },
  "040413": {
    "name": "Uraca",
    "sku": "13",
    "skuDepPro": "0404"
  },
  "040414": {
    "name": "Viraco",
    "sku": "14",
    "skuDepPro": "0404"
  },
  "040501": {
    "name": "Chivay",
    "sku": "01",
    "skuDepPro": "0405"
  },
  "040502": {
    "name": "Achoma",
    "sku": "02",
    "skuDepPro": "0405"
  },
  "040503": {
    "name": "Cabanaconde",
    "sku": "03",
    "skuDepPro": "0405"
  },
  "040504": {
    "name": "Callalli",
    "sku": "04",
    "skuDepPro": "0405"
  },
  "040505": {
    "name": "Caylloma",
    "sku": "05",
    "skuDepPro": "0405"
  },
  "040506": {
    "name": "Coporaque",
    "sku": "06",
    "skuDepPro": "0405"
  },
  "040507": {
    "name": "Huambo",
    "sku": "07",
    "skuDepPro": "0405"
  },
  "040508": {
    "name": "Huanca",
    "sku": "08",
    "skuDepPro": "0405"
  },
  "040509": {
    "name": "Ichupampa",
    "sku": "09",
    "skuDepPro": "0405"
  },
  "040510": {
    "name": "Lari",
    "sku": "10",
    "skuDepPro": "0405"
  },
  "040511": {
    "name": "Lluta",
    "sku": "11",
    "skuDepPro": "0405"
  },
  "040512": {
    "name": "Maca",
    "sku": "12",
    "skuDepPro": "0405"
  },
  "040513": {
    "name": "Madrigal",
    "sku": "13",
    "skuDepPro": "0405"
  },
  "040514": {
    "name": "San Antonio de Chuca",
    "sku": "14",
    "skuDepPro": "0405"
  },
  "040515": {
    "name": "Sibayo",
    "sku": "15",
    "skuDepPro": "0405"
  },
  "040516": {
    "name": "Tapay",
    "sku": "16",
    "skuDepPro": "0405"
  },
  "040517": {
    "name": "Tisco",
    "sku": "17",
    "skuDepPro": "0405"
  },
  "040518": {
    "name": "Tuti",
    "sku": "18",
    "skuDepPro": "0405"
  },
  "040519": {
    "name": "Yanque",
    "sku": "19",
    "skuDepPro": "0405"
  },
  "040520": {
    "name": "Majes",
    "sku": "20",
    "skuDepPro": "0405"
  },
  "040601": {
    "name": "Chuquibamba",
    "sku": "01",
    "skuDepPro": "0406"
  },
  "040602": {
    "name": "Andaray",
    "sku": "02",
    "skuDepPro": "0406"
  },
  "040603": {
    "name": "Cayarani",
    "sku": "03",
    "skuDepPro": "0406"
  },
  "040604": {
    "name": "Chichas",
    "sku": "04",
    "skuDepPro": "0406"
  },
  "040605": {
    "name": "Iray",
    "sku": "05",
    "skuDepPro": "0406"
  },
  "040606": {
    "name": "Rio Grande",
    "sku": "06",
    "skuDepPro": "0406"
  },
  "040607": {
    "name": "Salamanca",
    "sku": "07",
    "skuDepPro": "0406"
  },
  "040608": {
    "name": "Yanaquihua",
    "sku": "08",
    "skuDepPro": "0406"
  },
  "040701": {
    "name": "Mollendo",
    "sku": "01",
    "skuDepPro": "0407"
  },
  "040702": {
    "name": "Cocachacra",
    "sku": "02",
    "skuDepPro": "0407"
  },
  "040703": {
    "name": "Dean Valdivia",
    "sku": "03",
    "skuDepPro": "0407"
  },
  "040704": {
    "name": "Islay",
    "sku": "04",
    "skuDepPro": "0407"
  },
  "040705": {
    "name": "Mejia",
    "sku": "05",
    "skuDepPro": "0407"
  },
  "040706": {
    "name": "Punta de Bombon",
    "sku": "06",
    "skuDepPro": "0407"
  },
  "040801": {
    "name": "Cotahuasi",
    "sku": "01",
    "skuDepPro": "0408"
  },
  "040802": {
    "name": "Alca",
    "sku": "02",
    "skuDepPro": "0408"
  },
  "040803": {
    "name": "Charcana",
    "sku": "03",
    "skuDepPro": "0408"
  },
  "040804": {
    "name": "Huaynacotas",
    "sku": "04",
    "skuDepPro": "0408"
  },
  "040805": {
    "name": "Pampamarca",
    "sku": "05",
    "skuDepPro": "0408"
  },
  "040806": {
    "name": "Puyca",
    "sku": "06",
    "skuDepPro": "0408"
  },
  "040807": {
    "name": "Quechualla",
    "sku": "07",
    "skuDepPro": "0408"
  },
  "040808": {
    "name": "Sayla",
    "sku": "08",
    "skuDepPro": "0408"
  },
  "040809": {
    "name": "Tauria",
    "sku": "09",
    "skuDepPro": "0408"
  },
  "040810": {
    "name": "Tomepampa",
    "sku": "10",
    "skuDepPro": "0408"
  },
  "040811": {
    "name": "Toro",
    "sku": "11",
    "skuDepPro": "0408"
  },
  "050101": {
    "name": "Ayacucho",
    "sku": "01",
    "skuDepPro": "0501"
  },
  "050102": {
    "name": "Acocro",
    "sku": "02",
    "skuDepPro": "0501"
  },
  "050103": {
    "name": "Acos Vinchos",
    "sku": "03",
    "skuDepPro": "0501"
  },
  "050104": {
    "name": "Carmen Alto",
    "sku": "04",
    "skuDepPro": "0501"
  },
  "050105": {
    "name": "Chiara",
    "sku": "05",
    "skuDepPro": "0501"
  },
  "050106": {
    "name": "Ocros",
    "sku": "06",
    "skuDepPro": "0501"
  },
  "050107": {
    "name": "Pacaycasa",
    "sku": "07",
    "skuDepPro": "0501"
  },
  "050108": {
    "name": "Quinua",
    "sku": "08",
    "skuDepPro": "0501"
  },
  "050109": {
    "name": "San Jose de Ticllas",
    "sku": "09",
    "skuDepPro": "0501"
  },
  "050110": {
    "name": "San Juan Bautista",
    "sku": "10",
    "skuDepPro": "0501"
  },
  "050111": {
    "name": "Santiago de Pischa",
    "sku": "11",
    "skuDepPro": "0501"
  },
  "050112": {
    "name": "Socos",
    "sku": "12",
    "skuDepPro": "0501"
  },
  "050113": {
    "name": "Tambillo",
    "sku": "13",
    "skuDepPro": "0501"
  },
  "050114": {
    "name": "Vinchos",
    "sku": "14",
    "skuDepPro": "0501"
  },
  "050115": {
    "name": "Jesus Nazareno",
    "sku": "15",
    "skuDepPro": "0501"
  },
  "050116": {
    "name": "Andres Avelino Caceres Dorregaray",
    "sku": "16",
    "skuDepPro": "0501"
  },
  "050201": {
    "name": "Cangallo",
    "sku": "01",
    "skuDepPro": "0502"
  },
  "050202": {
    "name": "Chuschi",
    "sku": "02",
    "skuDepPro": "0502"
  },
  "050203": {
    "name": "Los Morochucos",
    "sku": "03",
    "skuDepPro": "0502"
  },
  "050204": {
    "name": "Maria Parado de Bellido",
    "sku": "04",
    "skuDepPro": "0502"
  },
  "050205": {
    "name": "Paras",
    "sku": "05",
    "skuDepPro": "0502"
  },
  "050206": {
    "name": "Totos",
    "sku": "06",
    "skuDepPro": "0502"
  },
  "050301": {
    "name": "Sancos",
    "sku": "01",
    "skuDepPro": "0503"
  },
  "050302": {
    "name": "Carapo",
    "sku": "02",
    "skuDepPro": "0503"
  },
  "050303": {
    "name": "Sacsamarca",
    "sku": "03",
    "skuDepPro": "0503"
  },
  "050304": {
    "name": "Santiago de Lucanamarca",
    "sku": "04",
    "skuDepPro": "0503"
  },
  "050401": {
    "name": "Huanta",
    "sku": "01",
    "skuDepPro": "0504"
  },
  "050402": {
    "name": "Ayahuanco",
    "sku": "02",
    "skuDepPro": "0504"
  },
  "050403": {
    "name": "Huamanguilla",
    "sku": "03",
    "skuDepPro": "0504"
  },
  "050404": {
    "name": "Iguain",
    "sku": "04",
    "skuDepPro": "0504"
  },
  "050405": {
    "name": "Luricocha",
    "sku": "05",
    "skuDepPro": "0504"
  },
  "050406": {
    "name": "Santillana",
    "sku": "06",
    "skuDepPro": "0504"
  },
  "050407": {
    "name": "Sivia",
    "sku": "07",
    "skuDepPro": "0504"
  },
  "050408": {
    "name": "Llochegua",
    "sku": "08",
    "skuDepPro": "0504"
  },
  "050409": {
    "name": "Canayre",
    "sku": "09",
    "skuDepPro": "0504"
  },
  "050410": {
    "name": "Uchuraccay",
    "sku": "10",
    "skuDepPro": "0504"
  },
  "050411": {
    "name": "Pucacolpa",
    "sku": "11",
    "skuDepPro": "0504"
  },
  "050412": {
    "name": "Chaca",
    "sku": "12",
    "skuDepPro": "0504"
  },
  "050501": {
    "name": "San Miguel",
    "sku": "01",
    "skuDepPro": "0505"
  },
  "050502": {
    "name": "Anco",
    "sku": "02",
    "skuDepPro": "0505"
  },
  "050503": {
    "name": "Ayna",
    "sku": "03",
    "skuDepPro": "0505"
  },
  "050504": {
    "name": "Chilcas",
    "sku": "04",
    "skuDepPro": "0505"
  },
  "050505": {
    "name": "Chungui",
    "sku": "05",
    "skuDepPro": "0505"
  },
  "050506": {
    "name": "Luis Carranza",
    "sku": "06",
    "skuDepPro": "0505"
  },
  "050507": {
    "name": "Santa Rosa",
    "sku": "07",
    "skuDepPro": "0505"
  },
  "050508": {
    "name": "Tambo",
    "sku": "08",
    "skuDepPro": "0505"
  },
  "050509": {
    "name": "Samugari",
    "sku": "09",
    "skuDepPro": "0505"
  },
  "050510": {
    "name": "Anchihuay",
    "sku": "10",
    "skuDepPro": "0505"
  },
  "050511": {
    "name": "Oronccoy",
    "sku": "11",
    "skuDepPro": "0505"
  },
  "050601": {
    "name": "Puquio",
    "sku": "01",
    "skuDepPro": "0506"
  },
  "050602": {
    "name": "Aucara",
    "sku": "02",
    "skuDepPro": "0506"
  },
  "050603": {
    "name": "Cabana",
    "sku": "03",
    "skuDepPro": "0506"
  },
  "050604": {
    "name": "Carmen Salcedo",
    "sku": "04",
    "skuDepPro": "0506"
  },
  "050605": {
    "name": "Chaviña",
    "sku": "05",
    "skuDepPro": "0506"
  },
  "050606": {
    "name": "Chipao",
    "sku": "06",
    "skuDepPro": "0506"
  },
  "050607": {
    "name": "Huac-Huas",
    "sku": "07",
    "skuDepPro": "0506"
  },
  "050608": {
    "name": "Laramate",
    "sku": "08",
    "skuDepPro": "0506"
  },
  "050609": {
    "name": "Leoncio Prado",
    "sku": "09",
    "skuDepPro": "0506"
  },
  "050610": {
    "name": "Llauta",
    "sku": "10",
    "skuDepPro": "0506"
  },
  "050611": {
    "name": "Lucanas",
    "sku": "11",
    "skuDepPro": "0506"
  },
  "050612": {
    "name": "Ocaña",
    "sku": "12",
    "skuDepPro": "0506"
  },
  "050613": {
    "name": "Otoca",
    "sku": "13",
    "skuDepPro": "0506"
  },
  "050614": {
    "name": "Saisa",
    "sku": "14",
    "skuDepPro": "0506"
  },
  "050615": {
    "name": "San Cristobal",
    "sku": "15",
    "skuDepPro": "0506"
  },
  "050616": {
    "name": "San Juan",
    "sku": "16",
    "skuDepPro": "0506"
  },
  "050617": {
    "name": "San Pedro",
    "sku": "17",
    "skuDepPro": "0506"
  },
  "050618": {
    "name": "San Pedro de Palco",
    "sku": "18",
    "skuDepPro": "0506"
  },
  "050619": {
    "name": "Sancos",
    "sku": "19",
    "skuDepPro": "0506"
  },
  "050620": {
    "name": "Santa Ana de Huaycahuacho",
    "sku": "20",
    "skuDepPro": "0506"
  },
  "050621": {
    "name": "Santa Lucia",
    "sku": "21",
    "skuDepPro": "0506"
  },
  "050701": {
    "name": "Coracora",
    "sku": "01",
    "skuDepPro": "0507"
  },
  "050702": {
    "name": "Chumpi",
    "sku": "02",
    "skuDepPro": "0507"
  },
  "050703": {
    "name": "Coronel Castañeda",
    "sku": "03",
    "skuDepPro": "0507"
  },
  "050704": {
    "name": "Pacapausa",
    "sku": "04",
    "skuDepPro": "0507"
  },
  "050705": {
    "name": "Pullo",
    "sku": "05",
    "skuDepPro": "0507"
  },
  "050706": {
    "name": "Puyusca",
    "sku": "06",
    "skuDepPro": "0507"
  },
  "050707": {
    "name": "San Francisco de Ravacayco",
    "sku": "07",
    "skuDepPro": "0507"
  },
  "050708": {
    "name": "Upahuacho",
    "sku": "08",
    "skuDepPro": "0507"
  },
  "050801": {
    "name": "Pausa",
    "sku": "01",
    "skuDepPro": "0508"
  },
  "050802": {
    "name": "Colta",
    "sku": "02",
    "skuDepPro": "0508"
  },
  "050803": {
    "name": "Corculla",
    "sku": "03",
    "skuDepPro": "0508"
  },
  "050804": {
    "name": "Lampa",
    "sku": "04",
    "skuDepPro": "0508"
  },
  "050805": {
    "name": "Marcabamba",
    "sku": "05",
    "skuDepPro": "0508"
  },
  "050806": {
    "name": "Oyolo",
    "sku": "06",
    "skuDepPro": "0508"
  },
  "050807": {
    "name": "Pararca",
    "sku": "07",
    "skuDepPro": "0508"
  },
  "050808": {
    "name": "San Javier de Alpabamba",
    "sku": "08",
    "skuDepPro": "0508"
  },
  "050809": {
    "name": "San Jose de Ushua",
    "sku": "09",
    "skuDepPro": "0508"
  },
  "050810": {
    "name": "Sara Sara",
    "sku": "10",
    "skuDepPro": "0508"
  },
  "050901": {
    "name": "Querobamba",
    "sku": "01",
    "skuDepPro": "0509"
  },
  "050902": {
    "name": "Belen",
    "sku": "02",
    "skuDepPro": "0509"
  },
  "050903": {
    "name": "Chalcos",
    "sku": "03",
    "skuDepPro": "0509"
  },
  "050904": {
    "name": "Chilcayoc",
    "sku": "04",
    "skuDepPro": "0509"
  },
  "050905": {
    "name": "Huacaña",
    "sku": "05",
    "skuDepPro": "0509"
  },
  "050906": {
    "name": "Morcolla",
    "sku": "06",
    "skuDepPro": "0509"
  },
  "050907": {
    "name": "Paico",
    "sku": "07",
    "skuDepPro": "0509"
  },
  "050908": {
    "name": "San Pedro de Larcay",
    "sku": "08",
    "skuDepPro": "0509"
  },
  "050909": {
    "name": "San Salvador de Quije",
    "sku": "09",
    "skuDepPro": "0509"
  },
  "050910": {
    "name": "Santiago de Paucaray",
    "sku": "10",
    "skuDepPro": "0509"
  },
  "050911": {
    "name": "Soras",
    "sku": "11",
    "skuDepPro": "0509"
  },
  "051001": {
    "name": "Huancapi",
    "sku": "01",
    "skuDepPro": "0510"
  },
  "051002": {
    "name": "Alcamenca",
    "sku": "02",
    "skuDepPro": "0510"
  },
  "051003": {
    "name": "Apongo",
    "sku": "03",
    "skuDepPro": "0510"
  },
  "051004": {
    "name": "Asquipata",
    "sku": "04",
    "skuDepPro": "0510"
  },
  "051005": {
    "name": "Canaria",
    "sku": "05",
    "skuDepPro": "0510"
  },
  "051006": {
    "name": "Cayara",
    "sku": "06",
    "skuDepPro": "0510"
  },
  "051007": {
    "name": "Colca",
    "sku": "07",
    "skuDepPro": "0510"
  },
  "051008": {
    "name": "Huamanquiquia",
    "sku": "08",
    "skuDepPro": "0510"
  },
  "051009": {
    "name": "Huancaraylla",
    "sku": "09",
    "skuDepPro": "0510"
  },
  "051010": {
    "name": "Huaya",
    "sku": "10",
    "skuDepPro": "0510"
  },
  "051011": {
    "name": "Sarhua",
    "sku": "11",
    "skuDepPro": "0510"
  },
  "051012": {
    "name": "Vilcanchos",
    "sku": "12",
    "skuDepPro": "0510"
  },
  "051101": {
    "name": "Vilcas Huaman",
    "sku": "01",
    "skuDepPro": "0511"
  },
  "051102": {
    "name": "Accomarca",
    "sku": "02",
    "skuDepPro": "0511"
  },
  "051103": {
    "name": "Carhuanca",
    "sku": "03",
    "skuDepPro": "0511"
  },
  "051104": {
    "name": "Concepción",
    "sku": "04",
    "skuDepPro": "0511"
  },
  "051105": {
    "name": "Huambalpa",
    "sku": "05",
    "skuDepPro": "0511"
  },
  "051106": {
    "name": "Independencia",
    "sku": "06",
    "skuDepPro": "0511"
  },
  "051107": {
    "name": "Saurama",
    "sku": "07",
    "skuDepPro": "0511"
  },
  "051108": {
    "name": "Vischongo",
    "sku": "08",
    "skuDepPro": "0511"
  },
  "060101": {
    "name": "Cajamarca",
    "sku": "01",
    "skuDepPro": "0601"
  },
  "060102": {
    "name": "Asuncion",
    "sku": "02",
    "skuDepPro": "0601"
  },
  "060103": {
    "name": "Chetilla",
    "sku": "03",
    "skuDepPro": "0601"
  },
  "060104": {
    "name": "Cospan",
    "sku": "04",
    "skuDepPro": "0601"
  },
  "060105": {
    "name": "Encañada",
    "sku": "05",
    "skuDepPro": "0601"
  },
  "060106": {
    "name": "Jesus",
    "sku": "06",
    "skuDepPro": "0601"
  },
  "060107": {
    "name": "Llacanora",
    "sku": "07",
    "skuDepPro": "0601"
  },
  "060108": {
    "name": "Los Baños del Inca",
    "sku": "08",
    "skuDepPro": "0601"
  },
  "060109": {
    "name": "Magdalena",
    "sku": "09",
    "skuDepPro": "0601"
  },
  "060110": {
    "name": "Matara",
    "sku": "10",
    "skuDepPro": "0601"
  },
  "060111": {
    "name": "Namora",
    "sku": "11",
    "skuDepPro": "0601"
  },
  "060112": {
    "name": "San Juan",
    "sku": "12",
    "skuDepPro": "0601"
  },
  "060201": {
    "name": "Cajabamba",
    "sku": "01",
    "skuDepPro": "0602"
  },
  "060202": {
    "name": "Cachachi",
    "sku": "02",
    "skuDepPro": "0602"
  },
  "060203": {
    "name": "Condebamba",
    "sku": "03",
    "skuDepPro": "0602"
  },
  "060204": {
    "name": "Sitacocha",
    "sku": "04",
    "skuDepPro": "0602"
  },
  "060301": {
    "name": "Celendin",
    "sku": "01",
    "skuDepPro": "0603"
  },
  "060302": {
    "name": "Chumuch",
    "sku": "02",
    "skuDepPro": "0603"
  },
  "060303": {
    "name": "Cortegana",
    "sku": "03",
    "skuDepPro": "0603"
  },
  "060304": {
    "name": "Huasmin",
    "sku": "04",
    "skuDepPro": "0603"
  },
  "060305": {
    "name": "Jorge Chavez",
    "sku": "05",
    "skuDepPro": "0603"
  },
  "060306": {
    "name": "Jose Galvez",
    "sku": "06",
    "skuDepPro": "0603"
  },
  "060307": {
    "name": "Miguel Iglesias",
    "sku": "07",
    "skuDepPro": "0603"
  },
  "060308": {
    "name": "Oxamarca",
    "sku": "08",
    "skuDepPro": "0603"
  },
  "060309": {
    "name": "Sorochuco",
    "sku": "09",
    "skuDepPro": "0603"
  },
  "060310": {
    "name": "Sucre",
    "sku": "10",
    "skuDepPro": "0603"
  },
  "060311": {
    "name": "Utco",
    "sku": "11",
    "skuDepPro": "0603"
  },
  "060312": {
    "name": "La Libertad de Pallan",
    "sku": "12",
    "skuDepPro": "0603"
  },
  "060401": {
    "name": "Chota",
    "sku": "01",
    "skuDepPro": "0604"
  },
  "060402": {
    "name": "Anguia",
    "sku": "02",
    "skuDepPro": "0604"
  },
  "060403": {
    "name": "Chadin",
    "sku": "03",
    "skuDepPro": "0604"
  },
  "060404": {
    "name": "Chiguirip",
    "sku": "04",
    "skuDepPro": "0604"
  },
  "060405": {
    "name": "Chimban",
    "sku": "05",
    "skuDepPro": "0604"
  },
  "060406": {
    "name": "Choropampa",
    "sku": "06",
    "skuDepPro": "0604"
  },
  "060407": {
    "name": "Cochabamba",
    "sku": "07",
    "skuDepPro": "0604"
  },
  "060408": {
    "name": "Conchan",
    "sku": "08",
    "skuDepPro": "0604"
  },
  "060409": {
    "name": "Huambos",
    "sku": "09",
    "skuDepPro": "0604"
  },
  "060410": {
    "name": "Lajas",
    "sku": "10",
    "skuDepPro": "0604"
  },
  "060411": {
    "name": "Llama",
    "sku": "11",
    "skuDepPro": "0604"
  },
  "060412": {
    "name": "Miracosta",
    "sku": "12",
    "skuDepPro": "0604"
  },
  "060413": {
    "name": "Paccha",
    "sku": "13",
    "skuDepPro": "0604"
  },
  "060414": {
    "name": "Pion",
    "sku": "14",
    "skuDepPro": "0604"
  },
  "060415": {
    "name": "Querocoto",
    "sku": "15",
    "skuDepPro": "0604"
  },
  "060416": {
    "name": "San Juan de Licupis",
    "sku": "16",
    "skuDepPro": "0604"
  },
  "060417": {
    "name": "Tacabamba",
    "sku": "17",
    "skuDepPro": "0604"
  },
  "060418": {
    "name": "Tocmoche",
    "sku": "18",
    "skuDepPro": "0604"
  },
  "060419": {
    "name": "Chalamarca",
    "sku": "19",
    "skuDepPro": "0604"
  },
  "060501": {
    "name": "Contumaza",
    "sku": "01",
    "skuDepPro": "0605"
  },
  "060502": {
    "name": "Chilete",
    "sku": "02",
    "skuDepPro": "0605"
  },
  "060503": {
    "name": "Cupisnique",
    "sku": "03",
    "skuDepPro": "0605"
  },
  "060504": {
    "name": "Guzmango",
    "sku": "04",
    "skuDepPro": "0605"
  },
  "060505": {
    "name": "San Benito",
    "sku": "05",
    "skuDepPro": "0605"
  },
  "060506": {
    "name": "Santa Cruz de Toledo",
    "sku": "06",
    "skuDepPro": "0605"
  },
  "060507": {
    "name": "Tantarica",
    "sku": "07",
    "skuDepPro": "0605"
  },
  "060508": {
    "name": "Yonan",
    "sku": "08",
    "skuDepPro": "0605"
  },
  "060601": {
    "name": "Cutervo",
    "sku": "01",
    "skuDepPro": "0606"
  },
  "060602": {
    "name": "Callayuc",
    "sku": "02",
    "skuDepPro": "0606"
  },
  "060603": {
    "name": "Choros",
    "sku": "03",
    "skuDepPro": "0606"
  },
  "060604": {
    "name": "Cujillo",
    "sku": "04",
    "skuDepPro": "0606"
  },
  "060605": {
    "name": "La Ramada",
    "sku": "05",
    "skuDepPro": "0606"
  },
  "060606": {
    "name": "Pimpingos",
    "sku": "06",
    "skuDepPro": "0606"
  },
  "060607": {
    "name": "Querocotillo",
    "sku": "07",
    "skuDepPro": "0606"
  },
  "060608": {
    "name": "San Andres de Cutervo",
    "sku": "08",
    "skuDepPro": "0606"
  },
  "060609": {
    "name": "San Juan de Cutervo",
    "sku": "09",
    "skuDepPro": "0606"
  },
  "060610": {
    "name": "San Luis de Lucma",
    "sku": "10",
    "skuDepPro": "0606"
  },
  "060611": {
    "name": "Santa Cruz",
    "sku": "11",
    "skuDepPro": "0606"
  },
  "060612": {
    "name": "Santo Domingo de la Capilla",
    "sku": "12",
    "skuDepPro": "0606"
  },
  "060613": {
    "name": "Santo Tomas",
    "sku": "13",
    "skuDepPro": "0606"
  },
  "060614": {
    "name": "Socota",
    "sku": "14",
    "skuDepPro": "0606"
  },
  "060615": {
    "name": "Toribio Casanova",
    "sku": "15",
    "skuDepPro": "0606"
  },
  "060701": {
    "name": "Bambamarca",
    "sku": "01",
    "skuDepPro": "0607"
  },
  "060702": {
    "name": "Chugur",
    "sku": "02",
    "skuDepPro": "0607"
  },
  "060703": {
    "name": "Hualgayoc",
    "sku": "03",
    "skuDepPro": "0607"
  },
  "060801": {
    "name": "Jaen",
    "sku": "01",
    "skuDepPro": "0608"
  },
  "060802": {
    "name": "Bellavista",
    "sku": "02",
    "skuDepPro": "0608"
  },
  "060803": {
    "name": "Chontali",
    "sku": "03",
    "skuDepPro": "0608"
  },
  "060804": {
    "name": "Colasay",
    "sku": "04",
    "skuDepPro": "0608"
  },
  "060805": {
    "name": "Huabal",
    "sku": "05",
    "skuDepPro": "0608"
  },
  "060806": {
    "name": "Las Pirias",
    "sku": "06",
    "skuDepPro": "0608"
  },
  "060807": {
    "name": "Pomahuaca",
    "sku": "07",
    "skuDepPro": "0608"
  },
  "060808": {
    "name": "Pucara",
    "sku": "08",
    "skuDepPro": "0608"
  },
  "060809": {
    "name": "Sallique",
    "sku": "09",
    "skuDepPro": "0608"
  },
  "060810": {
    "name": "San Felipe",
    "sku": "10",
    "skuDepPro": "0608"
  },
  "060811": {
    "name": "San Jose del Alto",
    "sku": "11",
    "skuDepPro": "0608"
  },
  "060812": {
    "name": "Santa Rosa",
    "sku": "12",
    "skuDepPro": "0608"
  },
  "060901": {
    "name": "San Ignacio",
    "sku": "01",
    "skuDepPro": "0609"
  },
  "060902": {
    "name": "Chirinos",
    "sku": "02",
    "skuDepPro": "0609"
  },
  "060903": {
    "name": "Huarango",
    "sku": "03",
    "skuDepPro": "0609"
  },
  "060904": {
    "name": "La Coipa",
    "sku": "04",
    "skuDepPro": "0609"
  },
  "060905": {
    "name": "Namballe",
    "sku": "05",
    "skuDepPro": "0609"
  },
  "060906": {
    "name": "San Jose de Lourdes",
    "sku": "06",
    "skuDepPro": "0609"
  },
  "060907": {
    "name": "Tabaconas",
    "sku": "07",
    "skuDepPro": "0609"
  },
  "061001": {
    "name": "Pedro Galvez",
    "sku": "01",
    "skuDepPro": "0610"
  },
  "061002": {
    "name": "Chancay",
    "sku": "02",
    "skuDepPro": "0610"
  },
  "061003": {
    "name": "Eduardo Villanueva",
    "sku": "03",
    "skuDepPro": "0610"
  },
  "061004": {
    "name": "Gregorio Pita",
    "sku": "04",
    "skuDepPro": "0610"
  },
  "061005": {
    "name": "Ichocan",
    "sku": "05",
    "skuDepPro": "0610"
  },
  "061006": {
    "name": "Jose Manuel Quiroz",
    "sku": "06",
    "skuDepPro": "0610"
  },
  "061007": {
    "name": "Jose Sabogal",
    "sku": "07",
    "skuDepPro": "0610"
  },
  "061101": {
    "name": "San Miguel",
    "sku": "01",
    "skuDepPro": "0611"
  },
  "061102": {
    "name": "Bolívar",
    "sku": "02",
    "skuDepPro": "0611"
  },
  "061103": {
    "name": "Calquis",
    "sku": "03",
    "skuDepPro": "0611"
  },
  "061104": {
    "name": "Catilluc",
    "sku": "04",
    "skuDepPro": "0611"
  },
  "061105": {
    "name": "El Prado",
    "sku": "05",
    "skuDepPro": "0611"
  },
  "061106": {
    "name": "La Florida",
    "sku": "06",
    "skuDepPro": "0611"
  },
  "061107": {
    "name": "Llapa",
    "sku": "07",
    "skuDepPro": "0611"
  },
  "061108": {
    "name": "Nanchoc",
    "sku": "08",
    "skuDepPro": "0611"
  },
  "061109": {
    "name": "Niepos",
    "sku": "09",
    "skuDepPro": "0611"
  },
  "061110": {
    "name": "San Gregorio",
    "sku": "10",
    "skuDepPro": "0611"
  },
  "061111": {
    "name": "San Silvestre de Cochan",
    "sku": "11",
    "skuDepPro": "0611"
  },
  "061112": {
    "name": "Tongod",
    "sku": "12",
    "skuDepPro": "0611"
  },
  "061113": {
    "name": "Union Agua Blanca",
    "sku": "13",
    "skuDepPro": "0611"
  },
  "061201": {
    "name": "San Pablo",
    "sku": "01",
    "skuDepPro": "0612"
  },
  "061202": {
    "name": "San Bernardino",
    "sku": "02",
    "skuDepPro": "0612"
  },
  "061203": {
    "name": "San Luis",
    "sku": "03",
    "skuDepPro": "0612"
  },
  "061204": {
    "name": "Tumbaden",
    "sku": "04",
    "skuDepPro": "0612"
  },
  "061301": {
    "name": "Santa Cruz",
    "sku": "01",
    "skuDepPro": "0613"
  },
  "061302": {
    "name": "Andabamba",
    "sku": "02",
    "skuDepPro": "0613"
  },
  "061303": {
    "name": "Catache",
    "sku": "03",
    "skuDepPro": "0613"
  },
  "061304": {
    "name": "Chancaybaños",
    "sku": "04",
    "skuDepPro": "0613"
  },
  "061305": {
    "name": "La Esperanza",
    "sku": "05",
    "skuDepPro": "0613"
  },
  "061306": {
    "name": "Ninabamba",
    "sku": "06",
    "skuDepPro": "0613"
  },
  "061307": {
    "name": "Pulan",
    "sku": "07",
    "skuDepPro": "0613"
  },
  "061308": {
    "name": "Saucepampa",
    "sku": "08",
    "skuDepPro": "0613"
  },
  "061309": {
    "name": "Sexi",
    "sku": "09",
    "skuDepPro": "0613"
  },
  "061310": {
    "name": "Uticyacu",
    "sku": "10",
    "skuDepPro": "0613"
  },
  "061311": {
    "name": "Yauyucan",
    "sku": "11",
    "skuDepPro": "0613"
  },
  "070101": {
    "name": "Callao",
    "sku": "01",
    "skuDepPro": "0701"
  },
  "070102": {
    "name": "Bellavista",
    "sku": "02",
    "skuDepPro": "0701"
  },
  "070103": {
    "name": "Carmen de la Legua Reynoso",
    "sku": "03",
    "skuDepPro": "0701"
  },
  "070104": {
    "name": "La Perla",
    "sku": "04",
    "skuDepPro": "0701"
  },
  "070105": {
    "name": "La Punta",
    "sku": "05",
    "skuDepPro": "0701"
  },
  "070106": {
    "name": "Ventanilla",
    "sku": "06",
    "skuDepPro": "0701"
  },
  "070107": {
    "name": "Mi Peru",
    "sku": "07",
    "skuDepPro": "0701"
  },
  "080101": {
    "name": "Cusco",
    "sku": "01",
    "skuDepPro": "0801"
  },
  "080102": {
    "name": "Ccorca",
    "sku": "02",
    "skuDepPro": "0801"
  },
  "080103": {
    "name": "Poroy",
    "sku": "03",
    "skuDepPro": "0801"
  },
  "080104": {
    "name": "San Jeronimo",
    "sku": "04",
    "skuDepPro": "0801"
  },
  "080105": {
    "name": "San Sebastian",
    "sku": "05",
    "skuDepPro": "0801"
  },
  "080106": {
    "name": "Santiago",
    "sku": "06",
    "skuDepPro": "0801"
  },
  "080107": {
    "name": "Saylla",
    "sku": "07",
    "skuDepPro": "0801"
  },
  "080108": {
    "name": "Wanchaq",
    "sku": "08",
    "skuDepPro": "0801"
  },
  "080201": {
    "name": "Acomayo",
    "sku": "01",
    "skuDepPro": "0802"
  },
  "080202": {
    "name": "Acopia",
    "sku": "02",
    "skuDepPro": "0802"
  },
  "080203": {
    "name": "Acos",
    "sku": "03",
    "skuDepPro": "0802"
  },
  "080204": {
    "name": "Mosoc Llacta",
    "sku": "04",
    "skuDepPro": "0802"
  },
  "080205": {
    "name": "Pomacanchi",
    "sku": "05",
    "skuDepPro": "0802"
  },
  "080206": {
    "name": "Rondocan",
    "sku": "06",
    "skuDepPro": "0802"
  },
  "080207": {
    "name": "Sangarara",
    "sku": "07",
    "skuDepPro": "0802"
  },
  "080301": {
    "name": "Anta",
    "sku": "01",
    "skuDepPro": "0803"
  },
  "080302": {
    "name": "Ancahuasi",
    "sku": "02",
    "skuDepPro": "0803"
  },
  "080303": {
    "name": "Cachimayo",
    "sku": "03",
    "skuDepPro": "0803"
  },
  "080304": {
    "name": "Chinchaypujio",
    "sku": "04",
    "skuDepPro": "0803"
  },
  "080305": {
    "name": "Huarocondo",
    "sku": "05",
    "skuDepPro": "0803"
  },
  "080306": {
    "name": "Limatambo",
    "sku": "06",
    "skuDepPro": "0803"
  },
  "080307": {
    "name": "Mollepata",
    "sku": "07",
    "skuDepPro": "0803"
  },
  "080308": {
    "name": "Pucyura",
    "sku": "08",
    "skuDepPro": "0803"
  },
  "080309": {
    "name": "Zurite",
    "sku": "09",
    "skuDepPro": "0803"
  },
  "080401": {
    "name": "Calca",
    "sku": "01",
    "skuDepPro": "0804"
  },
  "080402": {
    "name": "Coya",
    "sku": "02",
    "skuDepPro": "0804"
  },
  "080403": {
    "name": "Lamay",
    "sku": "03",
    "skuDepPro": "0804"
  },
  "080404": {
    "name": "Lares",
    "sku": "04",
    "skuDepPro": "0804"
  },
  "080405": {
    "name": "Pisac",
    "sku": "05",
    "skuDepPro": "0804"
  },
  "080406": {
    "name": "San Salvador",
    "sku": "06",
    "skuDepPro": "0804"
  },
  "080407": {
    "name": "Taray",
    "sku": "07",
    "skuDepPro": "0804"
  },
  "080408": {
    "name": "Yanatile",
    "sku": "08",
    "skuDepPro": "0804"
  },
  "080501": {
    "name": "Yanaoca",
    "sku": "01",
    "skuDepPro": "0805"
  },
  "080502": {
    "name": "Checca",
    "sku": "02",
    "skuDepPro": "0805"
  },
  "080503": {
    "name": "Kunturkanki",
    "sku": "03",
    "skuDepPro": "0805"
  },
  "080504": {
    "name": "Langui",
    "sku": "04",
    "skuDepPro": "0805"
  },
  "080505": {
    "name": "Layo",
    "sku": "05",
    "skuDepPro": "0805"
  },
  "080506": {
    "name": "Pampamarca",
    "sku": "06",
    "skuDepPro": "0805"
  },
  "080507": {
    "name": "Quehue",
    "sku": "07",
    "skuDepPro": "0805"
  },
  "080508": {
    "name": "Tupac Amaru",
    "sku": "08",
    "skuDepPro": "0805"
  },
  "080601": {
    "name": "Sicuani",
    "sku": "01",
    "skuDepPro": "0806"
  },
  "080602": {
    "name": "Checacupe",
    "sku": "02",
    "skuDepPro": "0806"
  },
  "080603": {
    "name": "Combapata",
    "sku": "03",
    "skuDepPro": "0806"
  },
  "080604": {
    "name": "Marangani",
    "sku": "04",
    "skuDepPro": "0806"
  },
  "080605": {
    "name": "Pitumarca",
    "sku": "05",
    "skuDepPro": "0806"
  },
  "080606": {
    "name": "San Pablo",
    "sku": "06",
    "skuDepPro": "0806"
  },
  "080607": {
    "name": "San Pedro",
    "sku": "07",
    "skuDepPro": "0806"
  },
  "080608": {
    "name": "Tinta",
    "sku": "08",
    "skuDepPro": "0806"
  },
  "080701": {
    "name": "Santo Tomas",
    "sku": "01",
    "skuDepPro": "0807"
  },
  "080702": {
    "name": "Capacmarca",
    "sku": "02",
    "skuDepPro": "0807"
  },
  "080703": {
    "name": "Chamaca",
    "sku": "03",
    "skuDepPro": "0807"
  },
  "080704": {
    "name": "Colquemarca",
    "sku": "04",
    "skuDepPro": "0807"
  },
  "080705": {
    "name": "Livitaca",
    "sku": "05",
    "skuDepPro": "0807"
  },
  "080706": {
    "name": "Llusco",
    "sku": "06",
    "skuDepPro": "0807"
  },
  "080707": {
    "name": "Quiñota",
    "sku": "07",
    "skuDepPro": "0807"
  },
  "080708": {
    "name": "Velille",
    "sku": "08",
    "skuDepPro": "0807"
  },
  "080801": {
    "name": "Espinar",
    "sku": "01",
    "skuDepPro": "0808"
  },
  "080802": {
    "name": "Condoroma",
    "sku": "02",
    "skuDepPro": "0808"
  },
  "080803": {
    "name": "Coporaque",
    "sku": "03",
    "skuDepPro": "0808"
  },
  "080804": {
    "name": "Ocoruro",
    "sku": "04",
    "skuDepPro": "0808"
  },
  "080805": {
    "name": "Pallpata",
    "sku": "05",
    "skuDepPro": "0808"
  },
  "080806": {
    "name": "Pichigua",
    "sku": "06",
    "skuDepPro": "0808"
  },
  "080807": {
    "name": "Suyckutambo",
    "sku": "07",
    "skuDepPro": "0808"
  },
  "080808": {
    "name": "Alto Pichigua",
    "sku": "08",
    "skuDepPro": "0808"
  },
  "080901": {
    "name": "Santa Ana",
    "sku": "01",
    "skuDepPro": "0809"
  },
  "080902": {
    "name": "Echarate",
    "sku": "02",
    "skuDepPro": "0809"
  },
  "080903": {
    "name": "Huayopata",
    "sku": "03",
    "skuDepPro": "0809"
  },
  "080904": {
    "name": "Maranura",
    "sku": "04",
    "skuDepPro": "0809"
  },
  "080905": {
    "name": "Ocobamba",
    "sku": "05",
    "skuDepPro": "0809"
  },
  "080906": {
    "name": "Quellouno",
    "sku": "06",
    "skuDepPro": "0809"
  },
  "080907": {
    "name": "Kimbiri",
    "sku": "07",
    "skuDepPro": "0809"
  },
  "080908": {
    "name": "Santa Teresa",
    "sku": "08",
    "skuDepPro": "0809"
  },
  "080909": {
    "name": "Vilcabamba",
    "sku": "09",
    "skuDepPro": "0809"
  },
  "080910": {
    "name": "Pichari",
    "sku": "10",
    "skuDepPro": "0809"
  },
  "080911": {
    "name": "Inkawasi",
    "sku": "11",
    "skuDepPro": "0809"
  },
  "080912": {
    "name": "Villa Virgen",
    "sku": "12",
    "skuDepPro": "0809"
  },
  "080913": {
    "name": "Villa Kintiarina",
    "sku": "13",
    "skuDepPro": "0809"
  },
  "080914": {
    "name": "Megantoni",
    "sku": "14",
    "skuDepPro": "0809"
  },
  "081001": {
    "name": "Paruro",
    "sku": "01",
    "skuDepPro": "0810"
  },
  "081002": {
    "name": "Accha",
    "sku": "02",
    "skuDepPro": "0810"
  },
  "081003": {
    "name": "Ccapi",
    "sku": "03",
    "skuDepPro": "0810"
  },
  "081004": {
    "name": "Colcha",
    "sku": "04",
    "skuDepPro": "0810"
  },
  "081005": {
    "name": "Huanoquite",
    "sku": "05",
    "skuDepPro": "0810"
  },
  "081006": {
    "name": "Omacha",
    "sku": "06",
    "skuDepPro": "0810"
  },
  "081007": {
    "name": "Paccaritambo",
    "sku": "07",
    "skuDepPro": "0810"
  },
  "081008": {
    "name": "Pillpinto",
    "sku": "08",
    "skuDepPro": "0810"
  },
  "081009": {
    "name": "Yaurisque",
    "sku": "09",
    "skuDepPro": "0810"
  },
  "081101": {
    "name": "Paucartambo",
    "sku": "01",
    "skuDepPro": "0811"
  },
  "081102": {
    "name": "Caicay",
    "sku": "02",
    "skuDepPro": "0811"
  },
  "081103": {
    "name": "Challabamba",
    "sku": "03",
    "skuDepPro": "0811"
  },
  "081104": {
    "name": "Colquepata",
    "sku": "04",
    "skuDepPro": "0811"
  },
  "081105": {
    "name": "Huancarani",
    "sku": "05",
    "skuDepPro": "0811"
  },
  "081106": {
    "name": "Kosñipata",
    "sku": "06",
    "skuDepPro": "0811"
  },
  "081201": {
    "name": "Urcos",
    "sku": "01",
    "skuDepPro": "0812"
  },
  "081202": {
    "name": "Andahuaylillas",
    "sku": "02",
    "skuDepPro": "0812"
  },
  "081203": {
    "name": "Camanti",
    "sku": "03",
    "skuDepPro": "0812"
  },
  "081204": {
    "name": "Ccarhuayo",
    "sku": "04",
    "skuDepPro": "0812"
  },
  "081205": {
    "name": "Ccatca",
    "sku": "05",
    "skuDepPro": "0812"
  },
  "081206": {
    "name": "Cusipata",
    "sku": "06",
    "skuDepPro": "0812"
  },
  "081207": {
    "name": "Huaro",
    "sku": "07",
    "skuDepPro": "0812"
  },
  "081208": {
    "name": "Lucre",
    "sku": "08",
    "skuDepPro": "0812"
  },
  "081209": {
    "name": "Marcapata",
    "sku": "09",
    "skuDepPro": "0812"
  },
  "081210": {
    "name": "Ocongate",
    "sku": "10",
    "skuDepPro": "0812"
  },
  "081211": {
    "name": "Oropesa",
    "sku": "11",
    "skuDepPro": "0812"
  },
  "081212": {
    "name": "Quiquijana",
    "sku": "12",
    "skuDepPro": "0812"
  },
  "081301": {
    "name": "Urubamba",
    "sku": "01",
    "skuDepPro": "0813"
  },
  "081302": {
    "name": "Chinchero",
    "sku": "02",
    "skuDepPro": "0813"
  },
  "081303": {
    "name": "Huayllabamba",
    "sku": "03",
    "skuDepPro": "0813"
  },
  "081304": {
    "name": "Machupicchu",
    "sku": "04",
    "skuDepPro": "0813"
  },
  "081305": {
    "name": "Maras",
    "sku": "05",
    "skuDepPro": "0813"
  },
  "081306": {
    "name": "Ollantaytambo",
    "sku": "06",
    "skuDepPro": "0813"
  },
  "081307": {
    "name": "Yucay",
    "sku": "07",
    "skuDepPro": "0813"
  },
  "090101": {
    "name": "Huancavelica",
    "sku": "01",
    "skuDepPro": "0901"
  },
  "090102": {
    "name": "Acobambilla",
    "sku": "02",
    "skuDepPro": "0901"
  },
  "090103": {
    "name": "Acoria",
    "sku": "03",
    "skuDepPro": "0901"
  },
  "090104": {
    "name": "Conayca",
    "sku": "04",
    "skuDepPro": "0901"
  },
  "090105": {
    "name": "Cuenca",
    "sku": "05",
    "skuDepPro": "0901"
  },
  "090106": {
    "name": "Huachocolpa",
    "sku": "06",
    "skuDepPro": "0901"
  },
  "090107": {
    "name": "Huayllahuara",
    "sku": "07",
    "skuDepPro": "0901"
  },
  "090108": {
    "name": "Izcuchaca",
    "sku": "08",
    "skuDepPro": "0901"
  },
  "090109": {
    "name": "Laria",
    "sku": "09",
    "skuDepPro": "0901"
  },
  "090110": {
    "name": "Manta",
    "sku": "10",
    "skuDepPro": "0901"
  },
  "090111": {
    "name": "Mariscal Caceres",
    "sku": "11",
    "skuDepPro": "0901"
  },
  "090112": {
    "name": "Moya",
    "sku": "12",
    "skuDepPro": "0901"
  },
  "090113": {
    "name": "Nuevo Occoro",
    "sku": "13",
    "skuDepPro": "0901"
  },
  "090114": {
    "name": "Palca",
    "sku": "14",
    "skuDepPro": "0901"
  },
  "090115": {
    "name": "Pilchaca",
    "sku": "15",
    "skuDepPro": "0901"
  },
  "090116": {
    "name": "Vilca",
    "sku": "16",
    "skuDepPro": "0901"
  },
  "090117": {
    "name": "Yauli",
    "sku": "17",
    "skuDepPro": "0901"
  },
  "090118": {
    "name": "Ascension",
    "sku": "18",
    "skuDepPro": "0901"
  },
  "090119": {
    "name": "Huando",
    "sku": "19",
    "skuDepPro": "0901"
  },
  "090201": {
    "name": "Acobamba",
    "sku": "01",
    "skuDepPro": "0902"
  },
  "090202": {
    "name": "Andabamba",
    "sku": "02",
    "skuDepPro": "0902"
  },
  "090203": {
    "name": "Anta",
    "sku": "03",
    "skuDepPro": "0902"
  },
  "090204": {
    "name": "Caja",
    "sku": "04",
    "skuDepPro": "0902"
  },
  "090205": {
    "name": "Marcas",
    "sku": "05",
    "skuDepPro": "0902"
  },
  "090206": {
    "name": "Paucara",
    "sku": "06",
    "skuDepPro": "0902"
  },
  "090207": {
    "name": "Pomacocha",
    "sku": "07",
    "skuDepPro": "0902"
  },
  "090208": {
    "name": "Rosario",
    "sku": "08",
    "skuDepPro": "0902"
  },
  "090301": {
    "name": "Lircay",
    "sku": "01",
    "skuDepPro": "0903"
  },
  "090302": {
    "name": "Anchonga",
    "sku": "02",
    "skuDepPro": "0903"
  },
  "090303": {
    "name": "Callanmarca",
    "sku": "03",
    "skuDepPro": "0903"
  },
  "090304": {
    "name": "Ccochaccasa",
    "sku": "04",
    "skuDepPro": "0903"
  },
  "090305": {
    "name": "Chincho",
    "sku": "05",
    "skuDepPro": "0903"
  },
  "090306": {
    "name": "Congalla",
    "sku": "06",
    "skuDepPro": "0903"
  },
  "090307": {
    "name": "Huanca-Huanca",
    "sku": "07",
    "skuDepPro": "0903"
  },
  "090308": {
    "name": "Huayllay Grande",
    "sku": "08",
    "skuDepPro": "0903"
  },
  "090309": {
    "name": "Julcamarca",
    "sku": "09",
    "skuDepPro": "0903"
  },
  "090310": {
    "name": "San Antonio de Antaparco",
    "sku": "10",
    "skuDepPro": "0903"
  },
  "090311": {
    "name": "Santo Tomas de Pata",
    "sku": "11",
    "skuDepPro": "0903"
  },
  "090312": {
    "name": "Secclla",
    "sku": "12",
    "skuDepPro": "0903"
  },
  "090401": {
    "name": "Castrovirreyna",
    "sku": "01",
    "skuDepPro": "0904"
  },
  "090402": {
    "name": "Arma",
    "sku": "02",
    "skuDepPro": "0904"
  },
  "090403": {
    "name": "Aurahua",
    "sku": "03",
    "skuDepPro": "0904"
  },
  "090404": {
    "name": "Capillas",
    "sku": "04",
    "skuDepPro": "0904"
  },
  "090405": {
    "name": "Chupamarca",
    "sku": "05",
    "skuDepPro": "0904"
  },
  "090406": {
    "name": "Cocas",
    "sku": "06",
    "skuDepPro": "0904"
  },
  "090407": {
    "name": "Huachos",
    "sku": "07",
    "skuDepPro": "0904"
  },
  "090408": {
    "name": "Huamatambo",
    "sku": "08",
    "skuDepPro": "0904"
  },
  "090409": {
    "name": "Mollepampa",
    "sku": "09",
    "skuDepPro": "0904"
  },
  "090410": {
    "name": "San Juan",
    "sku": "10",
    "skuDepPro": "0904"
  },
  "090411": {
    "name": "Santa Ana",
    "sku": "11",
    "skuDepPro": "0904"
  },
  "090412": {
    "name": "Tantara",
    "sku": "12",
    "skuDepPro": "0904"
  },
  "090413": {
    "name": "Ticrapo",
    "sku": "13",
    "skuDepPro": "0904"
  },
  "090501": {
    "name": "Churcampa",
    "sku": "01",
    "skuDepPro": "0905"
  },
  "090502": {
    "name": "Anco",
    "sku": "02",
    "skuDepPro": "0905"
  },
  "090503": {
    "name": "Chinchihuasi",
    "sku": "03",
    "skuDepPro": "0905"
  },
  "090504": {
    "name": "El Carmen",
    "sku": "04",
    "skuDepPro": "0905"
  },
  "090505": {
    "name": "La Merced",
    "sku": "05",
    "skuDepPro": "0905"
  },
  "090506": {
    "name": "Locroja",
    "sku": "06",
    "skuDepPro": "0905"
  },
  "090507": {
    "name": "Paucarbamba",
    "sku": "07",
    "skuDepPro": "0905"
  },
  "090508": {
    "name": "San Miguel de Mayocc",
    "sku": "08",
    "skuDepPro": "0905"
  },
  "090509": {
    "name": "San Pedro de Coris",
    "sku": "09",
    "skuDepPro": "0905"
  },
  "090510": {
    "name": "Pachamarca",
    "sku": "10",
    "skuDepPro": "0905"
  },
  "090511": {
    "name": "Cosme",
    "sku": "11",
    "skuDepPro": "0905"
  },
  "090601": {
    "name": "Huaytara",
    "sku": "01",
    "skuDepPro": "0906"
  },
  "090602": {
    "name": "Ayavi",
    "sku": "02",
    "skuDepPro": "0906"
  },
  "090603": {
    "name": "Cordova",
    "sku": "03",
    "skuDepPro": "0906"
  },
  "090604": {
    "name": "Huayacundo Arma",
    "sku": "04",
    "skuDepPro": "0906"
  },
  "090605": {
    "name": "Laramarca",
    "sku": "05",
    "skuDepPro": "0906"
  },
  "090606": {
    "name": "Ocoyo",
    "sku": "06",
    "skuDepPro": "0906"
  },
  "090607": {
    "name": "Pilpichaca",
    "sku": "07",
    "skuDepPro": "0906"
  },
  "090608": {
    "name": "Querco",
    "sku": "08",
    "skuDepPro": "0906"
  },
  "090609": {
    "name": "Quito-Arma",
    "sku": "09",
    "skuDepPro": "0906"
  },
  "090610": {
    "name": "San Antonio de Cusicancha",
    "sku": "10",
    "skuDepPro": "0906"
  },
  "090611": {
    "name": "San Francisco de Sangayaico",
    "sku": "11",
    "skuDepPro": "0906"
  },
  "090612": {
    "name": "San Isidro",
    "sku": "12",
    "skuDepPro": "0906"
  },
  "090613": {
    "name": "Santiago de Chocorvos",
    "sku": "13",
    "skuDepPro": "0906"
  },
  "090614": {
    "name": "Santiago de Quirahuara",
    "sku": "14",
    "skuDepPro": "0906"
  },
  "090615": {
    "name": "Santo Domingo de Capillas",
    "sku": "15",
    "skuDepPro": "0906"
  },
  "090616": {
    "name": "Tambo",
    "sku": "16",
    "skuDepPro": "0906"
  },
  "090701": {
    "name": "Pampas",
    "sku": "01",
    "skuDepPro": "0907"
  },
  "090702": {
    "name": "Acostambo",
    "sku": "02",
    "skuDepPro": "0907"
  },
  "090703": {
    "name": "Acraquia",
    "sku": "03",
    "skuDepPro": "0907"
  },
  "090704": {
    "name": "Ahuaycha",
    "sku": "04",
    "skuDepPro": "0907"
  },
  "090705": {
    "name": "Colcabamba",
    "sku": "05",
    "skuDepPro": "0907"
  },
  "090706": {
    "name": "Daniel Hernandez",
    "sku": "06",
    "skuDepPro": "0907"
  },
  "090707": {
    "name": "Huachocolpa",
    "sku": "07",
    "skuDepPro": "0907"
  },
  "090709": {
    "name": "Huaribamba",
    "sku": "09",
    "skuDepPro": "0907"
  },
  "090710": {
    "name": "ñahuimpuquio",
    "sku": "10",
    "skuDepPro": "0907"
  },
  "090711": {
    "name": "Pazos",
    "sku": "11",
    "skuDepPro": "0907"
  },
  "090713": {
    "name": "Quishuar",
    "sku": "13",
    "skuDepPro": "0907"
  },
  "090714": {
    "name": "Salcabamba",
    "sku": "14",
    "skuDepPro": "0907"
  },
  "090715": {
    "name": "Salcahuasi",
    "sku": "15",
    "skuDepPro": "0907"
  },
  "090716": {
    "name": "San Marcos de Rocchac",
    "sku": "16",
    "skuDepPro": "0907"
  },
  "090717": {
    "name": "Surcubamba",
    "sku": "17",
    "skuDepPro": "0907"
  },
  "090718": {
    "name": "Tintay Puncu",
    "sku": "18",
    "skuDepPro": "0907"
  },
  "090719": {
    "name": "Quichuas",
    "sku": "19",
    "skuDepPro": "0907"
  },
  "090720": {
    "name": "Andaymarca",
    "sku": "20",
    "skuDepPro": "0907"
  },
  "090721": {
    "name": "Roble",
    "sku": "21",
    "skuDepPro": "0907"
  },
  "090722": {
    "name": "Pichos",
    "sku": "22",
    "skuDepPro": "0907"
  },
  "090723": {
    "name": "Santiago de Tucuma",
    "sku": "23",
    "skuDepPro": "0907"
  }
};
