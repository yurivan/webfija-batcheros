import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Upload } from '../model/upload.model'
import * as globals from './globals';

@Injectable()
export class OrderService {

	baseUrl = globals.ORDER_BASE_URL + "order";

	constructor(private http: HttpService) {
		this.http = http;
	}

	// Sprint 7
	actualizarEstado(webOrder: any) {
		let url = this.baseUrl + '/web/savepending';
		let body = JSON.stringify(webOrder);
		return this.http.post(url, body)
			.map(res => res.json());
	}

	cerrarVenta(webOrder: any) {
		let url = this.baseUrl + '/web/save';

		let body = JSON.stringify(webOrder);

		return this.http.post(url, body)
			.map(res => res.json());
	}

	uploadAudio(orderId: string, file: File) {
		let url = this.baseUrl + '/web/upload';
		// let url = 'http://localhost:8181/order/web/upload';
		let upload = new Upload();
		upload.requestId = new Date().getTime();
		upload.params = { 'orderId': orderId };
		upload.files = { file: file };
		//this.http.upload(url, upload); // Sprint 3 - ahora se retorna el objeto
		return this.http.upload(url, upload);
	}

	uploadAudioMasivo(file: File[]) {
		let url = this.baseUrl + '/web/uploadmasivo';
		let upload = new Upload();
		upload.requestId = new Date().getTime();
		//upload.params = {'orderId': '0'};
		upload.files = { file: file };
		return this.http.upload(url, upload);
	}

	cancelSale(webOrder: any) {
		let url = this.baseUrl + '/web/cancel';
		let body = JSON.stringify(webOrder);
		return this.http.post(url, body)
			.map(res => res.json());
	}

	responseCancelList() {
		return globals.CANCEL_SALE.reason;
	}
}
