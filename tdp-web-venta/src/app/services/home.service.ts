import { Injectable } from '@angular/core';
import { Http,Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';

@Injectable()
export class HomeService {
	baseUrl = globals.ORDER_BASE_URL+"order/salesReport";
  userId_data : string;

  name: string;
  constructor(private http: HttpService){
		this.http = http;
    this.userId_data = "";
	}

	getData(numDias){
    let order = JSON.parse(localStorage.getItem('order'));

		if (order && order.user) {
			this.userId_data = order.user.userId;
		}
    let data = '{"vendorId":"'+this.userId_data+'","numDays":'+numDias+'}';
		return this.http.post(this.baseUrl+'/count',data).map(res => res.json());


 /*return [
    {
      "status": "EFECTIVO",
      "quantity": 16
    },
    {
      "status": "NO EFECTIVO",
      "quantity": 16
    },
    {
      "status": "PRE EFECTIVO",
      "quantity": 1
    }
  ];*/

     }
}
