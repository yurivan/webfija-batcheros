import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/Rx';
import { HttpService } from './http.service';
import { LocalStorageService } from './ls.service';
//import { UploadService } from './upload.service';


import * as globals from './globals';
var $ = require('jquery');



import { Router, Event, NavigationEnd } from '@angular/router';
import { Order } from '../model/order';



@Injectable()
export class ProgressBarService {
	progressBarPercentage: string
	isTrue: boolean;
	isHere: any;
	model;
	showModal: boolean;
	showActualProduct: boolean;
	showOffering: boolean;
	showSVA: boolean;
	showContract: boolean;
	type: string;
	order: Order;
	showClientData: boolean;//glazaror sprint2 se adiciona para ocultar/mostrar cajas de texto telefono, celular y correo cuando se trata de nuevo cliente
	
	constructor(private router: Router) {
	}

	getProgressStatus() {
		this.order = JSON.parse(localStorage.getItem('order'));
		//console.log("Works");
		$(document).ready(function () {
			$('.input_tel').keypress(function (tecla) {
				if (tecla.charCode < 48 || tecla.charCode > 57) return false;
			});
		});

		// router
		this.router.events.subscribe((event: Event) => {
			if (event instanceof NavigationEnd) {
				$("#myModal").hide();
				if (event.url == "/acciones" || event.url == "/inicioOut") {
					this.isTrue = true;
					$("#pagina_anterior").show();
					$("#reportess").prop("style", "display: block !important;cursor: pointer !important;")
					$("#bandeja").show() 
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 0%");
					$("#pop").fadeOut(350);
					// $("#cerrar_venta").addClass("none");
					$("#pagina_anterior").addClass("none");
					$("#myBtn").addClass("none");
					// $("#Venderr").addClass("none")
				
					$("#reportess").removeClass("none")
					// $("#Venderr").attr("href","/acciones")
					 
					
				}
				else if (event.url == "/contacto" || event.url == "/ofertasBiOut") {
					this.isTrue = true;
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 9%");
					document.getElementById("parrafo").innerHTML = "9%";
					// $("#cerrar_venta").removeClass("none");
					var bandeja_audio = document.getElementById("bandeja_audio");
					//$("#bandeja_audio").removeAttr("href")
					
					$(".patras > #pagina_anterior").removeClass("none");
					$("#Venderr").removeClass("none")
					$(".patras").removeClass("none");

					$("ul > #myBtn").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})
				// $("#Venderr").removeClass("none");

					 
				  
				}
				else if (event.url == "/searchUser" || event.url == "/scoringOut") {
					this.isTrue = true;
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 18%");
					//$(".parrafo").text("style idth: 18%");
					document.getElementById("parrafo").innerHTML = "18%";
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");
					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})
					
					$("ul > #myBtn").removeClass("none");

					
					$("#Venderr").removeAttr("href")



				}
				else if (event.url == "/direccion" || event.url == "/direccionOut") {
					this.isTrue = true;
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 27%");
					document.getElementById("parrafo").innerHTML = "27%";
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");

					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})
					
				}
				else if (event.url == "/campanas" || event.url == "/campanasOut") {
					this.isTrue = true;
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 32%");
					document.getElementById("parrafo").innerHTML = "32%";
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");

					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})

				 
					
				}
				else if (event.url == "/offering" || event.url == "/offeringOut") {
					this.isTrue = true;
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 36%");
					document.getElementById("parrafo").innerHTML = "36%";
					    
					var bandeja_audio = document.getElementById("bandeja_audio");
					//$("#bandeja_audio").removeAttr("href")
					
					$("#Venderr").removeAttr("href")
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");
					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})

					$("ul > #myBtn").removeClass("none");
					

				}
				else if (event.url == "/saleProcess/park") {
					this.isTrue = true;
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 30%");
					document.getElementById("parrafo").innerHTML = "30%";
					//$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");
					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})
					var bandeja_audio = document.getElementById("bandeja_audio");
					//$("#bandeja_audio").removeAttr("href")
					
					
					$("ul > #myBtn").removeClass("none");
					
				}
				else if (event.url == "/sva" || event.url == "/svaOut") {
					this.isTrue = true;
					var bandeja_audio = document.getElementById("bandeja_audio");
					$("#bandeja_audio").removeAttr("href")
					
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 45%");
					document.getElementById("parrafo").innerHTML = "45%";
					$(".patras > #pagina_anterior").removeClass("none");
					$(".input-effect input").val("");
					$(".input-effect input").focusout(function () {
						if ($(this).val() != "") {
							$(this).addClass("has-content");
						} else {
							$(this).removeClass("has-content");
						}

					})
					//efecto label select		
					$(".paraselect select").val("");
					$(".paraselect select").focusout(function () {
						if ($(this).val() != "") {
							$(this).addClass("has-content");
						} else {
							$(this).removeClass("has-content");
						}


					})
					
					var bandeja_audio = document.getElementById("bandeja_audio");
					$("#bandeja_audio").removeAttr("href")
					
					$("#Venderr").removeAttr("href")
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})

					$("ul > #myBtn").removeClass("none");
					
				}
				else if (event.url == "/saleProcess/salecondition" || event.url == "/condicionOut") {
					this.isTrue = true;
					var bandeja_audio = document.getElementById("bandeja_audio");
					$("#bandeja_audio").removeAttr("href")
					
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 63%");
					document.getElementById("parrafo").innerHTML = "63%";
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");

					$(".paraselect select").focusout(function () {
						if ($(this).val() != "") {
							$(this).addClass("has-content");
						} else {
							$(this).removeClass("has-content");
						}

					})
					
					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})

					$("ul > #myBtn").removeClass("none");
					
				}
				else if (event.url == "/saleProcess/reniec" || event.url == "/validacionOut" || event.url == "/saleProcess/searchRuc" || event.url == "/saleProcess/datoscliente") { //Sprint 24 RUC
					this.isTrue = true;
					var bandeja_audio = document.getElementById("bandeja_audio");
					$("#bandeja_audio").removeAttr("href")
					
					
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					if(this.order.user.sellerChannelEquivalentCampaign == 'IN' || this.order.user.sellerChannelEquivalentCampaign == 'CROSS'){
						$("#porcentaje").prop("style", "width: 18%");
						document.getElementById("parrafo").innerHTML = "18%";
					}else{
					$("#porcentaje").prop("style", "width: 65%");
					document.getElementById("parrafo").innerHTML = "65%";
					}
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");
					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})

					$("ul > #myBtn").removeClass("none");
					
				}
				else if (event.url == "/saleProcess/salesSummary" || event.url == "/resumenOut") {
					this.isTrue = true;
					var bandeja_audio = document.getElementById("bandeja_audio");
					$("#bandeja_audio").removeAttr("href")
					
					
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 72%");
					document.getElementById("parrafo").innerHTML = "72%";
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");
					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})

					$("ul > #myBtn").removeClass("none");
					
				}
				else if (event.url == "/saleProcess/contract" || event.url == "/contratosOut") {
					this.isTrue = true;
					var bandeja_audio = document.getElementById("bandeja_audio");
					$("#bandeja_audio").removeAttr("href")
					
					
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 87%");
					document.getElementById("parrafo").innerHTML = "87%";
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");
					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})

					$("ul > #myBtn").removeClass("none");
					
				}
				else if (event.url == "/contactoinstalacion" || event.url == "/contactoinstalacionOut") {
					this.isTrue = true;
					var bandeja_audio = document.getElementById("bandeja_audio");
					$("#bandeja_audio").removeAttr("href")
					
					
					//console.log("------------validación por url-----------")
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 95%");
					document.getElementById("parrafo").innerHTML = "95%";
					$("#cerrar_venta").removeClass("none");
					$(".patras").removeClass("none");
					$(".patras > #pagina_anterior").removeClass("none");
					$("#reportes").click(() => {
						$("#myModal").show();
					})

					$("ul > #myBtn").removeClass("none");
					
				}
				else if (event.url == "/saleProcess/close" || event.url == "/closeOut") {
					this.isTrue = true;
					var bandeja_audio = document.getElementById("bandeja_audio");
					//$("#bandeja_audio").removeAttr("href")
					
					//hhhh
					$("#Venderr").attr("href", "/acciones")
			 
					//$("#bandeja_audio").attr("href", "/audiocarga")
					//$("#cierrate").attr("href", "/login");
					//$("#cierrate").click(function () {
						// localStorage.removeItem('token');
						// localStorage.removeItem('refresh_token');
						// localStorage.removeItem('user');
						// localStorage.removeItem('order');
					//});


					//console.log("------------validación por url-----------")
					$("#pagina_anterior").hide();
					$(".noen").prop("style", "visibility: visible !important;");
					$("#porcentaje").prop("style", "width: 100%");
					document.getElementById("parrafo").innerHTML = "100%";
					$("#cerrar_venta").removeClass("none");
					// $(".patras").removeClass("none"); 
					$("ul > #myBtn").addClass("none");


					// $(function () {
					// 	//----- OPEN
					// 	$('[data-popup-open-logout]').on('click', function (e) {
					// 		$("#motivo_2").val("");
					// 		$('[data-popup=popup-2]').fadeIn(350);
					// 		e.preventDefault();
					// 	});
					// 	//----- CLOSE
					// 	$('[data-popup-close-logout').on('click', function (e) {
					// 		$('[data-popup=popup-2]').fadeOut(350);
					// 		e.preventDefault();
					// 	});//----- OPEN

					// });
				}
				else if (event.url == "/login") {
					$("#myModal").prop("style", "visibility: display: none !important; z-index: 500 !important;")
					$("#pop_close_session").modal("hide")
					//console.log("------------validación login por url-----------")
					// $(".noen").prop("style", "visibility: visible !important;");
					$(".progres").prop("style", "visibility: hidden !important;");
					// $(".noen").addClass("ssss");
					// $(".patras").addClass("none");
					// $("#progress_bar_main").addClass("none");
					$(".patras > #pagina_anterior").addClass("none");

				} else if (event.url == "/register") {

					$(".patras").addClass("none");
					// $("#progress_bar_main").addClass("none");
					// $("#progress_bar_main").prop("style", "visibility:invisible !important;");

				}
				else if (event.url == "/reset") {

					$(".patras").addClass("none");
					// $("#progress_bar_main").addClass("none");
					// $("#progress_bar_main").prop("style", "visibility:invisible !important;");
					$(".progres").prop("style", "visibility: hidden !important;");
				} else if (event.url == "/home") {
					// $("#myModal").prop("style","visibility: hidden !important")

					// $("#myModal").hide()
					/*$("#Venderr").click(() => {
						this.router.navigate(["/acciones"])
					}) */
					/*$("#Venderr").click(() => {
						$('#cancel').modal('hide')
					})*/
					$("#bandeja").show()
					$("#reportess").prop("style", "display: none !important;")
					$("#porcentaje").prop("style", "width: 0%");
					$("#parrafo").text("");
					//$("#reportess").attr("href", "/home")
					//
						//;

					//$("#reportess").addClass("none")

						  /*$("#cierrate").attr("href", "/login");
						  
							$("#cierrate").click(function() {
								localStorage.removeItem('token');
								localStorage.removeItem('refresh_token');
								localStorage.removeItem('user');
								localStorage.removeItem('order');
							});*/
					// $("#myBtn").prop("style", "display: none !important;");
					// $(".sesion_logout").prop("style", "display: none !important;");

					// $("#progress_bar_main").addClass("none");
					// $("#progress_bar_main").prop("style", "visibility:invisible !important;");
					// $(".btn").prop("style", "visibility: hidden !important;");

				}
				else if (event.url == "/audiocarga") {
					console.log("audiocarga");
					$("#Venderr").prop("style", "display: block !important")
					$("#bandeja").hide()
					$("#Venderr").attr("href", "/acciones")
					//$("#reportess").attr("href", "/home")
					//$("#bandeja_audio").attr("href", "/audiocarga")
				}
				else if (event.url == "/audiooutdetalle") {
					this.isTrue = true;
					// var bandeja_audio = document.getElementById("bandeja_audio");
					// $("#bandeja_audio").removeAttr("href")
					//console.log("------------validación por url-----------")
					// $("#parrafo").text("25%");

					// $(".porcentaje").prop("style", "width: 100%");

				}
			 
				else {
					$(".patras").addClass("none");

					// $("#progress_bar_main").addClass("none");
					// $("#progress_bar_main").prop("style", "visibility:invisible !important;");
					//(".noen").prop("style", "visibility: visible !important;");
					// $(".progres").prop("style", "visibility: hidden !important;");

				}

				// canal OUT
				


			}
		})



	}
}