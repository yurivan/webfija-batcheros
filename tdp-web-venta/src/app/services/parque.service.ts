import { Injectable } from '@angular/core';
import { Http,Headers, RequestOptions } from '@angular/http';
import { Customer } from '../model/customer';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';

@Injectable()
export class ParqueService {
	baseUrl = globals.CUSTOMER_BASE_URL+"customer";

	constructor(private http: HttpService){
		this.http = http;
	}

//	getData(customer : Customer){
//    let data = '{"documentType":'+ '"' +customer.documentType+ '","documentNumber":'+ '"' +customer.documentNumber+'"}';
//		return this.http.post(this.baseUrl + '/services',data).map(res => res.json());
//    }
	
	getData(customer : Customer, orderId: string){
       let data = '{"documentType":'+ '"' +customer.documentType+ '","documentNumber":'+ '"' +customer.documentNumber+'", "id":'+'"'+orderId+'"}';
           return this.http.post(this.baseUrl + '/web/services',data).map(res => res.json());
	}
}
