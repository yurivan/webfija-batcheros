import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Upload } from '../model/upload.model'
import * as globals from './globals';

@Injectable()
export class OutCallService {

	baseUrl = globals.ORDER_BASE_URL + "outcall";

	constructor(private http: HttpService) {
		this.http = http;
    }
    
    getListAudio(codatis: string, fechaInicio: string, fechaFin: string) {
        let request = {
            codigoVendedor: codatis,
            fechaInicio: fechaInicio,
            fechaFin: fechaFin
        };
        let body = JSON.stringify(request);

        return this.http.post(this.baseUrl+ '/bandejaVentaArchivos', body)
            .map(res => res.json());
    }
}
