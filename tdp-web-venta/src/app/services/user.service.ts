import { BehaviorSubject }     from 'rxjs/BehaviorSubject';
import { Injectable }  from '@angular/core';
import { HttpService } from './http.service';
import * as globals from './globals';
import { Order } from '../model/order';
import { User } from '../model/user';
import { Subscription }   from 'rxjs/Subscription';

@Injectable()
export class UserService {
	private userChangedSource = new BehaviorSubject<number>(0);
	userChanged$ = this.userChangedSource.asObservable();
	baseUrl = globals.USER_BASE_URL + 'user/web';
	counter = 0;
	userId : string;
	constructor(private http: HttpService){
		this.http._eventBus.subscribe(x => {
			if (x.reason == 'security') {
				let order= JSON.parse(localStorage.getItem('order'));
				order.user = new User();
				localStorage.setItem('order', JSON.stringify(order));
				this.userChangedSource.next(this.counter++);
			}
		});
	}
	
	//glazaror
	/*userChangedSourceNext(dataUser) {
		let order= JSON.parse(localStorage.getItem('order'));
		if(!order){
			order = new Order();
		}
		let userId = null;
		if (order.user) {
			userId = order.user.userId;
		}
		order.user = dataUser.responseData.responseData.user;
		//console.log(data.responseData.parameter);
		globals.parameter(dataUser.responseData.responseData.parameter);
		order.user.userId= userId;
		localStorage.setItem('order', JSON.stringify(order));
		
		this.userChangedSource.next(this.counter++);
	}*/

	checkUser() {
		//let url = this.baseUrl + '/info';
		let url = this.baseUrl + '/infodetail';
		let data = '{"tipoAplicacion":"WEB"}';
		
		return this.http.post(url, data)
		.map(response => response.json())
		.toPromise()
		.then(data => {
			let order= JSON.parse(localStorage.getItem('order'));
			if(!order){
				order = new Order();
			}
			let userId = null;
			if (order.user) {
				userId = order.user.userId;
			}
			order.user = data.responseData.user;
			globals.parameter(data.responseData.parameter);
			order.user.userId= userId;
			localStorage.setItem('order', JSON.stringify(order));
			this.userChangedSource.next(this.counter++);
		})
		.catch(err => {
			localStorage.removeItem('order');
			localStorage.removeItem('token');
			localStorage.removeItem('refresh_token');
		});
	}

	register(tipoDoc:string, dni:string, codAtis:string, pwd:string) {
		let url = this.baseUrl + '/register';
		let body = JSON.stringify({
			tipoDoc:tipoDoc,
			codAtis:codAtis,
			dni:dni,
			password:pwd
		});
		return this.http.post(url, body)
				.map(res => res.json());
	}
	
	//glazaror reseteo contraseña
	reset(codAtis:string, pwd:string) {
		let url = this.baseUrl + '/reset';
		let body = JSON.stringify({
			newPwd:pwd,
			codAtis:codAtis
		});
		return this.http.post(url, body)
				.map(res => res.json());
	}

	changePassword(codAtis, password, newPassword) {
		let url = this.baseUrl + '/changePassword';

		let bodyData = '{"codAtis":"' + codAtis + '","pwd":"' + password + '","repeatPwd":"' + password + '"}';

		return this.http.post(url, bodyData)
			.map(response => {
				let data = response.json();

				return data;
			});
	}

	getToken(){
		let url = this.baseUrl + '/generarToken';
		const user = JSON.parse(localStorage.getItem('order'))['user']
		//console.log(user)
		let body = JSON.stringify({
			tipdoc: ((user.document.length==8)?'1':'2'),
			numero_de_doc: user.document,
    		codatis: user.userId,
			canal_de_venta: user.channel,
			canalequivcampania: user.sellerChannelEquivalentCampaign,
			entidad: user.entity,
    		punto_de_venta: user.group,
    		asesor_nombre: user.name+' '+user.lastName
		});
		return this.http.post(url, body).map(res => res.json());
	}

	desencriptarToken(token){
		let url = this.baseUrl + '/desencriptarToken';
		let body = JSON.stringify({	
			token: token
		});
		return this.http.post(url, body).map(res => res.json());
	}

	niveles(nivel, canal){
		let url = this.baseUrl+'/getAccessByNivel';
        
		let bodyData = '{"nivel":"'+nivel+'", "canal":"'+canal+'"}';
		 
		 return this.http.post(url, bodyData)
		 .map(response => {
			 let data = response.json();
			 
			 return data;
		 });
	}

	loginConvergencia(pto_venta){
		let url = this.baseUrl+'/getLoginConvergencia';
        
		let bodyData = '{"pto_venta":"'+pto_venta+'"}';
		 
		 return this.http.post(url, bodyData)
		 .map(response => {
			 let data = response.json();
			 
			 return data;
		 });
	}

}
