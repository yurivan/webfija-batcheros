import { Injectable }                     from '@angular/core';
import { Http, Headers,RequestOptions, URLSearchParams, ResponseType } from '@angular/http';
import { Router }                         from '@angular/router';
import { HttpService }                    from './http.service';
import { UserService }                    from './user.service';
// import 'rxjs/Rx';
import * as globals from './globals';



var $ = require('jquery');
@Injectable()
export class LoginService {
	baseUrl = globals.BASE_URL + 'login';
	//baseUrl = globals.BASE_URL + 'auth/oauth2';
    baseUrlRoot = globals.BASE_URL;
    // Variable para pasar la data del response
    newData

	constructor(private http: Http, private userService: UserService,private httpServices : HttpService){
		
				
	}

	checkUser() {
		return this.userService.checkUser();
	}

	login(username, password) {
		let url = this.baseUrl + '/token';
		//let url = 'https://api.us.apiconnect.ibmcloud.com/telefonicaperu-developers/sb/auth/oauth2/token';
		let headers = new Headers();
    	headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('X_HTTP_APPSOURCE', globals.SOURCE);
        headers.append('X_HTTP_APPVERSION', globals.VERSION);
        headers.append('X_HTTP_USUARIO', username);

		let urlSearchParams = new URLSearchParams();
		urlSearchParams.append('username', username);
		urlSearchParams.append('password', password);
	    urlSearchParams.append('client_id', globals.CLIENT_ID);
	    urlSearchParams.append('grant_type', 'password');
	    urlSearchParams.append('scope', 'app_venta_fija');
		return this.http.post(url, urlSearchParams.toString(), { headers: headers })
		.map(response => {
			let data = response.json();
			// Guarda la data en una variable global, para poder usarla en el component
			this.newData = data
			if (data) {
				/*localStorage.setItem('token', data.access_token);
				localStorage.setItem('refresh_token', data.refresh_token);*/
				localStorage.setItem('token', data.responseData.token);
				localStorage.setItem('refresh_token', data.responseData.token);
			}
			return data;
		});
	}
    
    changePassword(codAtis, password, newPassword) {
        let url = this.baseUrlRoot+'user/web/changePassword';
        
       let bodyData = '{"codAtis":"'+codAtis+'","pwd":"'+password+'","newpwd":"'+newPassword+'"}';
        
        return this.httpServices.post(url, bodyData)
        .map(response => {
            let data = response.json();
            
            return data;
        });
        }
		
		         
	 

	logout(atis:string,source:string) {
		let save = this.baseUrlRoot+"user/web/saveLog";
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
		
	    let request = {
		};
		
		request = {	
			atis: atis,
			source:source
		};

		let body = JSON.stringify(request);
 
		localStorage.removeItem('token');
		localStorage.removeItem('refresh_token');
		localStorage.removeItem('user');
		localStorage.removeItem('order');

		localStorage.removeItem('codigoInterConect');
		localStorage.removeItem('rentaInterConect');

		localStorage.removeItem('select-campana');
		localStorage.removeItem('productsCampaniaSeleccionada');
 


		return this.http.post(save, body,options)
	  
		.map(response => {
			let data = response;
			return data;
		});
 
	 
	 
	 
	}

	// Función para obtener la nueva variable global
	returndataresponse(){
	    return this.newData
	}

	niveles(nivel, canal){
		let url = this.baseUrlRoot+'user/web/getAccessByNivel';
        
		let bodyData = '{"nivel":"'+nivel+'", "canal":"'+canal+'"}';
		 
		 return this.httpServices.post(url, bodyData)
		 .map(response => {
			 let data = response.json();
			 
			 return data;
		 });
	}
}
