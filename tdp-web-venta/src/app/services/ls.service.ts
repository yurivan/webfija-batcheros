import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from "rxjs/Rx";
import { Order } from '../model/order';
import { OnOff } from '../model/onoff';
@Injectable()

export class LocalStorageService {
 
 private order:BehaviorSubject<Order> = new BehaviorSubject<Order>(null);
 order$ = this.order.asObservable();
 constructor() {
	 // por defecto seteamos los datos de cliente como correctos
	 // Sprint 6 - Seteamos con el valor 1 por default... solo el celular obligatorio
	 
 }
 
  getData(){
    var json  = JSON.parse(localStorage.getItem('order'));
    return json;
  }
 
  setData(data:Order){
    localStorage.setItem('order',JSON.stringify(data));
    this.order.next(data);
  }
  
  //sprint2 para persistir la ultima ubicacion utilizada por el vendedor
  setUbicacionDefault(codigoDepartamento:number, codigoProvincia:number, codigoDistrito:number) {
	  let ubicacionDefault = {'codigoDepartamento': codigoDepartamento, 'codigoProvincia': codigoProvincia, 'codigoDistrito': codigoDistrito};
	  localStorage.setItem('ubicacionDefault', JSON.stringify(ubicacionDefault));
  }

  setCodInterconectDefault(codigoInterConect:string,rentaInterConect:string) {
    let codInterDefault = {'codigoInterConect': codigoInterConect, 'rentaInterConect':rentaInterConect};
    localStorage.setItem('codigoInterConect', codigoInterConect);
    localStorage.setItem('rentaInterConect', rentaInterConect);
  }
    //sprint2 para obtener la ultima ubicacion utilizada por el vendedor
  getCodInterconectDefault() {
    var codInterDefault = localStorage.getItem('codInterDefault');
    if (codInterDefault != null) {
    return JSON.parse(codInterDefault);
    }
    return null;
  }
  //sprint2 para obtener la ultima ubicacion utilizada por el vendedor
  getUbicacionDefault() {
	  var ubicacionDefault = localStorage.getItem('ubicacionDefault');
	  if (ubicacionDefault != null) {
		return JSON.parse(ubicacionDefault);
	  }
	  return null;
  }

  //sprint 8 Para guardar la ubicacion del pedidos
  setUbicacionPedido(departamento:string, provincia:string, distrito:string){
      let ubicacionPedido = {'departamento': departamento, 'provincia': provincia, 'distrito': distrito};
	  localStorage.setItem('ubicacionPedido', JSON.stringify(ubicacionPedido));
  }
  //sprint 8 Para obtener la ubicacion del pedidos
  getUbicacionPedido() {
	  var ubicacionPedido = localStorage.getItem('ubicacionPedido');
	  if (ubicacionPedido != null) {
		   return JSON.parse(ubicacionPedido);
	  }
	  return null;
  }

  validarOnOff(data:OnOff,getFecha:string){
    var horaString = getFecha.substring((getFecha.length-8),(getFecha.length-3)); 
  
    var horaInicio = parseFloat(data.hourStart.replace(":","."));
    var horaFin = parseFloat(data.hourEnd.replace(":","."));
    var horaServer = parseFloat(horaString.replace(":","."));

    if(horaInicio<horaServer && horaFin>horaServer){
        return true;
    }else{
        return false;
    }
	}

}