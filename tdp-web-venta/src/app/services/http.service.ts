import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
import { Injectable } from '@angular/core';
//import { Http, Headers } from '@angular/http';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import * as globals from './globals';
import {Upload} from '../model/upload.model'

interface HttpEvent {
  type: any;
  reason?: any;
  value?: number;
  id?: number;
  name?: string;
}

@Injectable()
export class HttpService {
	_eventBus: Subject<HttpEvent>;
	private _activeRequests: number;

	constructor(private http: Http){
		this._eventBus = new Subject<HttpEvent>();
		this._activeRequests = 0;
	}

  createHeaders() {
    
    let headers = new Headers();
    let token = localStorage.getItem('token');  
    let salesReportDetail = JSON.parse(localStorage.getItem('salesReportDetail'));
    let order = JSON.parse(localStorage.getItem('order'));    
    if( order != null ){   
        headers.append('X_HTTP_USUARIO',order.user.userId);
        
        if( order.customer != null ){
            headers.append('X_HTTP_DOCIDENT',order.customer.documentNumber);
        }
    }
    
    headers.append('X_HTTP_APPSOURCE', globals.SOURCE);
    headers.append('X_HTTP_APPVERSION', globals.VERSION);
    headers.append('X-IBM-Client-Id', globals.CLIENT_ID);
    // headers.append('X-IBM-Client-Secret', globals.CLIENT_SECRET);
    headers.append('Authorization', 'Bearer '+token);
    headers.append('Access-Token', token);
    headers.append('Content-Type', 'application/json');
    return headers;
  }

	get(url) {
    let headers = this.createHeaders();
		return this.http.get(url, { headers: headers }).catch(err => this.processRequestError(err));
	}

  post(url, data) {
      
    let headers = this.createHeaders();
    return this.http.post(url, data, { headers: headers }).catch(err => this.processRequestError(err));
  }
  
  //new
  downloadFile(url, data, tipoArchivo) {
	  let mimeType = "";
	  if (tipoArchivo == 'xls') {
		  mimeType = 'application/vnd.ms-excel';
	  }
	  this.getFile(url, data)
		.subscribe(fileData => 
		  {
		  let b:any = new Blob([fileData], { type: mimeType });
		  var url= window.URL.createObjectURL(b);
			window.open(url);
		  }
		);
  }
  
  public getFile(path, data):Observable<any>{
    let options = new RequestOptions({responseType: ResponseContentType.Blob, headers: this.createHeaders()});
    return this.http.post(path, data, options)
        .map((response: Response) => <Blob>response.blob())  ;
  }
  
  put(url, data) {
		this._activeRequests++;
    let headers = this.createHeaders();
    return this.http.put(url, data, { headers: headers }).catch(err => this.processRequestError(err));
  }

  delete(url) {
		this._activeRequests++;
    let headers = this.createHeaders();
    return this.http.delete(url, { headers: headers }).catch(err => this.processRequestError(err));
  }

  mockUploadStart() {
    this._eventBus.next({type: 'progress', reason: 'upload', value: 0, id: 1});
  }

  mockUploadProgress() {
    this._eventBus.next({type: 'progress', reason: 'upload', value: 20, id: 1});
  }

  mockUploadFinish() {
    this._eventBus.next({type: 'progress', reason: 'upload', value: 100, id: 1});
  }

  upload(url :string, upload:Upload) {
    return new Promise((resolve, reject) => {
      let formData:FormData = new FormData();
      let xhr:XMLHttpRequest = new XMLHttpRequest();
      let params = upload.params;
      let files = upload.files;
      let requestId = upload.requestId;
      var f:File;
      if (params) {
        for (var key in params) {
          if (params.hasOwnProperty(key)) {
            formData.append(key, params[key]);
          }
        }
      }
      if (files) {
        for (var key in files) {
          if (files.hasOwnProperty(key)) {
            f = files[key];
			// Sprint 3 - carga masiva de archivos
			if (f instanceof FileList) {
				var i = 0;
				for (var fileItem in f) {
					formData.append(key, f[i]);
					i++;
				}
				
			} else {
				formData.append(key, f);
			}
          }
        }
      }
      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          } else {
            reject(xhr.response);
          }
        }
      }

      xhr.upload.onprogress = (event) => {
        let progress = Math.round(event.loaded / event.total * 100);
        this._eventBus.next({type: 'progress', reason: 'upload', value: progress, id: requestId, name: f.name});
      };
      let token = localStorage.getItem('token');

      xhr.open('POST', url, true);
      xhr.setRequestHeader('X-IBM-Client-Id', globals.CLIENT_ID);
      xhr.setRequestHeader('Authorization', 'Bearer '+token);
      xhr.setRequestHeader('Access-Token', token);
      this._eventBus.next({type: 'progress', reason: 'upload', value: 0, id: requestId});
      xhr.send(formData);
    });
  }

  processRequestError(err) {
    if (err && err.status === 500) {
      let body = JSON.parse(err._body);
      if (body.moreInformation && body.moreInformation.indexOf('Security') != -1) {
        this._eventBus.next({type: 'exception', reason: 'security'})
      }
    }
    return Observable.throw(err);
  }
}

