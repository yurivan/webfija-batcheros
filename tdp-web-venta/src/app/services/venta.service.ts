import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';
import 'rxjs/Rx';

@Injectable()
export class VentaService {
	baseUrl = globals.ORDER_BASE_URL + "order/salesReport";
	baseUrlAudios = globals.ORDER_BASE_URL + "order/bandejaVentaAudio";
	baseUrlReporteAudio = globals.ORDER_BASE_URL + "order/web/report";
	baseUrlReporteHistorico = globals.ORDER_BASE_URL + "order/web/report/ventahistoricoreporte";

	constructor(private http: HttpService) {
		this.http = http;
	}

	getData(numDays, userId, statusVenta) {
		let data = '{"numDays":' + numDays + ',"vendorId":"' + userId + '","status":"' + statusVenta + '"}';
		return this.http.post(this.baseUrl, data).map(res => res.json());
	}

	getDataReporte(numDays, userId, statusVenta) {
		let data = '{"numDays":' + numDays + ',"vendorId":"' + userId + '","status":"' + statusVenta + '"}';
		return this.http.downloadFile(this.baseUrlReporteHistorico, data, 'xls');
	}

	getDataFilter(numDays, userId, idTransaccion, filterDni) {
		let data = '{"numDays":' + numDays + ',"vendorId":"' + userId + '","idTransaccion":"' + idTransaccion + '","filterDni":"' + filterDni + '"}';
		return this.http.post(this.baseUrl, data).map(res => res.json());
	}

	getDataFilterReporte(numDays, userId, idTransaccion, filterDni, statusVenta) {
		let data = '{"numDays":' + numDays + ',"vendorId":"' + userId + '","idTransaccion":"' + idTransaccion + '","filterDni":"' + filterDni + '","status":"' + statusVenta + '"}';
		return this.http.downloadFile(this.baseUrlReporteHistorico, data, 'xls');
	}

	getDataRequestJson(nombrePuntoVenta, entidadPuntoVenta, codigoEstadoAudio, codigoVendedor, buscarPorVendedor, dniCliente, fechaInicio, fechaFin) {
		let data = '{"nombrePuntoVenta":"' + nombrePuntoVenta + '","entidad":"' + entidadPuntoVenta
			+ '","codigoEstado":"' + codigoEstadoAudio + '","buscarPorVendedor":"' + buscarPorVendedor + '"';

		/*if (codigoVendedor != null && codigoVendedor != '') {
			data += ',"codigoVendedor":"' + codigoVendedor + '"';
		}*/
		data += ',"codigoVendedor":"' + codigoVendedor + '"';

		if (dniCliente != null && dniCliente != '') {
			data += ',"dniCliente":"' + dniCliente + '"';
		}

		if (fechaInicio != null && fechaInicio != '' && fechaFin != null && fechaFin != '') {
			data += ',"fechaInicio":"' + fechaInicio + '"';
			data += ',"fechaFin":"' + fechaFin + '"';
		}

		data = data + '}';
		return data;
	}

	getDataRequestJson2(nombrePuntoVenta, entidadPuntoVenta, codigoEstadoAudio, codigoVendedor, buscarPorVendedor, dniCliente, fechaInicio, fechaFin,filtroRestringeHoras) {
		let data = '{"nombrePuntoVenta":"' + nombrePuntoVenta + '","entidad":"' + entidadPuntoVenta
			+ '","codigoEstado":"' + codigoEstadoAudio + '","buscarPorVendedor":"' + buscarPorVendedor + '"';

		/*if (codigoVendedor != null && codigoVendedor != '') {
			data += ',"codigoVendedor":"' + codigoVendedor + '"';
		}*/
		data += ',"codigoVendedor":"' + codigoVendedor + '"';

		if (dniCliente != null && dniCliente != '') {
			data += ',"dniCliente":"' + dniCliente + '"';
		}

		if (fechaInicio != null && fechaInicio != '' && fechaFin != null && fechaFin != '') {
			data += ',"fechaInicio":"' + fechaInicio + '"';
			data += ',"fechaFin":"' + fechaFin + '"';
		}

		data += ',"filtroRestringeHoras":"' + filtroRestringeHoras + '"';

		data = data + '}';
		return data;
	}

	// Sprint 3 - obtener ventas pendientes de subir audio
	getVentasByPuntoVenta(nombrePuntoVenta, entidadPuntoVenta, codigoEstadoAudio, codigoVendedor, buscarPorVendedor, dniCliente, fechaInicio, fechaFin,filtroRestringeHoras) {
		let data = this.getDataRequestJson2(nombrePuntoVenta, entidadPuntoVenta, codigoEstadoAudio, codigoVendedor, buscarPorVendedor, dniCliente, fechaInicio, fechaFin,filtroRestringeHoras);
		return this.http.post(this.baseUrlAudios, data).map(res => res.json());
	}

	//reporte excel de ventas
	getReporteVenta(nombrePuntoVenta, entidadPuntoVenta, codigoEstadoAudio, codigoVendedor, buscarPorVendedor, dniCliente, fechaInicio, fechaFin, exporta) {
		let data = this.getDataRequestJson2(nombrePuntoVenta, entidadPuntoVenta, codigoEstadoAudio, codigoVendedor, buscarPorVendedor, dniCliente, fechaInicio, fechaFin, exporta);
		return this.http.downloadFile(this.baseUrlReporteAudio + "/audioventareporte", data, 'xls');
	}

	//reporte excel de ventas
	getReporteVentaHistorico(codigoVendedor, codigoEstadoAudio, fechaInicio, fechaFin) {
		let data = this.getDataRequestJson("", "", codigoEstadoAudio, codigoVendedor, "", "", fechaInicio, fechaFin);
		//return this.http.downloadFile("http://localhost:8080/" + "order/web/report" + "/ventareporte", data, 'xls');
		return this.http.downloadFile(this.baseUrlReporteAudio + "/ventareporte", data, 'xls');
	}
}
