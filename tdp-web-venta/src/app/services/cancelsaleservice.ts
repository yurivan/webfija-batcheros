import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
 
@Injectable()
export class CancelSaleService {
    private subject = new Subject<any>();
 
    sendStatus(status: boolean) {
        this.subject.next({ value: status });
    }
 
    clearStatus() {
        this.subject.next();
    }
 
    getStatus(): Observable<any> {
        return this.subject.asObservable();
    }
}