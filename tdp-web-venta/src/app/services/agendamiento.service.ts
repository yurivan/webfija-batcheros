import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpService } from './http.service';
import * as globals from './globals';

@Injectable()
export class AgendamientoService {

    constructor(private http: HttpService, private http2: Http) {
        this.http = http;
    }

    agendamiento = globals.CUSTOMER_BASE_URL + "config/initial";
    calendario = globals.CUSTOMER_BASE_URL + "calendario/agendamiento";
    agendamientoSave = globals.CUSTOMER_BASE_URL + "agendamiento";

    getInitial() {
        let request = {
        };
        let body = JSON.stringify(request);

        return this.http.post(this.agendamiento, body)
            .map(res => res.json());
    }

    getCalendarioInit(){
        let request = {
        };
        let body = JSON.stringify(request);

        return this.http.post(this.calendario, body)
            .map(res => res.json());
    }
         
    saveAgendamiento(id_visor:string,agentClientName:string,agentClientTel:string,agentClientFrangaHoraria:string,agentClientFechaSeleccionada:string,cuponesUtilizadoActualizado:number,id_cupon:number){
        let request = {
        };
        request = {
            id_visor: id_visor,
            agentClientName: agentClientName,
            agentClientTel: agentClientTel,
            agentClientFrangaHoraria: agentClientFrangaHoraria,
            agentClientFechaSeleccionada: agentClientFechaSeleccionada,
            cupones_id:id_cupon,
            cupones_utilizados:cuponesUtilizadoActualizado
        };
        let body = JSON.stringify(request);

        return this.http.post(this.agendamientoSave, body)
        .map(res => res.json());
    }

    
 
}    