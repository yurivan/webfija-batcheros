import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { ParqueService } from '../services/parque.service';
import { DireccionService } from '../services/direccion.service';

import { Producto } from '../model/producto';
import { ProductOffering } from '../model/productOffering';
import { Adresses } from '../model/adresses';
import { Order } from '../model/order';
import { User } from '../model/user';


import { Customer } from '../model/customer';
import { Localizacion } from '../model/localizacion';
import { LocalStorageService } from '../services/ls.service';
import { forEach } from '@angular/router/src/utils/collection';
import { Address } from 'cluster';
var $ = require('jquery');
(<any>window).jQuery = $
import { ProgressBarService } from '../services/progressbar.service';
import { JqueryAnimation } from '../services/globaljqueryanimation.service';
import { ValidateAddress } from '../model/validateAddress';
import { tokenName } from '@angular/compiler';
import { IfStmt } from '@angular/compiler/src/output/output_ast';
import { Argumentarios } from '../model/argumentarios';

declare var google: any;

//Componente controller de direccion

@Component({
    moduleId: 'direccion',
    selector: 'direccion',
    templateUrl: './direccion.template.html',
    providers: [ParqueService, DireccionService]

})

export class DireccionComponent implements OnInit {

    model: Customer = new Customer;
    private parqueService: ParqueService;
    private order: Order;
    private customer: Customer;
    private loading: boolean;
    private loadError: boolean;
    private mensaje_error: string;
    product: Producto[];
    marcadores: any[];
    infowindow: any;
    infowindowSearch: any;
    flagFtth: boolean;
    argumentarios: Argumentarios[];
    mjsUser: string;

    adresses: Adresses[];
    validateAddress: ValidateAddress;
    direccionService: DireccionService;
    objDepart: any[];
    private keyDeparts: string[];
    objProvs: any[];
    showProvs = new Array();
    private keyProvs: string[];
    private departSeleccionado = 0;
    selectProv: any;
    selectDepart: any;
    map: any;
    private keyDistrs: string[];
    objDists: any[];
    showDists = new Array();
    selectDist: any;
    proviSeleccionado = 0;
    distSeleccionado = 0;
    geocoder: any;
    addressSelection: string;
    addressText: string;
    addressTextMsg: string;
    referenciaText: string;
    addressInterior: string;

    inputNumero: any;
    inputInterior: any;
    marker1: any;

    localizacion: Localizacion;
    zoom: number;
    loadGifDepart: boolean;
    loadDepart: boolean;
    loadGifProv: boolean;
    loadProv: boolean;
    dataIncompleta: boolean = false;
    address: boolean = false;

    nameDepart: string;
    nameProvince: string;
    nameDistrict: string;
    codeDepart: string;
    codeProvince: string;
    codeDistrict: string;
    reload: boolean;
    lat: string = "";
    lng: string = "";
    latitud: any;
    messageDirection: string;
    directionNotExist: boolean;
    mobilePhone: string

    mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios
    messageTelephone: string
    isTheNumberOk: boolean
    errorInTelephone: boolean

    address_referencia: string;
    address_ubigeoGeocodificado: string;
    address_descripcionUbigeo: string;
    address_direccionGeocodificada: string;
    address_tipoVia: string;
    address_nombreVia: string;
    address_numeroPuerta1: string;
    address_numeroPuerta2: string;
    address_cuadra: string;
    address_tipoInterior: string;
    address_numeroInterior: string;
    address_piso: string;
    address_tipoVivienda: string;
    address_nombreVivienda: string;
    address_tipoUrbanizacion: string;
    address_nombreUrbanizacion: string;
    address_manzana: string;
    address_lote: string;
    address_kilometro: string;
    address_nivelConfianza: number;
    intentosValidador: number;
    reintentosValidador: number = 1;
    flagCopyCoordenadas: boolean;
    strCopyCoordenadas: string;

    strCodeValidateAtis: string;
    strMessageValidateAtis: string;
    flagMessageValidateAtis: boolean = false;
    isOcultarDireccion: boolean = true;

    //@Output() sendDataProduct = new EventEmitter();

    //temporales Validacion ATIS
    tmp_address_tipoVia: string;
    tmp_address_nombreVia: string;
    tmp_address_numeroPuerta1: string;
    tmp_address_numeroPuerta2: string;
    tmp_address_cuadra: string;
    tmp_address_tipoInterior: string;
    tmp_address_numeroInterior: string;
    tmp_address_piso: string;
    tmp_address_tipoVivienda: string;
    tmp_address_nombreVivienda: string;
    tmp_address_tipoUrbanizacion: string;
    tmp_address_nombreUrbanizacion: string;
    tmp_address_manzana: string;
    tmp_address_lote: string;
    tmp_address_kilometro: string;
    tmp_address_referencia: string;

    flag_address_tipoVia: boolean = true;
    flag_address_nombreVia: boolean = true;
    flag_address_numeroPuerta1: boolean = true;
    flag_address_tipoUrbanizacion: boolean = true;
    flag_address_nombreUrbanizacion: boolean = true;
    flag_address_manzana: boolean = true;
    flag_address_lote: boolean = true;


    flagViewCompleteAddress: boolean = false;

    flagTelefono: boolean = false;

    flagContingenciaAddress: boolean = false;

    message_error_normalizador: string = "";
    flagErrorNormalizador: boolean = false;

    constructor(parqueService: ParqueService,
        direccionService: DireccionService,
        private ls: LocalStorageService,
        private router: Router,
        private progresBar: ProgressBarService,
        private jqueryAnimation: JqueryAnimation) {
        this.parqueService = parqueService;
        this.direccionService = direccionService;
        this.addressSelection = "";
        this.zoom = 0;
        this.addressText = "";
        this.addressTextMsg = "";
        this.loadGifDepart = false;
        this.loadDepart = true;
        this.loadGifProv = false;
        this.loadProv = true;
        this.nameDepart = "";
        this.nameProvince = "";
        this.nameDistrict = "";

        this.inputNumero = "";
        this.addressInterior = "";
        this.reload = false;
        this.latitud = "";
        this.messageDirection = "";
        this.directionNotExist = false;
        this.mensajeValidacionDatosCliente = "";//Sprint 6
        this.messageTelephone = ""
        this.isTheNumberOk = false
        this.errorInTelephone = false

    }


    ngOnInit() {
        $('html, body').css('zoom', '86%');
        this.jqueryAnimation.animationInput()
        this.progresBar.getProgressStatus()
        this.loading = true;

        this.localizacion = new Localizacion();
        this.order = JSON.parse(localStorage.getItem('order'));
        //console.log(`telefono ${this.order.customer.telephone} -- mobile ${this.order.customer.mobilePhone}`)
        if (this.order.customer.mobilePhone == null || this.order.customer.mobilePhone == "") {
            this.mobilePhone = ""
            //$("#telefono_tienda").prop("style", "display: block !important;")
            this.flagTelefono = true;
        } else {
            this.mobilePhone = this.order.customer.mobilePhone
        }
        let ooo = JSON.parse(localStorage.getItem('order'));
        this.argumentarios = JSON.parse(localStorage.getItem('argumentarios'));

        for (let i in this.argumentarios) {
            let obj = new Argumentarios;
            obj = this.argumentarios[i];
            if (obj.pantalla.toUpperCase() == "direccion".toUpperCase()) {
                this.mjsUser = obj.mensaje;
            }
        }

        let grupo = ooo.user.typeFlujoTipificacion;
        //console.log("grupo " + grupo)
        if (grupo == "PRESENCIAL") {

            $("#referencia_tienda").prop("style", "display: block !important;");
        } else {
            $("#telefono_tienda").prop("style", "display: none !important;")
        }



        this.customer = this.order.customer;
        this.order.selectedOffering = new ProductOffering();
        this.order.sva = [];
        this.ls.setData(this.order);

        //verificamos si es que ya se guardo la direccion anteriormente
        if (this.order.product && this.order.product.address) {
            this.addressText = this.order.product.address.substring(0, this.order.product.address.indexOf(","));
        }

        this.inicializarServicioProducto();
        this.obtenerDepartamentos();
        this.cargarValoresDefaultDeScoring();//glazaror se verifican valores seleccionados en pantalla scoring
        /*Sprint 15*/
        let strIntentosValidador = (localStorage.getItem('paramNormalizador')).substring(1, 2);
        this.intentosValidador = +strIntentosValidador;
        if (this.intentosValidador == null) {
            this.intentosValidador = 3
        }
        //Sprint 16
        this.flagCopyCoordenadas = false;
        this.strCopyCoordenadas = 'Copiar Coordenadas';
    }



    //glazaror se verifican valores seleccionados en la pantalla de scoring
    cargarValoresDefaultDeScoring() {
        if (this.departSeleccionado == 0 && this.proviSeleccionado == 0 && this.distSeleccionado == 0) {
            //entonces verificamos los datos seleccionados de la pantalla scoring
            if (this.order.departmentScoring && this.order.provinceScoring && this.order.districtScoring) {
                this.departSeleccionado = this.order.departmentScoring;
                this.selDepart();
                this.proviSeleccionado = this.order.provinceScoring;
                this.selProvincia();
                this.distSeleccionado = this.order.districtScoring;
            }
        }
    }

    inicializarServicioProducto() {
        if (!this.customer) {

            this.loadError = true;
            this.mensaje_error = "La operacion no envio los datos correspondiente del cliente";
        } else {
            this.parqueService.getData(this.customer, this.order.id)
                .subscribe(
                    data => this.cargarServicio(data),
                    err => this.errorCargaParque(err),
                    () => this.cargarParque(this.product));
        }
    }

    selDepart() {
        if (this.reload) {
            this.reload = false;
        }
        if (this.marker1) {
            this.marker1.setMap(null);
        }
        this.showDists = [];
        this.showProvs = [];
        this.distSeleccionado = 0;
        this.loadGifProv = true;
        this.loadProv = false;
        this.addressSelection = "";

        this.limpiarDireccion('department');

        if (this.departSeleccionado == 0) {
            this.loadGifProv = false;
            this.loadProv = true;
            this.proviSeleccionado = 0;
            this.nameDepart = "";
            return;
        }

        this.obtenerProvincias(this.departSeleccionado);
        this.nameDepart = this.objDepart[this.departSeleccionado].name;
        this.addressSelection = this.formatSearch(this.addressText, this.nameDistrict, this.nameProvince, this.nameDepart);
    }

    limpiarDireccion(tipo) {
        if ('department' == tipo) {
            this.addressText = "";
            this.nameDistrict = "";
            this.nameProvince = "";
            this.addressInterior = "";
        } else if ('province' == tipo) {
            this.addressText = "";
            this.nameDistrict = "";
            this.addressInterior = "";
        } else if ('district' == tipo) {
            this.addressText = "";
            this.addressInterior = "";
        }

    }

    selProvincia() {

        if (this.marker1) {
            this.marker1.setMap(null);
        }
        this.loadGifDepart = true;
        this.loadDepart = false;
        this.showDists = [];
        this.distSeleccionado = 0;
        this.addressSelection = "";
        this.limpiarDireccion('province');

        if (this.proviSeleccionado == 0) {
            this.loadGifDepart = false;
            this.loadDepart = true;
            return;
        }
        this.obtenerDistritos(this.departSeleccionado + this.proviSeleccionado);
        this.nameProvince = this.objProvs[this.departSeleccionado + this.proviSeleccionado].name;
        this.addressSelection = this.formatSearch(this.addressText, this.nameDistrict, this.nameProvince, this.nameDepart);
    }

    selDistrito() {
        this.addressSelection = "";
        this.limpiarDireccion('district');

        if (this.distSeleccionado == 0) {
            return;
        }

        this.nameDistrict = this.objDists[this.departSeleccionado + this.proviSeleccionado + this.distSeleccionado].name;
        var directionSearch = this.formatSearchGoogleMap(this.nameDepart, this.nameProvince, this.nameDistrict, this.addressText);
        this.codeAddress(directionSearch, this.map, 1);
        this.addressSelection = this.formatSearch(this.addressText, this.nameDistrict, this.nameProvince, this.nameDepart);

        this.map.setZoom(15);

    }

    obtenerDistritos(skuDep) {
        if (this.objDists) {
            this.completarDistritos(skuDep);

        } else {

            this.cargarDistritos(this.direccionService.getDataDistritos(), skuDep);


        }
    }

    obtenerProvincias(skuDep) {
        if (this.objProvs) {
            this.completarProvincias(skuDep)
        } else {
            this.cargarProvincias(this.direccionService.getDataProvincias(), skuDep);
        }
    }

    obtenerDepartamentos() {

        this.cargarDepartamentos(this.direccionService.getDataDepartamento());
        this.completarDepartamentos()
    }

    completarDepartamentos() {
        this.keyDeparts = Object.keys(this.objDepart);
        //console.log(Object.keys(this.objDepart).length);
    }

    cargarDepartamentos(data) {
        this.objDepart = data;
    }

    completarProvincias(skuDep1) {
        let skuProv;
        this.showProvs.length = 0;
        this.keyProvs = Object.keys(this.objProvs);
        this.keyProvs.forEach((item, index) => {
            if (this.objProvs[item].skuDep == skuDep1) {
                if (this.reload && this.order.product && this.objProvs[item].name == this.order.product.province) {
                    skuProv = this.objProvs[item].sku;
                    this.nameProvince = this.objProvs[item].name;
                }
                this.showProvs.push(this.objProvs[item]);
            }
        });

        if (this.reload && this.order.product) {
            this.proviSeleccionado = skuProv;
            this.obtenerDistritos(this.departSeleccionado + this.proviSeleccionado);
        }
        this.loadGifProv = false;
        this.loadProv = true;
    }

    errorCargaProvincias(err) {
        console.log(err);
    }

    cargarProvincias(data, skuDep) {
        this.objProvs = data;
        this.completarProvincias(skuDep)
    }



    errorCargaDistritos(err) {
        console.log(err);
    }

    cargarDistritos(data, skuDep) {
        this.objDists = data;
        this.completarDistritos(skuDep)
    }

    completarDistritos(skuDepPro) {
        let skuDist;
        this.showDists.length = 0;
        this.keyDistrs = Object.keys(this.objDists);
        this.keyDistrs.forEach((item, index) => {
            if (this.objDists[item].skuDepPro == skuDepPro) {
                if (this.reload && this.order.product && this.objDists[item].name == this.order.product.district) {
                    skuDist = this.objDists[item].sku;
                    this.nameDistrict = this.objDists[item].name;
                }
                this.showDists.push(this.objDists[item]);
            }
        });

        if (this.showDists) {
            this.distSeleccionado = 0;
        }

        if (this.reload && this.order.product) {
            this.distSeleccionado = skuDist;
            this.addressText = this.order.product.address.substring(0, this.order.product.address.indexOf(","));
            this.addressInterior = this.order.product.additionalAddress;
            //lo optimo es reutilizar las coordenadas con las que ya se cuentan... sin embargo hay un problema pendiente de revisar con esta funcionalidad...
            //por el momento simplemente se consume nuevamente el servicio de normalizador
            this.addressSelection = this.formatSearch(this.addressText, this.nameDepart, this.nameProvince, this.nameDistrict);
            this.mapearDireccion();
            this.reload = false;
        }
        this.loadGifDepart = false;
        this.loadDepart = true;
    }

    cargarDireccionesMapa(productos: Producto[]) {
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(-12.0547975, -77.0194743),
            //mapTypeControl: false,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 12

        });
        var service = new google.maps.places.PlacesService(this.map);
        if (productos) {
            for (let i = 0; i < productos.length; i++) {
                if (productos[i].parkType == "CMS") {
                    var position = new google.maps.LatLng(Number(productos[i].coordinateY), Number(productos[i].coordinateX));
                    var marker = new google.maps.Marker({
                        map: this.map,
                        position: position,//{lat: Number(productos[i].coordinateX), lng: Number(productos[i].coordinateY)}, //var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
                        title: productos[i].productDescription
                    });
                    marker.setIcon('./src/app/imagenes/estrellita.png');
                }
            }
        } else {
            //console.log('no se obtuvo los productos del parque');
        }

        if (this.order.product) {

            let key;

            for (let i = 0; i < this.keyDeparts.length; i++) {
                if (this.order.product && this.objDepart[this.keyDeparts[i]].name == this.order.product.department) {

                    key = this.objDepart[this.keyDeparts[i]].sku;

                    this.departSeleccionado = key;
                    this.nameDepart = this.objDepart[this.keyDeparts[i]].name;
                    this.reload = true;
                    break;
                }

            }
            this.obtenerProvincias(key);

        } else {
            this.loading = false;
        }
    }

    errorCargaParque(err) {
        this.loading = false;
        this.loadError = true;
        this.mensaje_error = "El servicio customer dio un error desconocido, Por favor vuelva a intentarlo";
    }

    cargarServicio(data) {
        this.product = data.responseData;
    }

    cargarParque(pro: Producto[]) {
        this.cargarDireccionesMapa(this.product);
    }


    codeAddress(address, map, typeSearch) {
        this.directionNotExist = false;

        this.geocoder = new google.maps.Geocoder();
        this.infowindowSearch = new google.maps.InfoWindow();
        let _this = this;

        let jsonSearch = typeSearch == 2 ? { 'location': address } : { 'address': address };

        this.geocoder.geocode(jsonSearch, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                _this.dataIncompleta = false;
                map.setCenter(results[0].geometry.location);
                _this.localizacion.coordenadaY = results[0].geometry.location.lat();
                _this.localizacion.coordenadaX = results[0].geometry.location.lng();
                if (_this.marker1 != null) {
                    _this.marker1.setMap(null);
                }
                _this.marker1 = [];
                _this.marker1 = new google.maps.Marker({
                    map: map,
                    draggable: true,
                    position: results[0].geometry.location,
                    title: results[0].formatted_address
                });

                _this.infowindowSearch.setContent("<b>" + _this.addressText + "</b>"
                    + "<br/>Y: " + _this.localizacion.coordenadaY
                    + "<br/>X: " + _this.localizacion.coordenadaX);
                _this.infowindowSearch.open(map, _this.marker1);

                google.maps.event.addListener(_this.marker1, 'click', function () {
                    _this.geocoder.geocode({
                        latLng: _this.marker1.getPosition()
                    }, function (responses) {
                        let inputAdress: any;
                        inputAdress = document.getElementById("direccion");
                        if (responses && responses.length > 0) {
                            _this.localizacion.coordenadaY = responses[0].geometry.location.lat();
                            _this.localizacion.coordenadaX = responses[0].geometry.location.lng();
                            _this.infowindowSearch.setContent("<b>" + _this.addressText + "</b>"
                                + "<br/>Y: " + _this.localizacion.coordenadaY
                                + "<br/>X: " + _this.localizacion.coordenadaX);
                            _this.infowindowSearch.open(map, _this.marker1);
                        } else {
                            inputAdress.value = 'No puedo encontrar esta direccion.';
                        }
                    });
                })

                google.maps.event.addListener(_this.marker1, 'dragend', function () {
                    _this.geocoder.geocode({
                        latLng: _this.marker1.getPosition()
                    }, function (responses) {
                        let inputAdress: any;
                        inputAdress = document.getElementById("direccion");
                        if (responses && responses.length > 0) {
                            _this.localizacion.coordenadaY = responses[0].geometry.location.lat();
                            _this.localizacion.coordenadaX = responses[0].geometry.location.lng();
                            _this.infowindowSearch.setContent("<b>" + _this.addressText + "</b>"
                                + "<br/>Y: " + _this.localizacion.coordenadaY
                                + "<br/>X: " + _this.localizacion.coordenadaX);
                            _this.infowindowSearch.open(map, _this.marker1);
                        } else {
                            inputAdress.value = 'No puedo encontrar esta direccion.';
                        }
                    });
                })
            } else if (status == google.maps.GeocoderStatus.ZERO_RESULTS) {
                _this.directionNotExist = true;
                if (_this.marker1) {
                    _this.marker1.setMap(null);
                }
            }
        })
    }

    mapearDireccion() {
        this.flagCopyCoordenadas = false;
        this.strCopyCoordenadas = 'Copiar Coordenadas';
        if (this.intentosValidador >= this.reintentosValidador) {
            this.validacionDireccion(this.addressText)
        } else {
            this.getNormalizador();
        }
    }

    getNormalizador() {
        this.loading = true;
        this.directionNotExist = false;
        this.map.setZoom(17);
        //Llamar al servicio Normalizador
        this.codeDepart = this.departSeleccionado + "";
        this.codeProvince = this.proviSeleccionado + "";
        this.codeDistrict = this.distSeleccionado + "";
        var codeUbigeo = this.codeDepart + this.codeProvince + this.codeDistrict;
        let _this = this
        this.direccionService.getGeocodificarDireccion(codeUbigeo, this.addressText, this.objDists[this.departSeleccionado + this.proviSeleccionado + this.distSeleccionado].name, this.objProvs[this.departSeleccionado + this.proviSeleccionado].name, this.order.id).subscribe(
            data => {
                if (data.responseCode == "500") {
                    //Sprint 26 - contingencia normalizador dochoaro
                    _this.searchAddressContingencia()
                    _this.flagContingenciaAddress = true;
                    _this.loading = false;
                    _this.message_error_normalizador = data.responseMessage;
                    _this.flagErrorNormalizador = true;
                } else {
                    _this.flagContingenciaAddress = false;
                    _this.mostrarDireccionesServicio(data)
                }
            }
        );
    }

    searchAddressContingencia() {
        var directionSearch = this.formatSearchGoogleMap(this.nameDepart, this.nameProvince, this.nameDistrict, this.addressText.toString());
        this.codeAddress(directionSearch, this.map, 1);
    }

    validacionDireccion(addressText) {
        this.direccionService.getMessageValidationAddress(addressText).subscribe(
            data => {
                this.respuestaValidacionDireccion(data)
            },
        );
    }

    validacionDireccion2(addressText) {
        this.direccionService.getMessageValidationAddress(addressText).subscribe(
            data => {
                this.respuestaValidacionDireccion2(data)
            },
        );
    }

    respuestaValidacionDireccion(data) {
        try {
            this.validateAddress = data.responseData;
            $("#idLabelDireccion").html(this.validateAddress.isValidAddressCompleteText.message);
            $("#idDivDireccion").addClass("error_in");
            $("#idMsmDireccion").show();
            $("#direccion").blur();
            if (this.validateAddress.isValidAddressCompleteText.code == "0") {
                $("#idLabelDireccion").html('');
                $("#idDivDireccion").removeClass("error_in");
                $("#idMsmDireccion").show();
                $("#direccion").blur();
                this.getNormalizador();
            } else {
                this.reintentosValidador++;
                this.loading = false;
            }
        } catch (error) {
            $("#idLabelDireccion").html("Dirección no encontrada.");
            $("#idDivDireccion").addClass("error_in");
            $("#idMsmDireccion").show();
            $("#direccion").blur();
        }
    }

    respuestaValidacionDireccion2(data) {
        try {
            this.validateAddress = data.responseData;
            $("#idLabelDireccion").html(this.validateAddress.isValidAddressCompleteText.message);
            $("#idDivDireccion").addClass("error_in");
            $("#idMsmDireccion").show();
            $("#direccion").blur();
        } catch (error) {
            $("#idLabelDireccion").html("Dirección no encontrada.");
            $("#idDivDireccion").addClass("error_in");
            $("#idMsmDireccion").show();
            $("#direccion").blur();
        }
    }

    //Sprint 16
    getCoordenadas() {
        var save = document.createElement("input");;
        save.setAttribute("value", "Y: " + this.localizacion.coordenadaY + " X: " + this.localizacion.coordenadaX);
        document.body.appendChild(save);
        save.select();
        document.execCommand("copy");
        document.body.removeChild(save);
        this.strCopyCoordenadas = 'Copiado';
    }

    obtenerValidacionAtis() {
        if (!this.localizacion.coordenadaY || !this.localizacion.coordenadaX) {
            this.loading = false;
            alert("Debe ingresar una direccion")
            return;
        }

        if (this.addressText == '') {
            this.loading = false;
            alert("Debe ingresar una direccion")
            return;
        }

        this.loading = true;
        let _this = this;
        if (this.flagContingenciaAddress) {
            this.loading = false;
            this.flagMessageValidateAtis = true;
            this.isOcultarDireccion = false;
            this.mostrarModalDireccionTemporal()

        } else {
            _this.guardarDireccion();
        }
        //Para una proximo pase
        /*this.direccionService.getValidationAtis(
            this.objProvs[this.departSeleccionado + this.proviSeleccionado].name,
            this.address_tipoVia,
            this.address_nombreVia,
            this.address_numeroPuerta1,
            this.address_numeroPuerta2,
            this.address_cuadra,
            this.address_tipoInterior,
            this.address_numeroInterior,
            this.address_piso,
            this.address_tipoVivienda,
            this.address_nombreVivienda,
            this.address_tipoUrbanizacion,
            this.address_nombreUrbanizacion,
            this.address_manzana,
            this.address_lote,
            this.address_kilometro).subscribe(
                data => {
                    if (data == null) {
                        alert("Normalizador - Error al usar validación ATIS, intente de nuevo.")
                        _this.guardarDireccion();
                    } else {
                        _this.mostrarModalValidacionAtis(data);
                    }
                },
                err => {
                    alert("Normalizador - Error al usar validación ATIS, intente de nuevo.")
                    _this.guardarDireccion();
                });*/
    }

    mostrarModalValidacionAtis(data) {
        if (data.responseCode == 0) {
            this.guardarDireccion();
        } else {
            this.strCodeValidateAtis = data.responseCode;
            this.strMessageValidateAtis = data.responseMessage;
            this.loading = false;
            this.flagMessageValidateAtis = true;
            this.isOcultarDireccion = false;
            //this.mostrarModalDireccionTemporal(this.strCodeValidateAtis);
        }
    }

    mostrarModalDireccionTemporal() {
        this.flagViewCompleteAddress = true;

        this.tmp_address_tipoVia = this.address_tipoVia; //Importante
        this.tmp_address_nombreVia = /*this.address_nombreVia+*/this.addressText; //Importante
        this.tmp_address_numeroPuerta1 = this.address_numeroPuerta1; //Importante
        this.tmp_address_numeroPuerta2 = this.address_numeroPuerta2;
        this.tmp_address_cuadra = this.address_cuadra;
        this.tmp_address_tipoInterior = this.address_tipoInterior;
        this.tmp_address_numeroInterior = this.address_numeroInterior;
        this.tmp_address_piso = this.address_piso;
        this.tmp_address_tipoVivienda = this.address_tipoVivienda;
        this.tmp_address_nombreVivienda = this.address_nombreVivienda;
        this.tmp_address_tipoUrbanizacion = this.address_tipoUrbanizacion; //Importante
        this.tmp_address_nombreUrbanizacion = this.address_nombreUrbanizacion; //Importante
        this.tmp_address_manzana = this.address_manzana; //Importante
        this.tmp_address_lote = this.address_lote; //Importante
        this.tmp_address_kilometro = this.address_kilometro;
        this.tmp_address_referencia = this.address_referencia;

        /*switch (strCodeValidateAtis) {
            case "1":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = true
                this.flag_address_numeroPuerta1 = true
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "2":
                this.flag_address_tipoVia = true
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = true
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "3":
                this.flag_address_tipoVia = true
                this.flag_address_nombreVia = true
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "4":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = true
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "5":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "7":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = false
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "8":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = false
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "9":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = false
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "10":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = false
                this.flag_address_nombreUrbanizacion = false
                this.flag_address_manzana = false
                this.flag_address_lote = true
                break;
            case "11":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = false
                this.flag_address_nombreUrbanizacion = false
                this.flag_address_manzana = true
                this.flag_address_lote = false
                break;
            case "13":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = true
                break;
            case "14":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = true
                this.flag_address_lote = false
                break;
            case "15":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "16":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = false
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "18":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = false
                this.flag_address_nombreUrbanizacion = false
                this.flag_address_manzana = true
                this.flag_address_lote = true
                break;
            case "19":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = true
                this.flag_address_numeroPuerta1 = true
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "20":
                this.flag_address_tipoVia = true
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = true
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "21":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = true
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "22":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = true
                this.flag_address_tipoUrbanizacion = false
                this.flag_address_nombreUrbanizacion = false
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "25":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = true
                this.flag_address_nombreUrbanizacion = false
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "34":
                this.flag_address_tipoVia = false
                this.flag_address_nombreVia = false
                this.flag_address_numeroPuerta1 = false
                this.flag_address_tipoUrbanizacion = false
                this.flag_address_nombreUrbanizacion = true
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
            case "36":
                this.flag_address_tipoVia = true
                this.flag_address_nombreVia = true
                this.flag_address_numeroPuerta1 = true
                this.flag_address_tipoUrbanizacion = false
                this.flag_address_nombreUrbanizacion = false
                this.flag_address_manzana = false
                this.flag_address_lote = false
                break;
        }*/
    }

    continueCompleteAddress() {
        this.address_tipoVia = this.tmp_address_tipoVia; //Importante
        this.address_nombreVia = this.tmp_address_nombreVia; //Importante
        this.address_numeroPuerta1 = this.tmp_address_numeroPuerta1; //Importante
        this.address_numeroPuerta2 = this.tmp_address_numeroPuerta2;
        this.address_cuadra = this.tmp_address_cuadra;
        this.address_tipoInterior = this.tmp_address_tipoInterior;
        this.address_numeroInterior = this.tmp_address_numeroInterior;
        this.address_tipoVivienda = this.tmp_address_tipoVivienda;
        this.address_nombreVivienda = this.tmp_address_nombreVivienda;
        this.address_tipoUrbanizacion = this.tmp_address_tipoUrbanizacion; //Importante
        this.address_nombreUrbanizacion = this.tmp_address_nombreUrbanizacion; //Importante
        this.address_manzana = this.tmp_address_manzana; //Importante
        this.address_lote = this.tmp_address_lote; //Importante
        this.address_piso = this.tmp_address_piso;
        this.address_referencia = this.tmp_address_referencia;
        this.concatAddress();

        if(this.order.offline.flag.indexOf('3') == -1){
            this.order.offline.flag = this.order.offline.flag + (this.order.offline.flag?',3':'3');               
        }

        this.guardarDireccion();
    }

    concatAddress() {
        this.addressText = "";
        if (this.address_tipoVia != null) {
            this.addressText = this.addressText + this.address_tipoVia;
        }
        if (this.address_nombreVia != null) {
            this.addressText = this.addressText + " " + this.address_nombreVia
        }
        if (this.address_numeroPuerta1 != null) {
            this.addressText = this.addressText + " " + this.address_numeroPuerta1
        }
        if (this.address_numeroPuerta2 != null) {
            this.addressText = this.addressText + " " + this.address_numeroPuerta2
        }
        if (this.address_cuadra != null) {
            this.addressText = this.addressText + " " + this.address_cuadra
        }
        if (this.address_tipoInterior != null) {
            this.addressText = this.addressText + " " + this.address_tipoInterior
        }
        if (this.address_numeroInterior != null) {
            this.addressText = this.addressText + " " + this.address_numeroInterior
        }
        if (this.address_tipoVivienda != null) {
            this.addressText = this.addressText + " " + this.address_tipoVivienda
        }
        if (this.address_nombreVivienda !=null) {
            this.addressText = this.addressText + " " + this.address_nombreVivienda
        }
        if (this.address_tipoUrbanizacion != null) {
            this.addressText = this.addressText + " " + this.address_tipoUrbanizacion
        }
        if (this.address_nombreUrbanizacion != null) {
            this.addressText = this.addressText + " " + this.address_nombreUrbanizacion
        }
        if (this.address_manzana != null) {
            this.addressText = this.addressText + " " + this.address_manzana
        }
        if (this.address_lote != null) {
            this.addressText = this.addressText + " " + this.address_lote
        }
        if (this.address_piso != null) {
            this.addressText = this.addressText + " " + this.address_piso
        }

    }

    cancelCompleteAddress() {
        this.flagViewCompleteAddress = false;

        this.tmp_address_tipoVia = "";
        this.tmp_address_nombreVia = "";
        this.tmp_address_numeroPuerta1 = "";
        this.tmp_address_numeroPuerta2 = "";
        this.tmp_address_cuadra = "";
        this.tmp_address_tipoInterior = "";
        this.tmp_address_numeroInterior = "";
        this.tmp_address_tipoVivienda = "";
        this.tmp_address_nombreVivienda = "";
        this.tmp_address_tipoUrbanizacion = "";
        this.tmp_address_nombreUrbanizacion = "";
        this.tmp_address_manzana = "";
        this.tmp_address_lote = "";
        this.tmp_address_piso = "";
        this.tmp_address_referencia = "";
    }

    mostrarDireccionesServicio(data) {
        let inputAdress: any;
        let paramNivelConfianza: any;
        let codeMessageNivelConfianza: any;
        let messageNivelConfianza: any;
        let flagMessageNivelCongianzaUp: boolean = false;
        let codeMsmAddressNotFound: any;
        let messageMsmAddressNotFoung: any;
        inputAdress = document.getElementById("direccion");
        let tmp_ref: string;
        let _this = this;
        this.flagFtth = false
        try {
            this.adresses = data.responseData[0].addresses;
            paramNivelConfianza = data.responseData[0].paramNivelConfianza;
            codeMessageNivelConfianza = data.responseData[0].codeMessageNivelConfianza;
            messageNivelConfianza = data.responseData[0].messageNivelConfianza;

            codeMsmAddressNotFound = data.responseData[0].codeMsmAddressNotFound;
            messageMsmAddressNotFoung = data.responseData[0].messageMsmAddressNotFoung;

            if (this.referenciaText == undefined) {
                this.referenciaText = ""
                tmp_ref = this.referenciaText;
            } else {
                tmp_ref = this.referenciaText;
            }

            if (this.adresses.length == 0) {
                if (codeMsmAddressNotFound == "4") {
                    this.getMapNull();
                    alert(messageMsmAddressNotFoung);
                } else {
                    this.validacionDireccion2(this.addressText);
                }
            } else {
                let distZoom;
                if (this.adresses.length == 1) {
                    distZoom = 18;
                } else {
                    distZoom = 14;
                }
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: distZoom,
                    center: new google.maps.LatLng(Number(this.adresses[0].yPunto), Number(this.adresses[0].xPunto)),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });


                if (this.marcadores) {
                    for (let marcador of this.marcadores) {
                        marcador.setMap(null)
                    }
                }

                this.marcadores = new Array()
                this.infowindow = new google.maps.InfoWindow();

                this.direccionService.getFtth(
                    this.objDepart[this.departSeleccionado].name.toUpperCase(),
                    this.objProvs[this.departSeleccionado + this.proviSeleccionado].name.toUpperCase(),
                    this.objDists[this.departSeleccionado + this.proviSeleccionado + this.distSeleccionado].name.toUpperCase()
                ).subscribe(
                    data => {
                        for (let coordenada of data.responseData) {
                            var marker = new google.maps.Marker({
                                position: new google.maps.LatLng(coordenada.coordenadaY, coordenada.coordenadaX),
                                icon: $("#fibra").attr('src'),
                                map: map,
                                title: 'Fibra Óptica'
                            });
                            marker.addListener('click', (function (marker) {
                                return function () {
                                    _this.flagFtth = true
                                    _this.infowindowSearch.close()
                                    _this.localizacion.direccion = coordenada.direccion
                                    inputAdress.value = coordenada.direccion
                                    _this.addressText = coordenada.direccion
                                    _this.infowindow.setContent("<b>" + coordenada.direccion + "</b>" +
                                        "<br><b>Se usara este punto de instalación</b><br>Y: " + coordenada.coordenadaY
                                        + "<br>X: " + coordenada.coordenadaX);
                                    _this.infowindow.open(map, marker);
                                    _this.localizacion.coordenadaY = marker.getPosition().lat();
                                    _this.localizacion.coordenadaX = marker.getPosition().lng();
                                }
                            })(marker))
                            _this.marcadores.push(marker)
                        }
                    },
                    err => {

                    });
                this.infowindowSearch = new google.maps.InfoWindow();
                this.geocoder = new google.maps.Geocoder();
                var marker, i;

                if (this.adresses.length == 1) {
                    inputAdress.value = this.adresses[0].direccion;
                    this.localizacion.coordenadaY = this.adresses[0].yPunto;
                    this.localizacion.coordenadaX = this.adresses[0].xPunto;
                    this.addressText = inputAdress.value + "";
                    //this.referenciaText = this.adresses[0].referencia + tmp_ref
                    //Sprint 13 - para enviar automatizador
                    this.address_cuadra = this.adresses[0].cuadra;
                    this.address_descripcionUbigeo = this.adresses[0].descripcionUbigeo;
                    this.address_direccionGeocodificada = this.adresses[0].direccionGeocodificada;
                    this.address_kilometro = this.adresses[0].kilometro;
                    this.address_lote = this.adresses[0].lote;
                    this.address_manzana = this.adresses[0].manzana;
                    this.address_nivelConfianza = this.adresses[0].nivelConfianza;
                    this.address_nombreUrbanizacion = this.adresses[0].nombreUrbanizacion;
                    this.address_nombreVia = this.adresses[0].nombreVia;
                    this.address_nombreVivienda = this.adresses[0].nombreVivienda;
                    this.address_numeroInterior = this.adresses[0].numeroInterior;
                    this.address_numeroPuerta1 = this.adresses[0].numeroPuerta1
                    this.address_numeroPuerta2 = this.adresses[0].numeroPuerta2
                    this.address_piso = this.adresses[0].piso
                    this.address_referencia = this.referenciaText
                    this.address_tipoInterior = this.adresses[0].tipoInterior
                    this.address_tipoUrbanizacion = this.adresses[0].tipoUrbanizacion
                    this.address_tipoVia = this.adresses[0].tipoVia
                    this.address_tipoVivienda = this.adresses[0].tipoVivienda
                    this.address_ubigeoGeocodificado = this.adresses[0].ubigeoGeocodificado

                    if (paramNivelConfianza != 0) {
                        if (this.adresses[0].nivelConfianza <= paramNivelConfianza) {
                            flagMessageNivelCongianzaUp = false;
                        } else {
                            flagMessageNivelCongianzaUp = true;
                        }
                    }

                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(Number(this.adresses[0].yPunto), Number(this.adresses[0].xPunto)),
                        map: map,
                        draggable: true
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            inputAdress.value = _this.adresses[0].direccion
                            _this.addressText = _this.adresses[0].direccion + "";
                            _this.referenciaText = "";
                            _this.referenciaText = _this.adresses[0].referencia + tmp_ref
                            //Sprint 13 - para enviar automatizador
                            _this.address_cuadra = _this.adresses[0].cuadra;
                            _this.address_descripcionUbigeo = _this.adresses[0].descripcionUbigeo;
                            _this.address_direccionGeocodificada = _this.adresses[0].direccionGeocodificada;
                            _this.address_kilometro = _this.adresses[0].kilometro;
                            _this.address_lote = _this.adresses[0].lote;
                            _this.address_manzana = _this.adresses[0].manzana;
                            _this.address_nivelConfianza = _this.adresses[0].nivelConfianza;
                            _this.address_nombreUrbanizacion = _this.adresses[0].nombreUrbanizacion;
                            _this.address_nombreVia = _this.adresses[0].nombreVia;
                            _this.address_nombreVivienda = _this.adresses[0].nombreVivienda;
                            _this.address_numeroInterior = _this.adresses[0].numeroInterior;
                            _this.address_numeroPuerta1 = _this.adresses[0].numeroPuerta1
                            _this.address_numeroPuerta2 = _this.adresses[0].numeroPuerta2
                            _this.address_piso = _this.adresses[0].piso
                            _this.address_referencia = _this.referenciaText
                            _this.address_tipoInterior = _this.adresses[0].tipoInterior
                            _this.address_tipoUrbanizacion = _this.adresses[0].tipoUrbanizacion
                            _this.address_tipoVia = _this.adresses[0].tipoVia
                            _this.address_tipoVivienda = _this.adresses[0].tipoVivienda
                            _this.address_ubigeoGeocodificado = _this.adresses[0].ubigeoGeocodificado
                            _this.geocoder.geocode({
                                latLng: marker.getPosition()
                            }, function (responses) {
                                if (responses && responses.length > 0) {
                                    _this.localizacion.coordenadaY = responses[0].geometry.location.lat();
                                    _this.localizacion.coordenadaX = responses[0].geometry.location.lng();
                                }
                                if (_this.infowindow) {
                                    _this.infowindow.close();
                                }
                                _this.flagFtth = false
                                _this.infowindowSearch.setContent("<b>" + _this.addressText + "</b>"
                                    + "<br/>Y: " + _this.localizacion.coordenadaY
                                    + "<br/>X: " + _this.localizacion.coordenadaX);
                                _this.infowindowSearch.open(map, marker);
                                _this.strCopyCoordenadas = 'Copiar Coordenadas';
                            });
                        }
                    })(marker, i));

                    google.maps.event.addListener(marker, 'dragend', (function (marker, i) {
                        return function () {
                            _this.addressText = _this.adresses[0].direccion + "";
                            _this.referenciaText = ""
                            _this.referenciaText = _this.adresses[0].referencia + tmp_ref

                            //Sprint 13 - para enviar automatizador
                            _this.address_cuadra = _this.adresses[0].cuadra;
                            _this.address_descripcionUbigeo = _this.adresses[0].descripcionUbigeo;
                            _this.address_direccionGeocodificada = _this.adresses[0].direccionGeocodificada;
                            _this.address_kilometro = _this.adresses[0].kilometro;
                            _this.address_lote = _this.adresses[0].lote;
                            _this.address_manzana = _this.adresses[0].manzana;
                            _this.address_nivelConfianza = _this.adresses[0].nivelConfianza;
                            _this.address_nombreUrbanizacion = _this.adresses[0].nombreUrbanizacion;
                            _this.address_nombreVia = _this.adresses[0].nombreVia;
                            _this.address_nombreVivienda = _this.adresses[0].nombreVivienda;
                            _this.address_numeroInterior = _this.adresses[0].numeroInterior;
                            _this.address_numeroPuerta1 = _this.adresses[0].numeroPuerta1
                            _this.address_numeroPuerta2 = _this.adresses[0].numeroPuerta2
                            _this.address_piso = _this.adresses[0].piso
                            _this.address_referencia = _this.referenciaText
                            _this.address_tipoInterior = _this.adresses[0].tipoInterior
                            _this.address_tipoUrbanizacion = _this.adresses[0].tipoUrbanizacion
                            _this.address_tipoVia = _this.adresses[0].tipoVia
                            _this.address_tipoVivienda = _this.adresses[0].tipoVivienda
                            _this.address_ubigeoGeocodificado = _this.adresses[0].ubigeoGeocodificado
                            _this.geocoder.geocode({
                                latLng: marker.getPosition()
                            }, function (responses) {
                                if (responses && responses.length > 0) {
                                    _this.localizacion.coordenadaY = responses[0].geometry.location.lat();
                                    _this.localizacion.coordenadaX = responses[0].geometry.location.lng();
                                }
                                _this.infowindowSearch.setContent("<b>" + _this.addressText + "</b>"
                                    + "<br/>Y: " + _this.localizacion.coordenadaY
                                    + "<br/>X: " + _this.localizacion.coordenadaX);
                                _this.infowindowSearch.open(map, marker);
                                _this.strCopyCoordenadas = 'Copiar Coordenadas';
                            });
                        }
                    })(marker, i));
                    _this.infowindowSearch.setContent("<b>" + this.addressText + "</b>"
                        + "<br/>Y: " + this.localizacion.coordenadaY
                        + "<br/>X: " + this.localizacion.coordenadaX);
                    _this.infowindowSearch.open(map, marker);
                    this.loading = false;
                    this.onReadyDireccionMapa();
                    this.flagCopyCoordenadas = true;
                } else {
                    for (let dir of this.adresses) {

                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(Number(dir.yPunto), Number(dir.xPunto)),
                            map: map,
                            draggable: true
                        });

                        google.maps.event.addListener(marker, 'click', (function (marker, i) {
                            return function () {
                                inputAdress.value = _this.adresses[0].direccion
                                _this.addressText = dir.direccion + "";
                                _this.referenciaText = ""
                                _this.referenciaText = dir.referencia + tmp_ref

                                //Sprint 13 - para enviar automatizador
                                _this.address_cuadra = dir.cuadra;
                                _this.address_descripcionUbigeo = dir.descripcionUbigeo;
                                _this.address_direccionGeocodificada = dir.direccionGeocodificada;
                                _this.address_kilometro = dir.kilometro;
                                _this.address_lote = dir.lote;
                                _this.address_manzana = dir.manzana;
                                _this.address_nivelConfianza = dir.nivelConfianza;
                                _this.address_nombreUrbanizacion = dir.nombreUrbanizacion;
                                _this.address_nombreVia = dir.nombreVia;
                                _this.address_nombreVivienda = dir.nombreVivienda;
                                _this.address_numeroInterior = dir.numeroInterior;
                                _this.address_numeroPuerta1 = dir.numeroPuerta1
                                _this.address_numeroPuerta2 = dir.numeroPuerta2
                                _this.address_piso = dir.piso
                                _this.address_referencia = _this.referenciaText
                                _this.address_tipoInterior = dir.tipoInterior
                                _this.address_tipoUrbanizacion = dir.tipoUrbanizacion
                                _this.address_tipoVia = dir.tipoVia
                                _this.address_tipoVivienda = dir.tipoVivienda
                                _this.address_ubigeoGeocodificado = dir.ubigeoGeocodificado
                                _this.geocoder.geocode({
                                    latLng: marker.getPosition()
                                }, function (responses) {
                                    if (responses && responses.length > 0) {
                                        _this.localizacion.coordenadaY = responses[0].geometry.location.lat();
                                        _this.localizacion.coordenadaX = responses[0].geometry.location.lng();
                                        _this.infowindowSearch.setContent("<b>" + dir.direccion + "</b>"
                                            + "<br/>Y: " + responses[0].geometry.location.lat()
                                            + "<br/>X: " + responses[0].geometry.location.lng());
                                        _this.infowindowSearch.open(map, marker);
                                        _this.flagCopyCoordenadas = true;
                                        _this.strCopyCoordenadas = 'Copiar Coordenadas';
                                    }
                                });
                            }
                        })(marker, i));

                        google.maps.event.addListener(marker, 'dragend', (function (marker, i) {
                            return function () {
                                _this.addressText = dir.direccion + "";
                                _this.referenciaText = ""
                                _this.referenciaText = dir.referencia + tmp_ref

                                //Sprint 13 - para enviar automatizador
                                _this.address_cuadra = dir.cuadra;
                                _this.address_descripcionUbigeo = dir.descripcionUbigeo;
                                _this.address_direccionGeocodificada = dir.direccionGeocodificada;
                                _this.address_kilometro = dir.kilometro;
                                _this.address_lote = dir.lote;
                                _this.address_manzana = dir.manzana;
                                _this.address_nivelConfianza = dir.nivelConfianza;
                                _this.address_nombreUrbanizacion = dir.nombreUrbanizacion;
                                _this.address_nombreVia = dir.nombreVia;
                                _this.address_nombreVivienda = dir.nombreVivienda;
                                _this.address_numeroInterior = dir.numeroInterior;
                                _this.address_numeroPuerta1 = dir.numeroPuerta1
                                _this.address_numeroPuerta2 = dir.numeroPuerta2
                                _this.address_piso = dir.piso
                                _this.address_referencia = _this.referenciaText
                                _this.address_tipoInterior = dir.tipoInterior
                                _this.address_tipoUrbanizacion = dir.tipoUrbanizacion
                                _this.address_tipoVia = dir.tipoVia
                                _this.address_tipoVivienda = dir.tipoVivienda
                                _this.address_ubigeoGeocodificado = dir.ubigeoGeocodificado
                                _this.geocoder.geocode({
                                    latLng: marker.getPosition()
                                }, function (responses) {
                                    if (responses && responses.length > 0) {
                                        _this.localizacion.coordenadaY = responses[0].geometry.location.lat();
                                        _this.localizacion.coordenadaX = responses[0].geometry.location.lng();
                                        _this.infowindowSearch.setContent("<b>" + dir.direccion + "</b>"
                                            + "<br/>Y: " + responses[0].geometry.location.lat()
                                            + "<br/>X: " + responses[0].geometry.location.lng());
                                        _this.infowindowSearch.open(map, marker);
                                        _this.flagCopyCoordenadas = true;
                                        _this.strCopyCoordenadas = 'Copiar Coordenadas';

                                    }
                                });
                            }
                        })(marker, i));
                        //Sprint 15
                        if (paramNivelConfianza != 0) {

                            if (dir.nivelConfianza <= paramNivelConfianza) {
                                flagMessageNivelCongianzaUp = false;
                            } else {
                                flagMessageNivelCongianzaUp = true;
                            }
                        }
                    }
                    this.loading = false;
                    this.onReadyDireccionMapa();
                }
            }
            if (flagMessageNivelCongianzaUp) {
                alert(messageNivelConfianza);
            }

        } catch (error) {
            this.validacionDireccion2(this.addressText);
            this.getMapNull();
        }
    }

    getMapNull() {
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(-12.0547975, -77.0194743),
            //mapTypeControl: false,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoom: 12

        });
        var service = new google.maps.places.PlacesService(this.map);
        this.localizacion.coordenadaY = null;
        this.localizacion.coordenadaX = null;
        this.loading = false;
    }

    deleteStyleError() {
        $("#idDivDireccion").removeClass("error_in");
        $("#idMsmDireccion").hide();
    }
    //telefono
    deleteStyleError2() {
        $("#telefono_tienda").removeClass("error_in");
        $("#idMsmTelefono").hide();
    }

    validateCountTel() {
        var tel = $("#tel").val() + "";
        var countTel = tel.length;
        if ((countTel == 7 || countTel == 9) && Number(tel)) {
            $("#telefono_tienda").removeClass("error_in");
            $("#idMsmTelefono").hide();
        } else {
            $("#telefono_tienda").addClass("error_in");
            $("#idMsmTelefono").show();
        }
    }

    errorInNumber(message: string) {
        this.errorInTelephone = true
        this.messageTelephone = `${message}`
        this.isTheNumberOk = false
        $("#idMsmTelefono").show()
    }

    correctNumber(number: string) {
        this.isTheNumberOk = true
        this.messageTelephone = "El teléfono se ha guardado correctamente. " + number
        this.ls.setUbicacionPedido(this.nameDepart, this.nameProvince, this.order.product.district);
        $('html, body').css('zoom', '100%');
        this.router.navigate(['campanas']);
    }

    consultarValidacionAtis() {
        this.obtenerValidacionAtis();
    }

    guardarDireccion() {

        if (localStorage.getItem("visita")) {
            localStorage.removeItem("visita")
        }


        if (localStorage.getItem("dataRed")) {
            localStorage.removeItem("dataRed")
        }
        if (this.ls.getData().editandoCliente) {
            this.mensajeValidacionDatosCliente = "Celular";
            $('#modalCliente').modal('show');
            return;
        }

        if (!this.departSeleccionado || !this.proviSeleccionado
            || !this.distSeleccionado || !this.localizacion.coordenadaX || !this.localizacion.coordenadaY || !this.addressText) {
            this.dataIncompleta = true;
            $("#idDivDireccion").addClass("error_in");
            $("#idMsmDireccion").show();
            $("#direccion").blur()
            this.messageDirection = "Por favor, ingrese la direccion.";
            return;
        }

        if(this.addressText.trim()==''){
            alert('Direccion vacia, favor de completar direccion.');
            return;
        }

        //this.inputInterior = document.getElementById("txtInterior");
        this.addressSelection = this.formatSearch(this.addressText, this.objDists[this.departSeleccionado + this.proviSeleccionado + this.distSeleccionado].name, this.nameProvince, this.nameDepart);
        this.order.product = new Producto();
        this.order.product.parkType = '';
        this.order.product.department = this.objDepart[this.departSeleccionado].name;
        this.order.product.province = this.objProvs[this.departSeleccionado + this.proviSeleccionado].name;
        this.order.product.district = this.objDists[this.departSeleccionado + this.proviSeleccionado + this.distSeleccionado].name;
        this.order.product.ubigeoCode = this.departSeleccionado + this.proviSeleccionado + this.distSeleccionado + '';
        this.order.product.sourceType = '';
        this.order.product.phone = '';
        this.order.product.coordinateX = this.localizacion.coordenadaX;
        this.order.product.coordinateY = this.localizacion.coordenadaY;
        this.order.product.address = this.addressSelection; //this.addressText + " " + this.addressNumText;
        this.order.product.additionalAddress = this.addressInterior;
        //Sprint 13 - Envio datos normalizador para automatizador parte 2
        this.order.address_cuadra = this.address_cuadra
        this.order.address_descripcionUbigeo = this.address_descripcionUbigeo
        this.order.address_direccionGeocodificada = this.address_direccionGeocodificada
        this.order.address_kilometro = this.address_kilometro
        this.order.address_lote = this.address_lote
        this.order.address_manzana = this.address_manzana
        this.order.address_nivelConfianza = this.address_nivelConfianza + ""
        this.order.address_nombreUrbanizacion = this.address_nombreUrbanizacion
        this.order.address_nombreVia = this.address_nombreVia
        this.order.address_nombreVivienda = this.address_nombreVivienda
        this.order.address_numeroInterior = this.address_numeroInterior
        this.order.address_numeroPuerta1 = this.address_numeroPuerta1
        this.order.address_numeroPuerta2 = this.address_numeroPuerta2
        this.order.address_piso = this.address_piso
        this.order.address_referencia = this.address_referencia
        this.order.address_tipoInterior = this.address_tipoInterior
        this.order.address_tipoUrbanizacion = this.address_tipoUrbanizacion
        this.order.address_tipoVia = this.address_tipoVia
        this.order.address_tipoVivienda = this.address_tipoVivienda
        this.order.address_ubigeoGeocodificado = this.address_ubigeoGeocodificado
        this.order.flagFtth = this.flagFtth

        this.dataIncompleta = false;
        this.order.status = 'D';
        //console.log("SE SETEO ")
        //console.log(this.order);
        this.ls.setData(this.order);

        let resumen = {
            producto: "producto",
            direccion: this.addressSelection,
            costo: "69",
            precio: "169"
        }

        localStorage.setItem("resumen", JSON.stringify(resumen));
        //console.log("resumen ---- .-.- -- -- - ----" + JSON.parse(localStorage.getItem('resumen')));

        let grupo = this.order.user.typeFlujoTipificacion;

        if (grupo == "PRESENCIAL") {
            this.order.customer.mobilePhone = this.mobilePhone;
            var celular = this.order.customer.mobilePhone;
            var cantidad = celular.length;
            this.ls.setData(this.order);

            //console.log(this.order);

            if (((cantidad == 7) || (cantidad == 9)) && Number(celular)) {
                this.ls.setUbicacionPedido(this.nameDepart, this.nameProvince, this.order.product.district);
                $('html, body').css('zoom', '100%');
                this.router.navigate(['campanas']);
            } else {
                if (this.mobilePhone.length <= 7 && this.mobilePhone.length > 0) {
                    if (this.mobilePhone.charAt(0) == '0') {
                        this.errorInNumber("El teléfono debe contar con : 0 + Ciudad + Fijo")
                    } else {
                        this.errorInNumber("Ingrese un número de celular válido")
                    }

                } else {
                    if (this.mobilePhone.length == 0) {
                        this.errorInNumber(`Ingrese un número por favor.`)
                    } else {
                        this.errorInNumber(`El teléfono debe tener 9 dígitos: ${celular}`)
                    }

                }
                this.loading = false;
                this.ls.setUbicacionPedido(this.nameDepart, this.nameProvince, this.order.product.district);
                $('html, body').css('zoom', '100%');
                this.router.navigate(['campanas']);
            }
        } else {
            this.order.customer.mobilePhone = this.mobilePhone;
            var celular = this.order.customer.mobilePhone;
            //console.log("celular  2 " + celular)
            this.ls.setData(this.order);
            this.ls.setUbicacionPedido(this.nameDepart, this.nameProvince, this.order.product.district);
            $('html, body').css('zoom', '100%');
            this.router.navigate(['campanas']);
        }

        this.ls.setUbicacionPedido(this.nameDepart, this.nameProvince, this.order.product.district);
    }


    formatSearch(txtDirection, comboDistrit, comboProvince, comboDepartament) {
        if (comboProvince == 'Prov. Const. del Callao') {
            return (txtDirection.trim() + ', ' + comboDistrit.trim() + ', ' + comboDepartament.trim() + ', Peru').replace(", ,", ",");
        } else {
            return (txtDirection.trim() + ', ' + comboDistrit.trim() + ', ' + comboProvince.trim() + ', ' + comboDepartament.trim() + ', Peru').replace(", ,", ",");
        }
    }

    formatSearchGoogleMap(nameDepart, nameProvince, nameDistrict, addressText) {
        if (nameProvince == 'Prov. Const. del Callao') {
            return '\'' + nameDepart + '\' \'' + nameDistrict + '\' \'' + addressText + '\'';
        } else {
            return '\'' + nameDepart + '\' \'' + nameProvince + '\' \'' + nameDistrict + '\' \'' + addressText + '\'';
        }
    }


    moveMarker(mark) {
        this.geocodePosition(mark.getPosition());
    }


    geocodePosition(pos) {

        this.geocoder.geocode({
            latLng: pos
        }, this.updateDirection);
    }

    updateDirection(responses) {
        if (responses && responses.length > 0) {
            this.addressText = responses[0].formatted_address;
        } else {
            this.addressText = 'No se pudo encontrar esta direccion.';
        }
    }

    // Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"
    cerrarDialogo(nombreModal) {
        $("#" + nombreModal).modal('hide');
    }

    onReadyDireccionMapa() {

        if ($("#direccion").val() != "") {
            $("#direccion").addClass("has-content")
        } else {
            $("#direccion").removeClass("has-content")
        }

    }

}
