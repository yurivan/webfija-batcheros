import { Estados } from '../model/estadosTr';

export class Tracking {
    mes: string;
    clientesTotales: number;
    estados: Estados[];
}
    