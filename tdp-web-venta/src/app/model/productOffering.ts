import { Sva } from "./sva";

export class ProductOffering {

      productCode: string;
      productName: string;
      discount: string;
      price: number;
      promPrice: number;
      monthPeriod: number;
      financingCost: number;
      financingMonth: number;
      installCost: number;
      returnMonth: number;
      returnPeriod: string;
      equipmentType: string;
      lineType: string;
      cashPrice: number;
      internetSpeed: number;
      productTypeCode: string;
      productType: string;
      commercialOperation: string;
      paymentMethod: string;
      campaign: string;
      internetTech: string;
      tvSignal: string;
      tvTech: string;
      productCategoryCode: string;
      productCategory: string;
      expertCode: string;
      id: number;
      cantidaddecos: number;
      sva:Sva;

      promoInternetSpeed: number;
      periodoPromoInternetSpeed: number;

      equipamientoLinea: string;
      equipamientoInternet: string;
      equipamientoTv: string;

      // Sprint 10
      altaPuraSvasBloqueTV : string;
      altaPuraSvasLinea : string;
      altaPuraSvasInternet : string;

      //Sprint 12 - guardado automatizador parte 1
      cod_fac_tec_cd: string;
      msx_cbr_voi_ges_in: string;
      msx_cbr_voi_gis_in: string;
      msx_ind_snl_gis_cd: string;
      msx_ind_gpo_gis_cd: string;
      cod_ind_sen_cms: string;
      cod_cab_cms: string;

      cobre_blq_vta: string;
      cobre_blq_trm: string;

      flag_offline: boolean;
}
