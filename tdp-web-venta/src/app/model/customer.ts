export class Customer {

  	documentType: string;
  	documentNumber: string;
 	firstName: string;
    lastName1: string;
    lastName2: string;
    email: string;
    telephone: string;
    mobilePhone: string;
    birthDate: string;
    expDate: string;
    birthDep: string;
    birthCity: string;
    birthDist: string;
    nameFather: string;
    nameMother: string;
	//Sprint 7... se adiciona propiedad nacionalidad
	nationality: string;
    //Sprint 24 RUC
    razonSocial : string;
    estContrib : string;
    condDomicilio : string;
    direccion : string;
    tipoDocumentoRrll : string;
    numeroDocumentoRrll : string;
    nombreCompletoRrll : string;
}