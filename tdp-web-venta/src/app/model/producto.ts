export class Producto {

    parkType: string;
    subscriber: string;
    serviceCode: string;
    productCode: string;
    productDescription: string;
    status: string;
    sourceType: string;
    coordinateX: string;
    coordinateY: string;
    department: string;
    province: string;
    district: string;
    address: string;
    ubigeoCode: string;
    phone: string;
    psprincipal: string;
    pslinea: string;
    psinternet: string;
    pstv: string;

    //CALCULATOR INFO
    codATis: string;
    codCms: string;
    rentaTotal: string;
    velInter: string;

    //comentado
    nombrePaquete: string;
    additionalAddress: string;
    productTypeCode: number;
    numDecos: number;
    productType: string;

}
