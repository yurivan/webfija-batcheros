export class Venta {

 orderId: string;
      dni: string;
      productName: string;
      saleDate: Date;
      price: number;
      promPrice: number;
      discount: number;
      monthPeriod: number;
      lineType: string;
      cashPrice: number;
      status: string;
      requestCode: string;
      statusRequest: string;
      cmsAtisStatus: string;
      clientName: string;
      commercialOperation: string;
      cancelDescription: string;
      enableDigitalInvoice: string;
      disableWhitePage: string;
  
}