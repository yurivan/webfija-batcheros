import { User } from '../model/user';
import { Customer } from '../model/customer';
import { Producto } from '../model/producto';
import { ProductOffering } from '../model/productOffering';
import { Sva } from '../model/sva';

export class Order {
	id: string;
	cip: string;
	type: string;
	status: string;
	user: User;
	customer: Customer;
	product: Producto;
	selectedOffering: ProductOffering;
	selectedSVA: any;
	sva: Sva[];
	equipment: any;
	equipmentName: string;
	disaffiliationWhitePagesGuide: boolean; //condicion preventa: PACKVERDE
	affiliationElectronicInvoice: boolean; //condicion preventa: PACKVERDE
	affiliationDataProtection: boolean; //condicion preventa: PROTECCION DATOS
	publicationWhitePagesGuide: boolean; //condicion preventa: PACKVERDE
	shippingContractsForEmail: boolean; //si es que se ingresa el correo electronico entonces el valor debe ser true
	parentalProtection: boolean;//Sprint 6 - condicion preventa: FILTRO WEB PARENTAL
	automaticDebit: boolean; //Sprint 21 - condicion preventa: FILTRO DEBITO AUTOMATICO
	originOrder: string;

	cambiaContrasenaDemanda: boolean//glazaror

	//glazaror se adicionan propiedades para scoring... se puede adicionar otro model especifico para el scoring
	departmentScoring: number;
	provinceScoring: number;
	districtScoring: number;

	scoringDataModel: any;
	showClientData: boolean;//glazaror sprint2

	editandoCliente: boolean; // Sprint6 - para controlar la edicion de datos del cliente
	ingresoCorreoObligatorio: boolean; // Sprint6 - para controlar ingreso obligatorio de correo

	condicionFiltroWebParental: boolean;
	condicionPackVerde: boolean;
	condicionTratamientoDatos: boolean;
	condicionDebitoAutomatico: boolean;

	datetimeSalesCondition: string;

	condicionesVentaSeleccionados: boolean = false;

	campaniaSeleccionada; // Sprint 10 - valido para alta pura y migra 

	estadoEnvioHDEC: string;// Sprint 11 - estado de envio a HDEC

	//Sprint 13 - guardado de campo normalizador hacia automatizador parte 2
	address_referencia: string;
	address_ubigeoGeocodificado: string;
	address_descripcionUbigeo: string;
	address_direccionGeocodificada: string;
	address_tipoVia: string;
	address_nombreVia: string;
	address_numeroPuerta1: string;
	address_numeroPuerta2: string;
	address_cuadra: string;
	address_tipoInterior: string;
	address_numeroInterior: string;
	address_piso: string;
	address_tipoVivienda: string;
	address_nombreVivienda: string;
	address_tipoUrbanizacion: string;
	address_nombreUrbanizacion: string;
	address_manzana: string;
	address_lote: string;
	address_kilometro: string;
	address_nivelConfianza: string;

	perfilVenta: string;
	ofertasBi: boolean;

	ventaEcha: boolean;
	flujoAgendamiento: boolean;//flujo de agendamiento
	constructor() {
		this.status = 'N';
		this.showClientData = false;//glazaror sprint2
		this.editandoCliente = false;// Sprint6 - para controlar la edicion de datos del cliente
		this.ingresoCorreoObligatorio = false;// Sprint6 - para controlar ingreso obligatorio de correo
		this.offline = new offline;
	}

	atis_cms: string;

	whatsapp: boolean;
	flagFtth: boolean;

	offline: offline;

}
class offline {
	flag: string ;
	body:  Map<string, string>;
	constructor(){
		this.flag = '';
		this.body = new Map<string, string>()
	}
}