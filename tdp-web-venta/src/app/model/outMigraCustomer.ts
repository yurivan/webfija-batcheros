export class OutMigraCustomer {
    parkType: string;
    productDescription: string;
    status: string;
    sourceType: string;
    subscriber: string;
    department: string;
    province: string;
    district: string;
    address: string;
    ubigeoCode: string;
    phone: string;
    psprincipal: string;
    pslinea: string;
    psinternet: string;
    pstv: string;
    numDecos: string;
    rentaTotal: string;
    velInter: string;
}