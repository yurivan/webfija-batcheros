import { Lista } from '../model/lista';

export class Estados {
    nombre: string;
    color: string;
    cantidad: number;
    lista: Lista[];
}