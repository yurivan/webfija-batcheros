export class Sva {

  	code: string;
  	description: string;
  	type: string;
 	unit: string;
    cost: string;
    paymentDescription: string;
    id: number;
    financingMonth: number;
    bloquePadre: string;
    clickPadre: boolean;    
    svaMass : boolean;

}