export class Lista {
    id: string;
    name: string;
    document: string;
    codigoPedido: string;
    producto: string;
    tecnologia: string;
    estado: string;
    idtransaccion: string;
    codigoAtis: string;
    anio: string;
    mes: string;
    dia: string;
    hora: string;
    estadocolor: string;
    tipoCaida: string;
    motivoCaida: string;
    telefonoAsignado: string;
    telefonoCliente: string;
    telefonoCliente2: string;
    pagoAdelantado: string;
    direccion: string;
    fVenta: string;
    precio: string;
    preciopromocional: string;
    operacioncomercial: string;
    tipolinea: string;
    descuento: string;
    periodopromocional: string;
    facturaelectronica: string;
    paginasblancas: string;
    ordenTrabajo: string;
    decosSmart: number;
    decosHD: number;
    decosDVR: number;

    codigocip: string;
    estadocip:string;
    debitoAuto: string;
    tratamientDatos: string;
    webParental: string;
    reciboElect: string;

    //Opcional
    color:string;
    fechaCompleta: string;
    offline: string;
}