export class Recupero{
    orderId: string;
    motivoCaida: string;
    fechaPago: Date;
    direccion: string;
    telefono: string;
    svaDvr: number;
    svaHd: number;
    svaSmart: number;
    docnumber: string;
}