//sprint 8
export class Adresses {
    direccion: string;
    xPunto: string;
    yPunto: string;
    referencia: string;

    //Sprint 13
    ubigeoGeocodificado: string;
    descripcionUbigeo: string;
    direccionGeocodificada: string;
    tipoVia: string;
    nombreVia: string;
    numeroPuerta1: string;
    numeroPuerta2: string;
    cuadra: string;
    tipoInterior: string;
    numeroInterior: string;
    piso: string;
    tipoVivienda: string;
    nombreVivienda: string;
    tipoUrbanizacion: string;
    nombreUrbanizacion: string;
    manzana: string;
    lote: string;
    kilometro: string;
    nivelConfianza: number;
}