//Argumentarios mensajes a mostrar al usuario
export class Argumentarios {
    
        id : string;
        canal : string;
        entidad : string;
        nivel : string;
        flujo : string;
        mensaje : string;
        pantalla : string;
    }