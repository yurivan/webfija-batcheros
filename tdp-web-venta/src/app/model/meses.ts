import { Estados } from '../model/estados';
import { Dias } from '../model/dias';

export class Meses {
    order: string;
    anio: string;
    mes: string;
    name: string;
    color: string;
    clientestotal: number;
    estados: Estados[];
    dias: Dias[];
}