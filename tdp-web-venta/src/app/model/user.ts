export class User {
  username: string;
  password: string;
  name: string;
  lastName: string;
  userId: string;
  resetPwd: string;
  channel : string;
  entity : string;
  group : string;
  groupPermission : string;
  sellerChannelAtis : string;
  sellerSegment : string;
  campaigns : string;
  
  sellerChannelEquivalentCampaign : string;
  sellerEntityEquivalentCampaign : string;

  niveles: string;

  typeFlujoTipificacion: string;
}
