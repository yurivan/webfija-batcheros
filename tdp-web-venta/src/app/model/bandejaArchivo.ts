export class BandejaArchivo {
    orderId: string;
    saleDate: string;
    statusAudio: string;
    statusAudioDescripcion: string;
    codigoVendedor: string;
    whatsapp: number;
}