import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';
import { User } from '../model/user';
import { Order } from '../model/order';
import { Router } from '@angular/router';
import { LocalStorageService } from '../services/ls.service';
import { AppComponent } from '../../app.component';

@Component({
    moduleId: 'login',
    selector: 'login',
    templateUrl: './changePassword.template.html'
})
export class ChangePasswordComponent implements OnInit {
    model: User = new User;
    error: boolean;
    loading: boolean;
    //
    password: string;
    confirmPassword: string;
    userId: string = '';
    messageLogin: string = '';
    info: boolean;
    passwordOld: string;
	success: boolean;

    constructor(private loginService: LoginService,
        private userService: UserService,
        private router: Router,
        private ls: LocalStorageService, private appComponent: AppComponent) {

        this.info = true;
        this.messageLogin = "Estimado usuario por seguridad necesita cambiar su contrase\u00F1a ingresando los siguientes datos: ";
		this.success = false;//glazaror
    }

    ngOnInit() {
        
        let order: Order = this.ls.getData();
        if (order && order.user) {
			if (order.cambiaContrasenaDemanda != null && order.cambiaContrasenaDemanda == true) { //glazaror
				// info = false... para no mostrar mensaje informativo de primer cambio de contrasena obligatorio
				this.info = false;
			}
            //console.log('resetPassword '+order.user.resetPwd);
            /*if(order.user.resetPwd == '0'){
                this.router.navigate(['/login'], {});
            }else{*/
            this.userId = order.user.userId;    
            //}
            
        }

    }

    doChangePassword() {
        //console.log("clave " + this.password + " confirm clave " + this.confirmPassword);
		
		if(this.password.length < 8){
		    this.messageLogin = "Por favor, verifique que las contrase\u00F1as tengan al menos ocho (8) caracteres.";
			this.error = true;
			this.info = false;
			this.password = '';
			this.passwordOld = '';
			this.confirmPassword = '';
            return;
		}

		if (this.password != this.confirmPassword) {
			this.messageLogin = "Por favor, verifique que las contrase\u00F1as coincidan.";
			this.error = true;
			this.info = false;
			this.password = '';
			this.passwordOld = '';
			this.confirmPassword = '';
		} else {

			this.error = false;
			this.info = true;

			var _this = this;
			this.loading = true;
			//console.log('clave nueva ' + this.password + ' nueva clave confirmada ' + this.confirmPassword);

			this.loginService.changePassword(this.userId, this.passwordOld, this.password)
				.subscribe(response => {
					if (response.responseCode == '0') {
						_this.error = false;
						_this.loading = false;
						//_this.router.navigate(['/home'], {});
						_this.success = true;
						
						// Sprint 6 - actualizamos el resetpwd del user
						let order: Order = _this.ls.getData();
						order.user.resetPwd = '0';
						_this.ls.setData(order);
						_this.appComponent.session();

					} else {
						if (response.responseCode == '1') {
							_this.error = true;
							_this.info = false;
							_this.loading = false;
							_this.password = '';
							_this.passwordOld = '';
							_this.confirmPassword = '';
							//_this.messageLogin = response.responseMessage; //glazaror se comenta el mensaje retornado del servidor ya que es muy general
							_this.messageLogin = "Contrase\u00F1a actual incorrecta";
						} else {
							//glazaror... se verifica que la contrase�a nueva sea distinta a la actual
							if (_this.passwordOld == _this.password) {
								_this.messageLogin = "La nueva contrase\u00F1a debe ser distinta a la actual.";
								_this.error = true;
								_this.info = false;
								_this.password = '';
								_this.passwordOld = '';
								_this.confirmPassword = '';
								_this.loading = false;
							} else {
								_this.messageLogin = response.responseMessage;
								_this.error = true;
								_this.info = false;
								_this.loading = false;
								_this.password = '';
								_this.passwordOld = '';
								_this.confirmPassword = '';
							}
						}

					}

				}, err => {
					_this.error = true;
					_this.loading = false;
				}
            );
        }
		
    }
}
