import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderService } from '../services/order.service';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';
import { ProgressBarService } from '../services/progressbar.service';

import {Customer } from '../model/customer'; 
import { User } from '../model/user'; 



var $ = require('jquery');
@Component({
	moduleId: 'closeOut',
	selector: 'closeOut',
	templateUrl: './closeOut.template.html',
	providers: [OrderService]
})
export class CloseOutComponent implements OnInit{
	order: Order;
    orderService: OrderService;
    loading : boolean = false;
    mostrarIDventa: boolean = false;


    model: Customer = new Customer;
	newCustomer: Customer[];



	constructor(private router: Router, 
		orderService: OrderService, 
		private ls: LocalStorageService,
		private progresBar: ProgressBarService){
		this.router = router;
		this.orderService = orderService;
		this.order = this.ls.getData();
  		this.order.status="N";
  		this.ls.setData(this.order);
	}
	ngOnDestroy(){
		localStorage.removeItem("ventaFinalizada")
	}
	ngOnInit(){

		localStorage.setItem("ventaFinalizada","ok")
		history.pushState(null, document.title, location.href);
		window.addEventListener('popstate', function (event)
		{
		  history.pushState(null, document.title, location.href);
		//   alert("La venta esta Realizada no puede retroceder.")
		  
		});


                var _this = this;
		this.progresBar.getProgressStatus()
		this.order = JSON.parse(localStorage.getItem('order'));
		//console.log("this.order " + JSON.stringify(this.order))
		var userPerfil = this.order.user.typeFlujoTipificacion;
		//console.log("userPerfil "+userPerfil)
		//console.log("this.order 2 " + JSON.stringify(this.order.condicionPackVerde))		 
		if (userPerfil.indexOf("PRESENCIAL") == 0){
			_this.mostrarIDventa=false
		}else{
			_this.mostrarIDventa=true
		}
	}
}
