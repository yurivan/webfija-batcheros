import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


import { CustomerService } from '../services/customer.service';
import { Customer } from '../model/customer';
import { Order } from '../model/order';
import { OnOff } from '../model/onoff';
import { LocalStorageService } from '../services/ls.service';
import { HistoricoService } from '../services/historico.service';
import { Tracking } from '../model/tracking';


// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';

import { ProgressBarService } from '../services/progressbar.service';
 

var $ = require('jquery');

@Component({
	moduleId: 'pruebadeconcepto',
	selector: 'pruebadeconcepto',
	templateUrl: './pruebadeconcepto.template.html',
	providers: [CustomerService]
})
//--
export class pruebadeconceptocomponent implements OnInit {
	
	newCustomer: Customer[];

	order: Order;
	onoff: OnOff;
	modelDirectio: string = "";
	selectedType: any;
	documentNumber: any;
	isTrue = true;
	loading = false;
	previousUrl;
	errorDocumentType: boolean;
	errorDocumentNumber: boolean;


	showDni: boolean = true;
	showCarneExtranjeria: boolean = false;
	docNroDni: string;
	docNroCarnetExt: string;
	errorConexion: string;
	msgError: boolean = false;

	// Sprint 7... tipos de documento a mostrar son configurables
	showTipoDocumentoDNI: boolean = true;
	showTipoDocumentoCEX: boolean = false;
	showTipoDocumentoPAS: boolean = false;
	showTipoDocumentoRUC: boolean = false;

	showPasaporte: boolean = false;
	showRUC: boolean = false;
	blackListPhone: string[];

	mjsOnOff: string;

	constructor(private router: Router,
		private customerService: CustomerService,
		private ls: LocalStorageService, 
		private progresBar: ProgressBarService) {

		this.router = router;
		this.customerService = customerService;
		this.ls = ls;
	}

	ngOnInit() {
		this.onoff = JSON.parse(localStorage.getItem("dataOnOff")); 
		this.order = JSON.parse(localStorage.getItem('order'));
	 
	 
		
	}
}
