import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';
import { OrderService } from '../services/order.service';
import { BlackListService } from '../services/blacklist.service';

import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';

// Sprint 6 - se adiciona subscription y messageservice
//import { Subscription } from 'rxjs/Subscription';
import { MessageService } from '../services/message.service';
var $ = require('jquery');
import { ProgressBarService } from '../services/progressbar.service';
import { BoundCallbackObservable } from 'rxjs/observable/BoundCallbackObservable';
import { Parameters } from '../model/parameters';
import { stringify } from 'querystring';


(<any>window).jQuery = $

@Component({
	moduleId: 'salecondition',
	selector: 'salecondition',
	templateUrl: './salecondition.template.html',
	providers: [CustomerService, OrderService]
})

export class SaleConditionComponent implements OnInit {
	@ViewChild('fileInput') inputEl: ElementRef;
	conditions: Parameters[] = [];
	order: Order;
	canalEntidad: Parameters;
	closeSaleError: boolean;
	closeSaleErrorMessage: string;
	private loading: boolean;
	private condicionFiltroWebParental: boolean;
	private condicionPackVerde: boolean;
	private condicionTratamientoDatos: boolean;
	private condicionDebitoAutomatico: boolean;
	correoElectronicoObligatorio: boolean = false;
	mensajeValidacionDatosCliente: string;// Sprint 6 - mensaje de validacion celular/correo obligatorios

	private loadError: boolean;
	private mensaje_error: String;
	disaffiliationWhitePagesGuide = true;
	affiliationElectronicInvoice = true;
	affiliationDataProtection = true;
	publicationWhitePagesGuide = true;
	shippingContractsForEmail = true;

	//checks de pack verde
	isCheckedVER1: boolean = true;
	isCheckedVER2: boolean = false;

	//checks de tratamiento de datos
	isCheckedTRA1: boolean = true;
	isCheckedTRA2: boolean = false;

	//checks de filtro parental
	isCheckedFWP1: boolean = false;
	isCheckedFWP2: boolean = false;

	//checks de filtro debito automatico
	isCheckedDAU1: boolean = false;
	isCheckedDAU2: boolean = false;

	showmodalcorreo: boolean = false;

	//correo electronico
	correoElectronico: string;
	validarCorreo: string;

	showAlert: boolean;
	mensajeError: string;

	datetimeSalesCondition: string;

	existVER: boolean = false;
	existTRA: boolean = false;
	existFWP: boolean = false;
	existDAU: boolean = false;



	constructor(private router: Router,
		private customerService: CustomerService,
		private orderService: OrderService,
		private ls: LocalStorageService,
		private messageService: MessageService,
		private progresBar: ProgressBarService,
		private blackListService: BlackListService) {
		this.router = router;
		this.customerService = customerService;
		this.closeSaleError = false;
		this.closeSaleErrorMessage = '';

		// Sprint 6 - 4 datos adicionales
		this.mensajeValidacionDatosCliente = "";
		this.condicionFiltroWebParental = false;
		this.condicionPackVerde = false;
		this.condicionTratamientoDatos = false;
		this.condicionDebitoAutomatico = false;

		this.showmodalcorreo = false;
	}

	ngOnInit() {

		this.progresBar.getProgressStatus()

		this.loading = true;
		this.order = JSON.parse(localStorage.getItem('order'));
		this.canalEntidad = JSON.parse(localStorage.getItem('canalEntidad'))
		//console.log("this.order.disaffiliationWhitePagesGuide " + this.order.disaffiliationWhitePagesGuide)
		//console.log("this.order.affiliationElectronicInvoice " + this.order.affiliationElectronicInvoice)
		//console.log("this.order.publicationWhitePagesGuide " + this.order.publicationWhitePagesGuide)

		if (this.order.disaffiliationWhitePagesGuide) {
			if (this.order.affiliationElectronicInvoice) {
				if (!this.order.publicationWhitePagesGuide) {
					this.condicionPackVerde = true;
				} else {
					this.condicionPackVerde = false;
				}
			}
		}

		/*if (this.order.disaffiliationWhitePagesGuide &&
			this.order.affiliationElectronicInvoice &&
			!this.order.publicationWhitePagesGuide) {*/
		/*this.condicionPackVerde = this.order.disaffiliationWhitePagesGuide
		this.condicionPackVerde = this.order.disaffiliationWhitePagesGuide*/
		//this.condicionPackVerde = true;
		/*} else {
			this.condicionPackVerde = false;

		}*/
		if (this.order.parentalProtection != undefined) {
			//console.log("this.order.parentalProtection " + this.order.parentalProtection)
			//console.log("this.condicionFiltroWebParental " + this.condicionFiltroWebParental)
			this.condicionFiltroWebParental = this.order.parentalProtection
		}
		if (this.order.affiliationDataProtection != undefined) {
			//console.log("this.order.affiliationDataProtection " + this.order.affiliationDataProtection)
			//console.log("this.condicionTratamientoDatos " + this.condicionTratamientoDatos)
			this.condicionTratamientoDatos = this.order.affiliationDataProtection
		}
		if (this.order.automaticDebit != undefined) {
			//console.log("this.order.affiliationDataProtection " + this.order.automaticDebit)
			//console.log("this.condicionTratamientoDatos " + this.condicionDebitoAutomatico)
			this.condicionDebitoAutomatico = this.order.automaticDebit
		}
		let _this = this;
		this.customerService.getSaleConditions(this.order)
			.subscribe(
				data => {
					if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
						_this.conditions = data.responseData.conditions;
						_this.getData();
						_this.loading = false;

					} else {
						alert(data.responseMessage);
					}
				},
				err => {
					console.log(err)
				}
			);

		$("#collapse1").on("hide.bs.collapse", function () {
			$("#hrefCollapse").html('<span class="glyphicon glyphicon-menu-left"></span>');

			$("#tablaDiv").removeClass('col-lg-9');
			$("#buttonDiv").removeClass('col-lg-3');

			$("#tablaDiv").addClass('col-lg-11');
			$("#buttonDiv").addClass('col-lg-1');
		});
		$("#collapse1").on("show.bs.collapse", function () {
			$("#hrefCollapse").html('Resumen <span class="glyphicon glyphicon-menu-right"></span>');
			$("#tablaDiv").removeClass('col-lg-11');
			$("#buttonDiv").removeClass('col-lg-1');

			$("#tablaDiv").addClass('col-lg-9');
			$("#buttonDiv").addClass('col-lg-3');
		});

		//verificamos si es que el cliente tiene registrado un correo
		if (this.order.customer.email != null) {
			this.correoElectronico = this.order.customer.email;
			if (this.correoElectronico) {
				$("#email").addClass("has-content")
			} else {
				$("#email").removeClass("has-content")
			}

		}

		this.aplicarEfectos();
	}

	aplicarEfectos() {
		$(document).ready(() => {
			if ($("#email").val() != "") {
				$("#email").addClass("has-content");
			} else {
				$("#email").removeClass("has-content");
			}

		})
	}

	getData() {
		for (let i = 0; i < this.conditions.length; i++) {
			let array = this.conditions[i].strValue.split('|')
			this.conditions[i].titulo = array[0];
			this.conditions[i].contenido = array[1];
			switch (this.conditions[i].category) {
				case "PACKVERDE":
					switch (this.conditions[i].strfilter) {
						case "0":
							this.isCheckedVER1 = true;
							this.isCheckedVER2 = false;
							this.condicionPackVerde = true;
							break;
						case "1":
							this.isCheckedVER1 = false;
							this.isCheckedVER2 = true;
							this.condicionPackVerde = false;
							break;
						case "2":
							this.isCheckedVER1 = false;
							this.isCheckedVER2 = false;
							break;
					}
					this.existVER = true;
					break;
				case "TRATAMIENTODATOS":
					switch (this.conditions[i].strfilter) {
						case "0":
							this.isCheckedTRA1 = true;
							this.isCheckedTRA2 = false;
							this.condicionTratamientoDatos = true;
							break;
						case "1":
							this.isCheckedTRA1 = false;
							this.isCheckedTRA2 = true;
							this.condicionTratamientoDatos = false;
							break;
						case "2":
							this.isCheckedTRA1 = false;
							this.isCheckedTRA2 = false;
							break;
					}
					this.existTRA = true;
					break;
				case "FILTROWEBPARENTAL":
					switch (this.conditions[i].strfilter) {
						case "0":
							this.isCheckedFWP1 = true;
							this.isCheckedFWP2 = false;
							this.condicionFiltroWebParental = true;
							break;
						case "1":
							this.isCheckedFWP1 = false;
							this.isCheckedFWP2 = true;
							this.condicionFiltroWebParental = false;
							break;
						case "2":
							this.isCheckedFWP1 = false;
							this.isCheckedFWP2 = false;
							break;
					}
					this.existFWP = true;
					break;
				case "DEBITOAUTOMATICO":
					switch (this.conditions[i].strfilter) {
						case "0":
							this.isCheckedDAU1 = true;
							this.isCheckedDAU2 = false;
							this.condicionDebitoAutomatico = true;
							break;
						case "1":
							this.isCheckedDAU1 = false;
							this.isCheckedDAU2 = true;
							this.condicionDebitoAutomatico = false;
							break;
						case "2":
							this.isCheckedDAU1 = false;
							this.isCheckedDAU2 = false;
							break;
					}
					this.existDAU = true;
					break;
			}
		}

		this.getDateTime();
		//this.inicializadorNo();
		this.evaluarCondicionesVentaDefault();
	}

	evaluarCondicionesVentaDefault() {
		//si es que las condiciones de venta ya han sido seleccionadas anteriormente entonces mostramos los checks que han sido seleccionados
		if (this.order.condicionesVentaSeleccionados) {
			//validamos los checks x default
			if (this.condicionPackVerde) {
				this.isCheckedVER1 = true;
				this.correoElectronicoObligatorio = true;
				this.isCheckedVER2 = false;
			} else {
				this.isCheckedVER1 = false;
				this.correoElectronicoObligatorio = false;
				this.isCheckedVER2 = true;
			}

			if (this.condicionFiltroWebParental) {
				this.isCheckedFWP1 = true;
				this.isCheckedFWP2 = false;
			} else {
				this.isCheckedFWP1 = false;
				this.isCheckedFWP2 = true;
			}

			if (this.condicionTratamientoDatos) {
				this.isCheckedTRA1 = true;
				this.isCheckedTRA2 = false;
			} else {
				this.isCheckedTRA1 = false;
				this.isCheckedTRA2 = true;
			}

			if (this.condicionDebitoAutomatico) {
				this.isCheckedDAU1 = true;
				this.isCheckedDAU2 = false;
			} else {
				this.isCheckedDAU1 = false;
				this.isCheckedDAU2 = true;
			}
		} /*else {
			// si es que aun no se han seleccionado los checks entonces cargamos los checks por default
			this.isCheckedVER1 = true;
			this.isCheckedVER2 = false;

			this.isCheckedTRA1 = true;
			this.isCheckedTRA2 = false;

			this.isCheckedFWP1 = false;
			this.isCheckedFWP2 = true;

			this.isCheckedDAU1 = false;
			this.isCheckedDAU2 = true;

			this.correoElectronicoObligatorio = true;
			this.condicionPackVerde = true;
			this.condicionTratamientoDatos = true;
			this.condicionDebitoAutomatico = true;


		}*/

	}

	inicializadorNo() {
		$('#VER2').prop('checked', true);
		$('#TRA2').prop('checked', true);
		$('#FWP2').prop('checked', true);
		$('#DAU2').prop('checked', true);
	}

	continuar() {
		this.showAlert = false;
		this.correoElectronico = $.trim($('#email').val());
		
		//si es q el correo electronico esta vacio lanzamos error
		if (!this.validateEmail(this.correoElectronico)) {
			$('#maxDecosWarning').html("Correo electrónico no válido.");
			$('#maxDecosWarning').show();
			return;
		}
		
		if (this.isCheckedVER1 && (this.correoElectronico == null || this.correoElectronico == "")) {
			this.showAlert = true;
			return;
		}

		if (this.isCheckedDAU1 && (this.correoElectronico == null || this.correoElectronico == "")) {
			this.showAlert = true;
			return;
		}

		if (this.isCheckedDAU1 == false && this.isCheckedDAU2 == false && this.existDAU) {
			$('#maxDecosWarning').html("Debe seleccionar una opción para Debito Automático.");
			$('#maxDecosWarning').show();
			return;
		}

		if (this.isCheckedFWP1 == false && this.isCheckedFWP2 == false && this.existFWP) {
			$('#maxDecosWarning').html("Debe seleccionar una opción para Web Parental.");
			$('#maxDecosWarning').show();
			return;
		}

		if (this.isCheckedTRA1 == false && this.isCheckedTRA2 == false && this.existTRA) {
			$('#maxDecosWarning').html("Debe seleccionar una opción para Tratamiento de Datos.");
			$('#maxDecosWarning').show();
			return;
		}

		if (this.isCheckedVER1 == false && this.isCheckedVER2 == false && this.existVER) {
			$('#maxDecosWarning').html("Debe seleccionar una opción para Pack Verde.");
			$('#maxDecosWarning').show();
			return;
		}


		var customer = this.ls.getData();

		//obtenemos los datos de la orden actualizada
		this.order = this.ls.getData();




		//guardamos las condiciones seleccionadas en localstorage

		//condicion de preventa PACKVERDE
		this.order.disaffiliationWhitePagesGuide = this.condicionPackVerde;
		this.order.affiliationElectronicInvoice = this.condicionPackVerde;
		this.order.publicationWhitePagesGuide = !this.condicionPackVerde;

		//condicion de preventa FILTROWEBPARENTAL
		this.order.parentalProtection = this.condicionFiltroWebParental;

		//condicion de preventa TRATAMIENTODATOS
		this.order.affiliationDataProtection = this.condicionTratamientoDatos;

		//condicion de preventa DEBITOAUTOMATICO
		this.order.automaticDebit = this.condicionDebitoAutomatico;

		//indicamos que las condiciones de venta ya han sido seleccionadas
		this.order.condicionesVentaSeleccionados = true;

		//condicion de envio de contrato por correo
		if (this.order.customer.email == null || this.order.customer.email == '') {
			this.order.shippingContractsForEmail = false;
		} else {
			this.order.shippingContractsForEmail = true;
		}

		//verificamos las condiciones seleccionadas:
		this.order.condicionFiltroWebParental = this.condicionFiltroWebParental;
		this.order.condicionPackVerde = this.condicionPackVerde;
		this.order.condicionTratamientoDatos = this.condicionTratamientoDatos;
		this.order.condicionDebitoAutomatico = this.condicionDebitoAutomatico;

		//Sprint 16
		this.order.datetimeSalesCondition = this.datetimeSalesCondition;

		//si es que isCheckedVER1 == true entonces obtenemos el correo electronico
		if (this.isCheckedVER1) {
			this.order.customer.email = this.correoElectronico;
		}

		if (this.isCheckedDAU1) {
			this.order.customer.email = this.correoElectronico;
		}

		//salvamos los datos
		this.ls.setData(this.order);

		//console.log("show order");
		//console.log(this.order);
		// this.order.customer.documentNumber = "15701923374"
		// this.order.customer.documentType = "CEX"
		let resultDoc = this.order.customer.documentNumber
		var valorCondicional = resultDoc.substring(0, 2)
		let validacion = false
		if ("10" == valorCondicional) {
			validacion = false
		} else {
			validacion = true

		}

		// Si es que el cliente se identifico con DNI entonces redireccionamos a la pantalla de RENIEC
// <<<<<<< Updated upstream
// 		if (this.order.customer && this.order.customer.documentType == "DNI") {
// 			this.router.navigate(['saleProcess/reniec']);
// 			// } else if(this.order.customer.documentType == "CEX") {
// 		} else if (this.order.customer && this.order.customer.documentType == "RUC") {
// 			if ("10" == valorCondicional) {
// 				this.router.navigate(['saleProcess/reniec']);
// 			} else {
// 				this.router.navigate(['/documenttype']);
// 			}
// 		}

// 		else {
// 			this.router.navigate(['/saleProcess/datoscliente']);
// =======
		if (this.order.customer && this.order.customer.documentType == "DNI") {
			if ((this.order.user.typeFlujoTipificacion=='REMOTO') && this.order.type=='A') {
				this.router.navigate(['/saleProcess/salesSummary']);
			}else{
				this.router.navigate(['saleProcess/reniec']);
			}

			
		// } else if(this.order.customer.documentType == "CEX") {
		}else if(this.order.customer && this.order.customer.documentType == "RUC"){
			if(this.order.user.typeFlujoTipificacion=='REMOTO'){
				this.router.navigate(['/saleProcess/salesSummary']);
			}else{
				if (valorCondicional == "20" || valorCondicional == "15" || valorCondicional == "17") {
					this.router.navigate(['saleProcess/searchRuc']);//Sprint 24 RUC
				}
				else if("10" == valorCondicional){
					this.router.navigate(['saleProcess/reniec']);
				}
				else {
					if(this.order.customer.nationality){
						this.router.navigate(['/saleProcess/salesSummary']);
						
					}else{
					this.router.navigate(['/documenttype']); 
					}
				}
			} 
			
		}  
	 
		else{
			this.router.navigate(['/saleProcess/datoscliente']); 
 
		}
		this.sendTiendaVenta();
	}

	selectYesNo(idCond) {
		var strId = idCond + "";
		switch (strId.substring(0, 3)) {
			case "FWP": {
				if (strId == "FWP1") {
					$("#" + idCond).prop('checked', true);
					$("#FWP2").prop('checked', false);

					this.isCheckedFWP1 = true;
					this.isCheckedFWP2 = false;
				} else {
					$("#" + idCond).prop('checked', true);
					$("#FWP1").prop('checked', false);
					this.isCheckedFWP1 = false;
					this.isCheckedFWP2 = true;
				}
				break;
			}
			case "VER": {
				if (strId == "VER1") {
					$("#" + idCond).prop('checked', true);
					$("#VER2").prop('checked', false);
					this.isCheckedVER1 = true;
					this.isCheckedVER2 = false;
				} else {
					$("#" + idCond).prop('checked', true);
					$("#VER1").prop('checked', false);
					this.isCheckedVER1 = false;
					this.isCheckedVER2 = true;
				}
				break;
			}
			case "TRA": {
				if (strId == "TRA1") {
					$("#" + idCond).prop('checked', true);
					$("#TRA2").prop('checked', false);
					this.isCheckedTRA1 = true;
					this.isCheckedTRA2 = false;
				} else {
					$("#" + idCond).prop('checked', true);
					$("#TRA1").prop('checked', false);
					this.isCheckedTRA1 = false;
					this.isCheckedTRA2 = true;
				}
				break;
			}

			case "DAU": {
				if (strId == "DAU1") {
					$("#" + idCond).prop('checked', true);
					$("#DAU2").prop('checked', false);
					this.isCheckedDAU1 = true;
					this.isCheckedDAU2 = false;
				} else {
					$("#" + idCond).prop('checked', true);
					$("#DAU1").prop('checked', false);
					this.isCheckedDAU1 = false;
					this.isCheckedDAU2 = true;
				}
				break;
			}
		}
	}

	getDateTime() {
		this.blackListService.getDateTime().subscribe(
			data => {
				this.datetimeSalesCondition = data.responseData;
				//console.log(this.datetimeSalesCondition)
			},
			err => { }
		);
	}

	seleccionar(value, category, item) {
		this.getDateTime();
		//obtenemos los datos de la orden actualizada
		this.order = this.ls.getData();
		if ("PACKVERDE" == category) {

			var mensaje = "";
			if (this.order.customer.email == "" || this.order.customer.email == null) {
				if (value.target.checked && item == 1) {
					mensaje = "CORREO OBLIGATORIO";
					this.correoElectronicoObligatorio = true;
					this.order.ingresoCorreoObligatorio = true;
					this.order.editandoCliente = true;
					this.ls.setData(this.order);
				} else {
					mensaje = "CORREO OPCIONAL SIN EDITAR";
					this.correoElectronicoObligatorio = false;
					$('#maxDecosWarning').hide();
					this.order.ingresoCorreoObligatorio = false;
					this.order.editandoCliente = false;
					this.ls.setData(this.order);
				}
			} else {
				if (value.target.checked && item == 1) {
					mensaje = "NOTIFICACION CORREO OBLIGATORIO";
				} else {
					mensaje = "NOTIFICACION CORREO OPCIONAL";
				}
			}
			this.messageService.sendMessage(mensaje);
		}
		if ("PACKVERDE" == category) {
			if (item == 1) {
				this.condicionPackVerde = value.target.checked;
				this.correoElectronicoObligatorio = true;
			} else if (item == 2) {
				this.condicionPackVerde = false;
				this.correoElectronicoObligatorio = false;
				$('#maxDecosWarning').hide();
			}
		} else if ("FILTROWEBPARENTAL" == category) {
			if (item == 1) {
				this.condicionFiltroWebParental = value.target.checked;
			} else if (item == 2) {
				this.condicionFiltroWebParental = false;
			}
		} else if ("TRATAMIENTODATOS" == category) {
			if (item == 1) {
				this.condicionTratamientoDatos = value.target.checked;
			} else if (item == 2) {
				this.condicionTratamientoDatos = false;
			}
		} else if ("DEBITOAUTOMATICO" == category) {
			if (item == 1) {
				this.condicionDebitoAutomatico = value.target.checked;
			} else if (item == 2) {
				this.condicionDebitoAutomatico = false;
			}
		}
	}

	// Sprint 4 - Para cerrar la ventana de dialogo "Datos del Cliente"
	cerrarDialogo(nombreModal) {
		$("#" + nombreModal).modal('hide');
	}

	// Copiado de SaleSummary
	sendTiendaVenta() {
		this.loading = true;
		this.order.disaffiliationWhitePagesGuide = this.disaffiliationWhitePagesGuide;
		this.order.affiliationElectronicInvoice = this.affiliationElectronicInvoice;
		this.order.affiliationDataProtection = this.affiliationDataProtection;
		this.order.publicationWhitePagesGuide = this.publicationWhitePagesGuide;
		this.order.shippingContractsForEmail = this.shippingContractsForEmail;

		let webOrder = {
			userId: this.order.user.userId,
			phone: this.order.product.phone,
			department: this.order.product.department,
			province: this.order.product.province,
			district: this.order.product.district,
			address: this.order.product.address,
			latitudeInstallation: this.order.product.coordinateY ? this.order.product.coordinateY : 0,
			longitudeInstallation: this.order.product.coordinateX ? this.order.product.coordinateX : 0,
			addressAdditionalInfo: this.order.product.additionalAddress ? this.order.product.additionalAddress : '',
			sva: [],
			customer: this.order.customer,
			product: this.order.selectedOffering,
			type: this.order.type,
			disaffiliationWhitePagesGuide: this.order.disaffiliationWhitePagesGuide,
			affiliationElectronicInvoice: this.order.affiliationElectronicInvoice,
			affiliationDataProtection: this.order.affiliationDataProtection,
			publicationWhitePagesGuide: this.order.publicationWhitePagesGuide,
			shippingContractsForEmail: this.order.shippingContractsForEmail,
			originOrder: this.order.user.groupPermission,
			// Sprint 5 - campos adicionales al cerrar la venta: nombrePuntoVenta y entidad
			entidad: this.order.user.entity,
			nombrePuntoVenta: this.order.user.group,
			// Sprint 6 - condicion preventa filtro web parental
			parentalProtection: this.order.parentalProtection,
			// Sprint 9 - Nombre del producto de origen (solo servirá para flujo SVA)
			sourceProductName: this.order.product.productDescription,
			// Sprint 10 - Campania seleccionada para altas y migras
			campaniaSeleccionada: this.order.campaniaSeleccionada,
			// Sprint 13 - Envio datos normalizador hacia automatizador parte 2
			address_referencia: this.order.address_referencia,
			address_ubigeoGeocodificado: this.order.address_ubigeoGeocodificado,
			address_descripcionUbigeo: this.order.address_descripcionUbigeo,
			address_direccionGeocodificada: this.order.address_direccionGeocodificada,
			address_tipoVia: this.order.address_tipoVia,
			address_nombreVia: this.order.address_nombreVia,
			address_numeroPuerta1: this.order.address_numeroPuerta1,
			address_numeroPuerta2: this.order.address_numeroPuerta2,
			address_cuadra: this.order.address_cuadra,
			address_tipoInterior: this.order.address_tipoInterior,
			address_numeroInterior: this.order.address_numeroInterior,
			address_piso: this.order.address_piso,
			address_tipoVivienda: this.order.address_tipoVivienda,
			address_nombreVivienda: this.order.address_nombreVivienda,
			address_tipoUrbanizacion: this.order.address_tipoUrbanizacion,
			address_nombreUrbanizacion: this.order.address_nombreUrbanizacion,
			address_manzana: this.order.address_manzana,
			address_lote: this.order.address_lote,
			address_kilometro: this.order.address_kilometro,
			address_nivelConfianza: this.order.address_nivelConfianza,
			// Sprint 15 - Anthony
			serviceType: this.order.product.parkType,
			cmsServiceCode: this.order.product.serviceCode,
			cmsCustomer: this.order.product.subscriber
		};


		//fix 
		//console.log("webOrder Tienda");
		//console.log(webOrder);

		for (let i = 0; i < this.order.sva.length; i++) {
			let sva = this.order.sva[i];
			webOrder.sva.push(sva.id);

		}

		localStorage.setItem("webOrder", JSON.stringify(webOrder));
		
		this.loading = true;
	
	}

	mostrarError(err) {
		this.loading = false;
		this.loadError = true;
		if (err.responseMessage)
			this.mensaje_error = err.responseMessage;
		else
			this.mensaje_error = "No se pudo cerrar la venta, Por favor vuelva a intentarlo."
	}

	deleteSpaceEmail(event) {
		//console.log(event);
		$.trim($('#email').val());



		this.checkEmailArroba()

		$('#email').mailtip({

		});


		// let correo = this.correoElectronico;

		// if(correo.indexOf("@") > 0){
		// 	////console.log("@"+correo.indexOf("@")) 
		// 	//console.log("Ingresando data positivo") 
		// 	this.showmodalcorreo = true;
		// }else{
		// 	//console.log("Aún nada") 
		// }

	}

	aceptarProveedor() {
		let texto = $("#proveedor option:selected").text();
		//console.log(texto)
		this.showmodalcorreo = false;

		this.correoElectronico = this.correoElectronico + texto;

	}

	cancel() {
		this.showmodalcorreo = false;
	}

	validateEmail(o) {
		if (!this.correoElectronicoObligatorio && (o == null || o == "")) {
			return true;
		}
		var email_regex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
		if (!email_regex.test(o)) {
			return false;
		}
		else {
			return true;
		}
	}

	validateEmail2(o) {
		if (!this.correoElectronicoObligatorio && (o == null || o == "")) {
			//console.log("No arroba");
			return true;

		}
		var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
		if (this.correoElectronicoObligatorio && !email_regex.test(o)) {
			//console.log("  arroba");
			return false;
		}
		else {
			//console.log("No else arroba else");
			return true;
		}
	}

	retroceso() {
		if (!this.validateEmail(this.correoElectronico)) {
			$('#maxDecosWarning').html("Correo electrónico no válido.");
			$('#maxDecosWarning').show();
			return;
		}
	}

	checkEmailArroba() {
		'use strict';

		(function ($) {
			// invalid email char test regexp
			var INVALIDEMAILRE = /[^\u4e00-\u9fa5_a-zA-Z0-9]/;
			// is support oninput event
			var hasInputEvent = 'oninput' in document.createElement('input');
			// is ie 9
			var ISIE9 = /MSIE 9.0/i.test(window.navigator.appVersion || window.navigator.userAgent);

			/**
			 * is a number
			 * @param value
			 * @returns {boolean}
			 */
			function isNumber(value) {
				return typeof value === 'number' && isFinite(value);
			}

			/**
			 * create popup tip
			 * @param input
			 * @param config
			 * @returns {*}
			 */
			function createTip(input, config) {
				var tip = null;

				// only create tip and binding event once
				if (!input.data('data-mailtip')) {
					var wrap = input.parent();

					// set parent node position
					!/absolute|relative/i.test(wrap.css('position')) && wrap.css('position', 'relative');
					// off input autocomplete
					input.attr('autocomplete', 'off');

					var offset = input.offset();
					var wrapOffset = wrap.offset();

					tip = $('<ul class="ui-mailtip" style="display: none; float: none; '
						+ 'position:absolute; margin: 0; padding: 0; z-index: '
						+ config.zIndex + '"></ul>');

					// insert tip after input
					input.after(tip);

					// set tip style
					tip.css({
						top: offset.top - wrapOffset.top + input.outerHeight() + config.offsetTop,
						left: offset.left - wrapOffset.left + config.offsetLeft,
						width: config.width === 'input' ? input.outerWidth() - tip.outerWidth() + tip.width() : config.width
					});

					// when width is auto, set min width equal input width
					if (config.width === 'auto') {
						tip.css('min-width', input.outerWidth() - tip.outerWidth() + tip.width());
					}

					// binding event
					tip.on('mouseenter mouseleave click', 'li', function (e) {
						var selected = $(this);

						switch (e.type) {
							case 'mouseenter':
								selected.addClass('hover');
								break;
							case 'click':
								var mail = selected.attr('title');

								input.val(mail).focus();
								config.onselected.call(input[0], mail);
								break;
							case 'mouseleave':
								selected.removeClass('hover');
								break;
							default:
								break;
						}
					});

					// when on click if the target element not input, hide tip
					$(document).on('click', function (e) {
						if (e.target === input[0]) return;

						tip.hide();
					});

					input.data('data-mailtip', tip);
				}

				return tip || input.data('data-mailtip');
			}

			/**
			 * create mail list item
			 * @param value
			 * @param mails
			 * @returns {*}
			 */
			function createItems(value, mails) {
				var mail;
				var domain;
				var items = '';
				var atIndex = value.indexOf('@');
				var hasAt = atIndex !== -1;

				if (hasAt) {
					domain = value.substring(atIndex + 1);
					value = value.substring(0, atIndex);
				}

				for (var i = 0, len = mails.length; i < len; i++) {
					mail = mails[i];

					if (hasAt && mail.indexOf(domain) !== 0) continue;

					items += '<li title="' + value + '@' + mail + '"><p>' + value + '@' + mail + '</p></li>';
				}

				// active first item
				return items.replace('<li', '<li class="active"');
			}

			/**
			 * change list active state
			 * @param tip
			 * @param up
			 */
			function changeActive(tip, up) {
				var itemActive = tip.find('li.active');

				if (up) {
					var itemPrev = itemActive.prev();

					itemPrev = itemPrev.length ? itemPrev : tip.find('li:last');
					itemActive.removeClass('active');
					itemPrev.addClass('active');
				} else {
					var itemNext = itemActive.next();

					itemNext = itemNext.length ? itemNext : tip.find('li:first');
					itemActive.removeClass('active');
					itemNext.addClass('active');
				}
			}

			/**
			 * toggle tip
			 * @param tip
			 * @param value
			 * @param mails
			 */
			function toggleTip(tip, value, mails) {
				var atIndex = value.indexOf('@');

				// if input text is empty or has invalid char or begin with @ or more than two @, hide tip
				if (!value
					|| atIndex === 0
					|| atIndex !== value.lastIndexOf('@')
					|| INVALIDEMAILRE.test(atIndex === -1 ? value : value.substring(0, atIndex))) {
					tip.hide();
				} else {
					var items = createItems(value, mails);

					// if has match mails show tip
					if (items) {
						tip.html(items).show();
					} else {
						tip.hide();
					}
				}
			}

			/**
			 * exports
			 * @param config
			 * @returns {*}
			 */
			$.fn.mailtip = function (config) {
				var defaults = {
					mails: [
						'gmail.com', 'outlook.com', 'hotmail.com'
					],
					onselected: $.noop,
					width: 'auto',
					offsetTop: -1,
					offsetLeft: 0,
					zIndex: 10
				};

				config = $.extend({}, defaults, config);
				config.zIndex = isNumber(config.zIndex) ? config.zIndex : defaults.zIndex;
				config.offsetTop = isNumber(config.offsetTop) ? config.offsetTop : defaults.offsetTop;
				config.offsetLeft = isNumber(config.offsetLeft) ? config.offsetLeft : defaults.offsetLeft;
				config.onselected = $.isFunction(config.onselected) ? config.onselected : defaults.onselected;
				config.width = config.width === 'input' || isNumber(config.width) ? config.width : defaults.width;

				return this.each(function () {
					// input
					var input = $(this);
					// tip
					var tip = createTip(input, config);

					// binding key down event
					input.on('keydown', function (e) {
						// if tip is visible do nothing
						if (tip.css('display') === 'none') return;

						switch (e.keyCode) {
							// backspace
							case 8:
								// shit! ie9 input event has a bug, backspace do not trigger input event
								if (ISIE9) {
									input.trigger('input');
								}
								break;
							// tab
							case 9:
								tip.hide();
								break;
							// up
							case 38:
								e.preventDefault();
								changeActive(tip, true);
								break;
							// down
							case 40:
								e.preventDefault();
								//changeActive(tip);
								break;
							// enter
							case 13:
								e.preventDefault();

								var mail = tip.find('li.active').attr('title');

								input.val(mail).focus();
								tip.hide();
								config.onselected.call(this, mail);
								break;
							default:
								break;
						}
					});

					// binding input or propertychange event
					if (hasInputEvent) {
						input.on('input', function () {
							toggleTip(tip, this.value, config.mails);
						});
					} else {
						input.on('propertychange', function (e) {
							if (e.originalEvent.propertyName === 'value') {
								toggleTip(tip, this.value, config.mails);
							}
						});
					}

					// shit! ie9 input event has a bug, backspace do not trigger input event
					if (ISIE9) {
						input.on('keyup', function (e) {
							if (e.keyCode === 8) {
								toggleTip(tip, this.value, config.mails);
							}
						});
					}
				});
			};
		}($));

	}
}
