import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { SalesReportService } from '../services/salesReport.service';
import { ActivatedRoute } from '@angular/router';

@Component({
	moduleId: 'salesReportDetail',
	selector: 'salesReportDetail',
	templateUrl: './salesReportDetail.template.html',
	providers: [SalesReportService]
})

export class SalesReportDetailComponent implements OnInit {
	salesReportDetail = [];

	idOrder: string;
	constructor(private salesReportService: SalesReportService, private location: Location, private activeRoute: ActivatedRoute) {
		this.salesReportService = salesReportService;
		this.idOrder = "";
		this.cargarParametros(this.activeRoute);
	}

	cargarParametros(activeRoute) {
		activeRoute.queryParams.subscribe(params => {
			this.idOrder = params['id'];
		});
	}

	ngOnInit() {
		this.salesReportService.getData(this.idOrder)//"-Ka2uHX_ZFuk62SqEAaa"
			.subscribe(
				data => {
					if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
						this.salesReportDetail = data.responseData
						localStorage.setItem('salesReportDetail', JSON.stringify(Object.assign(this.salesReportDetail)))
					} else {
					//	console.log(data.responseMessage);
					}
				},
				err => {
					console.log(err)
				}
			);
	}

	goBack() {
		this.location.back();
	}
}
