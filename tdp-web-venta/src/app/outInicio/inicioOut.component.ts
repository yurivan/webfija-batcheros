import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

import { CustomerService } from '../services/customer.service';
import { Order } from '../model/order';
import { LocalStorageService } from '../services/ls.service';

// Sprint 7... tipos de documento a mostrar son configurables
import * as globals from '../services/globals';
import { ProgressBarService } from '../services/progressbar.service';
import { SvaService } from '../services/sva.service';
import { Customer } from '../model/customer';
import { Producto } from '../model/producto';

var $ = require('jquery');
class RequestDepartment {
	id: string
	value: any
}
@Component({
	moduleId: 'inicioOut',
	selector: 'inicioOut',
	templateUrl: './inicioOut.template.html',
	providers: [SvaService,CustomerService]
})

export class InicioOutComponent implements OnInit {

	order: Order;
	selectedType: any;
	documentNumber: any;
	loading = false;
	errorDocumentType: boolean;
	errorDocumentNumber: boolean;

	showDni: boolean = true;
	showCarneExtranjeria: boolean = false;
	showOtrosCE: boolean = false;
	docNroDni: string;
	docNroCarnetExt: string;
	docNroOtrosCE: string;

	showTipoDocumentoDNI: boolean = true;
	showTipoDocumentoCEX: boolean = false;
	showTipoDocumentoPAS: boolean = false;
	showTipoDocumentoRUC: boolean = false;
	showTipoDocumentoOtrosCE: boolean = false;

	showPasaporte: boolean = false;
	showRUC: boolean = false;

	showUbicacionDefault: boolean;

	deudaClienteMensaje: string;
	deudaClienteMensajeEstado: boolean = false;

	isTrue = true;

	//Sprint 24 RUC
	objRuc: any;
	ruc_SinData : boolean = false;
	private loadError: boolean;
	private mensaje_error: String;

	//Sprint 25 Contingencia RUC
	loadErrorDeuda : boolean = false;
	mensaje_error_ruc : string;

	@ViewChild('buttonEvaluar') buttonEvaluarRef: ElementRef;

	constructor(private router: Router,
		private ls: LocalStorageService,
		private svaService: SvaService,
		private progresBar: ProgressBarService,
		private customerService : CustomerService /*Sprint 24 RUC */) {
		this.router = router;
		this.ls = ls;
		this.svaService = svaService;

	}

	ngOnInit() {
		localStorage.removeItem('codigoInterConect');
		localStorage.removeItem('rentaInterConect');
		if (localStorage.getItem("dataRed")) {
			localStorage.removeItem("dataRed")
		}
		//$(document).ready(() => {
		//console.log("acciones")
		$('ul > #reportes').removeClass('none');
		$('ul > #Venderr').addClass('none');
		//})
		this.progresBar.getProgressStatus()
		$(".progres").prop("style", "visibility: hidden !important;");

		if (this.router.url !== "/searchUser") {
			this.isTrue = false;
		}

		this.order = this.ls.getData();
		this.order.perfilVenta="out";

		this.selectedType = 'DNI';
		this.documentNumber = null;
		this.docNroDni = '';
		this.showTipoDocumentoCEX = globals.SHOW_CEX;
		this.showTipoDocumentoPAS = globals.SHOW_PAS;
		this.showTipoDocumentoRUC = globals.SHOW_RUC;
		this.showTipoDocumentoOtrosCE = globals.SHOW_OTROSCE;

		this.showUbicacionDefault = false;

		this.aplicarEfectos();
	}

	aplicarEfectosChange() {

		$("#tablaDat").addClass("none");

		$(".input-effect input").val("");

		$(".input-effect input").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
		$(".paraselect select").val("");
		$(".paraselect select").focusout(function () {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		})
	}

	focusout(target) {
		this.focusoutById(target.id);
	}

	focusoutById(id) {
		var target = $("#" + id);
		if (target.val() != "") {
			target.addClass("has-content");
		} else {
			target.removeClass("has-content");
		}
	}

	aplicarEfectos() {
		var _this = this;
		$(document).ready(function () {
			var inputsIdSinEfecto = "";
			if (_this.order.departmentScoring) {
				inputsIdSinEfecto = "#divDocumentNumber";
				_this.focusoutById("docNroDni");
			}
			$(".input-effect:not(" + inputsIdSinEfecto + ") input").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})
			// obviamos el efecto para el tipo de documento
			// si es que tenemos una ubicacion por default entonces tambien debemos obviar el departamento, provincia y distrito
			var idsSinEfecto = "#divDocumentType";
			if (_this.showUbicacionDefault) {
				idsSinEfecto += ",#divDepartament,#divCity,#divDistrict";
			}
			$(".paraselect:not(" + idsSinEfecto + ") select").val("");
			$(".paraselect select").focusout(function () {
				if ($(this).val() != "") {
					$(this).addClass("has-content");
				} else {
					$(this).removeClass("has-content");
				}
			})
		})
	}

	onChangeSelectDocument(value) {

		if (value == "DNI") {
			this.docNroCarnetExt = '';
			this.docNroOtrosCE = '';
		} else if (value == "CEX") {
			this.docNroDni = '';
			this.docNroOtrosCE = ''
		} else {
			this.docNroCarnetExt = ''
			this.docNroDni = ''
		}

		switch (value) {

			case "DNI":

				this.showDni = true;
				this.showCarneExtranjeria = false;

				this.showPasaporte = false;
				this.showRUC = false;
				this.showOtrosCE = false;
				break;

			case "CEX":

				this.showCarneExtranjeria = true;
				this.showDni = false;

				this.showPasaporte = false;
				this.showRUC = false;
				this.showOtrosCE = false;

				break;

			case "Otros":

				this.showOtrosCE = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;

				this.showPasaporte = false;
				this.showRUC = false;
				break;

			case "PAS":

				this.showPasaporte = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showRUC = false;
				this.showOtrosCE = false;

				break;

			case "RUC":

				this.showRUC = true;
				this.showCarneExtranjeria = false;
				this.showDni = false;
				this.showPasaporte = false;
				this.showOtrosCE = false;

				break;

			default:
				this.showDni = true;
				this.showCarneExtranjeria = false;
				this.showPasaporte = false;
				this.showRUC = false;
				this.showOtrosCE = false;
				this.selectedType = "DNI";
				break;
		}
	}

	doEnter() {
		this.loading=true;
		let ooo = JSON.parse(localStorage.getItem('order'));
		let grupo = ooo.user.groupPermission;
		this.order.user = this.order.user;

		//Por el momento es para alta
		this.order.type = 'A';
		this.order.customer=new Customer();
		this.order.customer.documentNumber=this.docNroDni || this.docNroCarnetExt || this.docNroOtrosCE;
		this.order.customer.documentType=this.selectedType;
		//this.ls.setData(this.order);

		//Sprint 24 RUC
		if(this.order.customer.documentType == 'RUC'){
			this.consultaRuc();
		}
		else{
		this.buscarOfertas();
	}
	}

	buscarOfertas() {
		console.log('Order ' + JSON.stringify(this.order));
		let _this=this;
		this.documentNumber = this.docNroDni || this.docNroCarnetExt || this.docNroOtrosCE;
		this.svaService.getOfertasProduct(this.selectedType, this.documentNumber).subscribe(
			data => {
				if (data.responseData.length > 0) {
					_this.order.customer.firstName=data.responseData[0].customerName;
					_this.order.product=new Producto();
					_this.order.product.district=data.responseData[0].distrito;
					_this.order.product.province=data.responseData[0].provincia;
					_this.order.product.department=data.responseData[0].departamento;
					_this.order.ofertasBi = true;
					_this.ls.setData(_this.order);
					localStorage.setItem('ofertasBi', JSON.stringify(data.responseData));
					//Mijail
					localStorage.setItem('docCustomer', _this.selectedType + ',' + _this.documentNumber);
					_this.loading=false;
					_this.router.navigate(['ofertasBiOut']);
				} else {
					_this.loading=false;
					//alert("No se encontro resultado, se procedera de forma normal");
					_this.order.ofertasBi = false;
					_this.ls.setData(_this.order);
					//console.log(_this.order)
					_this.router.navigate(['scoringOut']);
				}
			},
			err => { 
				//console.log('error: ' + err);
			 }
		);
	}

	//Sprint 24 RUC
	consultaRuc(){
		this.ruc_SinData = false;
		this.loading = true;
		let _scope = this;
		_scope.order.offline.flag = '';
		this.customerService.getdataCustomerRuc(this.order.customer.documentNumber)
            .subscribe(
                data => {
					//console.log('Data_Response_RUC' + JSON.stringify(data));
					if(data.responseCode == '-2' && data.responseMessage.indexOf('FLUJO_OFFLINE') !== -1){
						let msj : string[] = data.responseMessage.split('|');
						_scope.order.offline.flag = _scope.order.offline.flag == '' ? '1' : _scope.order.offline.flag + ',1';
						_scope.ls.setData(_scope.order);
						//_scope.respuestaDataCustomer = true;
						//_scope.tiene_deuda = false;
						//_scope.serv_deuda_act = false;
						_scope.loadErrorDeuda = true;
						_scope.loading = false;
						_scope.loadError = false;
						_scope.mensaje_error_ruc = msj[1];
						return;
					}
                    else if (typeof data.responseData !== 'undefined' && data.responseData !== null) {
                        // add data perfil tienda
                        _scope.objRuc = data.responseData;
						//console.log("objeto Ruc " + JSON.stringify(_scope.objRuc))
						if(_scope.objRuc.estContrib != 'ACTIVO'){
							_scope.mostrarError('El estado del contribuyente no es activo.');
							_scope.loading = false;
							_scope.ruc_SinData = true;
						}
						else if(_scope.objRuc.condDomicilio != 'HABIDO'){
							_scope.mostrarError('La condición del domicilio del contribuyente no es valida.');
							_scope.loading = false;
							_scope.ruc_SinData = true;
						}
						else{
							_scope.order.customer.razonSocial = _scope.objRuc.razonSocial;
							_scope.order.customer.estContrib = _scope.objRuc.estContrib;
							_scope.order.customer.condDomicilio = _scope.objRuc.condDomicilio;
							_scope.order.customer.direccion = _scope.objRuc.direccion;
							_scope.order.customer.firstName = _scope.objRuc.razonSocial;
							_scope.buscarOfertas();
						}

                    } else {
						_scope.mostrarError('El RUC ingresado no existe o no está activo.');
						_scope.loading = false;
						_scope.ruc_SinData = true;
                    }
                },
                err => {
				//	console.log(err);
					_scope.loading = false;
					_scope.mostrarError('Ocurrio un Error al Consultar al contribuyente');
					_scope.ruc_SinData = true;
                }/*,
                () => {

                    if (_scope.objRuc != null) {
                        _scope.loadError = false;
                    } else {
						//console.log(JSON.stringify(_scope.objRuc));
						_scope.mostrarError('El RUC ingresado no existe o no está activo.');
						_scope.loading = false;
						_scope.ruc_SinData = true;
                    }
                }*/);

	}

	salirVenta(){
		this.router.navigate(['/principalOut']);
	}

	continuarVenta() {
		this.loadErrorDeuda = false;
		this.mensaje_error_ruc = "";
		this.loading = true;
		this.buscarOfertas();
	}

	mostrarError(mensaje) {
		this.loading = false;
		this.loadError = true;
		this.mensaje_error = mensaje;
	}

	metodoAlternativo() {
		let ooo = JSON.parse(localStorage.getItem('order'));
		let grupo = ooo.user.groupPermission;
		let order = new Order();
		order.user = this.order.user;

		//Por el momento es para alta
		order.type = 'A';
		this.ls.setData(order);

		this.router.navigate(['scoringOut']);
	}

	cancelarConsulta() {
		this.loadError = false;
		this.mensaje_error = "";
	}
}