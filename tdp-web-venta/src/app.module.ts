import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './app/home/home.component';
import { LoginComponent } from './app/login/login.component';
import { RegisterComponent } from './app/login/register.component';
import { SearchUserComponent } from './app/searchUser/searchuser.component';
import { SaleProcessComponent } from './app/saleProcess/saleprocess.component';
import { ParkComponent } from './app/park/park.component';
import { OfferingComponent } from './app/offering/offering.component';
import { SvaComponent } from './app/sva/sva.component';
import { VentaComponent } from './app/venta/venta.component';
import { SalesReportDetailComponent } from './app/salesReport/salesReportDetail.component';

import { DireccionComponent } from './app/direccion/direccion.component';

import { CampanasComponent } from './app/campanas/campanas.component';

import { SalesSummaryComponent } from './app/salesSummary/salesSummary.component';
import { ContractComponent } from './app/contract/contract.component';
import { ValidReniecComponent } from './app/reniec/validReniec.component';
import { CloseComponent } from './app/contract/close.component';

import { SearchProduct } from './app/offering/productname.pipe';
import { SearchProductType } from './app/offering/producttype.pipe';
import { SearchCampaign } from './app/offering/campaign.pipe';
import { Capitalize } from './app/home/capitalize.pipe';

import { AppRoutingModule } from './app-routing.module';

import { HttpService } from './app/services/http.service';
import { LoginService } from './app/services/login.service';
import { UserService } from './app/services/user.service';
import { LocalStorageService } from './app/services/ls.service'
import { BlackListService } from './app/services/blacklist.service';

import { AgendamientoService } from './app/services/agendamiento.service';

import { ReporteService } from './app/services/reporteVenta.service';

import { MessageService } from './app/services/message.service'//Sprint 6

import { AuthGuard } from './app/authGuard.injectable';

import { ChangePasswordComponent } from './app/changePassword/changePassword.component';

import { CancelSaleComponent } from './app/saleProcess/cancelsale.component';

import { ResetComponent } from './app/login/reset.component';//glazaror resetcomponent

import { ScoringComponent } from './app/scoring/scoring.component';//glazaror ScoringComponent sprint2

import { NgIdleModule } from '@ng-idle/core';
import { MomentModule } from 'angular2-moment'; // optional, provides moment-style pipes for date formatting

import { AudioCargaModule } from './app/audiocarga/audiocarga.module';//Sprint 3 - AudioCargaModule
import { VentaAudioDetailModule } from './app/detalleVentaAudio/ventaAudioDetail.module';//Sprint 3 - VentaAudioDetailModule

import { SaleConditionComponent } from './app/saleCondition/salecondition.component';//Sprint 6 - SaleCondition

import { AccionesComponent } from './app/acciones/acciones.component';//Sprint 8 - UX
import { ContactoComponent } from './app/contacto/contacto.component';//Sprint 8 - UX
import { ProgressBarService } from './app/services/progressbar.service';//Sprint 8 - UX
import { JqueryAnimation } from './app/services/globaljqueryanimation.service'

import { ContactoInstalacionComponent } from './app/contactoinstalacion/contactoinstalacion.component';//Sprint 8 - UX
import { DatosClienteComponent } from './app/datoscliente/datoscliente.component';//Sprint 8 - UX
import { CancelSaleService } from '../src/app/services/cancelsaleservice'

import { HistoricoComponent } from './app/historico/historico.component'; //Sprint 16 - Historico Ventas - Trazabilidad
import { ResultadosComponent } from './app/resultados/resultados.component'; //Sprint 16 - Resultados Ventas - Trazabilidad
import { TrackingComponent } from './app/tracking/tracking.component'; //Sprint 17 - Tracking Ventas - Trazabilidad

import { InicioOutComponent } from './app/outInicio/inicioOut.component'; //Sprint 18 - Out
import { SvaOutComponent } from './app/outSva/svaOut.component';
import { DireccionOutComponent } from './app/outDireccion/direccionOut.component';
import { OfferingOutComponent } from './app/outOffering/offeringOut.component';
import { OfertasBiOutComponent } from './app/outOfertasBi/ofertasBiOut.component'; //Sprint 18 - Out
import { ScoringOutComponent } from './app/outScoring/scoringOut.component'; //Sprint 18 - Out
import { ContratosOutComponent } from './app/outContratos/contratosOut.component'; //Sprint 18 - Out
import { CondicionOutComponent } from './app/outCondicion/condicionOut.component'; //Sprint 18 - Out
import { ResumenOutComponent } from './app/outResumen/resumenOut.component'; //Sprint 18 - Out
import { ValidacionOutComponent } from './app/outValidacion/validacionOut.component'; //Sprint 18 - Out
import { CampanasOutComponent } from './app/outCampanas/campanasOut.component'; //Sprint 18 - Out
import { ContactoInstalacionOutComponent } from './app/outContactoInstalacion/contactoinstalacionOut.component'; //Sprint 18 - Out
import { CloseOutComponent } from './app/outClose/closeOut.component'; //Sprint 18 - Out

import { audiooutcomponent } from './app/outaudio/audioout.component'; //Sprint 18 - Out Audio
import { audiooutdetallecomponent } from './app/outaudiodetalle/audiooutdetalle.component'; //Sprint 18 - Out Audio
import { AgendamientoComponent } from './app/agendamiento/agendamiento.component'; //Sprint 18 - Out Audio


//**************************** Out Asesor migraciones ****************************
 
import { principaloutcomponent } from './app/outprincipal/principalout.component';  
import { migraoutevaluarcomponent } from './app/outmigraevaluar/migraoutevaluar.component';  
import { migraoutparquecomponent } from './app/outmigraparque/migraoutparque.component';  
import { migraoutofferingcomponent } from './app/outmigraoffering/migraoutoffering.component';  
import { migraoutsvacomponent } from './app/outmigrasva/migraoutsva.component';  
import { migraoutcondicionescomponent } from './app/outmigracondiciones/migraoutcondiciones.component';  
import { migraoutresumencomponent } from './app/outmigraresumen/migraoutresumen.component';  
import { migraoutrenieccomponent } from './app/outmigrareniec/migraoutreniec.component';  
import { migraoutlecturacomponent} from './app/outmigralecturacontrato/migraoutlectura.component';  

//**************************** FIN Out Asesor migraciones ****************************


/*********************  Pruebas   ****************************/
import { pruebadeconceptocomponent} from './app/pruebadeconcepto/pruebadeconcepto.component';  
/*********************  Pruebas   ****************************/

// +++++++ documenttypecomponent
import { documenttypecomponent} from './app/documenttype/documenttype.component';  
import { ReportesComponent} from './app/reportes/reportes.component';  
// +++++++ documenttypecomponent

//Sprint 24 RUC
import {SearchRucComponent} from './app/searchRuc/searchRuc.component';
import {ExcelService} from './app/services/sharedServices/excel.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';

@NgModule({
  imports: [BrowserModule,FormsModule,
      AppRoutingModule, HttpModule, MomentModule, NgIdleModule.forRoot(), AudioCargaModule /*sprint 3*/,
       VentaAudioDetailModule /*sprint 3*/,AngularMultiSelectModule],

  declarations: [AppComponent, HomeComponent, LoginComponent, SearchUserComponent, SaleProcessComponent, ParkComponent, OfferingComponent, 
    SvaComponent, SalesReportDetailComponent, VentaComponent, SearchProduct, SearchCampaign, 
    SearchProductType, SalesSummaryComponent, ContractComponent, DireccionComponent, 
    ValidReniecComponent, CloseComponent, Capitalize, ChangePasswordComponent, RegisterComponent, 
    CancelSaleComponent, ResetComponent, ScoringComponent, SaleConditionComponent, AccionesComponent, 
    ContactoComponent, ContactoInstalacionComponent, DatosClienteComponent, 
    CampanasComponent,HistoricoComponent, ResultadosComponent, TrackingComponent,
    InicioOutComponent,OfertasBiOutComponent,ScoringOutComponent,ContratosOutComponent,
    SvaOutComponent, DireccionOutComponent, OfferingOutComponent, CondicionOutComponent, 
    ResumenOutComponent, ValidacionOutComponent,CampanasOutComponent,ContactoInstalacionOutComponent,CloseOutComponent, audiooutcomponent, audiooutdetallecomponent, AgendamientoComponent,
    principaloutcomponent,migraoutevaluarcomponent,migraoutparquecomponent,migraoutofferingcomponent,migraoutsvacomponent,
    migraoutcondicionescomponent,migraoutresumencomponent,migraoutrenieccomponent,migraoutlecturacomponent,pruebadeconceptocomponent,documenttypecomponent,ReportesComponent,SearchRucComponent],


  providers: [HttpService, UserService, LoginService, LocalStorageService, MessageService, BlackListService,AgendamientoService, AuthGuard, ProgressBarService, JqueryAnimation, CancelSaleService,ReporteService,ExcelService,
    {
      provide: APP_INITIALIZER,
      useFactory: a => () => a.checkUser(),
      deps: [LoginService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
