import { Component, OnInit, OnDestroy, ChangeDetectorRef, HostListener } from '@angular/core';
import { Router, NavigationEnd, Event, ActivatedRoute } from '@angular/router';
import { User } from './app/model/user';

import { UserService } from './app/services/user.service';
import { LoginService } from './app/services/login.service';
import { HttpService } from './app/services/http.service';
import { BlackListService } from './app/services/blacklist.service';
import { AgendamientoService } from './app/services/agendamiento.service';
import { Subscription } from 'rxjs/Subscription';
import { LocalStorageService } from './app/services/ls.service';
import { OrderService } from './app/services/order.service';

import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import * as globals from './app/services/globals';

//glazaror se adiciona import para el model de order
import { Order } from './app/model/order';
 

import { Location } from '@angular/common';
import { CancelSaleService } from './app/services/cancelsaleservice';
import { Parameters } from './app/model/parameters';

var $ = require('jquery');

@Component({
	selector: 'my-app',
	templateUrl: './app.template.html',
	providers: [OrderService]
})
export class AppComponent implements OnInit, OnDestroy {
	subscriptionBtnCancelSale: Subscription
	dataStatus: any;
	subscription: Subscription;
	user: User;
	uploads: any[] = [];
	idleState = 'INIT';
	porcentaje: String;
	timedOut = false;
	sessionMessage = '';
	customReason: any;
	anotherReason: boolean;
	errorOption: boolean;
	errorCustomReason: boolean;
	errorSending: boolean;

	//datos cancel
	cancelList: any;
	selectedOption: any;
	name_text: any;
	cancelDescription: any;
	webOrder: any;
	loading: any;
	order: Order;
 

	anotherReasonText: string;
	showCancelSale: boolean = false

	redirectToHome: boolean = false
	goToAudioCarga: boolean = true
	permiso: string;


	showModalCancelSale: boolean = false;
	/*PERMISOS*/
	showVenderLink: boolean = false;
	showReportesLink: boolean = false;
	showAudiosLink: boolean = false;
	showTrackLink: boolean = false;
	/*FIN PERMISOS*/

	showCancelSaleLink: boolean = false;
	showGoBackLink: boolean = false;
	showProgressBar: boolean = false;
	showModalCloseSession: boolean = false;
	currentURL: string = "";
	pageToGo: string = "";
	granted: boolean = true;
	accesos: any;
	accesosOtorgados: string[];
	currentRoute: string;
	canalEntidad: Parameters;
	flujo: String;

	constructor(private ref: ChangeDetectorRef,
		private router: Router,
		private userService: UserService,
		private httpService: HttpService,
		private loginService: LoginService,
		private ls: LocalStorageService,
		private blackList: BlackListService,
		private agendamiento: AgendamientoService,
		private idle: Idle,
		private orderService: OrderService,
		private _location: Location,
		private r: ActivatedRoute,
		private cancelSaleService: CancelSaleService) {

		this.user = new User();
		let me = this;
		this.goToAudioCarga = false
		this.orderService = orderService;
		window.onbeforeunload = function (e) {
			if (me.uploads.length > 0) {
				return 'Es posible que los cambios no se guarden.';
			}
		};
		this.ls.order$.subscribe(o => {
			if (o && o.user) {
				this.user = o.user;
			}
		});
		this.subscription = this.userService.userChanged$.subscribe(data => {
			let order = JSON.parse(localStorage.getItem('order'));
			let token = localStorage.getItem('token');
			if (order && order.user && order.user.name) {
				this.user = order.user;
				//console.log(this.user)
			} else if (token) {
				this.onClickLogout();
			}
		});

		this.httpService._eventBus.subscribe(data => {
			if (data.reason === 'upload') {
				//console.log(data);
				let currentUploads = [];
				let found = false;
				this.uploads.forEach(function (el, i, arr) {
					if (el.id == data.id) {
						found = true;
						if (data.value != 100) {
							currentUploads.push(data);
						}
					} else if (data.value != 100) {
						currentUploads.push(el);
					}
				});
				if (!found && data.value != 100) {
					currentUploads.push(data);
				}
				this.uploads = currentUploads;
				this.ref.detectChanges();
			}
		});

		this.accesos = {}
		this.currentRoute = '';
		this.accesos['venta'] = []
		this.accesos['ventaout'] = []
		this.accesos['audioout'] = []
		this.accesos['audioin'] = []
		this.accesos['reporte'] = []
		this.accesos['track'] = []

		this.accesos['venta'][0] = "/acciones"
		this.accesos['venta'][1] = "/contacto"
		this.accesos['venta'][2] = "/searchUser"
		this.accesos['venta'][3] = "/saleProcess/park"
		this.accesos['venta'][4] = "/direccion"
		this.accesos['venta'][5] = "/campanas"
		this.accesos['venta'][6] = "/offering"
		this.accesos['venta'][7] = "/sva"
		this.accesos['venta'][8] = "/saleProcess/salecondition"
		this.accesos['venta'][9] = "/saleProcess/reniec"
		this.accesos['venta'][10] = "/saleProcess/salesSummary"
		this.accesos['venta'][11] = "/saleProcess/contract"
		this.accesos['venta'][12] = "/contactoinstalacion"
		this.accesos['venta'][13] = "/agendamiento"
		this.accesos['venta'][14] = "/saleProcess/close"
		this.accesos['venta'][15] = "/saleProcess/searchRuc" // Sprint 24 RUC
		this.accesos['venta'][16] = "/saleProcess/datoscliente" // Sprint 24 RUC

		this.accesos['ventaout'][0] = "/principalout"
		this.accesos['ventaout'][1] = "/inicioOut"
		this.accesos['ventaout'][2] = "/ofertasBiOut"
		this.accesos['ventaout'][3] = "/scoringOut"
		this.accesos['ventaout'][4] = "/direccionOut"
		this.accesos['ventaout'][5] = "/campanasOut"
		this.accesos['ventaout'][6] = "/offeringOut"
		this.accesos['ventaout'][7] = "/svaOut"
		this.accesos['ventaout'][8] = "/condicionOut"
		this.accesos['ventaout'][9] = "/validacionOut"
		this.accesos['ventaout'][10] = "/resumenOut"
		this.accesos['ventaout'][11] = "/contratosOut"
		this.accesos['ventaout'][12] = "/contactoinstalacionOut"
		this.accesos['ventaout'][13] = "/closeOut"
		this.accesos['ventaout'][14] = "/migraoutevaluar"
		this.accesos['ventaout'][15] = "/migraoutparque"
		this.accesos['ventaout'][16] = "/migraoutoffering"
		this.accesos['ventaout'][17] = "/migraoutsva"
		this.accesos['ventaout'][18] = "/saleProcess/searchRuc" // Sprint 24 RUC
		this.accesos['ventaout'][19] = "/saleProcess/datoscliente" // Sprint 24 RUC

		this.accesos['audioout'][0] = "/audioout"
		this.accesos['audioout'][1] = "/audiooutdetalle"

		this.accesos['audioin'][0] = "/audiocarga"
		this.accesos['audioin'][1] = "/home/venta/detail"

		this.accesos['track'][0] = "/historico"
		this.accesos['track'][1] = "/tracking"
		this.accesos['track'][2] = "/resultados"

		this.accesos['reporte'][0] = "/reportes/opCom"
		this.accesos['reporte'][1] = "/reportes/estado"
		this.accesos['reporte'][2] = "/reportes/producto"
		this.accesos['reporte'][3] = "/reportes/asesor"

	}

	backClicked() {
		//console.log("Validacion Duplicado-Ingreso al Regresar al Contrato" + this.ls.getData().ventaEcha)
		if (this.ls.getData().ventaEcha) {
			alert("La venta esta Realizada no puede retroceder.")
		} else {
			this._location.back();
		}
		//this._location.back();
	}


	vender_redireccion() {
		this.router.navigate(['/acciones']);
	}
	home_redireccion() {
		$("#myBtn").addClass("none");
		this.router.navigate(['/home']);
	}
	session() {
		// sets an idle timeout of 5 seconds, for testing purposes.
		this.idle.setIdle(globals.SESSION_IDLE);
		// sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
		this.idle.setTimeout(globals.SESSION_TIMEOUT);
		// sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
		this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

		this.idle.onIdleEnd.subscribe(() => {
			this.idleState = 'ACTIVE';
			$('#sessionModal').modal('hide');
		});
		this.idle.onTimeout.subscribe(() => {
			this.idleState = 'TIMEOUT';
			this.sessionMessage = 'La sesión ha expirado!';
			this.timedOut = true;
			this.onClickLogout();
			$('#sessionModal').modal('hide');
			$('#timeoutModal').modal('show');
		});
		this.idle.onIdleStart.subscribe(() => {
			this.idleState = 'IDLE';
			$('#sessionModal').modal('show');
		});
		this.idle.onTimeoutWarning.subscribe((countdown) => {
			this.idleState = 'You will time out in ' + countdown + ' seconds!';
			this.sessionMessage = 'La sesión va a finalizar en ' + countdown + ' segundos!. Desea mantenerse conectado?.';
		});

		this.reset();


	}

	cargarDatosCancelacionVenta() {
		this.cancelList = [];
		this.cancelList = this.orderService.responseCancelList();
	}

	sendReason(event) {
		this.selectedOption = event;
		if (this.selectedOption.id == this.cancelList.length) {
			this.anotherReason = true;
		} else {
			this.customReason = null;
			this.anotherReason = false;
		}
		this.cancelDescription = this.selectedOption.value;


		let reason_position = $("#reason").val()


		$("#incompleto_alert").addClass("incompleto");
		$("#incompleto_alert").html("Debes seleccionar una categoría.");

		if (reason_position == "4: Object") {
			$("#motivo").removeClass("none");
			$("#motivo_2").removeClass("none");
		}
		else {
			$("#motivo").addClass("none");
			$("#motivo_2").addClass("none");
		}
	}
	reset() {
		this.idle.watch();
		this.idleState = 'ACTIVE';
		this.timedOut = false;
	}

	onClickLogout() {
		 this.idle.stop();
		this.order = this.ls.getData();

		let atis=  this.order.user.userId


		this.loginService.logout(atis, 'WEB').subscribe(
			data => {
				if(data.statusText==="OK"){
					// recordar retirar el producción los log 
					console.log(data)
				}else{
					// recordar retirar el producción los log 

					console.log("error OK")
				}
			}, (err) => {
					// recordar retirar el producción los log 
				console.log(err);
			});



		 this.user = new User();
	  this.showModalCloseSession = false;
		 this.router.navigate(['/login'], {});
	}

	//glazaror cambio de clave
	onClickChangePassword() {
		let order: Order = this.ls.getData();
		//Para que se puso resetPwd = 1?
		order.cambiaContrasenaDemanda = true;
		this.ls.setData(order);
		this.router.navigate(['/changePassword'], {});
	}

	ngOnInit() {
		$(window).ready(() => {
			$('#reportes').show()
		})
		// popup de cancelar venta
		this.anotherReason = false;
		this.selectedOption = 0;
		this.cargarDatosCancelacionVenta();
		$('#reason').prop('selectedIndex', 0);

		this.loadRouterLinks();
		//console.log(this.user);

		//this.validaracceso()
	}

	ngAfterViewInit() {
		this.subscriptionBtnCancelSale = this.cancelSaleService.getStatus()
			.subscribe(status => {
				{
					this.dataStatus = status,
						this.showCancelSaleLink = this.dataStatus
				}
			})
	}

	ngOnDestroy() {
		this.subscription.unsubscribe();
		this.subscription.unsubscribe();
	}

	inicio() {
		let order = JSON.parse(localStorage.getItem('order'));
		//console.log("order " + JSON.stringify(order))
		if (order != null) {
			//console.log("this order in app.component " + this.order.status)
			if (order.status != 'N') {
				$('#cancel').modal('show');
			} else {
				this.router.navigate(['/home']);
			}
		}
	}

	// Sprint 3 - carga pantalla de subida de audios
	cargarPantallaSubidaAudios() {
		let order = JSON.parse(localStorage.getItem('order'));
		if (order.status != 'N') {
			$('#cancel').modal('show');
		} else {
			this.router.navigate(['/audiocarga']);
		}
	}

	computeWidth(v) {
		return v + '%';
	}

	//cancel
	ok() {
		localStorage.removeItem('codigoInterConect');
		localStorage.removeItem('rentaInterConect');
		let reason_position_ = this.cancelDescription;
		let vacio_razon = this.anotherReasonText;

		if ($("#reason").val() == 0) {
			$("#incompleto_alert").removeClass("incompleto");
			$("#incompleto_alert").html("Debes seleccionar una categoría.");
			//console.log("completa selección");
		} else {
			if (this.selectedOption.id == 4) {
				if (vacio_razon == "") {
					$("#incompleto_alert").removeClass("incompleto");
					$("#incompleto_alert").html("Debes completar el motivo de cancelación.");
					//console.log("Debes completar el motivo de cancelación.");
				} else {
					reason_position_ = vacio_razon;
					this.errorOption = false;
					this.errorSending = false;
					this.errorCustomReason = false;
					if (this.selectedOption.id == 0 || this.selectedOption == 0 || this.selectedOption.value == null) {
						this.errorOption = true;
						$("#reason").focus();
						//console.log("entro")
					} else {
						this.errorOption = false;
						if (this.selectedOption.id == this.cancelList.length) {
							if (this.anotherReasonText != null && typeof (this.anotherReasonText) != 'undefined' && !this.isEmpty(this.anotherReasonText)) {
								if (this.anotherReasonText.trim().length == 0) {
									this.errorCustomReason = true;
									//console.log("entro2222")
								} else {
									this.errorCustomReason = false;
									this.orderData();
								}
							} else {
								this.errorCustomReason = true;
							}
						} else {
							this.orderData();
						}
					}
				}
			} else {
				this.errorOption = false;
				this.errorSending = false;
				this.errorCustomReason = false;
				if (this.selectedOption.id == 0 || this.selectedOption == 0 || this.selectedOption.value == null) {
					this.errorOption = true;
					$("#reason").focus();
				} else {
					this.errorOption = false;
					if (this.selectedOption.id == this.cancelList.length) {
						if (this.customReason != null && typeof (this.customReason) != 'undefined' && !this.isEmpty(this.customReason)) {
							if (this.customReason.trim().length == 0) {
								this.errorCustomReason = true;
							} else {
								this.errorCustomReason = false;
								this.orderData();
							}
						} else {
							this.errorCustomReason = true;
						}
					} else {
						this.orderData();
					}
				}
				$("#incompleto_alert").addClass("incompleto");
			}
		}
	}

	isEmpty(o) {
		for (var i in o) { return false; }
		return true;
	}

	orderData() {
		this.webOrder = {};
		this.order = this.ls.getData();
		this.webOrder.userId = this.order.user.userId;
		this.webOrder.customer = this.order.customer;
		if (this.order.product != null && this.order.type != globals.ORDER_ALTA_NUEVA) {
			this.webOrder.phone = this.order.product.phone;
			this.webOrder.department = this.order.product.department;
			this.webOrder.province = this.order.product.province;
			this.webOrder.district = this.order.product.district;
			this.webOrder.address = this.order.product.address;
			this.webOrder.latitudeInstallation = this.order.product.coordinateY ? this.order.product.coordinateY : 0;
			this.webOrder.longitudeInstallation = this.order.product.coordinateX ? this.order.product.coordinateX : 0;
			this.webOrder.addressAdditionalInfo = this.order.product.additionalAddress ? this.order.product.additionalAddress : null;
		}
		if (this.order.selectedOffering != null) {
			this.webOrder.product = this.order.selectedOffering;
		}
		if (this.cancelDescription == 'Otras razones') {
			this.webOrder.cancelDescription = this.anotherReasonText;
		} else {
			this.webOrder.cancelDescription = this.cancelDescription;
		}
		this.webOrder.sva = [];
		if (this.order.status != "C") {
			this.webOrder.type = this.order.type;
		}
		if (this.order.status == "S" || this.order.status == "R" || this.order.status == "T") {
			this.webOrder.disaffiliationWhitePagesGuide = this.order.disaffiliationWhitePagesGuide;
			this.webOrder.affiliationElectronicInvoice = this.order.affiliationElectronicInvoice;
			this.webOrder.affiliationDataProtection = this.order.affiliationDataProtection;
			this.webOrder.publicationWhitePagesGuide = this.order.publicationWhitePagesGuide;
			this.webOrder.shippingContractsForEmail = this.order.shippingContractsForEmail;
		}
		if (this.order.sva != null) {
			for (let i = 0; i < this.order.sva.length; i++) {
				let sva = this.order.sva[i];
				this.webOrder.sva.push(sva.id);
			}
		}

		this.loading = true;

		// Sprint 7... si es que ya se tiene un id de venta generado entonces enviamos este id en la cancelacion de venta
		if (this.order.id != null && this.order.id != '') {
			this.webOrder.orderId = this.order.id;
		}

		this.orderService.cancelSale(this.webOrder).subscribe(
			data => {
				if (data.responseCode == "00") {
					this.errorSending = false;
					this.showModalCancelSale = false;
					this.redirect();
				} else {
					this.errorSending = true;
					//console.log("Error---- cancelar")

					let estado_ = localStorage.getItem('estado_');

					$("#incompleto_alert").removeClass("incompleto");
					$("#incompleto_alert").html("Ocurrio un error al cancelar la venta: " + data.responseData);

					if (estado_ == "reporte") {
						this.router.navigate(['/home']);
					} else {
						if (this.goToAudioCarga) {
							this.router.navigate(['/audiocarga']);
						} else {
							let capta = localStorage.getItem("capta")
							if (capta == 'capta') {
								this.router.navigate(['/principalout']);
							} else {
								this.router.navigate(['/acciones']);
							}
							this.showModalCancelSale = false;
						}
					}


				}
				this.loading = false;
			},
			err => {
				this.loading = false;
				//console.log(err);
			}
		);
	}

	closeModal() {
		$('#cancel1').modal('hide');
	}
	//reportes
	validateView() {
		let ruta = this.router
		let url = ruta.url
		//console.log("validateview1 ")
		if (url == "/acciones" || url == "/saleProcess/close" || url == "/audiocarga") {
			ruta.navigate(["/acciones"])
		} else {
			$('#cancel').modal('show');
			//console.log("validateview2 ")
			this.showMyModal()
		}
	}

	//vender
	vistaactiva() {
		// Get the modal
		var modal = document.getElementById('myModal');
	}

	showMyModal() {
		//$("#myModal").prop("style", "visibility: display: block !important; z-index: 500 !important;")
		//$('#myModal').modal('show');
	}

	closeSession() {
		let ruta = this.router
		let url = this.router.url
		if (this.validateUrls(url)) {
			$("#pop_close_session").modal("show")
		} else {
			this.onClickLogout()
		}
	}

	goToAudiosTray() {
		//console.log("bandeja ")
		let ruta = this.router
		let url = this.router.url
		if (this.validateUrls(url)) {
			this.goToAudioCarga = true
			$("#myModal").modal("show")
		} else {
			ruta.navigate(["/audiocarga"])
		}
	}
	validateUrls(url: string) {
		if (url != "/home") {
			if (url != "/saleProcess/close") {
				if (url != "/acciones") {
					return true
				} else {
					return false
				}
			} else {
				return false
			}
		} else {
			return false
		}
	}

	loadRouterLinks() {
		// router
		this.router.events.subscribe((event: Event) => {
			if (event instanceof NavigationEnd) {
				this.currentURL = event.url;

				if (event.url == "/audioout" || event.url == "/reportes") {
					this.enableLinksOutPage();

				} else if (event.url == "/acciones" || event.url == "/tracking" || event.url == "/historico" || event.url == "/principalout") {
					this.enableLinksAccionesPage();

				} else if (event.url == "/contacto" || event.url == "/searchUser" || event.url == "/resultados") {
					this.enableLinksContactoAndSearchUserPages();

				} else if (event.url == "/home") {
					this.enableLinksHomePage();

				} else if (event.url == "/audiocarga") {
					this.enableLinksAccionesPage();

				} else if ("/direccion" == this.currentURL || "/offering" == this.currentURL || "/sva" == this.currentURL
					|| "/saleProcess/salecondition" == this.currentURL || "/saleProcess/reniec" == this.currentURL
					|| "/saleProcess/salesSummary" == this.currentURL || "/saleProcess/contract" == this.currentURL
					|| "/contactoinstalacion" == this.currentURL || "/saleProcess/close" == this.currentURL
					|| "/saleProcess/datoscliente" == this.currentURL || "/saleProcess/park" == this.currentURL
					|| "/campanas" == this.currentURL || "/inicioOut" == this.currentURL || "/ofertasBiOut" == this.currentURL || "/scoringOut" == this.currentURL || "/direccionOut" == this.currentURL
					|| "/campanaOut" == this.currentURL || "/offeringOut" == this.currentURL || "/svaOut" == this.currentURL
					|| "/condicionOut" == this.currentURL || "/validacionOut" == this.currentURL || "/resumenOut" == this.currentURL
					|| "/contratosOut" == this.currentURL || "/contactoinstalacionOut" == this.currentURL
					|| "/closeOut" == this.currentURL || "/migraoutevaluar" == this.currentURL || "/migraoutparque" == this.currentURL
					|| "/migraoutparque" == this.currentURL || "/migraoutoffering" == this.currentURL || "/migraoutsva" == this.currentURL
					|| "/migraoutcondiciones" == this.currentURL || "/migraoutresumen" == this.currentURL || "/migraoutreniec" == this.currentURL
					|| "/migraoutlectura" == this.currentURL || "/saleProcess/searchRuc" == this.currentURL) {

					this.enableLinksOtherPages();

				} else if (event.url == "/audiooutdetalle") {// Canal out
					this.enableLinkkBack();
				}

			}
		})
	}

	clickOpcionMenu(pageToGoNew, type) {

		this.pageToGo = pageToGoNew;
		$('#reason').prop('selectedIndex', -1);
		this.anotherReasonText = "";
		this.selectedOption = 0;

		//dependiendo del this.currentURL
		if ("/acciones" == this.currentURL || "/historico" == this.currentURL || "/tracking" == this.currentURL || "/resultados" == this.currentURL || "/principalout" == this.currentURL || "/reportes" == this.currentURL || "/audioout" == this.currentURL || "/audiooutdetalle" == this.currentURL) {
			//nada
			this.showModalCancelSale = false;
			this.redirect(type);
		} else if ("/contacto" == this.currentURL || "/audiocarga" == this.currentURL || "/home" == this.currentURL || this.currentURL.startsWith("/home/venta") || this.currentURL.startsWith("/home/venta/detail")) {
			//redireccionamos
			this.redirect();
		} else if ("/searchUser" == this.currentURL || "/ofertasBiOut" == this.currentURL || "/inicioOut" == this.currentURL) {
			//si es que ya se identifico al cliente entonces tenemos q mostrar la ventana de cancelacion de venta
			let order = this.ls.getData();
			if (order.customer && order.customer.documentNumber) {
				//insertar logica para mostrar popup de cancelar venta
				this.showModalCancelSale = true;
			} else {
				//de lo contrario simplemente debemos redireccionar
				this.redirect();
			}
			//en el resto de pantallas del flujo de venta se debe mostrar siempre el popup de cancelar venta
		} else if ("/direccion" == this.currentURL || "/offering" == this.currentURL || "/sva" == this.currentURL
			|| "/saleProcess/salecondition" == this.currentURL || "/saleProcess/reniec" == this.currentURL
			|| "/saleProcess/salesSummary" == this.currentURL || "/saleProcess/contract" == this.currentURL
			|| "/contactoinstalacion" == this.currentURL || "/saleProcess/datoscliente" == this.currentURL
			|| "/saleProcess/park" == this.currentURL || "/scoringOut" == this.currentURL || "/direccionOut" == this.currentURL
			|| "/campanaOut" == this.currentURL || "/offeringOut" == this.currentURL || "/svaOut" == this.currentURL
			|| "/condicionOut" == this.currentURL || "/validacionOut" == this.currentURL || "/resumenOut" == this.currentURL
			|| "/contratosOut" == this.currentURL || "/contactoinstalacionOut" == this.currentURL
			|| "/migraoutparque" == this.currentURL || "/migraoutoffering" == this.currentURL
			|| "/migraoutevaluar" == this.currentURL || "/saleProcess/searchRuc" == this.currentURL) {
			//insertar logica para mostrar popup de cancelar venta
			this.showModalCancelSale = true;
		} else if ("/saleProcess/close" == this.currentURL) {
			//redireccionamos
			this.redirect();
		}
		else if ("/campanas" == this.currentURL) {
			this.showModalCancelSale = true;
		} else if (this.currentURL.indexOf("/reportes") > -1) {
			this.redirect(type);
		}

	}

	showModalCloseSessionDialog() {
		this.showModalCloseSession = true;
	}

	cancelModalCloseSessionDialog() {
		this.showModalCloseSession = false;
	}

	saveCancelacionVenta() {
		//pendiente pasar logica de la funcion "ok()"
		//por el momento solo funciona la redireccion
		this.showModalCancelSale = false;
		this.redirect();

	}

	redirect(type = null) {

		//Por el momento veo esta forma, llamar al order
		this.order = JSON.parse(localStorage.getItem('order'));

		if ((this.pageToGo == "VENTA" && this.currentURL == "/acciones") || (this.pageToGo == "VENTA" && this.currentURL == "/principalout")) {
			//Estamos en la misma pagina... por las puras redireccionar 
			return;
		}
		if (this.pageToGo == "REPORTES" && this.currentURL == "/historico") {
			//Estamos en la misma pagina... por las puras redireccionar
			return;
		}
		if ((this.pageToGo == "BANDEJA" && this.currentURL == "/audiocarga") || (this.pageToGo == "BANDEJA" && this.currentURL == "/audioout")) {
			//Estamos en la misma pagina... por las puras redireccionar
			return;
		}

		this.showProgressBar = false;
		if ("VENTA" == this.pageToGo) {
				if (localStorage.getItem("dataRed")) {
					localStorage.removeItem("dataRed")
				}
			if (this.flujo=="REMOTO OUT") {
				this.router.navigate(['/principalout']);
			} else {
				this.router.navigate(['/acciones']);
			}
		} else if ("REPORTES" == this.pageToGo) {
			if (localStorage.getItem("dataRed")) {
				localStorage.removeItem("dataRed")
			}
			this.router.navigate(['/historico']);
		} else if ("TRACK" == this.pageToGo) {
			if (localStorage.getItem("dataRed")) {
				localStorage.removeItem("dataRed")
			}
			this.router.navigate(['/tracking']);
		} else if ("BANDEJA" == this.pageToGo) {
			if (localStorage.getItem("dataRed")) {
				localStorage.removeItem("dataRed")
			}
			if (this.flujo=="REMOTO OUT") {
				this.router.navigate(['/audioout']);
			} else {
				this.router.navigate(['/audiocarga']);
			}
		} else if ("REPORTESVENTA" == this.pageToGo) {
			this.router.navigate(['/reportes', type]);
		}
	}

	cancelCancelacionVenta() {
		this.showModalCancelSale = false;
	}

	enableLinksAccionesPage() {
		this.showCancelSaleLink = false;
		this.showGoBackLink = false;
		this.showProgressBar = false;
	}

	enableLinksOutPage() {
		this.showCancelSaleLink = false;
		this.showGoBackLink = false;
		this.showProgressBar = false;

	}

	enableLinkkBack() {
		this.showGoBackLink = true;
		this.showProgressBar = true;
	}

	enableLinksContactoAndSearchUserPages() {
		this.showCancelSaleLink = false;
		this.showGoBackLink = true;
		this.showProgressBar = true;
	}

	enableLinksHomePage() {
		this.showCancelSaleLink = false;
		this.showGoBackLink = false;
		this.showProgressBar = false;
	}

	enableLinksOtherPages() {
		this.showCancelSaleLink = true;
		this.showGoBackLink = true;
		this.showProgressBar = true;
	}

	validaracceso() {

		/*se actualiza la equivalencia*/
		this.canalEntidad = JSON.parse(localStorage.getItem('canalEntidad'))
		//console.log(this.user);
		
		if (this.canalEntidad !== null ){
			this.flujo=this.user.typeFlujoTipificacion;
		}

		var base = ["/", "/login"]
		this.accesosOtorgados = []
		var _scope = this

		this.router.events.subscribe((e) => {
			if (e instanceof NavigationEnd) {
				this.currentRoute = e.url
			}
		});
		//console.log(this.user)

		_scope.granted = true

		$("#data").addClass("temp")

		this.userService.niveles(this.user.niveles, this.user.channel)
		.subscribe(data => {

			
			_scope.showVenderLink = data.responseData.f_venta;
			_scope.showReportesLink = data.responseData.f_repventa;	
			_scope.showAudiosLink = data.responseData.f_bandeja;	
			_scope.showTrackLink = data.responseData.f_reptrack;	
			
			if(_scope.showVenderLink){
				if(_scope.flujo=="REMOTO OUT")
					_scope.accesosOtorgados=_scope.accesosOtorgados.concat(_scope.accesos['ventaout'])
				else	
					_scope.accesosOtorgados=_scope.accesosOtorgados.concat(_scope.accesos['venta'])
			}if(_scope.showReportesLink){
				_scope.accesosOtorgados=_scope.accesosOtorgados.concat(_scope.accesos['reporte'])
			}if(_scope.showAudiosLink){
				if(_scope.flujo=="REMOTO OUT")
					_scope.accesosOtorgados=_scope.accesosOtorgados.concat(_scope.accesos['audioout'])
				else	
					_scope.accesosOtorgados=_scope.accesosOtorgados.concat(_scope.accesos['audioin'])
			}if(_scope.showTrackLink){
				_scope.accesosOtorgados=_scope.accesosOtorgados.concat(_scope.accesos['track'])			
			}

				if (!base.includes(this.currentRoute)) {
					if (_scope.accesosOtorgados.filter(x => this.currentRoute.includes(x)).length == 0) {
						_scope.granted = false
					} else {
						_scope.granted = true
					}
				} else {
					_scope.granted = true
				}

				$("#data").removeClass("temp")

				//console.log(_scope.currentRoute);
				//console.log("acceso "+_scope.granted);		
				//console.log(_scope.accesosOtorgados);		
			}, error => {
				$("#data").removeClass("temp")
			});
	}
}   