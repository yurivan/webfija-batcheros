import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './app/home/home.component';
import { LoginComponent } from './app/login/login.component';
import { RegisterComponent } from './app/login/register.component';
import { SearchUserComponent } from './app/searchUser/searchuser.component';
import { ParkComponent } from './app/park/park.component';
import { SaleProcessComponent } from './app/saleProcess/saleprocess.component';
import { OfferingComponent } from './app/offering/offering.component';
import { SvaComponent } from './app/sva/sva.component';
import { SalesReportDetailComponent } from './app/salesReport/salesReportDetail.component';
import { AuthGuard } from './app/authGuard.injectable';
import { VentaComponent } from './app/venta/venta.component';
import { DireccionComponent } from './app/direccion/direccion.component';

import { CampanasComponent } from './app/campanas/campanas.component';

import { ValidReniecComponent } from './app/reniec/validReniec.component';
import { SalesSummaryComponent } from './app/salesSummary/salesSummary.component';
import { ContractComponent } from './app/contract/contract.component';
import { CloseComponent } from './app/contract/close.component';
import { ChangePasswordComponent } from './app/changePassword/changePassword.component';
import { ResetComponent } from './app/login/reset.component';//glazaror nuevo componente
import { ScoringComponent } from './app/scoring/scoring.component';//glazaror nuevo componente sprint2
import { AudioCargaComponent } from './app/audiocarga/audiocarga.component';// Sprint3 - se adiciona AudioCargaComponent
import { VentaAudioDetailComponent } from './app/detalleVentaAudio/ventaAudioDetail.component';// Sprint3 - se adiciona VentaAudioDetailComponent
import { SaleConditionComponent } from './app/saleCondition/salecondition.component';// Sprint6 - se adiciona SaleConditionComponent

import { AccionesComponent } from './app/acciones/acciones.component';// Sprint8 - UX
import { ContactoComponent } from './app/contacto/contacto.component';// Sprint8 - UX
import { ContactoInstalacionComponent } from './app/contactoinstalacion/contactoinstalacion.component';// Sprint8 - UX
import { DatosClienteComponent } from './app/datoscliente/datoscliente.component';// Sprint8 - UX

import { HistoricoComponent } from './app/historico/historico.component'; //Sprint 16 - Historico Ventas - Trazabilidad
import { ResultadosComponent } from './app/resultados/resultados.component'; //Sprint 16 - Resultados Ventas - Trazabilidad
import { TrackingComponent } from './app/tracking/tracking.component'; //Sprint 17 - Traking Ventas - Trazabilidad

import { InicioOutComponent } from './app/outInicio/inicioOut.component'; //Sprint 18 - Out
import { DireccionOutComponent } from './app/outDireccion/direccionOut.component'; //Sprint 18 - Out
import { SvaOutComponent } from './app/outSva/svaOut.component'; //Sprint 18 - Out
import { OfferingOutComponent } from './app/outOffering/offeringOut.component'; //Sprint 18 - Out

import { OfertasBiOutComponent } from './app/outOfertasBi/ofertasBiOut.component'; //Sprint 18 - Out
import { ScoringOutComponent } from './app/outScoring/scoringOut.component'; //Sprint 18 - Out
import { ContratosOutComponent } from './app/outContratos/contratosOut.component'; //Sprint 18 - Out
import { CondicionOutComponent } from './app/outCondicion/condicionOut.component'; //Sprint 18 - Out
import { ResumenOutComponent } from './app/outResumen/resumenOut.component'; //Sprint 18 - Out
import { ValidacionOutComponent } from './app/outValidacion/validacionOut.component'; //Sprint 18 - Out
import { CampanasOutComponent } from './app/outCampanas/campanasOut.component'; //Sprint 18 - Out
import { ContactoInstalacionOutComponent } from './app/outContactoInstalacion/contactoinstalacionOut.component'; //Sprint 18 - Out
import { CloseOutComponent } from './app/outClose/closeOut.component'; //Sprint 18 - Out

import { audiooutcomponent } from './app/outaudio/audioout.component'; //Sprint 18 - Out Audio
import { audiooutdetallecomponent } from './app/outaudiodetalle/audiooutdetalle.component'; //Sprint 18 - Out Audio
import { AgendamientoComponent } from './app/agendamiento/agendamiento.component'; //Sprint 18 - Out Audio

//**************************** Out Asesor migraciones ****************************
 
import { principaloutcomponent } from './app/outprincipal/principalout.component';  
import { migraoutevaluarcomponent } from './app/outmigraevaluar/migraoutevaluar.component';  
import { migraoutparquecomponent } from './app/outmigraparque/migraoutparque.component';  
import { migraoutofferingcomponent } from './app/outmigraoffering/migraoutoffering.component';  
import { migraoutsvacomponent } from './app/outmigrasva/migraoutsva.component';  
import { migraoutcondicionescomponent } from './app/outmigracondiciones/migraoutcondiciones.component';  
import { migraoutresumencomponent } from './app/outmigraresumen/migraoutresumen.component';  
import { migraoutrenieccomponent } from './app/outmigrareniec/migraoutreniec.component';  
import { migraoutlecturacomponent} from './app/outmigralecturacontrato/migraoutlectura.component';  


//**************************** fin Out Asesor migraciones ****************************


/*********************  Pruebas   ****************************/
import { pruebadeconceptocomponent} from './app/pruebadeconcepto/pruebadeconcepto.component';  

// +++++++ documenttypecomponent
import { documenttypecomponent} from './app/documenttype/documenttype.component';  
// +++++++ documenttypecomponent

import { ReportesComponent} from './app/reportes/reportes.component';  

/*********************  Pruebas   ****************************/

//Sprint 24 RUC
import {SearchRucComponent} from './app/searchRuc/searchRuc.component';

const APP_ROUTES: Routes = [
	{ path: '', redirectTo: '/login', pathMatch: 'full' },
	{ path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
	{ path: 'login', component: LoginComponent },//login LoginComponent
	{ path: 'register', component: RegisterComponent },
	{ path: 'reset', component: ResetComponent },//glazaror ResetComponent

	{ path: 'changePassword', component: ChangePasswordComponent },
	{ path: 'home/venta', component: VentaComponent, canActivate: [AuthGuard] },
	{ path: 'acciones', component: AccionesComponent, canActivate: [AuthGuard] },// Sprint 8... UX
	{ path: 'contacto', component: ContactoComponent, canActivate: [AuthGuard] },// Sprint 8... UX
	{ path: 'contactoinstalacion', component: ContactoInstalacionComponent, canActivate: [AuthGuard] },// Sprint 8... UX
	{ path: 'searchUser', component: SearchUserComponent, canActivate: [AuthGuard] },
	{ path: 'audiocarga', component: AudioCargaComponent, canActivate: [AuthGuard] },// Sprint3

	{ path: 'historico', component: HistoricoComponent, canActivate: [AuthGuard] },// Sprint 16
	{ path: 'resultados', component: ResultadosComponent, canActivate: [AuthGuard] },// Sprint 16
	{ path: 'tracking', component: TrackingComponent, canActivate: [AuthGuard] },// Sprint 17


	/********************* Sprint 18 - Flujo OUT*********************/
	{ path: 'inicioOut', component: InicioOutComponent, canActivate: [AuthGuard] },
	{ path: 'ofertasBiOut', component: OfertasBiOutComponent, canActivate: [AuthGuard] },
	{ path: 'scoringOut', component: ScoringOutComponent, canActivate: [AuthGuard] },
	{ path: 'direccionOut', component: DireccionOutComponent, canActivate: [AuthGuard] },
	{ path: 'campanasOut', component: CampanasOutComponent, canActivate: [AuthGuard] },
	{ path: 'offeringOut', component: OfferingOutComponent, canActivate: [AuthGuard] },
	{ path: 'svaOut', component: SvaOutComponent, canActivate: [AuthGuard] },
	{ path: 'condicionOut', component: CondicionOutComponent, canActivate: [AuthGuard] },
	{ path: 'validacionOut', component: ValidacionOutComponent, canActivate: [AuthGuard] },
	{ path: 'resumenOut', component: ResumenOutComponent, canActivate: [AuthGuard] },
	{ path: 'contratosOut', component: ContratosOutComponent, canActivate: [AuthGuard] },
	{ path: 'contactoinstalacionOut', component: ContactoInstalacionOutComponent, canActivate: [AuthGuard] },
	{ path: 'closeOut', component: CloseOutComponent, canActivate: [AuthGuard] },
	/****************************************************************/

	/********************* Sprint 18 - Flujo OUT BACK*********************/
	{ path: 'audioout', component: audiooutcomponent, canActivate: [AuthGuard] },
	{ path: 'audiooutdetalle', component: audiooutdetallecomponent, canActivate: [AuthGuard] },
	{ path: 'agendamiento', component: AgendamientoComponent, canActivate: [AuthGuard] },
	/****************************************************************/


	/*********************  Pruebas   ****************************/

	{ path: 'pruebadeconcepto', component: pruebadeconceptocomponent, canActivate: [AuthGuard] },

	/*********************  Pruebas   ****************************/

	// +++++++ documenttypecomponent
	{ path: 'documenttype', component: documenttypecomponent, canActivate: [AuthGuard] },
    // +++++++ documenttypecomponent
	
	/********************* Sprint 20 - Flujo OUT ASESOR MIGRACIONES*********************/
	 
	   
	{ path: 'principalout', component: principaloutcomponent, canActivate: [AuthGuard] },
	{ path: 'migraoutevaluar', component: migraoutevaluarcomponent, canActivate: [AuthGuard] }, 
	{ path: 'migraoutparque', component: migraoutparquecomponent, canActivate: [AuthGuard] },
	{ path: 'migraoutoffering', component: migraoutofferingcomponent, canActivate: [AuthGuard] },

	{ path: 'migraoutsva', component: migraoutsvacomponent, canActivate: [AuthGuard] },
	{ path: 'migraoutcondiciones', component: migraoutcondicionescomponent, canActivate: [AuthGuard] },
	{ path: 'migraoutresumen', component: migraoutresumencomponent, canActivate: [AuthGuard] },
	{ path: 'migraoutreniec', component: migraoutrenieccomponent, canActivate: [AuthGuard] },
	{ path: 'migraoutlectura', component: migraoutlecturacomponent, canActivate: [AuthGuard] },
	
	
	/****************************************************************/
	{
		path: 'saleProcess', component: SaleProcessComponent, children: [
			{ path: 'park', component: ParkComponent, canActivate: [AuthGuard] },
			/*{path: 'offering', component: OfferingComponent, canActivate: [AuthGuard]},*/
			/*{path: 'sva', component: SvaComponent, canActivate: [AuthGuard]},*/
			{ path: 'salesSummary', component: SalesSummaryComponent, canActivate: [AuthGuard] },
			{ path: 'reniec', component: ValidReniecComponent, canActivate: [AuthGuard] },
			{ path: 'contract', component: ContractComponent, canActivate: [AuthGuard] },
			/*{path: 'direccion', component: DireccionComponent, canActivate: [AuthGuard]},*/
			{ path: 'close', component: CloseComponent, canActivate: [AuthGuard] },
			{ path: 'scoring', component: ScoringComponent },//glazaror ScoringComponent sprint2
			{ path: 'salecondition', component: SaleConditionComponent },// Sprint6 - Condiciones del contrato,
			{ path: 'datoscliente', component: DatosClienteComponent },// Sprint9 - UX Captura datos cliente con carne ext, pasaporte, ruc
			{ path: 'searchRuc', component: SearchRucComponent, canActivate: [AuthGuard] }, //Sprint 24 RUC
		], canActivate: [AuthGuard]
	},

	{ path: 'offering', component: OfferingComponent, canActivate: [AuthGuard] },//Sprint 8
	{ path: 'direccion', component: DireccionComponent, canActivate: [AuthGuard] },//Sprint 8

	{ path: 'campanas', component: CampanasComponent, canActivate: [AuthGuard] },//Sprint 8

	{ path: 'sva', component: SvaComponent, canActivate: [AuthGuard] },//Sprint 8

	{ path: 'home/venta/detail', component: SalesReportDetailComponent },
	{ path: 'audiocarga/ventaaudiodetail', component: VentaAudioDetailComponent },// Sprint 3
	{ path: 'reportes/:type', component: ReportesComponent },// Sprint 23
	{ path: '**', redirectTo: '/home' }


];

@NgModule({
	imports: [RouterModule.forRoot(APP_ROUTES)],
	exports: [RouterModule]
})

export class AppRoutingModule { }
